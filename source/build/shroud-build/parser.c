// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _41EndLineTable()
{
    int _0, _1, _2;
    

    /** 	LineTable = append(LineTable, -2)*/
    Append(&_38LineTable_17039, _38LineTable_17039, -2);

    /** end procedure*/
    return;
    ;
}


void _41CreateTopLevel()
{
    int _28611 = NOVALUE;
    int _28609 = NOVALUE;
    int _28607 = NOVALUE;
    int _28605 = NOVALUE;
    int _28603 = NOVALUE;
    int _28601 = NOVALUE;
    int _28599 = NOVALUE;
    int _28597 = NOVALUE;
    int _28595 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	SymTab[TopLevelSub][S_NUM_ARGS] = 0*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38TopLevelSub_16953 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _28595 = NOVALUE;

    /** 	SymTab[TopLevelSub][S_TEMPS] = 0*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38TopLevelSub_16953 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_TEMPS_16643))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TEMPS_16643)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_TEMPS_16643);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _28597 = NOVALUE;

    /** 	SymTab[TopLevelSub][S_CODE] = {}*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38TopLevelSub_16953 + ((s1_ptr)_2)->base);
    RefDS(_22663);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_CODE_16610))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_CODE_16610);
    _1 = *(int *)_2;
    *(int *)_2 = _22663;
    DeRef(_1);
    _28599 = NOVALUE;

    /** 	SymTab[TopLevelSub][S_LINETAB] = {}*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38TopLevelSub_16953 + ((s1_ptr)_2)->base);
    RefDS(_22663);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_LINETAB_16633))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_LINETAB_16633)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_LINETAB_16633);
    _1 = *(int *)_2;
    *(int *)_2 = _22663;
    DeRef(_1);
    _28601 = NOVALUE;

    /** 	SymTab[TopLevelSub][S_FIRSTLINE] = 1*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38TopLevelSub_16953 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_FIRSTLINE_16638))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FIRSTLINE_16638)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_FIRSTLINE_16638);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _28603 = NOVALUE;

    /** 	SymTab[TopLevelSub][S_REFLIST] = {}*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38TopLevelSub_16953 + ((s1_ptr)_2)->base);
    RefDS(_22663);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 24);
    _1 = *(int *)_2;
    *(int *)_2 = _22663;
    DeRef(_1);
    _28605 = NOVALUE;

    /** 	SymTab[TopLevelSub][S_NREFS] = 1*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38TopLevelSub_16953 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _28607 = NOVALUE;

    /** 	SymTab[TopLevelSub][S_RESIDENT_TASK] = 1*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38TopLevelSub_16953 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 25);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _28609 = NOVALUE;

    /** 	SymTab[TopLevelSub][S_SAVED_PRIVATES] = {}*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38TopLevelSub_16953 + ((s1_ptr)_2)->base);
    RefDS(_22663);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 26);
    _1 = *(int *)_2;
    *(int *)_2 = _22663;
    DeRef(_1);
    _28611 = NOVALUE;

    /** 	Start_block( PROC, TopLevelSub )*/
    _68Start_block(27, _38TopLevelSub_16953);

    /** end procedure*/
    return;
    ;
}


void _41CheckForUndefinedGotoLabels()
{
    int _28626 = NOVALUE;
    int _28625 = NOVALUE;
    int _28621 = NOVALUE;
    int _28619 = NOVALUE;
    int _28617 = NOVALUE;
    int _28615 = NOVALUE;
    int _28614 = NOVALUE;
    int _28613 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(goto_delay) do*/
    if (IS_SEQUENCE(_38goto_delay_17076)){
            _28613 = SEQ_PTR(_38goto_delay_17076)->length;
    }
    else {
        _28613 = 1;
    }
    {
        int _i_55226;
        _i_55226 = 1;
L1: 
        if (_i_55226 > _28613){
            goto L2; // [8] 104
        }

        /** 		if not equal(goto_delay[i],"") then*/
        _2 = (int)SEQ_PTR(_38goto_delay_17076);
        _28614 = (int)*(((s1_ptr)_2)->base + _i_55226);
        if (_28614 == _22663)
        _28615 = 1;
        else if (IS_ATOM_INT(_28614) && IS_ATOM_INT(_22663))
        _28615 = 0;
        else
        _28615 = (compare(_28614, _22663) == 0);
        _28614 = NOVALUE;
        if (_28615 != 0)
        goto L3; // [27] 97
        _28615 = NOVALUE;

        /** 			line_number = goto_line[i][1] -- tell compiler the correct line number*/
        _2 = (int)SEQ_PTR(_41goto_line_55132);
        _28617 = (int)*(((s1_ptr)_2)->base + _i_55226);
        _2 = (int)SEQ_PTR(_28617);
        _38line_number_16947 = (int)*(((s1_ptr)_2)->base + 1);
        if (!IS_ATOM_INT(_38line_number_16947)){
            _38line_number_16947 = (long)DBL_PTR(_38line_number_16947)->dbl;
        }
        _28617 = NOVALUE;

        /** 			gline_number = goto_line[i][1] -- tell compiler the correct line number*/
        _2 = (int)SEQ_PTR(_41goto_line_55132);
        _28619 = (int)*(((s1_ptr)_2)->base + _i_55226);
        _2 = (int)SEQ_PTR(_28619);
        _38gline_number_16951 = (int)*(((s1_ptr)_2)->base + 1);
        if (!IS_ATOM_INT(_38gline_number_16951)){
            _38gline_number_16951 = (long)DBL_PTR(_38gline_number_16951)->dbl;
        }
        _28619 = NOVALUE;

        /** 			ThisLine = goto_line[i][2] -- tell compiler the correct line number*/
        _2 = (int)SEQ_PTR(_41goto_line_55132);
        _28621 = (int)*(((s1_ptr)_2)->base + _i_55226);
        DeRef(_46ThisLine_49482);
        _2 = (int)SEQ_PTR(_28621);
        _46ThisLine_49482 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_46ThisLine_49482);
        _28621 = NOVALUE;

        /** 			bp = length(ThisLine)*/
        if (IS_SEQUENCE(_46ThisLine_49482)){
                _46bp_49486 = SEQ_PTR(_46ThisLine_49482)->length;
        }
        else {
            _46bp_49486 = 1;
        }

        /** 				CompileErr(156, {goto_delay[i]})*/
        _2 = (int)SEQ_PTR(_38goto_delay_17076);
        _28625 = (int)*(((s1_ptr)_2)->base + _i_55226);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_28625);
        *((int *)(_2+4)) = _28625;
        _28626 = MAKE_SEQ(_1);
        _28625 = NOVALUE;
        _46CompileErr(156, _28626, 0);
        _28626 = NOVALUE;
L3: 

        /** 	end for*/
        _i_55226 = _i_55226 + 1;
        goto L1; // [99] 15
L2: 
        ;
    }

    /** end procedure*/
    return;
    ;
}


void _41PushGoto()
{
    int _28627 = NOVALUE;
    int _0, _1, _2;
    

    /** 	goto_stack = append(goto_stack, {goto_addr, goto_list, goto_labels, goto_delay, goto_line, goto_ref, label_block, goto_init })*/
    _1 = NewS1(8);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_41goto_addr_55134);
    *((int *)(_2+4)) = _41goto_addr_55134;
    RefDS(_38goto_list_17077);
    *((int *)(_2+8)) = _38goto_list_17077;
    RefDS(_41goto_labels_55133);
    *((int *)(_2+12)) = _41goto_labels_55133;
    RefDS(_38goto_delay_17076);
    *((int *)(_2+16)) = _38goto_delay_17076;
    RefDS(_41goto_line_55132);
    *((int *)(_2+20)) = _41goto_line_55132;
    RefDS(_41goto_ref_55136);
    *((int *)(_2+24)) = _41goto_ref_55136;
    RefDS(_41label_block_55137);
    *((int *)(_2+28)) = _41label_block_55137;
    Ref(_41goto_init_55139);
    *((int *)(_2+32)) = _41goto_init_55139;
    _28627 = MAKE_SEQ(_1);
    RefDS(_28627);
    Append(&_41goto_stack_55135, _41goto_stack_55135, _28627);
    DeRefDS(_28627);
    _28627 = NOVALUE;

    /** 	goto_addr = {}*/
    RefDS(_22663);
    DeRefDS(_41goto_addr_55134);
    _41goto_addr_55134 = _22663;

    /** 	goto_list = {}*/
    RefDS(_22663);
    DeRefDS(_38goto_list_17077);
    _38goto_list_17077 = _22663;

    /** 	goto_labels = {}*/
    RefDS(_22663);
    DeRefDS(_41goto_labels_55133);
    _41goto_labels_55133 = _22663;

    /** 	goto_delay = {}*/
    RefDS(_22663);
    DeRefDS(_38goto_delay_17076);
    _38goto_delay_17076 = _22663;

    /** 	goto_line = {}*/
    RefDS(_22663);
    DeRefDS(_41goto_line_55132);
    _41goto_line_55132 = _22663;

    /** 	goto_ref = {}*/
    RefDS(_22663);
    DeRefDS(_41goto_ref_55136);
    _41goto_ref_55136 = _22663;

    /** 	label_block = {}*/
    RefDS(_22663);
    DeRefDS(_41label_block_55137);
    _41label_block_55137 = _22663;

    /** 	goto_init = map:new()*/
    _0 = _29new(690);
    DeRef(_41goto_init_55139);
    _41goto_init_55139 = _0;

    /** end procedure*/
    return;
    ;
}


void _41PopGoto()
{
    int _28654 = NOVALUE;
    int _28652 = NOVALUE;
    int _28651 = NOVALUE;
    int _28649 = NOVALUE;
    int _28648 = NOVALUE;
    int _28646 = NOVALUE;
    int _28645 = NOVALUE;
    int _28643 = NOVALUE;
    int _28642 = NOVALUE;
    int _28640 = NOVALUE;
    int _28639 = NOVALUE;
    int _28637 = NOVALUE;
    int _28636 = NOVALUE;
    int _28634 = NOVALUE;
    int _28633 = NOVALUE;
    int _28631 = NOVALUE;
    int _28630 = NOVALUE;
    int _0, _1, _2;
    

    /** 	CheckForUndefinedGotoLabels()*/
    _41CheckForUndefinedGotoLabels();

    /** 	goto_addr   = goto_stack[$][1]*/
    if (IS_SEQUENCE(_41goto_stack_55135)){
            _28630 = SEQ_PTR(_41goto_stack_55135)->length;
    }
    else {
        _28630 = 1;
    }
    _2 = (int)SEQ_PTR(_41goto_stack_55135);
    _28631 = (int)*(((s1_ptr)_2)->base + _28630);
    DeRef(_41goto_addr_55134);
    _2 = (int)SEQ_PTR(_28631);
    _41goto_addr_55134 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_41goto_addr_55134);
    _28631 = NOVALUE;

    /** 	goto_list   = goto_stack[$][2]*/
    if (IS_SEQUENCE(_41goto_stack_55135)){
            _28633 = SEQ_PTR(_41goto_stack_55135)->length;
    }
    else {
        _28633 = 1;
    }
    _2 = (int)SEQ_PTR(_41goto_stack_55135);
    _28634 = (int)*(((s1_ptr)_2)->base + _28633);
    DeRef(_38goto_list_17077);
    _2 = (int)SEQ_PTR(_28634);
    _38goto_list_17077 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_38goto_list_17077);
    _28634 = NOVALUE;

    /** 	goto_labels = goto_stack[$][3]*/
    if (IS_SEQUENCE(_41goto_stack_55135)){
            _28636 = SEQ_PTR(_41goto_stack_55135)->length;
    }
    else {
        _28636 = 1;
    }
    _2 = (int)SEQ_PTR(_41goto_stack_55135);
    _28637 = (int)*(((s1_ptr)_2)->base + _28636);
    DeRef(_41goto_labels_55133);
    _2 = (int)SEQ_PTR(_28637);
    _41goto_labels_55133 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_41goto_labels_55133);
    _28637 = NOVALUE;

    /** 	goto_delay  = goto_stack[$][4]*/
    if (IS_SEQUENCE(_41goto_stack_55135)){
            _28639 = SEQ_PTR(_41goto_stack_55135)->length;
    }
    else {
        _28639 = 1;
    }
    _2 = (int)SEQ_PTR(_41goto_stack_55135);
    _28640 = (int)*(((s1_ptr)_2)->base + _28639);
    DeRef(_38goto_delay_17076);
    _2 = (int)SEQ_PTR(_28640);
    _38goto_delay_17076 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_38goto_delay_17076);
    _28640 = NOVALUE;

    /** 	goto_line   = goto_stack[$][5]*/
    if (IS_SEQUENCE(_41goto_stack_55135)){
            _28642 = SEQ_PTR(_41goto_stack_55135)->length;
    }
    else {
        _28642 = 1;
    }
    _2 = (int)SEQ_PTR(_41goto_stack_55135);
    _28643 = (int)*(((s1_ptr)_2)->base + _28642);
    DeRef(_41goto_line_55132);
    _2 = (int)SEQ_PTR(_28643);
    _41goto_line_55132 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_41goto_line_55132);
    _28643 = NOVALUE;

    /** 	goto_ref    = goto_stack[$][6]*/
    if (IS_SEQUENCE(_41goto_stack_55135)){
            _28645 = SEQ_PTR(_41goto_stack_55135)->length;
    }
    else {
        _28645 = 1;
    }
    _2 = (int)SEQ_PTR(_41goto_stack_55135);
    _28646 = (int)*(((s1_ptr)_2)->base + _28645);
    DeRef(_41goto_ref_55136);
    _2 = (int)SEQ_PTR(_28646);
    _41goto_ref_55136 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_41goto_ref_55136);
    _28646 = NOVALUE;

    /** 	label_block = goto_stack[$][7]*/
    if (IS_SEQUENCE(_41goto_stack_55135)){
            _28648 = SEQ_PTR(_41goto_stack_55135)->length;
    }
    else {
        _28648 = 1;
    }
    _2 = (int)SEQ_PTR(_41goto_stack_55135);
    _28649 = (int)*(((s1_ptr)_2)->base + _28648);
    DeRef(_41label_block_55137);
    _2 = (int)SEQ_PTR(_28649);
    _41label_block_55137 = (int)*(((s1_ptr)_2)->base + 7);
    Ref(_41label_block_55137);
    _28649 = NOVALUE;

    /** 	goto_init   = goto_stack[$][8]*/
    if (IS_SEQUENCE(_41goto_stack_55135)){
            _28651 = SEQ_PTR(_41goto_stack_55135)->length;
    }
    else {
        _28651 = 1;
    }
    _2 = (int)SEQ_PTR(_41goto_stack_55135);
    _28652 = (int)*(((s1_ptr)_2)->base + _28651);
    DeRef(_41goto_init_55139);
    _2 = (int)SEQ_PTR(_28652);
    _41goto_init_55139 = (int)*(((s1_ptr)_2)->base + 8);
    Ref(_41goto_init_55139);
    _28652 = NOVALUE;

    /** 	goto_stack = remove( goto_stack, length( goto_stack ) )*/
    if (IS_SEQUENCE(_41goto_stack_55135)){
            _28654 = SEQ_PTR(_41goto_stack_55135)->length;
    }
    else {
        _28654 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_41goto_stack_55135);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_28654)) ? _28654 : (long)(DBL_PTR(_28654)->dbl);
        int stop = (IS_ATOM_INT(_28654)) ? _28654 : (long)(DBL_PTR(_28654)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_41goto_stack_55135), start, &_41goto_stack_55135 );
            }
            else Tail(SEQ_PTR(_41goto_stack_55135), stop+1, &_41goto_stack_55135);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_41goto_stack_55135), start, &_41goto_stack_55135);
        }
        else {
            assign_slice_seq = &assign_space;
            _41goto_stack_55135 = Remove_elements(start, stop, (SEQ_PTR(_41goto_stack_55135)->ref == 1));
        }
    }
    _28654 = NOVALUE;
    _28654 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


void _41EnterTopLevel(int _end_line_table_55292)
{
    int _28669 = NOVALUE;
    int _28668 = NOVALUE;
    int _28666 = NOVALUE;
    int _28665 = NOVALUE;
    int _28663 = NOVALUE;
    int _28661 = NOVALUE;
    int _28660 = NOVALUE;
    int _28658 = NOVALUE;
    int _28656 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	if CurrentSub then*/
    if (_38CurrentSub_16954 == 0)
    {
        goto L1; // [7] 59
    }
    else{
    }

    /** 		if end_line_table then*/
    if (_end_line_table_55292 == 0)
    {
        goto L2; // [12] 58
    }
    else{
    }

    /** 			EndLineTable()*/
    _41EndLineTable();

    /** 			SymTab[CurrentSub][S_LINETAB] = LineTable*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38CurrentSub_16954 + ((s1_ptr)_2)->base);
    RefDS(_38LineTable_17039);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_LINETAB_16633))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_LINETAB_16633)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_LINETAB_16633);
    _1 = *(int *)_2;
    *(int *)_2 = _38LineTable_17039;
    DeRef(_1);
    _28656 = NOVALUE;

    /** 			SymTab[CurrentSub][S_CODE] = Code*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38CurrentSub_16954 + ((s1_ptr)_2)->base);
    RefDS(_38Code_17038);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_CODE_16610))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_CODE_16610);
    _1 = *(int *)_2;
    *(int *)_2 = _38Code_17038;
    DeRef(_1);
    _28658 = NOVALUE;
L2: 
L1: 

    /** 	if length(goto_stack) then*/
    if (IS_SEQUENCE(_41goto_stack_55135)){
            _28660 = SEQ_PTR(_41goto_stack_55135)->length;
    }
    else {
        _28660 = 1;
    }
    if (_28660 == 0)
    {
        _28660 = NOVALUE;
        goto L3; // [66] 74
    }
    else{
        _28660 = NOVALUE;
    }

    /** 		PopGoto()*/
    _41PopGoto();
L3: 

    /** 	LineTable = SymTab[TopLevelSub][S_LINETAB]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28661 = (int)*(((s1_ptr)_2)->base + _38TopLevelSub_16953);
    DeRef(_38LineTable_17039);
    _2 = (int)SEQ_PTR(_28661);
    if (!IS_ATOM_INT(_38S_LINETAB_16633)){
        _38LineTable_17039 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_LINETAB_16633)->dbl));
    }
    else{
        _38LineTable_17039 = (int)*(((s1_ptr)_2)->base + _38S_LINETAB_16633);
    }
    Ref(_38LineTable_17039);
    _28661 = NOVALUE;

    /** 	Code = SymTab[TopLevelSub][S_CODE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28663 = (int)*(((s1_ptr)_2)->base + _38TopLevelSub_16953);
    DeRef(_38Code_17038);
    _2 = (int)SEQ_PTR(_28663);
    if (!IS_ATOM_INT(_38S_CODE_16610)){
        _38Code_17038 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    }
    else{
        _38Code_17038 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
    }
    Ref(_38Code_17038);
    _28663 = NOVALUE;

    /** 	previous_op = -1*/
    _38previous_op_17062 = -1;

    /** 	CurrentSub = TopLevelSub*/
    _38CurrentSub_16954 = _38TopLevelSub_16953;

    /** 	clear_last()*/
    _43clear_last();

    /** 	if length( branch_stack ) then*/
    if (IS_SEQUENCE(_41branch_stack_55121)){
            _28665 = SEQ_PTR(_41branch_stack_55121)->length;
    }
    else {
        _28665 = 1;
    }
    if (_28665 == 0)
    {
        _28665 = NOVALUE;
        goto L4; // [135] 169
    }
    else{
        _28665 = NOVALUE;
    }

    /** 		branch_list = branch_stack[$]*/
    if (IS_SEQUENCE(_41branch_stack_55121)){
            _28666 = SEQ_PTR(_41branch_stack_55121)->length;
    }
    else {
        _28666 = 1;
    }
    DeRef(_41branch_list_55120);
    _2 = (int)SEQ_PTR(_41branch_stack_55121);
    _41branch_list_55120 = (int)*(((s1_ptr)_2)->base + _28666);
    Ref(_41branch_list_55120);

    /** 		branch_stack = tail( branch_stack )*/
    if (IS_SEQUENCE(_41branch_stack_55121)){
            _28668 = SEQ_PTR(_41branch_stack_55121)->length;
    }
    else {
        _28668 = 1;
    }
    _28669 = _28668 - 1;
    _28668 = NOVALUE;
    {
        int len = SEQ_PTR(_41branch_stack_55121)->length;
        int size = (IS_ATOM_INT(_28669)) ? _28669 : (long)(DBL_PTR(_28669)->dbl);
        if (size <= 0) {
            DeRef(_41branch_stack_55121);
            _41branch_stack_55121 = MAKE_SEQ(NewS1(0));
        }
        else if (len <= size) {
            RefDS(_41branch_stack_55121);
            DeRef(_41branch_stack_55121);
            _41branch_stack_55121 = _41branch_stack_55121;
        }
        else Tail(SEQ_PTR(_41branch_stack_55121), len-size+1, &_41branch_stack_55121);
    }
    _28669 = NOVALUE;
L4: 

    /** end procedure*/
    return;
    ;
}


void _41LeaveTopLevel()
{
    int _28674 = NOVALUE;
    int _28672 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	branch_stack = append( branch_stack, branch_list )*/
    RefDS(_41branch_list_55120);
    Append(&_41branch_stack_55121, _41branch_stack_55121, _41branch_list_55120);

    /** 	branch_list = {}*/
    RefDS(_22663);
    DeRefDS(_41branch_list_55120);
    _41branch_list_55120 = _22663;

    /** 	PushGoto()*/
    _41PushGoto();

    /** 	LastLineNumber = -1*/
    _62LastLineNumber_24627 = -1;

    /** 	SymTab[TopLevelSub][S_LINETAB] = LineTable*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38TopLevelSub_16953 + ((s1_ptr)_2)->base);
    RefDS(_38LineTable_17039);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_LINETAB_16633))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_LINETAB_16633)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_LINETAB_16633);
    _1 = *(int *)_2;
    *(int *)_2 = _38LineTable_17039;
    DeRef(_1);
    _28672 = NOVALUE;

    /** 	SymTab[TopLevelSub][S_CODE] = Code*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38TopLevelSub_16953 + ((s1_ptr)_2)->base);
    RefDS(_38Code_17038);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_CODE_16610))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_CODE_16610);
    _1 = *(int *)_2;
    *(int *)_2 = _38Code_17038;
    DeRef(_1);
    _28674 = NOVALUE;

    /** 	LineTable = {}*/
    RefDS(_22663);
    DeRefDS(_38LineTable_17039);
    _38LineTable_17039 = _22663;

    /** 	Code = {}*/
    RefDS(_22663);
    DeRefDS(_38Code_17038);
    _38Code_17038 = _22663;

    /** 	previous_op = -1*/
    _38previous_op_17062 = -1;

    /** 	clear_last()*/
    _43clear_last();

    /** end procedure*/
    return;
    ;
}


void _41InitParser()
{
    int _0, _1, _2;
    

    /** 	goto_stack = {}*/
    RefDS(_22663);
    DeRef(_41goto_stack_55135);
    _41goto_stack_55135 = _22663;

    /** 	goto_labels = {}*/
    RefDS(_22663);
    DeRef(_41goto_labels_55133);
    _41goto_labels_55133 = _22663;

    /** 	label_block = {}*/
    RefDS(_22663);
    DeRef(_41label_block_55137);
    _41label_block_55137 = _22663;

    /** 	goto_ref = {}*/
    RefDS(_22663);
    DeRef(_41goto_ref_55136);
    _41goto_ref_55136 = _22663;

    /** 	goto_addr = {}*/
    RefDS(_22663);
    DeRef(_41goto_addr_55134);
    _41goto_addr_55134 = _22663;

    /** 	goto_line = {}*/
    RefDS(_22663);
    DeRef(_41goto_line_55132);
    _41goto_line_55132 = _22663;

    /** 	break_list = {}*/
    RefDS(_22663);
    DeRefi(_41break_list_55140);
    _41break_list_55140 = _22663;

    /** 	break_delay = {}*/
    RefDS(_22663);
    DeRef(_41break_delay_55141);
    _41break_delay_55141 = _22663;

    /** 	exit_list = {}*/
    RefDS(_22663);
    DeRefi(_41exit_list_55142);
    _41exit_list_55142 = _22663;

    /** 	exit_delay = {}*/
    RefDS(_22663);
    DeRef(_41exit_delay_55143);
    _41exit_delay_55143 = _22663;

    /** 	continue_list = {}*/
    RefDS(_22663);
    DeRefi(_41continue_list_55144);
    _41continue_list_55144 = _22663;

    /** 	continue_delay = {}*/
    RefDS(_22663);
    DeRef(_41continue_delay_55145);
    _41continue_delay_55145 = _22663;

    /** 	init_stack = {}*/
    RefDS(_22663);
    DeRefi(_41init_stack_55155);
    _41init_stack_55155 = _22663;

    /** 	CurrentSub = 0*/
    _38CurrentSub_16954 = 0;

    /** 	CreateTopLevel()*/
    _41CreateTopLevel();

    /** 	EnterTopLevel()*/
    _41EnterTopLevel(1);

    /** 	backed_up_tok = {}*/
    RefDS(_22663);
    DeRef(_41backed_up_tok_55129);
    _41backed_up_tok_55129 = _22663;

    /** 	loop_stack = {}*/
    RefDS(_22663);
    DeRefi(_41loop_stack_55156);
    _41loop_stack_55156 = _22663;

    /** 	stmt_nest = 0*/
    _41stmt_nest_55154 = 0;

    /** 	loop_labels = {}*/
    RefDS(_22663);
    DeRef(_41loop_labels_55150);
    _41loop_labels_55150 = _22663;

    /** 	if_labels = {}*/
    RefDS(_22663);
    DeRef(_41if_labels_55151);
    _41if_labels_55151 = _22663;

    /** 	if_stack = {}*/
    RefDS(_22663);
    DeRefi(_41if_stack_55157);
    _41if_stack_55157 = _22663;

    /** 	continue_addr = {}*/
    RefDS(_22663);
    DeRefi(_41continue_addr_55147);
    _41continue_addr_55147 = _22663;

    /** 	retry_addr = {}*/
    RefDS(_22663);
    DeRefi(_41retry_addr_55148);
    _41retry_addr_55148 = _22663;

    /** 	entry_addr = {}*/
    RefDS(_22663);
    DeRefi(_41entry_addr_55146);
    _41entry_addr_55146 = _22663;

    /** 	block_list = {}*/
    RefDS(_22663);
    DeRefi(_41block_list_55152);
    _41block_list_55152 = _22663;

    /** 	block_index = 0*/
    _41block_index_55153 = 0;

    /** 	param_num = -1*/
    _41param_num_55131 = -1;

    /** 	entry_stack = {}*/
    RefDS(_22663);
    DeRef(_41entry_stack_55149);
    _41entry_stack_55149 = _22663;

    /** 	goto_init = map:new()*/
    _0 = _29new(690);
    DeRef(_41goto_init_55139);
    _41goto_init_55139 = _0;

    /** end procedure*/
    return;
    ;
}


void _41NotReached(int _tok_55371, int _keyword_55372)
{
    int _28695 = NOVALUE;
    int _28694 = NOVALUE;
    int _28693 = NOVALUE;
    int _28692 = NOVALUE;
    int _28691 = NOVALUE;
    int _28690 = NOVALUE;
    int _28688 = NOVALUE;
    int _28687 = NOVALUE;
    int _28686 = NOVALUE;
    int _28685 = NOVALUE;
    int _28683 = NOVALUE;
    int _28682 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_tok_55371)) {
        _1 = (long)(DBL_PTR(_tok_55371)->dbl);
        if (UNIQUE(DBL_PTR(_tok_55371)) && (DBL_PTR(_tok_55371)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_tok_55371);
        _tok_55371 = _1;
    }

    /** 	if not find(tok, {END, ELSE, ELSIF, END_OF_FILE, CASE, IFDEF, ELSIFDEF, ELSEDEF}) then*/
    _1 = NewS1(8);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 402;
    *((int *)(_2+8)) = 23;
    *((int *)(_2+12)) = 414;
    *((int *)(_2+16)) = -21;
    *((int *)(_2+20)) = 186;
    *((int *)(_2+24)) = 407;
    *((int *)(_2+28)) = 408;
    *((int *)(_2+32)) = 409;
    _28682 = MAKE_SEQ(_1);
    _28683 = find_from(_tok_55371, _28682, 1);
    DeRefDS(_28682);
    _28682 = NOVALUE;
    if (_28683 != 0)
    goto L1; // [39] 135
    _28683 = NOVALUE;

    /** 		if equal(keyword, "goto") and find(tok, {LOOP, LABEL, WHILE}) then*/
    if (_keyword_55372 == _27035)
    _28685 = 1;
    else if (IS_ATOM_INT(_keyword_55372) && IS_ATOM_INT(_27035))
    _28685 = 0;
    else
    _28685 = (compare(_keyword_55372, _27035) == 0);
    if (_28685 == 0) {
        goto L2; // [48] 79
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 422;
    *((int *)(_2+8)) = 419;
    *((int *)(_2+12)) = 47;
    _28687 = MAKE_SEQ(_1);
    _28688 = find_from(_tok_55371, _28687, 1);
    DeRefDS(_28687);
    _28687 = NOVALUE;
    if (_28688 == 0)
    {
        _28688 = NOVALUE;
        goto L2; // [70] 79
    }
    else{
        _28688 = NOVALUE;
    }

    /** 			return*/
    DeRefDSi(_keyword_55372);
    return;
L2: 

    /** 		if equal(keyword, "abort()") and tok = LABEL then*/
    if (_keyword_55372 == _28689)
    _28690 = 1;
    else if (IS_ATOM_INT(_keyword_55372) && IS_ATOM_INT(_28689))
    _28690 = 0;
    else
    _28690 = (compare(_keyword_55372, _28689) == 0);
    if (_28690 == 0) {
        goto L3; // [85] 105
    }
    _28692 = (_tok_55371 == 419);
    if (_28692 == 0)
    {
        DeRef(_28692);
        _28692 = NOVALUE;
        goto L3; // [96] 105
    }
    else{
        DeRef(_28692);
        _28692 = NOVALUE;
    }

    /** 			return*/
    DeRefDSi(_keyword_55372);
    return;
L3: 

    /** 		Warning(218, not_reached_warning_flag,*/
    _2 = (int)SEQ_PTR(_35known_files_15596);
    _28693 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    Ref(_28693);
    _28694 = _55name_ext(_28693);
    _28693 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _28694;
    *((int *)(_2+8)) = _38line_number_16947;
    RefDS(_keyword_55372);
    *((int *)(_2+12)) = _keyword_55372;
    _28695 = MAKE_SEQ(_1);
    _28694 = NOVALUE;
    _46Warning(218, 512, _28695);
    _28695 = NOVALUE;
L1: 

    /** end procedure*/
    DeRefDSi(_keyword_55372);
    return;
    ;
}


void _41Forward_InitCheck(int _tok_55411, int _ref_55412)
{
    int _sym_55414 = NOVALUE;
    int _28701 = NOVALUE;
    int _28700 = NOVALUE;
    int _28699 = NOVALUE;
    int _28697 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if ref then*/
    if (_ref_55412 == 0)
    {
        goto L1; // [5] 83
    }
    else{
    }

    /** 		integer sym = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_55411);
    _sym_55414 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_sym_55414)){
        _sym_55414 = (long)DBL_PTR(_sym_55414)->dbl;
    }

    /** 		if tok[T_ID] = QUALIFIED_VARIABLE then*/
    _2 = (int)SEQ_PTR(_tok_55411);
    _28697 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _28697, 512)){
        _28697 = NOVALUE;
        goto L2; // [28] 50
    }
    _28697 = NOVALUE;

    /** 			set_qualified_fwd( SymTab[sym][S_FILE_NO] )*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28699 = (int)*(((s1_ptr)_2)->base + _sym_55414);
    _2 = (int)SEQ_PTR(_28699);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _28700 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _28700 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _28699 = NOVALUE;
    Ref(_28700);
    _62set_qualified_fwd(_28700);
    _28700 = NOVALUE;
L2: 

    /** 		ref = new_forward_reference( GLOBAL_INIT_CHECK, tok[T_SYM], GLOBAL_INIT_CHECK )*/
    _2 = (int)SEQ_PTR(_tok_55411);
    _28701 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_28701);
    _ref_55412 = _40new_forward_reference(109, _28701, 109);
    _28701 = NOVALUE;
    if (!IS_ATOM_INT(_ref_55412)) {
        _1 = (long)(DBL_PTR(_ref_55412)->dbl);
        if (UNIQUE(DBL_PTR(_ref_55412)) && (DBL_PTR(_ref_55412)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_55412);
        _ref_55412 = _1;
    }

    /** 		emit_op( GLOBAL_INIT_CHECK )*/
    _43emit_op(109);

    /** 		emit_addr( sym )*/
    _43emit_addr(_sym_55414);
L1: 

    /** end procedure*/
    DeRef(_tok_55411);
    return;
    ;
}


void _41InitCheck(int _sym_55439, int _ref_55440)
{
    int _28778 = NOVALUE;
    int _28777 = NOVALUE;
    int _28776 = NOVALUE;
    int _28775 = NOVALUE;
    int _28774 = NOVALUE;
    int _28773 = NOVALUE;
    int _28772 = NOVALUE;
    int _28771 = NOVALUE;
    int _28769 = NOVALUE;
    int _28767 = NOVALUE;
    int _28766 = NOVALUE;
    int _28764 = NOVALUE;
    int _28763 = NOVALUE;
    int _28762 = NOVALUE;
    int _28761 = NOVALUE;
    int _28760 = NOVALUE;
    int _28759 = NOVALUE;
    int _28758 = NOVALUE;
    int _28757 = NOVALUE;
    int _28756 = NOVALUE;
    int _28755 = NOVALUE;
    int _28754 = NOVALUE;
    int _28753 = NOVALUE;
    int _28752 = NOVALUE;
    int _28751 = NOVALUE;
    int _28749 = NOVALUE;
    int _28748 = NOVALUE;
    int _28747 = NOVALUE;
    int _28746 = NOVALUE;
    int _28745 = NOVALUE;
    int _28744 = NOVALUE;
    int _28743 = NOVALUE;
    int _28742 = NOVALUE;
    int _28741 = NOVALUE;
    int _28739 = NOVALUE;
    int _28738 = NOVALUE;
    int _28737 = NOVALUE;
    int _28736 = NOVALUE;
    int _28735 = NOVALUE;
    int _28734 = NOVALUE;
    int _28733 = NOVALUE;
    int _28732 = NOVALUE;
    int _28731 = NOVALUE;
    int _28730 = NOVALUE;
    int _28729 = NOVALUE;
    int _28728 = NOVALUE;
    int _28727 = NOVALUE;
    int _28726 = NOVALUE;
    int _28725 = NOVALUE;
    int _28724 = NOVALUE;
    int _28723 = NOVALUE;
    int _28722 = NOVALUE;
    int _28721 = NOVALUE;
    int _28720 = NOVALUE;
    int _28719 = NOVALUE;
    int _28718 = NOVALUE;
    int _28716 = NOVALUE;
    int _28715 = NOVALUE;
    int _28714 = NOVALUE;
    int _28713 = NOVALUE;
    int _28712 = NOVALUE;
    int _28711 = NOVALUE;
    int _28710 = NOVALUE;
    int _28709 = NOVALUE;
    int _28708 = NOVALUE;
    int _28707 = NOVALUE;
    int _28706 = NOVALUE;
    int _28705 = NOVALUE;
    int _28703 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	if sym < 0 or (SymTab[sym][S_MODE] = M_NORMAL and*/
    _28703 = (_sym_55439 < 0);
    if (_28703 != 0) {
        goto L1; // [11] 90
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28705 = (int)*(((s1_ptr)_2)->base + _sym_55439);
    _2 = (int)SEQ_PTR(_28705);
    _28706 = (int)*(((s1_ptr)_2)->base + 3);
    _28705 = NOVALUE;
    if (IS_ATOM_INT(_28706)) {
        _28707 = (_28706 == 1);
    }
    else {
        _28707 = binary_op(EQUALS, _28706, 1);
    }
    _28706 = NOVALUE;
    if (IS_ATOM_INT(_28707)) {
        if (_28707 == 0) {
            _28708 = 0;
            goto L2; // [33] 59
        }
    }
    else {
        if (DBL_PTR(_28707)->dbl == 0.0) {
            _28708 = 0;
            goto L2; // [33] 59
        }
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28709 = (int)*(((s1_ptr)_2)->base + _sym_55439);
    _2 = (int)SEQ_PTR(_28709);
    _28710 = (int)*(((s1_ptr)_2)->base + 4);
    _28709 = NOVALUE;
    if (IS_ATOM_INT(_28710)) {
        _28711 = (_28710 != 2);
    }
    else {
        _28711 = binary_op(NOTEQ, _28710, 2);
    }
    _28710 = NOVALUE;
    DeRef(_28708);
    if (IS_ATOM_INT(_28711))
    _28708 = (_28711 != 0);
    else
    _28708 = DBL_PTR(_28711)->dbl != 0.0;
L2: 
    if (_28708 == 0) {
        DeRef(_28712);
        _28712 = 0;
        goto L3; // [59] 85
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28713 = (int)*(((s1_ptr)_2)->base + _sym_55439);
    _2 = (int)SEQ_PTR(_28713);
    _28714 = (int)*(((s1_ptr)_2)->base + 4);
    _28713 = NOVALUE;
    if (IS_ATOM_INT(_28714)) {
        _28715 = (_28714 != 4);
    }
    else {
        _28715 = binary_op(NOTEQ, _28714, 4);
    }
    _28714 = NOVALUE;
    if (IS_ATOM_INT(_28715))
    _28712 = (_28715 != 0);
    else
    _28712 = DBL_PTR(_28715)->dbl != 0.0;
L3: 
    if (_28712 == 0)
    {
        _28712 = NOVALUE;
        goto L4; // [86] 502
    }
    else{
        _28712 = NOVALUE;
    }
L1: 

    /** 		if sym < 0 or ((SymTab[sym][S_SCOPE] != SC_PRIVATE and*/
    _28716 = (_sym_55439 < 0);
    if (_28716 != 0) {
        goto L5; // [96] 213
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28718 = (int)*(((s1_ptr)_2)->base + _sym_55439);
    _2 = (int)SEQ_PTR(_28718);
    _28719 = (int)*(((s1_ptr)_2)->base + 4);
    _28718 = NOVALUE;
    if (IS_ATOM_INT(_28719)) {
        _28720 = (_28719 != 3);
    }
    else {
        _28720 = binary_op(NOTEQ, _28719, 3);
    }
    _28719 = NOVALUE;
    if (IS_ATOM_INT(_28720)) {
        if (_28720 == 0) {
            DeRef(_28721);
            _28721 = 0;
            goto L6; // [118] 144
        }
    }
    else {
        if (DBL_PTR(_28720)->dbl == 0.0) {
            DeRef(_28721);
            _28721 = 0;
            goto L6; // [118] 144
        }
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28722 = (int)*(((s1_ptr)_2)->base + _sym_55439);
    _2 = (int)SEQ_PTR(_28722);
    _28723 = (int)*(((s1_ptr)_2)->base + 1);
    _28722 = NOVALUE;
    if (_28723 == _38NOVALUE_16800)
    _28724 = 1;
    else if (IS_ATOM_INT(_28723) && IS_ATOM_INT(_38NOVALUE_16800))
    _28724 = 0;
    else
    _28724 = (compare(_28723, _38NOVALUE_16800) == 0);
    _28723 = NOVALUE;
    DeRef(_28721);
    _28721 = (_28724 != 0);
L6: 
    if (_28721 != 0) {
        DeRef(_28725);
        _28725 = 1;
        goto L7; // [144] 208
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28726 = (int)*(((s1_ptr)_2)->base + _sym_55439);
    _2 = (int)SEQ_PTR(_28726);
    _28727 = (int)*(((s1_ptr)_2)->base + 4);
    _28726 = NOVALUE;
    if (IS_ATOM_INT(_28727)) {
        _28728 = (_28727 == 3);
    }
    else {
        _28728 = binary_op(EQUALS, _28727, 3);
    }
    _28727 = NOVALUE;
    if (IS_ATOM_INT(_28728)) {
        if (_28728 == 0) {
            DeRef(_28729);
            _28729 = 0;
            goto L8; // [166] 204
        }
    }
    else {
        if (DBL_PTR(_28728)->dbl == 0.0) {
            DeRef(_28729);
            _28729 = 0;
            goto L8; // [166] 204
        }
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28730 = (int)*(((s1_ptr)_2)->base + _sym_55439);
    _2 = (int)SEQ_PTR(_28730);
    _28731 = (int)*(((s1_ptr)_2)->base + 16);
    _28730 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28732 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_28732);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _28733 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _28733 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    _28732 = NOVALUE;
    if (IS_ATOM_INT(_28731) && IS_ATOM_INT(_28733)) {
        _28734 = (_28731 >= _28733);
    }
    else {
        _28734 = binary_op(GREATEREQ, _28731, _28733);
    }
    _28731 = NOVALUE;
    _28733 = NOVALUE;
    DeRef(_28729);
    if (IS_ATOM_INT(_28734))
    _28729 = (_28734 != 0);
    else
    _28729 = DBL_PTR(_28734)->dbl != 0.0;
L8: 
    DeRef(_28725);
    _28725 = (_28729 != 0);
L7: 
    if (_28725 == 0)
    {
        _28725 = NOVALUE;
        goto L9; // [209] 566
    }
    else{
        _28725 = NOVALUE;
    }
L5: 

    /** 			if sym < 0 or (SymTab[sym][S_INITLEVEL] = -1)*/
    _28735 = (_sym_55439 < 0);
    if (_28735 != 0) {
        _28736 = 1;
        goto LA; // [219] 243
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28737 = (int)*(((s1_ptr)_2)->base + _sym_55439);
    _2 = (int)SEQ_PTR(_28737);
    _28738 = (int)*(((s1_ptr)_2)->base + 14);
    _28737 = NOVALUE;
    if (IS_ATOM_INT(_28738)) {
        _28739 = (_28738 == -1);
    }
    else {
        _28739 = binary_op(EQUALS, _28738, -1);
    }
    _28738 = NOVALUE;
    if (IS_ATOM_INT(_28739))
    _28736 = (_28739 != 0);
    else
    _28736 = DBL_PTR(_28739)->dbl != 0.0;
LA: 
    if (_28736 != 0) {
        goto LB; // [243] 270
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28741 = (int)*(((s1_ptr)_2)->base + _sym_55439);
    _2 = (int)SEQ_PTR(_28741);
    _28742 = (int)*(((s1_ptr)_2)->base + 4);
    _28741 = NOVALUE;
    if (IS_ATOM_INT(_28742)) {
        _28743 = (_28742 != 3);
    }
    else {
        _28743 = binary_op(NOTEQ, _28742, 3);
    }
    _28742 = NOVALUE;
    if (_28743 == 0) {
        DeRef(_28743);
        _28743 = NOVALUE;
        goto L9; // [266] 566
    }
    else {
        if (!IS_ATOM_INT(_28743) && DBL_PTR(_28743)->dbl == 0.0){
            DeRef(_28743);
            _28743 = NOVALUE;
            goto L9; // [266] 566
        }
        DeRef(_28743);
        _28743 = NOVALUE;
    }
    DeRef(_28743);
    _28743 = NOVALUE;
LB: 

    /** 				if ref then*/
    if (_ref_55440 == 0)
    {
        goto LC; // [272] 375
    }
    else{
    }

    /** 					if sym > 0 and (SymTab[sym][S_SCOPE] = SC_UNDEFINED) then*/
    _28744 = (_sym_55439 > 0);
    if (_28744 == 0) {
        goto LD; // [281] 317
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28746 = (int)*(((s1_ptr)_2)->base + _sym_55439);
    _2 = (int)SEQ_PTR(_28746);
    _28747 = (int)*(((s1_ptr)_2)->base + 4);
    _28746 = NOVALUE;
    if (IS_ATOM_INT(_28747)) {
        _28748 = (_28747 == 9);
    }
    else {
        _28748 = binary_op(EQUALS, _28747, 9);
    }
    _28747 = NOVALUE;
    if (_28748 == 0) {
        DeRef(_28748);
        _28748 = NOVALUE;
        goto LD; // [304] 317
    }
    else {
        if (!IS_ATOM_INT(_28748) && DBL_PTR(_28748)->dbl == 0.0){
            DeRef(_28748);
            _28748 = NOVALUE;
            goto LD; // [304] 317
        }
        DeRef(_28748);
        _28748 = NOVALUE;
    }
    DeRef(_28748);
    _28748 = NOVALUE;

    /** 						emit_op(PRIVATE_INIT_CHECK)*/
    _43emit_op(30);
    goto LE; // [314] 369
LD: 

    /** 					elsif sym < 0 or find(SymTab[sym][S_SCOPE], SCOPE_TYPES) then*/
    _28749 = (_sym_55439 < 0);
    if (_28749 != 0) {
        goto LF; // [323] 351
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28751 = (int)*(((s1_ptr)_2)->base + _sym_55439);
    _2 = (int)SEQ_PTR(_28751);
    _28752 = (int)*(((s1_ptr)_2)->base + 4);
    _28751 = NOVALUE;
    _28753 = find_from(_28752, _41SCOPE_TYPES_55113, 1);
    _28752 = NOVALUE;
    if (_28753 == 0)
    {
        _28753 = NOVALUE;
        goto L10; // [347] 361
    }
    else{
        _28753 = NOVALUE;
    }
LF: 

    /** 						emit_op(GLOBAL_INIT_CHECK) -- will become NOP2*/
    _43emit_op(109);
    goto LE; // [358] 369
L10: 

    /** 						emit_op(PRIVATE_INIT_CHECK)*/
    _43emit_op(30);
LE: 

    /** 					emit_addr(sym)*/
    _43emit_addr(_sym_55439);
LC: 

    /** 				if sym > 0 */
    _28754 = (_sym_55439 > 0);
    if (_28754 == 0) {
        _28755 = 0;
        goto L11; // [381] 411
    }
    _28756 = (_41short_circuit_55122 <= 0);
    if (_28756 != 0) {
        _28757 = 1;
        goto L12; // [391] 407
    }
    _28758 = (_41short_circuit_B_55124 == _9FALSE_426);
    _28757 = (_28758 != 0);
L12: 
    _28755 = (_28757 != 0);
L11: 
    if (_28755 == 0) {
        goto L9; // [411] 566
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28760 = (int)*(((s1_ptr)_2)->base + _sym_55439);
    _2 = (int)SEQ_PTR(_28760);
    _28761 = (int)*(((s1_ptr)_2)->base + 4);
    _28760 = NOVALUE;
    if (IS_ATOM_INT(_28761)) {
        _28762 = (_28761 != 3);
    }
    else {
        _28762 = binary_op(NOTEQ, _28761, 3);
    }
    _28761 = NOVALUE;
    if (IS_ATOM_INT(_28762)) {
        _28763 = (_28762 == 0);
    }
    else {
        _28763 = unary_op(NOT, _28762);
    }
    DeRef(_28762);
    _28762 = NOVALUE;
    if (_28763 == 0) {
        DeRef(_28763);
        _28763 = NOVALUE;
        goto L9; // [437] 566
    }
    else {
        if (!IS_ATOM_INT(_28763) && DBL_PTR(_28763)->dbl == 0.0){
            DeRef(_28763);
            _28763 = NOVALUE;
            goto L9; // [437] 566
        }
        DeRef(_28763);
        _28763 = NOVALUE;
    }
    DeRef(_28763);
    _28763 = NOVALUE;

    /** 					if CurrentSub != TopLevelSub */
    _28764 = (_38CurrentSub_16954 != _38TopLevelSub_16953);
    if (_28764 != 0) {
        goto L13; // [450] 470
    }
    if (IS_SEQUENCE(_35known_files_15596)){
            _28766 = SEQ_PTR(_35known_files_15596)->length;
    }
    else {
        _28766 = 1;
    }
    _28767 = (_38current_file_no_16946 == _28766);
    _28766 = NOVALUE;
    if (_28767 == 0)
    {
        DeRef(_28767);
        _28767 = NOVALUE;
        goto L9; // [466] 566
    }
    else{
        DeRef(_28767);
        _28767 = NOVALUE;
    }
L13: 

    /** 						init_stack = append(init_stack, sym)*/
    Append(&_41init_stack_55155, _41init_stack_55155, _sym_55439);

    /** 						SymTab[sym][S_INITLEVEL] = stmt_nest*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_55439 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 14);
    _1 = *(int *)_2;
    *(int *)_2 = _41stmt_nest_55154;
    DeRef(_1);
    _28769 = NOVALUE;
    goto L9; // [499] 566
L4: 

    /** 	elsif ref and sym > 0 and sym_mode( sym ) = M_CONSTANT and equal( NOVALUE, sym_obj( sym ) ) then*/
    if (_ref_55440 == 0) {
        _28771 = 0;
        goto L14; // [504] 516
    }
    _28772 = (_sym_55439 > 0);
    _28771 = (_28772 != 0);
L14: 
    if (_28771 == 0) {
        _28773 = 0;
        goto L15; // [516] 534
    }
    _28774 = _55sym_mode(_sym_55439);
    if (IS_ATOM_INT(_28774)) {
        _28775 = (_28774 == 2);
    }
    else {
        _28775 = binary_op(EQUALS, _28774, 2);
    }
    DeRef(_28774);
    _28774 = NOVALUE;
    if (IS_ATOM_INT(_28775))
    _28773 = (_28775 != 0);
    else
    _28773 = DBL_PTR(_28775)->dbl != 0.0;
L15: 
    if (_28773 == 0) {
        goto L16; // [534] 565
    }
    _28777 = _55sym_obj(_sym_55439);
    if (_38NOVALUE_16800 == _28777)
    _28778 = 1;
    else if (IS_ATOM_INT(_38NOVALUE_16800) && IS_ATOM_INT(_28777))
    _28778 = 0;
    else
    _28778 = (compare(_38NOVALUE_16800, _28777) == 0);
    DeRef(_28777);
    _28777 = NOVALUE;
    if (_28778 == 0)
    {
        _28778 = NOVALUE;
        goto L16; // [549] 565
    }
    else{
        _28778 = NOVALUE;
    }

    /** 		emit_op( GLOBAL_INIT_CHECK )*/
    _43emit_op(109);

    /** 		emit_addr(sym)*/
    _43emit_addr(_sym_55439);
L16: 
L9: 

    /** end procedure*/
    DeRef(_28703);
    _28703 = NOVALUE;
    DeRef(_28716);
    _28716 = NOVALUE;
    DeRef(_28707);
    _28707 = NOVALUE;
    DeRef(_28711);
    _28711 = NOVALUE;
    DeRef(_28715);
    _28715 = NOVALUE;
    DeRef(_28735);
    _28735 = NOVALUE;
    DeRef(_28720);
    _28720 = NOVALUE;
    DeRef(_28728);
    _28728 = NOVALUE;
    DeRef(_28744);
    _28744 = NOVALUE;
    DeRef(_28734);
    _28734 = NOVALUE;
    DeRef(_28739);
    _28739 = NOVALUE;
    DeRef(_28749);
    _28749 = NOVALUE;
    DeRef(_28754);
    _28754 = NOVALUE;
    DeRef(_28756);
    _28756 = NOVALUE;
    DeRef(_28758);
    _28758 = NOVALUE;
    DeRef(_28764);
    _28764 = NOVALUE;
    DeRef(_28772);
    _28772 = NOVALUE;
    DeRef(_28775);
    _28775 = NOVALUE;
    return;
    ;
}


void _41InitDelete()
{
    int _28791 = NOVALUE;
    int _28790 = NOVALUE;
    int _28788 = NOVALUE;
    int _28787 = NOVALUE;
    int _28786 = NOVALUE;
    int _28785 = NOVALUE;
    int _28784 = NOVALUE;
    int _28783 = NOVALUE;
    int _28782 = NOVALUE;
    int _28781 = NOVALUE;
    int _28780 = NOVALUE;
    int _28779 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	while length(init_stack) and*/
L1: 
    if (IS_SEQUENCE(_41init_stack_55155)){
            _28779 = SEQ_PTR(_41init_stack_55155)->length;
    }
    else {
        _28779 = 1;
    }
    if (_28779 == 0) {
        goto L2; // [11] 91
    }
    if (IS_SEQUENCE(_41init_stack_55155)){
            _28781 = SEQ_PTR(_41init_stack_55155)->length;
    }
    else {
        _28781 = 1;
    }
    _2 = (int)SEQ_PTR(_41init_stack_55155);
    _28782 = (int)*(((s1_ptr)_2)->base + _28781);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28783 = (int)*(((s1_ptr)_2)->base + _28782);
    _2 = (int)SEQ_PTR(_28783);
    _28784 = (int)*(((s1_ptr)_2)->base + 14);
    _28783 = NOVALUE;
    if (IS_ATOM_INT(_28784)) {
        _28785 = (_28784 > _41stmt_nest_55154);
    }
    else {
        _28785 = binary_op(GREATER, _28784, _41stmt_nest_55154);
    }
    _28784 = NOVALUE;
    if (_28785 <= 0) {
        if (_28785 == 0) {
            DeRef(_28785);
            _28785 = NOVALUE;
            goto L2; // [43] 91
        }
        else {
            if (!IS_ATOM_INT(_28785) && DBL_PTR(_28785)->dbl == 0.0){
                DeRef(_28785);
                _28785 = NOVALUE;
                goto L2; // [43] 91
            }
            DeRef(_28785);
            _28785 = NOVALUE;
        }
    }
    DeRef(_28785);
    _28785 = NOVALUE;

    /** 		SymTab[init_stack[$]][S_INITLEVEL] = -1*/
    if (IS_SEQUENCE(_41init_stack_55155)){
            _28786 = SEQ_PTR(_41init_stack_55155)->length;
    }
    else {
        _28786 = 1;
    }
    _2 = (int)SEQ_PTR(_41init_stack_55155);
    _28787 = (int)*(((s1_ptr)_2)->base + _28786);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_28787 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 14);
    _1 = *(int *)_2;
    *(int *)_2 = -1;
    DeRef(_1);
    _28788 = NOVALUE;

    /** 		init_stack = init_stack[1..$-1]*/
    if (IS_SEQUENCE(_41init_stack_55155)){
            _28790 = SEQ_PTR(_41init_stack_55155)->length;
    }
    else {
        _28790 = 1;
    }
    _28791 = _28790 - 1;
    _28790 = NOVALUE;
    rhs_slice_target = (object_ptr)&_41init_stack_55155;
    RHS_Slice(_41init_stack_55155, 1, _28791);

    /** 	end while*/
    goto L1; // [88] 6
L2: 

    /** end procedure*/
    _28782 = NOVALUE;
    _28787 = NOVALUE;
    DeRef(_28791);
    _28791 = NOVALUE;
    return;
    ;
}


void _41emit_forward_addr()
{
    int _28793 = NOVALUE;
    int _0, _1, _2;
    

    /** 	emit_addr(0)*/
    _43emit_addr(0);

    /** 	branch_list = append(branch_list, length(Code))*/
    if (IS_SEQUENCE(_38Code_17038)){
            _28793 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _28793 = 1;
    }
    Append(&_41branch_list_55120, _41branch_list_55120, _28793);
    _28793 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


void _41StraightenBranches()
{
    int _br_55613 = NOVALUE;
    int _target_55614 = NOVALUE;
    int _28814 = NOVALUE;
    int _28813 = NOVALUE;
    int _28812 = NOVALUE;
    int _28811 = NOVALUE;
    int _28809 = NOVALUE;
    int _28808 = NOVALUE;
    int _28807 = NOVALUE;
    int _28805 = NOVALUE;
    int _28804 = NOVALUE;
    int _28803 = NOVALUE;
    int _28802 = NOVALUE;
    int _28800 = NOVALUE;
    int _28797 = NOVALUE;
    int _28796 = NOVALUE;
    int _28795 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L1; // [5] 14
    }
    else{
    }

    /** 		return -- do it in back-end*/
    return;
L1: 

    /** 	for i = length(branch_list) to 1 by -1 do*/
    if (IS_SEQUENCE(_41branch_list_55120)){
            _28795 = SEQ_PTR(_41branch_list_55120)->length;
    }
    else {
        _28795 = 1;
    }
    {
        int _i_55618;
        _i_55618 = _28795;
L2: 
        if (_i_55618 < 1){
            goto L3; // [21] 170
        }

        /** 		if branch_list[i] > length(Code) then*/
        _2 = (int)SEQ_PTR(_41branch_list_55120);
        _28796 = (int)*(((s1_ptr)_2)->base + _i_55618);
        if (IS_SEQUENCE(_38Code_17038)){
                _28797 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _28797 = 1;
        }
        if (binary_op_a(LESSEQ, _28796, _28797)){
            _28796 = NOVALUE;
            _28797 = NOVALUE;
            goto L4; // [41] 53
        }
        _28796 = NOVALUE;
        _28797 = NOVALUE;

        /** 			CompileErr("wtf")*/
        RefDS(_28799);
        RefDS(_22663);
        _46CompileErr(_28799, _22663, 0);
L4: 

        /** 		target = Code[branch_list[i]]*/
        _2 = (int)SEQ_PTR(_41branch_list_55120);
        _28800 = (int)*(((s1_ptr)_2)->base + _i_55618);
        _2 = (int)SEQ_PTR(_38Code_17038);
        if (!IS_ATOM_INT(_28800)){
            _target_55614 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_28800)->dbl));
        }
        else{
            _target_55614 = (int)*(((s1_ptr)_2)->base + _28800);
        }
        if (!IS_ATOM_INT(_target_55614)){
            _target_55614 = (long)DBL_PTR(_target_55614)->dbl;
        }

        /** 		if target <= length(Code) and target > 0 then*/
        if (IS_SEQUENCE(_38Code_17038)){
                _28802 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _28802 = 1;
        }
        _28803 = (_target_55614 <= _28802);
        _28802 = NOVALUE;
        if (_28803 == 0) {
            goto L5; // [80] 163
        }
        _28805 = (_target_55614 > 0);
        if (_28805 == 0)
        {
            DeRef(_28805);
            _28805 = NOVALUE;
            goto L5; // [89] 163
        }
        else{
            DeRef(_28805);
            _28805 = NOVALUE;
        }

        /** 			br = Code[target]*/
        _2 = (int)SEQ_PTR(_38Code_17038);
        _br_55613 = (int)*(((s1_ptr)_2)->base + _target_55614);
        if (!IS_ATOM_INT(_br_55613)){
            _br_55613 = (long)DBL_PTR(_br_55613)->dbl;
        }

        /** 			if br = ELSE or br = ENDWHILE or br = EXIT then*/
        _28807 = (_br_55613 == 23);
        if (_28807 != 0) {
            _28808 = 1;
            goto L6; // [110] 124
        }
        _28809 = (_br_55613 == 22);
        _28808 = (_28809 != 0);
L6: 
        if (_28808 != 0) {
            goto L7; // [124] 139
        }
        _28811 = (_br_55613 == 61);
        if (_28811 == 0)
        {
            DeRef(_28811);
            _28811 = NOVALUE;
            goto L8; // [135] 162
        }
        else{
            DeRef(_28811);
            _28811 = NOVALUE;
        }
L7: 

        /** 				backpatch(branch_list[i], Code[target+1])*/
        _2 = (int)SEQ_PTR(_41branch_list_55120);
        _28812 = (int)*(((s1_ptr)_2)->base + _i_55618);
        _28813 = _target_55614 + 1;
        _2 = (int)SEQ_PTR(_38Code_17038);
        _28814 = (int)*(((s1_ptr)_2)->base + _28813);
        Ref(_28812);
        Ref(_28814);
        _43backpatch(_28812, _28814);
        _28812 = NOVALUE;
        _28814 = NOVALUE;
L8: 
L5: 

        /** 	end for*/
        _i_55618 = _i_55618 + -1;
        goto L2; // [165] 28
L3: 
        ;
    }

    /** 	branch_list = {}*/
    RefDS(_22663);
    DeRef(_41branch_list_55120);
    _41branch_list_55120 = _22663;

    /** end procedure*/
    _28800 = NOVALUE;
    DeRef(_28803);
    _28803 = NOVALUE;
    DeRef(_28807);
    _28807 = NOVALUE;
    DeRef(_28809);
    _28809 = NOVALUE;
    DeRef(_28813);
    _28813 = NOVALUE;
    return;
    ;
}


void _41PatchEList(int _base_55666)
{
    int _break_top_55667 = NOVALUE;
    int _n_55668 = NOVALUE;
    int _28831 = NOVALUE;
    int _28830 = NOVALUE;
    int _28829 = NOVALUE;
    int _28825 = NOVALUE;
    int _28824 = NOVALUE;
    int _28823 = NOVALUE;
    int _28821 = NOVALUE;
    int _28820 = NOVALUE;
    int _28818 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length(break_list) then*/
    if (IS_SEQUENCE(_41break_list_55140)){
            _28818 = SEQ_PTR(_41break_list_55140)->length;
    }
    else {
        _28818 = 1;
    }
    if (_28818 != 0)
    goto L1; // [10] 19
    _28818 = NOVALUE;

    /** 		return*/
    return;
L1: 

    /** 	break_top = 0*/
    _break_top_55667 = 0;

    /** 	for i=length(break_list) to base+1 by -1 do*/
    if (IS_SEQUENCE(_41break_list_55140)){
            _28820 = SEQ_PTR(_41break_list_55140)->length;
    }
    else {
        _28820 = 1;
    }
    _28821 = _base_55666 + 1;
    if (_28821 > MAXINT){
        _28821 = NewDouble((double)_28821);
    }
    {
        int _i_55673;
        _i_55673 = _28820;
L2: 
        if (binary_op_a(LESS, _i_55673, _28821)){
            goto L3; // [35] 129
        }

        /** 		n=break_delay[i]*/
        _2 = (int)SEQ_PTR(_41break_delay_55141);
        if (!IS_ATOM_INT(_i_55673)){
            _n_55668 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55673)->dbl));
        }
        else{
            _n_55668 = (int)*(((s1_ptr)_2)->base + _i_55673);
        }
        if (!IS_ATOM_INT(_n_55668))
        _n_55668 = (long)DBL_PTR(_n_55668)->dbl;

        /** 		break_delay[i] -= (n>0)*/
        _28823 = (_n_55668 > 0);
        _2 = (int)SEQ_PTR(_41break_delay_55141);
        if (!IS_ATOM_INT(_i_55673)){
            _28824 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55673)->dbl));
        }
        else{
            _28824 = (int)*(((s1_ptr)_2)->base + _i_55673);
        }
        if (IS_ATOM_INT(_28824)) {
            _28825 = _28824 - _28823;
            if ((long)((unsigned long)_28825 +(unsigned long) HIGH_BITS) >= 0){
                _28825 = NewDouble((double)_28825);
            }
        }
        else {
            _28825 = binary_op(MINUS, _28824, _28823);
        }
        _28824 = NOVALUE;
        _28823 = NOVALUE;
        _2 = (int)SEQ_PTR(_41break_delay_55141);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _41break_delay_55141 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_i_55673))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55673)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _i_55673);
        _1 = *(int *)_2;
        *(int *)_2 = _28825;
        if( _1 != _28825 ){
            DeRef(_1);
        }
        _28825 = NOVALUE;

        /** 		if n>1 then*/
        if (_n_55668 <= 1)
        goto L4; // [72] 93

        /** 			if break_top = 0 then*/
        if (_break_top_55667 != 0)
        goto L5; // [78] 122

        /** 				break_top = i*/
        Ref(_i_55673);
        _break_top_55667 = _i_55673;
        if (!IS_ATOM_INT(_break_top_55667)) {
            _1 = (long)(DBL_PTR(_break_top_55667)->dbl);
            if (UNIQUE(DBL_PTR(_break_top_55667)) && (DBL_PTR(_break_top_55667)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_break_top_55667);
            _break_top_55667 = _1;
        }
        goto L5; // [90] 122
L4: 

        /** 		elsif n=1 then*/
        if (_n_55668 != 1)
        goto L6; // [95] 121

        /** 			backpatch(break_list[i],length(Code)+1)*/
        _2 = (int)SEQ_PTR(_41break_list_55140);
        if (!IS_ATOM_INT(_i_55673)){
            _28829 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55673)->dbl));
        }
        else{
            _28829 = (int)*(((s1_ptr)_2)->base + _i_55673);
        }
        if (IS_SEQUENCE(_38Code_17038)){
                _28830 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _28830 = 1;
        }
        _28831 = _28830 + 1;
        _28830 = NOVALUE;
        _43backpatch(_28829, _28831);
        _28829 = NOVALUE;
        _28831 = NOVALUE;
L6: 
L5: 

        /** 	end for*/
        _0 = _i_55673;
        if (IS_ATOM_INT(_i_55673)) {
            _i_55673 = _i_55673 + -1;
            if ((long)((unsigned long)_i_55673 +(unsigned long) HIGH_BITS) >= 0){
                _i_55673 = NewDouble((double)_i_55673);
            }
        }
        else {
            _i_55673 = binary_op_a(PLUS, _i_55673, -1);
        }
        DeRef(_0);
        goto L2; // [124] 42
L3: 
        ;
        DeRef(_i_55673);
    }

    /** 	if break_top=0 then*/
    if (_break_top_55667 != 0)
    goto L7; // [131] 141

    /** 	    break_top=base*/
    _break_top_55667 = _base_55666;
L7: 

    /** 	break_delay = break_delay[1..break_top]*/
    rhs_slice_target = (object_ptr)&_41break_delay_55141;
    RHS_Slice(_41break_delay_55141, 1, _break_top_55667);

    /** 	break_list = break_list[1..break_top]*/
    rhs_slice_target = (object_ptr)&_41break_list_55140;
    RHS_Slice(_41break_list_55140, 1, _break_top_55667);

    /** end procedure*/
    DeRef(_28821);
    _28821 = NOVALUE;
    return;
    ;
}


void _41PatchNList(int _base_55697)
{
    int _next_top_55698 = NOVALUE;
    int _n_55699 = NOVALUE;
    int _28848 = NOVALUE;
    int _28847 = NOVALUE;
    int _28846 = NOVALUE;
    int _28842 = NOVALUE;
    int _28841 = NOVALUE;
    int _28840 = NOVALUE;
    int _28838 = NOVALUE;
    int _28837 = NOVALUE;
    int _28835 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length(continue_list) then*/
    if (IS_SEQUENCE(_41continue_list_55144)){
            _28835 = SEQ_PTR(_41continue_list_55144)->length;
    }
    else {
        _28835 = 1;
    }
    if (_28835 != 0)
    goto L1; // [10] 19
    _28835 = NOVALUE;

    /** 		return*/
    return;
L1: 

    /** 	next_top = 0*/
    _next_top_55698 = 0;

    /** 	for i=length(continue_list) to base+1 by -1 do*/
    if (IS_SEQUENCE(_41continue_list_55144)){
            _28837 = SEQ_PTR(_41continue_list_55144)->length;
    }
    else {
        _28837 = 1;
    }
    _28838 = _base_55697 + 1;
    if (_28838 > MAXINT){
        _28838 = NewDouble((double)_28838);
    }
    {
        int _i_55704;
        _i_55704 = _28837;
L2: 
        if (binary_op_a(LESS, _i_55704, _28838)){
            goto L3; // [35] 129
        }

        /** 		n=continue_delay[i]*/
        _2 = (int)SEQ_PTR(_41continue_delay_55145);
        if (!IS_ATOM_INT(_i_55704)){
            _n_55699 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55704)->dbl));
        }
        else{
            _n_55699 = (int)*(((s1_ptr)_2)->base + _i_55704);
        }
        if (!IS_ATOM_INT(_n_55699))
        _n_55699 = (long)DBL_PTR(_n_55699)->dbl;

        /** 		continue_delay[i] -= (n>0)*/
        _28840 = (_n_55699 > 0);
        _2 = (int)SEQ_PTR(_41continue_delay_55145);
        if (!IS_ATOM_INT(_i_55704)){
            _28841 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55704)->dbl));
        }
        else{
            _28841 = (int)*(((s1_ptr)_2)->base + _i_55704);
        }
        if (IS_ATOM_INT(_28841)) {
            _28842 = _28841 - _28840;
            if ((long)((unsigned long)_28842 +(unsigned long) HIGH_BITS) >= 0){
                _28842 = NewDouble((double)_28842);
            }
        }
        else {
            _28842 = binary_op(MINUS, _28841, _28840);
        }
        _28841 = NOVALUE;
        _28840 = NOVALUE;
        _2 = (int)SEQ_PTR(_41continue_delay_55145);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _41continue_delay_55145 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_i_55704))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55704)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _i_55704);
        _1 = *(int *)_2;
        *(int *)_2 = _28842;
        if( _1 != _28842 ){
            DeRef(_1);
        }
        _28842 = NOVALUE;

        /** 		if n>1 then*/
        if (_n_55699 <= 1)
        goto L4; // [72] 93

        /** 			if next_top = 0 then*/
        if (_next_top_55698 != 0)
        goto L5; // [78] 122

        /** 				next_top = i*/
        Ref(_i_55704);
        _next_top_55698 = _i_55704;
        if (!IS_ATOM_INT(_next_top_55698)) {
            _1 = (long)(DBL_PTR(_next_top_55698)->dbl);
            if (UNIQUE(DBL_PTR(_next_top_55698)) && (DBL_PTR(_next_top_55698)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_next_top_55698);
            _next_top_55698 = _1;
        }
        goto L5; // [90] 122
L4: 

        /** 		elsif n=1 then*/
        if (_n_55699 != 1)
        goto L6; // [95] 121

        /** 			backpatch(continue_list[i],length(Code)+1)*/
        _2 = (int)SEQ_PTR(_41continue_list_55144);
        if (!IS_ATOM_INT(_i_55704)){
            _28846 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55704)->dbl));
        }
        else{
            _28846 = (int)*(((s1_ptr)_2)->base + _i_55704);
        }
        if (IS_SEQUENCE(_38Code_17038)){
                _28847 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _28847 = 1;
        }
        _28848 = _28847 + 1;
        _28847 = NOVALUE;
        _43backpatch(_28846, _28848);
        _28846 = NOVALUE;
        _28848 = NOVALUE;
L6: 
L5: 

        /** 	end for*/
        _0 = _i_55704;
        if (IS_ATOM_INT(_i_55704)) {
            _i_55704 = _i_55704 + -1;
            if ((long)((unsigned long)_i_55704 +(unsigned long) HIGH_BITS) >= 0){
                _i_55704 = NewDouble((double)_i_55704);
            }
        }
        else {
            _i_55704 = binary_op_a(PLUS, _i_55704, -1);
        }
        DeRef(_0);
        goto L2; // [124] 42
L3: 
        ;
        DeRef(_i_55704);
    }

    /** 	if next_top=0 then*/
    if (_next_top_55698 != 0)
    goto L7; // [131] 141

    /** 	    next_top=base*/
    _next_top_55698 = _base_55697;
L7: 

    /** 	continue_delay =continue_delay[1..next_top]*/
    rhs_slice_target = (object_ptr)&_41continue_delay_55145;
    RHS_Slice(_41continue_delay_55145, 1, _next_top_55698);

    /** 	continue_list = continue_list[1..next_top]*/
    rhs_slice_target = (object_ptr)&_41continue_list_55144;
    RHS_Slice(_41continue_list_55144, 1, _next_top_55698);

    /** end procedure*/
    DeRef(_28838);
    _28838 = NOVALUE;
    return;
    ;
}


void _41PatchXList(int _base_55728)
{
    int _exit_top_55729 = NOVALUE;
    int _n_55730 = NOVALUE;
    int _28865 = NOVALUE;
    int _28864 = NOVALUE;
    int _28863 = NOVALUE;
    int _28859 = NOVALUE;
    int _28858 = NOVALUE;
    int _28857 = NOVALUE;
    int _28855 = NOVALUE;
    int _28854 = NOVALUE;
    int _28852 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length(exit_list) then*/
    if (IS_SEQUENCE(_41exit_list_55142)){
            _28852 = SEQ_PTR(_41exit_list_55142)->length;
    }
    else {
        _28852 = 1;
    }
    if (_28852 != 0)
    goto L1; // [10] 19
    _28852 = NOVALUE;

    /** 		return*/
    return;
L1: 

    /** 	exit_top = 0*/
    _exit_top_55729 = 0;

    /** 	for i=length(exit_list) to base+1 by -1 do*/
    if (IS_SEQUENCE(_41exit_list_55142)){
            _28854 = SEQ_PTR(_41exit_list_55142)->length;
    }
    else {
        _28854 = 1;
    }
    _28855 = _base_55728 + 1;
    if (_28855 > MAXINT){
        _28855 = NewDouble((double)_28855);
    }
    {
        int _i_55735;
        _i_55735 = _28854;
L2: 
        if (binary_op_a(LESS, _i_55735, _28855)){
            goto L3; // [35] 129
        }

        /** 		n=exit_delay[i]*/
        _2 = (int)SEQ_PTR(_41exit_delay_55143);
        if (!IS_ATOM_INT(_i_55735)){
            _n_55730 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55735)->dbl));
        }
        else{
            _n_55730 = (int)*(((s1_ptr)_2)->base + _i_55735);
        }
        if (!IS_ATOM_INT(_n_55730))
        _n_55730 = (long)DBL_PTR(_n_55730)->dbl;

        /** 		exit_delay[i] -= (n>0)*/
        _28857 = (_n_55730 > 0);
        _2 = (int)SEQ_PTR(_41exit_delay_55143);
        if (!IS_ATOM_INT(_i_55735)){
            _28858 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55735)->dbl));
        }
        else{
            _28858 = (int)*(((s1_ptr)_2)->base + _i_55735);
        }
        if (IS_ATOM_INT(_28858)) {
            _28859 = _28858 - _28857;
            if ((long)((unsigned long)_28859 +(unsigned long) HIGH_BITS) >= 0){
                _28859 = NewDouble((double)_28859);
            }
        }
        else {
            _28859 = binary_op(MINUS, _28858, _28857);
        }
        _28858 = NOVALUE;
        _28857 = NOVALUE;
        _2 = (int)SEQ_PTR(_41exit_delay_55143);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _41exit_delay_55143 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_i_55735))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55735)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _i_55735);
        _1 = *(int *)_2;
        *(int *)_2 = _28859;
        if( _1 != _28859 ){
            DeRef(_1);
        }
        _28859 = NOVALUE;

        /** 		if n>1 then*/
        if (_n_55730 <= 1)
        goto L4; // [72] 93

        /** 			if exit_top = 0 then*/
        if (_exit_top_55729 != 0)
        goto L5; // [78] 122

        /** 				exit_top = i*/
        Ref(_i_55735);
        _exit_top_55729 = _i_55735;
        if (!IS_ATOM_INT(_exit_top_55729)) {
            _1 = (long)(DBL_PTR(_exit_top_55729)->dbl);
            if (UNIQUE(DBL_PTR(_exit_top_55729)) && (DBL_PTR(_exit_top_55729)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_exit_top_55729);
            _exit_top_55729 = _1;
        }
        goto L5; // [90] 122
L4: 

        /** 		elsif n=1 then*/
        if (_n_55730 != 1)
        goto L6; // [95] 121

        /** 			backpatch(exit_list[i],length(Code)+1)*/
        _2 = (int)SEQ_PTR(_41exit_list_55142);
        if (!IS_ATOM_INT(_i_55735)){
            _28863 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55735)->dbl));
        }
        else{
            _28863 = (int)*(((s1_ptr)_2)->base + _i_55735);
        }
        if (IS_SEQUENCE(_38Code_17038)){
                _28864 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _28864 = 1;
        }
        _28865 = _28864 + 1;
        _28864 = NOVALUE;
        _43backpatch(_28863, _28865);
        _28863 = NOVALUE;
        _28865 = NOVALUE;
L6: 
L5: 

        /** 	end for*/
        _0 = _i_55735;
        if (IS_ATOM_INT(_i_55735)) {
            _i_55735 = _i_55735 + -1;
            if ((long)((unsigned long)_i_55735 +(unsigned long) HIGH_BITS) >= 0){
                _i_55735 = NewDouble((double)_i_55735);
            }
        }
        else {
            _i_55735 = binary_op_a(PLUS, _i_55735, -1);
        }
        DeRef(_0);
        goto L2; // [124] 42
L3: 
        ;
        DeRef(_i_55735);
    }

    /** 	if exit_top=0 then*/
    if (_exit_top_55729 != 0)
    goto L7; // [131] 141

    /** 	    exit_top=base*/
    _exit_top_55729 = _base_55728;
L7: 

    /** 	exit_delay = exit_delay [1..exit_top]*/
    rhs_slice_target = (object_ptr)&_41exit_delay_55143;
    RHS_Slice(_41exit_delay_55143, 1, _exit_top_55729);

    /** 	exit_list = exit_list [1..exit_top]*/
    rhs_slice_target = (object_ptr)&_41exit_list_55142;
    RHS_Slice(_41exit_list_55142, 1, _exit_top_55729);

    /** end procedure*/
    DeRef(_28855);
    _28855 = NOVALUE;
    return;
    ;
}


void _41putback(int _t_55760)
{
    int _28870 = NOVALUE;
    int _0, _1, _2;
    

    /** 	backed_up_tok = append(backed_up_tok, t)*/
    Ref(_t_55760);
    Append(&_41backed_up_tok_55129, _41backed_up_tok_55129, _t_55760);

    /** 	if t[T_SYM] then*/
    _2 = (int)SEQ_PTR(_t_55760);
    _28870 = (int)*(((s1_ptr)_2)->base + 2);
    if (_28870 == 0) {
        _28870 = NOVALUE;
        goto L1; // [17] 79
    }
    else {
        if (!IS_ATOM_INT(_28870) && DBL_PTR(_28870)->dbl == 0.0){
            _28870 = NOVALUE;
            goto L1; // [17] 79
        }
        _28870 = NOVALUE;
    }
    _28870 = NOVALUE;

    /** 		putback_ForwardLine     = ForwardLine*/
    Ref(_46ForwardLine_49483);
    DeRef(_46putback_ForwardLine_49484);
    _46putback_ForwardLine_49484 = _46ForwardLine_49483;

    /** 		putback_forward_bp      = forward_bp*/
    _46putback_forward_bp_49488 = _46forward_bp_49487;

    /** 		putback_fwd_line_number = fwd_line_number*/
    _38putback_fwd_line_number_16949 = _38fwd_line_number_16948;

    /** 		if last_fwd_line_number then*/
    if (_38last_fwd_line_number_16950 == 0)
    {
        goto L2; // [49] 78
    }
    else{
    }

    /** 			ForwardLine     = last_ForwardLine*/
    Ref(_46last_ForwardLine_49485);
    DeRef(_46ForwardLine_49483);
    _46ForwardLine_49483 = _46last_ForwardLine_49485;

    /** 			forward_bp      = last_forward_bp*/
    _46forward_bp_49487 = _46last_forward_bp_49489;

    /** 			fwd_line_number = last_fwd_line_number*/
    _38fwd_line_number_16948 = _38last_fwd_line_number_16950;
L2: 
L1: 

    /** end procedure*/
    DeRef(_t_55760);
    return;
    ;
}


void _41start_recording()
{
    int _0, _1, _2;
    

    /** 	psm_stack &= Parser_mode*/
    Append(&_41psm_stack_55779, _41psm_stack_55779, _38Parser_mode_17071);

    /** 	can_stack = append(can_stack,canned_tokens)*/
    Ref(_41canned_tokens_55166);
    Append(&_41can_stack_55780, _41can_stack_55780, _41canned_tokens_55166);

    /** 	idx_stack &= canned_index*/
    Append(&_41idx_stack_55781, _41idx_stack_55781, _41canned_index_55167);

    /** 	tok_stack = append(tok_stack,backed_up_tok)*/
    RefDS(_41backed_up_tok_55129);
    Append(&_41tok_stack_55782, _41tok_stack_55782, _41backed_up_tok_55129);

    /** 	canned_tokens = {}*/
    RefDS(_22663);
    DeRef(_41canned_tokens_55166);
    _41canned_tokens_55166 = _22663;

    /** 	Parser_mode = PAM_RECORD*/
    _38Parser_mode_17071 = 1;

    /** 	clear_last()*/
    _43clear_last();

    /** end procedure*/
    return;
    ;
}


int _41restore_parser()
{
    int _n_55795 = NOVALUE;
    int _tok_55796 = NOVALUE;
    int _x_55797 = NOVALUE;
    int _28901 = NOVALUE;
    int _28900 = NOVALUE;
    int _28899 = NOVALUE;
    int _28897 = NOVALUE;
    int _28893 = NOVALUE;
    int _28892 = NOVALUE;
    int _28890 = NOVALUE;
    int _28888 = NOVALUE;
    int _28887 = NOVALUE;
    int _28885 = NOVALUE;
    int _28883 = NOVALUE;
    int _28882 = NOVALUE;
    int _28880 = NOVALUE;
    int _28878 = NOVALUE;
    int _28877 = NOVALUE;
    int _28875 = NOVALUE;
    int _0, _1, _2;
    

    /** 	n=Parser_mode*/
    _n_55795 = _38Parser_mode_17071;

    /** 	x = canned_tokens*/
    Ref(_41canned_tokens_55166);
    DeRef(_x_55797);
    _x_55797 = _41canned_tokens_55166;

    /** 	canned_tokens = can_stack[$]*/
    if (IS_SEQUENCE(_41can_stack_55780)){
            _28875 = SEQ_PTR(_41can_stack_55780)->length;
    }
    else {
        _28875 = 1;
    }
    DeRef(_41canned_tokens_55166);
    _2 = (int)SEQ_PTR(_41can_stack_55780);
    _41canned_tokens_55166 = (int)*(((s1_ptr)_2)->base + _28875);
    Ref(_41canned_tokens_55166);

    /** 	can_stack     = can_stack[1..$-1]*/
    if (IS_SEQUENCE(_41can_stack_55780)){
            _28877 = SEQ_PTR(_41can_stack_55780)->length;
    }
    else {
        _28877 = 1;
    }
    _28878 = _28877 - 1;
    _28877 = NOVALUE;
    rhs_slice_target = (object_ptr)&_41can_stack_55780;
    RHS_Slice(_41can_stack_55780, 1, _28878);

    /** 	canned_index  = idx_stack[$]*/
    if (IS_SEQUENCE(_41idx_stack_55781)){
            _28880 = SEQ_PTR(_41idx_stack_55781)->length;
    }
    else {
        _28880 = 1;
    }
    _2 = (int)SEQ_PTR(_41idx_stack_55781);
    _41canned_index_55167 = (int)*(((s1_ptr)_2)->base + _28880);

    /** 	idx_stack     = idx_stack[1..$-1]*/
    if (IS_SEQUENCE(_41idx_stack_55781)){
            _28882 = SEQ_PTR(_41idx_stack_55781)->length;
    }
    else {
        _28882 = 1;
    }
    _28883 = _28882 - 1;
    _28882 = NOVALUE;
    rhs_slice_target = (object_ptr)&_41idx_stack_55781;
    RHS_Slice(_41idx_stack_55781, 1, _28883);

    /** 	Parser_mode   = psm_stack[$]*/
    if (IS_SEQUENCE(_41psm_stack_55779)){
            _28885 = SEQ_PTR(_41psm_stack_55779)->length;
    }
    else {
        _28885 = 1;
    }
    _2 = (int)SEQ_PTR(_41psm_stack_55779);
    _38Parser_mode_17071 = (int)*(((s1_ptr)_2)->base + _28885);

    /** 	psm_stack     = psm_stack[1..$-1]*/
    if (IS_SEQUENCE(_41psm_stack_55779)){
            _28887 = SEQ_PTR(_41psm_stack_55779)->length;
    }
    else {
        _28887 = 1;
    }
    _28888 = _28887 - 1;
    _28887 = NOVALUE;
    rhs_slice_target = (object_ptr)&_41psm_stack_55779;
    RHS_Slice(_41psm_stack_55779, 1, _28888);

    /** 	tok 		  = tok_stack[$]*/
    if (IS_SEQUENCE(_41tok_stack_55782)){
            _28890 = SEQ_PTR(_41tok_stack_55782)->length;
    }
    else {
        _28890 = 1;
    }
    DeRef(_tok_55796);
    _2 = (int)SEQ_PTR(_41tok_stack_55782);
    _tok_55796 = (int)*(((s1_ptr)_2)->base + _28890);
    RefDS(_tok_55796);

    /** 	tok_stack 	  = tok_stack[1..$-1]*/
    if (IS_SEQUENCE(_41tok_stack_55782)){
            _28892 = SEQ_PTR(_41tok_stack_55782)->length;
    }
    else {
        _28892 = 1;
    }
    _28893 = _28892 - 1;
    _28892 = NOVALUE;
    rhs_slice_target = (object_ptr)&_41tok_stack_55782;
    RHS_Slice(_41tok_stack_55782, 1, _28893);

    /** 	clear_last()*/
    _43clear_last();

    /** 	if n=PAM_PLAYBACK then*/
    if (_n_55795 != -1)
    goto L1; // [137] 150

    /** 		return {}*/
    RefDS(_22663);
    DeRefDS(_tok_55796);
    DeRefDS(_x_55797);
    _28878 = NOVALUE;
    _28883 = NOVALUE;
    _28888 = NOVALUE;
    _28893 = NOVALUE;
    return _22663;
    goto L2; // [147] 167
L1: 

    /** 	elsif n = PAM_NORMAL then*/
    if (_n_55795 != 0)
    goto L3; // [154] 166

    /** 		use_private_list = 0*/
    _38use_private_list_17079 = 0;
L3: 
L2: 

    /** 	if length(backed_up_tok) > 0 then*/
    if (IS_SEQUENCE(_41backed_up_tok_55129)){
            _28897 = SEQ_PTR(_41backed_up_tok_55129)->length;
    }
    else {
        _28897 = 1;
    }
    if (_28897 <= 0)
    goto L4; // [174] 199

    /** 		return x[1..$-1]*/
    if (IS_SEQUENCE(_x_55797)){
            _28899 = SEQ_PTR(_x_55797)->length;
    }
    else {
        _28899 = 1;
    }
    _28900 = _28899 - 1;
    _28899 = NOVALUE;
    rhs_slice_target = (object_ptr)&_28901;
    RHS_Slice(_x_55797, 1, _28900);
    DeRef(_tok_55796);
    DeRefDS(_x_55797);
    DeRef(_28878);
    _28878 = NOVALUE;
    DeRef(_28883);
    _28883 = NOVALUE;
    DeRef(_28888);
    _28888 = NOVALUE;
    DeRef(_28893);
    _28893 = NOVALUE;
    _28900 = NOVALUE;
    return _28901;
    goto L5; // [196] 206
L4: 

    /** 		return x*/
    DeRef(_tok_55796);
    DeRef(_28878);
    _28878 = NOVALUE;
    DeRef(_28883);
    _28883 = NOVALUE;
    DeRef(_28888);
    _28888 = NOVALUE;
    DeRef(_28893);
    _28893 = NOVALUE;
    DeRef(_28900);
    _28900 = NOVALUE;
    DeRef(_28901);
    _28901 = NOVALUE;
    return _x_55797;
L5: 
    ;
}


void _41start_playback(int _s_55837)
{
    int _0, _1, _2;
    

    /** 	psm_stack &= Parser_mode*/
    Append(&_41psm_stack_55779, _41psm_stack_55779, _38Parser_mode_17071);

    /** 	can_stack = append(can_stack,canned_tokens)*/
    Ref(_41canned_tokens_55166);
    Append(&_41can_stack_55780, _41can_stack_55780, _41canned_tokens_55166);

    /** 	idx_stack &= canned_index*/
    Append(&_41idx_stack_55781, _41idx_stack_55781, _41canned_index_55167);

    /** 	tok_stack = append(tok_stack,backed_up_tok)*/
    RefDS(_41backed_up_tok_55129);
    Append(&_41tok_stack_55782, _41tok_stack_55782, _41backed_up_tok_55129);

    /** 	canned_index = 1*/
    _41canned_index_55167 = 1;

    /** 	canned_tokens = s*/
    RefDS(_s_55837);
    DeRef(_41canned_tokens_55166);
    _41canned_tokens_55166 = _s_55837;

    /** 	backed_up_tok = {}*/
    RefDS(_22663);
    DeRefDS(_41backed_up_tok_55129);
    _41backed_up_tok_55129 = _22663;

    /** 	Parser_mode = PAM_PLAYBACK*/
    _38Parser_mode_17071 = -1;

    /** end procedure*/
    DeRefDS(_s_55837);
    return;
    ;
}


void _41restore_parseargs_states()
{
    int _s_55859 = NOVALUE;
    int _n_55860 = NOVALUE;
    int _28921 = NOVALUE;
    int _28920 = NOVALUE;
    int _28912 = NOVALUE;
    int _28911 = NOVALUE;
    int _28909 = NOVALUE;
    int _0, _1, _2;
    

    /** 	s = parseargs_states[$]*/
    if (IS_SEQUENCE(_41parseargs_states_55845)){
            _28909 = SEQ_PTR(_41parseargs_states_55845)->length;
    }
    else {
        _28909 = 1;
    }
    DeRef(_s_55859);
    _2 = (int)SEQ_PTR(_41parseargs_states_55845);
    _s_55859 = (int)*(((s1_ptr)_2)->base + _28909);
    RefDS(_s_55859);

    /** 	parseargs_states = parseargs_states[1..$-1]*/
    if (IS_SEQUENCE(_41parseargs_states_55845)){
            _28911 = SEQ_PTR(_41parseargs_states_55845)->length;
    }
    else {
        _28911 = 1;
    }
    _28912 = _28911 - 1;
    _28911 = NOVALUE;
    rhs_slice_target = (object_ptr)&_41parseargs_states_55845;
    RHS_Slice(_41parseargs_states_55845, 1, _28912);

    /** 	n=s[PS_POSITION]*/
    _2 = (int)SEQ_PTR(_s_55859);
    _n_55860 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_n_55860))
    _n_55860 = (long)DBL_PTR(_n_55860)->dbl;

    /** 	private_list = private_list[1..n]*/
    rhs_slice_target = (object_ptr)&_41private_list_55853;
    RHS_Slice(_41private_list_55853, 1, _n_55860);

    /** 	private_sym = private_sym[1..n]*/
    rhs_slice_target = (object_ptr)&_38private_sym_17078;
    RHS_Slice(_38private_sym_17078, 1, _n_55860);

    /** 	lock_scanner = s[PS_SCAN_LOCK]*/
    _2 = (int)SEQ_PTR(_s_55859);
    _41lock_scanner_55854 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_41lock_scanner_55854))
    _41lock_scanner_55854 = (long)DBL_PTR(_41lock_scanner_55854)->dbl;

    /** 	use_private_list = s[PS_USE_LIST]*/
    _2 = (int)SEQ_PTR(_s_55859);
    _38use_private_list_17079 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_38use_private_list_17079)){
        _38use_private_list_17079 = (long)DBL_PTR(_38use_private_list_17079)->dbl;
    }

    /** 	on_arg = s[PS_ON_ARG]*/
    _2 = (int)SEQ_PTR(_s_55859);
    _41on_arg_55855 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_41on_arg_55855))
    _41on_arg_55855 = (long)DBL_PTR(_41on_arg_55855)->dbl;

    /** 	nested_calls = nested_calls[1..$-1]*/
    if (IS_SEQUENCE(_41nested_calls_55856)){
            _28920 = SEQ_PTR(_41nested_calls_55856)->length;
    }
    else {
        _28920 = 1;
    }
    _28921 = _28920 - 1;
    _28920 = NOVALUE;
    rhs_slice_target = (object_ptr)&_41nested_calls_55856;
    RHS_Slice(_41nested_calls_55856, 1, _28921);

    /** end procedure*/
    DeRefDS(_s_55859);
    _28912 = NOVALUE;
    _28921 = NOVALUE;
    return;
    ;
}


int _41read_recorded_token(int _n_55880)
{
    int _t_55882 = NOVALUE;
    int _p_55883 = NOVALUE;
    int _prev_Nne_55884 = NOVALUE;
    int _ts_55913 = NOVALUE;
    int _32363 = NOVALUE;
    int _32362 = NOVALUE;
    int _32361 = NOVALUE;
    int _32360 = NOVALUE;
    int _32359 = NOVALUE;
    int _32358 = NOVALUE;
    int _32357 = NOVALUE;
    int _32356 = NOVALUE;
    int _28995 = NOVALUE;
    int _28994 = NOVALUE;
    int _28993 = NOVALUE;
    int _28992 = NOVALUE;
    int _28988 = NOVALUE;
    int _28986 = NOVALUE;
    int _28985 = NOVALUE;
    int _28984 = NOVALUE;
    int _28983 = NOVALUE;
    int _28981 = NOVALUE;
    int _28980 = NOVALUE;
    int _28978 = NOVALUE;
    int _28977 = NOVALUE;
    int _28975 = NOVALUE;
    int _28972 = NOVALUE;
    int _28970 = NOVALUE;
    int _28968 = NOVALUE;
    int _28967 = NOVALUE;
    int _28966 = NOVALUE;
    int _28965 = NOVALUE;
    int _28962 = NOVALUE;
    int _28960 = NOVALUE;
    int _28956 = NOVALUE;
    int _28954 = NOVALUE;
    int _28952 = NOVALUE;
    int _28951 = NOVALUE;
    int _28950 = NOVALUE;
    int _28949 = NOVALUE;
    int _28948 = NOVALUE;
    int _28947 = NOVALUE;
    int _28946 = NOVALUE;
    int _28945 = NOVALUE;
    int _28944 = NOVALUE;
    int _28943 = NOVALUE;
    int _28942 = NOVALUE;
    int _28941 = NOVALUE;
    int _28939 = NOVALUE;
    int _28938 = NOVALUE;
    int _28936 = NOVALUE;
    int _28935 = NOVALUE;
    int _28934 = NOVALUE;
    int _28933 = NOVALUE;
    int _28932 = NOVALUE;
    int _28931 = NOVALUE;
    int _28930 = NOVALUE;
    int _28929 = NOVALUE;
    int _28926 = NOVALUE;
    int _28924 = NOVALUE;
    int _28923 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_n_55880)) {
        _1 = (long)(DBL_PTR(_n_55880)->dbl);
        if (UNIQUE(DBL_PTR(_n_55880)) && (DBL_PTR(_n_55880)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_n_55880);
        _n_55880 = _1;
    }

    /** 	integer p, prev_Nne*/

    /** 	if atom(Ns_recorded[n]) label "top if" then*/
    _2 = (int)SEQ_PTR(_38Ns_recorded_17073);
    _28923 = (int)*(((s1_ptr)_2)->base + _n_55880);
    _28924 = IS_ATOM(_28923);
    _28923 = NOVALUE;
    if (_28924 == 0)
    {
        _28924 = NOVALUE;
        goto L1; // [16] 403
    }
    else{
        _28924 = NOVALUE;
    }

    /** 		if use_private_list then*/
    if (_38use_private_list_17079 == 0)
    {
        goto L2; // [23] 171
    }
    else{
    }

    /** 			p = find( Recorded[n], private_list)*/
    _2 = (int)SEQ_PTR(_38Recorded_17072);
    _28926 = (int)*(((s1_ptr)_2)->base + _n_55880);
    _p_55883 = find_from(_28926, _41private_list_55853, 1);
    _28926 = NOVALUE;

    /** 			if p > 0 then -- the value of this parameter is known, use it*/
    if (_p_55883 <= 0)
    goto L3; // [43] 170

    /** 				if TRANSLATE*/
    if (_38TRANSLATE_16564 == 0) {
        goto L4; // [51] 150
    }
    _2 = (int)SEQ_PTR(_38private_sym_17078);
    _28930 = (int)*(((s1_ptr)_2)->base + _p_55883);
    if (IS_ATOM_INT(_28930)) {
        _28931 = (_28930 < 0);
    }
    else {
        _28931 = binary_op(LESS, _28930, 0);
    }
    _28930 = NOVALUE;
    if (IS_ATOM_INT(_28931)) {
        if (_28931 != 0) {
            _28932 = 1;
            goto L5; // [65] 97
        }
    }
    else {
        if (DBL_PTR(_28931)->dbl != 0.0) {
            _28932 = 1;
            goto L5; // [65] 97
        }
    }
    _2 = (int)SEQ_PTR(_38private_sym_17078);
    _28933 = (int)*(((s1_ptr)_2)->base + _p_55883);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_28933)){
        _28934 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_28933)->dbl));
    }
    else{
        _28934 = (int)*(((s1_ptr)_2)->base + _28933);
    }
    _2 = (int)SEQ_PTR(_28934);
    _28935 = (int)*(((s1_ptr)_2)->base + 3);
    _28934 = NOVALUE;
    if (IS_ATOM_INT(_28935)) {
        _28936 = (_28935 == 3);
    }
    else {
        _28936 = binary_op(EQUALS, _28935, 3);
    }
    _28935 = NOVALUE;
    DeRef(_28932);
    if (IS_ATOM_INT(_28936))
    _28932 = (_28936 != 0);
    else
    _28932 = DBL_PTR(_28936)->dbl != 0.0;
L5: 
    if (_28932 == 0)
    {
        _28932 = NOVALUE;
        goto L4; // [98] 150
    }
    else{
        _28932 = NOVALUE;
    }

    /** 					symtab_index ts = NewTempSym()*/
    _ts_55913 = _55NewTempSym(0);
    if (!IS_ATOM_INT(_ts_55913)) {
        _1 = (long)(DBL_PTR(_ts_55913)->dbl);
        if (UNIQUE(DBL_PTR(_ts_55913)) && (DBL_PTR(_ts_55913)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ts_55913);
        _ts_55913 = _1;
    }

    /** 					Code &= { ASSIGN, private_sym[p], ts }*/
    _2 = (int)SEQ_PTR(_38private_sym_17078);
    _28938 = (int)*(((s1_ptr)_2)->base + _p_55883);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 18;
    Ref(_28938);
    *((int *)(_2+8)) = _28938;
    *((int *)(_2+12)) = _ts_55913;
    _28939 = MAKE_SEQ(_1);
    _28938 = NOVALUE;
    Concat((object_ptr)&_38Code_17038, _38Code_17038, _28939);
    DeRefDS(_28939);
    _28939 = NOVALUE;

    /** 					return {VARIABLE, ts}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -100;
    ((int *)_2)[2] = _ts_55913;
    _28941 = MAKE_SEQ(_1);
    DeRef(_t_55882);
    _28933 = NOVALUE;
    DeRef(_28931);
    _28931 = NOVALUE;
    DeRef(_28936);
    _28936 = NOVALUE;
    return _28941;
    goto L6; // [147] 169
L4: 

    /** 					return {VARIABLE, private_sym[p]}*/
    _2 = (int)SEQ_PTR(_38private_sym_17078);
    _28942 = (int)*(((s1_ptr)_2)->base + _p_55883);
    Ref(_28942);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -100;
    ((int *)_2)[2] = _28942;
    _28943 = MAKE_SEQ(_1);
    _28942 = NOVALUE;
    DeRef(_t_55882);
    _28933 = NOVALUE;
    DeRef(_28931);
    _28931 = NOVALUE;
    DeRef(_28941);
    _28941 = NOVALUE;
    DeRef(_28936);
    _28936 = NOVALUE;
    return _28943;
L6: 
L3: 
L2: 

    /** 		prev_Nne = No_new_entry*/
    _prev_Nne_55884 = _55No_new_entry_48227;

    /** 		No_new_entry = 1*/
    _55No_new_entry_48227 = 1;

    /** 		if Recorded_sym[n] > 0 and  sym_scope( Recorded_sym[n] ) != SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_38Recorded_sym_17074);
    _28944 = (int)*(((s1_ptr)_2)->base + _n_55880);
    if (IS_ATOM_INT(_28944)) {
        _28945 = (_28944 > 0);
    }
    else {
        _28945 = binary_op(GREATER, _28944, 0);
    }
    _28944 = NOVALUE;
    if (IS_ATOM_INT(_28945)) {
        if (_28945 == 0) {
            goto L7; // [199] 250
        }
    }
    else {
        if (DBL_PTR(_28945)->dbl == 0.0) {
            goto L7; // [199] 250
        }
    }
    _2 = (int)SEQ_PTR(_38Recorded_sym_17074);
    _28947 = (int)*(((s1_ptr)_2)->base + _n_55880);
    Ref(_28947);
    _28948 = _55sym_scope(_28947);
    _28947 = NOVALUE;
    if (IS_ATOM_INT(_28948)) {
        _28949 = (_28948 != 9);
    }
    else {
        _28949 = binary_op(NOTEQ, _28948, 9);
    }
    DeRef(_28948);
    _28948 = NOVALUE;
    if (_28949 == 0) {
        DeRef(_28949);
        _28949 = NOVALUE;
        goto L7; // [220] 250
    }
    else {
        if (!IS_ATOM_INT(_28949) && DBL_PTR(_28949)->dbl == 0.0){
            DeRef(_28949);
            _28949 = NOVALUE;
            goto L7; // [220] 250
        }
        DeRef(_28949);
        _28949 = NOVALUE;
    }
    DeRef(_28949);
    _28949 = NOVALUE;

    /** 			t = { sym_token( Recorded_sym[n] ), Recorded_sym[n] }*/
    _2 = (int)SEQ_PTR(_38Recorded_sym_17074);
    _28950 = (int)*(((s1_ptr)_2)->base + _n_55880);
    Ref(_28950);
    _28951 = _55sym_token(_28950);
    _28950 = NOVALUE;
    _2 = (int)SEQ_PTR(_38Recorded_sym_17074);
    _28952 = (int)*(((s1_ptr)_2)->base + _n_55880);
    Ref(_28952);
    DeRef(_t_55882);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _28951;
    ((int *)_2)[2] = _28952;
    _t_55882 = MAKE_SEQ(_1);
    _28952 = NOVALUE;
    _28951 = NOVALUE;

    /** 			break "top if"*/
    goto L8; // [247] 728
L7: 

    /** 		t = keyfind(Recorded[n],-1)*/
    _2 = (int)SEQ_PTR(_38Recorded_17072);
    _28954 = (int)*(((s1_ptr)_2)->base + _n_55880);
    RefDS(_28954);
    DeRef(_32362);
    _32362 = _28954;
    _32363 = _55hashfn(_32362);
    _32362 = NOVALUE;
    RefDS(_28954);
    _0 = _t_55882;
    _t_55882 = _55keyfind(_28954, -1, _38current_file_no_16946, 0, _32363);
    DeRef(_0);
    _28954 = NOVALUE;
    _32363 = NOVALUE;

    /** 		if t[T_ID] = IGNORED then*/
    _2 = (int)SEQ_PTR(_t_55882);
    _28956 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _28956, 509)){
        _28956 = NOVALUE;
        goto L8; // [286] 728
    }
    _28956 = NOVALUE;

    /** 	        p = Recorded_sym[n]*/
    _2 = (int)SEQ_PTR(_38Recorded_sym_17074);
    _p_55883 = (int)*(((s1_ptr)_2)->base + _n_55880);
    if (!IS_ATOM_INT(_p_55883)){
        _p_55883 = (long)DBL_PTR(_p_55883)->dbl;
    }

    /** 	        if p = 0 then*/
    if (_p_55883 != 0)
    goto L9; // [302] 380

    /** 				No_new_entry = 0*/
    _55No_new_entry_48227 = 0;

    /** 				t = keyfind( Recorded[n], -1 )*/
    _2 = (int)SEQ_PTR(_38Recorded_17072);
    _28960 = (int)*(((s1_ptr)_2)->base + _n_55880);
    RefDS(_28960);
    DeRef(_32360);
    _32360 = _28960;
    _32361 = _55hashfn(_32360);
    _32360 = NOVALUE;
    RefDS(_28960);
    _0 = _t_55882;
    _t_55882 = _55keyfind(_28960, -1, _38current_file_no_16946, 0, _32361);
    DeRef(_0);
    _28960 = NOVALUE;
    _32361 = NOVALUE;

    /** 				No_new_entry = 1*/
    _55No_new_entry_48227 = 1;

    /** 				if t[T_ID] = IGNORED then*/
    _2 = (int)SEQ_PTR(_t_55882);
    _28962 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _28962, 509)){
        _28962 = NOVALUE;
        goto L8; // [355] 728
    }
    _28962 = NOVALUE;

    /** 					CompileErr(157,{Recorded[n]})*/
    _2 = (int)SEQ_PTR(_38Recorded_17072);
    _28965 = (int)*(((s1_ptr)_2)->base + _n_55880);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_28965);
    *((int *)(_2+4)) = _28965;
    _28966 = MAKE_SEQ(_1);
    _28965 = NOVALUE;
    _46CompileErr(157, _28966, 0);
    _28966 = NOVALUE;
    goto L8; // [377] 728
L9: 

    /** 				t = {SymTab[p][S_TOKEN], p}*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28967 = (int)*(((s1_ptr)_2)->base + _p_55883);
    _2 = (int)SEQ_PTR(_28967);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _28968 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _28968 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _28967 = NOVALUE;
    Ref(_28968);
    DeRef(_t_55882);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _28968;
    ((int *)_2)[2] = _p_55883;
    _t_55882 = MAKE_SEQ(_1);
    _28968 = NOVALUE;
    goto L8; // [400] 728
L1: 

    /** 		prev_Nne = No_new_entry*/
    _prev_Nne_55884 = _55No_new_entry_48227;

    /** 		No_new_entry = 1*/
    _55No_new_entry_48227 = 1;

    /** 		t = keyfind(Ns_recorded[n],-1, , 1)*/
    _2 = (int)SEQ_PTR(_38Ns_recorded_17073);
    _28970 = (int)*(((s1_ptr)_2)->base + _n_55880);
    Ref(_28970);
    DeRef(_32358);
    _32358 = _28970;
    _32359 = _55hashfn(_32358);
    _32358 = NOVALUE;
    Ref(_28970);
    _0 = _t_55882;
    _t_55882 = _55keyfind(_28970, -1, _38current_file_no_16946, 1, _32359);
    DeRef(_0);
    _28970 = NOVALUE;
    _32359 = NOVALUE;

    /** 		if t[T_ID] != NAMESPACE then*/
    _2 = (int)SEQ_PTR(_t_55882);
    _28972 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _28972, 523)){
        _28972 = NOVALUE;
        goto LA; // [454] 520
    }
    _28972 = NOVALUE;

    /** 			p = Ns_recorded_sym[n]*/
    _2 = (int)SEQ_PTR(_38Ns_recorded_sym_17075);
    _p_55883 = (int)*(((s1_ptr)_2)->base + _n_55880);
    if (!IS_ATOM_INT(_p_55883)){
        _p_55883 = (long)DBL_PTR(_p_55883)->dbl;
    }

    /** 			if p = 0 or sym_token( p ) != NAMESPACE then*/
    _28975 = (_p_55883 == 0);
    if (_28975 != 0) {
        goto LB; // [474] 493
    }
    _28977 = _55sym_token(_p_55883);
    if (IS_ATOM_INT(_28977)) {
        _28978 = (_28977 != 523);
    }
    else {
        _28978 = binary_op(NOTEQ, _28977, 523);
    }
    DeRef(_28977);
    _28977 = NOVALUE;
    if (_28978 == 0) {
        DeRef(_28978);
        _28978 = NOVALUE;
        goto LC; // [489] 511
    }
    else {
        if (!IS_ATOM_INT(_28978) && DBL_PTR(_28978)->dbl == 0.0){
            DeRef(_28978);
            _28978 = NOVALUE;
            goto LC; // [489] 511
        }
        DeRef(_28978);
        _28978 = NOVALUE;
    }
    DeRef(_28978);
    _28978 = NOVALUE;
LB: 

    /** 				CompileErr(153, {Ns_recorded[n]})*/
    _2 = (int)SEQ_PTR(_38Ns_recorded_17073);
    _28980 = (int)*(((s1_ptr)_2)->base + _n_55880);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_28980);
    *((int *)(_2+4)) = _28980;
    _28981 = MAKE_SEQ(_1);
    _28980 = NOVALUE;
    _46CompileErr(153, _28981, 0);
    _28981 = NOVALUE;
LC: 

    /** 			t = {NAMESPACE, p}*/
    DeRef(_t_55882);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 523;
    ((int *)_2)[2] = _p_55883;
    _t_55882 = MAKE_SEQ(_1);
LA: 

    /** 		t = keyfind(Recorded[n],SymTab[t[T_SYM]][S_OBJ])*/
    _2 = (int)SEQ_PTR(_38Recorded_17072);
    _28983 = (int)*(((s1_ptr)_2)->base + _n_55880);
    _2 = (int)SEQ_PTR(_t_55882);
    _28984 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_28984)){
        _28985 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_28984)->dbl));
    }
    else{
        _28985 = (int)*(((s1_ptr)_2)->base + _28984);
    }
    _2 = (int)SEQ_PTR(_28985);
    _28986 = (int)*(((s1_ptr)_2)->base + 1);
    _28985 = NOVALUE;
    RefDS(_28983);
    DeRef(_32356);
    _32356 = _28983;
    _32357 = _55hashfn(_32356);
    _32356 = NOVALUE;
    RefDS(_28983);
    Ref(_28986);
    _0 = _t_55882;
    _t_55882 = _55keyfind(_28983, _28986, _38current_file_no_16946, 0, _32357);
    DeRef(_0);
    _28983 = NOVALUE;
    _28986 = NOVALUE;
    _32357 = NOVALUE;

    /** 		if t[T_ID] = IGNORED then*/
    _2 = (int)SEQ_PTR(_t_55882);
    _28988 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _28988, 509)){
        _28988 = NOVALUE;
        goto LD; // [573] 630
    }
    _28988 = NOVALUE;

    /** 	        p = Recorded_sym[n]*/
    _2 = (int)SEQ_PTR(_38Recorded_sym_17074);
    _p_55883 = (int)*(((s1_ptr)_2)->base + _n_55880);
    if (!IS_ATOM_INT(_p_55883)){
        _p_55883 = (long)DBL_PTR(_p_55883)->dbl;
    }

    /** 	        if p = 0 then*/
    if (_p_55883 != 0)
    goto LE; // [589] 611

    /** 	        	CompileErr(157,{Recorded[n]})*/
    _2 = (int)SEQ_PTR(_38Recorded_17072);
    _28992 = (int)*(((s1_ptr)_2)->base + _n_55880);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_28992);
    *((int *)(_2+4)) = _28992;
    _28993 = MAKE_SEQ(_1);
    _28992 = NOVALUE;
    _46CompileErr(157, _28993, 0);
    _28993 = NOVALUE;
LE: 

    /** 		    t = {SymTab[p][S_TOKEN], p}*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28994 = (int)*(((s1_ptr)_2)->base + _p_55883);
    _2 = (int)SEQ_PTR(_28994);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _28995 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _28995 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _28994 = NOVALUE;
    Ref(_28995);
    DeRef(_t_55882);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _28995;
    ((int *)_2)[2] = _p_55883;
    _t_55882 = MAKE_SEQ(_1);
    _28995 = NOVALUE;
LD: 

    /** 		n = t[T_ID]*/
    _2 = (int)SEQ_PTR(_t_55882);
    _n_55880 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_n_55880)){
        _n_55880 = (long)DBL_PTR(_n_55880)->dbl;
    }

    /** 		if n = VARIABLE then*/
    if (_n_55880 != -100)
    goto LF; // [644] 660

    /** 			n = QUALIFIED_VARIABLE*/
    _n_55880 = 512;
    goto L10; // [657] 719
LF: 

    /** 		elsif n = FUNC then*/
    if (_n_55880 != 501)
    goto L11; // [664] 680

    /** 			n = QUALIFIED_FUNC*/
    _n_55880 = 520;
    goto L10; // [677] 719
L11: 

    /** 		elsif n = PROC then*/
    if (_n_55880 != 27)
    goto L12; // [684] 700

    /** 			n = QUALIFIED_PROC*/
    _n_55880 = 521;
    goto L10; // [697] 719
L12: 

    /** 		elsif n = TYPE then*/
    if (_n_55880 != 504)
    goto L13; // [704] 718

    /** 			n = QUALIFIED_TYPE*/
    _n_55880 = 522;
L13: 
L10: 

    /** 		t[T_ID] = n*/
    _2 = (int)SEQ_PTR(_t_55882);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _t_55882 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _n_55880;
    DeRef(_1);
L8: 

    /** 	No_new_entry = prev_Nne*/
    _55No_new_entry_48227 = _prev_Nne_55884;

    /**   	return t*/
    _28933 = NOVALUE;
    DeRef(_28931);
    _28931 = NOVALUE;
    DeRef(_28941);
    _28941 = NOVALUE;
    DeRef(_28936);
    _28936 = NOVALUE;
    DeRef(_28943);
    _28943 = NOVALUE;
    DeRef(_28975);
    _28975 = NOVALUE;
    DeRef(_28945);
    _28945 = NOVALUE;
    _28984 = NOVALUE;
    return _t_55882;
    ;
}


int _41next_token()
{
    int _t_56063 = NOVALUE;
    int _s_56064 = NOVALUE;
    int _29034 = NOVALUE;
    int _29033 = NOVALUE;
    int _29032 = NOVALUE;
    int _29031 = NOVALUE;
    int _29030 = NOVALUE;
    int _29029 = NOVALUE;
    int _29028 = NOVALUE;
    int _29027 = NOVALUE;
    int _29025 = NOVALUE;
    int _29024 = NOVALUE;
    int _29023 = NOVALUE;
    int _29022 = NOVALUE;
    int _29020 = NOVALUE;
    int _29018 = NOVALUE;
    int _29016 = NOVALUE;
    int _29012 = NOVALUE;
    int _29009 = NOVALUE;
    int _29006 = NOVALUE;
    int _29004 = NOVALUE;
    int _29002 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence s*/

    /** 	if length(backed_up_tok) > 0 then*/
    if (IS_SEQUENCE(_41backed_up_tok_55129)){
            _29002 = SEQ_PTR(_41backed_up_tok_55129)->length;
    }
    else {
        _29002 = 1;
    }
    if (_29002 <= 0)
    goto L1; // [10] 82

    /** 		t = backed_up_tok[$]*/
    if (IS_SEQUENCE(_41backed_up_tok_55129)){
            _29004 = SEQ_PTR(_41backed_up_tok_55129)->length;
    }
    else {
        _29004 = 1;
    }
    DeRef(_t_56063);
    _2 = (int)SEQ_PTR(_41backed_up_tok_55129);
    _t_56063 = (int)*(((s1_ptr)_2)->base + _29004);
    Ref(_t_56063);

    /** 		backed_up_tok = remove( backed_up_tok, length( backed_up_tok ) )*/
    if (IS_SEQUENCE(_41backed_up_tok_55129)){
            _29006 = SEQ_PTR(_41backed_up_tok_55129)->length;
    }
    else {
        _29006 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_41backed_up_tok_55129);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_29006)) ? _29006 : (long)(DBL_PTR(_29006)->dbl);
        int stop = (IS_ATOM_INT(_29006)) ? _29006 : (long)(DBL_PTR(_29006)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_41backed_up_tok_55129), start, &_41backed_up_tok_55129 );
            }
            else Tail(SEQ_PTR(_41backed_up_tok_55129), stop+1, &_41backed_up_tok_55129);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_41backed_up_tok_55129), start, &_41backed_up_tok_55129);
        }
        else {
            assign_slice_seq = &assign_space;
            _41backed_up_tok_55129 = Remove_elements(start, stop, (SEQ_PTR(_41backed_up_tok_55129)->ref == 1));
        }
    }
    _29006 = NOVALUE;
    _29006 = NOVALUE;

    /** 		if putback_fwd_line_number then*/
    if (_38putback_fwd_line_number_16949 == 0)
    {
        goto L2; // [43] 349
    }
    else{
    }

    /** 			ForwardLine     = putback_ForwardLine*/
    Ref(_46putback_ForwardLine_49484);
    DeRef(_46ForwardLine_49483);
    _46ForwardLine_49483 = _46putback_ForwardLine_49484;

    /** 			forward_bp      = putback_forward_bp*/
    _46forward_bp_49487 = _46putback_forward_bp_49488;

    /** 			fwd_line_number = putback_fwd_line_number*/
    _38fwd_line_number_16948 = _38putback_fwd_line_number_16949;

    /** 			putback_fwd_line_number = 0*/
    _38putback_fwd_line_number_16949 = 0;
    goto L2; // [79] 349
L1: 

    /** 	elsif Parser_mode = PAM_PLAYBACK then*/
    if (_38Parser_mode_17071 != -1)
    goto L3; // [88] 302

    /** 		if canned_index <= length(canned_tokens) then*/
    if (IS_SEQUENCE(_41canned_tokens_55166)){
            _29009 = SEQ_PTR(_41canned_tokens_55166)->length;
    }
    else {
        _29009 = 1;
    }
    if (_41canned_index_55167 > _29009)
    goto L4; // [101] 150

    /** 			t = canned_tokens[canned_index]*/
    DeRef(_t_56063);
    _2 = (int)SEQ_PTR(_41canned_tokens_55166);
    _t_56063 = (int)*(((s1_ptr)_2)->base + _41canned_index_55167);
    Ref(_t_56063);

    /** 			if canned_index < length(canned_tokens) then*/
    if (IS_SEQUENCE(_41canned_tokens_55166)){
            _29012 = SEQ_PTR(_41canned_tokens_55166)->length;
    }
    else {
        _29012 = 1;
    }
    if (_41canned_index_55167 >= _29012)
    goto L5; // [124] 139

    /** 				canned_index += 1*/
    _41canned_index_55167 = _41canned_index_55167 + 1;
    goto L6; // [136] 157
L5: 

    /** 	            s = restore_parser()*/
    _0 = _s_56064;
    _s_56064 = _41restore_parser();
    DeRef(_0);
    goto L6; // [147] 157
L4: 

    /** 	    	InternalErr(266)*/
    RefDS(_22663);
    _46InternalErr(266, _22663);
L6: 

    /** 		if t[T_ID] = RECORDED then*/
    _2 = (int)SEQ_PTR(_t_56063);
    _29016 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29016, 508)){
        _29016 = NOVALUE;
        goto L7; // [169] 188
    }
    _29016 = NOVALUE;

    /** 			t=read_recorded_token(t[T_SYM])*/
    _2 = (int)SEQ_PTR(_t_56063);
    _29018 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_29018);
    _0 = _t_56063;
    _t_56063 = _41read_recorded_token(_29018);
    DeRef(_0);
    _29018 = NOVALUE;
    goto L2; // [185] 349
L7: 

    /** 		elsif t[T_ID] = DEF_PARAM then*/
    _2 = (int)SEQ_PTR(_t_56063);
    _29020 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29020, 510)){
        _29020 = NOVALUE;
        goto L2; // [198] 349
    }
    _29020 = NOVALUE;

    /**         	for i=length(nested_calls) to 1 by -1 do*/
    if (IS_SEQUENCE(_41nested_calls_55856)){
            _29022 = SEQ_PTR(_41nested_calls_55856)->length;
    }
    else {
        _29022 = 1;
    }
    {
        int _i_56111;
        _i_56111 = _29022;
L8: 
        if (_i_56111 < 1){
            goto L9; // [209] 290
        }

        /**         	    if nested_calls[i] = t[T_SYM][2] then*/
        _2 = (int)SEQ_PTR(_41nested_calls_55856);
        _29023 = (int)*(((s1_ptr)_2)->base + _i_56111);
        _2 = (int)SEQ_PTR(_t_56063);
        _29024 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_29024);
        _29025 = (int)*(((s1_ptr)_2)->base + 2);
        _29024 = NOVALUE;
        if (binary_op_a(NOTEQ, _29023, _29025)){
            _29023 = NOVALUE;
            _29025 = NOVALUE;
            goto LA; // [234] 283
        }
        _29023 = NOVALUE;
        _29025 = NOVALUE;

        /** 					return {VARIABLE, private_sym[parseargs_states[i][PS_POSITION]+t[T_SYM][1]]}*/
        _2 = (int)SEQ_PTR(_41parseargs_states_55845);
        _29027 = (int)*(((s1_ptr)_2)->base + _i_56111);
        _2 = (int)SEQ_PTR(_29027);
        _29028 = (int)*(((s1_ptr)_2)->base + 1);
        _29027 = NOVALUE;
        _2 = (int)SEQ_PTR(_t_56063);
        _29029 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_29029);
        _29030 = (int)*(((s1_ptr)_2)->base + 1);
        _29029 = NOVALUE;
        if (IS_ATOM_INT(_29028) && IS_ATOM_INT(_29030)) {
            _29031 = _29028 + _29030;
        }
        else {
            _29031 = binary_op(PLUS, _29028, _29030);
        }
        _29028 = NOVALUE;
        _29030 = NOVALUE;
        _2 = (int)SEQ_PTR(_38private_sym_17078);
        if (!IS_ATOM_INT(_29031)){
            _29032 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29031)->dbl));
        }
        else{
            _29032 = (int)*(((s1_ptr)_2)->base + _29031);
        }
        Ref(_29032);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = -100;
        ((int *)_2)[2] = _29032;
        _29033 = MAKE_SEQ(_1);
        _29032 = NOVALUE;
        DeRef(_t_56063);
        DeRef(_s_56064);
        DeRef(_29031);
        _29031 = NOVALUE;
        return _29033;
LA: 

        /** 			end for*/
        _i_56111 = _i_56111 + -1;
        goto L8; // [285] 216
L9: 
        ;
    }

    /** 			CompileErr(98)*/
    RefDS(_22663);
    _46CompileErr(98, _22663, 0);
    goto L2; // [299] 349
L3: 

    /** 	elsif lock_scanner then*/
    if (_41lock_scanner_55854 == 0)
    {
        goto LB; // [306] 324
    }
    else{
    }

    /** 		return {PLAYBACK_ENDS,0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 505;
    ((int *)_2)[2] = 0;
    _29034 = MAKE_SEQ(_1);
    DeRef(_t_56063);
    DeRef(_s_56064);
    DeRef(_29033);
    _29033 = NOVALUE;
    DeRef(_29031);
    _29031 = NOVALUE;
    return _29034;
    goto L2; // [321] 349
LB: 

    /** 	    t = Scanner()*/
    _0 = _t_56063;
    _t_56063 = _62Scanner();
    DeRef(_0);

    /** 	    if Parser_mode = PAM_RECORD then*/
    if (_38Parser_mode_17071 != 1)
    goto LC; // [335] 348

    /** 	        canned_tokens = append(canned_tokens,t)*/
    Ref(_t_56063);
    Append(&_41canned_tokens_55166, _41canned_tokens_55166, _t_56063);
LC: 
L2: 

    /** 	putback_fwd_line_number = 0*/
    _38putback_fwd_line_number_16949 = 0;

    /** 	return t*/
    DeRef(_s_56064);
    DeRef(_29033);
    _29033 = NOVALUE;
    DeRef(_29034);
    _29034 = NOVALUE;
    DeRef(_29031);
    _29031 = NOVALUE;
    return _t_56063;
    ;
}


int _41Expr_list()
{
    int _tok_56146 = NOVALUE;
    int _n_56147 = NOVALUE;
    int _29050 = NOVALUE;
    int _29047 = NOVALUE;
    int _29046 = NOVALUE;
    int _29044 = NOVALUE;
    int _29043 = NOVALUE;
    int _29039 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer n*/

    /** 	tok = next_token()*/
    _0 = _tok_56146;
    _tok_56146 = _41next_token();
    DeRef(_0);

    /** 	putback(tok)*/
    Ref(_tok_56146);
    _41putback(_tok_56146);

    /** 	if tok[T_ID] = RIGHT_BRACE then*/
    _2 = (int)SEQ_PTR(_tok_56146);
    _29039 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29039, -25)){
        _29039 = NOVALUE;
        goto L1; // [23] 36
    }
    _29039 = NOVALUE;

    /** 		return 0*/
    DeRef(_tok_56146);
    return 0;
    goto L2; // [33] 142
L1: 

    /** 		n = 0*/
    _n_56147 = 0;

    /** 		short_circuit -= 1*/
    _41short_circuit_55122 = _41short_circuit_55122 - 1;

    /** 		while TRUE do*/
L3: 
    if (_9TRUE_428 == 0)
    {
        goto L4; // [56] 133
    }
    else{
    }

    /** 			gListItem &= 1*/
    Append(&_41gListItem_55158, _41gListItem_55158, 1);

    /** 			Expr()*/
    _41Expr();

    /** 			n += gListItem[$]*/
    if (IS_SEQUENCE(_41gListItem_55158)){
            _29043 = SEQ_PTR(_41gListItem_55158)->length;
    }
    else {
        _29043 = 1;
    }
    _2 = (int)SEQ_PTR(_41gListItem_55158);
    _29044 = (int)*(((s1_ptr)_2)->base + _29043);
    _n_56147 = _n_56147 + _29044;
    _29044 = NOVALUE;

    /** 			gListItem = gListItem[1 .. $-1]*/
    if (IS_SEQUENCE(_41gListItem_55158)){
            _29046 = SEQ_PTR(_41gListItem_55158)->length;
    }
    else {
        _29046 = 1;
    }
    _29047 = _29046 - 1;
    _29046 = NOVALUE;
    rhs_slice_target = (object_ptr)&_41gListItem_55158;
    RHS_Slice(_41gListItem_55158, 1, _29047);

    /** 			tok = next_token()*/
    _0 = _tok_56146;
    _tok_56146 = _41next_token();
    DeRef(_0);

    /** 			if tok[T_ID] != COMMA then*/
    _2 = (int)SEQ_PTR(_tok_56146);
    _29050 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _29050, -30)){
        _29050 = NOVALUE;
        goto L3; // [119] 54
    }
    _29050 = NOVALUE;

    /** 				exit*/
    goto L4; // [125] 133

    /** 		end while*/
    goto L3; // [130] 54
L4: 

    /** 		short_circuit += 1*/
    _41short_circuit_55122 = _41short_circuit_55122 + 1;
L2: 

    /** 	putback(tok)*/
    Ref(_tok_56146);
    _41putback(_tok_56146);

    /** 	return n*/
    DeRef(_tok_56146);
    DeRef(_29047);
    _29047 = NOVALUE;
    return _n_56147;
    ;
}


void _41tok_match(int _tok_56175, int _prevtok_56176)
{
    int _t_56178 = NOVALUE;
    int _expected_56179 = NOVALUE;
    int _actual_56180 = NOVALUE;
    int _prevname_56181 = NOVALUE;
    int _29062 = NOVALUE;
    int _29060 = NOVALUE;
    int _29057 = NOVALUE;
    int _29054 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence expected, actual, prevname*/

    /** 	t = next_token()*/
    _0 = _t_56178;
    _t_56178 = _41next_token();
    DeRef(_0);

    /** 	if t[T_ID] != tok then*/
    _2 = (int)SEQ_PTR(_t_56178);
    _29054 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _29054, _tok_56175)){
        _29054 = NOVALUE;
        goto L1; // [20] 92
    }
    _29054 = NOVALUE;

    /** 		expected = LexName(tok)*/
    RefDS(_27166);
    _0 = _expected_56179;
    _expected_56179 = _43LexName(_tok_56175, _27166);
    DeRef(_0);

    /** 		actual = LexName(t[T_ID])*/
    _2 = (int)SEQ_PTR(_t_56178);
    _29057 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_29057);
    RefDS(_27166);
    _0 = _actual_56180;
    _actual_56180 = _43LexName(_29057, _27166);
    DeRef(_0);
    _29057 = NOVALUE;

    /** 		if prevtok = 0 then*/
    if (_prevtok_56176 != 0)
    goto L2; // [50] 68

    /** 			CompileErr(132, {expected, actual})*/
    RefDS(_actual_56180);
    RefDS(_expected_56179);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _expected_56179;
    ((int *)_2)[2] = _actual_56180;
    _29060 = MAKE_SEQ(_1);
    _46CompileErr(132, _29060, 0);
    _29060 = NOVALUE;
    goto L3; // [65] 91
L2: 

    /** 			prevname = LexName(prevtok)*/
    RefDS(_27166);
    _0 = _prevname_56181;
    _prevname_56181 = _43LexName(_prevtok_56176, _27166);
    DeRef(_0);

    /** 			CompileErr(138, {expected, prevname, actual})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_expected_56179);
    *((int *)(_2+4)) = _expected_56179;
    RefDS(_prevname_56181);
    *((int *)(_2+8)) = _prevname_56181;
    RefDS(_actual_56180);
    *((int *)(_2+12)) = _actual_56180;
    _29062 = MAKE_SEQ(_1);
    _46CompileErr(138, _29062, 0);
    _29062 = NOVALUE;
L3: 
L1: 

    /** end procedure*/
    DeRef(_t_56178);
    DeRef(_expected_56179);
    DeRef(_actual_56180);
    DeRef(_prevname_56181);
    return;
    ;
}


void _41UndefinedVar(int _s_56215)
{
    int _dup_56217 = NOVALUE;
    int _errmsg_56218 = NOVALUE;
    int _rname_56219 = NOVALUE;
    int _fname_56220 = NOVALUE;
    int _29085 = NOVALUE;
    int _29084 = NOVALUE;
    int _29082 = NOVALUE;
    int _29080 = NOVALUE;
    int _29079 = NOVALUE;
    int _29077 = NOVALUE;
    int _29075 = NOVALUE;
    int _29073 = NOVALUE;
    int _29072 = NOVALUE;
    int _29071 = NOVALUE;
    int _29070 = NOVALUE;
    int _29069 = NOVALUE;
    int _29067 = NOVALUE;
    int _29066 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_s_56215)) {
        _1 = (long)(DBL_PTR(_s_56215)->dbl);
        if (UNIQUE(DBL_PTR(_s_56215)) && (DBL_PTR(_s_56215)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_56215);
        _s_56215 = _1;
    }

    /** 	sequence errmsg*/

    /** 	sequence rname*/

    /** 	sequence fname*/

    /** 	if SymTab[s][S_SCOPE] = SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29066 = (int)*(((s1_ptr)_2)->base + _s_56215);
    _2 = (int)SEQ_PTR(_29066);
    _29067 = (int)*(((s1_ptr)_2)->base + 4);
    _29066 = NOVALUE;
    if (binary_op_a(NOTEQ, _29067, 9)){
        _29067 = NOVALUE;
        goto L1; // [25] 55
    }
    _29067 = NOVALUE;

    /** 		CompileErr(19, {SymTab[s][S_NAME]})*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29069 = (int)*(((s1_ptr)_2)->base + _s_56215);
    _2 = (int)SEQ_PTR(_29069);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _29070 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _29070 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _29069 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_29070);
    *((int *)(_2+4)) = _29070;
    _29071 = MAKE_SEQ(_1);
    _29070 = NOVALUE;
    _46CompileErr(19, _29071, 0);
    _29071 = NOVALUE;
    goto L2; // [52] 202
L1: 

    /** 	elsif SymTab[s][S_SCOPE] = SC_MULTIPLY_DEFINED then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29072 = (int)*(((s1_ptr)_2)->base + _s_56215);
    _2 = (int)SEQ_PTR(_29072);
    _29073 = (int)*(((s1_ptr)_2)->base + 4);
    _29072 = NOVALUE;
    if (binary_op_a(NOTEQ, _29073, 10)){
        _29073 = NOVALUE;
        goto L3; // [71] 179
    }
    _29073 = NOVALUE;

    /** 		rname = SymTab[s][S_NAME]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29075 = (int)*(((s1_ptr)_2)->base + _s_56215);
    DeRef(_rname_56219);
    _2 = (int)SEQ_PTR(_29075);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _rname_56219 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _rname_56219 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    Ref(_rname_56219);
    _29075 = NOVALUE;

    /** 		errmsg = ""*/
    RefDS(_22663);
    DeRef(_errmsg_56218);
    _errmsg_56218 = _22663;

    /** 		for i = 1 to length(dup_globals) do*/
    if (IS_SEQUENCE(_55dup_globals_48216)){
            _29077 = SEQ_PTR(_55dup_globals_48216)->length;
    }
    else {
        _29077 = 1;
    }
    {
        int _i_56246;
        _i_56246 = 1;
L4: 
        if (_i_56246 > _29077){
            goto L5; // [105] 163
        }

        /** 			dup = dup_globals[i]*/
        _2 = (int)SEQ_PTR(_55dup_globals_48216);
        _dup_56217 = (int)*(((s1_ptr)_2)->base + _i_56246);
        if (!IS_ATOM_INT(_dup_56217)){
            _dup_56217 = (long)DBL_PTR(_dup_56217)->dbl;
        }

        /** 			fname = known_files[SymTab[dup][S_FILE_NO]]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _29079 = (int)*(((s1_ptr)_2)->base + _dup_56217);
        _2 = (int)SEQ_PTR(_29079);
        if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
            _29080 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
        }
        else{
            _29080 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
        }
        _29079 = NOVALUE;
        DeRef(_fname_56220);
        _2 = (int)SEQ_PTR(_35known_files_15596);
        if (!IS_ATOM_INT(_29080)){
            _fname_56220 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29080)->dbl));
        }
        else{
            _fname_56220 = (int)*(((s1_ptr)_2)->base + _29080);
        }
        Ref(_fname_56220);

        /** 			errmsg &= "    " & fname & "\n"*/
        {
            int concat_list[3];

            concat_list[0] = _22815;
            concat_list[1] = _fname_56220;
            concat_list[2] = _25639;
            Concat_N((object_ptr)&_29082, concat_list, 3);
        }
        Concat((object_ptr)&_errmsg_56218, _errmsg_56218, _29082);
        DeRefDS(_29082);
        _29082 = NOVALUE;

        /** 		end for*/
        _i_56246 = _i_56246 + 1;
        goto L4; // [158] 112
L5: 
        ;
    }

    /** 		CompileErr(23, {rname, rname, errmsg})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDSn(_rname_56219, 2);
    *((int *)(_2+4)) = _rname_56219;
    *((int *)(_2+8)) = _rname_56219;
    RefDS(_errmsg_56218);
    *((int *)(_2+12)) = _errmsg_56218;
    _29084 = MAKE_SEQ(_1);
    _46CompileErr(23, _29084, 0);
    _29084 = NOVALUE;
    goto L2; // [176] 202
L3: 

    /** 	elsif length(symbol_resolution_warning) then*/
    if (IS_SEQUENCE(_38symbol_resolution_warning_17065)){
            _29085 = SEQ_PTR(_38symbol_resolution_warning_17065)->length;
    }
    else {
        _29085 = 1;
    }
    if (_29085 == 0)
    {
        _29085 = NOVALUE;
        goto L6; // [186] 201
    }
    else{
        _29085 = NOVALUE;
    }

    /** 		Warning( symbol_resolution_warning, resolution_warning_flag)*/
    RefDS(_38symbol_resolution_warning_17065);
    RefDS(_22663);
    _46Warning(_38symbol_resolution_warning_17065, 1, _22663);
L6: 
L2: 

    /** end procedure*/
    DeRef(_errmsg_56218);
    DeRef(_rname_56219);
    DeRef(_fname_56220);
    _29080 = NOVALUE;
    return;
    ;
}


void _41WrongNumberArgs(int _subsym_56270, int _only_56271)
{
    int _msgno_56272 = NOVALUE;
    int _29097 = NOVALUE;
    int _29096 = NOVALUE;
    int _29095 = NOVALUE;
    int _29094 = NOVALUE;
    int _29093 = NOVALUE;
    int _29091 = NOVALUE;
    int _29089 = NOVALUE;
    int _29087 = NOVALUE;
    int _29086 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_subsym_56270)) {
        _1 = (long)(DBL_PTR(_subsym_56270)->dbl);
        if (UNIQUE(DBL_PTR(_subsym_56270)) && (DBL_PTR(_subsym_56270)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subsym_56270);
        _subsym_56270 = _1;
    }

    /** 	if SymTab[subsym][S_NUM_ARGS] = 1 then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29086 = (int)*(((s1_ptr)_2)->base + _subsym_56270);
    _2 = (int)SEQ_PTR(_29086);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _29087 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _29087 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    _29086 = NOVALUE;
    if (binary_op_a(NOTEQ, _29087, 1)){
        _29087 = NOVALUE;
        goto L1; // [19] 49
    }
    _29087 = NOVALUE;

    /** 		if length(only) = 0 then*/
    if (IS_SEQUENCE(_only_56271)){
            _29089 = SEQ_PTR(_only_56271)->length;
    }
    else {
        _29089 = 1;
    }
    if (_29089 != 0)
    goto L2; // [28] 40

    /** 			msgno = 20*/
    _msgno_56272 = 20;
    goto L3; // [37] 73
L2: 

    /** 			msgno = 237*/
    _msgno_56272 = 237;
    goto L3; // [46] 73
L1: 

    /** 		if length(only) = 0 then*/
    if (IS_SEQUENCE(_only_56271)){
            _29091 = SEQ_PTR(_only_56271)->length;
    }
    else {
        _29091 = 1;
    }
    if (_29091 != 0)
    goto L4; // [54] 66

    /** 			msgno = 236*/
    _msgno_56272 = 236;
    goto L5; // [63] 72
L4: 

    /** 			msgno = 238*/
    _msgno_56272 = 238;
L5: 
L3: 

    /** 	CompileErr(msgno, {SymTab[subsym][S_NAME], SymTab[subsym][S_NUM_ARGS]})*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29093 = (int)*(((s1_ptr)_2)->base + _subsym_56270);
    _2 = (int)SEQ_PTR(_29093);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _29094 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _29094 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _29093 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29095 = (int)*(((s1_ptr)_2)->base + _subsym_56270);
    _2 = (int)SEQ_PTR(_29095);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _29096 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _29096 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    _29095 = NOVALUE;
    Ref(_29096);
    Ref(_29094);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _29094;
    ((int *)_2)[2] = _29096;
    _29097 = MAKE_SEQ(_1);
    _29096 = NOVALUE;
    _29094 = NOVALUE;
    _46CompileErr(_msgno_56272, _29097, 0);
    _29097 = NOVALUE;

    /** end procedure*/
    DeRefDSi(_only_56271);
    return;
    ;
}


void _41MissingArgs(int _subsym_56301)
{
    int _eentry_56302 = NOVALUE;
    int _29102 = NOVALUE;
    int _29101 = NOVALUE;
    int _29100 = NOVALUE;
    int _29099 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence eentry = SymTab[subsym]*/
    DeRef(_eentry_56302);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _eentry_56302 = (int)*(((s1_ptr)_2)->base + _subsym_56301);
    Ref(_eentry_56302);

    /** 	CompileErr(235, {eentry[S_NAME], eentry[S_DEF_ARGS][2]})*/
    _2 = (int)SEQ_PTR(_eentry_56302);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _29099 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _29099 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _2 = (int)SEQ_PTR(_eentry_56302);
    _29100 = (int)*(((s1_ptr)_2)->base + 28);
    _2 = (int)SEQ_PTR(_29100);
    _29101 = (int)*(((s1_ptr)_2)->base + 2);
    _29100 = NOVALUE;
    Ref(_29101);
    Ref(_29099);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _29099;
    ((int *)_2)[2] = _29101;
    _29102 = MAKE_SEQ(_1);
    _29101 = NOVALUE;
    _29099 = NOVALUE;
    _46CompileErr(235, _29102, 0);
    _29102 = NOVALUE;

    /** end procedure*/
    DeRefDS(_eentry_56302);
    return;
    ;
}


void _41Parse_default_arg(int _subsym_56315, int _arg_56316, int _fwd_private_list_56317, int _fwd_private_sym_56318)
{
    int _param_56320 = NOVALUE;
    int _29122 = NOVALUE;
    int _29121 = NOVALUE;
    int _29120 = NOVALUE;
    int _29119 = NOVALUE;
    int _29118 = NOVALUE;
    int _29117 = NOVALUE;
    int _29116 = NOVALUE;
    int _29115 = NOVALUE;
    int _29114 = NOVALUE;
    int _29113 = NOVALUE;
    int _29112 = NOVALUE;
    int _29111 = NOVALUE;
    int _29110 = NOVALUE;
    int _29108 = NOVALUE;
    int _29107 = NOVALUE;
    int _29104 = NOVALUE;
    int _29103 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_subsym_56315)) {
        _1 = (long)(DBL_PTR(_subsym_56315)->dbl);
        if (UNIQUE(DBL_PTR(_subsym_56315)) && (DBL_PTR(_subsym_56315)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subsym_56315);
        _subsym_56315 = _1;
    }
    if (!IS_ATOM_INT(_arg_56316)) {
        _1 = (long)(DBL_PTR(_arg_56316)->dbl);
        if (UNIQUE(DBL_PTR(_arg_56316)) && (DBL_PTR(_arg_56316)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_arg_56316);
        _arg_56316 = _1;
    }

    /** 	symtab_index param = subsym*/
    _param_56320 = _subsym_56315;

    /** 	on_arg = arg*/
    _41on_arg_55855 = _arg_56316;

    /** 	parseargs_states = append(parseargs_states,*/
    if (IS_SEQUENCE(_41private_list_55853)){
            _29103 = SEQ_PTR(_41private_list_55853)->length;
    }
    else {
        _29103 = 1;
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _29103;
    *((int *)(_2+8)) = _41lock_scanner_55854;
    *((int *)(_2+12)) = _38use_private_list_17079;
    *((int *)(_2+16)) = _41on_arg_55855;
    _29104 = MAKE_SEQ(_1);
    _29103 = NOVALUE;
    RefDS(_29104);
    Append(&_41parseargs_states_55845, _41parseargs_states_55845, _29104);
    DeRefDS(_29104);
    _29104 = NOVALUE;

    /** 	nested_calls &= subsym*/
    Append(&_41nested_calls_55856, _41nested_calls_55856, _subsym_56315);

    /** 	for i = 1 to arg do*/
    _29107 = _arg_56316;
    {
        int _i_56327;
        _i_56327 = 1;
L1: 
        if (_i_56327 > _29107){
            goto L2; // [60] 90
        }

        /** 		param = SymTab[param][S_NEXT]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _29108 = (int)*(((s1_ptr)_2)->base + _param_56320);
        _2 = (int)SEQ_PTR(_29108);
        _param_56320 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_param_56320)){
            _param_56320 = (long)DBL_PTR(_param_56320)->dbl;
        }
        _29108 = NOVALUE;

        /** 	end for*/
        _i_56327 = _i_56327 + 1;
        goto L1; // [85] 67
L2: 
        ;
    }

    /** 	private_list = fwd_private_list*/
    RefDS(_fwd_private_list_56317);
    DeRef(_41private_list_55853);
    _41private_list_55853 = _fwd_private_list_56317;

    /** 	private_sym  = fwd_private_sym*/
    RefDS(_fwd_private_sym_56318);
    DeRef(_38private_sym_17078);
    _38private_sym_17078 = _fwd_private_sym_56318;

    /** 	if atom(SymTab[param][S_CODE]) then  -- but no default set*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29110 = (int)*(((s1_ptr)_2)->base + _param_56320);
    _2 = (int)SEQ_PTR(_29110);
    if (!IS_ATOM_INT(_38S_CODE_16610)){
        _29111 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    }
    else{
        _29111 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
    }
    _29110 = NOVALUE;
    _29112 = IS_ATOM(_29111);
    _29111 = NOVALUE;
    if (_29112 == 0)
    {
        _29112 = NOVALUE;
        goto L3; // [121] 162
    }
    else{
        _29112 = NOVALUE;
    }

    /** 		CompileErr(26, {arg, SymTab[subsym][S_NAME], SymTab[param][S_NAME]})*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29113 = (int)*(((s1_ptr)_2)->base + _subsym_56315);
    _2 = (int)SEQ_PTR(_29113);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _29114 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _29114 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _29113 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29115 = (int)*(((s1_ptr)_2)->base + _param_56320);
    _2 = (int)SEQ_PTR(_29115);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _29116 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _29116 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _29115 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _arg_56316;
    Ref(_29114);
    *((int *)(_2+8)) = _29114;
    Ref(_29116);
    *((int *)(_2+12)) = _29116;
    _29117 = MAKE_SEQ(_1);
    _29116 = NOVALUE;
    _29114 = NOVALUE;
    _46CompileErr(26, _29117, 0);
    _29117 = NOVALUE;
L3: 

    /** 	use_private_list = 1*/
    _38use_private_list_17079 = 1;

    /** 	lock_scanner = 1*/
    _41lock_scanner_55854 = 1;

    /** 	start_playback(SymTab[param][S_CODE] )*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29118 = (int)*(((s1_ptr)_2)->base + _param_56320);
    _2 = (int)SEQ_PTR(_29118);
    if (!IS_ATOM_INT(_38S_CODE_16610)){
        _29119 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    }
    else{
        _29119 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
    }
    _29118 = NOVALUE;
    Ref(_29119);
    _41start_playback(_29119);
    _29119 = NOVALUE;

    /** 	call_proc(forward_expr, {})*/
    _0 = (int)_00[_41forward_expr_56142].addr;
    (*(int (*)())_0)(
                         );

    /** 	add_private_symbol( Top(), SymTab[param][S_NAME] )*/
    _29120 = _43Top();
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29121 = (int)*(((s1_ptr)_2)->base + _param_56320);
    _2 = (int)SEQ_PTR(_29121);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _29122 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _29122 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _29121 = NOVALUE;
    Ref(_29122);
    _40add_private_symbol(_29120, _29122);
    _29120 = NOVALUE;
    _29122 = NOVALUE;

    /** 	lock_scanner = 0*/
    _41lock_scanner_55854 = 0;

    /** 	restore_parseargs_states()*/
    _41restore_parseargs_states();

    /** end procedure*/
    DeRefDS(_fwd_private_list_56317);
    DeRefDS(_fwd_private_sym_56318);
    return;
    ;
}


void _41ParseArgs(int _subsym_56365)
{
    int _n_56366 = NOVALUE;
    int _fda_56367 = NOVALUE;
    int _lnda_56368 = NOVALUE;
    int _tok_56370 = NOVALUE;
    int _s_56372 = NOVALUE;
    int _var_code_56373 = NOVALUE;
    int _name_56374 = NOVALUE;
    int _29216 = NOVALUE;
    int _29214 = NOVALUE;
    int _29210 = NOVALUE;
    int _29209 = NOVALUE;
    int _29208 = NOVALUE;
    int _29205 = NOVALUE;
    int _29202 = NOVALUE;
    int _29200 = NOVALUE;
    int _29198 = NOVALUE;
    int _29196 = NOVALUE;
    int _29194 = NOVALUE;
    int _29193 = NOVALUE;
    int _29192 = NOVALUE;
    int _29191 = NOVALUE;
    int _29190 = NOVALUE;
    int _29189 = NOVALUE;
    int _29188 = NOVALUE;
    int _29182 = NOVALUE;
    int _29180 = NOVALUE;
    int _29177 = NOVALUE;
    int _29174 = NOVALUE;
    int _29169 = NOVALUE;
    int _29167 = NOVALUE;
    int _29166 = NOVALUE;
    int _29165 = NOVALUE;
    int _29163 = NOVALUE;
    int _29160 = NOVALUE;
    int _29157 = NOVALUE;
    int _29155 = NOVALUE;
    int _29153 = NOVALUE;
    int _29151 = NOVALUE;
    int _29149 = NOVALUE;
    int _29148 = NOVALUE;
    int _29147 = NOVALUE;
    int _29146 = NOVALUE;
    int _29145 = NOVALUE;
    int _29144 = NOVALUE;
    int _29143 = NOVALUE;
    int _29141 = NOVALUE;
    int _29139 = NOVALUE;
    int _29135 = NOVALUE;
    int _29134 = NOVALUE;
    int _29132 = NOVALUE;
    int _29131 = NOVALUE;
    int _29129 = NOVALUE;
    int _29128 = NOVALUE;
    int _29127 = NOVALUE;
    int _29126 = NOVALUE;
    int _29125 = NOVALUE;
    int _29123 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_subsym_56365)) {
        _1 = (long)(DBL_PTR(_subsym_56365)->dbl);
        if (UNIQUE(DBL_PTR(_subsym_56365)) && (DBL_PTR(_subsym_56365)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subsym_56365);
        _subsym_56365 = _1;
    }

    /** 	object var_code*/

    /** 	sequence name*/

    /** 	n = SymTab[subsym][S_NUM_ARGS]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29123 = (int)*(((s1_ptr)_2)->base + _subsym_56365);
    _2 = (int)SEQ_PTR(_29123);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _n_56366 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _n_56366 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    if (!IS_ATOM_INT(_n_56366)){
        _n_56366 = (long)DBL_PTR(_n_56366)->dbl;
    }
    _29123 = NOVALUE;

    /** 	if sequence(SymTab[subsym][S_DEF_ARGS]) then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29125 = (int)*(((s1_ptr)_2)->base + _subsym_56365);
    _2 = (int)SEQ_PTR(_29125);
    _29126 = (int)*(((s1_ptr)_2)->base + 28);
    _29125 = NOVALUE;
    _29127 = IS_SEQUENCE(_29126);
    _29126 = NOVALUE;
    if (_29127 == 0)
    {
        _29127 = NOVALUE;
        goto L1; // [40] 86
    }
    else{
        _29127 = NOVALUE;
    }

    /** 		fda = SymTab[subsym][S_DEF_ARGS][1]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29128 = (int)*(((s1_ptr)_2)->base + _subsym_56365);
    _2 = (int)SEQ_PTR(_29128);
    _29129 = (int)*(((s1_ptr)_2)->base + 28);
    _29128 = NOVALUE;
    _2 = (int)SEQ_PTR(_29129);
    _fda_56367 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_fda_56367)){
        _fda_56367 = (long)DBL_PTR(_fda_56367)->dbl;
    }
    _29129 = NOVALUE;

    /** 		lnda = SymTab[subsym][S_DEF_ARGS][2]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29131 = (int)*(((s1_ptr)_2)->base + _subsym_56365);
    _2 = (int)SEQ_PTR(_29131);
    _29132 = (int)*(((s1_ptr)_2)->base + 28);
    _29131 = NOVALUE;
    _2 = (int)SEQ_PTR(_29132);
    _lnda_56368 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_lnda_56368)){
        _lnda_56368 = (long)DBL_PTR(_lnda_56368)->dbl;
    }
    _29132 = NOVALUE;
    goto L2; // [83] 97
L1: 

    /** 		fda = 0*/
    _fda_56367 = 0;

    /** 		lnda = 0*/
    _lnda_56368 = 0;
L2: 

    /** 	s = subsym*/
    _s_56372 = _subsym_56365;

    /** 	parseargs_states = append(parseargs_states,*/
    if (IS_SEQUENCE(_41private_list_55853)){
            _29134 = SEQ_PTR(_41private_list_55853)->length;
    }
    else {
        _29134 = 1;
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _29134;
    *((int *)(_2+8)) = _41lock_scanner_55854;
    *((int *)(_2+12)) = _38use_private_list_17079;
    *((int *)(_2+16)) = _41on_arg_55855;
    _29135 = MAKE_SEQ(_1);
    _29134 = NOVALUE;
    RefDS(_29135);
    Append(&_41parseargs_states_55845, _41parseargs_states_55845, _29135);
    DeRefDS(_29135);
    _29135 = NOVALUE;

    /** 	nested_calls &= subsym*/
    Append(&_41nested_calls_55856, _41nested_calls_55856, _subsym_56365);

    /** 	lock_scanner = 0*/
    _41lock_scanner_55854 = 0;

    /** 	on_arg = 0*/
    _41on_arg_55855 = 0;

    /** 	short_circuit -= 1*/
    _41short_circuit_55122 = _41short_circuit_55122 - 1;

    /** 	for i = 1 to n do*/
    _29139 = _n_56366;
    {
        int _i_56403;
        _i_56403 = 1;
L3: 
        if (_i_56403 > _29139){
            goto L4; // [161] 915
        }

        /** 	  	tok = next_token()*/
        _0 = _tok_56370;
        _tok_56370 = _41next_token();
        DeRef(_0);

        /** 		if tok[T_ID] = COMMA then*/
        _2 = (int)SEQ_PTR(_tok_56370);
        _29141 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(NOTEQ, _29141, -30)){
            _29141 = NOVALUE;
            goto L5; // [183] 392
        }
        _29141 = NOVALUE;

        /** 			if SymTab[subsym][S_OPCODE] then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _29143 = (int)*(((s1_ptr)_2)->base + _subsym_56365);
        _2 = (int)SEQ_PTR(_29143);
        _29144 = (int)*(((s1_ptr)_2)->base + 21);
        _29143 = NOVALUE;
        if (_29144 == 0) {
            _29144 = NOVALUE;
            goto L6; // [201] 261
        }
        else {
            if (!IS_ATOM_INT(_29144) && DBL_PTR(_29144)->dbl == 0.0){
                _29144 = NOVALUE;
                goto L6; // [201] 261
            }
            _29144 = NOVALUE;
        }
        _29144 = NOVALUE;

        /** 				if atom(SymTab[subsym][S_CODE]) then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _29145 = (int)*(((s1_ptr)_2)->base + _subsym_56365);
        _2 = (int)SEQ_PTR(_29145);
        if (!IS_ATOM_INT(_38S_CODE_16610)){
            _29146 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
        }
        else{
            _29146 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
        }
        _29145 = NOVALUE;
        _29147 = IS_ATOM(_29146);
        _29146 = NOVALUE;
        if (_29147 == 0)
        {
            _29147 = NOVALUE;
            goto L7; // [221] 232
        }
        else{
            _29147 = NOVALUE;
        }

        /** 					var_code = 0*/
        DeRef(_var_code_56373);
        _var_code_56373 = 0;
        goto L8; // [229] 251
L7: 

        /** 					var_code = SymTab[subsym][S_CODE][i]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _29148 = (int)*(((s1_ptr)_2)->base + _subsym_56365);
        _2 = (int)SEQ_PTR(_29148);
        if (!IS_ATOM_INT(_38S_CODE_16610)){
            _29149 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
        }
        else{
            _29149 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
        }
        _29148 = NOVALUE;
        DeRef(_var_code_56373);
        _2 = (int)SEQ_PTR(_29149);
        _var_code_56373 = (int)*(((s1_ptr)_2)->base + _i_56403);
        Ref(_var_code_56373);
        _29149 = NOVALUE;
L8: 

        /** 				name = ""*/
        RefDS(_22663);
        DeRef(_name_56374);
        _name_56374 = _22663;
        goto L9; // [258] 308
L6: 

        /** 				s = SymTab[s][S_NEXT]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _29151 = (int)*(((s1_ptr)_2)->base + _s_56372);
        _2 = (int)SEQ_PTR(_29151);
        _s_56372 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_s_56372)){
            _s_56372 = (long)DBL_PTR(_s_56372)->dbl;
        }
        _29151 = NOVALUE;

        /** 				var_code = SymTab[s][S_CODE]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _29153 = (int)*(((s1_ptr)_2)->base + _s_56372);
        DeRef(_var_code_56373);
        _2 = (int)SEQ_PTR(_29153);
        if (!IS_ATOM_INT(_38S_CODE_16610)){
            _var_code_56373 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
        }
        else{
            _var_code_56373 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
        }
        Ref(_var_code_56373);
        _29153 = NOVALUE;

        /** 				name = SymTab[s][S_NAME]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _29155 = (int)*(((s1_ptr)_2)->base + _s_56372);
        DeRef(_name_56374);
        _2 = (int)SEQ_PTR(_29155);
        if (!IS_ATOM_INT(_38S_NAME_16598)){
            _name_56374 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
        }
        else{
            _name_56374 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
        }
        Ref(_name_56374);
        _29155 = NOVALUE;
L9: 

        /** 			if atom(var_code) then  -- but no default set*/
        _29157 = IS_ATOM(_var_code_56373);
        if (_29157 == 0)
        {
            _29157 = NOVALUE;
            goto LA; // [315] 326
        }
        else{
            _29157 = NOVALUE;
        }

        /** 				CompileErr(29,i)*/
        _46CompileErr(29, _i_56403, 0);
LA: 

        /** 			use_private_list = 1*/
        _38use_private_list_17079 = 1;

        /** 			start_playback(var_code)*/
        Ref(_var_code_56373);
        _41start_playback(_var_code_56373);

        /** 			lock_scanner=1*/
        _41lock_scanner_55854 = 1;

        /** 			Expr()*/
        _41Expr();

        /** 			lock_scanner=0*/
        _41lock_scanner_55854 = 0;

        /** 			on_arg += 1*/
        _41on_arg_55855 = _41on_arg_55855 + 1;

        /** 			private_list = append(private_list,name)*/
        RefDS(_name_56374);
        Append(&_41private_list_55853, _41private_list_55853, _name_56374);

        /** 			private_sym &= Top()*/
        _29160 = _43Top();
        if (IS_SEQUENCE(_38private_sym_17078) && IS_ATOM(_29160)) {
            Ref(_29160);
            Append(&_38private_sym_17078, _38private_sym_17078, _29160);
        }
        else if (IS_ATOM(_38private_sym_17078) && IS_SEQUENCE(_29160)) {
        }
        else {
            Concat((object_ptr)&_38private_sym_17078, _38private_sym_17078, _29160);
        }
        DeRef(_29160);
        _29160 = NOVALUE;

        /** 			backed_up_tok = {tok} -- ????*/
        _0 = _41backed_up_tok_55129;
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_tok_56370);
        *((int *)(_2+4)) = _tok_56370;
        _41backed_up_tok_55129 = MAKE_SEQ(_1);
        DeRef(_0);
        goto LB; // [389] 520
L5: 

        /** 		elsif tok[T_ID] != RIGHT_ROUND then*/
        _2 = (int)SEQ_PTR(_tok_56370);
        _29163 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(EQUALS, _29163, -27)){
            _29163 = NOVALUE;
            goto LC; // [402] 519
        }
        _29163 = NOVALUE;

        /** 			if SymTab[subsym][S_OPCODE] then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _29165 = (int)*(((s1_ptr)_2)->base + _subsym_56365);
        _2 = (int)SEQ_PTR(_29165);
        _29166 = (int)*(((s1_ptr)_2)->base + 21);
        _29165 = NOVALUE;
        if (_29166 == 0) {
            _29166 = NOVALUE;
            goto LD; // [420] 433
        }
        else {
            if (!IS_ATOM_INT(_29166) && DBL_PTR(_29166)->dbl == 0.0){
                _29166 = NOVALUE;
                goto LD; // [420] 433
            }
            _29166 = NOVALUE;
        }
        _29166 = NOVALUE;

        /** 				name = ""*/
        RefDS(_22663);
        DeRef(_name_56374);
        _name_56374 = _22663;
        goto LE; // [430] 466
LD: 

        /** 				s = SymTab[s][S_NEXT]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _29167 = (int)*(((s1_ptr)_2)->base + _s_56372);
        _2 = (int)SEQ_PTR(_29167);
        _s_56372 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_s_56372)){
            _s_56372 = (long)DBL_PTR(_s_56372)->dbl;
        }
        _29167 = NOVALUE;

        /** 				name = SymTab[s][S_NAME]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _29169 = (int)*(((s1_ptr)_2)->base + _s_56372);
        DeRef(_name_56374);
        _2 = (int)SEQ_PTR(_29169);
        if (!IS_ATOM_INT(_38S_NAME_16598)){
            _name_56374 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
        }
        else{
            _name_56374 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
        }
        Ref(_name_56374);
        _29169 = NOVALUE;
LE: 

        /** 			use_private_list = Parser_mode != PAM_NORMAL*/
        _38use_private_list_17079 = (_38Parser_mode_17071 != 0);

        /** 			putback(tok)*/
        Ref(_tok_56370);
        _41putback(_tok_56370);

        /** 			Expr()*/
        _41Expr();

        /** 			on_arg += 1*/
        _41on_arg_55855 = _41on_arg_55855 + 1;

        /** 			private_list = append(private_list,name)*/
        RefDS(_name_56374);
        Append(&_41private_list_55853, _41private_list_55853, _name_56374);

        /** 			private_sym &= Top()*/
        _29174 = _43Top();
        if (IS_SEQUENCE(_38private_sym_17078) && IS_ATOM(_29174)) {
            Ref(_29174);
            Append(&_38private_sym_17078, _38private_sym_17078, _29174);
        }
        else if (IS_ATOM(_38private_sym_17078) && IS_SEQUENCE(_29174)) {
        }
        else {
            Concat((object_ptr)&_38private_sym_17078, _38private_sym_17078, _29174);
        }
        DeRef(_29174);
        _29174 = NOVALUE;
LC: 
LB: 

        /** 		if on_arg != n then*/
        if (_41on_arg_55855 == _n_56366)
        goto LF; // [524] 908

        /** 			if tok[T_ID] = RIGHT_ROUND then*/
        _2 = (int)SEQ_PTR(_tok_56370);
        _29177 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(NOTEQ, _29177, -27)){
            _29177 = NOVALUE;
            goto L10; // [538] 548
        }
        _29177 = NOVALUE;

        /** 				putback( tok )*/
        Ref(_tok_56370);
        _41putback(_tok_56370);
L10: 

        /** 			tok = next_token()*/
        _0 = _tok_56370;
        _tok_56370 = _41next_token();
        DeRef(_0);

        /** 			if tok[T_ID] != COMMA then*/
        _2 = (int)SEQ_PTR(_tok_56370);
        _29180 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(EQUALS, _29180, -30)){
            _29180 = NOVALUE;
            goto L11; // [563] 907
        }
        _29180 = NOVALUE;

        /** 		  		if tok[T_ID] = RIGHT_ROUND then*/
        _2 = (int)SEQ_PTR(_tok_56370);
        _29182 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(NOTEQ, _29182, -27)){
            _29182 = NOVALUE;
            goto L12; // [577] 892
        }
        _29182 = NOVALUE;

        /** 					if fda=0 then*/
        if (_fda_56367 != 0)
        goto L13; // [585] 598

        /** 						WrongNumberArgs(subsym, "")*/
        RefDS(_22663);
        _41WrongNumberArgs(_subsym_56365, _22663);
        goto L14; // [595] 613
L13: 

        /** 					elsif i<lnda then*/
        if (_i_56403 >= _lnda_56368)
        goto L15; // [602] 612

        /** 						MissingArgs(subsym)*/
        _41MissingArgs(_subsym_56365);
L15: 
L14: 

        /** 					lock_scanner = 1*/
        _41lock_scanner_55854 = 1;

        /** 					use_private_list = 1*/
        _38use_private_list_17079 = 1;

        /** 					while on_arg < n do*/
L16: 
        if (_41on_arg_55855 >= _n_56366)
        goto L17; // [632] 841

        /** 						on_arg += 1*/
        _41on_arg_55855 = _41on_arg_55855 + 1;

        /** 						if SymTab[subsym][S_OPCODE] then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _29188 = (int)*(((s1_ptr)_2)->base + _subsym_56365);
        _2 = (int)SEQ_PTR(_29188);
        _29189 = (int)*(((s1_ptr)_2)->base + 21);
        _29188 = NOVALUE;
        if (_29189 == 0) {
            _29189 = NOVALUE;
            goto L18; // [658] 720
        }
        else {
            if (!IS_ATOM_INT(_29189) && DBL_PTR(_29189)->dbl == 0.0){
                _29189 = NOVALUE;
                goto L18; // [658] 720
            }
            _29189 = NOVALUE;
        }
        _29189 = NOVALUE;

        /** 							if atom(SymTab[subsym][S_CODE]) then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _29190 = (int)*(((s1_ptr)_2)->base + _subsym_56365);
        _2 = (int)SEQ_PTR(_29190);
        if (!IS_ATOM_INT(_38S_CODE_16610)){
            _29191 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
        }
        else{
            _29191 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
        }
        _29190 = NOVALUE;
        _29192 = IS_ATOM(_29191);
        _29191 = NOVALUE;
        if (_29192 == 0)
        {
            _29192 = NOVALUE;
            goto L19; // [678] 689
        }
        else{
            _29192 = NOVALUE;
        }

        /** 								var_code = 0*/
        DeRef(_var_code_56373);
        _var_code_56373 = 0;
        goto L1A; // [686] 710
L19: 

        /** 								var_code = SymTab[subsym][S_CODE][on_arg]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _29193 = (int)*(((s1_ptr)_2)->base + _subsym_56365);
        _2 = (int)SEQ_PTR(_29193);
        if (!IS_ATOM_INT(_38S_CODE_16610)){
            _29194 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
        }
        else{
            _29194 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
        }
        _29193 = NOVALUE;
        DeRef(_var_code_56373);
        _2 = (int)SEQ_PTR(_29194);
        _var_code_56373 = (int)*(((s1_ptr)_2)->base + _41on_arg_55855);
        Ref(_var_code_56373);
        _29194 = NOVALUE;
L1A: 

        /** 							name = ""*/
        RefDS(_22663);
        DeRef(_name_56374);
        _name_56374 = _22663;
        goto L1B; // [717] 767
L18: 

        /** 							s = SymTab[s][S_NEXT]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _29196 = (int)*(((s1_ptr)_2)->base + _s_56372);
        _2 = (int)SEQ_PTR(_29196);
        _s_56372 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_s_56372)){
            _s_56372 = (long)DBL_PTR(_s_56372)->dbl;
        }
        _29196 = NOVALUE;

        /** 							var_code = SymTab[s][S_CODE]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _29198 = (int)*(((s1_ptr)_2)->base + _s_56372);
        DeRef(_var_code_56373);
        _2 = (int)SEQ_PTR(_29198);
        if (!IS_ATOM_INT(_38S_CODE_16610)){
            _var_code_56373 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
        }
        else{
            _var_code_56373 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
        }
        Ref(_var_code_56373);
        _29198 = NOVALUE;

        /** 							name = SymTab[s][S_NAME]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _29200 = (int)*(((s1_ptr)_2)->base + _s_56372);
        DeRef(_name_56374);
        _2 = (int)SEQ_PTR(_29200);
        if (!IS_ATOM_INT(_38S_NAME_16598)){
            _name_56374 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
        }
        else{
            _name_56374 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
        }
        Ref(_name_56374);
        _29200 = NOVALUE;
L1B: 

        /** 						if sequence(var_code) then*/
        _29202 = IS_SEQUENCE(_var_code_56373);
        if (_29202 == 0)
        {
            _29202 = NOVALUE;
            goto L1C; // [774] 826
        }
        else{
            _29202 = NOVALUE;
        }

        /** 							putback( tok )*/
        Ref(_tok_56370);
        _41putback(_tok_56370);

        /** 							start_playback(var_code)*/
        Ref(_var_code_56373);
        _41start_playback(_var_code_56373);

        /** 							Expr()*/
        _41Expr();

        /** 							if on_arg < n then*/
        if (_41on_arg_55855 >= _n_56366)
        goto L16; // [795] 630

        /** 								private_list = append(private_list,name)*/
        RefDS(_name_56374);
        Append(&_41private_list_55853, _41private_list_55853, _name_56374);

        /** 								private_sym &= Top()*/
        _29205 = _43Top();
        if (IS_SEQUENCE(_38private_sym_17078) && IS_ATOM(_29205)) {
            Ref(_29205);
            Append(&_38private_sym_17078, _38private_sym_17078, _29205);
        }
        else if (IS_ATOM(_38private_sym_17078) && IS_SEQUENCE(_29205)) {
        }
        else {
            Concat((object_ptr)&_38private_sym_17078, _38private_sym_17078, _29205);
        }
        DeRef(_29205);
        _29205 = NOVALUE;
        goto L16; // [823] 630
L1C: 

        /** 							CompileErr(29, on_arg)*/
        _46CompileErr(29, _41on_arg_55855, 0);

        /** 		  		    end while*/
        goto L16; // [838] 630
L17: 

        /** 					short_circuit += 1*/
        _41short_circuit_55122 = _41short_circuit_55122 + 1;

        /** 					if backed_up_tok[$][T_ID] = PLAYBACK_ENDS then*/
        if (IS_SEQUENCE(_41backed_up_tok_55129)){
                _29208 = SEQ_PTR(_41backed_up_tok_55129)->length;
        }
        else {
            _29208 = 1;
        }
        _2 = (int)SEQ_PTR(_41backed_up_tok_55129);
        _29209 = (int)*(((s1_ptr)_2)->base + _29208);
        _2 = (int)SEQ_PTR(_29209);
        _29210 = (int)*(((s1_ptr)_2)->base + 1);
        _29209 = NOVALUE;
        if (binary_op_a(NOTEQ, _29210, 505)){
            _29210 = NOVALUE;
            goto L1D; // [868] 880
        }
        _29210 = NOVALUE;

        /** 						backed_up_tok = {}*/
        RefDS(_22663);
        DeRefDS(_41backed_up_tok_55129);
        _41backed_up_tok_55129 = _22663;
L1D: 

        /** 					restore_parseargs_states()*/
        _41restore_parseargs_states();

        /** 					return*/
        DeRef(_tok_56370);
        DeRef(_var_code_56373);
        DeRef(_name_56374);
        return;
        goto L1E; // [889] 906
L12: 

        /** 					putback(tok)*/
        Ref(_tok_56370);
        _41putback(_tok_56370);

        /** 					tok_match(COMMA)*/
        _41tok_match(-30, 0);
L1E: 
L11: 
LF: 

        /** 	end for*/
        _i_56403 = _i_56403 + 1;
        goto L3; // [910] 168
L4: 
        ;
    }

    /** 	tok = next_token()*/
    _0 = _tok_56370;
    _tok_56370 = _41next_token();
    DeRef(_0);

    /** 	short_circuit += 1*/
    _41short_circuit_55122 = _41short_circuit_55122 + 1;

    /** 	if tok[T_ID] != RIGHT_ROUND then*/
    _2 = (int)SEQ_PTR(_tok_56370);
    _29214 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _29214, -27)){
        _29214 = NOVALUE;
        goto L1F; // [938] 980
    }
    _29214 = NOVALUE;

    /** 		if tok[T_ID] = COMMA then*/
    _2 = (int)SEQ_PTR(_tok_56370);
    _29216 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29216, -30)){
        _29216 = NOVALUE;
        goto L20; // [952] 965
    }
    _29216 = NOVALUE;

    /** 			WrongNumberArgs(subsym, "only ")*/
    RefDS(_29218);
    _41WrongNumberArgs(_subsym_56365, _29218);
    goto L21; // [962] 979
L20: 

    /** 			putback(tok)*/
    Ref(_tok_56370);
    _41putback(_tok_56370);

    /** 			tok_match(RIGHT_ROUND)*/
    _41tok_match(-27, 0);
L21: 
L1F: 

    /** 	restore_parseargs_states()*/
    _41restore_parseargs_states();

    /** end procedure*/
    DeRef(_tok_56370);
    DeRef(_var_code_56373);
    DeRef(_name_56374);
    return;
    ;
}


void _41Forward_var(int _tok_56579, int _init_check_56580, int _op_56581)
{
    int _ref_56585 = NOVALUE;
    int _29222 = NOVALUE;
    int _29220 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_op_56581)) {
        _1 = (long)(DBL_PTR(_op_56581)->dbl);
        if (UNIQUE(DBL_PTR(_op_56581)) && (DBL_PTR(_op_56581)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_op_56581);
        _op_56581 = _1;
    }

    /** 	ref = new_forward_reference( VARIABLE, tok[T_SYM], op )*/
    _2 = (int)SEQ_PTR(_tok_56579);
    _29220 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_29220);
    _ref_56585 = _40new_forward_reference(-100, _29220, _op_56581);
    _29220 = NOVALUE;
    if (!IS_ATOM_INT(_ref_56585)) {
        _1 = (long)(DBL_PTR(_ref_56585)->dbl);
        if (UNIQUE(DBL_PTR(_ref_56585)) && (DBL_PTR(_ref_56585)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_56585);
        _ref_56585 = _1;
    }

    /** 	emit_opnd( - ref )*/
    if ((unsigned long)_ref_56585 == 0xC0000000)
    _29222 = (int)NewDouble((double)-0xC0000000);
    else
    _29222 = - _ref_56585;
    _43emit_opnd(_29222);
    _29222 = NOVALUE;

    /** 	if init_check != -1 then*/
    if (_init_check_56580 == -1)
    goto L1; // [33] 44

    /** 		Forward_InitCheck( tok, init_check )*/
    Ref(_tok_56579);
    _41Forward_InitCheck(_tok_56579, _init_check_56580);
L1: 

    /** end procedure*/
    DeRef(_tok_56579);
    return;
    ;
}


void _41Forward_call(int _tok_56598, int _opcode_56599)
{
    int _args_56602 = NOVALUE;
    int _proc_56604 = NOVALUE;
    int _tok_id_56607 = NOVALUE;
    int _id_56614 = NOVALUE;
    int _fc_pc_56637 = NOVALUE;
    int _29241 = NOVALUE;
    int _29240 = NOVALUE;
    int _29237 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer args = 0*/
    _args_56602 = 0;

    /** 	symtab_index proc = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_56598);
    _proc_56604 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_proc_56604)){
        _proc_56604 = (long)DBL_PTR(_proc_56604)->dbl;
    }

    /** 	integer tok_id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_56598);
    _tok_id_56607 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_tok_id_56607)){
        _tok_id_56607 = (long)DBL_PTR(_tok_id_56607)->dbl;
    }

    /** 	remove_symbol( proc )*/
    _55remove_symbol(_proc_56604);

    /** 	short_circuit -= 1*/
    _41short_circuit_55122 = _41short_circuit_55122 - 1;

    /** 	while 1 do*/
L1: 

    /** 		tok = next_token()*/
    _0 = _tok_56598;
    _tok_56598 = _41next_token();
    DeRef(_0);

    /** 		integer id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_56598);
    _id_56614 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_id_56614)){
        _id_56614 = (long)DBL_PTR(_id_56614)->dbl;
    }

    /** 		switch id do*/
    _0 = _id_56614;
    switch ( _0 ){ 

        /** 			case COMMA then*/
        case -30:

        /** 				emit_opnd( 0 ) -- clean this up later*/
        _43emit_opnd(0);

        /** 				args += 1*/
        _args_56602 = _args_56602 + 1;
        goto L2; // [83] 166

        /** 			case RIGHT_ROUND then*/
        case -27:

        /** 				exit*/
        goto L3; // [93] 173
        goto L2; // [95] 166

        /** 			case else*/
        default:

        /** 				putback( tok )*/
        Ref(_tok_56598);
        _41putback(_tok_56598);

        /** 				call_proc( forward_expr, {} )*/
        _0 = (int)_00[_41forward_expr_56142].addr;
        (*(int (*)())_0)(
                             );

        /** 				args += 1*/
        _args_56602 = _args_56602 + 1;

        /** 				tok = next_token()*/
        _0 = _tok_56598;
        _tok_56598 = _41next_token();
        DeRef(_0);

        /** 				id = tok[T_ID]*/
        _2 = (int)SEQ_PTR(_tok_56598);
        _id_56614 = (int)*(((s1_ptr)_2)->base + 1);
        if (!IS_ATOM_INT(_id_56614)){
            _id_56614 = (long)DBL_PTR(_id_56614)->dbl;
        }

        /** 				if id = RIGHT_ROUND then*/
        if (_id_56614 != -27)
        goto L4; // [138] 149

        /** 					exit*/
        goto L3; // [146] 173
L4: 

        /** 				if id != COMMA then*/
        if (_id_56614 == -30)
        goto L5; // [153] 165

        /** 						CompileErr(69)*/
        RefDS(_22663);
        _46CompileErr(69, _22663, 0);
L5: 
    ;}L2: 

    /** 	end while*/
    goto L1; // [170] 46
L3: 

    /** 	integer fc_pc = length( Code ) + 1*/
    if (IS_SEQUENCE(_38Code_17038)){
            _29237 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29237 = 1;
    }
    _fc_pc_56637 = _29237 + 1;
    _29237 = NOVALUE;

    /** 	emit_opnd( args )*/
    _43emit_opnd(_args_56602);

    /** 	op_info1 = proc*/
    _43op_info1_51218 = _proc_56604;

    /** 	if tok_id = QUALIFIED_VARIABLE then*/
    if (_tok_id_56607 != 512)
    goto L6; // [200] 224

    /** 		set_qualified_fwd( SymTab[proc][S_FILE_NO] )*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29240 = (int)*(((s1_ptr)_2)->base + _proc_56604);
    _2 = (int)SEQ_PTR(_29240);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _29241 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _29241 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _29240 = NOVALUE;
    Ref(_29241);
    _62set_qualified_fwd(_29241);
    _29241 = NOVALUE;
    goto L7; // [221] 230
L6: 

    /** 		set_qualified_fwd( -1 )*/
    _62set_qualified_fwd(-1);
L7: 

    /** 	emit_op( opcode )*/
    _43emit_op(_opcode_56599);

    /** 	if not TRANSLATE then*/
    if (_38TRANSLATE_16564 != 0)
    goto L8; // [239] 258

    /** 		if OpTrace then*/
    if (_38OpTrace_17015 == 0)
    {
        goto L9; // [246] 257
    }
    else{
    }

    /** 			emit_op(UPDATE_GLOBALS)*/
    _43emit_op(89);
L9: 
L8: 

    /** 	short_circuit += 1*/
    _41short_circuit_55122 = _41short_circuit_55122 + 1;

    /** end procedure*/
    DeRef(_tok_56598);
    return;
    ;
}


void _41Object_call(int _tok_56665)
{
    int _tok2_56667 = NOVALUE;
    int _tok3_56668 = NOVALUE;
    int _save_factors_56669 = NOVALUE;
    int _save_lhs_subs_level_56670 = NOVALUE;
    int _sym_56672 = NOVALUE;
    int _29301 = NOVALUE;
    int _29299 = NOVALUE;
    int _29298 = NOVALUE;
    int _29295 = NOVALUE;
    int _29294 = NOVALUE;
    int _29290 = NOVALUE;
    int _29284 = NOVALUE;
    int _29281 = NOVALUE;
    int _29280 = NOVALUE;
    int _29279 = NOVALUE;
    int _29277 = NOVALUE;
    int _29276 = NOVALUE;
    int _29274 = NOVALUE;
    int _29273 = NOVALUE;
    int _29270 = NOVALUE;
    int _29269 = NOVALUE;
    int _29268 = NOVALUE;
    int _29266 = NOVALUE;
    int _29265 = NOVALUE;
    int _29263 = NOVALUE;
    int _29262 = NOVALUE;
    int _29261 = NOVALUE;
    int _29260 = NOVALUE;
    int _29258 = NOVALUE;
    int _29257 = NOVALUE;
    int _29255 = NOVALUE;
    int _29254 = NOVALUE;
    int _29251 = NOVALUE;
    int _29249 = NOVALUE;
    int _29248 = NOVALUE;
    int _29246 = NOVALUE;
    int _29245 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	integer save_factors, save_lhs_subs_level*/

    /** 	tok2 = next_token()*/
    _0 = _tok2_56667;
    _tok2_56667 = _41next_token();
    DeRef(_0);

    /** 	if tok2[T_ID] = VARIABLE or tok2[T_ID] = QUALIFIED_VARIABLE then*/
    _2 = (int)SEQ_PTR(_tok2_56667);
    _29245 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29245)) {
        _29246 = (_29245 == -100);
    }
    else {
        _29246 = binary_op(EQUALS, _29245, -100);
    }
    _29245 = NOVALUE;
    if (IS_ATOM_INT(_29246)) {
        if (_29246 != 0) {
            goto L1; // [22] 43
        }
    }
    else {
        if (DBL_PTR(_29246)->dbl != 0.0) {
            goto L1; // [22] 43
        }
    }
    _2 = (int)SEQ_PTR(_tok2_56667);
    _29248 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29248)) {
        _29249 = (_29248 == 512);
    }
    else {
        _29249 = binary_op(EQUALS, _29248, 512);
    }
    _29248 = NOVALUE;
    if (_29249 == 0) {
        DeRef(_29249);
        _29249 = NOVALUE;
        goto L2; // [39] 586
    }
    else {
        if (!IS_ATOM_INT(_29249) && DBL_PTR(_29249)->dbl == 0.0){
            DeRef(_29249);
            _29249 = NOVALUE;
            goto L2; // [39] 586
        }
        DeRef(_29249);
        _29249 = NOVALUE;
    }
    DeRef(_29249);
    _29249 = NOVALUE;
L1: 

    /** 		tok3 = next_token()*/
    _0 = _tok3_56668;
    _tok3_56668 = _41next_token();
    DeRef(_0);

    /** 		if tok3[T_ID] = RIGHT_ROUND then*/
    _2 = (int)SEQ_PTR(_tok3_56668);
    _29251 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29251, -27)){
        _29251 = NOVALUE;
        goto L3; // [58] 155
    }
    _29251 = NOVALUE;

    /** 			sym = tok2[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok2_56667);
    _sym_56672 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_sym_56672)){
        _sym_56672 = (long)DBL_PTR(_sym_56672)->dbl;
    }

    /** 			if SymTab[sym][S_SCOPE] = SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29254 = (int)*(((s1_ptr)_2)->base + _sym_56672);
    _2 = (int)SEQ_PTR(_29254);
    _29255 = (int)*(((s1_ptr)_2)->base + 4);
    _29254 = NOVALUE;
    if (binary_op_a(NOTEQ, _29255, 9)){
        _29255 = NOVALUE;
        goto L4; // [88] 108
    }
    _29255 = NOVALUE;

    /** 				Forward_var( tok2 )*/
    _2 = (int)SEQ_PTR(_tok2_56667);
    _29257 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_tok2_56667);
    Ref(_29257);
    _41Forward_var(_tok2_56667, -1, _29257);
    _29257 = NOVALUE;
    goto L5; // [105] 147
L4: 

    /** 				SymTab[sym][S_USAGE] = or_bits(SymTab[sym][S_USAGE], U_READ)*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_56672 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29260 = (int)*(((s1_ptr)_2)->base + _sym_56672);
    _2 = (int)SEQ_PTR(_29260);
    _29261 = (int)*(((s1_ptr)_2)->base + 5);
    _29260 = NOVALUE;
    if (IS_ATOM_INT(_29261)) {
        {unsigned long tu;
             tu = (unsigned long)_29261 | (unsigned long)1;
             _29262 = MAKE_UINT(tu);
        }
    }
    else {
        _29262 = binary_op(OR_BITS, _29261, 1);
    }
    _29261 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _29262;
    if( _1 != _29262 ){
        DeRef(_1);
    }
    _29262 = NOVALUE;
    _29258 = NOVALUE;

    /** 				emit_opnd(sym)*/
    _43emit_opnd(_sym_56672);
L5: 

    /** 			putback( tok3 )*/
    Ref(_tok3_56668);
    _41putback(_tok3_56668);
    goto L6; // [152] 575
L3: 

    /** 		elsif tok3[T_ID] = COMMA then*/
    _2 = (int)SEQ_PTR(_tok3_56668);
    _29263 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29263, -30)){
        _29263 = NOVALUE;
        goto L7; // [165] 184
    }
    _29263 = NOVALUE;

    /** 			WrongNumberArgs(tok[T_SYM], "")*/
    _2 = (int)SEQ_PTR(_tok_56665);
    _29265 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_29265);
    RefDS(_22663);
    _41WrongNumberArgs(_29265, _22663);
    _29265 = NOVALUE;
    goto L6; // [181] 575
L7: 

    /** 		elsif tok3[T_ID] = LEFT_ROUND then*/
    _2 = (int)SEQ_PTR(_tok3_56668);
    _29266 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29266, -26)){
        _29266 = NOVALUE;
        goto L8; // [194] 244
    }
    _29266 = NOVALUE;

    /** 			if SymTab[tok2[T_SYM]][S_SCOPE] = SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_tok2_56667);
    _29268 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_29268)){
        _29269 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29268)->dbl));
    }
    else{
        _29269 = (int)*(((s1_ptr)_2)->base + _29268);
    }
    _2 = (int)SEQ_PTR(_29269);
    _29270 = (int)*(((s1_ptr)_2)->base + 4);
    _29269 = NOVALUE;
    if (binary_op_a(NOTEQ, _29270, 9)){
        _29270 = NOVALUE;
        goto L9; // [220] 235
    }
    _29270 = NOVALUE;

    /** 				Forward_call( tok2, FUNC_FORWARD )*/
    Ref(_tok2_56667);
    _41Forward_call(_tok2_56667, 196);
    goto L6; // [232] 575
L9: 

    /** 				Function_call( tok2 )*/
    Ref(_tok2_56667);
    _41Function_call(_tok2_56667);
    goto L6; // [241] 575
L8: 

    /** 			sym = tok2[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok2_56667);
    _sym_56672 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_sym_56672)){
        _sym_56672 = (long)DBL_PTR(_sym_56672)->dbl;
    }

    /** 			if SymTab[sym][S_SCOPE] = SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29273 = (int)*(((s1_ptr)_2)->base + _sym_56672);
    _2 = (int)SEQ_PTR(_29273);
    _29274 = (int)*(((s1_ptr)_2)->base + 4);
    _29273 = NOVALUE;
    if (binary_op_a(NOTEQ, _29274, 9)){
        _29274 = NOVALUE;
        goto LA; // [270] 292
    }
    _29274 = NOVALUE;

    /** 				Forward_var( tok2, TRUE )*/
    _2 = (int)SEQ_PTR(_tok2_56667);
    _29276 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_tok2_56667);
    Ref(_29276);
    _41Forward_var(_tok2_56667, _9TRUE_428, _29276);
    _29276 = NOVALUE;
    goto LB; // [289] 339
LA: 

    /** 				SymTab[sym][S_USAGE] = or_bits(SymTab[sym][S_USAGE], U_READ)*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_56672 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29279 = (int)*(((s1_ptr)_2)->base + _sym_56672);
    _2 = (int)SEQ_PTR(_29279);
    _29280 = (int)*(((s1_ptr)_2)->base + 5);
    _29279 = NOVALUE;
    if (IS_ATOM_INT(_29280)) {
        {unsigned long tu;
             tu = (unsigned long)_29280 | (unsigned long)1;
             _29281 = MAKE_UINT(tu);
        }
    }
    else {
        _29281 = binary_op(OR_BITS, _29280, 1);
    }
    _29280 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _29281;
    if( _1 != _29281 ){
        DeRef(_1);
    }
    _29281 = NOVALUE;
    _29277 = NOVALUE;

    /** 				InitCheck(sym, TRUE)*/
    _41InitCheck(_sym_56672, _9TRUE_428);

    /** 				emit_opnd(sym)*/
    _43emit_opnd(_sym_56672);
LB: 

    /** 			if sym = left_sym then*/
    if (_sym_56672 != _41left_sym_55163)
    goto LC; // [343] 353

    /** 				lhs_subs_level = 0*/
    _41lhs_subs_level_55161 = 0;
LC: 

    /** 			tok2 = tok3*/
    Ref(_tok3_56668);
    DeRef(_tok2_56667);
    _tok2_56667 = _tok3_56668;

    /** 			current_sequence = append(current_sequence, sym)*/
    Append(&_43current_sequence_51226, _43current_sequence_51226, _sym_56672);

    /** 			while tok2[T_ID] = LEFT_SQUARE do*/
LD: 
    _2 = (int)SEQ_PTR(_tok2_56667);
    _29284 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29284, -28)){
        _29284 = NOVALUE;
        goto LE; // [381] 551
    }
    _29284 = NOVALUE;

    /** 				subs_depth += 1*/
    _41subs_depth_55164 = _41subs_depth_55164 + 1;

    /** 				if lhs_subs_level >= 0 then*/
    if (_41lhs_subs_level_55161 < 0)
    goto LF; // [397] 410

    /** 					lhs_subs_level += 1*/
    _41lhs_subs_level_55161 = _41lhs_subs_level_55161 + 1;
LF: 

    /** 				save_factors = factors*/
    _save_factors_56669 = _41factors_55160;

    /** 				save_lhs_subs_level = lhs_subs_level*/
    _save_lhs_subs_level_56670 = _41lhs_subs_level_55161;

    /** 				call_proc(forward_expr, {})*/
    _0 = (int)_00[_41forward_expr_56142].addr;
    (*(int (*)())_0)(
                         );

    /** 				tok2 = next_token()*/
    _0 = _tok2_56667;
    _tok2_56667 = _41next_token();
    DeRef(_0);

    /** 				if tok2[T_ID] = SLICE then*/
    _2 = (int)SEQ_PTR(_tok2_56667);
    _29290 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29290, 513)){
        _29290 = NOVALUE;
        goto L10; // [446] 484
    }
    _29290 = NOVALUE;

    /** 					call_proc(forward_expr, {})*/
    _0 = (int)_00[_41forward_expr_56142].addr;
    (*(int (*)())_0)(
                         );

    /** 					emit_op(RHS_SLICE)*/
    _43emit_op(46);

    /** 					tok_match(RIGHT_SQUARE)*/
    _41tok_match(-29, 0);

    /** 					tok2 = next_token()*/
    _0 = _tok2_56667;
    _tok2_56667 = _41next_token();
    DeRef(_0);

    /** 					exit*/
    goto LE; // [479] 551
    goto L11; // [481] 531
L10: 

    /** 					putback(tok2)*/
    Ref(_tok2_56667);
    _41putback(_tok2_56667);

    /** 					tok_match(RIGHT_SQUARE)*/
    _41tok_match(-29, 0);

    /** 					subs_depth -= 1*/
    _41subs_depth_55164 = _41subs_depth_55164 - 1;

    /** 					current_sequence = current_sequence[1..$-1]*/
    if (IS_SEQUENCE(_43current_sequence_51226)){
            _29294 = SEQ_PTR(_43current_sequence_51226)->length;
    }
    else {
        _29294 = 1;
    }
    _29295 = _29294 - 1;
    _29294 = NOVALUE;
    rhs_slice_target = (object_ptr)&_43current_sequence_51226;
    RHS_Slice(_43current_sequence_51226, 1, _29295);

    /** 					emit_op(RHS_SUBS)*/
    _43emit_op(25);
L11: 

    /** 				factors = save_factors*/
    _41factors_55160 = _save_factors_56669;

    /** 				lhs_subs_level = save_lhs_subs_level*/
    _41lhs_subs_level_55161 = _save_lhs_subs_level_56670;

    /** 				tok2 = next_token()*/
    _0 = _tok2_56667;
    _tok2_56667 = _41next_token();
    DeRef(_0);

    /** 			end while*/
    goto LD; // [548] 373
LE: 

    /** 			current_sequence = current_sequence[1..$-1]*/
    if (IS_SEQUENCE(_43current_sequence_51226)){
            _29298 = SEQ_PTR(_43current_sequence_51226)->length;
    }
    else {
        _29298 = 1;
    }
    _29299 = _29298 - 1;
    _29298 = NOVALUE;
    rhs_slice_target = (object_ptr)&_43current_sequence_51226;
    RHS_Slice(_43current_sequence_51226, 1, _29299);

    /** 			putback(tok2)*/
    Ref(_tok2_56667);
    _41putback(_tok2_56667);
L6: 

    /** 		tok_match( RIGHT_ROUND )*/
    _41tok_match(-27, 0);
    goto L12; // [583] 603
L2: 

    /** 		putback(tok2)*/
    Ref(_tok2_56667);
    _41putback(_tok2_56667);

    /** 		ParseArgs(tok[T_SYM])*/
    _2 = (int)SEQ_PTR(_tok_56665);
    _29301 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_29301);
    _41ParseArgs(_29301);
    _29301 = NOVALUE;
L12: 

    /** end procedure*/
    DeRef(_tok_56665);
    DeRef(_tok2_56667);
    DeRef(_tok3_56668);
    _29268 = NOVALUE;
    DeRef(_29246);
    _29246 = NOVALUE;
    DeRef(_29295);
    _29295 = NOVALUE;
    DeRef(_29299);
    _29299 = NOVALUE;
    return;
    ;
}


void _41Function_call(int _tok_56810)
{
    int _id_56811 = NOVALUE;
    int _scope_56812 = NOVALUE;
    int _opcode_56813 = NOVALUE;
    int _e_56814 = NOVALUE;
    int _29342 = NOVALUE;
    int _29341 = NOVALUE;
    int _29340 = NOVALUE;
    int _29339 = NOVALUE;
    int _29338 = NOVALUE;
    int _29337 = NOVALUE;
    int _29336 = NOVALUE;
    int _29334 = NOVALUE;
    int _29333 = NOVALUE;
    int _29331 = NOVALUE;
    int _29330 = NOVALUE;
    int _29329 = NOVALUE;
    int _29328 = NOVALUE;
    int _29327 = NOVALUE;
    int _29326 = NOVALUE;
    int _29325 = NOVALUE;
    int _29324 = NOVALUE;
    int _29323 = NOVALUE;
    int _29322 = NOVALUE;
    int _29321 = NOVALUE;
    int _29320 = NOVALUE;
    int _29319 = NOVALUE;
    int _29318 = NOVALUE;
    int _29317 = NOVALUE;
    int _29315 = NOVALUE;
    int _29313 = NOVALUE;
    int _29312 = NOVALUE;
    int _29310 = NOVALUE;
    int _29308 = NOVALUE;
    int _29307 = NOVALUE;
    int _29306 = NOVALUE;
    int _29305 = NOVALUE;
    int _29303 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_56810);
    _id_56811 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_id_56811)){
        _id_56811 = (long)DBL_PTR(_id_56811)->dbl;
    }

    /** 	if id = FUNC or id = TYPE then*/
    _29303 = (_id_56811 == 501);
    if (_29303 != 0) {
        goto L1; // [19] 34
    }
    _29305 = (_id_56811 == 504);
    if (_29305 == 0)
    {
        DeRef(_29305);
        _29305 = NOVALUE;
        goto L2; // [30] 46
    }
    else{
        DeRef(_29305);
        _29305 = NOVALUE;
    }
L1: 

    /** 		UndefinedVar( tok[T_SYM] )*/
    _2 = (int)SEQ_PTR(_tok_56810);
    _29306 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_29306);
    _41UndefinedVar(_29306);
    _29306 = NOVALUE;
L2: 

    /** 	e = SymTab[tok[T_SYM]][S_EFFECT]*/
    _2 = (int)SEQ_PTR(_tok_56810);
    _29307 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_29307)){
        _29308 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29307)->dbl));
    }
    else{
        _29308 = (int)*(((s1_ptr)_2)->base + _29307);
    }
    _2 = (int)SEQ_PTR(_29308);
    _e_56814 = (int)*(((s1_ptr)_2)->base + 23);
    if (!IS_ATOM_INT(_e_56814)){
        _e_56814 = (long)DBL_PTR(_e_56814)->dbl;
    }
    _29308 = NOVALUE;

    /** 	if e then*/
    if (_e_56814 == 0)
    {
        goto L3; // [70] 229
    }
    else{
    }

    /** 		if e = E_ALL_EFFECT or tok[T_SYM] > left_sym then*/
    _29310 = (_e_56814 == 1073741823);
    if (_29310 != 0) {
        goto L4; // [81] 102
    }
    _2 = (int)SEQ_PTR(_tok_56810);
    _29312 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_29312)) {
        _29313 = (_29312 > _41left_sym_55163);
    }
    else {
        _29313 = binary_op(GREATER, _29312, _41left_sym_55163);
    }
    _29312 = NOVALUE;
    if (_29313 == 0) {
        DeRef(_29313);
        _29313 = NOVALUE;
        goto L5; // [98] 111
    }
    else {
        if (!IS_ATOM_INT(_29313) && DBL_PTR(_29313)->dbl == 0.0){
            DeRef(_29313);
            _29313 = NOVALUE;
            goto L5; // [98] 111
        }
        DeRef(_29313);
        _29313 = NOVALUE;
    }
    DeRef(_29313);
    _29313 = NOVALUE;
L4: 

    /** 			side_effect_calls = or_bits(side_effect_calls, e)*/
    {unsigned long tu;
         tu = (unsigned long)_41side_effect_calls_55159 | (unsigned long)_e_56814;
         _41side_effect_calls_55159 = MAKE_UINT(tu);
    }
L5: 

    /** 		SymTab[CurrentSub][S_EFFECT] = or_bits(SymTab[CurrentSub][S_EFFECT], e)*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38CurrentSub_16954 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29317 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_29317);
    _29318 = (int)*(((s1_ptr)_2)->base + 23);
    _29317 = NOVALUE;
    if (IS_ATOM_INT(_29318)) {
        {unsigned long tu;
             tu = (unsigned long)_29318 | (unsigned long)_e_56814;
             _29319 = MAKE_UINT(tu);
        }
    }
    else {
        _29319 = binary_op(OR_BITS, _29318, _e_56814);
    }
    _29318 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 23);
    _1 = *(int *)_2;
    *(int *)_2 = _29319;
    if( _1 != _29319 ){
        DeRef(_1);
    }
    _29319 = NOVALUE;
    _29315 = NOVALUE;

    /** 		if short_circuit > 0 and short_circuit_B and*/
    _29320 = (_41short_circuit_55122 > 0);
    if (_29320 == 0) {
        _29321 = 0;
        goto L6; // [154] 164
    }
    _29321 = (_41short_circuit_B_55124 != 0);
L6: 
    if (_29321 == 0) {
        goto L7; // [164] 228
    }
    _29323 = find_from(_id_56811, _39FUNC_TOKS_16559, 1);
    if (_29323 == 0)
    {
        _29323 = NOVALUE;
        goto L7; // [176] 228
    }
    else{
        _29323 = NOVALUE;
    }

    /** 			Warning(219, short_circuit_warning_flag,*/
    _2 = (int)SEQ_PTR(_35known_files_15596);
    _29324 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    Ref(_29324);
    RefDS(_22663);
    _29325 = _13abbreviate_path(_29324, _22663);
    _29324 = NOVALUE;
    _2 = (int)SEQ_PTR(_tok_56810);
    _29326 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_29326)){
        _29327 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29326)->dbl));
    }
    else{
        _29327 = (int)*(((s1_ptr)_2)->base + _29326);
    }
    _2 = (int)SEQ_PTR(_29327);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _29328 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _29328 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _29327 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _29325;
    *((int *)(_2+8)) = _38line_number_16947;
    Ref(_29328);
    *((int *)(_2+12)) = _29328;
    _29329 = MAKE_SEQ(_1);
    _29328 = NOVALUE;
    _29325 = NOVALUE;
    _46Warning(219, 2, _29329);
    _29329 = NOVALUE;
L7: 
L3: 

    /** 	tok_match(LEFT_ROUND)*/
    _41tok_match(-26, 0);

    /** 	scope = SymTab[tok[T_SYM]][S_SCOPE]*/
    _2 = (int)SEQ_PTR(_tok_56810);
    _29330 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_29330)){
        _29331 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29330)->dbl));
    }
    else{
        _29331 = (int)*(((s1_ptr)_2)->base + _29330);
    }
    _2 = (int)SEQ_PTR(_29331);
    _scope_56812 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_scope_56812)){
        _scope_56812 = (long)DBL_PTR(_scope_56812)->dbl;
    }
    _29331 = NOVALUE;

    /** 	opcode = SymTab[tok[T_SYM]][S_OPCODE]*/
    _2 = (int)SEQ_PTR(_tok_56810);
    _29333 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_29333)){
        _29334 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29333)->dbl));
    }
    else{
        _29334 = (int)*(((s1_ptr)_2)->base + _29333);
    }
    _2 = (int)SEQ_PTR(_29334);
    _opcode_56813 = (int)*(((s1_ptr)_2)->base + 21);
    if (!IS_ATOM_INT(_opcode_56813)){
        _opcode_56813 = (long)DBL_PTR(_opcode_56813)->dbl;
    }
    _29334 = NOVALUE;

    /** 	if equal(SymTab[tok[T_SYM]][S_NAME],"object") and scope = SC_PREDEF then*/
    _2 = (int)SEQ_PTR(_tok_56810);
    _29336 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_29336)){
        _29337 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29336)->dbl));
    }
    else{
        _29337 = (int)*(((s1_ptr)_2)->base + _29336);
    }
    _2 = (int)SEQ_PTR(_29337);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _29338 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _29338 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _29337 = NOVALUE;
    if (_29338 == _25325)
    _29339 = 1;
    else if (IS_ATOM_INT(_29338) && IS_ATOM_INT(_25325))
    _29339 = 0;
    else
    _29339 = (compare(_29338, _25325) == 0);
    _29338 = NOVALUE;
    if (_29339 == 0) {
        goto L8; // [305] 327
    }
    _29341 = (_scope_56812 == 7);
    if (_29341 == 0)
    {
        DeRef(_29341);
        _29341 = NOVALUE;
        goto L8; // [316] 327
    }
    else{
        DeRef(_29341);
        _29341 = NOVALUE;
    }

    /** 		Object_call( tok )*/
    Ref(_tok_56810);
    _41Object_call(_tok_56810);
    goto L9; // [324] 339
L8: 

    /** 		ParseArgs(tok[T_SYM])*/
    _2 = (int)SEQ_PTR(_tok_56810);
    _29342 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_29342);
    _41ParseArgs(_29342);
    _29342 = NOVALUE;
L9: 

    /** 	if scope = SC_PREDEF then*/
    if (_scope_56812 != 7)
    goto LA; // [343] 355

    /** 		emit_op(opcode)*/
    _43emit_op(_opcode_56813);
    goto LB; // [352] 393
LA: 

    /** 		op_info1 = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_56810);
    _43op_info1_51218 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_43op_info1_51218)){
        _43op_info1_51218 = (long)DBL_PTR(_43op_info1_51218)->dbl;
    }

    /** 		emit_or_inline()*/
    _69emit_or_inline();

    /** 		if not TRANSLATE then*/
    if (_38TRANSLATE_16564 != 0)
    goto LC; // [373] 392

    /** 			if OpTrace then*/
    if (_38OpTrace_17015 == 0)
    {
        goto LD; // [380] 391
    }
    else{
    }

    /** 				emit_op(UPDATE_GLOBALS)*/
    _43emit_op(89);
LD: 
LC: 
LB: 

    /** end procedure*/
    DeRef(_tok_56810);
    DeRef(_29303);
    _29303 = NOVALUE;
    _29307 = NOVALUE;
    DeRef(_29310);
    _29310 = NOVALUE;
    DeRef(_29320);
    _29320 = NOVALUE;
    _29326 = NOVALUE;
    _29330 = NOVALUE;
    _29333 = NOVALUE;
    _29336 = NOVALUE;
    return;
    ;
}


void _41Factor()
{
    int _tok_56918 = NOVALUE;
    int _id_56919 = NOVALUE;
    int _n_56920 = NOVALUE;
    int _save_factors_56921 = NOVALUE;
    int _save_lhs_subs_level_56922 = NOVALUE;
    int _sym_56924 = NOVALUE;
    int _forward_56955 = NOVALUE;
    int _29404 = NOVALUE;
    int _29403 = NOVALUE;
    int _29402 = NOVALUE;
    int _29400 = NOVALUE;
    int _29399 = NOVALUE;
    int _29398 = NOVALUE;
    int _29397 = NOVALUE;
    int _29396 = NOVALUE;
    int _29394 = NOVALUE;
    int _29390 = NOVALUE;
    int _29389 = NOVALUE;
    int _29386 = NOVALUE;
    int _29385 = NOVALUE;
    int _29381 = NOVALUE;
    int _29375 = NOVALUE;
    int _29370 = NOVALUE;
    int _29369 = NOVALUE;
    int _29368 = NOVALUE;
    int _29366 = NOVALUE;
    int _29365 = NOVALUE;
    int _29363 = NOVALUE;
    int _29361 = NOVALUE;
    int _29360 = NOVALUE;
    int _29359 = NOVALUE;
    int _29357 = NOVALUE;
    int _29350 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	integer id, n*/

    /** 	integer save_factors, save_lhs_subs_level*/

    /** 	factors += 1*/
    _41factors_55160 = _41factors_55160 + 1;

    /** 	tok = next_token()*/
    _0 = _tok_56918;
    _tok_56918 = _41next_token();
    DeRef(_0);

    /** 	id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_56918);
    _id_56919 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_id_56919)){
        _id_56919 = (long)DBL_PTR(_id_56919)->dbl;
    }

    /** 	if id = RECORDED then*/
    if (_id_56919 != 508)
    goto L1; // [32] 59

    /** 		tok = read_recorded_token(tok[T_SYM])*/
    _2 = (int)SEQ_PTR(_tok_56918);
    _29350 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_29350);
    _0 = _tok_56918;
    _tok_56918 = _41read_recorded_token(_29350);
    DeRef(_0);
    _29350 = NOVALUE;

    /** 		id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_56918);
    _id_56919 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_id_56919)){
        _id_56919 = (long)DBL_PTR(_id_56919)->dbl;
    }
L1: 

    /** 	switch id label "factor" do*/
    _0 = _id_56919;
    switch ( _0 ){ 

        /** 		case VARIABLE, QUALIFIED_VARIABLE then*/
        case -100:
        case 512:

        /** 			sym = tok[T_SYM]*/
        _2 = (int)SEQ_PTR(_tok_56918);
        _sym_56924 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_sym_56924)){
            _sym_56924 = (long)DBL_PTR(_sym_56924)->dbl;
        }

        /** 			if sym < 0 or SymTab[sym][S_SCOPE] = SC_UNDEFINED then*/
        _29357 = (_sym_56924 < 0);
        if (_29357 != 0) {
            goto L2; // [88] 115
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _29359 = (int)*(((s1_ptr)_2)->base + _sym_56924);
        _2 = (int)SEQ_PTR(_29359);
        _29360 = (int)*(((s1_ptr)_2)->base + 4);
        _29359 = NOVALUE;
        if (IS_ATOM_INT(_29360)) {
            _29361 = (_29360 == 9);
        }
        else {
            _29361 = binary_op(EQUALS, _29360, 9);
        }
        _29360 = NOVALUE;
        if (_29361 == 0) {
            DeRef(_29361);
            _29361 = NOVALUE;
            goto L3; // [111] 177
        }
        else {
            if (!IS_ATOM_INT(_29361) && DBL_PTR(_29361)->dbl == 0.0){
                DeRef(_29361);
                _29361 = NOVALUE;
                goto L3; // [111] 177
            }
            DeRef(_29361);
            _29361 = NOVALUE;
        }
        DeRef(_29361);
        _29361 = NOVALUE;
L2: 

        /** 				token forward = next_token()*/
        _0 = _forward_56955;
        _forward_56955 = _41next_token();
        DeRef(_0);

        /** 				if forward[T_ID] = LEFT_ROUND then*/
        _2 = (int)SEQ_PTR(_forward_56955);
        _29363 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(NOTEQ, _29363, -26)){
            _29363 = NOVALUE;
            goto L4; // [130] 151
        }
        _29363 = NOVALUE;

        /** 					Forward_call( tok, FUNC_FORWARD )*/
        Ref(_tok_56918);
        _41Forward_call(_tok_56918, 196);

        /** 					break "factor"*/
        DeRef(_forward_56955);
        _forward_56955 = NOVALUE;
        goto L5; // [146] 696
        goto L6; // [148] 172
L4: 

        /** 					putback( forward )*/
        Ref(_forward_56955);
        _41putback(_forward_56955);

        /** 					Forward_var( tok, TRUE )*/
        _2 = (int)SEQ_PTR(_tok_56918);
        _29365 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_tok_56918);
        Ref(_29365);
        _41Forward_var(_tok_56918, _9TRUE_428, _29365);
        _29365 = NOVALUE;
L6: 
        DeRef(_forward_56955);
        _forward_56955 = NOVALUE;
        goto L7; // [174] 229
L3: 

        /** 				UndefinedVar(sym)*/
        _41UndefinedVar(_sym_56924);

        /** 				SymTab[sym][S_USAGE] = or_bits(SymTab[sym][S_USAGE], U_READ)*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_sym_56924 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _29368 = (int)*(((s1_ptr)_2)->base + _sym_56924);
        _2 = (int)SEQ_PTR(_29368);
        _29369 = (int)*(((s1_ptr)_2)->base + 5);
        _29368 = NOVALUE;
        if (IS_ATOM_INT(_29369)) {
            {unsigned long tu;
                 tu = (unsigned long)_29369 | (unsigned long)1;
                 _29370 = MAKE_UINT(tu);
            }
        }
        else {
            _29370 = binary_op(OR_BITS, _29369, 1);
        }
        _29369 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = _29370;
        if( _1 != _29370 ){
            DeRef(_1);
        }
        _29370 = NOVALUE;
        _29366 = NOVALUE;

        /** 				InitCheck(sym, TRUE)*/
        _41InitCheck(_sym_56924, _9TRUE_428);

        /** 				emit_opnd(sym)*/
        _43emit_opnd(_sym_56924);
L7: 

        /** 			if sym = left_sym then*/
        if (_sym_56924 != _41left_sym_55163)
        goto L8; // [233] 243

        /** 				lhs_subs_level = 0 -- start counting subscripts*/
        _41lhs_subs_level_55161 = 0;
L8: 

        /** 			short_circuit -= 1*/
        _41short_circuit_55122 = _41short_circuit_55122 - 1;

        /** 			tok = next_token()*/
        _0 = _tok_56918;
        _tok_56918 = _41next_token();
        DeRef(_0);

        /** 			current_sequence = append(current_sequence, sym)*/
        Append(&_43current_sequence_51226, _43current_sequence_51226, _sym_56924);

        /** 			while tok[T_ID] = LEFT_SQUARE do*/
L9: 
        _2 = (int)SEQ_PTR(_tok_56918);
        _29375 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(NOTEQ, _29375, -28)){
            _29375 = NOVALUE;
            goto LA; // [279] 450
        }
        _29375 = NOVALUE;

        /** 				subs_depth += 1*/
        _41subs_depth_55164 = _41subs_depth_55164 + 1;

        /** 				if lhs_subs_level >= 0 then*/
        if (_41lhs_subs_level_55161 < 0)
        goto LB; // [295] 308

        /** 					lhs_subs_level += 1*/
        _41lhs_subs_level_55161 = _41lhs_subs_level_55161 + 1;
LB: 

        /** 				save_factors = factors*/
        _save_factors_56921 = _41factors_55160;

        /** 				save_lhs_subs_level = lhs_subs_level*/
        _save_lhs_subs_level_56922 = _41lhs_subs_level_55161;

        /** 				call_proc(forward_expr, {})*/
        _0 = (int)_00[_41forward_expr_56142].addr;
        (*(int (*)())_0)(
                             );

        /** 				tok = next_token()*/
        _0 = _tok_56918;
        _tok_56918 = _41next_token();
        DeRef(_0);

        /** 				if tok[T_ID] = SLICE then*/
        _2 = (int)SEQ_PTR(_tok_56918);
        _29381 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(NOTEQ, _29381, 513)){
            _29381 = NOVALUE;
            goto LC; // [344] 382
        }
        _29381 = NOVALUE;

        /** 					call_proc(forward_expr, {})*/
        _0 = (int)_00[_41forward_expr_56142].addr;
        (*(int (*)())_0)(
                             );

        /** 					emit_op(RHS_SLICE)*/
        _43emit_op(46);

        /** 					tok_match(RIGHT_SQUARE)*/
        _41tok_match(-29, 0);

        /** 					tok = next_token()*/
        _0 = _tok_56918;
        _tok_56918 = _41next_token();
        DeRef(_0);

        /** 					exit*/
        goto LA; // [377] 450
        goto LD; // [379] 430
LC: 

        /** 					putback(tok)*/
        Ref(_tok_56918);
        _41putback(_tok_56918);

        /** 					tok_match(RIGHT_SQUARE)*/
        _41tok_match(-29, 0);

        /** 					subs_depth -= 1*/
        _41subs_depth_55164 = _41subs_depth_55164 - 1;

        /** 					current_sequence = head( current_sequence, length( current_sequence ) - 1 )*/
        if (IS_SEQUENCE(_43current_sequence_51226)){
                _29385 = SEQ_PTR(_43current_sequence_51226)->length;
        }
        else {
            _29385 = 1;
        }
        _29386 = _29385 - 1;
        _29385 = NOVALUE;
        {
            int len = SEQ_PTR(_43current_sequence_51226)->length;
            int size = (IS_ATOM_INT(_29386)) ? _29386 : (object)(DBL_PTR(_29386)->dbl);
            if (size <= 0){
                DeRef( _43current_sequence_51226 );
                _43current_sequence_51226 = MAKE_SEQ(NewS1(0));
            }
            else if (len <= size) {
                RefDS(_43current_sequence_51226);
                DeRef(_43current_sequence_51226);
                _43current_sequence_51226 = _43current_sequence_51226;
            }
            else{
                Head(SEQ_PTR(_43current_sequence_51226),size+1,&_43current_sequence_51226);
            }
        }
        _29386 = NOVALUE;

        /** 					emit_op(RHS_SUBS) -- current_sequence will be updated*/
        _43emit_op(25);
LD: 

        /** 				factors = save_factors*/
        _41factors_55160 = _save_factors_56921;

        /** 				lhs_subs_level = save_lhs_subs_level*/
        _41lhs_subs_level_55161 = _save_lhs_subs_level_56922;

        /** 				tok = next_token()*/
        _0 = _tok_56918;
        _tok_56918 = _41next_token();
        DeRef(_0);

        /** 			end while*/
        goto L9; // [447] 271
LA: 

        /** 			current_sequence = head( current_sequence, length( current_sequence ) - 1 )*/
        if (IS_SEQUENCE(_43current_sequence_51226)){
                _29389 = SEQ_PTR(_43current_sequence_51226)->length;
        }
        else {
            _29389 = 1;
        }
        _29390 = _29389 - 1;
        _29389 = NOVALUE;
        {
            int len = SEQ_PTR(_43current_sequence_51226)->length;
            int size = (IS_ATOM_INT(_29390)) ? _29390 : (object)(DBL_PTR(_29390)->dbl);
            if (size <= 0){
                DeRef( _43current_sequence_51226 );
                _43current_sequence_51226 = MAKE_SEQ(NewS1(0));
            }
            else if (len <= size) {
                RefDS(_43current_sequence_51226);
                DeRef(_43current_sequence_51226);
                _43current_sequence_51226 = _43current_sequence_51226;
            }
            else{
                Head(SEQ_PTR(_43current_sequence_51226),size+1,&_43current_sequence_51226);
            }
        }
        _29390 = NOVALUE;

        /** 			putback(tok)*/
        Ref(_tok_56918);
        _41putback(_tok_56918);

        /** 			short_circuit += 1*/
        _41short_circuit_55122 = _41short_circuit_55122 + 1;
        goto L5; // [482] 696

        /** 		case DOLLAR then*/
        case -22:

        /** 			tok = next_token()*/
        _0 = _tok_56918;
        _tok_56918 = _41next_token();
        DeRef(_0);

        /** 			putback(tok)*/
        Ref(_tok_56918);
        _41putback(_tok_56918);

        /** 			if tok[T_ID] = RIGHT_BRACE then*/
        _2 = (int)SEQ_PTR(_tok_56918);
        _29394 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(NOTEQ, _29394, -25)){
            _29394 = NOVALUE;
            goto LE; // [508] 526
        }
        _29394 = NOVALUE;

        /** 				gListItem[$] = 0*/
        if (IS_SEQUENCE(_41gListItem_55158)){
                _29396 = SEQ_PTR(_41gListItem_55158)->length;
        }
        else {
            _29396 = 1;
        }
        _2 = (int)SEQ_PTR(_41gListItem_55158);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _41gListItem_55158 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _29396);
        *(int *)_2 = 0;
        goto L5; // [523] 696
LE: 

        /** 				if subs_depth > 0 and length(current_sequence) then*/
        _29397 = (_41subs_depth_55164 > 0);
        if (_29397 == 0) {
            goto LF; // [534] 557
        }
        if (IS_SEQUENCE(_43current_sequence_51226)){
                _29399 = SEQ_PTR(_43current_sequence_51226)->length;
        }
        else {
            _29399 = 1;
        }
        if (_29399 == 0)
        {
            _29399 = NOVALUE;
            goto LF; // [544] 557
        }
        else{
            _29399 = NOVALUE;
        }

        /** 					emit_op(DOLLAR)*/
        _43emit_op(-22);
        goto L5; // [554] 696
LF: 

        /** 					CompileErr(21)*/
        RefDS(_22663);
        _46CompileErr(21, _22663, 0);
        goto L5; // [566] 696

        /** 		case ATOM then*/
        case 502:

        /** 			emit_opnd(tok[T_SYM])*/
        _2 = (int)SEQ_PTR(_tok_56918);
        _29400 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_29400);
        _43emit_opnd(_29400);
        _29400 = NOVALUE;
        goto L5; // [583] 696

        /** 		case LEFT_BRACE then*/
        case -24:

        /** 			n = Expr_list()*/
        _n_56920 = _41Expr_list();
        if (!IS_ATOM_INT(_n_56920)) {
            _1 = (long)(DBL_PTR(_n_56920)->dbl);
            if (UNIQUE(DBL_PTR(_n_56920)) && (DBL_PTR(_n_56920)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_n_56920);
            _n_56920 = _1;
        }

        /** 			tok_match(RIGHT_BRACE)*/
        _41tok_match(-25, 0);

        /** 			op_info1 = n*/
        _43op_info1_51218 = _n_56920;

        /** 			emit_op(RIGHT_BRACE_N)*/
        _43emit_op(31);
        goto L5; // [618] 696

        /** 		case STRING then*/
        case 503:

        /** 			emit_opnd(tok[T_SYM])*/
        _2 = (int)SEQ_PTR(_tok_56918);
        _29402 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_29402);
        _43emit_opnd(_29402);
        _29402 = NOVALUE;
        goto L5; // [635] 696

        /** 		case LEFT_ROUND then*/
        case -26:

        /** 			call_proc(forward_expr, {})*/
        _0 = (int)_00[_41forward_expr_56142].addr;
        (*(int (*)())_0)(
                             );

        /** 			tok_match(RIGHT_ROUND)*/
        _41tok_match(-27, 0);
        goto L5; // [656] 696

        /** 		case FUNC, TYPE, QUALIFIED_FUNC, QUALIFIED_TYPE then*/
        case 501:
        case 504:
        case 520:
        case 522:

        /** 			Function_call( tok )*/
        Ref(_tok_56918);
        _41Function_call(_tok_56918);
        goto L5; // [673] 696

        /** 		case else*/
        default:

        /** 			CompileErr(135, {LexName(id)})*/
        RefDS(_27166);
        _29403 = _43LexName(_id_56919, _27166);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _29403;
        _29404 = MAKE_SEQ(_1);
        _29403 = NOVALUE;
        _46CompileErr(135, _29404, 0);
        _29404 = NOVALUE;
    ;}L5: 

    /** end procedure*/
    DeRef(_tok_56918);
    DeRef(_29357);
    _29357 = NOVALUE;
    DeRef(_29397);
    _29397 = NOVALUE;
    return;
    ;
}


void _41UFactor()
{
    int _tok_57077 = NOVALUE;
    int _29410 = NOVALUE;
    int _29408 = NOVALUE;
    int _29406 = NOVALUE;
    int _0, _1, _2;
    

    /** 	tok = next_token()*/
    _0 = _tok_57077;
    _tok_57077 = _41next_token();
    DeRef(_0);

    /** 	if tok[T_ID] = MINUS then*/
    _2 = (int)SEQ_PTR(_tok_57077);
    _29406 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29406, 10)){
        _29406 = NOVALUE;
        goto L1; // [16] 34
    }
    _29406 = NOVALUE;

    /** 		Factor()*/
    _41Factor();

    /** 		emit_op(UMINUS)*/
    _43emit_op(12);
    goto L2; // [31] 93
L1: 

    /** 	elsif tok[T_ID] = NOT then*/
    _2 = (int)SEQ_PTR(_tok_57077);
    _29408 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29408, 7)){
        _29408 = NOVALUE;
        goto L3; // [44] 62
    }
    _29408 = NOVALUE;

    /** 		Factor()*/
    _41Factor();

    /** 		emit_op(NOT)*/
    _43emit_op(7);
    goto L2; // [59] 93
L3: 

    /** 	elsif tok[T_ID] = PLUS then*/
    _2 = (int)SEQ_PTR(_tok_57077);
    _29410 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29410, 11)){
        _29410 = NOVALUE;
        goto L4; // [72] 83
    }
    _29410 = NOVALUE;

    /** 		Factor()*/
    _41Factor();
    goto L2; // [80] 93
L4: 

    /** 		putback(tok)*/
    Ref(_tok_57077);
    _41putback(_tok_57077);

    /** 		Factor()*/
    _41Factor();
L2: 

    /** end procedure*/
    DeRef(_tok_57077);
    return;
    ;
}


int _41Term()
{
    int _tok_57102 = NOVALUE;
    int _29418 = NOVALUE;
    int _29417 = NOVALUE;
    int _29416 = NOVALUE;
    int _29414 = NOVALUE;
    int _29413 = NOVALUE;
    int _0, _1, _2;
    

    /** 	UFactor()*/
    _41UFactor();

    /** 	tok = next_token()*/
    _0 = _tok_57102;
    _tok_57102 = _41next_token();
    DeRef(_0);

    /** 	while tok[T_ID] = reserved:MULTIPLY or tok[T_ID] = reserved:DIVIDE do*/
L1: 
    _2 = (int)SEQ_PTR(_tok_57102);
    _29413 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29413)) {
        _29414 = (_29413 == 13);
    }
    else {
        _29414 = binary_op(EQUALS, _29413, 13);
    }
    _29413 = NOVALUE;
    if (IS_ATOM_INT(_29414)) {
        if (_29414 != 0) {
            goto L2; // [25] 44
        }
    }
    else {
        if (DBL_PTR(_29414)->dbl != 0.0) {
            goto L2; // [25] 44
        }
    }
    _2 = (int)SEQ_PTR(_tok_57102);
    _29416 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29416)) {
        _29417 = (_29416 == 14);
    }
    else {
        _29417 = binary_op(EQUALS, _29416, 14);
    }
    _29416 = NOVALUE;
    if (_29417 <= 0) {
        if (_29417 == 0) {
            DeRef(_29417);
            _29417 = NOVALUE;
            goto L3; // [40] 69
        }
        else {
            if (!IS_ATOM_INT(_29417) && DBL_PTR(_29417)->dbl == 0.0){
                DeRef(_29417);
                _29417 = NOVALUE;
                goto L3; // [40] 69
            }
            DeRef(_29417);
            _29417 = NOVALUE;
        }
    }
    DeRef(_29417);
    _29417 = NOVALUE;
L2: 

    /** 		UFactor()*/
    _41UFactor();

    /** 		emit_op(tok[T_ID])*/
    _2 = (int)SEQ_PTR(_tok_57102);
    _29418 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_29418);
    _43emit_op(_29418);
    _29418 = NOVALUE;

    /** 		tok = next_token()*/
    _0 = _tok_57102;
    _tok_57102 = _41next_token();
    DeRef(_0);

    /** 	end while*/
    goto L1; // [66] 15
L3: 

    /** 	return tok*/
    DeRef(_29414);
    _29414 = NOVALUE;
    return _tok_57102;
    ;
}


int _41aexpr()
{
    int _tok_57119 = NOVALUE;
    int _id_57120 = NOVALUE;
    int _29425 = NOVALUE;
    int _29424 = NOVALUE;
    int _29422 = NOVALUE;
    int _29421 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer id*/

    /** 	tok = Term()*/
    _0 = _tok_57119;
    _tok_57119 = _41Term();
    DeRef(_0);

    /** 	while tok[T_ID] = PLUS or tok[T_ID] = MINUS do*/
L1: 
    _2 = (int)SEQ_PTR(_tok_57119);
    _29421 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29421)) {
        _29422 = (_29421 == 11);
    }
    else {
        _29422 = binary_op(EQUALS, _29421, 11);
    }
    _29421 = NOVALUE;
    if (IS_ATOM_INT(_29422)) {
        if (_29422 != 0) {
            goto L2; // [25] 46
        }
    }
    else {
        if (DBL_PTR(_29422)->dbl != 0.0) {
            goto L2; // [25] 46
        }
    }
    _2 = (int)SEQ_PTR(_tok_57119);
    _29424 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29424)) {
        _29425 = (_29424 == 10);
    }
    else {
        _29425 = binary_op(EQUALS, _29424, 10);
    }
    _29424 = NOVALUE;
    if (_29425 <= 0) {
        if (_29425 == 0) {
            DeRef(_29425);
            _29425 = NOVALUE;
            goto L3; // [42] 71
        }
        else {
            if (!IS_ATOM_INT(_29425) && DBL_PTR(_29425)->dbl == 0.0){
                DeRef(_29425);
                _29425 = NOVALUE;
                goto L3; // [42] 71
            }
            DeRef(_29425);
            _29425 = NOVALUE;
        }
    }
    DeRef(_29425);
    _29425 = NOVALUE;
L2: 

    /** 		id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_57119);
    _id_57120 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_id_57120)){
        _id_57120 = (long)DBL_PTR(_id_57120)->dbl;
    }

    /** 		tok = Term()*/
    _0 = _tok_57119;
    _tok_57119 = _41Term();
    DeRef(_0);

    /** 		emit_op(id)*/
    _43emit_op(_id_57120);

    /** 	end while*/
    goto L1; // [68] 13
L3: 

    /** 	return tok*/
    DeRef(_29422);
    _29422 = NOVALUE;
    return _tok_57119;
    ;
}


int _41cexpr()
{
    int _tok_57139 = NOVALUE;
    int _concat_count_57140 = NOVALUE;
    int _29429 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer concat_count*/

    /** 	tok = aexpr()*/
    _0 = _tok_57139;
    _tok_57139 = _41aexpr();
    DeRef(_0);

    /** 	concat_count = 0*/
    _concat_count_57140 = 0;

    /** 	while tok[T_ID] = reserved:CONCAT do*/
L1: 
    _2 = (int)SEQ_PTR(_tok_57139);
    _29429 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29429, 15)){
        _29429 = NOVALUE;
        goto L2; // [24] 44
    }
    _29429 = NOVALUE;

    /** 		tok = aexpr()*/
    _0 = _tok_57139;
    _tok_57139 = _41aexpr();
    DeRef(_0);

    /** 		concat_count += 1*/
    _concat_count_57140 = _concat_count_57140 + 1;

    /** 	end while*/
    goto L1; // [41] 18
L2: 

    /** 	if concat_count = 1 then*/
    if (_concat_count_57140 != 1)
    goto L3; // [46] 58

    /** 		emit_op( reserved:CONCAT )*/
    _43emit_op(15);
    goto L4; // [55] 81
L3: 

    /** 	elsif concat_count > 1 then*/
    if (_concat_count_57140 <= 1)
    goto L5; // [60] 80

    /** 		op_info1 = concat_count+1*/
    _43op_info1_51218 = _concat_count_57140 + 1;

    /** 		emit_op(CONCAT_N)*/
    _43emit_op(157);
L5: 
L4: 

    /** 	return tok*/
    return _tok_57139;
    ;
}


int _41rexpr()
{
    int _tok_57160 = NOVALUE;
    int _id_57161 = NOVALUE;
    int _29441 = NOVALUE;
    int _29440 = NOVALUE;
    int _29439 = NOVALUE;
    int _29438 = NOVALUE;
    int _29437 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer id*/

    /** 	tok = cexpr()*/
    _0 = _tok_57160;
    _tok_57160 = _41cexpr();
    DeRef(_0);

    /** 	while tok[T_ID] <= GREATER and tok[T_ID] >= LESS do*/
L1: 
    _2 = (int)SEQ_PTR(_tok_57160);
    _29437 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29437)) {
        _29438 = (_29437 <= 6);
    }
    else {
        _29438 = binary_op(LESSEQ, _29437, 6);
    }
    _29437 = NOVALUE;
    if (IS_ATOM_INT(_29438)) {
        if (_29438 == 0) {
            goto L2; // [25] 70
        }
    }
    else {
        if (DBL_PTR(_29438)->dbl == 0.0) {
            goto L2; // [25] 70
        }
    }
    _2 = (int)SEQ_PTR(_tok_57160);
    _29440 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29440)) {
        _29441 = (_29440 >= 1);
    }
    else {
        _29441 = binary_op(GREATEREQ, _29440, 1);
    }
    _29440 = NOVALUE;
    if (_29441 <= 0) {
        if (_29441 == 0) {
            DeRef(_29441);
            _29441 = NOVALUE;
            goto L2; // [42] 70
        }
        else {
            if (!IS_ATOM_INT(_29441) && DBL_PTR(_29441)->dbl == 0.0){
                DeRef(_29441);
                _29441 = NOVALUE;
                goto L2; // [42] 70
            }
            DeRef(_29441);
            _29441 = NOVALUE;
        }
    }
    DeRef(_29441);
    _29441 = NOVALUE;

    /** 		id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_57160);
    _id_57161 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_id_57161)){
        _id_57161 = (long)DBL_PTR(_id_57161)->dbl;
    }

    /** 		tok = cexpr()*/
    _0 = _tok_57160;
    _tok_57160 = _41cexpr();
    DeRef(_0);

    /** 		emit_op(id)*/
    _43emit_op(_id_57161);

    /** 	end while*/
    goto L1; // [67] 13
L2: 

    /** 	return tok*/
    DeRef(_29438);
    _29438 = NOVALUE;
    return _tok_57160;
    ;
}


void _41Expr()
{
    int _tok_57187 = NOVALUE;
    int _id_57188 = NOVALUE;
    int _patch_57189 = NOVALUE;
    int _29464 = NOVALUE;
    int _29462 = NOVALUE;
    int _29461 = NOVALUE;
    int _29459 = NOVALUE;
    int _29458 = NOVALUE;
    int _29457 = NOVALUE;
    int _29456 = NOVALUE;
    int _29455 = NOVALUE;
    int _29449 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer id*/

    /** 	integer patch*/

    /** 	ExprLine = ThisLine*/
    Ref(_46ThisLine_49482);
    DeRef(_41ExprLine_57182);
    _41ExprLine_57182 = _46ThisLine_49482;

    /** 	expr_bp = bp*/
    _41expr_bp_57183 = _46bp_49486;

    /** 	id = -1*/
    _id_57188 = -1;

    /** 	patch = 0*/
    _patch_57189 = 0;

    /** 	while TRUE do*/
L1: 
    if (_9TRUE_428 == 0)
    {
        goto L2; // [40] 300
    }
    else{
    }

    /** 		if id != -1 then*/
    if (_id_57188 == -1)
    goto L3; // [45] 116

    /** 			if id != XOR then*/
    if (_id_57188 == 152)
    goto L4; // [53] 115

    /** 				if short_circuit > 0 then*/
    if (_41short_circuit_55122 <= 0)
    goto L5; // [61] 114

    /** 					if id = OR then*/
    if (_id_57188 != 9)
    goto L6; // [69] 83

    /** 						emit_op(SC1_OR)*/
    _43emit_op(143);
    goto L7; // [80] 91
L6: 

    /** 						emit_op(SC1_AND)*/
    _43emit_op(141);
L7: 

    /** 					patch = length(Code)+1*/
    if (IS_SEQUENCE(_38Code_17038)){
            _29449 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29449 = 1;
    }
    _patch_57189 = _29449 + 1;
    _29449 = NOVALUE;

    /** 					emit_forward_addr()*/
    _41emit_forward_addr();

    /** 					short_circuit_B = TRUE*/
    _41short_circuit_B_55124 = _9TRUE_428;
L5: 
L4: 
L3: 

    /** 		tok = rexpr()*/
    _0 = _tok_57187;
    _tok_57187 = _41rexpr();
    DeRef(_0);

    /** 		if id != -1 then*/
    if (_id_57188 == -1)
    goto L8; // [123] 268

    /** 			if id != XOR then*/
    if (_id_57188 == 152)
    goto L9; // [131] 261

    /** 				if short_circuit > 0 then*/
    if (_41short_circuit_55122 <= 0)
    goto LA; // [139] 252

    /** 					if tok[T_ID] != THEN and tok[T_ID] != DO then*/
    _2 = (int)SEQ_PTR(_tok_57187);
    _29455 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29455)) {
        _29456 = (_29455 != 410);
    }
    else {
        _29456 = binary_op(NOTEQ, _29455, 410);
    }
    _29455 = NOVALUE;
    if (IS_ATOM_INT(_29456)) {
        if (_29456 == 0) {
            goto LB; // [157] 206
        }
    }
    else {
        if (DBL_PTR(_29456)->dbl == 0.0) {
            goto LB; // [157] 206
        }
    }
    _2 = (int)SEQ_PTR(_tok_57187);
    _29458 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29458)) {
        _29459 = (_29458 != 411);
    }
    else {
        _29459 = binary_op(NOTEQ, _29458, 411);
    }
    _29458 = NOVALUE;
    if (_29459 == 0) {
        DeRef(_29459);
        _29459 = NOVALUE;
        goto LB; // [174] 206
    }
    else {
        if (!IS_ATOM_INT(_29459) && DBL_PTR(_29459)->dbl == 0.0){
            DeRef(_29459);
            _29459 = NOVALUE;
            goto LB; // [174] 206
        }
        DeRef(_29459);
        _29459 = NOVALUE;
    }
    DeRef(_29459);
    _29459 = NOVALUE;

    /** 						if id = OR then*/
    if (_id_57188 != 9)
    goto LC; // [181] 195

    /** 							emit_op(SC2_OR)*/
    _43emit_op(144);
    goto LD; // [192] 219
LC: 

    /** 							emit_op(SC2_AND)*/
    _43emit_op(142);
    goto LD; // [203] 219
LB: 

    /** 						SC1_type = id -- if/while/elsif must patch*/
    _41SC1_type_55127 = _id_57188;

    /** 						emit_op(SC2_NULL)*/
    _43emit_op(145);
LD: 

    /** 					if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto LE; // [223] 234
    }
    else{
    }

    /** 						emit_op(NOP1)   -- to get label here*/
    _43emit_op(159);
LE: 

    /** 					backpatch(patch, length(Code)+1)*/
    if (IS_SEQUENCE(_38Code_17038)){
            _29461 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29461 = 1;
    }
    _29462 = _29461 + 1;
    _29461 = NOVALUE;
    _43backpatch(_patch_57189, _29462);
    _29462 = NOVALUE;
    goto LF; // [249] 267
LA: 

    /** 					emit_op(id)*/
    _43emit_op(_id_57188);
    goto LF; // [258] 267
L9: 

    /** 				emit_op(id)*/
    _43emit_op(_id_57188);
LF: 
L8: 

    /** 		id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_57187);
    _id_57188 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_id_57188)){
        _id_57188 = (long)DBL_PTR(_id_57188)->dbl;
    }

    /** 		if not find(id, boolOps) then*/
    _29464 = find_from(_id_57188, _41boolOps_57177, 1);
    if (_29464 != 0)
    goto L1; // [287] 38
    _29464 = NOVALUE;

    /** 			exit*/
    goto L2; // [292] 300

    /** 	end while*/
    goto L1; // [297] 38
L2: 

    /** 	putback(tok)*/
    Ref(_tok_57187);
    _41putback(_tok_57187);

    /** 	SC1_patch = patch -- extra line*/
    _41SC1_patch_55126 = _patch_57189;

    /** end procedure*/
    DeRef(_tok_57187);
    DeRef(_29456);
    _29456 = NOVALUE;
    return;
    ;
}


void _41TypeCheck(int _var_57264)
{
    int _which_type_57265 = NOVALUE;
    int _ref_57275 = NOVALUE;
    int _ref_57308 = NOVALUE;
    int _29520 = NOVALUE;
    int _29519 = NOVALUE;
    int _29518 = NOVALUE;
    int _29517 = NOVALUE;
    int _29516 = NOVALUE;
    int _29514 = NOVALUE;
    int _29513 = NOVALUE;
    int _29512 = NOVALUE;
    int _29511 = NOVALUE;
    int _29510 = NOVALUE;
    int _29509 = NOVALUE;
    int _29507 = NOVALUE;
    int _29506 = NOVALUE;
    int _29503 = NOVALUE;
    int _29502 = NOVALUE;
    int _29501 = NOVALUE;
    int _29500 = NOVALUE;
    int _29495 = NOVALUE;
    int _29494 = NOVALUE;
    int _29490 = NOVALUE;
    int _29488 = NOVALUE;
    int _29487 = NOVALUE;
    int _29486 = NOVALUE;
    int _29484 = NOVALUE;
    int _29483 = NOVALUE;
    int _29482 = NOVALUE;
    int _29481 = NOVALUE;
    int _29480 = NOVALUE;
    int _29479 = NOVALUE;
    int _29476 = NOVALUE;
    int _29474 = NOVALUE;
    int _29472 = NOVALUE;
    int _29471 = NOVALUE;
    int _29470 = NOVALUE;
    int _29468 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if var < 0 or SymTab[var][S_SCOPE] = SC_UNDEFINED then*/
    _29468 = (_var_57264 < 0);
    if (_29468 != 0) {
        goto L1; // [9] 36
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29470 = (int)*(((s1_ptr)_2)->base + _var_57264);
    _2 = (int)SEQ_PTR(_29470);
    _29471 = (int)*(((s1_ptr)_2)->base + 4);
    _29470 = NOVALUE;
    if (IS_ATOM_INT(_29471)) {
        _29472 = (_29471 == 9);
    }
    else {
        _29472 = binary_op(EQUALS, _29471, 9);
    }
    _29471 = NOVALUE;
    if (_29472 == 0) {
        DeRef(_29472);
        _29472 = NOVALUE;
        goto L2; // [32] 76
    }
    else {
        if (!IS_ATOM_INT(_29472) && DBL_PTR(_29472)->dbl == 0.0){
            DeRef(_29472);
            _29472 = NOVALUE;
            goto L2; // [32] 76
        }
        DeRef(_29472);
        _29472 = NOVALUE;
    }
    DeRef(_29472);
    _29472 = NOVALUE;
L1: 

    /** 		integer ref = new_forward_reference( TYPE_CHECK, var, TYPE_CHECK_FORWARD )*/
    _ref_57275 = _40new_forward_reference(65, _var_57264, 197);
    if (!IS_ATOM_INT(_ref_57275)) {
        _1 = (long)(DBL_PTR(_ref_57275)->dbl);
        if (UNIQUE(DBL_PTR(_ref_57275)) && (DBL_PTR(_ref_57275)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_57275);
        _ref_57275 = _1;
    }

    /** 		Code &= { TYPE_CHECK_FORWARD, var, OpTypeCheck }*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 197;
    *((int *)(_2+8)) = _var_57264;
    *((int *)(_2+12)) = _38OpTypeCheck_17016;
    _29474 = MAKE_SEQ(_1);
    Concat((object_ptr)&_38Code_17038, _38Code_17038, _29474);
    DeRefDS(_29474);
    _29474 = NOVALUE;

    /** 		return*/
    DeRef(_29468);
    _29468 = NOVALUE;
    return;
L2: 

    /** 	which_type = SymTab[var][S_VTYPE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29476 = (int)*(((s1_ptr)_2)->base + _var_57264);
    _2 = (int)SEQ_PTR(_29476);
    _which_type_57265 = (int)*(((s1_ptr)_2)->base + 15);
    if (!IS_ATOM_INT(_which_type_57265)){
        _which_type_57265 = (long)DBL_PTR(_which_type_57265)->dbl;
    }
    _29476 = NOVALUE;

    /** 	if which_type = 0 then*/
    if (_which_type_57265 != 0)
    goto L3; // [96] 106

    /** 		return	-- Not a typed identifier.*/
    DeRef(_29468);
    _29468 = NOVALUE;
    return;
L3: 

    /** 	if which_type > 0 and length(SymTab[which_type]) < S_TOKEN then*/
    _29479 = (_which_type_57265 > 0);
    if (_29479 == 0) {
        goto L4; // [112] 141
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29481 = (int)*(((s1_ptr)_2)->base + _which_type_57265);
    if (IS_SEQUENCE(_29481)){
            _29482 = SEQ_PTR(_29481)->length;
    }
    else {
        _29482 = 1;
    }
    _29481 = NOVALUE;
    if (IS_ATOM_INT(_38S_TOKEN_16603)) {
        _29483 = (_29482 < _38S_TOKEN_16603);
    }
    else {
        _29483 = binary_op(LESS, _29482, _38S_TOKEN_16603);
    }
    _29482 = NOVALUE;
    if (_29483 == 0) {
        DeRef(_29483);
        _29483 = NOVALUE;
        goto L4; // [132] 141
    }
    else {
        if (!IS_ATOM_INT(_29483) && DBL_PTR(_29483)->dbl == 0.0){
            DeRef(_29483);
            _29483 = NOVALUE;
            goto L4; // [132] 141
        }
        DeRef(_29483);
        _29483 = NOVALUE;
    }
    DeRef(_29483);
    _29483 = NOVALUE;

    /** 		return	-- Not a typed identifier.*/
    DeRef(_29468);
    _29468 = NOVALUE;
    DeRef(_29479);
    _29479 = NOVALUE;
    _29481 = NOVALUE;
    return;
L4: 

    /** 	if which_type < 0 or SymTab[which_type][S_TOKEN] = VARIABLE  then*/
    _29484 = (_which_type_57265 < 0);
    if (_29484 != 0) {
        goto L5; // [147] 174
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29486 = (int)*(((s1_ptr)_2)->base + _which_type_57265);
    _2 = (int)SEQ_PTR(_29486);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _29487 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _29487 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _29486 = NOVALUE;
    if (IS_ATOM_INT(_29487)) {
        _29488 = (_29487 == -100);
    }
    else {
        _29488 = binary_op(EQUALS, _29487, -100);
    }
    _29487 = NOVALUE;
    if (_29488 == 0) {
        DeRef(_29488);
        _29488 = NOVALUE;
        goto L6; // [170] 214
    }
    else {
        if (!IS_ATOM_INT(_29488) && DBL_PTR(_29488)->dbl == 0.0){
            DeRef(_29488);
            _29488 = NOVALUE;
            goto L6; // [170] 214
        }
        DeRef(_29488);
        _29488 = NOVALUE;
    }
    DeRef(_29488);
    _29488 = NOVALUE;
L5: 

    /** 		integer ref = new_forward_reference( TYPE_CHECK, which_type, TYPE )*/
    _ref_57308 = _40new_forward_reference(65, _which_type_57265, 504);
    if (!IS_ATOM_INT(_ref_57308)) {
        _1 = (long)(DBL_PTR(_ref_57308)->dbl);
        if (UNIQUE(DBL_PTR(_ref_57308)) && (DBL_PTR(_ref_57308)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_57308);
        _ref_57308 = _1;
    }

    /** 		Code &= { TYPE_CHECK_FORWARD, var, OpTypeCheck }*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 197;
    *((int *)(_2+8)) = _var_57264;
    *((int *)(_2+12)) = _38OpTypeCheck_17016;
    _29490 = MAKE_SEQ(_1);
    Concat((object_ptr)&_38Code_17038, _38Code_17038, _29490);
    DeRefDS(_29490);
    _29490 = NOVALUE;

    /** 		return*/
    DeRef(_29468);
    _29468 = NOVALUE;
    DeRef(_29479);
    _29479 = NOVALUE;
    _29481 = NOVALUE;
    DeRef(_29484);
    _29484 = NOVALUE;
    return;
L6: 

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L7; // [220] 317
    }
    else{
    }

    /** 		if OpTypeCheck then*/
    if (_38OpTypeCheck_17016 == 0)
    {
        goto L8; // [227] 481
    }
    else{
    }

    /** 			switch which_type do*/
    if( _41_57322_cases == 0 ){
        _41_57322_cases = 1;
        SEQ_PTR( _29492 )->base[1] = _55object_type_47055;
        SEQ_PTR( _29492 )->base[2] = _55sequence_type_47059;
        SEQ_PTR( _29492 )->base[3] = _55atom_type_47057;
        SEQ_PTR( _29492 )->base[4] = _55integer_type_47061;
    }
    _1 = find(_which_type_57265, _29492);
    switch ( _1 ){ 

        /** 				case object_type, sequence_type, atom_type then*/
        case 1:
        case 2:
        case 3:

        /** 				case integer_type then*/
        goto L8; // [247] 481
        case 4:

        /** 					op_info1 = var*/
        _43op_info1_51218 = _var_57264;

        /** 					emit_op(INTEGER_CHECK)*/
        _43emit_op(96);
        goto L8; // [265] 481

        /** 				case else*/
        case 0:

        /** 					if SymTab[which_type][S_EFFECT] then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _29494 = (int)*(((s1_ptr)_2)->base + _which_type_57265);
        _2 = (int)SEQ_PTR(_29494);
        _29495 = (int)*(((s1_ptr)_2)->base + 23);
        _29494 = NOVALUE;
        if (_29495 == 0) {
            _29495 = NOVALUE;
            goto L9; // [285] 312
        }
        else {
            if (!IS_ATOM_INT(_29495) && DBL_PTR(_29495)->dbl == 0.0){
                _29495 = NOVALUE;
                goto L9; // [285] 312
            }
            _29495 = NOVALUE;
        }
        _29495 = NOVALUE;

        /** 						emit_opnd(var)*/
        _43emit_opnd(_var_57264);

        /** 						op_info1 = which_type*/
        _43op_info1_51218 = _which_type_57265;

        /** 						emit_or_inline()*/
        _69emit_or_inline();

        /** 						emit_op(TYPE_CHECK)*/
        _43emit_op(65);
L9: 
    ;}    goto L8; // [314] 481
L7: 

    /** 		if OpTypeCheck then*/
    if (_38OpTypeCheck_17016 == 0)
    {
        goto LA; // [321] 480
    }
    else{
    }

    /** 			if which_type != object_type then*/
    if (_which_type_57265 == _55object_type_47055)
    goto LB; // [328] 479

    /** 				if which_type = integer_type then*/
    if (_which_type_57265 != _55integer_type_47061)
    goto LC; // [336] 357

    /** 						op_info1 = var*/
    _43op_info1_51218 = _var_57264;

    /** 						emit_op(INTEGER_CHECK)*/
    _43emit_op(96);
    goto LD; // [354] 478
LC: 

    /** 				elsif which_type = sequence_type then*/
    if (_which_type_57265 != _55sequence_type_47059)
    goto LE; // [361] 382

    /** 						op_info1 = var*/
    _43op_info1_51218 = _var_57264;

    /** 						emit_op(SEQUENCE_CHECK)*/
    _43emit_op(97);
    goto LD; // [379] 478
LE: 

    /** 				elsif which_type = atom_type then*/
    if (_which_type_57265 != _55atom_type_47057)
    goto LF; // [386] 407

    /** 						op_info1 = var*/
    _43op_info1_51218 = _var_57264;

    /** 						emit_op(ATOM_CHECK)*/
    _43emit_op(101);
    goto LD; // [404] 478
LF: 

    /** 						if SymTab[SymTab[which_type][S_NEXT]][S_VTYPE] =*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29500 = (int)*(((s1_ptr)_2)->base + _which_type_57265);
    _2 = (int)SEQ_PTR(_29500);
    _29501 = (int)*(((s1_ptr)_2)->base + 2);
    _29500 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_29501)){
        _29502 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29501)->dbl));
    }
    else{
        _29502 = (int)*(((s1_ptr)_2)->base + _29501);
    }
    _2 = (int)SEQ_PTR(_29502);
    _29503 = (int)*(((s1_ptr)_2)->base + 15);
    _29502 = NOVALUE;
    if (binary_op_a(NOTEQ, _29503, _55integer_type_47061)){
        _29503 = NOVALUE;
        goto L10; // [435] 454
    }
    _29503 = NOVALUE;

    /** 							op_info1 = var*/
    _43op_info1_51218 = _var_57264;

    /** 							emit_op(INTEGER_CHECK) -- need integer conversion*/
    _43emit_op(96);
L10: 

    /** 						emit_opnd(var)*/
    _43emit_opnd(_var_57264);

    /** 						op_info1 = which_type*/
    _43op_info1_51218 = _which_type_57265;

    /** 						emit_or_inline()*/
    _69emit_or_inline();

    /** 						emit_op(TYPE_CHECK)*/
    _43emit_op(65);
LD: 
LB: 
LA: 
L8: 

    /** 	if TRANSLATE or not OpTypeCheck then*/
    if (_38TRANSLATE_16564 != 0) {
        goto L11; // [485] 499
    }
    _29506 = (_38OpTypeCheck_17016 == 0);
    if (_29506 == 0)
    {
        DeRef(_29506);
        _29506 = NOVALUE;
        goto L12; // [495] 620
    }
    else{
        DeRef(_29506);
        _29506 = NOVALUE;
    }
L11: 

    /** 		op_info1 = var*/
    _43op_info1_51218 = _var_57264;

    /** 		if which_type = sequence_type or*/
    _29507 = (_which_type_57265 == _55sequence_type_47059);
    if (_29507 != 0) {
        goto L13; // [514] 553
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29509 = (int)*(((s1_ptr)_2)->base + _which_type_57265);
    _2 = (int)SEQ_PTR(_29509);
    _29510 = (int)*(((s1_ptr)_2)->base + 2);
    _29509 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_29510)){
        _29511 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29510)->dbl));
    }
    else{
        _29511 = (int)*(((s1_ptr)_2)->base + _29510);
    }
    _2 = (int)SEQ_PTR(_29511);
    _29512 = (int)*(((s1_ptr)_2)->base + 15);
    _29511 = NOVALUE;
    if (IS_ATOM_INT(_29512)) {
        _29513 = (_29512 == _55sequence_type_47059);
    }
    else {
        _29513 = binary_op(EQUALS, _29512, _55sequence_type_47059);
    }
    _29512 = NOVALUE;
    if (_29513 == 0) {
        DeRef(_29513);
        _29513 = NOVALUE;
        goto L14; // [549] 563
    }
    else {
        if (!IS_ATOM_INT(_29513) && DBL_PTR(_29513)->dbl == 0.0){
            DeRef(_29513);
            _29513 = NOVALUE;
            goto L14; // [549] 563
        }
        DeRef(_29513);
        _29513 = NOVALUE;
    }
    DeRef(_29513);
    _29513 = NOVALUE;
L13: 

    /** 			emit_op(SEQUENCE_CHECK)*/
    _43emit_op(97);
    goto L15; // [560] 619
L14: 

    /** 		elsif which_type = integer_type or*/
    _29514 = (_which_type_57265 == _55integer_type_47061);
    if (_29514 != 0) {
        goto L16; // [571] 610
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29516 = (int)*(((s1_ptr)_2)->base + _which_type_57265);
    _2 = (int)SEQ_PTR(_29516);
    _29517 = (int)*(((s1_ptr)_2)->base + 2);
    _29516 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_29517)){
        _29518 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29517)->dbl));
    }
    else{
        _29518 = (int)*(((s1_ptr)_2)->base + _29517);
    }
    _2 = (int)SEQ_PTR(_29518);
    _29519 = (int)*(((s1_ptr)_2)->base + 15);
    _29518 = NOVALUE;
    if (IS_ATOM_INT(_29519)) {
        _29520 = (_29519 == _55integer_type_47061);
    }
    else {
        _29520 = binary_op(EQUALS, _29519, _55integer_type_47061);
    }
    _29519 = NOVALUE;
    if (_29520 == 0) {
        DeRef(_29520);
        _29520 = NOVALUE;
        goto L17; // [606] 618
    }
    else {
        if (!IS_ATOM_INT(_29520) && DBL_PTR(_29520)->dbl == 0.0){
            DeRef(_29520);
            _29520 = NOVALUE;
            goto L17; // [606] 618
        }
        DeRef(_29520);
        _29520 = NOVALUE;
    }
    DeRef(_29520);
    _29520 = NOVALUE;
L16: 

    /** 			emit_op(INTEGER_CHECK)*/
    _43emit_op(96);
L17: 
L15: 
L12: 

    /** end procedure*/
    DeRef(_29468);
    _29468 = NOVALUE;
    DeRef(_29479);
    _29479 = NOVALUE;
    _29481 = NOVALUE;
    DeRef(_29484);
    _29484 = NOVALUE;
    _29501 = NOVALUE;
    DeRef(_29507);
    _29507 = NOVALUE;
    _29510 = NOVALUE;
    DeRef(_29514);
    _29514 = NOVALUE;
    _29517 = NOVALUE;
    return;
    ;
}


void _41Assignment(int _left_var_57429)
{
    int _tok_57431 = NOVALUE;
    int _subs_57432 = NOVALUE;
    int _slice_57433 = NOVALUE;
    int _assign_op_57434 = NOVALUE;
    int _subs1_patch_57435 = NOVALUE;
    int _dangerous_57437 = NOVALUE;
    int _lname_57561 = NOVALUE;
    int _temp_len_57578 = NOVALUE;
    int _29619 = NOVALUE;
    int _29618 = NOVALUE;
    int _29617 = NOVALUE;
    int _29616 = NOVALUE;
    int _29615 = NOVALUE;
    int _29614 = NOVALUE;
    int _29613 = NOVALUE;
    int _29612 = NOVALUE;
    int _29603 = NOVALUE;
    int _29602 = NOVALUE;
    int _29601 = NOVALUE;
    int _29600 = NOVALUE;
    int _29599 = NOVALUE;
    int _29598 = NOVALUE;
    int _29597 = NOVALUE;
    int _29596 = NOVALUE;
    int _29595 = NOVALUE;
    int _29594 = NOVALUE;
    int _29593 = NOVALUE;
    int _29592 = NOVALUE;
    int _29591 = NOVALUE;
    int _29590 = NOVALUE;
    int _29589 = NOVALUE;
    int _29587 = NOVALUE;
    int _29586 = NOVALUE;
    int _29585 = NOVALUE;
    int _29583 = NOVALUE;
    int _29578 = NOVALUE;
    int _29577 = NOVALUE;
    int _29574 = NOVALUE;
    int _29573 = NOVALUE;
    int _29571 = NOVALUE;
    int _29565 = NOVALUE;
    int _29560 = NOVALUE;
    int _29557 = NOVALUE;
    int _29556 = NOVALUE;
    int _29553 = NOVALUE;
    int _29550 = NOVALUE;
    int _29549 = NOVALUE;
    int _29548 = NOVALUE;
    int _29546 = NOVALUE;
    int _29545 = NOVALUE;
    int _29544 = NOVALUE;
    int _29543 = NOVALUE;
    int _29542 = NOVALUE;
    int _29541 = NOVALUE;
    int _29539 = NOVALUE;
    int _29538 = NOVALUE;
    int _29537 = NOVALUE;
    int _29536 = NOVALUE;
    int _29534 = NOVALUE;
    int _29533 = NOVALUE;
    int _29532 = NOVALUE;
    int _29531 = NOVALUE;
    int _29530 = NOVALUE;
    int _29528 = NOVALUE;
    int _29527 = NOVALUE;
    int _29526 = NOVALUE;
    int _29523 = NOVALUE;
    int _29522 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	integer subs, slice, assign_op, subs1_patch*/

    /** 	left_sym = left_var[T_SYM]*/
    _2 = (int)SEQ_PTR(_left_var_57429);
    _41left_sym_55163 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_41left_sym_55163)){
        _41left_sym_55163 = (long)DBL_PTR(_41left_sym_55163)->dbl;
    }

    /** 	if SymTab[left_sym][S_SCOPE] = SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29522 = (int)*(((s1_ptr)_2)->base + _41left_sym_55163);
    _2 = (int)SEQ_PTR(_29522);
    _29523 = (int)*(((s1_ptr)_2)->base + 4);
    _29522 = NOVALUE;
    if (binary_op_a(NOTEQ, _29523, 9)){
        _29523 = NOVALUE;
        goto L1; // [31] 54
    }
    _29523 = NOVALUE;

    /** 		Forward_var( left_var, ,ASSIGN )*/
    Ref(_left_var_57429);
    _41Forward_var(_left_var_57429, -1, 18);

    /** 		left_sym = Pop() -- pops off what forward var emitted, because it gets emitted later*/
    _0 = _43Pop();
    _41left_sym_55163 = _0;
    if (!IS_ATOM_INT(_41left_sym_55163)) {
        _1 = (long)(DBL_PTR(_41left_sym_55163)->dbl);
        if (UNIQUE(DBL_PTR(_41left_sym_55163)) && (DBL_PTR(_41left_sym_55163)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_41left_sym_55163);
        _41left_sym_55163 = _1;
    }
    goto L2; // [51] 267
L1: 

    /** 		UndefinedVar(left_sym)*/
    _41UndefinedVar(_41left_sym_55163);

    /** 		if SymTab[left_sym][S_SCOPE] = SC_LOOP_VAR or*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29526 = (int)*(((s1_ptr)_2)->base + _41left_sym_55163);
    _2 = (int)SEQ_PTR(_29526);
    _29527 = (int)*(((s1_ptr)_2)->base + 4);
    _29526 = NOVALUE;
    if (IS_ATOM_INT(_29527)) {
        _29528 = (_29527 == 2);
    }
    else {
        _29528 = binary_op(EQUALS, _29527, 2);
    }
    _29527 = NOVALUE;
    if (IS_ATOM_INT(_29528)) {
        if (_29528 != 0) {
            goto L3; // [83] 112
        }
    }
    else {
        if (DBL_PTR(_29528)->dbl != 0.0) {
            goto L3; // [83] 112
        }
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29530 = (int)*(((s1_ptr)_2)->base + _41left_sym_55163);
    _2 = (int)SEQ_PTR(_29530);
    _29531 = (int)*(((s1_ptr)_2)->base + 4);
    _29530 = NOVALUE;
    if (IS_ATOM_INT(_29531)) {
        _29532 = (_29531 == 4);
    }
    else {
        _29532 = binary_op(EQUALS, _29531, 4);
    }
    _29531 = NOVALUE;
    if (_29532 == 0) {
        DeRef(_29532);
        _29532 = NOVALUE;
        goto L4; // [108] 122
    }
    else {
        if (!IS_ATOM_INT(_29532) && DBL_PTR(_29532)->dbl == 0.0){
            DeRef(_29532);
            _29532 = NOVALUE;
            goto L4; // [108] 122
        }
        DeRef(_29532);
        _29532 = NOVALUE;
    }
    DeRef(_29532);
    _29532 = NOVALUE;
L3: 

    /** 			CompileErr(109)*/
    RefDS(_22663);
    _46CompileErr(109, _22663, 0);
    goto L5; // [119] 229
L4: 

    /** 		elsif SymTab[left_sym][S_MODE] = M_CONSTANT then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29533 = (int)*(((s1_ptr)_2)->base + _41left_sym_55163);
    _2 = (int)SEQ_PTR(_29533);
    _29534 = (int)*(((s1_ptr)_2)->base + 3);
    _29533 = NOVALUE;
    if (binary_op_a(NOTEQ, _29534, 2)){
        _29534 = NOVALUE;
        goto L6; // [140] 154
    }
    _29534 = NOVALUE;

    /** 			CompileErr(110)*/
    RefDS(_22663);
    _46CompileErr(110, _22663, 0);
    goto L5; // [151] 229
L6: 

    /** 		elsif find(SymTab[left_sym][S_SCOPE], SCOPE_TYPES) then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29536 = (int)*(((s1_ptr)_2)->base + _41left_sym_55163);
    _2 = (int)SEQ_PTR(_29536);
    _29537 = (int)*(((s1_ptr)_2)->base + 4);
    _29536 = NOVALUE;
    _29538 = find_from(_29537, _41SCOPE_TYPES_55113, 1);
    _29537 = NOVALUE;
    if (_29538 == 0)
    {
        _29538 = NOVALUE;
        goto L7; // [177] 228
    }
    else{
        _29538 = NOVALUE;
    }

    /** 			SymTab[CurrentSub][S_EFFECT] = or_bits(SymTab[CurrentSub][S_EFFECT],*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38CurrentSub_16954 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29541 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_29541);
    _29542 = (int)*(((s1_ptr)_2)->base + 23);
    _29541 = NOVALUE;
    _29543 = (_41left_sym_55163 % 29);
    _29544 = power(2, _29543);
    _29543 = NOVALUE;
    if (IS_ATOM_INT(_29542) && IS_ATOM_INT(_29544)) {
        {unsigned long tu;
             tu = (unsigned long)_29542 | (unsigned long)_29544;
             _29545 = MAKE_UINT(tu);
        }
    }
    else {
        _29545 = binary_op(OR_BITS, _29542, _29544);
    }
    _29542 = NOVALUE;
    DeRef(_29544);
    _29544 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 23);
    _1 = *(int *)_2;
    *(int *)_2 = _29545;
    if( _1 != _29545 ){
        DeRef(_1);
    }
    _29545 = NOVALUE;
    _29539 = NOVALUE;
L7: 
L5: 

    /** 		SymTab[left_sym][S_USAGE] = or_bits(SymTab[left_sym][S_USAGE], U_WRITTEN)*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_41left_sym_55163 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29548 = (int)*(((s1_ptr)_2)->base + _41left_sym_55163);
    _2 = (int)SEQ_PTR(_29548);
    _29549 = (int)*(((s1_ptr)_2)->base + 5);
    _29548 = NOVALUE;
    if (IS_ATOM_INT(_29549)) {
        {unsigned long tu;
             tu = (unsigned long)_29549 | (unsigned long)2;
             _29550 = MAKE_UINT(tu);
        }
    }
    else {
        _29550 = binary_op(OR_BITS, _29549, 2);
    }
    _29549 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _29550;
    if( _1 != _29550 ){
        DeRef(_1);
    }
    _29550 = NOVALUE;
    _29546 = NOVALUE;
L2: 

    /** 	tok = next_token()*/
    _0 = _tok_57431;
    _tok_57431 = _41next_token();
    DeRef(_0);

    /** 	subs = 0*/
    _subs_57432 = 0;

    /** 	slice = FALSE*/
    _slice_57433 = _9FALSE_426;

    /** 	dangerous = FALSE*/
    _dangerous_57437 = _9FALSE_426;

    /** 	side_effect_calls = 0*/
    _41side_effect_calls_55159 = 0;

    /** 	emit_opnd(left_sym)*/
    _43emit_opnd(_41left_sym_55163);

    /** 	current_sequence = append(current_sequence, left_sym)*/
    Append(&_43current_sequence_51226, _43current_sequence_51226, _41left_sym_55163);

    /** 	while tok[T_ID] = LEFT_SQUARE do*/
L8: 
    _2 = (int)SEQ_PTR(_tok_57431);
    _29553 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29553, -28)){
        _29553 = NOVALUE;
        goto L9; // [330] 522
    }
    _29553 = NOVALUE;

    /** 		subs_depth += 1*/
    _41subs_depth_55164 = _41subs_depth_55164 + 1;

    /** 		if lhs_ptr then*/
    if (_43lhs_ptr_51228 == 0)
    {
        goto LA; // [346] 404
    }
    else{
    }

    /** 			current_sequence = head( current_sequence, length( current_sequence ) - 1 )*/
    if (IS_SEQUENCE(_43current_sequence_51226)){
            _29556 = SEQ_PTR(_43current_sequence_51226)->length;
    }
    else {
        _29556 = 1;
    }
    _29557 = _29556 - 1;
    _29556 = NOVALUE;
    {
        int len = SEQ_PTR(_43current_sequence_51226)->length;
        int size = (IS_ATOM_INT(_29557)) ? _29557 : (object)(DBL_PTR(_29557)->dbl);
        if (size <= 0){
            DeRef( _43current_sequence_51226 );
            _43current_sequence_51226 = MAKE_SEQ(NewS1(0));
        }
        else if (len <= size) {
            RefDS(_43current_sequence_51226);
            DeRef(_43current_sequence_51226);
            _43current_sequence_51226 = _43current_sequence_51226;
        }
        else{
            Head(SEQ_PTR(_43current_sequence_51226),size+1,&_43current_sequence_51226);
        }
    }
    _29557 = NOVALUE;

    /** 			if subs = 1 then*/
    if (_subs_57432 != 1)
    goto LB; // [370] 395

    /** 				subs1_patch = length(Code)+1*/
    if (IS_SEQUENCE(_38Code_17038)){
            _29560 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29560 = 1;
    }
    _subs1_patch_57435 = _29560 + 1;
    _29560 = NOVALUE;

    /** 				emit_op(LHS_SUBS1) -- creates new current_sequence*/
    _43emit_op(161);
    goto LC; // [392] 403
LB: 

    /** 				emit_op(LHS_SUBS) -- adds to current_sequence*/
    _43emit_op(95);
LC: 
LA: 

    /** 		subs += 1*/
    _subs_57432 = _subs_57432 + 1;

    /** 		if subs = 1 then*/
    if (_subs_57432 != 1)
    goto LD; // [412] 427

    /** 			InitCheck(left_sym, TRUE)*/
    _41InitCheck(_41left_sym_55163, _9TRUE_428);
LD: 

    /** 		Expr()*/
    _41Expr();

    /** 		tok = next_token()*/
    _0 = _tok_57431;
    _tok_57431 = _41next_token();
    DeRef(_0);

    /** 		if tok[T_ID] = SLICE then*/
    _2 = (int)SEQ_PTR(_tok_57431);
    _29565 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29565, 513)){
        _29565 = NOVALUE;
        goto LE; // [446] 483
    }
    _29565 = NOVALUE;

    /** 			Expr()*/
    _41Expr();

    /** 			slice = TRUE*/
    _slice_57433 = _9TRUE_428;

    /** 			tok_match(RIGHT_SQUARE)*/
    _41tok_match(-29, 0);

    /** 			tok = next_token()*/
    _0 = _tok_57431;
    _tok_57431 = _41next_token();
    DeRef(_0);

    /** 			exit  -- no further subs or slices allowed*/
    goto L9; // [478] 522
    goto LF; // [480] 505
LE: 

    /** 			putback(tok)*/
    Ref(_tok_57431);
    _41putback(_tok_57431);

    /** 			tok_match(RIGHT_SQUARE)*/
    _41tok_match(-29, 0);

    /** 			subs_depth -= 1*/
    _41subs_depth_55164 = _41subs_depth_55164 - 1;
LF: 

    /** 		tok = next_token()*/
    _0 = _tok_57431;
    _tok_57431 = _41next_token();
    DeRef(_0);

    /** 		lhs_ptr = TRUE*/
    _43lhs_ptr_51228 = _9TRUE_428;

    /** 	end while*/
    goto L8; // [519] 322
L9: 

    /** 	lhs_ptr = FALSE*/
    _43lhs_ptr_51228 = _9FALSE_426;

    /** 	assign_op = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_57431);
    _assign_op_57434 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_assign_op_57434)){
        _assign_op_57434 = (long)DBL_PTR(_assign_op_57434)->dbl;
    }

    /** 	if not find(assign_op, ASSIGN_OPS) then*/
    _29571 = find_from(_assign_op_57434, _41ASSIGN_OPS_55105, 1);
    if (_29571 != 0)
    goto L10; // [548] 608
    _29571 = NOVALUE;

    /** 		sequence lname = SymTab[left_var[T_SYM]][S_NAME]*/
    _2 = (int)SEQ_PTR(_left_var_57429);
    _29573 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_29573)){
        _29574 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29573)->dbl));
    }
    else{
        _29574 = (int)*(((s1_ptr)_2)->base + _29573);
    }
    DeRef(_lname_57561);
    _2 = (int)SEQ_PTR(_29574);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _lname_57561 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _lname_57561 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    Ref(_lname_57561);
    _29574 = NOVALUE;

    /** 		if assign_op = COLON then*/
    if (_assign_op_57434 != -23)
    goto L11; // [577] 595

    /** 			CompileErr(133, {lname})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_lname_57561);
    *((int *)(_2+4)) = _lname_57561;
    _29577 = MAKE_SEQ(_1);
    _46CompileErr(133, _29577, 0);
    _29577 = NOVALUE;
    goto L12; // [592] 607
L11: 

    /** 			CompileErr(76, {lname})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_lname_57561);
    *((int *)(_2+4)) = _lname_57561;
    _29578 = MAKE_SEQ(_1);
    _46CompileErr(76, _29578, 0);
    _29578 = NOVALUE;
L12: 
L10: 
    DeRef(_lname_57561);
    _lname_57561 = NOVALUE;

    /** 	if subs = 0 then*/
    if (_subs_57432 != 0)
    goto L13; // [612] 740

    /** 		integer temp_len = length(Code)*/
    if (IS_SEQUENCE(_38Code_17038)){
            _temp_len_57578 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _temp_len_57578 = 1;
    }

    /** 		if assign_op = EQUALS then*/
    if (_assign_op_57434 != 3)
    goto L14; // [627] 648

    /** 			Expr() -- RHS expression*/
    _41Expr();

    /** 			InitCheck(left_sym, FALSE)*/
    _41InitCheck(_41left_sym_55163, _9FALSE_426);
    goto L15; // [645] 721
L14: 

    /** 			InitCheck(left_sym, TRUE)*/
    _41InitCheck(_41left_sym_55163, _9TRUE_428);

    /** 			if left_sym > 0 then*/
    if (_41left_sym_55163 <= 0)
    goto L16; // [662] 704

    /** 				SymTab[left_sym][S_USAGE] = or_bits(SymTab[left_sym][S_USAGE], U_READ)*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_41left_sym_55163 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29585 = (int)*(((s1_ptr)_2)->base + _41left_sym_55163);
    _2 = (int)SEQ_PTR(_29585);
    _29586 = (int)*(((s1_ptr)_2)->base + 5);
    _29585 = NOVALUE;
    if (IS_ATOM_INT(_29586)) {
        {unsigned long tu;
             tu = (unsigned long)_29586 | (unsigned long)1;
             _29587 = MAKE_UINT(tu);
        }
    }
    else {
        _29587 = binary_op(OR_BITS, _29586, 1);
    }
    _29586 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _29587;
    if( _1 != _29587 ){
        DeRef(_1);
    }
    _29587 = NOVALUE;
    _29583 = NOVALUE;
L16: 

    /** 			emit_opnd(left_sym)*/
    _43emit_opnd(_41left_sym_55163);

    /** 			Expr() -- RHS expression*/
    _41Expr();

    /** 			emit_assign_op(assign_op)*/
    _43emit_assign_op(_assign_op_57434);
L15: 

    /** 		emit_op(ASSIGN)*/
    _43emit_op(18);

    /** 		TypeCheck(left_sym)*/
    _41TypeCheck(_41left_sym_55163);
    goto L17; // [737] 1164
L13: 

    /** 		factors = 0*/
    _41factors_55160 = 0;

    /** 		lhs_subs_level = -1*/
    _41lhs_subs_level_55161 = -1;

    /** 		Expr() -- RHS expression*/
    _41Expr();

    /** 		if subs > 1 then*/
    if (_subs_57432 <= 1)
    goto L18; // [756] 895

    /** 			if left_sym < 0 or SymTab[left_sym][S_SCOPE] != SC_PRIVATE and*/
    _29589 = (_41left_sym_55163 < 0);
    if (_29589 != 0) {
        _29590 = 1;
        goto L19; // [768] 796
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29591 = (int)*(((s1_ptr)_2)->base + _41left_sym_55163);
    _2 = (int)SEQ_PTR(_29591);
    _29592 = (int)*(((s1_ptr)_2)->base + 4);
    _29591 = NOVALUE;
    if (IS_ATOM_INT(_29592)) {
        _29593 = (_29592 != 3);
    }
    else {
        _29593 = binary_op(NOTEQ, _29592, 3);
    }
    _29592 = NOVALUE;
    if (IS_ATOM_INT(_29593))
    _29590 = (_29593 != 0);
    else
    _29590 = DBL_PTR(_29593)->dbl != 0.0;
L19: 
    if (_29590 == 0) {
        goto L1A; // [796] 830
    }
    _29595 = (_41left_sym_55163 % 29);
    _29596 = power(2, _29595);
    _29595 = NOVALUE;
    if (IS_ATOM_INT(_29596)) {
        {unsigned long tu;
             tu = (unsigned long)_41side_effect_calls_55159 & (unsigned long)_29596;
             _29597 = MAKE_UINT(tu);
        }
    }
    else {
        temp_d.dbl = (double)_41side_effect_calls_55159;
        _29597 = Dand_bits(&temp_d, DBL_PTR(_29596));
    }
    DeRef(_29596);
    _29596 = NOVALUE;
    if (_29597 == 0) {
        DeRef(_29597);
        _29597 = NOVALUE;
        goto L1A; // [819] 830
    }
    else {
        if (!IS_ATOM_INT(_29597) && DBL_PTR(_29597)->dbl == 0.0){
            DeRef(_29597);
            _29597 = NOVALUE;
            goto L1A; // [819] 830
        }
        DeRef(_29597);
        _29597 = NOVALUE;
    }
    DeRef(_29597);
    _29597 = NOVALUE;

    /** 				dangerous = TRUE*/
    _dangerous_57437 = _9TRUE_428;
L1A: 

    /** 			if factors = 1 and*/
    _29598 = (_41factors_55160 == 1);
    if (_29598 == 0) {
        _29599 = 0;
        goto L1B; // [838] 852
    }
    _29600 = (_41lhs_subs_level_55161 >= 0);
    _29599 = (_29600 != 0);
L1B: 
    if (_29599 == 0) {
        goto L1C; // [852] 878
    }
    _29602 = _subs_57432 + _slice_57433;
    if ((long)((unsigned long)_29602 + (unsigned long)HIGH_BITS) >= 0) 
    _29602 = NewDouble((double)_29602);
    if (IS_ATOM_INT(_29602)) {
        _29603 = (_41lhs_subs_level_55161 < _29602);
    }
    else {
        _29603 = ((double)_41lhs_subs_level_55161 < DBL_PTR(_29602)->dbl);
    }
    DeRef(_29602);
    _29602 = NOVALUE;
    if (_29603 == 0)
    {
        DeRef(_29603);
        _29603 = NOVALUE;
        goto L1C; // [867] 878
    }
    else{
        DeRef(_29603);
        _29603 = NOVALUE;
    }

    /** 				dangerous = TRUE*/
    _dangerous_57437 = _9TRUE_428;
L1C: 

    /** 			if dangerous then*/
    if (_dangerous_57437 == 0)
    {
        goto L1D; // [880] 894
    }
    else{
    }

    /** 				backpatch(subs1_patch, LHS_SUBS1_COPY)*/
    _43backpatch(_subs1_patch_57435, 166);
L1D: 
L18: 

    /** 		if slice then*/
    if (_slice_57433 == 0)
    {
        goto L1E; // [897] 965
    }
    else{
    }

    /** 			if assign_op != EQUALS then*/
    if (_assign_op_57434 == 3)
    goto L1F; // [904] 938

    /** 				if subs = 1 then*/
    if (_subs_57432 != 1)
    goto L20; // [910] 924

    /** 					emit_op(ASSIGN_OP_SLICE)*/
    _43emit_op(150);
    goto L21; // [921] 932
L20: 

    /** 					emit_op(PASSIGN_OP_SLICE)*/
    _43emit_op(165);
L21: 

    /** 				emit_assign_op(assign_op)*/
    _43emit_assign_op(_assign_op_57434);
L1F: 

    /** 			if subs = 1 then*/
    if (_subs_57432 != 1)
    goto L22; // [940] 954

    /** 				emit_op(ASSIGN_SLICE)*/
    _43emit_op(45);
    goto L23; // [951] 1055
L22: 

    /** 				emit_op(PASSIGN_SLICE)*/
    _43emit_op(163);
    goto L23; // [962] 1055
L1E: 

    /** 			if assign_op = EQUALS then*/
    if (_assign_op_57434 != 3)
    goto L24; // [969] 1000

    /** 				if subs = 1 then*/
    if (_subs_57432 != 1)
    goto L25; // [975] 989

    /** 					emit_op(ASSIGN_SUBS)*/
    _43emit_op(16);
    goto L26; // [986] 1054
L25: 

    /** 					emit_op(PASSIGN_SUBS)*/
    _43emit_op(162);
    goto L26; // [997] 1054
L24: 

    /** 				if subs = 1 then*/
    if (_subs_57432 != 1)
    goto L27; // [1002] 1016

    /** 					emit_op(ASSIGN_OP_SUBS)*/
    _43emit_op(149);
    goto L28; // [1013] 1024
L27: 

    /** 					emit_op(PASSIGN_OP_SUBS)*/
    _43emit_op(164);
L28: 

    /** 				emit_assign_op(assign_op)*/
    _43emit_assign_op(_assign_op_57434);

    /** 				if subs = 1 then*/
    if (_subs_57432 != 1)
    goto L29; // [1031] 1045

    /** 					emit_op(ASSIGN_SUBS2)*/
    _43emit_op(148);
    goto L2A; // [1042] 1053
L29: 

    /** 					emit_op(PASSIGN_SUBS)*/
    _43emit_op(162);
L2A: 
L26: 
L23: 

    /** 		if subs > 1 then*/
    if (_subs_57432 <= 1)
    goto L2B; // [1057] 1109

    /** 			if dangerous then*/
    if (_dangerous_57437 == 0)
    {
        goto L2C; // [1063] 1100
    }
    else{
    }

    /** 				emit_opnd(left_sym)*/
    _43emit_opnd(_41left_sym_55163);

    /** 				emit_opnd(lhs_subs1_copy_temp) -- will be freed*/
    _43emit_opnd(_43lhs_subs1_copy_temp_51231);

    /** 				emit_temp( lhs_subs1_copy_temp, NEW_REFERENCE )*/
    _43emit_temp(_43lhs_subs1_copy_temp_51231, 1);

    /** 				emit_op(ASSIGN)*/
    _43emit_op(18);
    goto L2D; // [1097] 1108
L2C: 

    /** 				TempFree(lhs_subs1_copy_temp)*/
    _43TempFree(_43lhs_subs1_copy_temp_51231);
L2D: 
L2B: 

    /** 		if OpTypeCheck and (left_sym < 0 or SymTab[left_sym][S_VTYPE] != sequence_type) then*/
    if (_38OpTypeCheck_17016 == 0) {
        goto L2E; // [1113] 1163
    }
    _29613 = (_41left_sym_55163 < 0);
    if (_29613 != 0) {
        DeRef(_29614);
        _29614 = 1;
        goto L2F; // [1123] 1151
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29615 = (int)*(((s1_ptr)_2)->base + _41left_sym_55163);
    _2 = (int)SEQ_PTR(_29615);
    _29616 = (int)*(((s1_ptr)_2)->base + 15);
    _29615 = NOVALUE;
    if (IS_ATOM_INT(_29616)) {
        _29617 = (_29616 != _55sequence_type_47059);
    }
    else {
        _29617 = binary_op(NOTEQ, _29616, _55sequence_type_47059);
    }
    _29616 = NOVALUE;
    if (IS_ATOM_INT(_29617))
    _29614 = (_29617 != 0);
    else
    _29614 = DBL_PTR(_29617)->dbl != 0.0;
L2F: 
    if (_29614 == 0)
    {
        _29614 = NOVALUE;
        goto L2E; // [1152] 1163
    }
    else{
        _29614 = NOVALUE;
    }

    /** 			TypeCheck(left_sym)*/
    _41TypeCheck(_41left_sym_55163);
L2E: 
L17: 

    /** 	current_sequence = head( current_sequence, length( current_sequence ) - 1 )*/
    if (IS_SEQUENCE(_43current_sequence_51226)){
            _29618 = SEQ_PTR(_43current_sequence_51226)->length;
    }
    else {
        _29618 = 1;
    }
    _29619 = _29618 - 1;
    _29618 = NOVALUE;
    {
        int len = SEQ_PTR(_43current_sequence_51226)->length;
        int size = (IS_ATOM_INT(_29619)) ? _29619 : (object)(DBL_PTR(_29619)->dbl);
        if (size <= 0){
            DeRef( _43current_sequence_51226 );
            _43current_sequence_51226 = MAKE_SEQ(NewS1(0));
        }
        else if (len <= size) {
            RefDS(_43current_sequence_51226);
            DeRef(_43current_sequence_51226);
            _43current_sequence_51226 = _43current_sequence_51226;
        }
        else{
            Head(SEQ_PTR(_43current_sequence_51226),size+1,&_43current_sequence_51226);
        }
    }
    _29619 = NOVALUE;

    /** 	if not TRANSLATE then*/
    if (_38TRANSLATE_16564 != 0)
    goto L30; // [1187] 1213

    /** 		if OpTrace then*/
    if (_38OpTrace_17015 == 0)
    {
        goto L31; // [1194] 1212
    }
    else{
    }

    /** 			emit_op(DISPLAY_VAR)*/
    _43emit_op(87);

    /** 			emit_addr(left_sym)*/
    _43emit_addr(_41left_sym_55163);
L31: 
L30: 

    /** end procedure*/
    DeRef(_left_var_57429);
    DeRef(_tok_57431);
    _29573 = NOVALUE;
    DeRef(_29528);
    _29528 = NOVALUE;
    DeRef(_29589);
    _29589 = NOVALUE;
    DeRef(_29598);
    _29598 = NOVALUE;
    DeRef(_29593);
    _29593 = NOVALUE;
    DeRef(_29600);
    _29600 = NOVALUE;
    DeRef(_29613);
    _29613 = NOVALUE;
    DeRef(_29617);
    _29617 = NOVALUE;
    return;
    ;
}


void _41Return_statement()
{
    int _tok_57720 = NOVALUE;
    int _pop_57721 = NOVALUE;
    int _last_op_57727 = NOVALUE;
    int _last_pc_57730 = NOVALUE;
    int _is_tail_57733 = NOVALUE;
    int _29653 = NOVALUE;
    int _29651 = NOVALUE;
    int _29650 = NOVALUE;
    int _29649 = NOVALUE;
    int _29648 = NOVALUE;
    int _29646 = NOVALUE;
    int _29645 = NOVALUE;
    int _29644 = NOVALUE;
    int _29643 = NOVALUE;
    int _29642 = NOVALUE;
    int _29641 = NOVALUE;
    int _29640 = NOVALUE;
    int _29639 = NOVALUE;
    int _29635 = NOVALUE;
    int _29634 = NOVALUE;
    int _29632 = NOVALUE;
    int _29631 = NOVALUE;
    int _29630 = NOVALUE;
    int _29629 = NOVALUE;
    int _29628 = NOVALUE;
    int _29627 = NOVALUE;
    int _29626 = NOVALUE;
    int _29625 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer pop*/

    /** 	if CurrentSub = TopLevelSub then*/
    if (_38CurrentSub_16954 != _38TopLevelSub_16953)
    goto L1; // [9] 21

    /** 		CompileErr(130)*/
    RefDS(_22663);
    _46CompileErr(130, _22663, 0);
L1: 

    /** 	integer*/

    /** 		last_op = Last_op(),*/
    _last_op_57727 = _43Last_op();
    if (!IS_ATOM_INT(_last_op_57727)) {
        _1 = (long)(DBL_PTR(_last_op_57727)->dbl);
        if (UNIQUE(DBL_PTR(_last_op_57727)) && (DBL_PTR(_last_op_57727)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_last_op_57727);
        _last_op_57727 = _1;
    }

    /** 		last_pc = Last_pc(),*/
    _last_pc_57730 = _43Last_pc();
    if (!IS_ATOM_INT(_last_pc_57730)) {
        _1 = (long)(DBL_PTR(_last_pc_57730)->dbl);
        if (UNIQUE(DBL_PTR(_last_pc_57730)) && (DBL_PTR(_last_pc_57730)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_last_pc_57730);
        _last_pc_57730 = _1;
    }

    /** 		is_tail = 0*/
    _is_tail_57733 = 0;

    /** 	if last_op = PROC and length(Code) > last_pc and Code[last_pc+1] = CurrentSub then*/
    _29625 = (_last_op_57727 == 27);
    if (_29625 == 0) {
        _29626 = 0;
        goto L2; // [50] 67
    }
    if (IS_SEQUENCE(_38Code_17038)){
            _29627 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29627 = 1;
    }
    _29628 = (_29627 > _last_pc_57730);
    _29627 = NOVALUE;
    _29626 = (_29628 != 0);
L2: 
    if (_29626 == 0) {
        goto L3; // [67] 97
    }
    _29630 = _last_pc_57730 + 1;
    _2 = (int)SEQ_PTR(_38Code_17038);
    _29631 = (int)*(((s1_ptr)_2)->base + _29630);
    if (IS_ATOM_INT(_29631)) {
        _29632 = (_29631 == _38CurrentSub_16954);
    }
    else {
        _29632 = binary_op(EQUALS, _29631, _38CurrentSub_16954);
    }
    _29631 = NOVALUE;
    if (_29632 == 0) {
        DeRef(_29632);
        _29632 = NOVALUE;
        goto L3; // [88] 97
    }
    else {
        if (!IS_ATOM_INT(_29632) && DBL_PTR(_29632)->dbl == 0.0){
            DeRef(_29632);
            _29632 = NOVALUE;
            goto L3; // [88] 97
        }
        DeRef(_29632);
        _29632 = NOVALUE;
    }
    DeRef(_29632);
    _29632 = NOVALUE;

    /** 		is_tail = 1*/
    _is_tail_57733 = 1;
L3: 

    /** 	if not TRANSLATE then*/
    if (_38TRANSLATE_16564 != 0)
    goto L4; // [101] 127

    /** 		if OpTrace then*/
    if (_38OpTrace_17015 == 0)
    {
        goto L5; // [108] 126
    }
    else{
    }

    /** 			emit_op(ERASE_PRIVATE_NAMES)*/
    _43emit_op(88);

    /** 			emit_addr(CurrentSub)*/
    _43emit_addr(_38CurrentSub_16954);
L5: 
L4: 

    /** 	if SymTab[CurrentSub][S_TOKEN] != PROC then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _29634 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_29634);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _29635 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _29635 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _29634 = NOVALUE;
    if (binary_op_a(EQUALS, _29635, 27)){
        _29635 = NOVALUE;
        goto L6; // [145] 271
    }
    _29635 = NOVALUE;

    /** 		Expr()*/
    _41Expr();

    /** 		last_op = Last_op()*/
    _last_op_57727 = _43Last_op();
    if (!IS_ATOM_INT(_last_op_57727)) {
        _1 = (long)(DBL_PTR(_last_op_57727)->dbl);
        if (UNIQUE(DBL_PTR(_last_op_57727)) && (DBL_PTR(_last_op_57727)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_last_op_57727);
        _last_op_57727 = _1;
    }

    /** 		last_pc = Last_pc()*/
    _last_pc_57730 = _43Last_pc();
    if (!IS_ATOM_INT(_last_pc_57730)) {
        _1 = (long)(DBL_PTR(_last_pc_57730)->dbl);
        if (UNIQUE(DBL_PTR(_last_pc_57730)) && (DBL_PTR(_last_pc_57730)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_last_pc_57730);
        _last_pc_57730 = _1;
    }

    /** 		if last_op = PROC and length(Code) > last_pc and Code[last_pc+1] = CurrentSub then*/
    _29639 = (_last_op_57727 == 27);
    if (_29639 == 0) {
        _29640 = 0;
        goto L7; // [175] 192
    }
    if (IS_SEQUENCE(_38Code_17038)){
            _29641 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29641 = 1;
    }
    _29642 = (_29641 > _last_pc_57730);
    _29641 = NOVALUE;
    _29640 = (_29642 != 0);
L7: 
    if (_29640 == 0) {
        goto L8; // [192] 251
    }
    _29644 = _last_pc_57730 + 1;
    _2 = (int)SEQ_PTR(_38Code_17038);
    _29645 = (int)*(((s1_ptr)_2)->base + _29644);
    if (IS_ATOM_INT(_29645)) {
        _29646 = (_29645 == _38CurrentSub_16954);
    }
    else {
        _29646 = binary_op(EQUALS, _29645, _38CurrentSub_16954);
    }
    _29645 = NOVALUE;
    if (_29646 == 0) {
        DeRef(_29646);
        _29646 = NOVALUE;
        goto L8; // [213] 251
    }
    else {
        if (!IS_ATOM_INT(_29646) && DBL_PTR(_29646)->dbl == 0.0){
            DeRef(_29646);
            _29646 = NOVALUE;
            goto L8; // [213] 251
        }
        DeRef(_29646);
        _29646 = NOVALUE;
    }
    DeRef(_29646);
    _29646 = NOVALUE;

    /** 			pop = Pop() -- prevent cg_stack (code generation stack) leakage*/
    _pop_57721 = _43Pop();
    if (!IS_ATOM_INT(_pop_57721)) {
        _1 = (long)(DBL_PTR(_pop_57721)->dbl);
        if (UNIQUE(DBL_PTR(_pop_57721)) && (DBL_PTR(_pop_57721)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pop_57721);
        _pop_57721 = _1;
    }

    /** 			Code[Last_pc()] = PROC_TAIL*/
    _29648 = _43Last_pc();
    _2 = (int)SEQ_PTR(_38Code_17038);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _38Code_17038 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_29648))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_29648)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _29648);
    _1 = *(int *)_2;
    *(int *)_2 = 203;
    DeRef(_1);

    /** 			if object(pop_temps()) then end if*/
    _29649 = _43pop_temps();
    if( NOVALUE == _29649 ){
        _29650 = 0;
    }
    else{
        if (IS_ATOM_INT(_29649))
        _29650 = 1;
        else if (IS_ATOM_DBL(_29649)) {
             if (IS_ATOM_INT(DoubleToInt(_29649))) {
                 _29650 = 1;
                 } else {
                     _29650 = 2;
                } } else if (IS_SEQUENCE(_29649))
                _29650 = 3;
                else
                _29650 = 0;
            }
            DeRef(_29649);
            _29649 = NOVALUE;
            if (_29650 == 0)
            {
                _29650 = NOVALUE;
                goto L9; // [244] 298
            }
            else{
                _29650 = NOVALUE;
            }
            goto L9; // [248] 298
L8: 

            /** 			FuncReturn = TRUE*/
            _41FuncReturn_55130 = _9TRUE_428;

            /** 			emit_op(RETURNF)*/
            _43emit_op(28);
            goto L9; // [268] 298
L6: 

            /** 		if is_tail then*/
            if (_is_tail_57733 == 0)
            {
                goto LA; // [273] 290
            }
            else{
            }

            /** 			Code[Last_pc()] = PROC_TAIL*/
            _29651 = _43Last_pc();
            _2 = (int)SEQ_PTR(_38Code_17038);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _38Code_17038 = MAKE_SEQ(_2);
            }
            if (!IS_ATOM_INT(_29651))
            _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_29651)->dbl));
            else
            _2 = (int)(((s1_ptr)_2)->base + _29651);
            _1 = *(int *)_2;
            *(int *)_2 = 203;
            DeRef(_1);
LA: 

            /** 		emit_op(RETURNP)*/
            _43emit_op(29);
L9: 

            /** 	tok = next_token()*/
            _0 = _tok_57720;
            _tok_57720 = _41next_token();
            DeRef(_0);

            /** 	putback(tok)*/
            Ref(_tok_57720);
            _41putback(_tok_57720);

            /** 	NotReached(tok[T_ID], "return")*/
            _2 = (int)SEQ_PTR(_tok_57720);
            _29653 = (int)*(((s1_ptr)_2)->base + 1);
            Ref(_29653);
            RefDS(_27093);
            _41NotReached(_29653, _27093);
            _29653 = NOVALUE;

            /** end procedure*/
            DeRef(_tok_57720);
            DeRef(_29625);
            _29625 = NOVALUE;
            DeRef(_29628);
            _29628 = NOVALUE;
            DeRef(_29630);
            _29630 = NOVALUE;
            DeRef(_29639);
            _29639 = NOVALUE;
            DeRef(_29642);
            _29642 = NOVALUE;
            DeRef(_29644);
            _29644 = NOVALUE;
            DeRef(_29648);
            _29648 = NOVALUE;
            DeRef(_29651);
            _29651 = NOVALUE;
            return;
    ;
}


int _41exit_level(int _tok_57809, int _flag_57810)
{
    int _arg_57811 = NOVALUE;
    int _n_57812 = NOVALUE;
    int _num_labels_57813 = NOVALUE;
    int _negative_57814 = NOVALUE;
    int _labels_57815 = NOVALUE;
    int _29683 = NOVALUE;
    int _29682 = NOVALUE;
    int _29681 = NOVALUE;
    int _29680 = NOVALUE;
    int _29679 = NOVALUE;
    int _29675 = NOVALUE;
    int _29674 = NOVALUE;
    int _29673 = NOVALUE;
    int _29671 = NOVALUE;
    int _29670 = NOVALUE;
    int _29669 = NOVALUE;
    int _29668 = NOVALUE;
    int _29666 = NOVALUE;
    int _29661 = NOVALUE;
    int _29660 = NOVALUE;
    int _29658 = NOVALUE;
    int _29655 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer negative = 0*/
    _negative_57814 = 0;

    /** 	if flag then*/
    if (_flag_57810 == 0)
    {
        goto L1; // [10] 25
    }
    else{
    }

    /** 		labels = if_labels*/
    RefDS(_41if_labels_55151);
    DeRef(_labels_57815);
    _labels_57815 = _41if_labels_55151;
    goto L2; // [22] 35
L1: 

    /** 		labels = loop_labels*/
    RefDS(_41loop_labels_55150);
    DeRef(_labels_57815);
    _labels_57815 = _41loop_labels_55150;
L2: 

    /** 	num_labels = length(labels)*/
    if (IS_SEQUENCE(_labels_57815)){
            _num_labels_57813 = SEQ_PTR(_labels_57815)->length;
    }
    else {
        _num_labels_57813 = 1;
    }

    /** 	if tok[T_ID] = MINUS then*/
    _2 = (int)SEQ_PTR(_tok_57809);
    _29655 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29655, 10)){
        _29655 = NOVALUE;
        goto L3; // [52] 67
    }
    _29655 = NOVALUE;

    /** 		tok = next_token()*/
    _0 = _tok_57809;
    _tok_57809 = _41next_token();
    DeRef(_0);

    /** 		negative = 1*/
    _negative_57814 = 1;
L3: 

    /** 	if tok[T_ID]=ATOM then*/
    _2 = (int)SEQ_PTR(_tok_57809);
    _29658 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29658, 502)){
        _29658 = NOVALUE;
        goto L4; // [77] 178
    }
    _29658 = NOVALUE;

    /** 		arg = SymTab[tok[T_SYM]][S_OBJ]*/
    _2 = (int)SEQ_PTR(_tok_57809);
    _29660 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_29660)){
        _29661 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29660)->dbl));
    }
    else{
        _29661 = (int)*(((s1_ptr)_2)->base + _29660);
    }
    DeRef(_arg_57811);
    _2 = (int)SEQ_PTR(_29661);
    _arg_57811 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_arg_57811);
    _29661 = NOVALUE;

    /** 		n = floor(arg)*/
    if (IS_ATOM_INT(_arg_57811))
    _n_57812 = e_floor(_arg_57811);
    else
    _n_57812 = unary_op(FLOOR, _arg_57811);
    if (!IS_ATOM_INT(_n_57812)) {
        _1 = (long)(DBL_PTR(_n_57812)->dbl);
        if (UNIQUE(DBL_PTR(_n_57812)) && (DBL_PTR(_n_57812)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_n_57812);
        _n_57812 = _1;
    }

    /** 		if negative then*/
    if (_negative_57814 == 0)
    {
        goto L5; // [110] 122
    }
    else{
    }

    /** 			n = num_labels - n*/
    _n_57812 = _num_labels_57813 - _n_57812;
    goto L6; // [119] 135
L5: 

    /** 		elsif n = 0 then*/
    if (_n_57812 != 0)
    goto L7; // [124] 134

    /** 			n = num_labels*/
    _n_57812 = _num_labels_57813;
L7: 
L6: 

    /** 		if n<=0 or n>num_labels then*/
    _29666 = (_n_57812 <= 0);
    if (_29666 != 0) {
        goto L8; // [141] 154
    }
    _29668 = (_n_57812 > _num_labels_57813);
    if (_29668 == 0)
    {
        DeRef(_29668);
        _29668 = NOVALUE;
        goto L9; // [150] 162
    }
    else{
        DeRef(_29668);
        _29668 = NOVALUE;
    }
L8: 

    /** 			CompileErr(87)*/
    RefDS(_22663);
    _46CompileErr(87, _22663, 0);
L9: 

    /** 		return {n, next_token()}*/
    _29669 = _41next_token();
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _n_57812;
    ((int *)_2)[2] = _29669;
    _29670 = MAKE_SEQ(_1);
    _29669 = NOVALUE;
    DeRef(_tok_57809);
    DeRef(_arg_57811);
    DeRef(_labels_57815);
    _29660 = NOVALUE;
    DeRef(_29666);
    _29666 = NOVALUE;
    return _29670;
    goto LA; // [175] 266
L4: 

    /** 	elsif tok[T_ID]=STRING then*/
    _2 = (int)SEQ_PTR(_tok_57809);
    _29671 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29671, 503)){
        _29671 = NOVALUE;
        goto LB; // [188] 255
    }
    _29671 = NOVALUE;

    /** 		n = find(SymTab[tok[T_SYM]][S_OBJ],labels)*/
    _2 = (int)SEQ_PTR(_tok_57809);
    _29673 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_29673)){
        _29674 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29673)->dbl));
    }
    else{
        _29674 = (int)*(((s1_ptr)_2)->base + _29673);
    }
    _2 = (int)SEQ_PTR(_29674);
    _29675 = (int)*(((s1_ptr)_2)->base + 1);
    _29674 = NOVALUE;
    _n_57812 = find_from(_29675, _labels_57815, 1);
    _29675 = NOVALUE;

    /** 		if n = 0 then*/
    if (_n_57812 != 0)
    goto LC; // [219] 231

    /** 			CompileErr(152)*/
    RefDS(_22663);
    _46CompileErr(152, _22663, 0);
LC: 

    /** 		return {num_labels + 1 - n, next_token()}*/
    _29679 = _num_labels_57813 + 1;
    if (_29679 > MAXINT){
        _29679 = NewDouble((double)_29679);
    }
    if (IS_ATOM_INT(_29679)) {
        _29680 = _29679 - _n_57812;
        if ((long)((unsigned long)_29680 +(unsigned long) HIGH_BITS) >= 0){
            _29680 = NewDouble((double)_29680);
        }
    }
    else {
        _29680 = NewDouble(DBL_PTR(_29679)->dbl - (double)_n_57812);
    }
    DeRef(_29679);
    _29679 = NOVALUE;
    _29681 = _41next_token();
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _29680;
    ((int *)_2)[2] = _29681;
    _29682 = MAKE_SEQ(_1);
    _29681 = NOVALUE;
    _29680 = NOVALUE;
    DeRef(_tok_57809);
    DeRef(_arg_57811);
    DeRef(_labels_57815);
    _29660 = NOVALUE;
    DeRef(_29666);
    _29666 = NOVALUE;
    DeRef(_29670);
    _29670 = NOVALUE;
    _29673 = NOVALUE;
    return _29682;
    goto LA; // [252] 266
LB: 

    /** 		return {1, tok} -- no parameters*/
    Ref(_tok_57809);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = _tok_57809;
    _29683 = MAKE_SEQ(_1);
    DeRef(_tok_57809);
    DeRef(_arg_57811);
    DeRef(_labels_57815);
    _29660 = NOVALUE;
    DeRef(_29666);
    _29666 = NOVALUE;
    DeRef(_29670);
    _29670 = NOVALUE;
    _29673 = NOVALUE;
    DeRef(_29682);
    _29682 = NOVALUE;
    return _29683;
LA: 
    ;
}


void _41GLabel_statement()
{
    int _tok_57873 = NOVALUE;
    int _labbel_57874 = NOVALUE;
    int _laddr_57875 = NOVALUE;
    int _n_57876 = NOVALUE;
    int _29702 = NOVALUE;
    int _29700 = NOVALUE;
    int _29699 = NOVALUE;
    int _29698 = NOVALUE;
    int _29697 = NOVALUE;
    int _29695 = NOVALUE;
    int _29692 = NOVALUE;
    int _29690 = NOVALUE;
    int _29688 = NOVALUE;
    int _29687 = NOVALUE;
    int _29685 = NOVALUE;
    int _0, _1, _2;
    

    /** 	object labbel*/

    /** 	object laddr*/

    /** 	integer n*/

    /** 	tok = next_token()*/
    _0 = _tok_57873;
    _tok_57873 = _41next_token();
    DeRef(_0);

    /** 	if tok[T_ID] != STRING then*/
    _2 = (int)SEQ_PTR(_tok_57873);
    _29685 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _29685, 503)){
        _29685 = NOVALUE;
        goto L1; // [22] 34
    }
    _29685 = NOVALUE;

    /** 		CompileErr(35)*/
    RefDS(_22663);
    _46CompileErr(35, _22663, 0);
L1: 

    /** 	labbel = SymTab[tok[T_SYM]][S_OBJ]*/
    _2 = (int)SEQ_PTR(_tok_57873);
    _29687 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_29687)){
        _29688 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29687)->dbl));
    }
    else{
        _29688 = (int)*(((s1_ptr)_2)->base + _29687);
    }
    DeRef(_labbel_57874);
    _2 = (int)SEQ_PTR(_29688);
    _labbel_57874 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_labbel_57874);
    _29688 = NOVALUE;

    /** 	laddr = length(Code) + 1*/
    if (IS_SEQUENCE(_38Code_17038)){
            _29690 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29690 = 1;
    }
    _laddr_57875 = _29690 + 1;
    _29690 = NOVALUE;

    /** 	if find(labbel, goto_labels) then*/
    _29692 = find_from(_labbel_57874, _41goto_labels_55133, 1);
    if (_29692 == 0)
    {
        _29692 = NOVALUE;
        goto L2; // [74] 85
    }
    else{
        _29692 = NOVALUE;
    }

    /** 		CompileErr(59)*/
    RefDS(_22663);
    _46CompileErr(59, _22663, 0);
L2: 

    /** 	goto_labels = append(goto_labels, labbel)*/
    Ref(_labbel_57874);
    Append(&_41goto_labels_55133, _41goto_labels_55133, _labbel_57874);

    /** 	goto_addr = append(goto_addr, laddr)*/
    Append(&_41goto_addr_55134, _41goto_addr_55134, _laddr_57875);

    /** 	label_block = append( label_block, top_block() )*/
    _29695 = _68top_block(0);
    Ref(_29695);
    Append(&_41label_block_55137, _41label_block_55137, _29695);
    DeRef(_29695);
    _29695 = NOVALUE;

    /** 	while n with entry do*/
    goto L3; // [115] 174
L4: 
    if (_n_57876 == 0)
    {
        goto L5; // [120] 188
    }
    else{
    }

    /** 		backpatch(goto_list[n], laddr)*/
    _2 = (int)SEQ_PTR(_38goto_list_17077);
    _29697 = (int)*(((s1_ptr)_2)->base + _n_57876);
    Ref(_29697);
    _43backpatch(_29697, _laddr_57875);
    _29697 = NOVALUE;

    /** 		set_glabel_block( goto_ref[n], top_block() )*/
    _2 = (int)SEQ_PTR(_41goto_ref_55136);
    _29698 = (int)*(((s1_ptr)_2)->base + _n_57876);
    _29699 = _68top_block(0);
    Ref(_29698);
    _40set_glabel_block(_29698, _29699);
    _29698 = NOVALUE;
    _29699 = NOVALUE;

    /** 		goto_delay[n] = "" --clear it*/
    RefDS(_22663);
    _2 = (int)SEQ_PTR(_38goto_delay_17076);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _38goto_delay_17076 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _n_57876);
    _1 = *(int *)_2;
    *(int *)_2 = _22663;
    DeRef(_1);

    /** 		goto_line[n] = {-1,""} --clear it*/
    RefDS(_22663);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = _22663;
    _29700 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_41goto_line_55132);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _41goto_line_55132 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _n_57876);
    _1 = *(int *)_2;
    *(int *)_2 = _29700;
    if( _1 != _29700 ){
        DeRef(_1);
    }
    _29700 = NOVALUE;

    /** 	entry*/
L3: 

    /** 		n = find(labbel, goto_delay)*/
    _n_57876 = find_from(_labbel_57874, _38goto_delay_17076, 1);

    /** 	end while*/
    goto L4; // [185] 118
L5: 

    /** 	force_uninitialize( map:get( goto_init, labbel, {} ) )*/
    Ref(_41goto_init_55139);
    Ref(_labbel_57874);
    RefDS(_22663);
    _29702 = _29get(_41goto_init_55139, _labbel_57874, _22663);
    _41force_uninitialize(_29702);
    _29702 = NOVALUE;

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L6; // [205] 221
    }
    else{
    }

    /** 		emit_op(GLABEL)*/
    _43emit_op(189);

    /** 		emit_addr(laddr)*/
    _43emit_addr(_laddr_57875);
L6: 

    /** end procedure*/
    DeRef(_tok_57873);
    DeRef(_labbel_57874);
    _29687 = NOVALUE;
    return;
    ;
}


void _41Goto_statement()
{
    int _tok_57923 = NOVALUE;
    int _n_57924 = NOVALUE;
    int _num_labels_57925 = NOVALUE;
    int _32355 = NOVALUE;
    int _29741 = NOVALUE;
    int _29740 = NOVALUE;
    int _29737 = NOVALUE;
    int _29736 = NOVALUE;
    int _29735 = NOVALUE;
    int _29734 = NOVALUE;
    int _29733 = NOVALUE;
    int _29732 = NOVALUE;
    int _29731 = NOVALUE;
    int _29730 = NOVALUE;
    int _29729 = NOVALUE;
    int _29728 = NOVALUE;
    int _29727 = NOVALUE;
    int _29726 = NOVALUE;
    int _29724 = NOVALUE;
    int _29723 = NOVALUE;
    int _29721 = NOVALUE;
    int _29720 = NOVALUE;
    int _29718 = NOVALUE;
    int _29717 = NOVALUE;
    int _29715 = NOVALUE;
    int _29714 = NOVALUE;
    int _29713 = NOVALUE;
    int _29712 = NOVALUE;
    int _29709 = NOVALUE;
    int _29708 = NOVALUE;
    int _29707 = NOVALUE;
    int _29705 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer n*/

    /** 	integer num_labels*/

    /** 	tok = next_token()*/
    _0 = _tok_57923;
    _tok_57923 = _41next_token();
    DeRef(_0);

    /** 	num_labels = length(goto_labels)*/
    if (IS_SEQUENCE(_41goto_labels_55133)){
            _num_labels_57925 = SEQ_PTR(_41goto_labels_55133)->length;
    }
    else {
        _num_labels_57925 = 1;
    }

    /** 	if tok[T_ID]=STRING then*/
    _2 = (int)SEQ_PTR(_tok_57923);
    _29705 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29705, 503)){
        _29705 = NOVALUE;
        goto L1; // [27] 271
    }
    _29705 = NOVALUE;

    /** 		n = find(SymTab[tok[T_SYM]][S_OBJ],goto_labels)*/
    _2 = (int)SEQ_PTR(_tok_57923);
    _29707 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_29707)){
        _29708 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29707)->dbl));
    }
    else{
        _29708 = (int)*(((s1_ptr)_2)->base + _29707);
    }
    _2 = (int)SEQ_PTR(_29708);
    _29709 = (int)*(((s1_ptr)_2)->base + 1);
    _29708 = NOVALUE;
    _n_57924 = find_from(_29709, _41goto_labels_55133, 1);
    _29709 = NOVALUE;

    /** 		if n = 0 then*/
    if (_n_57924 != 0)
    goto L2; // [60] 245

    /** 			goto_delay &= {SymTab[tok[T_SYM]][S_OBJ]}*/
    _2 = (int)SEQ_PTR(_tok_57923);
    _29712 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_29712)){
        _29713 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29712)->dbl));
    }
    else{
        _29713 = (int)*(((s1_ptr)_2)->base + _29712);
    }
    _2 = (int)SEQ_PTR(_29713);
    _29714 = (int)*(((s1_ptr)_2)->base + 1);
    _29713 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_29714);
    *((int *)(_2+4)) = _29714;
    _29715 = MAKE_SEQ(_1);
    _29714 = NOVALUE;
    Concat((object_ptr)&_38goto_delay_17076, _38goto_delay_17076, _29715);
    DeRefDS(_29715);
    _29715 = NOVALUE;

    /** 			goto_list &= length(Code)+2 --not 1???*/
    if (IS_SEQUENCE(_38Code_17038)){
            _29717 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29717 = 1;
    }
    _29718 = _29717 + 2;
    _29717 = NOVALUE;
    Append(&_38goto_list_17077, _38goto_list_17077, _29718);
    _29718 = NOVALUE;

    /** 			goto_line &= {{line_number,ThisLine}}*/
    Ref(_46ThisLine_49482);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _38line_number_16947;
    ((int *)_2)[2] = _46ThisLine_49482;
    _29720 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _29720;
    _29721 = MAKE_SEQ(_1);
    _29720 = NOVALUE;
    Concat((object_ptr)&_41goto_line_55132, _41goto_line_55132, _29721);
    DeRefDS(_29721);
    _29721 = NOVALUE;

    /** 			goto_ref &= new_forward_reference( GOTO, top_block() )*/
    _29723 = _68top_block(0);
    _32355 = 188;
    _29724 = _40new_forward_reference(188, _29723, 188);
    _29723 = NOVALUE;
    _32355 = NOVALUE;
    if (IS_SEQUENCE(_41goto_ref_55136) && IS_ATOM(_29724)) {
        Ref(_29724);
        Append(&_41goto_ref_55136, _41goto_ref_55136, _29724);
    }
    else if (IS_ATOM(_41goto_ref_55136) && IS_SEQUENCE(_29724)) {
    }
    else {
        Concat((object_ptr)&_41goto_ref_55136, _41goto_ref_55136, _29724);
    }
    DeRef(_29724);
    _29724 = NOVALUE;

    /** 			map:put( goto_init, SymTab[tok[T_SYM]][S_OBJ], get_private_uninitialized() )*/
    _2 = (int)SEQ_PTR(_tok_57923);
    _29726 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_29726)){
        _29727 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29726)->dbl));
    }
    else{
        _29727 = (int)*(((s1_ptr)_2)->base + _29726);
    }
    _2 = (int)SEQ_PTR(_29727);
    _29728 = (int)*(((s1_ptr)_2)->base + 1);
    _29727 = NOVALUE;
    _29729 = _41get_private_uninitialized();
    Ref(_41goto_init_55139);
    Ref(_29728);
    _29put(_41goto_init_55139, _29728, _29729, 1, 23);
    _29728 = NOVALUE;
    _29729 = NOVALUE;

    /** 			add_data( goto_ref[$], sym_obj( tok[T_SYM] ) )*/
    if (IS_SEQUENCE(_41goto_ref_55136)){
            _29730 = SEQ_PTR(_41goto_ref_55136)->length;
    }
    else {
        _29730 = 1;
    }
    _2 = (int)SEQ_PTR(_41goto_ref_55136);
    _29731 = (int)*(((s1_ptr)_2)->base + _29730);
    _2 = (int)SEQ_PTR(_tok_57923);
    _29732 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_29732);
    _29733 = _55sym_obj(_29732);
    _29732 = NOVALUE;
    Ref(_29731);
    _40add_data(_29731, _29733);
    _29731 = NOVALUE;
    _29733 = NOVALUE;

    /** 			set_line( goto_ref[$], line_number, ThisLine, bp )*/
    if (IS_SEQUENCE(_41goto_ref_55136)){
            _29734 = SEQ_PTR(_41goto_ref_55136)->length;
    }
    else {
        _29734 = 1;
    }
    _2 = (int)SEQ_PTR(_41goto_ref_55136);
    _29735 = (int)*(((s1_ptr)_2)->base + _29734);
    Ref(_29735);
    Ref(_46ThisLine_49482);
    _40set_line(_29735, _38line_number_16947, _46ThisLine_49482, _46bp_49486);
    _29735 = NOVALUE;
    goto L3; // [242] 263
L2: 

    /** 			Goto_block( top_block(), label_block[n] )*/
    _29736 = _68top_block(0);
    _2 = (int)SEQ_PTR(_41label_block_55137);
    _29737 = (int)*(((s1_ptr)_2)->base + _n_57924);
    Ref(_29737);
    _68Goto_block(_29736, _29737, 0);
    _29736 = NOVALUE;
    _29737 = NOVALUE;
L3: 

    /** 		tok = next_token()*/
    _0 = _tok_57923;
    _tok_57923 = _41next_token();
    DeRef(_0);
    goto L4; // [268] 279
L1: 

    /** 		CompileErr(96)*/
    RefDS(_22663);
    _46CompileErr(96, _22663, 0);
L4: 

    /** 	emit_op(GOTO)*/
    _43emit_op(188);

    /** 	if n = 0 then*/
    if (_n_57924 != 0)
    goto L5; // [290] 302

    /** 		emit_addr(0) -- to be back-patched*/
    _43emit_addr(0);
    goto L6; // [299] 314
L5: 

    /** 		emit_addr(goto_addr[n])*/
    _2 = (int)SEQ_PTR(_41goto_addr_55134);
    _29740 = (int)*(((s1_ptr)_2)->base + _n_57924);
    Ref(_29740);
    _43emit_addr(_29740);
    _29740 = NOVALUE;
L6: 

    /** 	putback(tok)*/
    Ref(_tok_57923);
    _41putback(_tok_57923);

    /** 	NotReached(tok[T_ID], "goto")*/
    _2 = (int)SEQ_PTR(_tok_57923);
    _29741 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_29741);
    RefDS(_27035);
    _41NotReached(_29741, _27035);
    _29741 = NOVALUE;

    /** end procedure*/
    DeRef(_tok_57923);
    _29707 = NOVALUE;
    _29712 = NOVALUE;
    _29726 = NOVALUE;
    return;
    ;
}


void _41Exit_statement()
{
    int _addr_inlined_AppendXList_at_63_58026 = NOVALUE;
    int _tok_58009 = NOVALUE;
    int _by_ref_58010 = NOVALUE;
    int _29749 = NOVALUE;
    int _29748 = NOVALUE;
    int _29747 = NOVALUE;
    int _29746 = NOVALUE;
    int _29744 = NOVALUE;
    int _29742 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence by_ref*/

    /** 	if not length(loop_stack) then*/
    if (IS_SEQUENCE(_41loop_stack_55156)){
            _29742 = SEQ_PTR(_41loop_stack_55156)->length;
    }
    else {
        _29742 = 1;
    }
    if (_29742 != 0)
    goto L1; // [10] 21
    _29742 = NOVALUE;

    /** 		CompileErr(88)*/
    RefDS(_22663);
    _46CompileErr(88, _22663, 0);
L1: 

    /** 	by_ref = exit_level(next_token(),0) -- can't pass tok by reference*/
    _29744 = _41next_token();
    _0 = _by_ref_58010;
    _by_ref_58010 = _41exit_level(_29744, 0);
    DeRef(_0);
    _29744 = NOVALUE;

    /** 	Leave_blocks( by_ref[1], LOOP_BLOCK )*/
    _2 = (int)SEQ_PTR(_by_ref_58010);
    _29746 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_29746);
    _68Leave_blocks(_29746, 1);
    _29746 = NOVALUE;

    /** 	emit_op(EXIT)*/
    _43emit_op(61);

    /** 	AppendXList(length(Code)+1)*/
    if (IS_SEQUENCE(_38Code_17038)){
            _29747 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29747 = 1;
    }
    _29748 = _29747 + 1;
    _29747 = NOVALUE;
    _addr_inlined_AppendXList_at_63_58026 = _29748;
    _29748 = NOVALUE;

    /** 	exit_list = append(exit_list, addr)*/
    Append(&_41exit_list_55142, _41exit_list_55142, _addr_inlined_AppendXList_at_63_58026);

    /** end procedure*/
    goto L2; // [78] 81
L2: 

    /** 	exit_delay &= by_ref[1]*/
    _2 = (int)SEQ_PTR(_by_ref_58010);
    _29749 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_41exit_delay_55143) && IS_ATOM(_29749)) {
        Ref(_29749);
        Append(&_41exit_delay_55143, _41exit_delay_55143, _29749);
    }
    else if (IS_ATOM(_41exit_delay_55143) && IS_SEQUENCE(_29749)) {
    }
    else {
        Concat((object_ptr)&_41exit_delay_55143, _41exit_delay_55143, _29749);
    }
    _29749 = NOVALUE;

    /** 	emit_forward_addr()    -- to be back-patched*/
    _41emit_forward_addr();

    /** 	tok = by_ref[2]*/
    DeRef(_tok_58009);
    _2 = (int)SEQ_PTR(_by_ref_58010);
    _tok_58009 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_tok_58009);

    /** 	putback(tok)*/
    Ref(_tok_58009);
    _41putback(_tok_58009);

    /** end procedure*/
    DeRef(_tok_58009);
    DeRefDS(_by_ref_58010);
    return;
    ;
}


void _41Continue_statement()
{
    int _addr_inlined_AppendNList_at_149_58070 = NOVALUE;
    int _tok_58033 = NOVALUE;
    int _by_ref_58034 = NOVALUE;
    int _loop_level_58035 = NOVALUE;
    int _29775 = NOVALUE;
    int _29772 = NOVALUE;
    int _29771 = NOVALUE;
    int _29770 = NOVALUE;
    int _29769 = NOVALUE;
    int _29768 = NOVALUE;
    int _29767 = NOVALUE;
    int _29765 = NOVALUE;
    int _29764 = NOVALUE;
    int _29763 = NOVALUE;
    int _29762 = NOVALUE;
    int _29761 = NOVALUE;
    int _29760 = NOVALUE;
    int _29759 = NOVALUE;
    int _29758 = NOVALUE;
    int _29756 = NOVALUE;
    int _29754 = NOVALUE;
    int _29752 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence by_ref*/

    /** 	integer loop_level*/

    /** 	if not length(loop_stack) then*/
    if (IS_SEQUENCE(_41loop_stack_55156)){
            _29752 = SEQ_PTR(_41loop_stack_55156)->length;
    }
    else {
        _29752 = 1;
    }
    if (_29752 != 0)
    goto L1; // [12] 23
    _29752 = NOVALUE;

    /** 		CompileErr(49)*/
    RefDS(_22663);
    _46CompileErr(49, _22663, 0);
L1: 

    /** 	by_ref = exit_level(next_token(),0) -- can't pass tok by reference*/
    _29754 = _41next_token();
    _0 = _by_ref_58034;
    _by_ref_58034 = _41exit_level(_29754, 0);
    DeRef(_0);
    _29754 = NOVALUE;

    /** 	Leave_blocks( by_ref[1], LOOP_BLOCK )*/
    _2 = (int)SEQ_PTR(_by_ref_58034);
    _29756 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_29756);
    _68Leave_blocks(_29756, 1);
    _29756 = NOVALUE;

    /** 	emit_op(ELSE)*/
    _43emit_op(23);

    /** 	loop_level = by_ref[1]*/
    _2 = (int)SEQ_PTR(_by_ref_58034);
    _loop_level_58035 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_loop_level_58035))
    _loop_level_58035 = (long)DBL_PTR(_loop_level_58035)->dbl;

    /** 	if continue_addr[$+1-loop_level] then -- address is known for while loops*/
    if (IS_SEQUENCE(_41continue_addr_55147)){
            _29758 = SEQ_PTR(_41continue_addr_55147)->length;
    }
    else {
        _29758 = 1;
    }
    _29759 = _29758 + 1;
    _29758 = NOVALUE;
    _29760 = _29759 - _loop_level_58035;
    _29759 = NOVALUE;
    _2 = (int)SEQ_PTR(_41continue_addr_55147);
    _29761 = (int)*(((s1_ptr)_2)->base + _29760);
    if (_29761 == 0)
    {
        _29761 = NOVALUE;
        goto L2; // [79] 138
    }
    else{
        _29761 = NOVALUE;
    }

    /** 		if continue_addr[$+1-loop_level] < 0 then*/
    if (IS_SEQUENCE(_41continue_addr_55147)){
            _29762 = SEQ_PTR(_41continue_addr_55147)->length;
    }
    else {
        _29762 = 1;
    }
    _29763 = _29762 + 1;
    _29762 = NOVALUE;
    _29764 = _29763 - _loop_level_58035;
    _29763 = NOVALUE;
    _2 = (int)SEQ_PTR(_41continue_addr_55147);
    _29765 = (int)*(((s1_ptr)_2)->base + _29764);
    if (_29765 >= 0)
    goto L3; // [101] 113

    /** 			CompileErr(49)*/
    RefDS(_22663);
    _46CompileErr(49, _22663, 0);
L3: 

    /** 		emit_addr(continue_addr[$+1-loop_level])*/
    if (IS_SEQUENCE(_41continue_addr_55147)){
            _29767 = SEQ_PTR(_41continue_addr_55147)->length;
    }
    else {
        _29767 = 1;
    }
    _29768 = _29767 + 1;
    _29767 = NOVALUE;
    _29769 = _29768 - _loop_level_58035;
    _29768 = NOVALUE;
    _2 = (int)SEQ_PTR(_41continue_addr_55147);
    _29770 = (int)*(((s1_ptr)_2)->base + _29769);
    _43emit_addr(_29770);
    _29770 = NOVALUE;
    goto L4; // [135] 182
L2: 

    /** 		AppendNList(length(Code)+1)*/
    if (IS_SEQUENCE(_38Code_17038)){
            _29771 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29771 = 1;
    }
    _29772 = _29771 + 1;
    _29771 = NOVALUE;
    _addr_inlined_AppendNList_at_149_58070 = _29772;
    _29772 = NOVALUE;

    /** 	continue_list = append(continue_list, addr)*/
    Append(&_41continue_list_55144, _41continue_list_55144, _addr_inlined_AppendNList_at_149_58070);

    /** end procedure*/
    goto L5; // [164] 167
L5: 

    /** 		continue_delay &= loop_level*/
    Append(&_41continue_delay_55145, _41continue_delay_55145, _loop_level_58035);

    /** 		emit_forward_addr()    -- to be back-patched*/
    _41emit_forward_addr();
L4: 

    /** 	tok = by_ref[2]*/
    DeRef(_tok_58033);
    _2 = (int)SEQ_PTR(_by_ref_58034);
    _tok_58033 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_tok_58033);

    /** 	putback(tok)*/
    Ref(_tok_58033);
    _41putback(_tok_58033);

    /** 	NotReached(tok[T_ID], "continue")*/
    _2 = (int)SEQ_PTR(_tok_58033);
    _29775 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_29775);
    RefDS(_26997);
    _41NotReached(_29775, _26997);
    _29775 = NOVALUE;

    /** end procedure*/
    DeRef(_tok_58033);
    DeRefDS(_by_ref_58034);
    _29765 = NOVALUE;
    DeRef(_29760);
    _29760 = NOVALUE;
    DeRef(_29764);
    _29764 = NOVALUE;
    DeRef(_29769);
    _29769 = NOVALUE;
    return;
    ;
}


void _41Retry_statement()
{
    int _by_ref_58077 = NOVALUE;
    int _tok_58079 = NOVALUE;
    int _29799 = NOVALUE;
    int _29797 = NOVALUE;
    int _29796 = NOVALUE;
    int _29795 = NOVALUE;
    int _29794 = NOVALUE;
    int _29793 = NOVALUE;
    int _29791 = NOVALUE;
    int _29790 = NOVALUE;
    int _29789 = NOVALUE;
    int _29788 = NOVALUE;
    int _29787 = NOVALUE;
    int _29785 = NOVALUE;
    int _29784 = NOVALUE;
    int _29783 = NOVALUE;
    int _29782 = NOVALUE;
    int _29781 = NOVALUE;
    int _29780 = NOVALUE;
    int _29778 = NOVALUE;
    int _29776 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length(loop_stack) then*/
    if (IS_SEQUENCE(_41loop_stack_55156)){
            _29776 = SEQ_PTR(_41loop_stack_55156)->length;
    }
    else {
        _29776 = 1;
    }
    if (_29776 != 0)
    goto L1; // [8] 19
    _29776 = NOVALUE;

    /** 		CompileErr(131)*/
    RefDS(_22663);
    _46CompileErr(131, _22663, 0);
L1: 

    /** 	by_ref = exit_level(next_token(),0) -- can't pass tok by reference*/
    _29778 = _41next_token();
    _0 = _by_ref_58077;
    _by_ref_58077 = _41exit_level(_29778, 0);
    DeRef(_0);
    _29778 = NOVALUE;

    /** 	Leave_blocks( by_ref[1], LOOP_BLOCK )*/
    _2 = (int)SEQ_PTR(_by_ref_58077);
    _29780 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_29780);
    _68Leave_blocks(_29780, 1);
    _29780 = NOVALUE;

    /** 	if loop_stack[$+1-by_ref[1]]=FOR then*/
    if (IS_SEQUENCE(_41loop_stack_55156)){
            _29781 = SEQ_PTR(_41loop_stack_55156)->length;
    }
    else {
        _29781 = 1;
    }
    _29782 = _29781 + 1;
    _29781 = NOVALUE;
    _2 = (int)SEQ_PTR(_by_ref_58077);
    _29783 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29783)) {
        _29784 = _29782 - _29783;
    }
    else {
        _29784 = binary_op(MINUS, _29782, _29783);
    }
    _29782 = NOVALUE;
    _29783 = NOVALUE;
    _2 = (int)SEQ_PTR(_41loop_stack_55156);
    if (!IS_ATOM_INT(_29784)){
        _29785 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29784)->dbl));
    }
    else{
        _29785 = (int)*(((s1_ptr)_2)->base + _29784);
    }
    if (_29785 != 21)
    goto L2; // [68] 82

    /** 		emit_op(RETRY) -- for Translator to emit a label at the right place*/
    _43emit_op(184);
    goto L3; // [79] 125
L2: 

    /** 		if retry_addr[$+1-by_ref[1]] < 0 then*/
    if (IS_SEQUENCE(_41retry_addr_55148)){
            _29787 = SEQ_PTR(_41retry_addr_55148)->length;
    }
    else {
        _29787 = 1;
    }
    _29788 = _29787 + 1;
    _29787 = NOVALUE;
    _2 = (int)SEQ_PTR(_by_ref_58077);
    _29789 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29789)) {
        _29790 = _29788 - _29789;
    }
    else {
        _29790 = binary_op(MINUS, _29788, _29789);
    }
    _29788 = NOVALUE;
    _29789 = NOVALUE;
    _2 = (int)SEQ_PTR(_41retry_addr_55148);
    if (!IS_ATOM_INT(_29790)){
        _29791 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29790)->dbl));
    }
    else{
        _29791 = (int)*(((s1_ptr)_2)->base + _29790);
    }
    if (_29791 >= 0)
    goto L4; // [105] 117

    /** 			CompileErr(131)*/
    RefDS(_22663);
    _46CompileErr(131, _22663, 0);
L4: 

    /** 		emit_op(ELSE)*/
    _43emit_op(23);
L3: 

    /** 	emit_addr(retry_addr[$+1-by_ref[1]])*/
    if (IS_SEQUENCE(_41retry_addr_55148)){
            _29793 = SEQ_PTR(_41retry_addr_55148)->length;
    }
    else {
        _29793 = 1;
    }
    _29794 = _29793 + 1;
    _29793 = NOVALUE;
    _2 = (int)SEQ_PTR(_by_ref_58077);
    _29795 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29795)) {
        _29796 = _29794 - _29795;
    }
    else {
        _29796 = binary_op(MINUS, _29794, _29795);
    }
    _29794 = NOVALUE;
    _29795 = NOVALUE;
    _2 = (int)SEQ_PTR(_41retry_addr_55148);
    if (!IS_ATOM_INT(_29796)){
        _29797 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29796)->dbl));
    }
    else{
        _29797 = (int)*(((s1_ptr)_2)->base + _29796);
    }
    _43emit_addr(_29797);
    _29797 = NOVALUE;

    /** 	tok = by_ref[2]*/
    DeRef(_tok_58079);
    _2 = (int)SEQ_PTR(_by_ref_58077);
    _tok_58079 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_tok_58079);

    /** 	putback(tok)*/
    Ref(_tok_58079);
    _41putback(_tok_58079);

    /** 	NotReached(tok[T_ID], "retry")*/
    _2 = (int)SEQ_PTR(_tok_58079);
    _29799 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_29799);
    RefDS(_27091);
    _41NotReached(_29799, _27091);
    _29799 = NOVALUE;

    /** end procedure*/
    DeRefDS(_by_ref_58077);
    DeRef(_tok_58079);
    _29785 = NOVALUE;
    _29791 = NOVALUE;
    DeRef(_29784);
    _29784 = NOVALUE;
    DeRef(_29790);
    _29790 = NOVALUE;
    DeRef(_29796);
    _29796 = NOVALUE;
    return;
    ;
}


int _41in_switch()
{
    int _29804 = NOVALUE;
    int _29803 = NOVALUE;
    int _29802 = NOVALUE;
    int _29801 = NOVALUE;
    int _29800 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length( if_stack ) and if_stack[$] = SWITCH then*/
    if (IS_SEQUENCE(_41if_stack_55157)){
            _29800 = SEQ_PTR(_41if_stack_55157)->length;
    }
    else {
        _29800 = 1;
    }
    if (_29800 == 0) {
        goto L1; // [8] 40
    }
    if (IS_SEQUENCE(_41if_stack_55157)){
            _29802 = SEQ_PTR(_41if_stack_55157)->length;
    }
    else {
        _29802 = 1;
    }
    _2 = (int)SEQ_PTR(_41if_stack_55157);
    _29803 = (int)*(((s1_ptr)_2)->base + _29802);
    _29804 = (_29803 == 185);
    _29803 = NOVALUE;
    if (_29804 == 0)
    {
        DeRef(_29804);
        _29804 = NOVALUE;
        goto L1; // [28] 40
    }
    else{
        DeRef(_29804);
        _29804 = NOVALUE;
    }

    /** 		return 1*/
    return 1;
    goto L2; // [37] 47
L1: 

    /** 		return 0*/
    return 0;
L2: 
    ;
}


void _41Break_statement()
{
    int _addr_inlined_AppendEList_at_63_58149 = NOVALUE;
    int _tok_58132 = NOVALUE;
    int _by_ref_58133 = NOVALUE;
    int _29815 = NOVALUE;
    int _29812 = NOVALUE;
    int _29811 = NOVALUE;
    int _29810 = NOVALUE;
    int _29809 = NOVALUE;
    int _29807 = NOVALUE;
    int _29805 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence by_ref*/

    /** 	if not length(if_labels) then*/
    if (IS_SEQUENCE(_41if_labels_55151)){
            _29805 = SEQ_PTR(_41if_labels_55151)->length;
    }
    else {
        _29805 = 1;
    }
    if (_29805 != 0)
    goto L1; // [10] 21
    _29805 = NOVALUE;

    /** 		CompileErr(40)*/
    RefDS(_22663);
    _46CompileErr(40, _22663, 0);
L1: 

    /** 	by_ref = exit_level(next_token(),1)*/
    _29807 = _41next_token();
    _0 = _by_ref_58133;
    _by_ref_58133 = _41exit_level(_29807, 1);
    DeRef(_0);
    _29807 = NOVALUE;

    /** 	Leave_blocks( by_ref[1], CONDITIONAL_BLOCK )*/
    _2 = (int)SEQ_PTR(_by_ref_58133);
    _29809 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_29809);
    _68Leave_blocks(_29809, 2);
    _29809 = NOVALUE;

    /** 	emit_op(ELSE)*/
    _43emit_op(23);

    /** 	AppendEList(length(Code)+1)*/
    if (IS_SEQUENCE(_38Code_17038)){
            _29810 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29810 = 1;
    }
    _29811 = _29810 + 1;
    _29810 = NOVALUE;
    _addr_inlined_AppendEList_at_63_58149 = _29811;
    _29811 = NOVALUE;

    /** 	break_list = append(break_list, addr)*/
    Append(&_41break_list_55140, _41break_list_55140, _addr_inlined_AppendEList_at_63_58149);

    /** end procedure*/
    goto L2; // [78] 81
L2: 

    /** 	break_delay &= by_ref[1]*/
    _2 = (int)SEQ_PTR(_by_ref_58133);
    _29812 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_41break_delay_55141) && IS_ATOM(_29812)) {
        Ref(_29812);
        Append(&_41break_delay_55141, _41break_delay_55141, _29812);
    }
    else if (IS_ATOM(_41break_delay_55141) && IS_SEQUENCE(_29812)) {
    }
    else {
        Concat((object_ptr)&_41break_delay_55141, _41break_delay_55141, _29812);
    }
    _29812 = NOVALUE;

    /** 	emit_forward_addr()    -- to be back-patched*/
    _41emit_forward_addr();

    /** 	tok = by_ref[2]*/
    DeRef(_tok_58132);
    _2 = (int)SEQ_PTR(_by_ref_58133);
    _tok_58132 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_tok_58132);

    /** 	putback(tok)*/
    Ref(_tok_58132);
    _41putback(_tok_58132);

    /** 	NotReached(tok[T_ID], "break")*/
    _2 = (int)SEQ_PTR(_tok_58132);
    _29815 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_29815);
    RefDS(_26981);
    _41NotReached(_29815, _26981);
    _29815 = NOVALUE;

    /** end procedure*/
    DeRef(_tok_58132);
    DeRefDS(_by_ref_58133);
    return;
    ;
}


int _41finish_block_header(int _opcode_58158)
{
    int _tok_58160 = NOVALUE;
    int _labbel_58161 = NOVALUE;
    int _has_entry_58162 = NOVALUE;
    int _29869 = NOVALUE;
    int _29868 = NOVALUE;
    int _29867 = NOVALUE;
    int _29866 = NOVALUE;
    int _29863 = NOVALUE;
    int _29858 = NOVALUE;
    int _29855 = NOVALUE;
    int _29853 = NOVALUE;
    int _29850 = NOVALUE;
    int _29849 = NOVALUE;
    int _29847 = NOVALUE;
    int _29844 = NOVALUE;
    int _29841 = NOVALUE;
    int _29840 = NOVALUE;
    int _29838 = NOVALUE;
    int _29836 = NOVALUE;
    int _29833 = NOVALUE;
    int _29830 = NOVALUE;
    int _29829 = NOVALUE;
    int _29827 = NOVALUE;
    int _29825 = NOVALUE;
    int _29824 = NOVALUE;
    int _29823 = NOVALUE;
    int _29820 = NOVALUE;
    int _29817 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	object labbel*/

    /** 	integer has_entry*/

    /** 	tok = next_token()*/
    _0 = _tok_58160;
    _tok_58160 = _41next_token();
    DeRef(_0);

    /** 	has_entry=0*/
    _has_entry_58162 = 0;

    /** 	if tok[T_ID] = WITH then*/
    _2 = (int)SEQ_PTR(_tok_58160);
    _29817 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29817, 420)){
        _29817 = NOVALUE;
        goto L1; // [27] 156
    }
    _29817 = NOVALUE;

    /** 		tok = next_token()*/
    _0 = _tok_58160;
    _tok_58160 = _41next_token();
    DeRef(_0);

    /** 		switch tok[T_ID] do*/
    _2 = (int)SEQ_PTR(_tok_58160);
    _29820 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_29820) ){
        goto L2; // [44] 138
    }
    if(!IS_ATOM_INT(_29820)){
        if( (DBL_PTR(_29820)->dbl != (double) ((int) DBL_PTR(_29820)->dbl) ) ){
            goto L2; // [44] 138
        }
        _0 = (int) DBL_PTR(_29820)->dbl;
    }
    else {
        _0 = _29820;
    };
    _29820 = NOVALUE;
    switch ( _0 ){ 

        /** 		    case ENTRY then*/
        case 424:

        /** 				if not (opcode = WHILE or opcode = LOOP) then*/
        _29823 = (_opcode_58158 == 47);
        if (_29823 != 0) {
            DeRef(_29824);
            _29824 = 1;
            goto L3; // [61] 75
        }
        _29825 = (_opcode_58158 == 422);
        _29824 = (_29825 != 0);
L3: 
        if (_29824 != 0)
        goto L4; // [75] 86
        _29824 = NOVALUE;

        /** 					CompileErr(14)*/
        RefDS(_22663);
        _46CompileErr(14, _22663, 0);
L4: 

        /** 			    has_entry = 1*/
        _has_entry_58162 = 1;
        goto L5; // [91] 148

        /** 			case FALLTHRU then*/
        case 431:

        /** 				if not opcode = SWITCH then*/
        _29827 = (_opcode_58158 == 0);
        if (_29827 != 185)
        goto L6; // [104] 116

        /** 					CompileErr(13)*/
        RefDS(_22663);
        _46CompileErr(13, _22663, 0);
L6: 

        /** 				switch_stack[$][SWITCH_FALLTHRU] = 1*/
        if (IS_SEQUENCE(_41switch_stack_55357)){
                _29829 = SEQ_PTR(_41switch_stack_55357)->length;
        }
        else {
            _29829 = 1;
        }
        _2 = (int)SEQ_PTR(_41switch_stack_55357);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _41switch_stack_55357 = MAKE_SEQ(_2);
        }
        _3 = (int)(_29829 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = 1;
        DeRef(_1);
        _29830 = NOVALUE;
        goto L5; // [134] 148

        /** 			case else*/
        default:
L2: 

        /** 			    CompileErr(27)*/
        RefDS(_22663);
        _46CompileErr(27, _22663, 0);
    ;}L5: 

    /**         tok = next_token()*/
    _0 = _tok_58160;
    _tok_58160 = _41next_token();
    DeRef(_0);
    goto L7; // [153] 244
L1: 

    /** 	elsif tok[T_ID] = WITHOUT then*/
    _2 = (int)SEQ_PTR(_tok_58160);
    _29833 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29833, 421)){
        _29833 = NOVALUE;
        goto L8; // [166] 243
    }
    _29833 = NOVALUE;

    /** 		tok = next_token()*/
    _0 = _tok_58160;
    _tok_58160 = _41next_token();
    DeRef(_0);

    /** 		if tok[T_ID] = FALLTHRU then*/
    _2 = (int)SEQ_PTR(_tok_58160);
    _29836 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29836, 431)){
        _29836 = NOVALUE;
        goto L9; // [185] 229
    }
    _29836 = NOVALUE;

    /** 			if not opcode = SWITCH then*/
    _29838 = (_opcode_58158 == 0);
    if (_29838 != 185)
    goto LA; // [196] 208

    /** 				CompileErr(15)*/
    RefDS(_22663);
    _46CompileErr(15, _22663, 0);
LA: 

    /** 			switch_stack[$][SWITCH_FALLTHRU] = 0*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _29840 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _29840 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _41switch_stack_55357 = MAKE_SEQ(_2);
    }
    _3 = (int)(_29840 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _29841 = NOVALUE;
    goto LB; // [226] 237
L9: 

    /** 			CompileErr(27)*/
    RefDS(_22663);
    _46CompileErr(27, _22663, 0);
LB: 

    /**         tok = next_token()*/
    _0 = _tok_58160;
    _tok_58160 = _41next_token();
    DeRef(_0);
L8: 
L7: 

    /** 	labbel=0*/
    DeRef(_labbel_58161);
    _labbel_58161 = 0;

    /** 	if tok[T_ID]=LABEL then*/
    _2 = (int)SEQ_PTR(_tok_58160);
    _29844 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29844, 419)){
        _29844 = NOVALUE;
        goto LC; // [259] 321
    }
    _29844 = NOVALUE;

    /** 		tok = next_token()*/
    _0 = _tok_58160;
    _tok_58160 = _41next_token();
    DeRef(_0);

    /** 		if tok[T_ID] != STRING then*/
    _2 = (int)SEQ_PTR(_tok_58160);
    _29847 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _29847, 503)){
        _29847 = NOVALUE;
        goto LD; // [278] 290
    }
    _29847 = NOVALUE;

    /** 			CompileErr(38)*/
    RefDS(_22663);
    _46CompileErr(38, _22663, 0);
LD: 

    /** 		labbel = SymTab[tok[T_SYM]][S_OBJ]*/
    _2 = (int)SEQ_PTR(_tok_58160);
    _29849 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_29849)){
        _29850 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29849)->dbl));
    }
    else{
        _29850 = (int)*(((s1_ptr)_2)->base + _29849);
    }
    DeRef(_labbel_58161);
    _2 = (int)SEQ_PTR(_29850);
    _labbel_58161 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_labbel_58161);
    _29850 = NOVALUE;

    /** 		block_label( labbel )*/
    Ref(_labbel_58161);
    _68block_label(_labbel_58161);

    /** 		tok = next_token()*/
    _0 = _tok_58160;
    _tok_58160 = _41next_token();
    DeRef(_0);
LC: 

    /** 	if opcode = IF or opcode = SWITCH then*/
    _29853 = (_opcode_58158 == 20);
    if (_29853 != 0) {
        goto LE; // [329] 344
    }
    _29855 = (_opcode_58158 == 185);
    if (_29855 == 0)
    {
        DeRef(_29855);
        _29855 = NOVALUE;
        goto LF; // [340] 355
    }
    else{
        DeRef(_29855);
        _29855 = NOVALUE;
    }
LE: 

    /** 		if_labels = append(if_labels,labbel)*/
    Ref(_labbel_58161);
    Append(&_41if_labels_55151, _41if_labels_55151, _labbel_58161);
    goto L10; // [352] 364
LF: 

    /** 		loop_labels = append(loop_labels,labbel)*/
    Ref(_labbel_58161);
    Append(&_41loop_labels_55150, _41loop_labels_55150, _labbel_58161);
L10: 

    /** 	if block_index=length(block_list) then*/
    if (IS_SEQUENCE(_41block_list_55152)){
            _29858 = SEQ_PTR(_41block_list_55152)->length;
    }
    else {
        _29858 = 1;
    }
    if (_41block_index_55153 != _29858)
    goto L11; // [373] 396

    /** 	    block_list &= opcode*/
    Append(&_41block_list_55152, _41block_list_55152, _opcode_58158);

    /** 	    block_index += 1*/
    _41block_index_55153 = _41block_index_55153 + 1;
    goto L12; // [393] 415
L11: 

    /** 	    block_index += 1*/
    _41block_index_55153 = _41block_index_55153 + 1;

    /** 	    block_list[block_index] = opcode*/
    _2 = (int)SEQ_PTR(_41block_list_55152);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _41block_list_55152 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _41block_index_55153);
    *(int *)_2 = _opcode_58158;
L12: 

    /** 	if tok[T_ID]=ENTRY then*/
    _2 = (int)SEQ_PTR(_tok_58160);
    _29863 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29863, 424)){
        _29863 = NOVALUE;
        goto L13; // [425] 453
    }
    _29863 = NOVALUE;

    /** 	    if has_entry then*/
    if (_has_entry_58162 == 0)
    {
        goto L14; // [431] 442
    }
    else{
    }

    /** 	        CompileErr(64)*/
    RefDS(_22663);
    _46CompileErr(64, _22663, 0);
L14: 

    /** 	    has_entry=1*/
    _has_entry_58162 = 1;

    /** 	    tok=next_token()*/
    _0 = _tok_58160;
    _tok_58160 = _41next_token();
    DeRef(_0);
L13: 

    /** 	if has_entry and (opcode = IF or opcode = SWITCH) then*/
    if (_has_entry_58162 == 0) {
        goto L15; // [455] 491
    }
    _29867 = (_opcode_58158 == 20);
    if (_29867 != 0) {
        DeRef(_29868);
        _29868 = 1;
        goto L16; // [465] 479
    }
    _29869 = (_opcode_58158 == 185);
    _29868 = (_29869 != 0);
L16: 
    if (_29868 == 0)
    {
        _29868 = NOVALUE;
        goto L15; // [480] 491
    }
    else{
        _29868 = NOVALUE;
    }

    /** 		CompileErr(80)*/
    RefDS(_22663);
    _46CompileErr(80, _22663, 0);
L15: 

    /** 	if opcode = IF then*/
    if (_opcode_58158 != 20)
    goto L17; // [495] 511

    /** 		opcode = THEN*/
    _opcode_58158 = 410;
    goto L18; // [508] 521
L17: 

    /** 		opcode = DO*/
    _opcode_58158 = 411;
L18: 

    /** 	putback(tok)*/
    Ref(_tok_58160);
    _41putback(_tok_58160);

    /** 	tok_match(opcode)*/
    _41tok_match(_opcode_58158, 0);

    /** 	return has_entry*/
    DeRef(_tok_58160);
    DeRef(_labbel_58161);
    DeRef(_29823);
    _29823 = NOVALUE;
    DeRef(_29825);
    _29825 = NOVALUE;
    DeRef(_29827);
    _29827 = NOVALUE;
    DeRef(_29838);
    _29838 = NOVALUE;
    _29849 = NOVALUE;
    DeRef(_29853);
    _29853 = NOVALUE;
    DeRef(_29867);
    _29867 = NOVALUE;
    DeRef(_29869);
    _29869 = NOVALUE;
    return _has_entry_58162;
    ;
}


void _41If_statement()
{
    int _addr_inlined_AppendEList_at_624_58408 = NOVALUE;
    int _addr_inlined_AppendEList_at_260_58337 = NOVALUE;
    int _tok_58280 = NOVALUE;
    int _prev_false_58281 = NOVALUE;
    int _prev_false2_58282 = NOVALUE;
    int _elist_base_58283 = NOVALUE;
    int _temps_58291 = NOVALUE;
    int _32354 = NOVALUE;
    int _29935 = NOVALUE;
    int _29934 = NOVALUE;
    int _29931 = NOVALUE;
    int _29930 = NOVALUE;
    int _29928 = NOVALUE;
    int _29927 = NOVALUE;
    int _29926 = NOVALUE;
    int _29924 = NOVALUE;
    int _29923 = NOVALUE;
    int _29921 = NOVALUE;
    int _29920 = NOVALUE;
    int _29919 = NOVALUE;
    int _29917 = NOVALUE;
    int _29916 = NOVALUE;
    int _29914 = NOVALUE;
    int _29913 = NOVALUE;
    int _29912 = NOVALUE;
    int _29911 = NOVALUE;
    int _29909 = NOVALUE;
    int _29908 = NOVALUE;
    int _29905 = NOVALUE;
    int _29903 = NOVALUE;
    int _29902 = NOVALUE;
    int _29901 = NOVALUE;
    int _29898 = NOVALUE;
    int _29895 = NOVALUE;
    int _29894 = NOVALUE;
    int _29892 = NOVALUE;
    int _29891 = NOVALUE;
    int _29889 = NOVALUE;
    int _29888 = NOVALUE;
    int _29886 = NOVALUE;
    int _29883 = NOVALUE;
    int _29881 = NOVALUE;
    int _29880 = NOVALUE;
    int _29879 = NOVALUE;
    int _29875 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer prev_false*/

    /** 	integer prev_false2*/

    /** 	integer elist_base*/

    /** 	if_stack &= IF*/
    Append(&_41if_stack_55157, _41if_stack_55157, 20);

    /** 	Start_block( IF )*/
    _68Start_block(20, 0);

    /** 	elist_base = length(break_list)*/
    if (IS_SEQUENCE(_41break_list_55140)){
            _elist_base_58283 = SEQ_PTR(_41break_list_55140)->length;
    }
    else {
        _elist_base_58283 = 1;
    }

    /** 	short_circuit += 1*/
    _41short_circuit_55122 = _41short_circuit_55122 + 1;

    /** 	short_circuit_B = FALSE*/
    _41short_circuit_B_55124 = _9FALSE_426;

    /** 	SC1_type = 0*/
    _41SC1_type_55127 = 0;

    /** 	Expr()*/
    _41Expr();

    /** 	sequence temps = get_temps()*/
    _32354 = Repeat(_22663, 2);
    _0 = _temps_58291;
    _temps_58291 = _43get_temps(_32354);
    DeRef(_0);
    _32354 = NOVALUE;

    /** 	emit_op(IF)*/
    _43emit_op(20);

    /** 	prev_false = length(Code)+1*/
    if (IS_SEQUENCE(_38Code_17038)){
            _29875 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29875 = 1;
    }
    _prev_false_58281 = _29875 + 1;
    _29875 = NOVALUE;

    /** 	emit_forward_addr() -- to be patched*/
    _41emit_forward_addr();

    /** 	prev_false2=finish_block_header(IF)  -- 0*/
    _prev_false2_58282 = _41finish_block_header(20);
    if (!IS_ATOM_INT(_prev_false2_58282)) {
        _1 = (long)(DBL_PTR(_prev_false2_58282)->dbl);
        if (UNIQUE(DBL_PTR(_prev_false2_58282)) && (DBL_PTR(_prev_false2_58282)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_prev_false2_58282);
        _prev_false2_58282 = _1;
    }

    /** 	if SC1_type = OR then*/
    if (_41SC1_type_55127 != 9)
    goto L1; // [106] 159

    /** 		backpatch(SC1_patch-3, SC1_OR_IF)*/
    _29879 = _41SC1_patch_55126 - 3;
    if ((long)((unsigned long)_29879 +(unsigned long) HIGH_BITS) >= 0){
        _29879 = NewDouble((double)_29879);
    }
    _43backpatch(_29879, 147);
    _29879 = NOVALUE;

    /** 		if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L2; // [128] 139
    }
    else{
    }

    /** 			emit_op(NOP1)  -- to get label here*/
    _43emit_op(159);
L2: 

    /** 		backpatch(SC1_patch, length(Code)+1)*/
    if (IS_SEQUENCE(_38Code_17038)){
            _29880 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29880 = 1;
    }
    _29881 = _29880 + 1;
    _29880 = NOVALUE;
    _43backpatch(_41SC1_patch_55126, _29881);
    _29881 = NOVALUE;
    goto L3; // [156] 192
L1: 

    /** 	elsif SC1_type = AND then*/
    if (_41SC1_type_55127 != 8)
    goto L4; // [165] 191

    /** 		backpatch(SC1_patch-3, SC1_AND_IF)*/
    _29883 = _41SC1_patch_55126 - 3;
    if ((long)((unsigned long)_29883 +(unsigned long) HIGH_BITS) >= 0){
        _29883 = NewDouble((double)_29883);
    }
    _43backpatch(_29883, 146);
    _29883 = NOVALUE;

    /** 		prev_false2 = SC1_patch*/
    _prev_false2_58282 = _41SC1_patch_55126;
L4: 
L3: 

    /** 	short_circuit -= 1*/
    _41short_circuit_55122 = _41short_circuit_55122 - 1;

    /** 	Statement_list()*/
    _41Statement_list();

    /** 	tok = next_token()*/
    _0 = _tok_58280;
    _tok_58280 = _41next_token();
    DeRef(_0);

    /** 	while tok[T_ID] = ELSIF do*/
L5: 
    _2 = (int)SEQ_PTR(_tok_58280);
    _29886 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29886, 414)){
        _29886 = NOVALUE;
        goto L6; // [222] 532
    }
    _29886 = NOVALUE;

    /** 		Sibling_block( IF )*/
    _68Sibling_block(20);

    /** 		emit_op(ELSE)*/
    _43emit_op(23);

    /** 		AppendEList(length(Code)+1)*/
    if (IS_SEQUENCE(_38Code_17038)){
            _29888 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29888 = 1;
    }
    _29889 = _29888 + 1;
    _29888 = NOVALUE;
    _addr_inlined_AppendEList_at_260_58337 = _29889;
    _29889 = NOVALUE;

    /** 	break_list = append(break_list, addr)*/
    Append(&_41break_list_55140, _41break_list_55140, _addr_inlined_AppendEList_at_260_58337);

    /** end procedure*/
    goto L7; // [266] 269
L7: 

    /** 		break_delay &= 1*/
    Append(&_41break_delay_55141, _41break_delay_55141, 1);

    /** 		emit_forward_addr()  -- to be patched*/
    _41emit_forward_addr();

    /** 		if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L8; // [287] 298
    }
    else{
    }

    /** 			emit_op(NOP1)*/
    _43emit_op(159);
L8: 

    /** 		backpatch(prev_false, length(Code)+1)*/
    if (IS_SEQUENCE(_38Code_17038)){
            _29891 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29891 = 1;
    }
    _29892 = _29891 + 1;
    _29891 = NOVALUE;
    _43backpatch(_prev_false_58281, _29892);
    _29892 = NOVALUE;

    /** 		if prev_false2 != 0 then*/
    if (_prev_false2_58282 == 0)
    goto L9; // [315] 335

    /** 			backpatch(prev_false2, length(Code)+1)*/
    if (IS_SEQUENCE(_38Code_17038)){
            _29894 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29894 = 1;
    }
    _29895 = _29894 + 1;
    _29894 = NOVALUE;
    _43backpatch(_prev_false2_58282, _29895);
    _29895 = NOVALUE;
L9: 

    /** 		StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 		short_circuit += 1*/
    _41short_circuit_55122 = _41short_circuit_55122 + 1;

    /** 		short_circuit_B = FALSE*/
    _41short_circuit_B_55124 = _9FALSE_426;

    /** 		SC1_type = 0*/
    _41SC1_type_55127 = 0;

    /** 		push_temps( temps )*/
    RefDS(_temps_58291);
    _43push_temps(_temps_58291);

    /** 		Expr()*/
    _41Expr();

    /** 		temps = get_temps( temps )*/
    RefDS(_temps_58291);
    _0 = _temps_58291;
    _temps_58291 = _43get_temps(_temps_58291);
    DeRefDS(_0);

    /** 		emit_op(IF)*/
    _43emit_op(20);

    /** 		prev_false = length(Code)+1*/
    if (IS_SEQUENCE(_38Code_17038)){
            _29898 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29898 = 1;
    }
    _prev_false_58281 = _29898 + 1;
    _29898 = NOVALUE;

    /** 		prev_false2 = 0*/
    _prev_false2_58282 = 0;

    /** 		emit_forward_addr() -- to be patched*/
    _41emit_forward_addr();

    /** 		if SC1_type = OR then*/
    if (_41SC1_type_55127 != 9)
    goto LA; // [416] 469

    /** 			backpatch(SC1_patch-3, SC1_OR_IF)*/
    _29901 = _41SC1_patch_55126 - 3;
    if ((long)((unsigned long)_29901 +(unsigned long) HIGH_BITS) >= 0){
        _29901 = NewDouble((double)_29901);
    }
    _43backpatch(_29901, 147);
    _29901 = NOVALUE;

    /** 			if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto LB; // [438] 449
    }
    else{
    }

    /** 				emit_op(NOP1)*/
    _43emit_op(159);
LB: 

    /** 			backpatch(SC1_patch, length(Code)+1)*/
    if (IS_SEQUENCE(_38Code_17038)){
            _29902 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29902 = 1;
    }
    _29903 = _29902 + 1;
    _29902 = NOVALUE;
    _43backpatch(_41SC1_patch_55126, _29903);
    _29903 = NOVALUE;
    goto LC; // [466] 502
LA: 

    /** 		elsif SC1_type = AND then*/
    if (_41SC1_type_55127 != 8)
    goto LD; // [475] 501

    /** 			backpatch(SC1_patch-3, SC1_AND_IF)*/
    _29905 = _41SC1_patch_55126 - 3;
    if ((long)((unsigned long)_29905 +(unsigned long) HIGH_BITS) >= 0){
        _29905 = NewDouble((double)_29905);
    }
    _43backpatch(_29905, 146);
    _29905 = NOVALUE;

    /** 			prev_false2 = SC1_patch*/
    _prev_false2_58282 = _41SC1_patch_55126;
LD: 
LC: 

    /** 		short_circuit -= 1*/
    _41short_circuit_55122 = _41short_circuit_55122 - 1;

    /** 		tok_match(THEN)*/
    _41tok_match(410, 0);

    /** 		Statement_list()*/
    _41Statement_list();

    /** 		tok = next_token()*/
    _0 = _tok_58280;
    _tok_58280 = _41next_token();
    DeRef(_0);

    /** 	end while*/
    goto L5; // [529] 214
L6: 

    /** 	if tok[T_ID] = ELSE or length(temps[1]) then*/
    _2 = (int)SEQ_PTR(_tok_58280);
    _29908 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29908)) {
        _29909 = (_29908 == 23);
    }
    else {
        _29909 = binary_op(EQUALS, _29908, 23);
    }
    _29908 = NOVALUE;
    if (IS_ATOM_INT(_29909)) {
        if (_29909 != 0) {
            goto LE; // [546] 562
        }
    }
    else {
        if (DBL_PTR(_29909)->dbl != 0.0) {
            goto LE; // [546] 562
        }
    }
    _2 = (int)SEQ_PTR(_temps_58291);
    _29911 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_29911)){
            _29912 = SEQ_PTR(_29911)->length;
    }
    else {
        _29912 = 1;
    }
    _29911 = NOVALUE;
    if (_29912 == 0)
    {
        _29912 = NOVALUE;
        goto LF; // [558] 717
    }
    else{
        _29912 = NOVALUE;
    }
LE: 

    /** 		Sibling_block( IF )*/
    _68Sibling_block(20);

    /** 		StartSourceLine(FALSE, , COVERAGE_SUPPRESS )*/
    _43StartSourceLine(_9FALSE_426, 0, 1);

    /** 		emit_op(ELSE)*/
    _43emit_op(23);

    /** 		AppendEList(length(Code)+1)*/
    if (IS_SEQUENCE(_38Code_17038)){
            _29913 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29913 = 1;
    }
    _29914 = _29913 + 1;
    _29913 = NOVALUE;
    _addr_inlined_AppendEList_at_624_58408 = _29914;
    _29914 = NOVALUE;

    /** 	break_list = append(break_list, addr)*/
    Append(&_41break_list_55140, _41break_list_55140, _addr_inlined_AppendEList_at_624_58408);

    /** end procedure*/
    goto L10; // [613] 616
L10: 

    /** 		break_delay &= 1*/
    Append(&_41break_delay_55141, _41break_delay_55141, 1);

    /** 		emit_forward_addr() -- to be patched*/
    _41emit_forward_addr();

    /** 		if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L11; // [634] 645
    }
    else{
    }

    /** 			emit_op(NOP1)*/
    _43emit_op(159);
L11: 

    /** 		backpatch(prev_false, length(Code)+1)*/
    if (IS_SEQUENCE(_38Code_17038)){
            _29916 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29916 = 1;
    }
    _29917 = _29916 + 1;
    _29916 = NOVALUE;
    _43backpatch(_prev_false_58281, _29917);
    _29917 = NOVALUE;

    /** 		if prev_false2 != 0 then*/
    if (_prev_false2_58282 == 0)
    goto L12; // [662] 682

    /** 			backpatch(prev_false2, length(Code)+1)*/
    if (IS_SEQUENCE(_38Code_17038)){
            _29919 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29919 = 1;
    }
    _29920 = _29919 + 1;
    _29919 = NOVALUE;
    _43backpatch(_prev_false2_58282, _29920);
    _29920 = NOVALUE;
L12: 

    /** 		push_temps( temps )*/
    RefDS(_temps_58291);
    _43push_temps(_temps_58291);

    /** 		if tok[T_ID] = ELSE then*/
    _2 = (int)SEQ_PTR(_tok_58280);
    _29921 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29921, 23)){
        _29921 = NOVALUE;
        goto L13; // [697] 708
    }
    _29921 = NOVALUE;

    /** 			Statement_list()*/
    _41Statement_list();
    goto L14; // [705] 775
L13: 

    /** 			putback(tok)*/
    Ref(_tok_58280);
    _41putback(_tok_58280);
    goto L14; // [714] 775
LF: 

    /** 		putback(tok)*/
    Ref(_tok_58280);
    _41putback(_tok_58280);

    /** 		if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L15; // [726] 737
    }
    else{
    }

    /** 			emit_op(NOP1)*/
    _43emit_op(159);
L15: 

    /** 		backpatch(prev_false, length(Code)+1)*/
    if (IS_SEQUENCE(_38Code_17038)){
            _29923 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29923 = 1;
    }
    _29924 = _29923 + 1;
    _29923 = NOVALUE;
    _43backpatch(_prev_false_58281, _29924);
    _29924 = NOVALUE;

    /** 		if prev_false2 != 0 then*/
    if (_prev_false2_58282 == 0)
    goto L16; // [754] 774

    /** 			backpatch(prev_false2, length(Code)+1)*/
    if (IS_SEQUENCE(_38Code_17038)){
            _29926 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29926 = 1;
    }
    _29927 = _29926 + 1;
    _29926 = NOVALUE;
    _43backpatch(_prev_false2_58282, _29927);
    _29927 = NOVALUE;
L16: 
L14: 

    /** 	tok_match(END)*/
    _41tok_match(402, 0);

    /** 	tok_match(IF, END)*/
    _41tok_match(20, 402);

    /** 	End_block( IF )*/
    _68End_block(20);

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L17; // [804] 827
    }
    else{
    }

    /** 		if length(break_list) > elist_base then*/
    if (IS_SEQUENCE(_41break_list_55140)){
            _29928 = SEQ_PTR(_41break_list_55140)->length;
    }
    else {
        _29928 = 1;
    }
    if (_29928 <= _elist_base_58283)
    goto L18; // [814] 826

    /** 			emit_op(NOP1)  -- to emit label here*/
    _43emit_op(159);
L18: 
L17: 

    /** 	PatchEList(elist_base)*/
    _41PatchEList(_elist_base_58283);

    /** 	if_labels = if_labels[1..$-1]*/
    if (IS_SEQUENCE(_41if_labels_55151)){
            _29930 = SEQ_PTR(_41if_labels_55151)->length;
    }
    else {
        _29930 = 1;
    }
    _29931 = _29930 - 1;
    _29930 = NOVALUE;
    rhs_slice_target = (object_ptr)&_41if_labels_55151;
    RHS_Slice(_41if_labels_55151, 1, _29931);

    /** 	block_index -= 1*/
    _41block_index_55153 = _41block_index_55153 - 1;

    /** 	if_stack = if_stack[1..$-1]*/
    if (IS_SEQUENCE(_41if_stack_55157)){
            _29934 = SEQ_PTR(_41if_stack_55157)->length;
    }
    else {
        _29934 = 1;
    }
    _29935 = _29934 - 1;
    _29934 = NOVALUE;
    rhs_slice_target = (object_ptr)&_41if_stack_55157;
    RHS_Slice(_41if_stack_55157, 1, _29935);

    /** end procedure*/
    DeRef(_tok_58280);
    DeRef(_temps_58291);
    _29911 = NOVALUE;
    DeRef(_29909);
    _29909 = NOVALUE;
    _29931 = NOVALUE;
    _29935 = NOVALUE;
    return;
    ;
}


void _41exit_loop(int _exit_base_58468)
{
    int _29950 = NOVALUE;
    int _29949 = NOVALUE;
    int _29947 = NOVALUE;
    int _29946 = NOVALUE;
    int _29944 = NOVALUE;
    int _29943 = NOVALUE;
    int _29941 = NOVALUE;
    int _29940 = NOVALUE;
    int _29938 = NOVALUE;
    int _29937 = NOVALUE;
    int _0, _1, _2;
    

    /** 	PatchXList(exit_base)*/
    _41PatchXList(_exit_base_58468);

    /** 	loop_labels = loop_labels[1..$-1]*/
    if (IS_SEQUENCE(_41loop_labels_55150)){
            _29937 = SEQ_PTR(_41loop_labels_55150)->length;
    }
    else {
        _29937 = 1;
    }
    _29938 = _29937 - 1;
    _29937 = NOVALUE;
    rhs_slice_target = (object_ptr)&_41loop_labels_55150;
    RHS_Slice(_41loop_labels_55150, 1, _29938);

    /** 	loop_stack = loop_stack[1..$-1]*/
    if (IS_SEQUENCE(_41loop_stack_55156)){
            _29940 = SEQ_PTR(_41loop_stack_55156)->length;
    }
    else {
        _29940 = 1;
    }
    _29941 = _29940 - 1;
    _29940 = NOVALUE;
    rhs_slice_target = (object_ptr)&_41loop_stack_55156;
    RHS_Slice(_41loop_stack_55156, 1, _29941);

    /** 	continue_addr = continue_addr[1..$-1]*/
    if (IS_SEQUENCE(_41continue_addr_55147)){
            _29943 = SEQ_PTR(_41continue_addr_55147)->length;
    }
    else {
        _29943 = 1;
    }
    _29944 = _29943 - 1;
    _29943 = NOVALUE;
    rhs_slice_target = (object_ptr)&_41continue_addr_55147;
    RHS_Slice(_41continue_addr_55147, 1, _29944);

    /** 	retry_addr = retry_addr[1..$-1]*/
    if (IS_SEQUENCE(_41retry_addr_55148)){
            _29946 = SEQ_PTR(_41retry_addr_55148)->length;
    }
    else {
        _29946 = 1;
    }
    _29947 = _29946 - 1;
    _29946 = NOVALUE;
    rhs_slice_target = (object_ptr)&_41retry_addr_55148;
    RHS_Slice(_41retry_addr_55148, 1, _29947);

    /** 	entry_addr = entry_addr[1..$-1]*/
    if (IS_SEQUENCE(_41entry_addr_55146)){
            _29949 = SEQ_PTR(_41entry_addr_55146)->length;
    }
    else {
        _29949 = 1;
    }
    _29950 = _29949 - 1;
    _29949 = NOVALUE;
    rhs_slice_target = (object_ptr)&_41entry_addr_55146;
    RHS_Slice(_41entry_addr_55146, 1, _29950);

    /** 	block_index -= 1*/
    _41block_index_55153 = _41block_index_55153 - 1;

    /** end procedure*/
    _29938 = NOVALUE;
    _29941 = NOVALUE;
    _29944 = NOVALUE;
    _29947 = NOVALUE;
    _29950 = NOVALUE;
    return;
    ;
}


void _41push_switch()
{
    int _29954 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if_stack &= SWITCH*/
    Append(&_41if_stack_55157, _41if_stack_55157, 185);

    /** 	switch_stack = append( switch_stack, { {}, {}, 0, 0, 0, 0 })*/
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    RefDSn(_22663, 2);
    *((int *)(_2+4)) = _22663;
    *((int *)(_2+8)) = _22663;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 0;
    *((int *)(_2+20)) = 0;
    *((int *)(_2+24)) = 0;
    _29954 = MAKE_SEQ(_1);
    RefDS(_29954);
    Append(&_41switch_stack_55357, _41switch_stack_55357, _29954);
    DeRefDS(_29954);
    _29954 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


void _41pop_switch(int _break_base_58493)
{
    int _29969 = NOVALUE;
    int _29968 = NOVALUE;
    int _29966 = NOVALUE;
    int _29965 = NOVALUE;
    int _29963 = NOVALUE;
    int _29962 = NOVALUE;
    int _29960 = NOVALUE;
    int _29959 = NOVALUE;
    int _29958 = NOVALUE;
    int _29957 = NOVALUE;
    int _0, _1, _2;
    

    /** 	PatchEList( break_base )*/
    _41PatchEList(_break_base_58493);

    /** 	block_index -= 1*/
    _41block_index_55153 = _41block_index_55153 - 1;

    /** 	if length(switch_stack[$][SWITCH_CASES]) > 0 then*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _29957 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _29957 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    _29958 = (int)*(((s1_ptr)_2)->base + _29957);
    _2 = (int)SEQ_PTR(_29958);
    _29959 = (int)*(((s1_ptr)_2)->base + 1);
    _29958 = NOVALUE;
    if (IS_SEQUENCE(_29959)){
            _29960 = SEQ_PTR(_29959)->length;
    }
    else {
        _29960 = 1;
    }
    _29959 = NOVALUE;
    if (_29960 <= 0)
    goto L1; // [36] 48

    /** 		End_block( CASE )*/
    _68End_block(186);
L1: 

    /** 	if_labels = if_labels[1..$-1]*/
    if (IS_SEQUENCE(_41if_labels_55151)){
            _29962 = SEQ_PTR(_41if_labels_55151)->length;
    }
    else {
        _29962 = 1;
    }
    _29963 = _29962 - 1;
    _29962 = NOVALUE;
    rhs_slice_target = (object_ptr)&_41if_labels_55151;
    RHS_Slice(_41if_labels_55151, 1, _29963);

    /** 	if_stack  = if_stack[1..$-1]*/
    if (IS_SEQUENCE(_41if_stack_55157)){
            _29965 = SEQ_PTR(_41if_stack_55157)->length;
    }
    else {
        _29965 = 1;
    }
    _29966 = _29965 - 1;
    _29965 = NOVALUE;
    rhs_slice_target = (object_ptr)&_41if_stack_55157;
    RHS_Slice(_41if_stack_55157, 1, _29966);

    /** 	switch_stack  = switch_stack[1..$-1]*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _29968 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _29968 = 1;
    }
    _29969 = _29968 - 1;
    _29968 = NOVALUE;
    rhs_slice_target = (object_ptr)&_41switch_stack_55357;
    RHS_Slice(_41switch_stack_55357, 1, _29969);

    /** end procedure*/
    _29959 = NOVALUE;
    _29963 = NOVALUE;
    _29966 = NOVALUE;
    _29969 = NOVALUE;
    return;
    ;
}


void _41add_case(int _sym_58514, int _sign_58515)
{
    int _29995 = NOVALUE;
    int _29994 = NOVALUE;
    int _29993 = NOVALUE;
    int _29992 = NOVALUE;
    int _29991 = NOVALUE;
    int _29990 = NOVALUE;
    int _29989 = NOVALUE;
    int _29988 = NOVALUE;
    int _29986 = NOVALUE;
    int _29985 = NOVALUE;
    int _29984 = NOVALUE;
    int _29983 = NOVALUE;
    int _29982 = NOVALUE;
    int _29981 = NOVALUE;
    int _29979 = NOVALUE;
    int _29978 = NOVALUE;
    int _29976 = NOVALUE;
    int _29975 = NOVALUE;
    int _29974 = NOVALUE;
    int _29973 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	if sign < 0 then*/
    if (_sign_58515 >= 0)
    goto L1; // [5] 15

    /** 		sym = -sym*/
    _0 = _sym_58514;
    if (IS_ATOM_INT(_sym_58514)) {
        if ((unsigned long)_sym_58514 == 0xC0000000)
        _sym_58514 = (int)NewDouble((double)-0xC0000000);
        else
        _sym_58514 = - _sym_58514;
    }
    else {
        _sym_58514 = unary_op(UMINUS, _sym_58514);
    }
    DeRefi(_0);
L1: 

    /** 	if find(sym, switch_stack[$][SWITCH_CASES] ) = 0 then*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _29973 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _29973 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    _29974 = (int)*(((s1_ptr)_2)->base + _29973);
    _2 = (int)SEQ_PTR(_29974);
    _29975 = (int)*(((s1_ptr)_2)->base + 1);
    _29974 = NOVALUE;
    _29976 = find_from(_sym_58514, _29975, 1);
    _29975 = NOVALUE;
    if (_29976 != 0)
    goto L2; // [37] 154

    /** 		switch_stack[$][SWITCH_CASES]       = append( switch_stack[$][SWITCH_CASES], sym )*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _29978 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _29978 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _41switch_stack_55357 = MAKE_SEQ(_2);
    }
    _3 = (int)(_29978 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _29981 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _29981 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    _29982 = (int)*(((s1_ptr)_2)->base + _29981);
    _2 = (int)SEQ_PTR(_29982);
    _29983 = (int)*(((s1_ptr)_2)->base + 1);
    _29982 = NOVALUE;
    Ref(_sym_58514);
    Append(&_29984, _29983, _sym_58514);
    _29983 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _29984;
    if( _1 != _29984 ){
        DeRef(_1);
    }
    _29984 = NOVALUE;
    _29979 = NOVALUE;

    /** 		switch_stack[$][SWITCH_JUMP_TABLE] &= length(Code) + 1*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _29985 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _29985 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _41switch_stack_55357 = MAKE_SEQ(_2);
    }
    _3 = (int)(_29985 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_38Code_17038)){
            _29988 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _29988 = 1;
    }
    _29989 = _29988 + 1;
    _29988 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    _29990 = (int)*(((s1_ptr)_2)->base + 2);
    _29986 = NOVALUE;
    if (IS_SEQUENCE(_29990) && IS_ATOM(_29989)) {
        Append(&_29991, _29990, _29989);
    }
    else if (IS_ATOM(_29990) && IS_SEQUENCE(_29989)) {
    }
    else {
        Concat((object_ptr)&_29991, _29990, _29989);
        _29990 = NOVALUE;
    }
    _29990 = NOVALUE;
    _29989 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _29991;
    if( _1 != _29991 ){
        DeRef(_1);
    }
    _29991 = NOVALUE;
    _29986 = NOVALUE;

    /** 		if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L3; // [117] 162
    }
    else{
    }

    /** 			emit_addr( CASE )*/
    _43emit_addr(186);

    /** 			emit_addr( length( switch_stack[$][SWITCH_CASES] ) )*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _29992 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _29992 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    _29993 = (int)*(((s1_ptr)_2)->base + _29992);
    _2 = (int)SEQ_PTR(_29993);
    _29994 = (int)*(((s1_ptr)_2)->base + 1);
    _29993 = NOVALUE;
    if (IS_SEQUENCE(_29994)){
            _29995 = SEQ_PTR(_29994)->length;
    }
    else {
        _29995 = 1;
    }
    _29994 = NOVALUE;
    _43emit_addr(_29995);
    _29995 = NOVALUE;
    goto L3; // [151] 162
L2: 

    /** 		CompileErr( 63 )*/
    RefDS(_22663);
    _46CompileErr(63, _22663, 0);
L3: 

    /** end procedure*/
    DeRef(_sym_58514);
    _29994 = NOVALUE;
    return;
    ;
}


void _41case_else()
{
    int _30003 = NOVALUE;
    int _30002 = NOVALUE;
    int _30000 = NOVALUE;
    int _29999 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	switch_stack[$][SWITCH_ELSE] = length(Code) + 1*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _29999 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _29999 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _41switch_stack_55357 = MAKE_SEQ(_2);
    }
    _3 = (int)(_29999 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_38Code_17038)){
            _30002 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _30002 = 1;
    }
    _30003 = _30002 + 1;
    _30002 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _30003;
    if( _1 != _30003 ){
        DeRef(_1);
    }
    _30003 = NOVALUE;
    _30000 = NOVALUE;

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L1; // [32] 48
    }
    else{
    }

    /** 		emit_addr( CASE )*/
    _43emit_addr(186);

    /** 		emit_addr( 0 )*/
    _43emit_addr(0);
L1: 

    /** end procedure*/
    return;
    ;
}


void _41Case_statement()
{
    int _else_case_2__tmp_at149_58611 = NOVALUE;
    int _else_case_1__tmp_at149_58610 = NOVALUE;
    int _else_case_inlined_else_case_at_149_58609 = NOVALUE;
    int _tok_58573 = NOVALUE;
    int _condition_58575 = NOVALUE;
    int _start_line_58604 = NOVALUE;
    int _sign_58615 = NOVALUE;
    int _fwd_58628 = NOVALUE;
    int _symi_58638 = NOVALUE;
    int _fwdref_58707 = NOVALUE;
    int _32353 = NOVALUE;
    int _30085 = NOVALUE;
    int _30084 = NOVALUE;
    int _30083 = NOVALUE;
    int _30082 = NOVALUE;
    int _30081 = NOVALUE;
    int _30080 = NOVALUE;
    int _30078 = NOVALUE;
    int _30077 = NOVALUE;
    int _30076 = NOVALUE;
    int _30075 = NOVALUE;
    int _30074 = NOVALUE;
    int _30073 = NOVALUE;
    int _30071 = NOVALUE;
    int _30068 = NOVALUE;
    int _30065 = NOVALUE;
    int _30064 = NOVALUE;
    int _30063 = NOVALUE;
    int _30062 = NOVALUE;
    int _30059 = NOVALUE;
    int _30058 = NOVALUE;
    int _30057 = NOVALUE;
    int _30056 = NOVALUE;
    int _30053 = NOVALUE;
    int _30052 = NOVALUE;
    int _30051 = NOVALUE;
    int _30050 = NOVALUE;
    int _30048 = NOVALUE;
    int _30047 = NOVALUE;
    int _30046 = NOVALUE;
    int _30044 = NOVALUE;
    int _30043 = NOVALUE;
    int _30042 = NOVALUE;
    int _30041 = NOVALUE;
    int _30040 = NOVALUE;
    int _30038 = NOVALUE;
    int _30037 = NOVALUE;
    int _30035 = NOVALUE;
    int _30034 = NOVALUE;
    int _30033 = NOVALUE;
    int _30032 = NOVALUE;
    int _30028 = NOVALUE;
    int _30027 = NOVALUE;
    int _30026 = NOVALUE;
    int _30023 = NOVALUE;
    int _30020 = NOVALUE;
    int _30017 = NOVALUE;
    int _30016 = NOVALUE;
    int _30015 = NOVALUE;
    int _30014 = NOVALUE;
    int _30013 = NOVALUE;
    int _30012 = NOVALUE;
    int _30011 = NOVALUE;
    int _30009 = NOVALUE;
    int _30008 = NOVALUE;
    int _30007 = NOVALUE;
    int _30006 = NOVALUE;
    int _30004 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	if not in_switch() then*/
    _30004 = _41in_switch();
    if (IS_ATOM_INT(_30004)) {
        if (_30004 != 0){
            DeRef(_30004);
            _30004 = NOVALUE;
            goto L1; // [6] 17
        }
    }
    else {
        if (DBL_PTR(_30004)->dbl != 0.0){
            DeRef(_30004);
            _30004 = NOVALUE;
            goto L1; // [6] 17
        }
    }
    DeRef(_30004);
    _30004 = NOVALUE;

    /** 		CompileErr( 34 )*/
    RefDS(_22663);
    _46CompileErr(34, _22663, 0);
L1: 

    /** 	if length(switch_stack[$][SWITCH_CASES]) > 0 then*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _30006 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _30006 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    _30007 = (int)*(((s1_ptr)_2)->base + _30006);
    _2 = (int)SEQ_PTR(_30007);
    _30008 = (int)*(((s1_ptr)_2)->base + 1);
    _30007 = NOVALUE;
    if (IS_SEQUENCE(_30008)){
            _30009 = SEQ_PTR(_30008)->length;
    }
    else {
        _30009 = 1;
    }
    _30008 = NOVALUE;
    if (_30009 <= 0)
    goto L2; // [37] 105

    /** 		Sibling_block( CASE )*/
    _68Sibling_block(186);

    /** 		if not switch_stack[$][SWITCH_FALLTHRU] and*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _30011 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _30011 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    _30012 = (int)*(((s1_ptr)_2)->base + _30011);
    _2 = (int)SEQ_PTR(_30012);
    _30013 = (int)*(((s1_ptr)_2)->base + 5);
    _30012 = NOVALUE;
    if (IS_ATOM_INT(_30013)) {
        _30014 = (_30013 == 0);
    }
    else {
        _30014 = unary_op(NOT, _30013);
    }
    _30013 = NOVALUE;
    if (IS_ATOM_INT(_30014)) {
        if (_30014 == 0) {
            goto L3; // [68] 114
        }
    }
    else {
        if (DBL_PTR(_30014)->dbl == 0.0) {
            goto L3; // [68] 114
        }
    }
    _30016 = (_41fallthru_case_58569 == 0);
    if (_30016 == 0)
    {
        DeRef(_30016);
        _30016 = NOVALUE;
        goto L3; // [78] 114
    }
    else{
        DeRef(_30016);
        _30016 = NOVALUE;
    }

    /** 			putback( {CASE, 0} )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 186;
    ((int *)_2)[2] = 0;
    _30017 = MAKE_SEQ(_1);
    _41putback(_30017);
    _30017 = NOVALUE;

    /** 			Break_statement()*/
    _41Break_statement();

    /** 			tok = next_token()*/
    _0 = _tok_58573;
    _tok_58573 = _41next_token();
    DeRef(_0);
    goto L3; // [102] 114
L2: 

    /** 		Start_block( CASE )*/
    _68Start_block(186, 0);
L3: 

    /** 	StartSourceLine(TRUE, , COVERAGE_SUPPRESS)*/
    _43StartSourceLine(_9TRUE_428, 0, 1);

    /** 	fallthru_case = 0*/
    _41fallthru_case_58569 = 0;

    /** 	integer start_line = line_number*/
    _start_line_58604 = _38line_number_16947;

    /** 	while 1 do*/
L4: 

    /** 		if else_case() then*/

    /** 	return switch_stack[$][SWITCH_ELSE]*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _else_case_1__tmp_at149_58610 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _else_case_1__tmp_at149_58610 = 1;
    }
    DeRef(_else_case_2__tmp_at149_58611);
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    _else_case_2__tmp_at149_58611 = (int)*(((s1_ptr)_2)->base + _else_case_1__tmp_at149_58610);
    RefDS(_else_case_2__tmp_at149_58611);
    DeRef(_else_case_inlined_else_case_at_149_58609);
    _2 = (int)SEQ_PTR(_else_case_2__tmp_at149_58611);
    _else_case_inlined_else_case_at_149_58609 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_else_case_inlined_else_case_at_149_58609);
    DeRef(_else_case_2__tmp_at149_58611);
    _else_case_2__tmp_at149_58611 = NOVALUE;
    if (_else_case_inlined_else_case_at_149_58609 == 0) {
        goto L5; // [166] 177
    }
    else {
        if (!IS_ATOM_INT(_else_case_inlined_else_case_at_149_58609) && DBL_PTR(_else_case_inlined_else_case_at_149_58609)->dbl == 0.0){
            goto L5; // [166] 177
        }
    }

    /** 			CompileErr( 33 )*/
    RefDS(_22663);
    _46CompileErr(33, _22663, 0);
L5: 

    /** 		maybe_namespace()*/
    _62maybe_namespace();

    /** 		tok = next_token()*/
    _0 = _tok_58573;
    _tok_58573 = _41next_token();
    DeRef(_0);

    /** 		integer sign = 1*/
    _sign_58615 = 1;

    /** 		if tok[T_ID] = MINUS then*/
    _2 = (int)SEQ_PTR(_tok_58573);
    _30020 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30020, 10)){
        _30020 = NOVALUE;
        goto L6; // [201] 218
    }
    _30020 = NOVALUE;

    /** 			sign = -1*/
    _sign_58615 = -1;

    /** 			tok = next_token()*/
    _0 = _tok_58573;
    _tok_58573 = _41next_token();
    DeRef(_0);
    goto L7; // [215] 239
L6: 

    /** 		elsif tok[T_ID] = PLUS then*/
    _2 = (int)SEQ_PTR(_tok_58573);
    _30023 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30023, 11)){
        _30023 = NOVALUE;
        goto L8; // [228] 238
    }
    _30023 = NOVALUE;

    /** 			tok = next_token()*/
    _0 = _tok_58573;
    _tok_58573 = _41next_token();
    DeRef(_0);
L8: 
L7: 

    /** 		integer fwd*/

    /** 		if not find( tok[T_ID], {ATOM, STRING, ELSE} ) then*/
    _2 = (int)SEQ_PTR(_tok_58573);
    _30026 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _39ATOM_16394;
    *((int *)(_2+8)) = 503;
    *((int *)(_2+12)) = 23;
    _30027 = MAKE_SEQ(_1);
    _30028 = find_from(_30026, _30027, 1);
    _30026 = NOVALUE;
    DeRefDS(_30027);
    _30027 = NOVALUE;
    if (_30028 != 0)
    goto L9; // [266] 441
    _30028 = NOVALUE;

    /** 			integer symi = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_58573);
    _symi_58638 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_symi_58638)){
        _symi_58638 = (long)DBL_PTR(_symi_58638)->dbl;
    }

    /** 			fwd = -1*/
    _fwd_58628 = -1;

    /** 			if symi > 0 then*/
    if (_symi_58638 <= 0)
    goto LA; // [286] 436

    /** 				if find(tok[T_ID] , VAR_TOKS) then*/
    _2 = (int)SEQ_PTR(_tok_58573);
    _30032 = (int)*(((s1_ptr)_2)->base + 1);
    _30033 = find_from(_30032, _39VAR_TOKS_16557, 1);
    _30032 = NOVALUE;
    if (_30033 == 0)
    {
        _30033 = NOVALUE;
        goto LB; // [305] 435
    }
    else{
        _30033 = NOVALUE;
    }

    /** 					if SymTab[symi][S_SCOPE] = SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30034 = (int)*(((s1_ptr)_2)->base + _symi_58638);
    _2 = (int)SEQ_PTR(_30034);
    _30035 = (int)*(((s1_ptr)_2)->base + 4);
    _30034 = NOVALUE;
    if (binary_op_a(NOTEQ, _30035, 9)){
        _30035 = NOVALUE;
        goto LC; // [324] 336
    }
    _30035 = NOVALUE;

    /** 						fwd = symi*/
    _fwd_58628 = _symi_58638;
    goto LD; // [333] 434
LC: 

    /** 					elsif SymTab[symi][S_MODE] = M_CONSTANT then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30037 = (int)*(((s1_ptr)_2)->base + _symi_58638);
    _2 = (int)SEQ_PTR(_30037);
    _30038 = (int)*(((s1_ptr)_2)->base + 3);
    _30037 = NOVALUE;
    if (binary_op_a(NOTEQ, _30038, 2)){
        _30038 = NOVALUE;
        goto LE; // [352] 433
    }
    _30038 = NOVALUE;

    /** 						fwd = 0*/
    _fwd_58628 = 0;

    /** 						if SymTab[symi][S_CODE] then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30040 = (int)*(((s1_ptr)_2)->base + _symi_58638);
    _2 = (int)SEQ_PTR(_30040);
    if (!IS_ATOM_INT(_38S_CODE_16610)){
        _30041 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    }
    else{
        _30041 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
    }
    _30040 = NOVALUE;
    if (_30041 == 0) {
        _30041 = NOVALUE;
        goto LF; // [375] 399
    }
    else {
        if (!IS_ATOM_INT(_30041) && DBL_PTR(_30041)->dbl == 0.0){
            _30041 = NOVALUE;
            goto LF; // [375] 399
        }
        _30041 = NOVALUE;
    }
    _30041 = NOVALUE;

    /** 							tok[T_SYM] = SymTab[symi][S_CODE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30042 = (int)*(((s1_ptr)_2)->base + _symi_58638);
    _2 = (int)SEQ_PTR(_30042);
    if (!IS_ATOM_INT(_38S_CODE_16610)){
        _30043 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    }
    else{
        _30043 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
    }
    _30042 = NOVALUE;
    Ref(_30043);
    _2 = (int)SEQ_PTR(_tok_58573);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _tok_58573 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _30043;
    if( _1 != _30043 ){
        DeRef(_1);
    }
    _30043 = NOVALUE;
LF: 

    /** 						SymTab[symi][S_USAGE] = or_bits( SymTab[symi][S_USAGE], U_READ )*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_symi_58638 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30046 = (int)*(((s1_ptr)_2)->base + _symi_58638);
    _2 = (int)SEQ_PTR(_30046);
    _30047 = (int)*(((s1_ptr)_2)->base + 5);
    _30046 = NOVALUE;
    if (IS_ATOM_INT(_30047)) {
        {unsigned long tu;
             tu = (unsigned long)_30047 | (unsigned long)1;
             _30048 = MAKE_UINT(tu);
        }
    }
    else {
        _30048 = binary_op(OR_BITS, _30047, 1);
    }
    _30047 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _30048;
    if( _1 != _30048 ){
        DeRef(_1);
    }
    _30048 = NOVALUE;
    _30044 = NOVALUE;
LE: 
LD: 
LB: 
LA: 
    goto L10; // [438] 447
L9: 

    /** 			fwd = 0*/
    _fwd_58628 = 0;
L10: 

    /** 		if fwd < 0 then*/
    if (_fwd_58628 >= 0)
    goto L11; // [451] 477

    /** 			CompileErr( 91, {find_category(tok[T_ID])})*/
    _2 = (int)SEQ_PTR(_tok_58573);
    _30050 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_30050);
    _30051 = _65find_category(_30050);
    _30050 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _30051;
    _30052 = MAKE_SEQ(_1);
    _30051 = NOVALUE;
    _46CompileErr(91, _30052, 0);
    _30052 = NOVALUE;
L11: 

    /** 		if tok[T_ID] = ELSE then*/
    _2 = (int)SEQ_PTR(_tok_58573);
    _30053 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30053, 23)){
        _30053 = NOVALUE;
        goto L12; // [487] 550
    }
    _30053 = NOVALUE;

    /** 			if sign = -1 then*/
    if (_sign_58615 != -1)
    goto L13; // [493] 505

    /** 				CompileErr( 71 )*/
    RefDS(_22663);
    _46CompileErr(71, _22663, 0);
L13: 

    /** 			if length(switch_stack[$][SWITCH_CASES]) = 0 then*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _30056 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _30056 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    _30057 = (int)*(((s1_ptr)_2)->base + _30056);
    _2 = (int)SEQ_PTR(_30057);
    _30058 = (int)*(((s1_ptr)_2)->base + 1);
    _30057 = NOVALUE;
    if (IS_SEQUENCE(_30058)){
            _30059 = SEQ_PTR(_30058)->length;
    }
    else {
        _30059 = 1;
    }
    _30058 = NOVALUE;
    if (_30059 != 0)
    goto L14; // [525] 537

    /** 				CompileErr( 44 )*/
    RefDS(_22663);
    _46CompileErr(44, _22663, 0);
L14: 

    /** 			case_else()*/
    _41case_else();

    /** 			exit*/
    goto L15; // [545] 789
    goto L16; // [547] 623
L12: 

    /** 		elsif fwd then*/
    if (_fwd_58628 == 0)
    {
        goto L17; // [552] 606
    }
    else{
    }

    /** 			integer fwdref = new_forward_reference( CASE, fwd )*/
    DeRef(_32353);
    _32353 = 186;
    _fwdref_58707 = _40new_forward_reference(186, _fwd_58628, 186);
    _32353 = NOVALUE;
    if (!IS_ATOM_INT(_fwdref_58707)) {
        _1 = (long)(DBL_PTR(_fwdref_58707)->dbl);
        if (UNIQUE(DBL_PTR(_fwdref_58707)) && (DBL_PTR(_fwdref_58707)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fwdref_58707);
        _fwdref_58707 = _1;
    }

    /** 			add_case( {fwdref}, sign )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _fwdref_58707;
    _30062 = MAKE_SEQ(_1);
    _41add_case(_30062, _sign_58615);
    _30062 = NOVALUE;

    /** 			fwd:set_data( fwdref, switch_stack[$][SWITCH_PC] )*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _30063 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _30063 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    _30064 = (int)*(((s1_ptr)_2)->base + _30063);
    _2 = (int)SEQ_PTR(_30064);
    _30065 = (int)*(((s1_ptr)_2)->base + 4);
    _30064 = NOVALUE;
    Ref(_30065);
    _40set_data(_fwdref_58707, _30065);
    _30065 = NOVALUE;
    goto L16; // [603] 623
L17: 

    /** 			condition = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_58573);
    _condition_58575 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_condition_58575)){
        _condition_58575 = (long)DBL_PTR(_condition_58575)->dbl;
    }

    /** 			add_case( condition, sign )*/
    _41add_case(_condition_58575, _sign_58615);
L16: 

    /** 		tok = next_token()*/
    _0 = _tok_58573;
    _tok_58573 = _41next_token();
    DeRef(_0);

    /** 		if tok[T_ID] = THEN then*/
    _2 = (int)SEQ_PTR(_tok_58573);
    _30068 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30068, 410)){
        _30068 = NOVALUE;
        goto L18; // [638] 744
    }
    _30068 = NOVALUE;

    /** 			tok = next_token()*/
    _0 = _tok_58573;
    _tok_58573 = _41next_token();
    DeRef(_0);

    /** 			if tok[T_ID] = CASE then*/
    _2 = (int)SEQ_PTR(_tok_58573);
    _30071 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30071, 186)){
        _30071 = NOVALUE;
        goto L19; // [657] 729
    }
    _30071 = NOVALUE;

    /** 				if switch_stack[$][SWITCH_FALLTHRU] then*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _30073 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _30073 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    _30074 = (int)*(((s1_ptr)_2)->base + _30073);
    _2 = (int)SEQ_PTR(_30074);
    _30075 = (int)*(((s1_ptr)_2)->base + 5);
    _30074 = NOVALUE;
    if (_30075 == 0) {
        _30075 = NOVALUE;
        goto L1A; // [678] 693
    }
    else {
        if (!IS_ATOM_INT(_30075) && DBL_PTR(_30075)->dbl == 0.0){
            _30075 = NOVALUE;
            goto L1A; // [678] 693
        }
        _30075 = NOVALUE;
    }
    _30075 = NOVALUE;

    /** 					start_line = line_number*/
    _start_line_58604 = _38line_number_16947;
    goto L1B; // [690] 782
L1A: 

    /** 					putback( tok )*/
    Ref(_tok_58573);
    _41putback(_tok_58573);

    /** 					Warning(220, empty_case_warning_flag,*/
    _2 = (int)SEQ_PTR(_35known_files_15596);
    _30076 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    Ref(_30076);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _30076;
    ((int *)_2)[2] = _start_line_58604;
    _30077 = MAKE_SEQ(_1);
    _30076 = NOVALUE;
    _46Warning(220, 2048, _30077);
    _30077 = NOVALUE;

    /** 					exit*/
    goto L15; // [723] 789
    goto L1B; // [726] 782
L19: 

    /** 				putback( tok )*/
    Ref(_tok_58573);
    _41putback(_tok_58573);

    /** 				exit*/
    goto L15; // [738] 789
    goto L1B; // [741] 782
L18: 

    /** 		elsif tok[T_ID] != COMMA then*/
    _2 = (int)SEQ_PTR(_tok_58573);
    _30078 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _30078, -30)){
        _30078 = NOVALUE;
        goto L1C; // [754] 781
    }
    _30078 = NOVALUE;

    /** 			CompileErr(66,{LexName(tok[T_ID])})*/
    _2 = (int)SEQ_PTR(_tok_58573);
    _30080 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_30080);
    RefDS(_27166);
    _30081 = _43LexName(_30080, _27166);
    _30080 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _30081;
    _30082 = MAKE_SEQ(_1);
    _30081 = NOVALUE;
    _46CompileErr(66, _30082, 0);
    _30082 = NOVALUE;
L1C: 
L1B: 

    /** 	end while*/
    goto L4; // [786] 144
L15: 

    /** 	StartSourceLine( TRUE )*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 	emit_temp( switch_stack[$][SWITCH_VALUE], NEW_REFERENCE )*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _30083 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _30083 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    _30084 = (int)*(((s1_ptr)_2)->base + _30083);
    _2 = (int)SEQ_PTR(_30084);
    _30085 = (int)*(((s1_ptr)_2)->base + 6);
    _30084 = NOVALUE;
    Ref(_30085);
    _43emit_temp(_30085, 1);
    _30085 = NOVALUE;

    /** 	flush_temps()*/
    RefDS(_22663);
    _43flush_temps(_22663);

    /** end procedure*/
    DeRef(_tok_58573);
    _30008 = NOVALUE;
    DeRef(_30014);
    _30014 = NOVALUE;
    _30058 = NOVALUE;
    return;
    ;
}


void _41Fallthru_statement()
{
    int _30086 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not in_switch() then*/
    _30086 = _41in_switch();
    if (IS_ATOM_INT(_30086)) {
        if (_30086 != 0){
            DeRef(_30086);
            _30086 = NOVALUE;
            goto L1; // [6] 17
        }
    }
    else {
        if (DBL_PTR(_30086)->dbl != 0.0){
            DeRef(_30086);
            _30086 = NOVALUE;
            goto L1; // [6] 17
        }
    }
    DeRef(_30086);
    _30086 = NOVALUE;

    /** 		CompileErr( 22 )*/
    RefDS(_22663);
    _46CompileErr(22, _22663, 0);
L1: 

    /** 	tok_match( CASE )*/
    _41tok_match(186, 0);

    /** 	fallthru_case = 1*/
    _41fallthru_case_58569 = 1;

    /** 	Case_statement()*/
    _41Case_statement();

    /** end procedure*/
    return;
    ;
}


void _41update_translator_info(int _sym_58773, int _all_ints_58774, int _has_integer_58775, int _has_atom_58776, int _has_sequence_58777)
{
    int _30111 = NOVALUE;
    int _30109 = NOVALUE;
    int _30107 = NOVALUE;
    int _30105 = NOVALUE;
    int _30103 = NOVALUE;
    int _30102 = NOVALUE;
    int _30100 = NOVALUE;
    int _30098 = NOVALUE;
    int _30097 = NOVALUE;
    int _30096 = NOVALUE;
    int _30095 = NOVALUE;
    int _30094 = NOVALUE;
    int _30092 = NOVALUE;
    int _30090 = NOVALUE;
    int _30088 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	SymTab[sym][S_MODE] = M_TEMP    -- override CONSTANT for compile*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_58773 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 3;
    DeRef(_1);
    _30088 = NOVALUE;

    /** 	SymTab[sym][S_GTYPE] = TYPE_SEQUENCE*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_58773 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = 8;
    DeRef(_1);
    _30090 = NOVALUE;

    /** 	SymTab[sym][S_SEQ_LEN] = length( SymTab[sym][S_OBJ] )*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_58773 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30094 = (int)*(((s1_ptr)_2)->base + _sym_58773);
    _2 = (int)SEQ_PTR(_30094);
    _30095 = (int)*(((s1_ptr)_2)->base + 1);
    _30094 = NOVALUE;
    if (IS_SEQUENCE(_30095)){
            _30096 = SEQ_PTR(_30095)->length;
    }
    else {
        _30096 = 1;
    }
    _30095 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _30096;
    if( _1 != _30096 ){
        DeRef(_1);
    }
    _30096 = NOVALUE;
    _30092 = NOVALUE;

    /** 	if SymTab[sym][S_SEQ_LEN] > 0 then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30097 = (int)*(((s1_ptr)_2)->base + _sym_58773);
    _2 = (int)SEQ_PTR(_30097);
    _30098 = (int)*(((s1_ptr)_2)->base + 32);
    _30097 = NOVALUE;
    if (binary_op_a(LESSEQ, _30098, 0)){
        _30098 = NOVALUE;
        goto L1; // [89] 198
    }
    _30098 = NOVALUE;

    /** 		if all_ints then*/
    if (_all_ints_58774 == 0)
    {
        goto L2; // [95] 118
    }
    else{
    }

    /** 			SymTab[sym][S_SEQ_ELEM] = TYPE_INTEGER*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_58773 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _30100 = NOVALUE;
    goto L3; // [115] 216
L2: 

    /** 		elsif has_atom + has_sequence + has_integer > 1 then*/
    _30102 = _has_atom_58776 + _has_sequence_58777;
    if ((long)((unsigned long)_30102 + (unsigned long)HIGH_BITS) >= 0) 
    _30102 = NewDouble((double)_30102);
    if (IS_ATOM_INT(_30102)) {
        _30103 = _30102 + _has_integer_58775;
        if ((long)((unsigned long)_30103 + (unsigned long)HIGH_BITS) >= 0) 
        _30103 = NewDouble((double)_30103);
    }
    else {
        _30103 = NewDouble(DBL_PTR(_30102)->dbl + (double)_has_integer_58775);
    }
    DeRef(_30102);
    _30102 = NOVALUE;
    if (binary_op_a(LESSEQ, _30103, 1)){
        DeRef(_30103);
        _30103 = NOVALUE;
        goto L4; // [128] 152
    }
    DeRef(_30103);
    _30103 = NOVALUE;

    /** 			SymTab[sym][S_SEQ_ELEM] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_58773 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);
    _30105 = NOVALUE;
    goto L3; // [149] 216
L4: 

    /** 		elsif has_atom then*/
    if (_has_atom_58776 == 0)
    {
        goto L5; // [154] 177
    }
    else{
    }

    /** 			SymTab[sym][S_SEQ_ELEM] = TYPE_ATOM*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_58773 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 4;
    DeRef(_1);
    _30107 = NOVALUE;
    goto L3; // [174] 216
L5: 

    /** 			SymTab[sym][S_SEQ_ELEM] = TYPE_SEQUENCE*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_58773 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 8;
    DeRef(_1);
    _30109 = NOVALUE;
    goto L3; // [195] 216
L1: 

    /** 		SymTab[sym][S_SEQ_ELEM] = TYPE_NULL*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_58773 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _30111 = NOVALUE;
L3: 

    /** end procedure*/
    _30095 = NOVALUE;
    return;
    ;
}


void _41optimize_switch(int _switch_pc_58838, int _else_bp_58839, int _cases_58840, int _jump_table_58841)
{
    int _values_58842 = NOVALUE;
    int _min_58846 = NOVALUE;
    int _max_58848 = NOVALUE;
    int _all_ints_58850 = NOVALUE;
    int _has_integer_58851 = NOVALUE;
    int _has_atom_58852 = NOVALUE;
    int _has_sequence_58853 = NOVALUE;
    int _has_unassigned_58854 = NOVALUE;
    int _has_fwdref_58855 = NOVALUE;
    int _sym_58862 = NOVALUE;
    int _sign_58864 = NOVALUE;
    int _else_target_58928 = NOVALUE;
    int _opcode_58931 = NOVALUE;
    int _delta_58937 = NOVALUE;
    int _jump_58947 = NOVALUE;
    int _switch_table_58951 = NOVALUE;
    int _offset_58954 = NOVALUE;
    int _30189 = NOVALUE;
    int _30188 = NOVALUE;
    int _30187 = NOVALUE;
    int _30186 = NOVALUE;
    int _30184 = NOVALUE;
    int _30182 = NOVALUE;
    int _30179 = NOVALUE;
    int _30178 = NOVALUE;
    int _30177 = NOVALUE;
    int _30176 = NOVALUE;
    int _30175 = NOVALUE;
    int _30174 = NOVALUE;
    int _30173 = NOVALUE;
    int _30170 = NOVALUE;
    int _30168 = NOVALUE;
    int _30167 = NOVALUE;
    int _30166 = NOVALUE;
    int _30165 = NOVALUE;
    int _30164 = NOVALUE;
    int _30163 = NOVALUE;
    int _30162 = NOVALUE;
    int _30158 = NOVALUE;
    int _30157 = NOVALUE;
    int _30155 = NOVALUE;
    int _30154 = NOVALUE;
    int _30153 = NOVALUE;
    int _30152 = NOVALUE;
    int _30151 = NOVALUE;
    int _30150 = NOVALUE;
    int _30149 = NOVALUE;
    int _30148 = NOVALUE;
    int _30147 = NOVALUE;
    int _30146 = NOVALUE;
    int _30144 = NOVALUE;
    int _30143 = NOVALUE;
    int _30139 = NOVALUE;
    int _30136 = NOVALUE;
    int _30135 = NOVALUE;
    int _30134 = NOVALUE;
    int _30132 = NOVALUE;
    int _30131 = NOVALUE;
    int _30130 = NOVALUE;
    int _30129 = NOVALUE;
    int _30128 = NOVALUE;
    int _30126 = NOVALUE;
    int _30125 = NOVALUE;
    int _30124 = NOVALUE;
    int _30120 = NOVALUE;
    int _30119 = NOVALUE;
    int _30118 = NOVALUE;
    int _30114 = NOVALUE;
    int _30113 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	sequence values = switch_stack[$][SWITCH_CASES]*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _30113 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _30113 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    _30114 = (int)*(((s1_ptr)_2)->base + _30113);
    DeRef(_values_58842);
    _2 = (int)SEQ_PTR(_30114);
    _values_58842 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_values_58842);
    _30114 = NOVALUE;

    /** 	atom min =  1e+300*/
    RefDS(_30116);
    DeRef(_min_58846);
    _min_58846 = _30116;

    /** 	atom max = -1e+300*/
    RefDS(_30117);
    DeRef(_max_58848);
    _max_58848 = _30117;

    /** 	integer all_ints = 1*/
    _all_ints_58850 = 1;

    /** 	integer has_integer    = 0*/
    _has_integer_58851 = 0;

    /** 	integer has_atom       = 0*/
    _has_atom_58852 = 0;

    /** 	integer has_sequence   = 0*/
    _has_sequence_58853 = 0;

    /** 	integer has_unassigned = 0*/
    _has_unassigned_58854 = 0;

    /** 	integer has_fwdref     = 0*/
    _has_fwdref_58855 = 0;

    /** 	for i = 1 to length( values ) do*/
    if (IS_SEQUENCE(_values_58842)){
            _30118 = SEQ_PTR(_values_58842)->length;
    }
    else {
        _30118 = 1;
    }
    {
        int _i_58857;
        _i_58857 = 1;
L1: 
        if (_i_58857 > _30118){
            goto L2; // [73] 294
        }

        /** 		if sequence( values[i] ) then*/
        _2 = (int)SEQ_PTR(_values_58842);
        _30119 = (int)*(((s1_ptr)_2)->base + _i_58857);
        _30120 = IS_SEQUENCE(_30119);
        _30119 = NOVALUE;
        if (_30120 == 0)
        {
            _30120 = NOVALUE;
            goto L3; // [89] 102
        }
        else{
            _30120 = NOVALUE;
        }

        /** 			has_fwdref = 1*/
        _has_fwdref_58855 = 1;

        /** 			exit*/
        goto L2; // [99] 294
L3: 

        /** 		integer sym = values[i]*/
        _2 = (int)SEQ_PTR(_values_58842);
        _sym_58862 = (int)*(((s1_ptr)_2)->base + _i_58857);
        if (!IS_ATOM_INT(_sym_58862))
        _sym_58862 = (long)DBL_PTR(_sym_58862)->dbl;

        /** 		integer sign*/

        /** 		if sym < 0 then*/
        if (_sym_58862 >= 0)
        goto L4; // [112] 131

        /** 			sign = -1*/
        _sign_58864 = -1;

        /** 			sym = -sym*/
        _sym_58862 = - _sym_58862;
        goto L5; // [128] 137
L4: 

        /** 			sign = 1*/
        _sign_58864 = 1;
L5: 

        /** 		if not equal(SymTab[sym][S_OBJ], NOVALUE) then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _30124 = (int)*(((s1_ptr)_2)->base + _sym_58862);
        _2 = (int)SEQ_PTR(_30124);
        _30125 = (int)*(((s1_ptr)_2)->base + 1);
        _30124 = NOVALUE;
        if (_30125 == _38NOVALUE_16800)
        _30126 = 1;
        else if (IS_ATOM_INT(_30125) && IS_ATOM_INT(_38NOVALUE_16800))
        _30126 = 0;
        else
        _30126 = (compare(_30125, _38NOVALUE_16800) == 0);
        _30125 = NOVALUE;
        if (_30126 != 0)
        goto L6; // [157] 273
        _30126 = NOVALUE;

        /** 			values[i] = sign * SymTab[sym][S_OBJ]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _30128 = (int)*(((s1_ptr)_2)->base + _sym_58862);
        _2 = (int)SEQ_PTR(_30128);
        _30129 = (int)*(((s1_ptr)_2)->base + 1);
        _30128 = NOVALUE;
        if (IS_ATOM_INT(_30129)) {
            if (_sign_58864 == (short)_sign_58864 && _30129 <= INT15 && _30129 >= -INT15)
            _30130 = _sign_58864 * _30129;
            else
            _30130 = NewDouble(_sign_58864 * (double)_30129);
        }
        else {
            _30130 = binary_op(MULTIPLY, _sign_58864, _30129);
        }
        _30129 = NOVALUE;
        _2 = (int)SEQ_PTR(_values_58842);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values_58842 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_58857);
        _1 = *(int *)_2;
        *(int *)_2 = _30130;
        if( _1 != _30130 ){
            DeRef(_1);
        }
        _30130 = NOVALUE;

        /** 			if not integer( values[i] ) then*/
        _2 = (int)SEQ_PTR(_values_58842);
        _30131 = (int)*(((s1_ptr)_2)->base + _i_58857);
        if (IS_ATOM_INT(_30131))
        _30132 = 1;
        else if (IS_ATOM_DBL(_30131))
        _30132 = IS_ATOM_INT(DoubleToInt(_30131));
        else
        _30132 = 0;
        _30131 = NOVALUE;
        if (_30132 != 0)
        goto L7; // [193] 230
        _30132 = NOVALUE;

        /** 				all_ints = 0*/
        _all_ints_58850 = 0;

        /** 				if atom( values[i] ) then*/
        _2 = (int)SEQ_PTR(_values_58842);
        _30134 = (int)*(((s1_ptr)_2)->base + _i_58857);
        _30135 = IS_ATOM(_30134);
        _30134 = NOVALUE;
        if (_30135 == 0)
        {
            _30135 = NOVALUE;
            goto L8; // [210] 221
        }
        else{
            _30135 = NOVALUE;
        }

        /** 					has_atom = 1*/
        _has_atom_58852 = 1;
        goto L9; // [218] 285
L8: 

        /** 					has_sequence = 1*/
        _has_sequence_58853 = 1;
        goto L9; // [227] 285
L7: 

        /** 				has_integer = 1*/
        _has_integer_58851 = 1;

        /** 				if values[i] < min then*/
        _2 = (int)SEQ_PTR(_values_58842);
        _30136 = (int)*(((s1_ptr)_2)->base + _i_58857);
        if (binary_op_a(GREATEREQ, _30136, _min_58846)){
            _30136 = NOVALUE;
            goto LA; // [241] 252
        }
        _30136 = NOVALUE;

        /** 					min = values[i]*/
        DeRef(_min_58846);
        _2 = (int)SEQ_PTR(_values_58842);
        _min_58846 = (int)*(((s1_ptr)_2)->base + _i_58857);
        Ref(_min_58846);
LA: 

        /** 				if values[i] > max then*/
        _2 = (int)SEQ_PTR(_values_58842);
        _30139 = (int)*(((s1_ptr)_2)->base + _i_58857);
        if (binary_op_a(LESSEQ, _30139, _max_58848)){
            _30139 = NOVALUE;
            goto L9; // [258] 285
        }
        _30139 = NOVALUE;

        /** 					max = values[i]*/
        DeRef(_max_58848);
        _2 = (int)SEQ_PTR(_values_58842);
        _max_58848 = (int)*(((s1_ptr)_2)->base + _i_58857);
        Ref(_max_58848);
        goto L9; // [270] 285
L6: 

        /** 			has_unassigned = 1*/
        _has_unassigned_58854 = 1;

        /** 			exit*/
        goto L2; // [282] 294
L9: 

        /** 	end for*/
        _i_58857 = _i_58857 + 1;
        goto L1; // [289] 80
L2: 
        ;
    }

    /** 	if has_unassigned or has_fwdref then*/
    if (_has_unassigned_58854 != 0) {
        goto LB; // [296] 305
    }
    if (_has_fwdref_58855 == 0)
    {
        goto LC; // [301] 325
    }
    else{
    }
LB: 

    /** 		values = switch_stack[$][SWITCH_CASES]*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _30143 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _30143 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    _30144 = (int)*(((s1_ptr)_2)->base + _30143);
    DeRef(_values_58842);
    _2 = (int)SEQ_PTR(_30144);
    _values_58842 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_values_58842);
    _30144 = NOVALUE;
LC: 

    /** 	if switch_stack[$][SWITCH_ELSE] then*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _30146 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _30146 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    _30147 = (int)*(((s1_ptr)_2)->base + _30146);
    _2 = (int)SEQ_PTR(_30147);
    _30148 = (int)*(((s1_ptr)_2)->base + 3);
    _30147 = NOVALUE;
    if (_30148 == 0) {
        _30148 = NOVALUE;
        goto LD; // [342] 371
    }
    else {
        if (!IS_ATOM_INT(_30148) && DBL_PTR(_30148)->dbl == 0.0){
            _30148 = NOVALUE;
            goto LD; // [342] 371
        }
        _30148 = NOVALUE;
    }
    _30148 = NOVALUE;

    /** 			Code[else_bp] = switch_stack[$][SWITCH_ELSE]*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _30149 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _30149 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    _30150 = (int)*(((s1_ptr)_2)->base + _30149);
    _2 = (int)SEQ_PTR(_30150);
    _30151 = (int)*(((s1_ptr)_2)->base + 3);
    _30150 = NOVALUE;
    Ref(_30151);
    _2 = (int)SEQ_PTR(_38Code_17038);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _38Code_17038 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _else_bp_58839);
    _1 = *(int *)_2;
    *(int *)_2 = _30151;
    if( _1 != _30151 ){
        DeRef(_1);
    }
    _30151 = NOVALUE;
    goto LE; // [368] 395
LD: 

    /** 		Code[else_bp] = length(Code) + 1 + TRANSLATE*/
    if (IS_SEQUENCE(_38Code_17038)){
            _30152 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _30152 = 1;
    }
    _30153 = _30152 + 1;
    _30152 = NOVALUE;
    _30154 = _30153 + _38TRANSLATE_16564;
    _30153 = NOVALUE;
    _2 = (int)SEQ_PTR(_38Code_17038);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _38Code_17038 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _else_bp_58839);
    _1 = *(int *)_2;
    *(int *)_2 = _30154;
    if( _1 != _30154 ){
        DeRef(_1);
    }
    _30154 = NOVALUE;
LE: 

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto LF; // [399] 426
    }
    else{
    }

    /** 		SymTab[cases][S_OBJ] &= 0*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_cases_58840 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _30157 = (int)*(((s1_ptr)_2)->base + 1);
    _30155 = NOVALUE;
    if (IS_SEQUENCE(_30157) && IS_ATOM(0)) {
        Append(&_30158, _30157, 0);
    }
    else if (IS_ATOM(_30157) && IS_SEQUENCE(0)) {
    }
    else {
        Concat((object_ptr)&_30158, _30157, 0);
        _30157 = NOVALUE;
    }
    _30157 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _30158;
    if( _1 != _30158 ){
        DeRef(_1);
    }
    _30158 = NOVALUE;
    _30155 = NOVALUE;
LF: 

    /** 	integer else_target = Code[else_bp]*/
    _2 = (int)SEQ_PTR(_38Code_17038);
    _else_target_58928 = (int)*(((s1_ptr)_2)->base + _else_bp_58839);
    if (!IS_ATOM_INT(_else_target_58928)){
        _else_target_58928 = (long)DBL_PTR(_else_target_58928)->dbl;
    }

    /** 	integer opcode = SWITCH*/
    _opcode_58931 = 185;

    /** 	if has_unassigned or has_fwdref then*/
    if (_has_unassigned_58854 != 0) {
        goto L10; // [447] 456
    }
    if (_has_fwdref_58855 == 0)
    {
        goto L11; // [452] 468
    }
    else{
    }
L10: 

    /** 		opcode = SWITCH_RT*/
    _opcode_58931 = 202;
    goto L12; // [465] 642
L11: 

    /** 	elsif all_ints then*/
    if (_all_ints_58850 == 0)
    {
        goto L13; // [470] 639
    }
    else{
    }

    /** 		atom delta = max - min*/
    DeRef(_delta_58937);
    if (IS_ATOM_INT(_max_58848) && IS_ATOM_INT(_min_58846)) {
        _delta_58937 = _max_58848 - _min_58846;
        if ((long)((unsigned long)_delta_58937 +(unsigned long) HIGH_BITS) >= 0){
            _delta_58937 = NewDouble((double)_delta_58937);
        }
    }
    else {
        if (IS_ATOM_INT(_max_58848)) {
            _delta_58937 = NewDouble((double)_max_58848 - DBL_PTR(_min_58846)->dbl);
        }
        else {
            if (IS_ATOM_INT(_min_58846)) {
                _delta_58937 = NewDouble(DBL_PTR(_max_58848)->dbl - (double)_min_58846);
            }
            else
            _delta_58937 = NewDouble(DBL_PTR(_max_58848)->dbl - DBL_PTR(_min_58846)->dbl);
        }
    }

    /** 		if not TRANSLATE and  delta < 1024 and delta >= 0 then*/
    _30162 = (_38TRANSLATE_16564 == 0);
    if (_30162 == 0) {
        _30163 = 0;
        goto L14; // [486] 498
    }
    if (IS_ATOM_INT(_delta_58937)) {
        _30164 = (_delta_58937 < 1024);
    }
    else {
        _30164 = (DBL_PTR(_delta_58937)->dbl < (double)1024);
    }
    _30163 = (_30164 != 0);
L14: 
    if (_30163 == 0) {
        goto L15; // [498] 628
    }
    if (IS_ATOM_INT(_delta_58937)) {
        _30166 = (_delta_58937 >= 0);
    }
    else {
        _30166 = (DBL_PTR(_delta_58937)->dbl >= (double)0);
    }
    if (_30166 == 0)
    {
        DeRef(_30166);
        _30166 = NOVALUE;
        goto L15; // [507] 628
    }
    else{
        DeRef(_30166);
        _30166 = NOVALUE;
    }

    /** 			opcode = SWITCH_SPI*/
    _opcode_58931 = 192;

    /** 			sequence jump = switch_stack[$][SWITCH_JUMP_TABLE]*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _30167 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _30167 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    _30168 = (int)*(((s1_ptr)_2)->base + _30167);
    DeRef(_jump_58947);
    _2 = (int)SEQ_PTR(_30168);
    _jump_58947 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_jump_58947);
    _30168 = NOVALUE;

    /** 			sequence switch_table = repeat( else_target, delta + 1 )*/
    if (IS_ATOM_INT(_delta_58937)) {
        _30170 = _delta_58937 + 1;
    }
    else
    _30170 = binary_op(PLUS, 1, _delta_58937);
    DeRef(_switch_table_58951);
    _switch_table_58951 = Repeat(_else_target_58928, _30170);
    DeRef(_30170);
    _30170 = NOVALUE;

    /** 			integer offset = min - 1*/
    if (IS_ATOM_INT(_min_58846)) {
        _offset_58954 = _min_58846 - 1;
    }
    else {
        _offset_58954 = NewDouble(DBL_PTR(_min_58846)->dbl - (double)1);
    }
    if (!IS_ATOM_INT(_offset_58954)) {
        _1 = (long)(DBL_PTR(_offset_58954)->dbl);
        if (UNIQUE(DBL_PTR(_offset_58954)) && (DBL_PTR(_offset_58954)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_offset_58954);
        _offset_58954 = _1;
    }

    /** 			for i = 1 to length( values ) do*/
    if (IS_SEQUENCE(_values_58842)){
            _30173 = SEQ_PTR(_values_58842)->length;
    }
    else {
        _30173 = 1;
    }
    {
        int _i_58957;
        _i_58957 = 1;
L16: 
        if (_i_58957 > _30173){
            goto L17; // [561] 593
        }

        /** 				switch_table[values[i] - offset] = jump[i]*/
        _2 = (int)SEQ_PTR(_values_58842);
        _30174 = (int)*(((s1_ptr)_2)->base + _i_58957);
        if (IS_ATOM_INT(_30174)) {
            _30175 = _30174 - _offset_58954;
            if ((long)((unsigned long)_30175 +(unsigned long) HIGH_BITS) >= 0){
                _30175 = NewDouble((double)_30175);
            }
        }
        else {
            _30175 = binary_op(MINUS, _30174, _offset_58954);
        }
        _30174 = NOVALUE;
        _2 = (int)SEQ_PTR(_jump_58947);
        _30176 = (int)*(((s1_ptr)_2)->base + _i_58957);
        Ref(_30176);
        _2 = (int)SEQ_PTR(_switch_table_58951);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _switch_table_58951 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_30175))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_30175)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _30175);
        _1 = *(int *)_2;
        *(int *)_2 = _30176;
        if( _1 != _30176 ){
            DeRef(_1);
        }
        _30176 = NOVALUE;

        /** 			end for*/
        _i_58957 = _i_58957 + 1;
        goto L16; // [588] 568
L17: 
        ;
    }

    /** 			Code[switch_pc + 2] = offset*/
    _30177 = _switch_pc_58838 + 2;
    _2 = (int)SEQ_PTR(_38Code_17038);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _38Code_17038 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _30177);
    _1 = *(int *)_2;
    *(int *)_2 = _offset_58954;
    DeRef(_1);

    /** 			switch_stack[$][SWITCH_JUMP_TABLE] = switch_table*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _30178 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _30178 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _41switch_stack_55357 = MAKE_SEQ(_2);
    }
    _3 = (int)(_30178 + ((s1_ptr)_2)->base);
    RefDS(_switch_table_58951);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _switch_table_58951;
    DeRef(_1);
    _30179 = NOVALUE;
    DeRef(_jump_58947);
    _jump_58947 = NOVALUE;
    DeRefDS(_switch_table_58951);
    _switch_table_58951 = NOVALUE;
    goto L18; // [625] 638
L15: 

    /** 			opcode = SWITCH_I*/
    _opcode_58931 = 193;
L18: 
L13: 
    DeRef(_delta_58937);
    _delta_58937 = NOVALUE;
L12: 

    /** 	Code[switch_pc] = opcode*/
    _2 = (int)SEQ_PTR(_38Code_17038);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _38Code_17038 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _switch_pc_58838);
    _1 = *(int *)_2;
    *(int *)_2 = _opcode_58931;
    DeRef(_1);

    /** 	if opcode != SWITCH_SPI then*/
    if (_opcode_58931 == 192)
    goto L19; // [654] 691

    /** 		SymTab[cases][S_OBJ] = values*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_cases_58840 + ((s1_ptr)_2)->base);
    RefDS(_values_58842);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _values_58842;
    DeRef(_1);
    _30182 = NOVALUE;

    /** 		if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L1A; // [677] 690
    }
    else{
    }

    /** 			update_translator_info( cases, all_ints, has_integer, has_atom, has_sequence )*/
    _41update_translator_info(_cases_58840, _all_ints_58850, _has_integer_58851, _has_atom_58852, _has_sequence_58853);
L1A: 
L19: 

    /** 	SymTab[jump_table][S_OBJ] = switch_stack[$][SWITCH_JUMP_TABLE] - switch_pc*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_jump_table_58841 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _30186 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _30186 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    _30187 = (int)*(((s1_ptr)_2)->base + _30186);
    _2 = (int)SEQ_PTR(_30187);
    _30188 = (int)*(((s1_ptr)_2)->base + 2);
    _30187 = NOVALUE;
    if (IS_ATOM_INT(_30188)) {
        _30189 = _30188 - _switch_pc_58838;
        if ((long)((unsigned long)_30189 +(unsigned long) HIGH_BITS) >= 0){
            _30189 = NewDouble((double)_30189);
        }
    }
    else {
        _30189 = binary_op(MINUS, _30188, _switch_pc_58838);
    }
    _30188 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _30189;
    if( _1 != _30189 ){
        DeRef(_1);
    }
    _30189 = NOVALUE;
    _30184 = NOVALUE;

    /** end procedure*/
    DeRef(_values_58842);
    DeRef(_min_58846);
    DeRef(_max_58848);
    DeRef(_30162);
    _30162 = NOVALUE;
    DeRef(_30164);
    _30164 = NOVALUE;
    DeRef(_30177);
    _30177 = NOVALUE;
    DeRef(_30175);
    _30175 = NOVALUE;
    return;
    ;
}


void _41Switch_statement()
{
    int _else_case_2__tmp_at256_59051 = NOVALUE;
    int _else_case_1__tmp_at256_59050 = NOVALUE;
    int _else_case_inlined_else_case_at_256_59049 = NOVALUE;
    int _break_base_58989 = NOVALUE;
    int _cases_58991 = NOVALUE;
    int _jump_table_58992 = NOVALUE;
    int _else_bp_58993 = NOVALUE;
    int _switch_pc_58994 = NOVALUE;
    int _t_59031 = NOVALUE;
    int _30220 = NOVALUE;
    int _30219 = NOVALUE;
    int _30218 = NOVALUE;
    int _30217 = NOVALUE;
    int _30216 = NOVALUE;
    int _30212 = NOVALUE;
    int _30208 = NOVALUE;
    int _30207 = NOVALUE;
    int _30205 = NOVALUE;
    int _30204 = NOVALUE;
    int _30202 = NOVALUE;
    int _30201 = NOVALUE;
    int _30199 = NOVALUE;
    int _30198 = NOVALUE;
    int _30197 = NOVALUE;
    int _30196 = NOVALUE;
    int _30195 = NOVALUE;
    int _30194 = NOVALUE;
    int _30192 = NOVALUE;
    int _30191 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	integer else_bp*/

    /** 	integer switch_pc*/

    /** 	push_switch()*/
    _41push_switch();

    /** 	break_base = length(break_list)*/
    if (IS_SEQUENCE(_41break_list_55140)){
            _break_base_58989 = SEQ_PTR(_41break_list_55140)->length;
    }
    else {
        _break_base_58989 = 1;
    }

    /** 	Expr()*/
    _41Expr();

    /** 	switch_stack[$][SWITCH_VALUE] = Top()*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _30191 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _30191 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _41switch_stack_55357 = MAKE_SEQ(_2);
    }
    _3 = (int)(_30191 + ((s1_ptr)_2)->base);
    _30194 = _43Top();
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _30194;
    if( _1 != _30194 ){
        DeRef(_1);
    }
    _30194 = NOVALUE;
    _30192 = NOVALUE;

    /** 	clear_temp( switch_stack[$][SWITCH_VALUE] )*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _30195 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _30195 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    _30196 = (int)*(((s1_ptr)_2)->base + _30195);
    _2 = (int)SEQ_PTR(_30196);
    _30197 = (int)*(((s1_ptr)_2)->base + 6);
    _30196 = NOVALUE;
    Ref(_30197);
    _43clear_temp(_30197);
    _30197 = NOVALUE;

    /** 	cases = NewStringSym( {-1, length(SymTab) } )*/
    if (IS_SEQUENCE(_35SymTab_15595)){
            _30198 = SEQ_PTR(_35SymTab_15595)->length;
    }
    else {
        _30198 = 1;
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = _30198;
    _30199 = MAKE_SEQ(_1);
    _30198 = NOVALUE;
    _cases_58991 = _55NewStringSym(_30199);
    _30199 = NOVALUE;
    if (!IS_ATOM_INT(_cases_58991)) {
        _1 = (long)(DBL_PTR(_cases_58991)->dbl);
        if (UNIQUE(DBL_PTR(_cases_58991)) && (DBL_PTR(_cases_58991)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_cases_58991);
        _cases_58991 = _1;
    }

    /** 	emit_opnd( cases )*/
    _43emit_opnd(_cases_58991);

    /** 	jump_table = NewStringSym( {-2, length(SymTab) } )*/
    if (IS_SEQUENCE(_35SymTab_15595)){
            _30201 = SEQ_PTR(_35SymTab_15595)->length;
    }
    else {
        _30201 = 1;
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -2;
    ((int *)_2)[2] = _30201;
    _30202 = MAKE_SEQ(_1);
    _30201 = NOVALUE;
    _jump_table_58992 = _55NewStringSym(_30202);
    _30202 = NOVALUE;
    if (!IS_ATOM_INT(_jump_table_58992)) {
        _1 = (long)(DBL_PTR(_jump_table_58992)->dbl);
        if (UNIQUE(DBL_PTR(_jump_table_58992)) && (DBL_PTR(_jump_table_58992)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_jump_table_58992);
        _jump_table_58992 = _1;
    }

    /** 	emit_opnd( jump_table )*/
    _43emit_opnd(_jump_table_58992);

    /** 	if finish_block_header(SWITCH) then end if*/
    _30204 = _41finish_block_header(185);
    if (_30204 == 0) {
        DeRef(_30204);
        _30204 = NOVALUE;
        goto L1; // [113] 117
    }
    else {
        if (!IS_ATOM_INT(_30204) && DBL_PTR(_30204)->dbl == 0.0){
            DeRef(_30204);
            _30204 = NOVALUE;
            goto L1; // [113] 117
        }
        DeRef(_30204);
        _30204 = NOVALUE;
    }
    DeRef(_30204);
    _30204 = NOVALUE;
L1: 

    /** 	switch_pc = length(Code) + 1*/
    if (IS_SEQUENCE(_38Code_17038)){
            _30205 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _30205 = 1;
    }
    _switch_pc_58994 = _30205 + 1;
    _30205 = NOVALUE;

    /** 	switch_stack[$][SWITCH_PC] = switch_pc*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _30207 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _30207 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _41switch_stack_55357 = MAKE_SEQ(_2);
    }
    _3 = (int)(_30207 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _switch_pc_58994;
    DeRef(_1);
    _30208 = NOVALUE;

    /** 	emit_op(SWITCH)*/
    _43emit_op(185);

    /** 	emit_forward_addr()  -- the else*/
    _41emit_forward_addr();

    /** 	else_bp = length( Code )*/
    if (IS_SEQUENCE(_38Code_17038)){
            _else_bp_58993 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _else_bp_58993 = 1;
    }

    /** 	t = next_token()*/
    _0 = _t_59031;
    _t_59031 = _41next_token();
    DeRef(_0);

    /** 	if t[T_ID] = CASE then*/
    _2 = (int)SEQ_PTR(_t_59031);
    _30212 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30212, 186)){
        _30212 = NOVALUE;
        goto L2; // [179] 194
    }
    _30212 = NOVALUE;

    /** 		Case_statement()*/
    _41Case_statement();

    /** 		Statement_list()*/
    _41Statement_list();
    goto L3; // [191] 200
L2: 

    /** 		putback(t)*/
    Ref(_t_59031);
    _41putback(_t_59031);
L3: 

    /** 	optimize_switch( switch_pc, else_bp, cases, jump_table )*/
    _41optimize_switch(_switch_pc_58994, _else_bp_58993, _cases_58991, _jump_table_58992);

    /** 	tok_match(END)*/
    _41tok_match(402, 0);

    /** 	tok_match(SWITCH, END)*/
    _41tok_match(185, 402);

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L4; // [230] 241
    }
    else{
    }

    /** 		emit_op(NOPSWITCH)*/
    _43emit_op(187);
L4: 

    /** 	if not else_case() then*/

    /** 	return switch_stack[$][SWITCH_ELSE]*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _else_case_1__tmp_at256_59050 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _else_case_1__tmp_at256_59050 = 1;
    }
    DeRef(_else_case_2__tmp_at256_59051);
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    _else_case_2__tmp_at256_59051 = (int)*(((s1_ptr)_2)->base + _else_case_1__tmp_at256_59050);
    RefDS(_else_case_2__tmp_at256_59051);
    DeRef(_else_case_inlined_else_case_at_256_59049);
    _2 = (int)SEQ_PTR(_else_case_2__tmp_at256_59051);
    _else_case_inlined_else_case_at_256_59049 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_else_case_inlined_else_case_at_256_59049);
    DeRef(_else_case_2__tmp_at256_59051);
    _else_case_2__tmp_at256_59051 = NOVALUE;
    if (IS_ATOM_INT(_else_case_inlined_else_case_at_256_59049)) {
        if (_else_case_inlined_else_case_at_256_59049 != 0){
            goto L5; // [263] 337
        }
    }
    else {
        if (DBL_PTR(_else_case_inlined_else_case_at_256_59049)->dbl != 0.0){
            goto L5; // [263] 337
        }
    }

    /** 		if not TRANSLATE then*/
    if (_38TRANSLATE_16564 != 0)
    goto L6; // [270] 313

    /** 			StartSourceLine( TRUE, , COVERAGE_SUPPRESS )*/
    _43StartSourceLine(_9TRUE_428, 0, 1);

    /** 			emit_temp( switch_stack[$][SWITCH_VALUE], NEW_REFERENCE )*/
    if (IS_SEQUENCE(_41switch_stack_55357)){
            _30216 = SEQ_PTR(_41switch_stack_55357)->length;
    }
    else {
        _30216 = 1;
    }
    _2 = (int)SEQ_PTR(_41switch_stack_55357);
    _30217 = (int)*(((s1_ptr)_2)->base + _30216);
    _2 = (int)SEQ_PTR(_30217);
    _30218 = (int)*(((s1_ptr)_2)->base + 6);
    _30217 = NOVALUE;
    Ref(_30218);
    _43emit_temp(_30218, 1);
    _30218 = NOVALUE;

    /** 			flush_temps()*/
    RefDS(_22663);
    _43flush_temps(_22663);
L6: 

    /** 		Warning(221, no_case_else_warning_flag,*/
    _2 = (int)SEQ_PTR(_35known_files_15596);
    _30219 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    Ref(_30219);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _30219;
    ((int *)_2)[2] = _38line_number_16947;
    _30220 = MAKE_SEQ(_1);
    _30219 = NOVALUE;
    _46Warning(221, 4096, _30220);
    _30220 = NOVALUE;
L5: 

    /** 	pop_switch( break_base )*/
    _41pop_switch(_break_base_58989);

    /** end procedure*/
    DeRef(_t_59031);
    return;
    ;
}


int _41get_private_uninitialized()
{
    int _uninitialized_59074 = NOVALUE;
    int _s_59080 = NOVALUE;
    int _pu_59086 = NOVALUE;
    int _30237 = NOVALUE;
    int _30235 = NOVALUE;
    int _30234 = NOVALUE;
    int _30233 = NOVALUE;
    int _30232 = NOVALUE;
    int _30231 = NOVALUE;
    int _30230 = NOVALUE;
    int _30229 = NOVALUE;
    int _30228 = NOVALUE;
    int _30227 = NOVALUE;
    int _30226 = NOVALUE;
    int _30225 = NOVALUE;
    int _30222 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence uninitialized = {}*/
    RefDS(_22663);
    DeRefi(_uninitialized_59074);
    _uninitialized_59074 = _22663;

    /** 	if CurrentSub != TopLevelSub then*/
    if (_38CurrentSub_16954 == _38TopLevelSub_16953)
    goto L1; // [14] 149

    /** 		symtab_pointer s = SymTab[CurrentSub][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30222 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_30222);
    _s_59080 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_59080)){
        _s_59080 = (long)DBL_PTR(_s_59080)->dbl;
    }
    _30222 = NOVALUE;

    /** 		sequence pu = { SC_PRIVATE, SC_UNDEFINED }*/
    DeRefi(_pu_59086);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 3;
    ((int *)_2)[2] = 9;
    _pu_59086 = MAKE_SEQ(_1);

    /** 		while s and find( SymTab[s][S_SCOPE], pu ) do*/
L2: 
    if (_s_59080 == 0) {
        goto L3; // [51] 148
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30226 = (int)*(((s1_ptr)_2)->base + _s_59080);
    _2 = (int)SEQ_PTR(_30226);
    _30227 = (int)*(((s1_ptr)_2)->base + 4);
    _30226 = NOVALUE;
    _30228 = find_from(_30227, _pu_59086, 1);
    _30227 = NOVALUE;
    if (_30228 == 0)
    {
        _30228 = NOVALUE;
        goto L3; // [73] 148
    }
    else{
        _30228 = NOVALUE;
    }

    /** 			if SymTab[s][S_SCOPE] = SC_PRIVATE and SymTab[s][S_INITLEVEL] = -1 then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30229 = (int)*(((s1_ptr)_2)->base + _s_59080);
    _2 = (int)SEQ_PTR(_30229);
    _30230 = (int)*(((s1_ptr)_2)->base + 4);
    _30229 = NOVALUE;
    if (IS_ATOM_INT(_30230)) {
        _30231 = (_30230 == 3);
    }
    else {
        _30231 = binary_op(EQUALS, _30230, 3);
    }
    _30230 = NOVALUE;
    if (IS_ATOM_INT(_30231)) {
        if (_30231 == 0) {
            goto L4; // [96] 127
        }
    }
    else {
        if (DBL_PTR(_30231)->dbl == 0.0) {
            goto L4; // [96] 127
        }
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30233 = (int)*(((s1_ptr)_2)->base + _s_59080);
    _2 = (int)SEQ_PTR(_30233);
    _30234 = (int)*(((s1_ptr)_2)->base + 14);
    _30233 = NOVALUE;
    if (IS_ATOM_INT(_30234)) {
        _30235 = (_30234 == -1);
    }
    else {
        _30235 = binary_op(EQUALS, _30234, -1);
    }
    _30234 = NOVALUE;
    if (_30235 == 0) {
        DeRef(_30235);
        _30235 = NOVALUE;
        goto L4; // [117] 127
    }
    else {
        if (!IS_ATOM_INT(_30235) && DBL_PTR(_30235)->dbl == 0.0){
            DeRef(_30235);
            _30235 = NOVALUE;
            goto L4; // [117] 127
        }
        DeRef(_30235);
        _30235 = NOVALUE;
    }
    DeRef(_30235);
    _30235 = NOVALUE;

    /** 				uninitialized &= s*/
    Append(&_uninitialized_59074, _uninitialized_59074, _s_59080);
L4: 

    /** 			s = SymTab[s][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30237 = (int)*(((s1_ptr)_2)->base + _s_59080);
    _2 = (int)SEQ_PTR(_30237);
    _s_59080 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_59080)){
        _s_59080 = (long)DBL_PTR(_s_59080)->dbl;
    }
    _30237 = NOVALUE;

    /** 		end while*/
    goto L2; // [145] 51
L3: 
L1: 
    DeRefi(_pu_59086);
    _pu_59086 = NOVALUE;

    /** 	return uninitialized*/
    DeRef(_30231);
    _30231 = NOVALUE;
    return _uninitialized_59074;
    ;
}


void _41While_statement()
{
    int _bp1_59117 = NOVALUE;
    int _bp2_59118 = NOVALUE;
    int _exit_base_59119 = NOVALUE;
    int _next_base_59120 = NOVALUE;
    int _uninitialized_59121 = NOVALUE;
    int _temps_59191 = NOVALUE;
    int _30273 = NOVALUE;
    int _30272 = NOVALUE;
    int _30271 = NOVALUE;
    int _30267 = NOVALUE;
    int _30266 = NOVALUE;
    int _30264 = NOVALUE;
    int _30262 = NOVALUE;
    int _30261 = NOVALUE;
    int _30260 = NOVALUE;
    int _30256 = NOVALUE;
    int _30254 = NOVALUE;
    int _30252 = NOVALUE;
    int _30246 = NOVALUE;
    int _30244 = NOVALUE;
    int _30243 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence uninitialized = get_private_uninitialized()*/
    _0 = _uninitialized_59121;
    _uninitialized_59121 = _41get_private_uninitialized();
    DeRef(_0);

    /** 	entry_stack = append( entry_stack, uninitialized )*/
    RefDS(_uninitialized_59121);
    Append(&_41entry_stack_55149, _41entry_stack_55149, _uninitialized_59121);

    /** 	Start_block( WHILE )*/
    _68Start_block(47, 0);

    /** 	exit_base = length(exit_list)*/
    if (IS_SEQUENCE(_41exit_list_55142)){
            _exit_base_59119 = SEQ_PTR(_41exit_list_55142)->length;
    }
    else {
        _exit_base_59119 = 1;
    }

    /** 	next_base = length(continue_list)*/
    if (IS_SEQUENCE(_41continue_list_55144)){
            _next_base_59120 = SEQ_PTR(_41continue_list_55144)->length;
    }
    else {
        _next_base_59120 = 1;
    }

    /** 	entry_addr &= length(Code)+1*/
    if (IS_SEQUENCE(_38Code_17038)){
            _30243 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _30243 = 1;
    }
    _30244 = _30243 + 1;
    _30243 = NOVALUE;
    Append(&_41entry_addr_55146, _41entry_addr_55146, _30244);
    _30244 = NOVALUE;

    /** 	emit_op(NOP2) -- Entry_statement may patch this later*/
    _43emit_op(110);

    /** 	emit_addr(0)*/
    _43emit_addr(0);

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L1; // [71] 82
    }
    else{
    }

    /** 		emit_op(NOPWHILE)*/
    _43emit_op(158);
L1: 

    /** 	bp1 = length(Code)+1*/
    if (IS_SEQUENCE(_38Code_17038)){
            _30246 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _30246 = 1;
    }
    _bp1_59117 = _30246 + 1;
    _30246 = NOVALUE;

    /** 	continue_addr &= bp1*/
    Append(&_41continue_addr_55147, _41continue_addr_55147, _bp1_59117);

    /** 	short_circuit += 1*/
    _41short_circuit_55122 = _41short_circuit_55122 + 1;

    /** 	short_circuit_B = FALSE*/
    _41short_circuit_B_55124 = _9FALSE_426;

    /** 	SC1_type = 0*/
    _41SC1_type_55127 = 0;

    /** 	Expr()*/
    _41Expr();

    /** 	optimized_while = FALSE*/
    _43optimized_while_51220 = _9FALSE_426;

    /** 	emit_op(WHILE)*/
    _43emit_op(47);

    /** 	short_circuit -= 1*/
    _41short_circuit_55122 = _41short_circuit_55122 - 1;

    /** 	if not optimized_while then*/
    if (_43optimized_while_51220 != 0)
    goto L2; // [153] 174

    /** 		bp2 = length(Code)+1*/
    if (IS_SEQUENCE(_38Code_17038)){
            _30252 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _30252 = 1;
    }
    _bp2_59118 = _30252 + 1;
    _30252 = NOVALUE;

    /** 		emit_forward_addr() -- will be patched*/
    _41emit_forward_addr();
    goto L3; // [171] 180
L2: 

    /** 		bp2 = 0*/
    _bp2_59118 = 0;
L3: 

    /** 	if finish_block_header(WHILE)=0 then*/
    _30254 = _41finish_block_header(47);
    if (binary_op_a(NOTEQ, _30254, 0)){
        DeRef(_30254);
        _30254 = NOVALUE;
        goto L4; // [188] 204
    }
    DeRef(_30254);
    _30254 = NOVALUE;

    /** 		entry_addr[$]=-1*/
    if (IS_SEQUENCE(_41entry_addr_55146)){
            _30256 = SEQ_PTR(_41entry_addr_55146)->length;
    }
    else {
        _30256 = 1;
    }
    _2 = (int)SEQ_PTR(_41entry_addr_55146);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _41entry_addr_55146 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _30256);
    *(int *)_2 = -1;
L4: 

    /** 	loop_stack &= WHILE*/
    Append(&_41loop_stack_55156, _41loop_stack_55156, 47);

    /** 	exit_base = length(exit_list)*/
    if (IS_SEQUENCE(_41exit_list_55142)){
            _exit_base_59119 = SEQ_PTR(_41exit_list_55142)->length;
    }
    else {
        _exit_base_59119 = 1;
    }

    /** 	if SC1_type = OR then*/
    if (_41SC1_type_55127 != 9)
    goto L5; // [227] 280

    /** 		backpatch(SC1_patch-3, SC1_OR_IF)*/
    _30260 = _41SC1_patch_55126 - 3;
    if ((long)((unsigned long)_30260 +(unsigned long) HIGH_BITS) >= 0){
        _30260 = NewDouble((double)_30260);
    }
    _43backpatch(_30260, 147);
    _30260 = NOVALUE;

    /** 		if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L6; // [249] 260
    }
    else{
    }

    /** 			emit_op(NOP1)*/
    _43emit_op(159);
L6: 

    /** 		backpatch(SC1_patch, length(Code)+1)*/
    if (IS_SEQUENCE(_38Code_17038)){
            _30261 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _30261 = 1;
    }
    _30262 = _30261 + 1;
    _30261 = NOVALUE;
    _43backpatch(_41SC1_patch_55126, _30262);
    _30262 = NOVALUE;
    goto L7; // [277] 331
L5: 

    /** 	elsif SC1_type = AND then*/
    if (_41SC1_type_55127 != 8)
    goto L8; // [286] 330

    /** 		backpatch(SC1_patch-3, SC1_AND_IF)*/
    _30264 = _41SC1_patch_55126 - 3;
    if ((long)((unsigned long)_30264 +(unsigned long) HIGH_BITS) >= 0){
        _30264 = NewDouble((double)_30264);
    }
    _43backpatch(_30264, 146);
    _30264 = NOVALUE;

    /** 		AppendXList(SC1_patch)*/

    /** 	exit_list = append(exit_list, addr)*/
    Append(&_41exit_list_55142, _41exit_list_55142, _41SC1_patch_55126);

    /** end procedure*/
    goto L9; // [318] 321
L9: 

    /** 		exit_delay &= 1*/
    Append(&_41exit_delay_55143, _41exit_delay_55143, 1);
L8: 
L7: 

    /** 	retry_addr &= length(Code)+1*/
    if (IS_SEQUENCE(_38Code_17038)){
            _30266 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _30266 = 1;
    }
    _30267 = _30266 + 1;
    _30266 = NOVALUE;
    Append(&_41retry_addr_55148, _41retry_addr_55148, _30267);
    _30267 = NOVALUE;

    /** 	sequence temps = pop_temps()*/
    _0 = _temps_59191;
    _temps_59191 = _43pop_temps();
    DeRef(_0);

    /** 	push_temps( temps )*/
    RefDS(_temps_59191);
    _43push_temps(_temps_59191);

    /** 	Statement_list()*/
    _41Statement_list();

    /** 	PatchNList(next_base)*/
    _41PatchNList(_next_base_59120);

    /** 	tok_match(END)*/
    _41tok_match(402, 0);

    /** 	tok_match(WHILE, END)*/
    _41tok_match(47, 402);

    /** 	End_block( WHILE )*/
    _68End_block(47);

    /** 	StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 	emit_op(ENDWHILE)*/
    _43emit_op(22);

    /** 	emit_addr(bp1)*/
    _43emit_addr(_bp1_59117);

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto LA; // [421] 432
    }
    else{
    }

    /** 		emit_op(NOP1)*/
    _43emit_op(159);
LA: 

    /** 	if bp2 != 0 then*/
    if (_bp2_59118 == 0)
    goto LB; // [436] 456

    /** 		backpatch(bp2, length(Code)+1)*/
    if (IS_SEQUENCE(_38Code_17038)){
            _30271 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _30271 = 1;
    }
    _30272 = _30271 + 1;
    _30271 = NOVALUE;
    _43backpatch(_bp2_59118, _30272);
    _30272 = NOVALUE;
LB: 

    /** 	exit_loop(exit_base)*/
    _41exit_loop(_exit_base_59119);

    /** 	entry_stack = remove( entry_stack, length( entry_stack ) )*/
    if (IS_SEQUENCE(_41entry_stack_55149)){
            _30273 = SEQ_PTR(_41entry_stack_55149)->length;
    }
    else {
        _30273 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_41entry_stack_55149);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_30273)) ? _30273 : (long)(DBL_PTR(_30273)->dbl);
        int stop = (IS_ATOM_INT(_30273)) ? _30273 : (long)(DBL_PTR(_30273)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_41entry_stack_55149), start, &_41entry_stack_55149 );
            }
            else Tail(SEQ_PTR(_41entry_stack_55149), stop+1, &_41entry_stack_55149);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_41entry_stack_55149), start, &_41entry_stack_55149);
        }
        else {
            assign_slice_seq = &assign_space;
            _41entry_stack_55149 = Remove_elements(start, stop, (SEQ_PTR(_41entry_stack_55149)->ref == 1));
        }
    }
    _30273 = NOVALUE;
    _30273 = NOVALUE;

    /** 	push_temps( temps )*/
    RefDS(_temps_59191);
    _43push_temps(_temps_59191);

    /** end procedure*/
    DeRef(_uninitialized_59121);
    DeRefDS(_temps_59191);
    return;
    ;
}


void _41Loop_statement()
{
    int _bp1_59221 = NOVALUE;
    int _exit_base_59222 = NOVALUE;
    int _next_base_59223 = NOVALUE;
    int _t_59225 = NOVALUE;
    int _uninitialized_59228 = NOVALUE;
    int _30297 = NOVALUE;
    int _30295 = NOVALUE;
    int _30294 = NOVALUE;
    int _30293 = NOVALUE;
    int _30287 = NOVALUE;
    int _30286 = NOVALUE;
    int _30284 = NOVALUE;
    int _30281 = NOVALUE;
    int _30280 = NOVALUE;
    int _30279 = NOVALUE;
    int _0, _1, _2;
    

    /** 	Start_block( LOOP )*/
    _68Start_block(422, 0);

    /** 	sequence uninitialized = get_private_uninitialized()*/
    _0 = _uninitialized_59228;
    _uninitialized_59228 = _41get_private_uninitialized();
    DeRef(_0);

    /** 	entry_stack = append( entry_stack, uninitialized )*/
    RefDS(_uninitialized_59228);
    Append(&_41entry_stack_55149, _41entry_stack_55149, _uninitialized_59228);

    /** 	exit_base = length(exit_list)*/
    if (IS_SEQUENCE(_41exit_list_55142)){
            _exit_base_59222 = SEQ_PTR(_41exit_list_55142)->length;
    }
    else {
        _exit_base_59222 = 1;
    }

    /** 	next_base = length(continue_list)*/
    if (IS_SEQUENCE(_41continue_list_55144)){
            _next_base_59223 = SEQ_PTR(_41continue_list_55144)->length;
    }
    else {
        _next_base_59223 = 1;
    }

    /** 	emit_op(NOP2) -- Entry_statement() may patch this*/
    _43emit_op(110);

    /** 	emit_addr(0)*/
    _43emit_addr(0);

    /** 	if finish_block_header(LOOP) then*/
    _30279 = _41finish_block_header(422);
    if (_30279 == 0) {
        DeRef(_30279);
        _30279 = NOVALUE;
        goto L1; // [58] 81
    }
    else {
        if (!IS_ATOM_INT(_30279) && DBL_PTR(_30279)->dbl == 0.0){
            DeRef(_30279);
            _30279 = NOVALUE;
            goto L1; // [58] 81
        }
        DeRef(_30279);
        _30279 = NOVALUE;
    }
    DeRef(_30279);
    _30279 = NOVALUE;

    /** 	    entry_addr &= length(Code)-1*/
    if (IS_SEQUENCE(_38Code_17038)){
            _30280 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _30280 = 1;
    }
    _30281 = _30280 - 1;
    _30280 = NOVALUE;
    Append(&_41entry_addr_55146, _41entry_addr_55146, _30281);
    _30281 = NOVALUE;
    goto L2; // [78] 90
L1: 

    /** 		entry_addr &= -1*/
    Append(&_41entry_addr_55146, _41entry_addr_55146, -1);
L2: 

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L3; // [94] 105
    }
    else{
    }

    /** 		emit_op(NOP1)*/
    _43emit_op(159);
L3: 

    /** 	bp1 = length(Code)+1*/
    if (IS_SEQUENCE(_38Code_17038)){
            _30284 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _30284 = 1;
    }
    _bp1_59221 = _30284 + 1;
    _30284 = NOVALUE;

    /** 	retry_addr &= length(Code)+1*/
    if (IS_SEQUENCE(_38Code_17038)){
            _30286 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _30286 = 1;
    }
    _30287 = _30286 + 1;
    _30286 = NOVALUE;
    Append(&_41retry_addr_55148, _41retry_addr_55148, _30287);
    _30287 = NOVALUE;

    /** 	continue_addr &= 0*/
    Append(&_41continue_addr_55147, _41continue_addr_55147, 0);

    /** 	loop_stack &= LOOP*/
    Append(&_41loop_stack_55156, _41loop_stack_55156, 422);

    /** 	Statement_list()*/
    _41Statement_list();

    /** 	End_block( LOOP )*/
    _68End_block(422);

    /** 	tok_match(UNTIL)*/
    _41tok_match(423, 0);

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L4; // [174] 185
    }
    else{
    }

    /** 		emit_op(NOP1)*/
    _43emit_op(159);
L4: 

    /** 	PatchNList(next_base)*/
    _41PatchNList(_next_base_59223);

    /** 	StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 	short_circuit += 1*/
    _41short_circuit_55122 = _41short_circuit_55122 + 1;

    /** 	short_circuit_B = FALSE*/
    _41short_circuit_B_55124 = _9FALSE_426;

    /** 	SC1_type = 0*/
    _41SC1_type_55127 = 0;

    /** 	Expr()*/
    _41Expr();

    /** 	if SC1_type = OR then*/
    if (_41SC1_type_55127 != 9)
    goto L5; // [231] 284

    /** 		backpatch(SC1_patch-3, SC1_OR_IF)*/
    _30293 = _41SC1_patch_55126 - 3;
    if ((long)((unsigned long)_30293 +(unsigned long) HIGH_BITS) >= 0){
        _30293 = NewDouble((double)_30293);
    }
    _43backpatch(_30293, 147);
    _30293 = NOVALUE;

    /** 		if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L6; // [253] 264
    }
    else{
    }

    /** 		    emit_op(NOP1)  -- to get label here*/
    _43emit_op(159);
L6: 

    /** 		backpatch(SC1_patch, length(Code)+1)*/
    if (IS_SEQUENCE(_38Code_17038)){
            _30294 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _30294 = 1;
    }
    _30295 = _30294 + 1;
    _30294 = NOVALUE;
    _43backpatch(_41SC1_patch_55126, _30295);
    _30295 = NOVALUE;
    goto L7; // [281] 310
L5: 

    /** 	elsif SC1_type = AND then*/
    if (_41SC1_type_55127 != 8)
    goto L8; // [290] 309

    /** 		backpatch(SC1_patch-3, SC1_AND_IF)*/
    _30297 = _41SC1_patch_55126 - 3;
    if ((long)((unsigned long)_30297 +(unsigned long) HIGH_BITS) >= 0){
        _30297 = NewDouble((double)_30297);
    }
    _43backpatch(_30297, 146);
    _30297 = NOVALUE;
L8: 
L7: 

    /** 	short_circuit -= 1*/
    _41short_circuit_55122 = _41short_circuit_55122 - 1;

    /** 	emit_op(IF)*/
    _43emit_op(20);

    /** 	emit_addr(bp1)*/
    _43emit_addr(_bp1_59221);

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L9; // [334] 345
    }
    else{
    }

    /** 		emit_op(NOP1)*/
    _43emit_op(159);
L9: 

    /** 	exit_loop(exit_base)*/
    _41exit_loop(_exit_base_59222);

    /** 	tok_match(END)*/
    _41tok_match(402, 0);

    /** 	tok_match(LOOP, END)*/
    _41tok_match(422, 402);

    /** end procedure*/
    DeRef(_uninitialized_59228);
    return;
    ;
}


void _41Ifdef_statement()
{
    int _option_59307 = NOVALUE;
    int _matched_59308 = NOVALUE;
    int _has_matched_59309 = NOVALUE;
    int _in_matched_59310 = NOVALUE;
    int _dead_ifdef_59311 = NOVALUE;
    int _in_elsedef_59312 = NOVALUE;
    int _tok_59314 = NOVALUE;
    int _keyw_59315 = NOVALUE;
    int _parser_id_59319 = NOVALUE;
    int _negate_59335 = NOVALUE;
    int _conjunction_59336 = NOVALUE;
    int _at_start_59337 = NOVALUE;
    int _prev_conj_59338 = NOVALUE;
    int _this_matched_59411 = NOVALUE;
    int _gotword_59427 = NOVALUE;
    int _gotthen_59428 = NOVALUE;
    int _if_lvl_59429 = NOVALUE;
    int _30414 = NOVALUE;
    int _30413 = NOVALUE;
    int _30409 = NOVALUE;
    int _30407 = NOVALUE;
    int _30404 = NOVALUE;
    int _30402 = NOVALUE;
    int _30401 = NOVALUE;
    int _30397 = NOVALUE;
    int _30394 = NOVALUE;
    int _30391 = NOVALUE;
    int _30387 = NOVALUE;
    int _30385 = NOVALUE;
    int _30384 = NOVALUE;
    int _30383 = NOVALUE;
    int _30382 = NOVALUE;
    int _30381 = NOVALUE;
    int _30380 = NOVALUE;
    int _30379 = NOVALUE;
    int _30375 = NOVALUE;
    int _30372 = NOVALUE;
    int _30371 = NOVALUE;
    int _30370 = NOVALUE;
    int _30366 = NOVALUE;
    int _30365 = NOVALUE;
    int _30364 = NOVALUE;
    int _30361 = NOVALUE;
    int _30358 = NOVALUE;
    int _30357 = NOVALUE;
    int _30356 = NOVALUE;
    int _30354 = NOVALUE;
    int _30343 = NOVALUE;
    int _30341 = NOVALUE;
    int _30340 = NOVALUE;
    int _30339 = NOVALUE;
    int _30338 = NOVALUE;
    int _30337 = NOVALUE;
    int _30336 = NOVALUE;
    int _30335 = NOVALUE;
    int _30332 = NOVALUE;
    int _30331 = NOVALUE;
    int _30329 = NOVALUE;
    int _30327 = NOVALUE;
    int _30326 = NOVALUE;
    int _30324 = NOVALUE;
    int _30322 = NOVALUE;
    int _30321 = NOVALUE;
    int _30319 = NOVALUE;
    int _30318 = NOVALUE;
    int _30317 = NOVALUE;
    int _30314 = NOVALUE;
    int _30312 = NOVALUE;
    int _30309 = NOVALUE;
    int _30308 = NOVALUE;
    int _30307 = NOVALUE;
    int _30305 = NOVALUE;
    int _30303 = NOVALUE;
    int _30302 = NOVALUE;
    int _30301 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer matched = 0, has_matched = 0,  in_matched = 0, dead_ifdef = 0, in_elsedef = 0*/
    _matched_59308 = 0;
    _has_matched_59309 = 0;
    _in_matched_59310 = 0;
    _dead_ifdef_59311 = 0;
    _in_elsedef_59312 = 0;

    /** 	sequence keyw ="ifdef"*/
    RefDS(_27043);
    DeRefi(_keyw_59315);
    _keyw_59315 = _27043;

    /** 	live_ifdef += 1*/
    _41live_ifdef_59303 = _41live_ifdef_59303 + 1;

    /** 	ifdef_lineno &= line_number*/
    Append(&_41ifdef_lineno_59304, _41ifdef_lineno_59304, _38line_number_16947);

    /** 	integer parser_id*/

    /** 	if CurrentSub != TopLevelSub or length(if_labels) or length(loop_labels) then*/
    _30301 = (_38CurrentSub_16954 != _38TopLevelSub_16953);
    if (_30301 != 0) {
        _30302 = 1;
        goto L1; // [55] 68
    }
    if (IS_SEQUENCE(_41if_labels_55151)){
            _30303 = SEQ_PTR(_41if_labels_55151)->length;
    }
    else {
        _30303 = 1;
    }
    _30302 = (_30303 != 0);
L1: 
    if (_30302 != 0) {
        goto L2; // [68] 82
    }
    if (IS_SEQUENCE(_41loop_labels_55150)){
            _30305 = SEQ_PTR(_41loop_labels_55150)->length;
    }
    else {
        _30305 = 1;
    }
    if (_30305 == 0)
    {
        _30305 = NOVALUE;
        goto L3; // [78] 92
    }
    else{
        _30305 = NOVALUE;
    }
L2: 

    /** 		parser_id = forward_Statement_list*/
    _parser_id_59319 = _41forward_Statement_list_58155;
    goto L4; // [89] 100
L3: 

    /** 		parser_id = top_level_parser*/
    _parser_id_59319 = _41top_level_parser_59302;
L4: 

    /** 	while 1 label "top" do*/
L5: 

    /** 		if matched = 0 and in_elsedef = 0 then*/
    _30307 = (_matched_59308 == 0);
    if (_30307 == 0) {
        goto L6; // [111] 632
    }
    _30309 = (_in_elsedef_59312 == 0);
    if (_30309 == 0)
    {
        DeRef(_30309);
        _30309 = NOVALUE;
        goto L6; // [120] 632
    }
    else{
        DeRef(_30309);
        _30309 = NOVALUE;
    }

    /** 			integer negate = 0, conjunction = 0*/
    _negate_59335 = 0;
    _conjunction_59336 = 0;

    /** 			integer at_start = 1*/
    _at_start_59337 = 1;

    /** 			sequence prev_conj = ""*/
    RefDS(_22663);
    DeRef(_prev_conj_59338);
    _prev_conj_59338 = _22663;

    /** 			while 1 label "deflist" do*/
L7: 

    /** 				option = StringToken()*/
    RefDS(_5);
    _0 = _option_59307;
    _option_59307 = _62StringToken(_5);
    DeRef(_0);

    /** 				if equal(option, "then") then*/
    if (_option_59307 == _27110)
    _30312 = 1;
    else if (IS_ATOM_INT(_option_59307) && IS_ATOM_INT(_27110))
    _30312 = 0;
    else
    _30312 = (compare(_option_59307, _27110) == 0);
    if (_30312 == 0)
    {
        _30312 = NOVALUE;
        goto L8; // [162] 234
    }
    else{
        _30312 = NOVALUE;
    }

    /** 					if at_start = 1 then*/
    if (_at_start_59337 != 1)
    goto L9; // [167] 185

    /** 						CompileErr(6, {keyw})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_keyw_59315);
    *((int *)(_2+4)) = _keyw_59315;
    _30314 = MAKE_SEQ(_1);
    _46CompileErr(6, _30314, 0);
    _30314 = NOVALUE;
    goto LA; // [182] 518
L9: 

    /** 					elsif conjunction = 0 then*/
    if (_conjunction_59336 != 0)
    goto LB; // [187] 219

    /** 						if negate = 0 then*/
    if (_negate_59335 != 0)
    goto LC; // [193] 204

    /** 							exit "deflist"*/
    goto LD; // [199] 606
    goto LA; // [201] 518
LC: 

    /** 							CompileErr(11, {keyw})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_keyw_59315);
    *((int *)(_2+4)) = _keyw_59315;
    _30317 = MAKE_SEQ(_1);
    _46CompileErr(11, _30317, 0);
    _30317 = NOVALUE;
    goto LA; // [216] 518
LB: 

    /** 						CompileErr(8, {keyw, prev_conj})*/
    RefDS(_prev_conj_59338);
    RefDS(_keyw_59315);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _keyw_59315;
    ((int *)_2)[2] = _prev_conj_59338;
    _30318 = MAKE_SEQ(_1);
    _46CompileErr(8, _30318, 0);
    _30318 = NOVALUE;
    goto LA; // [231] 518
L8: 

    /** 				elsif equal(option, "not") then*/
    if (_option_59307 == _27071)
    _30319 = 1;
    else if (IS_ATOM_INT(_option_59307) && IS_ATOM_INT(_27071))
    _30319 = 0;
    else
    _30319 = (compare(_option_59307, _27071) == 0);
    if (_30319 == 0)
    {
        _30319 = NOVALUE;
        goto LE; // [240] 276
    }
    else{
        _30319 = NOVALUE;
    }

    /** 					if negate = 0 then*/
    if (_negate_59335 != 0)
    goto LF; // [245] 261

    /** 						negate = 1*/
    _negate_59335 = 1;

    /** 						continue "deflist"*/
    goto L7; // [256] 148
    goto LA; // [258] 518
LF: 

    /** 						CompileErr(7, {keyw})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_keyw_59315);
    *((int *)(_2+4)) = _keyw_59315;
    _30321 = MAKE_SEQ(_1);
    _46CompileErr(7, _30321, 0);
    _30321 = NOVALUE;
    goto LA; // [273] 518
LE: 

    /** 				elsif equal(option, "and") then*/
    if (_option_59307 == _26975)
    _30322 = 1;
    else if (IS_ATOM_INT(_option_59307) && IS_ATOM_INT(_26975))
    _30322 = 0;
    else
    _30322 = (compare(_option_59307, _26975) == 0);
    if (_30322 == 0)
    {
        _30322 = NOVALUE;
        goto L10; // [282] 345
    }
    else{
        _30322 = NOVALUE;
    }

    /** 					if at_start = 1 then*/
    if (_at_start_59337 != 1)
    goto L11; // [287] 305

    /** 						CompileErr(2, {keyw})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_keyw_59315);
    *((int *)(_2+4)) = _keyw_59315;
    _30324 = MAKE_SEQ(_1);
    _46CompileErr(2, _30324, 0);
    _30324 = NOVALUE;
    goto LA; // [302] 518
L11: 

    /** 					elsif conjunction = 0 then*/
    if (_conjunction_59336 != 0)
    goto L12; // [307] 330

    /** 						conjunction = 1*/
    _conjunction_59336 = 1;

    /** 						prev_conj = option*/
    RefDS(_option_59307);
    DeRef(_prev_conj_59338);
    _prev_conj_59338 = _option_59307;

    /** 						continue "deflist"*/
    goto L7; // [325] 148
    goto LA; // [327] 518
L12: 

    /** 						CompileErr(10,{keyw,prev_conj})*/
    RefDS(_prev_conj_59338);
    RefDS(_keyw_59315);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _keyw_59315;
    ((int *)_2)[2] = _prev_conj_59338;
    _30326 = MAKE_SEQ(_1);
    _46CompileErr(10, _30326, 0);
    _30326 = NOVALUE;
    goto LA; // [342] 518
L10: 

    /** 				elsif equal(option, "or") then*/
    if (_option_59307 == _27075)
    _30327 = 1;
    else if (IS_ATOM_INT(_option_59307) && IS_ATOM_INT(_27075))
    _30327 = 0;
    else
    _30327 = (compare(_option_59307, _27075) == 0);
    if (_30327 == 0)
    {
        _30327 = NOVALUE;
        goto L13; // [351] 414
    }
    else{
        _30327 = NOVALUE;
    }

    /** 					if at_start = 1 then*/
    if (_at_start_59337 != 1)
    goto L14; // [356] 374

    /** 						CompileErr(6, {keyw})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_keyw_59315);
    *((int *)(_2+4)) = _keyw_59315;
    _30329 = MAKE_SEQ(_1);
    _46CompileErr(6, _30329, 0);
    _30329 = NOVALUE;
    goto LA; // [371] 518
L14: 

    /** 					elsif conjunction = 0 then*/
    if (_conjunction_59336 != 0)
    goto L15; // [376] 399

    /** 						conjunction = 2*/
    _conjunction_59336 = 2;

    /** 						prev_conj = option*/
    RefDS(_option_59307);
    DeRef(_prev_conj_59338);
    _prev_conj_59338 = _option_59307;

    /** 						continue "deflist"*/
    goto L7; // [394] 148
    goto LA; // [396] 518
L15: 

    /** 						CompileErr(9, {keyw, prev_conj})*/
    RefDS(_prev_conj_59338);
    RefDS(_keyw_59315);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _keyw_59315;
    ((int *)_2)[2] = _prev_conj_59338;
    _30331 = MAKE_SEQ(_1);
    _46CompileErr(9, _30331, 0);
    _30331 = NOVALUE;
    goto LA; // [411] 518
L13: 

    /** 				elsif length(option) = 0 then*/
    if (IS_SEQUENCE(_option_59307)){
            _30332 = SEQ_PTR(_option_59307)->length;
    }
    else {
        _30332 = 1;
    }
    if (_30332 != 0)
    goto L16; // [419] 454

    /** 					if at_start = 1 then*/
    if (_at_start_59337 != 1)
    goto L17; // [425] 443

    /** 						CompileErr(122, {keyw})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_keyw_59315);
    *((int *)(_2+4)) = _keyw_59315;
    _30335 = MAKE_SEQ(_1);
    _46CompileErr(122, _30335, 0);
    _30335 = NOVALUE;
    goto LA; // [440] 518
L17: 

    /** 						CompileErr(82)*/
    RefDS(_22663);
    _46CompileErr(82, _22663, 0);
    goto LA; // [451] 518
L16: 

    /** 				elsif not at_start and length(prev_conj) = 0 then*/
    _30336 = (_at_start_59337 == 0);
    if (_30336 == 0) {
        goto L18; // [459] 488
    }
    if (IS_SEQUENCE(_prev_conj_59338)){
            _30338 = SEQ_PTR(_prev_conj_59338)->length;
    }
    else {
        _30338 = 1;
    }
    _30339 = (_30338 == 0);
    _30338 = NOVALUE;
    if (_30339 == 0)
    {
        DeRef(_30339);
        _30339 = NOVALUE;
        goto L18; // [471] 488
    }
    else{
        DeRef(_30339);
        _30339 = NOVALUE;
    }

    /** 					CompileErr(4, {keyw})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_keyw_59315);
    *((int *)(_2+4)) = _keyw_59315;
    _30340 = MAKE_SEQ(_1);
    _46CompileErr(4, _30340, 0);
    _30340 = NOVALUE;
    goto LA; // [485] 518
L18: 

    /** 				elsif t_identifier(option) = 0 then*/
    RefDS(_option_59307);
    _30341 = _9t_identifier(_option_59307);
    if (binary_op_a(NOTEQ, _30341, 0)){
        DeRef(_30341);
        _30341 = NOVALUE;
        goto L19; // [494] 512
    }
    DeRef(_30341);
    _30341 = NOVALUE;

    /** 					CompileErr(3, {keyw})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_keyw_59315);
    *((int *)(_2+4)) = _keyw_59315;
    _30343 = MAKE_SEQ(_1);
    _46CompileErr(3, _30343, 0);
    _30343 = NOVALUE;
    goto LA; // [509] 518
L19: 

    /** 					at_start = 0*/
    _at_start_59337 = 0;
LA: 

    /** 				integer this_matched = find(option, OpDefines)*/
    _this_matched_59411 = find_from(_option_59307, _38OpDefines_17019, 1);

    /** 				if negate then*/
    if (_negate_59335 == 0)
    {
        goto L1A; // [529] 543
    }
    else{
    }

    /** 					this_matched = not this_matched*/
    _this_matched_59411 = (_this_matched_59411 == 0);

    /** 					negate = 0*/
    _negate_59335 = 0;
L1A: 

    /** 				if conjunction = 0 then*/
    if (_conjunction_59336 != 0)
    goto L1B; // [545] 557

    /** 					matched = this_matched*/
    _matched_59308 = _this_matched_59411;
    goto L1C; // [554] 599
L1B: 

    /** 					if conjunction = 1 then*/
    if (_conjunction_59336 != 1)
    goto L1D; // [559] 572

    /** 						matched = matched and this_matched*/
    _matched_59308 = (_matched_59308 != 0 && _this_matched_59411 != 0);
    goto L1E; // [569] 586
L1D: 

    /** 					elsif conjunction = 2 then*/
    if (_conjunction_59336 != 2)
    goto L1F; // [574] 585

    /** 						matched = matched or this_matched*/
    _matched_59308 = (_matched_59308 != 0 || _this_matched_59411 != 0);
L1F: 
L1E: 

    /** 					conjunction = 0*/
    _conjunction_59336 = 0;

    /** 					prev_conj = ""*/
    RefDS(_22663);
    DeRef(_prev_conj_59338);
    _prev_conj_59338 = _22663;
L1C: 

    /** 			end while*/
    goto L7; // [603] 148
LD: 

    /** 			in_matched = matched*/
    _in_matched_59310 = _matched_59308;

    /** 			if matched then*/
    if (_matched_59308 == 0)
    {
        goto L20; // [613] 631
    }
    else{
    }

    /** 				No_new_entry = 0*/
    _55No_new_entry_48227 = 0;

    /** 				call_proc(parser_id, {})*/
    _0 = (int)_00[_parser_id_59319].addr;
    (*(int (*)())_0)(
                         );
L20: 
L6: 
    DeRef(_prev_conj_59338);
    _prev_conj_59338 = NOVALUE;

    /** 		integer gotword = 0*/
    _gotword_59427 = 0;

    /** 		integer gotthen = 0*/
    _gotthen_59428 = 0;

    /** 		integer if_lvl  = 0*/
    _if_lvl_59429 = 0;

    /** 		No_new_entry = not matched*/
    _55No_new_entry_48227 = (_matched_59308 == 0);

    /** 		has_matched = has_matched or matched*/
    _has_matched_59309 = (_has_matched_59309 != 0 || _matched_59308 != 0);

    /** 		keyw = "elsifdef"*/
    RefDS(_27011);
    DeRefi(_keyw_59315);
    _keyw_59315 = _27011;

    /** 		while 1 do*/
L21: 

    /** 			tok = next_token()*/
    _0 = _tok_59314;
    _tok_59314 = _41next_token();
    DeRef(_0);

    /** 			if tok[T_ID] = END_OF_FILE then*/
    _2 = (int)SEQ_PTR(_tok_59314);
    _30354 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30354, -21)){
        _30354 = NOVALUE;
        goto L22; // [689] 712
    }
    _30354 = NOVALUE;

    /** 				CompileErr(65, ifdef_lineno[$])*/
    if (IS_SEQUENCE(_41ifdef_lineno_59304)){
            _30356 = SEQ_PTR(_41ifdef_lineno_59304)->length;
    }
    else {
        _30356 = 1;
    }
    _2 = (int)SEQ_PTR(_41ifdef_lineno_59304);
    _30357 = (int)*(((s1_ptr)_2)->base + _30356);
    _46CompileErr(65, _30357, 0);
    _30357 = NOVALUE;
    goto L21; // [709] 674
L22: 

    /** 			elsif tok[T_ID] = END then*/
    _2 = (int)SEQ_PTR(_tok_59314);
    _30358 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30358, 402)){
        _30358 = NOVALUE;
        goto L23; // [722] 844
    }
    _30358 = NOVALUE;

    /** 				tok = next_token()*/
    _0 = _tok_59314;
    _tok_59314 = _41next_token();
    DeRef(_0);

    /** 				if tok[T_ID] = IFDEF then*/
    _2 = (int)SEQ_PTR(_tok_59314);
    _30361 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30361, 407)){
        _30361 = NOVALUE;
        goto L24; // [741] 769
    }
    _30361 = NOVALUE;

    /** 					if dead_ifdef then*/
    if (_dead_ifdef_59311 == 0)
    {
        goto L25; // [747] 759
    }
    else{
    }

    /** 						dead_ifdef -= 1*/
    _dead_ifdef_59311 = _dead_ifdef_59311 - 1;
    goto L21; // [756] 674
L25: 

    /** 						exit "top"*/
    goto L26; // [763] 1312
    goto L21; // [766] 674
L24: 

    /** 				elsif in_matched then*/
    if (_in_matched_59310 == 0)
    {
        goto L27; // [771] 793
    }
    else{
    }

    /** 					CompileErr(75, ifdef_lineno[$])*/
    if (IS_SEQUENCE(_41ifdef_lineno_59304)){
            _30364 = SEQ_PTR(_41ifdef_lineno_59304)->length;
    }
    else {
        _30364 = 1;
    }
    _2 = (int)SEQ_PTR(_41ifdef_lineno_59304);
    _30365 = (int)*(((s1_ptr)_2)->base + _30364);
    _46CompileErr(75, _30365, 0);
    _30365 = NOVALUE;
    goto L21; // [790] 674
L27: 

    /** 					if tok[T_ID] = IF then*/
    _2 = (int)SEQ_PTR(_tok_59314);
    _30366 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30366, 20)){
        _30366 = NOVALUE;
        goto L21; // [803] 674
    }
    _30366 = NOVALUE;

    /** 						if if_lvl > 0 then*/
    if (_if_lvl_59429 <= 0)
    goto L28; // [809] 822

    /** 							if_lvl -= 1*/
    _if_lvl_59429 = _if_lvl_59429 - 1;
    goto L21; // [819] 674
L28: 

    /** 							CompileErr(111, ifdef_lineno[$])*/
    if (IS_SEQUENCE(_41ifdef_lineno_59304)){
            _30370 = SEQ_PTR(_41ifdef_lineno_59304)->length;
    }
    else {
        _30370 = 1;
    }
    _2 = (int)SEQ_PTR(_41ifdef_lineno_59304);
    _30371 = (int)*(((s1_ptr)_2)->base + _30370);
    _46CompileErr(111, _30371, 0);
    _30371 = NOVALUE;
    goto L21; // [841] 674
L23: 

    /** 			elsif tok[T_ID] = IF then*/
    _2 = (int)SEQ_PTR(_tok_59314);
    _30372 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30372, 20)){
        _30372 = NOVALUE;
        goto L29; // [854] 867
    }
    _30372 = NOVALUE;

    /** 				if_lvl += 1*/
    _if_lvl_59429 = _if_lvl_59429 + 1;
    goto L21; // [864] 674
L29: 

    /** 			elsif tok[T_ID] = ELSE then*/
    _2 = (int)SEQ_PTR(_tok_59314);
    _30375 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30375, 23)){
        _30375 = NOVALUE;
        goto L2A; // [877] 913
    }
    _30375 = NOVALUE;

    /** 				if not in_matched then*/
    if (_in_matched_59310 != 0)
    goto L21; // [883] 674

    /** 					if if_lvl = 0 then*/
    if (_if_lvl_59429 != 0)
    goto L21; // [888] 674

    /** 						CompileErr(108, ifdef_lineno[$])*/
    if (IS_SEQUENCE(_41ifdef_lineno_59304)){
            _30379 = SEQ_PTR(_41ifdef_lineno_59304)->length;
    }
    else {
        _30379 = 1;
    }
    _2 = (int)SEQ_PTR(_41ifdef_lineno_59304);
    _30380 = (int)*(((s1_ptr)_2)->base + _30379);
    _46CompileErr(108, _30380, 0);
    _30380 = NOVALUE;
    goto L21; // [910] 674
L2A: 

    /** 			elsif tok[T_ID] = ELSIFDEF and not dead_ifdef then*/
    _2 = (int)SEQ_PTR(_tok_59314);
    _30381 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_30381)) {
        _30382 = (_30381 == 408);
    }
    else {
        _30382 = binary_op(EQUALS, _30381, 408);
    }
    _30381 = NOVALUE;
    if (IS_ATOM_INT(_30382)) {
        if (_30382 == 0) {
            goto L2B; // [927] 1065
        }
    }
    else {
        if (DBL_PTR(_30382)->dbl == 0.0) {
            goto L2B; // [927] 1065
        }
    }
    _30384 = (_dead_ifdef_59311 == 0);
    if (_30384 == 0)
    {
        DeRef(_30384);
        _30384 = NOVALUE;
        goto L2B; // [935] 1065
    }
    else{
        DeRef(_30384);
        _30384 = NOVALUE;
    }

    /** 				if has_matched then*/
    if (_has_matched_59309 == 0)
    {
        goto L2C; // [940] 1305
    }
    else{
    }

    /** 					in_matched = 0*/
    _in_matched_59310 = 0;

    /** 					No_new_entry = 1*/
    _55No_new_entry_48227 = 1;

    /** 					gotword = 0*/
    _gotword_59427 = 0;

    /** 					gotthen = 0*/
    _gotthen_59428 = 0;

    /** 					while length(option) > 0 with entry do*/
    goto L2D; // [967] 1009
L2E: 
    if (IS_SEQUENCE(_option_59307)){
            _30385 = SEQ_PTR(_option_59307)->length;
    }
    else {
        _30385 = 1;
    }
    if (_30385 <= 0)
    goto L2F; // [975] 1022

    /** 						if equal(option, "then") then*/
    if (_option_59307 == _27110)
    _30387 = 1;
    else if (IS_ATOM_INT(_option_59307) && IS_ATOM_INT(_27110))
    _30387 = 0;
    else
    _30387 = (compare(_option_59307, _27110) == 0);
    if (_30387 == 0)
    {
        _30387 = NOVALUE;
        goto L30; // [985] 1000
    }
    else{
        _30387 = NOVALUE;
    }

    /** 							gotthen = 1*/
    _gotthen_59428 = 1;

    /** 							exit*/
    goto L2F; // [995] 1022
    goto L31; // [997] 1006
L30: 

    /** 							gotword = 1*/
    _gotword_59427 = 1;
L31: 

    /** 					entry*/
L2D: 

    /** 						option = StringToken()*/
    RefDS(_5);
    _0 = _option_59307;
    _option_59307 = _62StringToken(_5);
    DeRef(_0);

    /** 					end while*/
    goto L2E; // [1019] 970
L2F: 

    /** 					if gotword = 0 then*/
    if (_gotword_59427 != 0)
    goto L32; // [1024] 1036

    /** 						CompileErr(78)*/
    RefDS(_22663);
    _46CompileErr(78, _22663, 0);
L32: 

    /** 					if gotthen = 0 then*/
    if (_gotthen_59428 != 0)
    goto L33; // [1038] 1050

    /** 						CompileErr(77)*/
    RefDS(_22663);
    _46CompileErr(77, _22663, 0);
L33: 

    /** 					read_line()*/
    _62read_line();
    goto L21; // [1054] 674

    /** 					exit*/
    goto L2C; // [1059] 1305
    goto L21; // [1062] 674
L2B: 

    /** 			elsif tok[T_ID] = ELSEDEF then*/
    _2 = (int)SEQ_PTR(_tok_59314);
    _30391 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30391, 409)){
        _30391 = NOVALUE;
        goto L34; // [1075] 1235
    }
    _30391 = NOVALUE;

    /** 				gotword = line_number*/
    _gotword_59427 = _38line_number_16947;

    /** 				option = StringToken()*/
    RefDS(_5);
    _0 = _option_59307;
    _option_59307 = _62StringToken(_5);
    DeRef(_0);

    /** 				if length(option) > 0 then*/
    if (IS_SEQUENCE(_option_59307)){
            _30394 = SEQ_PTR(_option_59307)->length;
    }
    else {
        _30394 = 1;
    }
    if (_30394 <= 0)
    goto L35; // [1101] 1135

    /** 					if line_number = gotword then*/
    if (_38line_number_16947 != _gotword_59427)
    goto L36; // [1109] 1121

    /** 						CompileErr(116)*/
    RefDS(_22663);
    _46CompileErr(116, _22663, 0);
L36: 

    /** 					bp -= length(option)*/
    if (IS_SEQUENCE(_option_59307)){
            _30397 = SEQ_PTR(_option_59307)->length;
    }
    else {
        _30397 = 1;
    }
    _46bp_49486 = _46bp_49486 - _30397;
    _30397 = NOVALUE;
L35: 

    /** 				if not dead_ifdef then*/
    if (_dead_ifdef_59311 != 0)
    goto L21; // [1137] 674

    /** 					if has_matched then*/
    if (_has_matched_59309 == 0)
    {
        goto L37; // [1142] 1164
    }
    else{
    }

    /** 						in_matched = 0*/
    _in_matched_59310 = 0;

    /** 						No_new_entry = 1*/
    _55No_new_entry_48227 = 1;

    /** 						read_line()*/
    _62read_line();
    goto L21; // [1161] 674
L37: 

    /** 						No_new_entry = 0*/
    _55No_new_entry_48227 = 0;

    /** 						in_elsedef = 1*/
    _in_elsedef_59312 = 1;

    /** 						call_proc(parser_id, {})*/
    _0 = (int)_00[_parser_id_59319].addr;
    (*(int (*)())_0)(
                         );

    /** 						tok_match(END)*/
    _41tok_match(402, 0);

    /** 						tok_match(IFDEF, END)*/
    _41tok_match(407, 402);

    /** 						live_ifdef -= 1*/
    _41live_ifdef_59303 = _41live_ifdef_59303 - 1;

    /** 						ifdef_lineno = ifdef_lineno[1..$-1]*/
    if (IS_SEQUENCE(_41ifdef_lineno_59304)){
            _30401 = SEQ_PTR(_41ifdef_lineno_59304)->length;
    }
    else {
        _30401 = 1;
    }
    _30402 = _30401 - 1;
    _30401 = NOVALUE;
    rhs_slice_target = (object_ptr)&_41ifdef_lineno_59304;
    RHS_Slice(_41ifdef_lineno_59304, 1, _30402);

    /** 						return*/
    DeRef(_option_59307);
    DeRef(_tok_59314);
    DeRefi(_keyw_59315);
    DeRef(_30301);
    _30301 = NOVALUE;
    DeRef(_30307);
    _30307 = NOVALUE;
    DeRef(_30336);
    _30336 = NOVALUE;
    _30402 = NOVALUE;
    DeRef(_30382);
    _30382 = NOVALUE;
    return;
    goto L21; // [1232] 674
L34: 

    /** 			elsif tok[T_ID] = IFDEF then*/
    _2 = (int)SEQ_PTR(_tok_59314);
    _30404 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30404, 407)){
        _30404 = NOVALUE;
        goto L38; // [1245] 1258
    }
    _30404 = NOVALUE;

    /** 				dead_ifdef += 1*/
    _dead_ifdef_59311 = _dead_ifdef_59311 + 1;
    goto L21; // [1255] 674
L38: 

    /** 			elsif tok[T_ID] = INCLUDE then*/
    _2 = (int)SEQ_PTR(_tok_59314);
    _30407 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30407, 418)){
        _30407 = NOVALUE;
        goto L39; // [1268] 1279
    }
    _30407 = NOVALUE;

    /** 				read_line()*/
    _62read_line();
    goto L21; // [1276] 674
L39: 

    /** 			elsif tok[T_ID] = CASE then*/
    _2 = (int)SEQ_PTR(_tok_59314);
    _30409 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30409, 186)){
        _30409 = NOVALUE;
        goto L21; // [1289] 674
    }
    _30409 = NOVALUE;

    /** 				tok = next_token()*/
    _0 = _tok_59314;
    _tok_59314 = _41next_token();
    DeRef(_0);

    /** 		end while*/
    goto L21; // [1302] 674
L2C: 

    /** 	end while*/
    goto L5; // [1309] 105
L26: 

    /** 	live_ifdef -= 1*/
    _41live_ifdef_59303 = _41live_ifdef_59303 - 1;

    /** 	ifdef_lineno = ifdef_lineno[1..$-1]*/
    if (IS_SEQUENCE(_41ifdef_lineno_59304)){
            _30413 = SEQ_PTR(_41ifdef_lineno_59304)->length;
    }
    else {
        _30413 = 1;
    }
    _30414 = _30413 - 1;
    _30413 = NOVALUE;
    rhs_slice_target = (object_ptr)&_41ifdef_lineno_59304;
    RHS_Slice(_41ifdef_lineno_59304, 1, _30414);

    /** 	No_new_entry = 0*/
    _55No_new_entry_48227 = 0;

    /** end procedure*/
    DeRef(_option_59307);
    DeRef(_tok_59314);
    DeRefi(_keyw_59315);
    DeRef(_30301);
    _30301 = NOVALUE;
    DeRef(_30307);
    _30307 = NOVALUE;
    DeRef(_30336);
    _30336 = NOVALUE;
    DeRef(_30402);
    _30402 = NOVALUE;
    DeRef(_30382);
    _30382 = NOVALUE;
    _30414 = NOVALUE;
    return;
    ;
}


int _41SetPrivateScope(int _s_59575, int _type_sym_59577, int _n_59578)
{
    int _hashval_59579 = NOVALUE;
    int _scope_59580 = NOVALUE;
    int _t_59582 = NOVALUE;
    int _30435 = NOVALUE;
    int _30434 = NOVALUE;
    int _30433 = NOVALUE;
    int _30432 = NOVALUE;
    int _30431 = NOVALUE;
    int _30430 = NOVALUE;
    int _30427 = NOVALUE;
    int _30424 = NOVALUE;
    int _30422 = NOVALUE;
    int _30420 = NOVALUE;
    int _30416 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_s_59575)) {
        _1 = (long)(DBL_PTR(_s_59575)->dbl);
        if (UNIQUE(DBL_PTR(_s_59575)) && (DBL_PTR(_s_59575)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_59575);
        _s_59575 = _1;
    }

    /** 	scope = SymTab[s][S_SCOPE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30416 = (int)*(((s1_ptr)_2)->base + _s_59575);
    _2 = (int)SEQ_PTR(_30416);
    _scope_59580 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_scope_59580)){
        _scope_59580 = (long)DBL_PTR(_scope_59580)->dbl;
    }
    _30416 = NOVALUE;

    /** 	switch scope do*/
    _0 = _scope_59580;
    switch ( _0 ){ 

        /** 		case SC_PRIVATE then*/
        case 3:

        /** 			DefinedYet(s)*/
        _55DefinedYet(_s_59575);

        /** 			Block_var( s )*/
        _68Block_var(_s_59575);

        /** 			return s*/
        return _s_59575;
        goto L1; // [50] 260

        /** 		case SC_LOOP_VAR then*/
        case 2:

        /** 			DefinedYet(s)*/
        _55DefinedYet(_s_59575);

        /** 			return s*/
        return _s_59575;
        goto L1; // [67] 260

        /** 		case SC_UNDEFINED, SC_MULTIPLY_DEFINED then*/
        case 9:
        case 10:

        /** 			SymTab[s][S_SCOPE] = SC_PRIVATE*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_s_59575 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 4);
        _1 = *(int *)_2;
        *(int *)_2 = 3;
        DeRef(_1);
        _30420 = NOVALUE;

        /** 			SymTab[s][S_VARNUM] = n*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_s_59575 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 16);
        _1 = *(int *)_2;
        *(int *)_2 = _n_59578;
        DeRef(_1);
        _30422 = NOVALUE;

        /** 			SymTab[s][S_VTYPE] = type_sym*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_s_59575 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 15);
        _1 = *(int *)_2;
        *(int *)_2 = _type_sym_59577;
        DeRef(_1);
        _30424 = NOVALUE;

        /** 			if type_sym < 0 then*/
        if (_type_sym_59577 >= 0)
        goto L2; // [124] 135

        /** 				register_forward_type( s, type_sym )*/
        _40register_forward_type(_s_59575, _type_sym_59577);
L2: 

        /** 			Block_var( s )*/
        _68Block_var(_s_59575);

        /** 			return s*/
        return _s_59575;
        goto L1; // [146] 260

        /** 		case SC_LOCAL, SC_GLOBAL, SC_PREDEF, SC_PUBLIC, SC_EXPORT then*/
        case 5:
        case 6:
        case 7:
        case 13:
        case 11:

        /** 			hashval = SymTab[s][S_HASHVAL]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _30427 = (int)*(((s1_ptr)_2)->base + _s_59575);
        _2 = (int)SEQ_PTR(_30427);
        _hashval_59579 = (int)*(((s1_ptr)_2)->base + 11);
        if (!IS_ATOM_INT(_hashval_59579)){
            _hashval_59579 = (long)DBL_PTR(_hashval_59579)->dbl;
        }
        _30427 = NOVALUE;

        /** 			t = buckets[hashval]*/
        _2 = (int)SEQ_PTR(_55buckets_47051);
        _t_59582 = (int)*(((s1_ptr)_2)->base + _hashval_59579);
        if (!IS_ATOM_INT(_t_59582)){
            _t_59582 = (long)DBL_PTR(_t_59582)->dbl;
        }

        /** 			buckets[hashval] = NewEntry(SymTab[s][S_NAME], n, SC_PRIVATE,*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _30430 = (int)*(((s1_ptr)_2)->base + _s_59575);
        _2 = (int)SEQ_PTR(_30430);
        if (!IS_ATOM_INT(_38S_NAME_16598)){
            _30431 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
        }
        else{
            _30431 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
        }
        _30430 = NOVALUE;
        Ref(_30431);
        _30432 = _55NewEntry(_30431, _n_59578, 3, -100, _hashval_59579, _t_59582, _type_sym_59577);
        _30431 = NOVALUE;
        _2 = (int)SEQ_PTR(_55buckets_47051);
        _2 = (int)(((s1_ptr)_2)->base + _hashval_59579);
        _1 = *(int *)_2;
        *(int *)_2 = _30432;
        if( _1 != _30432 ){
            DeRef(_1);
        }
        _30432 = NOVALUE;

        /** 			Block_var( buckets[hashval] )*/
        _2 = (int)SEQ_PTR(_55buckets_47051);
        _30433 = (int)*(((s1_ptr)_2)->base + _hashval_59579);
        Ref(_30433);
        _68Block_var(_30433);
        _30433 = NOVALUE;

        /** 			return buckets[hashval]*/
        _2 = (int)SEQ_PTR(_55buckets_47051);
        _30434 = (int)*(((s1_ptr)_2)->base + _hashval_59579);
        Ref(_30434);
        return _30434;
        goto L1; // [243] 260

        /** 		case else*/
        default:

        /** 			InternalErr(267, {scope})*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _scope_59580;
        _30435 = MAKE_SEQ(_1);
        _46InternalErr(267, _30435);
        _30435 = NOVALUE;
    ;}L1: 

    /** 	return 0*/
    _30434 = NOVALUE;
    return 0;
    ;
}


void _41For_statement()
{
    int _bp1_59647 = NOVALUE;
    int _bp2_59648 = NOVALUE;
    int _exit_base_59649 = NOVALUE;
    int _next_base_59650 = NOVALUE;
    int _end_op_59651 = NOVALUE;
    int _tok_59653 = NOVALUE;
    int _loop_var_59654 = NOVALUE;
    int _loop_var_sym_59656 = NOVALUE;
    int _save_syms_59657 = NOVALUE;
    int _30482 = NOVALUE;
    int _30480 = NOVALUE;
    int _30479 = NOVALUE;
    int _30473 = NOVALUE;
    int _30471 = NOVALUE;
    int _30469 = NOVALUE;
    int _30468 = NOVALUE;
    int _30467 = NOVALUE;
    int _30466 = NOVALUE;
    int _30464 = NOVALUE;
    int _30462 = NOVALUE;
    int _30461 = NOVALUE;
    int _30460 = NOVALUE;
    int _30459 = NOVALUE;
    int _30457 = NOVALUE;
    int _30455 = NOVALUE;
    int _30451 = NOVALUE;
    int _30449 = NOVALUE;
    int _30446 = NOVALUE;
    int _30444 = NOVALUE;
    int _30438 = NOVALUE;
    int _30437 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	sequence save_syms*/

    /** 	Start_block( FOR )*/
    _68Start_block(21, 0);

    /** 	loop_var = next_token()*/
    _0 = _loop_var_59654;
    _loop_var_59654 = _41next_token();
    DeRef(_0);

    /** 	if not find(loop_var[T_ID], ADDR_TOKS) then*/
    _2 = (int)SEQ_PTR(_loop_var_59654);
    _30437 = (int)*(((s1_ptr)_2)->base + 1);
    _30438 = find_from(_30437, _39ADDR_TOKS_16551, 1);
    _30437 = NOVALUE;
    if (_30438 != 0)
    goto L1; // [31] 42
    _30438 = NOVALUE;

    /** 		CompileErr(28)*/
    RefDS(_22663);
    _46CompileErr(28, _22663, 0);
L1: 

    /** 	if BIND then*/
    if (_38BIND_16567 == 0)
    {
        goto L2; // [46] 55
    }
    else{
    }

    /** 		add_ref(loop_var)*/
    Ref(_loop_var_59654);
    _55add_ref(_loop_var_59654);
L2: 

    /** 	tok_match(EQUALS)*/
    _41tok_match(3, 0);

    /** 	exit_base = length(exit_list)*/
    if (IS_SEQUENCE(_41exit_list_55142)){
            _exit_base_59649 = SEQ_PTR(_41exit_list_55142)->length;
    }
    else {
        _exit_base_59649 = 1;
    }

    /** 	next_base = length(continue_list)*/
    if (IS_SEQUENCE(_41continue_list_55144)){
            _next_base_59650 = SEQ_PTR(_41continue_list_55144)->length;
    }
    else {
        _next_base_59650 = 1;
    }

    /** 	Expr()*/
    _41Expr();

    /** 	tok_match(TO)*/
    _41tok_match(403, 0);

    /** 	exit_base = length(exit_list)*/
    if (IS_SEQUENCE(_41exit_list_55142)){
            _exit_base_59649 = SEQ_PTR(_41exit_list_55142)->length;
    }
    else {
        _exit_base_59649 = 1;
    }

    /** 	Expr()*/
    _41Expr();

    /** 	tok = next_token()*/
    _0 = _tok_59653;
    _tok_59653 = _41next_token();
    DeRef(_0);

    /** 	if tok[T_ID] = BY then*/
    _2 = (int)SEQ_PTR(_tok_59653);
    _30444 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30444, 404)){
        _30444 = NOVALUE;
        goto L3; // [115] 135
    }
    _30444 = NOVALUE;

    /** 		Expr()*/
    _41Expr();

    /** 		end_op = ENDFOR_GENERAL -- will be set at runtime by FOR op*/
    _end_op_59651 = 39;
    goto L4; // [132] 159
L3: 

    /** 		emit_opnd(NewIntSym(1))*/
    _30446 = _55NewIntSym(1);
    _43emit_opnd(_30446);
    _30446 = NOVALUE;

    /** 		putback(tok)*/
    Ref(_tok_59653);
    _41putback(_tok_59653);

    /** 		end_op = ENDFOR_INT_UP1*/
    _end_op_59651 = 54;
L4: 

    /** 	loop_var_sym = loop_var[T_SYM]*/
    _2 = (int)SEQ_PTR(_loop_var_59654);
    _loop_var_sym_59656 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_loop_var_sym_59656)){
        _loop_var_sym_59656 = (long)DBL_PTR(_loop_var_sym_59656)->dbl;
    }

    /** 	if CurrentSub = TopLevelSub then*/
    if (_38CurrentSub_16954 != _38TopLevelSub_16953)
    goto L5; // [175] 221

    /** 		DefinedYet(loop_var_sym)*/
    _55DefinedYet(_loop_var_sym_59656);

    /** 		SymTab[loop_var_sym][S_SCOPE] = SC_GLOOP_VAR*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_loop_var_sym_59656 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 4;
    DeRef(_1);
    _30449 = NOVALUE;

    /** 		SymTab[loop_var_sym][S_VTYPE] = object_type*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_loop_var_sym_59656 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 15);
    _1 = *(int *)_2;
    *(int *)_2 = _55object_type_47055;
    DeRef(_1);
    _30451 = NOVALUE;
    goto L6; // [218] 265
L5: 

    /** 		loop_var_sym = SetPrivateScope(loop_var_sym, object_type, param_num)*/
    _loop_var_sym_59656 = _41SetPrivateScope(_loop_var_sym_59656, _55object_type_47055, _41param_num_55131);
    if (!IS_ATOM_INT(_loop_var_sym_59656)) {
        _1 = (long)(DBL_PTR(_loop_var_sym_59656)->dbl);
        if (UNIQUE(DBL_PTR(_loop_var_sym_59656)) && (DBL_PTR(_loop_var_sym_59656)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_loop_var_sym_59656);
        _loop_var_sym_59656 = _1;
    }

    /** 		param_num += 1*/
    _41param_num_55131 = _41param_num_55131 + 1;

    /** 		SymTab[loop_var_sym][S_SCOPE] = SC_LOOP_VAR*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_loop_var_sym_59656 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _30455 = NOVALUE;

    /** 		Pop_block_var()*/
    _68Pop_block_var();
L6: 

    /** 	SymTab[loop_var_sym][S_USAGE] = or_bits(SymTab[loop_var_sym][S_USAGE], U_USED)*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_loop_var_sym_59656 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30459 = (int)*(((s1_ptr)_2)->base + _loop_var_sym_59656);
    _2 = (int)SEQ_PTR(_30459);
    _30460 = (int)*(((s1_ptr)_2)->base + 5);
    _30459 = NOVALUE;
    if (IS_ATOM_INT(_30460)) {
        {unsigned long tu;
             tu = (unsigned long)_30460 | (unsigned long)3;
             _30461 = MAKE_UINT(tu);
        }
    }
    else {
        _30461 = binary_op(OR_BITS, _30460, 3);
    }
    _30460 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _30461;
    if( _1 != _30461 ){
        DeRef(_1);
    }
    _30461 = NOVALUE;
    _30457 = NOVALUE;

    /** 	op_info1 = loop_var_sym*/
    _43op_info1_51218 = _loop_var_sym_59656;

    /** 	emit_op(FOR)*/
    _43emit_op(21);

    /** 	emit_addr(loop_var_sym)*/
    _43emit_addr(_loop_var_sym_59656);

    /** 	if finish_block_header(FOR) then*/
    _30462 = _41finish_block_header(21);
    if (_30462 == 0) {
        DeRef(_30462);
        _30462 = NOVALUE;
        goto L7; // [325] 336
    }
    else {
        if (!IS_ATOM_INT(_30462) && DBL_PTR(_30462)->dbl == 0.0){
            DeRef(_30462);
            _30462 = NOVALUE;
            goto L7; // [325] 336
        }
        DeRef(_30462);
        _30462 = NOVALUE;
    }
    DeRef(_30462);
    _30462 = NOVALUE;

    /** 		CompileErr(83)*/
    RefDS(_22663);
    _46CompileErr(83, _22663, 0);
L7: 

    /** 	entry_addr &= 0*/
    Append(&_41entry_addr_55146, _41entry_addr_55146, 0);

    /** 	bp1 = length(Code)+1*/
    if (IS_SEQUENCE(_38Code_17038)){
            _30464 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _30464 = 1;
    }
    _bp1_59647 = _30464 + 1;
    _30464 = NOVALUE;

    /** 	emit_addr(0) -- will be patched - don't straighten*/
    _43emit_addr(0);

    /** 	save_syms = Code[$-5..$-3] -- could be temps, but can't get rid of them yet*/
    if (IS_SEQUENCE(_38Code_17038)){
            _30466 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _30466 = 1;
    }
    _30467 = _30466 - 5;
    _30466 = NOVALUE;
    if (IS_SEQUENCE(_38Code_17038)){
            _30468 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _30468 = 1;
    }
    _30469 = _30468 - 3;
    _30468 = NOVALUE;
    rhs_slice_target = (object_ptr)&_save_syms_59657;
    RHS_Slice(_38Code_17038, _30467, _30469);

    /** 	for i = 1 to 3 do*/
    {
        int _i_59745;
        _i_59745 = 1;
L8: 
        if (_i_59745 > 3){
            goto L9; // [385] 408
        }

        /** 		clear_temp( save_syms[i] )*/
        _2 = (int)SEQ_PTR(_save_syms_59657);
        _30471 = (int)*(((s1_ptr)_2)->base + _i_59745);
        Ref(_30471);
        _43clear_temp(_30471);
        _30471 = NOVALUE;

        /** 	end for*/
        _i_59745 = _i_59745 + 1;
        goto L8; // [403] 392
L9: 
        ;
    }

    /** 	flush_temps()*/
    RefDS(_22663);
    _43flush_temps(_22663);

    /** 	bp2 = length(Code)*/
    if (IS_SEQUENCE(_38Code_17038)){
            _bp2_59648 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _bp2_59648 = 1;
    }

    /** 	retry_addr &= bp2 + 1*/
    _30473 = _bp2_59648 + 1;
    Append(&_41retry_addr_55148, _41retry_addr_55148, _30473);
    _30473 = NOVALUE;

    /** 	continue_addr &= 0*/
    Append(&_41continue_addr_55147, _41continue_addr_55147, 0);

    /** 	loop_stack &= FOR*/
    Append(&_41loop_stack_55156, _41loop_stack_55156, 21);

    /** 	if not TRANSLATE then*/
    if (_38TRANSLATE_16564 != 0)
    goto LA; // [454] 478

    /** 		if OpTrace then*/
    if (_38OpTrace_17015 == 0)
    {
        goto LB; // [461] 477
    }
    else{
    }

    /** 			emit_op(DISPLAY_VAR)*/
    _43emit_op(87);

    /** 			emit_addr(loop_var_sym)*/
    _43emit_addr(_loop_var_sym_59656);
LB: 
LA: 

    /** 	Statement_list()*/
    _41Statement_list();

    /** 	tok_match(END)*/
    _41tok_match(402, 0);

    /** 	tok_match(FOR, END)*/
    _41tok_match(21, 402);

    /** 	End_block( FOR )*/
    _68End_block(21);

    /** 	StartSourceLine(TRUE, TRANSLATE)*/
    _43StartSourceLine(_9TRUE_428, _38TRANSLATE_16564, 2);

    /** 	op_info1 = loop_var_sym*/
    _43op_info1_51218 = _loop_var_sym_59656;

    /** 	op_info2 = bp2 + 1*/
    _43op_info2_51219 = _bp2_59648 + 1;

    /** 	PatchNList(next_base)*/
    _41PatchNList(_next_base_59650);

    /** 	emit_op(end_op)*/
    _43emit_op(_end_op_59651);

    /** 	backpatch(bp1, length(Code)+1)*/
    if (IS_SEQUENCE(_38Code_17038)){
            _30479 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _30479 = 1;
    }
    _30480 = _30479 + 1;
    _30479 = NOVALUE;
    _43backpatch(_bp1_59647, _30480);
    _30480 = NOVALUE;

    /** 	if not TRANSLATE then*/
    if (_38TRANSLATE_16564 != 0)
    goto LC; // [566] 590

    /** 		if OpTrace then*/
    if (_38OpTrace_17015 == 0)
    {
        goto LD; // [573] 589
    }
    else{
    }

    /** 			emit_op(ERASE_SYMBOL)*/
    _43emit_op(90);

    /** 			emit_addr(loop_var_sym)*/
    _43emit_addr(_loop_var_sym_59656);
LD: 
LC: 

    /** 	Hide(loop_var_sym)*/
    _55Hide(_loop_var_sym_59656);

    /** 	exit_loop(exit_base)*/
    _41exit_loop(_exit_base_59649);

    /** 	for i = 1 to 3 do*/
    {
        int _i_59791;
        _i_59791 = 1;
LE: 
        if (_i_59791 > 3){
            goto LF; // [602] 628
        }

        /** 		emit_temp( save_syms[i], NEW_REFERENCE )*/
        _2 = (int)SEQ_PTR(_save_syms_59657);
        _30482 = (int)*(((s1_ptr)_2)->base + _i_59791);
        Ref(_30482);
        _43emit_temp(_30482, 1);
        _30482 = NOVALUE;

        /** 	end for*/
        _i_59791 = _i_59791 + 1;
        goto LE; // [623] 609
LF: 
        ;
    }

    /** 	flush_temps()*/
    RefDS(_22663);
    _43flush_temps(_22663);

    /** end procedure*/
    DeRef(_tok_59653);
    DeRef(_loop_var_59654);
    DeRef(_save_syms_59657);
    DeRef(_30467);
    _30467 = NOVALUE;
    DeRef(_30469);
    _30469 = NOVALUE;
    return;
    ;
}


int _41CompileType(int _type_ptr_59799)
{
    int _t_59800 = NOVALUE;
    int _30493 = NOVALUE;
    int _30492 = NOVALUE;
    int _30491 = NOVALUE;
    int _30485 = NOVALUE;
    int _30484 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_type_ptr_59799)) {
        _1 = (long)(DBL_PTR(_type_ptr_59799)->dbl);
        if (UNIQUE(DBL_PTR(_type_ptr_59799)) && (DBL_PTR(_type_ptr_59799)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_type_ptr_59799);
        _type_ptr_59799 = _1;
    }

    /** 	if type_ptr < 0 then*/
    if (_type_ptr_59799 >= 0)
    goto L1; // [5] 16

    /** 		return type_ptr*/
    return _type_ptr_59799;
L1: 

    /** 	if SymTab[type_ptr][S_TOKEN] = OBJECT then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30484 = (int)*(((s1_ptr)_2)->base + _type_ptr_59799);
    _2 = (int)SEQ_PTR(_30484);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _30485 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _30485 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _30484 = NOVALUE;
    if (binary_op_a(NOTEQ, _30485, 415)){
        _30485 = NOVALUE;
        goto L2; // [32] 47
    }
    _30485 = NOVALUE;

    /** 		return TYPE_OBJECT*/
    return 16;
    goto L3; // [44] 218
L2: 

    /** 	elsif type_ptr = integer_type then*/
    if (_type_ptr_59799 != _55integer_type_47061)
    goto L4; // [51] 66

    /** 		return TYPE_INTEGER*/
    return 1;
    goto L3; // [63] 218
L4: 

    /** 	elsif type_ptr = atom_type then*/
    if (_type_ptr_59799 != _55atom_type_47057)
    goto L5; // [70] 85

    /** 		return TYPE_ATOM*/
    return 4;
    goto L3; // [82] 218
L5: 

    /** 	elsif type_ptr = sequence_type then*/
    if (_type_ptr_59799 != _55sequence_type_47059)
    goto L6; // [89] 104

    /** 		return TYPE_SEQUENCE*/
    return 8;
    goto L3; // [101] 218
L6: 

    /** 	elsif type_ptr = object_type then*/
    if (_type_ptr_59799 != _55object_type_47055)
    goto L7; // [108] 123

    /** 		return TYPE_OBJECT*/
    return 16;
    goto L3; // [120] 218
L7: 

    /** 		t = SymTab[SymTab[type_ptr][S_NEXT]][S_VTYPE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30491 = (int)*(((s1_ptr)_2)->base + _type_ptr_59799);
    _2 = (int)SEQ_PTR(_30491);
    _30492 = (int)*(((s1_ptr)_2)->base + 2);
    _30491 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_30492)){
        _30493 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_30492)->dbl));
    }
    else{
        _30493 = (int)*(((s1_ptr)_2)->base + _30492);
    }
    _2 = (int)SEQ_PTR(_30493);
    _t_59800 = (int)*(((s1_ptr)_2)->base + 15);
    if (!IS_ATOM_INT(_t_59800)){
        _t_59800 = (long)DBL_PTR(_t_59800)->dbl;
    }
    _30493 = NOVALUE;

    /** 		if t = integer_type then*/
    if (_t_59800 != _55integer_type_47061)
    goto L8; // [155] 170

    /** 			return TYPE_INTEGER*/
    _30492 = NOVALUE;
    return 1;
    goto L9; // [167] 217
L8: 

    /** 		elsif t = atom_type then*/
    if (_t_59800 != _55atom_type_47057)
    goto LA; // [174] 189

    /** 			return TYPE_ATOM*/
    _30492 = NOVALUE;
    return 4;
    goto L9; // [186] 217
LA: 

    /** 		elsif t = sequence_type then*/
    if (_t_59800 != _55sequence_type_47059)
    goto LB; // [193] 208

    /** 			return TYPE_SEQUENCE*/
    _30492 = NOVALUE;
    return 8;
    goto L9; // [205] 217
LB: 

    /** 			return TYPE_OBJECT*/
    _30492 = NOVALUE;
    return 16;
L9: 
L3: 
    ;
}


int _41get_assigned_sym()
{
    int _30506 = NOVALUE;
    int _30505 = NOVALUE;
    int _30504 = NOVALUE;
    int _30502 = NOVALUE;
    int _30501 = NOVALUE;
    int _30500 = NOVALUE;
    int _30499 = NOVALUE;
    int _30498 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not find( Code[$-2], {ASSIGN, ASSIGN_I}) then*/
    if (IS_SEQUENCE(_38Code_17038)){
            _30498 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _30498 = 1;
    }
    _30499 = _30498 - 2;
    _30498 = NOVALUE;
    _2 = (int)SEQ_PTR(_38Code_17038);
    _30500 = (int)*(((s1_ptr)_2)->base + _30499);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 18;
    ((int *)_2)[2] = 113;
    _30501 = MAKE_SEQ(_1);
    _30502 = find_from(_30500, _30501, 1);
    _30500 = NOVALUE;
    DeRefDS(_30501);
    _30501 = NOVALUE;
    if (_30502 != 0)
    goto L1; // [29] 39
    _30502 = NOVALUE;

    /** 		return 0*/
    _30499 = NOVALUE;
    return 0;
L1: 

    /** 	return Code[$-1]*/
    if (IS_SEQUENCE(_38Code_17038)){
            _30504 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _30504 = 1;
    }
    _30505 = _30504 - 1;
    _30504 = NOVALUE;
    _2 = (int)SEQ_PTR(_38Code_17038);
    _30506 = (int)*(((s1_ptr)_2)->base + _30505);
    Ref(_30506);
    DeRef(_30499);
    _30499 = NOVALUE;
    _30505 = NOVALUE;
    return _30506;
    ;
}


void _41Assign_Constant(int _sym_59869)
{
    int _valsym_59871 = NOVALUE;
    int _val_59874 = NOVALUE;
    int _30533 = NOVALUE;
    int _30532 = NOVALUE;
    int _30530 = NOVALUE;
    int _30529 = NOVALUE;
    int _30528 = NOVALUE;
    int _30526 = NOVALUE;
    int _30525 = NOVALUE;
    int _30524 = NOVALUE;
    int _30522 = NOVALUE;
    int _30521 = NOVALUE;
    int _30520 = NOVALUE;
    int _30518 = NOVALUE;
    int _30517 = NOVALUE;
    int _30516 = NOVALUE;
    int _30514 = NOVALUE;
    int _30512 = NOVALUE;
    int _30510 = NOVALUE;
    int _30508 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	symtab_index valsym = Pop() -- pop the sym for the constant, too*/
    _valsym_59871 = _43Pop();
    if (!IS_ATOM_INT(_valsym_59871)) {
        _1 = (long)(DBL_PTR(_valsym_59871)->dbl);
        if (UNIQUE(DBL_PTR(_valsym_59871)) && (DBL_PTR(_valsym_59871)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_valsym_59871);
        _valsym_59871 = _1;
    }

    /** 	object val = SymTab[valsym][S_OBJ]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30508 = (int)*(((s1_ptr)_2)->base + _valsym_59871);
    DeRef(_val_59874);
    _2 = (int)SEQ_PTR(_30508);
    _val_59874 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_val_59874);
    _30508 = NOVALUE;

    /** 	SymTab[sym][S_OBJ] = val*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59869 + ((s1_ptr)_2)->base);
    Ref(_val_59874);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _val_59874;
    DeRef(_1);
    _30510 = NOVALUE;

    /** 	SymTab[sym][S_INITLEVEL] = 0*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59869 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 14);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _30512 = NOVALUE;

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L1; // [58] 197
    }
    else{
    }

    /** 		SymTab[sym][S_GTYPE] = SymTab[valsym][S_GTYPE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59869 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30516 = (int)*(((s1_ptr)_2)->base + _valsym_59871);
    _2 = (int)SEQ_PTR(_30516);
    _30517 = (int)*(((s1_ptr)_2)->base + 36);
    _30516 = NOVALUE;
    Ref(_30517);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = _30517;
    if( _1 != _30517 ){
        DeRef(_1);
    }
    _30517 = NOVALUE;
    _30514 = NOVALUE;

    /** 		SymTab[sym][S_SEQ_ELEM] = SymTab[valsym][S_SEQ_ELEM]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59869 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30520 = (int)*(((s1_ptr)_2)->base + _valsym_59871);
    _2 = (int)SEQ_PTR(_30520);
    _30521 = (int)*(((s1_ptr)_2)->base + 33);
    _30520 = NOVALUE;
    Ref(_30521);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = _30521;
    if( _1 != _30521 ){
        DeRef(_1);
    }
    _30521 = NOVALUE;
    _30518 = NOVALUE;

    /** 		SymTab[sym][S_OBJ_MIN] = SymTab[valsym][S_OBJ_MIN]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59869 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30524 = (int)*(((s1_ptr)_2)->base + _valsym_59871);
    _2 = (int)SEQ_PTR(_30524);
    _30525 = (int)*(((s1_ptr)_2)->base + 30);
    _30524 = NOVALUE;
    Ref(_30525);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 30);
    _1 = *(int *)_2;
    *(int *)_2 = _30525;
    if( _1 != _30525 ){
        DeRef(_1);
    }
    _30525 = NOVALUE;
    _30522 = NOVALUE;

    /** 		SymTab[sym][S_OBJ_MAX] = SymTab[valsym][S_OBJ_MAX]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59869 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30528 = (int)*(((s1_ptr)_2)->base + _valsym_59871);
    _2 = (int)SEQ_PTR(_30528);
    _30529 = (int)*(((s1_ptr)_2)->base + 31);
    _30528 = NOVALUE;
    Ref(_30529);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 31);
    _1 = *(int *)_2;
    *(int *)_2 = _30529;
    if( _1 != _30529 ){
        DeRef(_1);
    }
    _30529 = NOVALUE;
    _30526 = NOVALUE;

    /** 		SymTab[sym][S_SEQ_LEN] = SymTab[valsym][S_SEQ_LEN]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59869 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30532 = (int)*(((s1_ptr)_2)->base + _valsym_59871);
    _2 = (int)SEQ_PTR(_30532);
    _30533 = (int)*(((s1_ptr)_2)->base + 32);
    _30532 = NOVALUE;
    Ref(_30533);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _30533;
    if( _1 != _30533 ){
        DeRef(_1);
    }
    _30533 = NOVALUE;
    _30530 = NOVALUE;
L1: 

    /** end procedure*/
    DeRef(_val_59874);
    return;
    ;
}


int _41Global_declaration(int _type_ptr_59931, int _scope_59932)
{
    int _new_symbols_59933 = NOVALUE;
    int _tok_59935 = NOVALUE;
    int _tsym_59936 = NOVALUE;
    int _prevtok_59937 = NOVALUE;
    int _sym_59939 = NOVALUE;
    int _valsym_59940 = NOVALUE;
    int _prevsym_59941 = NOVALUE;
    int _deltasym_59942 = NOVALUE;
    int _h_59943 = NOVALUE;
    int _count_59944 = NOVALUE;
    int _val_59945 = NOVALUE;
    int _usedval_59946 = NOVALUE;
    int _deltafunc_59947 = NOVALUE;
    int _delta_59948 = NOVALUE;
    int _is_fwd_ref_59949 = NOVALUE;
    int _ptok_59966 = NOVALUE;
    int _negate_59982 = NOVALUE;
    int _one_60322 = NOVALUE;
    int _32352 = NOVALUE;
    int _32351 = NOVALUE;
    int _32350 = NOVALUE;
    int _30743 = NOVALUE;
    int _30741 = NOVALUE;
    int _30739 = NOVALUE;
    int _30737 = NOVALUE;
    int _30735 = NOVALUE;
    int _30732 = NOVALUE;
    int _30730 = NOVALUE;
    int _30729 = NOVALUE;
    int _30728 = NOVALUE;
    int _30727 = NOVALUE;
    int _30726 = NOVALUE;
    int _30725 = NOVALUE;
    int _30723 = NOVALUE;
    int _30721 = NOVALUE;
    int _30719 = NOVALUE;
    int _30717 = NOVALUE;
    int _30716 = NOVALUE;
    int _30715 = NOVALUE;
    int _30713 = NOVALUE;
    int _30712 = NOVALUE;
    int _30711 = NOVALUE;
    int _30710 = NOVALUE;
    int _30709 = NOVALUE;
    int _30708 = NOVALUE;
    int _30705 = NOVALUE;
    int _30704 = NOVALUE;
    int _30702 = NOVALUE;
    int _30701 = NOVALUE;
    int _30697 = NOVALUE;
    int _30694 = NOVALUE;
    int _30692 = NOVALUE;
    int _30688 = NOVALUE;
    int _30687 = NOVALUE;
    int _30686 = NOVALUE;
    int _30683 = NOVALUE;
    int _30679 = NOVALUE;
    int _30678 = NOVALUE;
    int _30677 = NOVALUE;
    int _30676 = NOVALUE;
    int _30675 = NOVALUE;
    int _30674 = NOVALUE;
    int _30671 = NOVALUE;
    int _30668 = NOVALUE;
    int _30666 = NOVALUE;
    int _30663 = NOVALUE;
    int _30662 = NOVALUE;
    int _30661 = NOVALUE;
    int _30659 = NOVALUE;
    int _30657 = NOVALUE;
    int _30656 = NOVALUE;
    int _30655 = NOVALUE;
    int _30654 = NOVALUE;
    int _30652 = NOVALUE;
    int _30651 = NOVALUE;
    int _30650 = NOVALUE;
    int _30649 = NOVALUE;
    int _30645 = NOVALUE;
    int _30644 = NOVALUE;
    int _30643 = NOVALUE;
    int _30642 = NOVALUE;
    int _30641 = NOVALUE;
    int _30640 = NOVALUE;
    int _30637 = NOVALUE;
    int _30635 = NOVALUE;
    int _30634 = NOVALUE;
    int _30633 = NOVALUE;
    int _30632 = NOVALUE;
    int _30631 = NOVALUE;
    int _30628 = NOVALUE;
    int _30626 = NOVALUE;
    int _30624 = NOVALUE;
    int _30623 = NOVALUE;
    int _30622 = NOVALUE;
    int _30621 = NOVALUE;
    int _30620 = NOVALUE;
    int _30619 = NOVALUE;
    int _30618 = NOVALUE;
    int _30616 = NOVALUE;
    int _30613 = NOVALUE;
    int _30611 = NOVALUE;
    int _30610 = NOVALUE;
    int _30609 = NOVALUE;
    int _30608 = NOVALUE;
    int _30607 = NOVALUE;
    int _30606 = NOVALUE;
    int _30605 = NOVALUE;
    int _30604 = NOVALUE;
    int _30601 = NOVALUE;
    int _30600 = NOVALUE;
    int _30599 = NOVALUE;
    int _30597 = NOVALUE;
    int _30596 = NOVALUE;
    int _30595 = NOVALUE;
    int _30594 = NOVALUE;
    int _30593 = NOVALUE;
    int _30591 = NOVALUE;
    int _30590 = NOVALUE;
    int _30589 = NOVALUE;
    int _30587 = NOVALUE;
    int _30586 = NOVALUE;
    int _30584 = NOVALUE;
    int _30581 = NOVALUE;
    int _30579 = NOVALUE;
    int _30577 = NOVALUE;
    int _30574 = NOVALUE;
    int _30569 = NOVALUE;
    int _30562 = NOVALUE;
    int _30561 = NOVALUE;
    int _30559 = NOVALUE;
    int _30556 = NOVALUE;
    int _30549 = NOVALUE;
    int _30546 = NOVALUE;
    int _30545 = NOVALUE;
    int _30543 = NOVALUE;
    int _30539 = NOVALUE;
    int _30538 = NOVALUE;
    int _30537 = NOVALUE;
    int _30536 = NOVALUE;
    int _30535 = NOVALUE;
    int _30534 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_type_ptr_59931)) {
        _1 = (long)(DBL_PTR(_type_ptr_59931)->dbl);
        if (UNIQUE(DBL_PTR(_type_ptr_59931)) && (DBL_PTR(_type_ptr_59931)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_type_ptr_59931);
        _type_ptr_59931 = _1;
    }

    /** 	object tsym*/

    /** 	object prevtok = 0*/
    DeRef(_prevtok_59937);
    _prevtok_59937 = 0;

    /** 	symtab_index sym, valsym, prevsym = 0, deltasym = 0*/
    _prevsym_59941 = 0;
    _deltasym_59942 = 0;

    /** 	integer h, count = 0*/
    _count_59944 = 0;

    /** 	atom val = 1, usedval*/
    _val_59945 = 1;

    /** 	integer deltafunc = '+'*/
    _deltafunc_59947 = 43;

    /** 	atom delta = 1*/
    DeRef(_delta_59948);
    _delta_59948 = 1;

    /** 	new_symbols = {}*/
    RefDS(_22663);
    DeRefi(_new_symbols_59933);
    _new_symbols_59933 = _22663;

    /** 	integer is_fwd_ref = 0*/
    _is_fwd_ref_59949 = 0;

    /** 	if type_ptr > 0 and SymTab[type_ptr][S_SCOPE] = SC_UNDEFINED then*/
    _30534 = (_type_ptr_59931 > 0);
    if (_30534 == 0) {
        goto L1; // [62] 117
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30536 = (int)*(((s1_ptr)_2)->base + _type_ptr_59931);
    _2 = (int)SEQ_PTR(_30536);
    _30537 = (int)*(((s1_ptr)_2)->base + 4);
    _30536 = NOVALUE;
    if (IS_ATOM_INT(_30537)) {
        _30538 = (_30537 == 9);
    }
    else {
        _30538 = binary_op(EQUALS, _30537, 9);
    }
    _30537 = NOVALUE;
    if (_30538 == 0) {
        DeRef(_30538);
        _30538 = NOVALUE;
        goto L1; // [85] 117
    }
    else {
        if (!IS_ATOM_INT(_30538) && DBL_PTR(_30538)->dbl == 0.0){
            DeRef(_30538);
            _30538 = NOVALUE;
            goto L1; // [85] 117
        }
        DeRef(_30538);
        _30538 = NOVALUE;
    }
    DeRef(_30538);
    _30538 = NOVALUE;

    /** 		is_fwd_ref = 1*/
    _is_fwd_ref_59949 = 1;

    /** 		Hide(type_ptr)*/
    _55Hide(_type_ptr_59931);

    /** 		type_ptr = -new_forward_reference( TYPE, type_ptr )*/
    DeRef(_32352);
    _32352 = 504;
    _30539 = _40new_forward_reference(504, _type_ptr_59931, 504);
    _32352 = NOVALUE;
    if (IS_ATOM_INT(_30539)) {
        if ((unsigned long)_30539 == 0xC0000000)
        _type_ptr_59931 = (int)NewDouble((double)-0xC0000000);
        else
        _type_ptr_59931 = - _30539;
    }
    else {
        _type_ptr_59931 = unary_op(UMINUS, _30539);
    }
    DeRef(_30539);
    _30539 = NOVALUE;
    if (!IS_ATOM_INT(_type_ptr_59931)) {
        _1 = (long)(DBL_PTR(_type_ptr_59931)->dbl);
        if (UNIQUE(DBL_PTR(_type_ptr_59931)) && (DBL_PTR(_type_ptr_59931)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_type_ptr_59931);
        _type_ptr_59931 = _1;
    }
L1: 

    /** 	if type_ptr = -1 then*/
    if (_type_ptr_59931 != -1)
    goto L2; // [119] 495

    /** 		sequence ptok = next_token()*/
    _0 = _ptok_59966;
    _ptok_59966 = _41next_token();
    DeRef(_0);

    /** 		if ptok[T_ID] = TYPE_DECL then*/
    _2 = (int)SEQ_PTR(_ptok_59966);
    _30543 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30543, 416)){
        _30543 = NOVALUE;
        goto L3; // [140] 183
    }
    _30543 = NOVALUE;

    /** 			putback(keyfind("enum",-1))*/
    RefDS(_27019);
    DeRef(_32350);
    _32350 = _27019;
    _32351 = _55hashfn(_32350);
    _32350 = NOVALUE;
    RefDS(_27019);
    _30545 = _55keyfind(_27019, -1, _38current_file_no_16946, 0, _32351);
    _32351 = NOVALUE;
    _41putback(_30545);
    _30545 = NOVALUE;

    /** 			SubProg(TYPE_DECL, scope)*/
    _41SubProg(416, _scope_59932);

    /** 			return {}*/
    RefDS(_22663);
    DeRefDS(_ptok_59966);
    DeRefi(_new_symbols_59933);
    DeRef(_tok_59935);
    DeRef(_prevtok_59937);
    DeRef(_delta_59948);
    DeRef(_30534);
    _30534 = NOVALUE;
    return _22663;
    goto L4; // [180] 494
L3: 

    /** 		elsif ptok[T_ID] = BY then*/
    _2 = (int)SEQ_PTR(_ptok_59966);
    _30546 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30546, 404)){
        _30546 = NOVALUE;
        goto L5; // [193] 456
    }
    _30546 = NOVALUE;

    /** 			integer negate = 0*/
    _negate_59982 = 0;

    /** 			ptok = next_token()*/
    _0 = _ptok_59966;
    _ptok_59966 = _41next_token();
    DeRefDS(_0);

    /** 			switch ptok[T_ID] do*/
    _2 = (int)SEQ_PTR(_ptok_59966);
    _30549 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_30549) ){
        goto L6; // [217] 296
    }
    if(!IS_ATOM_INT(_30549)){
        if( (DBL_PTR(_30549)->dbl != (double) ((int) DBL_PTR(_30549)->dbl) ) ){
            goto L6; // [217] 296
        }
        _0 = (int) DBL_PTR(_30549)->dbl;
    }
    else {
        _0 = _30549;
    };
    _30549 = NOVALUE;
    switch ( _0 ){ 

        /** 				case reserved:MULTIPLY then*/
        case 13:

        /** 					deltafunc = '*'*/
        _deltafunc_59947 = 42;

        /** 					ptok = next_token()*/
        _0 = _ptok_59966;
        _ptok_59966 = _41next_token();
        DeRef(_0);
        goto L7; // [238] 304

        /** 				case reserved:DIVIDE then*/
        case 14:

        /** 					deltafunc = '/'*/
        _deltafunc_59947 = 47;

        /** 					ptok = next_token()*/
        _0 = _ptok_59966;
        _ptok_59966 = _41next_token();
        DeRef(_0);
        goto L7; // [256] 304

        /** 				case MINUS then*/
        case 10:

        /** 					deltafunc = '-'*/
        _deltafunc_59947 = 45;

        /** 					ptok = next_token()*/
        _0 = _ptok_59966;
        _ptok_59966 = _41next_token();
        DeRef(_0);
        goto L7; // [274] 304

        /** 				case PLUS then*/
        case 11:

        /** 					deltafunc = '+'*/
        _deltafunc_59947 = 43;

        /** 					ptok = next_token()*/
        _0 = _ptok_59966;
        _ptok_59966 = _41next_token();
        DeRef(_0);
        goto L7; // [292] 304

        /** 				case else*/
        default:
L6: 

        /** 					deltafunc = '+'*/
        _deltafunc_59947 = 43;
    ;}L7: 

    /** 			if ptok[T_ID] = MINUS then*/
    _2 = (int)SEQ_PTR(_ptok_59966);
    _30556 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30556, 10)){
        _30556 = NOVALUE;
        goto L8; // [314] 331
    }
    _30556 = NOVALUE;

    /** 				negate = 1*/
    _negate_59982 = 1;

    /** 				ptok = next_token()*/
    _0 = _ptok_59966;
    _ptok_59966 = _41next_token();
    DeRefDS(_0);
L8: 

    /** 			if ptok[T_ID] != ATOM then*/
    _2 = (int)SEQ_PTR(_ptok_59966);
    _30559 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _30559, 502)){
        _30559 = NOVALUE;
        goto L9; // [341] 353
    }
    _30559 = NOVALUE;

    /** 				CompileErr( 344 )*/
    RefDS(_22663);
    _46CompileErr(344, _22663, 0);
L9: 

    /** 			delta = SymTab[ptok[T_SYM]][S_OBJ]*/
    _2 = (int)SEQ_PTR(_ptok_59966);
    _30561 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_30561)){
        _30562 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_30561)->dbl));
    }
    else{
        _30562 = (int)*(((s1_ptr)_2)->base + _30561);
    }
    DeRef(_delta_59948);
    _2 = (int)SEQ_PTR(_30562);
    _delta_59948 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_delta_59948);
    _30562 = NOVALUE;

    /** 			if negate then*/
    if (_negate_59982 == 0)
    {
        goto LA; // [375] 384
    }
    else{
    }

    /** 				delta = -delta*/
    _0 = _delta_59948;
    if (IS_ATOM_INT(_delta_59948)) {
        if ((unsigned long)_delta_59948 == 0xC0000000)
        _delta_59948 = (int)NewDouble((double)-0xC0000000);
        else
        _delta_59948 = - _delta_59948;
    }
    else {
        _delta_59948 = unary_op(UMINUS, _delta_59948);
    }
    DeRef(_0);
LA: 

    /** 			switch deltafunc do*/
    _0 = _deltafunc_59947;
    switch ( _0 ){ 

        /** 				case '/' then*/
        case 47:

        /** 					delta = 1 / delta*/
        _0 = _delta_59948;
        if (IS_ATOM_INT(_delta_59948)) {
            _delta_59948 = (1 % _delta_59948) ? NewDouble((double)1 / _delta_59948) : (1 / _delta_59948);
        }
        else {
            _delta_59948 = NewDouble((double)1 / DBL_PTR(_delta_59948)->dbl);
        }
        DeRef(_0);

        /** 					deltafunc = '*'*/
        _deltafunc_59947 = 42;
        goto LB; // [406] 423

        /** 				case '-' then*/
        case 45:

        /** 					delta = -delta*/
        _0 = _delta_59948;
        if (IS_ATOM_INT(_delta_59948)) {
            if ((unsigned long)_delta_59948 == 0xC0000000)
            _delta_59948 = (int)NewDouble((double)-0xC0000000);
            else
            _delta_59948 = - _delta_59948;
        }
        else {
            _delta_59948 = unary_op(UMINUS, _delta_59948);
        }
        DeRef(_0);

        /** 					deltafunc = '+'*/
        _deltafunc_59947 = 43;
    ;}LB: 

    /** 			if integer(delta) then*/
    if (IS_ATOM_INT(_delta_59948))
    _30569 = 1;
    else if (IS_ATOM_DBL(_delta_59948))
    _30569 = IS_ATOM_INT(DoubleToInt(_delta_59948));
    else
    _30569 = 0;
    if (_30569 == 0)
    {
        _30569 = NOVALUE;
        goto LC; // [428] 442
    }
    else{
        _30569 = NOVALUE;
    }

    /** 				deltasym = NewIntSym(delta)*/
    Ref(_delta_59948);
    _deltasym_59942 = _55NewIntSym(_delta_59948);
    if (!IS_ATOM_INT(_deltasym_59942)) {
        _1 = (long)(DBL_PTR(_deltasym_59942)->dbl);
        if (UNIQUE(DBL_PTR(_deltasym_59942)) && (DBL_PTR(_deltasym_59942)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_deltasym_59942);
        _deltasym_59942 = _1;
    }
    goto LD; // [439] 451
LC: 

    /** 				deltasym = NewDoubleSym(delta)*/
    Ref(_delta_59948);
    _deltasym_59942 = _55NewDoubleSym(_delta_59948);
    if (!IS_ATOM_INT(_deltasym_59942)) {
        _1 = (long)(DBL_PTR(_deltasym_59942)->dbl);
        if (UNIQUE(DBL_PTR(_deltasym_59942)) && (DBL_PTR(_deltasym_59942)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_deltasym_59942);
        _deltasym_59942 = _1;
    }
LD: 
    goto L4; // [453] 494
L5: 

    /** 			deltasym = NewIntSym(1)*/
    _deltasym_59942 = _55NewIntSym(1);
    if (!IS_ATOM_INT(_deltasym_59942)) {
        _1 = (long)(DBL_PTR(_deltasym_59942)->dbl);
        if (UNIQUE(DBL_PTR(_deltasym_59942)) && (DBL_PTR(_deltasym_59942)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_deltasym_59942);
        _deltasym_59942 = _1;
    }

    /** 			if deltasym > 0 then*/
    if (_deltasym_59942 <= 0)
    goto LE; // [466] 488

    /** 				SymTab[deltasym][S_USAGE] = U_READ*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_deltasym_59942 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _30574 = NOVALUE;
LE: 

    /** 			putback(ptok)*/
    RefDS(_ptok_59966);
    _41putback(_ptok_59966);
L4: 
L2: 
    DeRef(_ptok_59966);
    _ptok_59966 = NOVALUE;

    /** 	valsym = 0*/
    _valsym_59940 = 0;

    /** 	while TRUE do*/
LF: 
    if (_9TRUE_428 == 0)
    {
        goto L10; // [511] 2131
    }
    else{
    }

    /** 		tok = next_token()*/
    _0 = _tok_59935;
    _tok_59935 = _41next_token();
    DeRef(_0);

    /** 		if tok[T_ID] = DOLLAR then*/
    _2 = (int)SEQ_PTR(_tok_59935);
    _30577 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30577, -22)){
        _30577 = NOVALUE;
        goto L11; // [529] 568
    }
    _30577 = NOVALUE;

    /** 			if not equal(prevtok, 0) then*/
    if (_prevtok_59937 == 0)
    _30579 = 1;
    else if (IS_ATOM_INT(_prevtok_59937) && IS_ATOM_INT(0))
    _30579 = 0;
    else
    _30579 = (compare(_prevtok_59937, 0) == 0);
    if (_30579 != 0)
    goto L12; // [539] 567
    _30579 = NOVALUE;

    /** 				if prevtok[T_ID] = COMMA then*/
    _2 = (int)SEQ_PTR(_prevtok_59937);
    _30581 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30581, -30)){
        _30581 = NOVALUE;
        goto L13; // [552] 566
    }
    _30581 = NOVALUE;

    /** 					tok = next_token()*/
    _0 = _tok_59935;
    _tok_59935 = _41next_token();
    DeRef(_0);

    /** 					exit*/
    goto L10; // [563] 2131
L13: 
L12: 
L11: 

    /** 		if tok[T_ID] = END_OF_FILE then*/
    _2 = (int)SEQ_PTR(_tok_59935);
    _30584 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30584, -21)){
        _30584 = NOVALUE;
        goto L14; // [578] 590
    }
    _30584 = NOVALUE;

    /** 			CompileErr( 32 )*/
    RefDS(_22663);
    _46CompileErr(32, _22663, 0);
L14: 

    /** 		if not find(tok[T_ID], ADDR_TOKS) then*/
    _2 = (int)SEQ_PTR(_tok_59935);
    _30586 = (int)*(((s1_ptr)_2)->base + 1);
    _30587 = find_from(_30586, _39ADDR_TOKS_16551, 1);
    _30586 = NOVALUE;
    if (_30587 != 0)
    goto L15; // [605] 630
    _30587 = NOVALUE;

    /** 			CompileErr(25, {find_category(tok[T_ID])} )*/
    _2 = (int)SEQ_PTR(_tok_59935);
    _30589 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_30589);
    _30590 = _65find_category(_30589);
    _30589 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _30590;
    _30591 = MAKE_SEQ(_1);
    _30590 = NOVALUE;
    _46CompileErr(25, _30591, 0);
    _30591 = NOVALUE;
L15: 

    /** 		sym = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_59935);
    _sym_59939 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_sym_59939)){
        _sym_59939 = (long)DBL_PTR(_sym_59939)->dbl;
    }

    /** 		DefinedYet(sym)*/
    _55DefinedYet(_sym_59939);

    /** 		if find(SymTab[sym][S_SCOPE], {SC_GLOBAL, SC_PREDEF, SC_PUBLIC, SC_EXPORT}) then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30593 = (int)*(((s1_ptr)_2)->base + _sym_59939);
    _2 = (int)SEQ_PTR(_30593);
    _30594 = (int)*(((s1_ptr)_2)->base + 4);
    _30593 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 6;
    *((int *)(_2+8)) = 7;
    *((int *)(_2+12)) = 13;
    *((int *)(_2+16)) = 11;
    _30595 = MAKE_SEQ(_1);
    _30596 = find_from(_30594, _30595, 1);
    _30594 = NOVALUE;
    DeRefDS(_30595);
    _30595 = NOVALUE;
    if (_30596 == 0)
    {
        _30596 = NOVALUE;
        goto L16; // [679] 741
    }
    else{
        _30596 = NOVALUE;
    }

    /** 			h = SymTab[sym][S_HASHVAL]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30597 = (int)*(((s1_ptr)_2)->base + _sym_59939);
    _2 = (int)SEQ_PTR(_30597);
    _h_59943 = (int)*(((s1_ptr)_2)->base + 11);
    if (!IS_ATOM_INT(_h_59943)){
        _h_59943 = (long)DBL_PTR(_h_59943)->dbl;
    }
    _30597 = NOVALUE;

    /** 			sym = NewEntry(SymTab[sym][S_NAME], 0, 0, VARIABLE, h, buckets[h], 0)*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30599 = (int)*(((s1_ptr)_2)->base + _sym_59939);
    _2 = (int)SEQ_PTR(_30599);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _30600 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _30600 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _30599 = NOVALUE;
    _2 = (int)SEQ_PTR(_55buckets_47051);
    _30601 = (int)*(((s1_ptr)_2)->base + _h_59943);
    Ref(_30600);
    Ref(_30601);
    _sym_59939 = _55NewEntry(_30600, 0, 0, -100, _h_59943, _30601, 0);
    _30600 = NOVALUE;
    _30601 = NOVALUE;
    if (!IS_ATOM_INT(_sym_59939)) {
        _1 = (long)(DBL_PTR(_sym_59939)->dbl);
        if (UNIQUE(DBL_PTR(_sym_59939)) && (DBL_PTR(_sym_59939)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_59939);
        _sym_59939 = _1;
    }

    /** 			buckets[h] = sym*/
    _2 = (int)SEQ_PTR(_55buckets_47051);
    _2 = (int)(((s1_ptr)_2)->base + _h_59943);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_59939;
    DeRef(_1);
L16: 

    /** 		new_symbols = append(new_symbols, sym)*/
    Append(&_new_symbols_59933, _new_symbols_59933, _sym_59939);

    /** 		Block_var( sym )*/
    _68Block_var(_sym_59939);

    /** 		if SymTab[sym][S_SCOPE] = SC_UNDEFINED and SymTab[sym][S_FILE_NO] != current_file_no then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30604 = (int)*(((s1_ptr)_2)->base + _sym_59939);
    _2 = (int)SEQ_PTR(_30604);
    _30605 = (int)*(((s1_ptr)_2)->base + 4);
    _30604 = NOVALUE;
    if (IS_ATOM_INT(_30605)) {
        _30606 = (_30605 == 9);
    }
    else {
        _30606 = binary_op(EQUALS, _30605, 9);
    }
    _30605 = NOVALUE;
    if (IS_ATOM_INT(_30606)) {
        if (_30606 == 0) {
            goto L17; // [772] 816
        }
    }
    else {
        if (DBL_PTR(_30606)->dbl == 0.0) {
            goto L17; // [772] 816
        }
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30608 = (int)*(((s1_ptr)_2)->base + _sym_59939);
    _2 = (int)SEQ_PTR(_30608);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _30609 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _30609 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _30608 = NOVALUE;
    if (IS_ATOM_INT(_30609)) {
        _30610 = (_30609 != _38current_file_no_16946);
    }
    else {
        _30610 = binary_op(NOTEQ, _30609, _38current_file_no_16946);
    }
    _30609 = NOVALUE;
    if (_30610 == 0) {
        DeRef(_30610);
        _30610 = NOVALUE;
        goto L17; // [795] 816
    }
    else {
        if (!IS_ATOM_INT(_30610) && DBL_PTR(_30610)->dbl == 0.0){
            DeRef(_30610);
            _30610 = NOVALUE;
            goto L17; // [795] 816
        }
        DeRef(_30610);
        _30610 = NOVALUE;
    }
    DeRef(_30610);
    _30610 = NOVALUE;

    /** 			SymTab[sym][S_FILE_NO] = current_file_no*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_FILE_NO_16594))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    _1 = *(int *)_2;
    *(int *)_2 = _38current_file_no_16946;
    DeRef(_1);
    _30611 = NOVALUE;
L17: 

    /** 		SymTab[sym][S_SCOPE] = scope*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _scope_59932;
    DeRef(_1);
    _30613 = NOVALUE;

    /** 		if type_ptr = 0 then*/
    if (_type_ptr_59931 != 0)
    goto L18; // [833] 1168

    /** 			SymTab[sym][S_MODE] = M_CONSTANT*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _30616 = NOVALUE;

    /** 			buckets[SymTab[sym][S_HASHVAL]] = SymTab[sym][S_SAMEHASH]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30618 = (int)*(((s1_ptr)_2)->base + _sym_59939);
    _2 = (int)SEQ_PTR(_30618);
    _30619 = (int)*(((s1_ptr)_2)->base + 11);
    _30618 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30620 = (int)*(((s1_ptr)_2)->base + _sym_59939);
    _2 = (int)SEQ_PTR(_30620);
    _30621 = (int)*(((s1_ptr)_2)->base + 9);
    _30620 = NOVALUE;
    Ref(_30621);
    _2 = (int)SEQ_PTR(_55buckets_47051);
    if (!IS_ATOM_INT(_30619))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_30619)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _30619);
    _1 = *(int *)_2;
    *(int *)_2 = _30621;
    if( _1 != _30621 ){
        DeRef(_1);
    }
    _30621 = NOVALUE;

    /** 			tok_match(EQUALS)*/
    _41tok_match(3, 0);

    /** 			StartSourceLine(FALSE, , COVERAGE_OVERRIDE)*/
    _43StartSourceLine(_9FALSE_426, 0, 3);

    /** 			emit_opnd(sym)*/
    _43emit_opnd(_sym_59939);

    /** 			Expr()  -- no new symbols can be defined in here*/
    _41Expr();

    /** 			buckets[SymTab[sym][S_HASHVAL]] = sym*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30622 = (int)*(((s1_ptr)_2)->base + _sym_59939);
    _2 = (int)SEQ_PTR(_30622);
    _30623 = (int)*(((s1_ptr)_2)->base + 11);
    _30622 = NOVALUE;
    _2 = (int)SEQ_PTR(_55buckets_47051);
    if (!IS_ATOM_INT(_30623))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_30623)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _30623);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_59939;
    DeRef(_1);

    /** 			SymTab[sym][S_USAGE] = U_WRITTEN*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _30624 = NOVALUE;

    /** 			if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L19; // [955] 993
    }
    else{
    }

    /** 				SymTab[sym][S_GTYPE] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);
    _30626 = NOVALUE;

    /** 				SymTab[sym][S_OBJ] = NOVALUE -- distinguish from literals*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);
    _30628 = NOVALUE;
L19: 

    /** 			valsym = Top()*/
    _valsym_59940 = _43Top();
    if (!IS_ATOM_INT(_valsym_59940)) {
        _1 = (long)(DBL_PTR(_valsym_59940)->dbl);
        if (UNIQUE(DBL_PTR(_valsym_59940)) && (DBL_PTR(_valsym_59940)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_valsym_59940);
        _valsym_59940 = _1;
    }

    /** 			if valsym > 0 and compare( SymTab[valsym][S_OBJ], NOVALUE ) then*/
    _30631 = (_valsym_59940 > 0);
    if (_30631 == 0) {
        goto L1A; // [1006] 1047
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30633 = (int)*(((s1_ptr)_2)->base + _valsym_59940);
    _2 = (int)SEQ_PTR(_30633);
    _30634 = (int)*(((s1_ptr)_2)->base + 1);
    _30633 = NOVALUE;
    if (IS_ATOM_INT(_30634) && IS_ATOM_INT(_38NOVALUE_16800)){
        _30635 = (_30634 < _38NOVALUE_16800) ? -1 : (_30634 > _38NOVALUE_16800);
    }
    else{
        _30635 = compare(_30634, _38NOVALUE_16800);
    }
    _30634 = NOVALUE;
    if (_30635 == 0)
    {
        _30635 = NOVALUE;
        goto L1A; // [1029] 1047
    }
    else{
        _30635 = NOVALUE;
    }

    /** 				Assign_Constant( sym )*/
    _41Assign_Constant(_sym_59939);

    /** 				sym = Pop()*/
    _sym_59939 = _43Pop();
    if (!IS_ATOM_INT(_sym_59939)) {
        _1 = (long)(DBL_PTR(_sym_59939)->dbl);
        if (UNIQUE(DBL_PTR(_sym_59939)) && (DBL_PTR(_sym_59939)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_59939);
        _sym_59939 = _1;
    }
    goto L1B; // [1044] 2090
L1A: 

    /** 				emit_op(ASSIGN)*/
    _43emit_op(18);

    /** 				if Last_op() = ASSIGN then*/
    _30637 = _43Last_op();
    if (binary_op_a(NOTEQ, _30637, 18)){
        DeRef(_30637);
        _30637 = NOVALUE;
        goto L1C; // [1061] 1075
    }
    DeRef(_30637);
    _30637 = NOVALUE;

    /** 					valsym = get_assigned_sym()*/
    _valsym_59940 = _41get_assigned_sym();
    if (!IS_ATOM_INT(_valsym_59940)) {
        _1 = (long)(DBL_PTR(_valsym_59940)->dbl);
        if (UNIQUE(DBL_PTR(_valsym_59940)) && (DBL_PTR(_valsym_59940)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_valsym_59940);
        _valsym_59940 = _1;
    }
    goto L1D; // [1072] 1083
L1C: 

    /** 					valsym = -1*/
    _valsym_59940 = -1;
L1D: 

    /** 				if valsym > 0 and compare( SymTab[valsym][S_OBJ], NOVALUE ) then*/
    _30640 = (_valsym_59940 > 0);
    if (_30640 == 0) {
        goto L1E; // [1089] 1131
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30642 = (int)*(((s1_ptr)_2)->base + _valsym_59940);
    _2 = (int)SEQ_PTR(_30642);
    _30643 = (int)*(((s1_ptr)_2)->base + 1);
    _30642 = NOVALUE;
    if (IS_ATOM_INT(_30643) && IS_ATOM_INT(_38NOVALUE_16800)){
        _30644 = (_30643 < _38NOVALUE_16800) ? -1 : (_30643 > _38NOVALUE_16800);
    }
    else{
        _30644 = compare(_30643, _38NOVALUE_16800);
    }
    _30643 = NOVALUE;
    if (_30644 == 0)
    {
        _30644 = NOVALUE;
        goto L1E; // [1112] 1131
    }
    else{
        _30644 = NOVALUE;
    }

    /** 					SymTab[sym][S_CODE] = valsym*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_CODE_16610))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_CODE_16610);
    _1 = *(int *)_2;
    *(int *)_2 = _valsym_59940;
    DeRef(_1);
    _30645 = NOVALUE;
L1E: 

    /** 				if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L1B; // [1135] 2090
    }
    else{
    }

    /** 					count += 1*/
    _count_59944 = _count_59944 + 1;

    /** 					if count = 10 then*/
    if (_count_59944 != 10)
    goto L1B; // [1146] 2090

    /** 						count = 0*/
    _count_59944 = 0;

    /** 						emit_op( RETURNT )*/
    _43emit_op(34);
    goto L1B; // [1165] 2090
L18: 

    /** 		elsif type_ptr = -1 and not is_fwd_ref then*/
    _30649 = (_type_ptr_59931 == -1);
    if (_30649 == 0) {
        goto L1F; // [1174] 1917
    }
    _30651 = (_is_fwd_ref_59949 == 0);
    if (_30651 == 0)
    {
        DeRef(_30651);
        _30651 = NOVALUE;
        goto L1F; // [1182] 1917
    }
    else{
        DeRef(_30651);
        _30651 = NOVALUE;
    }

    /** 			StartSourceLine(FALSE, , COVERAGE_OVERRIDE )*/
    _43StartSourceLine(_9FALSE_426, 0, 3);

    /** 			SymTab[sym][S_MODE] = M_CONSTANT*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _30652 = NOVALUE;

    /** 			buckets[SymTab[sym][S_HASHVAL]] = SymTab[sym][S_SAMEHASH]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30654 = (int)*(((s1_ptr)_2)->base + _sym_59939);
    _2 = (int)SEQ_PTR(_30654);
    _30655 = (int)*(((s1_ptr)_2)->base + 11);
    _30654 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30656 = (int)*(((s1_ptr)_2)->base + _sym_59939);
    _2 = (int)SEQ_PTR(_30656);
    _30657 = (int)*(((s1_ptr)_2)->base + 9);
    _30656 = NOVALUE;
    Ref(_30657);
    _2 = (int)SEQ_PTR(_55buckets_47051);
    if (!IS_ATOM_INT(_30655))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_30655)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _30655);
    _1 = *(int *)_2;
    *(int *)_2 = _30657;
    if( _1 != _30657 ){
        DeRef(_1);
    }
    _30657 = NOVALUE;

    /** 			tok = next_token()*/
    _0 = _tok_59935;
    _tok_59935 = _41next_token();
    DeRef(_0);

    /** 			StartSourceLine(FALSE, , COVERAGE_OVERRIDE)*/
    _43StartSourceLine(_9FALSE_426, 0, 3);

    /** 			emit_opnd(sym)*/
    _43emit_opnd(_sym_59939);

    /** 			if tok[T_ID] = EQUALS then*/
    _2 = (int)SEQ_PTR(_tok_59935);
    _30659 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30659, 3)){
        _30659 = NOVALUE;
        goto L20; // [1276] 1521
    }
    _30659 = NOVALUE;

    /** 				Expr()*/
    _41Expr();

    /** 				buckets[SymTab[sym][S_HASHVAL]] = sym*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30661 = (int)*(((s1_ptr)_2)->base + _sym_59939);
    _2 = (int)SEQ_PTR(_30661);
    _30662 = (int)*(((s1_ptr)_2)->base + 11);
    _30661 = NOVALUE;
    _2 = (int)SEQ_PTR(_55buckets_47051);
    if (!IS_ATOM_INT(_30662))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_30662)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _30662);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_59939;
    DeRef(_1);

    /** 				SymTab[sym][S_USAGE] = U_WRITTEN*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _30663 = NOVALUE;

    /** 				valsym = Top()*/
    _valsym_59940 = _43Top();
    if (!IS_ATOM_INT(_valsym_59940)) {
        _1 = (long)(DBL_PTR(_valsym_59940)->dbl);
        if (UNIQUE(DBL_PTR(_valsym_59940)) && (DBL_PTR(_valsym_59940)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_valsym_59940);
        _valsym_59940 = _1;
    }

    /** 				if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L21; // [1332] 1370
    }
    else{
    }

    /** 					SymTab[sym][S_GTYPE] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);
    _30666 = NOVALUE;

    /** 					SymTab[sym][S_OBJ] = NOVALUE     -- distinguish from literals*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);
    _30668 = NOVALUE;
L21: 

    /** 				valsym = Top()*/
    _valsym_59940 = _43Top();
    if (!IS_ATOM_INT(_valsym_59940)) {
        _1 = (long)(DBL_PTR(_valsym_59940)->dbl);
        if (UNIQUE(DBL_PTR(_valsym_59940)) && (DBL_PTR(_valsym_59940)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_valsym_59940);
        _valsym_59940 = _1;
    }

    /** 				emit_op(ASSIGN)*/
    _43emit_op(18);

    /** 				if Last_op() = ASSIGN then*/
    _30671 = _43Last_op();
    if (binary_op_a(NOTEQ, _30671, 18)){
        DeRef(_30671);
        _30671 = NOVALUE;
        goto L22; // [1391] 1405
    }
    DeRef(_30671);
    _30671 = NOVALUE;

    /** 					valsym = get_assigned_sym()*/
    _valsym_59940 = _41get_assigned_sym();
    if (!IS_ATOM_INT(_valsym_59940)) {
        _1 = (long)(DBL_PTR(_valsym_59940)->dbl);
        if (UNIQUE(DBL_PTR(_valsym_59940)) && (DBL_PTR(_valsym_59940)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_valsym_59940);
        _valsym_59940 = _1;
    }
    goto L23; // [1402] 1413
L22: 

    /** 					valsym = -1*/
    _valsym_59940 = -1;
L23: 

    /** 				if valsym > 0 and compare( SymTab[valsym][S_OBJ], NOVALUE ) then*/
    _30674 = (_valsym_59940 > 0);
    if (_30674 == 0) {
        goto L24; // [1419] 1461
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30676 = (int)*(((s1_ptr)_2)->base + _valsym_59940);
    _2 = (int)SEQ_PTR(_30676);
    _30677 = (int)*(((s1_ptr)_2)->base + 1);
    _30676 = NOVALUE;
    if (IS_ATOM_INT(_30677) && IS_ATOM_INT(_38NOVALUE_16800)){
        _30678 = (_30677 < _38NOVALUE_16800) ? -1 : (_30677 > _38NOVALUE_16800);
    }
    else{
        _30678 = compare(_30677, _38NOVALUE_16800);
    }
    _30677 = NOVALUE;
    if (_30678 == 0)
    {
        _30678 = NOVALUE;
        goto L24; // [1442] 1461
    }
    else{
        _30678 = NOVALUE;
    }

    /** 					SymTab[sym][S_CODE] = valsym*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_CODE_16610))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_CODE_16610);
    _1 = *(int *)_2;
    *(int *)_2 = _valsym_59940;
    DeRef(_1);
    _30679 = NOVALUE;
L24: 

    /** 				if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L25; // [1465] 1816
    }
    else{
    }

    /** 					count += 1*/
    _count_59944 = _count_59944 + 1;

    /** 					if count = 10 then*/
    if (_count_59944 != 10)
    goto L26; // [1476] 1493

    /** 						count = 0*/
    _count_59944 = 0;

    /** 						emit_op( RETURNT )*/
    _43emit_op(34);
L26: 

    /** 					SymTab[sym][S_USAGE] = U_READ*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _30683 = NOVALUE;

    /** 					valsym = get_assigned_sym()*/
    _valsym_59940 = _41get_assigned_sym();
    if (!IS_ATOM_INT(_valsym_59940)) {
        _1 = (long)(DBL_PTR(_valsym_59940)->dbl);
        if (UNIQUE(DBL_PTR(_valsym_59940)) && (DBL_PTR(_valsym_59940)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_valsym_59940);
        _valsym_59940 = _1;
    }
    goto L25; // [1518] 1816
L20: 

    /** 				buckets[SymTab[sym][S_HASHVAL]] = sym*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30686 = (int)*(((s1_ptr)_2)->base + _sym_59939);
    _2 = (int)SEQ_PTR(_30686);
    _30687 = (int)*(((s1_ptr)_2)->base + 11);
    _30686 = NOVALUE;
    _2 = (int)SEQ_PTR(_55buckets_47051);
    if (!IS_ATOM_INT(_30687))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_30687)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _30687);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_59939;
    DeRef(_1);

    /** 				SymTab[sym][S_USAGE] = U_WRITTEN*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _30688 = NOVALUE;

    /** 				if prevsym = 0 then*/
    if (_prevsym_59941 != 0)
    goto L27; // [1560] 1633

    /** 				    symtab_index one = NewIntSym(1)*/
    _one_60322 = _55NewIntSym(1);
    if (!IS_ATOM_INT(_one_60322)) {
        _1 = (long)(DBL_PTR(_one_60322)->dbl);
        if (UNIQUE(DBL_PTR(_one_60322)) && (DBL_PTR(_one_60322)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_one_60322);
        _one_60322 = _1;
    }

    /** 				    SymTab[one][S_USAGE] = U_USED*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_one_60322 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 3;
    DeRef(_1);
    _30692 = NOVALUE;

    /** 					emit_opnd(sym)*/
    _43emit_opnd(_sym_59939);

    /** 					emit_opnd(one)*/
    _43emit_opnd(_one_60322);

    /** 					SymTab[sym][S_CODE] = one*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_CODE_16610))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_CODE_16610);
    _1 = *(int *)_2;
    *(int *)_2 = _one_60322;
    DeRef(_1);
    _30694 = NOVALUE;

    /** 					emit_op(ASSIGN)*/
    _43emit_op(18);

    /** 					valsym = Top()*/
    _valsym_59940 = _43Top();
    if (!IS_ATOM_INT(_valsym_59940)) {
        _1 = (long)(DBL_PTR(_valsym_59940)->dbl);
        if (UNIQUE(DBL_PTR(_valsym_59940)) && (DBL_PTR(_valsym_59940)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_valsym_59940);
        _valsym_59940 = _1;
    }
    goto L28; // [1630] 1755
L27: 

    /** 					emit_opnd(deltasym)*/
    _43emit_opnd(_deltasym_59942);

    /** 					emit_opnd(prevsym)*/
    _43emit_opnd(_prevsym_59941);

    /** 					SymTab[deltasym][S_USAGE] = U_READ					*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_deltasym_59942 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _30697 = NOVALUE;

    /** 					switch deltafunc do*/
    _0 = _deltafunc_59947;
    switch ( _0 ){ 

        /** 					case '+', '-' then*/
        case 43:
        case 45:

        /** 						emit_op(reserved:PLUS)*/
        _43emit_op(11);
        goto L29; // [1678] 1690

        /** 					case else*/
        default:

        /** 						emit_op(reserved:MULTIPLY)*/
        _43emit_op(13);
    ;}L29: 

    /** 					SymTab[Top()][S_USAGE] = U_READ*/
    _30701 = _43Top();
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_30701))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_30701)->dbl));
    else
    _3 = (int)(_30701 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _30702 = NOVALUE;

    /** 					emit_opnd(sym)*/
    _43emit_opnd(_sym_59939);

    /** 					emit_opnd(Top())*/
    _30704 = _43Top();
    _43emit_opnd(_30704);
    _30704 = NOVALUE;

    /** 					emit_op(ASSIGN)*/
    _43emit_op(18);

    /** 					SymTab[sym][S_USAGE] = U_READ*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _30705 = NOVALUE;

    /** 					valsym = get_assigned_sym()*/
    _valsym_59940 = _41get_assigned_sym();
    if (!IS_ATOM_INT(_valsym_59940)) {
        _1 = (long)(DBL_PTR(_valsym_59940)->dbl);
        if (UNIQUE(DBL_PTR(_valsym_59940)) && (DBL_PTR(_valsym_59940)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_valsym_59940);
        _valsym_59940 = _1;
    }
L28: 

    /** 				if valsym > 0 and compare( SymTab[valsym][S_OBJ], NOVALUE ) then*/
    _30708 = (_valsym_59940 > 0);
    if (_30708 == 0) {
        goto L2A; // [1761] 1803
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30710 = (int)*(((s1_ptr)_2)->base + _valsym_59940);
    _2 = (int)SEQ_PTR(_30710);
    _30711 = (int)*(((s1_ptr)_2)->base + 1);
    _30710 = NOVALUE;
    if (IS_ATOM_INT(_30711) && IS_ATOM_INT(_38NOVALUE_16800)){
        _30712 = (_30711 < _38NOVALUE_16800) ? -1 : (_30711 > _38NOVALUE_16800);
    }
    else{
        _30712 = compare(_30711, _38NOVALUE_16800);
    }
    _30711 = NOVALUE;
    if (_30712 == 0)
    {
        _30712 = NOVALUE;
        goto L2A; // [1784] 1803
    }
    else{
        _30712 = NOVALUE;
    }

    /** 					SymTab[sym][S_CODE] = valsym*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_CODE_16610))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_CODE_16610);
    _1 = *(int *)_2;
    *(int *)_2 = _valsym_59940;
    DeRef(_1);
    _30713 = NOVALUE;
L2A: 

    /** 				putback(tok)*/
    Ref(_tok_59935);
    _41putback(_tok_59935);

    /** 				valsym = 0*/
    _valsym_59940 = 0;
L25: 

    /** 			emit_opnd(sym)*/
    _43emit_opnd(_sym_59939);

    /** 			op_info1 = sym*/
    _43op_info1_51218 = _sym_59939;

    /** 			emit_op(ATOM_CHECK)*/
    _43emit_op(101);

    /** 			buckets[SymTab[sym][S_HASHVAL]] = sym*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30715 = (int)*(((s1_ptr)_2)->base + _sym_59939);
    _2 = (int)SEQ_PTR(_30715);
    _30716 = (int)*(((s1_ptr)_2)->base + 11);
    _30715 = NOVALUE;
    _2 = (int)SEQ_PTR(_55buckets_47051);
    if (!IS_ATOM_INT(_30716))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_30716)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _30716);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_59939;
    DeRef(_1);

    /** 			SymTab[sym][S_USAGE] = U_WRITTEN*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _30717 = NOVALUE;

    /** 			if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L1B; // [1876] 2090
    }
    else{
    }

    /** 				SymTab[sym][S_GTYPE] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);
    _30719 = NOVALUE;

    /** 				SymTab[sym][S_OBJ] = NOVALUE     -- distinguish from literals*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);
    _30721 = NOVALUE;
    goto L1B; // [1914] 2090
L1F: 

    /** 			SymTab[sym][S_MODE] = M_NORMAL*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _30723 = NOVALUE;

    /** 			if type_ptr > 0 and SymTab[type_ptr][S_TOKEN] = OBJECT then*/
    _30725 = (_type_ptr_59931 > 0);
    if (_30725 == 0) {
        goto L2B; // [1940] 1986
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30727 = (int)*(((s1_ptr)_2)->base + _type_ptr_59931);
    _2 = (int)SEQ_PTR(_30727);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _30728 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _30728 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _30727 = NOVALUE;
    if (IS_ATOM_INT(_30728)) {
        _30729 = (_30728 == 415);
    }
    else {
        _30729 = binary_op(EQUALS, _30728, 415);
    }
    _30728 = NOVALUE;
    if (_30729 == 0) {
        DeRef(_30729);
        _30729 = NOVALUE;
        goto L2B; // [1963] 1986
    }
    else {
        if (!IS_ATOM_INT(_30729) && DBL_PTR(_30729)->dbl == 0.0){
            DeRef(_30729);
            _30729 = NOVALUE;
            goto L2B; // [1963] 1986
        }
        DeRef(_30729);
        _30729 = NOVALUE;
    }
    DeRef(_30729);
    _30729 = NOVALUE;

    /** 				SymTab[sym][S_VTYPE] = object_type*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 15);
    _1 = *(int *)_2;
    *(int *)_2 = _55object_type_47055;
    DeRef(_1);
    _30730 = NOVALUE;
    goto L2C; // [1983] 2015
L2B: 

    /** 				SymTab[sym][S_VTYPE] = type_ptr*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 15);
    _1 = *(int *)_2;
    *(int *)_2 = _type_ptr_59931;
    DeRef(_1);
    _30732 = NOVALUE;

    /** 				if type_ptr < 0 then*/
    if (_type_ptr_59931 >= 0)
    goto L2D; // [2003] 2014

    /** 					register_forward_type( sym, type_ptr )*/
    _40register_forward_type(_sym_59939, _type_ptr_59931);
L2D: 
L2C: 

    /** 			if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L2E; // [2019] 2042
    }
    else{
    }

    /** 				SymTab[sym][S_GTYPE] = CompileType(type_ptr)*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59939 + ((s1_ptr)_2)->base);
    _30737 = _41CompileType(_type_ptr_59931);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = _30737;
    if( _1 != _30737 ){
        DeRef(_1);
    }
    _30737 = NOVALUE;
    _30735 = NOVALUE;
L2E: 

    /** 	   		tok = next_token()*/
    _0 = _tok_59935;
    _tok_59935 = _41next_token();
    DeRef(_0);

    /**    			putback(tok)*/
    Ref(_tok_59935);
    _41putback(_tok_59935);

    /** 	   		if tok[T_ID] = EQUALS then -- assign on declare*/
    _2 = (int)SEQ_PTR(_tok_59935);
    _30739 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30739, 3)){
        _30739 = NOVALUE;
        goto L2F; // [2062] 2089
    }
    _30739 = NOVALUE;

    /** 	   			StartSourceLine( FALSE, , COVERAGE_OVERRIDE )*/
    _43StartSourceLine(_9FALSE_426, 0, 3);

    /** 	   			Assignment({VARIABLE,sym})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -100;
    ((int *)_2)[2] = _sym_59939;
    _30741 = MAKE_SEQ(_1);
    _41Assignment(_30741);
    _30741 = NOVALUE;
L2F: 
L1B: 

    /** 		tok = next_token()*/
    _0 = _tok_59935;
    _tok_59935 = _41next_token();
    DeRef(_0);

    /** 		if tok[T_ID] != COMMA then*/
    _2 = (int)SEQ_PTR(_tok_59935);
    _30743 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _30743, -30)){
        _30743 = NOVALUE;
        goto L30; // [2105] 2114
    }
    _30743 = NOVALUE;

    /** 			exit*/
    goto L10; // [2111] 2131
L30: 

    /** 		prevtok = tok*/
    Ref(_tok_59935);
    DeRef(_prevtok_59937);
    _prevtok_59937 = _tok_59935;

    /** 		prevsym = sym*/
    _prevsym_59941 = _sym_59939;

    /** 	end while*/
    goto LF; // [2128] 509
L10: 

    /** 	putback(tok)*/
    Ref(_tok_59935);
    _41putback(_tok_59935);

    /** 	return new_symbols*/
    DeRef(_tok_59935);
    DeRef(_prevtok_59937);
    DeRef(_delta_59948);
    DeRef(_30534);
    _30534 = NOVALUE;
    _30561 = NOVALUE;
    _30619 = NOVALUE;
    DeRef(_30606);
    _30606 = NOVALUE;
    _30623 = NOVALUE;
    DeRef(_30631);
    _30631 = NOVALUE;
    DeRef(_30640);
    _30640 = NOVALUE;
    DeRef(_30649);
    _30649 = NOVALUE;
    _30655 = NOVALUE;
    _30662 = NOVALUE;
    DeRef(_30674);
    _30674 = NOVALUE;
    _30687 = NOVALUE;
    DeRef(_30701);
    _30701 = NOVALUE;
    DeRef(_30708);
    _30708 = NOVALUE;
    _30716 = NOVALUE;
    DeRef(_30725);
    _30725 = NOVALUE;
    return _new_symbols_59933;
    ;
}


void _41Private_declaration(int _type_sym_60467)
{
    int _tok_60469 = NOVALUE;
    int _sym_60471 = NOVALUE;
    int _32349 = NOVALUE;
    int _32348 = NOVALUE;
    int _32347 = NOVALUE;
    int _30769 = NOVALUE;
    int _30767 = NOVALUE;
    int _30765 = NOVALUE;
    int _30763 = NOVALUE;
    int _30761 = NOVALUE;
    int _30759 = NOVALUE;
    int _30757 = NOVALUE;
    int _30754 = NOVALUE;
    int _30752 = NOVALUE;
    int _30751 = NOVALUE;
    int _30748 = NOVALUE;
    int _30746 = NOVALUE;
    int _30745 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_type_sym_60467)) {
        _1 = (long)(DBL_PTR(_type_sym_60467)->dbl);
        if (UNIQUE(DBL_PTR(_type_sym_60467)) && (DBL_PTR(_type_sym_60467)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_type_sym_60467);
        _type_sym_60467 = _1;
    }

    /** 	if SymTab[type_sym][S_SCOPE] = SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30745 = (int)*(((s1_ptr)_2)->base + _type_sym_60467);
    _2 = (int)SEQ_PTR(_30745);
    _30746 = (int)*(((s1_ptr)_2)->base + 4);
    _30745 = NOVALUE;
    if (binary_op_a(NOTEQ, _30746, 9)){
        _30746 = NOVALUE;
        goto L1; // [19] 47
    }
    _30746 = NOVALUE;

    /** 		Hide( type_sym )*/
    _55Hide(_type_sym_60467);

    /** 		type_sym = -new_forward_reference( TYPE, type_sym )*/
    _32349 = 504;
    _30748 = _40new_forward_reference(504, _type_sym_60467, 504);
    _32349 = NOVALUE;
    if (IS_ATOM_INT(_30748)) {
        if ((unsigned long)_30748 == 0xC0000000)
        _type_sym_60467 = (int)NewDouble((double)-0xC0000000);
        else
        _type_sym_60467 = - _30748;
    }
    else {
        _type_sym_60467 = unary_op(UMINUS, _30748);
    }
    DeRef(_30748);
    _30748 = NOVALUE;
    if (!IS_ATOM_INT(_type_sym_60467)) {
        _1 = (long)(DBL_PTR(_type_sym_60467)->dbl);
        if (UNIQUE(DBL_PTR(_type_sym_60467)) && (DBL_PTR(_type_sym_60467)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_type_sym_60467);
        _type_sym_60467 = _1;
    }
L1: 

    /** 	while TRUE do*/
L2: 
    if (_9TRUE_428 == 0)
    {
        goto L3; // [54] 257
    }
    else{
    }

    /** 		tok = next_token()*/
    _0 = _tok_60469;
    _tok_60469 = _41next_token();
    DeRef(_0);

    /** 		if not find(tok[T_ID], ID_TOKS) then*/
    _2 = (int)SEQ_PTR(_tok_60469);
    _30751 = (int)*(((s1_ptr)_2)->base + 1);
    _30752 = find_from(_30751, _39ID_TOKS_16553, 1);
    _30751 = NOVALUE;
    if (_30752 != 0)
    goto L4; // [77] 88
    _30752 = NOVALUE;

    /** 			CompileErr(24)*/
    RefDS(_22663);
    _46CompileErr(24, _22663, 0);
L4: 

    /** 		sym = SetPrivateScope(tok[T_SYM], type_sym, param_num)*/
    _2 = (int)SEQ_PTR(_tok_60469);
    _30754 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_30754);
    _sym_60471 = _41SetPrivateScope(_30754, _type_sym_60467, _41param_num_55131);
    _30754 = NOVALUE;
    if (!IS_ATOM_INT(_sym_60471)) {
        _1 = (long)(DBL_PTR(_sym_60471)->dbl);
        if (UNIQUE(DBL_PTR(_sym_60471)) && (DBL_PTR(_sym_60471)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_60471);
        _sym_60471 = _1;
    }

    /** 		param_num += 1*/
    _41param_num_55131 = _41param_num_55131 + 1;

    /** 		if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L5; // [118] 141
    }
    else{
    }

    /** 			SymTab[sym][S_GTYPE] = CompileType(type_sym)*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_60471 + ((s1_ptr)_2)->base);
    _30759 = _41CompileType(_type_sym_60467);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = _30759;
    if( _1 != _30759 ){
        DeRef(_1);
    }
    _30759 = NOVALUE;
    _30757 = NOVALUE;
L5: 

    /**    		tok = next_token()*/
    _0 = _tok_60469;
    _tok_60469 = _41next_token();
    DeRef(_0);

    /**    		if tok[T_ID] = EQUALS then -- assign on declare*/
    _2 = (int)SEQ_PTR(_tok_60469);
    _30761 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30761, 3)){
        _30761 = NOVALUE;
        goto L6; // [156] 233
    }
    _30761 = NOVALUE;

    /** 		    putback(tok)*/
    Ref(_tok_60469);
    _41putback(_tok_60469);

    /** 		    StartSourceLine( TRUE )*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 		    Assignment({VARIABLE,sym})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -100;
    ((int *)_2)[2] = _sym_60471;
    _30763 = MAKE_SEQ(_1);
    _41Assignment(_30763);
    _30763 = NOVALUE;

    /** 			tok = next_token()*/
    _0 = _tok_60469;
    _tok_60469 = _41next_token();
    DeRef(_0);

    /** 			if tok[T_ID]=IGNORED then*/
    _2 = (int)SEQ_PTR(_tok_60469);
    _30765 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30765, 509)){
        _30765 = NOVALUE;
        goto L7; // [202] 232
    }
    _30765 = NOVALUE;

    /** 				tok = keyfind(tok[T_SYM],-1)*/
    _2 = (int)SEQ_PTR(_tok_60469);
    _30767 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_30767);
    DeRef(_32347);
    _32347 = _30767;
    _32348 = _55hashfn(_32347);
    _32347 = NOVALUE;
    Ref(_30767);
    _0 = _tok_60469;
    _tok_60469 = _55keyfind(_30767, -1, _38current_file_no_16946, 0, _32348);
    DeRef(_0);
    _30767 = NOVALUE;
    _32348 = NOVALUE;
L7: 
L6: 

    /** 		if tok[T_ID] != COMMA then*/
    _2 = (int)SEQ_PTR(_tok_60469);
    _30769 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _30769, -30)){
        _30769 = NOVALUE;
        goto L2; // [243] 52
    }
    _30769 = NOVALUE;

    /** 			exit*/
    goto L3; // [249] 257

    /** 	end while*/
    goto L2; // [254] 52
L3: 

    /** 	putback(tok)*/
    Ref(_tok_60469);
    _41putback(_tok_60469);

    /** end procedure*/
    DeRef(_tok_60469);
    return;
    ;
}


void _41Procedure_call(int _tok_60533)
{
    int _n_60534 = NOVALUE;
    int _scope_60535 = NOVALUE;
    int _opcode_60536 = NOVALUE;
    int _temp_tok_60538 = NOVALUE;
    int _s_60540 = NOVALUE;
    int _sub_60541 = NOVALUE;
    int _30805 = NOVALUE;
    int _30800 = NOVALUE;
    int _30799 = NOVALUE;
    int _30798 = NOVALUE;
    int _30797 = NOVALUE;
    int _30796 = NOVALUE;
    int _30795 = NOVALUE;
    int _30794 = NOVALUE;
    int _30793 = NOVALUE;
    int _30792 = NOVALUE;
    int _30791 = NOVALUE;
    int _30790 = NOVALUE;
    int _30788 = NOVALUE;
    int _30787 = NOVALUE;
    int _30786 = NOVALUE;
    int _30785 = NOVALUE;
    int _30784 = NOVALUE;
    int _30783 = NOVALUE;
    int _30782 = NOVALUE;
    int _30780 = NOVALUE;
    int _30779 = NOVALUE;
    int _30778 = NOVALUE;
    int _30776 = NOVALUE;
    int _30774 = NOVALUE;
    int _30772 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	tok_match(LEFT_ROUND)*/
    _41tok_match(-26, 0);

    /** 	s = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_60533);
    _s_60540 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_60540)){
        _s_60540 = (long)DBL_PTR(_s_60540)->dbl;
    }

    /** 	sub=s*/
    _sub_60541 = _s_60540;

    /** 	n = SymTab[s][S_NUM_ARGS]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30772 = (int)*(((s1_ptr)_2)->base + _s_60540);
    _2 = (int)SEQ_PTR(_30772);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _n_60534 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _n_60534 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    if (!IS_ATOM_INT(_n_60534)){
        _n_60534 = (long)DBL_PTR(_n_60534)->dbl;
    }
    _30772 = NOVALUE;

    /** 	scope = SymTab[s][S_SCOPE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30774 = (int)*(((s1_ptr)_2)->base + _s_60540);
    _2 = (int)SEQ_PTR(_30774);
    _scope_60535 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_scope_60535)){
        _scope_60535 = (long)DBL_PTR(_scope_60535)->dbl;
    }
    _30774 = NOVALUE;

    /** 	opcode = SymTab[s][S_OPCODE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30776 = (int)*(((s1_ptr)_2)->base + _s_60540);
    _2 = (int)SEQ_PTR(_30776);
    _opcode_60536 = (int)*(((s1_ptr)_2)->base + 21);
    if (!IS_ATOM_INT(_opcode_60536)){
        _opcode_60536 = (long)DBL_PTR(_opcode_60536)->dbl;
    }
    _30776 = NOVALUE;

    /** 	if SymTab[s][S_EFFECT] then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30778 = (int)*(((s1_ptr)_2)->base + _s_60540);
    _2 = (int)SEQ_PTR(_30778);
    _30779 = (int)*(((s1_ptr)_2)->base + 23);
    _30778 = NOVALUE;
    if (_30779 == 0) {
        _30779 = NOVALUE;
        goto L1; // [88] 139
    }
    else {
        if (!IS_ATOM_INT(_30779) && DBL_PTR(_30779)->dbl == 0.0){
            _30779 = NOVALUE;
            goto L1; // [88] 139
        }
        _30779 = NOVALUE;
    }
    _30779 = NOVALUE;

    /** 		SymTab[CurrentSub][S_EFFECT] = or_bits(SymTab[CurrentSub][S_EFFECT],*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38CurrentSub_16954 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30782 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_30782);
    _30783 = (int)*(((s1_ptr)_2)->base + 23);
    _30782 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30784 = (int)*(((s1_ptr)_2)->base + _s_60540);
    _2 = (int)SEQ_PTR(_30784);
    _30785 = (int)*(((s1_ptr)_2)->base + 23);
    _30784 = NOVALUE;
    if (IS_ATOM_INT(_30783) && IS_ATOM_INT(_30785)) {
        {unsigned long tu;
             tu = (unsigned long)_30783 | (unsigned long)_30785;
             _30786 = MAKE_UINT(tu);
        }
    }
    else {
        _30786 = binary_op(OR_BITS, _30783, _30785);
    }
    _30783 = NOVALUE;
    _30785 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 23);
    _1 = *(int *)_2;
    *(int *)_2 = _30786;
    if( _1 != _30786 ){
        DeRef(_1);
    }
    _30786 = NOVALUE;
    _30780 = NOVALUE;
L1: 

    /** 	ParseArgs(s)*/
    _41ParseArgs(_s_60540);

    /** 	for i=1 to n+1 do*/
    _30787 = _n_60534 + 1;
    if (_30787 > MAXINT){
        _30787 = NewDouble((double)_30787);
    }
    {
        int _i_60578;
        _i_60578 = 1;
L2: 
        if (binary_op_a(GREATER, _i_60578, _30787)){
            goto L3; // [150] 180
        }

        /** 		s = SymTab[s][S_NEXT]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _30788 = (int)*(((s1_ptr)_2)->base + _s_60540);
        _2 = (int)SEQ_PTR(_30788);
        _s_60540 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_s_60540)){
            _s_60540 = (long)DBL_PTR(_s_60540)->dbl;
        }
        _30788 = NOVALUE;

        /** 	end for*/
        _0 = _i_60578;
        if (IS_ATOM_INT(_i_60578)) {
            _i_60578 = _i_60578 + 1;
            if ((long)((unsigned long)_i_60578 +(unsigned long) HIGH_BITS) >= 0){
                _i_60578 = NewDouble((double)_i_60578);
            }
        }
        else {
            _i_60578 = binary_op_a(PLUS, _i_60578, 1);
        }
        DeRef(_0);
        goto L2; // [175] 157
L3: 
        ;
        DeRef(_i_60578);
    }

    /** 	while s and SymTab[s][S_SCOPE]=SC_PRIVATE do*/
L4: 
    if (_s_60540 == 0) {
        goto L5; // [185] 281
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30791 = (int)*(((s1_ptr)_2)->base + _s_60540);
    _2 = (int)SEQ_PTR(_30791);
    _30792 = (int)*(((s1_ptr)_2)->base + 4);
    _30791 = NOVALUE;
    if (IS_ATOM_INT(_30792)) {
        _30793 = (_30792 == 3);
    }
    else {
        _30793 = binary_op(EQUALS, _30792, 3);
    }
    _30792 = NOVALUE;
    if (_30793 <= 0) {
        if (_30793 == 0) {
            DeRef(_30793);
            _30793 = NOVALUE;
            goto L5; // [208] 281
        }
        else {
            if (!IS_ATOM_INT(_30793) && DBL_PTR(_30793)->dbl == 0.0){
                DeRef(_30793);
                _30793 = NOVALUE;
                goto L5; // [208] 281
            }
            DeRef(_30793);
            _30793 = NOVALUE;
        }
    }
    DeRef(_30793);
    _30793 = NOVALUE;

    /** 		if sequence(SymTab[s][S_CODE]) then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30794 = (int)*(((s1_ptr)_2)->base + _s_60540);
    _2 = (int)SEQ_PTR(_30794);
    if (!IS_ATOM_INT(_38S_CODE_16610)){
        _30795 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    }
    else{
        _30795 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
    }
    _30794 = NOVALUE;
    _30796 = IS_SEQUENCE(_30795);
    _30795 = NOVALUE;
    if (_30796 == 0)
    {
        _30796 = NOVALUE;
        goto L6; // [228] 260
    }
    else{
        _30796 = NOVALUE;
    }

    /** 			start_playback(SymTab[s][S_CODE])*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30797 = (int)*(((s1_ptr)_2)->base + _s_60540);
    _2 = (int)SEQ_PTR(_30797);
    if (!IS_ATOM_INT(_38S_CODE_16610)){
        _30798 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    }
    else{
        _30798 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
    }
    _30797 = NOVALUE;
    Ref(_30798);
    _41start_playback(_30798);
    _30798 = NOVALUE;

    /** 			Assignment({VARIABLE,s})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -100;
    ((int *)_2)[2] = _s_60540;
    _30799 = MAKE_SEQ(_1);
    _41Assignment(_30799);
    _30799 = NOVALUE;
L6: 

    /** 		s = SymTab[s][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30800 = (int)*(((s1_ptr)_2)->base + _s_60540);
    _2 = (int)SEQ_PTR(_30800);
    _s_60540 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_60540)){
        _s_60540 = (long)DBL_PTR(_s_60540)->dbl;
    }
    _30800 = NOVALUE;

    /** 	end while*/
    goto L4; // [278] 185
L5: 

    /** 	s = sub*/
    _s_60540 = _sub_60541;

    /** 	if scope = SC_PREDEF then*/
    if (_scope_60535 != 7)
    goto L7; // [292] 335

    /** 		emit_op(opcode)*/
    _43emit_op(_opcode_60536);

    /** 		if opcode = ABORT then*/
    if (_opcode_60536 != 126)
    goto L8; // [305] 370

    /** 			temp_tok = next_token()*/
    _0 = _temp_tok_60538;
    _temp_tok_60538 = _41next_token();
    DeRef(_0);

    /** 			putback(temp_tok)*/
    Ref(_temp_tok_60538);
    _41putback(_temp_tok_60538);

    /** 			NotReached(temp_tok[T_ID], "abort()")*/
    _2 = (int)SEQ_PTR(_temp_tok_60538);
    _30805 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_30805);
    RefDS(_28689);
    _41NotReached(_30805, _28689);
    _30805 = NOVALUE;
    goto L8; // [332] 370
L7: 

    /** 		op_info1 = s*/
    _43op_info1_51218 = _s_60540;

    /** 		emit_or_inline()*/
    _69emit_or_inline();

    /** 		if not TRANSLATE then*/
    if (_38TRANSLATE_16564 != 0)
    goto L9; // [350] 369

    /** 			if OpTrace then*/
    if (_38OpTrace_17015 == 0)
    {
        goto LA; // [357] 368
    }
    else{
    }

    /** 				emit_op(UPDATE_GLOBALS)*/
    _43emit_op(89);
LA: 
L9: 
L8: 

    /** end procedure*/
    DeRef(_tok_60533);
    DeRef(_temp_tok_60538);
    DeRef(_30787);
    _30787 = NOVALUE;
    return;
    ;
}


void _41Print_statement()
{
    int _30812 = NOVALUE;
    int _30811 = NOVALUE;
    int _30810 = NOVALUE;
    int _30808 = NOVALUE;
    int _30807 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	emit_opnd(NewIntSym(1)) -- stdout*/
    _30807 = _55NewIntSym(1);
    _43emit_opnd(_30807);
    _30807 = NOVALUE;

    /** 	Expr()*/
    _41Expr();

    /** 	emit_op(QPRINT)*/
    _43emit_op(36);

    /** 	SymTab[CurrentSub][S_EFFECT] = or_bits(SymTab[CurrentSub][S_EFFECT],*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38CurrentSub_16954 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30810 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_30810);
    _30811 = (int)*(((s1_ptr)_2)->base + 23);
    _30810 = NOVALUE;
    if (IS_ATOM_INT(_30811)) {
        {unsigned long tu;
             tu = (unsigned long)_30811 | (unsigned long)536870912;
             _30812 = MAKE_UINT(tu);
        }
    }
    else {
        _30812 = binary_op(OR_BITS, _30811, 536870912);
    }
    _30811 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 23);
    _1 = *(int *)_2;
    *(int *)_2 = _30812;
    if( _1 != _30812 ){
        DeRef(_1);
    }
    _30812 = NOVALUE;
    _30808 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


void _41Entry_statement()
{
    int _addr_60649 = NOVALUE;
    int _30838 = NOVALUE;
    int _30837 = NOVALUE;
    int _30836 = NOVALUE;
    int _30835 = NOVALUE;
    int _30834 = NOVALUE;
    int _30833 = NOVALUE;
    int _30832 = NOVALUE;
    int _30831 = NOVALUE;
    int _30827 = NOVALUE;
    int _30824 = NOVALUE;
    int _30823 = NOVALUE;
    int _30821 = NOVALUE;
    int _30820 = NOVALUE;
    int _30818 = NOVALUE;
    int _30817 = NOVALUE;
    int _30816 = NOVALUE;
    int _30814 = NOVALUE;
    int _30813 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length(loop_stack) or block_index=0 then*/
    if (IS_SEQUENCE(_41loop_stack_55156)){
            _30813 = SEQ_PTR(_41loop_stack_55156)->length;
    }
    else {
        _30813 = 1;
    }
    _30814 = (_30813 == 0);
    _30813 = NOVALUE;
    if (_30814 != 0) {
        goto L1; // [11] 26
    }
    _30816 = (_41block_index_55153 == 0);
    if (_30816 == 0)
    {
        DeRef(_30816);
        _30816 = NOVALUE;
        goto L2; // [22] 34
    }
    else{
        DeRef(_30816);
        _30816 = NOVALUE;
    }
L1: 

    /** 		CompileErr(144)*/
    RefDS(_22663);
    _46CompileErr(144, _22663, 0);
L2: 

    /** 	if block_list[block_index]=IF or block_list[block_index]=SWITCH then*/
    _2 = (int)SEQ_PTR(_41block_list_55152);
    _30817 = (int)*(((s1_ptr)_2)->base + _41block_index_55153);
    _30818 = (_30817 == 20);
    _30817 = NOVALUE;
    if (_30818 != 0) {
        goto L3; // [50] 73
    }
    _2 = (int)SEQ_PTR(_41block_list_55152);
    _30820 = (int)*(((s1_ptr)_2)->base + _41block_index_55153);
    _30821 = (_30820 == 185);
    _30820 = NOVALUE;
    if (_30821 == 0)
    {
        DeRef(_30821);
        _30821 = NOVALUE;
        goto L4; // [69] 83
    }
    else{
        DeRef(_30821);
        _30821 = NOVALUE;
    }
L3: 

    /** 		CompileErr(143)*/
    RefDS(_22663);
    _46CompileErr(143, _22663, 0);
    goto L5; // [80] 109
L4: 

    /** 	elsif loop_stack[$] = FOR then  -- not allowed in an innermost for loop*/
    if (IS_SEQUENCE(_41loop_stack_55156)){
            _30823 = SEQ_PTR(_41loop_stack_55156)->length;
    }
    else {
        _30823 = 1;
    }
    _2 = (int)SEQ_PTR(_41loop_stack_55156);
    _30824 = (int)*(((s1_ptr)_2)->base + _30823);
    if (_30824 != 21)
    goto L6; // [96] 108

    /** 		CompileErr(142)*/
    RefDS(_22663);
    _46CompileErr(142, _22663, 0);
L6: 
L5: 

    /** 	addr = entry_addr[$]*/
    if (IS_SEQUENCE(_41entry_addr_55146)){
            _30827 = SEQ_PTR(_41entry_addr_55146)->length;
    }
    else {
        _30827 = 1;
    }
    _2 = (int)SEQ_PTR(_41entry_addr_55146);
    _addr_60649 = (int)*(((s1_ptr)_2)->base + _30827);

    /** 	if addr=0  then*/
    if (_addr_60649 != 0)
    goto L7; // [122] 136

    /** 		CompileErr(141)*/
    RefDS(_22663);
    _46CompileErr(141, _22663, 0);
    goto L8; // [133] 151
L7: 

    /** 	elsif addr<0 then*/
    if (_addr_60649 >= 0)
    goto L9; // [138] 150

    /** 		CompileErr(73)*/
    RefDS(_22663);
    _46CompileErr(73, _22663, 0);
L9: 
L8: 

    /** 	backpatch(addr,ELSE)*/
    _43backpatch(_addr_60649, 23);

    /** 	backpatch(addr+1,length(Code)+1+(TRANSLATE>0))*/
    _30831 = _addr_60649 + 1;
    if (_30831 > MAXINT){
        _30831 = NewDouble((double)_30831);
    }
    if (IS_SEQUENCE(_38Code_17038)){
            _30832 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _30832 = 1;
    }
    _30833 = _30832 + 1;
    _30832 = NOVALUE;
    _30834 = (_38TRANSLATE_16564 > 0);
    _30835 = _30833 + _30834;
    _30833 = NOVALUE;
    _30834 = NOVALUE;
    _43backpatch(_30831, _30835);
    _30831 = NOVALUE;
    _30835 = NOVALUE;

    /** 	entry_addr[$] = 0*/
    if (IS_SEQUENCE(_41entry_addr_55146)){
            _30836 = SEQ_PTR(_41entry_addr_55146)->length;
    }
    else {
        _30836 = 1;
    }
    _2 = (int)SEQ_PTR(_41entry_addr_55146);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _41entry_addr_55146 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _30836);
    *(int *)_2 = 0;

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto LA; // [203] 214
    }
    else{
    }

    /** 	    emit_op(NOP1)*/
    _43emit_op(159);
LA: 

    /** 	force_uninitialize( entry_stack[$] )*/
    if (IS_SEQUENCE(_41entry_stack_55149)){
            _30837 = SEQ_PTR(_41entry_stack_55149)->length;
    }
    else {
        _30837 = 1;
    }
    _2 = (int)SEQ_PTR(_41entry_stack_55149);
    _30838 = (int)*(((s1_ptr)_2)->base + _30837);
    Ref(_30838);
    _41force_uninitialize(_30838);
    _30838 = NOVALUE;

    /** end procedure*/
    DeRef(_30814);
    _30814 = NOVALUE;
    _30824 = NOVALUE;
    DeRef(_30818);
    _30818 = NOVALUE;
    return;
    ;
}


void _41force_uninitialize(int _uninitialized_60701)
{
    int _30841 = NOVALUE;
    int _30840 = NOVALUE;
    int _30839 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	for i = 1 to length( uninitialized ) do*/
    if (IS_SEQUENCE(_uninitialized_60701)){
            _30839 = SEQ_PTR(_uninitialized_60701)->length;
    }
    else {
        _30839 = 1;
    }
    {
        int _i_60703;
        _i_60703 = 1;
L1: 
        if (_i_60703 > _30839){
            goto L2; // [8] 41
        }

        /** 		SymTab[uninitialized[i]][S_INITLEVEL] = -1*/
        _2 = (int)SEQ_PTR(_uninitialized_60701);
        _30840 = (int)*(((s1_ptr)_2)->base + _i_60703);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_30840))
        _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_30840)->dbl));
        else
        _3 = (int)(_30840 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 14);
        _1 = *(int *)_2;
        *(int *)_2 = -1;
        DeRef(_1);
        _30841 = NOVALUE;

        /** 	end for*/
        _i_60703 = _i_60703 + 1;
        goto L1; // [36] 15
L2: 
        ;
    }

    /** end procedure*/
    DeRefDS(_uninitialized_60701);
    _30840 = NOVALUE;
    return;
    ;
}


void _41Statement_list()
{
    int _tok_60713 = NOVALUE;
    int _id_60714 = NOVALUE;
    int _forward_60737 = NOVALUE;
    int _test_60886 = NOVALUE;
    int _30913 = NOVALUE;
    int _30912 = NOVALUE;
    int _30909 = NOVALUE;
    int _30907 = NOVALUE;
    int _30906 = NOVALUE;
    int _30903 = NOVALUE;
    int _30901 = NOVALUE;
    int _30900 = NOVALUE;
    int _30899 = NOVALUE;
    int _30896 = NOVALUE;
    int _30894 = NOVALUE;
    int _30892 = NOVALUE;
    int _30890 = NOVALUE;
    int _30872 = NOVALUE;
    int _30871 = NOVALUE;
    int _30869 = NOVALUE;
    int _30867 = NOVALUE;
    int _30866 = NOVALUE;
    int _30864 = NOVALUE;
    int _30862 = NOVALUE;
    int _30861 = NOVALUE;
    int _30860 = NOVALUE;
    int _30859 = NOVALUE;
    int _30854 = NOVALUE;
    int _30851 = NOVALUE;
    int _30850 = NOVALUE;
    int _30849 = NOVALUE;
    int _30848 = NOVALUE;
    int _30846 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer id*/

    /** 	stmt_nest += 1*/
    _41stmt_nest_55154 = _41stmt_nest_55154 + 1;

    /** 	while TRUE do*/
L1: 
    if (_9TRUE_428 == 0)
    {
        goto L2; // [18] 1131
    }
    else{
    }

    /** 		tok = next_token()*/
    _0 = _tok_60713;
    _tok_60713 = _41next_token();
    DeRef(_0);

    /** 		id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_60713);
    _id_60714 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_id_60714)){
        _id_60714 = (long)DBL_PTR(_id_60714)->dbl;
    }

    /** 		if id = VARIABLE or id = QUALIFIED_VARIABLE then*/
    _30846 = (_id_60714 == -100);
    if (_30846 != 0) {
        goto L3; // [44] 59
    }
    _30848 = (_id_60714 == 512);
    if (_30848 == 0)
    {
        DeRef(_30848);
        _30848 = NOVALUE;
        goto L4; // [55] 233
    }
    else{
        DeRef(_30848);
        _30848 = NOVALUE;
    }
L3: 

    /** 			if SymTab[tok[T_SYM]][S_SCOPE] = SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_tok_60713);
    _30849 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_30849)){
        _30850 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_30849)->dbl));
    }
    else{
        _30850 = (int)*(((s1_ptr)_2)->base + _30849);
    }
    _2 = (int)SEQ_PTR(_30850);
    _30851 = (int)*(((s1_ptr)_2)->base + 4);
    _30850 = NOVALUE;
    if (binary_op_a(NOTEQ, _30851, 9)){
        _30851 = NOVALUE;
        goto L5; // [81] 212
    }
    _30851 = NOVALUE;

    /** 				token forward = next_token()*/
    _0 = _forward_60737;
    _forward_60737 = _41next_token();
    DeRef(_0);

    /** 				switch forward[T_ID] do*/
    _2 = (int)SEQ_PTR(_forward_60737);
    _30854 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_30854) ){
        goto L6; // [98] 206
    }
    if(!IS_ATOM_INT(_30854)){
        if( (DBL_PTR(_30854)->dbl != (double) ((int) DBL_PTR(_30854)->dbl) ) ){
            goto L6; // [98] 206
        }
        _0 = (int) DBL_PTR(_30854)->dbl;
    }
    else {
        _0 = _30854;
    };
    _30854 = NOVALUE;
    switch ( _0 ){ 

        /** 					case LEFT_ROUND then*/
        case -26:

        /** 						StartSourceLine( TRUE )*/
        _43StartSourceLine(_9TRUE_428, 0, 2);

        /** 						Forward_call( tok )*/
        Ref(_tok_60713);
        _41Forward_call(_tok_60713, 195);

        /** 						flush_temps()*/
        RefDS(_22663);
        _43flush_temps(_22663);

        /** 						continue*/
        DeRef(_forward_60737);
        _forward_60737 = NOVALUE;
        goto L1; // [135] 16
        goto L6; // [137] 206

        /** 					case VARIABLE then*/
        case -100:

        /** 						putback( forward )*/
        Ref(_forward_60737);
        _41putback(_forward_60737);

        /** 						if param_num != -1 then*/
        if (_41param_num_55131 == -1)
        goto L7; // [152] 178

        /** 							param_num += 1*/
        _41param_num_55131 = _41param_num_55131 + 1;

        /** 							Private_declaration( tok[T_SYM] )*/
        _2 = (int)SEQ_PTR(_tok_60713);
        _30859 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_30859);
        _41Private_declaration(_30859);
        _30859 = NOVALUE;
        goto L8; // [175] 194
L7: 

        /** 							Global_declaration( tok[T_SYM], SC_LOCAL )*/
        _2 = (int)SEQ_PTR(_tok_60713);
        _30860 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_30860);
        _30861 = _41Global_declaration(_30860, 5);
        _30860 = NOVALUE;
L8: 

        /** 						flush_temps()*/
        RefDS(_22663);
        _43flush_temps(_22663);

        /** 						continue*/
        DeRef(_forward_60737);
        _forward_60737 = NOVALUE;
        goto L1; // [203] 16
    ;}L6: 

    /** 				putback( forward )*/
    Ref(_forward_60737);
    _41putback(_forward_60737);
L5: 
    DeRef(_forward_60737);
    _forward_60737 = NOVALUE;

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			Assignment(tok)*/
    Ref(_tok_60713);
    _41Assignment(_tok_60713);
    goto L9; // [230] 1121
L4: 

    /** 		elsif id = PROC or id = QUALIFIED_PROC then*/
    _30862 = (_id_60714 == 27);
    if (_30862 != 0) {
        goto LA; // [241] 256
    }
    _30864 = (_id_60714 == 521);
    if (_30864 == 0)
    {
        DeRef(_30864);
        _30864 = NOVALUE;
        goto LB; // [252] 295
    }
    else{
        DeRef(_30864);
        _30864 = NOVALUE;
    }
LA: 

    /** 			if id = PROC then*/
    if (_id_60714 != 27)
    goto LC; // [260] 276

    /** 				UndefinedVar( tok[T_SYM] )*/
    _2 = (int)SEQ_PTR(_tok_60713);
    _30866 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_30866);
    _41UndefinedVar(_30866);
    _30866 = NOVALUE;
LC: 

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			Procedure_call(tok)*/
    Ref(_tok_60713);
    _41Procedure_call(_tok_60713);
    goto L9; // [292] 1121
LB: 

    /** 		elsif id = FUNC or id = QUALIFIED_FUNC then*/
    _30867 = (_id_60714 == 501);
    if (_30867 != 0) {
        goto LD; // [303] 318
    }
    _30869 = (_id_60714 == 520);
    if (_30869 == 0)
    {
        DeRef(_30869);
        _30869 = NOVALUE;
        goto LE; // [314] 370
    }
    else{
        DeRef(_30869);
        _30869 = NOVALUE;
    }
LD: 

    /** 			if id = FUNC then*/
    if (_id_60714 != 501)
    goto LF; // [322] 338

    /** 				UndefinedVar( tok[T_SYM] )*/
    _2 = (int)SEQ_PTR(_tok_60713);
    _30871 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_30871);
    _41UndefinedVar(_30871);
    _30871 = NOVALUE;
LF: 

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			Procedure_call(tok)*/
    Ref(_tok_60713);
    _41Procedure_call(_tok_60713);

    /** 			clear_op()*/
    _43clear_op();

    /** 			if Pop() then end if*/
    _30872 = _43Pop();
    if (_30872 == 0) {
        DeRef(_30872);
        _30872 = NOVALUE;
        goto L9; // [363] 1121
    }
    else {
        if (!IS_ATOM_INT(_30872) && DBL_PTR(_30872)->dbl == 0.0){
            DeRef(_30872);
            _30872 = NOVALUE;
            goto L9; // [363] 1121
        }
        DeRef(_30872);
        _30872 = NOVALUE;
    }
    DeRef(_30872);
    _30872 = NOVALUE;
    goto L9; // [367] 1121
LE: 

    /** 		elsif id = IF then*/
    if (_id_60714 != 20)
    goto L10; // [374] 396

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			If_statement()*/
    _41If_statement();
    goto L9; // [393] 1121
L10: 

    /** 		elsif id = FOR then*/
    if (_id_60714 != 21)
    goto L11; // [400] 422

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			For_statement()*/
    _41For_statement();
    goto L9; // [419] 1121
L11: 

    /** 		elsif id = RETURN then*/
    if (_id_60714 != 413)
    goto L12; // [426] 448

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			Return_statement()*/
    _41Return_statement();
    goto L9; // [445] 1121
L12: 

    /** 		elsif id = LABEL then*/
    if (_id_60714 != 419)
    goto L13; // [452] 474

    /** 			StartSourceLine(TRUE, , COVERAGE_SUPPRESS )*/
    _43StartSourceLine(_9TRUE_428, 0, 1);

    /** 			GLabel_statement()*/
    _41GLabel_statement();
    goto L9; // [471] 1121
L13: 

    /** 		elsif id = GOTO then*/
    if (_id_60714 != 188)
    goto L14; // [478] 500

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			Goto_statement()*/
    _41Goto_statement();
    goto L9; // [497] 1121
L14: 

    /** 		elsif id = EXIT then*/
    if (_id_60714 != 61)
    goto L15; // [504] 526

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			Exit_statement()*/
    _41Exit_statement();
    goto L9; // [523] 1121
L15: 

    /** 		elsif id = BREAK then*/
    if (_id_60714 != 425)
    goto L16; // [530] 552

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			Break_statement()*/
    _41Break_statement();
    goto L9; // [549] 1121
L16: 

    /** 		elsif id = WHILE then*/
    if (_id_60714 != 47)
    goto L17; // [556] 578

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			While_statement()*/
    _41While_statement();
    goto L9; // [575] 1121
L17: 

    /** 		elsif id = LOOP then*/
    if (_id_60714 != 422)
    goto L18; // [582] 604

    /** 		    StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 	        Loop_statement()*/
    _41Loop_statement();
    goto L9; // [601] 1121
L18: 

    /** 		elsif id = ENTRY then*/
    if (_id_60714 != 424)
    goto L19; // [608] 630

    /** 		    StartSourceLine(TRUE, , COVERAGE_SUPPRESS )*/
    _43StartSourceLine(_9TRUE_428, 0, 1);

    /** 		    Entry_statement()*/
    _41Entry_statement();
    goto L9; // [627] 1121
L19: 

    /** 		elsif id = QUESTION_MARK then*/
    if (_id_60714 != -31)
    goto L1A; // [634] 656

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			Print_statement()*/
    _41Print_statement();
    goto L9; // [653] 1121
L1A: 

    /** 		elsif id = CONTINUE then*/
    if (_id_60714 != 426)
    goto L1B; // [660] 682

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			Continue_statement()*/
    _41Continue_statement();
    goto L9; // [679] 1121
L1B: 

    /** 		elsif id = RETRY then*/
    if (_id_60714 != 184)
    goto L1C; // [686] 708

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			Retry_statement()*/
    _41Retry_statement();
    goto L9; // [705] 1121
L1C: 

    /** 		elsif id = IFDEF then*/
    if (_id_60714 != 407)
    goto L1D; // [712] 734

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			Ifdef_statement()*/
    _41Ifdef_statement();
    goto L9; // [731] 1121
L1D: 

    /** 		elsif id = CASE then*/
    if (_id_60714 != 186)
    goto L1E; // [738] 749

    /** 			Case_statement()*/
    _41Case_statement();
    goto L9; // [746] 1121
L1E: 

    /** 		elsif id = SWITCH then*/
    if (_id_60714 != 185)
    goto L1F; // [753] 775

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			Switch_statement()*/
    _41Switch_statement();
    goto L9; // [772] 1121
L1F: 

    /** 		elsif id = FALLTHRU then*/
    if (_id_60714 != 431)
    goto L20; // [779] 790

    /** 			Fallthru_statement()*/
    _41Fallthru_statement();
    goto L9; // [787] 1121
L20: 

    /** 		elsif id = TYPE or id = QUALIFIED_TYPE then*/
    _30890 = (_id_60714 == 504);
    if (_30890 != 0) {
        goto L21; // [798] 813
    }
    _30892 = (_id_60714 == 522);
    if (_30892 == 0)
    {
        DeRef(_30892);
        _30892 = NOVALUE;
        goto L22; // [809] 942
    }
    else{
        DeRef(_30892);
        _30892 = NOVALUE;
    }
L21: 

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			token test = next_token()*/
    _0 = _test_60886;
    _test_60886 = _41next_token();
    DeRef(_0);

    /** 			putback( test )*/
    Ref(_test_60886);
    _41putback(_test_60886);

    /** 			if test[T_ID] = LEFT_ROUND then*/
    _2 = (int)SEQ_PTR(_test_60886);
    _30894 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30894, -26)){
        _30894 = NOVALUE;
        goto L23; // [844] 890
    }
    _30894 = NOVALUE;

    /** 				StartSourceLine( TRUE )*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 				Procedure_call(tok)*/
    Ref(_tok_60713);
    _41Procedure_call(_tok_60713);

    /** 				clear_op()*/
    _43clear_op();

    /** 				if Pop() then end if*/
    _30896 = _43Pop();
    if (_30896 == 0) {
        DeRef(_30896);
        _30896 = NOVALUE;
        goto L24; // [873] 877
    }
    else {
        if (!IS_ATOM_INT(_30896) && DBL_PTR(_30896)->dbl == 0.0){
            DeRef(_30896);
            _30896 = NOVALUE;
            goto L24; // [873] 877
        }
        DeRef(_30896);
        _30896 = NOVALUE;
    }
    DeRef(_30896);
    _30896 = NOVALUE;
L24: 

    /** 				ExecCommand()*/
    _41ExecCommand();

    /** 				continue*/
    DeRef(_test_60886);
    _test_60886 = NOVALUE;
    goto L1; // [885] 16
    goto L25; // [887] 937
L23: 

    /** 				if param_num != -1 then*/
    if (_41param_num_55131 == -1)
    goto L26; // [894] 920

    /** 					param_num += 1*/
    _41param_num_55131 = _41param_num_55131 + 1;

    /** 					Private_declaration( tok[T_SYM] )*/
    _2 = (int)SEQ_PTR(_tok_60713);
    _30899 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_30899);
    _41Private_declaration(_30899);
    _30899 = NOVALUE;
    goto L27; // [917] 936
L26: 

    /** 					Global_declaration( tok[T_SYM], SC_LOCAL )*/
    _2 = (int)SEQ_PTR(_tok_60713);
    _30900 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_30900);
    _30901 = _41Global_declaration(_30900, 5);
    _30900 = NOVALUE;
L27: 
L25: 
    DeRef(_test_60886);
    _test_60886 = NOVALUE;
    goto L9; // [939] 1121
L22: 

    /** 			if id = ELSE then*/
    if (_id_60714 != 23)
    goto L28; // [946] 1000

    /** 				if length(if_stack) = 0 then*/
    if (IS_SEQUENCE(_41if_stack_55157)){
            _30903 = SEQ_PTR(_41if_stack_55157)->length;
    }
    else {
        _30903 = 1;
    }
    if (_30903 != 0)
    goto L29; // [957] 1057

    /** 					if live_ifdef > 0 then*/
    if (_41live_ifdef_59303 <= 0)
    goto L2A; // [965] 988

    /** 						CompileErr(134, ifdef_lineno[$])*/
    if (IS_SEQUENCE(_41ifdef_lineno_59304)){
            _30906 = SEQ_PTR(_41ifdef_lineno_59304)->length;
    }
    else {
        _30906 = 1;
    }
    _2 = (int)SEQ_PTR(_41ifdef_lineno_59304);
    _30907 = (int)*(((s1_ptr)_2)->base + _30906);
    _46CompileErr(134, _30907, 0);
    _30907 = NOVALUE;
    goto L29; // [985] 1057
L2A: 

    /** 						CompileErr(118)*/
    RefDS(_22663);
    _46CompileErr(118, _22663, 0);
    goto L29; // [997] 1057
L28: 

    /** 			elsif id = ELSIF then*/
    if (_id_60714 != 414)
    goto L2B; // [1004] 1056

    /** 				if length(if_stack) = 0 then*/
    if (IS_SEQUENCE(_41if_stack_55157)){
            _30909 = SEQ_PTR(_41if_stack_55157)->length;
    }
    else {
        _30909 = 1;
    }
    if (_30909 != 0)
    goto L2C; // [1015] 1055

    /** 					if live_ifdef > 0 then*/
    if (_41live_ifdef_59303 <= 0)
    goto L2D; // [1023] 1046

    /** 						CompileErr(139, ifdef_lineno[$])*/
    if (IS_SEQUENCE(_41ifdef_lineno_59304)){
            _30912 = SEQ_PTR(_41ifdef_lineno_59304)->length;
    }
    else {
        _30912 = 1;
    }
    _2 = (int)SEQ_PTR(_41ifdef_lineno_59304);
    _30913 = (int)*(((s1_ptr)_2)->base + _30912);
    _46CompileErr(139, _30913, 0);
    _30913 = NOVALUE;
    goto L2E; // [1043] 1054
L2D: 

    /** 						CompileErr(119)*/
    RefDS(_22663);
    _46CompileErr(119, _22663, 0);
L2E: 
L2C: 
L2B: 
L29: 

    /** 			putback( tok )*/
    Ref(_tok_60713);
    _41putback(_tok_60713);

    /** 			switch id do*/
    _0 = _id_60714;
    switch ( _0 ){ 

        /** 				case END, ELSEDEF, ELSIFDEF, ELSIF, ELSE, UNTIL then*/
        case 402:
        case 409:
        case 408:
        case 414:
        case 23:
        case 423:

        /** 					stmt_nest -= 1*/
        _41stmt_nest_55154 = _41stmt_nest_55154 - 1;

        /** 					InitDelete()*/
        _41InitDelete();

        /** 					flush_temps()*/
        RefDS(_22663);
        _43flush_temps(_22663);

        /** 					return*/
        DeRef(_tok_60713);
        DeRef(_30846);
        _30846 = NOVALUE;
        _30849 = NOVALUE;
        DeRef(_30862);
        _30862 = NOVALUE;
        DeRef(_30861);
        _30861 = NOVALUE;
        DeRef(_30867);
        _30867 = NOVALUE;
        DeRef(_30890);
        _30890 = NOVALUE;
        DeRef(_30901);
        _30901 = NOVALUE;
        return;
        goto L2F; // [1105] 1120

        /** 				case else*/
        default:

        /** 					tok_match( END )*/
        _41tok_match(402, 0);
    ;}L2F: 
L9: 

    /** 		flush_temps()*/
    RefDS(_22663);
    _43flush_temps(_22663);

    /** 	end while*/
    goto L1; // [1128] 16
L2: 

    /** end procedure*/
    DeRef(_tok_60713);
    DeRef(_30846);
    _30846 = NOVALUE;
    _30849 = NOVALUE;
    DeRef(_30862);
    _30862 = NOVALUE;
    DeRef(_30861);
    _30861 = NOVALUE;
    DeRef(_30867);
    _30867 = NOVALUE;
    DeRef(_30890);
    _30890 = NOVALUE;
    DeRef(_30901);
    _30901 = NOVALUE;
    return;
    ;
}


void _41SubProg(int _prog_type_60956, int _scope_60957)
{
    int _h_60958 = NOVALUE;
    int _pt_60959 = NOVALUE;
    int _p_60961 = NOVALUE;
    int _type_sym_60962 = NOVALUE;
    int _sym_60963 = NOVALUE;
    int _tok_60965 = NOVALUE;
    int _prog_name_60966 = NOVALUE;
    int _first_def_arg_60967 = NOVALUE;
    int _again_60968 = NOVALUE;
    int _type_enum_60969 = NOVALUE;
    int _i1_sym_60970 = NOVALUE;
    int _enum_syms_60971 = NOVALUE;
    int _type_enum_gline_60972 = NOVALUE;
    int _real_gline_60973 = NOVALUE;
    int _tsym_60984 = NOVALUE;
    int _seq_symbol_60995 = NOVALUE;
    int _middle_def_args_61192 = NOVALUE;
    int _last_nda_61193 = NOVALUE;
    int _start_def_61194 = NOVALUE;
    int _last_link_61196 = NOVALUE;
    int _temptok_61223 = NOVALUE;
    int _undef_type_61225 = NOVALUE;
    int _tokcat_61274 = NOVALUE;
    int _last_comparison_61511 = NOVALUE;
    int _32346 = NOVALUE;
    int _32345 = NOVALUE;
    int _32344 = NOVALUE;
    int _32343 = NOVALUE;
    int _32342 = NOVALUE;
    int _32341 = NOVALUE;
    int _32340 = NOVALUE;
    int _31187 = NOVALUE;
    int _31185 = NOVALUE;
    int _31184 = NOVALUE;
    int _31182 = NOVALUE;
    int _31181 = NOVALUE;
    int _31180 = NOVALUE;
    int _31179 = NOVALUE;
    int _31178 = NOVALUE;
    int _31176 = NOVALUE;
    int _31175 = NOVALUE;
    int _31172 = NOVALUE;
    int _31171 = NOVALUE;
    int _31170 = NOVALUE;
    int _31169 = NOVALUE;
    int _31167 = NOVALUE;
    int _31158 = NOVALUE;
    int _31156 = NOVALUE;
    int _31155 = NOVALUE;
    int _31154 = NOVALUE;
    int _31152 = NOVALUE;
    int _31151 = NOVALUE;
    int _31150 = NOVALUE;
    int _31149 = NOVALUE;
    int _31147 = NOVALUE;
    int _31145 = NOVALUE;
    int _31144 = NOVALUE;
    int _31143 = NOVALUE;
    int _31142 = NOVALUE;
    int _31139 = NOVALUE;
    int _31138 = NOVALUE;
    int _31137 = NOVALUE;
    int _31135 = NOVALUE;
    int _31132 = NOVALUE;
    int _31131 = NOVALUE;
    int _31130 = NOVALUE;
    int _31128 = NOVALUE;
    int _31127 = NOVALUE;
    int _31124 = NOVALUE;
    int _31122 = NOVALUE;
    int _31120 = NOVALUE;
    int _31119 = NOVALUE;
    int _31118 = NOVALUE;
    int _31117 = NOVALUE;
    int _31115 = NOVALUE;
    int _31114 = NOVALUE;
    int _31113 = NOVALUE;
    int _31112 = NOVALUE;
    int _31111 = NOVALUE;
    int _31110 = NOVALUE;
    int _31107 = NOVALUE;
    int _31105 = NOVALUE;
    int _31103 = NOVALUE;
    int _31101 = NOVALUE;
    int _31099 = NOVALUE;
    int _31096 = NOVALUE;
    int _31094 = NOVALUE;
    int _31093 = NOVALUE;
    int _31090 = NOVALUE;
    int _31086 = NOVALUE;
    int _31085 = NOVALUE;
    int _31083 = NOVALUE;
    int _31081 = NOVALUE;
    int _31079 = NOVALUE;
    int _31077 = NOVALUE;
    int _31075 = NOVALUE;
    int _31073 = NOVALUE;
    int _31072 = NOVALUE;
    int _31071 = NOVALUE;
    int _31070 = NOVALUE;
    int _31069 = NOVALUE;
    int _31068 = NOVALUE;
    int _31067 = NOVALUE;
    int _31066 = NOVALUE;
    int _31065 = NOVALUE;
    int _31064 = NOVALUE;
    int _31063 = NOVALUE;
    int _31062 = NOVALUE;
    int _31059 = NOVALUE;
    int _31058 = NOVALUE;
    int _31057 = NOVALUE;
    int _31056 = NOVALUE;
    int _31055 = NOVALUE;
    int _31054 = NOVALUE;
    int _31053 = NOVALUE;
    int _31052 = NOVALUE;
    int _31051 = NOVALUE;
    int _31050 = NOVALUE;
    int _31049 = NOVALUE;
    int _31048 = NOVALUE;
    int _31047 = NOVALUE;
    int _31046 = NOVALUE;
    int _31045 = NOVALUE;
    int _31043 = NOVALUE;
    int _31041 = NOVALUE;
    int _31040 = NOVALUE;
    int _31035 = NOVALUE;
    int _31034 = NOVALUE;
    int _31032 = NOVALUE;
    int _31031 = NOVALUE;
    int _31030 = NOVALUE;
    int _31029 = NOVALUE;
    int _31028 = NOVALUE;
    int _31027 = NOVALUE;
    int _31026 = NOVALUE;
    int _31025 = NOVALUE;
    int _31024 = NOVALUE;
    int _31023 = NOVALUE;
    int _31021 = NOVALUE;
    int _31020 = NOVALUE;
    int _31018 = NOVALUE;
    int _31017 = NOVALUE;
    int _31016 = NOVALUE;
    int _31015 = NOVALUE;
    int _31014 = NOVALUE;
    int _31013 = NOVALUE;
    int _31012 = NOVALUE;
    int _31010 = NOVALUE;
    int _31007 = NOVALUE;
    int _31005 = NOVALUE;
    int _31003 = NOVALUE;
    int _31001 = NOVALUE;
    int _30999 = NOVALUE;
    int _30997 = NOVALUE;
    int _30995 = NOVALUE;
    int _30993 = NOVALUE;
    int _30991 = NOVALUE;
    int _30990 = NOVALUE;
    int _30989 = NOVALUE;
    int _30988 = NOVALUE;
    int _30987 = NOVALUE;
    int _30986 = NOVALUE;
    int _30985 = NOVALUE;
    int _30983 = NOVALUE;
    int _30982 = NOVALUE;
    int _30980 = NOVALUE;
    int _30978 = NOVALUE;
    int _30976 = NOVALUE;
    int _30975 = NOVALUE;
    int _30972 = NOVALUE;
    int _30971 = NOVALUE;
    int _30970 = NOVALUE;
    int _30969 = NOVALUE;
    int _30968 = NOVALUE;
    int _30966 = NOVALUE;
    int _30965 = NOVALUE;
    int _30964 = NOVALUE;
    int _30963 = NOVALUE;
    int _30962 = NOVALUE;
    int _30960 = NOVALUE;
    int _30959 = NOVALUE;
    int _30958 = NOVALUE;
    int _30956 = NOVALUE;
    int _30955 = NOVALUE;
    int _30954 = NOVALUE;
    int _30953 = NOVALUE;
    int _30949 = NOVALUE;
    int _30948 = NOVALUE;
    int _30947 = NOVALUE;
    int _30945 = NOVALUE;
    int _30944 = NOVALUE;
    int _30943 = NOVALUE;
    int _30942 = NOVALUE;
    int _30941 = NOVALUE;
    int _30940 = NOVALUE;
    int _30937 = NOVALUE;
    int _30936 = NOVALUE;
    int _30935 = NOVALUE;
    int _30933 = NOVALUE;
    int _30932 = NOVALUE;
    int _30931 = NOVALUE;
    int _30929 = NOVALUE;
    int _30928 = NOVALUE;
    int _30926 = NOVALUE;
    int _30925 = NOVALUE;
    int _30924 = NOVALUE;
    int _30920 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_prog_type_60956)) {
        _1 = (long)(DBL_PTR(_prog_type_60956)->dbl);
        if (UNIQUE(DBL_PTR(_prog_type_60956)) && (DBL_PTR(_prog_type_60956)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_prog_type_60956);
        _prog_type_60956 = _1;
    }

    /** 	integer first_def_arg*/

    /** 	integer again*/

    /** 	integer type_enum*/

    /** 	object i1_sym*/

    /** 	sequence enum_syms = {}*/
    RefDS(_22663);
    DeRef(_enum_syms_60971);
    _enum_syms_60971 = _22663;

    /** 	integer type_enum_gline, real_gline*/

    /** 	LeaveTopLevel()*/
    _41LeaveTopLevel();

    /** 	prog_name = next_token()*/
    _0 = _prog_name_60966;
    _prog_name_60966 = _41next_token();
    DeRef(_0);

    /** 	if prog_name[T_ID] = END_OF_FILE then*/
    _2 = (int)SEQ_PTR(_prog_name_60966);
    _30920 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30920, -21)){
        _30920 = NOVALUE;
        goto L1; // [41] 53
    }
    _30920 = NOVALUE;

    /** 		CompileErr( 32 )*/
    RefDS(_22663);
    _46CompileErr(32, _22663, 0);
L1: 

    /** 	type_enum =  0*/
    _type_enum_60969 = 0;

    /** 	if prog_type = TYPE_DECL then*/
    if (_prog_type_60956 != 416)
    goto L2; // [62] 308

    /** 		object tsym = prog_name[T_SYM]*/
    DeRef(_tsym_60984);
    _2 = (int)SEQ_PTR(_prog_name_60966);
    _tsym_60984 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_tsym_60984);

    /** 		if equal(sym_name(prog_name[T_SYM]),"enum") then*/
    _2 = (int)SEQ_PTR(_prog_name_60966);
    _30924 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_30924);
    _30925 = _55sym_name(_30924);
    _30924 = NOVALUE;
    if (_30925 == _27019)
    _30926 = 1;
    else if (IS_ATOM_INT(_30925) && IS_ATOM_INT(_27019))
    _30926 = 0;
    else
    _30926 = (compare(_30925, _27019) == 0);
    DeRef(_30925);
    _30925 = NOVALUE;
    if (_30926 == 0)
    {
        _30926 = NOVALUE;
        goto L3; // [90] 305
    }
    else{
        _30926 = NOVALUE;
    }

    /** 			EnterTopLevel( FALSE )*/
    _41EnterTopLevel(_9FALSE_426);

    /** 			type_enum_gline = gline_number*/
    _type_enum_gline_60972 = _38gline_number_16951;

    /** 			type_enum = 1*/
    _type_enum_60969 = 1;

    /** 			sequence seq_symbol*/

    /** 			prog_name = next_token()*/
    _0 = _prog_name_60966;
    _prog_name_60966 = _41next_token();
    DeRef(_0);

    /** 			if not find(prog_name[T_ID], ADDR_TOKS) then*/
    _2 = (int)SEQ_PTR(_prog_name_60966);
    _30928 = (int)*(((s1_ptr)_2)->base + 1);
    _30929 = find_from(_30928, _39ADDR_TOKS_16551, 1);
    _30928 = NOVALUE;
    if (_30929 != 0)
    goto L4; // [136] 161
    _30929 = NOVALUE;

    /** 				CompileErr(25, {find_category(prog_name[T_ID])} )*/
    _2 = (int)SEQ_PTR(_prog_name_60966);
    _30931 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_30931);
    _30932 = _65find_category(_30931);
    _30931 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _30932;
    _30933 = MAKE_SEQ(_1);
    _30932 = NOVALUE;
    _46CompileErr(25, _30933, 0);
    _30933 = NOVALUE;
L4: 

    /** 			enum_syms = Global_declaration(-1, scope)*/
    _0 = _enum_syms_60971;
    _enum_syms_60971 = _41Global_declaration(-1, _scope_60957);
    DeRef(_0);

    /** 			seq_symbol = enum_syms*/
    RefDS(_enum_syms_60971);
    DeRef(_seq_symbol_60995);
    _seq_symbol_60995 = _enum_syms_60971;

    /** 			for i = 1 to length( enum_syms ) do*/
    if (IS_SEQUENCE(_enum_syms_60971)){
            _30935 = SEQ_PTR(_enum_syms_60971)->length;
    }
    else {
        _30935 = 1;
    }
    {
        int _i_61011;
        _i_61011 = 1;
L5: 
        if (_i_61011 > _30935){
            goto L6; // [182] 210
        }

        /** 				seq_symbol[i] = sym_obj(enum_syms[i])*/
        _2 = (int)SEQ_PTR(_enum_syms_60971);
        _30936 = (int)*(((s1_ptr)_2)->base + _i_61011);
        Ref(_30936);
        _30937 = _55sym_obj(_30936);
        _30936 = NOVALUE;
        _2 = (int)SEQ_PTR(_seq_symbol_60995);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _seq_symbol_60995 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_61011);
        _1 = *(int *)_2;
        *(int *)_2 = _30937;
        if( _1 != _30937 ){
            DeRef(_1);
        }
        _30937 = NOVALUE;

        /** 			end for*/
        _i_61011 = _i_61011 + 1;
        goto L5; // [205] 189
L6: 
        ;
    }

    /** 			i1_sym = keyfind("i1",-1)*/
    RefDS(_30938);
    DeRef(_32345);
    _32345 = _30938;
    _32346 = _55hashfn(_32345);
    _32345 = NOVALUE;
    RefDS(_30938);
    _0 = _i1_sym_60970;
    _i1_sym_60970 = _55keyfind(_30938, -1, _38current_file_no_16946, 0, _32346);
    DeRef(_0);
    _32346 = NOVALUE;

    /** 			putback(keyfind("return",-1))*/
    RefDS(_27093);
    DeRef(_32343);
    _32343 = _27093;
    _32344 = _55hashfn(_32343);
    _32343 = NOVALUE;
    RefDS(_27093);
    _30940 = _55keyfind(_27093, -1, _38current_file_no_16946, 0, _32344);
    _32344 = NOVALUE;
    _41putback(_30940);
    _30940 = NOVALUE;

    /** 			putback({RIGHT_ROUND,0})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -27;
    ((int *)_2)[2] = 0;
    _30941 = MAKE_SEQ(_1);
    _41putback(_30941);
    _30941 = NOVALUE;

    /** 			putback(i1_sym)*/
    Ref(_i1_sym_60970);
    _41putback(_i1_sym_60970);

    /** 			putback(keyfind("object",-1))*/
    RefDS(_25325);
    DeRef(_32341);
    _32341 = _25325;
    _32342 = _55hashfn(_32341);
    _32341 = NOVALUE;
    RefDS(_25325);
    _30942 = _55keyfind(_25325, -1, _38current_file_no_16946, 0, _32342);
    _32342 = NOVALUE;
    _41putback(_30942);
    _30942 = NOVALUE;

    /** 			putback({LEFT_ROUND,0})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -26;
    ((int *)_2)[2] = 0;
    _30943 = MAKE_SEQ(_1);
    _41putback(_30943);
    _30943 = NOVALUE;

    /** 			LeaveTopLevel()*/
    _41LeaveTopLevel();
L3: 
    DeRef(_seq_symbol_60995);
    _seq_symbol_60995 = NOVALUE;
L2: 
    DeRef(_tsym_60984);
    _tsym_60984 = NOVALUE;

    /** 	if not find(prog_name[T_ID], ADDR_TOKS) then*/
    _2 = (int)SEQ_PTR(_prog_name_60966);
    _30944 = (int)*(((s1_ptr)_2)->base + 1);
    _30945 = find_from(_30944, _39ADDR_TOKS_16551, 1);
    _30944 = NOVALUE;
    if (_30945 != 0)
    goto L7; // [325] 350
    _30945 = NOVALUE;

    /** 		CompileErr(25, {find_category(prog_name[T_ID])} )*/
    _2 = (int)SEQ_PTR(_prog_name_60966);
    _30947 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_30947);
    _30948 = _65find_category(_30947);
    _30947 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _30948;
    _30949 = MAKE_SEQ(_1);
    _30948 = NOVALUE;
    _46CompileErr(25, _30949, 0);
    _30949 = NOVALUE;
L7: 

    /** 	p = prog_name[T_SYM]*/
    _2 = (int)SEQ_PTR(_prog_name_60966);
    _p_60961 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_p_60961)){
        _p_60961 = (long)DBL_PTR(_p_60961)->dbl;
    }

    /** 	DefinedYet(p)*/
    _55DefinedYet(_p_60961);

    /** 	if prog_type = PROCEDURE then*/
    if (_prog_type_60956 != 405)
    goto L8; // [369] 385

    /** 		pt = PROC*/
    _pt_60959 = 27;
    goto L9; // [382] 415
L8: 

    /** 	elsif prog_type = FUNCTION then*/
    if (_prog_type_60956 != 406)
    goto LA; // [389] 405

    /** 		pt = FUNC*/
    _pt_60959 = 501;
    goto L9; // [402] 415
LA: 

    /** 		pt = TYPE*/
    _pt_60959 = 504;
L9: 

    /** 	clear_fwd_refs()*/
    _40clear_fwd_refs();

    /** 	if find(SymTab[p][S_SCOPE], {SC_PREDEF, SC_GLOBAL, SC_PUBLIC, SC_EXPORT, SC_OVERRIDE}) then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30953 = (int)*(((s1_ptr)_2)->base + _p_60961);
    _2 = (int)SEQ_PTR(_30953);
    _30954 = (int)*(((s1_ptr)_2)->base + 4);
    _30953 = NOVALUE;
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 7;
    *((int *)(_2+8)) = 6;
    *((int *)(_2+12)) = 13;
    *((int *)(_2+16)) = 11;
    *((int *)(_2+20)) = 12;
    _30955 = MAKE_SEQ(_1);
    _30956 = find_from(_30954, _30955, 1);
    _30954 = NOVALUE;
    DeRefDS(_30955);
    _30955 = NOVALUE;
    if (_30956 == 0)
    {
        _30956 = NOVALUE;
        goto LB; // [456] 652
    }
    else{
        _30956 = NOVALUE;
    }

    /** 		if scope = SC_OVERRIDE then*/
    if (_scope_60957 != 12)
    goto LC; // [463] 589

    /** 			if SymTab[p][S_SCOPE] = SC_PREDEF or SymTab[p][S_SCOPE] = SC_OVERRIDE then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30958 = (int)*(((s1_ptr)_2)->base + _p_60961);
    _2 = (int)SEQ_PTR(_30958);
    _30959 = (int)*(((s1_ptr)_2)->base + 4);
    _30958 = NOVALUE;
    if (IS_ATOM_INT(_30959)) {
        _30960 = (_30959 == 7);
    }
    else {
        _30960 = binary_op(EQUALS, _30959, 7);
    }
    _30959 = NOVALUE;
    if (IS_ATOM_INT(_30960)) {
        if (_30960 != 0) {
            goto LD; // [487] 514
        }
    }
    else {
        if (DBL_PTR(_30960)->dbl != 0.0) {
            goto LD; // [487] 514
        }
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30962 = (int)*(((s1_ptr)_2)->base + _p_60961);
    _2 = (int)SEQ_PTR(_30962);
    _30963 = (int)*(((s1_ptr)_2)->base + 4);
    _30962 = NOVALUE;
    if (IS_ATOM_INT(_30963)) {
        _30964 = (_30963 == 12);
    }
    else {
        _30964 = binary_op(EQUALS, _30963, 12);
    }
    _30963 = NOVALUE;
    if (_30964 == 0) {
        DeRef(_30964);
        _30964 = NOVALUE;
        goto LE; // [510] 588
    }
    else {
        if (!IS_ATOM_INT(_30964) && DBL_PTR(_30964)->dbl == 0.0){
            DeRef(_30964);
            _30964 = NOVALUE;
            goto LE; // [510] 588
        }
        DeRef(_30964);
        _30964 = NOVALUE;
    }
    DeRef(_30964);
    _30964 = NOVALUE;
LD: 

    /** 					if SymTab[p][S_SCOPE] = SC_OVERRIDE then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30965 = (int)*(((s1_ptr)_2)->base + _p_60961);
    _2 = (int)SEQ_PTR(_30965);
    _30966 = (int)*(((s1_ptr)_2)->base + 4);
    _30965 = NOVALUE;
    if (binary_op_a(NOTEQ, _30966, 12)){
        _30966 = NOVALUE;
        goto LF; // [530] 542
    }
    _30966 = NOVALUE;

    /** 						again = 223*/
    _again_60968 = 223;
    goto L10; // [539] 548
LF: 

    /** 						again = 222*/
    _again_60968 = 222;
L10: 

    /** 					Warning(again, override_warning_flag,*/
    _2 = (int)SEQ_PTR(_35known_files_15596);
    _30968 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30969 = (int)*(((s1_ptr)_2)->base + _p_60961);
    _2 = (int)SEQ_PTR(_30969);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _30970 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _30970 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _30969 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_30968);
    *((int *)(_2+4)) = _30968;
    *((int *)(_2+8)) = _38line_number_16947;
    Ref(_30970);
    *((int *)(_2+12)) = _30970;
    _30971 = MAKE_SEQ(_1);
    _30970 = NOVALUE;
    _30968 = NOVALUE;
    _46Warning(_again_60968, 4, _30971);
    _30971 = NOVALUE;
LE: 
LC: 

    /** 		h = SymTab[p][S_HASHVAL]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30972 = (int)*(((s1_ptr)_2)->base + _p_60961);
    _2 = (int)SEQ_PTR(_30972);
    _h_60958 = (int)*(((s1_ptr)_2)->base + 11);
    if (!IS_ATOM_INT(_h_60958)){
        _h_60958 = (long)DBL_PTR(_h_60958)->dbl;
    }
    _30972 = NOVALUE;

    /** 		sym = buckets[h]*/
    _2 = (int)SEQ_PTR(_55buckets_47051);
    _sym_60963 = (int)*(((s1_ptr)_2)->base + _h_60958);
    if (!IS_ATOM_INT(_sym_60963)){
        _sym_60963 = (long)DBL_PTR(_sym_60963)->dbl;
    }

    /** 		p = NewEntry(SymTab[p][S_NAME], 0, 0, pt, h, sym, 0)*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30975 = (int)*(((s1_ptr)_2)->base + _p_60961);
    _2 = (int)SEQ_PTR(_30975);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _30976 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _30976 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _30975 = NOVALUE;
    Ref(_30976);
    _p_60961 = _55NewEntry(_30976, 0, 0, _pt_60959, _h_60958, _sym_60963, 0);
    _30976 = NOVALUE;
    if (!IS_ATOM_INT(_p_60961)) {
        _1 = (long)(DBL_PTR(_p_60961)->dbl);
        if (UNIQUE(DBL_PTR(_p_60961)) && (DBL_PTR(_p_60961)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_p_60961);
        _p_60961 = _1;
    }

    /** 		buckets[h] = p*/
    _2 = (int)SEQ_PTR(_55buckets_47051);
    _2 = (int)(((s1_ptr)_2)->base + _h_60958);
    _1 = *(int *)_2;
    *(int *)_2 = _p_60961;
    DeRef(_1);
LB: 

    /** 	Start_block( pt, p )*/
    _68Start_block(_pt_60959, _p_60961);

    /** 	CurrentSub = p*/
    _38CurrentSub_16954 = _p_60961;

    /** 	first_def_arg = 0*/
    _first_def_arg_60967 = 0;

    /** 	temps_allocated = 0*/
    _55temps_allocated_47575 = 0;

    /** 	SymTab[p][S_SCOPE] = scope*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_60961 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _scope_60957;
    DeRef(_1);
    _30978 = NOVALUE;

    /** 	SymTab[p][S_TOKEN] = pt*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_60961 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_TOKEN_16603))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    _1 = *(int *)_2;
    *(int *)_2 = _pt_60959;
    DeRef(_1);
    _30980 = NOVALUE;

    /** 	if length(SymTab[p]) < SIZEOF_ROUTINE_ENTRY then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30982 = (int)*(((s1_ptr)_2)->base + _p_60961);
    if (IS_SEQUENCE(_30982)){
            _30983 = SEQ_PTR(_30982)->length;
    }
    else {
        _30983 = 1;
    }
    _30982 = NOVALUE;
    if (_30983 >= _38SIZEOF_ROUTINE_ENTRY_16724)
    goto L11; // [722] 764

    /** 		SymTab[p] = SymTab[p] & repeat(0, SIZEOF_ROUTINE_ENTRY -*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30985 = (int)*(((s1_ptr)_2)->base + _p_60961);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _30986 = (int)*(((s1_ptr)_2)->base + _p_60961);
    if (IS_SEQUENCE(_30986)){
            _30987 = SEQ_PTR(_30986)->length;
    }
    else {
        _30987 = 1;
    }
    _30986 = NOVALUE;
    _30988 = _38SIZEOF_ROUTINE_ENTRY_16724 - _30987;
    _30987 = NOVALUE;
    _30989 = Repeat(0, _30988);
    _30988 = NOVALUE;
    if (IS_SEQUENCE(_30985) && IS_ATOM(_30989)) {
    }
    else if (IS_ATOM(_30985) && IS_SEQUENCE(_30989)) {
        Ref(_30985);
        Prepend(&_30990, _30989, _30985);
    }
    else {
        Concat((object_ptr)&_30990, _30985, _30989);
        _30985 = NOVALUE;
    }
    _30985 = NOVALUE;
    DeRefDS(_30989);
    _30989 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _p_60961);
    _1 = *(int *)_2;
    *(int *)_2 = _30990;
    if( _1 != _30990 ){
        DeRef(_1);
    }
    _30990 = NOVALUE;
L11: 

    /** 	SymTab[p][S_CODE] = 0*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_60961 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_CODE_16610))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_CODE_16610);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _30991 = NOVALUE;

    /** 	SymTab[p][S_LINETAB] = 0*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_60961 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_LINETAB_16633))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_LINETAB_16633)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_LINETAB_16633);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _30993 = NOVALUE;

    /** 	SymTab[p][S_EFFECT] = E_PURE*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_60961 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 23);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _30995 = NOVALUE;

    /** 	SymTab[p][S_REFLIST] = {}*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_60961 + ((s1_ptr)_2)->base);
    RefDS(_22663);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 24);
    _1 = *(int *)_2;
    *(int *)_2 = _22663;
    DeRef(_1);
    _30997 = NOVALUE;

    /** 	SymTab[p][S_FIRSTLINE] = gline_number*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_60961 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_FIRSTLINE_16638))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FIRSTLINE_16638)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_FIRSTLINE_16638);
    _1 = *(int *)_2;
    *(int *)_2 = _38gline_number_16951;
    DeRef(_1);
    _30999 = NOVALUE;

    /** 	SymTab[p][S_TEMPS] = 0*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_60961 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_TEMPS_16643))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TEMPS_16643)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_TEMPS_16643);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _31001 = NOVALUE;

    /** 	SymTab[p][S_RESIDENT_TASK] = 0*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_60961 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 25);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _31003 = NOVALUE;

    /** 	SymTab[p][S_SAVED_PRIVATES] = {}*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_60961 + ((s1_ptr)_2)->base);
    RefDS(_22663);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 26);
    _1 = *(int *)_2;
    *(int *)_2 = _22663;
    DeRef(_1);
    _31005 = NOVALUE;

    /** 	if type_enum then*/
    if (_type_enum_60969 == 0)
    {
        goto L12; // [890] 947
    }
    else{
    }

    /** 		SymTab[p][S_FIRSTLINE] = type_enum_gline*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_60961 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_FIRSTLINE_16638))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FIRSTLINE_16638)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_FIRSTLINE_16638);
    _1 = *(int *)_2;
    *(int *)_2 = _type_enum_gline_60972;
    DeRef(_1);
    _31007 = NOVALUE;

    /** 		real_gline = gline_number*/
    _real_gline_60973 = _38gline_number_16951;

    /** 		gline_number = type_enum_gline*/
    _38gline_number_16951 = _type_enum_gline_60972;

    /** 		StartSourceLine( FALSE, , COVERAGE_OVERRIDE )*/
    _43StartSourceLine(_9FALSE_426, 0, 3);

    /** 		gline_number = real_gline*/
    _38gline_number_16951 = _real_gline_60973;
    goto L13; // [944] 959
L12: 

    /** 		StartSourceLine(FALSE, , COVERAGE_OVERRIDE)*/
    _43StartSourceLine(_9FALSE_426, 0, 3);
L13: 

    /** 	tok_match(LEFT_ROUND)*/
    _41tok_match(-26, 0);

    /** 	tok = next_token()*/
    _0 = _tok_60965;
    _tok_60965 = _41next_token();
    DeRef(_0);

    /** 	param_num = 0*/
    _41param_num_55131 = 0;

    /** 	sequence middle_def_args = {}*/
    RefDS(_22663);
    DeRef(_middle_def_args_61192);
    _middle_def_args_61192 = _22663;

    /** 	integer last_nda = 0, start_def = 0*/
    _last_nda_61193 = 0;
    _start_def_61194 = 0;

    /** 	symtab_index last_link = p*/
    _last_link_61196 = _p_60961;

    /** 	while tok[T_ID] != RIGHT_ROUND do*/
L14: 
    _2 = (int)SEQ_PTR(_tok_60965);
    _31010 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _31010, -27)){
        _31010 = NOVALUE;
        goto L15; // [1012] 1783
    }
    _31010 = NOVALUE;

    /** 		if tok[T_ID] != TYPE and tok[T_ID] != QUALIFIED_TYPE then*/
    _2 = (int)SEQ_PTR(_tok_60965);
    _31012 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31012)) {
        _31013 = (_31012 != 504);
    }
    else {
        _31013 = binary_op(NOTEQ, _31012, 504);
    }
    _31012 = NOVALUE;
    if (IS_ATOM_INT(_31013)) {
        if (_31013 == 0) {
            goto L16; // [1030] 1254
        }
    }
    else {
        if (DBL_PTR(_31013)->dbl == 0.0) {
            goto L16; // [1030] 1254
        }
    }
    _2 = (int)SEQ_PTR(_tok_60965);
    _31015 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31015)) {
        _31016 = (_31015 != 522);
    }
    else {
        _31016 = binary_op(NOTEQ, _31015, 522);
    }
    _31015 = NOVALUE;
    if (_31016 == 0) {
        DeRef(_31016);
        _31016 = NOVALUE;
        goto L16; // [1047] 1254
    }
    else {
        if (!IS_ATOM_INT(_31016) && DBL_PTR(_31016)->dbl == 0.0){
            DeRef(_31016);
            _31016 = NOVALUE;
            goto L16; // [1047] 1254
        }
        DeRef(_31016);
        _31016 = NOVALUE;
    }
    DeRef(_31016);
    _31016 = NOVALUE;

    /** 			if tok[T_ID] = VARIABLE or tok[T_ID] = QUALIFIED_VARIABLE then*/
    _2 = (int)SEQ_PTR(_tok_60965);
    _31017 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31017)) {
        _31018 = (_31017 == -100);
    }
    else {
        _31018 = binary_op(EQUALS, _31017, -100);
    }
    _31017 = NOVALUE;
    if (IS_ATOM_INT(_31018)) {
        if (_31018 != 0) {
            goto L17; // [1064] 1085
        }
    }
    else {
        if (DBL_PTR(_31018)->dbl != 0.0) {
            goto L17; // [1064] 1085
        }
    }
    _2 = (int)SEQ_PTR(_tok_60965);
    _31020 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31020)) {
        _31021 = (_31020 == 512);
    }
    else {
        _31021 = binary_op(EQUALS, _31020, 512);
    }
    _31020 = NOVALUE;
    if (_31021 == 0) {
        DeRef(_31021);
        _31021 = NOVALUE;
        goto L18; // [1081] 1245
    }
    else {
        if (!IS_ATOM_INT(_31021) && DBL_PTR(_31021)->dbl == 0.0){
            DeRef(_31021);
            _31021 = NOVALUE;
            goto L18; // [1081] 1245
        }
        DeRef(_31021);
        _31021 = NOVALUE;
    }
    DeRef(_31021);
    _31021 = NOVALUE;
L17: 

    /** 				token temptok = next_token()*/
    _0 = _temptok_61223;
    _temptok_61223 = _41next_token();
    DeRef(_0);

    /** 				integer undef_type = 0*/
    _undef_type_61225 = 0;

    /** 				if temptok[T_ID] != TYPE and temptok[T_ID] != QUALIFIED_TYPE then*/
    _2 = (int)SEQ_PTR(_temptok_61223);
    _31023 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31023)) {
        _31024 = (_31023 != 504);
    }
    else {
        _31024 = binary_op(NOTEQ, _31023, 504);
    }
    _31023 = NOVALUE;
    if (IS_ATOM_INT(_31024)) {
        if (_31024 == 0) {
            goto L19; // [1109] 1210
        }
    }
    else {
        if (DBL_PTR(_31024)->dbl == 0.0) {
            goto L19; // [1109] 1210
        }
    }
    _2 = (int)SEQ_PTR(_temptok_61223);
    _31026 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31026)) {
        _31027 = (_31026 != 522);
    }
    else {
        _31027 = binary_op(NOTEQ, _31026, 522);
    }
    _31026 = NOVALUE;
    if (_31027 == 0) {
        DeRef(_31027);
        _31027 = NOVALUE;
        goto L19; // [1126] 1210
    }
    else {
        if (!IS_ATOM_INT(_31027) && DBL_PTR(_31027)->dbl == 0.0){
            DeRef(_31027);
            _31027 = NOVALUE;
            goto L19; // [1126] 1210
        }
        DeRef(_31027);
        _31027 = NOVALUE;
    }
    DeRef(_31027);
    _31027 = NOVALUE;

    /** 					if find( temptok[T_ID], FULL_ID_TOKS) then*/
    _2 = (int)SEQ_PTR(_temptok_61223);
    _31028 = (int)*(((s1_ptr)_2)->base + 1);
    _31029 = find_from(_31028, _39FULL_ID_TOKS_16555, 1);
    _31028 = NOVALUE;
    if (_31029 == 0)
    {
        _31029 = NOVALUE;
        goto L1A; // [1144] 1209
    }
    else{
        _31029 = NOVALUE;
    }

    /** 						if SymTab[tok[T_SYM]][S_SCOPE] = SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_tok_60965);
    _31030 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_31030)){
        _31031 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31030)->dbl));
    }
    else{
        _31031 = (int)*(((s1_ptr)_2)->base + _31030);
    }
    _2 = (int)SEQ_PTR(_31031);
    _31032 = (int)*(((s1_ptr)_2)->base + 4);
    _31031 = NOVALUE;
    if (binary_op_a(NOTEQ, _31032, 9)){
        _31032 = NOVALUE;
        goto L1B; // [1169] 1200
    }
    _31032 = NOVALUE;

    /** 							undef_type = - new_forward_reference( TYPE, tok[T_SYM] )*/
    _2 = (int)SEQ_PTR(_tok_60965);
    _31034 = (int)*(((s1_ptr)_2)->base + 2);
    DeRef(_32340);
    _32340 = 504;
    Ref(_31034);
    _31035 = _40new_forward_reference(504, _31034, 504);
    _31034 = NOVALUE;
    _32340 = NOVALUE;
    if (IS_ATOM_INT(_31035)) {
        if ((unsigned long)_31035 == 0xC0000000)
        _undef_type_61225 = (int)NewDouble((double)-0xC0000000);
        else
        _undef_type_61225 = - _31035;
    }
    else {
        _undef_type_61225 = unary_op(UMINUS, _31035);
    }
    DeRef(_31035);
    _31035 = NOVALUE;
    if (!IS_ATOM_INT(_undef_type_61225)) {
        _1 = (long)(DBL_PTR(_undef_type_61225)->dbl);
        if (UNIQUE(DBL_PTR(_undef_type_61225)) && (DBL_PTR(_undef_type_61225)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_undef_type_61225);
        _undef_type_61225 = _1;
    }
    goto L1C; // [1197] 1208
L1B: 

    /** 							CompileErr(37)*/
    RefDS(_22663);
    _46CompileErr(37, _22663, 0);
L1C: 
L1A: 
L19: 

    /** 				putback(temptok) -- Return whatever came after the name back onto the token stream.*/
    Ref(_temptok_61223);
    _41putback(_temptok_61223);

    /** 				if undef_type != 0 then*/
    if (_undef_type_61225 == 0)
    goto L1D; // [1217] 1232

    /** 					tok[T_SYM] = undef_type*/
    _2 = (int)SEQ_PTR(_tok_60965);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _tok_60965 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _undef_type_61225;
    DeRef(_1);
    goto L1E; // [1229] 1240
L1D: 

    /** 					CompileErr(37)*/
    RefDS(_22663);
    _46CompileErr(37, _22663, 0);
L1E: 
    DeRef(_temptok_61223);
    _temptok_61223 = NOVALUE;
    goto L1F; // [1242] 1253
L18: 

    /** 				CompileErr(37)*/
    RefDS(_22663);
    _46CompileErr(37, _22663, 0);
L1F: 
L16: 

    /** 		type_sym = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_60965);
    _type_sym_60962 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_type_sym_60962)){
        _type_sym_60962 = (long)DBL_PTR(_type_sym_60962)->dbl;
    }

    /** 		tok = next_token()*/
    _0 = _tok_60965;
    _tok_60965 = _41next_token();
    DeRef(_0);

    /** 		if not find(tok[T_ID], ID_TOKS) then*/
    _2 = (int)SEQ_PTR(_tok_60965);
    _31040 = (int)*(((s1_ptr)_2)->base + 1);
    _31041 = find_from(_31040, _39ID_TOKS_16553, 1);
    _31040 = NOVALUE;
    if (_31041 != 0)
    goto L20; // [1284] 1398
    _31041 = NOVALUE;

    /** 			sequence tokcat = find_category(tok[T_ID])*/
    _2 = (int)SEQ_PTR(_tok_60965);
    _31043 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_31043);
    _0 = _tokcat_61274;
    _tokcat_61274 = _65find_category(_31043);
    DeRef(_0);
    _31043 = NOVALUE;

    /** 			if tok[T_SYM] != 0 and length(SymTab[tok[T_SYM]]) >= S_NAME then*/
    _2 = (int)SEQ_PTR(_tok_60965);
    _31045 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_31045)) {
        _31046 = (_31045 != 0);
    }
    else {
        _31046 = binary_op(NOTEQ, _31045, 0);
    }
    _31045 = NOVALUE;
    if (IS_ATOM_INT(_31046)) {
        if (_31046 == 0) {
            goto L21; // [1313] 1374
        }
    }
    else {
        if (DBL_PTR(_31046)->dbl == 0.0) {
            goto L21; // [1313] 1374
        }
    }
    _2 = (int)SEQ_PTR(_tok_60965);
    _31048 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_31048)){
        _31049 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31048)->dbl));
    }
    else{
        _31049 = (int)*(((s1_ptr)_2)->base + _31048);
    }
    if (IS_SEQUENCE(_31049)){
            _31050 = SEQ_PTR(_31049)->length;
    }
    else {
        _31050 = 1;
    }
    _31049 = NOVALUE;
    if (IS_ATOM_INT(_38S_NAME_16598)) {
        _31051 = (_31050 >= _38S_NAME_16598);
    }
    else {
        _31051 = binary_op(GREATEREQ, _31050, _38S_NAME_16598);
    }
    _31050 = NOVALUE;
    if (_31051 == 0) {
        DeRef(_31051);
        _31051 = NOVALUE;
        goto L21; // [1339] 1374
    }
    else {
        if (!IS_ATOM_INT(_31051) && DBL_PTR(_31051)->dbl == 0.0){
            DeRef(_31051);
            _31051 = NOVALUE;
            goto L21; // [1339] 1374
        }
        DeRef(_31051);
        _31051 = NOVALUE;
    }
    DeRef(_31051);
    _31051 = NOVALUE;

    /** 				CompileErr(90, {tokcat, SymTab[tok[T_SYM]][S_NAME]})*/
    _2 = (int)SEQ_PTR(_tok_60965);
    _31052 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_31052)){
        _31053 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31052)->dbl));
    }
    else{
        _31053 = (int)*(((s1_ptr)_2)->base + _31052);
    }
    _2 = (int)SEQ_PTR(_31053);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _31054 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _31054 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _31053 = NOVALUE;
    Ref(_31054);
    RefDS(_tokcat_61274);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _tokcat_61274;
    ((int *)_2)[2] = _31054;
    _31055 = MAKE_SEQ(_1);
    _31054 = NOVALUE;
    _46CompileErr(90, _31055, 0);
    _31055 = NOVALUE;
    goto L22; // [1371] 1397
L21: 

    /** 				CompileErr(92, {LexName(tok[T_ID])})*/
    _2 = (int)SEQ_PTR(_tok_60965);
    _31056 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_31056);
    RefDS(_27166);
    _31057 = _43LexName(_31056, _27166);
    _31056 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _31057;
    _31058 = MAKE_SEQ(_1);
    _31057 = NOVALUE;
    _46CompileErr(92, _31058, 0);
    _31058 = NOVALUE;
L22: 
L20: 
    DeRef(_tokcat_61274);
    _tokcat_61274 = NOVALUE;

    /** 		sym = SetPrivateScope(tok[T_SYM], type_sym, param_num)*/
    _2 = (int)SEQ_PTR(_tok_60965);
    _31059 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_31059);
    _sym_60963 = _41SetPrivateScope(_31059, _type_sym_60962, _41param_num_55131);
    _31059 = NOVALUE;
    if (!IS_ATOM_INT(_sym_60963)) {
        _1 = (long)(DBL_PTR(_sym_60963)->dbl);
        if (UNIQUE(DBL_PTR(_sym_60963)) && (DBL_PTR(_sym_60963)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_60963);
        _sym_60963 = _1;
    }

    /** 		param_num += 1*/
    _41param_num_55131 = _41param_num_55131 + 1;

    /** 		if SymTab[last_link][S_NEXT] != sym*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31062 = (int)*(((s1_ptr)_2)->base + _last_link_61196);
    _2 = (int)SEQ_PTR(_31062);
    _31063 = (int)*(((s1_ptr)_2)->base + 2);
    _31062 = NOVALUE;
    if (IS_ATOM_INT(_31063)) {
        _31064 = (_31063 != _sym_60963);
    }
    else {
        _31064 = binary_op(NOTEQ, _31063, _sym_60963);
    }
    _31063 = NOVALUE;
    if (IS_ATOM_INT(_31064)) {
        if (_31064 == 0) {
            goto L23; // [1444] 1525
        }
    }
    else {
        if (DBL_PTR(_31064)->dbl == 0.0) {
            goto L23; // [1444] 1525
        }
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31066 = (int)*(((s1_ptr)_2)->base + _last_link_61196);
    _2 = (int)SEQ_PTR(_31066);
    _31067 = (int)*(((s1_ptr)_2)->base + 2);
    _31066 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_31067)){
        _31068 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31067)->dbl));
    }
    else{
        _31068 = (int)*(((s1_ptr)_2)->base + _31067);
    }
    _2 = (int)SEQ_PTR(_31068);
    _31069 = (int)*(((s1_ptr)_2)->base + 4);
    _31068 = NOVALUE;
    if (IS_ATOM_INT(_31069)) {
        _31070 = (_31069 == 9);
    }
    else {
        _31070 = binary_op(EQUALS, _31069, 9);
    }
    _31069 = NOVALUE;
    if (_31070 == 0) {
        DeRef(_31070);
        _31070 = NOVALUE;
        goto L23; // [1479] 1525
    }
    else {
        if (!IS_ATOM_INT(_31070) && DBL_PTR(_31070)->dbl == 0.0){
            DeRef(_31070);
            _31070 = NOVALUE;
            goto L23; // [1479] 1525
        }
        DeRef(_31070);
        _31070 = NOVALUE;
    }
    DeRef(_31070);
    _31070 = NOVALUE;

    /** 			SymTab[SymTab[last_link][S_NEXT]][S_NEXT] = 0*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31071 = (int)*(((s1_ptr)_2)->base + _last_link_61196);
    _2 = (int)SEQ_PTR(_31071);
    _31072 = (int)*(((s1_ptr)_2)->base + 2);
    _31071 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_31072))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_31072)->dbl));
    else
    _3 = (int)(_31072 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _31073 = NOVALUE;

    /** 			SymTab[last_link][S_NEXT] = sym*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_last_link_61196 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_60963;
    DeRef(_1);
    _31075 = NOVALUE;
L23: 

    /** 		last_link = sym*/
    _last_link_61196 = _sym_60963;

    /** 		if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L24; // [1536] 1559
    }
    else{
    }

    /** 			SymTab[sym][S_GTYPE] = CompileType(type_sym)*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_60963 + ((s1_ptr)_2)->base);
    _31079 = _41CompileType(_type_sym_60962);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = _31079;
    if( _1 != _31079 ){
        DeRef(_1);
    }
    _31079 = NOVALUE;
    _31077 = NOVALUE;
L24: 

    /** 		tok = next_token()*/
    _0 = _tok_60965;
    _tok_60965 = _41next_token();
    DeRef(_0);

    /** 		if tok[T_ID] = EQUALS then -- defaulted parameter*/
    _2 = (int)SEQ_PTR(_tok_60965);
    _31081 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31081, 3)){
        _31081 = NOVALUE;
        goto L25; // [1574] 1654
    }
    _31081 = NOVALUE;

    /** 			start_recording()*/
    _41start_recording();

    /** 			Expr()*/
    _41Expr();

    /** 			SymTab[sym][S_CODE] = restore_parser()*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_60963 + ((s1_ptr)_2)->base);
    _31085 = _41restore_parser();
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_CODE_16610))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_CODE_16610);
    _1 = *(int *)_2;
    *(int *)_2 = _31085;
    if( _1 != _31085 ){
        DeRef(_1);
    }
    _31085 = NOVALUE;
    _31083 = NOVALUE;

    /** 			if Pop() then end if -- don't leak the default argument*/
    _31086 = _43Pop();
    if (_31086 == 0) {
        DeRef(_31086);
        _31086 = NOVALUE;
        goto L26; // [1609] 1613
    }
    else {
        if (!IS_ATOM_INT(_31086) && DBL_PTR(_31086)->dbl == 0.0){
            DeRef(_31086);
            _31086 = NOVALUE;
            goto L26; // [1609] 1613
        }
        DeRef(_31086);
        _31086 = NOVALUE;
    }
    DeRef(_31086);
    _31086 = NOVALUE;
L26: 

    /** 			tok = next_token()*/
    _0 = _tok_60965;
    _tok_60965 = _41next_token();
    DeRef(_0);

    /** 			if first_def_arg = 0 then*/
    if (_first_def_arg_60967 != 0)
    goto L27; // [1620] 1632

    /** 				first_def_arg = param_num*/
    _first_def_arg_60967 = _41param_num_55131;
L27: 

    /** 			previous_op = -1 -- no interferences betwen defparms, or them and subsequent code*/
    _38previous_op_17062 = -1;

    /** 			if start_def = 0 then*/
    if (_start_def_61194 != 0)
    goto L28; // [1639] 1711

    /** 				start_def = param_num*/
    _start_def_61194 = _41param_num_55131;
    goto L28; // [1651] 1711
L25: 

    /** 			last_nda = param_num*/
    _last_nda_61193 = _41param_num_55131;

    /** 			if start_def then*/
    if (_start_def_61194 == 0)
    {
        goto L29; // [1663] 1710
    }
    else{
    }

    /** 				if start_def = param_num-1 then*/
    _31090 = _41param_num_55131 - 1;
    if ((long)((unsigned long)_31090 +(unsigned long) HIGH_BITS) >= 0){
        _31090 = NewDouble((double)_31090);
    }
    if (binary_op_a(NOTEQ, _start_def_61194, _31090)){
        DeRef(_31090);
        _31090 = NOVALUE;
        goto L2A; // [1674] 1687
    }
    DeRef(_31090);
    _31090 = NOVALUE;

    /** 					middle_def_args &= start_def*/
    Append(&_middle_def_args_61192, _middle_def_args_61192, _start_def_61194);
    goto L2B; // [1684] 1704
L2A: 

    /** 					middle_def_args = append(middle_def_args, {start_def, param_num-1})*/
    _31093 = _41param_num_55131 - 1;
    if ((long)((unsigned long)_31093 +(unsigned long) HIGH_BITS) >= 0){
        _31093 = NewDouble((double)_31093);
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _start_def_61194;
    ((int *)_2)[2] = _31093;
    _31094 = MAKE_SEQ(_1);
    _31093 = NOVALUE;
    RefDS(_31094);
    Append(&_middle_def_args_61192, _middle_def_args_61192, _31094);
    DeRefDS(_31094);
    _31094 = NOVALUE;
L2B: 

    /** 				start_def = 0*/
    _start_def_61194 = 0;
L29: 
L28: 

    /** 		if tok[T_ID] = COMMA then*/
    _2 = (int)SEQ_PTR(_tok_60965);
    _31096 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31096, -30)){
        _31096 = NOVALUE;
        goto L2C; // [1721] 1755
    }
    _31096 = NOVALUE;

    /** 			tok = next_token()*/
    _0 = _tok_60965;
    _tok_60965 = _41next_token();
    DeRef(_0);

    /** 			if tok[T_ID] = RIGHT_ROUND then*/
    _2 = (int)SEQ_PTR(_tok_60965);
    _31099 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31099, -27)){
        _31099 = NOVALUE;
        goto L14; // [1740] 1004
    }
    _31099 = NOVALUE;

    /** 				CompileErr(85)*/
    RefDS(_22663);
    _46CompileErr(85, _22663, 0);
    goto L14; // [1752] 1004
L2C: 

    /** 		elsif tok[T_ID] != RIGHT_ROUND then*/
    _2 = (int)SEQ_PTR(_tok_60965);
    _31101 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _31101, -27)){
        _31101 = NOVALUE;
        goto L14; // [1765] 1004
    }
    _31101 = NOVALUE;

    /** 			CompileErr(41)*/
    RefDS(_22663);
    _46CompileErr(41, _22663, 0);

    /** 	end while*/
    goto L14; // [1780] 1004
L15: 

    /** 	Code = {} -- removes any spurious code emitted while recording parameters*/
    RefDS(_22663);
    DeRef(_38Code_17038);
    _38Code_17038 = _22663;

    /** 	SymTab[p][S_NUM_ARGS] = param_num*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_60961 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    _1 = *(int *)_2;
    *(int *)_2 = _41param_num_55131;
    DeRef(_1);
    _31103 = NOVALUE;

    /** 	SymTab[p][S_DEF_ARGS] = {first_def_arg, last_nda, middle_def_args}*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_60961 + ((s1_ptr)_2)->base);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _first_def_arg_60967;
    *((int *)(_2+8)) = _last_nda_61193;
    RefDS(_middle_def_args_61192);
    *((int *)(_2+12)) = _middle_def_args_61192;
    _31107 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 28);
    _1 = *(int *)_2;
    *(int *)_2 = _31107;
    if( _1 != _31107 ){
        DeRef(_1);
    }
    _31107 = NOVALUE;
    _31105 = NOVALUE;

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L2D; // [1832] 1866
    }
    else{
    }

    /** 		if param_num > max_params then*/
    if (_41param_num_55131 <= _43max_params_51224)
    goto L2E; // [1841] 1855

    /** 			max_params = param_num*/
    _43max_params_51224 = _41param_num_55131;
L2E: 

    /** 		num_routines += 1*/
    _38num_routines_16955 = _38num_routines_16955 + 1;
L2D: 

    /** 	if SymTab[p][S_TOKEN] = TYPE and param_num != 1 then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31110 = (int)*(((s1_ptr)_2)->base + _p_60961);
    _2 = (int)SEQ_PTR(_31110);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _31111 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _31111 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _31110 = NOVALUE;
    if (IS_ATOM_INT(_31111)) {
        _31112 = (_31111 == 504);
    }
    else {
        _31112 = binary_op(EQUALS, _31111, 504);
    }
    _31111 = NOVALUE;
    if (IS_ATOM_INT(_31112)) {
        if (_31112 == 0) {
            goto L2F; // [1886] 1908
        }
    }
    else {
        if (DBL_PTR(_31112)->dbl == 0.0) {
            goto L2F; // [1886] 1908
        }
    }
    _31114 = (_41param_num_55131 != 1);
    if (_31114 == 0)
    {
        DeRef(_31114);
        _31114 = NOVALUE;
        goto L2F; // [1897] 1908
    }
    else{
        DeRef(_31114);
        _31114 = NOVALUE;
    }

    /** 		CompileErr(148)*/
    RefDS(_22663);
    _46CompileErr(148, _22663, 0);
L2F: 

    /** 	include_routine()*/
    _52include_routine();

    /** 	sym = SymTab[p][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31115 = (int)*(((s1_ptr)_2)->base + _p_60961);
    _2 = (int)SEQ_PTR(_31115);
    _sym_60963 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_sym_60963)){
        _sym_60963 = (long)DBL_PTR(_sym_60963)->dbl;
    }
    _31115 = NOVALUE;

    /** 	for i = 1 to SymTab[p][S_NUM_ARGS] do*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31117 = (int)*(((s1_ptr)_2)->base + _p_60961);
    _2 = (int)SEQ_PTR(_31117);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _31118 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _31118 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    _31117 = NOVALUE;
    {
        int _i_61428;
        _i_61428 = 1;
L30: 
        if (binary_op_a(GREATER, _i_61428, _31118)){
            goto L31; // [1942] 2021
        }

        /** 		while SymTab[sym][S_SCOPE] != SC_PRIVATE do*/
L32: 
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _31119 = (int)*(((s1_ptr)_2)->base + _sym_60963);
        _2 = (int)SEQ_PTR(_31119);
        _31120 = (int)*(((s1_ptr)_2)->base + 4);
        _31119 = NOVALUE;
        if (binary_op_a(EQUALS, _31120, 3)){
            _31120 = NOVALUE;
            goto L33; // [1968] 1993
        }
        _31120 = NOVALUE;

        /** 			sym = SymTab[sym][S_NEXT]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _31122 = (int)*(((s1_ptr)_2)->base + _sym_60963);
        _2 = (int)SEQ_PTR(_31122);
        _sym_60963 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_sym_60963)){
            _sym_60963 = (long)DBL_PTR(_sym_60963)->dbl;
        }
        _31122 = NOVALUE;

        /** 		end while*/
        goto L32; // [1990] 1954
L33: 

        /** 		TypeCheck(sym)*/
        _41TypeCheck(_sym_60963);

        /** 		sym = SymTab[sym][S_NEXT]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _31124 = (int)*(((s1_ptr)_2)->base + _sym_60963);
        _2 = (int)SEQ_PTR(_31124);
        _sym_60963 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_sym_60963)){
            _sym_60963 = (long)DBL_PTR(_sym_60963)->dbl;
        }
        _31124 = NOVALUE;

        /** 	end for*/
        _0 = _i_61428;
        if (IS_ATOM_INT(_i_61428)) {
            _i_61428 = _i_61428 + 1;
            if ((long)((unsigned long)_i_61428 +(unsigned long) HIGH_BITS) >= 0){
                _i_61428 = NewDouble((double)_i_61428);
            }
        }
        else {
            _i_61428 = binary_op_a(PLUS, _i_61428, 1);
        }
        DeRef(_0);
        goto L30; // [2016] 1949
L31: 
        ;
        DeRef(_i_61428);
    }

    /** 	tok = next_token()*/
    _0 = _tok_60965;
    _tok_60965 = _41next_token();
    DeRef(_0);

    /** 	while tok[T_ID] = TYPE or tok[T_ID] = QUALIFIED_TYPE do*/
L34: 
    _2 = (int)SEQ_PTR(_tok_60965);
    _31127 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31127)) {
        _31128 = (_31127 == 504);
    }
    else {
        _31128 = binary_op(EQUALS, _31127, 504);
    }
    _31127 = NOVALUE;
    if (IS_ATOM_INT(_31128)) {
        if (_31128 != 0) {
            goto L35; // [2043] 2064
        }
    }
    else {
        if (DBL_PTR(_31128)->dbl != 0.0) {
            goto L35; // [2043] 2064
        }
    }
    _2 = (int)SEQ_PTR(_tok_60965);
    _31130 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31130)) {
        _31131 = (_31130 == 522);
    }
    else {
        _31131 = binary_op(EQUALS, _31130, 522);
    }
    _31130 = NOVALUE;
    if (_31131 <= 0) {
        if (_31131 == 0) {
            DeRef(_31131);
            _31131 = NOVALUE;
            goto L36; // [2060] 2085
        }
        else {
            if (!IS_ATOM_INT(_31131) && DBL_PTR(_31131)->dbl == 0.0){
                DeRef(_31131);
                _31131 = NOVALUE;
                goto L36; // [2060] 2085
            }
            DeRef(_31131);
            _31131 = NOVALUE;
        }
    }
    DeRef(_31131);
    _31131 = NOVALUE;
L35: 

    /** 		Private_declaration(tok[T_SYM])*/
    _2 = (int)SEQ_PTR(_tok_60965);
    _31132 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_31132);
    _41Private_declaration(_31132);
    _31132 = NOVALUE;

    /** 		tok = next_token()*/
    _0 = _tok_60965;
    _tok_60965 = _41next_token();
    DeRef(_0);

    /** 	end while*/
    goto L34; // [2082] 2031
L36: 

    /** 	if not TRANSLATE then*/
    if (_38TRANSLATE_16564 != 0)
    goto L37; // [2089] 2192

    /** 		if OpTrace then*/
    if (_38OpTrace_17015 == 0)
    {
        goto L38; // [2096] 2191
    }
    else{
    }

    /** 			emit_op(ERASE_PRIVATE_NAMES)*/
    _43emit_op(88);

    /** 			emit_addr(p)*/
    _43emit_addr(_p_60961);

    /** 			sym = SymTab[p][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31135 = (int)*(((s1_ptr)_2)->base + _p_60961);
    _2 = (int)SEQ_PTR(_31135);
    _sym_60963 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_sym_60963)){
        _sym_60963 = (long)DBL_PTR(_sym_60963)->dbl;
    }
    _31135 = NOVALUE;

    /** 			for i = 1 to SymTab[p][S_NUM_ARGS] do*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31137 = (int)*(((s1_ptr)_2)->base + _p_60961);
    _2 = (int)SEQ_PTR(_31137);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _31138 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _31138 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    _31137 = NOVALUE;
    {
        int _i_61475;
        _i_61475 = 1;
L39: 
        if (binary_op_a(GREATER, _i_61475, _31138)){
            goto L3A; // [2141] 2183
        }

        /** 				emit_op(DISPLAY_VAR)*/
        _43emit_op(87);

        /** 				emit_addr(sym)*/
        _43emit_addr(_sym_60963);

        /** 				sym = SymTab[sym][S_NEXT]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _31139 = (int)*(((s1_ptr)_2)->base + _sym_60963);
        _2 = (int)SEQ_PTR(_31139);
        _sym_60963 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_sym_60963)){
            _sym_60963 = (long)DBL_PTR(_sym_60963)->dbl;
        }
        _31139 = NOVALUE;

        /** 			end for*/
        _0 = _i_61475;
        if (IS_ATOM_INT(_i_61475)) {
            _i_61475 = _i_61475 + 1;
            if ((long)((unsigned long)_i_61475 +(unsigned long) HIGH_BITS) >= 0){
                _i_61475 = NewDouble((double)_i_61475);
            }
        }
        else {
            _i_61475 = binary_op_a(PLUS, _i_61475, 1);
        }
        DeRef(_0);
        goto L39; // [2178] 2148
L3A: 
        ;
        DeRef(_i_61475);
    }

    /** 			emit_op(UPDATE_GLOBALS)*/
    _43emit_op(89);
L38: 
L37: 

    /** 	putback(tok)*/
    Ref(_tok_60965);
    _41putback(_tok_60965);

    /** 	FuncReturn = FALSE*/
    _41FuncReturn_55130 = _9FALSE_426;

    /** 	if type_enum then*/
    if (_type_enum_60969 == 0)
    {
        goto L3B; // [2208] 2448
    }
    else{
    }

    /** 		stmt_nest += 1*/
    _41stmt_nest_55154 = _41stmt_nest_55154 + 1;

    /** 		tok_match(RETURN)*/
    _41tok_match(413, 0);

    /** 		emit_opnd(i1_sym[T_SYM])*/
    _2 = (int)SEQ_PTR(_i1_sym_60970);
    _31142 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_31142);
    _43emit_opnd(_31142);
    _31142 = NOVALUE;

    /** 		emit_opnd(enum_syms[1])*/
    _2 = (int)SEQ_PTR(_enum_syms_60971);
    _31143 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_31143);
    _43emit_opnd(_31143);
    _31143 = NOVALUE;

    /** 		emit_op(EQUAL)*/
    _43emit_op(153);

    /** 		SymTab[Top()][S_USAGE] = U_USED*/
    _31144 = _43Top();
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_31144))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_31144)->dbl));
    else
    _3 = (int)(_31144 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 3;
    DeRef(_1);
    _31145 = NOVALUE;

    /** 		for ei = 2 to length(enum_syms) do*/
    if (IS_SEQUENCE(_enum_syms_60971)){
            _31147 = SEQ_PTR(_enum_syms_60971)->length;
    }
    else {
        _31147 = 1;
    }
    {
        int _ei_61508;
        _ei_61508 = 2;
L3C: 
        if (_ei_61508 > _31147){
            goto L3D; // [2281] 2391
        }

        /** 			symtab_index last_comparison = Top()*/
        _last_comparison_61511 = _43Top();
        if (!IS_ATOM_INT(_last_comparison_61511)) {
            _1 = (long)(DBL_PTR(_last_comparison_61511)->dbl);
            if (UNIQUE(DBL_PTR(_last_comparison_61511)) && (DBL_PTR(_last_comparison_61511)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_last_comparison_61511);
            _last_comparison_61511 = _1;
        }

        /** 			emit_opnd(i1_sym[T_SYM])*/
        _2 = (int)SEQ_PTR(_i1_sym_60970);
        _31149 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_31149);
        _43emit_opnd(_31149);
        _31149 = NOVALUE;

        /** 			emit_opnd(enum_syms[ei])*/
        _2 = (int)SEQ_PTR(_enum_syms_60971);
        _31150 = (int)*(((s1_ptr)_2)->base + _ei_61508);
        Ref(_31150);
        _43emit_opnd(_31150);
        _31150 = NOVALUE;

        /** 			emit_op(EQUAL)*/
        _43emit_op(153);

        /** 			SymTab[Top()][S_USAGE] = U_USED*/
        _31151 = _43Top();
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_31151))
        _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_31151)->dbl));
        else
        _3 = (int)(_31151 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = 3;
        DeRef(_1);
        _31152 = NOVALUE;

        /** 			emit_opnd(Top())*/
        _31154 = _43Top();
        _43emit_opnd(_31154);
        _31154 = NOVALUE;

        /** 			emit_opnd(last_comparison)*/
        _43emit_opnd(_last_comparison_61511);

        /** 			emit_op(OR)*/
        _43emit_op(9);

        /** 			SymTab[Top()][S_USAGE] = U_USED*/
        _31155 = _43Top();
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_31155))
        _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_31155)->dbl));
        else
        _3 = (int)(_31155 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = 3;
        DeRef(_1);
        _31156 = NOVALUE;

        /** 		end for*/
        _ei_61508 = _ei_61508 + 1;
        goto L3C; // [2386] 2288
L3D: 
        ;
    }

    /** 		emit_opnd(Top())*/
    _31158 = _43Top();
    _43emit_opnd(_31158);
    _31158 = NOVALUE;

    /** 		emit_op(RETURNF)*/
    _43emit_op(28);

    /** 		flush_temps()*/
    RefDS(_22663);
    _43flush_temps(_22663);

    /** 		stmt_nest -= 1*/
    _41stmt_nest_55154 = _41stmt_nest_55154 - 1;

    /** 		InitDelete()*/
    _41InitDelete();

    /** 		flush_temps()*/
    RefDS(_22663);
    _43flush_temps(_22663);

    /** 		FuncReturn = TRUE*/
    _41FuncReturn_55130 = _9TRUE_428;

    /** 		tok_match(END)*/
    _41tok_match(402, 0);
    goto L3E; // [2445] 2461
L3B: 

    /** 		Statement_list()*/
    _41Statement_list();

    /** 		tok_match(END)*/
    _41tok_match(402, 0);
L3E: 

    /** 	tok_match(prog_type, END)*/
    _41tok_match(_prog_type_60956, 402);

    /** 	if prog_type != PROCEDURE then*/
    if (_prog_type_60956 == 405)
    goto L3F; // [2473] 2521

    /** 		if not FuncReturn then*/
    if (_41FuncReturn_55130 != 0)
    goto L40; // [2481] 2511

    /** 			if prog_type = FUNCTION then*/
    if (_prog_type_60956 != 406)
    goto L41; // [2488] 2502

    /** 				CompileErr(120)*/
    RefDS(_22663);
    _46CompileErr(120, _22663, 0);
    goto L42; // [2499] 2510
L41: 

    /** 				CompileErr(149)*/
    RefDS(_22663);
    _46CompileErr(149, _22663, 0);
L42: 
L40: 

    /** 		emit_op(BADRETURNF) -- function/type shouldn't reach here*/
    _43emit_op(43);
    goto L43; // [2518] 2583
L3F: 

    /** 		StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 		if not TRANSLATE then*/
    if (_38TRANSLATE_16564 != 0)
    goto L44; // [2536] 2560

    /** 			if OpTrace then*/
    if (_38OpTrace_17015 == 0)
    {
        goto L45; // [2543] 2559
    }
    else{
    }

    /** 				emit_op(ERASE_PRIVATE_NAMES)*/
    _43emit_op(88);

    /** 				emit_addr(p)*/
    _43emit_addr(_p_60961);
L45: 
L44: 

    /** 		emit_op(RETURNP)*/
    _43emit_op(29);

    /** 		if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L46; // [2571] 2582
    }
    else{
    }

    /** 			emit_op(BADRETURNF) -- just to mark end of procedure*/
    _43emit_op(43);
L46: 
L43: 

    /** 	Drop_block( pt )*/
    _68Drop_block(_pt_60959);

    /** 	if Strict_Override > 0 then*/
    if (_38Strict_Override_17012 <= 0)
    goto L47; // [2592] 2607

    /** 		Strict_Override -= 1	-- Reset at the end of each routine.*/
    _38Strict_Override_17012 = _38Strict_Override_17012 - 1;
L47: 

    /** 	SymTab[p][S_STACK_SPACE] += temps_allocated + param_num*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_60961 + ((s1_ptr)_2)->base);
    _31169 = _55temps_allocated_47575 + _41param_num_55131;
    if ((long)((unsigned long)_31169 + (unsigned long)HIGH_BITS) >= 0) 
    _31169 = NewDouble((double)_31169);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!IS_ATOM_INT(_38S_STACK_SPACE_16658)){
        _31170 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_STACK_SPACE_16658)->dbl));
    }
    else{
        _31170 = (int)*(((s1_ptr)_2)->base + _38S_STACK_SPACE_16658);
    }
    _31167 = NOVALUE;
    if (IS_ATOM_INT(_31170) && IS_ATOM_INT(_31169)) {
        _31171 = _31170 + _31169;
        if ((long)((unsigned long)_31171 + (unsigned long)HIGH_BITS) >= 0) 
        _31171 = NewDouble((double)_31171);
    }
    else {
        _31171 = binary_op(PLUS, _31170, _31169);
    }
    _31170 = NOVALUE;
    DeRef(_31169);
    _31169 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_STACK_SPACE_16658))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_STACK_SPACE_16658)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_STACK_SPACE_16658);
    _1 = *(int *)_2;
    *(int *)_2 = _31171;
    if( _1 != _31171 ){
        DeRef(_1);
    }
    _31171 = NOVALUE;
    _31167 = NOVALUE;

    /** 	if temps_allocated + param_num > max_stack_per_call then*/
    _31172 = _55temps_allocated_47575 + _41param_num_55131;
    if ((long)((unsigned long)_31172 + (unsigned long)HIGH_BITS) >= 0) 
    _31172 = NewDouble((double)_31172);
    if (binary_op_a(LESSEQ, _31172, _38max_stack_per_call_17063)){
        DeRef(_31172);
        _31172 = NOVALUE;
        goto L48; // [2650] 2667
    }
    DeRef(_31172);
    _31172 = NOVALUE;

    /** 		max_stack_per_call = temps_allocated + param_num*/
    _38max_stack_per_call_17063 = _55temps_allocated_47575 + _41param_num_55131;
L48: 

    /** 	param_num = -1*/
    _41param_num_55131 = -1;

    /** 	StraightenBranches()*/
    _41StraightenBranches();

    /** 	check_inline( p )*/
    _69check_inline(_p_60961);

    /** 	param_num = -1*/
    _41param_num_55131 = -1;

    /** 	EnterTopLevel()*/
    _41EnterTopLevel(1);

    /** 	if length( enum_syms ) then*/
    if (IS_SEQUENCE(_enum_syms_60971)){
            _31175 = SEQ_PTR(_enum_syms_60971)->length;
    }
    else {
        _31175 = 1;
    }
    if (_31175 == 0)
    {
        _31175 = NOVALUE;
        goto L49; // [2696] 2783
    }
    else{
        _31175 = NOVALUE;
    }

    /** 		SymTab[p][S_NEXT] = SymTab[enum_syms[$]][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_60961 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_enum_syms_60971)){
            _31178 = SEQ_PTR(_enum_syms_60971)->length;
    }
    else {
        _31178 = 1;
    }
    _2 = (int)SEQ_PTR(_enum_syms_60971);
    _31179 = (int)*(((s1_ptr)_2)->base + _31178);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_31179)){
        _31180 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31179)->dbl));
    }
    else{
        _31180 = (int)*(((s1_ptr)_2)->base + _31179);
    }
    _2 = (int)SEQ_PTR(_31180);
    _31181 = (int)*(((s1_ptr)_2)->base + 2);
    _31180 = NOVALUE;
    Ref(_31181);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _31181;
    if( _1 != _31181 ){
        DeRef(_1);
    }
    _31181 = NOVALUE;
    _31176 = NOVALUE;

    /** 		SymTab[last_sym][S_NEXT] = enum_syms[1]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_55last_sym_47064 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_enum_syms_60971);
    _31184 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_31184);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _31184;
    if( _1 != _31184 ){
        DeRef(_1);
    }
    _31184 = NOVALUE;
    _31182 = NOVALUE;

    /** 		last_sym = enum_syms[$]*/
    if (IS_SEQUENCE(_enum_syms_60971)){
            _31185 = SEQ_PTR(_enum_syms_60971)->length;
    }
    else {
        _31185 = 1;
    }
    _2 = (int)SEQ_PTR(_enum_syms_60971);
    _55last_sym_47064 = (int)*(((s1_ptr)_2)->base + _31185);
    if (!IS_ATOM_INT(_55last_sym_47064)){
        _55last_sym_47064 = (long)DBL_PTR(_55last_sym_47064)->dbl;
    }

    /** 		SymTab[last_sym][S_NEXT] = 0*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_55last_sym_47064 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _31187 = NOVALUE;
L49: 

    /** end procedure*/
    DeRef(_tok_60965);
    DeRef(_prog_name_60966);
    DeRef(_i1_sym_60970);
    DeRef(_enum_syms_60971);
    DeRef(_middle_def_args_61192);
    DeRef(_31013);
    _31013 = NOVALUE;
    DeRef(_31024);
    _31024 = NOVALUE;
    _30982 = NOVALUE;
    _31049 = NOVALUE;
    DeRef(_31144);
    _31144 = NOVALUE;
    DeRef(_31112);
    _31112 = NOVALUE;
    DeRef(_31151);
    _31151 = NOVALUE;
    _30986 = NOVALUE;
    _31048 = NOVALUE;
    _31067 = NOVALUE;
    _31072 = NOVALUE;
    _31052 = NOVALUE;
    _31138 = NOVALUE;
    DeRef(_30960);
    _30960 = NOVALUE;
    _31030 = NOVALUE;
    DeRef(_31064);
    _31064 = NOVALUE;
    _31118 = NOVALUE;
    DeRef(_31018);
    _31018 = NOVALUE;
    DeRef(_31128);
    _31128 = NOVALUE;
    DeRef(_31046);
    _31046 = NOVALUE;
    _31179 = NOVALUE;
    DeRef(_31155);
    _31155 = NOVALUE;
    return;
    ;
}


void _41InitGlobals()
{
    int _31206 = NOVALUE;
    int _31204 = NOVALUE;
    int _31203 = NOVALUE;
    int _31202 = NOVALUE;
    int _31201 = NOVALUE;
    int _31200 = NOVALUE;
    int _31199 = NOVALUE;
    int _31197 = NOVALUE;
    int _31196 = NOVALUE;
    int _31195 = NOVALUE;
    int _31194 = NOVALUE;
    int _31192 = NOVALUE;
    int _31191 = NOVALUE;
    int _31190 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ResetTP()*/
    _62ResetTP();

    /** 	OpTypeCheck = TRUE*/
    _38OpTypeCheck_17016 = _9TRUE_428;

    /** 	OpDefines &= {*/
    _31190 = _26version_major();
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _31190;
    _31191 = MAKE_SEQ(_1);
    _31190 = NOVALUE;
    _31192 = EPrintf(-9999999, _31189, _31191);
    DeRefDS(_31191);
    _31191 = NOVALUE;
    _31194 = _26version_major();
    _31195 = _26version_minor();
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _31194;
    ((int *)_2)[2] = _31195;
    _31196 = MAKE_SEQ(_1);
    _31195 = NOVALUE;
    _31194 = NOVALUE;
    _31197 = EPrintf(-9999999, _31193, _31196);
    DeRefDS(_31196);
    _31196 = NOVALUE;
    _31199 = _26version_major();
    _31200 = _26version_minor();
    _31201 = _26version_patch();
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _31199;
    *((int *)(_2+8)) = _31200;
    *((int *)(_2+12)) = _31201;
    _31202 = MAKE_SEQ(_1);
    _31201 = NOVALUE;
    _31200 = NOVALUE;
    _31199 = NOVALUE;
    _31203 = EPrintf(-9999999, _31198, _31202);
    DeRefDS(_31202);
    _31202 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _31192;
    *((int *)(_2+8)) = _31197;
    *((int *)(_2+12)) = _31203;
    _31204 = MAKE_SEQ(_1);
    _31203 = NOVALUE;
    _31197 = NOVALUE;
    _31192 = NOVALUE;
    Concat((object_ptr)&_38OpDefines_17019, _38OpDefines_17019, _31204);
    DeRefDS(_31204);
    _31204 = NOVALUE;

    /** 	OpDefines &= GetPlatformDefines()*/
    _31206 = _42GetPlatformDefines(0);
    if (IS_SEQUENCE(_38OpDefines_17019) && IS_ATOM(_31206)) {
        Ref(_31206);
        Append(&_38OpDefines_17019, _38OpDefines_17019, _31206);
    }
    else if (IS_ATOM(_38OpDefines_17019) && IS_SEQUENCE(_31206)) {
    }
    else {
        Concat((object_ptr)&_38OpDefines_17019, _38OpDefines_17019, _31206);
    }
    DeRef(_31206);
    _31206 = NOVALUE;

    /** 	OpInline = DEFAULT_INLINE*/
    _38OpInline_17023 = 30;

    /** 	OpIndirectInclude = 1*/
    _38OpIndirectInclude_17024 = 1;

    /** end procedure*/
    return;
    ;
}


void _41not_supported_compile(int _feature_61672)
{
    int _31208 = NOVALUE;
    int _0, _1, _2;
    

    /** 	CompileErr(5, {feature, version_name})*/
    RefDS(_38version_name_16578);
    RefDS(_feature_61672);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _feature_61672;
    ((int *)_2)[2] = _38version_name_16578;
    _31208 = MAKE_SEQ(_1);
    _46CompileErr(5, _31208, 0);
    _31208 = NOVALUE;

    /** end procedure*/
    DeRefDSi(_feature_61672);
    return;
    ;
}


void _41SetWith(int _on_off_61678)
{
    int _option_61679 = NOVALUE;
    int _idx_61680 = NOVALUE;
    int _reset_flags_61681 = NOVALUE;
    int _tok_61733 = NOVALUE;
    int _good_sofar_61781 = NOVALUE;
    int _tok_61784 = NOVALUE;
    int _warning_extra_61786 = NOVALUE;
    int _endlist_61877 = NOVALUE;
    int _tok_62021 = NOVALUE;
    int _31355 = NOVALUE;
    int _31353 = NOVALUE;
    int _31352 = NOVALUE;
    int _31351 = NOVALUE;
    int _31350 = NOVALUE;
    int _31347 = NOVALUE;
    int _31346 = NOVALUE;
    int _31345 = NOVALUE;
    int _31343 = NOVALUE;
    int _31341 = NOVALUE;
    int _31338 = NOVALUE;
    int _31336 = NOVALUE;
    int _31335 = NOVALUE;
    int _31334 = NOVALUE;
    int _31333 = NOVALUE;
    int _31332 = NOVALUE;
    int _31328 = NOVALUE;
    int _31326 = NOVALUE;
    int _31324 = NOVALUE;
    int _31320 = NOVALUE;
    int _31315 = NOVALUE;
    int _31314 = NOVALUE;
    int _31308 = NOVALUE;
    int _31307 = NOVALUE;
    int _31306 = NOVALUE;
    int _31305 = NOVALUE;
    int _31304 = NOVALUE;
    int _31303 = NOVALUE;
    int _31302 = NOVALUE;
    int _31301 = NOVALUE;
    int _31300 = NOVALUE;
    int _31299 = NOVALUE;
    int _31297 = NOVALUE;
    int _31296 = NOVALUE;
    int _31294 = NOVALUE;
    int _31293 = NOVALUE;
    int _31292 = NOVALUE;
    int _31290 = NOVALUE;
    int _31289 = NOVALUE;
    int _31287 = NOVALUE;
    int _31284 = NOVALUE;
    int _31282 = NOVALUE;
    int _31279 = NOVALUE;
    int _31278 = NOVALUE;
    int _31277 = NOVALUE;
    int _31276 = NOVALUE;
    int _31269 = NOVALUE;
    int _31268 = NOVALUE;
    int _31266 = NOVALUE;
    int _31263 = NOVALUE;
    int _31262 = NOVALUE;
    int _31260 = NOVALUE;
    int _31259 = NOVALUE;
    int _31258 = NOVALUE;
    int _31257 = NOVALUE;
    int _31256 = NOVALUE;
    int _31255 = NOVALUE;
    int _31252 = NOVALUE;
    int _31251 = NOVALUE;
    int _31250 = NOVALUE;
    int _31249 = NOVALUE;
    int _31248 = NOVALUE;
    int _31247 = NOVALUE;
    int _31244 = NOVALUE;
    int _31243 = NOVALUE;
    int _31242 = NOVALUE;
    int _31240 = NOVALUE;
    int _31237 = NOVALUE;
    int _31235 = NOVALUE;
    int _31234 = NOVALUE;
    int _31232 = NOVALUE;
    int _31231 = NOVALUE;
    int _31230 = NOVALUE;
    int _31229 = NOVALUE;
    int _31228 = NOVALUE;
    int _31227 = NOVALUE;
    int _31225 = NOVALUE;
    int _31222 = NOVALUE;
    int _31221 = NOVALUE;
    int _31220 = NOVALUE;
    int _31219 = NOVALUE;
    int _31217 = NOVALUE;
    int _31216 = NOVALUE;
    int _31215 = NOVALUE;
    int _31214 = NOVALUE;
    int _31212 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer reset_flags = 1*/
    _reset_flags_61681 = 1;

    /** 	option = StringToken("&+=")*/
    RefDS(_31209);
    _0 = _option_61679;
    _option_61679 = _62StringToken(_31209);
    DeRef(_0);

    /** 	if equal(option, "type_check") then*/
    if (_option_61679 == _31211)
    _31212 = 1;
    else if (IS_ATOM_INT(_option_61679) && IS_ATOM_INT(_31211))
    _31212 = 0;
    else
    _31212 = (compare(_option_61679, _31211) == 0);
    if (_31212 == 0)
    {
        _31212 = NOVALUE;
        goto L1; // [22] 35
    }
    else{
        _31212 = NOVALUE;
    }

    /** 		OpTypeCheck = on_off*/
    _38OpTypeCheck_17016 = _on_off_61678;
    goto L2; // [32] 1521
L1: 

    /** 	elsif equal(option, "profile") then*/
    if (_option_61679 == _31213)
    _31214 = 1;
    else if (IS_ATOM_INT(_option_61679) && IS_ATOM_INT(_31213))
    _31214 = 0;
    else
    _31214 = (compare(_option_61679, _31213) == 0);
    if (_31214 == 0)
    {
        _31214 = NOVALUE;
        goto L3; // [41] 121
    }
    else{
        _31214 = NOVALUE;
    }

    /** 		if not TRANSLATE and not BIND then*/
    _31215 = (_38TRANSLATE_16564 == 0);
    if (_31215 == 0) {
        goto L2; // [51] 1521
    }
    _31217 = (_38BIND_16567 == 0);
    if (_31217 == 0)
    {
        DeRef(_31217);
        _31217 = NOVALUE;
        goto L2; // [61] 1521
    }
    else{
        DeRef(_31217);
        _31217 = NOVALUE;
    }

    /** 			OpProfileStatement = on_off*/
    _38OpProfileStatement_17017 = _on_off_61678;

    /** 			if OpProfileStatement then*/
    if (_38OpProfileStatement_17017 == 0)
    {
        goto L2; // [75] 1521
    }
    else{
    }

    /** 				if AnyTimeProfile then*/
    if (_35AnyTimeProfile_15617 == 0)
    {
        goto L4; // [82] 106
    }
    else{
    }

    /** 					Warning(224, mixed_profile_warning_flag)*/
    RefDS(_22663);
    _46Warning(224, 1024, _22663);

    /** 					OpProfileStatement = FALSE*/
    _38OpProfileStatement_17017 = _9FALSE_426;
    goto L2; // [103] 1521
L4: 

    /** 					AnyStatementProfile = TRUE*/
    _35AnyStatementProfile_15618 = _9TRUE_428;
    goto L2; // [118] 1521
L3: 

    /** 	elsif equal(option, "profile_time") then*/
    if (_option_61679 == _31218)
    _31219 = 1;
    else if (IS_ATOM_INT(_option_61679) && IS_ATOM_INT(_31218))
    _31219 = 0;
    else
    _31219 = (compare(_option_61679, _31218) == 0);
    if (_31219 == 0)
    {
        _31219 = NOVALUE;
        goto L5; // [127] 361
    }
    else{
        _31219 = NOVALUE;
    }

    /** 		if not TRANSLATE and not BIND then*/
    _31220 = (_38TRANSLATE_16564 == 0);
    if (_31220 == 0) {
        goto L2; // [137] 1521
    }
    _31222 = (_38BIND_16567 == 0);
    if (_31222 == 0)
    {
        DeRef(_31222);
        _31222 = NOVALUE;
        goto L2; // [147] 1521
    }
    else{
        DeRef(_31222);
        _31222 = NOVALUE;
    }

    /** 			if not IWINDOWS then*/
    if (_42IWINDOWS_17109 != 0)
    goto L6; // [154] 169

    /** 				if on_off then*/
    if (_on_off_61678 == 0)
    {
        goto L7; // [159] 168
    }
    else{
    }

    /** 					not_supported_compile("profile_time")*/
    RefDS(_31218);
    _41not_supported_compile(_31218);
L7: 
L6: 

    /** 			OpProfileTime = on_off*/
    _38OpProfileTime_17018 = _on_off_61678;

    /** 			if OpProfileTime then*/
    if (_38OpProfileTime_17018 == 0)
    {
        goto L8; // [180] 355
    }
    else{
    }

    /** 				if AnyStatementProfile then*/
    if (_35AnyStatementProfile_15618 == 0)
    {
        goto L9; // [187] 209
    }
    else{
    }

    /** 					Warning(224,mixed_profile_warning_flag)*/
    RefDS(_22663);
    _46Warning(224, 1024, _22663);

    /** 					OpProfileTime = FALSE*/
    _38OpProfileTime_17018 = _9FALSE_426;
L9: 

    /** 				token tok = next_token()*/
    _0 = _tok_61733;
    _tok_61733 = _41next_token();
    DeRef(_0);

    /** 				if tok[T_ID] = ATOM then*/
    _2 = (int)SEQ_PTR(_tok_61733);
    _31225 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31225, 502)){
        _31225 = NOVALUE;
        goto LA; // [224] 316
    }
    _31225 = NOVALUE;

    /** 					if integer(SymTab[tok[T_SYM]][S_OBJ]) then*/
    _2 = (int)SEQ_PTR(_tok_61733);
    _31227 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_31227)){
        _31228 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31227)->dbl));
    }
    else{
        _31228 = (int)*(((s1_ptr)_2)->base + _31227);
    }
    _2 = (int)SEQ_PTR(_31228);
    _31229 = (int)*(((s1_ptr)_2)->base + 1);
    _31228 = NOVALUE;
    if (IS_ATOM_INT(_31229))
    _31230 = 1;
    else if (IS_ATOM_DBL(_31229))
    _31230 = IS_ATOM_INT(DoubleToInt(_31229));
    else
    _31230 = 0;
    _31229 = NOVALUE;
    if (_31230 == 0)
    {
        _31230 = NOVALUE;
        goto LB; // [251] 279
    }
    else{
        _31230 = NOVALUE;
    }

    /** 						sample_size = SymTab[tok[T_SYM]][S_OBJ]*/
    _2 = (int)SEQ_PTR(_tok_61733);
    _31231 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_31231)){
        _31232 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31231)->dbl));
    }
    else{
        _31232 = (int)*(((s1_ptr)_2)->base + _31231);
    }
    _2 = (int)SEQ_PTR(_31232);
    _38sample_size_17064 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_38sample_size_17064)){
        _38sample_size_17064 = (long)DBL_PTR(_38sample_size_17064)->dbl;
    }
    _31232 = NOVALUE;
    goto LC; // [276] 287
LB: 

    /** 						sample_size = -1*/
    _38sample_size_17064 = -1;
LC: 

    /** 					if sample_size < 1 and OpProfileTime then*/
    _31234 = (_38sample_size_17064 < 1);
    if (_31234 == 0) {
        goto LD; // [295] 329
    }
    if (_38OpProfileTime_17018 == 0)
    {
        goto LD; // [302] 329
    }
    else{
    }

    /** 						CompileErr(136)*/
    RefDS(_22663);
    _46CompileErr(136, _22663, 0);
    goto LD; // [313] 329
LA: 

    /** 					putback(tok)*/
    Ref(_tok_61733);
    _41putback(_tok_61733);

    /** 					sample_size = DEFAULT_SAMPLE_SIZE*/
    _38sample_size_17064 = 25000;
LD: 

    /** 				if OpProfileTime then*/
    if (_38OpProfileTime_17018 == 0)
    {
        goto LE; // [333] 354
    }
    else{
    }

    /** 					if IWINDOWS then*/
    if (_42IWINDOWS_17109 == 0)
    {
        goto LF; // [340] 353
    }
    else{
    }

    /** 						AnyTimeProfile = TRUE*/
    _35AnyTimeProfile_15617 = _9TRUE_428;
LF: 
LE: 
L8: 
    DeRef(_tok_61733);
    _tok_61733 = NOVALUE;
    goto L2; // [358] 1521
L5: 

    /** 	elsif equal(option, "trace") then*/
    if (_option_61679 == _31236)
    _31237 = 1;
    else if (IS_ATOM_INT(_option_61679) && IS_ATOM_INT(_31236))
    _31237 = 0;
    else
    _31237 = (compare(_option_61679, _31236) == 0);
    if (_31237 == 0)
    {
        _31237 = NOVALUE;
        goto L10; // [367] 388
    }
    else{
        _31237 = NOVALUE;
    }

    /** 		if not BIND then*/
    if (_38BIND_16567 != 0)
    goto L2; // [374] 1521

    /** 			OpTrace = on_off*/
    _38OpTrace_17015 = _on_off_61678;
    goto L2; // [385] 1521
L10: 

    /** 	elsif equal(option, "warning") then*/
    if (_option_61679 == _31239)
    _31240 = 1;
    else if (IS_ATOM_INT(_option_61679) && IS_ATOM_INT(_31239))
    _31240 = 0;
    else
    _31240 = (compare(_option_61679, _31239) == 0);
    if (_31240 == 0)
    {
        _31240 = NOVALUE;
        goto L11; // [394] 1234
    }
    else{
        _31240 = NOVALUE;
    }

    /** 		integer good_sofar = line_number*/
    _good_sofar_61781 = _38line_number_16947;

    /** 		reset_flags = 1*/
    _reset_flags_61681 = 1;

    /** 		token tok = next_token()*/
    _0 = _tok_61784;
    _tok_61784 = _41next_token();
    DeRef(_0);

    /** 		integer warning_extra = 1*/
    _warning_extra_61786 = 1;

    /** 		if find(tok[T_ID], {CONCAT_EQUALS, PLUS_EQUALS}) != 0 then*/
    _2 = (int)SEQ_PTR(_tok_61784);
    _31242 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _39CONCAT_EQUALS_16428;
    ((int *)_2)[2] = 515;
    _31243 = MAKE_SEQ(_1);
    _31244 = find_from(_31242, _31243, 1);
    _31242 = NOVALUE;
    DeRefDS(_31243);
    _31243 = NOVALUE;
    if (_31244 == 0)
    goto L12; // [442] 501

    /** 			tok = next_token()*/
    _0 = _tok_61784;
    _tok_61784 = _41next_token();
    DeRef(_0);

    /** 			if tok[T_ID] != LEFT_BRACE and tok[T_ID] != LEFT_ROUND then*/
    _2 = (int)SEQ_PTR(_tok_61784);
    _31247 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31247)) {
        _31248 = (_31247 != -24);
    }
    else {
        _31248 = binary_op(NOTEQ, _31247, -24);
    }
    _31247 = NOVALUE;
    if (IS_ATOM_INT(_31248)) {
        if (_31248 == 0) {
            goto L13; // [465] 493
        }
    }
    else {
        if (DBL_PTR(_31248)->dbl == 0.0) {
            goto L13; // [465] 493
        }
    }
    _2 = (int)SEQ_PTR(_tok_61784);
    _31250 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31250)) {
        _31251 = (_31250 != -26);
    }
    else {
        _31251 = binary_op(NOTEQ, _31250, -26);
    }
    _31250 = NOVALUE;
    if (_31251 == 0) {
        DeRef(_31251);
        _31251 = NOVALUE;
        goto L13; // [482] 493
    }
    else {
        if (!IS_ATOM_INT(_31251) && DBL_PTR(_31251)->dbl == 0.0){
            DeRef(_31251);
            _31251 = NOVALUE;
            goto L13; // [482] 493
        }
        DeRef(_31251);
        _31251 = NOVALUE;
    }
    DeRef(_31251);
    _31251 = NOVALUE;

    /** 				CompileErr(160)*/
    RefDS(_22663);
    _46CompileErr(160, _22663, 0);
L13: 

    /** 			reset_flags = 0*/
    _reset_flags_61681 = 0;
    goto L14; // [498] 727
L12: 

    /** 		elsif tok[T_ID] = EQUALS then*/
    _2 = (int)SEQ_PTR(_tok_61784);
    _31252 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31252, 3)){
        _31252 = NOVALUE;
        goto L15; // [511] 570
    }
    _31252 = NOVALUE;

    /** 			tok = next_token()*/
    _0 = _tok_61784;
    _tok_61784 = _41next_token();
    DeRef(_0);

    /** 			if tok[T_ID] != LEFT_BRACE and tok[T_ID] != LEFT_ROUND then*/
    _2 = (int)SEQ_PTR(_tok_61784);
    _31255 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31255)) {
        _31256 = (_31255 != -24);
    }
    else {
        _31256 = binary_op(NOTEQ, _31255, -24);
    }
    _31255 = NOVALUE;
    if (IS_ATOM_INT(_31256)) {
        if (_31256 == 0) {
            goto L16; // [534] 562
        }
    }
    else {
        if (DBL_PTR(_31256)->dbl == 0.0) {
            goto L16; // [534] 562
        }
    }
    _2 = (int)SEQ_PTR(_tok_61784);
    _31258 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31258)) {
        _31259 = (_31258 != -26);
    }
    else {
        _31259 = binary_op(NOTEQ, _31258, -26);
    }
    _31258 = NOVALUE;
    if (_31259 == 0) {
        DeRef(_31259);
        _31259 = NOVALUE;
        goto L16; // [551] 562
    }
    else {
        if (!IS_ATOM_INT(_31259) && DBL_PTR(_31259)->dbl == 0.0){
            DeRef(_31259);
            _31259 = NOVALUE;
            goto L16; // [551] 562
        }
        DeRef(_31259);
        _31259 = NOVALUE;
    }
    DeRef(_31259);
    _31259 = NOVALUE;

    /** 				CompileErr(160)*/
    RefDS(_22663);
    _46CompileErr(160, _22663, 0);
L16: 

    /** 			reset_flags = 1*/
    _reset_flags_61681 = 1;
    goto L14; // [567] 727
L15: 

    /** 		elsif tok[T_ID] = VARIABLE then*/
    _2 = (int)SEQ_PTR(_tok_61784);
    _31260 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31260, -100)){
        _31260 = NOVALUE;
        goto L17; // [580] 726
    }
    _31260 = NOVALUE;

    /** 			option = SymTab[tok[T_SYM]][S_NAME]*/
    _2 = (int)SEQ_PTR(_tok_61784);
    _31262 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_31262)){
        _31263 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31262)->dbl));
    }
    else{
        _31263 = (int)*(((s1_ptr)_2)->base + _31262);
    }
    DeRef(_option_61679);
    _2 = (int)SEQ_PTR(_31263);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _option_61679 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _option_61679 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    Ref(_option_61679);
    _31263 = NOVALUE;

    /** 			if equal(option, "save") then*/
    if (_option_61679 == _31265)
    _31266 = 1;
    else if (IS_ATOM_INT(_option_61679) && IS_ATOM_INT(_31265))
    _31266 = 0;
    else
    _31266 = (compare(_option_61679, _31265) == 0);
    if (_31266 == 0)
    {
        _31266 = NOVALUE;
        goto L18; // [612] 636
    }
    else{
        _31266 = NOVALUE;
    }

    /** 				prev_OpWarning = OpWarning*/
    _38prev_OpWarning_17014 = _38OpWarning_17013;

    /** 				warning_extra = FALSE*/
    _warning_extra_61786 = _9FALSE_426;
    goto L19; // [633] 725
L18: 

    /** 			elsif equal(option, "restore") then*/
    if (_option_61679 == _31267)
    _31268 = 1;
    else if (IS_ATOM_INT(_option_61679) && IS_ATOM_INT(_31267))
    _31268 = 0;
    else
    _31268 = (compare(_option_61679, _31267) == 0);
    if (_31268 == 0)
    {
        _31268 = NOVALUE;
        goto L1A; // [642] 666
    }
    else{
        _31268 = NOVALUE;
    }

    /** 				OpWarning = prev_OpWarning*/
    _38OpWarning_17013 = _38prev_OpWarning_17014;

    /** 				warning_extra = FALSE*/
    _warning_extra_61786 = _9FALSE_426;
    goto L19; // [663] 725
L1A: 

    /** 			elsif equal(option, "strict") then*/
    if (_option_61679 == _26244)
    _31269 = 1;
    else if (IS_ATOM_INT(_option_61679) && IS_ATOM_INT(_26244))
    _31269 = 0;
    else
    _31269 = (compare(_option_61679, _26244) == 0);
    if (_31269 == 0)
    {
        _31269 = NOVALUE;
        goto L1B; // [672] 724
    }
    else{
        _31269 = NOVALUE;
    }

    /** 				if on_off = 0 then*/
    if (_on_off_61678 != 0)
    goto L1C; // [677] 694

    /** 					Strict_Override += 1*/
    _38Strict_Override_17012 = _38Strict_Override_17012 + 1;
    goto L1D; // [691] 714
L1C: 

    /** 				elsif Strict_Override > 0 then*/
    if (_38Strict_Override_17012 <= 0)
    goto L1E; // [698] 713

    /** 					Strict_Override -= 1*/
    _38Strict_Override_17012 = _38Strict_Override_17012 - 1;
L1E: 
L1D: 

    /** 				warning_extra = FALSE*/
    _warning_extra_61786 = _9FALSE_426;
L1B: 
L19: 
L17: 
L14: 

    /** 		if warning_extra = TRUE then*/
    if (_warning_extra_61786 != _9TRUE_428)
    goto L1F; // [731] 1229

    /** 			if reset_flags then*/
    if (_reset_flags_61681 == 0)
    {
        goto L20; // [737] 769
    }
    else{
    }

    /** 				if on_off = 0 then*/
    if (_on_off_61678 != 0)
    goto L21; // [742] 758

    /** 					OpWarning = no_warning_flag*/
    _38OpWarning_17013 = 0;
    goto L22; // [755] 768
L21: 

    /** 					OpWarning = all_warning_flag*/
    _38OpWarning_17013 = 32767;
L22: 
L20: 

    /** 			if find(tok[T_ID], {LEFT_BRACE, LEFT_ROUND}) then*/
    _2 = (int)SEQ_PTR(_tok_61784);
    _31276 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -24;
    ((int *)_2)[2] = -26;
    _31277 = MAKE_SEQ(_1);
    _31278 = find_from(_31276, _31277, 1);
    _31276 = NOVALUE;
    DeRefDS(_31277);
    _31277 = NOVALUE;
    if (_31278 == 0)
    {
        _31278 = NOVALUE;
        goto L23; // [790] 1222
    }
    else{
        _31278 = NOVALUE;
    }

    /** 				integer endlist*/

    /** 				if tok[T_ID] = LEFT_BRACE then*/
    _2 = (int)SEQ_PTR(_tok_61784);
    _31279 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31279, -24)){
        _31279 = NOVALUE;
        goto L24; // [805] 821
    }
    _31279 = NOVALUE;

    /** 					endlist = RIGHT_BRACE*/
    _endlist_61877 = -25;
    goto L25; // [818] 831
L24: 

    /** 					endlist = RIGHT_ROUND*/
    _endlist_61877 = -27;
L25: 

    /** 				tok = next_token()*/
    _0 = _tok_61784;
    _tok_61784 = _41next_token();
    DeRef(_0);

    /** 				while tok[T_ID] != endlist do*/
L26: 
    _2 = (int)SEQ_PTR(_tok_61784);
    _31282 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _31282, _endlist_61877)){
        _31282 = NOVALUE;
        goto L27; // [849] 1217
    }
    _31282 = NOVALUE;

    /** 					if tok[T_ID] = COMMA then*/
    _2 = (int)SEQ_PTR(_tok_61784);
    _31284 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31284, -30)){
        _31284 = NOVALUE;
        goto L28; // [863] 877
    }
    _31284 = NOVALUE;

    /** 						tok = next_token()*/
    _0 = _tok_61784;
    _tok_61784 = _41next_token();
    DeRef(_0);

    /** 						continue*/
    goto L26; // [874] 841
L28: 

    /** 					if tok[T_ID] = STRING then*/
    _2 = (int)SEQ_PTR(_tok_61784);
    _31287 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31287, 503)){
        _31287 = NOVALUE;
        goto L29; // [887] 916
    }
    _31287 = NOVALUE;

    /** 						option = SymTab[tok[T_SYM]][S_OBJ]*/
    _2 = (int)SEQ_PTR(_tok_61784);
    _31289 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_31289)){
        _31290 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31289)->dbl));
    }
    else{
        _31290 = (int)*(((s1_ptr)_2)->base + _31289);
    }
    DeRef(_option_61679);
    _2 = (int)SEQ_PTR(_31290);
    _option_61679 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_option_61679);
    _31290 = NOVALUE;
    goto L2A; // [913] 1064
L29: 

    /** 					elsif length(SymTab[tok[T_SYM]]) >= S_NAME then*/
    _2 = (int)SEQ_PTR(_tok_61784);
    _31292 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_31292)){
        _31293 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31292)->dbl));
    }
    else{
        _31293 = (int)*(((s1_ptr)_2)->base + _31292);
    }
    if (IS_SEQUENCE(_31293)){
            _31294 = SEQ_PTR(_31293)->length;
    }
    else {
        _31294 = 1;
    }
    _31293 = NOVALUE;
    if (binary_op_a(LESS, _31294, _38S_NAME_16598)){
        _31294 = NOVALUE;
        goto L2B; // [935] 964
    }
    _31294 = NOVALUE;

    /** 						option = SymTab[tok[T_SYM]][S_NAME]*/
    _2 = (int)SEQ_PTR(_tok_61784);
    _31296 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_31296)){
        _31297 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31296)->dbl));
    }
    else{
        _31297 = (int)*(((s1_ptr)_2)->base + _31296);
    }
    DeRef(_option_61679);
    _2 = (int)SEQ_PTR(_31297);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _option_61679 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _option_61679 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    Ref(_option_61679);
    _31297 = NOVALUE;
    goto L2A; // [961] 1064
L2B: 

    /** 						option = ""*/
    RefDS(_22663);
    DeRef(_option_61679);
    _option_61679 = _22663;

    /** 						for k = 1 to length(keylist) do*/
    if (IS_SEQUENCE(_65keylist_23552)){
            _31299 = SEQ_PTR(_65keylist_23552)->length;
    }
    else {
        _31299 = 1;
    }
    {
        int _k_61924;
        _k_61924 = 1;
L2C: 
        if (_k_61924 > _31299){
            goto L2D; // [978] 1063
        }

        /** 							if keylist[k][S_SCOPE] = SC_KEYWORD and*/
        _2 = (int)SEQ_PTR(_65keylist_23552);
        _31300 = (int)*(((s1_ptr)_2)->base + _k_61924);
        _2 = (int)SEQ_PTR(_31300);
        _31301 = (int)*(((s1_ptr)_2)->base + 4);
        _31300 = NOVALUE;
        if (IS_ATOM_INT(_31301)) {
            _31302 = (_31301 == 8);
        }
        else {
            _31302 = binary_op(EQUALS, _31301, 8);
        }
        _31301 = NOVALUE;
        if (IS_ATOM_INT(_31302)) {
            if (_31302 == 0) {
                goto L2E; // [1005] 1056
            }
        }
        else {
            if (DBL_PTR(_31302)->dbl == 0.0) {
                goto L2E; // [1005] 1056
            }
        }
        _2 = (int)SEQ_PTR(_65keylist_23552);
        _31304 = (int)*(((s1_ptr)_2)->base + _k_61924);
        _2 = (int)SEQ_PTR(_31304);
        if (!IS_ATOM_INT(_38S_TOKEN_16603)){
            _31305 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
        }
        else{
            _31305 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
        }
        _31304 = NOVALUE;
        _2 = (int)SEQ_PTR(_tok_61784);
        _31306 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_31305) && IS_ATOM_INT(_31306)) {
            _31307 = (_31305 == _31306);
        }
        else {
            _31307 = binary_op(EQUALS, _31305, _31306);
        }
        _31305 = NOVALUE;
        _31306 = NOVALUE;
        if (_31307 == 0) {
            DeRef(_31307);
            _31307 = NOVALUE;
            goto L2E; // [1032] 1056
        }
        else {
            if (!IS_ATOM_INT(_31307) && DBL_PTR(_31307)->dbl == 0.0){
                DeRef(_31307);
                _31307 = NOVALUE;
                goto L2E; // [1032] 1056
            }
            DeRef(_31307);
            _31307 = NOVALUE;
        }
        DeRef(_31307);
        _31307 = NOVALUE;

        /** 									option = keylist[k][S_NAME]*/
        _2 = (int)SEQ_PTR(_65keylist_23552);
        _31308 = (int)*(((s1_ptr)_2)->base + _k_61924);
        DeRef(_option_61679);
        _2 = (int)SEQ_PTR(_31308);
        if (!IS_ATOM_INT(_38S_NAME_16598)){
            _option_61679 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
        }
        else{
            _option_61679 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
        }
        Ref(_option_61679);
        _31308 = NOVALUE;

        /** 									exit*/
        goto L2D; // [1053] 1063
L2E: 

        /** 						end for*/
        _k_61924 = _k_61924 + 1;
        goto L2C; // [1058] 985
L2D: 
        ;
    }
L2A: 

    /** 					idx = find(option, warning_names)*/
    _idx_61680 = find_from(_option_61679, _38warning_names_16990, 1);

    /** 					if idx = 0 then*/
    if (_idx_61680 != 0)
    goto L2F; // [1075] 1128

    /** 	 					if good_sofar != line_number then*/
    if (_good_sofar_61781 == _38line_number_16947)
    goto L30; // [1083] 1095

    /**  							CompileErr(147)*/
    RefDS(_22663);
    _46CompileErr(147, _22663, 0);
L30: 

    /** 						Warning(225, 0,*/
    _2 = (int)SEQ_PTR(_35known_files_15596);
    _31314 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_31314);
    *((int *)(_2+4)) = _31314;
    *((int *)(_2+8)) = _38line_number_16947;
    RefDS(_option_61679);
    *((int *)(_2+12)) = _option_61679;
    _31315 = MAKE_SEQ(_1);
    _31314 = NOVALUE;
    _46Warning(225, 0, _31315);
    _31315 = NOVALUE;

    /** 						tok = next_token()*/
    _0 = _tok_61784;
    _tok_61784 = _41next_token();
    DeRef(_0);

    /** 						continue*/
    goto L26; // [1125] 841
L2F: 

    /** 					idx = warning_flags[idx]*/
    _2 = (int)SEQ_PTR(_38warning_flags_16988);
    _idx_61680 = (int)*(((s1_ptr)_2)->base + _idx_61680);

    /** 					if idx = 0 then*/
    if (_idx_61680 != 0)
    goto L31; // [1140] 1174

    /** 						if on_off then*/
    if (_on_off_61678 == 0)
    {
        goto L32; // [1146] 1161
    }
    else{
    }

    /** 							OpWarning = no_warning_flag*/
    _38OpWarning_17013 = 0;
    goto L33; // [1158] 1207
L32: 

    /** 						    OpWarning = all_warning_flag*/
    _38OpWarning_17013 = 32767;
    goto L33; // [1171] 1207
L31: 

    /** 						if on_off then*/
    if (_on_off_61678 == 0)
    {
        goto L34; // [1176] 1192
    }
    else{
    }

    /** 							OpWarning = or_bits(OpWarning, idx)*/
    {unsigned long tu;
         tu = (unsigned long)_38OpWarning_17013 | (unsigned long)_idx_61680;
         _38OpWarning_17013 = MAKE_UINT(tu);
    }
    if (!IS_ATOM_INT(_38OpWarning_17013)) {
        _1 = (long)(DBL_PTR(_38OpWarning_17013)->dbl);
        if (UNIQUE(DBL_PTR(_38OpWarning_17013)) && (DBL_PTR(_38OpWarning_17013)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_38OpWarning_17013);
        _38OpWarning_17013 = _1;
    }
    goto L35; // [1189] 1206
L34: 

    /** 						    OpWarning = and_bits(OpWarning, not_bits(idx))*/
    _31320 = not_bits(_idx_61680);
    if (IS_ATOM_INT(_31320)) {
        {unsigned long tu;
             tu = (unsigned long)_38OpWarning_17013 & (unsigned long)_31320;
             _38OpWarning_17013 = MAKE_UINT(tu);
        }
    }
    else {
        temp_d.dbl = (double)_38OpWarning_17013;
        _38OpWarning_17013 = Dand_bits(&temp_d, DBL_PTR(_31320));
    }
    DeRef(_31320);
    _31320 = NOVALUE;
    if (!IS_ATOM_INT(_38OpWarning_17013)) {
        _1 = (long)(DBL_PTR(_38OpWarning_17013)->dbl);
        if (UNIQUE(DBL_PTR(_38OpWarning_17013)) && (DBL_PTR(_38OpWarning_17013)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_38OpWarning_17013);
        _38OpWarning_17013 = _1;
    }
L35: 
L33: 

    /** 					tok = next_token()*/
    _0 = _tok_61784;
    _tok_61784 = _41next_token();
    DeRef(_0);

    /** 				end while*/
    goto L26; // [1214] 841
L27: 
    goto L36; // [1219] 1228
L23: 

    /** 				putback(tok)*/
    Ref(_tok_61784);
    _41putback(_tok_61784);
L36: 
L1F: 
    DeRef(_tok_61784);
    _tok_61784 = NOVALUE;
    goto L2; // [1231] 1521
L11: 

    /** 	elsif equal(option, "define") then*/
    if (_option_61679 == _31323)
    _31324 = 1;
    else if (IS_ATOM_INT(_option_61679) && IS_ATOM_INT(_31323))
    _31324 = 0;
    else
    _31324 = (compare(_option_61679, _31323) == 0);
    if (_31324 == 0)
    {
        _31324 = NOVALUE;
        goto L37; // [1240] 1363
    }
    else{
        _31324 = NOVALUE;
    }

    /** 		option = StringToken()*/
    RefDS(_5);
    _0 = _option_61679;
    _option_61679 = _62StringToken(_5);
    DeRefDS(_0);

    /** 		if length(option) = 0 then*/
    if (IS_SEQUENCE(_option_61679)){
            _31326 = SEQ_PTR(_option_61679)->length;
    }
    else {
        _31326 = 1;
    }
    if (_31326 != 0)
    goto L38; // [1256] 1270

    /** 			CompileErr(81)*/
    RefDS(_22663);
    _46CompileErr(81, _22663, 0);
    goto L39; // [1267] 1288
L38: 

    /** 		elsif not t_identifier(option) then*/
    RefDS(_option_61679);
    _31328 = _9t_identifier(_option_61679);
    if (IS_ATOM_INT(_31328)) {
        if (_31328 != 0){
            DeRef(_31328);
            _31328 = NOVALUE;
            goto L3A; // [1276] 1287
        }
    }
    else {
        if (DBL_PTR(_31328)->dbl != 0.0){
            DeRef(_31328);
            _31328 = NOVALUE;
            goto L3A; // [1276] 1287
        }
    }
    DeRef(_31328);
    _31328 = NOVALUE;

    /** 			CompileErr(61)*/
    RefDS(_22663);
    _46CompileErr(61, _22663, 0);
L3A: 
L39: 

    /** 		if on_off = 0 then*/
    if (_on_off_61678 != 0)
    goto L3B; // [1290] 1345

    /** 			idx = find(option, OpDefines)*/
    _idx_61680 = find_from(_option_61679, _38OpDefines_17019, 1);

    /** 			if idx then*/
    if (_idx_61680 == 0)
    {
        goto L2; // [1305] 1521
    }
    else{
    }

    /** 				OpDefines = OpDefines[1..idx-1]&OpDefines[idx+1..$]*/
    _31332 = _idx_61680 - 1;
    rhs_slice_target = (object_ptr)&_31333;
    RHS_Slice(_38OpDefines_17019, 1, _31332);
    _31334 = _idx_61680 + 1;
    if (IS_SEQUENCE(_38OpDefines_17019)){
            _31335 = SEQ_PTR(_38OpDefines_17019)->length;
    }
    else {
        _31335 = 1;
    }
    rhs_slice_target = (object_ptr)&_31336;
    RHS_Slice(_38OpDefines_17019, _31334, _31335);
    Concat((object_ptr)&_38OpDefines_17019, _31333, _31336);
    DeRefDS(_31333);
    _31333 = NOVALUE;
    DeRef(_31333);
    _31333 = NOVALUE;
    DeRefDS(_31336);
    _31336 = NOVALUE;
    goto L2; // [1342] 1521
L3B: 

    /** 			OpDefines &= {option}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_option_61679);
    *((int *)(_2+4)) = _option_61679;
    _31338 = MAKE_SEQ(_1);
    Concat((object_ptr)&_38OpDefines_17019, _38OpDefines_17019, _31338);
    DeRefDS(_31338);
    _31338 = NOVALUE;
    goto L2; // [1360] 1521
L37: 

    /** 	elsif equal(option, "inline") then*/
    if (_option_61679 == _31340)
    _31341 = 1;
    else if (IS_ATOM_INT(_option_61679) && IS_ATOM_INT(_31340))
    _31341 = 0;
    else
    _31341 = (compare(_option_61679, _31340) == 0);
    if (_31341 == 0)
    {
        _31341 = NOVALUE;
        goto L3C; // [1369] 1455
    }
    else{
        _31341 = NOVALUE;
    }

    /** 		if on_off then*/
    if (_on_off_61678 == 0)
    {
        goto L3D; // [1374] 1444
    }
    else{
    }

    /** 			token tok = next_token()*/
    _0 = _tok_62021;
    _tok_62021 = _41next_token();
    DeRef(_0);

    /** 			if tok[T_ID] = ATOM then*/
    _2 = (int)SEQ_PTR(_tok_62021);
    _31343 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31343, 502)){
        _31343 = NOVALUE;
        goto L3E; // [1392] 1424
    }
    _31343 = NOVALUE;

    /** 				OpInline = floor( SymTab[tok[T_SYM]][S_OBJ] )*/
    _2 = (int)SEQ_PTR(_tok_62021);
    _31345 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_31345)){
        _31346 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31345)->dbl));
    }
    else{
        _31346 = (int)*(((s1_ptr)_2)->base + _31345);
    }
    _2 = (int)SEQ_PTR(_31346);
    _31347 = (int)*(((s1_ptr)_2)->base + 1);
    _31346 = NOVALUE;
    if (IS_ATOM_INT(_31347))
    _38OpInline_17023 = e_floor(_31347);
    else
    _38OpInline_17023 = unary_op(FLOOR, _31347);
    _31347 = NOVALUE;
    if (!IS_ATOM_INT(_38OpInline_17023)) {
        _1 = (long)(DBL_PTR(_38OpInline_17023)->dbl);
        if (UNIQUE(DBL_PTR(_38OpInline_17023)) && (DBL_PTR(_38OpInline_17023)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_38OpInline_17023);
        _38OpInline_17023 = _1;
    }
    goto L3F; // [1421] 1439
L3E: 

    /** 				putback(tok)*/
    Ref(_tok_62021);
    _41putback(_tok_62021);

    /** 				OpInline = DEFAULT_INLINE*/
    _38OpInline_17023 = 30;
L3F: 
    DeRef(_tok_62021);
    _tok_62021 = NOVALUE;
    goto L2; // [1441] 1521
L3D: 

    /** 			OpInline = 0*/
    _38OpInline_17023 = 0;
    goto L2; // [1452] 1521
L3C: 

    /** 	elsif equal( option, "indirect_includes" ) then*/
    if (_option_61679 == _31349)
    _31350 = 1;
    else if (IS_ATOM_INT(_option_61679) && IS_ATOM_INT(_31349))
    _31350 = 0;
    else
    _31350 = (compare(_option_61679, _31349) == 0);
    if (_31350 == 0)
    {
        _31350 = NOVALUE;
        goto L40; // [1461] 1474
    }
    else{
        _31350 = NOVALUE;
    }

    /** 		OpIndirectInclude = on_off*/
    _38OpIndirectInclude_17024 = _on_off_61678;
    goto L2; // [1471] 1521
L40: 

    /** 	elsif equal(option, "batch") then*/
    if (_option_61679 == _26241)
    _31351 = 1;
    else if (IS_ATOM_INT(_option_61679) && IS_ATOM_INT(_26241))
    _31351 = 0;
    else
    _31351 = (compare(_option_61679, _26241) == 0);
    if (_31351 == 0)
    {
        _31351 = NOVALUE;
        goto L41; // [1480] 1493
    }
    else{
        _31351 = NOVALUE;
    }

    /** 		batch_job = on_off*/
    _38batch_job_16959 = _on_off_61678;
    goto L2; // [1490] 1521
L41: 

    /** 	elsif integer(to_number(option, -1)) then*/
    RefDS(_option_61679);
    _31352 = _11to_number(_option_61679, -1);
    if (IS_ATOM_INT(_31352))
    _31353 = 1;
    else if (IS_ATOM_DBL(_31352))
    _31353 = IS_ATOM_INT(DoubleToInt(_31352));
    else
    _31353 = 0;
    DeRef(_31352);
    _31352 = NOVALUE;
    if (_31353 == 0)
    {
        _31353 = NOVALUE;
        goto L42; // [1503] 1509
    }
    else{
        _31353 = NOVALUE;
    }
    goto L2; // [1506] 1521
L42: 

    /** 		CompileErr(154, {option})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_option_61679);
    *((int *)(_2+4)) = _option_61679;
    _31355 = MAKE_SEQ(_1);
    _46CompileErr(154, _31355, 0);
    _31355 = NOVALUE;
L2: 

    /** end procedure*/
    DeRef(_option_61679);
    DeRef(_31215);
    _31215 = NOVALUE;
    DeRef(_31220);
    _31220 = NOVALUE;
    _31227 = NOVALUE;
    _31231 = NOVALUE;
    DeRef(_31234);
    _31234 = NOVALUE;
    _31262 = NOVALUE;
    DeRef(_31248);
    _31248 = NOVALUE;
    DeRef(_31256);
    _31256 = NOVALUE;
    _31289 = NOVALUE;
    _31292 = NOVALUE;
    _31293 = NOVALUE;
    _31296 = NOVALUE;
    DeRef(_31332);
    _31332 = NOVALUE;
    DeRef(_31302);
    _31302 = NOVALUE;
    _31345 = NOVALUE;
    DeRef(_31334);
    _31334 = NOVALUE;
    return;
    ;
}


void _41ExecCommand()
{
    int _0, _1, _2;
    

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L1; // [5] 16
    }
    else{
    }

    /** 		emit_op(RETURNT)*/
    _43emit_op(34);
L1: 

    /** 	StraightenBranches()  -- straighten top-level*/
    _41StraightenBranches();

    /** end procedure*/
    return;
    ;
}


int _41undefined_var(int _tok_62065, int _scope_62066)
{
    int _forward_62068 = NOVALUE;
    int _31361 = NOVALUE;
    int _31360 = NOVALUE;
    int _31357 = NOVALUE;
    int _0, _1, _2;
    

    /** 	token forward = next_token()*/
    _0 = _forward_62068;
    _forward_62068 = _41next_token();
    DeRef(_0);

    /** 		switch forward[T_ID] do*/
    _2 = (int)SEQ_PTR(_forward_62068);
    _31357 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_31357) ){
        goto L1; // [16] 84
    }
    if(!IS_ATOM_INT(_31357)){
        if( (DBL_PTR(_31357)->dbl != (double) ((int) DBL_PTR(_31357)->dbl) ) ){
            goto L1; // [16] 84
        }
        _0 = (int) DBL_PTR(_31357)->dbl;
    }
    else {
        _0 = _31357;
    };
    _31357 = NOVALUE;
    switch ( _0 ){ 

        /** 			case LEFT_ROUND then*/
        case -26:

        /** 				StartSourceLine( TRUE )*/
        _43StartSourceLine(_9TRUE_428, 0, 2);

        /** 				Forward_call( tok )*/
        Ref(_tok_62065);
        _41Forward_call(_tok_62065, 195);

        /** 				return 1*/
        DeRef(_tok_62065);
        DeRef(_forward_62068);
        return 1;
        goto L2; // [50] 98

        /** 			case VARIABLE then*/
        case -100:

        /** 				putback( forward )*/
        Ref(_forward_62068);
        _41putback(_forward_62068);

        /** 				Global_declaration( tok[T_SYM], scope )*/
        _2 = (int)SEQ_PTR(_tok_62065);
        _31360 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_31360);
        _31361 = _41Global_declaration(_31360, _scope_62066);
        _31360 = NOVALUE;

        /** 				return 1*/
        DeRef(_tok_62065);
        DeRef(_forward_62068);
        DeRef(_31361);
        _31361 = NOVALUE;
        return 1;
        goto L2; // [80] 98

        /** 			case else*/
        default:
L1: 

        /** 				putback( forward )*/
        Ref(_forward_62068);
        _41putback(_forward_62068);

        /** 				return 0*/
        DeRef(_tok_62065);
        DeRef(_forward_62068);
        DeRef(_31361);
        _31361 = NOVALUE;
        return 0;
    ;}L2: 
    ;
}


void _41real_parser(int _nested_62087)
{
    int _tok_62089 = NOVALUE;
    int _id_62090 = NOVALUE;
    int _scope_62091 = NOVALUE;
    int _test_62232 = NOVALUE;
    int _31494 = NOVALUE;
    int _31492 = NOVALUE;
    int _31491 = NOVALUE;
    int _31490 = NOVALUE;
    int _31489 = NOVALUE;
    int _31488 = NOVALUE;
    int _31487 = NOVALUE;
    int _31486 = NOVALUE;
    int _31481 = NOVALUE;
    int _31480 = NOVALUE;
    int _31477 = NOVALUE;
    int _31475 = NOVALUE;
    int _31474 = NOVALUE;
    int _31471 = NOVALUE;
    int _31458 = NOVALUE;
    int _31451 = NOVALUE;
    int _31450 = NOVALUE;
    int _31448 = NOVALUE;
    int _31446 = NOVALUE;
    int _31445 = NOVALUE;
    int _31443 = NOVALUE;
    int _31441 = NOVALUE;
    int _31436 = NOVALUE;
    int _31434 = NOVALUE;
    int _31432 = NOVALUE;
    int _31431 = NOVALUE;
    int _31430 = NOVALUE;
    int _31428 = NOVALUE;
    int _31426 = NOVALUE;
    int _31424 = NOVALUE;
    int _31422 = NOVALUE;
    int _31421 = NOVALUE;
    int _31420 = NOVALUE;
    int _31419 = NOVALUE;
    int _31418 = NOVALUE;
    int _31417 = NOVALUE;
    int _31416 = NOVALUE;
    int _31415 = NOVALUE;
    int _31414 = NOVALUE;
    int _31413 = NOVALUE;
    int _31412 = NOVALUE;
    int _31411 = NOVALUE;
    int _31410 = NOVALUE;
    int _31409 = NOVALUE;
    int _31407 = NOVALUE;
    int _31406 = NOVALUE;
    int _31405 = NOVALUE;
    int _31404 = NOVALUE;
    int _31402 = NOVALUE;
    int _31400 = NOVALUE;
    int _31399 = NOVALUE;
    int _31398 = NOVALUE;
    int _31396 = NOVALUE;
    int _31389 = NOVALUE;
    int _31387 = NOVALUE;
    int _31386 = NOVALUE;
    int _31385 = NOVALUE;
    int _31384 = NOVALUE;
    int _31383 = NOVALUE;
    int _31382 = NOVALUE;
    int _31381 = NOVALUE;
    int _31379 = NOVALUE;
    int _31378 = NOVALUE;
    int _31377 = NOVALUE;
    int _31376 = NOVALUE;
    int _31375 = NOVALUE;
    int _31374 = NOVALUE;
    int _31373 = NOVALUE;
    int _31372 = NOVALUE;
    int _31371 = NOVALUE;
    int _31370 = NOVALUE;
    int _31368 = NOVALUE;
    int _31364 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_nested_62087)) {
        _1 = (long)(DBL_PTR(_nested_62087)->dbl);
        if (UNIQUE(DBL_PTR(_nested_62087)) && (DBL_PTR(_nested_62087)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_nested_62087);
        _nested_62087 = _1;
    }

    /** 	integer id*/

    /** 	integer scope*/

    /** 	while TRUE do  -- infinite loop until scanner aborts*/
L1: 
    if (_9TRUE_428 == 0)
    {
        goto L2; // [14] 1843
    }
    else{
    }

    /** 		if OpInline = 25000 then*/
    if (_38OpInline_17023 != 25000)
    goto L3; // [21] 35

    /** 			CompileErr("OpInline went nuts: [1]", OpInline )*/
    RefDS(_31363);
    _46CompileErr(_31363, _38OpInline_17023, 0);
L3: 

    /** 		start_index = length(Code)+1*/
    if (IS_SEQUENCE(_38Code_17038)){
            _31364 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _31364 = 1;
    }
    _41start_index_55128 = _31364 + 1;
    _31364 = NOVALUE;

    /** 		tok = next_token()*/
    _0 = _tok_62089;
    _tok_62089 = _41next_token();
    DeRef(_0);

    /** 		id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_62089);
    _id_62090 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_id_62090)){
        _id_62090 = (long)DBL_PTR(_id_62090)->dbl;
    }

    /** 		if id = VARIABLE or id = QUALIFIED_VARIABLE then*/
    _31368 = (_id_62090 == -100);
    if (_31368 != 0) {
        goto L4; // [69] 84
    }
    _31370 = (_id_62090 == 512);
    if (_31370 == 0)
    {
        DeRef(_31370);
        _31370 = NOVALUE;
        goto L5; // [80] 153
    }
    else{
        DeRef(_31370);
        _31370 = NOVALUE;
    }
L4: 

    /** 			if SymTab[tok[T_SYM]][S_SCOPE] = SC_UNDEFINED*/
    _2 = (int)SEQ_PTR(_tok_62089);
    _31371 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_31371)){
        _31372 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31371)->dbl));
    }
    else{
        _31372 = (int)*(((s1_ptr)_2)->base + _31371);
    }
    _2 = (int)SEQ_PTR(_31372);
    _31373 = (int)*(((s1_ptr)_2)->base + 4);
    _31372 = NOVALUE;
    if (IS_ATOM_INT(_31373)) {
        _31374 = (_31373 == 9);
    }
    else {
        _31374 = binary_op(EQUALS, _31373, 9);
    }
    _31373 = NOVALUE;
    if (IS_ATOM_INT(_31374)) {
        if (_31374 == 0) {
            goto L6; // [110] 130
        }
    }
    else {
        if (DBL_PTR(_31374)->dbl == 0.0) {
            goto L6; // [110] 130
        }
    }
    Ref(_tok_62089);
    _31376 = _41undefined_var(_tok_62089, 5);
    if (_31376 == 0) {
        DeRef(_31376);
        _31376 = NOVALUE;
        goto L6; // [122] 130
    }
    else {
        if (!IS_ATOM_INT(_31376) && DBL_PTR(_31376)->dbl == 0.0){
            DeRef(_31376);
            _31376 = NOVALUE;
            goto L6; // [122] 130
        }
        DeRef(_31376);
        _31376 = NOVALUE;
    }
    DeRef(_31376);
    _31376 = NOVALUE;

    /** 				continue*/
    goto L1; // [127] 12
L6: 

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			Assignment(tok)*/
    Ref(_tok_62089);
    _41Assignment(_tok_62089);

    /** 			ExecCommand()*/
    _41ExecCommand();
    goto L7; // [150] 1833
L5: 

    /** 		elsif id = PROCEDURE or id = FUNCTION or id = TYPE_DECL then*/
    _31377 = (_id_62090 == 405);
    if (_31377 != 0) {
        _31378 = 1;
        goto L8; // [161] 175
    }
    _31379 = (_id_62090 == 406);
    _31378 = (_31379 != 0);
L8: 
    if (_31378 != 0) {
        goto L9; // [175] 190
    }
    _31381 = (_id_62090 == 416);
    if (_31381 == 0)
    {
        DeRef(_31381);
        _31381 = NOVALUE;
        goto LA; // [186] 207
    }
    else{
        DeRef(_31381);
        _31381 = NOVALUE;
    }
L9: 

    /** 			SubProg(tok[T_ID], SC_LOCAL)*/
    _2 = (int)SEQ_PTR(_tok_62089);
    _31382 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_31382);
    _41SubProg(_31382, 5);
    _31382 = NOVALUE;
    goto L7; // [204] 1833
LA: 

    /** 		elsif id = GLOBAL or id = EXPORT or id = OVERRIDE or id = PUBLIC then*/
    _31383 = (_id_62090 == 412);
    if (_31383 != 0) {
        _31384 = 1;
        goto LB; // [215] 229
    }
    _31385 = (_id_62090 == 428);
    _31384 = (_31385 != 0);
LB: 
    if (_31384 != 0) {
        _31386 = 1;
        goto LC; // [229] 243
    }
    _31387 = (_id_62090 == 429);
    _31386 = (_31387 != 0);
LC: 
    if (_31386 != 0) {
        goto LD; // [243] 258
    }
    _31389 = (_id_62090 == 430);
    if (_31389 == 0)
    {
        DeRef(_31389);
        _31389 = NOVALUE;
        goto LE; // [254] 628
    }
    else{
        DeRef(_31389);
        _31389 = NOVALUE;
    }
LD: 

    /** 			if id = GLOBAL then*/
    if (_id_62090 != 412)
    goto LF; // [262] 278

    /** 			    scope = SC_GLOBAL*/
    _scope_62091 = 6;
    goto L10; // [275] 337
LF: 

    /** 			elsif id = EXPORT then*/
    if (_id_62090 != 428)
    goto L11; // [282] 298

    /** 				scope = SC_EXPORT*/
    _scope_62091 = 11;
    goto L10; // [295] 337
L11: 

    /** 			elsif id = OVERRIDE then*/
    if (_id_62090 != 429)
    goto L12; // [302] 318

    /** 				scope = SC_OVERRIDE*/
    _scope_62091 = 12;
    goto L10; // [315] 337
L12: 

    /** 			elsif id = PUBLIC then*/
    if (_id_62090 != 430)
    goto L13; // [322] 336

    /** 				scope = SC_PUBLIC*/
    _scope_62091 = 13;
L13: 
L10: 

    /** 			tok = next_token()*/
    _0 = _tok_62089;
    _tok_62089 = _41next_token();
    DeRef(_0);

    /** 			id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_62089);
    _id_62090 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_id_62090)){
        _id_62090 = (long)DBL_PTR(_id_62090)->dbl;
    }

    /** 			if id = TYPE or id = QUALIFIED_TYPE then*/
    _31396 = (_id_62090 == 504);
    if (_31396 != 0) {
        goto L14; // [360] 375
    }
    _31398 = (_id_62090 == 522);
    if (_31398 == 0)
    {
        DeRef(_31398);
        _31398 = NOVALUE;
        goto L15; // [371] 393
    }
    else{
        DeRef(_31398);
        _31398 = NOVALUE;
    }
L14: 

    /** 				Global_declaration(tok[T_SYM], scope )*/
    _2 = (int)SEQ_PTR(_tok_62089);
    _31399 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_31399);
    _31400 = _41Global_declaration(_31399, _scope_62091);
    _31399 = NOVALUE;
    goto L7; // [390] 1833
L15: 

    /** 			elsif id = CONSTANT then*/
    if (_id_62090 != 417)
    goto L16; // [397] 417

    /** 				Global_declaration(0, scope )*/
    _31402 = _41Global_declaration(0, _scope_62091);

    /** 				ExecCommand()*/
    _41ExecCommand();
    goto L7; // [414] 1833
L16: 

    /** 			elsif id = ENUM then*/
    if (_id_62090 != 427)
    goto L17; // [421] 441

    /** 				Global_declaration(-1, scope )*/
    _31404 = _41Global_declaration(-1, _scope_62091);

    /** 				ExecCommand()*/
    _41ExecCommand();
    goto L7; // [438] 1833
L17: 

    /** 			elsif id = PROCEDURE or id = FUNCTION or id = TYPE_DECL then*/
    _31405 = (_id_62090 == 405);
    if (_31405 != 0) {
        _31406 = 1;
        goto L18; // [449] 463
    }
    _31407 = (_id_62090 == 406);
    _31406 = (_31407 != 0);
L18: 
    if (_31406 != 0) {
        goto L19; // [463] 478
    }
    _31409 = (_id_62090 == 416);
    if (_31409 == 0)
    {
        DeRef(_31409);
        _31409 = NOVALUE;
        goto L1A; // [474] 489
    }
    else{
        DeRef(_31409);
        _31409 = NOVALUE;
    }
L19: 

    /** 				SubProg(id, scope )*/
    _41SubProg(_id_62090, _scope_62091);
    goto L7; // [486] 1833
L1A: 

    /** 			elsif (scope = SC_PUBLIC) and id = INCLUDE then*/
    _31410 = (_scope_62091 == 13);
    if (_31410 == 0) {
        goto L1B; // [499] 525
    }
    _31412 = (_id_62090 == 418);
    if (_31412 == 0)
    {
        DeRef(_31412);
        _31412 = NOVALUE;
        goto L1B; // [510] 525
    }
    else{
        DeRef(_31412);
        _31412 = NOVALUE;
    }

    /** 				IncludeScan( 1 )*/
    _62IncludeScan(1);

    /** 				PushGoto()*/
    _41PushGoto();
    goto L7; // [522] 1833
L1B: 

    /** 			elsif (id = VARIABLE or id = QUALIFIED_VARIABLE)*/
    _31413 = (_id_62090 == -100);
    if (_31413 != 0) {
        _31414 = 1;
        goto L1C; // [533] 547
    }
    _31415 = (_id_62090 == 512);
    _31414 = (_31415 != 0);
L1C: 
    if (_31414 == 0) {
        _31416 = 0;
        goto L1D; // [547] 579
    }
    _2 = (int)SEQ_PTR(_tok_62089);
    _31417 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_31417)){
        _31418 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31417)->dbl));
    }
    else{
        _31418 = (int)*(((s1_ptr)_2)->base + _31417);
    }
    _2 = (int)SEQ_PTR(_31418);
    _31419 = (int)*(((s1_ptr)_2)->base + 4);
    _31418 = NOVALUE;
    if (IS_ATOM_INT(_31419)) {
        _31420 = (_31419 == 9);
    }
    else {
        _31420 = binary_op(EQUALS, _31419, 9);
    }
    _31419 = NOVALUE;
    if (IS_ATOM_INT(_31420))
    _31416 = (_31420 != 0);
    else
    _31416 = DBL_PTR(_31420)->dbl != 0.0;
L1D: 
    if (_31416 == 0) {
        goto L1E; // [579] 599
    }
    Ref(_tok_62089);
    _31422 = _41undefined_var(_tok_62089, _scope_62091);
    if (_31422 == 0) {
        DeRef(_31422);
        _31422 = NOVALUE;
        goto L1E; // [589] 599
    }
    else {
        if (!IS_ATOM_INT(_31422) && DBL_PTR(_31422)->dbl == 0.0){
            DeRef(_31422);
            _31422 = NOVALUE;
            goto L1E; // [589] 599
        }
        DeRef(_31422);
        _31422 = NOVALUE;
    }
    DeRef(_31422);
    _31422 = NOVALUE;

    /** 				continue*/
    goto L1; // [594] 12
    goto L7; // [596] 1833
L1E: 

    /** 			elsif scope = SC_GLOBAL then*/
    if (_scope_62091 != 6)
    goto L1F; // [603] 617

    /** 				CompileErr( 18 )*/
    RefDS(_22663);
    _46CompileErr(18, _22663, 0);
    goto L7; // [614] 1833
L1F: 

    /** 				CompileErr( 16 )*/
    RefDS(_22663);
    _46CompileErr(16, _22663, 0);
    goto L7; // [625] 1833
LE: 

    /** 		elsif id = TYPE or id = QUALIFIED_TYPE then*/
    _31424 = (_id_62090 == 504);
    if (_31424 != 0) {
        goto L20; // [636] 651
    }
    _31426 = (_id_62090 == 522);
    if (_31426 == 0)
    {
        DeRef(_31426);
        _31426 = NOVALUE;
        goto L21; // [647] 738
    }
    else{
        DeRef(_31426);
        _31426 = NOVALUE;
    }
L20: 

    /** 			token test = next_token()*/
    _0 = _test_62232;
    _test_62232 = _41next_token();
    DeRef(_0);

    /** 			putback( test )*/
    Ref(_test_62232);
    _41putback(_test_62232);

    /** 			if test[T_ID] = LEFT_ROUND then*/
    _2 = (int)SEQ_PTR(_test_62232);
    _31428 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31428, -26)){
        _31428 = NOVALUE;
        goto L22; // [671] 711
    }
    _31428 = NOVALUE;

    /** 					StartSourceLine( TRUE )*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 					Procedure_call(tok)*/
    Ref(_tok_62089);
    _41Procedure_call(_tok_62089);

    /** 					clear_op()*/
    _43clear_op();

    /** 					if Pop() then end if*/
    _31430 = _43Pop();
    if (_31430 == 0) {
        DeRef(_31430);
        _31430 = NOVALUE;
        goto L23; // [700] 704
    }
    else {
        if (!IS_ATOM_INT(_31430) && DBL_PTR(_31430)->dbl == 0.0){
            DeRef(_31430);
            _31430 = NOVALUE;
            goto L23; // [700] 704
        }
        DeRef(_31430);
        _31430 = NOVALUE;
    }
    DeRef(_31430);
    _31430 = NOVALUE;
L23: 

    /** 					ExecCommand()*/
    _41ExecCommand();
    goto L24; // [708] 727
L22: 

    /** 				Global_declaration( tok[T_SYM], SC_LOCAL )*/
    _2 = (int)SEQ_PTR(_tok_62089);
    _31431 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_31431);
    _31432 = _41Global_declaration(_31431, 5);
    _31431 = NOVALUE;
L24: 

    /** 			continue*/
    DeRef(_test_62232);
    _test_62232 = NOVALUE;
    goto L1; // [731] 12
    goto L7; // [735] 1833
L21: 

    /** 		elsif id = CONSTANT then*/
    if (_id_62090 != 417)
    goto L25; // [742] 762

    /** 			Global_declaration(0, SC_LOCAL)*/
    _31434 = _41Global_declaration(0, 5);

    /** 			ExecCommand()*/
    _41ExecCommand();
    goto L7; // [759] 1833
L25: 

    /** 		elsif id = ENUM then*/
    if (_id_62090 != 427)
    goto L26; // [766] 786

    /** 			Global_declaration(-1, SC_LOCAL)*/
    _31436 = _41Global_declaration(-1, 5);

    /** 			ExecCommand()*/
    _41ExecCommand();
    goto L7; // [783] 1833
L26: 

    /** 		elsif id = IF then*/
    if (_id_62090 != 20)
    goto L27; // [790] 816

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			If_statement()*/
    _41If_statement();

    /** 			ExecCommand()*/
    _41ExecCommand();
    goto L7; // [813] 1833
L27: 

    /** 		elsif id = FOR then*/
    if (_id_62090 != 21)
    goto L28; // [820] 846

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			For_statement()*/
    _41For_statement();

    /** 			ExecCommand()*/
    _41ExecCommand();
    goto L7; // [843] 1833
L28: 

    /** 		elsif id = WHILE then*/
    if (_id_62090 != 47)
    goto L29; // [850] 876

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			While_statement()*/
    _41While_statement();

    /** 			ExecCommand()*/
    _41ExecCommand();
    goto L7; // [873] 1833
L29: 

    /** 		elsif id = LOOP then*/
    if (_id_62090 != 422)
    goto L2A; // [880] 906

    /** 		    StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 		    Loop_statement()*/
    _41Loop_statement();

    /** 		    ExecCommand()*/
    _41ExecCommand();
    goto L7; // [903] 1833
L2A: 

    /** 		elsif id = PROC or id = QUALIFIED_PROC then*/
    _31441 = (_id_62090 == 27);
    if (_31441 != 0) {
        goto L2B; // [914] 929
    }
    _31443 = (_id_62090 == 521);
    if (_31443 == 0)
    {
        DeRef(_31443);
        _31443 = NOVALUE;
        goto L2C; // [925] 972
    }
    else{
        DeRef(_31443);
        _31443 = NOVALUE;
    }
L2B: 

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			if id = PROC then*/
    if (_id_62090 != 27)
    goto L2D; // [944] 960

    /** 				UndefinedVar( tok[T_SYM] )*/
    _2 = (int)SEQ_PTR(_tok_62089);
    _31445 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_31445);
    _41UndefinedVar(_31445);
    _31445 = NOVALUE;
L2D: 

    /** 			Procedure_call(tok)*/
    Ref(_tok_62089);
    _41Procedure_call(_tok_62089);

    /** 			ExecCommand()*/
    _41ExecCommand();
    goto L7; // [969] 1833
L2C: 

    /** 		elsif id = FUNC or id = QUALIFIED_FUNC then*/
    _31446 = (_id_62090 == 501);
    if (_31446 != 0) {
        goto L2E; // [980] 995
    }
    _31448 = (_id_62090 == 520);
    if (_31448 == 0)
    {
        DeRef(_31448);
        _31448 = NOVALUE;
        goto L2F; // [991] 1051
    }
    else{
        DeRef(_31448);
        _31448 = NOVALUE;
    }
L2E: 

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			if id = FUNC then*/
    if (_id_62090 != 501)
    goto L30; // [1010] 1026

    /** 				UndefinedVar( tok[T_SYM] )*/
    _2 = (int)SEQ_PTR(_tok_62089);
    _31450 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_31450);
    _41UndefinedVar(_31450);
    _31450 = NOVALUE;
L30: 

    /** 			Procedure_call(tok)*/
    Ref(_tok_62089);
    _41Procedure_call(_tok_62089);

    /** 			clear_op()*/
    _43clear_op();

    /** 			if Pop() then end if*/
    _31451 = _43Pop();
    if (_31451 == 0) {
        DeRef(_31451);
        _31451 = NOVALUE;
        goto L31; // [1040] 1044
    }
    else {
        if (!IS_ATOM_INT(_31451) && DBL_PTR(_31451)->dbl == 0.0){
            DeRef(_31451);
            _31451 = NOVALUE;
            goto L31; // [1040] 1044
        }
        DeRef(_31451);
        _31451 = NOVALUE;
    }
    DeRef(_31451);
    _31451 = NOVALUE;
L31: 

    /** 			ExecCommand()*/
    _41ExecCommand();
    goto L7; // [1048] 1833
L2F: 

    /** 		elsif id = RETURN then*/
    if (_id_62090 != 413)
    goto L32; // [1055] 1066

    /** 			Return_statement() -- will fail - not allowed at top level*/
    _41Return_statement();
    goto L7; // [1063] 1833
L32: 

    /** 		elsif id = EXIT then*/
    if (_id_62090 != 61)
    goto L33; // [1070] 1108

    /** 			if nested then*/
    if (_nested_62087 == 0)
    {
        goto L34; // [1076] 1097
    }
    else{
    }

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			Exit_statement()*/
    _41Exit_statement();
    goto L7; // [1094] 1833
L34: 

    /** 			CompileErr(89)*/
    RefDS(_22663);
    _46CompileErr(89, _22663, 0);
    goto L7; // [1105] 1833
L33: 

    /** 		elsif id = INCLUDE then*/
    if (_id_62090 != 418)
    goto L35; // [1112] 1128

    /** 			IncludeScan( 0 )*/
    _62IncludeScan(0);

    /** 			PushGoto()*/
    _41PushGoto();
    goto L7; // [1125] 1833
L35: 

    /** 		elsif id = WITH then*/
    if (_id_62090 != 420)
    goto L36; // [1132] 1146

    /** 			SetWith(TRUE)*/
    _41SetWith(_9TRUE_428);
    goto L7; // [1143] 1833
L36: 

    /** 		elsif id = WITHOUT then*/
    if (_id_62090 != 421)
    goto L37; // [1150] 1164

    /** 			SetWith(FALSE)*/
    _41SetWith(_9FALSE_426);
    goto L7; // [1161] 1833
L37: 

    /** 		elsif id = END_OF_FILE then*/
    if (_id_62090 != -21)
    goto L38; // [1168] 1285

    /** 			if IncludePop() then*/
    _31458 = _62IncludePop();
    if (_31458 == 0) {
        DeRef(_31458);
        _31458 = NOVALUE;
        goto L39; // [1177] 1273
    }
    else {
        if (!IS_ATOM_INT(_31458) && DBL_PTR(_31458)->dbl == 0.0){
            DeRef(_31458);
            _31458 = NOVALUE;
            goto L39; // [1177] 1273
        }
        DeRef(_31458);
        _31458 = NOVALUE;
    }
    DeRef(_31458);
    _31458 = NOVALUE;

    /** 				backed_up_tok = {}*/
    RefDS(_22663);
    DeRef(_41backed_up_tok_55129);
    _41backed_up_tok_55129 = _22663;

    /** 				PopGoto()*/
    _41PopGoto();

    /** 				read_line()*/
    _62read_line();

    /** 				last_ForwardLine     = ThisLine*/
    Ref(_46ThisLine_49482);
    DeRef(_46last_ForwardLine_49485);
    _46last_ForwardLine_49485 = _46ThisLine_49482;

    /** 				last_fwd_line_number = line_number*/
    _38last_fwd_line_number_16950 = _38line_number_16947;

    /** 				last_forward_bp      = bp*/
    _46last_forward_bp_49489 = _46bp_49486;

    /** 				putback_ForwardLine     = ThisLine*/
    Ref(_46ThisLine_49482);
    DeRef(_46putback_ForwardLine_49484);
    _46putback_ForwardLine_49484 = _46ThisLine_49482;

    /** 				putback_fwd_line_number = line_number*/
    _38putback_fwd_line_number_16949 = _38line_number_16947;

    /** 				putback_forward_bp      = bp*/
    _46putback_forward_bp_49488 = _46bp_49486;

    /** 				ForwardLine     = ThisLine*/
    Ref(_46ThisLine_49482);
    DeRef(_46ForwardLine_49483);
    _46ForwardLine_49483 = _46ThisLine_49482;

    /** 				fwd_line_number = line_number*/
    _38fwd_line_number_16948 = _38line_number_16947;

    /** 				forward_bp      = bp*/
    _46forward_bp_49487 = _46bp_49486;
    goto L7; // [1270] 1833
L39: 

    /** 				CheckForUndefinedGotoLabels()*/
    _41CheckForUndefinedGotoLabels();

    /** 				exit -- all finished*/
    goto L2; // [1279] 1843
    goto L7; // [1282] 1833
L38: 

    /** 		elsif id = QUESTION_MARK then*/
    if (_id_62090 != -31)
    goto L3A; // [1289] 1315

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			Print_statement()*/
    _41Print_statement();

    /** 			ExecCommand()*/
    _41ExecCommand();
    goto L7; // [1312] 1833
L3A: 

    /** 		elsif id = LABEL then*/
    if (_id_62090 != 419)
    goto L3B; // [1319] 1341

    /** 			StartSourceLine(TRUE, , COVERAGE_SUPPRESS)*/
    _43StartSourceLine(_9TRUE_428, 0, 1);

    /** 			GLabel_statement()*/
    _41GLabel_statement();
    goto L7; // [1338] 1833
L3B: 

    /** 		elsif id = GOTO then*/
    if (_id_62090 != 188)
    goto L3C; // [1345] 1367

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			Goto_statement()*/
    _41Goto_statement();
    goto L7; // [1364] 1833
L3C: 

    /** 		elsif id = CONTINUE then*/
    if (_id_62090 != 426)
    goto L3D; // [1371] 1409

    /** 			if nested then*/
    if (_nested_62087 == 0)
    {
        goto L3E; // [1377] 1398
    }
    else{
    }

    /** 				StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 				Continue_statement()*/
    _41Continue_statement();
    goto L7; // [1395] 1833
L3E: 

    /** 				CompileErr(50)*/
    RefDS(_22663);
    _46CompileErr(50, _22663, 0);
    goto L7; // [1406] 1833
L3D: 

    /** 		elsif id = RETRY then*/
    if (_id_62090 != 184)
    goto L3F; // [1413] 1451

    /** 			if nested then*/
    if (_nested_62087 == 0)
    {
        goto L40; // [1419] 1440
    }
    else{
    }

    /** 				StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 				Retry_statement()*/
    _41Retry_statement();
    goto L7; // [1437] 1833
L40: 

    /** 				CompileErr(128)*/
    RefDS(_22663);
    _46CompileErr(128, _22663, 0);
    goto L7; // [1448] 1833
L3F: 

    /** 		elsif id = BREAK then*/
    if (_id_62090 != 425)
    goto L41; // [1455] 1493

    /** 			if nested then*/
    if (_nested_62087 == 0)
    {
        goto L42; // [1461] 1482
    }
    else{
    }

    /** 				StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 				Break_statement()*/
    _41Break_statement();
    goto L7; // [1479] 1833
L42: 

    /** 				CompileErr(39)*/
    RefDS(_22663);
    _46CompileErr(39, _22663, 0);
    goto L7; // [1490] 1833
L41: 

    /** 		elsif id = ENTRY then*/
    if (_id_62090 != 424)
    goto L43; // [1497] 1535

    /** 			if nested then*/
    if (_nested_62087 == 0)
    {
        goto L44; // [1503] 1524
    }
    else{
    }

    /** 			    StartSourceLine(TRUE, , COVERAGE_SUPPRESS)*/
    _43StartSourceLine(_9TRUE_428, 0, 1);

    /** 			    Entry_statement()*/
    _41Entry_statement();
    goto L7; // [1521] 1833
L44: 

    /** 				CompileErr(72)*/
    RefDS(_22663);
    _46CompileErr(72, _22663, 0);
    goto L7; // [1532] 1833
L43: 

    /** 		elsif id = IFDEF then*/
    if (_id_62090 != 407)
    goto L45; // [1539] 1561

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			Ifdef_statement()*/
    _41Ifdef_statement();
    goto L7; // [1558] 1833
L45: 

    /** 		elsif id = CASE then*/
    if (_id_62090 != 186)
    goto L46; // [1565] 1576

    /** 			Case_statement()*/
    _41Case_statement();
    goto L7; // [1573] 1833
L46: 

    /** 		elsif id = SWITCH then*/
    if (_id_62090 != 185)
    goto L47; // [1580] 1602

    /** 			StartSourceLine(TRUE)*/
    _43StartSourceLine(_9TRUE_428, 0, 2);

    /** 			Switch_statement()*/
    _41Switch_statement();
    goto L7; // [1599] 1833
L47: 

    /** 		elsif id = ILLEGAL_CHAR then*/
    if (_id_62090 != -20)
    goto L48; // [1606] 1620

    /** 			CompileErr(102)*/
    RefDS(_22663);
    _46CompileErr(102, _22663, 0);
    goto L7; // [1617] 1833
L48: 

    /** 			if nested then*/
    if (_nested_62087 == 0)
    {
        goto L49; // [1622] 1774
    }
    else{
    }

    /** 				if id = ELSE then*/
    if (_id_62090 != 23)
    goto L4A; // [1629] 1683

    /** 					if length(if_stack) = 0 then*/
    if (IS_SEQUENCE(_41if_stack_55157)){
            _31471 = SEQ_PTR(_41if_stack_55157)->length;
    }
    else {
        _31471 = 1;
    }
    if (_31471 != 0)
    goto L4B; // [1640] 1740

    /** 						if live_ifdef > 0 then*/
    if (_41live_ifdef_59303 <= 0)
    goto L4C; // [1648] 1671

    /** 							CompileErr(134, ifdef_lineno[$])*/
    if (IS_SEQUENCE(_41ifdef_lineno_59304)){
            _31474 = SEQ_PTR(_41ifdef_lineno_59304)->length;
    }
    else {
        _31474 = 1;
    }
    _2 = (int)SEQ_PTR(_41ifdef_lineno_59304);
    _31475 = (int)*(((s1_ptr)_2)->base + _31474);
    _46CompileErr(134, _31475, 0);
    _31475 = NOVALUE;
    goto L4B; // [1668] 1740
L4C: 

    /** 							CompileErr(118)*/
    RefDS(_22663);
    _46CompileErr(118, _22663, 0);
    goto L4B; // [1680] 1740
L4A: 

    /** 				elsif id = ELSIF then*/
    if (_id_62090 != 414)
    goto L4D; // [1687] 1739

    /** 					if length(if_stack) = 0 then*/
    if (IS_SEQUENCE(_41if_stack_55157)){
            _31477 = SEQ_PTR(_41if_stack_55157)->length;
    }
    else {
        _31477 = 1;
    }
    if (_31477 != 0)
    goto L4E; // [1698] 1738

    /** 						if live_ifdef > 0 then*/
    if (_41live_ifdef_59303 <= 0)
    goto L4F; // [1706] 1729

    /** 							CompileErr(139, ifdef_lineno[$])*/
    if (IS_SEQUENCE(_41ifdef_lineno_59304)){
            _31480 = SEQ_PTR(_41ifdef_lineno_59304)->length;
    }
    else {
        _31480 = 1;
    }
    _2 = (int)SEQ_PTR(_41ifdef_lineno_59304);
    _31481 = (int)*(((s1_ptr)_2)->base + _31480);
    _46CompileErr(139, _31481, 0);
    _31481 = NOVALUE;
    goto L50; // [1726] 1737
L4F: 

    /** 							CompileErr(119)*/
    RefDS(_22663);
    _46CompileErr(119, _22663, 0);
L50: 
L4E: 
L4D: 
L4B: 

    /** 				putback(tok)*/
    Ref(_tok_62089);
    _41putback(_tok_62089);

    /** 				if stmt_nest > 0 then*/
    if (_41stmt_nest_55154 <= 0)
    goto L51; // [1749] 1766

    /** 					stmt_nest -= 1*/
    _41stmt_nest_55154 = _41stmt_nest_55154 - 1;

    /** 					InitDelete()*/
    _41InitDelete();
L51: 

    /** 				return*/
    DeRef(_tok_62089);
    _31371 = NOVALUE;
    DeRef(_31402);
    _31402 = NOVALUE;
    DeRef(_31407);
    _31407 = NOVALUE;
    DeRef(_31404);
    _31404 = NOVALUE;
    DeRef(_31383);
    _31383 = NOVALUE;
    DeRef(_31396);
    _31396 = NOVALUE;
    DeRef(_31413);
    _31413 = NOVALUE;
    DeRef(_31434);
    _31434 = NOVALUE;
    DeRef(_31368);
    _31368 = NOVALUE;
    DeRef(_31420);
    _31420 = NOVALUE;
    _31417 = NOVALUE;
    DeRef(_31446);
    _31446 = NOVALUE;
    DeRef(_31410);
    _31410 = NOVALUE;
    DeRef(_31385);
    _31385 = NOVALUE;
    DeRef(_31379);
    _31379 = NOVALUE;
    DeRef(_31415);
    _31415 = NOVALUE;
    DeRef(_31424);
    _31424 = NOVALUE;
    DeRef(_31436);
    _31436 = NOVALUE;
    DeRef(_31374);
    _31374 = NOVALUE;
    DeRef(_31400);
    _31400 = NOVALUE;
    DeRef(_31377);
    _31377 = NOVALUE;
    DeRef(_31441);
    _31441 = NOVALUE;
    DeRef(_31387);
    _31387 = NOVALUE;
    DeRef(_31405);
    _31405 = NOVALUE;
    DeRef(_31432);
    _31432 = NOVALUE;
    return;
    goto L52; // [1771] 1832
L49: 

    /** 				if id = END then*/
    if (_id_62090 != 402)
    goto L53; // [1778] 1809

    /** 					tok = next_token()*/
    _0 = _tok_62089;
    _tok_62089 = _41next_token();
    DeRef(_0);

    /** 					CompileErr(17, {find_token_text(tok[T_ID])})*/
    _2 = (int)SEQ_PTR(_tok_62089);
    _31486 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_31486);
    _31487 = _65find_token_text(_31486);
    _31486 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _31487;
    _31488 = MAKE_SEQ(_1);
    _31487 = NOVALUE;
    _46CompileErr(17, _31488, 0);
    _31488 = NOVALUE;
L53: 

    /** 				CompileErr(117, { match_replace(",", find_token_text(id), "") })*/
    _31489 = _65find_token_text(_id_62090);
    RefDS(_26989);
    RefDS(_22663);
    _31490 = _12match_replace(_26989, _31489, _22663, 0);
    _31489 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _31490;
    _31491 = MAKE_SEQ(_1);
    _31490 = NOVALUE;
    _46CompileErr(117, _31491, 0);
    _31491 = NOVALUE;
L52: 
L7: 

    /** 		flush_temps()*/
    RefDS(_22663);
    _43flush_temps(_22663);

    /** 	end while*/
    goto L1; // [1840] 12
L2: 

    /** 	emit_op(RETURNT)*/
    _43emit_op(34);

    /** 	clear_last()*/
    _43clear_last();

    /** 	StraightenBranches()*/
    _41StraightenBranches();

    /** 	SymTab[TopLevelSub][S_CODE] = Code*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38TopLevelSub_16953 + ((s1_ptr)_2)->base);
    RefDS(_38Code_17038);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_CODE_16610))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_CODE_16610);
    _1 = *(int *)_2;
    *(int *)_2 = _38Code_17038;
    DeRef(_1);
    _31492 = NOVALUE;

    /** 	EndLineTable()*/
    _41EndLineTable();

    /** 	SymTab[TopLevelSub][S_LINETAB] = LineTable*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38TopLevelSub_16953 + ((s1_ptr)_2)->base);
    RefDS(_38LineTable_17039);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_LINETAB_16633))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_LINETAB_16633)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_LINETAB_16633);
    _1 = *(int *)_2;
    *(int *)_2 = _38LineTable_17039;
    DeRef(_1);
    _31494 = NOVALUE;

    /** end procedure*/
    DeRef(_tok_62089);
    _31371 = NOVALUE;
    DeRef(_31402);
    _31402 = NOVALUE;
    DeRef(_31407);
    _31407 = NOVALUE;
    DeRef(_31404);
    _31404 = NOVALUE;
    DeRef(_31383);
    _31383 = NOVALUE;
    DeRef(_31396);
    _31396 = NOVALUE;
    DeRef(_31413);
    _31413 = NOVALUE;
    DeRef(_31434);
    _31434 = NOVALUE;
    DeRef(_31368);
    _31368 = NOVALUE;
    DeRef(_31420);
    _31420 = NOVALUE;
    _31417 = NOVALUE;
    DeRef(_31446);
    _31446 = NOVALUE;
    DeRef(_31410);
    _31410 = NOVALUE;
    DeRef(_31385);
    _31385 = NOVALUE;
    DeRef(_31379);
    _31379 = NOVALUE;
    DeRef(_31415);
    _31415 = NOVALUE;
    DeRef(_31424);
    _31424 = NOVALUE;
    DeRef(_31436);
    _31436 = NOVALUE;
    DeRef(_31374);
    _31374 = NOVALUE;
    DeRef(_31400);
    _31400 = NOVALUE;
    DeRef(_31377);
    _31377 = NOVALUE;
    DeRef(_31441);
    _31441 = NOVALUE;
    DeRef(_31387);
    _31387 = NOVALUE;
    DeRef(_31405);
    _31405 = NOVALUE;
    DeRef(_31432);
    _31432 = NOVALUE;
    return;
    ;
}


void _41parser()
{
    int _0, _1, _2;
    

    /** 	real_parser(0)*/
    _41real_parser(0);

    /** 	mark_final_targets()*/
    _55mark_final_targets();

    /** 	resolve_unincluded_globals( 1 )*/
    _55resolve_unincluded_globals(1);

    /** 	Resolve_forward_references( 1 )*/
    _40Resolve_forward_references(1);

    /** 	inline_deferred_calls()*/
    _69inline_deferred_calls();

    /** 	End_block( PROC )*/
    _68End_block(27);

    /** 	Code = {}*/
    RefDS(_22663);
    DeRef(_38Code_17038);
    _38Code_17038 = _22663;

    /** 	LineTable = {}*/
    RefDS(_22663);
    DeRef(_38LineTable_17039);
    _38LineTable_17039 = _22663;

    /** end procedure*/
    return;
    ;
}


void _41nested_parser()
{
    int _0, _1, _2;
    

    /** 	real_parser(1)*/
    _41real_parser(1);

    /** end procedure*/
    return;
    ;
}



// 0x7D665463
