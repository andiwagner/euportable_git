// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _72set_out_dir(int _out_65422)
{
    int _32913 = NOVALUE;
    int _32912 = NOVALUE;
    int _32911 = NOVALUE;
    int _32910 = NOVALUE;
    int _32909 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length( out ) and out[$] != SLASH then*/
    if (IS_SEQUENCE(_out_65422)){
            _32909 = SEQ_PTR(_out_65422)->length;
    }
    else {
        _32909 = 1;
    }
    if (_32909 == 0) {
        goto L1; // [8] 38
    }
    if (IS_SEQUENCE(_out_65422)){
            _32911 = SEQ_PTR(_out_65422)->length;
    }
    else {
        _32911 = 1;
    }
    _2 = (int)SEQ_PTR(_out_65422);
    _32912 = (int)*(((s1_ptr)_2)->base + _32911);
    if (IS_ATOM_INT(_32912)) {
        _32913 = (_32912 != 92);
    }
    else {
        _32913 = binary_op(NOTEQ, _32912, 92);
    }
    _32912 = NOVALUE;
    if (_32913 == 0) {
        DeRef(_32913);
        _32913 = NOVALUE;
        goto L1; // [26] 38
    }
    else {
        if (!IS_ATOM_INT(_32913) && DBL_PTR(_32913)->dbl == 0.0){
            DeRef(_32913);
            _32913 = NOVALUE;
            goto L1; // [26] 38
        }
        DeRef(_32913);
        _32913 = NOVALUE;
    }
    DeRef(_32913);
    _32913 = NOVALUE;

    /** 		out &= SLASH*/
    Append(&_out_65422, _out_65422, 92);
L1: 

    /** 	out_dir = out*/
    RefDS(_out_65422);
    DeRef(_72out_dir_65413);
    _72out_dir_65413 = _out_65422;

    /** 	return 0*/
    DeRefDS(_out_65422);
    return 0;
    ;
}


int _72suppress_dependencies(int _o_65434)
{
    int _0, _1, _2;
    

    /** 	show_dependencies = 0*/
    _72show_dependencies_65417 = 0;

    /** 	return 0*/
    return 0;
    ;
}


int _72suppress_callgraphs(int _o_65437)
{
    int _0, _1, _2;
    

    /** 	show_callgraphs = 0*/
    _72show_callgraphs_65418 = 0;

    /** 	return 0*/
    return 0;
    ;
}


int _72suppress_stdlib(int _o_65440)
{
    int _0, _1, _2;
    

    /** 	show_stdlib = 1*/
    _71show_stdlib_65018 = 1;

    /** 	return 0*/
    return 0;
    ;
}


int _72document_file(int _name_65444)
{
    int _32915 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not find( name, files ) then*/
    _32915 = find_from(_name_65444, _72files_65419, 1);
    if (_32915 != 0)
    goto L1; // [12] 24
    _32915 = NOVALUE;

    /** 		files = append( files, name )*/
    RefDS(_name_65444);
    Append(&_72files_65419, _72files_65419, _name_65444);
L1: 

    /** 	return 0*/
    DeRefDS(_name_65444);
    return 0;
    ;
}


int _72dir_exists(int _path_65451)
{
    int _32919 = NOVALUE;
    int _32918 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return file_type( path ) > 0*/
    RefDS(_path_65451);
    _32918 = _9file_type(_path_65451);
    if (IS_ATOM_INT(_32918)) {
        _32919 = (_32918 > 0);
    }
    else {
        _32919 = binary_op(GREATER, _32918, 0);
    }
    DeRef(_32918);
    _32918 = NOVALUE;
    DeRefDS(_path_65451);
    return _32919;
    ;
}


void _72make_dir(int _path_65457)
{
    int _32924 = NOVALUE;
    int _32923 = NOVALUE;
    int _32920 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not create_directory( path ) then*/
    RefDS(_path_65457);
    _32920 = _9create_directory(_path_65457, 448, 1);
    if (IS_ATOM_INT(_32920)) {
        if (_32920 != 0){
            DeRef(_32920);
            _32920 = NOVALUE;
            goto L1; // [11] 29
        }
    }
    else {
        if (DBL_PTR(_32920)->dbl != 0.0){
            DeRef(_32920);
            _32920 = NOVALUE;
            goto L1; // [11] 29
        }
    }
    DeRef(_32920);
    _32920 = NOVALUE;

    /** 		crash( sprintf( "could not create directory '%s'", {path} ) )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_path_65457);
    *((int *)(_2+4)) = _path_65457;
    _32923 = MAKE_SEQ(_1);
    _32924 = EPrintf(-9999999, _32922, _32923);
    DeRefDS(_32923);
    _32923 = NOVALUE;
    RefDS(_22682);
    _8crash(_32924, _22682);
    _32924 = NOVALUE;
L1: 

    /** end procedure*/
    DeRefDS(_path_65457);
    return;
    ;
}


void _72make_dirs()
{
    int _32935 = NOVALUE;
    int _32934 = NOVALUE;
    int _32933 = NOVALUE;
    int _32932 = NOVALUE;
    int _32930 = NOVALUE;
    int _32929 = NOVALUE;
    int _32928 = NOVALUE;
    int _32927 = NOVALUE;
    int _32926 = NOVALUE;
    int _32925 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length( out_dir ) and not dir_exists( out_dir ) then*/
    if (IS_SEQUENCE(_72out_dir_65413)){
            _32925 = SEQ_PTR(_72out_dir_65413)->length;
    }
    else {
        _32925 = 1;
    }
    if (_32925 == 0) {
        goto L1; // [8] 33
    }
    RefDS(_72out_dir_65413);
    _32927 = _72dir_exists(_72out_dir_65413);
    if (IS_ATOM_INT(_32927)) {
        _32928 = (_32927 == 0);
    }
    else {
        _32928 = unary_op(NOT, _32927);
    }
    DeRef(_32927);
    _32927 = NOVALUE;
    if (_32928 == 0) {
        DeRef(_32928);
        _32928 = NOVALUE;
        goto L1; // [22] 33
    }
    else {
        if (!IS_ATOM_INT(_32928) && DBL_PTR(_32928)->dbl == 0.0){
            DeRef(_32928);
            _32928 = NOVALUE;
            goto L1; // [22] 33
        }
        DeRef(_32928);
        _32928 = NOVALUE;
    }
    DeRef(_32928);
    _32928 = NOVALUE;

    /** 		make_dir( out_dir )*/
    RefDS(_72out_dir_65413);
    _72make_dir(_72out_dir_65413);
L1: 

    /** 	if (show_dependencies or show_callgraphs) and not dir_exists( out_dir & "image" ) then*/
    if (_72show_dependencies_65417 != 0) {
        _32929 = 1;
        goto L2; // [37] 47
    }
    _32929 = (_72show_callgraphs_65418 != 0);
L2: 
    if (_32929 == 0) {
        goto L3; // [47] 80
    }
    Concat((object_ptr)&_32932, _72out_dir_65413, _32931);
    _32933 = _72dir_exists(_32932);
    _32932 = NOVALUE;
    if (IS_ATOM_INT(_32933)) {
        _32934 = (_32933 == 0);
    }
    else {
        _32934 = unary_op(NOT, _32933);
    }
    DeRef(_32933);
    _32933 = NOVALUE;
    if (_32934 == 0) {
        DeRef(_32934);
        _32934 = NOVALUE;
        goto L3; // [65] 80
    }
    else {
        if (!IS_ATOM_INT(_32934) && DBL_PTR(_32934)->dbl == 0.0){
            DeRef(_32934);
            _32934 = NOVALUE;
            goto L3; // [65] 80
        }
        DeRef(_32934);
        _32934 = NOVALUE;
    }
    DeRef(_32934);
    _32934 = NOVALUE;

    /** 		make_dir( out_dir & "image" )*/
    Concat((object_ptr)&_32935, _72out_dir_65413, _32931);
    _72make_dir(_32935);
    _32935 = NOVALUE;
L3: 

    /** end procedure*/
    return;
    ;
}


int _72safe_open(int _name_65483)
{
    int _fn_65484 = NOVALUE;
    int _32941 = NOVALUE;
    int _32940 = NOVALUE;
    int _32936 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer fn = open( out_dir & name, "w" )*/
    Concat((object_ptr)&_32936, _72out_dir_65413, _name_65483);
    _fn_65484 = EOpen(_32936, _22788, 0);
    DeRefDS(_32936);
    _32936 = NOVALUE;

    /** 	if fn = -1 then*/
    if (_fn_65484 != -1)
    goto L1; // [18] 37

    /** 		crash( sprintf( "Could not open file '%s' for writing", {name} ) )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_name_65483);
    *((int *)(_2+4)) = _name_65483;
    _32940 = MAKE_SEQ(_1);
    _32941 = EPrintf(-9999999, _32939, _32940);
    DeRefDS(_32940);
    _32940 = NOVALUE;
    RefDS(_22682);
    _8crash(_32941, _22682);
    _32941 = NOVALUE;
L1: 

    /** 	return fn*/
    DeRefDS(_name_65483);
    return _fn_65484;
    ;
}


void _72header(int _fn_65495, int _format_65496, int _data_65497)
{
    int _title_65498 = NOVALUE;
    int _32944 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence title = sprintf( format, data )*/
    DeRefi(_title_65498);
    _title_65498 = EPrintf(-9999999, _format_65496, _data_65497);

    /** 	printf( fn, "<html><head><title>%s</title></head>\n", { title } )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_title_65498);
    *((int *)(_2+4)) = _title_65498;
    _32944 = MAKE_SEQ(_1);
    EPrintf(_fn_65495, _32943, _32944);
    DeRefDS(_32944);
    _32944 = NOVALUE;

    /** 	puts( fn, "<body>\n")*/
    EPuts(_fn_65495, _32945); // DJP 

    /** end procedure*/
    DeRefDS(_format_65496);
    DeRefDS(_data_65497);
    DeRefDSi(_title_65498);
    return;
    ;
}


int _72underscore_name(int _name_65509)
{
    int _0, _1, _2;
    

    /** 	name = match_replace( '\\', name, '_' )*/
    RefDS(_name_65509);
    _0 = _name_65509;
    _name_65509 = _7match_replace(92, _name_65509, 95, 0);
    DeRefDS(_0);

    /** 	name = match_replace( '/', name,  '_' )*/
    RefDS(_name_65509);
    _0 = _name_65509;
    _name_65509 = _7match_replace(47, _name_65509, 95, 0);
    DeRefDS(_0);

    /** 	return name*/
    return _name_65509;
    ;
}


void _72write_index()
{
    int _link_1__tmp_at177_65566 = NOVALUE;
    int _link_inlined_link_at_177_65565 = NOVALUE;
    int _name_inlined_link_at_174_65564 = NOVALUE;
    int _fn_65522 = NOVALUE;
    int _files_65536 = NOVALUE;
    int _32982 = NOVALUE;
    int _32981 = NOVALUE;
    int _32980 = NOVALUE;
    int _32979 = NOVALUE;
    int _32978 = NOVALUE;
    int _32976 = NOVALUE;
    int _32975 = NOVALUE;
    int _32974 = NOVALUE;
    int _32973 = NOVALUE;
    int _32971 = NOVALUE;
    int _32969 = NOVALUE;
    int _32968 = NOVALUE;
    int _32967 = NOVALUE;
    int _32966 = NOVALUE;
    int _32965 = NOVALUE;
    int _32964 = NOVALUE;
    int _32963 = NOVALUE;
    int _32959 = NOVALUE;
    int _32958 = NOVALUE;
    int _32956 = NOVALUE;
    int _32955 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer fn = safe_open( "index.html" )*/
    RefDS(_32952);
    _fn_65522 = _72safe_open(_32952);
    if (!IS_ATOM_INT(_fn_65522)) {
        _1 = (long)(DBL_PTR(_fn_65522)->dbl);
        if (UNIQUE(DBL_PTR(_fn_65522)) && (DBL_PTR(_fn_65522)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fn_65522);
        _fn_65522 = _1;
    }

    /** 	header( fn, "%s EuDox", {short_names[1]} )*/
    _2 = (int)SEQ_PTR(_71short_names_65016);
    _32955 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_32955);
    *((int *)(_2+4)) = _32955;
    _32956 = MAKE_SEQ(_1);
    _32955 = NOVALUE;
    RefDS(_32954);
    _72header(_fn_65522, _32954, _32956);
    _32956 = NOVALUE;

    /** 	printf( fn, "<h1>Documentation for %s</h1>\n", { short_names[1] } )*/
    _2 = (int)SEQ_PTR(_71short_names_65016);
    _32958 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_32958);
    *((int *)(_2+4)) = _32958;
    _32959 = MAKE_SEQ(_1);
    _32958 = NOVALUE;
    EPrintf(_fn_65522, _32957, _32959);
    DeRefDS(_32959);
    _32959 = NOVALUE;

    /** 	puts( fn, "<table><tr style=\"vertical-align:top;\"><td >\n" )*/
    EPuts(_fn_65522, _32960); // DJP 

    /** 	puts( fn, "<h2>Files:</h2>\n" )*/
    EPuts(_fn_65522, _32961); // DJP 

    /** 	puts( fn, "<ul>\n" )*/
    EPuts(_fn_65522, _32962); // DJP 

    /** 	sequence files = short_names*/
    RefDS(_71short_names_65016);
    DeRef(_files_65536);
    _files_65536 = _71short_names_65016;

    /** 	for i = 1 to length( files ) do*/
    if (IS_SEQUENCE(_files_65536)){
            _32963 = SEQ_PTR(_files_65536)->length;
    }
    else {
        _32963 = 1;
    }
    {
        int _i_65539;
        _i_65539 = 1;
L1: 
        if (_i_65539 > _32963){
            goto L2; // [71] 99
        }

        /** 		files[i] = { files[i], i }*/
        _2 = (int)SEQ_PTR(_files_65536);
        _32964 = (int)*(((s1_ptr)_2)->base + _i_65539);
        Ref(_32964);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _32964;
        ((int *)_2)[2] = _i_65539;
        _32965 = MAKE_SEQ(_1);
        _32964 = NOVALUE;
        _2 = (int)SEQ_PTR(_files_65536);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _files_65536 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_65539);
        _1 = *(int *)_2;
        *(int *)_2 = _32965;
        if( _1 != _32965 ){
            DeRef(_1);
        }
        _32965 = NOVALUE;

        /** 	end for*/
        _i_65539 = _i_65539 + 1;
        goto L1; // [94] 78
L2: 
        ;
    }

    /** 	files = files[1..1] & sort( files[2..$] )*/
    rhs_slice_target = (object_ptr)&_32966;
    RHS_Slice(_files_65536, 1, 1);
    if (IS_SEQUENCE(_files_65536)){
            _32967 = SEQ_PTR(_files_65536)->length;
    }
    else {
        _32967 = 1;
    }
    rhs_slice_target = (object_ptr)&_32968;
    RHS_Slice(_files_65536, 2, _32967);
    _32969 = _22sort(_32968, 1);
    _32968 = NOVALUE;
    if (IS_SEQUENCE(_32966) && IS_ATOM(_32969)) {
        Ref(_32969);
        Append(&_files_65536, _32966, _32969);
    }
    else if (IS_ATOM(_32966) && IS_SEQUENCE(_32969)) {
    }
    else {
        Concat((object_ptr)&_files_65536, _32966, _32969);
        DeRefDS(_32966);
        _32966 = NOVALUE;
    }
    DeRef(_32966);
    _32966 = NOVALUE;
    DeRef(_32969);
    _32969 = NOVALUE;

    /** 	for i = 1 to length( files ) do*/
    if (IS_SEQUENCE(_files_65536)){
            _32971 = SEQ_PTR(_files_65536)->length;
    }
    else {
        _32971 = 1;
    }
    {
        int _i_65550;
        _i_65550 = 1;
L3: 
        if (_i_65550 > _32971){
            goto L4; // [128] 217
        }

        /** 		if show_stdlib or (not std_libs[files[i][2]]) then*/
        if (_71show_stdlib_65018 != 0) {
            goto L5; // [139] 165
        }
        _2 = (int)SEQ_PTR(_files_65536);
        _32973 = (int)*(((s1_ptr)_2)->base + _i_65550);
        _2 = (int)SEQ_PTR(_32973);
        _32974 = (int)*(((s1_ptr)_2)->base + 2);
        _32973 = NOVALUE;
        _2 = (int)SEQ_PTR(_71std_libs_65017);
        if (!IS_ATOM_INT(_32974)){
            _32975 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32974)->dbl));
        }
        else{
            _32975 = (int)*(((s1_ptr)_2)->base + _32974);
        }
        _32976 = (_32975 == 0);
        _32975 = NOVALUE;
        if (_32976 == 0)
        {
            DeRef(_32976);
            _32976 = NOVALUE;
            goto L6; // [161] 210
        }
        else{
            DeRef(_32976);
            _32976 = NOVALUE;
        }
L5: 

        /** 			printf( fn, "<li><a href=\"%s\">%s</a></li>\n", { link( files[i][1] ), files[i][1] } )*/
        _2 = (int)SEQ_PTR(_files_65536);
        _32978 = (int)*(((s1_ptr)_2)->base + _i_65550);
        _2 = (int)SEQ_PTR(_32978);
        _32979 = (int)*(((s1_ptr)_2)->base + 1);
        _32978 = NOVALUE;
        Ref(_32979);
        DeRef(_name_inlined_link_at_174_65564);
        _name_inlined_link_at_174_65564 = _32979;
        _32979 = NOVALUE;

        /** 	return underscore_name( name ) & ".html"*/
        Ref(_name_inlined_link_at_174_65564);
        _0 = _link_1__tmp_at177_65566;
        _link_1__tmp_at177_65566 = _72underscore_name(_name_inlined_link_at_174_65564);
        DeRef(_0);
        if (IS_SEQUENCE(_link_1__tmp_at177_65566) && IS_ATOM(_32950)) {
        }
        else if (IS_ATOM(_link_1__tmp_at177_65566) && IS_SEQUENCE(_32950)) {
            Ref(_link_1__tmp_at177_65566);
            Prepend(&_link_inlined_link_at_177_65565, _32950, _link_1__tmp_at177_65566);
        }
        else {
            Concat((object_ptr)&_link_inlined_link_at_177_65565, _link_1__tmp_at177_65566, _32950);
        }
        DeRef(_name_inlined_link_at_174_65564);
        _name_inlined_link_at_174_65564 = NOVALUE;
        DeRef(_link_1__tmp_at177_65566);
        _link_1__tmp_at177_65566 = NOVALUE;
        _2 = (int)SEQ_PTR(_files_65536);
        _32980 = (int)*(((s1_ptr)_2)->base + _i_65550);
        _2 = (int)SEQ_PTR(_32980);
        _32981 = (int)*(((s1_ptr)_2)->base + 1);
        _32980 = NOVALUE;
        Ref(_32981);
        RefDS(_link_inlined_link_at_177_65565);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _link_inlined_link_at_177_65565;
        ((int *)_2)[2] = _32981;
        _32982 = MAKE_SEQ(_1);
        _32981 = NOVALUE;
        EPrintf(_fn_65522, _32977, _32982);
        DeRefDS(_32982);
        _32982 = NOVALUE;
L6: 

        /** 	end for*/
        _i_65550 = _i_65550 + 1;
        goto L3; // [212] 135
L4: 
        ;
    }

    /** 	puts( fn, "</ul>\n" )*/
    EPuts(_fn_65522, _32983); // DJP 

    /** 	puts( fn, "</td><td>\n" )*/
    EPuts(_fn_65522, _32984); // DJP 

    /** 	if show_dependencies then*/
    if (_72show_dependencies_65417 == 0)
    {
        goto L7; // [231] 245
    }
    else{
    }

    /** 		puts( fn, "<h2>Overall file dependency graph:</h2>\n" )*/
    EPuts(_fn_65522, _32985); // DJP 

    /** 		puts( fn, "<a href=\"image/all_dep.png\"><img src=\"image/all_dep.png\" style=\"max-width: 800px;\"/></a>\n" )*/
    EPuts(_fn_65522, _32986); // DJP 
L7: 

    /** 	puts( fn, "</td></tr></table>\n" )*/
    EPuts(_fn_65522, _32987); // DJP 

    /** 	footer( fn )*/

    /** 	puts( fn, "</body></html>\n" )*/
    EPuts(_fn_65522, _32946); // DJP 

    /** end procedure*/
    goto L8; // [259] 262
L8: 

    /** 	close( fn )*/
    EClose(_fn_65522);

    /** end procedure*/
    DeRef(_files_65536);
    _32974 = NOVALUE;
    return;
    ;
}


void _72make_routine_map()
{
    int _33009 = NOVALUE;
    int _33008 = NOVALUE;
    int _33006 = NOVALUE;
    int _33005 = NOVALUE;
    int _33004 = NOVALUE;
    int _33003 = NOVALUE;
    int _33002 = NOVALUE;
    int _33001 = NOVALUE;
    int _33000 = NOVALUE;
    int _32999 = NOVALUE;
    int _32998 = NOVALUE;
    int _32997 = NOVALUE;
    int _32996 = NOVALUE;
    int _32995 = NOVALUE;
    int _32994 = NOVALUE;
    int _32993 = NOVALUE;
    int _32992 = NOVALUE;
    int _32991 = NOVALUE;
    int _32990 = NOVALUE;
    int _32989 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for s = length( keylist ) + 1 to length( SymTab ) do*/
    if (IS_SEQUENCE(_63keylist_23599)){
            _32989 = SEQ_PTR(_63keylist_23599)->length;
    }
    else {
        _32989 = 1;
    }
    _32990 = _32989 + 1;
    _32989 = NOVALUE;
    if (IS_SEQUENCE(_26SymTab_11138)){
            _32991 = SEQ_PTR(_26SymTab_11138)->length;
    }
    else {
        _32991 = 1;
    }
    {
        int _s_65582;
        _s_65582 = _32990;
L1: 
        if (_s_65582 > _32991){
            goto L2; // [17] 163
        }

        /** 		if length( SymTab[s] ) = SIZEOF_ROUTINE_ENTRY and find( SymTab[s][S_TOKEN], {FUNC, PROC, TYPE})*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _32992 = (int)*(((s1_ptr)_2)->base + _s_65582);
        if (IS_SEQUENCE(_32992)){
                _32993 = SEQ_PTR(_32992)->length;
        }
        else {
            _32993 = 1;
        }
        _32992 = NOVALUE;
        _32994 = (_32993 == _25SIZEOF_ROUTINE_ENTRY_12038);
        _32993 = NOVALUE;
        if (_32994 == 0) {
            _32995 = 0;
            goto L3; // [41] 78
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _32996 = (int)*(((s1_ptr)_2)->base + _s_65582);
        _2 = (int)SEQ_PTR(_32996);
        if (!IS_ATOM_INT(_25S_TOKEN_11918)){
            _32997 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
        }
        else{
            _32997 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
        }
        _32996 = NOVALUE;
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = 501;
        *((int *)(_2+8)) = 27;
        *((int *)(_2+12)) = 504;
        _32998 = MAKE_SEQ(_1);
        _32999 = find_from(_32997, _32998, 1);
        _32997 = NOVALUE;
        DeRefDS(_32998);
        _32998 = NOVALUE;
        _32995 = (_32999 != 0);
L3: 
        if (_32995 == 0) {
            goto L4; // [78] 156
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _33001 = (int)*(((s1_ptr)_2)->base + _s_65582);
        _2 = (int)SEQ_PTR(_33001);
        _33002 = (int)*(((s1_ptr)_2)->base + 4);
        _33001 = NOVALUE;
        if (IS_ATOM_INT(_33002)) {
            _33003 = (_33002 != 7);
        }
        else {
            _33003 = binary_op(NOTEQ, _33002, 7);
        }
        _33002 = NOVALUE;
        if (_33003 == 0) {
            DeRef(_33003);
            _33003 = NOVALUE;
            goto L4; // [101] 156
        }
        else {
            if (!IS_ATOM_INT(_33003) && DBL_PTR(_33003)->dbl == 0.0){
                DeRef(_33003);
                _33003 = NOVALUE;
                goto L4; // [101] 156
            }
            DeRef(_33003);
            _33003 = NOVALUE;
        }
        DeRef(_33003);
        _33003 = NOVALUE;

        /** 			if not std_libs[ SymTab[s][S_FILE_NO] ] then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _33004 = (int)*(((s1_ptr)_2)->base + _s_65582);
        _2 = (int)SEQ_PTR(_33004);
        if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
            _33005 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
        }
        else{
            _33005 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
        }
        _33004 = NOVALUE;
        _2 = (int)SEQ_PTR(_71std_libs_65017);
        if (!IS_ATOM_INT(_33005)){
            _33006 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33005)->dbl));
        }
        else{
            _33006 = (int)*(((s1_ptr)_2)->base + _33005);
        }
        if (_33006 != 0)
        goto L5; // [124] 155
        _33006 = NOVALUE;

        /** 				map:put( routine_map, SymTab[s][S_FILE_NO], s, map:APPEND )*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _33008 = (int)*(((s1_ptr)_2)->base + _s_65582);
        _2 = (int)SEQ_PTR(_33008);
        if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
            _33009 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
        }
        else{
            _33009 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
        }
        _33008 = NOVALUE;
        Ref(_72routine_map_65577);
        Ref(_33009);
        _32put(_72routine_map_65577, _33009, _s_65582, 6, _32threshold_size_13318);
        _33009 = NOVALUE;
L5: 
L4: 

        /** 	end for*/
        _s_65582 = _s_65582 + 1;
        goto L1; // [158] 24
L2: 
        ;
    }

    /** end procedure*/
    DeRef(_32990);
    _32990 = NOVALUE;
    _32992 = NOVALUE;
    DeRef(_32994);
    _32994 = NOVALUE;
    _33005 = NOVALUE;
    return;
    ;
}


int _72signature(int _proc_65632)
{
    int _e_65634 = NOVALUE;
    int _sig_65637 = NOVALUE;
    int _args_65649 = NOVALUE;
    int _arg_sym_65654 = NOVALUE;
    int _arg_65658 = NOVALUE;
    int _33038 = NOVALUE;
    int _33034 = NOVALUE;
    int _33033 = NOVALUE;
    int _33032 = NOVALUE;
    int _33031 = NOVALUE;
    int _33030 = NOVALUE;
    int _33020 = NOVALUE;
    int _33019 = NOVALUE;
    int _33018 = NOVALUE;
    int _33017 = NOVALUE;
    int _33014 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_proc_65632)) {
        _1 = (long)(DBL_PTR(_proc_65632)->dbl);
        if (UNIQUE(DBL_PTR(_proc_65632)) && (DBL_PTR(_proc_65632)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_proc_65632);
        _proc_65632 = _1;
    }

    /** 	symtab_entry e = SymTab[proc]*/
    DeRef(_e_65634);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _e_65634 = (int)*(((s1_ptr)_2)->base + _proc_65632);
    Ref(_e_65634);

    /** 	sequence sig = map:get( scope_map, e[S_SCOPE], "" )*/
    _2 = (int)SEQ_PTR(_e_65634);
    _33014 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_72scope_map_65623);
    Ref(_33014);
    RefDS(_22682);
    _0 = _sig_65637;
    _sig_65637 = _32get(_72scope_map_65623, _33014, _22682);
    DeRef(_0);
    _33014 = NOVALUE;

    /** 	sig &= sprintf( "<a name=\"%d\">%s</a>(", { proc, SymTab[proc][S_NAME] } )*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _33017 = (int)*(((s1_ptr)_2)->base + _proc_65632);
    _2 = (int)SEQ_PTR(_33017);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _33018 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _33018 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _33017 = NOVALUE;
    Ref(_33018);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _proc_65632;
    ((int *)_2)[2] = _33018;
    _33019 = MAKE_SEQ(_1);
    _33018 = NOVALUE;
    _33020 = EPrintf(-9999999, _33016, _33019);
    DeRefDS(_33019);
    _33019 = NOVALUE;
    Concat((object_ptr)&_sig_65637, _sig_65637, _33020);
    DeRefDS(_33020);
    _33020 = NOVALUE;

    /** 	integer args = e[S_NUM_ARGS]*/
    _2 = (int)SEQ_PTR(_e_65634);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _args_65649 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _args_65649 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    if (!IS_ATOM_INT(_args_65649)){
        _args_65649 = (long)DBL_PTR(_args_65649)->dbl;
    }

    /** 	if args then*/
    if (_args_65649 == 0)
    {
        goto L1; // [69] 178
    }
    else{
    }

    /** 		symtab_index arg_sym = e[S_NEXT]*/
    _2 = (int)SEQ_PTR(_e_65634);
    _arg_sym_65654 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_arg_sym_65654)){
        _arg_sym_65654 = (long)DBL_PTR(_arg_sym_65654)->dbl;
    }

    /** 		symtab_entry arg = SymTab[arg_sym]*/
    DeRef(_arg_65658);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _arg_65658 = (int)*(((s1_ptr)_2)->base + _arg_sym_65654);
    Ref(_arg_65658);

    /** 		while args > 0 with entry do*/
    goto L2; // [94] 130
L3: 
    if (_args_65649 <= 0)
    goto L4; // [97] 177

    /** 			sig &= ", "*/
    Concat((object_ptr)&_sig_65637, _sig_65637, _33026);

    /** 			arg_sym = arg[S_NEXT]*/
    _2 = (int)SEQ_PTR(_arg_65658);
    _arg_sym_65654 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_arg_sym_65654)){
        _arg_sym_65654 = (long)DBL_PTR(_arg_sym_65654)->dbl;
    }

    /** 			arg = SymTab[arg_sym]*/
    DeRefDS(_arg_65658);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _arg_65658 = (int)*(((s1_ptr)_2)->base + _arg_sym_65654);
    Ref(_arg_65658);

    /** 		entry*/
L2: 

    /** 			sig &= SymTab[arg[S_VTYPE]][S_NAME] & " " & arg[S_NAME]*/
    _2 = (int)SEQ_PTR(_arg_65658);
    _33030 = (int)*(((s1_ptr)_2)->base + 15);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_33030)){
        _33031 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33030)->dbl));
    }
    else{
        _33031 = (int)*(((s1_ptr)_2)->base + _33030);
    }
    _2 = (int)SEQ_PTR(_33031);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _33032 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _33032 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _33031 = NOVALUE;
    _2 = (int)SEQ_PTR(_arg_65658);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _33033 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _33033 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    {
        int concat_list[3];

        concat_list[0] = _33033;
        concat_list[1] = _23968;
        concat_list[2] = _33032;
        Concat_N((object_ptr)&_33034, concat_list, 3);
    }
    _33033 = NOVALUE;
    _33032 = NOVALUE;
    Concat((object_ptr)&_sig_65637, _sig_65637, _33034);
    DeRefDS(_33034);
    _33034 = NOVALUE;

    /** 			args -= 1*/
    _args_65649 = _args_65649 - 1;

    /** 		end while*/
    goto L3; // [174] 97
L4: 
L1: 
    DeRef(_arg_65658);
    _arg_65658 = NOVALUE;

    /** 	return sig & " )"	*/
    Concat((object_ptr)&_33038, _sig_65637, _33037);
    DeRef(_e_65634);
    DeRefDS(_sig_65637);
    _33030 = NOVALUE;
    return _33038;
    ;
}


int _72routine_ref(int _f_65684, int _proc_65686, int _call_map_65687)
{
    int _file_map_65688 = NOVALUE;
    int _files_65693 = NOVALUE;
    int _names_65695 = NOVALUE;
    int _fn_65699 = NOVALUE;
    int _proc_map_65705 = NOVALUE;
    int _procs_65709 = NOVALUE;
    int _psym_65715 = NOVALUE;
    int _33062 = NOVALUE;
    int _33061 = NOVALUE;
    int _33060 = NOVALUE;
    int _33057 = NOVALUE;
    int _33056 = NOVALUE;
    int _33055 = NOVALUE;
    int _33053 = NOVALUE;
    int _33052 = NOVALUE;
    int _33050 = NOVALUE;
    int _33047 = NOVALUE;
    int _33045 = NOVALUE;
    int _33043 = NOVALUE;
    int _33040 = NOVALUE;
    int _33039 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_proc_65686)) {
        _1 = (long)(DBL_PTR(_proc_65686)->dbl);
        if (UNIQUE(DBL_PTR(_proc_65686)) && (DBL_PTR(_proc_65686)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_proc_65686);
        _proc_65686 = _1;
    }

    /** 	map:map file_map = new_extra( map:nested_get( call_map, {f, proc}) )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _f_65684;
    ((int *)_2)[2] = _proc_65686;
    _33039 = MAKE_SEQ(_1);
    Ref(_call_map_65687);
    _33040 = _32nested_get(_call_map_65687, _33039, 0);
    _33039 = NOVALUE;
    _0 = _file_map_65688;
    _file_map_65688 = _32new_extra(_33040, 690);
    DeRef(_0);
    _33040 = NOVALUE;

    /** 	sequence files = map:keys( file_map )*/
    Ref(_file_map_65688);
    _0 = _files_65693;
    _files_65693 = _32keys(_file_map_65688, 0);
    DeRef(_0);

    /** 	sequence names = {}*/
    RefDS(_22682);
    DeRef(_names_65695);
    _names_65695 = _22682;

    /** 	for fx = 1 to length( files ) do*/
    if (IS_SEQUENCE(_files_65693)){
            _33043 = SEQ_PTR(_files_65693)->length;
    }
    else {
        _33043 = 1;
    }
    {
        int _fx_65697;
        _fx_65697 = 1;
L1: 
        if (_fx_65697 > _33043){
            goto L2; // [43] 173
        }

        /** 		integer fn = files[fx]*/
        _2 = (int)SEQ_PTR(_files_65693);
        _fn_65699 = (int)*(((s1_ptr)_2)->base + _fx_65697);
        if (!IS_ATOM_INT(_fn_65699))
        _fn_65699 = (long)DBL_PTR(_fn_65699)->dbl;

        /** 		if not std_libs[fn] then*/
        _2 = (int)SEQ_PTR(_71std_libs_65017);
        _33045 = (int)*(((s1_ptr)_2)->base + _fn_65699);
        if (_33045 != 0)
        goto L3; // [64] 162
        _33045 = NOVALUE;

        /** 			map:map proc_map = new_extra( map:get( file_map, fn) )*/
        Ref(_file_map_65688);
        _33047 = _32get(_file_map_65688, _fn_65699, 0);
        _0 = _proc_map_65705;
        _proc_map_65705 = _32new_extra(_33047, 690);
        DeRef(_0);
        _33047 = NOVALUE;

        /** 			sequence procs = map:keys( proc_map )*/
        Ref(_proc_map_65705);
        _0 = _procs_65709;
        _procs_65709 = _32keys(_proc_map_65705, 0);
        DeRef(_0);

        /** 			for p = 1 to length( procs ) do*/
        if (IS_SEQUENCE(_procs_65709)){
                _33050 = SEQ_PTR(_procs_65709)->length;
        }
        else {
            _33050 = 1;
        }
        {
            int _p_65712;
            _p_65712 = 1;
L4: 
            if (_p_65712 > _33050){
                goto L5; // [94] 161
            }

            /** 				symtab_index psym = procs[p]*/
            _2 = (int)SEQ_PTR(_procs_65709);
            _psym_65715 = (int)*(((s1_ptr)_2)->base + _p_65712);
            if (!IS_ATOM_INT(_psym_65715)){
                _psym_65715 = (long)DBL_PTR(_psym_65715)->dbl;
            }

            /** 				if SymTab[psym][S_SCOPE] != SC_PREDEF then*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _33052 = (int)*(((s1_ptr)_2)->base + _psym_65715);
            _2 = (int)SEQ_PTR(_33052);
            _33053 = (int)*(((s1_ptr)_2)->base + 4);
            _33052 = NOVALUE;
            if (binary_op_a(EQUALS, _33053, 7)){
                _33053 = NOVALUE;
                goto L6; // [125] 152
            }
            _33053 = NOVALUE;

            /** 					names = append( names, { SymTab[psym][S_NAME], psym } )*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _33055 = (int)*(((s1_ptr)_2)->base + _psym_65715);
            _2 = (int)SEQ_PTR(_33055);
            if (!IS_ATOM_INT(_25S_NAME_11913)){
                _33056 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
            }
            else{
                _33056 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
            }
            _33055 = NOVALUE;
            Ref(_33056);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _33056;
            ((int *)_2)[2] = _psym_65715;
            _33057 = MAKE_SEQ(_1);
            _33056 = NOVALUE;
            RefDS(_33057);
            Append(&_names_65695, _names_65695, _33057);
            DeRefDS(_33057);
            _33057 = NOVALUE;
L6: 

            /** 			end for*/
            _p_65712 = _p_65712 + 1;
            goto L4; // [156] 101
L5: 
            ;
        }
L3: 
        DeRef(_proc_map_65705);
        _proc_map_65705 = NOVALUE;
        DeRef(_procs_65709);
        _procs_65709 = NOVALUE;

        /** 	end for*/
        _fx_65697 = _fx_65697 + 1;
        goto L1; // [168] 50
L2: 
        ;
    }

    /** 	names = sort( names )*/
    RefDS(_names_65695);
    _0 = _names_65695;
    _names_65695 = _22sort(_names_65695, 1);
    DeRefDS(_0);

    /** 	for n = 1  to length( names ) do*/
    if (IS_SEQUENCE(_names_65695)){
            _33060 = SEQ_PTR(_names_65695)->length;
    }
    else {
        _33060 = 1;
    }
    {
        int _n_65733;
        _n_65733 = 1;
L7: 
        if (_n_65733 > _33060){
            goto L8; // [187] 215
        }

        /** 		names[n] = names[n][2]*/
        _2 = (int)SEQ_PTR(_names_65695);
        _33061 = (int)*(((s1_ptr)_2)->base + _n_65733);
        _2 = (int)SEQ_PTR(_33061);
        _33062 = (int)*(((s1_ptr)_2)->base + 2);
        _33061 = NOVALUE;
        Ref(_33062);
        _2 = (int)SEQ_PTR(_names_65695);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _names_65695 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _n_65733);
        _1 = *(int *)_2;
        *(int *)_2 = _33062;
        if( _1 != _33062 ){
            DeRef(_1);
        }
        _33062 = NOVALUE;

        /** 	end for*/
        _n_65733 = _n_65733 + 1;
        goto L7; // [210] 194
L8: 
        ;
    }

    /** 	return names*/
    DeRef(_call_map_65687);
    DeRef(_file_map_65688);
    DeRef(_files_65693);
    return _names_65695;
    ;
}


int _72proc_link(int _proc_65740)
{
    int _e_65742 = NOVALUE;
    int _link_1__tmp_at31_65753 = NOVALUE;
    int _link_inlined_link_at_31_65752 = NOVALUE;
    int _name_inlined_link_at_28_65751 = NOVALUE;
    int _33069 = NOVALUE;
    int _33068 = NOVALUE;
    int _33067 = NOVALUE;
    int _33066 = NOVALUE;
    int _33065 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_proc_65740)) {
        _1 = (long)(DBL_PTR(_proc_65740)->dbl);
        if (UNIQUE(DBL_PTR(_proc_65740)) && (DBL_PTR(_proc_65740)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_proc_65740);
        _proc_65740 = _1;
    }

    /** 	symtab_entry e = SymTab[proc]*/
    DeRef(_e_65742);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _e_65742 = (int)*(((s1_ptr)_2)->base + _proc_65740);
    Ref(_e_65742);

    /** 	return sprintf( "<a href=\"%s#%d\">%s</a>", { link( short_names[e[S_FILE_NO]] ), proc, e[S_NAME] } )*/
    _2 = (int)SEQ_PTR(_e_65742);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _33065 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _33065 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _2 = (int)SEQ_PTR(_71short_names_65016);
    if (!IS_ATOM_INT(_33065)){
        _33066 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33065)->dbl));
    }
    else{
        _33066 = (int)*(((s1_ptr)_2)->base + _33065);
    }
    Ref(_33066);
    DeRef(_name_inlined_link_at_28_65751);
    _name_inlined_link_at_28_65751 = _33066;
    _33066 = NOVALUE;

    /** 	return underscore_name( name ) & ".html"*/
    Ref(_name_inlined_link_at_28_65751);
    _0 = _link_1__tmp_at31_65753;
    _link_1__tmp_at31_65753 = _72underscore_name(_name_inlined_link_at_28_65751);
    DeRef(_0);
    if (IS_SEQUENCE(_link_1__tmp_at31_65753) && IS_ATOM(_32950)) {
    }
    else if (IS_ATOM(_link_1__tmp_at31_65753) && IS_SEQUENCE(_32950)) {
        Ref(_link_1__tmp_at31_65753);
        Prepend(&_link_inlined_link_at_31_65752, _32950, _link_1__tmp_at31_65753);
    }
    else {
        Concat((object_ptr)&_link_inlined_link_at_31_65752, _link_1__tmp_at31_65753, _32950);
    }
    DeRef(_name_inlined_link_at_28_65751);
    _name_inlined_link_at_28_65751 = NOVALUE;
    DeRef(_link_1__tmp_at31_65753);
    _link_1__tmp_at31_65753 = NOVALUE;
    _2 = (int)SEQ_PTR(_e_65742);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _33067 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _33067 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_link_inlined_link_at_31_65752);
    *((int *)(_2+4)) = _link_inlined_link_at_31_65752;
    *((int *)(_2+8)) = _proc_65740;
    Ref(_33067);
    *((int *)(_2+12)) = _33067;
    _33068 = MAKE_SEQ(_1);
    _33067 = NOVALUE;
    _33069 = EPrintf(-9999999, _33064, _33068);
    DeRefDS(_33068);
    _33068 = NOVALUE;
    DeRefDS(_e_65742);
    _33065 = NOVALUE;
    return _33069;
    ;
}


void _72write_routines(int _fn_65760, int _f_65761)
{
    int _rsyms_65762 = NOVALUE;
    int _references_65788 = NOVALUE;
    int _referenced_by_65793 = NOVALUE;
    int _image_file_65818 = NOVALUE;
    int _33115 = NOVALUE;
    int _33112 = NOVALUE;
    int _33111 = NOVALUE;
    int _33110 = NOVALUE;
    int _33109 = NOVALUE;
    int _33108 = NOVALUE;
    int _33107 = NOVALUE;
    int _33104 = NOVALUE;
    int _33103 = NOVALUE;
    int _33102 = NOVALUE;
    int _33101 = NOVALUE;
    int _33097 = NOVALUE;
    int _33096 = NOVALUE;
    int _33095 = NOVALUE;
    int _33094 = NOVALUE;
    int _33091 = NOVALUE;
    int _33090 = NOVALUE;
    int _33088 = NOVALUE;
    int _33087 = NOVALUE;
    int _33086 = NOVALUE;
    int _33085 = NOVALUE;
    int _33084 = NOVALUE;
    int _33083 = NOVALUE;
    int _33081 = NOVALUE;
    int _33078 = NOVALUE;
    int _33077 = NOVALUE;
    int _33076 = NOVALUE;
    int _33075 = NOVALUE;
    int _33074 = NOVALUE;
    int _33073 = NOVALUE;
    int _33071 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence rsyms = map:get( routine_map, f, {} )*/
    Ref(_72routine_map_65577);
    RefDS(_22682);
    _0 = _rsyms_65762;
    _rsyms_65762 = _32get(_72routine_map_65577, _f_65761, _22682);
    DeRef(_0);

    /** 	if not length( rsyms) then*/
    if (IS_SEQUENCE(_rsyms_65762)){
            _33071 = SEQ_PTR(_rsyms_65762)->length;
    }
    else {
        _33071 = 1;
    }
    if (_33071 != 0)
    goto L1; // [22] 31
    _33071 = NOVALUE;

    /** 		return*/
    DeRefDS(_rsyms_65762);
    return;
L1: 

    /** 	for r = 1 to length( rsyms ) do*/
    if (IS_SEQUENCE(_rsyms_65762)){
            _33073 = SEQ_PTR(_rsyms_65762)->length;
    }
    else {
        _33073 = 1;
    }
    {
        int _r_65768;
        _r_65768 = 1;
L2: 
        if (_r_65768 > _33073){
            goto L3; // [36] 80
        }

        /** 		rsyms[r] = { SymTab[rsyms[r]][S_NAME], rsyms[r] }*/
        _2 = (int)SEQ_PTR(_rsyms_65762);
        _33074 = (int)*(((s1_ptr)_2)->base + _r_65768);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!IS_ATOM_INT(_33074)){
            _33075 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33074)->dbl));
        }
        else{
            _33075 = (int)*(((s1_ptr)_2)->base + _33074);
        }
        _2 = (int)SEQ_PTR(_33075);
        if (!IS_ATOM_INT(_25S_NAME_11913)){
            _33076 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
        }
        else{
            _33076 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
        }
        _33075 = NOVALUE;
        _2 = (int)SEQ_PTR(_rsyms_65762);
        _33077 = (int)*(((s1_ptr)_2)->base + _r_65768);
        Ref(_33077);
        Ref(_33076);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _33076;
        ((int *)_2)[2] = _33077;
        _33078 = MAKE_SEQ(_1);
        _33077 = NOVALUE;
        _33076 = NOVALUE;
        _2 = (int)SEQ_PTR(_rsyms_65762);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _rsyms_65762 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _r_65768);
        _1 = *(int *)_2;
        *(int *)_2 = _33078;
        if( _1 != _33078 ){
            DeRef(_1);
        }
        _33078 = NOVALUE;

        /** 	end for*/
        _r_65768 = _r_65768 + 1;
        goto L2; // [75] 43
L3: 
        ;
    }

    /** 	rsyms = sort( rsyms )*/
    RefDS(_rsyms_65762);
    _0 = _rsyms_65762;
    _rsyms_65762 = _22sort(_rsyms_65762, 1);
    DeRefDS(_0);

    /** 	puts( fn, "<h2>Routines:</h2>\n" )*/
    EPuts(_fn_65760, _33080); // DJP 

    /** 	for r = 1 to length( rsyms )  do*/
    if (IS_SEQUENCE(_rsyms_65762)){
            _33081 = SEQ_PTR(_rsyms_65762)->length;
    }
    else {
        _33081 = 1;
    }
    {
        int _r_65781;
        _r_65781 = 1;
L4: 
        if (_r_65781 > _33081){
            goto L5; // [99] 348
        }

        /** 		printf( fn, "<table><tr><td>Signature:</td><td>%s</td></tr>\n", { signature( rsyms[r][2] ) } )*/
        _2 = (int)SEQ_PTR(_rsyms_65762);
        _33083 = (int)*(((s1_ptr)_2)->base + _r_65781);
        _2 = (int)SEQ_PTR(_33083);
        _33084 = (int)*(((s1_ptr)_2)->base + 2);
        _33083 = NOVALUE;
        Ref(_33084);
        _33085 = _72signature(_33084);
        _33084 = NOVALUE;
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _33085;
        _33086 = MAKE_SEQ(_1);
        _33085 = NOVALUE;
        EPrintf(_fn_65760, _33082, _33086);
        DeRefDS(_33086);
        _33086 = NOVALUE;

        /** 		sequence references    = routine_ref( f, rsyms[r][2], called_from )*/
        _2 = (int)SEQ_PTR(_rsyms_65762);
        _33087 = (int)*(((s1_ptr)_2)->base + _r_65781);
        _2 = (int)SEQ_PTR(_33087);
        _33088 = (int)*(((s1_ptr)_2)->base + 2);
        _33087 = NOVALUE;
        Ref(_33088);
        Ref(_71called_from_65009);
        _0 = _references_65788;
        _references_65788 = _72routine_ref(_f_65761, _33088, _71called_from_65009);
        DeRef(_0);
        _33088 = NOVALUE;

        /** 		sequence referenced_by = routine_ref( f, rsyms[r][2], called_by )*/
        _2 = (int)SEQ_PTR(_rsyms_65762);
        _33090 = (int)*(((s1_ptr)_2)->base + _r_65781);
        _2 = (int)SEQ_PTR(_33090);
        _33091 = (int)*(((s1_ptr)_2)->base + 2);
        _33090 = NOVALUE;
        Ref(_33091);
        Ref(_71called_by_65011);
        _0 = _referenced_by_65793;
        _referenced_by_65793 = _72routine_ref(_f_65761, _33091, _71called_by_65011);
        DeRef(_0);
        _33091 = NOVALUE;

        /** 		puts( fn, "<tr><td>References:</td><td>" )*/
        EPuts(_fn_65760, _33093); // DJP 

        /** 		for i = 1 to length( references ) do*/
        if (IS_SEQUENCE(_references_65788)){
                _33094 = SEQ_PTR(_references_65788)->length;
        }
        else {
            _33094 = 1;
        }
        {
            int _i_65800;
            _i_65800 = 1;
L6: 
            if (_i_65800 > _33094){
                goto L7; // [178] 220
            }

            /** 			puts( fn, proc_link( references[i]  ) )*/
            _2 = (int)SEQ_PTR(_references_65788);
            _33095 = (int)*(((s1_ptr)_2)->base + _i_65800);
            Ref(_33095);
            _33096 = _72proc_link(_33095);
            _33095 = NOVALUE;
            EPuts(_fn_65760, _33096); // DJP 
            DeRef(_33096);
            _33096 = NOVALUE;

            /** 			if i < length( references ) then*/
            if (IS_SEQUENCE(_references_65788)){
                    _33097 = SEQ_PTR(_references_65788)->length;
            }
            else {
                _33097 = 1;
            }
            if (_i_65800 >= _33097)
            goto L8; // [203] 213

            /** 				puts( fn, ", " )*/
            EPuts(_fn_65760, _33026); // DJP 
L8: 

            /** 		end for*/
            _i_65800 = _i_65800 + 1;
            goto L6; // [215] 185
L7: 
            ;
        }

        /** 		puts( fn, "</td></tr>\n" )*/
        EPuts(_fn_65760, _33099); // DJP 

        /** 		puts( fn, "<tr><td>Referenced by:</td><td>" )*/
        EPuts(_fn_65760, _33100); // DJP 

        /** 		for i = 1 to length( referenced_by ) do*/
        if (IS_SEQUENCE(_referenced_by_65793)){
                _33101 = SEQ_PTR(_referenced_by_65793)->length;
        }
        else {
            _33101 = 1;
        }
        {
            int _i_65810;
            _i_65810 = 1;
L9: 
            if (_i_65810 > _33101){
                goto LA; // [235] 277
            }

            /** 			puts( fn, proc_link( referenced_by[i] ) )*/
            _2 = (int)SEQ_PTR(_referenced_by_65793);
            _33102 = (int)*(((s1_ptr)_2)->base + _i_65810);
            Ref(_33102);
            _33103 = _72proc_link(_33102);
            _33102 = NOVALUE;
            EPuts(_fn_65760, _33103); // DJP 
            DeRef(_33103);
            _33103 = NOVALUE;

            /** 			if i < length( referenced_by ) then*/
            if (IS_SEQUENCE(_referenced_by_65793)){
                    _33104 = SEQ_PTR(_referenced_by_65793)->length;
            }
            else {
                _33104 = 1;
            }
            if (_i_65810 >= _33104)
            goto LB; // [260] 270

            /** 				puts( fn, ", " )*/
            EPuts(_fn_65760, _33026); // DJP 
LB: 

            /** 		end for*/
            _i_65810 = _i_65810 + 1;
            goto L9; // [272] 242
LA: 
            ;
        }

        /** 		puts( fn, "</td></tr>\n" )*/
        EPuts(_fn_65760, _33099); // DJP 

        /** 		if show_callgraphs then*/
        if (_72show_callgraphs_65418 == 0)
        {
            goto LC; // [286] 332
        }
        else{
        }

        /** 			sequence image_file = sprintf( "image/%s_%s.png", { underscore_name( short_names[f] ), underscore_name( rsyms[r][1] ) } )*/
        _2 = (int)SEQ_PTR(_71short_names_65016);
        _33107 = (int)*(((s1_ptr)_2)->base + _f_65761);
        Ref(_33107);
        _33108 = _72underscore_name(_33107);
        _33107 = NOVALUE;
        _2 = (int)SEQ_PTR(_rsyms_65762);
        _33109 = (int)*(((s1_ptr)_2)->base + _r_65781);
        _2 = (int)SEQ_PTR(_33109);
        _33110 = (int)*(((s1_ptr)_2)->base + 1);
        _33109 = NOVALUE;
        Ref(_33110);
        _33111 = _72underscore_name(_33110);
        _33110 = NOVALUE;
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _33108;
        ((int *)_2)[2] = _33111;
        _33112 = MAKE_SEQ(_1);
        _33111 = NOVALUE;
        _33108 = NOVALUE;
        DeRefi(_image_file_65818);
        _image_file_65818 = EPrintf(-9999999, _33106, _33112);
        DeRefDS(_33112);
        _33112 = NOVALUE;

        /** 			printf( fn, "<tr><td>Call graph:</td><td><a href=\"%s\"><img src=\"%s\" style=\"max-width: 600px;\"/></a></td></tr>\n", repeat( image_file, 2 ) )*/
        _33115 = Repeat(_image_file_65818, 2);
        EPrintf(_fn_65760, _33114, _33115);
        DeRefDS(_33115);
        _33115 = NOVALUE;
LC: 
        DeRefi(_image_file_65818);
        _image_file_65818 = NOVALUE;

        /** 		puts( fn, "</table>\n<hr>\n" )*/
        EPuts(_fn_65760, _33116); // DJP 
        DeRef(_references_65788);
        _references_65788 = NOVALUE;
        DeRef(_referenced_by_65793);
        _referenced_by_65793 = NOVALUE;

        /** 	end for*/
        _r_65781 = _r_65781 + 1;
        goto L4; // [343] 106
L5: 
        ;
    }

    /** end procedure*/
    DeRef(_rsyms_65762);
    _33074 = NOVALUE;
    return;
    ;
}


void _72write_files()
{
    int _link_1__tmp_at47_65849 = NOVALUE;
    int _link_inlined_link_at_47_65848 = NOVALUE;
    int _name_inlined_link_at_44_65847 = NOVALUE;
    int _fn_65843 = NOVALUE;
    int _33132 = NOVALUE;
    int _33131 = NOVALUE;
    int _33130 = NOVALUE;
    int _33126 = NOVALUE;
    int _33125 = NOVALUE;
    int _33123 = NOVALUE;
    int _33121 = NOVALUE;
    int _33120 = NOVALUE;
    int _33119 = NOVALUE;
    int _33117 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for f = 1 to length( short_names ) do*/
    if (IS_SEQUENCE(_71short_names_65016)){
            _33117 = SEQ_PTR(_71short_names_65016)->length;
    }
    else {
        _33117 = 1;
    }
    {
        int _f_65834;
        _f_65834 = 1;
L1: 
        if (_f_65834 > _33117){
            goto L2; // [8] 168
        }

        /** 		if show_stdlib or not std_libs[f] then*/
        if (_71show_stdlib_65018 != 0) {
            goto L3; // [19] 37
        }
        _2 = (int)SEQ_PTR(_71std_libs_65017);
        _33119 = (int)*(((s1_ptr)_2)->base + _f_65834);
        _33120 = (_33119 == 0);
        _33119 = NOVALUE;
        if (_33120 == 0)
        {
            DeRef(_33120);
            _33120 = NOVALUE;
            goto L4; // [33] 159
        }
        else{
            DeRef(_33120);
            _33120 = NOVALUE;
        }
L3: 

        /** 			integer fn = safe_open( link( short_names[f] ) )*/
        _2 = (int)SEQ_PTR(_71short_names_65016);
        _33121 = (int)*(((s1_ptr)_2)->base + _f_65834);
        Ref(_33121);
        DeRef(_name_inlined_link_at_44_65847);
        _name_inlined_link_at_44_65847 = _33121;
        _33121 = NOVALUE;

        /** 	return underscore_name( name ) & ".html"*/
        Ref(_name_inlined_link_at_44_65847);
        _0 = _link_1__tmp_at47_65849;
        _link_1__tmp_at47_65849 = _72underscore_name(_name_inlined_link_at_44_65847);
        DeRef(_0);
        if (IS_SEQUENCE(_link_1__tmp_at47_65849) && IS_ATOM(_32950)) {
        }
        else if (IS_ATOM(_link_1__tmp_at47_65849) && IS_SEQUENCE(_32950)) {
            Ref(_link_1__tmp_at47_65849);
            Prepend(&_link_inlined_link_at_47_65848, _32950, _link_1__tmp_at47_65849);
        }
        else {
            Concat((object_ptr)&_link_inlined_link_at_47_65848, _link_1__tmp_at47_65849, _32950);
        }
        DeRef(_name_inlined_link_at_44_65847);
        _name_inlined_link_at_44_65847 = NOVALUE;
        DeRef(_link_1__tmp_at47_65849);
        _link_1__tmp_at47_65849 = NOVALUE;
        RefDS(_link_inlined_link_at_47_65848);
        _fn_65843 = _72safe_open(_link_inlined_link_at_47_65848);
        if (!IS_ATOM_INT(_fn_65843)) {
            _1 = (long)(DBL_PTR(_fn_65843)->dbl);
            if (UNIQUE(DBL_PTR(_fn_65843)) && (DBL_PTR(_fn_65843)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_fn_65843);
            _fn_65843 = _1;
        }

        /** 			header( fn, short_names[f] )*/
        _2 = (int)SEQ_PTR(_71short_names_65016);
        _33123 = (int)*(((s1_ptr)_2)->base + _f_65834);
        Ref(_33123);
        RefDS(_22682);
        _72header(_fn_65843, _33123, _22682);
        _33123 = NOVALUE;

        /** 			printf( fn, "<h1>%s</h1>\n", { short_names[f] } )*/
        _2 = (int)SEQ_PTR(_71short_names_65016);
        _33125 = (int)*(((s1_ptr)_2)->base + _f_65834);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_33125);
        *((int *)(_2+4)) = _33125;
        _33126 = MAKE_SEQ(_1);
        _33125 = NOVALUE;
        EPrintf(_fn_65843, _33124, _33126);
        DeRefDS(_33126);
        _33126 = NOVALUE;

        /** 			puts( fn, "<a href=\"index.html\">Main</a><br>\n" )*/
        EPuts(_fn_65843, _33127); // DJP 

        /** 			if show_dependencies then*/
        if (_72show_dependencies_65417 == 0)
        {
            goto L5; // [107] 136
        }
        else{
        }

        /** 				puts( fn, "<h2>Include dependencies</h2>\n" )*/
        EPuts(_fn_65843, _33128); // DJP 

        /** 				printf( fn, "<img src=\"image/%s.dep.png\"/>\n", {underscore_name( short_names[f] )} )*/
        _2 = (int)SEQ_PTR(_71short_names_65016);
        _33130 = (int)*(((s1_ptr)_2)->base + _f_65834);
        Ref(_33130);
        _33131 = _72underscore_name(_33130);
        _33130 = NOVALUE;
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _33131;
        _33132 = MAKE_SEQ(_1);
        _33131 = NOVALUE;
        EPrintf(_fn_65843, _33129, _33132);
        DeRefDS(_33132);
        _33132 = NOVALUE;
L5: 

        /** 			write_routines( fn, f )*/
        _72write_routines(_fn_65843, _f_65834);

        /** 			footer( fn )*/

        /** 	puts( fn, "</body></html>\n" )*/
        EPuts(_fn_65843, _32946); // DJP 

        /** end procedure*/
        goto L6; // [151] 154
L6: 

        /** 			close( fn )*/
        EClose(_fn_65843);
L4: 

        /** 	end for*/
        _f_65834 = _f_65834 + 1;
        goto L1; // [163] 15
L2: 
        ;
    }

    /** end procedure*/
    return;
    ;
}


void _72dependencies()
{
    int _dotfn_65870 = NOVALUE;
    int _void_65876 = NOVALUE;
    int _33150 = NOVALUE;
    int _33149 = NOVALUE;
    int _33148 = NOVALUE;
    int _33147 = NOVALUE;
    int _33145 = NOVALUE;
    int _33142 = NOVALUE;
    int _33141 = NOVALUE;
    int _33139 = NOVALUE;
    int _33138 = NOVALUE;
    int _33136 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not show_dependencies then*/
    if (_72show_dependencies_65417 != 0)
    goto L1; // [5] 14

    /** 		return*/
    return;
L1: 

    /** 	integer dotfn = safe_open( "_working_.dot" )*/
    RefDS(_33134);
    _dotfn_65870 = _72safe_open(_33134);
    if (!IS_ATOM_INT(_dotfn_65870)) {
        _1 = (long)(DBL_PTR(_dotfn_65870)->dbl);
        if (UNIQUE(DBL_PTR(_dotfn_65870)) && (DBL_PTR(_dotfn_65870)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_dotfn_65870);
        _dotfn_65870 = _1;
    }

    /** 	puts( dotfn, diagram_includes( 1, show_stdlib ) )*/
    _33136 = _71diagram_includes(1, _71show_stdlib_65018);
    EPuts(_dotfn_65870, _33136); // DJP 
    DeRef(_33136);
    _33136 = NOVALUE;

    /** 	close( dotfn )*/
    EClose(_dotfn_65870);

    /** 	object void = system_exec( sprintf("dot -Tpng \"%s_working_.dot\" -o \"%simage/all_dep.png\"", {out_dir, out_dir}), 2 )*/
    RefDS(_72out_dir_65413);
    RefDS(_72out_dir_65413);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _72out_dir_65413;
    ((int *)_2)[2] = _72out_dir_65413;
    _33138 = MAKE_SEQ(_1);
    _33139 = EPrintf(-9999999, _33137, _33138);
    DeRefDS(_33138);
    _33138 = NOVALUE;
    _void_65876 = system_exec_call(_33139, 2);
    DeRefDS(_33139);
    _33139 = NOVALUE;

    /** 	for f = 1 to length( short_names ) do*/
    if (IS_SEQUENCE(_71short_names_65016)){
            _33141 = SEQ_PTR(_71short_names_65016)->length;
    }
    else {
        _33141 = 1;
    }
    {
        int _f_65882;
        _f_65882 = 1;
L2: 
        if (_f_65882 > _33141){
            goto L3; // [63] 145
        }

        /** 		if not std_libs[f] then*/
        _2 = (int)SEQ_PTR(_71std_libs_65017);
        _33142 = (int)*(((s1_ptr)_2)->base + _f_65882);
        if (_33142 != 0)
        goto L4; // [78] 138
        _33142 = NOVALUE;

        /** 			dotfn = safe_open( "_working_.dot" )*/
        RefDS(_33134);
        _dotfn_65870 = _72safe_open(_33134);
        if (!IS_ATOM_INT(_dotfn_65870)) {
            _1 = (long)(DBL_PTR(_dotfn_65870)->dbl);
            if (UNIQUE(DBL_PTR(_dotfn_65870)) && (DBL_PTR(_dotfn_65870)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_dotfn_65870);
            _dotfn_65870 = _1;
        }

        /** 			puts( dotfn, diagram_file_deps( f ) )*/
        _33145 = _71diagram_file_deps(_f_65882);
        EPuts(_dotfn_65870, _33145); // DJP 
        DeRef(_33145);
        _33145 = NOVALUE;

        /** 			close( dotfn )*/
        EClose(_dotfn_65870);

        /** 			void = system_exec( */
        _2 = (int)SEQ_PTR(_71short_names_65016);
        _33147 = (int)*(((s1_ptr)_2)->base + _f_65882);
        Ref(_33147);
        _33148 = _72underscore_name(_33147);
        _33147 = NOVALUE;
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        RefDSn(_72out_dir_65413, 2);
        *((int *)(_2+4)) = _72out_dir_65413;
        *((int *)(_2+8)) = _72out_dir_65413;
        *((int *)(_2+12)) = _33148;
        _33149 = MAKE_SEQ(_1);
        _33148 = NOVALUE;
        _33150 = EPrintf(-9999999, _33146, _33149);
        DeRefDS(_33149);
        _33149 = NOVALUE;
        _void_65876 = system_exec_call(_33150, 2);
        DeRefDS(_33150);
        _33150 = NOVALUE;

        /** 			puts(1, '.' )*/
        EPuts(1, 46); // DJP 
L4: 

        /** 	end for*/
        _f_65882 = _f_65882 + 1;
        goto L2; // [140] 70
L3: 
        ;
    }

    /** 	puts( 1, "\n" )*/
    EPuts(1, _22834); // DJP 

    /** end procedure*/
    return;
    ;
}


int _72proc_file_name(int _proc_65902)
{
    int _e_65904 = NOVALUE;
    int _33160 = NOVALUE;
    int _33159 = NOVALUE;
    int _33158 = NOVALUE;
    int _33157 = NOVALUE;
    int _33156 = NOVALUE;
    int _33155 = NOVALUE;
    int _33154 = NOVALUE;
    int _0, _1, _2;
    

    /** 	symtab_entry e = SymTab[proc]*/
    DeRef(_e_65904);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _e_65904 = (int)*(((s1_ptr)_2)->base + _proc_65902);
    Ref(_e_65904);

    /** 	return sprintf( "%s_%s", { underscore_name( short_names[e[S_FILE_NO]] ), underscore_name( e[S_NAME] ) } )*/
    _2 = (int)SEQ_PTR(_e_65904);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _33154 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _33154 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _2 = (int)SEQ_PTR(_71short_names_65016);
    if (!IS_ATOM_INT(_33154)){
        _33155 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33154)->dbl));
    }
    else{
        _33155 = (int)*(((s1_ptr)_2)->base + _33154);
    }
    Ref(_33155);
    _33156 = _72underscore_name(_33155);
    _33155 = NOVALUE;
    _2 = (int)SEQ_PTR(_e_65904);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _33157 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _33157 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    Ref(_33157);
    _33158 = _72underscore_name(_33157);
    _33157 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33156;
    ((int *)_2)[2] = _33158;
    _33159 = MAKE_SEQ(_1);
    _33158 = NOVALUE;
    _33156 = NOVALUE;
    _33160 = EPrintf(-9999999, _33153, _33159);
    DeRefDS(_33159);
    _33159 = NOVALUE;
    DeRefDS(_e_65904);
    _33154 = NOVALUE;
    return _33160;
    ;
}


void _72call_graphs()
{
    int _files_65922 = NOVALUE;
    int _file_65927 = NOVALUE;
    int _procs_65933 = NOVALUE;
    int _proc_65939 = NOVALUE;
    int _name_65941 = NOVALUE;
    int _dn_65943 = NOVALUE;
    int _ok_65947 = NOVALUE;
    int _33175 = NOVALUE;
    int _33174 = NOVALUE;
    int _33172 = NOVALUE;
    int _33168 = NOVALUE;
    int _33165 = NOVALUE;
    int _33163 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not show_callgraphs then*/
    if (_72show_callgraphs_65418 != 0)
    goto L1; // [5] 14

    /** 		return*/
    DeRef(_files_65922);
    return;
L1: 

    /** 	sequence files = map:keys( routine_map )*/
    Ref(_72routine_map_65577);
    _0 = _files_65922;
    _files_65922 = _32keys(_72routine_map_65577, 0);
    DeRef(_0);

    /** 	for f = 1 to length( files ) do*/
    if (IS_SEQUENCE(_files_65922)){
            _33163 = SEQ_PTR(_files_65922)->length;
    }
    else {
        _33163 = 1;
    }
    {
        int _f_65925;
        _f_65925 = 1;
L2: 
        if (_f_65925 > _33163){
            goto L3; // [30] 163
        }

        /** 		integer file = files[f]*/
        _2 = (int)SEQ_PTR(_files_65922);
        _file_65927 = (int)*(((s1_ptr)_2)->base + _f_65925);
        if (!IS_ATOM_INT(_file_65927))
        _file_65927 = (long)DBL_PTR(_file_65927)->dbl;

        /** 		if not std_libs[file] then*/
        _2 = (int)SEQ_PTR(_71std_libs_65017);
        _33165 = (int)*(((s1_ptr)_2)->base + _file_65927);
        if (_33165 != 0)
        goto L4; // [51] 152
        _33165 = NOVALUE;

        /** 			sequence procs = map:get( routine_map, file, {} )*/
        Ref(_72routine_map_65577);
        RefDS(_22682);
        _0 = _procs_65933;
        _procs_65933 = _32get(_72routine_map_65577, _file_65927, _22682);
        DeRef(_0);

        /** 			for p = 1 to length( procs ) do*/
        if (IS_SEQUENCE(_procs_65933)){
                _33168 = SEQ_PTR(_procs_65933)->length;
        }
        else {
            _33168 = 1;
        }
        {
            int _p_65936;
            _p_65936 = 1;
L5: 
            if (_p_65936 > _33168){
                goto L6; // [71] 151
            }

            /** 				symtab_index proc = procs[p]*/
            _2 = (int)SEQ_PTR(_procs_65933);
            _proc_65939 = (int)*(((s1_ptr)_2)->base + _p_65936);
            if (!IS_ATOM_INT(_proc_65939)){
                _proc_65939 = (long)DBL_PTR(_proc_65939)->dbl;
            }

            /** 				sequence name = proc_file_name( proc )*/
            _0 = _name_65941;
            _name_65941 = _72proc_file_name(_proc_65939);
            DeRef(_0);

            /** 				integer dn = safe_open( "_working_.dot" )*/
            RefDS(_33134);
            _dn_65943 = _72safe_open(_33134);
            if (!IS_ATOM_INT(_dn_65943)) {
                _1 = (long)(DBL_PTR(_dn_65943)->dbl);
                if (UNIQUE(DBL_PTR(_dn_65943)) && (DBL_PTR(_dn_65943)->cleanup != 0))
                RTFatal("Cannot assign value with a destructor to an integer");                DeRefDS(_dn_65943);
                _dn_65943 = _1;
            }

            /** 				puts( dn, diagram_routine( proc ) )*/
            _33172 = _71diagram_routine(_proc_65939, 3, 3);
            EPuts(_dn_65943, _33172); // DJP 
            DeRef(_33172);
            _33172 = NOVALUE;

            /** 				close( dn )*/
            EClose(_dn_65943);

            /** 				integer ok = system_exec( sprintf( "dot -Tpng \"%s_working_.dot\" -o \"%simage/%s.png\"", {out_dir, out_dir, name}), 2)*/
            _1 = NewS1(3);
            _2 = (int)((s1_ptr)_1)->base;
            RefDSn(_72out_dir_65413, 2);
            *((int *)(_2+4)) = _72out_dir_65413;
            *((int *)(_2+8)) = _72out_dir_65413;
            RefDS(_name_65941);
            *((int *)(_2+12)) = _name_65941;
            _33174 = MAKE_SEQ(_1);
            _33175 = EPrintf(-9999999, _33173, _33174);
            DeRefDS(_33174);
            _33174 = NOVALUE;
            _ok_65947 = system_exec_call(_33175, 2);
            DeRefDS(_33175);
            _33175 = NOVALUE;

            /** 				puts(1, '.' )*/
            EPuts(1, 46); // DJP 
            DeRefDS(_name_65941);
            _name_65941 = NOVALUE;

            /** 			end for*/
            _p_65936 = _p_65936 + 1;
            goto L5; // [146] 78
L6: 
            ;
        }
L4: 
        DeRef(_procs_65933);
        _procs_65933 = NOVALUE;

        /** 	end for*/
        _f_65925 = _f_65925 + 1;
        goto L2; // [158] 37
L3: 
        ;
    }

    /** 	puts(1, '\n')*/
    EPuts(1, 10); // DJP 

    /** end procedure*/
    DeRef(_files_65922);
    return;
    ;
}


void _72generate()
{
    int _0, _1, _2;
    

    /** 	puts(1, "generating dox\n" )*/
    EPuts(1, _33177); // DJP 

    /** 	puts(1, "preparing file structure\n" )*/
    EPuts(1, _33178); // DJP 

    /** 	make_dirs()*/
    _72make_dirs();

    /** 	puts(1, "initializing data\n" )*/
    EPuts(1, _33179); // DJP 

    /** 	short_files()*/
    _71short_files();

    /** 	make_routine_map()*/
    _72make_routine_map();

    /** 	puts(1, "writing the index page\n" )*/
    EPuts(1, _33180); // DJP 

    /** 	write_index()*/
    _72write_index();

    /** 	puts(1, "writing the file pages\n" )*/
    EPuts(1, _33181); // DJP 

    /** 	write_files()*/
    _72write_files();

    /** 	puts(1, "generating the dependency graphs\n" )*/
    EPuts(1, _33182); // DJP 

    /** 	dependencies()*/
    _72dependencies();

    /** 	puts(1, "generating the call graphs\n" )*/
    EPuts(1, _33183); // DJP 

    /** 	call_graphs()*/
    _72call_graphs();

    /** end procedure*/
    return;
    ;
}



// 0x124E3A97
