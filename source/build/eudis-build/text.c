// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _4sprint(int _x_9226)
{
    int _s_9227 = NOVALUE;
    int _5053 = NOVALUE;
    int _5051 = NOVALUE;
    int _5050 = NOVALUE;
    int _5047 = NOVALUE;
    int _5046 = NOVALUE;
    int _5044 = NOVALUE;
    int _5043 = NOVALUE;
    int _5042 = NOVALUE;
    int _5041 = NOVALUE;
    int _5040 = NOVALUE;
    int _5039 = NOVALUE;
    int _5038 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(x) then*/
    _5038 = IS_ATOM(_x_9226);
    if (_5038 == 0)
    {
        _5038 = NOVALUE;
        goto L1; // [6] 22
    }
    else{
        _5038 = NOVALUE;
    }

    /** 		return sprintf("%.10g", x)*/
    _5039 = EPrintf(-9999999, _4707, _x_9226);
    DeRef(_x_9226);
    DeRef(_s_9227);
    return _5039;
    goto L2; // [19] 137
L1: 

    /** 		s = "{"*/
    RefDS(_750);
    DeRef(_s_9227);
    _s_9227 = _750;

    /** 		for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_9226)){
            _5040 = SEQ_PTR(_x_9226)->length;
    }
    else {
        _5040 = 1;
    }
    {
        int _i_9233;
        _i_9233 = 1;
L3: 
        if (_i_9233 > _5040){
            goto L4; // [34] 98
        }

        /** 			if atom(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_9226);
        _5041 = (int)*(((s1_ptr)_2)->base + _i_9233);
        _5042 = IS_ATOM(_5041);
        _5041 = NOVALUE;
        if (_5042 == 0)
        {
            _5042 = NOVALUE;
            goto L5; // [50] 70
        }
        else{
            _5042 = NOVALUE;
        }

        /** 				s &= sprintf("%.10g", x[i])*/
        _2 = (int)SEQ_PTR(_x_9226);
        _5043 = (int)*(((s1_ptr)_2)->base + _i_9233);
        _5044 = EPrintf(-9999999, _4707, _5043);
        _5043 = NOVALUE;
        Concat((object_ptr)&_s_9227, _s_9227, _5044);
        DeRefDS(_5044);
        _5044 = NOVALUE;
        goto L6; // [67] 85
L5: 

        /** 				s &= sprint(x[i])*/
        _2 = (int)SEQ_PTR(_x_9226);
        _5046 = (int)*(((s1_ptr)_2)->base + _i_9233);
        Ref(_5046);
        _5047 = _4sprint(_5046);
        _5046 = NOVALUE;
        if (IS_SEQUENCE(_s_9227) && IS_ATOM(_5047)) {
            Ref(_5047);
            Append(&_s_9227, _s_9227, _5047);
        }
        else if (IS_ATOM(_s_9227) && IS_SEQUENCE(_5047)) {
        }
        else {
            Concat((object_ptr)&_s_9227, _s_9227, _5047);
        }
        DeRef(_5047);
        _5047 = NOVALUE;
L6: 

        /** 			s &= ','*/
        Append(&_s_9227, _s_9227, 44);

        /** 		end for*/
        _i_9233 = _i_9233 + 1;
        goto L3; // [93] 41
L4: 
        ;
    }

    /** 		if s[$] = ',' then*/
    if (IS_SEQUENCE(_s_9227)){
            _5050 = SEQ_PTR(_s_9227)->length;
    }
    else {
        _5050 = 1;
    }
    _2 = (int)SEQ_PTR(_s_9227);
    _5051 = (int)*(((s1_ptr)_2)->base + _5050);
    if (binary_op_a(NOTEQ, _5051, 44)){
        _5051 = NOVALUE;
        goto L7; // [107] 123
    }
    _5051 = NOVALUE;

    /** 			s[$] = '}'*/
    if (IS_SEQUENCE(_s_9227)){
            _5053 = SEQ_PTR(_s_9227)->length;
    }
    else {
        _5053 = 1;
    }
    _2 = (int)SEQ_PTR(_s_9227);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _s_9227 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _5053);
    _1 = *(int *)_2;
    *(int *)_2 = 125;
    DeRef(_1);
    goto L8; // [120] 130
L7: 

    /** 			s &= '}'*/
    Append(&_s_9227, _s_9227, 125);
L8: 

    /** 		return s*/
    DeRef(_x_9226);
    DeRef(_5039);
    _5039 = NOVALUE;
    return _s_9227;
L2: 
    ;
}


int _4trim_head(int _source_9255, int _what_9256, int _ret_index_9257)
{
    int _lpos_9258 = NOVALUE;
    int _5064 = NOVALUE;
    int _5063 = NOVALUE;
    int _5060 = NOVALUE;
    int _5059 = NOVALUE;
    int _5057 = NOVALUE;
    int _5055 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_ret_index_9257)) {
        _1 = (long)(DBL_PTR(_ret_index_9257)->dbl);
        if (UNIQUE(DBL_PTR(_ret_index_9257)) && (DBL_PTR(_ret_index_9257)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret_index_9257);
        _ret_index_9257 = _1;
    }

    /** 	if atom(what) then*/
    _5055 = IS_ATOM(_what_9256);
    if (_5055 == 0)
    {
        _5055 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _5055 = NOVALUE;
    }

    /** 		what = {what}*/
    _0 = _what_9256;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_what_9256);
    *((int *)(_2+4)) = _what_9256;
    _what_9256 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	lpos = 1*/
    _lpos_9258 = 1;

    /** 	while lpos <= length(source) do*/
L2: 
    if (IS_SEQUENCE(_source_9255)){
            _5057 = SEQ_PTR(_source_9255)->length;
    }
    else {
        _5057 = 1;
    }
    if (_lpos_9258 > _5057)
    goto L3; // [33] 67

    /** 		if not find(source[lpos], what) then*/
    _2 = (int)SEQ_PTR(_source_9255);
    _5059 = (int)*(((s1_ptr)_2)->base + _lpos_9258);
    _5060 = find_from(_5059, _what_9256, 1);
    _5059 = NOVALUE;
    if (_5060 != 0)
    goto L4; // [48] 56
    _5060 = NOVALUE;

    /** 			exit*/
    goto L3; // [53] 67
L4: 

    /** 		lpos += 1*/
    _lpos_9258 = _lpos_9258 + 1;

    /** 	end while*/
    goto L2; // [64] 30
L3: 

    /** 	if ret_index then*/
    if (_ret_index_9257 == 0)
    {
        goto L5; // [69] 81
    }
    else{
    }

    /** 		return lpos*/
    DeRefDS(_source_9255);
    DeRef(_what_9256);
    return _lpos_9258;
    goto L6; // [78] 96
L5: 

    /** 		return source[lpos .. $]*/
    if (IS_SEQUENCE(_source_9255)){
            _5063 = SEQ_PTR(_source_9255)->length;
    }
    else {
        _5063 = 1;
    }
    rhs_slice_target = (object_ptr)&_5064;
    RHS_Slice(_source_9255, _lpos_9258, _5063);
    DeRefDS(_source_9255);
    DeRef(_what_9256);
    return _5064;
L6: 
    ;
}


int _4trim_tail(int _source_9276, int _what_9277, int _ret_index_9278)
{
    int _rpos_9279 = NOVALUE;
    int _5073 = NOVALUE;
    int _5070 = NOVALUE;
    int _5069 = NOVALUE;
    int _5065 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_ret_index_9278)) {
        _1 = (long)(DBL_PTR(_ret_index_9278)->dbl);
        if (UNIQUE(DBL_PTR(_ret_index_9278)) && (DBL_PTR(_ret_index_9278)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret_index_9278);
        _ret_index_9278 = _1;
    }

    /** 	if atom(what) then*/
    _5065 = IS_ATOM(_what_9277);
    if (_5065 == 0)
    {
        _5065 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _5065 = NOVALUE;
    }

    /** 		what = {what}*/
    _0 = _what_9277;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_what_9277);
    *((int *)(_2+4)) = _what_9277;
    _what_9277 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	rpos = length(source)*/
    if (IS_SEQUENCE(_source_9276)){
            _rpos_9279 = SEQ_PTR(_source_9276)->length;
    }
    else {
        _rpos_9279 = 1;
    }

    /** 	while rpos > 0 do*/
L2: 
    if (_rpos_9279 <= 0)
    goto L3; // [30] 64

    /** 		if not find(source[rpos], what) then*/
    _2 = (int)SEQ_PTR(_source_9276);
    _5069 = (int)*(((s1_ptr)_2)->base + _rpos_9279);
    _5070 = find_from(_5069, _what_9277, 1);
    _5069 = NOVALUE;
    if (_5070 != 0)
    goto L4; // [45] 53
    _5070 = NOVALUE;

    /** 			exit*/
    goto L3; // [50] 64
L4: 

    /** 		rpos -= 1*/
    _rpos_9279 = _rpos_9279 - 1;

    /** 	end while*/
    goto L2; // [61] 30
L3: 

    /** 	if ret_index then*/
    if (_ret_index_9278 == 0)
    {
        goto L5; // [66] 78
    }
    else{
    }

    /** 		return rpos*/
    DeRefDS(_source_9276);
    DeRef(_what_9277);
    return _rpos_9279;
    goto L6; // [75] 90
L5: 

    /** 		return source[1..rpos]*/
    rhs_slice_target = (object_ptr)&_5073;
    RHS_Slice(_source_9276, 1, _rpos_9279);
    DeRefDS(_source_9276);
    DeRef(_what_9277);
    return _5073;
L6: 
    ;
}


int _4trim(int _source_9296, int _what_9297, int _ret_index_9298)
{
    int _rpos_9299 = NOVALUE;
    int _lpos_9300 = NOVALUE;
    int _5094 = NOVALUE;
    int _5092 = NOVALUE;
    int _5090 = NOVALUE;
    int _5088 = NOVALUE;
    int _5085 = NOVALUE;
    int _5084 = NOVALUE;
    int _5079 = NOVALUE;
    int _5078 = NOVALUE;
    int _5076 = NOVALUE;
    int _5074 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_ret_index_9298)) {
        _1 = (long)(DBL_PTR(_ret_index_9298)->dbl);
        if (UNIQUE(DBL_PTR(_ret_index_9298)) && (DBL_PTR(_ret_index_9298)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret_index_9298);
        _ret_index_9298 = _1;
    }

    /** 	if atom(what) then*/
    _5074 = IS_ATOM(_what_9297);
    if (_5074 == 0)
    {
        _5074 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _5074 = NOVALUE;
    }

    /** 		what = {what}*/
    _0 = _what_9297;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_what_9297);
    *((int *)(_2+4)) = _what_9297;
    _what_9297 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	lpos = 1*/
    _lpos_9300 = 1;

    /** 	while lpos <= length(source) do*/
L2: 
    if (IS_SEQUENCE(_source_9296)){
            _5076 = SEQ_PTR(_source_9296)->length;
    }
    else {
        _5076 = 1;
    }
    if (_lpos_9300 > _5076)
    goto L3; // [33] 67

    /** 		if not find(source[lpos], what) then*/
    _2 = (int)SEQ_PTR(_source_9296);
    _5078 = (int)*(((s1_ptr)_2)->base + _lpos_9300);
    _5079 = find_from(_5078, _what_9297, 1);
    _5078 = NOVALUE;
    if (_5079 != 0)
    goto L4; // [48] 56
    _5079 = NOVALUE;

    /** 			exit*/
    goto L3; // [53] 67
L4: 

    /** 		lpos += 1*/
    _lpos_9300 = _lpos_9300 + 1;

    /** 	end while*/
    goto L2; // [64] 30
L3: 

    /** 	rpos = length(source)*/
    if (IS_SEQUENCE(_source_9296)){
            _rpos_9299 = SEQ_PTR(_source_9296)->length;
    }
    else {
        _rpos_9299 = 1;
    }

    /** 	while rpos > lpos do*/
L5: 
    if (_rpos_9299 <= _lpos_9300)
    goto L6; // [77] 111

    /** 		if not find(source[rpos], what) then*/
    _2 = (int)SEQ_PTR(_source_9296);
    _5084 = (int)*(((s1_ptr)_2)->base + _rpos_9299);
    _5085 = find_from(_5084, _what_9297, 1);
    _5084 = NOVALUE;
    if (_5085 != 0)
    goto L7; // [92] 100
    _5085 = NOVALUE;

    /** 			exit*/
    goto L6; // [97] 111
L7: 

    /** 		rpos -= 1*/
    _rpos_9299 = _rpos_9299 - 1;

    /** 	end while*/
    goto L5; // [108] 77
L6: 

    /** 	if ret_index then*/
    if (_ret_index_9298 == 0)
    {
        goto L8; // [113] 129
    }
    else{
    }

    /** 		return {lpos, rpos}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _lpos_9300;
    ((int *)_2)[2] = _rpos_9299;
    _5088 = MAKE_SEQ(_1);
    DeRefDS(_source_9296);
    DeRef(_what_9297);
    return _5088;
    goto L9; // [126] 180
L8: 

    /** 		if lpos = 1 then*/
    if (_lpos_9300 != 1)
    goto LA; // [131] 152

    /** 			if rpos = length(source) then*/
    if (IS_SEQUENCE(_source_9296)){
            _5090 = SEQ_PTR(_source_9296)->length;
    }
    else {
        _5090 = 1;
    }
    if (_rpos_9299 != _5090)
    goto LB; // [140] 151

    /** 				return source*/
    DeRef(_what_9297);
    DeRef(_5088);
    _5088 = NOVALUE;
    return _source_9296;
LB: 
LA: 

    /** 		if lpos > length(source) then*/
    if (IS_SEQUENCE(_source_9296)){
            _5092 = SEQ_PTR(_source_9296)->length;
    }
    else {
        _5092 = 1;
    }
    if (_lpos_9300 <= _5092)
    goto LC; // [157] 168

    /** 			return {}*/
    RefDS(_5);
    DeRefDS(_source_9296);
    DeRef(_what_9297);
    DeRef(_5088);
    _5088 = NOVALUE;
    return _5;
LC: 

    /** 		return source[lpos..rpos]*/
    rhs_slice_target = (object_ptr)&_5094;
    RHS_Slice(_source_9296, _lpos_9300, _rpos_9299);
    DeRefDS(_source_9296);
    DeRef(_what_9297);
    DeRef(_5088);
    _5088 = NOVALUE;
    return _5094;
L9: 
    ;
}


int _4load_code_page(int _cpname_9338)
{
    int _cpdata_9339 = NOVALUE;
    int _pos_9340 = NOVALUE;
    int _kv_9341 = NOVALUE;
    int _cp_source_9342 = NOVALUE;
    int _cp_db_9343 = NOVALUE;
    int _fh_9415 = NOVALUE;
    int _idx_9419 = NOVALUE;
    int _vers_9420 = NOVALUE;
    int _seek_1__tmp_at464_9435 = NOVALUE;
    int _seek_inlined_seek_at_464_9434 = NOVALUE;
    int _seek_1__tmp_at516_9446 = NOVALUE;
    int _seek_inlined_seek_at_516_9445 = NOVALUE;
    int _pos_inlined_seek_at_513_9444 = NOVALUE;
    int _5169 = NOVALUE;
    int _5168 = NOVALUE;
    int _5165 = NOVALUE;
    int _5160 = NOVALUE;
    int _5158 = NOVALUE;
    int _5157 = NOVALUE;
    int _5155 = NOVALUE;
    int _5148 = NOVALUE;
    int _5147 = NOVALUE;
    int _5146 = NOVALUE;
    int _5144 = NOVALUE;
    int _5143 = NOVALUE;
    int _5142 = NOVALUE;
    int _5140 = NOVALUE;
    int _5138 = NOVALUE;
    int _5137 = NOVALUE;
    int _5135 = NOVALUE;
    int _5134 = NOVALUE;
    int _5132 = NOVALUE;
    int _5131 = NOVALUE;
    int _5130 = NOVALUE;
    int _5129 = NOVALUE;
    int _5126 = NOVALUE;
    int _5124 = NOVALUE;
    int _5122 = NOVALUE;
    int _5121 = NOVALUE;
    int _5119 = NOVALUE;
    int _5118 = NOVALUE;
    int _5117 = NOVALUE;
    int _5115 = NOVALUE;
    int _5114 = NOVALUE;
    int _5113 = NOVALUE;
    int _5110 = NOVALUE;
    int _5109 = NOVALUE;
    int _5108 = NOVALUE;
    int _5107 = NOVALUE;
    int _5105 = NOVALUE;
    int _5104 = NOVALUE;
    int _5101 = NOVALUE;
    int _5100 = NOVALUE;
    int _0, _1, _2;
    

    /** 	cp_source = filesys:defaultext(cpname, ".ecp")*/
    RefDS(_cpname_9338);
    RefDS(_5096);
    _0 = _cp_source_9342;
    _cp_source_9342 = _9defaultext(_cpname_9338, _5096);
    DeRef(_0);

    /** 	cp_source = filesys:locate_file(cp_source)*/
    RefDS(_cp_source_9342);
    RefDS(_5);
    RefDS(_5);
    _0 = _cp_source_9342;
    _cp_source_9342 = _9locate_file(_cp_source_9342, _5, _5);
    DeRefDS(_0);

    /** 	cpdata = io:read_lines(cp_source)*/
    RefDS(_cp_source_9342);
    _0 = _cpdata_9339;
    _cpdata_9339 = _16read_lines(_cp_source_9342);
    DeRef(_0);

    /** 	if sequence(cpdata) then*/
    _5100 = IS_SEQUENCE(_cpdata_9339);
    if (_5100 == 0)
    {
        _5100 = NOVALUE;
        goto L1; // [33] 373
    }
    else{
        _5100 = NOVALUE;
    }

    /** 		pos = 0*/
    _pos_9340 = 0;

    /** 		while pos < length(cpdata) do*/
L2: 
    if (IS_SEQUENCE(_cpdata_9339)){
            _5101 = SEQ_PTR(_cpdata_9339)->length;
    }
    else {
        _5101 = 1;
    }
    if (_pos_9340 >= _5101)
    goto L3; // [49] 188

    /** 			pos += 1*/
    _pos_9340 = _pos_9340 + 1;

    /** 			cpdata[pos]  = trim(cpdata[pos])*/
    _2 = (int)SEQ_PTR(_cpdata_9339);
    _5104 = (int)*(((s1_ptr)_2)->base + _pos_9340);
    Ref(_5104);
    RefDS(_4443);
    _5105 = _4trim(_5104, _4443, 0);
    _5104 = NOVALUE;
    _2 = (int)SEQ_PTR(_cpdata_9339);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _cpdata_9339 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _pos_9340);
    _1 = *(int *)_2;
    *(int *)_2 = _5105;
    if( _1 != _5105 ){
        DeRef(_1);
    }
    _5105 = NOVALUE;

    /** 			if search:begins("--HEAD--", cpdata[pos]) then*/
    _2 = (int)SEQ_PTR(_cpdata_9339);
    _5107 = (int)*(((s1_ptr)_2)->base + _pos_9340);
    RefDS(_5106);
    Ref(_5107);
    _5108 = _7begins(_5106, _5107);
    _5107 = NOVALUE;
    if (_5108 == 0) {
        DeRef(_5108);
        _5108 = NOVALUE;
        goto L4; // [86] 94
    }
    else {
        if (!IS_ATOM_INT(_5108) && DBL_PTR(_5108)->dbl == 0.0){
            DeRef(_5108);
            _5108 = NOVALUE;
            goto L4; // [86] 94
        }
        DeRef(_5108);
        _5108 = NOVALUE;
    }
    DeRef(_5108);
    _5108 = NOVALUE;

    /** 				continue*/
    goto L2; // [91] 46
L4: 

    /** 			if cpdata[pos][1] = ';' then*/
    _2 = (int)SEQ_PTR(_cpdata_9339);
    _5109 = (int)*(((s1_ptr)_2)->base + _pos_9340);
    _2 = (int)SEQ_PTR(_5109);
    _5110 = (int)*(((s1_ptr)_2)->base + 1);
    _5109 = NOVALUE;
    if (binary_op_a(NOTEQ, _5110, 59)){
        _5110 = NOVALUE;
        goto L5; // [104] 113
    }
    _5110 = NOVALUE;

    /** 				continue	-- A comment line*/
    goto L2; // [110] 46
L5: 

    /** 			if search:begins("--CASE--", cpdata[pos]) then*/
    _2 = (int)SEQ_PTR(_cpdata_9339);
    _5113 = (int)*(((s1_ptr)_2)->base + _pos_9340);
    RefDS(_5112);
    Ref(_5113);
    _5114 = _7begins(_5112, _5113);
    _5113 = NOVALUE;
    if (_5114 == 0) {
        DeRef(_5114);
        _5114 = NOVALUE;
        goto L6; // [124] 132
    }
    else {
        if (!IS_ATOM_INT(_5114) && DBL_PTR(_5114)->dbl == 0.0){
            DeRef(_5114);
            _5114 = NOVALUE;
            goto L6; // [124] 132
        }
        DeRef(_5114);
        _5114 = NOVALUE;
    }
    DeRef(_5114);
    _5114 = NOVALUE;

    /** 				exit*/
    goto L3; // [129] 188
L6: 

    /** 			kv = keyvalues(cpdata[pos],,,,"")*/
    _2 = (int)SEQ_PTR(_cpdata_9339);
    _5115 = (int)*(((s1_ptr)_2)->base + _pos_9340);
    Ref(_5115);
    RefDS(_5263);
    RefDS(_5264);
    RefDS(_5265);
    RefDS(_5);
    _0 = _kv_9341;
    _kv_9341 = _4keyvalues(_5115, _5263, _5264, _5265, _5, 1);
    DeRef(_0);
    _5115 = NOVALUE;

    /** 			if equal(lower(kv[1][1]), "title") then*/
    _2 = (int)SEQ_PTR(_kv_9341);
    _5117 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_5117);
    _5118 = (int)*(((s1_ptr)_2)->base + 1);
    _5117 = NOVALUE;
    Ref(_5118);
    _5119 = _4lower(_5118);
    _5118 = NOVALUE;
    if (_5119 == _5120)
    _5121 = 1;
    else if (IS_ATOM_INT(_5119) && IS_ATOM_INT(_5120))
    _5121 = 0;
    else
    _5121 = (compare(_5119, _5120) == 0);
    DeRef(_5119);
    _5119 = NOVALUE;
    if (_5121 == 0)
    {
        _5121 = NOVALUE;
        goto L2; // [167] 46
    }
    else{
        _5121 = NOVALUE;
    }

    /** 				encoding_NAME = kv[1][2]*/
    _2 = (int)SEQ_PTR(_kv_9341);
    _5122 = (int)*(((s1_ptr)_2)->base + 1);
    DeRef(_4encoding_NAME_9334);
    _2 = (int)SEQ_PTR(_5122);
    _4encoding_NAME_9334 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_4encoding_NAME_9334);
    _5122 = NOVALUE;

    /** 		end while*/
    goto L2; // [185] 46
L3: 

    /** 		if pos > length(cpdata) then*/
    if (IS_SEQUENCE(_cpdata_9339)){
            _5124 = SEQ_PTR(_cpdata_9339)->length;
    }
    else {
        _5124 = 1;
    }
    if (_pos_9340 <= _5124)
    goto L7; // [193] 204

    /** 			return -2 -- No Case Conversion table found.*/
    DeRefDS(_cpname_9338);
    DeRef(_cpdata_9339);
    DeRef(_kv_9341);
    DeRef(_cp_source_9342);
    DeRef(_cp_db_9343);
    return -2;
L7: 

    /** 		upper_case_SET = ""*/
    RefDS(_5);
    DeRef(_4upper_case_SET_9333);
    _4upper_case_SET_9333 = _5;

    /** 		lower_case_SET = ""*/
    RefDS(_5);
    DeRef(_4lower_case_SET_9332);
    _4lower_case_SET_9332 = _5;

    /** 		while pos < length(cpdata) do*/
L8: 
    if (IS_SEQUENCE(_cpdata_9339)){
            _5126 = SEQ_PTR(_cpdata_9339)->length;
    }
    else {
        _5126 = 1;
    }
    if (_pos_9340 >= _5126)
    goto L9; // [226] 583

    /** 			pos += 1*/
    _pos_9340 = _pos_9340 + 1;

    /** 			cpdata[pos]  = trim(cpdata[pos])*/
    _2 = (int)SEQ_PTR(_cpdata_9339);
    _5129 = (int)*(((s1_ptr)_2)->base + _pos_9340);
    Ref(_5129);
    RefDS(_4443);
    _5130 = _4trim(_5129, _4443, 0);
    _5129 = NOVALUE;
    _2 = (int)SEQ_PTR(_cpdata_9339);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _cpdata_9339 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _pos_9340);
    _1 = *(int *)_2;
    *(int *)_2 = _5130;
    if( _1 != _5130 ){
        DeRef(_1);
    }
    _5130 = NOVALUE;

    /** 			if length(cpdata[pos]) < 3 then*/
    _2 = (int)SEQ_PTR(_cpdata_9339);
    _5131 = (int)*(((s1_ptr)_2)->base + _pos_9340);
    if (IS_SEQUENCE(_5131)){
            _5132 = SEQ_PTR(_5131)->length;
    }
    else {
        _5132 = 1;
    }
    _5131 = NOVALUE;
    if (_5132 >= 3)
    goto LA; // [261] 270

    /** 				continue*/
    goto L8; // [267] 223
LA: 

    /** 			if cpdata[pos][1] = ';' then*/
    _2 = (int)SEQ_PTR(_cpdata_9339);
    _5134 = (int)*(((s1_ptr)_2)->base + _pos_9340);
    _2 = (int)SEQ_PTR(_5134);
    _5135 = (int)*(((s1_ptr)_2)->base + 1);
    _5134 = NOVALUE;
    if (binary_op_a(NOTEQ, _5135, 59)){
        _5135 = NOVALUE;
        goto LB; // [280] 289
    }
    _5135 = NOVALUE;

    /** 				continue	-- A comment line*/
    goto L8; // [286] 223
LB: 

    /** 			if cpdata[pos][1] = '-' then*/
    _2 = (int)SEQ_PTR(_cpdata_9339);
    _5137 = (int)*(((s1_ptr)_2)->base + _pos_9340);
    _2 = (int)SEQ_PTR(_5137);
    _5138 = (int)*(((s1_ptr)_2)->base + 1);
    _5137 = NOVALUE;
    if (binary_op_a(NOTEQ, _5138, 45)){
        _5138 = NOVALUE;
        goto LC; // [299] 308
    }
    _5138 = NOVALUE;

    /** 				exit*/
    goto L9; // [305] 583
LC: 

    /** 			kv = keyvalues(cpdata[pos])*/
    _2 = (int)SEQ_PTR(_cpdata_9339);
    _5140 = (int)*(((s1_ptr)_2)->base + _pos_9340);
    Ref(_5140);
    RefDS(_5263);
    RefDS(_5264);
    RefDS(_5265);
    RefDS(_121);
    _0 = _kv_9341;
    _kv_9341 = _4keyvalues(_5140, _5263, _5264, _5265, _121, 1);
    DeRef(_0);
    _5140 = NOVALUE;

    /** 			upper_case_SET &= convert:hex_text(kv[1][1])*/
    _2 = (int)SEQ_PTR(_kv_9341);
    _5142 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_5142);
    _5143 = (int)*(((s1_ptr)_2)->base + 1);
    _5142 = NOVALUE;
    Ref(_5143);
    _5144 = _6hex_text(_5143);
    _5143 = NOVALUE;
    if (IS_SEQUENCE(_4upper_case_SET_9333) && IS_ATOM(_5144)) {
        Ref(_5144);
        Append(&_4upper_case_SET_9333, _4upper_case_SET_9333, _5144);
    }
    else if (IS_ATOM(_4upper_case_SET_9333) && IS_SEQUENCE(_5144)) {
    }
    else {
        Concat((object_ptr)&_4upper_case_SET_9333, _4upper_case_SET_9333, _5144);
    }
    DeRef(_5144);
    _5144 = NOVALUE;

    /** 			lower_case_SET &= convert:hex_text(kv[1][2])*/
    _2 = (int)SEQ_PTR(_kv_9341);
    _5146 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_5146);
    _5147 = (int)*(((s1_ptr)_2)->base + 2);
    _5146 = NOVALUE;
    Ref(_5147);
    _5148 = _6hex_text(_5147);
    _5147 = NOVALUE;
    if (IS_SEQUENCE(_4lower_case_SET_9332) && IS_ATOM(_5148)) {
        Ref(_5148);
        Append(&_4lower_case_SET_9332, _4lower_case_SET_9332, _5148);
    }
    else if (IS_ATOM(_4lower_case_SET_9332) && IS_SEQUENCE(_5148)) {
    }
    else {
        Concat((object_ptr)&_4lower_case_SET_9332, _4lower_case_SET_9332, _5148);
    }
    DeRef(_5148);
    _5148 = NOVALUE;

    /** 		end while*/
    goto L8; // [367] 223
    goto L9; // [370] 583
L1: 

    /** 		cp_db = filesys:locate_file("ecp.dat")*/
    RefDS(_5150);
    RefDS(_5);
    RefDS(_5);
    _0 = _cp_db_9343;
    _cp_db_9343 = _9locate_file(_5150, _5, _5);
    DeRef(_0);

    /** 		integer fh = open(cp_db, "rb")*/
    _fh_9415 = EOpen(_cp_db_9343, _1138, 0);

    /** 		if fh = -1 then*/
    if (_fh_9415 != -1)
    goto LD; // [392] 403

    /** 			return -2 -- Couldn't open DB*/
    DeRef(_idx_9419);
    DeRef(_vers_9420);
    DeRefDS(_cpname_9338);
    DeRef(_cpdata_9339);
    DeRef(_kv_9341);
    DeRef(_cp_source_9342);
    DeRefDS(_cp_db_9343);
    _5131 = NOVALUE;
    return -2;
LD: 

    /** 		object idx*/

    /** 		object vers*/

    /** 		vers = serialize:deserialize(fh)  -- get the database version*/
    _0 = _vers_9420;
    _vers_9420 = _24deserialize(_fh_9415, 1);
    DeRef(_0);

    /** 		if atom(vers) or length(vers) = 0 then*/
    _5155 = IS_ATOM(_vers_9420);
    if (_5155 != 0) {
        goto LE; // [419] 435
    }
    if (IS_SEQUENCE(_vers_9420)){
            _5157 = SEQ_PTR(_vers_9420)->length;
    }
    else {
        _5157 = 1;
    }
    _5158 = (_5157 == 0);
    _5157 = NOVALUE;
    if (_5158 == 0)
    {
        DeRef(_5158);
        _5158 = NOVALUE;
        goto LF; // [431] 442
    }
    else{
        DeRef(_5158);
        _5158 = NOVALUE;
    }
LE: 

    /** 			return -3 -- DB is wrong or corrupted.*/
    DeRef(_idx_9419);
    DeRef(_vers_9420);
    DeRefDS(_cpname_9338);
    DeRef(_cpdata_9339);
    DeRef(_kv_9341);
    DeRef(_cp_source_9342);
    DeRef(_cp_db_9343);
    _5131 = NOVALUE;
    return -3;
LF: 

    /** 		switch vers[1] do*/
    _2 = (int)SEQ_PTR(_vers_9420);
    _5160 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_5160) ){
        goto L10; // [448] 567
    }
    if(!IS_ATOM_INT(_5160)){
        if( (DBL_PTR(_5160)->dbl != (double) ((int) DBL_PTR(_5160)->dbl) ) ){
            goto L10; // [448] 567
        }
        _0 = (int) DBL_PTR(_5160)->dbl;
    }
    else {
        _0 = _5160;
    };
    _5160 = NOVALUE;
    switch ( _0 ){ 

        /** 			case 1, 2 then*/
        case 1:
        case 2:

        /** 				idx = serialize:deserialize(fh)  -- get Code Page index offset*/
        _0 = _idx_9419;
        _idx_9419 = _24deserialize(_fh_9415, 1);
        DeRef(_0);

        /** 				pos = io:seek(fh, idx)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_idx_9419);
        DeRef(_seek_1__tmp_at464_9435);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _fh_9415;
        ((int *)_2)[2] = _idx_9419;
        _seek_1__tmp_at464_9435 = MAKE_SEQ(_1);
        _pos_9340 = machine(19, _seek_1__tmp_at464_9435);
        DeRef(_seek_1__tmp_at464_9435);
        _seek_1__tmp_at464_9435 = NOVALUE;
        if (!IS_ATOM_INT(_pos_9340)) {
            _1 = (long)(DBL_PTR(_pos_9340)->dbl);
            if (UNIQUE(DBL_PTR(_pos_9340)) && (DBL_PTR(_pos_9340)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_9340);
            _pos_9340 = _1;
        }

        /** 				idx = serialize:deserialize(fh)	-- get the Code Page Index*/
        _0 = _idx_9419;
        _idx_9419 = _24deserialize(_fh_9415, 1);
        DeRef(_0);

        /** 				pos = find(cpname, idx[1])*/
        _2 = (int)SEQ_PTR(_idx_9419);
        _5165 = (int)*(((s1_ptr)_2)->base + 1);
        _pos_9340 = find_from(_cpname_9338, _5165, 1);
        _5165 = NOVALUE;

        /** 				if pos != 0 then*/
        if (_pos_9340 == 0)
        goto L11; // [503] 576

        /** 					pos = io:seek(fh, idx[2][pos])*/
        _2 = (int)SEQ_PTR(_idx_9419);
        _5168 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_5168);
        _5169 = (int)*(((s1_ptr)_2)->base + _pos_9340);
        _5168 = NOVALUE;
        Ref(_5169);
        DeRef(_pos_inlined_seek_at_513_9444);
        _pos_inlined_seek_at_513_9444 = _5169;
        _5169 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_513_9444);
        DeRef(_seek_1__tmp_at516_9446);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _fh_9415;
        ((int *)_2)[2] = _pos_inlined_seek_at_513_9444;
        _seek_1__tmp_at516_9446 = MAKE_SEQ(_1);
        _pos_9340 = machine(19, _seek_1__tmp_at516_9446);
        DeRef(_pos_inlined_seek_at_513_9444);
        _pos_inlined_seek_at_513_9444 = NOVALUE;
        DeRef(_seek_1__tmp_at516_9446);
        _seek_1__tmp_at516_9446 = NOVALUE;
        if (!IS_ATOM_INT(_pos_9340)) {
            _1 = (long)(DBL_PTR(_pos_9340)->dbl);
            if (UNIQUE(DBL_PTR(_pos_9340)) && (DBL_PTR(_pos_9340)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_9340);
            _pos_9340 = _1;
        }

        /** 					upper_case_SET = serialize:deserialize(fh) -- "uppercase"*/
        _0 = _24deserialize(_fh_9415, 1);
        DeRef(_4upper_case_SET_9333);
        _4upper_case_SET_9333 = _0;

        /** 					lower_case_SET = serialize:deserialize(fh) -- "lowercase"*/
        _0 = _24deserialize(_fh_9415, 1);
        DeRef(_4lower_case_SET_9332);
        _4lower_case_SET_9332 = _0;

        /** 					encoding_NAME = serialize:deserialize(fh) -- "title"*/
        _0 = _24deserialize(_fh_9415, 1);
        DeRef(_4encoding_NAME_9334);
        _4encoding_NAME_9334 = _0;
        goto L11; // [563] 576

        /** 			case else*/
        default:
L10: 

        /** 				return -4 -- Unhandled ecp database version.*/
        DeRef(_idx_9419);
        DeRef(_vers_9420);
        DeRefDS(_cpname_9338);
        DeRef(_cpdata_9339);
        DeRef(_kv_9341);
        DeRef(_cp_source_9342);
        DeRef(_cp_db_9343);
        _5131 = NOVALUE;
        return -4;
    ;}L11: 

    /** 		close(fh)*/
    EClose(_fh_9415);
    DeRef(_idx_9419);
    _idx_9419 = NOVALUE;
    DeRef(_vers_9420);
    _vers_9420 = NOVALUE;
L9: 

    /** 	return 0*/
    DeRefDS(_cpname_9338);
    DeRef(_cpdata_9339);
    DeRef(_kv_9341);
    DeRef(_cp_source_9342);
    DeRef(_cp_db_9343);
    _5131 = NOVALUE;
    return 0;
    ;
}


void _4set_encoding_properties(int _en_9454, int _lc_9455, int _uc_9456)
{
    int _res_9457 = NOVALUE;
    int _5193 = NOVALUE;
    int _5192 = NOVALUE;
    int _5191 = NOVALUE;
    int _5190 = NOVALUE;
    int _5189 = NOVALUE;
    int _5187 = NOVALUE;
    int _5186 = NOVALUE;
    int _5185 = NOVALUE;
    int _5181 = NOVALUE;
    int _5180 = NOVALUE;
    int _5179 = NOVALUE;
    int _5178 = NOVALUE;
    int _5177 = NOVALUE;
    int _5176 = NOVALUE;
    int _5175 = NOVALUE;
    int _5174 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(en) > 0 and length(lc) = 0 and length(uc) = 0 then*/
    if (IS_SEQUENCE(_en_9454)){
            _5174 = SEQ_PTR(_en_9454)->length;
    }
    else {
        _5174 = 1;
    }
    _5175 = (_5174 > 0);
    _5174 = NOVALUE;
    if (_5175 == 0) {
        _5176 = 0;
        goto L1; // [16] 31
    }
    if (IS_SEQUENCE(_lc_9455)){
            _5177 = SEQ_PTR(_lc_9455)->length;
    }
    else {
        _5177 = 1;
    }
    _5178 = (_5177 == 0);
    _5177 = NOVALUE;
    _5176 = (_5178 != 0);
L1: 
    if (_5176 == 0) {
        goto L2; // [31] 77
    }
    if (IS_SEQUENCE(_uc_9456)){
            _5180 = SEQ_PTR(_uc_9456)->length;
    }
    else {
        _5180 = 1;
    }
    _5181 = (_5180 == 0);
    _5180 = NOVALUE;
    if (_5181 == 0)
    {
        DeRef(_5181);
        _5181 = NOVALUE;
        goto L2; // [43] 77
    }
    else{
        DeRef(_5181);
        _5181 = NOVALUE;
    }

    /** 		res = load_code_page(en)*/
    RefDS(_en_9454);
    _res_9457 = _4load_code_page(_en_9454);
    if (!IS_ATOM_INT(_res_9457)) {
        _1 = (long)(DBL_PTR(_res_9457)->dbl);
        if (UNIQUE(DBL_PTR(_res_9457)) && (DBL_PTR(_res_9457)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_res_9457);
        _res_9457 = _1;
    }

    /** 		if res != 0 then*/
    if (_res_9457 == 0)
    goto L3; // [56] 71

    /** 			printf(2, "Failed to load code page '%s'. Error # %d\n", {en, res})*/
    RefDS(_en_9454);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _en_9454;
    ((int *)_2)[2] = _res_9457;
    _5185 = MAKE_SEQ(_1);
    EPrintf(2, _5184, _5185);
    DeRefDS(_5185);
    _5185 = NOVALUE;
L3: 

    /** 		return*/
    DeRefDS(_en_9454);
    DeRefDS(_lc_9455);
    DeRefDS(_uc_9456);
    DeRef(_5175);
    _5175 = NOVALUE;
    DeRef(_5178);
    _5178 = NOVALUE;
    return;
L2: 

    /** 	if length(lc) = length(uc) then*/
    if (IS_SEQUENCE(_lc_9455)){
            _5186 = SEQ_PTR(_lc_9455)->length;
    }
    else {
        _5186 = 1;
    }
    if (IS_SEQUENCE(_uc_9456)){
            _5187 = SEQ_PTR(_uc_9456)->length;
    }
    else {
        _5187 = 1;
    }
    if (_5186 != _5187)
    goto L4; // [85] 143

    /** 		if length(lc) = 0 and length(en) = 0 then*/
    if (IS_SEQUENCE(_lc_9455)){
            _5189 = SEQ_PTR(_lc_9455)->length;
    }
    else {
        _5189 = 1;
    }
    _5190 = (_5189 == 0);
    _5189 = NOVALUE;
    if (_5190 == 0) {
        goto L5; // [98] 121
    }
    if (IS_SEQUENCE(_en_9454)){
            _5192 = SEQ_PTR(_en_9454)->length;
    }
    else {
        _5192 = 1;
    }
    _5193 = (_5192 == 0);
    _5192 = NOVALUE;
    if (_5193 == 0)
    {
        DeRef(_5193);
        _5193 = NOVALUE;
        goto L5; // [110] 121
    }
    else{
        DeRef(_5193);
        _5193 = NOVALUE;
    }

    /** 			en = "ASCII"*/
    RefDS(_5095);
    DeRefDS(_en_9454);
    _en_9454 = _5095;
L5: 

    /** 		lower_case_SET = lc*/
    RefDS(_lc_9455);
    DeRef(_4lower_case_SET_9332);
    _4lower_case_SET_9332 = _lc_9455;

    /** 		upper_case_SET = uc*/
    RefDS(_uc_9456);
    DeRef(_4upper_case_SET_9333);
    _4upper_case_SET_9333 = _uc_9456;

    /** 		encoding_NAME = en*/
    RefDS(_en_9454);
    DeRef(_4encoding_NAME_9334);
    _4encoding_NAME_9334 = _en_9454;
L4: 

    /** end procedure*/
    DeRefDS(_en_9454);
    DeRefDS(_lc_9455);
    DeRefDS(_uc_9456);
    DeRef(_5175);
    _5175 = NOVALUE;
    DeRef(_5178);
    _5178 = NOVALUE;
    DeRef(_5190);
    _5190 = NOVALUE;
    return;
    ;
}


int _4get_encoding_properties()
{
    int _5194 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return {encoding_NAME, lower_case_SET, upper_case_SET}*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_4encoding_NAME_9334);
    *((int *)(_2+4)) = _4encoding_NAME_9334;
    RefDS(_4lower_case_SET_9332);
    *((int *)(_2+8)) = _4lower_case_SET_9332;
    RefDS(_4upper_case_SET_9333);
    *((int *)(_2+12)) = _4upper_case_SET_9333;
    _5194 = MAKE_SEQ(_1);
    return _5194;
    ;
}


int _4change_case(int _x_9511, int _api_9512)
{
    int _changed_text_9513 = NOVALUE;
    int _single_char_9514 = NOVALUE;
    int _len_9515 = NOVALUE;
    int _5225 = NOVALUE;
    int _5223 = NOVALUE;
    int _5221 = NOVALUE;
    int _5220 = NOVALUE;
    int _5217 = NOVALUE;
    int _5215 = NOVALUE;
    int _5213 = NOVALUE;
    int _5212 = NOVALUE;
    int _5211 = NOVALUE;
    int _5210 = NOVALUE;
    int _5209 = NOVALUE;
    int _5206 = NOVALUE;
    int _5204 = NOVALUE;
    int _0, _1, _2;
    

    /** 		integer single_char = 0*/
    _single_char_9514 = 0;

    /** 		if not string(x) then*/
    Ref(_x_9511);
    _5204 = _5string(_x_9511);
    if (IS_ATOM_INT(_5204)) {
        if (_5204 != 0){
            DeRef(_5204);
            _5204 = NOVALUE;
            goto L1; // [12] 95
        }
    }
    else {
        if (DBL_PTR(_5204)->dbl != 0.0){
            DeRef(_5204);
            _5204 = NOVALUE;
            goto L1; // [12] 95
        }
    }
    DeRef(_5204);
    _5204 = NOVALUE;

    /** 			if atom(x) then*/
    _5206 = IS_ATOM(_x_9511);
    if (_5206 == 0)
    {
        _5206 = NOVALUE;
        goto L2; // [20] 50
    }
    else{
        _5206 = NOVALUE;
    }

    /** 				if x = 0 then*/
    if (binary_op_a(NOTEQ, _x_9511, 0)){
        goto L3; // [25] 36
    }

    /** 					return 0*/
    DeRef(_x_9511);
    DeRef(_api_9512);
    DeRefi(_changed_text_9513);
    return 0;
L3: 

    /** 				x = {x}*/
    _0 = _x_9511;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_x_9511);
    *((int *)(_2+4)) = _x_9511;
    _x_9511 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 				single_char = 1*/
    _single_char_9514 = 1;
    goto L4; // [47] 94
L2: 

    /** 				for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_9511)){
            _5209 = SEQ_PTR(_x_9511)->length;
    }
    else {
        _5209 = 1;
    }
    {
        int _i_9527;
        _i_9527 = 1;
L5: 
        if (_i_9527 > _5209){
            goto L6; // [55] 87
        }

        /** 					x[i] = change_case(x[i], api)*/
        _2 = (int)SEQ_PTR(_x_9511);
        _5210 = (int)*(((s1_ptr)_2)->base + _i_9527);
        Ref(_api_9512);
        DeRef(_5211);
        _5211 = _api_9512;
        Ref(_5210);
        _5212 = _4change_case(_5210, _5211);
        _5210 = NOVALUE;
        _5211 = NOVALUE;
        _2 = (int)SEQ_PTR(_x_9511);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_9511 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_9527);
        _1 = *(int *)_2;
        *(int *)_2 = _5212;
        if( _1 != _5212 ){
            DeRef(_1);
        }
        _5212 = NOVALUE;

        /** 				end for*/
        _i_9527 = _i_9527 + 1;
        goto L5; // [82] 62
L6: 
        ;
    }

    /** 				return x*/
    DeRef(_api_9512);
    DeRefi(_changed_text_9513);
    return _x_9511;
L4: 
L1: 

    /** 		if length(x) = 0 then*/
    if (IS_SEQUENCE(_x_9511)){
            _5213 = SEQ_PTR(_x_9511)->length;
    }
    else {
        _5213 = 1;
    }
    if (_5213 != 0)
    goto L7; // [100] 111

    /** 			return x*/
    DeRef(_api_9512);
    DeRefi(_changed_text_9513);
    return _x_9511;
L7: 

    /** 		if length(x) >= tm_size then*/
    if (IS_SEQUENCE(_x_9511)){
            _5215 = SEQ_PTR(_x_9511)->length;
    }
    else {
        _5215 = 1;
    }
    if (_5215 < _4tm_size_9505)
    goto L8; // [118] 148

    /** 			tm_size = length(x) + 1*/
    if (IS_SEQUENCE(_x_9511)){
            _5217 = SEQ_PTR(_x_9511)->length;
    }
    else {
        _5217 = 1;
    }
    _4tm_size_9505 = _5217 + 1;
    _5217 = NOVALUE;

    /** 			free(temp_mem)*/
    Ref(_4temp_mem_9506);
    _12free(_4temp_mem_9506);

    /** 			temp_mem = allocate(tm_size)*/
    _0 = _12allocate(_4tm_size_9505, 0);
    DeRef(_4temp_mem_9506);
    _4temp_mem_9506 = _0;
L8: 

    /** 		poke(temp_mem, x)*/
    if (IS_ATOM_INT(_4temp_mem_9506)){
        poke_addr = (unsigned char *)_4temp_mem_9506;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_4temp_mem_9506)->dbl);
    }
    if (IS_ATOM_INT(_x_9511)) {
        *poke_addr = (unsigned char)_x_9511;
    }
    else if (IS_ATOM(_x_9511)) {
        _1 = (signed char)DBL_PTR(_x_9511)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_x_9511);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }

    /** 		len = c_func(api, {temp_mem, length(x)} )*/
    if (IS_SEQUENCE(_x_9511)){
            _5220 = SEQ_PTR(_x_9511)->length;
    }
    else {
        _5220 = 1;
    }
    Ref(_4temp_mem_9506);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _4temp_mem_9506;
    ((int *)_2)[2] = _5220;
    _5221 = MAKE_SEQ(_1);
    _5220 = NOVALUE;
    _len_9515 = call_c(1, _api_9512, _5221);
    DeRefDS(_5221);
    _5221 = NOVALUE;
    if (!IS_ATOM_INT(_len_9515)) {
        _1 = (long)(DBL_PTR(_len_9515)->dbl);
        if (UNIQUE(DBL_PTR(_len_9515)) && (DBL_PTR(_len_9515)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_len_9515);
        _len_9515 = _1;
    }

    /** 		changed_text = peek({temp_mem, len})*/
    Ref(_4temp_mem_9506);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _4temp_mem_9506;
    ((int *)_2)[2] = _len_9515;
    _5223 = MAKE_SEQ(_1);
    DeRefi(_changed_text_9513);
    _1 = (int)SEQ_PTR(_5223);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _changed_text_9513 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_5223);
    _5223 = NOVALUE;

    /** 		if single_char then*/
    if (_single_char_9514 == 0)
    {
        goto L9; // [188] 204
    }
    else{
    }

    /** 			return changed_text[1]*/
    _2 = (int)SEQ_PTR(_changed_text_9513);
    _5225 = (int)*(((s1_ptr)_2)->base + 1);
    DeRef(_x_9511);
    DeRef(_api_9512);
    DeRefDSi(_changed_text_9513);
    return _5225;
    goto LA; // [201] 211
L9: 

    /** 			return changed_text*/
    DeRef(_x_9511);
    DeRef(_api_9512);
    _5225 = NOVALUE;
    return _changed_text_9513;
LA: 
    ;
}


int _4lower(int _x_9553)
{
    int _5229 = NOVALUE;
    int _5228 = NOVALUE;
    int _5226 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(lower_case_SET) != 0 then*/
    if (IS_SEQUENCE(_4lower_case_SET_9332)){
            _5226 = SEQ_PTR(_4lower_case_SET_9332)->length;
    }
    else {
        _5226 = 1;
    }
    if (_5226 == 0)
    goto L1; // [8] 30

    /** 		return stdseq:mapping(x, upper_case_SET, lower_case_SET)*/
    Ref(_x_9553);
    RefDS(_4upper_case_SET_9333);
    RefDS(_4lower_case_SET_9332);
    _5228 = _21mapping(_x_9553, _4upper_case_SET_9333, _4lower_case_SET_9332, 0);
    DeRef(_x_9553);
    return _5228;
L1: 

    /** 	ifdef WINDOWS then*/

    /** 		return change_case(x, api_CharLowerBuff)*/
    Ref(_x_9553);
    Ref(_4api_CharLowerBuff_9489);
    _5229 = _4change_case(_x_9553, _4api_CharLowerBuff_9489);
    DeRef(_x_9553);
    DeRef(_5228);
    _5228 = NOVALUE;
    return _5229;
    ;
}


int _4upper(int _x_9561)
{
    int _5233 = NOVALUE;
    int _5232 = NOVALUE;
    int _5230 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(upper_case_SET) != 0 then*/
    if (IS_SEQUENCE(_4upper_case_SET_9333)){
            _5230 = SEQ_PTR(_4upper_case_SET_9333)->length;
    }
    else {
        _5230 = 1;
    }
    if (_5230 == 0)
    goto L1; // [8] 30

    /** 		return stdseq:mapping(x, lower_case_SET, upper_case_SET)*/
    Ref(_x_9561);
    RefDS(_4lower_case_SET_9332);
    RefDS(_4upper_case_SET_9333);
    _5232 = _21mapping(_x_9561, _4lower_case_SET_9332, _4upper_case_SET_9333, 0);
    DeRef(_x_9561);
    return _5232;
L1: 

    /** 	ifdef WINDOWS then*/

    /** 		return change_case(x, api_CharUpperBuff)*/
    Ref(_x_9561);
    Ref(_4api_CharUpperBuff_9497);
    _5233 = _4change_case(_x_9561, _4api_CharUpperBuff_9497);
    DeRef(_x_9561);
    DeRef(_5232);
    _5232 = NOVALUE;
    return _5233;
    ;
}


int _4proper(int _x_9569)
{
    int _pos_9570 = NOVALUE;
    int _inword_9571 = NOVALUE;
    int _convert_9572 = NOVALUE;
    int _res_9573 = NOVALUE;
    int _5262 = NOVALUE;
    int _5261 = NOVALUE;
    int _5260 = NOVALUE;
    int _5259 = NOVALUE;
    int _5258 = NOVALUE;
    int _5257 = NOVALUE;
    int _5256 = NOVALUE;
    int _5255 = NOVALUE;
    int _5254 = NOVALUE;
    int _5253 = NOVALUE;
    int _5251 = NOVALUE;
    int _5250 = NOVALUE;
    int _5246 = NOVALUE;
    int _5243 = NOVALUE;
    int _5240 = NOVALUE;
    int _5237 = NOVALUE;
    int _5236 = NOVALUE;
    int _5235 = NOVALUE;
    int _5234 = NOVALUE;
    int _0, _1, _2;
    

    /** 	inword = 0	-- Initially not in a word*/
    _inword_9571 = 0;

    /** 	convert = 1	-- Initially convert text*/
    _convert_9572 = 1;

    /** 	res = x		-- Work on a copy of the original, in case we need to restore.*/
    RefDS(_x_9569);
    DeRef(_res_9573);
    _res_9573 = _x_9569;

    /** 	for i = 1 to length(res) do*/
    if (IS_SEQUENCE(_res_9573)){
            _5234 = SEQ_PTR(_res_9573)->length;
    }
    else {
        _5234 = 1;
    }
    {
        int _i_9575;
        _i_9575 = 1;
L1: 
        if (_i_9575 > _5234){
            goto L2; // [25] 298
        }

        /** 		if integer(res[i]) then*/
        _2 = (int)SEQ_PTR(_res_9573);
        _5235 = (int)*(((s1_ptr)_2)->base + _i_9575);
        if (IS_ATOM_INT(_5235))
        _5236 = 1;
        else if (IS_ATOM_DBL(_5235))
        _5236 = IS_ATOM_INT(DoubleToInt(_5235));
        else
        _5236 = 0;
        _5235 = NOVALUE;
        if (_5236 == 0)
        {
            _5236 = NOVALUE;
            goto L3; // [41] 209
        }
        else{
            _5236 = NOVALUE;
        }

        /** 			if convert then*/
        if (_convert_9572 == 0)
        {
            goto L4; // [46] 291
        }
        else{
        }

        /** 				pos = types:t_upper(res[i])*/
        _2 = (int)SEQ_PTR(_res_9573);
        _5237 = (int)*(((s1_ptr)_2)->base + _i_9575);
        Ref(_5237);
        _pos_9570 = _5t_upper(_5237);
        _5237 = NOVALUE;
        if (!IS_ATOM_INT(_pos_9570)) {
            _1 = (long)(DBL_PTR(_pos_9570)->dbl);
            if (UNIQUE(DBL_PTR(_pos_9570)) && (DBL_PTR(_pos_9570)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_9570);
            _pos_9570 = _1;
        }

        /** 				if pos = 0 then*/
        if (_pos_9570 != 0)
        goto L5; // [63] 175

        /** 					pos = types:t_lower(res[i])*/
        _2 = (int)SEQ_PTR(_res_9573);
        _5240 = (int)*(((s1_ptr)_2)->base + _i_9575);
        Ref(_5240);
        _pos_9570 = _5t_lower(_5240);
        _5240 = NOVALUE;
        if (!IS_ATOM_INT(_pos_9570)) {
            _1 = (long)(DBL_PTR(_pos_9570)->dbl);
            if (UNIQUE(DBL_PTR(_pos_9570)) && (DBL_PTR(_pos_9570)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_9570);
            _pos_9570 = _1;
        }

        /** 					if pos = 0 then*/
        if (_pos_9570 != 0)
        goto L6; // [81] 138

        /** 						pos = t_digit(res[i])*/
        _2 = (int)SEQ_PTR(_res_9573);
        _5243 = (int)*(((s1_ptr)_2)->base + _i_9575);
        Ref(_5243);
        _pos_9570 = _5t_digit(_5243);
        _5243 = NOVALUE;
        if (!IS_ATOM_INT(_pos_9570)) {
            _1 = (long)(DBL_PTR(_pos_9570)->dbl);
            if (UNIQUE(DBL_PTR(_pos_9570)) && (DBL_PTR(_pos_9570)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_9570);
            _pos_9570 = _1;
        }

        /** 						if pos = 0 then*/
        if (_pos_9570 != 0)
        goto L4; // [99] 291

        /** 							pos = t_specword(res[i])*/
        _2 = (int)SEQ_PTR(_res_9573);
        _5246 = (int)*(((s1_ptr)_2)->base + _i_9575);
        Ref(_5246);
        _pos_9570 = _5t_specword(_5246);
        _5246 = NOVALUE;
        if (!IS_ATOM_INT(_pos_9570)) {
            _1 = (long)(DBL_PTR(_pos_9570)->dbl);
            if (UNIQUE(DBL_PTR(_pos_9570)) && (DBL_PTR(_pos_9570)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_9570);
            _pos_9570 = _1;
        }

        /** 							if pos then*/
        if (_pos_9570 == 0)
        {
            goto L7; // [117] 128
        }
        else{
        }

        /** 								inword = 1*/
        _inword_9571 = 1;
        goto L4; // [125] 291
L7: 

        /** 								inword = 0*/
        _inword_9571 = 0;
        goto L4; // [135] 291
L6: 

        /** 						if inword = 0 then*/
        if (_inword_9571 != 0)
        goto L4; // [140] 291

        /** 							if pos <= 26 then*/
        if (_pos_9570 > 26)
        goto L8; // [146] 165

        /** 								res[i] = upper(res[i]) -- Convert to uppercase*/
        _2 = (int)SEQ_PTR(_res_9573);
        _5250 = (int)*(((s1_ptr)_2)->base + _i_9575);
        Ref(_5250);
        _5251 = _4upper(_5250);
        _5250 = NOVALUE;
        _2 = (int)SEQ_PTR(_res_9573);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _res_9573 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_9575);
        _1 = *(int *)_2;
        *(int *)_2 = _5251;
        if( _1 != _5251 ){
            DeRef(_1);
        }
        _5251 = NOVALUE;
L8: 

        /** 							inword = 1	-- now we are in a word*/
        _inword_9571 = 1;
        goto L4; // [172] 291
L5: 

        /** 					if inword = 1 then*/
        if (_inword_9571 != 1)
        goto L9; // [177] 198

        /** 						res[i] = lower(res[i]) -- Convert to lowercase*/
        _2 = (int)SEQ_PTR(_res_9573);
        _5253 = (int)*(((s1_ptr)_2)->base + _i_9575);
        Ref(_5253);
        _5254 = _4lower(_5253);
        _5253 = NOVALUE;
        _2 = (int)SEQ_PTR(_res_9573);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _res_9573 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_9575);
        _1 = *(int *)_2;
        *(int *)_2 = _5254;
        if( _1 != _5254 ){
            DeRef(_1);
        }
        _5254 = NOVALUE;
        goto L4; // [195] 291
L9: 

        /** 						inword = 1	-- now we are in a word*/
        _inword_9571 = 1;
        goto L4; // [206] 291
L3: 

        /** 			if convert then*/
        if (_convert_9572 == 0)
        {
            goto LA; // [211] 263
        }
        else{
        }

        /** 				for j = 1 to i-1 do*/
        _5255 = _i_9575 - 1;
        {
            int _j_9615;
            _j_9615 = 1;
LB: 
            if (_j_9615 > _5255){
                goto LC; // [220] 257
            }

            /** 					if atom(x[j]) then*/
            _2 = (int)SEQ_PTR(_x_9569);
            _5256 = (int)*(((s1_ptr)_2)->base + _j_9615);
            _5257 = IS_ATOM(_5256);
            _5256 = NOVALUE;
            if (_5257 == 0)
            {
                _5257 = NOVALUE;
                goto LD; // [236] 250
            }
            else{
                _5257 = NOVALUE;
            }

            /** 						res[j] = x[j]*/
            _2 = (int)SEQ_PTR(_x_9569);
            _5258 = (int)*(((s1_ptr)_2)->base + _j_9615);
            Ref(_5258);
            _2 = (int)SEQ_PTR(_res_9573);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _res_9573 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_9615);
            _1 = *(int *)_2;
            *(int *)_2 = _5258;
            if( _1 != _5258 ){
                DeRef(_1);
            }
            _5258 = NOVALUE;
LD: 

            /** 				end for*/
            _j_9615 = _j_9615 + 1;
            goto LB; // [252] 227
LC: 
            ;
        }

        /** 				convert = 0*/
        _convert_9572 = 0;
LA: 

        /** 			if sequence(res[i]) then*/
        _2 = (int)SEQ_PTR(_res_9573);
        _5259 = (int)*(((s1_ptr)_2)->base + _i_9575);
        _5260 = IS_SEQUENCE(_5259);
        _5259 = NOVALUE;
        if (_5260 == 0)
        {
            _5260 = NOVALUE;
            goto LE; // [272] 290
        }
        else{
            _5260 = NOVALUE;
        }

        /** 				res[i] = proper(res[i])	-- recursive conversion*/
        _2 = (int)SEQ_PTR(_res_9573);
        _5261 = (int)*(((s1_ptr)_2)->base + _i_9575);
        Ref(_5261);
        _5262 = _4proper(_5261);
        _5261 = NOVALUE;
        _2 = (int)SEQ_PTR(_res_9573);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _res_9573 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_9575);
        _1 = *(int *)_2;
        *(int *)_2 = _5262;
        if( _1 != _5262 ){
            DeRef(_1);
        }
        _5262 = NOVALUE;
LE: 
L4: 

        /** 	end for*/
        _i_9575 = _i_9575 + 1;
        goto L1; // [293] 32
L2: 
        ;
    }

    /** 	return res*/
    DeRefDS(_x_9569);
    DeRef(_5255);
    _5255 = NOVALUE;
    return _res_9573;
    ;
}


int _4keyvalues(int _source_9628, int _pair_delim_9629, int _kv_delim_9631, int _quotes_9633, int _whitespace_9635, int _haskeys_9636)
{
    int _lKeyValues_9637 = NOVALUE;
    int _value__9638 = NOVALUE;
    int _key__9639 = NOVALUE;
    int _lAllDelim_9640 = NOVALUE;
    int _lWhitePair_9641 = NOVALUE;
    int _lStartBracket_9642 = NOVALUE;
    int _lEndBracket_9643 = NOVALUE;
    int _lBracketed_9644 = NOVALUE;
    int _lQuote_9645 = NOVALUE;
    int _pos__9646 = NOVALUE;
    int _lChar_9647 = NOVALUE;
    int _lBPos_9648 = NOVALUE;
    int _lWasKV_9649 = NOVALUE;
    int _5438 = NOVALUE;
    int _5435 = NOVALUE;
    int _5431 = NOVALUE;
    int _5428 = NOVALUE;
    int _5427 = NOVALUE;
    int _5426 = NOVALUE;
    int _5425 = NOVALUE;
    int _5424 = NOVALUE;
    int _5423 = NOVALUE;
    int _5422 = NOVALUE;
    int _5420 = NOVALUE;
    int _5419 = NOVALUE;
    int _5418 = NOVALUE;
    int _5417 = NOVALUE;
    int _5416 = NOVALUE;
    int _5415 = NOVALUE;
    int _5414 = NOVALUE;
    int _5413 = NOVALUE;
    int _5410 = NOVALUE;
    int _5409 = NOVALUE;
    int _5408 = NOVALUE;
    int _5407 = NOVALUE;
    int _5406 = NOVALUE;
    int _5405 = NOVALUE;
    int _5401 = NOVALUE;
    int _5399 = NOVALUE;
    int _5398 = NOVALUE;
    int _5395 = NOVALUE;
    int _5391 = NOVALUE;
    int _5389 = NOVALUE;
    int _5386 = NOVALUE;
    int _5383 = NOVALUE;
    int _5380 = NOVALUE;
    int _5377 = NOVALUE;
    int _5374 = NOVALUE;
    int _5371 = NOVALUE;
    int _5367 = NOVALUE;
    int _5366 = NOVALUE;
    int _5365 = NOVALUE;
    int _5364 = NOVALUE;
    int _5363 = NOVALUE;
    int _5362 = NOVALUE;
    int _5361 = NOVALUE;
    int _5359 = NOVALUE;
    int _5358 = NOVALUE;
    int _5357 = NOVALUE;
    int _5356 = NOVALUE;
    int _5355 = NOVALUE;
    int _5354 = NOVALUE;
    int _5353 = NOVALUE;
    int _5352 = NOVALUE;
    int _5350 = NOVALUE;
    int _5347 = NOVALUE;
    int _5345 = NOVALUE;
    int _5343 = NOVALUE;
    int _5342 = NOVALUE;
    int _5341 = NOVALUE;
    int _5340 = NOVALUE;
    int _5339 = NOVALUE;
    int _5338 = NOVALUE;
    int _5337 = NOVALUE;
    int _5336 = NOVALUE;
    int _5333 = NOVALUE;
    int _5332 = NOVALUE;
    int _5331 = NOVALUE;
    int _5330 = NOVALUE;
    int _5329 = NOVALUE;
    int _5326 = NOVALUE;
    int _5323 = NOVALUE;
    int _5320 = NOVALUE;
    int _5317 = NOVALUE;
    int _5316 = NOVALUE;
    int _5314 = NOVALUE;
    int _5313 = NOVALUE;
    int _5309 = NOVALUE;
    int _5306 = NOVALUE;
    int _5303 = NOVALUE;
    int _5299 = NOVALUE;
    int _5298 = NOVALUE;
    int _5297 = NOVALUE;
    int _5296 = NOVALUE;
    int _5292 = NOVALUE;
    int _5289 = NOVALUE;
    int _5286 = NOVALUE;
    int _5285 = NOVALUE;
    int _5283 = NOVALUE;
    int _5281 = NOVALUE;
    int _5275 = NOVALUE;
    int _5273 = NOVALUE;
    int _5271 = NOVALUE;
    int _5269 = NOVALUE;
    int _5267 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_haskeys_9636)) {
        _1 = (long)(DBL_PTR(_haskeys_9636)->dbl);
        if (UNIQUE(DBL_PTR(_haskeys_9636)) && (DBL_PTR(_haskeys_9636)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_haskeys_9636);
        _haskeys_9636 = _1;
    }

    /** 	source = trim(source)*/
    RefDS(_source_9628);
    RefDS(_4443);
    _0 = _source_9628;
    _source_9628 = _4trim(_source_9628, _4443, 0);
    DeRefDS(_0);

    /** 	if length(source) = 0 then*/
    if (IS_SEQUENCE(_source_9628)){
            _5267 = SEQ_PTR(_source_9628)->length;
    }
    else {
        _5267 = 1;
    }
    if (_5267 != 0)
    goto L1; // [20] 31

    /** 		return {}*/
    RefDS(_5);
    DeRefDS(_source_9628);
    DeRef(_pair_delim_9629);
    DeRef(_kv_delim_9631);
    DeRef(_quotes_9633);
    DeRef(_whitespace_9635);
    DeRef(_lKeyValues_9637);
    DeRef(_value__9638);
    DeRef(_key__9639);
    DeRef(_lAllDelim_9640);
    DeRef(_lWhitePair_9641);
    DeRefi(_lStartBracket_9642);
    DeRefi(_lEndBracket_9643);
    DeRefi(_lBracketed_9644);
    return _5;
L1: 

    /** 	if atom(pair_delim) then*/
    _5269 = IS_ATOM(_pair_delim_9629);
    if (_5269 == 0)
    {
        _5269 = NOVALUE;
        goto L2; // [36] 46
    }
    else{
        _5269 = NOVALUE;
    }

    /** 		pair_delim = {pair_delim}*/
    _0 = _pair_delim_9629;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pair_delim_9629);
    *((int *)(_2+4)) = _pair_delim_9629;
    _pair_delim_9629 = MAKE_SEQ(_1);
    DeRef(_0);
L2: 

    /** 	if atom(kv_delim) then*/
    _5271 = IS_ATOM(_kv_delim_9631);
    if (_5271 == 0)
    {
        _5271 = NOVALUE;
        goto L3; // [51] 61
    }
    else{
        _5271 = NOVALUE;
    }

    /** 		kv_delim = {kv_delim}*/
    _0 = _kv_delim_9631;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_kv_delim_9631);
    *((int *)(_2+4)) = _kv_delim_9631;
    _kv_delim_9631 = MAKE_SEQ(_1);
    DeRef(_0);
L3: 

    /** 	if atom(quotes) then*/
    _5273 = IS_ATOM(_quotes_9633);
    if (_5273 == 0)
    {
        _5273 = NOVALUE;
        goto L4; // [66] 76
    }
    else{
        _5273 = NOVALUE;
    }

    /** 		quotes = {quotes}*/
    _0 = _quotes_9633;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_quotes_9633);
    *((int *)(_2+4)) = _quotes_9633;
    _quotes_9633 = MAKE_SEQ(_1);
    DeRef(_0);
L4: 

    /** 	if atom(whitespace) then*/
    _5275 = IS_ATOM(_whitespace_9635);
    if (_5275 == 0)
    {
        _5275 = NOVALUE;
        goto L5; // [81] 91
    }
    else{
        _5275 = NOVALUE;
    }

    /** 		whitespace = {whitespace}*/
    _0 = _whitespace_9635;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_whitespace_9635);
    *((int *)(_2+4)) = _whitespace_9635;
    _whitespace_9635 = MAKE_SEQ(_1);
    DeRef(_0);
L5: 

    /** 	lAllDelim = whitespace & pair_delim & kv_delim*/
    {
        int concat_list[3];

        concat_list[0] = _kv_delim_9631;
        concat_list[1] = _pair_delim_9629;
        concat_list[2] = _whitespace_9635;
        Concat_N((object_ptr)&_lAllDelim_9640, concat_list, 3);
    }

    /** 	lWhitePair = whitespace & pair_delim*/
    if (IS_SEQUENCE(_whitespace_9635) && IS_ATOM(_pair_delim_9629)) {
        Ref(_pair_delim_9629);
        Append(&_lWhitePair_9641, _whitespace_9635, _pair_delim_9629);
    }
    else if (IS_ATOM(_whitespace_9635) && IS_SEQUENCE(_pair_delim_9629)) {
        Ref(_whitespace_9635);
        Prepend(&_lWhitePair_9641, _pair_delim_9629, _whitespace_9635);
    }
    else {
        Concat((object_ptr)&_lWhitePair_9641, _whitespace_9635, _pair_delim_9629);
    }

    /** 	lStartBracket = "{[("*/
    RefDS(_5279);
    DeRefi(_lStartBracket_9642);
    _lStartBracket_9642 = _5279;

    /** 	lEndBracket   = "}])"*/
    RefDS(_5280);
    DeRefi(_lEndBracket_9643);
    _lEndBracket_9643 = _5280;

    /** 	lKeyValues = {}*/
    RefDS(_5);
    DeRef(_lKeyValues_9637);
    _lKeyValues_9637 = _5;

    /** 	pos_ = 1*/
    _pos__9646 = 1;

    /** 	while pos_ <= length(source) do*/
L6: 
    if (IS_SEQUENCE(_source_9628)){
            _5281 = SEQ_PTR(_source_9628)->length;
    }
    else {
        _5281 = 1;
    }
    if (_pos__9646 > _5281)
    goto L7; // [139] 1222

    /** 		while pos_ < length(source) do*/
L8: 
    if (IS_SEQUENCE(_source_9628)){
            _5283 = SEQ_PTR(_source_9628)->length;
    }
    else {
        _5283 = 1;
    }
    if (_pos__9646 >= _5283)
    goto L9; // [151] 186

    /** 			if find(source[pos_], whitespace) = 0 then*/
    _2 = (int)SEQ_PTR(_source_9628);
    _5285 = (int)*(((s1_ptr)_2)->base + _pos__9646);
    _5286 = find_from(_5285, _whitespace_9635, 1);
    _5285 = NOVALUE;
    if (_5286 != 0)
    goto LA; // [166] 175

    /** 				exit*/
    goto L9; // [172] 186
LA: 

    /** 			pos_ +=1*/
    _pos__9646 = _pos__9646 + 1;

    /** 		end while*/
    goto L8; // [183] 148
L9: 

    /** 		key_ = ""*/
    RefDS(_5);
    DeRef(_key__9639);
    _key__9639 = _5;

    /** 		lQuote = 0*/
    _lQuote_9645 = 0;

    /** 		lChar = 0*/
    _lChar_9647 = 0;

    /** 		lWasKV = 0*/
    _lWasKV_9649 = 0;

    /** 		if haskeys then*/
    if (_haskeys_9636 == 0)
    {
        goto LB; // [210] 401
    }
    else{
    }

    /** 			while pos_ <= length(source) do*/
LC: 
    if (IS_SEQUENCE(_source_9628)){
            _5289 = SEQ_PTR(_source_9628)->length;
    }
    else {
        _5289 = 1;
    }
    if (_pos__9646 > _5289)
    goto LD; // [221] 335

    /** 				lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_9628);
    _lChar_9647 = (int)*(((s1_ptr)_2)->base + _pos__9646);
    if (!IS_ATOM_INT(_lChar_9647))
    _lChar_9647 = (long)DBL_PTR(_lChar_9647)->dbl;

    /** 				if find(lChar, quotes) != 0 then*/
    _5292 = find_from(_lChar_9647, _quotes_9633, 1);
    if (_5292 == 0)
    goto LE; // [238] 282

    /** 					if lChar = lQuote then*/
    if (_lChar_9647 != _lQuote_9645)
    goto LF; // [244] 261

    /** 						lQuote = 0*/
    _lQuote_9645 = 0;

    /** 						lChar = -1*/
    _lChar_9647 = -1;
    goto L10; // [258] 311
LF: 

    /** 					elsif lQuote = 0 then*/
    if (_lQuote_9645 != 0)
    goto L10; // [263] 311

    /** 						lQuote = lChar*/
    _lQuote_9645 = _lChar_9647;

    /** 						lChar = -1*/
    _lChar_9647 = -1;
    goto L10; // [279] 311
LE: 

    /** 				elsif lQuote = 0 and find(lChar, lAllDelim) != 0 then*/
    _5296 = (_lQuote_9645 == 0);
    if (_5296 == 0) {
        goto L11; // [288] 310
    }
    _5298 = find_from(_lChar_9647, _lAllDelim_9640, 1);
    _5299 = (_5298 != 0);
    _5298 = NOVALUE;
    if (_5299 == 0)
    {
        DeRef(_5299);
        _5299 = NOVALUE;
        goto L11; // [302] 310
    }
    else{
        DeRef(_5299);
        _5299 = NOVALUE;
    }

    /** 					exit*/
    goto LD; // [307] 335
L11: 
L10: 

    /** 				if lChar > 0 then*/
    if (_lChar_9647 <= 0)
    goto L12; // [313] 324

    /** 					key_ &= lChar*/
    Append(&_key__9639, _key__9639, _lChar_9647);
L12: 

    /** 				pos_ += 1*/
    _pos__9646 = _pos__9646 + 1;

    /** 			end while*/
    goto LC; // [332] 218
LD: 

    /** 			if find(lChar, whitespace) != 0 then*/
    _5303 = find_from(_lChar_9647, _whitespace_9635, 1);
    if (_5303 == 0)
    goto L13; // [342] 408

    /** 				pos_ += 1*/
    _pos__9646 = _pos__9646 + 1;

    /** 				while pos_ <= length(source) do*/
L14: 
    if (IS_SEQUENCE(_source_9628)){
            _5306 = SEQ_PTR(_source_9628)->length;
    }
    else {
        _5306 = 1;
    }
    if (_pos__9646 > _5306)
    goto L13; // [360] 408

    /** 					lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_9628);
    _lChar_9647 = (int)*(((s1_ptr)_2)->base + _pos__9646);
    if (!IS_ATOM_INT(_lChar_9647))
    _lChar_9647 = (long)DBL_PTR(_lChar_9647)->dbl;

    /** 					if find(lChar, whitespace) = 0 then*/
    _5309 = find_from(_lChar_9647, _whitespace_9635, 1);
    if (_5309 != 0)
    goto L15; // [377] 386

    /** 						exit*/
    goto L13; // [383] 408
L15: 

    /** 					pos_ +=1*/
    _pos__9646 = _pos__9646 + 1;

    /** 				end while*/
    goto L14; // [394] 357
    goto L13; // [398] 408
LB: 

    /** 			pos_ -= 1	-- Put back the last char.*/
    _pos__9646 = _pos__9646 - 1;
L13: 

    /** 		value_ = ""*/
    RefDS(_5);
    DeRef(_value__9638);
    _value__9638 = _5;

    /** 		if find(lChar, kv_delim) != 0  or not haskeys then*/
    _5313 = find_from(_lChar_9647, _kv_delim_9631, 1);
    _5314 = (_5313 != 0);
    _5313 = NOVALUE;
    if (_5314 != 0) {
        goto L16; // [426] 438
    }
    _5316 = (_haskeys_9636 == 0);
    if (_5316 == 0)
    {
        DeRef(_5316);
        _5316 = NOVALUE;
        goto L17; // [434] 911
    }
    else{
        DeRef(_5316);
        _5316 = NOVALUE;
    }
L16: 

    /** 			if find(lChar, kv_delim) != 0 then*/
    _5317 = find_from(_lChar_9647, _kv_delim_9631, 1);
    if (_5317 == 0)
    goto L18; // [445] 455

    /** 				lWasKV = 1*/
    _lWasKV_9649 = 1;
L18: 

    /** 			pos_ += 1*/
    _pos__9646 = _pos__9646 + 1;

    /** 			while pos_ <= length(source) do*/
L19: 
    if (IS_SEQUENCE(_source_9628)){
            _5320 = SEQ_PTR(_source_9628)->length;
    }
    else {
        _5320 = 1;
    }
    if (_pos__9646 > _5320)
    goto L1A; // [469] 506

    /** 				lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_9628);
    _lChar_9647 = (int)*(((s1_ptr)_2)->base + _pos__9646);
    if (!IS_ATOM_INT(_lChar_9647))
    _lChar_9647 = (long)DBL_PTR(_lChar_9647)->dbl;

    /** 				if find(lChar, whitespace) = 0 then*/
    _5323 = find_from(_lChar_9647, _whitespace_9635, 1);
    if (_5323 != 0)
    goto L1B; // [486] 495

    /** 					exit*/
    goto L1A; // [492] 506
L1B: 

    /** 				pos_ +=1*/
    _pos__9646 = _pos__9646 + 1;

    /** 			end while*/
    goto L19; // [503] 466
L1A: 

    /** 			lQuote = 0*/
    _lQuote_9645 = 0;

    /** 			lChar = 0*/
    _lChar_9647 = 0;

    /** 			lBracketed = {}*/
    RefDS(_5);
    DeRefi(_lBracketed_9644);
    _lBracketed_9644 = _5;

    /** 			while pos_ <= length(source) do*/
L1C: 
    if (IS_SEQUENCE(_source_9628)){
            _5326 = SEQ_PTR(_source_9628)->length;
    }
    else {
        _5326 = 1;
    }
    if (_pos__9646 > _5326)
    goto L1D; // [531] 813

    /** 				lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_9628);
    _lChar_9647 = (int)*(((s1_ptr)_2)->base + _pos__9646);
    if (!IS_ATOM_INT(_lChar_9647))
    _lChar_9647 = (long)DBL_PTR(_lChar_9647)->dbl;

    /** 				if length(lBracketed) = 0 and find(lChar, quotes) != 0 then*/
    if (IS_SEQUENCE(_lBracketed_9644)){
            _5329 = SEQ_PTR(_lBracketed_9644)->length;
    }
    else {
        _5329 = 1;
    }
    _5330 = (_5329 == 0);
    _5329 = NOVALUE;
    if (_5330 == 0) {
        goto L1E; // [550] 607
    }
    _5332 = find_from(_lChar_9647, _quotes_9633, 1);
    _5333 = (_5332 != 0);
    _5332 = NOVALUE;
    if (_5333 == 0)
    {
        DeRef(_5333);
        _5333 = NOVALUE;
        goto L1E; // [564] 607
    }
    else{
        DeRef(_5333);
        _5333 = NOVALUE;
    }

    /** 					if lChar = lQuote then*/
    if (_lChar_9647 != _lQuote_9645)
    goto L1F; // [569] 586

    /** 						lQuote = 0*/
    _lQuote_9645 = 0;

    /** 						lChar = -1*/
    _lChar_9647 = -1;
    goto L20; // [583] 789
L1F: 

    /** 					elsif lQuote = 0 then*/
    if (_lQuote_9645 != 0)
    goto L20; // [588] 789

    /** 						lQuote = lChar*/
    _lQuote_9645 = _lChar_9647;

    /** 						lChar = -1*/
    _lChar_9647 = -1;
    goto L20; // [604] 789
L1E: 

    /** 				elsif length(value_) = 1 and value_[1] = '~' and find(lChar, lStartBracket) > 0 then*/
    if (IS_SEQUENCE(_value__9638)){
            _5336 = SEQ_PTR(_value__9638)->length;
    }
    else {
        _5336 = 1;
    }
    _5337 = (_5336 == 1);
    _5336 = NOVALUE;
    if (_5337 == 0) {
        _5338 = 0;
        goto L21; // [616] 632
    }
    _2 = (int)SEQ_PTR(_value__9638);
    _5339 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_5339)) {
        _5340 = (_5339 == 126);
    }
    else {
        _5340 = binary_op(EQUALS, _5339, 126);
    }
    _5339 = NOVALUE;
    if (IS_ATOM_INT(_5340))
    _5338 = (_5340 != 0);
    else
    _5338 = DBL_PTR(_5340)->dbl != 0.0;
L21: 
    if (_5338 == 0) {
        goto L22; // [632] 669
    }
    _5342 = find_from(_lChar_9647, _lStartBracket_9642, 1);
    _5343 = (_5342 > 0);
    _5342 = NOVALUE;
    if (_5343 == 0)
    {
        DeRef(_5343);
        _5343 = NOVALUE;
        goto L22; // [646] 669
    }
    else{
        DeRef(_5343);
        _5343 = NOVALUE;
    }

    /** 					lBPos = find(lChar, lStartBracket)*/
    _lBPos_9648 = find_from(_lChar_9647, _lStartBracket_9642, 1);

    /** 					lBracketed &= lEndBracket[lBPos]*/
    _2 = (int)SEQ_PTR(_lEndBracket_9643);
    _5345 = (int)*(((s1_ptr)_2)->base + _lBPos_9648);
    Append(&_lBracketed_9644, _lBracketed_9644, _5345);
    _5345 = NOVALUE;
    goto L20; // [666] 789
L22: 

    /** 				elsif find(lChar, lStartBracket) > 0 then*/
    _5347 = find_from(_lChar_9647, _lStartBracket_9642, 1);
    if (_5347 <= 0)
    goto L23; // [676] 700

    /** 					lBPos = find(lChar, lStartBracket)*/
    _lBPos_9648 = find_from(_lChar_9647, _lStartBracket_9642, 1);

    /** 					lBracketed &= lEndBracket[lBPos]*/
    _2 = (int)SEQ_PTR(_lEndBracket_9643);
    _5350 = (int)*(((s1_ptr)_2)->base + _lBPos_9648);
    Append(&_lBracketed_9644, _lBracketed_9644, _5350);
    _5350 = NOVALUE;
    goto L20; // [697] 789
L23: 

    /** 				elsif length(lBracketed) != 0 and lChar = lBracketed[$] then*/
    if (IS_SEQUENCE(_lBracketed_9644)){
            _5352 = SEQ_PTR(_lBracketed_9644)->length;
    }
    else {
        _5352 = 1;
    }
    _5353 = (_5352 != 0);
    _5352 = NOVALUE;
    if (_5353 == 0) {
        goto L24; // [709] 745
    }
    if (IS_SEQUENCE(_lBracketed_9644)){
            _5355 = SEQ_PTR(_lBracketed_9644)->length;
    }
    else {
        _5355 = 1;
    }
    _2 = (int)SEQ_PTR(_lBracketed_9644);
    _5356 = (int)*(((s1_ptr)_2)->base + _5355);
    _5357 = (_lChar_9647 == _5356);
    _5356 = NOVALUE;
    if (_5357 == 0)
    {
        DeRef(_5357);
        _5357 = NOVALUE;
        goto L24; // [725] 745
    }
    else{
        DeRef(_5357);
        _5357 = NOVALUE;
    }

    /** 					lBracketed = lBracketed[1..$-1]*/
    if (IS_SEQUENCE(_lBracketed_9644)){
            _5358 = SEQ_PTR(_lBracketed_9644)->length;
    }
    else {
        _5358 = 1;
    }
    _5359 = _5358 - 1;
    _5358 = NOVALUE;
    rhs_slice_target = (object_ptr)&_lBracketed_9644;
    RHS_Slice(_lBracketed_9644, 1, _5359);
    goto L20; // [742] 789
L24: 

    /** 				elsif length(lBracketed) = 0 and lQuote = 0 and find(lChar, lWhitePair) != 0 then*/
    if (IS_SEQUENCE(_lBracketed_9644)){
            _5361 = SEQ_PTR(_lBracketed_9644)->length;
    }
    else {
        _5361 = 1;
    }
    _5362 = (_5361 == 0);
    _5361 = NOVALUE;
    if (_5362 == 0) {
        _5363 = 0;
        goto L25; // [754] 766
    }
    _5364 = (_lQuote_9645 == 0);
    _5363 = (_5364 != 0);
L25: 
    if (_5363 == 0) {
        goto L26; // [766] 788
    }
    _5366 = find_from(_lChar_9647, _lWhitePair_9641, 1);
    _5367 = (_5366 != 0);
    _5366 = NOVALUE;
    if (_5367 == 0)
    {
        DeRef(_5367);
        _5367 = NOVALUE;
        goto L26; // [780] 788
    }
    else{
        DeRef(_5367);
        _5367 = NOVALUE;
    }

    /** 					exit*/
    goto L1D; // [785] 813
L26: 
L20: 

    /** 				if lChar > 0 then*/
    if (_lChar_9647 <= 0)
    goto L27; // [791] 802

    /** 					value_ &= lChar*/
    Append(&_value__9638, _value__9638, _lChar_9647);
L27: 

    /** 				pos_ += 1*/
    _pos__9646 = _pos__9646 + 1;

    /** 			end while*/
    goto L1C; // [810] 528
L1D: 

    /** 			if find(lChar, whitespace) != 0  then*/
    _5371 = find_from(_lChar_9647, _whitespace_9635, 1);
    if (_5371 == 0)
    goto L28; // [820] 876

    /** 				pos_ += 1*/
    _pos__9646 = _pos__9646 + 1;

    /** 				while pos_ <= length(source) do*/
L29: 
    if (IS_SEQUENCE(_source_9628)){
            _5374 = SEQ_PTR(_source_9628)->length;
    }
    else {
        _5374 = 1;
    }
    if (_pos__9646 > _5374)
    goto L2A; // [838] 875

    /** 					lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_9628);
    _lChar_9647 = (int)*(((s1_ptr)_2)->base + _pos__9646);
    if (!IS_ATOM_INT(_lChar_9647))
    _lChar_9647 = (long)DBL_PTR(_lChar_9647)->dbl;

    /** 					if find(lChar, whitespace) = 0 then*/
    _5377 = find_from(_lChar_9647, _whitespace_9635, 1);
    if (_5377 != 0)
    goto L2B; // [855] 864

    /** 						exit*/
    goto L2A; // [861] 875
L2B: 

    /** 					pos_ +=1*/
    _pos__9646 = _pos__9646 + 1;

    /** 				end while*/
    goto L29; // [872] 835
L2A: 
L28: 

    /** 			if find(lChar, pair_delim) != 0  then*/
    _5380 = find_from(_lChar_9647, _pair_delim_9629, 1);
    if (_5380 == 0)
    goto L2C; // [883] 910

    /** 				pos_ += 1*/
    _pos__9646 = _pos__9646 + 1;

    /** 				if pos_ <= length(source) then*/
    if (IS_SEQUENCE(_source_9628)){
            _5383 = SEQ_PTR(_source_9628)->length;
    }
    else {
        _5383 = 1;
    }
    if (_pos__9646 > _5383)
    goto L2D; // [898] 909

    /** 					lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_9628);
    _lChar_9647 = (int)*(((s1_ptr)_2)->base + _pos__9646);
    if (!IS_ATOM_INT(_lChar_9647))
    _lChar_9647 = (long)DBL_PTR(_lChar_9647)->dbl;
L2D: 
L2C: 
L17: 

    /** 		if find(lChar, pair_delim) != 0  then*/
    _5386 = find_from(_lChar_9647, _pair_delim_9629, 1);
    if (_5386 == 0)
    goto L2E; // [918] 929

    /** 			pos_ += 1*/
    _pos__9646 = _pos__9646 + 1;
L2E: 

    /** 		if length(value_) = 0 then*/
    if (IS_SEQUENCE(_value__9638)){
            _5389 = SEQ_PTR(_value__9638)->length;
    }
    else {
        _5389 = 1;
    }
    if (_5389 != 0)
    goto L2F; // [934] 979

    /** 			if length(key_) = 0 then*/
    if (IS_SEQUENCE(_key__9639)){
            _5391 = SEQ_PTR(_key__9639)->length;
    }
    else {
        _5391 = 1;
    }
    if (_5391 != 0)
    goto L30; // [943] 958

    /** 				lKeyValues = append(lKeyValues, {})*/
    RefDS(_5);
    Append(&_lKeyValues_9637, _lKeyValues_9637, _5);

    /** 				continue*/
    goto L6; // [955] 136
L30: 

    /** 			if not lWasKV then*/
    if (_lWasKV_9649 != 0)
    goto L31; // [960] 978

    /** 				value_ = key_*/
    RefDS(_key__9639);
    DeRef(_value__9638);
    _value__9638 = _key__9639;

    /** 				key_ = ""*/
    RefDS(_5);
    DeRefDS(_key__9639);
    _key__9639 = _5;
L31: 
L2F: 

    /** 		if length(key_) = 0 then*/
    if (IS_SEQUENCE(_key__9639)){
            _5395 = SEQ_PTR(_key__9639)->length;
    }
    else {
        _5395 = 1;
    }
    if (_5395 != 0)
    goto L32; // [984] 1008

    /** 			if haskeys then*/
    if (_haskeys_9636 == 0)
    {
        goto L33; // [990] 1007
    }
    else{
    }

    /** 				key_ =  sprintf("p[%d]", length(lKeyValues) + 1)*/
    if (IS_SEQUENCE(_lKeyValues_9637)){
            _5398 = SEQ_PTR(_lKeyValues_9637)->length;
    }
    else {
        _5398 = 1;
    }
    _5399 = _5398 + 1;
    _5398 = NOVALUE;
    DeRefDS(_key__9639);
    _key__9639 = EPrintf(-9999999, _5397, _5399);
    _5399 = NOVALUE;
L33: 
L32: 

    /** 		if length(value_) > 0 then*/
    if (IS_SEQUENCE(_value__9638)){
            _5401 = SEQ_PTR(_value__9638)->length;
    }
    else {
        _5401 = 1;
    }
    if (_5401 <= 0)
    goto L34; // [1013] 1168

    /** 			lChar = value_[1]*/
    _2 = (int)SEQ_PTR(_value__9638);
    _lChar_9647 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_lChar_9647))
    _lChar_9647 = (long)DBL_PTR(_lChar_9647)->dbl;

    /** 			lBPos = find(lChar, lStartBracket)*/
    _lBPos_9648 = find_from(_lChar_9647, _lStartBracket_9642, 1);

    /** 			if lBPos > 0 and value_[$] = lEndBracket[lBPos] then*/
    _5405 = (_lBPos_9648 > 0);
    if (_5405 == 0) {
        goto L35; // [1036] 1149
    }
    if (IS_SEQUENCE(_value__9638)){
            _5407 = SEQ_PTR(_value__9638)->length;
    }
    else {
        _5407 = 1;
    }
    _2 = (int)SEQ_PTR(_value__9638);
    _5408 = (int)*(((s1_ptr)_2)->base + _5407);
    _2 = (int)SEQ_PTR(_lEndBracket_9643);
    _5409 = (int)*(((s1_ptr)_2)->base + _lBPos_9648);
    if (IS_ATOM_INT(_5408)) {
        _5410 = (_5408 == _5409);
    }
    else {
        _5410 = binary_op(EQUALS, _5408, _5409);
    }
    _5408 = NOVALUE;
    _5409 = NOVALUE;
    if (_5410 == 0) {
        DeRef(_5410);
        _5410 = NOVALUE;
        goto L35; // [1056] 1149
    }
    else {
        if (!IS_ATOM_INT(_5410) && DBL_PTR(_5410)->dbl == 0.0){
            DeRef(_5410);
            _5410 = NOVALUE;
            goto L35; // [1056] 1149
        }
        DeRef(_5410);
        _5410 = NOVALUE;
    }
    DeRef(_5410);
    _5410 = NOVALUE;

    /** 				if lChar = '(' then*/
    if (_lChar_9647 != 40)
    goto L36; // [1061] 1108

    /** 					value_ = keyvalues(value_[2..$-1], pair_delim, kv_delim, quotes, whitespace, haskeys)*/
    if (IS_SEQUENCE(_value__9638)){
            _5413 = SEQ_PTR(_value__9638)->length;
    }
    else {
        _5413 = 1;
    }
    _5414 = _5413 - 1;
    _5413 = NOVALUE;
    rhs_slice_target = (object_ptr)&_5415;
    RHS_Slice(_value__9638, 2, _5414);
    Ref(_pair_delim_9629);
    DeRef(_5416);
    _5416 = _pair_delim_9629;
    Ref(_kv_delim_9631);
    DeRef(_5417);
    _5417 = _kv_delim_9631;
    Ref(_quotes_9633);
    DeRef(_5418);
    _5418 = _quotes_9633;
    Ref(_whitespace_9635);
    DeRef(_5419);
    _5419 = _whitespace_9635;
    DeRef(_5420);
    _5420 = _haskeys_9636;
    _0 = _value__9638;
    _value__9638 = _4keyvalues(_5415, _5416, _5417, _5418, _5419, _5420);
    DeRefDS(_0);
    _5415 = NOVALUE;
    _5416 = NOVALUE;
    _5417 = NOVALUE;
    _5418 = NOVALUE;
    _5419 = NOVALUE;
    _5420 = NOVALUE;
    goto L37; // [1105] 1167
L36: 

    /** 					value_ = keyvalues(value_[2..$-1], pair_delim, kv_delim, quotes, whitespace, 0)*/
    if (IS_SEQUENCE(_value__9638)){
            _5422 = SEQ_PTR(_value__9638)->length;
    }
    else {
        _5422 = 1;
    }
    _5423 = _5422 - 1;
    _5422 = NOVALUE;
    rhs_slice_target = (object_ptr)&_5424;
    RHS_Slice(_value__9638, 2, _5423);
    Ref(_pair_delim_9629);
    DeRef(_5425);
    _5425 = _pair_delim_9629;
    Ref(_kv_delim_9631);
    DeRef(_5426);
    _5426 = _kv_delim_9631;
    Ref(_quotes_9633);
    DeRef(_5427);
    _5427 = _quotes_9633;
    Ref(_whitespace_9635);
    DeRef(_5428);
    _5428 = _whitespace_9635;
    _0 = _value__9638;
    _value__9638 = _4keyvalues(_5424, _5425, _5426, _5427, _5428, 0);
    DeRefDS(_0);
    _5424 = NOVALUE;
    _5425 = NOVALUE;
    _5426 = NOVALUE;
    _5427 = NOVALUE;
    _5428 = NOVALUE;
    goto L37; // [1146] 1167
L35: 

    /** 			elsif lChar = '~' then*/
    if (_lChar_9647 != 126)
    goto L38; // [1151] 1166

    /** 				value_ = value_[2 .. $]*/
    if (IS_SEQUENCE(_value__9638)){
            _5431 = SEQ_PTR(_value__9638)->length;
    }
    else {
        _5431 = 1;
    }
    rhs_slice_target = (object_ptr)&_value__9638;
    RHS_Slice(_value__9638, 2, _5431);
L38: 
L37: 
L34: 

    /** 		key_ = trim(key_)*/
    RefDS(_key__9639);
    RefDS(_4443);
    _0 = _key__9639;
    _key__9639 = _4trim(_key__9639, _4443, 0);
    DeRefDS(_0);

    /** 		value_ = trim(value_)*/
    RefDS(_value__9638);
    RefDS(_4443);
    _0 = _value__9638;
    _value__9638 = _4trim(_value__9638, _4443, 0);
    DeRefDS(_0);

    /** 		if length(key_) = 0 then*/
    if (IS_SEQUENCE(_key__9639)){
            _5435 = SEQ_PTR(_key__9639)->length;
    }
    else {
        _5435 = 1;
    }
    if (_5435 != 0)
    goto L39; // [1193] 1206

    /** 			lKeyValues = append(lKeyValues, value_)*/
    RefDS(_value__9638);
    Append(&_lKeyValues_9637, _lKeyValues_9637, _value__9638);
    goto L6; // [1203] 136
L39: 

    /** 			lKeyValues = append(lKeyValues, {key_, value_})*/
    RefDS(_value__9638);
    RefDS(_key__9639);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key__9639;
    ((int *)_2)[2] = _value__9638;
    _5438 = MAKE_SEQ(_1);
    RefDS(_5438);
    Append(&_lKeyValues_9637, _lKeyValues_9637, _5438);
    DeRefDS(_5438);
    _5438 = NOVALUE;

    /** 	end while*/
    goto L6; // [1219] 136
L7: 

    /** 	return lKeyValues*/
    DeRefDS(_source_9628);
    DeRef(_pair_delim_9629);
    DeRef(_kv_delim_9631);
    DeRef(_quotes_9633);
    DeRef(_whitespace_9635);
    DeRef(_value__9638);
    DeRef(_key__9639);
    DeRef(_lAllDelim_9640);
    DeRef(_lWhitePair_9641);
    DeRefi(_lStartBracket_9642);
    DeRefi(_lEndBracket_9643);
    DeRefi(_lBracketed_9644);
    DeRef(_5296);
    _5296 = NOVALUE;
    DeRef(_5314);
    _5314 = NOVALUE;
    DeRef(_5330);
    _5330 = NOVALUE;
    DeRef(_5337);
    _5337 = NOVALUE;
    DeRef(_5353);
    _5353 = NOVALUE;
    DeRef(_5340);
    _5340 = NOVALUE;
    DeRef(_5359);
    _5359 = NOVALUE;
    DeRef(_5362);
    _5362 = NOVALUE;
    DeRef(_5364);
    _5364 = NOVALUE;
    DeRef(_5405);
    _5405 = NOVALUE;
    DeRef(_5414);
    _5414 = NOVALUE;
    DeRef(_5423);
    _5423 = NOVALUE;
    return _lKeyValues_9637;
    ;
}


int _4escape(int _s_9876, int _what_9877)
{
    int _r_9879 = NOVALUE;
    int _5445 = NOVALUE;
    int _5443 = NOVALUE;
    int _5442 = NOVALUE;
    int _5441 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence r = ""*/
    RefDS(_5);
    DeRef(_r_9879);
    _r_9879 = _5;

    /** 	for i = 1 to length(s) do*/
    if (IS_SEQUENCE(_s_9876)){
            _5441 = SEQ_PTR(_s_9876)->length;
    }
    else {
        _5441 = 1;
    }
    {
        int _i_9881;
        _i_9881 = 1;
L1: 
        if (_i_9881 > _5441){
            goto L2; // [17] 62
        }

        /** 		if find(s[i], what) then*/
        _2 = (int)SEQ_PTR(_s_9876);
        _5442 = (int)*(((s1_ptr)_2)->base + _i_9881);
        _5443 = find_from(_5442, _what_9877, 1);
        _5442 = NOVALUE;
        if (_5443 == 0)
        {
            _5443 = NOVALUE;
            goto L3; // [35] 45
        }
        else{
            _5443 = NOVALUE;
        }

        /** 			r &= "\\"*/
        Concat((object_ptr)&_r_9879, _r_9879, _731);
L3: 

        /** 		r &= s[i]*/
        _2 = (int)SEQ_PTR(_s_9876);
        _5445 = (int)*(((s1_ptr)_2)->base + _i_9881);
        if (IS_SEQUENCE(_r_9879) && IS_ATOM(_5445)) {
            Ref(_5445);
            Append(&_r_9879, _r_9879, _5445);
        }
        else if (IS_ATOM(_r_9879) && IS_SEQUENCE(_5445)) {
        }
        else {
            Concat((object_ptr)&_r_9879, _r_9879, _5445);
        }
        _5445 = NOVALUE;

        /** 	end for*/
        _i_9881 = _i_9881 + 1;
        goto L1; // [57] 24
L2: 
        ;
    }

    /** 	return r*/
    DeRefDS(_s_9876);
    DeRefDS(_what_9877);
    return _r_9879;
    ;
}


int _4quote(int _text_in_9891, int _quote_pair_9892, int _esc_9894, int _sp_9896)
{
    int _5533 = NOVALUE;
    int _5532 = NOVALUE;
    int _5531 = NOVALUE;
    int _5529 = NOVALUE;
    int _5528 = NOVALUE;
    int _5527 = NOVALUE;
    int _5525 = NOVALUE;
    int _5524 = NOVALUE;
    int _5523 = NOVALUE;
    int _5522 = NOVALUE;
    int _5521 = NOVALUE;
    int _5520 = NOVALUE;
    int _5519 = NOVALUE;
    int _5518 = NOVALUE;
    int _5517 = NOVALUE;
    int _5515 = NOVALUE;
    int _5514 = NOVALUE;
    int _5513 = NOVALUE;
    int _5511 = NOVALUE;
    int _5510 = NOVALUE;
    int _5509 = NOVALUE;
    int _5508 = NOVALUE;
    int _5507 = NOVALUE;
    int _5506 = NOVALUE;
    int _5505 = NOVALUE;
    int _5504 = NOVALUE;
    int _5503 = NOVALUE;
    int _5501 = NOVALUE;
    int _5500 = NOVALUE;
    int _5498 = NOVALUE;
    int _5497 = NOVALUE;
    int _5496 = NOVALUE;
    int _5494 = NOVALUE;
    int _5493 = NOVALUE;
    int _5492 = NOVALUE;
    int _5491 = NOVALUE;
    int _5490 = NOVALUE;
    int _5489 = NOVALUE;
    int _5488 = NOVALUE;
    int _5487 = NOVALUE;
    int _5486 = NOVALUE;
    int _5485 = NOVALUE;
    int _5484 = NOVALUE;
    int _5483 = NOVALUE;
    int _5482 = NOVALUE;
    int _5481 = NOVALUE;
    int _5480 = NOVALUE;
    int _5479 = NOVALUE;
    int _5478 = NOVALUE;
    int _5475 = NOVALUE;
    int _5474 = NOVALUE;
    int _5473 = NOVALUE;
    int _5472 = NOVALUE;
    int _5471 = NOVALUE;
    int _5470 = NOVALUE;
    int _5469 = NOVALUE;
    int _5468 = NOVALUE;
    int _5467 = NOVALUE;
    int _5466 = NOVALUE;
    int _5465 = NOVALUE;
    int _5464 = NOVALUE;
    int _5463 = NOVALUE;
    int _5462 = NOVALUE;
    int _5459 = NOVALUE;
    int _5457 = NOVALUE;
    int _5456 = NOVALUE;
    int _5454 = NOVALUE;
    int _5452 = NOVALUE;
    int _5451 = NOVALUE;
    int _5450 = NOVALUE;
    int _5448 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_esc_9894)) {
        _1 = (long)(DBL_PTR(_esc_9894)->dbl);
        if (UNIQUE(DBL_PTR(_esc_9894)) && (DBL_PTR(_esc_9894)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_esc_9894);
        _esc_9894 = _1;
    }

    /** 	if length(text_in) = 0 then*/
    if (IS_SEQUENCE(_text_in_9891)){
            _5448 = SEQ_PTR(_text_in_9891)->length;
    }
    else {
        _5448 = 1;
    }
    if (_5448 != 0)
    goto L1; // [10] 21

    /** 		return text_in*/
    DeRef(_quote_pair_9892);
    DeRef(_sp_9896);
    return _text_in_9891;
L1: 

    /** 	if atom(quote_pair) then*/
    _5450 = IS_ATOM(_quote_pair_9892);
    if (_5450 == 0)
    {
        _5450 = NOVALUE;
        goto L2; // [26] 46
    }
    else{
        _5450 = NOVALUE;
    }

    /** 		quote_pair = {{quote_pair}, {quote_pair}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_quote_pair_9892);
    *((int *)(_2+4)) = _quote_pair_9892;
    _5451 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_quote_pair_9892);
    *((int *)(_2+4)) = _quote_pair_9892;
    _5452 = MAKE_SEQ(_1);
    DeRef(_quote_pair_9892);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5451;
    ((int *)_2)[2] = _5452;
    _quote_pair_9892 = MAKE_SEQ(_1);
    _5452 = NOVALUE;
    _5451 = NOVALUE;
    goto L3; // [43] 89
L2: 

    /** 	elsif length(quote_pair) = 1 then*/
    if (IS_SEQUENCE(_quote_pair_9892)){
            _5454 = SEQ_PTR(_quote_pair_9892)->length;
    }
    else {
        _5454 = 1;
    }
    if (_5454 != 1)
    goto L4; // [51] 72

    /** 		quote_pair = {quote_pair[1], quote_pair[1]}*/
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5456 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5457 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_5457);
    Ref(_5456);
    DeRef(_quote_pair_9892);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5456;
    ((int *)_2)[2] = _5457;
    _quote_pair_9892 = MAKE_SEQ(_1);
    _5457 = NOVALUE;
    _5456 = NOVALUE;
    goto L3; // [69] 89
L4: 

    /** 	elsif length(quote_pair) = 0 then*/
    if (IS_SEQUENCE(_quote_pair_9892)){
            _5459 = SEQ_PTR(_quote_pair_9892)->length;
    }
    else {
        _5459 = 1;
    }
    if (_5459 != 0)
    goto L5; // [77] 88

    /** 		quote_pair = {"\"", "\""}*/
    RefDS(_5440);
    RefDS(_5440);
    DeRef(_quote_pair_9892);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5440;
    ((int *)_2)[2] = _5440;
    _quote_pair_9892 = MAKE_SEQ(_1);
L5: 
L3: 

    /** 	if sequence(text_in[1]) then*/
    _2 = (int)SEQ_PTR(_text_in_9891);
    _5462 = (int)*(((s1_ptr)_2)->base + 1);
    _5463 = IS_SEQUENCE(_5462);
    _5462 = NOVALUE;
    if (_5463 == 0)
    {
        _5463 = NOVALUE;
        goto L6; // [98] 166
    }
    else{
        _5463 = NOVALUE;
    }

    /** 		for i = 1 to length(text_in) do*/
    if (IS_SEQUENCE(_text_in_9891)){
            _5464 = SEQ_PTR(_text_in_9891)->length;
    }
    else {
        _5464 = 1;
    }
    {
        int _i_9919;
        _i_9919 = 1;
L7: 
        if (_i_9919 > _5464){
            goto L8; // [106] 159
        }

        /** 			if sequence(text_in[i]) then*/
        _2 = (int)SEQ_PTR(_text_in_9891);
        _5465 = (int)*(((s1_ptr)_2)->base + _i_9919);
        _5466 = IS_SEQUENCE(_5465);
        _5465 = NOVALUE;
        if (_5466 == 0)
        {
            _5466 = NOVALUE;
            goto L9; // [122] 152
        }
        else{
            _5466 = NOVALUE;
        }

        /** 				text_in[i] = quote(text_in[i], quote_pair, esc, sp)*/
        _2 = (int)SEQ_PTR(_text_in_9891);
        _5467 = (int)*(((s1_ptr)_2)->base + _i_9919);
        Ref(_quote_pair_9892);
        DeRef(_5468);
        _5468 = _quote_pair_9892;
        DeRef(_5469);
        _5469 = _esc_9894;
        Ref(_sp_9896);
        DeRef(_5470);
        _5470 = _sp_9896;
        Ref(_5467);
        _5471 = _4quote(_5467, _5468, _5469, _5470);
        _5467 = NOVALUE;
        _5468 = NOVALUE;
        _5469 = NOVALUE;
        _5470 = NOVALUE;
        _2 = (int)SEQ_PTR(_text_in_9891);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _text_in_9891 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_9919);
        _1 = *(int *)_2;
        *(int *)_2 = _5471;
        if( _1 != _5471 ){
            DeRef(_1);
        }
        _5471 = NOVALUE;
L9: 

        /** 		end for*/
        _i_9919 = _i_9919 + 1;
        goto L7; // [154] 113
L8: 
        ;
    }

    /** 		return text_in*/
    DeRef(_quote_pair_9892);
    DeRef(_sp_9896);
    return _text_in_9891;
L6: 

    /** 	for i = 1 to length(sp) do*/
    if (IS_SEQUENCE(_sp_9896)){
            _5472 = SEQ_PTR(_sp_9896)->length;
    }
    else {
        _5472 = 1;
    }
    {
        int _i_9930;
        _i_9930 = 1;
LA: 
        if (_i_9930 > _5472){
            goto LB; // [171] 220
        }

        /** 		if find(sp[i], text_in) then*/
        _2 = (int)SEQ_PTR(_sp_9896);
        _5473 = (int)*(((s1_ptr)_2)->base + _i_9930);
        _5474 = find_from(_5473, _text_in_9891, 1);
        _5473 = NOVALUE;
        if (_5474 == 0)
        {
            _5474 = NOVALUE;
            goto LC; // [189] 197
        }
        else{
            _5474 = NOVALUE;
        }

        /** 			exit*/
        goto LB; // [194] 220
LC: 

        /** 		if i = length(sp) then*/
        if (IS_SEQUENCE(_sp_9896)){
                _5475 = SEQ_PTR(_sp_9896)->length;
        }
        else {
            _5475 = 1;
        }
        if (_i_9930 != _5475)
        goto LD; // [202] 213

        /** 			return text_in*/
        DeRef(_quote_pair_9892);
        DeRef(_sp_9896);
        return _text_in_9891;
LD: 

        /** 	end for*/
        _i_9930 = _i_9930 + 1;
        goto LA; // [215] 178
LB: 
        ;
    }

    /** 	if esc >= 0  then*/
    if (_esc_9894 < 0)
    goto LE; // [222] 561

    /** 		if atom(quote_pair[1]) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5478 = (int)*(((s1_ptr)_2)->base + 1);
    _5479 = IS_ATOM(_5478);
    _5478 = NOVALUE;
    if (_5479 == 0)
    {
        _5479 = NOVALUE;
        goto LF; // [235] 253
    }
    else{
        _5479 = NOVALUE;
    }

    /** 			quote_pair[1] = {quote_pair[1]}*/
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5480 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_5480);
    *((int *)(_2+4)) = _5480;
    _5481 = MAKE_SEQ(_1);
    _5480 = NOVALUE;
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _quote_pair_9892 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _5481;
    if( _1 != _5481 ){
        DeRef(_1);
    }
    _5481 = NOVALUE;
LF: 

    /** 		if atom(quote_pair[2]) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5482 = (int)*(((s1_ptr)_2)->base + 2);
    _5483 = IS_ATOM(_5482);
    _5482 = NOVALUE;
    if (_5483 == 0)
    {
        _5483 = NOVALUE;
        goto L10; // [262] 280
    }
    else{
        _5483 = NOVALUE;
    }

    /** 			quote_pair[2] = {quote_pair[2]}*/
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5484 = (int)*(((s1_ptr)_2)->base + 2);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_5484);
    *((int *)(_2+4)) = _5484;
    _5485 = MAKE_SEQ(_1);
    _5484 = NOVALUE;
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _quote_pair_9892 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _5485;
    if( _1 != _5485 ){
        DeRef(_1);
    }
    _5485 = NOVALUE;
L10: 

    /** 		if equal(quote_pair[1], quote_pair[2]) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5486 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5487 = (int)*(((s1_ptr)_2)->base + 2);
    if (_5486 == _5487)
    _5488 = 1;
    else if (IS_ATOM_INT(_5486) && IS_ATOM_INT(_5487))
    _5488 = 0;
    else
    _5488 = (compare(_5486, _5487) == 0);
    _5486 = NOVALUE;
    _5487 = NOVALUE;
    if (_5488 == 0)
    {
        _5488 = NOVALUE;
        goto L11; // [294] 372
    }
    else{
        _5488 = NOVALUE;
    }

    /** 			if match(quote_pair[1], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5489 = (int)*(((s1_ptr)_2)->base + 1);
    _5490 = e_match_from(_5489, _text_in_9891, 1);
    _5489 = NOVALUE;
    if (_5490 == 0)
    {
        _5490 = NOVALUE;
        goto L12; // [308] 560
    }
    else{
        _5490 = NOVALUE;
    }

    /** 				if match(esc & quote_pair[1], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5491 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_9894) && IS_ATOM(_5491)) {
    }
    else if (IS_ATOM(_esc_9894) && IS_SEQUENCE(_5491)) {
        Prepend(&_5492, _5491, _esc_9894);
    }
    else {
        Concat((object_ptr)&_5492, _esc_9894, _5491);
    }
    _5491 = NOVALUE;
    _5493 = e_match_from(_5492, _text_in_9891, 1);
    DeRefDS(_5492);
    _5492 = NOVALUE;
    if (_5493 == 0)
    {
        _5493 = NOVALUE;
        goto L13; // [326] 345
    }
    else{
        _5493 = NOVALUE;
    }

    /** 					text_in = search:match_replace(esc, text_in, esc & esc)*/
    Concat((object_ptr)&_5494, _esc_9894, _esc_9894);
    RefDS(_text_in_9891);
    _0 = _text_in_9891;
    _text_in_9891 = _7match_replace(_esc_9894, _text_in_9891, _5494, 0);
    DeRefDS(_0);
    _5494 = NOVALUE;
L13: 

    /** 				text_in = search:match_replace(quote_pair[1], text_in, esc & quote_pair[1])*/
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5496 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5497 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_9894) && IS_ATOM(_5497)) {
    }
    else if (IS_ATOM(_esc_9894) && IS_SEQUENCE(_5497)) {
        Prepend(&_5498, _5497, _esc_9894);
    }
    else {
        Concat((object_ptr)&_5498, _esc_9894, _5497);
    }
    _5497 = NOVALUE;
    Ref(_5496);
    RefDS(_text_in_9891);
    _0 = _text_in_9891;
    _text_in_9891 = _7match_replace(_5496, _text_in_9891, _5498, 0);
    DeRefDS(_0);
    _5496 = NOVALUE;
    _5498 = NOVALUE;
    goto L12; // [369] 560
L11: 

    /** 			if match(quote_pair[1], text_in) or*/
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5500 = (int)*(((s1_ptr)_2)->base + 1);
    _5501 = e_match_from(_5500, _text_in_9891, 1);
    _5500 = NOVALUE;
    if (_5501 != 0) {
        goto L14; // [383] 401
    }
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5503 = (int)*(((s1_ptr)_2)->base + 2);
    _5504 = e_match_from(_5503, _text_in_9891, 1);
    _5503 = NOVALUE;
    if (_5504 == 0)
    {
        _5504 = NOVALUE;
        goto L15; // [397] 473
    }
    else{
        _5504 = NOVALUE;
    }
L14: 

    /** 				if match(esc & quote_pair[1], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5505 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_9894) && IS_ATOM(_5505)) {
    }
    else if (IS_ATOM(_esc_9894) && IS_SEQUENCE(_5505)) {
        Prepend(&_5506, _5505, _esc_9894);
    }
    else {
        Concat((object_ptr)&_5506, _esc_9894, _5505);
    }
    _5505 = NOVALUE;
    _5507 = e_match_from(_5506, _text_in_9891, 1);
    DeRefDS(_5506);
    _5506 = NOVALUE;
    if (_5507 == 0)
    {
        _5507 = NOVALUE;
        goto L16; // [416] 449
    }
    else{
        _5507 = NOVALUE;
    }

    /** 					text_in = search:match_replace(esc & quote_pair[1], text_in, esc & esc & quote_pair[1])*/
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5508 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_9894) && IS_ATOM(_5508)) {
    }
    else if (IS_ATOM(_esc_9894) && IS_SEQUENCE(_5508)) {
        Prepend(&_5509, _5508, _esc_9894);
    }
    else {
        Concat((object_ptr)&_5509, _esc_9894, _5508);
    }
    _5508 = NOVALUE;
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5510 = (int)*(((s1_ptr)_2)->base + 1);
    {
        int concat_list[3];

        concat_list[0] = _5510;
        concat_list[1] = _esc_9894;
        concat_list[2] = _esc_9894;
        Concat_N((object_ptr)&_5511, concat_list, 3);
    }
    _5510 = NOVALUE;
    RefDS(_text_in_9891);
    _0 = _text_in_9891;
    _text_in_9891 = _7match_replace(_5509, _text_in_9891, _5511, 0);
    DeRefDS(_0);
    _5509 = NOVALUE;
    _5511 = NOVALUE;
L16: 

    /** 				text_in = match_replace(quote_pair[1], text_in, esc & quote_pair[1])*/
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5513 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5514 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_9894) && IS_ATOM(_5514)) {
    }
    else if (IS_ATOM(_esc_9894) && IS_SEQUENCE(_5514)) {
        Prepend(&_5515, _5514, _esc_9894);
    }
    else {
        Concat((object_ptr)&_5515, _esc_9894, _5514);
    }
    _5514 = NOVALUE;
    Ref(_5513);
    RefDS(_text_in_9891);
    _0 = _text_in_9891;
    _text_in_9891 = _7match_replace(_5513, _text_in_9891, _5515, 0);
    DeRefDS(_0);
    _5513 = NOVALUE;
    _5515 = NOVALUE;
L15: 

    /** 			if match(quote_pair[2], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5517 = (int)*(((s1_ptr)_2)->base + 2);
    _5518 = e_match_from(_5517, _text_in_9891, 1);
    _5517 = NOVALUE;
    if (_5518 == 0)
    {
        _5518 = NOVALUE;
        goto L17; // [484] 559
    }
    else{
        _5518 = NOVALUE;
    }

    /** 				if match(esc & quote_pair[2], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5519 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_esc_9894) && IS_ATOM(_5519)) {
    }
    else if (IS_ATOM(_esc_9894) && IS_SEQUENCE(_5519)) {
        Prepend(&_5520, _5519, _esc_9894);
    }
    else {
        Concat((object_ptr)&_5520, _esc_9894, _5519);
    }
    _5519 = NOVALUE;
    _5521 = e_match_from(_5520, _text_in_9891, 1);
    DeRefDS(_5520);
    _5520 = NOVALUE;
    if (_5521 == 0)
    {
        _5521 = NOVALUE;
        goto L18; // [502] 535
    }
    else{
        _5521 = NOVALUE;
    }

    /** 					text_in = search:match_replace(esc & quote_pair[2], text_in, esc & esc & quote_pair[2])*/
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5522 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_esc_9894) && IS_ATOM(_5522)) {
    }
    else if (IS_ATOM(_esc_9894) && IS_SEQUENCE(_5522)) {
        Prepend(&_5523, _5522, _esc_9894);
    }
    else {
        Concat((object_ptr)&_5523, _esc_9894, _5522);
    }
    _5522 = NOVALUE;
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5524 = (int)*(((s1_ptr)_2)->base + 2);
    {
        int concat_list[3];

        concat_list[0] = _5524;
        concat_list[1] = _esc_9894;
        concat_list[2] = _esc_9894;
        Concat_N((object_ptr)&_5525, concat_list, 3);
    }
    _5524 = NOVALUE;
    RefDS(_text_in_9891);
    _0 = _text_in_9891;
    _text_in_9891 = _7match_replace(_5523, _text_in_9891, _5525, 0);
    DeRefDS(_0);
    _5523 = NOVALUE;
    _5525 = NOVALUE;
L18: 

    /** 				text_in = search:match_replace(quote_pair[2], text_in, esc & quote_pair[2])*/
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5527 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5528 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_esc_9894) && IS_ATOM(_5528)) {
    }
    else if (IS_ATOM(_esc_9894) && IS_SEQUENCE(_5528)) {
        Prepend(&_5529, _5528, _esc_9894);
    }
    else {
        Concat((object_ptr)&_5529, _esc_9894, _5528);
    }
    _5528 = NOVALUE;
    Ref(_5527);
    RefDS(_text_in_9891);
    _0 = _text_in_9891;
    _text_in_9891 = _7match_replace(_5527, _text_in_9891, _5529, 0);
    DeRefDS(_0);
    _5527 = NOVALUE;
    _5529 = NOVALUE;
L17: 
L12: 
LE: 

    /** 	return quote_pair[1] & text_in & quote_pair[2]*/
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5531 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_9892);
    _5532 = (int)*(((s1_ptr)_2)->base + 2);
    {
        int concat_list[3];

        concat_list[0] = _5532;
        concat_list[1] = _text_in_9891;
        concat_list[2] = _5531;
        Concat_N((object_ptr)&_5533, concat_list, 3);
    }
    _5532 = NOVALUE;
    _5531 = NOVALUE;
    DeRefDS(_text_in_9891);
    DeRef(_quote_pair_9892);
    DeRef(_sp_9896);
    return _5533;
    ;
}


int _4dequote(int _text_in_10009, int _quote_pairs_10010, int _esc_10013)
{
    int _pos_10078 = NOVALUE;
    int _5611 = NOVALUE;
    int _5610 = NOVALUE;
    int _5609 = NOVALUE;
    int _5608 = NOVALUE;
    int _5607 = NOVALUE;
    int _5606 = NOVALUE;
    int _5605 = NOVALUE;
    int _5604 = NOVALUE;
    int _5603 = NOVALUE;
    int _5602 = NOVALUE;
    int _5601 = NOVALUE;
    int _5599 = NOVALUE;
    int _5598 = NOVALUE;
    int _5597 = NOVALUE;
    int _5596 = NOVALUE;
    int _5595 = NOVALUE;
    int _5594 = NOVALUE;
    int _5593 = NOVALUE;
    int _5592 = NOVALUE;
    int _5591 = NOVALUE;
    int _5590 = NOVALUE;
    int _5589 = NOVALUE;
    int _5586 = NOVALUE;
    int _5585 = NOVALUE;
    int _5584 = NOVALUE;
    int _5583 = NOVALUE;
    int _5582 = NOVALUE;
    int _5581 = NOVALUE;
    int _5580 = NOVALUE;
    int _5579 = NOVALUE;
    int _5578 = NOVALUE;
    int _5577 = NOVALUE;
    int _5576 = NOVALUE;
    int _5575 = NOVALUE;
    int _5574 = NOVALUE;
    int _5573 = NOVALUE;
    int _5572 = NOVALUE;
    int _5571 = NOVALUE;
    int _5569 = NOVALUE;
    int _5568 = NOVALUE;
    int _5567 = NOVALUE;
    int _5566 = NOVALUE;
    int _5565 = NOVALUE;
    int _5564 = NOVALUE;
    int _5563 = NOVALUE;
    int _5562 = NOVALUE;
    int _5561 = NOVALUE;
    int _5560 = NOVALUE;
    int _5559 = NOVALUE;
    int _5558 = NOVALUE;
    int _5557 = NOVALUE;
    int _5556 = NOVALUE;
    int _5555 = NOVALUE;
    int _5554 = NOVALUE;
    int _5553 = NOVALUE;
    int _5552 = NOVALUE;
    int _5550 = NOVALUE;
    int _5548 = NOVALUE;
    int _5546 = NOVALUE;
    int _5545 = NOVALUE;
    int _5543 = NOVALUE;
    int _5541 = NOVALUE;
    int _5540 = NOVALUE;
    int _5539 = NOVALUE;
    int _5538 = NOVALUE;
    int _5536 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_esc_10013)) {
        _1 = (long)(DBL_PTR(_esc_10013)->dbl);
        if (UNIQUE(DBL_PTR(_esc_10013)) && (DBL_PTR(_esc_10013)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_esc_10013);
        _esc_10013 = _1;
    }

    /** 	if length(text_in) = 0 then*/
    if (IS_SEQUENCE(_text_in_10009)){
            _5536 = SEQ_PTR(_text_in_10009)->length;
    }
    else {
        _5536 = 1;
    }
    if (_5536 != 0)
    goto L1; // [10] 21

    /** 		return text_in*/
    DeRef(_quote_pairs_10010);
    return _text_in_10009;
L1: 

    /** 	if atom(quote_pairs) then*/
    _5538 = IS_ATOM(_quote_pairs_10010);
    if (_5538 == 0)
    {
        _5538 = NOVALUE;
        goto L2; // [26] 50
    }
    else{
        _5538 = NOVALUE;
    }

    /** 		quote_pairs = {{{quote_pairs}, {quote_pairs}}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_quote_pairs_10010);
    *((int *)(_2+4)) = _quote_pairs_10010;
    _5539 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_quote_pairs_10010);
    *((int *)(_2+4)) = _quote_pairs_10010;
    _5540 = MAKE_SEQ(_1);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5539;
    ((int *)_2)[2] = _5540;
    _5541 = MAKE_SEQ(_1);
    _5540 = NOVALUE;
    _5539 = NOVALUE;
    _0 = _quote_pairs_10010;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5541;
    _quote_pairs_10010 = MAKE_SEQ(_1);
    DeRef(_0);
    _5541 = NOVALUE;
    goto L3; // [47] 97
L2: 

    /** 	elsif length(quote_pairs) = 1 then*/
    if (IS_SEQUENCE(_quote_pairs_10010)){
            _5543 = SEQ_PTR(_quote_pairs_10010)->length;
    }
    else {
        _5543 = 1;
    }
    if (_5543 != 1)
    goto L4; // [55] 76

    /** 		quote_pairs = {quote_pairs[1], quote_pairs[1]}*/
    _2 = (int)SEQ_PTR(_quote_pairs_10010);
    _5545 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pairs_10010);
    _5546 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_5546);
    Ref(_5545);
    DeRef(_quote_pairs_10010);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5545;
    ((int *)_2)[2] = _5546;
    _quote_pairs_10010 = MAKE_SEQ(_1);
    _5546 = NOVALUE;
    _5545 = NOVALUE;
    goto L3; // [73] 97
L4: 

    /** 	elsif length(quote_pairs) = 0 then*/
    if (IS_SEQUENCE(_quote_pairs_10010)){
            _5548 = SEQ_PTR(_quote_pairs_10010)->length;
    }
    else {
        _5548 = 1;
    }
    if (_5548 != 0)
    goto L5; // [81] 96

    /** 		quote_pairs = {{"\"", "\""}}*/
    RefDS(_5440);
    RefDS(_5440);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5440;
    ((int *)_2)[2] = _5440;
    _5550 = MAKE_SEQ(_1);
    _0 = _quote_pairs_10010;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5550;
    _quote_pairs_10010 = MAKE_SEQ(_1);
    DeRef(_0);
    _5550 = NOVALUE;
L5: 
L3: 

    /** 	if sequence(text_in[1]) then*/
    _2 = (int)SEQ_PTR(_text_in_10009);
    _5552 = (int)*(((s1_ptr)_2)->base + 1);
    _5553 = IS_SEQUENCE(_5552);
    _5552 = NOVALUE;
    if (_5553 == 0)
    {
        _5553 = NOVALUE;
        goto L6; // [106] 170
    }
    else{
        _5553 = NOVALUE;
    }

    /** 		for i = 1 to length(text_in) do*/
    if (IS_SEQUENCE(_text_in_10009)){
            _5554 = SEQ_PTR(_text_in_10009)->length;
    }
    else {
        _5554 = 1;
    }
    {
        int _i_10038;
        _i_10038 = 1;
L7: 
        if (_i_10038 > _5554){
            goto L8; // [114] 163
        }

        /** 			if sequence(text_in[i]) then*/
        _2 = (int)SEQ_PTR(_text_in_10009);
        _5555 = (int)*(((s1_ptr)_2)->base + _i_10038);
        _5556 = IS_SEQUENCE(_5555);
        _5555 = NOVALUE;
        if (_5556 == 0)
        {
            _5556 = NOVALUE;
            goto L9; // [130] 156
        }
        else{
            _5556 = NOVALUE;
        }

        /** 				text_in[i] = dequote(text_in[i], quote_pairs, esc)*/
        _2 = (int)SEQ_PTR(_text_in_10009);
        _5557 = (int)*(((s1_ptr)_2)->base + _i_10038);
        Ref(_quote_pairs_10010);
        DeRef(_5558);
        _5558 = _quote_pairs_10010;
        DeRef(_5559);
        _5559 = _esc_10013;
        Ref(_5557);
        _5560 = _4dequote(_5557, _5558, _5559);
        _5557 = NOVALUE;
        _5558 = NOVALUE;
        _5559 = NOVALUE;
        _2 = (int)SEQ_PTR(_text_in_10009);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _text_in_10009 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_10038);
        _1 = *(int *)_2;
        *(int *)_2 = _5560;
        if( _1 != _5560 ){
            DeRef(_1);
        }
        _5560 = NOVALUE;
L9: 

        /** 		end for*/
        _i_10038 = _i_10038 + 1;
        goto L7; // [158] 121
L8: 
        ;
    }

    /** 		return text_in*/
    DeRef(_quote_pairs_10010);
    return _text_in_10009;
L6: 

    /** 	for i = 1 to length(quote_pairs) do*/
    if (IS_SEQUENCE(_quote_pairs_10010)){
            _5561 = SEQ_PTR(_quote_pairs_10010)->length;
    }
    else {
        _5561 = 1;
    }
    {
        int _i_10048;
        _i_10048 = 1;
LA: 
        if (_i_10048 > _5561){
            goto LB; // [175] 466
        }

        /** 		if length(text_in) >= length(quote_pairs[i][1]) + length(quote_pairs[i][2]) then*/
        if (IS_SEQUENCE(_text_in_10009)){
                _5562 = SEQ_PTR(_text_in_10009)->length;
        }
        else {
            _5562 = 1;
        }
        _2 = (int)SEQ_PTR(_quote_pairs_10010);
        _5563 = (int)*(((s1_ptr)_2)->base + _i_10048);
        _2 = (int)SEQ_PTR(_5563);
        _5564 = (int)*(((s1_ptr)_2)->base + 1);
        _5563 = NOVALUE;
        if (IS_SEQUENCE(_5564)){
                _5565 = SEQ_PTR(_5564)->length;
        }
        else {
            _5565 = 1;
        }
        _5564 = NOVALUE;
        _2 = (int)SEQ_PTR(_quote_pairs_10010);
        _5566 = (int)*(((s1_ptr)_2)->base + _i_10048);
        _2 = (int)SEQ_PTR(_5566);
        _5567 = (int)*(((s1_ptr)_2)->base + 2);
        _5566 = NOVALUE;
        if (IS_SEQUENCE(_5567)){
                _5568 = SEQ_PTR(_5567)->length;
        }
        else {
            _5568 = 1;
        }
        _5567 = NOVALUE;
        _5569 = _5565 + _5568;
        if ((long)((unsigned long)_5569 + (unsigned long)HIGH_BITS) >= 0) 
        _5569 = NewDouble((double)_5569);
        _5565 = NOVALUE;
        _5568 = NOVALUE;
        if (binary_op_a(LESS, _5562, _5569)){
            _5562 = NOVALUE;
            DeRef(_5569);
            _5569 = NOVALUE;
            goto LC; // [213] 459
        }
        _5562 = NOVALUE;
        DeRef(_5569);
        _5569 = NOVALUE;

        /** 			if search:begins(quote_pairs[i][1], text_in) and search:ends(quote_pairs[i][2], text_in) then*/
        _2 = (int)SEQ_PTR(_quote_pairs_10010);
        _5571 = (int)*(((s1_ptr)_2)->base + _i_10048);
        _2 = (int)SEQ_PTR(_5571);
        _5572 = (int)*(((s1_ptr)_2)->base + 1);
        _5571 = NOVALUE;
        Ref(_5572);
        RefDS(_text_in_10009);
        _5573 = _7begins(_5572, _text_in_10009);
        _5572 = NOVALUE;
        if (IS_ATOM_INT(_5573)) {
            if (_5573 == 0) {
                goto LD; // [232] 456
            }
        }
        else {
            if (DBL_PTR(_5573)->dbl == 0.0) {
                goto LD; // [232] 456
            }
        }
        _2 = (int)SEQ_PTR(_quote_pairs_10010);
        _5575 = (int)*(((s1_ptr)_2)->base + _i_10048);
        _2 = (int)SEQ_PTR(_5575);
        _5576 = (int)*(((s1_ptr)_2)->base + 2);
        _5575 = NOVALUE;
        Ref(_5576);
        RefDS(_text_in_10009);
        _5577 = _7ends(_5576, _text_in_10009);
        _5576 = NOVALUE;
        if (_5577 == 0) {
            DeRef(_5577);
            _5577 = NOVALUE;
            goto LD; // [250] 456
        }
        else {
            if (!IS_ATOM_INT(_5577) && DBL_PTR(_5577)->dbl == 0.0){
                DeRef(_5577);
                _5577 = NOVALUE;
                goto LD; // [250] 456
            }
            DeRef(_5577);
            _5577 = NOVALUE;
        }
        DeRef(_5577);
        _5577 = NOVALUE;

        /** 				text_in = text_in[1 + length(quote_pairs[i][1]) .. $ - length(quote_pairs[i][2])]*/
        _2 = (int)SEQ_PTR(_quote_pairs_10010);
        _5578 = (int)*(((s1_ptr)_2)->base + _i_10048);
        _2 = (int)SEQ_PTR(_5578);
        _5579 = (int)*(((s1_ptr)_2)->base + 1);
        _5578 = NOVALUE;
        if (IS_SEQUENCE(_5579)){
                _5580 = SEQ_PTR(_5579)->length;
        }
        else {
            _5580 = 1;
        }
        _5579 = NOVALUE;
        _5581 = _5580 + 1;
        _5580 = NOVALUE;
        if (IS_SEQUENCE(_text_in_10009)){
                _5582 = SEQ_PTR(_text_in_10009)->length;
        }
        else {
            _5582 = 1;
        }
        _2 = (int)SEQ_PTR(_quote_pairs_10010);
        _5583 = (int)*(((s1_ptr)_2)->base + _i_10048);
        _2 = (int)SEQ_PTR(_5583);
        _5584 = (int)*(((s1_ptr)_2)->base + 2);
        _5583 = NOVALUE;
        if (IS_SEQUENCE(_5584)){
                _5585 = SEQ_PTR(_5584)->length;
        }
        else {
            _5585 = 1;
        }
        _5584 = NOVALUE;
        _5586 = _5582 - _5585;
        _5582 = NOVALUE;
        _5585 = NOVALUE;
        rhs_slice_target = (object_ptr)&_text_in_10009;
        RHS_Slice(_text_in_10009, _5581, _5586);

        /** 				integer pos = 1*/
        _pos_10078 = 1;

        /** 				while pos > 0 with entry do*/
        goto LE; // [300] 437
LF: 
        if (_pos_10078 <= 0)
        goto L10; // [303] 449

        /** 					if search:begins(quote_pairs[i][1], text_in[pos+1 .. $]) then*/
        _2 = (int)SEQ_PTR(_quote_pairs_10010);
        _5589 = (int)*(((s1_ptr)_2)->base + _i_10048);
        _2 = (int)SEQ_PTR(_5589);
        _5590 = (int)*(((s1_ptr)_2)->base + 1);
        _5589 = NOVALUE;
        _5591 = _pos_10078 + 1;
        if (_5591 > MAXINT){
            _5591 = NewDouble((double)_5591);
        }
        if (IS_SEQUENCE(_text_in_10009)){
                _5592 = SEQ_PTR(_text_in_10009)->length;
        }
        else {
            _5592 = 1;
        }
        rhs_slice_target = (object_ptr)&_5593;
        RHS_Slice(_text_in_10009, _5591, _5592);
        Ref(_5590);
        _5594 = _7begins(_5590, _5593);
        _5590 = NOVALUE;
        _5593 = NOVALUE;
        if (_5594 == 0) {
            DeRef(_5594);
            _5594 = NOVALUE;
            goto L11; // [334] 367
        }
        else {
            if (!IS_ATOM_INT(_5594) && DBL_PTR(_5594)->dbl == 0.0){
                DeRef(_5594);
                _5594 = NOVALUE;
                goto L11; // [334] 367
            }
            DeRef(_5594);
            _5594 = NOVALUE;
        }
        DeRef(_5594);
        _5594 = NOVALUE;

        /** 						text_in = text_in[1 .. pos-1] & text_in[pos + 1 .. $]*/
        _5595 = _pos_10078 - 1;
        rhs_slice_target = (object_ptr)&_5596;
        RHS_Slice(_text_in_10009, 1, _5595);
        _5597 = _pos_10078 + 1;
        if (_5597 > MAXINT){
            _5597 = NewDouble((double)_5597);
        }
        if (IS_SEQUENCE(_text_in_10009)){
                _5598 = SEQ_PTR(_text_in_10009)->length;
        }
        else {
            _5598 = 1;
        }
        rhs_slice_target = (object_ptr)&_5599;
        RHS_Slice(_text_in_10009, _5597, _5598);
        Concat((object_ptr)&_text_in_10009, _5596, _5599);
        DeRefDS(_5596);
        _5596 = NOVALUE;
        DeRef(_5596);
        _5596 = NOVALUE;
        DeRefDS(_5599);
        _5599 = NOVALUE;
        goto L12; // [364] 434
L11: 

        /** 					elsif search:begins(quote_pairs[i][2], text_in[pos+1 .. $]) then*/
        _2 = (int)SEQ_PTR(_quote_pairs_10010);
        _5601 = (int)*(((s1_ptr)_2)->base + _i_10048);
        _2 = (int)SEQ_PTR(_5601);
        _5602 = (int)*(((s1_ptr)_2)->base + 2);
        _5601 = NOVALUE;
        _5603 = _pos_10078 + 1;
        if (_5603 > MAXINT){
            _5603 = NewDouble((double)_5603);
        }
        if (IS_SEQUENCE(_text_in_10009)){
                _5604 = SEQ_PTR(_text_in_10009)->length;
        }
        else {
            _5604 = 1;
        }
        rhs_slice_target = (object_ptr)&_5605;
        RHS_Slice(_text_in_10009, _5603, _5604);
        Ref(_5602);
        _5606 = _7begins(_5602, _5605);
        _5602 = NOVALUE;
        _5605 = NOVALUE;
        if (_5606 == 0) {
            DeRef(_5606);
            _5606 = NOVALUE;
            goto L13; // [394] 427
        }
        else {
            if (!IS_ATOM_INT(_5606) && DBL_PTR(_5606)->dbl == 0.0){
                DeRef(_5606);
                _5606 = NOVALUE;
                goto L13; // [394] 427
            }
            DeRef(_5606);
            _5606 = NOVALUE;
        }
        DeRef(_5606);
        _5606 = NOVALUE;

        /** 						text_in = text_in[1 .. pos-1] & text_in[pos + 1 .. $]*/
        _5607 = _pos_10078 - 1;
        rhs_slice_target = (object_ptr)&_5608;
        RHS_Slice(_text_in_10009, 1, _5607);
        _5609 = _pos_10078 + 1;
        if (_5609 > MAXINT){
            _5609 = NewDouble((double)_5609);
        }
        if (IS_SEQUENCE(_text_in_10009)){
                _5610 = SEQ_PTR(_text_in_10009)->length;
        }
        else {
            _5610 = 1;
        }
        rhs_slice_target = (object_ptr)&_5611;
        RHS_Slice(_text_in_10009, _5609, _5610);
        Concat((object_ptr)&_text_in_10009, _5608, _5611);
        DeRefDS(_5608);
        _5608 = NOVALUE;
        DeRef(_5608);
        _5608 = NOVALUE;
        DeRefDS(_5611);
        _5611 = NOVALUE;
        goto L12; // [424] 434
L13: 

        /** 						pos += 1*/
        _pos_10078 = _pos_10078 + 1;
L12: 

        /** 				entry*/
LE: 

        /** 					pos = find(esc, text_in, pos)*/
        _pos_10078 = find_from(_esc_10013, _text_in_10009, _pos_10078);

        /** 				end while*/
        goto LF; // [446] 303
L10: 

        /** 				exit*/
        goto LB; // [453] 466
LD: 
LC: 

        /** 	end for*/
        _i_10048 = _i_10048 + 1;
        goto LA; // [461] 182
LB: 
        ;
    }

    /** 	return text_in*/
    DeRef(_quote_pairs_10010);
    _5564 = NOVALUE;
    _5567 = NOVALUE;
    _5579 = NOVALUE;
    DeRef(_5573);
    _5573 = NOVALUE;
    DeRef(_5581);
    _5581 = NOVALUE;
    _5584 = NOVALUE;
    DeRef(_5586);
    _5586 = NOVALUE;
    DeRef(_5595);
    _5595 = NOVALUE;
    DeRef(_5591);
    _5591 = NOVALUE;
    DeRef(_5607);
    _5607 = NOVALUE;
    DeRef(_5597);
    _5597 = NOVALUE;
    DeRef(_5603);
    _5603 = NOVALUE;
    DeRef(_5609);
    _5609 = NOVALUE;
    return _text_in_10009;
    ;
}


int _4format(int _format_pattern_10112, int _arg_list_10113)
{
    int _result_10114 = NOVALUE;
    int _in_token_10115 = NOVALUE;
    int _tch_10116 = NOVALUE;
    int _i_10117 = NOVALUE;
    int _tend_10118 = NOVALUE;
    int _cap_10119 = NOVALUE;
    int _align_10120 = NOVALUE;
    int _psign_10121 = NOVALUE;
    int _msign_10122 = NOVALUE;
    int _zfill_10123 = NOVALUE;
    int _bwz_10124 = NOVALUE;
    int _spacer_10125 = NOVALUE;
    int _alt_10126 = NOVALUE;
    int _width_10127 = NOVALUE;
    int _decs_10128 = NOVALUE;
    int _pos_10129 = NOVALUE;
    int _argn_10130 = NOVALUE;
    int _argl_10131 = NOVALUE;
    int _trimming_10132 = NOVALUE;
    int _hexout_10133 = NOVALUE;
    int _binout_10134 = NOVALUE;
    int _tsep_10135 = NOVALUE;
    int _istext_10136 = NOVALUE;
    int _prevargv_10137 = NOVALUE;
    int _currargv_10138 = NOVALUE;
    int _idname_10139 = NOVALUE;
    int _envsym_10140 = NOVALUE;
    int _envvar_10141 = NOVALUE;
    int _ep_10142 = NOVALUE;
    int _pflag_10143 = NOVALUE;
    int _count_10144 = NOVALUE;
    int _sp_10215 = NOVALUE;
    int _sp_10250 = NOVALUE;
    int _argtext_10297 = NOVALUE;
    int _tempv_10533 = NOVALUE;
    int _pretty_sprint_inlined_pretty_sprint_at_2492_10588 = NOVALUE;
    int _options_inlined_pretty_sprint_at_2489_10587 = NOVALUE;
    int _pretty_sprint_inlined_pretty_sprint_at_2548_10595 = NOVALUE;
    int _options_inlined_pretty_sprint_at_2545_10594 = NOVALUE;
    int _x_inlined_pretty_sprint_at_2542_10593 = NOVALUE;
    int _msg_inlined_crash_at_2696_10617 = NOVALUE;
    int _dpos_10679 = NOVALUE;
    int _dist_10680 = NOVALUE;
    int _bracketed_10681 = NOVALUE;
    int _6055 = NOVALUE;
    int _6054 = NOVALUE;
    int _6053 = NOVALUE;
    int _6051 = NOVALUE;
    int _6050 = NOVALUE;
    int _6049 = NOVALUE;
    int _6046 = NOVALUE;
    int _6045 = NOVALUE;
    int _6042 = NOVALUE;
    int _6040 = NOVALUE;
    int _6037 = NOVALUE;
    int _6036 = NOVALUE;
    int _6035 = NOVALUE;
    int _6032 = NOVALUE;
    int _6029 = NOVALUE;
    int _6028 = NOVALUE;
    int _6027 = NOVALUE;
    int _6026 = NOVALUE;
    int _6023 = NOVALUE;
    int _6022 = NOVALUE;
    int _6021 = NOVALUE;
    int _6018 = NOVALUE;
    int _6016 = NOVALUE;
    int _6013 = NOVALUE;
    int _6012 = NOVALUE;
    int _6011 = NOVALUE;
    int _6010 = NOVALUE;
    int _6007 = NOVALUE;
    int _6002 = NOVALUE;
    int _6001 = NOVALUE;
    int _6000 = NOVALUE;
    int _5999 = NOVALUE;
    int _5997 = NOVALUE;
    int _5996 = NOVALUE;
    int _5995 = NOVALUE;
    int _5994 = NOVALUE;
    int _5993 = NOVALUE;
    int _5992 = NOVALUE;
    int _5991 = NOVALUE;
    int _5986 = NOVALUE;
    int _5982 = NOVALUE;
    int _5981 = NOVALUE;
    int _5979 = NOVALUE;
    int _5977 = NOVALUE;
    int _5976 = NOVALUE;
    int _5975 = NOVALUE;
    int _5974 = NOVALUE;
    int _5973 = NOVALUE;
    int _5970 = NOVALUE;
    int _5968 = NOVALUE;
    int _5967 = NOVALUE;
    int _5966 = NOVALUE;
    int _5965 = NOVALUE;
    int _5962 = NOVALUE;
    int _5961 = NOVALUE;
    int _5959 = NOVALUE;
    int _5958 = NOVALUE;
    int _5957 = NOVALUE;
    int _5956 = NOVALUE;
    int _5955 = NOVALUE;
    int _5952 = NOVALUE;
    int _5951 = NOVALUE;
    int _5950 = NOVALUE;
    int _5947 = NOVALUE;
    int _5946 = NOVALUE;
    int _5944 = NOVALUE;
    int _5940 = NOVALUE;
    int _5939 = NOVALUE;
    int _5937 = NOVALUE;
    int _5936 = NOVALUE;
    int _5928 = NOVALUE;
    int _5924 = NOVALUE;
    int _5922 = NOVALUE;
    int _5921 = NOVALUE;
    int _5920 = NOVALUE;
    int _5917 = NOVALUE;
    int _5915 = NOVALUE;
    int _5914 = NOVALUE;
    int _5913 = NOVALUE;
    int _5911 = NOVALUE;
    int _5910 = NOVALUE;
    int _5909 = NOVALUE;
    int _5908 = NOVALUE;
    int _5905 = NOVALUE;
    int _5904 = NOVALUE;
    int _5903 = NOVALUE;
    int _5901 = NOVALUE;
    int _5900 = NOVALUE;
    int _5899 = NOVALUE;
    int _5898 = NOVALUE;
    int _5896 = NOVALUE;
    int _5894 = NOVALUE;
    int _5892 = NOVALUE;
    int _5890 = NOVALUE;
    int _5888 = NOVALUE;
    int _5886 = NOVALUE;
    int _5885 = NOVALUE;
    int _5884 = NOVALUE;
    int _5883 = NOVALUE;
    int _5882 = NOVALUE;
    int _5881 = NOVALUE;
    int _5879 = NOVALUE;
    int _5878 = NOVALUE;
    int _5876 = NOVALUE;
    int _5875 = NOVALUE;
    int _5873 = NOVALUE;
    int _5871 = NOVALUE;
    int _5870 = NOVALUE;
    int _5867 = NOVALUE;
    int _5865 = NOVALUE;
    int _5861 = NOVALUE;
    int _5859 = NOVALUE;
    int _5858 = NOVALUE;
    int _5857 = NOVALUE;
    int _5855 = NOVALUE;
    int _5854 = NOVALUE;
    int _5853 = NOVALUE;
    int _5852 = NOVALUE;
    int _5851 = NOVALUE;
    int _5849 = NOVALUE;
    int _5847 = NOVALUE;
    int _5846 = NOVALUE;
    int _5845 = NOVALUE;
    int _5844 = NOVALUE;
    int _5840 = NOVALUE;
    int _5837 = NOVALUE;
    int _5836 = NOVALUE;
    int _5833 = NOVALUE;
    int _5832 = NOVALUE;
    int _5831 = NOVALUE;
    int _5829 = NOVALUE;
    int _5828 = NOVALUE;
    int _5827 = NOVALUE;
    int _5826 = NOVALUE;
    int _5824 = NOVALUE;
    int _5822 = NOVALUE;
    int _5821 = NOVALUE;
    int _5820 = NOVALUE;
    int _5819 = NOVALUE;
    int _5818 = NOVALUE;
    int _5817 = NOVALUE;
    int _5815 = NOVALUE;
    int _5814 = NOVALUE;
    int _5813 = NOVALUE;
    int _5811 = NOVALUE;
    int _5810 = NOVALUE;
    int _5809 = NOVALUE;
    int _5808 = NOVALUE;
    int _5806 = NOVALUE;
    int _5803 = NOVALUE;
    int _5802 = NOVALUE;
    int _5800 = NOVALUE;
    int _5799 = NOVALUE;
    int _5797 = NOVALUE;
    int _5794 = NOVALUE;
    int _5793 = NOVALUE;
    int _5790 = NOVALUE;
    int _5788 = NOVALUE;
    int _5784 = NOVALUE;
    int _5782 = NOVALUE;
    int _5781 = NOVALUE;
    int _5780 = NOVALUE;
    int _5778 = NOVALUE;
    int _5776 = NOVALUE;
    int _5775 = NOVALUE;
    int _5774 = NOVALUE;
    int _5773 = NOVALUE;
    int _5772 = NOVALUE;
    int _5770 = NOVALUE;
    int _5768 = NOVALUE;
    int _5767 = NOVALUE;
    int _5766 = NOVALUE;
    int _5765 = NOVALUE;
    int _5763 = NOVALUE;
    int _5760 = NOVALUE;
    int _5758 = NOVALUE;
    int _5757 = NOVALUE;
    int _5756 = NOVALUE;
    int _5755 = NOVALUE;
    int _5754 = NOVALUE;
    int _5752 = NOVALUE;
    int _5751 = NOVALUE;
    int _5750 = NOVALUE;
    int _5748 = NOVALUE;
    int _5747 = NOVALUE;
    int _5746 = NOVALUE;
    int _5745 = NOVALUE;
    int _5743 = NOVALUE;
    int _5742 = NOVALUE;
    int _5741 = NOVALUE;
    int _5738 = NOVALUE;
    int _5737 = NOVALUE;
    int _5736 = NOVALUE;
    int _5735 = NOVALUE;
    int _5733 = NOVALUE;
    int _5732 = NOVALUE;
    int _5731 = NOVALUE;
    int _5730 = NOVALUE;
    int _5729 = NOVALUE;
    int _5726 = NOVALUE;
    int _5725 = NOVALUE;
    int _5724 = NOVALUE;
    int _5723 = NOVALUE;
    int _5721 = NOVALUE;
    int _5720 = NOVALUE;
    int _5719 = NOVALUE;
    int _5717 = NOVALUE;
    int _5716 = NOVALUE;
    int _5715 = NOVALUE;
    int _5713 = NOVALUE;
    int _5706 = NOVALUE;
    int _5704 = NOVALUE;
    int _5703 = NOVALUE;
    int _5696 = NOVALUE;
    int _5693 = NOVALUE;
    int _5689 = NOVALUE;
    int _5687 = NOVALUE;
    int _5686 = NOVALUE;
    int _5683 = NOVALUE;
    int _5681 = NOVALUE;
    int _5679 = NOVALUE;
    int _5676 = NOVALUE;
    int _5674 = NOVALUE;
    int _5673 = NOVALUE;
    int _5672 = NOVALUE;
    int _5671 = NOVALUE;
    int _5670 = NOVALUE;
    int _5667 = NOVALUE;
    int _5665 = NOVALUE;
    int _5664 = NOVALUE;
    int _5663 = NOVALUE;
    int _5660 = NOVALUE;
    int _5658 = NOVALUE;
    int _5656 = NOVALUE;
    int _5653 = NOVALUE;
    int _5652 = NOVALUE;
    int _5645 = NOVALUE;
    int _5642 = NOVALUE;
    int _5641 = NOVALUE;
    int _5634 = NOVALUE;
    int _5630 = NOVALUE;
    int _5627 = NOVALUE;
    int _5617 = NOVALUE;
    int _5615 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(arg_list) then*/
    _5615 = IS_ATOM(_arg_list_10113);
    if (_5615 == 0)
    {
        _5615 = NOVALUE;
        goto L1; // [8] 18
    }
    else{
        _5615 = NOVALUE;
    }

    /** 		arg_list = {arg_list}*/
    _0 = _arg_list_10113;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_arg_list_10113);
    *((int *)(_2+4)) = _arg_list_10113;
    _arg_list_10113 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	result = ""*/
    RefDS(_5);
    DeRef(_result_10114);
    _result_10114 = _5;

    /** 	in_token = 0*/
    _in_token_10115 = 0;

    /** 	i = 0*/
    _i_10117 = 0;

    /** 	tend = 0*/
    _tend_10118 = 0;

    /** 	argl = 0*/
    _argl_10131 = 0;

    /** 	spacer = 0*/
    _spacer_10125 = 0;

    /** 	prevargv = 0*/
    DeRef(_prevargv_10137);
    _prevargv_10137 = 0;

    /**     while i < length(format_pattern) do*/
L2: 
    if (IS_SEQUENCE(_format_pattern_10112)){
            _5617 = SEQ_PTR(_format_pattern_10112)->length;
    }
    else {
        _5617 = 1;
    }
    if (_i_10117 >= _5617)
    goto L3; // [63] 3585

    /**     	i += 1*/
    _i_10117 = _i_10117 + 1;

    /**     	tch = format_pattern[i]*/
    _2 = (int)SEQ_PTR(_format_pattern_10112);
    _tch_10116 = (int)*(((s1_ptr)_2)->base + _i_10117);
    if (!IS_ATOM_INT(_tch_10116))
    _tch_10116 = (long)DBL_PTR(_tch_10116)->dbl;

    /**     	if not in_token then*/
    if (_in_token_10115 != 0)
    goto L4; // [81] 210

    /**     		if tch = '[' then*/
    if (_tch_10116 != 91)
    goto L5; // [86] 200

    /**     			in_token = 1*/
    _in_token_10115 = 1;

    /**     			tend = 0*/
    _tend_10118 = 0;

    /** 				cap = 0*/
    _cap_10119 = 0;

    /** 				align = 0*/
    _align_10120 = 0;

    /** 				psign = 0*/
    _psign_10121 = 0;

    /** 				msign = 0*/
    _msign_10122 = 0;

    /** 				zfill = 0*/
    _zfill_10123 = 0;

    /** 				bwz = 0*/
    _bwz_10124 = 0;

    /** 				spacer = 0*/
    _spacer_10125 = 0;

    /** 				alt = 0*/
    _alt_10126 = 0;

    /**     			width = 0*/
    _width_10127 = 0;

    /**     			decs = -1*/
    _decs_10128 = -1;

    /**     			argn = 0*/
    _argn_10130 = 0;

    /**     			hexout = 0*/
    _hexout_10133 = 0;

    /**     			binout = 0*/
    _binout_10134 = 0;

    /**     			trimming = 0*/
    _trimming_10132 = 0;

    /**     			tsep = 0*/
    _tsep_10135 = 0;

    /**     			istext = 0*/
    _istext_10136 = 0;

    /**     			idname = ""*/
    RefDS(_5);
    DeRef(_idname_10139);
    _idname_10139 = _5;

    /**     			envvar = ""*/
    RefDS(_5);
    DeRefi(_envvar_10141);
    _envvar_10141 = _5;

    /**     			envsym = ""*/
    RefDS(_5);
    DeRef(_envsym_10140);
    _envsym_10140 = _5;
    goto L2; // [197] 60
L5: 

    /**     			result &= tch*/
    Append(&_result_10114, _result_10114, _tch_10116);
    goto L2; // [207] 60
L4: 

    /** 			switch tch do*/
    _0 = _tch_10116;
    switch ( _0 ){ 

        /**     			case ']' then*/
        case 93:

        /**     				in_token = 0*/
        _in_token_10115 = 0;

        /**     				tend = i*/
        _tend_10118 = _i_10117;
        goto L6; // [231] 1072

        /**     			case '[' then*/
        case 91:

        /** 	    			result &= tch*/
        Append(&_result_10114, _result_10114, _tch_10116);

        /** 	    			while i < length(format_pattern) do*/
L7: 
        if (IS_SEQUENCE(_format_pattern_10112)){
                _5627 = SEQ_PTR(_format_pattern_10112)->length;
        }
        else {
            _5627 = 1;
        }
        if (_i_10117 >= _5627)
        goto L6; // [251] 1072

        /** 	    				i += 1*/
        _i_10117 = _i_10117 + 1;

        /** 	    				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_10112);
        _5630 = (int)*(((s1_ptr)_2)->base + _i_10117);
        if (binary_op_a(NOTEQ, _5630, 93)){
            _5630 = NOVALUE;
            goto L7; // [267] 248
        }
        _5630 = NOVALUE;

        /** 	    					in_token = 0*/
        _in_token_10115 = 0;

        /** 	    					tend = 0*/
        _tend_10118 = 0;

        /** 	    					exit*/
        goto L6; // [283] 1072

        /** 	    			end while*/
        goto L7; // [288] 248
        goto L6; // [291] 1072

        /** 	    		case 'w', 'u', 'l' then*/
        case 119:
        case 117:
        case 108:

        /** 	    			cap = tch*/
        _cap_10119 = _tch_10116;
        goto L6; // [306] 1072

        /** 	    		case 'b' then*/
        case 98:

        /** 	    			bwz = 1*/
        _bwz_10124 = 1;
        goto L6; // [317] 1072

        /** 	    		case 's' then*/
        case 115:

        /** 	    			spacer = 1*/
        _spacer_10125 = 1;
        goto L6; // [328] 1072

        /** 	    		case 't' then*/
        case 116:

        /** 	    			trimming = 1*/
        _trimming_10132 = 1;
        goto L6; // [339] 1072

        /** 	    		case 'z' then*/
        case 122:

        /** 	    			zfill = 1*/
        _zfill_10123 = 1;
        goto L6; // [350] 1072

        /** 	    		case 'X' then*/
        case 88:

        /** 	    			hexout = 1*/
        _hexout_10133 = 1;
        goto L6; // [361] 1072

        /** 	    		case 'B' then*/
        case 66:

        /** 	    			binout = 1*/
        _binout_10134 = 1;
        goto L6; // [372] 1072

        /** 	    		case 'c', '<', '>' then*/
        case 99:
        case 60:
        case 62:

        /** 	    			align = tch*/
        _align_10120 = _tch_10116;
        goto L6; // [387] 1072

        /** 	    		case '+' then*/
        case 43:

        /** 	    			psign = 1*/
        _psign_10121 = 1;
        goto L6; // [398] 1072

        /** 	    		case '(' then*/
        case 40:

        /** 	    			msign = 1*/
        _msign_10122 = 1;
        goto L6; // [409] 1072

        /** 	    		case '?' then*/
        case 63:

        /** 	    			alt = 1*/
        _alt_10126 = 1;
        goto L6; // [420] 1072

        /** 	    		case 'T' then*/
        case 84:

        /** 	    			istext = 1*/
        _istext_10136 = 1;
        goto L6; // [431] 1072

        /** 	    		case ':' then*/
        case 58:

        /** 	    			while i < length(format_pattern) do*/
L8: 
        if (IS_SEQUENCE(_format_pattern_10112)){
                _5634 = SEQ_PTR(_format_pattern_10112)->length;
        }
        else {
            _5634 = 1;
        }
        if (_i_10117 >= _5634)
        goto L6; // [445] 1072

        /** 	    				i += 1*/
        _i_10117 = _i_10117 + 1;

        /** 	    				tch = format_pattern[i]*/
        _2 = (int)SEQ_PTR(_format_pattern_10112);
        _tch_10116 = (int)*(((s1_ptr)_2)->base + _i_10117);
        if (!IS_ATOM_INT(_tch_10116))
        _tch_10116 = (long)DBL_PTR(_tch_10116)->dbl;

        /** 	    				pos = find(tch, "0123456789")*/
        _pos_10129 = find_from(_tch_10116, _1211, 1);

        /** 	    				if pos = 0 then*/
        if (_pos_10129 != 0)
        goto L9; // [470] 485

        /** 	    					i -= 1*/
        _i_10117 = _i_10117 - 1;

        /** 	    					exit*/
        goto L6; // [482] 1072
L9: 

        /** 	    				width = width * 10 + pos - 1*/
        if (_width_10127 == (short)_width_10127)
        _5641 = _width_10127 * 10;
        else
        _5641 = NewDouble(_width_10127 * (double)10);
        if (IS_ATOM_INT(_5641)) {
            _5642 = _5641 + _pos_10129;
            if ((long)((unsigned long)_5642 + (unsigned long)HIGH_BITS) >= 0) 
            _5642 = NewDouble((double)_5642);
        }
        else {
            _5642 = NewDouble(DBL_PTR(_5641)->dbl + (double)_pos_10129);
        }
        DeRef(_5641);
        _5641 = NOVALUE;
        if (IS_ATOM_INT(_5642)) {
            _width_10127 = _5642 - 1;
        }
        else {
            _width_10127 = NewDouble(DBL_PTR(_5642)->dbl - (double)1);
        }
        DeRef(_5642);
        _5642 = NOVALUE;
        if (!IS_ATOM_INT(_width_10127)) {
            _1 = (long)(DBL_PTR(_width_10127)->dbl);
            if (UNIQUE(DBL_PTR(_width_10127)) && (DBL_PTR(_width_10127)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_width_10127);
            _width_10127 = _1;
        }

        /** 	    				if width = 0 then*/
        if (_width_10127 != 0)
        goto L8; // [505] 442

        /** 	    					zfill = '0'*/
        _zfill_10123 = 48;

        /** 	    			end while*/
        goto L8; // [517] 442
        goto L6; // [520] 1072

        /** 	    		case '.' then*/
        case 46:

        /** 	    			decs = 0*/
        _decs_10128 = 0;

        /** 	    			while i < length(format_pattern) do*/
LA: 
        if (IS_SEQUENCE(_format_pattern_10112)){
                _5645 = SEQ_PTR(_format_pattern_10112)->length;
        }
        else {
            _5645 = 1;
        }
        if (_i_10117 >= _5645)
        goto L6; // [539] 1072

        /** 	    				i += 1*/
        _i_10117 = _i_10117 + 1;

        /** 	    				tch = format_pattern[i]*/
        _2 = (int)SEQ_PTR(_format_pattern_10112);
        _tch_10116 = (int)*(((s1_ptr)_2)->base + _i_10117);
        if (!IS_ATOM_INT(_tch_10116))
        _tch_10116 = (long)DBL_PTR(_tch_10116)->dbl;

        /** 	    				pos = find(tch, "0123456789")*/
        _pos_10129 = find_from(_tch_10116, _1211, 1);

        /** 	    				if pos = 0 then*/
        if (_pos_10129 != 0)
        goto LB; // [564] 579

        /** 	    					i -= 1*/
        _i_10117 = _i_10117 - 1;

        /** 	    					exit*/
        goto L6; // [576] 1072
LB: 

        /** 	    				decs = decs * 10 + pos - 1*/
        if (_decs_10128 == (short)_decs_10128)
        _5652 = _decs_10128 * 10;
        else
        _5652 = NewDouble(_decs_10128 * (double)10);
        if (IS_ATOM_INT(_5652)) {
            _5653 = _5652 + _pos_10129;
            if ((long)((unsigned long)_5653 + (unsigned long)HIGH_BITS) >= 0) 
            _5653 = NewDouble((double)_5653);
        }
        else {
            _5653 = NewDouble(DBL_PTR(_5652)->dbl + (double)_pos_10129);
        }
        DeRef(_5652);
        _5652 = NOVALUE;
        if (IS_ATOM_INT(_5653)) {
            _decs_10128 = _5653 - 1;
        }
        else {
            _decs_10128 = NewDouble(DBL_PTR(_5653)->dbl - (double)1);
        }
        DeRef(_5653);
        _5653 = NOVALUE;
        if (!IS_ATOM_INT(_decs_10128)) {
            _1 = (long)(DBL_PTR(_decs_10128)->dbl);
            if (UNIQUE(DBL_PTR(_decs_10128)) && (DBL_PTR(_decs_10128)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_decs_10128);
            _decs_10128 = _1;
        }

        /** 	    			end while*/
        goto LA; // [597] 536
        goto L6; // [600] 1072

        /** 	    		case '{' then*/
        case 123:

        /** 	    			integer sp*/

        /** 	    			sp = i + 1*/
        _sp_10215 = _i_10117 + 1;

        /** 	    			i = sp*/
        _i_10117 = _sp_10215;

        /** 	    			while i < length(format_pattern) do*/
LC: 
        if (IS_SEQUENCE(_format_pattern_10112)){
                _5656 = SEQ_PTR(_format_pattern_10112)->length;
        }
        else {
            _5656 = 1;
        }
        if (_i_10117 >= _5656)
        goto LD; // [627] 672

        /** 	    				if format_pattern[i] = '}' then*/
        _2 = (int)SEQ_PTR(_format_pattern_10112);
        _5658 = (int)*(((s1_ptr)_2)->base + _i_10117);
        if (binary_op_a(NOTEQ, _5658, 125)){
            _5658 = NOVALUE;
            goto LE; // [637] 646
        }
        _5658 = NOVALUE;

        /** 	    					exit*/
        goto LD; // [643] 672
LE: 

        /** 	    				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_10112);
        _5660 = (int)*(((s1_ptr)_2)->base + _i_10117);
        if (binary_op_a(NOTEQ, _5660, 93)){
            _5660 = NOVALUE;
            goto LF; // [652] 661
        }
        _5660 = NOVALUE;

        /** 	    					exit*/
        goto LD; // [658] 672
LF: 

        /** 	    				i += 1*/
        _i_10117 = _i_10117 + 1;

        /** 	    			end while*/
        goto LC; // [669] 624
LD: 

        /** 	    			idname = trim(format_pattern[sp .. i-1]) & '='*/
        _5663 = _i_10117 - 1;
        rhs_slice_target = (object_ptr)&_5664;
        RHS_Slice(_format_pattern_10112, _sp_10215, _5663);
        RefDS(_4443);
        _5665 = _4trim(_5664, _4443, 0);
        _5664 = NOVALUE;
        if (IS_SEQUENCE(_5665) && IS_ATOM(61)) {
            Append(&_idname_10139, _5665, 61);
        }
        else if (IS_ATOM(_5665) && IS_SEQUENCE(61)) {
        }
        else {
            Concat((object_ptr)&_idname_10139, _5665, 61);
            DeRef(_5665);
            _5665 = NOVALUE;
        }
        DeRef(_5665);
        _5665 = NOVALUE;

        /**     				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_10112);
        _5667 = (int)*(((s1_ptr)_2)->base + _i_10117);
        if (binary_op_a(NOTEQ, _5667, 93)){
            _5667 = NOVALUE;
            goto L10; // [699] 710
        }
        _5667 = NOVALUE;

        /**     					i -= 1*/
        _i_10117 = _i_10117 - 1;
L10: 

        /**     				for j = 1 to length(arg_list) do*/
        if (IS_SEQUENCE(_arg_list_10113)){
                _5670 = SEQ_PTR(_arg_list_10113)->length;
        }
        else {
            _5670 = 1;
        }
        {
            int _j_10236;
            _j_10236 = 1;
L11: 
            if (_j_10236 > _5670){
                goto L12; // [715] 797
            }

            /**     					if sequence(arg_list[j]) then*/
            _2 = (int)SEQ_PTR(_arg_list_10113);
            _5671 = (int)*(((s1_ptr)_2)->base + _j_10236);
            _5672 = IS_SEQUENCE(_5671);
            _5671 = NOVALUE;
            if (_5672 == 0)
            {
                _5672 = NOVALUE;
                goto L13; // [731] 768
            }
            else{
                _5672 = NOVALUE;
            }

            /**     						if search:begins(idname, arg_list[j]) then*/
            _2 = (int)SEQ_PTR(_arg_list_10113);
            _5673 = (int)*(((s1_ptr)_2)->base + _j_10236);
            RefDS(_idname_10139);
            Ref(_5673);
            _5674 = _7begins(_idname_10139, _5673);
            _5673 = NOVALUE;
            if (_5674 == 0) {
                DeRef(_5674);
                _5674 = NOVALUE;
                goto L14; // [745] 767
            }
            else {
                if (!IS_ATOM_INT(_5674) && DBL_PTR(_5674)->dbl == 0.0){
                    DeRef(_5674);
                    _5674 = NOVALUE;
                    goto L14; // [745] 767
                }
                DeRef(_5674);
                _5674 = NOVALUE;
            }
            DeRef(_5674);
            _5674 = NOVALUE;

            /**     							if argn = 0 then*/
            if (_argn_10130 != 0)
            goto L15; // [752] 766

            /**     								argn = j*/
            _argn_10130 = _j_10236;

            /**     								exit*/
            goto L12; // [763] 797
L15: 
L14: 
L13: 

            /**     					if j = length(arg_list) then*/
            if (IS_SEQUENCE(_arg_list_10113)){
                    _5676 = SEQ_PTR(_arg_list_10113)->length;
            }
            else {
                _5676 = 1;
            }
            if (_j_10236 != _5676)
            goto L16; // [773] 790

            /**     						idname = ""*/
            RefDS(_5);
            DeRef(_idname_10139);
            _idname_10139 = _5;

            /**     						argn = -1*/
            _argn_10130 = -1;
L16: 

            /**     				end for*/
            _j_10236 = _j_10236 + 1;
            goto L11; // [792] 722
L12: 
            ;
        }
        goto L6; // [799] 1072

        /** 	    		case '%' then*/
        case 37:

        /** 	    			integer sp*/

        /** 	    			sp = i + 1*/
        _sp_10250 = _i_10117 + 1;

        /** 	    			i = sp*/
        _i_10117 = _sp_10250;

        /** 	    			while i < length(format_pattern) do*/
L17: 
        if (IS_SEQUENCE(_format_pattern_10112)){
                _5679 = SEQ_PTR(_format_pattern_10112)->length;
        }
        else {
            _5679 = 1;
        }
        if (_i_10117 >= _5679)
        goto L18; // [826] 871

        /** 	    				if format_pattern[i] = '%' then*/
        _2 = (int)SEQ_PTR(_format_pattern_10112);
        _5681 = (int)*(((s1_ptr)_2)->base + _i_10117);
        if (binary_op_a(NOTEQ, _5681, 37)){
            _5681 = NOVALUE;
            goto L19; // [836] 845
        }
        _5681 = NOVALUE;

        /** 	    					exit*/
        goto L18; // [842] 871
L19: 

        /** 	    				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_10112);
        _5683 = (int)*(((s1_ptr)_2)->base + _i_10117);
        if (binary_op_a(NOTEQ, _5683, 93)){
            _5683 = NOVALUE;
            goto L1A; // [851] 860
        }
        _5683 = NOVALUE;

        /** 	    					exit*/
        goto L18; // [857] 871
L1A: 

        /** 	    				i += 1*/
        _i_10117 = _i_10117 + 1;

        /** 	    			end while*/
        goto L17; // [868] 823
L18: 

        /** 	    			envsym = trim(format_pattern[sp .. i-1])*/
        _5686 = _i_10117 - 1;
        rhs_slice_target = (object_ptr)&_5687;
        RHS_Slice(_format_pattern_10112, _sp_10250, _5686);
        RefDS(_4443);
        _0 = _envsym_10140;
        _envsym_10140 = _4trim(_5687, _4443, 0);
        DeRef(_0);
        _5687 = NOVALUE;

        /**     				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_10112);
        _5689 = (int)*(((s1_ptr)_2)->base + _i_10117);
        if (binary_op_a(NOTEQ, _5689, 93)){
            _5689 = NOVALUE;
            goto L1B; // [894] 905
        }
        _5689 = NOVALUE;

        /**     					i -= 1*/
        _i_10117 = _i_10117 - 1;
L1B: 

        /**     				envvar = getenv(envsym)*/
        DeRefi(_envvar_10141);
        _envvar_10141 = EGetEnv(_envsym_10140);

        /**     				argn = -1*/
        _argn_10130 = -1;

        /**     				if atom(envvar) then*/
        _5693 = IS_ATOM(_envvar_10141);
        if (_5693 == 0)
        {
            _5693 = NOVALUE;
            goto L1C; // [920] 929
        }
        else{
            _5693 = NOVALUE;
        }

        /**     					envvar = ""*/
        RefDS(_5);
        DeRefi(_envvar_10141);
        _envvar_10141 = _5;
L1C: 
        goto L6; // [931] 1072

        /** 	    		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' then*/
        case 48:
        case 49:
        case 50:
        case 51:
        case 52:
        case 53:
        case 54:
        case 55:
        case 56:
        case 57:

        /** 	    			if argn = 0 then*/
        if (_argn_10130 != 0)
        goto L6; // [957] 1072

        /** 		    			i -= 1*/
        _i_10117 = _i_10117 - 1;

        /** 		    			while i < length(format_pattern) do*/
L1D: 
        if (IS_SEQUENCE(_format_pattern_10112)){
                _5696 = SEQ_PTR(_format_pattern_10112)->length;
        }
        else {
            _5696 = 1;
        }
        if (_i_10117 >= _5696)
        goto L6; // [975] 1072

        /** 		    				i += 1*/
        _i_10117 = _i_10117 + 1;

        /** 		    				tch = format_pattern[i]*/
        _2 = (int)SEQ_PTR(_format_pattern_10112);
        _tch_10116 = (int)*(((s1_ptr)_2)->base + _i_10117);
        if (!IS_ATOM_INT(_tch_10116))
        _tch_10116 = (long)DBL_PTR(_tch_10116)->dbl;

        /** 		    				pos = find(tch, "0123456789")*/
        _pos_10129 = find_from(_tch_10116, _1211, 1);

        /** 		    				if pos = 0 then*/
        if (_pos_10129 != 0)
        goto L1E; // [1000] 1015

        /** 		    					i -= 1*/
        _i_10117 = _i_10117 - 1;

        /** 		    					exit*/
        goto L6; // [1012] 1072
L1E: 

        /** 		    				argn = argn * 10 + pos - 1*/
        if (_argn_10130 == (short)_argn_10130)
        _5703 = _argn_10130 * 10;
        else
        _5703 = NewDouble(_argn_10130 * (double)10);
        if (IS_ATOM_INT(_5703)) {
            _5704 = _5703 + _pos_10129;
            if ((long)((unsigned long)_5704 + (unsigned long)HIGH_BITS) >= 0) 
            _5704 = NewDouble((double)_5704);
        }
        else {
            _5704 = NewDouble(DBL_PTR(_5703)->dbl + (double)_pos_10129);
        }
        DeRef(_5703);
        _5703 = NOVALUE;
        if (IS_ATOM_INT(_5704)) {
            _argn_10130 = _5704 - 1;
        }
        else {
            _argn_10130 = NewDouble(DBL_PTR(_5704)->dbl - (double)1);
        }
        DeRef(_5704);
        _5704 = NOVALUE;
        if (!IS_ATOM_INT(_argn_10130)) {
            _1 = (long)(DBL_PTR(_argn_10130)->dbl);
            if (UNIQUE(DBL_PTR(_argn_10130)) && (DBL_PTR(_argn_10130)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_argn_10130);
            _argn_10130 = _1;
        }

        /** 		    			end while*/
        goto L1D; // [1033] 972
        goto L6; // [1037] 1072

        /** 	    		case ',' then*/
        case 44:

        /** 	    			if i < length(format_pattern) then*/
        if (IS_SEQUENCE(_format_pattern_10112)){
                _5706 = SEQ_PTR(_format_pattern_10112)->length;
        }
        else {
            _5706 = 1;
        }
        if (_i_10117 >= _5706)
        goto L6; // [1048] 1072

        /** 	    				i +=1*/
        _i_10117 = _i_10117 + 1;

        /** 	    				tsep = format_pattern[i]*/
        _2 = (int)SEQ_PTR(_format_pattern_10112);
        _tsep_10135 = (int)*(((s1_ptr)_2)->base + _i_10117);
        if (!IS_ATOM_INT(_tsep_10135))
        _tsep_10135 = (long)DBL_PTR(_tsep_10135)->dbl;
        goto L6; // [1065] 1072

        /** 	    		case else*/
        default:
    ;}L6: 

    /**     		if tend > 0 then*/
    if (_tend_10118 <= 0)
    goto L1F; // [1074] 3577

    /**     			sequence argtext = ""*/
    RefDS(_5);
    DeRef(_argtext_10297);
    _argtext_10297 = _5;

    /**     			if argn = 0 then*/
    if (_argn_10130 != 0)
    goto L20; // [1089] 1100

    /**     				argn = argl + 1*/
    _argn_10130 = _argl_10131 + 1;
L20: 

    /**     			argl = argn*/
    _argl_10131 = _argn_10130;

    /**     			if argn < 1 or argn > length(arg_list) then*/
    _5713 = (_argn_10130 < 1);
    if (_5713 != 0) {
        goto L21; // [1111] 1127
    }
    if (IS_SEQUENCE(_arg_list_10113)){
            _5715 = SEQ_PTR(_arg_list_10113)->length;
    }
    else {
        _5715 = 1;
    }
    _5716 = (_argn_10130 > _5715);
    _5715 = NOVALUE;
    if (_5716 == 0)
    {
        DeRef(_5716);
        _5716 = NOVALUE;
        goto L22; // [1123] 1169
    }
    else{
        DeRef(_5716);
        _5716 = NOVALUE;
    }
L21: 

    /**     				if length(envvar) > 0 then*/
    if (IS_SEQUENCE(_envvar_10141)){
            _5717 = SEQ_PTR(_envvar_10141)->length;
    }
    else {
        _5717 = 1;
    }
    if (_5717 <= 0)
    goto L23; // [1134] 1153

    /**     					argtext = envvar*/
    Ref(_envvar_10141);
    DeRef(_argtext_10297);
    _argtext_10297 = _envvar_10141;

    /** 	    				currargv = envvar*/
    Ref(_envvar_10141);
    DeRef(_currargv_10138);
    _currargv_10138 = _envvar_10141;
    goto L24; // [1150] 2618
L23: 

    /**     					argtext = ""*/
    RefDS(_5);
    DeRef(_argtext_10297);
    _argtext_10297 = _5;

    /** 	    				currargv =""*/
    RefDS(_5);
    DeRef(_currargv_10138);
    _currargv_10138 = _5;
    goto L24; // [1166] 2618
L22: 

    /** 					if string(arg_list[argn]) then*/
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5719 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    Ref(_5719);
    _5720 = _5string(_5719);
    _5719 = NOVALUE;
    if (_5720 == 0) {
        DeRef(_5720);
        _5720 = NOVALUE;
        goto L25; // [1179] 1229
    }
    else {
        if (!IS_ATOM_INT(_5720) && DBL_PTR(_5720)->dbl == 0.0){
            DeRef(_5720);
            _5720 = NOVALUE;
            goto L25; // [1179] 1229
        }
        DeRef(_5720);
        _5720 = NOVALUE;
    }
    DeRef(_5720);
    _5720 = NOVALUE;

    /** 						if length(idname) > 0 then*/
    if (IS_SEQUENCE(_idname_10139)){
            _5721 = SEQ_PTR(_idname_10139)->length;
    }
    else {
        _5721 = 1;
    }
    if (_5721 <= 0)
    goto L26; // [1189] 1217

    /** 							argtext = arg_list[argn][length(idname) + 1 .. $]*/
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5723 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    if (IS_SEQUENCE(_idname_10139)){
            _5724 = SEQ_PTR(_idname_10139)->length;
    }
    else {
        _5724 = 1;
    }
    _5725 = _5724 + 1;
    _5724 = NOVALUE;
    if (IS_SEQUENCE(_5723)){
            _5726 = SEQ_PTR(_5723)->length;
    }
    else {
        _5726 = 1;
    }
    rhs_slice_target = (object_ptr)&_argtext_10297;
    RHS_Slice(_5723, _5725, _5726);
    _5723 = NOVALUE;
    goto L27; // [1214] 2611
L26: 

    /** 							argtext = arg_list[argn]*/
    DeRef(_argtext_10297);
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _argtext_10297 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    Ref(_argtext_10297);
    goto L27; // [1226] 2611
L25: 

    /** 					elsif integer(arg_list[argn]) then*/
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5729 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    if (IS_ATOM_INT(_5729))
    _5730 = 1;
    else if (IS_ATOM_DBL(_5729))
    _5730 = IS_ATOM_INT(DoubleToInt(_5729));
    else
    _5730 = 0;
    _5729 = NOVALUE;
    if (_5730 == 0)
    {
        _5730 = NOVALUE;
        goto L28; // [1238] 1783
    }
    else{
        _5730 = NOVALUE;
    }

    /** 						if istext then*/
    if (_istext_10136 == 0)
    {
        goto L29; // [1245] 1269
    }
    else{
    }

    /** 							argtext = {and_bits(0xFFFF_FFFF, math:abs(arg_list[argn]))}*/
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5731 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    Ref(_5731);
    _5732 = _18abs(_5731);
    _5731 = NOVALUE;
    _5733 = binary_op(AND_BITS, _2113, _5732);
    DeRef(_5732);
    _5732 = NOVALUE;
    _0 = _argtext_10297;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5733;
    _argtext_10297 = MAKE_SEQ(_1);
    DeRef(_0);
    _5733 = NOVALUE;
    goto L27; // [1266] 2611
L29: 

    /** 						elsif bwz != 0 and arg_list[argn] = 0 then*/
    _5735 = (_bwz_10124 != 0);
    if (_5735 == 0) {
        goto L2A; // [1277] 1304
    }
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5737 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    if (IS_ATOM_INT(_5737)) {
        _5738 = (_5737 == 0);
    }
    else {
        _5738 = binary_op(EQUALS, _5737, 0);
    }
    _5737 = NOVALUE;
    if (_5738 == 0) {
        DeRef(_5738);
        _5738 = NOVALUE;
        goto L2A; // [1290] 1304
    }
    else {
        if (!IS_ATOM_INT(_5738) && DBL_PTR(_5738)->dbl == 0.0){
            DeRef(_5738);
            _5738 = NOVALUE;
            goto L2A; // [1290] 1304
        }
        DeRef(_5738);
        _5738 = NOVALUE;
    }
    DeRef(_5738);
    _5738 = NOVALUE;

    /** 							argtext = repeat(' ', width)*/
    DeRef(_argtext_10297);
    _argtext_10297 = Repeat(32, _width_10127);
    goto L27; // [1301] 2611
L2A: 

    /** 						elsif binout = 1 then*/
    if (_binout_10134 != 1)
    goto L2B; // [1308] 1447

    /** 							argtext = stdseq:reverse( convert:int_to_bits(arg_list[argn], 32)) + '0'*/
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5741 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    Ref(_5741);
    _5742 = _6int_to_bits(_5741, 32);
    _5741 = NOVALUE;
    _5743 = _21reverse(_5742, 1, 0);
    _5742 = NOVALUE;
    DeRef(_argtext_10297);
    if (IS_ATOM_INT(_5743)) {
        _argtext_10297 = _5743 + 48;
        if ((long)((unsigned long)_argtext_10297 + (unsigned long)HIGH_BITS) >= 0) 
        _argtext_10297 = NewDouble((double)_argtext_10297);
    }
    else {
        _argtext_10297 = binary_op(PLUS, _5743, 48);
    }
    DeRef(_5743);
    _5743 = NOVALUE;

    /** 							if zfill != 0 and width > 0 then*/
    _5745 = (_zfill_10123 != 0);
    if (_5745 == 0) {
        goto L2C; // [1343] 1389
    }
    _5747 = (_width_10127 > 0);
    if (_5747 == 0)
    {
        DeRef(_5747);
        _5747 = NOVALUE;
        goto L2C; // [1354] 1389
    }
    else{
        DeRef(_5747);
        _5747 = NOVALUE;
    }

    /** 								if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5748 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5748 = 1;
    }
    if (_width_10127 <= _5748)
    goto L27; // [1364] 2611

    /** 									argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5750 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5750 = 1;
    }
    _5751 = _width_10127 - _5750;
    _5750 = NOVALUE;
    _5752 = Repeat(48, _5751);
    _5751 = NOVALUE;
    Concat((object_ptr)&_argtext_10297, _5752, _argtext_10297);
    DeRefDS(_5752);
    _5752 = NOVALUE;
    DeRef(_5752);
    _5752 = NOVALUE;
    goto L27; // [1386] 2611
L2C: 

    /** 								count = 1*/
    _count_10144 = 1;

    /** 								while count < length(argtext) and argtext[count] = '0' do*/
L2D: 
    if (IS_SEQUENCE(_argtext_10297)){
            _5754 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5754 = 1;
    }
    _5755 = (_count_10144 < _5754);
    _5754 = NOVALUE;
    if (_5755 == 0) {
        goto L2E; // [1406] 1433
    }
    _2 = (int)SEQ_PTR(_argtext_10297);
    _5757 = (int)*(((s1_ptr)_2)->base + _count_10144);
    if (IS_ATOM_INT(_5757)) {
        _5758 = (_5757 == 48);
    }
    else {
        _5758 = binary_op(EQUALS, _5757, 48);
    }
    _5757 = NOVALUE;
    if (_5758 <= 0) {
        if (_5758 == 0) {
            DeRef(_5758);
            _5758 = NOVALUE;
            goto L2E; // [1419] 1433
        }
        else {
            if (!IS_ATOM_INT(_5758) && DBL_PTR(_5758)->dbl == 0.0){
                DeRef(_5758);
                _5758 = NOVALUE;
                goto L2E; // [1419] 1433
            }
            DeRef(_5758);
            _5758 = NOVALUE;
        }
    }
    DeRef(_5758);
    _5758 = NOVALUE;

    /** 									count += 1*/
    _count_10144 = _count_10144 + 1;

    /** 								end while*/
    goto L2D; // [1430] 1399
L2E: 

    /** 								argtext = argtext[count .. $]*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5760 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5760 = 1;
    }
    rhs_slice_target = (object_ptr)&_argtext_10297;
    RHS_Slice(_argtext_10297, _count_10144, _5760);
    goto L27; // [1444] 2611
L2B: 

    /** 						elsif hexout = 0 then*/
    if (_hexout_10133 != 0)
    goto L2F; // [1451] 1717

    /** 							argtext = sprintf("%d", arg_list[argn])*/
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5763 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    DeRef(_argtext_10297);
    _argtext_10297 = EPrintf(-9999999, _740, _5763);
    _5763 = NOVALUE;

    /** 							if zfill != 0 and width > 0 then*/
    _5765 = (_zfill_10123 != 0);
    if (_5765 == 0) {
        goto L30; // [1473] 1570
    }
    _5767 = (_width_10127 > 0);
    if (_5767 == 0)
    {
        DeRef(_5767);
        _5767 = NOVALUE;
        goto L30; // [1484] 1570
    }
    else{
        DeRef(_5767);
        _5767 = NOVALUE;
    }

    /** 								if argtext[1] = '-' then*/
    _2 = (int)SEQ_PTR(_argtext_10297);
    _5768 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _5768, 45)){
        _5768 = NOVALUE;
        goto L31; // [1493] 1539
    }
    _5768 = NOVALUE;

    /** 									if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5770 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5770 = 1;
    }
    if (_width_10127 <= _5770)
    goto L32; // [1504] 1569

    /** 										argtext = '-' & repeat('0', width - length(argtext)) & argtext[2..$]*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5772 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5772 = 1;
    }
    _5773 = _width_10127 - _5772;
    _5772 = NOVALUE;
    _5774 = Repeat(48, _5773);
    _5773 = NOVALUE;
    if (IS_SEQUENCE(_argtext_10297)){
            _5775 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5775 = 1;
    }
    rhs_slice_target = (object_ptr)&_5776;
    RHS_Slice(_argtext_10297, 2, _5775);
    {
        int concat_list[3];

        concat_list[0] = _5776;
        concat_list[1] = _5774;
        concat_list[2] = 45;
        Concat_N((object_ptr)&_argtext_10297, concat_list, 3);
    }
    DeRefDS(_5776);
    _5776 = NOVALUE;
    DeRefDS(_5774);
    _5774 = NOVALUE;
    goto L32; // [1536] 1569
L31: 

    /** 									if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5778 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5778 = 1;
    }
    if (_width_10127 <= _5778)
    goto L33; // [1546] 1568

    /** 										argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5780 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5780 = 1;
    }
    _5781 = _width_10127 - _5780;
    _5780 = NOVALUE;
    _5782 = Repeat(48, _5781);
    _5781 = NOVALUE;
    Concat((object_ptr)&_argtext_10297, _5782, _argtext_10297);
    DeRefDS(_5782);
    _5782 = NOVALUE;
    DeRef(_5782);
    _5782 = NOVALUE;
L33: 
L32: 
L30: 

    /** 							if arg_list[argn] > 0 then*/
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5784 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    if (binary_op_a(LESSEQ, _5784, 0)){
        _5784 = NOVALUE;
        goto L34; // [1576] 1624
    }
    _5784 = NOVALUE;

    /** 								if psign then*/
    if (_psign_10121 == 0)
    {
        goto L27; // [1584] 2611
    }
    else{
    }

    /** 									if zfill = 0 then*/
    if (_zfill_10123 != 0)
    goto L35; // [1589] 1602

    /** 										argtext = '+' & argtext*/
    Prepend(&_argtext_10297, _argtext_10297, 43);
    goto L27; // [1599] 2611
L35: 

    /** 									elsif argtext[1] = '0' then*/
    _2 = (int)SEQ_PTR(_argtext_10297);
    _5788 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _5788, 48)){
        _5788 = NOVALUE;
        goto L27; // [1608] 2611
    }
    _5788 = NOVALUE;

    /** 										argtext[1] = '+'*/
    _2 = (int)SEQ_PTR(_argtext_10297);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _argtext_10297 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 43;
    DeRef(_1);
    goto L27; // [1621] 2611
L34: 

    /** 							elsif arg_list[argn] < 0 then*/
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5790 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    if (binary_op_a(GREATEREQ, _5790, 0)){
        _5790 = NOVALUE;
        goto L27; // [1630] 2611
    }
    _5790 = NOVALUE;

    /** 								if msign then*/
    if (_msign_10122 == 0)
    {
        goto L27; // [1638] 2611
    }
    else{
    }

    /** 									if zfill = 0 then*/
    if (_zfill_10123 != 0)
    goto L36; // [1643] 1666

    /** 										argtext = '(' & argtext[2..$] & ')'*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5793 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5793 = 1;
    }
    rhs_slice_target = (object_ptr)&_5794;
    RHS_Slice(_argtext_10297, 2, _5793);
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _5794;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_10297, concat_list, 3);
    }
    DeRefDS(_5794);
    _5794 = NOVALUE;
    goto L27; // [1663] 2611
L36: 

    /** 										if argtext[2] = '0' then*/
    _2 = (int)SEQ_PTR(_argtext_10297);
    _5797 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _5797, 48)){
        _5797 = NOVALUE;
        goto L37; // [1672] 1695
    }
    _5797 = NOVALUE;

    /** 											argtext = '(' & argtext[3..$] & ')'*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5799 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5799 = 1;
    }
    rhs_slice_target = (object_ptr)&_5800;
    RHS_Slice(_argtext_10297, 3, _5799);
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _5800;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_10297, concat_list, 3);
    }
    DeRefDS(_5800);
    _5800 = NOVALUE;
    goto L27; // [1692] 2611
L37: 

    /** 											argtext = argtext[2..$] & ')'*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5802 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5802 = 1;
    }
    rhs_slice_target = (object_ptr)&_5803;
    RHS_Slice(_argtext_10297, 2, _5802);
    Append(&_argtext_10297, _5803, 41);
    DeRefDS(_5803);
    _5803 = NOVALUE;
    goto L27; // [1714] 2611
L2F: 

    /** 							argtext = sprintf("%x", arg_list[argn])*/
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5806 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    DeRef(_argtext_10297);
    _argtext_10297 = EPrintf(-9999999, _5805, _5806);
    _5806 = NOVALUE;

    /** 							if zfill != 0 and width > 0 then*/
    _5808 = (_zfill_10123 != 0);
    if (_5808 == 0) {
        goto L27; // [1735] 2611
    }
    _5810 = (_width_10127 > 0);
    if (_5810 == 0)
    {
        DeRef(_5810);
        _5810 = NOVALUE;
        goto L27; // [1746] 2611
    }
    else{
        DeRef(_5810);
        _5810 = NOVALUE;
    }

    /** 								if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5811 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5811 = 1;
    }
    if (_width_10127 <= _5811)
    goto L27; // [1756] 2611

    /** 									argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5813 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5813 = 1;
    }
    _5814 = _width_10127 - _5813;
    _5813 = NOVALUE;
    _5815 = Repeat(48, _5814);
    _5814 = NOVALUE;
    Concat((object_ptr)&_argtext_10297, _5815, _argtext_10297);
    DeRefDS(_5815);
    _5815 = NOVALUE;
    DeRef(_5815);
    _5815 = NOVALUE;
    goto L27; // [1780] 2611
L28: 

    /** 					elsif atom(arg_list[argn]) then*/
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5817 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    _5818 = IS_ATOM(_5817);
    _5817 = NOVALUE;
    if (_5818 == 0)
    {
        _5818 = NOVALUE;
        goto L38; // [1792] 2195
    }
    else{
        _5818 = NOVALUE;
    }

    /** 						if istext then*/
    if (_istext_10136 == 0)
    {
        goto L39; // [1799] 1826
    }
    else{
    }

    /** 							argtext = {and_bits(0xFFFF_FFFF, math:abs(floor(arg_list[argn])))}*/
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5819 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    if (IS_ATOM_INT(_5819))
    _5820 = e_floor(_5819);
    else
    _5820 = unary_op(FLOOR, _5819);
    _5819 = NOVALUE;
    _5821 = _18abs(_5820);
    _5820 = NOVALUE;
    _5822 = binary_op(AND_BITS, _2113, _5821);
    DeRef(_5821);
    _5821 = NOVALUE;
    _0 = _argtext_10297;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5822;
    _argtext_10297 = MAKE_SEQ(_1);
    DeRef(_0);
    _5822 = NOVALUE;
    goto L27; // [1823] 2611
L39: 

    /** 							if hexout then*/
    if (_hexout_10133 == 0)
    {
        goto L3A; // [1830] 1898
    }
    else{
    }

    /** 								argtext = sprintf("%x", arg_list[argn])*/
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5824 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    DeRef(_argtext_10297);
    _argtext_10297 = EPrintf(-9999999, _5805, _5824);
    _5824 = NOVALUE;

    /** 								if zfill != 0 and width > 0 then*/
    _5826 = (_zfill_10123 != 0);
    if (_5826 == 0) {
        goto L27; // [1851] 2611
    }
    _5828 = (_width_10127 > 0);
    if (_5828 == 0)
    {
        DeRef(_5828);
        _5828 = NOVALUE;
        goto L27; // [1862] 2611
    }
    else{
        DeRef(_5828);
        _5828 = NOVALUE;
    }

    /** 									if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5829 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5829 = 1;
    }
    if (_width_10127 <= _5829)
    goto L27; // [1872] 2611

    /** 										argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5831 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5831 = 1;
    }
    _5832 = _width_10127 - _5831;
    _5831 = NOVALUE;
    _5833 = Repeat(48, _5832);
    _5832 = NOVALUE;
    Concat((object_ptr)&_argtext_10297, _5833, _argtext_10297);
    DeRefDS(_5833);
    _5833 = NOVALUE;
    DeRef(_5833);
    _5833 = NOVALUE;
    goto L27; // [1895] 2611
L3A: 

    /** 								argtext = trim(sprintf("%15.15g", arg_list[argn]))*/
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5836 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    _5837 = EPrintf(-9999999, _5835, _5836);
    _5836 = NOVALUE;
    RefDS(_4443);
    _0 = _argtext_10297;
    _argtext_10297 = _4trim(_5837, _4443, 0);
    DeRef(_0);
    _5837 = NOVALUE;

    /** 								while ep != 0 with entry do*/
    goto L3B; // [1918] 1941
L3C: 
    if (_ep_10142 == 0)
    goto L3D; // [1923] 1953

    /** 									argtext = remove(argtext, ep+2)*/
    _5840 = _ep_10142 + 2;
    if ((long)((unsigned long)_5840 + (unsigned long)HIGH_BITS) >= 0) 
    _5840 = NewDouble((double)_5840);
    {
        s1_ptr assign_space = SEQ_PTR(_argtext_10297);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_5840)) ? _5840 : (long)(DBL_PTR(_5840)->dbl);
        int stop = (IS_ATOM_INT(_5840)) ? _5840 : (long)(DBL_PTR(_5840)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_argtext_10297), start, &_argtext_10297 );
            }
            else Tail(SEQ_PTR(_argtext_10297), stop+1, &_argtext_10297);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_argtext_10297), start, &_argtext_10297);
        }
        else {
            assign_slice_seq = &assign_space;
            _argtext_10297 = Remove_elements(start, stop, (SEQ_PTR(_argtext_10297)->ref == 1));
        }
    }
    DeRef(_5840);
    _5840 = NOVALUE;
    _5840 = NOVALUE;

    /** 								entry*/
L3B: 

    /** 									ep = match("e+0", argtext)*/
    _ep_10142 = e_match_from(_5842, _argtext_10297, 1);

    /** 								end while*/
    goto L3C; // [1950] 1921
L3D: 

    /** 								if zfill != 0 and width > 0 then*/
    _5844 = (_zfill_10123 != 0);
    if (_5844 == 0) {
        goto L3E; // [1961] 2046
    }
    _5846 = (_width_10127 > 0);
    if (_5846 == 0)
    {
        DeRef(_5846);
        _5846 = NOVALUE;
        goto L3E; // [1972] 2046
    }
    else{
        DeRef(_5846);
        _5846 = NOVALUE;
    }

    /** 									if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5847 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5847 = 1;
    }
    if (_width_10127 <= _5847)
    goto L3F; // [1982] 2045

    /** 										if argtext[1] = '-' then*/
    _2 = (int)SEQ_PTR(_argtext_10297);
    _5849 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _5849, 45)){
        _5849 = NOVALUE;
        goto L40; // [1992] 2026
    }
    _5849 = NOVALUE;

    /** 											argtext = '-' & repeat('0', width - length(argtext)) & argtext[2..$]*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5851 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5851 = 1;
    }
    _5852 = _width_10127 - _5851;
    _5851 = NOVALUE;
    _5853 = Repeat(48, _5852);
    _5852 = NOVALUE;
    if (IS_SEQUENCE(_argtext_10297)){
            _5854 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5854 = 1;
    }
    rhs_slice_target = (object_ptr)&_5855;
    RHS_Slice(_argtext_10297, 2, _5854);
    {
        int concat_list[3];

        concat_list[0] = _5855;
        concat_list[1] = _5853;
        concat_list[2] = 45;
        Concat_N((object_ptr)&_argtext_10297, concat_list, 3);
    }
    DeRefDS(_5855);
    _5855 = NOVALUE;
    DeRefDS(_5853);
    _5853 = NOVALUE;
    goto L41; // [2023] 2044
L40: 

    /** 											argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5857 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5857 = 1;
    }
    _5858 = _width_10127 - _5857;
    _5857 = NOVALUE;
    _5859 = Repeat(48, _5858);
    _5858 = NOVALUE;
    Concat((object_ptr)&_argtext_10297, _5859, _argtext_10297);
    DeRefDS(_5859);
    _5859 = NOVALUE;
    DeRef(_5859);
    _5859 = NOVALUE;
L41: 
L3F: 
L3E: 

    /** 								if arg_list[argn] > 0 then*/
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5861 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    if (binary_op_a(LESSEQ, _5861, 0)){
        _5861 = NOVALUE;
        goto L42; // [2052] 2100
    }
    _5861 = NOVALUE;

    /** 									if psign  then*/
    if (_psign_10121 == 0)
    {
        goto L27; // [2060] 2611
    }
    else{
    }

    /** 										if zfill = 0 then*/
    if (_zfill_10123 != 0)
    goto L43; // [2065] 2078

    /** 											argtext = '+' & argtext*/
    Prepend(&_argtext_10297, _argtext_10297, 43);
    goto L27; // [2075] 2611
L43: 

    /** 										elsif argtext[1] = '0' then*/
    _2 = (int)SEQ_PTR(_argtext_10297);
    _5865 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _5865, 48)){
        _5865 = NOVALUE;
        goto L27; // [2084] 2611
    }
    _5865 = NOVALUE;

    /** 											argtext[1] = '+'*/
    _2 = (int)SEQ_PTR(_argtext_10297);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _argtext_10297 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 43;
    DeRef(_1);
    goto L27; // [2097] 2611
L42: 

    /** 								elsif arg_list[argn] < 0 then*/
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5867 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    if (binary_op_a(GREATEREQ, _5867, 0)){
        _5867 = NOVALUE;
        goto L27; // [2106] 2611
    }
    _5867 = NOVALUE;

    /** 									if msign then*/
    if (_msign_10122 == 0)
    {
        goto L27; // [2114] 2611
    }
    else{
    }

    /** 										if zfill = 0 then*/
    if (_zfill_10123 != 0)
    goto L44; // [2119] 2142

    /** 											argtext = '(' & argtext[2..$] & ')'*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5870 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5870 = 1;
    }
    rhs_slice_target = (object_ptr)&_5871;
    RHS_Slice(_argtext_10297, 2, _5870);
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _5871;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_10297, concat_list, 3);
    }
    DeRefDS(_5871);
    _5871 = NOVALUE;
    goto L27; // [2139] 2611
L44: 

    /** 											if argtext[2] = '0' then*/
    _2 = (int)SEQ_PTR(_argtext_10297);
    _5873 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _5873, 48)){
        _5873 = NOVALUE;
        goto L45; // [2148] 2171
    }
    _5873 = NOVALUE;

    /** 												argtext = '(' & argtext[3..$] & ')'*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5875 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5875 = 1;
    }
    rhs_slice_target = (object_ptr)&_5876;
    RHS_Slice(_argtext_10297, 3, _5875);
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _5876;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_10297, concat_list, 3);
    }
    DeRefDS(_5876);
    _5876 = NOVALUE;
    goto L27; // [2168] 2611
L45: 

    /** 												argtext = argtext[2..$] & ')'*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5878 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5878 = 1;
    }
    rhs_slice_target = (object_ptr)&_5879;
    RHS_Slice(_argtext_10297, 2, _5878);
    Append(&_argtext_10297, _5879, 41);
    DeRefDS(_5879);
    _5879 = NOVALUE;
    goto L27; // [2192] 2611
L38: 

    /** 						if alt != 0 and length(arg_list[argn]) = 2 then*/
    _5881 = (_alt_10126 != 0);
    if (_5881 == 0) {
        goto L46; // [2203] 2522
    }
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5883 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    if (IS_SEQUENCE(_5883)){
            _5884 = SEQ_PTR(_5883)->length;
    }
    else {
        _5884 = 1;
    }
    _5883 = NOVALUE;
    _5885 = (_5884 == 2);
    _5884 = NOVALUE;
    if (_5885 == 0)
    {
        DeRef(_5885);
        _5885 = NOVALUE;
        goto L46; // [2219] 2522
    }
    else{
        DeRef(_5885);
        _5885 = NOVALUE;
    }

    /** 							object tempv*/

    /** 							if atom(prevargv) then*/
    _5886 = IS_ATOM(_prevargv_10137);
    if (_5886 == 0)
    {
        _5886 = NOVALUE;
        goto L47; // [2229] 2265
    }
    else{
        _5886 = NOVALUE;
    }

    /** 								if prevargv != 1 then*/
    if (binary_op_a(EQUALS, _prevargv_10137, 1)){
        goto L48; // [2234] 2251
    }

    /** 									tempv = arg_list[argn][1]*/
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5888 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    DeRef(_tempv_10533);
    _2 = (int)SEQ_PTR(_5888);
    _tempv_10533 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_tempv_10533);
    _5888 = NOVALUE;
    goto L49; // [2248] 2299
L48: 

    /** 									tempv = arg_list[argn][2]*/
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5890 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    DeRef(_tempv_10533);
    _2 = (int)SEQ_PTR(_5890);
    _tempv_10533 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_tempv_10533);
    _5890 = NOVALUE;
    goto L49; // [2262] 2299
L47: 

    /** 								if length(prevargv) = 0 then*/
    if (IS_SEQUENCE(_prevargv_10137)){
            _5892 = SEQ_PTR(_prevargv_10137)->length;
    }
    else {
        _5892 = 1;
    }
    if (_5892 != 0)
    goto L4A; // [2270] 2287

    /** 									tempv = arg_list[argn][1]*/
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5894 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    DeRef(_tempv_10533);
    _2 = (int)SEQ_PTR(_5894);
    _tempv_10533 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_tempv_10533);
    _5894 = NOVALUE;
    goto L4B; // [2284] 2298
L4A: 

    /** 									tempv = arg_list[argn][2]*/
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5896 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    DeRef(_tempv_10533);
    _2 = (int)SEQ_PTR(_5896);
    _tempv_10533 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_tempv_10533);
    _5896 = NOVALUE;
L4B: 
L49: 

    /** 							if string(tempv) then*/
    Ref(_tempv_10533);
    _5898 = _5string(_tempv_10533);
    if (_5898 == 0) {
        DeRef(_5898);
        _5898 = NOVALUE;
        goto L4C; // [2307] 2320
    }
    else {
        if (!IS_ATOM_INT(_5898) && DBL_PTR(_5898)->dbl == 0.0){
            DeRef(_5898);
            _5898 = NOVALUE;
            goto L4C; // [2307] 2320
        }
        DeRef(_5898);
        _5898 = NOVALUE;
    }
    DeRef(_5898);
    _5898 = NOVALUE;

    /** 								argtext = tempv*/
    Ref(_tempv_10533);
    DeRef(_argtext_10297);
    _argtext_10297 = _tempv_10533;
    goto L4D; // [2317] 2517
L4C: 

    /** 							elsif integer(tempv) then*/
    if (IS_ATOM_INT(_tempv_10533))
    _5899 = 1;
    else if (IS_ATOM_DBL(_tempv_10533))
    _5899 = IS_ATOM_INT(DoubleToInt(_tempv_10533));
    else
    _5899 = 0;
    if (_5899 == 0)
    {
        _5899 = NOVALUE;
        goto L4E; // [2325] 2391
    }
    else{
        _5899 = NOVALUE;
    }

    /** 								if istext then*/
    if (_istext_10136 == 0)
    {
        goto L4F; // [2330] 2350
    }
    else{
    }

    /** 									argtext = {and_bits(0xFFFF_FFFF, math:abs(tempv))}*/
    Ref(_tempv_10533);
    _5900 = _18abs(_tempv_10533);
    _5901 = binary_op(AND_BITS, _2113, _5900);
    DeRef(_5900);
    _5900 = NOVALUE;
    _0 = _argtext_10297;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5901;
    _argtext_10297 = MAKE_SEQ(_1);
    DeRef(_0);
    _5901 = NOVALUE;
    goto L4D; // [2347] 2517
L4F: 

    /** 								elsif bwz != 0 and tempv = 0 then*/
    _5903 = (_bwz_10124 != 0);
    if (_5903 == 0) {
        goto L50; // [2358] 2381
    }
    if (IS_ATOM_INT(_tempv_10533)) {
        _5905 = (_tempv_10533 == 0);
    }
    else {
        _5905 = binary_op(EQUALS, _tempv_10533, 0);
    }
    if (_5905 == 0) {
        DeRef(_5905);
        _5905 = NOVALUE;
        goto L50; // [2367] 2381
    }
    else {
        if (!IS_ATOM_INT(_5905) && DBL_PTR(_5905)->dbl == 0.0){
            DeRef(_5905);
            _5905 = NOVALUE;
            goto L50; // [2367] 2381
        }
        DeRef(_5905);
        _5905 = NOVALUE;
    }
    DeRef(_5905);
    _5905 = NOVALUE;

    /** 									argtext = repeat(' ', width)*/
    DeRef(_argtext_10297);
    _argtext_10297 = Repeat(32, _width_10127);
    goto L4D; // [2378] 2517
L50: 

    /** 									argtext = sprintf("%d", tempv)*/
    DeRef(_argtext_10297);
    _argtext_10297 = EPrintf(-9999999, _740, _tempv_10533);
    goto L4D; // [2388] 2517
L4E: 

    /** 							elsif atom(tempv) then*/
    _5908 = IS_ATOM(_tempv_10533);
    if (_5908 == 0)
    {
        _5908 = NOVALUE;
        goto L51; // [2396] 2473
    }
    else{
        _5908 = NOVALUE;
    }

    /** 								if istext then*/
    if (_istext_10136 == 0)
    {
        goto L52; // [2401] 2424
    }
    else{
    }

    /** 									argtext = {and_bits(0xFFFF_FFFF, math:abs(floor(tempv)))}*/
    if (IS_ATOM_INT(_tempv_10533))
    _5909 = e_floor(_tempv_10533);
    else
    _5909 = unary_op(FLOOR, _tempv_10533);
    _5910 = _18abs(_5909);
    _5909 = NOVALUE;
    _5911 = binary_op(AND_BITS, _2113, _5910);
    DeRef(_5910);
    _5910 = NOVALUE;
    _0 = _argtext_10297;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5911;
    _argtext_10297 = MAKE_SEQ(_1);
    DeRef(_0);
    _5911 = NOVALUE;
    goto L4D; // [2421] 2517
L52: 

    /** 								elsif bwz != 0 and tempv = 0 then*/
    _5913 = (_bwz_10124 != 0);
    if (_5913 == 0) {
        goto L53; // [2432] 2455
    }
    if (IS_ATOM_INT(_tempv_10533)) {
        _5915 = (_tempv_10533 == 0);
    }
    else {
        _5915 = binary_op(EQUALS, _tempv_10533, 0);
    }
    if (_5915 == 0) {
        DeRef(_5915);
        _5915 = NOVALUE;
        goto L53; // [2441] 2455
    }
    else {
        if (!IS_ATOM_INT(_5915) && DBL_PTR(_5915)->dbl == 0.0){
            DeRef(_5915);
            _5915 = NOVALUE;
            goto L53; // [2441] 2455
        }
        DeRef(_5915);
        _5915 = NOVALUE;
    }
    DeRef(_5915);
    _5915 = NOVALUE;

    /** 									argtext = repeat(' ', width)*/
    DeRef(_argtext_10297);
    _argtext_10297 = Repeat(32, _width_10127);
    goto L4D; // [2452] 2517
L53: 

    /** 									argtext = trim(sprintf("%15.15g", tempv))*/
    _5917 = EPrintf(-9999999, _5835, _tempv_10533);
    RefDS(_4443);
    _0 = _argtext_10297;
    _argtext_10297 = _4trim(_5917, _4443, 0);
    DeRef(_0);
    _5917 = NOVALUE;
    goto L4D; // [2470] 2517
L51: 

    /** 								argtext = pretty:pretty_sprint( tempv,*/
    _1 = NewS1(10);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 2;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 1;
    *((int *)(_2+16)) = 1000;
    RefDS(_740);
    *((int *)(_2+20)) = _740;
    RefDS(_5919);
    *((int *)(_2+24)) = _5919;
    *((int *)(_2+28)) = 32;
    *((int *)(_2+32)) = 127;
    *((int *)(_2+36)) = 1;
    *((int *)(_2+40)) = 0;
    _5920 = MAKE_SEQ(_1);
    DeRef(_options_inlined_pretty_sprint_at_2489_10587);
    _options_inlined_pretty_sprint_at_2489_10587 = _5920;
    _5920 = NOVALUE;

    /** 	pretty_printing = 0*/
    _23pretty_printing_8552 = 0;

    /** 	pretty( x, options )*/
    Ref(_tempv_10533);
    RefDS(_options_inlined_pretty_sprint_at_2489_10587);
    _23pretty(_tempv_10533, _options_inlined_pretty_sprint_at_2489_10587);

    /** 	return pretty_line*/
    RefDS(_23pretty_line_8555);
    DeRef(_argtext_10297);
    _argtext_10297 = _23pretty_line_8555;
    DeRef(_options_inlined_pretty_sprint_at_2489_10587);
    _options_inlined_pretty_sprint_at_2489_10587 = NOVALUE;
L4D: 
    DeRef(_tempv_10533);
    _tempv_10533 = NOVALUE;
    goto L54; // [2519] 2598
L46: 

    /** 							argtext = pretty:pretty_sprint( arg_list[argn],*/
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _5921 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    _1 = NewS1(10);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 2;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 1;
    *((int *)(_2+16)) = 1000;
    RefDS(_740);
    *((int *)(_2+20)) = _740;
    RefDS(_5919);
    *((int *)(_2+24)) = _5919;
    *((int *)(_2+28)) = 32;
    *((int *)(_2+32)) = 127;
    *((int *)(_2+36)) = 1;
    *((int *)(_2+40)) = 0;
    _5922 = MAKE_SEQ(_1);
    Ref(_5921);
    DeRef(_x_inlined_pretty_sprint_at_2542_10593);
    _x_inlined_pretty_sprint_at_2542_10593 = _5921;
    _5921 = NOVALUE;
    DeRef(_options_inlined_pretty_sprint_at_2545_10594);
    _options_inlined_pretty_sprint_at_2545_10594 = _5922;
    _5922 = NOVALUE;

    /** 	pretty_printing = 0*/
    _23pretty_printing_8552 = 0;

    /** 	pretty( x, options )*/
    Ref(_x_inlined_pretty_sprint_at_2542_10593);
    RefDS(_options_inlined_pretty_sprint_at_2545_10594);
    _23pretty(_x_inlined_pretty_sprint_at_2542_10593, _options_inlined_pretty_sprint_at_2545_10594);

    /** 	return pretty_line*/
    RefDS(_23pretty_line_8555);
    DeRef(_argtext_10297);
    _argtext_10297 = _23pretty_line_8555;
    DeRef(_x_inlined_pretty_sprint_at_2542_10593);
    _x_inlined_pretty_sprint_at_2542_10593 = NOVALUE;
    DeRef(_options_inlined_pretty_sprint_at_2545_10594);
    _options_inlined_pretty_sprint_at_2545_10594 = NOVALUE;

    /** 						while ep != 0 with entry do*/
    goto L54; // [2575] 2598
L55: 
    if (_ep_10142 == 0)
    goto L56; // [2580] 2610

    /** 							argtext = remove(argtext, ep+2)*/
    _5924 = _ep_10142 + 2;
    if ((long)((unsigned long)_5924 + (unsigned long)HIGH_BITS) >= 0) 
    _5924 = NewDouble((double)_5924);
    {
        s1_ptr assign_space = SEQ_PTR(_argtext_10297);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_5924)) ? _5924 : (long)(DBL_PTR(_5924)->dbl);
        int stop = (IS_ATOM_INT(_5924)) ? _5924 : (long)(DBL_PTR(_5924)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_argtext_10297), start, &_argtext_10297 );
            }
            else Tail(SEQ_PTR(_argtext_10297), stop+1, &_argtext_10297);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_argtext_10297), start, &_argtext_10297);
        }
        else {
            assign_slice_seq = &assign_space;
            _argtext_10297 = Remove_elements(start, stop, (SEQ_PTR(_argtext_10297)->ref == 1));
        }
    }
    DeRef(_5924);
    _5924 = NOVALUE;
    _5924 = NOVALUE;

    /** 						entry*/
L54: 

    /** 							ep = match("e+0", argtext)*/
    _ep_10142 = e_match_from(_5842, _argtext_10297, 1);

    /** 						end while*/
    goto L55; // [2607] 2578
L56: 
L27: 

    /** 	    			currargv = arg_list[argn]*/
    DeRef(_currargv_10138);
    _2 = (int)SEQ_PTR(_arg_list_10113);
    _currargv_10138 = (int)*(((s1_ptr)_2)->base + _argn_10130);
    Ref(_currargv_10138);
L24: 

    /**     			if length(argtext) > 0 then*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5928 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5928 = 1;
    }
    if (_5928 <= 0)
    goto L57; // [2623] 3533

    /**     				switch cap do*/
    _0 = _cap_10119;
    switch ( _0 ){ 

        /**     					case 'u' then*/
        case 117:

        /**     						argtext = upper(argtext)*/
        RefDS(_argtext_10297);
        _0 = _argtext_10297;
        _argtext_10297 = _4upper(_argtext_10297);
        DeRefDS(_0);
        goto L58; // [2648] 2714

        /**     					case 'l' then*/
        case 108:

        /**     						argtext = lower(argtext)*/
        RefDS(_argtext_10297);
        _0 = _argtext_10297;
        _argtext_10297 = _4lower(_argtext_10297);
        DeRefDS(_0);
        goto L58; // [2662] 2714

        /**     					case 'w' then*/
        case 119:

        /**     						argtext = proper(argtext)*/
        RefDS(_argtext_10297);
        _0 = _argtext_10297;
        _argtext_10297 = _4proper(_argtext_10297);
        DeRefDS(_0);
        goto L58; // [2676] 2714

        /**     					case 0 then*/
        case 0:

        /** 							cap = cap*/
        _cap_10119 = _cap_10119;
        goto L58; // [2687] 2714

        /**     					case else*/
        default:

        /**     						error:crash("logic error: 'cap' mode in format.")*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_2696_10617);
        _msg_inlined_crash_at_2696_10617 = EPrintf(-9999999, _5935, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_2696_10617);

        /** end procedure*/
        goto L59; // [2708] 2711
L59: 
        DeRefi(_msg_inlined_crash_at_2696_10617);
        _msg_inlined_crash_at_2696_10617 = NOVALUE;
    ;}L58: 

    /** 					if atom(currargv) then*/
    _5936 = IS_ATOM(_currargv_10138);
    if (_5936 == 0)
    {
        _5936 = NOVALUE;
        goto L5A; // [2721] 2964
    }
    else{
        _5936 = NOVALUE;
    }

    /** 						if find('e', argtext) = 0 then*/
    _5937 = find_from(101, _argtext_10297, 1);
    if (_5937 != 0)
    goto L5B; // [2731] 2963

    /** 							pflag = 0*/
    _pflag_10143 = 0;

    /** 							if msign and currargv < 0 then*/
    if (_msign_10122 == 0) {
        goto L5C; // [2744] 2762
    }
    if (IS_ATOM_INT(_currargv_10138)) {
        _5940 = (_currargv_10138 < 0);
    }
    else {
        _5940 = binary_op(LESS, _currargv_10138, 0);
    }
    if (_5940 == 0) {
        DeRef(_5940);
        _5940 = NOVALUE;
        goto L5C; // [2753] 2762
    }
    else {
        if (!IS_ATOM_INT(_5940) && DBL_PTR(_5940)->dbl == 0.0){
            DeRef(_5940);
            _5940 = NOVALUE;
            goto L5C; // [2753] 2762
        }
        DeRef(_5940);
        _5940 = NOVALUE;
    }
    DeRef(_5940);
    _5940 = NOVALUE;

    /** 								pflag = 1*/
    _pflag_10143 = 1;
L5C: 

    /** 							if decs != -1 then*/
    if (_decs_10128 == -1)
    goto L5D; // [2766] 2962

    /** 								pos = find('.', argtext)*/
    _pos_10129 = find_from(46, _argtext_10297, 1);

    /** 								if pos then*/
    if (_pos_10129 == 0)
    {
        goto L5E; // [2779] 2907
    }
    else{
    }

    /** 									if decs = 0 then*/
    if (_decs_10128 != 0)
    goto L5F; // [2784] 2802

    /** 										argtext = argtext [1 .. pos-1 ]*/
    _5944 = _pos_10129 - 1;
    rhs_slice_target = (object_ptr)&_argtext_10297;
    RHS_Slice(_argtext_10297, 1, _5944);
    goto L60; // [2799] 2961
L5F: 

    /** 										pos = length(argtext) - pos - pflag*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5946 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5946 = 1;
    }
    _5947 = _5946 - _pos_10129;
    if ((long)((unsigned long)_5947 +(unsigned long) HIGH_BITS) >= 0){
        _5947 = NewDouble((double)_5947);
    }
    _5946 = NOVALUE;
    if (IS_ATOM_INT(_5947)) {
        _pos_10129 = _5947 - _pflag_10143;
    }
    else {
        _pos_10129 = NewDouble(DBL_PTR(_5947)->dbl - (double)_pflag_10143);
    }
    DeRef(_5947);
    _5947 = NOVALUE;
    if (!IS_ATOM_INT(_pos_10129)) {
        _1 = (long)(DBL_PTR(_pos_10129)->dbl);
        if (UNIQUE(DBL_PTR(_pos_10129)) && (DBL_PTR(_pos_10129)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_10129);
        _pos_10129 = _1;
    }

    /** 										if pos > decs then*/
    if (_pos_10129 <= _decs_10128)
    goto L61; // [2819] 2844

    /** 											argtext = argtext[ 1 .. $ - pos + decs ]*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5950 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5950 = 1;
    }
    _5951 = _5950 - _pos_10129;
    if ((long)((unsigned long)_5951 +(unsigned long) HIGH_BITS) >= 0){
        _5951 = NewDouble((double)_5951);
    }
    _5950 = NOVALUE;
    if (IS_ATOM_INT(_5951)) {
        _5952 = _5951 + _decs_10128;
    }
    else {
        _5952 = NewDouble(DBL_PTR(_5951)->dbl + (double)_decs_10128);
    }
    DeRef(_5951);
    _5951 = NOVALUE;
    rhs_slice_target = (object_ptr)&_argtext_10297;
    RHS_Slice(_argtext_10297, 1, _5952);
    goto L60; // [2841] 2961
L61: 

    /** 										elsif pos < decs then*/
    if (_pos_10129 >= _decs_10128)
    goto L60; // [2846] 2961

    /** 											if pflag then*/
    if (_pflag_10143 == 0)
    {
        goto L62; // [2852] 2886
    }
    else{
    }

    /** 												argtext = argtext[ 1 .. $ - 1 ] & repeat('0', decs - pos) & ')'*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5955 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5955 = 1;
    }
    _5956 = _5955 - 1;
    _5955 = NOVALUE;
    rhs_slice_target = (object_ptr)&_5957;
    RHS_Slice(_argtext_10297, 1, _5956);
    _5958 = _decs_10128 - _pos_10129;
    _5959 = Repeat(48, _5958);
    _5958 = NOVALUE;
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _5959;
        concat_list[2] = _5957;
        Concat_N((object_ptr)&_argtext_10297, concat_list, 3);
    }
    DeRefDS(_5959);
    _5959 = NOVALUE;
    DeRefDS(_5957);
    _5957 = NOVALUE;
    goto L60; // [2883] 2961
L62: 

    /** 												argtext = argtext & repeat('0', decs - pos)*/
    _5961 = _decs_10128 - _pos_10129;
    _5962 = Repeat(48, _5961);
    _5961 = NOVALUE;
    Concat((object_ptr)&_argtext_10297, _argtext_10297, _5962);
    DeRefDS(_5962);
    _5962 = NOVALUE;
    goto L60; // [2904] 2961
L5E: 

    /** 								elsif decs > 0 then*/
    if (_decs_10128 <= 0)
    goto L63; // [2909] 2960

    /** 									if pflag then*/
    if (_pflag_10143 == 0)
    {
        goto L64; // [2915] 2946
    }
    else{
    }

    /** 										argtext = argtext[1 .. $ - 1] & '.' & repeat('0', decs) & ')'*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5965 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5965 = 1;
    }
    _5966 = _5965 - 1;
    _5965 = NOVALUE;
    rhs_slice_target = (object_ptr)&_5967;
    RHS_Slice(_argtext_10297, 1, _5966);
    _5968 = Repeat(48, _decs_10128);
    {
        int concat_list[4];

        concat_list[0] = 41;
        concat_list[1] = _5968;
        concat_list[2] = 46;
        concat_list[3] = _5967;
        Concat_N((object_ptr)&_argtext_10297, concat_list, 4);
    }
    DeRefDS(_5968);
    _5968 = NOVALUE;
    DeRefDS(_5967);
    _5967 = NOVALUE;
    goto L65; // [2943] 2959
L64: 

    /** 										argtext = argtext & '.' & repeat('0', decs)*/
    _5970 = Repeat(48, _decs_10128);
    {
        int concat_list[3];

        concat_list[0] = _5970;
        concat_list[1] = 46;
        concat_list[2] = _argtext_10297;
        Concat_N((object_ptr)&_argtext_10297, concat_list, 3);
    }
    DeRefDS(_5970);
    _5970 = NOVALUE;
L65: 
L63: 
L60: 
L5D: 
L5B: 
L5A: 

    /**     				if align = 0 then*/
    if (_align_10120 != 0)
    goto L66; // [2968] 2995

    /**     					if atom(currargv) then*/
    _5973 = IS_ATOM(_currargv_10138);
    if (_5973 == 0)
    {
        _5973 = NOVALUE;
        goto L67; // [2977] 2988
    }
    else{
        _5973 = NOVALUE;
    }

    /**     						align = '>'*/
    _align_10120 = 62;
    goto L68; // [2985] 2994
L67: 

    /**     						align = '<'*/
    _align_10120 = 60;
L68: 
L66: 

    /**     				if atom(currargv) then*/
    _5974 = IS_ATOM(_currargv_10138);
    if (_5974 == 0)
    {
        _5974 = NOVALUE;
        goto L69; // [3000] 3237
    }
    else{
        _5974 = NOVALUE;
    }

    /** 	    				if tsep != 0 and zfill = 0 then*/
    _5975 = (_tsep_10135 != 0);
    if (_5975 == 0) {
        goto L6A; // [3011] 3234
    }
    _5977 = (_zfill_10123 == 0);
    if (_5977 == 0)
    {
        DeRef(_5977);
        _5977 = NOVALUE;
        goto L6A; // [3022] 3234
    }
    else{
        DeRef(_5977);
        _5977 = NOVALUE;
    }

    /** 	    					integer dpos*/

    /** 	    					integer dist*/

    /** 	    					integer bracketed*/

    /** 	    					if binout or hexout then*/
    if (_binout_10134 != 0) {
        goto L6B; // [3035] 3044
    }
    if (_hexout_10133 == 0)
    {
        goto L6C; // [3040] 3057
    }
    else{
    }
L6B: 

    /** 	    						dist = 4*/
    _dist_10680 = 4;

    /** 							psign = 0*/
    _psign_10121 = 0;
    goto L6D; // [3054] 3063
L6C: 

    /** 	    						dist = 3*/
    _dist_10680 = 3;
L6D: 

    /** 	    					bracketed = (argtext[1] = '(')*/
    _2 = (int)SEQ_PTR(_argtext_10297);
    _5979 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_5979)) {
        _bracketed_10681 = (_5979 == 40);
    }
    else {
        _bracketed_10681 = binary_op(EQUALS, _5979, 40);
    }
    _5979 = NOVALUE;
    if (!IS_ATOM_INT(_bracketed_10681)) {
        _1 = (long)(DBL_PTR(_bracketed_10681)->dbl);
        if (UNIQUE(DBL_PTR(_bracketed_10681)) && (DBL_PTR(_bracketed_10681)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bracketed_10681);
        _bracketed_10681 = _1;
    }

    /** 	    					if bracketed then*/
    if (_bracketed_10681 == 0)
    {
        goto L6E; // [3077] 3095
    }
    else{
    }

    /** 	    						argtext = argtext[2 .. $-1]*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5981 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5981 = 1;
    }
    _5982 = _5981 - 1;
    _5981 = NOVALUE;
    rhs_slice_target = (object_ptr)&_argtext_10297;
    RHS_Slice(_argtext_10297, 2, _5982);
L6E: 

    /** 	    					dpos = find('.', argtext)*/
    _dpos_10679 = find_from(46, _argtext_10297, 1);

    /** 	    					if dpos = 0 then*/
    if (_dpos_10679 != 0)
    goto L6F; // [3104] 3120

    /** 	    						dpos = length(argtext) + 1*/
    if (IS_SEQUENCE(_argtext_10297)){
            _5986 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _5986 = 1;
    }
    _dpos_10679 = _5986 + 1;
    _5986 = NOVALUE;
    goto L70; // [3117] 3134
L6F: 

    /** 	    						if tsep = '.' then*/
    if (_tsep_10135 != 46)
    goto L71; // [3122] 3133

    /** 	    							argtext[dpos] = ','*/
    _2 = (int)SEQ_PTR(_argtext_10297);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _argtext_10297 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _dpos_10679);
    _1 = *(int *)_2;
    *(int *)_2 = 44;
    DeRef(_1);
L71: 
L70: 

    /** 	    					while dpos > dist do*/
L72: 
    if (_dpos_10679 <= _dist_10680)
    goto L73; // [3141] 3219

    /** 	    						dpos -= dist*/
    _dpos_10679 = _dpos_10679 - _dist_10680;

    /** 	    						if dpos > 1 + (currargv < 0) * not msign + (currargv > 0) * psign then*/
    if (IS_ATOM_INT(_currargv_10138)) {
        _5991 = (_currargv_10138 < 0);
    }
    else {
        _5991 = binary_op(LESS, _currargv_10138, 0);
    }
    _5992 = (_msign_10122 == 0);
    if (IS_ATOM_INT(_5991)) {
        if (_5991 == (short)_5991 && _5992 <= INT15 && _5992 >= -INT15)
        _5993 = _5991 * _5992;
        else
        _5993 = NewDouble(_5991 * (double)_5992);
    }
    else {
        _5993 = binary_op(MULTIPLY, _5991, _5992);
    }
    DeRef(_5991);
    _5991 = NOVALUE;
    _5992 = NOVALUE;
    if (IS_ATOM_INT(_5993)) {
        _5994 = _5993 + 1;
        if (_5994 > MAXINT){
            _5994 = NewDouble((double)_5994);
        }
    }
    else
    _5994 = binary_op(PLUS, 1, _5993);
    DeRef(_5993);
    _5993 = NOVALUE;
    if (IS_ATOM_INT(_currargv_10138)) {
        _5995 = (_currargv_10138 > 0);
    }
    else {
        _5995 = binary_op(GREATER, _currargv_10138, 0);
    }
    if (IS_ATOM_INT(_5995)) {
        if (_5995 == (short)_5995 && _psign_10121 <= INT15 && _psign_10121 >= -INT15)
        _5996 = _5995 * _psign_10121;
        else
        _5996 = NewDouble(_5995 * (double)_psign_10121);
    }
    else {
        _5996 = binary_op(MULTIPLY, _5995, _psign_10121);
    }
    DeRef(_5995);
    _5995 = NOVALUE;
    if (IS_ATOM_INT(_5994) && IS_ATOM_INT(_5996)) {
        _5997 = _5994 + _5996;
        if ((long)((unsigned long)_5997 + (unsigned long)HIGH_BITS) >= 0) 
        _5997 = NewDouble((double)_5997);
    }
    else {
        _5997 = binary_op(PLUS, _5994, _5996);
    }
    DeRef(_5994);
    _5994 = NOVALUE;
    DeRef(_5996);
    _5996 = NOVALUE;
    if (binary_op_a(LESSEQ, _dpos_10679, _5997)){
        DeRef(_5997);
        _5997 = NOVALUE;
        goto L72; // [3184] 3139
    }
    DeRef(_5997);
    _5997 = NOVALUE;

    /** 	    							argtext = argtext[1.. dpos - 1] & tsep & argtext[dpos .. $]*/
    _5999 = _dpos_10679 - 1;
    rhs_slice_target = (object_ptr)&_6000;
    RHS_Slice(_argtext_10297, 1, _5999);
    if (IS_SEQUENCE(_argtext_10297)){
            _6001 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _6001 = 1;
    }
    rhs_slice_target = (object_ptr)&_6002;
    RHS_Slice(_argtext_10297, _dpos_10679, _6001);
    {
        int concat_list[3];

        concat_list[0] = _6002;
        concat_list[1] = _tsep_10135;
        concat_list[2] = _6000;
        Concat_N((object_ptr)&_argtext_10297, concat_list, 3);
    }
    DeRefDS(_6002);
    _6002 = NOVALUE;
    DeRefDS(_6000);
    _6000 = NOVALUE;

    /** 	    					end while*/
    goto L72; // [3216] 3139
L73: 

    /** 	    					if bracketed then*/
    if (_bracketed_10681 == 0)
    {
        goto L74; // [3221] 3233
    }
    else{
    }

    /** 	    						argtext = '(' & argtext & ')'*/
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _argtext_10297;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_10297, concat_list, 3);
    }
L74: 
L6A: 
L69: 

    /**     				if width <= 0 then*/
    if (_width_10127 > 0)
    goto L75; // [3241] 3251

    /**     					width = length(argtext)*/
    if (IS_SEQUENCE(_argtext_10297)){
            _width_10127 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _width_10127 = 1;
    }
L75: 

    /**     				if width < length(argtext) then*/
    if (IS_SEQUENCE(_argtext_10297)){
            _6007 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _6007 = 1;
    }
    if (_width_10127 >= _6007)
    goto L76; // [3256] 3387

    /**     					if align = '>' then*/
    if (_align_10120 != 62)
    goto L77; // [3262] 3290

    /**     						argtext = argtext[ $ - width + 1 .. $]*/
    if (IS_SEQUENCE(_argtext_10297)){
            _6010 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _6010 = 1;
    }
    _6011 = _6010 - _width_10127;
    if ((long)((unsigned long)_6011 +(unsigned long) HIGH_BITS) >= 0){
        _6011 = NewDouble((double)_6011);
    }
    _6010 = NOVALUE;
    if (IS_ATOM_INT(_6011)) {
        _6012 = _6011 + 1;
        if (_6012 > MAXINT){
            _6012 = NewDouble((double)_6012);
        }
    }
    else
    _6012 = binary_op(PLUS, 1, _6011);
    DeRef(_6011);
    _6011 = NOVALUE;
    if (IS_SEQUENCE(_argtext_10297)){
            _6013 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _6013 = 1;
    }
    rhs_slice_target = (object_ptr)&_argtext_10297;
    RHS_Slice(_argtext_10297, _6012, _6013);
    goto L78; // [3287] 3524
L77: 

    /**     					elsif align = 'c' then*/
    if (_align_10120 != 99)
    goto L79; // [3292] 3376

    /**     						pos = length(argtext) - width*/
    if (IS_SEQUENCE(_argtext_10297)){
            _6016 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _6016 = 1;
    }
    _pos_10129 = _6016 - _width_10127;
    _6016 = NOVALUE;

    /**     						if remainder(pos, 2) = 0 then*/
    _6018 = (_pos_10129 % 2);
    if (_6018 != 0)
    goto L7A; // [3311] 3344

    /**     							pos = pos / 2*/
    if (_pos_10129 & 1) {
        _pos_10129 = NewDouble((_pos_10129 >> 1) + 0.5);
    }
    else
    _pos_10129 = _pos_10129 >> 1;
    if (!IS_ATOM_INT(_pos_10129)) {
        _1 = (long)(DBL_PTR(_pos_10129)->dbl);
        if (UNIQUE(DBL_PTR(_pos_10129)) && (DBL_PTR(_pos_10129)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_10129);
        _pos_10129 = _1;
    }

    /**     							argtext = argtext[ pos + 1 .. $ - pos ]*/
    _6021 = _pos_10129 + 1;
    if (_6021 > MAXINT){
        _6021 = NewDouble((double)_6021);
    }
    if (IS_SEQUENCE(_argtext_10297)){
            _6022 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _6022 = 1;
    }
    _6023 = _6022 - _pos_10129;
    _6022 = NOVALUE;
    rhs_slice_target = (object_ptr)&_argtext_10297;
    RHS_Slice(_argtext_10297, _6021, _6023);
    goto L78; // [3341] 3524
L7A: 

    /**     							pos = floor(pos / 2)*/
    _pos_10129 = _pos_10129 >> 1;

    /**     							argtext = argtext[ pos + 1 .. $ - pos - 1]*/
    _6026 = _pos_10129 + 1;
    if (_6026 > MAXINT){
        _6026 = NewDouble((double)_6026);
    }
    if (IS_SEQUENCE(_argtext_10297)){
            _6027 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _6027 = 1;
    }
    _6028 = _6027 - _pos_10129;
    if ((long)((unsigned long)_6028 +(unsigned long) HIGH_BITS) >= 0){
        _6028 = NewDouble((double)_6028);
    }
    _6027 = NOVALUE;
    if (IS_ATOM_INT(_6028)) {
        _6029 = _6028 - 1;
    }
    else {
        _6029 = NewDouble(DBL_PTR(_6028)->dbl - (double)1);
    }
    DeRef(_6028);
    _6028 = NOVALUE;
    rhs_slice_target = (object_ptr)&_argtext_10297;
    RHS_Slice(_argtext_10297, _6026, _6029);
    goto L78; // [3373] 3524
L79: 

    /**     						argtext = argtext[ 1 .. width]*/
    rhs_slice_target = (object_ptr)&_argtext_10297;
    RHS_Slice(_argtext_10297, 1, _width_10127);
    goto L78; // [3384] 3524
L76: 

    /**     				elsif width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_10297)){
            _6032 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _6032 = 1;
    }
    if (_width_10127 <= _6032)
    goto L7B; // [3392] 3523

    /** 						if align = '>' then*/
    if (_align_10120 != 62)
    goto L7C; // [3398] 3422

    /** 							argtext = repeat(' ', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_10297)){
            _6035 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _6035 = 1;
    }
    _6036 = _width_10127 - _6035;
    _6035 = NOVALUE;
    _6037 = Repeat(32, _6036);
    _6036 = NOVALUE;
    Concat((object_ptr)&_argtext_10297, _6037, _argtext_10297);
    DeRefDS(_6037);
    _6037 = NOVALUE;
    DeRef(_6037);
    _6037 = NOVALUE;
    goto L7D; // [3419] 3522
L7C: 

    /**     					elsif align = 'c' then*/
    if (_align_10120 != 99)
    goto L7E; // [3424] 3504

    /**     						pos = width - length(argtext)*/
    if (IS_SEQUENCE(_argtext_10297)){
            _6040 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _6040 = 1;
    }
    _pos_10129 = _width_10127 - _6040;
    _6040 = NOVALUE;

    /**     						if remainder(pos, 2) = 0 then*/
    _6042 = (_pos_10129 % 2);
    if (_6042 != 0)
    goto L7F; // [3443] 3474

    /**     							pos = pos / 2*/
    if (_pos_10129 & 1) {
        _pos_10129 = NewDouble((_pos_10129 >> 1) + 0.5);
    }
    else
    _pos_10129 = _pos_10129 >> 1;
    if (!IS_ATOM_INT(_pos_10129)) {
        _1 = (long)(DBL_PTR(_pos_10129)->dbl);
        if (UNIQUE(DBL_PTR(_pos_10129)) && (DBL_PTR(_pos_10129)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_10129);
        _pos_10129 = _1;
    }

    /**     							argtext = repeat(' ', pos) & argtext & repeat(' ', pos)*/
    _6045 = Repeat(32, _pos_10129);
    _6046 = Repeat(32, _pos_10129);
    {
        int concat_list[3];

        concat_list[0] = _6046;
        concat_list[1] = _argtext_10297;
        concat_list[2] = _6045;
        Concat_N((object_ptr)&_argtext_10297, concat_list, 3);
    }
    DeRefDS(_6046);
    _6046 = NOVALUE;
    DeRefDS(_6045);
    _6045 = NOVALUE;
    goto L7D; // [3471] 3522
L7F: 

    /**     							pos = floor(pos / 2)*/
    _pos_10129 = _pos_10129 >> 1;

    /**     							argtext = repeat(' ', pos) & argtext & repeat(' ', pos + 1)*/
    _6049 = Repeat(32, _pos_10129);
    _6050 = _pos_10129 + 1;
    _6051 = Repeat(32, _6050);
    _6050 = NOVALUE;
    {
        int concat_list[3];

        concat_list[0] = _6051;
        concat_list[1] = _argtext_10297;
        concat_list[2] = _6049;
        Concat_N((object_ptr)&_argtext_10297, concat_list, 3);
    }
    DeRefDS(_6051);
    _6051 = NOVALUE;
    DeRefDS(_6049);
    _6049 = NOVALUE;
    goto L7D; // [3501] 3522
L7E: 

    /** 							argtext = argtext & repeat(' ', width - length(argtext))*/
    if (IS_SEQUENCE(_argtext_10297)){
            _6053 = SEQ_PTR(_argtext_10297)->length;
    }
    else {
        _6053 = 1;
    }
    _6054 = _width_10127 - _6053;
    _6053 = NOVALUE;
    _6055 = Repeat(32, _6054);
    _6054 = NOVALUE;
    Concat((object_ptr)&_argtext_10297, _argtext_10297, _6055);
    DeRefDS(_6055);
    _6055 = NOVALUE;
L7D: 
L7B: 
L78: 

    /**     				result &= argtext*/
    Concat((object_ptr)&_result_10114, _result_10114, _argtext_10297);
    goto L80; // [3530] 3546
L57: 

    /**     				if spacer then*/
    if (_spacer_10125 == 0)
    {
        goto L81; // [3535] 3545
    }
    else{
    }

    /**     					result &= ' '*/
    Append(&_result_10114, _result_10114, 32);
L81: 
L80: 

    /**    				if trimming then*/
    if (_trimming_10132 == 0)
    {
        goto L82; // [3550] 3564
    }
    else{
    }

    /**    					result = trim(result)*/
    RefDS(_result_10114);
    RefDS(_4443);
    _0 = _result_10114;
    _result_10114 = _4trim(_result_10114, _4443, 0);
    DeRefDS(_0);
L82: 

    /**     			tend = 0*/
    _tend_10118 = 0;

    /** 		    	prevargv = currargv*/
    Ref(_currargv_10138);
    DeRef(_prevargv_10137);
    _prevargv_10137 = _currargv_10138;
L1F: 
    DeRef(_argtext_10297);
    _argtext_10297 = NOVALUE;

    /**     end while*/
    goto L2; // [3582] 60
L3: 

    /** 	return result*/
    DeRefDS(_format_pattern_10112);
    DeRef(_arg_list_10113);
    DeRef(_prevargv_10137);
    DeRef(_currargv_10138);
    DeRef(_idname_10139);
    DeRef(_envsym_10140);
    DeRefi(_envvar_10141);
    DeRef(_5999);
    _5999 = NOVALUE;
    DeRef(_5713);
    _5713 = NOVALUE;
    DeRef(_5765);
    _5765 = NOVALUE;
    DeRef(_5745);
    _5745 = NOVALUE;
    DeRef(_5881);
    _5881 = NOVALUE;
    DeRef(_6026);
    _6026 = NOVALUE;
    DeRef(_5952);
    _5952 = NOVALUE;
    DeRef(_6023);
    _6023 = NOVALUE;
    DeRef(_5808);
    _5808 = NOVALUE;
    DeRef(_5903);
    _5903 = NOVALUE;
    DeRef(_5975);
    _5975 = NOVALUE;
    DeRef(_6021);
    _6021 = NOVALUE;
    DeRef(_5944);
    _5944 = NOVALUE;
    DeRef(_5844);
    _5844 = NOVALUE;
    DeRef(_6029);
    _6029 = NOVALUE;
    DeRef(_6042);
    _6042 = NOVALUE;
    DeRef(_5735);
    _5735 = NOVALUE;
    DeRef(_5686);
    _5686 = NOVALUE;
    DeRef(_5913);
    _5913 = NOVALUE;
    DeRef(_5966);
    _5966 = NOVALUE;
    DeRef(_5982);
    _5982 = NOVALUE;
    DeRef(_6018);
    _6018 = NOVALUE;
    DeRef(_5755);
    _5755 = NOVALUE;
    DeRef(_5663);
    _5663 = NOVALUE;
    DeRef(_5725);
    _5725 = NOVALUE;
    DeRef(_6012);
    _6012 = NOVALUE;
    _5883 = NOVALUE;
    DeRef(_5826);
    _5826 = NOVALUE;
    DeRef(_5956);
    _5956 = NOVALUE;
    return _result_10114;
    ;
}


int _4wrap(int _content_10791, int _width_10792, int _wrap_with_10793, int _wrap_at_10794)
{
    int _result_10799 = NOVALUE;
    int _split_at_10802 = NOVALUE;
    int _6085 = NOVALUE;
    int _6083 = NOVALUE;
    int _6081 = NOVALUE;
    int _6080 = NOVALUE;
    int _6079 = NOVALUE;
    int _6077 = NOVALUE;
    int _6076 = NOVALUE;
    int _6074 = NOVALUE;
    int _6071 = NOVALUE;
    int _6069 = NOVALUE;
    int _6068 = NOVALUE;
    int _6067 = NOVALUE;
    int _6065 = NOVALUE;
    int _6064 = NOVALUE;
    int _6063 = NOVALUE;
    int _6061 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_width_10792)) {
        _1 = (long)(DBL_PTR(_width_10792)->dbl);
        if (UNIQUE(DBL_PTR(_width_10792)) && (DBL_PTR(_width_10792)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_width_10792);
        _width_10792 = _1;
    }

    /** 	if length(content) < width then*/
    if (IS_SEQUENCE(_content_10791)){
            _6061 = SEQ_PTR(_content_10791)->length;
    }
    else {
        _6061 = 1;
    }
    if (_6061 >= _width_10792)
    goto L1; // [14] 25

    /** 		return content*/
    DeRefDS(_wrap_with_10793);
    DeRefDS(_wrap_at_10794);
    DeRef(_result_10799);
    return _content_10791;
L1: 

    /** 	sequence result = ""*/
    RefDS(_5);
    DeRef(_result_10799);
    _result_10799 = _5;

    /** 	while length(content) do*/
L2: 
    if (IS_SEQUENCE(_content_10791)){
            _6063 = SEQ_PTR(_content_10791)->length;
    }
    else {
        _6063 = 1;
    }
    if (_6063 == 0)
    {
        _6063 = NOVALUE;
        goto L3; // [40] 259
    }
    else{
        _6063 = NOVALUE;
    }

    /** 		integer split_at = 0*/
    _split_at_10802 = 0;

    /** 		for i = width to 1 by -1 do*/
    {
        int _i_10804;
        _i_10804 = _width_10792;
L4: 
        if (_i_10804 < 1){
            goto L5; // [50] 88
        }

        /** 			if find(content[i], wrap_at) then*/
        _2 = (int)SEQ_PTR(_content_10791);
        _6064 = (int)*(((s1_ptr)_2)->base + _i_10804);
        _6065 = find_from(_6064, _wrap_at_10794, 1);
        _6064 = NOVALUE;
        if (_6065 == 0)
        {
            _6065 = NOVALUE;
            goto L6; // [68] 81
        }
        else{
            _6065 = NOVALUE;
        }

        /** 				split_at = i*/
        _split_at_10802 = _i_10804;

        /** 				exit*/
        goto L5; // [78] 88
L6: 

        /** 		end for*/
        _i_10804 = _i_10804 + -1;
        goto L4; // [83] 57
L5: 
        ;
    }

    /** 		if split_at = 0 then*/
    if (_split_at_10802 != 0)
    goto L7; // [90] 172

    /** 			for i = width to length(content) do*/
    if (IS_SEQUENCE(_content_10791)){
            _6067 = SEQ_PTR(_content_10791)->length;
    }
    else {
        _6067 = 1;
    }
    {
        int _i_10811;
        _i_10811 = _width_10792;
L8: 
        if (_i_10811 > _6067){
            goto L9; // [99] 137
        }

        /** 				if find(content[i], wrap_at) then*/
        _2 = (int)SEQ_PTR(_content_10791);
        _6068 = (int)*(((s1_ptr)_2)->base + _i_10811);
        _6069 = find_from(_6068, _wrap_at_10794, 1);
        _6068 = NOVALUE;
        if (_6069 == 0)
        {
            _6069 = NOVALUE;
            goto LA; // [117] 130
        }
        else{
            _6069 = NOVALUE;
        }

        /** 					split_at = i*/
        _split_at_10802 = _i_10811;

        /** 					exit*/
        goto L9; // [127] 137
LA: 

        /** 			end for*/
        _i_10811 = _i_10811 + 1;
        goto L8; // [132] 106
L9: 
        ;
    }

    /** 			if split_at = 0 then*/
    if (_split_at_10802 != 0)
    goto LB; // [139] 171

    /** 				if length(result) then*/
    if (IS_SEQUENCE(_result_10799)){
            _6071 = SEQ_PTR(_result_10799)->length;
    }
    else {
        _6071 = 1;
    }
    if (_6071 == 0)
    {
        _6071 = NOVALUE;
        goto LC; // [148] 158
    }
    else{
        _6071 = NOVALUE;
    }

    /** 					result &= wrap_with*/
    Concat((object_ptr)&_result_10799, _result_10799, _wrap_with_10793);
LC: 

    /** 				result &= content*/
    Concat((object_ptr)&_result_10799, _result_10799, _content_10791);

    /** 				exit*/
    goto L3; // [168] 259
LB: 
L7: 

    /** 		if length(result) then*/
    if (IS_SEQUENCE(_result_10799)){
            _6074 = SEQ_PTR(_result_10799)->length;
    }
    else {
        _6074 = 1;
    }
    if (_6074 == 0)
    {
        _6074 = NOVALUE;
        goto LD; // [177] 187
    }
    else{
        _6074 = NOVALUE;
    }

    /** 			result &= wrap_with*/
    Concat((object_ptr)&_result_10799, _result_10799, _wrap_with_10793);
LD: 

    /** 		result &= trim(content[1..split_at])*/
    rhs_slice_target = (object_ptr)&_6076;
    RHS_Slice(_content_10791, 1, _split_at_10802);
    RefDS(_4443);
    _6077 = _4trim(_6076, _4443, 0);
    _6076 = NOVALUE;
    if (IS_SEQUENCE(_result_10799) && IS_ATOM(_6077)) {
        Ref(_6077);
        Append(&_result_10799, _result_10799, _6077);
    }
    else if (IS_ATOM(_result_10799) && IS_SEQUENCE(_6077)) {
    }
    else {
        Concat((object_ptr)&_result_10799, _result_10799, _6077);
    }
    DeRef(_6077);
    _6077 = NOVALUE;

    /** 		content = trim(content[split_at + 1..$])*/
    _6079 = _split_at_10802 + 1;
    if (_6079 > MAXINT){
        _6079 = NewDouble((double)_6079);
    }
    if (IS_SEQUENCE(_content_10791)){
            _6080 = SEQ_PTR(_content_10791)->length;
    }
    else {
        _6080 = 1;
    }
    rhs_slice_target = (object_ptr)&_6081;
    RHS_Slice(_content_10791, _6079, _6080);
    RefDS(_4443);
    _0 = _content_10791;
    _content_10791 = _4trim(_6081, _4443, 0);
    DeRefDS(_0);
    _6081 = NOVALUE;

    /** 		if length(content) < width then*/
    if (IS_SEQUENCE(_content_10791)){
            _6083 = SEQ_PTR(_content_10791)->length;
    }
    else {
        _6083 = 1;
    }
    if (_6083 >= _width_10792)
    goto LE; // [231] 252

    /** 			result &= wrap_with & content*/
    Concat((object_ptr)&_6085, _wrap_with_10793, _content_10791);
    Concat((object_ptr)&_result_10799, _result_10799, _6085);
    DeRefDS(_6085);
    _6085 = NOVALUE;

    /** 			exit*/
    goto L3; // [249] 259
LE: 

    /** 	end while*/
    goto L2; // [256] 37
L3: 

    /** 	return result*/
    DeRefDS(_content_10791);
    DeRefDS(_wrap_with_10793);
    DeRefDS(_wrap_at_10794);
    DeRef(_6079);
    _6079 = NOVALUE;
    return _result_10799;
    ;
}



// 0xCA7716E2
