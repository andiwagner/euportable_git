// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _21mapping(int _source_arg_5493, int _from_set_5494, int _to_set_5495, int _one_level_5496)
{
    int _pos_5497 = NOVALUE;
    int _2796 = NOVALUE;
    int _2795 = NOVALUE;
    int _2794 = NOVALUE;
    int _2793 = NOVALUE;
    int _2792 = NOVALUE;
    int _2791 = NOVALUE;
    int _2790 = NOVALUE;
    int _2789 = NOVALUE;
    int _2788 = NOVALUE;
    int _2786 = NOVALUE;
    int _2784 = NOVALUE;
    int _2783 = NOVALUE;
    int _2782 = NOVALUE;
    int _2780 = NOVALUE;
    int _2779 = NOVALUE;
    int _2778 = NOVALUE;
    int _2777 = NOVALUE;
    int _2775 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(source_arg) then*/
    _2775 = IS_ATOM(_source_arg_5493);
    if (_2775 == 0)
    {
        _2775 = NOVALUE;
        goto L1; // [12] 53
    }
    else{
        _2775 = NOVALUE;
    }

    /** 		pos = find(source_arg, from_set)*/
    _pos_5497 = find_from(_source_arg_5493, _from_set_5494, 1);

    /** 		if pos >= 1  and pos <= length(to_set) then*/
    _2777 = (_pos_5497 >= 1);
    if (_2777 == 0) {
        goto L2; // [28] 161
    }
    if (IS_SEQUENCE(_to_set_5495)){
            _2779 = SEQ_PTR(_to_set_5495)->length;
    }
    else {
        _2779 = 1;
    }
    _2780 = (_pos_5497 <= _2779);
    _2779 = NOVALUE;
    if (_2780 == 0)
    {
        DeRef(_2780);
        _2780 = NOVALUE;
        goto L2; // [40] 161
    }
    else{
        DeRef(_2780);
        _2780 = NOVALUE;
    }

    /** 			source_arg = to_set[pos]*/
    DeRef(_source_arg_5493);
    _2 = (int)SEQ_PTR(_to_set_5495);
    _source_arg_5493 = (int)*(((s1_ptr)_2)->base + _pos_5497);
    Ref(_source_arg_5493);
    goto L2; // [50] 161
L1: 

    /** 		for i = 1 to length(source_arg) do*/
    if (IS_SEQUENCE(_source_arg_5493)){
            _2782 = SEQ_PTR(_source_arg_5493)->length;
    }
    else {
        _2782 = 1;
    }
    {
        int _i_5509;
        _i_5509 = 1;
L3: 
        if (_i_5509 > _2782){
            goto L4; // [58] 160
        }

        /** 			if atom(source_arg[i]) or one_level then*/
        _2 = (int)SEQ_PTR(_source_arg_5493);
        _2783 = (int)*(((s1_ptr)_2)->base + _i_5509);
        _2784 = IS_ATOM(_2783);
        _2783 = NOVALUE;
        if (_2784 != 0) {
            goto L5; // [74] 83
        }
        if (_one_level_5496 == 0)
        {
            goto L6; // [79] 129
        }
        else{
        }
L5: 

        /** 				pos = find(source_arg[i], from_set)*/
        _2 = (int)SEQ_PTR(_source_arg_5493);
        _2786 = (int)*(((s1_ptr)_2)->base + _i_5509);
        _pos_5497 = find_from(_2786, _from_set_5494, 1);
        _2786 = NOVALUE;

        /** 				if pos >= 1  and pos <= length(to_set) then*/
        _2788 = (_pos_5497 >= 1);
        if (_2788 == 0) {
            goto L7; // [100] 153
        }
        if (IS_SEQUENCE(_to_set_5495)){
                _2790 = SEQ_PTR(_to_set_5495)->length;
        }
        else {
            _2790 = 1;
        }
        _2791 = (_pos_5497 <= _2790);
        _2790 = NOVALUE;
        if (_2791 == 0)
        {
            DeRef(_2791);
            _2791 = NOVALUE;
            goto L7; // [112] 153
        }
        else{
            DeRef(_2791);
            _2791 = NOVALUE;
        }

        /** 					source_arg[i] = to_set[pos]*/
        _2 = (int)SEQ_PTR(_to_set_5495);
        _2792 = (int)*(((s1_ptr)_2)->base + _pos_5497);
        Ref(_2792);
        _2 = (int)SEQ_PTR(_source_arg_5493);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _source_arg_5493 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_5509);
        _1 = *(int *)_2;
        *(int *)_2 = _2792;
        if( _1 != _2792 ){
            DeRef(_1);
        }
        _2792 = NOVALUE;
        goto L7; // [126] 153
L6: 

        /** 				source_arg[i] = mapping(source_arg[i], from_set, to_set)*/
        _2 = (int)SEQ_PTR(_source_arg_5493);
        _2793 = (int)*(((s1_ptr)_2)->base + _i_5509);
        RefDS(_from_set_5494);
        DeRef(_2794);
        _2794 = _from_set_5494;
        RefDS(_to_set_5495);
        DeRef(_2795);
        _2795 = _to_set_5495;
        Ref(_2793);
        _2796 = _21mapping(_2793, _2794, _2795, 0);
        _2793 = NOVALUE;
        _2794 = NOVALUE;
        _2795 = NOVALUE;
        _2 = (int)SEQ_PTR(_source_arg_5493);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _source_arg_5493 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_5509);
        _1 = *(int *)_2;
        *(int *)_2 = _2796;
        if( _1 != _2796 ){
            DeRef(_1);
        }
        _2796 = NOVALUE;
L7: 

        /** 		end for*/
        _i_5509 = _i_5509 + 1;
        goto L3; // [155] 65
L4: 
        ;
    }
L2: 

    /** 	return source_arg*/
    DeRefDS(_from_set_5494);
    DeRefDS(_to_set_5495);
    DeRef(_2777);
    _2777 = NOVALUE;
    DeRef(_2788);
    _2788 = NOVALUE;
    return _source_arg_5493;
    ;
}


int _21reverse(int _target_5530, int _pFrom_5531, int _pTo_5532)
{
    int _uppr_5533 = NOVALUE;
    int _n_5534 = NOVALUE;
    int _lLimit_5535 = NOVALUE;
    int _t_5536 = NOVALUE;
    int _2811 = NOVALUE;
    int _2810 = NOVALUE;
    int _2809 = NOVALUE;
    int _2807 = NOVALUE;
    int _2806 = NOVALUE;
    int _2804 = NOVALUE;
    int _2802 = NOVALUE;
    int _0, _1, _2;
    

    /** 	n = length(target)*/
    if (IS_SEQUENCE(_target_5530)){
            _n_5534 = SEQ_PTR(_target_5530)->length;
    }
    else {
        _n_5534 = 1;
    }

    /** 	if n < 2 then*/
    if (_n_5534 >= 2)
    goto L1; // [12] 23

    /** 		return target*/
    DeRef(_t_5536);
    return _target_5530;
L1: 

    /** 	if pFrom < 1 then*/
    if (_pFrom_5531 >= 1)
    goto L2; // [25] 35

    /** 		pFrom = 1*/
    _pFrom_5531 = 1;
L2: 

    /** 	if pTo < 1 then*/
    if (_pTo_5532 >= 1)
    goto L3; // [37] 48

    /** 		pTo = n + pTo*/
    _pTo_5532 = _n_5534 + _pTo_5532;
L3: 

    /** 	if pTo < pFrom or pFrom >= n then*/
    _2802 = (_pTo_5532 < _pFrom_5531);
    if (_2802 != 0) {
        goto L4; // [54] 67
    }
    _2804 = (_pFrom_5531 >= _n_5534);
    if (_2804 == 0)
    {
        DeRef(_2804);
        _2804 = NOVALUE;
        goto L5; // [63] 74
    }
    else{
        DeRef(_2804);
        _2804 = NOVALUE;
    }
L4: 

    /** 		return target*/
    DeRef(_t_5536);
    DeRef(_2802);
    _2802 = NOVALUE;
    return _target_5530;
L5: 

    /** 	if pTo > n then*/
    if (_pTo_5532 <= _n_5534)
    goto L6; // [76] 86

    /** 		pTo = n*/
    _pTo_5532 = _n_5534;
L6: 

    /** 	lLimit = floor((pFrom+pTo-1)/2)*/
    _2806 = _pFrom_5531 + _pTo_5532;
    if ((long)((unsigned long)_2806 + (unsigned long)HIGH_BITS) >= 0) 
    _2806 = NewDouble((double)_2806);
    if (IS_ATOM_INT(_2806)) {
        _2807 = _2806 - 1;
        if ((long)((unsigned long)_2807 +(unsigned long) HIGH_BITS) >= 0){
            _2807 = NewDouble((double)_2807);
        }
    }
    else {
        _2807 = NewDouble(DBL_PTR(_2806)->dbl - (double)1);
    }
    DeRef(_2806);
    _2806 = NOVALUE;
    if (IS_ATOM_INT(_2807)) {
        _lLimit_5535 = _2807 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _2807, 2);
        _lLimit_5535 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    DeRef(_2807);
    _2807 = NOVALUE;
    if (!IS_ATOM_INT(_lLimit_5535)) {
        _1 = (long)(DBL_PTR(_lLimit_5535)->dbl);
        if (UNIQUE(DBL_PTR(_lLimit_5535)) && (DBL_PTR(_lLimit_5535)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_lLimit_5535);
        _lLimit_5535 = _1;
    }

    /** 	t = target*/
    Ref(_target_5530);
    DeRef(_t_5536);
    _t_5536 = _target_5530;

    /** 	uppr = pTo*/
    _uppr_5533 = _pTo_5532;

    /** 	for lowr = pFrom to lLimit do*/
    _2809 = _lLimit_5535;
    {
        int _lowr_5555;
        _lowr_5555 = _pFrom_5531;
L7: 
        if (_lowr_5555 > _2809){
            goto L8; // [119] 159
        }

        /** 		t[uppr] = target[lowr]*/
        _2 = (int)SEQ_PTR(_target_5530);
        _2810 = (int)*(((s1_ptr)_2)->base + _lowr_5555);
        Ref(_2810);
        _2 = (int)SEQ_PTR(_t_5536);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _t_5536 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _uppr_5533);
        _1 = *(int *)_2;
        *(int *)_2 = _2810;
        if( _1 != _2810 ){
            DeRef(_1);
        }
        _2810 = NOVALUE;

        /** 		t[lowr] = target[uppr]*/
        _2 = (int)SEQ_PTR(_target_5530);
        _2811 = (int)*(((s1_ptr)_2)->base + _uppr_5533);
        Ref(_2811);
        _2 = (int)SEQ_PTR(_t_5536);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _t_5536 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _lowr_5555);
        _1 = *(int *)_2;
        *(int *)_2 = _2811;
        if( _1 != _2811 ){
            DeRef(_1);
        }
        _2811 = NOVALUE;

        /** 		uppr -= 1*/
        _uppr_5533 = _uppr_5533 - 1;

        /** 	end for*/
        _lowr_5555 = _lowr_5555 + 1;
        goto L7; // [154] 126
L8: 
        ;
    }

    /** 	return t*/
    DeRef(_target_5530);
    DeRef(_2802);
    _2802 = NOVALUE;
    return _t_5536;
    ;
}


int _21pad_tail(int _target_5631, int _size_5632, int _ch_5633)
{
    int _2848 = NOVALUE;
    int _2847 = NOVALUE;
    int _2846 = NOVALUE;
    int _2845 = NOVALUE;
    int _2843 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if size <= length(target) then*/
    if (IS_SEQUENCE(_target_5631)){
            _2843 = SEQ_PTR(_target_5631)->length;
    }
    else {
        _2843 = 1;
    }
    if (_size_5632 > _2843)
    goto L1; // [8] 19

    /** 		return target*/
    return _target_5631;
L1: 

    /** 	return target & repeat(ch, size - length(target))*/
    if (IS_SEQUENCE(_target_5631)){
            _2845 = SEQ_PTR(_target_5631)->length;
    }
    else {
        _2845 = 1;
    }
    _2846 = _size_5632 - _2845;
    _2845 = NOVALUE;
    _2847 = Repeat(_ch_5633, _2846);
    _2846 = NOVALUE;
    if (IS_SEQUENCE(_target_5631) && IS_ATOM(_2847)) {
    }
    else if (IS_ATOM(_target_5631) && IS_SEQUENCE(_2847)) {
        Ref(_target_5631);
        Prepend(&_2848, _2847, _target_5631);
    }
    else {
        Concat((object_ptr)&_2848, _target_5631, _2847);
    }
    DeRefDS(_2847);
    _2847 = NOVALUE;
    DeRef(_target_5631);
    return _2848;
    ;
}


int _21filter(int _source_5885, int _rid_5886, int _userdata_5887, int _rangetype_5888)
{
    int _dest_5889 = NOVALUE;
    int _idx_5890 = NOVALUE;
    int _3162 = NOVALUE;
    int _3161 = NOVALUE;
    int _3159 = NOVALUE;
    int _3158 = NOVALUE;
    int _3157 = NOVALUE;
    int _3156 = NOVALUE;
    int _3155 = NOVALUE;
    int _3152 = NOVALUE;
    int _3151 = NOVALUE;
    int _3150 = NOVALUE;
    int _3149 = NOVALUE;
    int _3146 = NOVALUE;
    int _3145 = NOVALUE;
    int _3144 = NOVALUE;
    int _3143 = NOVALUE;
    int _3142 = NOVALUE;
    int _3139 = NOVALUE;
    int _3138 = NOVALUE;
    int _3137 = NOVALUE;
    int _3136 = NOVALUE;
    int _3133 = NOVALUE;
    int _3132 = NOVALUE;
    int _3131 = NOVALUE;
    int _3130 = NOVALUE;
    int _3129 = NOVALUE;
    int _3126 = NOVALUE;
    int _3125 = NOVALUE;
    int _3124 = NOVALUE;
    int _3123 = NOVALUE;
    int _3120 = NOVALUE;
    int _3119 = NOVALUE;
    int _3118 = NOVALUE;
    int _3117 = NOVALUE;
    int _3116 = NOVALUE;
    int _3113 = NOVALUE;
    int _3112 = NOVALUE;
    int _3111 = NOVALUE;
    int _3110 = NOVALUE;
    int _3107 = NOVALUE;
    int _3106 = NOVALUE;
    int _3105 = NOVALUE;
    int _3104 = NOVALUE;
    int _3103 = NOVALUE;
    int _3100 = NOVALUE;
    int _3099 = NOVALUE;
    int _3098 = NOVALUE;
    int _3094 = NOVALUE;
    int _3091 = NOVALUE;
    int _3090 = NOVALUE;
    int _3089 = NOVALUE;
    int _3087 = NOVALUE;
    int _3086 = NOVALUE;
    int _3085 = NOVALUE;
    int _3084 = NOVALUE;
    int _3083 = NOVALUE;
    int _3080 = NOVALUE;
    int _3079 = NOVALUE;
    int _3078 = NOVALUE;
    int _3076 = NOVALUE;
    int _3075 = NOVALUE;
    int _3074 = NOVALUE;
    int _3073 = NOVALUE;
    int _3072 = NOVALUE;
    int _3069 = NOVALUE;
    int _3068 = NOVALUE;
    int _3067 = NOVALUE;
    int _3065 = NOVALUE;
    int _3064 = NOVALUE;
    int _3063 = NOVALUE;
    int _3062 = NOVALUE;
    int _3061 = NOVALUE;
    int _3058 = NOVALUE;
    int _3057 = NOVALUE;
    int _3056 = NOVALUE;
    int _3054 = NOVALUE;
    int _3053 = NOVALUE;
    int _3052 = NOVALUE;
    int _3051 = NOVALUE;
    int _3050 = NOVALUE;
    int _3048 = NOVALUE;
    int _3047 = NOVALUE;
    int _3046 = NOVALUE;
    int _3042 = NOVALUE;
    int _3039 = NOVALUE;
    int _3038 = NOVALUE;
    int _3037 = NOVALUE;
    int _3034 = NOVALUE;
    int _3031 = NOVALUE;
    int _3030 = NOVALUE;
    int _3029 = NOVALUE;
    int _3026 = NOVALUE;
    int _3023 = NOVALUE;
    int _3022 = NOVALUE;
    int _3021 = NOVALUE;
    int _3018 = NOVALUE;
    int _3015 = NOVALUE;
    int _3014 = NOVALUE;
    int _3013 = NOVALUE;
    int _3009 = NOVALUE;
    int _3006 = NOVALUE;
    int _3005 = NOVALUE;
    int _3004 = NOVALUE;
    int _3001 = NOVALUE;
    int _2998 = NOVALUE;
    int _2997 = NOVALUE;
    int _2996 = NOVALUE;
    int _2990 = NOVALUE;
    int _2988 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(source) = 0 then*/
    if (IS_SEQUENCE(_source_5885)){
            _2988 = SEQ_PTR(_source_5885)->length;
    }
    else {
        _2988 = 1;
    }
    if (_2988 != 0)
    goto L1; // [8] 19

    /** 		return source*/
    DeRefDS(_userdata_5887);
    DeRefDS(_rangetype_5888);
    DeRef(_dest_5889);
    return _source_5885;
L1: 

    /** 	dest = repeat(0, length(source))*/
    if (IS_SEQUENCE(_source_5885)){
            _2990 = SEQ_PTR(_source_5885)->length;
    }
    else {
        _2990 = 1;
    }
    DeRef(_dest_5889);
    _dest_5889 = Repeat(0, _2990);
    _2990 = NOVALUE;

    /** 	idx = 0*/
    _idx_5890 = 0;

    /** 	switch rid do*/
    _1 = find(_rid_5886, _2992);
    switch ( _1 ){ 

        /** 		case "<", "lt" then*/
        case 1:
        case 2:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_5885)){
                _2996 = SEQ_PTR(_source_5885)->length;
        }
        else {
            _2996 = 1;
        }
        {
            int _a_5902;
            _a_5902 = 1;
L2: 
            if (_a_5902 > _2996){
                goto L3; // [51] 96
            }

            /** 				if compare(source[a], userdata) < 0 then*/
            _2 = (int)SEQ_PTR(_source_5885);
            _2997 = (int)*(((s1_ptr)_2)->base + _a_5902);
            if (IS_ATOM_INT(_2997) && IS_ATOM_INT(_userdata_5887)){
                _2998 = (_2997 < _userdata_5887) ? -1 : (_2997 > _userdata_5887);
            }
            else{
                _2998 = compare(_2997, _userdata_5887);
            }
            _2997 = NOVALUE;
            if (_2998 >= 0)
            goto L4; // [68] 89

            /** 					idx += 1*/
            _idx_5890 = _idx_5890 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_5885);
            _3001 = (int)*(((s1_ptr)_2)->base + _a_5902);
            Ref(_3001);
            _2 = (int)SEQ_PTR(_dest_5889);
            _2 = (int)(((s1_ptr)_2)->base + _idx_5890);
            _1 = *(int *)_2;
            *(int *)_2 = _3001;
            if( _1 != _3001 ){
                DeRef(_1);
            }
            _3001 = NOVALUE;
L4: 

            /** 			end for*/
            _a_5902 = _a_5902 + 1;
            goto L2; // [91] 58
L3: 
            ;
        }
        goto L5; // [96] 1304

        /** 		case "<=", "le" then*/
        case 3:
        case 4:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_5885)){
                _3004 = SEQ_PTR(_source_5885)->length;
        }
        else {
            _3004 = 1;
        }
        {
            int _a_5914;
            _a_5914 = 1;
L6: 
            if (_a_5914 > _3004){
                goto L7; // [109] 154
            }

            /** 				if compare(source[a], userdata) <= 0 then*/
            _2 = (int)SEQ_PTR(_source_5885);
            _3005 = (int)*(((s1_ptr)_2)->base + _a_5914);
            if (IS_ATOM_INT(_3005) && IS_ATOM_INT(_userdata_5887)){
                _3006 = (_3005 < _userdata_5887) ? -1 : (_3005 > _userdata_5887);
            }
            else{
                _3006 = compare(_3005, _userdata_5887);
            }
            _3005 = NOVALUE;
            if (_3006 > 0)
            goto L8; // [126] 147

            /** 					idx += 1*/
            _idx_5890 = _idx_5890 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_5885);
            _3009 = (int)*(((s1_ptr)_2)->base + _a_5914);
            Ref(_3009);
            _2 = (int)SEQ_PTR(_dest_5889);
            _2 = (int)(((s1_ptr)_2)->base + _idx_5890);
            _1 = *(int *)_2;
            *(int *)_2 = _3009;
            if( _1 != _3009 ){
                DeRef(_1);
            }
            _3009 = NOVALUE;
L8: 

            /** 			end for*/
            _a_5914 = _a_5914 + 1;
            goto L6; // [149] 116
L7: 
            ;
        }
        goto L5; // [154] 1304

        /** 		case "=", "==", "eq" then*/
        case 5:
        case 6:
        case 7:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_5885)){
                _3013 = SEQ_PTR(_source_5885)->length;
        }
        else {
            _3013 = 1;
        }
        {
            int _a_5927;
            _a_5927 = 1;
L9: 
            if (_a_5927 > _3013){
                goto LA; // [169] 214
            }

            /** 				if compare(source[a], userdata) = 0 then*/
            _2 = (int)SEQ_PTR(_source_5885);
            _3014 = (int)*(((s1_ptr)_2)->base + _a_5927);
            if (IS_ATOM_INT(_3014) && IS_ATOM_INT(_userdata_5887)){
                _3015 = (_3014 < _userdata_5887) ? -1 : (_3014 > _userdata_5887);
            }
            else{
                _3015 = compare(_3014, _userdata_5887);
            }
            _3014 = NOVALUE;
            if (_3015 != 0)
            goto LB; // [186] 207

            /** 					idx += 1*/
            _idx_5890 = _idx_5890 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_5885);
            _3018 = (int)*(((s1_ptr)_2)->base + _a_5927);
            Ref(_3018);
            _2 = (int)SEQ_PTR(_dest_5889);
            _2 = (int)(((s1_ptr)_2)->base + _idx_5890);
            _1 = *(int *)_2;
            *(int *)_2 = _3018;
            if( _1 != _3018 ){
                DeRef(_1);
            }
            _3018 = NOVALUE;
LB: 

            /** 			end for*/
            _a_5927 = _a_5927 + 1;
            goto L9; // [209] 176
LA: 
            ;
        }
        goto L5; // [214] 1304

        /** 		case "!=", "ne" then*/
        case 8:
        case 9:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_5885)){
                _3021 = SEQ_PTR(_source_5885)->length;
        }
        else {
            _3021 = 1;
        }
        {
            int _a_5939;
            _a_5939 = 1;
LC: 
            if (_a_5939 > _3021){
                goto LD; // [227] 272
            }

            /** 				if compare(source[a], userdata) != 0 then*/
            _2 = (int)SEQ_PTR(_source_5885);
            _3022 = (int)*(((s1_ptr)_2)->base + _a_5939);
            if (IS_ATOM_INT(_3022) && IS_ATOM_INT(_userdata_5887)){
                _3023 = (_3022 < _userdata_5887) ? -1 : (_3022 > _userdata_5887);
            }
            else{
                _3023 = compare(_3022, _userdata_5887);
            }
            _3022 = NOVALUE;
            if (_3023 == 0)
            goto LE; // [244] 265

            /** 					idx += 1*/
            _idx_5890 = _idx_5890 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_5885);
            _3026 = (int)*(((s1_ptr)_2)->base + _a_5939);
            Ref(_3026);
            _2 = (int)SEQ_PTR(_dest_5889);
            _2 = (int)(((s1_ptr)_2)->base + _idx_5890);
            _1 = *(int *)_2;
            *(int *)_2 = _3026;
            if( _1 != _3026 ){
                DeRef(_1);
            }
            _3026 = NOVALUE;
LE: 

            /** 			end for*/
            _a_5939 = _a_5939 + 1;
            goto LC; // [267] 234
LD: 
            ;
        }
        goto L5; // [272] 1304

        /** 		case ">", "gt" then*/
        case 10:
        case 11:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_5885)){
                _3029 = SEQ_PTR(_source_5885)->length;
        }
        else {
            _3029 = 1;
        }
        {
            int _a_5951;
            _a_5951 = 1;
LF: 
            if (_a_5951 > _3029){
                goto L10; // [285] 330
            }

            /** 				if compare(source[a], userdata) > 0 then*/
            _2 = (int)SEQ_PTR(_source_5885);
            _3030 = (int)*(((s1_ptr)_2)->base + _a_5951);
            if (IS_ATOM_INT(_3030) && IS_ATOM_INT(_userdata_5887)){
                _3031 = (_3030 < _userdata_5887) ? -1 : (_3030 > _userdata_5887);
            }
            else{
                _3031 = compare(_3030, _userdata_5887);
            }
            _3030 = NOVALUE;
            if (_3031 <= 0)
            goto L11; // [302] 323

            /** 					idx += 1*/
            _idx_5890 = _idx_5890 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_5885);
            _3034 = (int)*(((s1_ptr)_2)->base + _a_5951);
            Ref(_3034);
            _2 = (int)SEQ_PTR(_dest_5889);
            _2 = (int)(((s1_ptr)_2)->base + _idx_5890);
            _1 = *(int *)_2;
            *(int *)_2 = _3034;
            if( _1 != _3034 ){
                DeRef(_1);
            }
            _3034 = NOVALUE;
L11: 

            /** 			end for*/
            _a_5951 = _a_5951 + 1;
            goto LF; // [325] 292
L10: 
            ;
        }
        goto L5; // [330] 1304

        /** 		case ">=", "ge" then*/
        case 12:
        case 13:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_5885)){
                _3037 = SEQ_PTR(_source_5885)->length;
        }
        else {
            _3037 = 1;
        }
        {
            int _a_5963;
            _a_5963 = 1;
L12: 
            if (_a_5963 > _3037){
                goto L13; // [343] 388
            }

            /** 				if compare(source[a], userdata) >= 0 then*/
            _2 = (int)SEQ_PTR(_source_5885);
            _3038 = (int)*(((s1_ptr)_2)->base + _a_5963);
            if (IS_ATOM_INT(_3038) && IS_ATOM_INT(_userdata_5887)){
                _3039 = (_3038 < _userdata_5887) ? -1 : (_3038 > _userdata_5887);
            }
            else{
                _3039 = compare(_3038, _userdata_5887);
            }
            _3038 = NOVALUE;
            if (_3039 < 0)
            goto L14; // [360] 381

            /** 					idx += 1*/
            _idx_5890 = _idx_5890 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_5885);
            _3042 = (int)*(((s1_ptr)_2)->base + _a_5963);
            Ref(_3042);
            _2 = (int)SEQ_PTR(_dest_5889);
            _2 = (int)(((s1_ptr)_2)->base + _idx_5890);
            _1 = *(int *)_2;
            *(int *)_2 = _3042;
            if( _1 != _3042 ){
                DeRef(_1);
            }
            _3042 = NOVALUE;
L14: 

            /** 			end for*/
            _a_5963 = _a_5963 + 1;
            goto L12; // [383] 350
L13: 
            ;
        }
        goto L5; // [388] 1304

        /** 		case "in" then*/
        case 14:

        /** 			switch rangetype do*/
        _1 = find(_rangetype_5888, _3044);
        switch ( _1 ){ 

            /** 				case "" then*/
            case 1:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_5885)){
                    _3046 = SEQ_PTR(_source_5885)->length;
            }
            else {
                _3046 = 1;
            }
            {
                int _a_5977;
                _a_5977 = 1;
L15: 
                if (_a_5977 > _3046){
                    goto L16; // [410] 455
                }

                /** 						if find(source[a], userdata)  then*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3047 = (int)*(((s1_ptr)_2)->base + _a_5977);
                _3048 = find_from(_3047, _userdata_5887, 1);
                _3047 = NOVALUE;
                if (_3048 == 0)
                {
                    _3048 = NOVALUE;
                    goto L17; // [428] 448
                }
                else{
                    _3048 = NOVALUE;
                }

                /** 							idx += 1*/
                _idx_5890 = _idx_5890 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3050 = (int)*(((s1_ptr)_2)->base + _a_5977);
                Ref(_3050);
                _2 = (int)SEQ_PTR(_dest_5889);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5890);
                _1 = *(int *)_2;
                *(int *)_2 = _3050;
                if( _1 != _3050 ){
                    DeRef(_1);
                }
                _3050 = NOVALUE;
L17: 

                /** 					end for*/
                _a_5977 = _a_5977 + 1;
                goto L15; // [450] 417
L16: 
                ;
            }
            goto L5; // [455] 1304

            /** 				case "[]" then*/
            case 2:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_5885)){
                    _3051 = SEQ_PTR(_source_5885)->length;
            }
            else {
                _3051 = 1;
            }
            {
                int _a_5986;
                _a_5986 = 1;
L18: 
                if (_a_5986 > _3051){
                    goto L19; // [466] 534
                }

                /** 						if compare(source[a], userdata[1]) >= 0 then*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3052 = (int)*(((s1_ptr)_2)->base + _a_5986);
                _2 = (int)SEQ_PTR(_userdata_5887);
                _3053 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_3052) && IS_ATOM_INT(_3053)){
                    _3054 = (_3052 < _3053) ? -1 : (_3052 > _3053);
                }
                else{
                    _3054 = compare(_3052, _3053);
                }
                _3052 = NOVALUE;
                _3053 = NOVALUE;
                if (_3054 < 0)
                goto L1A; // [487] 527

                /** 							if compare(source[a], userdata[2]) <= 0 then*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3056 = (int)*(((s1_ptr)_2)->base + _a_5986);
                _2 = (int)SEQ_PTR(_userdata_5887);
                _3057 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_3056) && IS_ATOM_INT(_3057)){
                    _3058 = (_3056 < _3057) ? -1 : (_3056 > _3057);
                }
                else{
                    _3058 = compare(_3056, _3057);
                }
                _3056 = NOVALUE;
                _3057 = NOVALUE;
                if (_3058 > 0)
                goto L1B; // [505] 526

                /** 								idx += 1*/
                _idx_5890 = _idx_5890 + 1;

                /** 								dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3061 = (int)*(((s1_ptr)_2)->base + _a_5986);
                Ref(_3061);
                _2 = (int)SEQ_PTR(_dest_5889);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5890);
                _1 = *(int *)_2;
                *(int *)_2 = _3061;
                if( _1 != _3061 ){
                    DeRef(_1);
                }
                _3061 = NOVALUE;
L1B: 
L1A: 

                /** 					end for*/
                _a_5986 = _a_5986 + 1;
                goto L18; // [529] 473
L19: 
                ;
            }
            goto L5; // [534] 1304

            /** 				case "[)" then*/
            case 3:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_5885)){
                    _3062 = SEQ_PTR(_source_5885)->length;
            }
            else {
                _3062 = 1;
            }
            {
                int _a_6002;
                _a_6002 = 1;
L1C: 
                if (_a_6002 > _3062){
                    goto L1D; // [545] 613
                }

                /** 						if compare(source[a], userdata[1]) >= 0 then*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3063 = (int)*(((s1_ptr)_2)->base + _a_6002);
                _2 = (int)SEQ_PTR(_userdata_5887);
                _3064 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_3063) && IS_ATOM_INT(_3064)){
                    _3065 = (_3063 < _3064) ? -1 : (_3063 > _3064);
                }
                else{
                    _3065 = compare(_3063, _3064);
                }
                _3063 = NOVALUE;
                _3064 = NOVALUE;
                if (_3065 < 0)
                goto L1E; // [566] 606

                /** 							if compare(source[a], userdata[2]) < 0 then*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3067 = (int)*(((s1_ptr)_2)->base + _a_6002);
                _2 = (int)SEQ_PTR(_userdata_5887);
                _3068 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_3067) && IS_ATOM_INT(_3068)){
                    _3069 = (_3067 < _3068) ? -1 : (_3067 > _3068);
                }
                else{
                    _3069 = compare(_3067, _3068);
                }
                _3067 = NOVALUE;
                _3068 = NOVALUE;
                if (_3069 >= 0)
                goto L1F; // [584] 605

                /** 								idx += 1*/
                _idx_5890 = _idx_5890 + 1;

                /** 								dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3072 = (int)*(((s1_ptr)_2)->base + _a_6002);
                Ref(_3072);
                _2 = (int)SEQ_PTR(_dest_5889);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5890);
                _1 = *(int *)_2;
                *(int *)_2 = _3072;
                if( _1 != _3072 ){
                    DeRef(_1);
                }
                _3072 = NOVALUE;
L1F: 
L1E: 

                /** 					end for*/
                _a_6002 = _a_6002 + 1;
                goto L1C; // [608] 552
L1D: 
                ;
            }
            goto L5; // [613] 1304

            /** 				case "(]" then*/
            case 4:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_5885)){
                    _3073 = SEQ_PTR(_source_5885)->length;
            }
            else {
                _3073 = 1;
            }
            {
                int _a_6018;
                _a_6018 = 1;
L20: 
                if (_a_6018 > _3073){
                    goto L21; // [624] 692
                }

                /** 						if compare(source[a], userdata[1]) > 0 then*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3074 = (int)*(((s1_ptr)_2)->base + _a_6018);
                _2 = (int)SEQ_PTR(_userdata_5887);
                _3075 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_3074) && IS_ATOM_INT(_3075)){
                    _3076 = (_3074 < _3075) ? -1 : (_3074 > _3075);
                }
                else{
                    _3076 = compare(_3074, _3075);
                }
                _3074 = NOVALUE;
                _3075 = NOVALUE;
                if (_3076 <= 0)
                goto L22; // [645] 685

                /** 							if compare(source[a], userdata[2]) <= 0 then*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3078 = (int)*(((s1_ptr)_2)->base + _a_6018);
                _2 = (int)SEQ_PTR(_userdata_5887);
                _3079 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_3078) && IS_ATOM_INT(_3079)){
                    _3080 = (_3078 < _3079) ? -1 : (_3078 > _3079);
                }
                else{
                    _3080 = compare(_3078, _3079);
                }
                _3078 = NOVALUE;
                _3079 = NOVALUE;
                if (_3080 > 0)
                goto L23; // [663] 684

                /** 								idx += 1*/
                _idx_5890 = _idx_5890 + 1;

                /** 								dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3083 = (int)*(((s1_ptr)_2)->base + _a_6018);
                Ref(_3083);
                _2 = (int)SEQ_PTR(_dest_5889);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5890);
                _1 = *(int *)_2;
                *(int *)_2 = _3083;
                if( _1 != _3083 ){
                    DeRef(_1);
                }
                _3083 = NOVALUE;
L23: 
L22: 

                /** 					end for*/
                _a_6018 = _a_6018 + 1;
                goto L20; // [687] 631
L21: 
                ;
            }
            goto L5; // [692] 1304

            /** 				case "()" then*/
            case 5:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_5885)){
                    _3084 = SEQ_PTR(_source_5885)->length;
            }
            else {
                _3084 = 1;
            }
            {
                int _a_6034;
                _a_6034 = 1;
L24: 
                if (_a_6034 > _3084){
                    goto L25; // [703] 771
                }

                /** 						if compare(source[a], userdata[1]) > 0 then*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3085 = (int)*(((s1_ptr)_2)->base + _a_6034);
                _2 = (int)SEQ_PTR(_userdata_5887);
                _3086 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_3085) && IS_ATOM_INT(_3086)){
                    _3087 = (_3085 < _3086) ? -1 : (_3085 > _3086);
                }
                else{
                    _3087 = compare(_3085, _3086);
                }
                _3085 = NOVALUE;
                _3086 = NOVALUE;
                if (_3087 <= 0)
                goto L26; // [724] 764

                /** 							if compare(source[a], userdata[2]) < 0 then*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3089 = (int)*(((s1_ptr)_2)->base + _a_6034);
                _2 = (int)SEQ_PTR(_userdata_5887);
                _3090 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_3089) && IS_ATOM_INT(_3090)){
                    _3091 = (_3089 < _3090) ? -1 : (_3089 > _3090);
                }
                else{
                    _3091 = compare(_3089, _3090);
                }
                _3089 = NOVALUE;
                _3090 = NOVALUE;
                if (_3091 >= 0)
                goto L27; // [742] 763

                /** 								idx += 1*/
                _idx_5890 = _idx_5890 + 1;

                /** 								dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3094 = (int)*(((s1_ptr)_2)->base + _a_6034);
                Ref(_3094);
                _2 = (int)SEQ_PTR(_dest_5889);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5890);
                _1 = *(int *)_2;
                *(int *)_2 = _3094;
                if( _1 != _3094 ){
                    DeRef(_1);
                }
                _3094 = NOVALUE;
L27: 
L26: 

                /** 					end for*/
                _a_6034 = _a_6034 + 1;
                goto L24; // [766] 710
L25: 
                ;
            }
            goto L5; // [771] 1304

            /** 				case else*/
            case 0:
        ;}        goto L5; // [778] 1304

        /** 		case "out" then*/
        case 15:

        /** 			switch rangetype do*/
        _1 = find(_rangetype_5888, _3096);
        switch ( _1 ){ 

            /** 				case "" then*/
            case 1:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_5885)){
                    _3098 = SEQ_PTR(_source_5885)->length;
            }
            else {
                _3098 = 1;
            }
            {
                int _a_6055;
                _a_6055 = 1;
L28: 
                if (_a_6055 > _3098){
                    goto L29; // [800] 845
                }

                /** 						if not find(source[a], userdata)  then*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3099 = (int)*(((s1_ptr)_2)->base + _a_6055);
                _3100 = find_from(_3099, _userdata_5887, 1);
                _3099 = NOVALUE;
                if (_3100 != 0)
                goto L2A; // [818] 838
                _3100 = NOVALUE;

                /** 							idx += 1*/
                _idx_5890 = _idx_5890 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3103 = (int)*(((s1_ptr)_2)->base + _a_6055);
                Ref(_3103);
                _2 = (int)SEQ_PTR(_dest_5889);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5890);
                _1 = *(int *)_2;
                *(int *)_2 = _3103;
                if( _1 != _3103 ){
                    DeRef(_1);
                }
                _3103 = NOVALUE;
L2A: 

                /** 					end for*/
                _a_6055 = _a_6055 + 1;
                goto L28; // [840] 807
L29: 
                ;
            }
            goto L5; // [845] 1304

            /** 				case "[]" then*/
            case 2:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_5885)){
                    _3104 = SEQ_PTR(_source_5885)->length;
            }
            else {
                _3104 = 1;
            }
            {
                int _a_6065;
                _a_6065 = 1;
L2B: 
                if (_a_6065 > _3104){
                    goto L2C; // [856] 943
                }

                /** 						if compare(source[a], userdata[1]) < 0 then*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3105 = (int)*(((s1_ptr)_2)->base + _a_6065);
                _2 = (int)SEQ_PTR(_userdata_5887);
                _3106 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_3105) && IS_ATOM_INT(_3106)){
                    _3107 = (_3105 < _3106) ? -1 : (_3105 > _3106);
                }
                else{
                    _3107 = compare(_3105, _3106);
                }
                _3105 = NOVALUE;
                _3106 = NOVALUE;
                if (_3107 >= 0)
                goto L2D; // [877] 900

                /** 							idx += 1*/
                _idx_5890 = _idx_5890 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3110 = (int)*(((s1_ptr)_2)->base + _a_6065);
                Ref(_3110);
                _2 = (int)SEQ_PTR(_dest_5889);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5890);
                _1 = *(int *)_2;
                *(int *)_2 = _3110;
                if( _1 != _3110 ){
                    DeRef(_1);
                }
                _3110 = NOVALUE;
                goto L2E; // [897] 936
L2D: 

                /** 						elsif compare(source[a], userdata[2]) > 0 then*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3111 = (int)*(((s1_ptr)_2)->base + _a_6065);
                _2 = (int)SEQ_PTR(_userdata_5887);
                _3112 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_3111) && IS_ATOM_INT(_3112)){
                    _3113 = (_3111 < _3112) ? -1 : (_3111 > _3112);
                }
                else{
                    _3113 = compare(_3111, _3112);
                }
                _3111 = NOVALUE;
                _3112 = NOVALUE;
                if (_3113 <= 0)
                goto L2F; // [914] 935

                /** 							idx += 1*/
                _idx_5890 = _idx_5890 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3116 = (int)*(((s1_ptr)_2)->base + _a_6065);
                Ref(_3116);
                _2 = (int)SEQ_PTR(_dest_5889);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5890);
                _1 = *(int *)_2;
                *(int *)_2 = _3116;
                if( _1 != _3116 ){
                    DeRef(_1);
                }
                _3116 = NOVALUE;
L2F: 
L2E: 

                /** 					end for*/
                _a_6065 = _a_6065 + 1;
                goto L2B; // [938] 863
L2C: 
                ;
            }
            goto L5; // [943] 1304

            /** 				case "[)" then*/
            case 3:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_5885)){
                    _3117 = SEQ_PTR(_source_5885)->length;
            }
            else {
                _3117 = 1;
            }
            {
                int _a_6083;
                _a_6083 = 1;
L30: 
                if (_a_6083 > _3117){
                    goto L31; // [954] 1041
                }

                /** 						if compare(source[a], userdata[1]) < 0 then*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3118 = (int)*(((s1_ptr)_2)->base + _a_6083);
                _2 = (int)SEQ_PTR(_userdata_5887);
                _3119 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_3118) && IS_ATOM_INT(_3119)){
                    _3120 = (_3118 < _3119) ? -1 : (_3118 > _3119);
                }
                else{
                    _3120 = compare(_3118, _3119);
                }
                _3118 = NOVALUE;
                _3119 = NOVALUE;
                if (_3120 >= 0)
                goto L32; // [975] 998

                /** 							idx += 1*/
                _idx_5890 = _idx_5890 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3123 = (int)*(((s1_ptr)_2)->base + _a_6083);
                Ref(_3123);
                _2 = (int)SEQ_PTR(_dest_5889);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5890);
                _1 = *(int *)_2;
                *(int *)_2 = _3123;
                if( _1 != _3123 ){
                    DeRef(_1);
                }
                _3123 = NOVALUE;
                goto L33; // [995] 1034
L32: 

                /** 						elsif compare(source[a], userdata[2]) >= 0 then*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3124 = (int)*(((s1_ptr)_2)->base + _a_6083);
                _2 = (int)SEQ_PTR(_userdata_5887);
                _3125 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_3124) && IS_ATOM_INT(_3125)){
                    _3126 = (_3124 < _3125) ? -1 : (_3124 > _3125);
                }
                else{
                    _3126 = compare(_3124, _3125);
                }
                _3124 = NOVALUE;
                _3125 = NOVALUE;
                if (_3126 < 0)
                goto L34; // [1012] 1033

                /** 							idx += 1*/
                _idx_5890 = _idx_5890 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3129 = (int)*(((s1_ptr)_2)->base + _a_6083);
                Ref(_3129);
                _2 = (int)SEQ_PTR(_dest_5889);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5890);
                _1 = *(int *)_2;
                *(int *)_2 = _3129;
                if( _1 != _3129 ){
                    DeRef(_1);
                }
                _3129 = NOVALUE;
L34: 
L33: 

                /** 					end for*/
                _a_6083 = _a_6083 + 1;
                goto L30; // [1036] 961
L31: 
                ;
            }
            goto L5; // [1041] 1304

            /** 				case "(]" then*/
            case 4:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_5885)){
                    _3130 = SEQ_PTR(_source_5885)->length;
            }
            else {
                _3130 = 1;
            }
            {
                int _a_6101;
                _a_6101 = 1;
L35: 
                if (_a_6101 > _3130){
                    goto L36; // [1052] 1139
                }

                /** 						if compare(source[a], userdata[1]) <= 0 then*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3131 = (int)*(((s1_ptr)_2)->base + _a_6101);
                _2 = (int)SEQ_PTR(_userdata_5887);
                _3132 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_3131) && IS_ATOM_INT(_3132)){
                    _3133 = (_3131 < _3132) ? -1 : (_3131 > _3132);
                }
                else{
                    _3133 = compare(_3131, _3132);
                }
                _3131 = NOVALUE;
                _3132 = NOVALUE;
                if (_3133 > 0)
                goto L37; // [1073] 1096

                /** 							idx += 1*/
                _idx_5890 = _idx_5890 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3136 = (int)*(((s1_ptr)_2)->base + _a_6101);
                Ref(_3136);
                _2 = (int)SEQ_PTR(_dest_5889);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5890);
                _1 = *(int *)_2;
                *(int *)_2 = _3136;
                if( _1 != _3136 ){
                    DeRef(_1);
                }
                _3136 = NOVALUE;
                goto L38; // [1093] 1132
L37: 

                /** 						elsif compare(source[a], userdata[2]) > 0 then*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3137 = (int)*(((s1_ptr)_2)->base + _a_6101);
                _2 = (int)SEQ_PTR(_userdata_5887);
                _3138 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_3137) && IS_ATOM_INT(_3138)){
                    _3139 = (_3137 < _3138) ? -1 : (_3137 > _3138);
                }
                else{
                    _3139 = compare(_3137, _3138);
                }
                _3137 = NOVALUE;
                _3138 = NOVALUE;
                if (_3139 <= 0)
                goto L39; // [1110] 1131

                /** 							idx += 1*/
                _idx_5890 = _idx_5890 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3142 = (int)*(((s1_ptr)_2)->base + _a_6101);
                Ref(_3142);
                _2 = (int)SEQ_PTR(_dest_5889);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5890);
                _1 = *(int *)_2;
                *(int *)_2 = _3142;
                if( _1 != _3142 ){
                    DeRef(_1);
                }
                _3142 = NOVALUE;
L39: 
L38: 

                /** 					end for*/
                _a_6101 = _a_6101 + 1;
                goto L35; // [1134] 1059
L36: 
                ;
            }
            goto L5; // [1139] 1304

            /** 				case "()" then*/
            case 5:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_5885)){
                    _3143 = SEQ_PTR(_source_5885)->length;
            }
            else {
                _3143 = 1;
            }
            {
                int _a_6119;
                _a_6119 = 1;
L3A: 
                if (_a_6119 > _3143){
                    goto L3B; // [1150] 1237
                }

                /** 						if compare(source[a], userdata[1]) <= 0 then*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3144 = (int)*(((s1_ptr)_2)->base + _a_6119);
                _2 = (int)SEQ_PTR(_userdata_5887);
                _3145 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_3144) && IS_ATOM_INT(_3145)){
                    _3146 = (_3144 < _3145) ? -1 : (_3144 > _3145);
                }
                else{
                    _3146 = compare(_3144, _3145);
                }
                _3144 = NOVALUE;
                _3145 = NOVALUE;
                if (_3146 > 0)
                goto L3C; // [1171] 1194

                /** 							idx += 1*/
                _idx_5890 = _idx_5890 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3149 = (int)*(((s1_ptr)_2)->base + _a_6119);
                Ref(_3149);
                _2 = (int)SEQ_PTR(_dest_5889);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5890);
                _1 = *(int *)_2;
                *(int *)_2 = _3149;
                if( _1 != _3149 ){
                    DeRef(_1);
                }
                _3149 = NOVALUE;
                goto L3D; // [1191] 1230
L3C: 

                /** 						elsif compare(source[a], userdata[2]) >= 0 then*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3150 = (int)*(((s1_ptr)_2)->base + _a_6119);
                _2 = (int)SEQ_PTR(_userdata_5887);
                _3151 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_3150) && IS_ATOM_INT(_3151)){
                    _3152 = (_3150 < _3151) ? -1 : (_3150 > _3151);
                }
                else{
                    _3152 = compare(_3150, _3151);
                }
                _3150 = NOVALUE;
                _3151 = NOVALUE;
                if (_3152 < 0)
                goto L3E; // [1208] 1229

                /** 							idx += 1*/
                _idx_5890 = _idx_5890 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5885);
                _3155 = (int)*(((s1_ptr)_2)->base + _a_6119);
                Ref(_3155);
                _2 = (int)SEQ_PTR(_dest_5889);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5890);
                _1 = *(int *)_2;
                *(int *)_2 = _3155;
                if( _1 != _3155 ){
                    DeRef(_1);
                }
                _3155 = NOVALUE;
L3E: 
L3D: 

                /** 					end for*/
                _a_6119 = _a_6119 + 1;
                goto L3A; // [1232] 1157
L3B: 
                ;
            }
            goto L5; // [1237] 1304

            /** 				case else*/
            case 0:
        ;}        goto L5; // [1244] 1304

        /** 		case else*/
        case 0:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_5885)){
                _3156 = SEQ_PTR(_source_5885)->length;
        }
        else {
            _3156 = 1;
        }
        {
            int _a_6138;
            _a_6138 = 1;
L3F: 
            if (_a_6138 > _3156){
                goto L40; // [1255] 1303
            }

            /** 				if call_func(rid, {source[a], userdata}) then*/
            _2 = (int)SEQ_PTR(_source_5885);
            _3157 = (int)*(((s1_ptr)_2)->base + _a_6138);
            Ref(_userdata_5887);
            Ref(_3157);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _3157;
            ((int *)_2)[2] = _userdata_5887;
            _3158 = MAKE_SEQ(_1);
            _3157 = NOVALUE;
            _1 = (int)SEQ_PTR(_3158);
            _2 = (int)((s1_ptr)_1)->base;
            _0 = (int)_00[_rid_5886].addr;
            Ref(*(int *)(_2+4));
            Ref(*(int *)(_2+8));
            _1 = (*(int (*)())_0)(
                                *(int *)(_2+4), 
                                *(int *)(_2+8)
                                 );
            DeRef(_3159);
            _3159 = _1;
            DeRefDS(_3158);
            _3158 = NOVALUE;
            if (_3159 == 0) {
                DeRef(_3159);
                _3159 = NOVALUE;
                goto L41; // [1276] 1296
            }
            else {
                if (!IS_ATOM_INT(_3159) && DBL_PTR(_3159)->dbl == 0.0){
                    DeRef(_3159);
                    _3159 = NOVALUE;
                    goto L41; // [1276] 1296
                }
                DeRef(_3159);
                _3159 = NOVALUE;
            }
            DeRef(_3159);
            _3159 = NOVALUE;

            /** 					idx += 1*/
            _idx_5890 = _idx_5890 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_5885);
            _3161 = (int)*(((s1_ptr)_2)->base + _a_6138);
            Ref(_3161);
            _2 = (int)SEQ_PTR(_dest_5889);
            _2 = (int)(((s1_ptr)_2)->base + _idx_5890);
            _1 = *(int *)_2;
            *(int *)_2 = _3161;
            if( _1 != _3161 ){
                DeRef(_1);
            }
            _3161 = NOVALUE;
L41: 

            /** 			end for*/
            _a_6138 = _a_6138 + 1;
            goto L3F; // [1298] 1262
L40: 
            ;
        }
    ;}L5: 

    /** 	return dest[1..idx]*/
    rhs_slice_target = (object_ptr)&_3162;
    RHS_Slice(_dest_5889, 1, _idx_5890);
    DeRefDS(_source_5885);
    DeRef(_userdata_5887);
    DeRef(_rangetype_5888);
    DeRefDS(_dest_5889);
    return _3162;
    ;
}


int _21filter_alpha(int _elem_6150, int _ud_6151)
{
    int _3163 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return t_alpha(elem)*/
    Ref(_elem_6150);
    _3163 = _5t_alpha(_elem_6150);
    DeRef(_elem_6150);
    return _3163;
    ;
}


int _21split(int _st_6195, int _delim_6196, int _no_empty_6197, int _limit_6198)
{
    int _ret_6199 = NOVALUE;
    int _start_6200 = NOVALUE;
    int _pos_6201 = NOVALUE;
    int _k_6253 = NOVALUE;
    int _3231 = NOVALUE;
    int _3229 = NOVALUE;
    int _3228 = NOVALUE;
    int _3224 = NOVALUE;
    int _3223 = NOVALUE;
    int _3222 = NOVALUE;
    int _3219 = NOVALUE;
    int _3218 = NOVALUE;
    int _3213 = NOVALUE;
    int _3212 = NOVALUE;
    int _3208 = NOVALUE;
    int _3204 = NOVALUE;
    int _3202 = NOVALUE;
    int _3201 = NOVALUE;
    int _3197 = NOVALUE;
    int _3195 = NOVALUE;
    int _3194 = NOVALUE;
    int _3193 = NOVALUE;
    int _3192 = NOVALUE;
    int _3189 = NOVALUE;
    int _3188 = NOVALUE;
    int _3187 = NOVALUE;
    int _3186 = NOVALUE;
    int _3185 = NOVALUE;
    int _3183 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence ret = {}*/
    RefDS(_5);
    DeRef(_ret_6199);
    _ret_6199 = _5;

    /** 	if length(st) = 0 then*/
    if (IS_SEQUENCE(_st_6195)){
            _3183 = SEQ_PTR(_st_6195)->length;
    }
    else {
        _3183 = 1;
    }
    if (_3183 != 0)
    goto L1; // [19] 30

    /** 		return ret*/
    DeRefDS(_st_6195);
    DeRefi(_delim_6196);
    return _ret_6199;
L1: 

    /** 	if sequence(delim) then*/
    _3185 = IS_SEQUENCE(_delim_6196);
    if (_3185 == 0)
    {
        _3185 = NOVALUE;
        goto L2; // [35] 211
    }
    else{
        _3185 = NOVALUE;
    }

    /** 		if equal(delim, "") then*/
    if (_delim_6196 == _5)
    _3186 = 1;
    else if (IS_ATOM_INT(_delim_6196) && IS_ATOM_INT(_5))
    _3186 = 0;
    else
    _3186 = (compare(_delim_6196, _5) == 0);
    if (_3186 == 0)
    {
        _3186 = NOVALUE;
        goto L3; // [44] 127
    }
    else{
        _3186 = NOVALUE;
    }

    /** 			for i = 1 to length(st) do*/
    if (IS_SEQUENCE(_st_6195)){
            _3187 = SEQ_PTR(_st_6195)->length;
    }
    else {
        _3187 = 1;
    }
    {
        int _i_6210;
        _i_6210 = 1;
L4: 
        if (_i_6210 > _3187){
            goto L5; // [52] 120
        }

        /** 				st[i] = {st[i]}*/
        _2 = (int)SEQ_PTR(_st_6195);
        _3188 = (int)*(((s1_ptr)_2)->base + _i_6210);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_3188);
        *((int *)(_2+4)) = _3188;
        _3189 = MAKE_SEQ(_1);
        _3188 = NOVALUE;
        _2 = (int)SEQ_PTR(_st_6195);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _st_6195 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_6210);
        _1 = *(int *)_2;
        *(int *)_2 = _3189;
        if( _1 != _3189 ){
            DeRef(_1);
        }
        _3189 = NOVALUE;

        /** 				limit -= 1*/
        _limit_6198 = _limit_6198 - 1;

        /** 				if limit = 0 then*/
        if (_limit_6198 != 0)
        goto L6; // [81] 113

        /** 					st = append(st[1 .. i],st[i+1 .. $])*/
        rhs_slice_target = (object_ptr)&_3192;
        RHS_Slice(_st_6195, 1, _i_6210);
        _3193 = _i_6210 + 1;
        if (IS_SEQUENCE(_st_6195)){
                _3194 = SEQ_PTR(_st_6195)->length;
        }
        else {
            _3194 = 1;
        }
        rhs_slice_target = (object_ptr)&_3195;
        RHS_Slice(_st_6195, _3193, _3194);
        RefDS(_3195);
        Append(&_st_6195, _3192, _3195);
        DeRefDS(_3192);
        _3192 = NOVALUE;
        DeRefDS(_3195);
        _3195 = NOVALUE;

        /** 					exit*/
        goto L5; // [110] 120
L6: 

        /** 			end for*/
        _i_6210 = _i_6210 + 1;
        goto L4; // [115] 59
L5: 
        ;
    }

    /** 			return st*/
    DeRefi(_delim_6196);
    DeRef(_ret_6199);
    DeRef(_3193);
    _3193 = NOVALUE;
    return _st_6195;
L3: 

    /** 		start = 1*/
    _start_6200 = 1;

    /** 		while start <= length(st) do*/
L7: 
    if (IS_SEQUENCE(_st_6195)){
            _3197 = SEQ_PTR(_st_6195)->length;
    }
    else {
        _3197 = 1;
    }
    if (_start_6200 > _3197)
    goto L8; // [140] 290

    /** 			pos = match(delim, st, start)*/
    _pos_6201 = e_match_from(_delim_6196, _st_6195, _start_6200);

    /** 			if pos = 0 then*/
    if (_pos_6201 != 0)
    goto L9; // [153] 162

    /** 				exit*/
    goto L8; // [159] 290
L9: 

    /** 			ret = append(ret, st[start..pos-1])*/
    _3201 = _pos_6201 - 1;
    rhs_slice_target = (object_ptr)&_3202;
    RHS_Slice(_st_6195, _start_6200, _3201);
    RefDS(_3202);
    Append(&_ret_6199, _ret_6199, _3202);
    DeRefDS(_3202);
    _3202 = NOVALUE;

    /** 			start = pos+length(delim)*/
    if (IS_SEQUENCE(_delim_6196)){
            _3204 = SEQ_PTR(_delim_6196)->length;
    }
    else {
        _3204 = 1;
    }
    _start_6200 = _pos_6201 + _3204;
    _3204 = NOVALUE;

    /** 			limit -= 1*/
    _limit_6198 = _limit_6198 - 1;

    /** 			if limit = 0 then*/
    if (_limit_6198 != 0)
    goto L7; // [194] 137

    /** 				exit*/
    goto L8; // [200] 290

    /** 		end while*/
    goto L7; // [205] 137
    goto L8; // [208] 290
L2: 

    /** 		start = 1*/
    _start_6200 = 1;

    /** 		while start <= length(st) do*/
LA: 
    if (IS_SEQUENCE(_st_6195)){
            _3208 = SEQ_PTR(_st_6195)->length;
    }
    else {
        _3208 = 1;
    }
    if (_start_6200 > _3208)
    goto LB; // [224] 289

    /** 			pos = find(delim, st, start)*/
    _pos_6201 = find_from(_delim_6196, _st_6195, _start_6200);

    /** 			if pos = 0 then*/
    if (_pos_6201 != 0)
    goto LC; // [237] 246

    /** 				exit*/
    goto LB; // [243] 289
LC: 

    /** 			ret = append(ret, st[start..pos-1])*/
    _3212 = _pos_6201 - 1;
    rhs_slice_target = (object_ptr)&_3213;
    RHS_Slice(_st_6195, _start_6200, _3212);
    RefDS(_3213);
    Append(&_ret_6199, _ret_6199, _3213);
    DeRefDS(_3213);
    _3213 = NOVALUE;

    /** 			start = pos + 1*/
    _start_6200 = _pos_6201 + 1;

    /** 			limit -= 1*/
    _limit_6198 = _limit_6198 - 1;

    /** 			if limit = 0 then*/
    if (_limit_6198 != 0)
    goto LA; // [275] 221

    /** 				exit*/
    goto LB; // [281] 289

    /** 		end while*/
    goto LA; // [286] 221
LB: 
L8: 

    /** 	ret = append(ret, st[start..$])*/
    if (IS_SEQUENCE(_st_6195)){
            _3218 = SEQ_PTR(_st_6195)->length;
    }
    else {
        _3218 = 1;
    }
    rhs_slice_target = (object_ptr)&_3219;
    RHS_Slice(_st_6195, _start_6200, _3218);
    RefDS(_3219);
    Append(&_ret_6199, _ret_6199, _3219);
    DeRefDS(_3219);
    _3219 = NOVALUE;

    /** 	integer k = length(ret)*/
    if (IS_SEQUENCE(_ret_6199)){
            _k_6253 = SEQ_PTR(_ret_6199)->length;
    }
    else {
        _k_6253 = 1;
    }

    /** 	if no_empty then*/
    if (_no_empty_6197 == 0)
    {
        goto LD; // [313] 378
    }
    else{
    }

    /** 		k = 0*/
    _k_6253 = 0;

    /** 		for i = 1 to length(ret) do*/
    if (IS_SEQUENCE(_ret_6199)){
            _3222 = SEQ_PTR(_ret_6199)->length;
    }
    else {
        _3222 = 1;
    }
    {
        int _i_6257;
        _i_6257 = 1;
LE: 
        if (_i_6257 > _3222){
            goto LF; // [326] 377
        }

        /** 			if length(ret[i]) != 0 then*/
        _2 = (int)SEQ_PTR(_ret_6199);
        _3223 = (int)*(((s1_ptr)_2)->base + _i_6257);
        if (IS_SEQUENCE(_3223)){
                _3224 = SEQ_PTR(_3223)->length;
        }
        else {
            _3224 = 1;
        }
        _3223 = NOVALUE;
        if (_3224 == 0)
        goto L10; // [342] 370

        /** 				k += 1*/
        _k_6253 = _k_6253 + 1;

        /** 				if k != i then*/
        if (_k_6253 == _i_6257)
        goto L11; // [354] 369

        /** 					ret[k] = ret[i]*/
        _2 = (int)SEQ_PTR(_ret_6199);
        _3228 = (int)*(((s1_ptr)_2)->base + _i_6257);
        Ref(_3228);
        _2 = (int)SEQ_PTR(_ret_6199);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _ret_6199 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _k_6253);
        _1 = *(int *)_2;
        *(int *)_2 = _3228;
        if( _1 != _3228 ){
            DeRef(_1);
        }
        _3228 = NOVALUE;
L11: 
L10: 

        /** 		end for*/
        _i_6257 = _i_6257 + 1;
        goto LE; // [372] 333
LF: 
        ;
    }
LD: 

    /** 	if k < length(ret) then*/
    if (IS_SEQUENCE(_ret_6199)){
            _3229 = SEQ_PTR(_ret_6199)->length;
    }
    else {
        _3229 = 1;
    }
    if (_k_6253 >= _3229)
    goto L12; // [383] 401

    /** 		return ret[1 .. k]*/
    rhs_slice_target = (object_ptr)&_3231;
    RHS_Slice(_ret_6199, 1, _k_6253);
    DeRefDS(_st_6195);
    DeRefi(_delim_6196);
    DeRefDS(_ret_6199);
    DeRef(_3201);
    _3201 = NOVALUE;
    DeRef(_3193);
    _3193 = NOVALUE;
    DeRef(_3212);
    _3212 = NOVALUE;
    _3223 = NOVALUE;
    return _3231;
    goto L13; // [398] 408
L12: 

    /** 		return ret*/
    DeRefDS(_st_6195);
    DeRefi(_delim_6196);
    DeRef(_3201);
    _3201 = NOVALUE;
    DeRef(_3193);
    _3193 = NOVALUE;
    DeRef(_3212);
    _3212 = NOVALUE;
    _3223 = NOVALUE;
    DeRef(_3231);
    _3231 = NOVALUE;
    return _ret_6199;
L13: 
    ;
}


int _21join(int _items_6322, int _delim_6323)
{
    int _ret_6325 = NOVALUE;
    int _3266 = NOVALUE;
    int _3265 = NOVALUE;
    int _3263 = NOVALUE;
    int _3262 = NOVALUE;
    int _3261 = NOVALUE;
    int _3260 = NOVALUE;
    int _3258 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length(items) then return {} end if*/
    if (IS_SEQUENCE(_items_6322)){
            _3258 = SEQ_PTR(_items_6322)->length;
    }
    else {
        _3258 = 1;
    }
    if (_3258 != 0)
    goto L1; // [8] 16
    _3258 = NOVALUE;
    RefDS(_5);
    DeRefDS(_items_6322);
    DeRef(_ret_6325);
    return _5;
L1: 

    /** 	ret = {}*/
    RefDS(_5);
    DeRef(_ret_6325);
    _ret_6325 = _5;

    /** 	for i=1 to length(items)-1 do*/
    if (IS_SEQUENCE(_items_6322)){
            _3260 = SEQ_PTR(_items_6322)->length;
    }
    else {
        _3260 = 1;
    }
    _3261 = _3260 - 1;
    _3260 = NOVALUE;
    {
        int _i_6330;
        _i_6330 = 1;
L2: 
        if (_i_6330 > _3261){
            goto L3; // [30] 58
        }

        /** 		ret &= items[i] & delim*/
        _2 = (int)SEQ_PTR(_items_6322);
        _3262 = (int)*(((s1_ptr)_2)->base + _i_6330);
        if (IS_SEQUENCE(_3262) && IS_ATOM(_delim_6323)) {
            Append(&_3263, _3262, _delim_6323);
        }
        else if (IS_ATOM(_3262) && IS_SEQUENCE(_delim_6323)) {
        }
        else {
            Concat((object_ptr)&_3263, _3262, _delim_6323);
            _3262 = NOVALUE;
        }
        _3262 = NOVALUE;
        if (IS_SEQUENCE(_ret_6325) && IS_ATOM(_3263)) {
        }
        else if (IS_ATOM(_ret_6325) && IS_SEQUENCE(_3263)) {
            Ref(_ret_6325);
            Prepend(&_ret_6325, _3263, _ret_6325);
        }
        else {
            Concat((object_ptr)&_ret_6325, _ret_6325, _3263);
        }
        DeRefDS(_3263);
        _3263 = NOVALUE;

        /** 	end for*/
        _i_6330 = _i_6330 + 1;
        goto L2; // [53] 37
L3: 
        ;
    }

    /** 	ret &= items[$]*/
    if (IS_SEQUENCE(_items_6322)){
            _3265 = SEQ_PTR(_items_6322)->length;
    }
    else {
        _3265 = 1;
    }
    _2 = (int)SEQ_PTR(_items_6322);
    _3266 = (int)*(((s1_ptr)_2)->base + _3265);
    if (IS_SEQUENCE(_ret_6325) && IS_ATOM(_3266)) {
        Ref(_3266);
        Append(&_ret_6325, _ret_6325, _3266);
    }
    else if (IS_ATOM(_ret_6325) && IS_SEQUENCE(_3266)) {
        Ref(_ret_6325);
        Prepend(&_ret_6325, _3266, _ret_6325);
    }
    else {
        Concat((object_ptr)&_ret_6325, _ret_6325, _3266);
    }
    _3266 = NOVALUE;

    /** 	return ret*/
    DeRefDS(_items_6322);
    DeRef(_3261);
    _3261 = NOVALUE;
    return _ret_6325;
    ;
}


int _21flatten(int _s_6433, int _delim_6434)
{
    int _ret_6435 = NOVALUE;
    int _x_6436 = NOVALUE;
    int _len_6437 = NOVALUE;
    int _pos_6438 = NOVALUE;
    int _temp_6457 = NOVALUE;
    int _3355 = NOVALUE;
    int _3354 = NOVALUE;
    int _3353 = NOVALUE;
    int _3351 = NOVALUE;
    int _3350 = NOVALUE;
    int _3349 = NOVALUE;
    int _3347 = NOVALUE;
    int _3345 = NOVALUE;
    int _3344 = NOVALUE;
    int _3343 = NOVALUE;
    int _3342 = NOVALUE;
    int _3340 = NOVALUE;
    int _3339 = NOVALUE;
    int _3338 = NOVALUE;
    int _3337 = NOVALUE;
    int _3336 = NOVALUE;
    int _3335 = NOVALUE;
    int _3334 = NOVALUE;
    int _3332 = NOVALUE;
    int _3331 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ret = s*/
    RefDS(_s_6433);
    DeRef(_ret_6435);
    _ret_6435 = _s_6433;

    /** 	pos = 1*/
    _pos_6438 = 1;

    /** 	len = length(ret)*/
    if (IS_SEQUENCE(_ret_6435)){
            _len_6437 = SEQ_PTR(_ret_6435)->length;
    }
    else {
        _len_6437 = 1;
    }

    /** 	while pos <= len do*/
L1: 
    if (_pos_6438 > _len_6437)
    goto L2; // [25] 189

    /** 		x = ret[pos]*/
    DeRef(_x_6436);
    _2 = (int)SEQ_PTR(_ret_6435);
    _x_6436 = (int)*(((s1_ptr)_2)->base + _pos_6438);
    Ref(_x_6436);

    /** 		if sequence(x) then*/
    _3331 = IS_SEQUENCE(_x_6436);
    if (_3331 == 0)
    {
        _3331 = NOVALUE;
        goto L3; // [40] 177
    }
    else{
        _3331 = NOVALUE;
    }

    /** 			if length(delim) = 0 then*/
    if (IS_SEQUENCE(_delim_6434)){
            _3332 = SEQ_PTR(_delim_6434)->length;
    }
    else {
        _3332 = 1;
    }
    if (_3332 != 0)
    goto L4; // [48] 92

    /** 				ret = ret[1..pos-1] & flatten(x) & ret[pos+1 .. $]*/
    _3334 = _pos_6438 - 1;
    rhs_slice_target = (object_ptr)&_3335;
    RHS_Slice(_ret_6435, 1, _3334);
    Ref(_x_6436);
    DeRef(_3336);
    _3336 = _x_6436;
    RefDS(_5);
    _3337 = _21flatten(_3336, _5);
    _3336 = NOVALUE;
    _3338 = _pos_6438 + 1;
    if (_3338 > MAXINT){
        _3338 = NewDouble((double)_3338);
    }
    if (IS_SEQUENCE(_ret_6435)){
            _3339 = SEQ_PTR(_ret_6435)->length;
    }
    else {
        _3339 = 1;
    }
    rhs_slice_target = (object_ptr)&_3340;
    RHS_Slice(_ret_6435, _3338, _3339);
    {
        int concat_list[3];

        concat_list[0] = _3340;
        concat_list[1] = _3337;
        concat_list[2] = _3335;
        Concat_N((object_ptr)&_ret_6435, concat_list, 3);
    }
    DeRefDS(_3340);
    _3340 = NOVALUE;
    DeRef(_3337);
    _3337 = NOVALUE;
    DeRefDS(_3335);
    _3335 = NOVALUE;
    goto L5; // [89] 169
L4: 

    /** 				sequence temp = ret[1..pos-1] & flatten(x)*/
    _3342 = _pos_6438 - 1;
    rhs_slice_target = (object_ptr)&_3343;
    RHS_Slice(_ret_6435, 1, _3342);
    Ref(_x_6436);
    DeRef(_3344);
    _3344 = _x_6436;
    RefDS(_5);
    _3345 = _21flatten(_3344, _5);
    _3344 = NOVALUE;
    if (IS_SEQUENCE(_3343) && IS_ATOM(_3345)) {
        Ref(_3345);
        Append(&_temp_6457, _3343, _3345);
    }
    else if (IS_ATOM(_3343) && IS_SEQUENCE(_3345)) {
    }
    else {
        Concat((object_ptr)&_temp_6457, _3343, _3345);
        DeRefDS(_3343);
        _3343 = NOVALUE;
    }
    DeRef(_3343);
    _3343 = NOVALUE;
    DeRef(_3345);
    _3345 = NOVALUE;

    /** 				if pos != length(ret) then*/
    if (IS_SEQUENCE(_ret_6435)){
            _3347 = SEQ_PTR(_ret_6435)->length;
    }
    else {
        _3347 = 1;
    }
    if (_pos_6438 == _3347)
    goto L6; // [120] 147

    /** 					ret = temp &  delim & ret[pos+1 .. $]*/
    _3349 = _pos_6438 + 1;
    if (_3349 > MAXINT){
        _3349 = NewDouble((double)_3349);
    }
    if (IS_SEQUENCE(_ret_6435)){
            _3350 = SEQ_PTR(_ret_6435)->length;
    }
    else {
        _3350 = 1;
    }
    rhs_slice_target = (object_ptr)&_3351;
    RHS_Slice(_ret_6435, _3349, _3350);
    {
        int concat_list[3];

        concat_list[0] = _3351;
        concat_list[1] = _delim_6434;
        concat_list[2] = _temp_6457;
        Concat_N((object_ptr)&_ret_6435, concat_list, 3);
    }
    DeRefDS(_3351);
    _3351 = NOVALUE;
    goto L7; // [144] 166
L6: 

    /** 					ret = temp & ret[pos+1 .. $]*/
    _3353 = _pos_6438 + 1;
    if (_3353 > MAXINT){
        _3353 = NewDouble((double)_3353);
    }
    if (IS_SEQUENCE(_ret_6435)){
            _3354 = SEQ_PTR(_ret_6435)->length;
    }
    else {
        _3354 = 1;
    }
    rhs_slice_target = (object_ptr)&_3355;
    RHS_Slice(_ret_6435, _3353, _3354);
    Concat((object_ptr)&_ret_6435, _temp_6457, _3355);
    DeRefDS(_3355);
    _3355 = NOVALUE;
L7: 
    DeRef(_temp_6457);
    _temp_6457 = NOVALUE;
L5: 

    /** 			len = length(ret)*/
    if (IS_SEQUENCE(_ret_6435)){
            _len_6437 = SEQ_PTR(_ret_6435)->length;
    }
    else {
        _len_6437 = 1;
    }
    goto L1; // [174] 25
L3: 

    /** 			pos += 1*/
    _pos_6438 = _pos_6438 + 1;

    /** 	end while*/
    goto L1; // [186] 25
L2: 

    /** 	return ret*/
    DeRefDS(_s_6433);
    DeRefi(_delim_6434);
    DeRef(_x_6436);
    DeRef(_3334);
    _3334 = NOVALUE;
    DeRef(_3342);
    _3342 = NOVALUE;
    DeRef(_3349);
    _3349 = NOVALUE;
    DeRef(_3338);
    _3338 = NOVALUE;
    DeRef(_3353);
    _3353 = NOVALUE;
    return _ret_6435;
    ;
}


int _21remove_subseq(int _source_list_6774, int _alt_value_6775)
{
    int _lResult_6776 = NOVALUE;
    int _lCOW_6777 = NOVALUE;
    int _3563 = NOVALUE;
    int _3562 = NOVALUE;
    int _3558 = NOVALUE;
    int _3555 = NOVALUE;
    int _3552 = NOVALUE;
    int _3551 = NOVALUE;
    int _3550 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer lCOW = 0*/
    _lCOW_6777 = 0;

    /** 	for i = 1 to length(source_list) do*/
    if (IS_SEQUENCE(_source_list_6774)){
            _3550 = SEQ_PTR(_source_list_6774)->length;
    }
    else {
        _3550 = 1;
    }
    {
        int _i_6779;
        _i_6779 = 1;
L1: 
        if (_i_6779 > _3550){
            goto L2; // [13] 121
        }

        /** 		if atom(source_list[i]) then*/
        _2 = (int)SEQ_PTR(_source_list_6774);
        _3551 = (int)*(((s1_ptr)_2)->base + _i_6779);
        _3552 = IS_ATOM(_3551);
        _3551 = NOVALUE;
        if (_3552 == 0)
        {
            _3552 = NOVALUE;
            goto L3; // [29] 69
        }
        else{
            _3552 = NOVALUE;
        }

        /** 			if lCOW != 0 then*/
        if (_lCOW_6777 == 0)
        goto L4; // [34] 116

        /** 				if lCOW != i then*/
        if (_lCOW_6777 == _i_6779)
        goto L5; // [40] 57

        /** 					lResult[lCOW] = source_list[i]*/
        _2 = (int)SEQ_PTR(_source_list_6774);
        _3555 = (int)*(((s1_ptr)_2)->base + _i_6779);
        Ref(_3555);
        _2 = (int)SEQ_PTR(_lResult_6776);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _lResult_6776 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _lCOW_6777);
        _1 = *(int *)_2;
        *(int *)_2 = _3555;
        if( _1 != _3555 ){
            DeRef(_1);
        }
        _3555 = NOVALUE;
L5: 

        /** 				lCOW += 1*/
        _lCOW_6777 = _lCOW_6777 + 1;

        /** 			continue*/
        goto L4; // [66] 116
L3: 

        /** 		if lCOW = 0 then*/
        if (_lCOW_6777 != 0)
        goto L6; // [71] 88

        /** 			lResult = source_list*/
        RefDS(_source_list_6774);
        DeRef(_lResult_6776);
        _lResult_6776 = _source_list_6774;

        /** 			lCOW = i*/
        _lCOW_6777 = _i_6779;
L6: 

        /** 		if not equal(alt_value, SEQ_NOALT) then*/
        if (_alt_value_6775 == _21SEQ_NOALT_6768)
        _3558 = 1;
        else if (IS_ATOM_INT(_alt_value_6775) && IS_ATOM_INT(_21SEQ_NOALT_6768))
        _3558 = 0;
        else
        _3558 = (compare(_alt_value_6775, _21SEQ_NOALT_6768) == 0);
        if (_3558 != 0)
        goto L7; // [96] 114
        _3558 = NOVALUE;

        /** 			lResult[lCOW] = alt_value*/
        Ref(_alt_value_6775);
        _2 = (int)SEQ_PTR(_lResult_6776);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _lResult_6776 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _lCOW_6777);
        _1 = *(int *)_2;
        *(int *)_2 = _alt_value_6775;
        DeRef(_1);

        /** 			lCOW += 1*/
        _lCOW_6777 = _lCOW_6777 + 1;
L7: 

        /** 	end for*/
L4: 
        _i_6779 = _i_6779 + 1;
        goto L1; // [116] 20
L2: 
        ;
    }

    /** 	if lCOW = 0 then*/
    if (_lCOW_6777 != 0)
    goto L8; // [123] 134

    /** 		return source_list*/
    DeRef(_alt_value_6775);
    DeRef(_lResult_6776);
    return _source_list_6774;
L8: 

    /** 	return lResult[1.. lCOW - 1]*/
    _3562 = _lCOW_6777 - 1;
    rhs_slice_target = (object_ptr)&_3563;
    RHS_Slice(_lResult_6776, 1, _3562);
    DeRefDS(_source_list_6774);
    DeRef(_alt_value_6775);
    DeRefDS(_lResult_6776);
    _3562 = NOVALUE;
    return _3563;
    ;
}



// 0xE8EB9051
