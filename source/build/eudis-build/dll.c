// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _11open_dll(int _file_name_2217)
{
    int _fh_2227 = NOVALUE;
    int _978 = NOVALUE;
    int _976 = NOVALUE;
    int _975 = NOVALUE;
    int _974 = NOVALUE;
    int _973 = NOVALUE;
    int _972 = NOVALUE;
    int _971 = NOVALUE;
    int _970 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(file_name) > 0 and types:string(file_name) then*/
    if (IS_SEQUENCE(_file_name_2217)){
            _970 = SEQ_PTR(_file_name_2217)->length;
    }
    else {
        _970 = 1;
    }
    _971 = (_970 > 0);
    _970 = NOVALUE;
    if (_971 == 0) {
        goto L1; // [12] 35
    }
    RefDS(_file_name_2217);
    _973 = _5string(_file_name_2217);
    if (_973 == 0) {
        DeRef(_973);
        _973 = NOVALUE;
        goto L1; // [21] 35
    }
    else {
        if (!IS_ATOM_INT(_973) && DBL_PTR(_973)->dbl == 0.0){
            DeRef(_973);
            _973 = NOVALUE;
            goto L1; // [21] 35
        }
        DeRef(_973);
        _973 = NOVALUE;
    }
    DeRef(_973);
    _973 = NOVALUE;

    /** 		return machine_func(M_OPEN_DLL, file_name)*/
    _974 = machine(50, _file_name_2217);
    DeRefDS(_file_name_2217);
    DeRef(_971);
    _971 = NOVALUE;
    return _974;
L1: 

    /** 	for idx = 1 to length(file_name) do*/
    if (IS_SEQUENCE(_file_name_2217)){
            _975 = SEQ_PTR(_file_name_2217)->length;
    }
    else {
        _975 = 1;
    }
    {
        int _idx_2225;
        _idx_2225 = 1;
L2: 
        if (_idx_2225 > _975){
            goto L3; // [40] 82
        }

        /** 		atom fh = machine_func(M_OPEN_DLL, file_name[idx])*/
        _2 = (int)SEQ_PTR(_file_name_2217);
        _976 = (int)*(((s1_ptr)_2)->base + _idx_2225);
        DeRef(_fh_2227);
        _fh_2227 = machine(50, _976);
        _976 = NOVALUE;

        /** 		if not fh = 0 then*/
        if (IS_ATOM_INT(_fh_2227)) {
            _978 = (_fh_2227 == 0);
        }
        else {
            _978 = unary_op(NOT, _fh_2227);
        }
        if (_978 != 0)
        goto L4; // [62] 73

        /** 			return fh*/
        DeRefDS(_file_name_2217);
        DeRef(_971);
        _971 = NOVALUE;
        DeRef(_974);
        _974 = NOVALUE;
        _978 = NOVALUE;
        return _fh_2227;
L4: 
        DeRef(_fh_2227);
        _fh_2227 = NOVALUE;

        /** 	end for*/
        _idx_2225 = _idx_2225 + 1;
        goto L2; // [77] 47
L3: 
        ;
    }

    /** 	return 0*/
    DeRefDS(_file_name_2217);
    DeRef(_971);
    _971 = NOVALUE;
    DeRef(_974);
    _974 = NOVALUE;
    DeRef(_978);
    _978 = NOVALUE;
    return 0;
    ;
}


int _11define_c_proc(int _lib_2241, int _routine_name_2242, int _arg_types_2243)
{
    int _safe_address_inlined_safe_address_at_13_2248 = NOVALUE;
    int _msg_inlined_crash_at_28_2252 = NOVALUE;
    int _987 = NOVALUE;
    int _986 = NOVALUE;
    int _984 = NOVALUE;
    int _983 = NOVALUE;
    int _982 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(routine_name) and not machine:safe_address(routine_name, 1, machine:A_EXECUTE) then*/
    _982 = 0;
    if (_982 == 0) {
        goto L1; // [8] 48
    }

    /** 	return 1*/
    _safe_address_inlined_safe_address_at_13_2248 = 1;
    _984 = (1 == 0);
    if (_984 == 0)
    {
        DeRef(_984);
        _984 = NOVALUE;
        goto L1; // [24] 48
    }
    else{
        DeRef(_984);
        _984 = NOVALUE;
    }

    /**         error:crash("A C function is being defined from Non-executable memory.")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_28_2252);
    _msg_inlined_crash_at_28_2252 = EPrintf(-9999999, _985, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_28_2252);

    /** end procedure*/
    goto L2; // [42] 45
L2: 
    DeRefi(_msg_inlined_crash_at_28_2252);
    _msg_inlined_crash_at_28_2252 = NOVALUE;
L1: 

    /** 	return machine_func(M_DEFINE_C, {lib, routine_name, arg_types, 0})*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_lib_2241);
    *((int *)(_2+4)) = _lib_2241;
    Ref(_routine_name_2242);
    *((int *)(_2+8)) = _routine_name_2242;
    RefDS(_arg_types_2243);
    *((int *)(_2+12)) = _arg_types_2243;
    *((int *)(_2+16)) = 0;
    _986 = MAKE_SEQ(_1);
    _987 = machine(51, _986);
    DeRefDS(_986);
    _986 = NOVALUE;
    DeRef(_lib_2241);
    DeRef(_routine_name_2242);
    DeRefDSi(_arg_types_2243);
    return _987;
    ;
}


int _11define_c_func(int _lib_2257, int _routine_name_2258, int _arg_types_2259, int _return_type_2260)
{
    int _safe_address_inlined_safe_address_at_13_2265 = NOVALUE;
    int _msg_inlined_crash_at_28_2268 = NOVALUE;
    int _992 = NOVALUE;
    int _991 = NOVALUE;
    int _990 = NOVALUE;
    int _989 = NOVALUE;
    int _988 = NOVALUE;
    int _0, _1, _2;
    

    /** 	  if atom(routine_name) and not machine:safe_address(routine_name, 1, machine:A_EXECUTE) then*/
    _988 = 0;
    if (_988 == 0) {
        goto L1; // [8] 48
    }

    /** 	return 1*/
    _safe_address_inlined_safe_address_at_13_2265 = 1;
    _990 = (1 == 0);
    if (_990 == 0)
    {
        DeRef(_990);
        _990 = NOVALUE;
        goto L1; // [24] 48
    }
    else{
        DeRef(_990);
        _990 = NOVALUE;
    }

    /** 	      error:crash("A C function is being defined from Non-executable memory.")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_28_2268);
    _msg_inlined_crash_at_28_2268 = EPrintf(-9999999, _985, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_28_2268);

    /** end procedure*/
    goto L2; // [42] 45
L2: 
    DeRefi(_msg_inlined_crash_at_28_2268);
    _msg_inlined_crash_at_28_2268 = NOVALUE;
L1: 

    /** 	  return machine_func(M_DEFINE_C, {lib, routine_name, arg_types, return_type})*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_lib_2257);
    *((int *)(_2+4)) = _lib_2257;
    Ref(_routine_name_2258);
    *((int *)(_2+8)) = _routine_name_2258;
    RefDS(_arg_types_2259);
    *((int *)(_2+12)) = _arg_types_2259;
    *((int *)(_2+16)) = _return_type_2260;
    _991 = MAKE_SEQ(_1);
    _992 = machine(51, _991);
    DeRefDS(_991);
    _991 = NOVALUE;
    DeRef(_lib_2257);
    DeRefi(_routine_name_2258);
    DeRefDSi(_arg_types_2259);
    return _992;
    ;
}



// 0xB649CB73
