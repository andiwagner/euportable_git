// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _54find_file(int _fname_45233)
{
    int _24142 = NOVALUE;
    int _24141 = NOVALUE;
    int _24140 = NOVALUE;
    int _24139 = NOVALUE;
    int _24137 = NOVALUE;
    int _24136 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(inc_dirs) do*/
    _24136 = 6;
    {
        int _i_45235;
        _i_45235 = 1;
L1: 
        if (_i_45235 > 6){
            goto L2; // [10] 64
        }

        /** 		if file_exists(inc_dirs[i] & "/" & fname) then*/
        _2 = (int)SEQ_PTR(_54inc_dirs_45222);
        _24137 = (int)*(((s1_ptr)_2)->base + _i_45235);
        {
            int concat_list[3];

            concat_list[0] = _fname_45233;
            concat_list[1] = _24138;
            concat_list[2] = _24137;
            Concat_N((object_ptr)&_24139, concat_list, 3);
        }
        _24137 = NOVALUE;
        _24140 = _9file_exists(_24139);
        _24139 = NOVALUE;
        if (_24140 == 0) {
            DeRef(_24140);
            _24140 = NOVALUE;
            goto L3; // [35] 57
        }
        else {
            if (!IS_ATOM_INT(_24140) && DBL_PTR(_24140)->dbl == 0.0){
                DeRef(_24140);
                _24140 = NOVALUE;
                goto L3; // [35] 57
            }
            DeRef(_24140);
            _24140 = NOVALUE;
        }
        DeRef(_24140);
        _24140 = NOVALUE;

        /** 			return inc_dirs[i] & "/" & fname*/
        _2 = (int)SEQ_PTR(_54inc_dirs_45222);
        _24141 = (int)*(((s1_ptr)_2)->base + _i_45235);
        {
            int concat_list[3];

            concat_list[0] = _fname_45233;
            concat_list[1] = _24138;
            concat_list[2] = _24141;
            Concat_N((object_ptr)&_24142, concat_list, 3);
        }
        _24141 = NOVALUE;
        DeRefDS(_fname_45233);
        return _24142;
L3: 

        /** 	end for*/
        _i_45235 = _i_45235 + 1;
        goto L1; // [59] 17
L2: 
        ;
    }

    /** 	return 0*/
    DeRefDS(_fname_45233);
    DeRef(_24142);
    _24142 = NOVALUE;
    return 0;
    ;
}


int _54find_all_includes(int _fname_45247, int _includes_45248)
{
    int _lines_45249 = NOVALUE;
    int _m_45255 = NOVALUE;
    int _full_fname_45260 = NOVALUE;
    int _24156 = NOVALUE;
    int _24155 = NOVALUE;
    int _24153 = NOVALUE;
    int _24151 = NOVALUE;
    int _24150 = NOVALUE;
    int _24148 = NOVALUE;
    int _24147 = NOVALUE;
    int _24145 = NOVALUE;
    int _24144 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence lines = read_lines(fname)*/
    RefDS(_fname_45247);
    _0 = _lines_45249;
    _lines_45249 = _16read_lines(_fname_45247);
    DeRef(_0);

    /** 	for i = 1 to length(lines) do*/
    if (IS_SEQUENCE(_lines_45249)){
            _24144 = SEQ_PTR(_lines_45249)->length;
    }
    else {
        _24144 = 1;
    }
    {
        int _i_45253;
        _i_45253 = 1;
L1: 
        if (_i_45253 > _24144){
            goto L2; // [18] 116
        }

        /** 		object m = regex:matches(re_include, lines[i])*/
        _2 = (int)SEQ_PTR(_lines_45249);
        _24145 = (int)*(((s1_ptr)_2)->base + _i_45253);
        Ref(_54re_include_45219);
        Ref(_24145);
        _0 = _m_45255;
        _m_45255 = _50matches(_54re_include_45219, _24145, 1, 0);
        DeRef(_0);
        _24145 = NOVALUE;

        /** 		if sequence(m) then*/
        _24147 = IS_SEQUENCE(_m_45255);
        if (_24147 == 0)
        {
            _24147 = NOVALUE;
            goto L3; // [45] 105
        }
        else{
            _24147 = NOVALUE;
        }

        /** 			object full_fname = find_file(m[3])*/
        _2 = (int)SEQ_PTR(_m_45255);
        _24148 = (int)*(((s1_ptr)_2)->base + 3);
        Ref(_24148);
        _0 = _full_fname_45260;
        _full_fname_45260 = _54find_file(_24148);
        DeRef(_0);
        _24148 = NOVALUE;

        /** 			if sequence(full_fname) then*/
        _24150 = IS_SEQUENCE(_full_fname_45260);
        if (_24150 == 0)
        {
            _24150 = NOVALUE;
            goto L4; // [63] 104
        }
        else{
            _24150 = NOVALUE;
        }

        /** 				if eu:find(full_fname, includes) = 0 then*/
        _24151 = find_from(_full_fname_45260, _includes_45248, 1);
        if (_24151 != 0)
        goto L5; // [73] 103

        /** 					includes &= { full_fname }*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_full_fname_45260);
        *((int *)(_2+4)) = _full_fname_45260;
        _24153 = MAKE_SEQ(_1);
        Concat((object_ptr)&_includes_45248, _includes_45248, _24153);
        DeRefDS(_24153);
        _24153 = NOVALUE;

        /** 					includes = find_all_includes(full_fname, includes)*/
        Ref(_full_fname_45260);
        DeRef(_24155);
        _24155 = _full_fname_45260;
        RefDS(_includes_45248);
        DeRef(_24156);
        _24156 = _includes_45248;
        _0 = _includes_45248;
        _includes_45248 = _54find_all_includes(_24155, _24156);
        DeRefDS(_0);
        _24155 = NOVALUE;
        _24156 = NOVALUE;
L5: 
L4: 
L3: 
        DeRef(_full_fname_45260);
        _full_fname_45260 = NOVALUE;
        DeRef(_m_45255);
        _m_45255 = NOVALUE;

        /** 	end for*/
        _i_45253 = _i_45253 + 1;
        goto L1; // [111] 25
L2: 
        ;
    }

    /** 	return includes*/
    DeRefDS(_fname_45247);
    DeRef(_lines_45249);
    return _includes_45248;
    ;
}


int _54quick_has_changed(int _fname_45275)
{
    int _d1_45276 = NOVALUE;
    int _all_files_45286 = NOVALUE;
    int _d2_45292 = NOVALUE;
    int _diff_2__tmp_at88_45302 = NOVALUE;
    int _diff_1__tmp_at88_45301 = NOVALUE;
    int _diff_inlined_diff_at_88_45300 = NOVALUE;
    int _24168 = NOVALUE;
    int _24166 = NOVALUE;
    int _24165 = NOVALUE;
    int _24163 = NOVALUE;
    int _24162 = NOVALUE;
    int _24160 = NOVALUE;
    int _24158 = NOVALUE;
    int _0, _1, _2;
    

    /** 	object d1 = file_timestamp(output_dir & filebase(fname) & ".bld")*/
    RefDS(_fname_45275);
    _24158 = _9filebase(_fname_45275);
    {
        int concat_list[3];

        concat_list[0] = _24159;
        concat_list[1] = _24158;
        concat_list[2] = _56output_dir_42676;
        Concat_N((object_ptr)&_24160, concat_list, 3);
    }
    DeRef(_24158);
    _24158 = NOVALUE;
    _0 = _d1_45276;
    _d1_45276 = _9file_timestamp(_24160);
    DeRef(_0);
    _24160 = NOVALUE;

    /** 	if atom(d1) then*/
    _24162 = IS_ATOM(_d1_45276);
    if (_24162 == 0)
    {
        _24162 = NOVALUE;
        goto L1; // [26] 36
    }
    else{
        _24162 = NOVALUE;
    }

    /** 		return 1*/
    DeRefDS(_fname_45275);
    DeRef(_d1_45276);
    DeRef(_all_files_45286);
    return 1;
L1: 

    /** 	sequence all_files = append(find_all_includes(fname), fname)*/
    RefDS(_fname_45275);
    RefDS(_22682);
    _24163 = _54find_all_includes(_fname_45275, _22682);
    RefDS(_fname_45275);
    Append(&_all_files_45286, _24163, _fname_45275);
    DeRef(_24163);
    _24163 = NOVALUE;

    /** 	for i = 1 to length(all_files) do*/
    if (IS_SEQUENCE(_all_files_45286)){
            _24165 = SEQ_PTR(_all_files_45286)->length;
    }
    else {
        _24165 = 1;
    }
    {
        int _i_45290;
        _i_45290 = 1;
L2: 
        if (_i_45290 > _24165){
            goto L3; // [52] 123
        }

        /** 		object d2 = file_timestamp(all_files[i])*/
        _2 = (int)SEQ_PTR(_all_files_45286);
        _24166 = (int)*(((s1_ptr)_2)->base + _i_45290);
        Ref(_24166);
        _0 = _d2_45292;
        _d2_45292 = _9file_timestamp(_24166);
        DeRef(_0);
        _24166 = NOVALUE;

        /** 		if atom(d2) then*/
        _24168 = IS_ATOM(_d2_45292);
        if (_24168 == 0)
        {
            _24168 = NOVALUE;
            goto L4; // [74] 84
        }
        else{
            _24168 = NOVALUE;
        }

        /** 			return 1*/
        DeRef(_d2_45292);
        DeRefDS(_fname_45275);
        DeRef(_d1_45276);
        DeRefDS(_all_files_45286);
        return 1;
L4: 

        /** 		if datetime:diff(d1, d2) > 0 then*/

        /** 	return datetimeToSeconds(dt2) - datetimeToSeconds(dt1)*/
        Ref(_d2_45292);
        _0 = _diff_1__tmp_at88_45301;
        _diff_1__tmp_at88_45301 = _10datetimeToSeconds(_d2_45292);
        DeRef(_0);
        Ref(_d1_45276);
        _0 = _diff_2__tmp_at88_45302;
        _diff_2__tmp_at88_45302 = _10datetimeToSeconds(_d1_45276);
        DeRef(_0);
        DeRef(_diff_inlined_diff_at_88_45300);
        if (IS_ATOM_INT(_diff_1__tmp_at88_45301) && IS_ATOM_INT(_diff_2__tmp_at88_45302)) {
            _diff_inlined_diff_at_88_45300 = _diff_1__tmp_at88_45301 - _diff_2__tmp_at88_45302;
            if ((long)((unsigned long)_diff_inlined_diff_at_88_45300 +(unsigned long) HIGH_BITS) >= 0){
                _diff_inlined_diff_at_88_45300 = NewDouble((double)_diff_inlined_diff_at_88_45300);
            }
        }
        else {
            _diff_inlined_diff_at_88_45300 = binary_op(MINUS, _diff_1__tmp_at88_45301, _diff_2__tmp_at88_45302);
        }
        DeRef(_diff_1__tmp_at88_45301);
        _diff_1__tmp_at88_45301 = NOVALUE;
        DeRef(_diff_2__tmp_at88_45302);
        _diff_2__tmp_at88_45302 = NOVALUE;
        if (binary_op_a(LESSEQ, _diff_inlined_diff_at_88_45300, 0)){
            goto L5; // [103] 114
        }

        /** 			return 1*/
        DeRef(_d2_45292);
        DeRefDS(_fname_45275);
        DeRef(_d1_45276);
        DeRef(_all_files_45286);
        return 1;
L5: 
        DeRef(_d2_45292);
        _d2_45292 = NOVALUE;

        /** 	end for*/
        _i_45290 = _i_45290 + 1;
        goto L2; // [118] 59
L3: 
        ;
    }

    /** 	return 0*/
    DeRefDS(_fname_45275);
    DeRef(_d1_45276);
    DeRef(_all_files_45286);
    return 0;
    ;
}


void _54update_checksum(int _raw_data_45356)
{
    int _24188 = NOVALUE;
    int _0, _1, _2;
    

    /** 	cfile_check = xor_bits(cfile_check, hash( raw_data, stdhash:HSIEH32))*/
    _24188 = calc_hash(_raw_data_45356, -5);
    _0 = _54cfile_check_45333;
    if (IS_ATOM_INT(_54cfile_check_45333) && IS_ATOM_INT(_24188)) {
        {unsigned long tu;
             tu = (unsigned long)_54cfile_check_45333 ^ (unsigned long)_24188;
             _54cfile_check_45333 = MAKE_UINT(tu);
        }
    }
    else {
        if (IS_ATOM_INT(_54cfile_check_45333)) {
            temp_d.dbl = (double)_54cfile_check_45333;
            _54cfile_check_45333 = Dxor_bits(&temp_d, DBL_PTR(_24188));
        }
        else {
            if (IS_ATOM_INT(_24188)) {
                temp_d.dbl = (double)_24188;
                _54cfile_check_45333 = Dxor_bits(DBL_PTR(_54cfile_check_45333), &temp_d);
            }
            else
            _54cfile_check_45333 = Dxor_bits(DBL_PTR(_54cfile_check_45333), DBL_PTR(_24188));
        }
    }
    DeRef(_0);
    DeRef(_24188);
    _24188 = NOVALUE;

    /** end procedure*/
    DeRef(_raw_data_45356);
    return;
    ;
}


void _54write_checksum(int _file_45361)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_file_45361)) {
        _1 = (long)(DBL_PTR(_file_45361)->dbl);
        if (UNIQUE(DBL_PTR(_file_45361)) && (DBL_PTR(_file_45361)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_file_45361);
        _file_45361 = _1;
    }

    /** 	printf( file, "\n// 0x%08x\n", cfile_check )*/
    EPrintf(_file_45361, _24190, _54cfile_check_45333);

    /** 	cfile_check = 0*/
    DeRef(_54cfile_check_45333);
    _54cfile_check_45333 = 0;

    /** end procedure*/
    return;
    ;
}


int _54find_file_element(int _needle_45365, int _files_45366)
{
    int _24206 = NOVALUE;
    int _24205 = NOVALUE;
    int _24204 = NOVALUE;
    int _24203 = NOVALUE;
    int _24202 = NOVALUE;
    int _24201 = NOVALUE;
    int _24200 = NOVALUE;
    int _24199 = NOVALUE;
    int _24198 = NOVALUE;
    int _24197 = NOVALUE;
    int _24196 = NOVALUE;
    int _24195 = NOVALUE;
    int _24194 = NOVALUE;
    int _24193 = NOVALUE;
    int _24192 = NOVALUE;
    int _24191 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for j = 1 to length(files) do*/
    if (IS_SEQUENCE(_files_45366)){
            _24191 = SEQ_PTR(_files_45366)->length;
    }
    else {
        _24191 = 1;
    }
    {
        int _j_45368;
        _j_45368 = 1;
L1: 
        if (_j_45368 > _24191){
            goto L2; // [10] 50
        }

        /** 		if equal(files[j][D_NAME],needle) then*/
        _2 = (int)SEQ_PTR(_files_45366);
        _24192 = (int)*(((s1_ptr)_2)->base + _j_45368);
        _2 = (int)SEQ_PTR(_24192);
        _24193 = (int)*(((s1_ptr)_2)->base + 1);
        _24192 = NOVALUE;
        if (_24193 == _needle_45365)
        _24194 = 1;
        else if (IS_ATOM_INT(_24193) && IS_ATOM_INT(_needle_45365))
        _24194 = 0;
        else
        _24194 = (compare(_24193, _needle_45365) == 0);
        _24193 = NOVALUE;
        if (_24194 == 0)
        {
            _24194 = NOVALUE;
            goto L3; // [33] 43
        }
        else{
            _24194 = NOVALUE;
        }

        /** 			return j*/
        DeRefDS(_needle_45365);
        DeRefDS(_files_45366);
        return _j_45368;
L3: 

        /** 	end for*/
        _j_45368 = _j_45368 + 1;
        goto L1; // [45] 17
L2: 
        ;
    }

    /** 	for j = 1 to length(files) do*/
    if (IS_SEQUENCE(_files_45366)){
            _24195 = SEQ_PTR(_files_45366)->length;
    }
    else {
        _24195 = 1;
    }
    {
        int _j_45376;
        _j_45376 = 1;
L4: 
        if (_j_45376 > _24195){
            goto L5; // [55] 103
        }

        /** 		if equal(lower(files[j][D_NAME]),lower(needle)) then*/
        _2 = (int)SEQ_PTR(_files_45366);
        _24196 = (int)*(((s1_ptr)_2)->base + _j_45376);
        _2 = (int)SEQ_PTR(_24196);
        _24197 = (int)*(((s1_ptr)_2)->base + 1);
        _24196 = NOVALUE;
        Ref(_24197);
        _24198 = _4lower(_24197);
        _24197 = NOVALUE;
        RefDS(_needle_45365);
        _24199 = _4lower(_needle_45365);
        if (_24198 == _24199)
        _24200 = 1;
        else if (IS_ATOM_INT(_24198) && IS_ATOM_INT(_24199))
        _24200 = 0;
        else
        _24200 = (compare(_24198, _24199) == 0);
        DeRef(_24198);
        _24198 = NOVALUE;
        DeRef(_24199);
        _24199 = NOVALUE;
        if (_24200 == 0)
        {
            _24200 = NOVALUE;
            goto L6; // [86] 96
        }
        else{
            _24200 = NOVALUE;
        }

        /** 			return j*/
        DeRefDS(_needle_45365);
        DeRefDS(_files_45366);
        return _j_45376;
L6: 

        /** 	end for*/
        _j_45376 = _j_45376 + 1;
        goto L4; // [98] 62
L5: 
        ;
    }

    /** 	for j = 1 to length(files) do*/
    if (IS_SEQUENCE(_files_45366)){
            _24201 = SEQ_PTR(_files_45366)->length;
    }
    else {
        _24201 = 1;
    }
    {
        int _j_45388;
        _j_45388 = 1;
L7: 
        if (_j_45388 > _24201){
            goto L8; // [108] 156
        }

        /** 		if equal(lower(files[j][D_ALTNAME]),lower(needle)) then*/
        _2 = (int)SEQ_PTR(_files_45366);
        _24202 = (int)*(((s1_ptr)_2)->base + _j_45388);
        _2 = (int)SEQ_PTR(_24202);
        _24203 = (int)*(((s1_ptr)_2)->base + 11);
        _24202 = NOVALUE;
        Ref(_24203);
        _24204 = _4lower(_24203);
        _24203 = NOVALUE;
        RefDS(_needle_45365);
        _24205 = _4lower(_needle_45365);
        if (_24204 == _24205)
        _24206 = 1;
        else if (IS_ATOM_INT(_24204) && IS_ATOM_INT(_24205))
        _24206 = 0;
        else
        _24206 = (compare(_24204, _24205) == 0);
        DeRef(_24204);
        _24204 = NOVALUE;
        DeRef(_24205);
        _24205 = NOVALUE;
        if (_24206 == 0)
        {
            _24206 = NOVALUE;
            goto L9; // [139] 149
        }
        else{
            _24206 = NOVALUE;
        }

        /** 			return j*/
        DeRefDS(_needle_45365);
        DeRefDS(_files_45366);
        return _j_45388;
L9: 

        /** 	end for*/
        _j_45388 = _j_45388 + 1;
        goto L7; // [151] 115
L8: 
        ;
    }

    /** 	return 0*/
    DeRefDS(_needle_45365);
    DeRefDS(_files_45366);
    return 0;
    ;
}


int _54adjust_for_command_line_passing(int _long_path_45409)
{
    int _slash_45410 = NOVALUE;
    int _longs_45418 = NOVALUE;
    int _short_path_45424 = NOVALUE;
    int _files_45430 = NOVALUE;
    int _file_location_45433 = NOVALUE;
    int _24245 = NOVALUE;
    int _24244 = NOVALUE;
    int _24242 = NOVALUE;
    int _24241 = NOVALUE;
    int _24239 = NOVALUE;
    int _24238 = NOVALUE;
    int _24236 = NOVALUE;
    int _24235 = NOVALUE;
    int _24232 = NOVALUE;
    int _24231 = NOVALUE;
    int _24229 = NOVALUE;
    int _24228 = NOVALUE;
    int _24227 = NOVALUE;
    int _24226 = NOVALUE;
    int _24225 = NOVALUE;
    int _24223 = NOVALUE;
    int _24222 = NOVALUE;
    int _24220 = NOVALUE;
    int _24218 = NOVALUE;
    int _24216 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if compiler_type = COMPILER_GCC then*/
    if (_54compiler_type_45317 != 1)
    goto L1; // [9] 21

    /** 		slash = '/'*/
    _slash_45410 = 47;
    goto L2; // [18] 49
L1: 

    /** 	elsif compiler_type = COMPILER_WATCOM then*/
    if (_54compiler_type_45317 != 2)
    goto L3; // [27] 39

    /** 		slash = '\\'*/
    _slash_45410 = 92;
    goto L2; // [36] 49
L3: 

    /** 		slash = SLASH*/
    _slash_45410 = 92;
L2: 

    /** 	ifdef UNIX then*/

    /** 		long_path = regex:find_replace(quote_pattern, long_path, "")*/
    Ref(_54quote_pattern_45402);
    RefDS(_long_path_45409);
    RefDS(_22682);
    _0 = _long_path_45409;
    _long_path_45409 = _50find_replace(_54quote_pattern_45402, _long_path_45409, _22682, 1, 0);
    DeRefDS(_0);

    /** 		sequence longs = split( slash_pattern, long_path )*/
    Ref(_54slash_pattern_45399);
    RefDS(_long_path_45409);
    _0 = _longs_45418;
    _longs_45418 = _50split(_54slash_pattern_45399, _long_path_45409, 1, 0);
    DeRef(_0);

    /** 		if length(longs)=0 then*/
    if (IS_SEQUENCE(_longs_45418)){
            _24216 = SEQ_PTR(_longs_45418)->length;
    }
    else {
        _24216 = 1;
    }
    if (_24216 != 0)
    goto L4; // [83] 94

    /** 			return long_path*/
    DeRefDS(_longs_45418);
    DeRef(_short_path_45424);
    return _long_path_45409;
L4: 

    /** 		sequence short_path = longs[1] & slash*/
    _2 = (int)SEQ_PTR(_longs_45418);
    _24218 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_24218) && IS_ATOM(_slash_45410)) {
        Append(&_short_path_45424, _24218, _slash_45410);
    }
    else if (IS_ATOM(_24218) && IS_SEQUENCE(_slash_45410)) {
    }
    else {
        Concat((object_ptr)&_short_path_45424, _24218, _slash_45410);
        _24218 = NOVALUE;
    }
    _24218 = NOVALUE;

    /** 		for i = 2 to length(longs) do*/
    if (IS_SEQUENCE(_longs_45418)){
            _24220 = SEQ_PTR(_longs_45418)->length;
    }
    else {
        _24220 = 1;
    }
    {
        int _i_45428;
        _i_45428 = 2;
L5: 
        if (_i_45428 > _24220){
            goto L6; // [111] 270
        }

        /** 			object files = dir(short_path)*/
        RefDS(_short_path_45424);
        _0 = _files_45430;
        _files_45430 = _9dir(_short_path_45424);
        DeRef(_0);

        /** 			integer file_location = 0*/
        _file_location_45433 = 0;

        /** 			if sequence(files) then*/
        _24222 = IS_SEQUENCE(_files_45430);
        if (_24222 == 0)
        {
            _24222 = NOVALUE;
            goto L7; // [134] 151
        }
        else{
            _24222 = NOVALUE;
        }

        /** 				file_location = find_file_element(longs[i], files)*/
        _2 = (int)SEQ_PTR(_longs_45418);
        _24223 = (int)*(((s1_ptr)_2)->base + _i_45428);
        Ref(_24223);
        Ref(_files_45430);
        _file_location_45433 = _54find_file_element(_24223, _files_45430);
        _24223 = NOVALUE;
        if (!IS_ATOM_INT(_file_location_45433)) {
            _1 = (long)(DBL_PTR(_file_location_45433)->dbl);
            if (UNIQUE(DBL_PTR(_file_location_45433)) && (DBL_PTR(_file_location_45433)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_file_location_45433);
            _file_location_45433 = _1;
        }
L7: 

        /** 			if file_location then*/
        if (_file_location_45433 == 0)
        {
            goto L8; // [153] 219
        }
        else{
        }

        /** 				if sequence(files[file_location][D_ALTNAME]) then*/
        _2 = (int)SEQ_PTR(_files_45430);
        _24225 = (int)*(((s1_ptr)_2)->base + _file_location_45433);
        _2 = (int)SEQ_PTR(_24225);
        _24226 = (int)*(((s1_ptr)_2)->base + 11);
        _24225 = NOVALUE;
        _24227 = IS_SEQUENCE(_24226);
        _24226 = NOVALUE;
        if (_24227 == 0)
        {
            _24227 = NOVALUE;
            goto L9; // [171] 193
        }
        else{
            _24227 = NOVALUE;
        }

        /** 					short_path &= files[file_location][D_ALTNAME]*/
        _2 = (int)SEQ_PTR(_files_45430);
        _24228 = (int)*(((s1_ptr)_2)->base + _file_location_45433);
        _2 = (int)SEQ_PTR(_24228);
        _24229 = (int)*(((s1_ptr)_2)->base + 11);
        _24228 = NOVALUE;
        if (IS_SEQUENCE(_short_path_45424) && IS_ATOM(_24229)) {
            Ref(_24229);
            Append(&_short_path_45424, _short_path_45424, _24229);
        }
        else if (IS_ATOM(_short_path_45424) && IS_SEQUENCE(_24229)) {
        }
        else {
            Concat((object_ptr)&_short_path_45424, _short_path_45424, _24229);
        }
        _24229 = NOVALUE;
        goto LA; // [190] 210
L9: 

        /** 					short_path &= files[file_location][D_NAME]*/
        _2 = (int)SEQ_PTR(_files_45430);
        _24231 = (int)*(((s1_ptr)_2)->base + _file_location_45433);
        _2 = (int)SEQ_PTR(_24231);
        _24232 = (int)*(((s1_ptr)_2)->base + 1);
        _24231 = NOVALUE;
        if (IS_SEQUENCE(_short_path_45424) && IS_ATOM(_24232)) {
            Ref(_24232);
            Append(&_short_path_45424, _short_path_45424, _24232);
        }
        else if (IS_ATOM(_short_path_45424) && IS_SEQUENCE(_24232)) {
        }
        else {
            Concat((object_ptr)&_short_path_45424, _short_path_45424, _24232);
        }
        _24232 = NOVALUE;
LA: 

        /** 				short_path &= slash*/
        Append(&_short_path_45424, _short_path_45424, _slash_45410);
        goto LB; // [216] 261
L8: 

        /** 				if not find(' ',longs[i]) then*/
        _2 = (int)SEQ_PTR(_longs_45418);
        _24235 = (int)*(((s1_ptr)_2)->base + _i_45428);
        _24236 = find_from(32, _24235, 1);
        _24235 = NOVALUE;
        if (_24236 != 0)
        goto LC; // [230] 254
        _24236 = NOVALUE;

        /** 					short_path &= longs[i] & slash*/
        _2 = (int)SEQ_PTR(_longs_45418);
        _24238 = (int)*(((s1_ptr)_2)->base + _i_45428);
        if (IS_SEQUENCE(_24238) && IS_ATOM(_slash_45410)) {
            Append(&_24239, _24238, _slash_45410);
        }
        else if (IS_ATOM(_24238) && IS_SEQUENCE(_slash_45410)) {
        }
        else {
            Concat((object_ptr)&_24239, _24238, _slash_45410);
            _24238 = NOVALUE;
        }
        _24238 = NOVALUE;
        Concat((object_ptr)&_short_path_45424, _short_path_45424, _24239);
        DeRefDS(_24239);
        _24239 = NOVALUE;

        /** 					continue*/
        DeRef(_files_45430);
        _files_45430 = NOVALUE;
        goto LD; // [251] 265
LC: 

        /** 				return 0*/
        DeRef(_files_45430);
        DeRefDS(_long_path_45409);
        DeRef(_longs_45418);
        DeRef(_short_path_45424);
        return 0;
LB: 
        DeRef(_files_45430);
        _files_45430 = NOVALUE;

        /** 		end for -- i*/
LD: 
        _i_45428 = _i_45428 + 1;
        goto L5; // [265] 118
L6: 
        ;
    }

    /** 		if short_path[$] = slash then*/
    if (IS_SEQUENCE(_short_path_45424)){
            _24241 = SEQ_PTR(_short_path_45424)->length;
    }
    else {
        _24241 = 1;
    }
    _2 = (int)SEQ_PTR(_short_path_45424);
    _24242 = (int)*(((s1_ptr)_2)->base + _24241);
    if (binary_op_a(NOTEQ, _24242, _slash_45410)){
        _24242 = NOVALUE;
        goto LE; // [279] 298
    }
    _24242 = NOVALUE;

    /** 			short_path = short_path[1..$-1]*/
    if (IS_SEQUENCE(_short_path_45424)){
            _24244 = SEQ_PTR(_short_path_45424)->length;
    }
    else {
        _24244 = 1;
    }
    _24245 = _24244 - 1;
    _24244 = NOVALUE;
    rhs_slice_target = (object_ptr)&_short_path_45424;
    RHS_Slice(_short_path_45424, 1, _24245);
LE: 

    /** 		return short_path*/
    DeRefDS(_long_path_45409);
    DeRef(_longs_45418);
    DeRef(_24245);
    _24245 = NOVALUE;
    return _short_path_45424;
    ;
}


int _54adjust_for_build_file(int _long_path_45471)
{
    int _short_path_45472 = NOVALUE;
    int _24253 = NOVALUE;
    int _24252 = NOVALUE;
    int _24251 = NOVALUE;
    int _24250 = NOVALUE;
    int _24249 = NOVALUE;
    int _24248 = NOVALUE;
    int _0, _1, _2;
    

    /**     object short_path = adjust_for_command_line_passing(long_path)*/
    RefDS(_long_path_45471);
    _0 = _short_path_45472;
    _short_path_45472 = _54adjust_for_command_line_passing(_long_path_45471);
    DeRef(_0);

    /**     if atom(short_path) then*/
    _24248 = IS_ATOM(_short_path_45472);
    if (_24248 == 0)
    {
        _24248 = NOVALUE;
        goto L1; // [14] 24
    }
    else{
        _24248 = NOVALUE;
    }

    /**     	return short_path*/
    DeRefDS(_long_path_45471);
    return _short_path_45472;
L1: 

    /** 	if compiler_type = COMPILER_GCC and build_system_type != BUILD_DIRECT and TWINDOWS then*/
    _24249 = (_54compiler_type_45317 == 1);
    if (_24249 == 0) {
        _24250 = 0;
        goto L2; // [34] 50
    }
    _24251 = (_54build_system_type_45311 != 3);
    _24250 = (_24251 != 0);
L2: 
    if (_24250 == 0) {
        goto L3; // [50] 73
    }
    if (_36TWINDOWS_14721 == 0)
    {
        goto L3; // [57] 73
    }
    else{
    }

    /** 		return windows_to_mingw_path(short_path)*/
    Ref(_short_path_45472);
    _24253 = _54windows_to_mingw_path(_short_path_45472);
    DeRefDS(_long_path_45471);
    DeRef(_short_path_45472);
    DeRef(_24249);
    _24249 = NOVALUE;
    DeRef(_24251);
    _24251 = NOVALUE;
    return _24253;
    goto L4; // [70] 80
L3: 

    /** 		return short_path*/
    DeRefDS(_long_path_45471);
    DeRef(_24249);
    _24249 = NOVALUE;
    DeRef(_24251);
    _24251 = NOVALUE;
    DeRef(_24253);
    _24253 = NOVALUE;
    return _short_path_45472;
L4: 
    ;
}


int _54setup_build()
{
    int _c_exe_45487 = NOVALUE;
    int _c_flags_45488 = NOVALUE;
    int _l_exe_45489 = NOVALUE;
    int _l_flags_45490 = NOVALUE;
    int _obj_ext_45491 = NOVALUE;
    int _exe_ext_45492 = NOVALUE;
    int _l_flags_begin_45493 = NOVALUE;
    int _rc_comp_45494 = NOVALUE;
    int _l_names_45495 = NOVALUE;
    int _l_ext_45496 = NOVALUE;
    int _t_slash_45497 = NOVALUE;
    int _eudir_45516 = NOVALUE;
    int _compile_dir_45574 = NOVALUE;
    int _24379 = NOVALUE;
    int _24378 = NOVALUE;
    int _24377 = NOVALUE;
    int _24374 = NOVALUE;
    int _24373 = NOVALUE;
    int _24370 = NOVALUE;
    int _24369 = NOVALUE;
    int _24362 = NOVALUE;
    int _24361 = NOVALUE;
    int _24356 = NOVALUE;
    int _24355 = NOVALUE;
    int _24350 = NOVALUE;
    int _24349 = NOVALUE;
    int _24346 = NOVALUE;
    int _24345 = NOVALUE;
    int _24335 = NOVALUE;
    int _24334 = NOVALUE;
    int _24319 = NOVALUE;
    int _24318 = NOVALUE;
    int _24314 = NOVALUE;
    int _24312 = NOVALUE;
    int _24311 = NOVALUE;
    int _24310 = NOVALUE;
    int _24309 = NOVALUE;
    int _24297 = NOVALUE;
    int _24296 = NOVALUE;
    int _24293 = NOVALUE;
    int _24281 = NOVALUE;
    int _24277 = NOVALUE;
    int _24274 = NOVALUE;
    int _24273 = NOVALUE;
    int _24272 = NOVALUE;
    int _24269 = NOVALUE;
    int _24268 = NOVALUE;
    int _24267 = NOVALUE;
    int _24264 = NOVALUE;
    int _24261 = NOVALUE;
    int _24254 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence c_exe   = "", c_flags = "", l_exe   = "", l_flags = "", obj_ext = "",*/
    RefDS(_22682);
    DeRefi(_c_exe_45487);
    _c_exe_45487 = _22682;
    RefDS(_22682);
    DeRef(_c_flags_45488);
    _c_flags_45488 = _22682;
    RefDS(_22682);
    DeRefi(_l_exe_45489);
    _l_exe_45489 = _22682;
    RefDS(_22682);
    DeRef(_l_flags_45490);
    _l_flags_45490 = _22682;
    RefDS(_22682);
    DeRefi(_obj_ext_45491);
    _obj_ext_45491 = _22682;

    /** 		exe_ext = "", l_flags_begin = "", rc_comp = "", l_names, l_ext, t_slash*/
    RefDS(_22682);
    DeRefi(_exe_ext_45492);
    _exe_ext_45492 = _22682;
    RefDS(_22682);
    DeRefi(_l_flags_begin_45493);
    _l_flags_begin_45493 = _22682;
    RefDS(_22682);
    DeRef(_rc_comp_45494);
    _rc_comp_45494 = _22682;

    /** 	if length(user_library) = 0 then*/
    if (IS_SEQUENCE(_56user_library_42666)){
            _24254 = SEQ_PTR(_56user_library_42666)->length;
    }
    else {
        _24254 = 1;
    }
    if (_24254 != 0)
    goto L1; // [52] 261

    /** 		if debug_option then*/
    if (_56debug_option_42664 == 0)
    {
        goto L2; // [60] 72
    }
    else{
    }

    /** 			l_names = { "eudbg", "eu" }*/
    RefDS(_24257);
    RefDS(_24256);
    DeRef(_l_names_45495);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24256;
    ((int *)_2)[2] = _24257;
    _l_names_45495 = MAKE_SEQ(_1);
    goto L3; // [69] 79
L2: 

    /** 			l_names = { "eu", "eudbg" }*/
    RefDS(_24256);
    RefDS(_24257);
    DeRef(_l_names_45495);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24257;
    ((int *)_2)[2] = _24256;
    _l_names_45495 = MAKE_SEQ(_1);
L3: 

    /** 		if TUNIX or compiler_type = COMPILER_GCC then*/
    if (_36TUNIX_14725 != 0) {
        goto L4; // [83] 100
    }
    _24261 = (_54compiler_type_45317 == 1);
    if (_24261 == 0)
    {
        DeRef(_24261);
        _24261 = NOVALUE;
        goto L5; // [96] 117
    }
    else{
        DeRef(_24261);
        _24261 = NOVALUE;
    }
L4: 

    /** 			l_ext = "a"*/
    RefDS(_22920);
    DeRefi(_l_ext_45496);
    _l_ext_45496 = _22920;

    /** 			t_slash = "/"*/
    RefDS(_24138);
    DeRefi(_t_slash_45497);
    _t_slash_45497 = _24138;
    goto L6; // [114] 140
L5: 

    /** 		elsif TWINDOWS then*/
    if (_36TWINDOWS_14721 == 0)
    {
        goto L7; // [121] 139
    }
    else{
    }

    /** 			l_ext = "lib"*/
    RefDS(_24262);
    DeRefi(_l_ext_45496);
    _l_ext_45496 = _24262;

    /** 			t_slash = "\\"*/
    RefDS(_23009);
    DeRefi(_t_slash_45497);
    _t_slash_45497 = _23009;
L7: 
L6: 

    /** 		object eudir = get_eucompiledir()*/
    _0 = _eudir_45516;
    _eudir_45516 = _56get_eucompiledir();
    DeRef(_0);

    /** 		if not file_exists(eudir) then*/
    Ref(_eudir_45516);
    _24264 = _9file_exists(_eudir_45516);
    if (IS_ATOM_INT(_24264)) {
        if (_24264 != 0){
            DeRef(_24264);
            _24264 = NOVALUE;
            goto L8; // [151] 172
        }
    }
    else {
        if (DBL_PTR(_24264)->dbl != 0.0){
            DeRef(_24264);
            _24264 = NOVALUE;
            goto L8; // [151] 172
        }
    }
    DeRef(_24264);
    _24264 = NOVALUE;

    /** 			printf(2,"Supplied directory \'%s\' is not a valid EUDIR\n",{get_eucompiledir()})*/
    _24267 = _56get_eucompiledir();
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _24267;
    _24268 = MAKE_SEQ(_1);
    _24267 = NOVALUE;
    EPrintf(2, _24266, _24268);
    DeRefDS(_24268);
    _24268 = NOVALUE;

    /** 			abort(1)*/
    UserCleanup(1);
L8: 

    /** 		for tk = 1 to length(l_names) label "translation kind" do*/
    _24269 = 2;
    {
        int _tk_45528;
        _tk_45528 = 1;
L9: 
        if (_tk_45528 > 2){
            goto LA; // [179] 260
        }

        /** 			user_library = eudir & sprintf("%sbin%s%s.%s",{t_slash, t_slash, l_names[tk],l_ext})*/
        _2 = (int)SEQ_PTR(_l_names_45495);
        _24272 = (int)*(((s1_ptr)_2)->base + _tk_45528);
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        RefDSn(_t_slash_45497, 2);
        *((int *)(_2+4)) = _t_slash_45497;
        *((int *)(_2+8)) = _t_slash_45497;
        RefDS(_24272);
        *((int *)(_2+12)) = _24272;
        RefDS(_l_ext_45496);
        *((int *)(_2+16)) = _l_ext_45496;
        _24273 = MAKE_SEQ(_1);
        _24272 = NOVALUE;
        _24274 = EPrintf(-9999999, _24271, _24273);
        DeRefDS(_24273);
        _24273 = NOVALUE;
        if (IS_SEQUENCE(_eudir_45516) && IS_ATOM(_24274)) {
        }
        else if (IS_ATOM(_eudir_45516) && IS_SEQUENCE(_24274)) {
            Ref(_eudir_45516);
            Prepend(&_56user_library_42666, _24274, _eudir_45516);
        }
        else {
            Concat((object_ptr)&_56user_library_42666, _eudir_45516, _24274);
        }
        DeRefDS(_24274);
        _24274 = NOVALUE;

        /** 			if TUNIX or compiler_type = COMPILER_GCC then*/
        if (_36TUNIX_14725 != 0) {
            goto LB; // [217] 234
        }
        _24277 = (_54compiler_type_45317 == 1);
        if (_24277 == 0)
        {
            DeRef(_24277);
            _24277 = NOVALUE;
            goto LC; // [230] 237
        }
        else{
            DeRef(_24277);
            _24277 = NOVALUE;
        }
LB: 

        /** 				ifdef UNIX then*/
LC: 

        /** 			if file_exists(user_library) then*/
        RefDS(_56user_library_42666);
        _24281 = _9file_exists(_56user_library_42666);
        if (_24281 == 0) {
            DeRef(_24281);
            _24281 = NOVALUE;
            goto LD; // [245] 253
        }
        else {
            if (!IS_ATOM_INT(_24281) && DBL_PTR(_24281)->dbl == 0.0){
                DeRef(_24281);
                _24281 = NOVALUE;
                goto LD; // [245] 253
            }
            DeRef(_24281);
            _24281 = NOVALUE;
        }
        DeRef(_24281);
        _24281 = NOVALUE;

        /** 				exit "translation kind"*/
        goto LA; // [250] 260
LD: 

        /** 		end for -- tk*/
        _tk_45528 = _tk_45528 + 1;
        goto L9; // [255] 186
LA: 
        ;
    }
L1: 
    DeRef(_eudir_45516);
    _eudir_45516 = NOVALUE;

    /** 	user_library = adjust_for_build_file(user_library)*/
    RefDS(_56user_library_42666);
    _0 = _54adjust_for_build_file(_56user_library_42666);
    DeRefDS(_56user_library_42666);
    _56user_library_42666 = _0;

    /** 	if TWINDOWS then*/
    if (_36TWINDOWS_14721 == 0)
    {
        goto LE; // [277] 334
    }
    else{
    }

    /** 		if compiler_type = COMPILER_WATCOM then*/
    if (_54compiler_type_45317 != 2)
    goto LF; // [286] 299

    /** 			c_flags &= " /dEWINDOWS"*/
    Concat((object_ptr)&_c_flags_45488, _c_flags_45488, _24284);
    goto L10; // [296] 306
LF: 

    /** 			c_flags &= " -DEWINDOWS"*/
    Concat((object_ptr)&_c_flags_45488, _c_flags_45488, _24286);
L10: 

    /** 		if dll_option then*/
    if (_56dll_option_42654 == 0)
    {
        goto L11; // [310] 323
    }
    else{
    }

    /** 			exe_ext = ".dll"*/
    RefDS(_24288);
    DeRefi(_exe_ext_45492);
    _exe_ext_45492 = _24288;
    goto L12; // [320] 375
L11: 

    /** 			exe_ext = ".exe"*/
    RefDS(_24289);
    DeRefi(_exe_ext_45492);
    _exe_ext_45492 = _24289;
    goto L12; // [331] 375
LE: 

    /** 	elsif TOSX then*/
    if (_36TOSX_14729 == 0)
    {
        goto L13; // [338] 359
    }
    else{
    }

    /** 		if dll_option then*/
    if (_56dll_option_42654 == 0)
    {
        goto L12; // [345] 375
    }
    else{
    }

    /** 			exe_ext = ".dylib"*/
    RefDS(_24290);
    DeRefi(_exe_ext_45492);
    _exe_ext_45492 = _24290;
    goto L12; // [356] 375
L13: 

    /** 		if dll_option then*/
    if (_56dll_option_42654 == 0)
    {
        goto L14; // [363] 374
    }
    else{
    }

    /** 			exe_ext = ".so"*/
    RefDS(_24291);
    DeRefi(_exe_ext_45492);
    _exe_ext_45492 = _24291;
L14: 
L12: 

    /** 	object compile_dir = get_eucompiledir()*/
    _0 = _compile_dir_45574;
    _compile_dir_45574 = _56get_eucompiledir();
    DeRef(_0);

    /** 	if not file_exists(compile_dir) then*/
    Ref(_compile_dir_45574);
    _24293 = _9file_exists(_compile_dir_45574);
    if (IS_ATOM_INT(_24293)) {
        if (_24293 != 0){
            DeRef(_24293);
            _24293 = NOVALUE;
            goto L15; // [386] 407
        }
    }
    else {
        if (DBL_PTR(_24293)->dbl != 0.0){
            DeRef(_24293);
            _24293 = NOVALUE;
            goto L15; // [386] 407
        }
    }
    DeRef(_24293);
    _24293 = NOVALUE;

    /** 		printf(2,"Couldn't get include directory '%s'",{get_eucompiledir()})*/
    _24296 = _56get_eucompiledir();
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _24296;
    _24297 = MAKE_SEQ(_1);
    _24296 = NOVALUE;
    EPrintf(2, _24295, _24297);
    DeRefDS(_24297);
    _24297 = NOVALUE;

    /** 		abort(1)*/
    UserCleanup(1);
L15: 

    /** 	switch compiler_type do*/
    _0 = _54compiler_type_45317;
    switch ( _0 ){ 

        /** 		case COMPILER_GCC then*/
        case 1:

        /** 			c_exe = "gcc"*/
        RefDS(_24300);
        DeRefi(_c_exe_45487);
        _c_exe_45487 = _24300;

        /** 			l_exe = "gcc"*/
        RefDS(_24300);
        DeRefi(_l_exe_45489);
        _l_exe_45489 = _24300;

        /** 			obj_ext = "o"*/
        RefDS(_24301);
        DeRefi(_obj_ext_45491);
        _obj_ext_45491 = _24301;

        /** 			if debug_option then*/
        if (_56debug_option_42664 == 0)
        {
            goto L16; // [445] 457
        }
        else{
        }

        /** 				c_flags &= " -g3"*/
        Concat((object_ptr)&_c_flags_45488, _c_flags_45488, _24302);
        goto L17; // [454] 464
L16: 

        /** 				c_flags &= " -fomit-frame-pointer"*/
        Concat((object_ptr)&_c_flags_45488, _c_flags_45488, _24304);
L17: 

        /** 			if dll_option then*/
        if (_56dll_option_42654 == 0)
        {
            goto L18; // [468] 478
        }
        else{
        }

        /** 				c_flags &= " -fPIC"*/
        Concat((object_ptr)&_c_flags_45488, _c_flags_45488, _24306);
L18: 

        /** 			c_flags &= sprintf(" -c -w -fsigned-char -O2 -m32 -I%s -ffast-math",*/
        _24309 = _56get_eucompiledir();
        _24310 = _54adjust_for_build_file(_24309);
        _24309 = NOVALUE;
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _24310;
        _24311 = MAKE_SEQ(_1);
        _24310 = NOVALUE;
        _24312 = EPrintf(-9999999, _24308, _24311);
        DeRefDS(_24311);
        _24311 = NOVALUE;
        Concat((object_ptr)&_c_flags_45488, _c_flags_45488, _24312);
        DeRefDS(_24312);
        _24312 = NOVALUE;

        /** 			if TWINDOWS and mno_cygwin then*/
        if (_36TWINDOWS_14721 == 0) {
            goto L19; // [503] 520
        }
        if (_54mno_cygwin_45338 == 0)
        {
            goto L19; // [510] 520
        }
        else{
        }

        /** 				c_flags &= " -mno-cygwin"*/
        Concat((object_ptr)&_c_flags_45488, _c_flags_45488, _24315);
L19: 

        /** 			l_flags = sprintf( "%s -m32 ", { adjust_for_build_file(user_library) })*/
        RefDS(_56user_library_42666);
        _24318 = _54adjust_for_build_file(_56user_library_42666);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _24318;
        _24319 = MAKE_SEQ(_1);
        _24318 = NOVALUE;
        DeRef(_l_flags_45490);
        _l_flags_45490 = EPrintf(-9999999, _24317, _24319);
        DeRefDS(_24319);
        _24319 = NOVALUE;

        /** 			if dll_option then*/
        if (_56dll_option_42654 == 0)
        {
            goto L1A; // [540] 550
        }
        else{
        }

        /** 				l_flags &= " -shared "*/
        Concat((object_ptr)&_l_flags_45490, _l_flags_45490, _24321);
L1A: 

        /** 			if TLINUX then*/
        if (_36TLINUX_14723 == 0)
        {
            goto L1B; // [554] 566
        }
        else{
        }

        /** 				l_flags &= " -ldl -lm -lpthread"*/
        Concat((object_ptr)&_l_flags_45490, _l_flags_45490, _24323);
        goto L1C; // [563] 635
L1B: 

        /** 			elsif TBSD then*/
        if (_36TBSD_14727 == 0)
        {
            goto L1D; // [570] 582
        }
        else{
        }

        /** 				l_flags &= " -lm -lpthread"*/
        Concat((object_ptr)&_l_flags_45490, _l_flags_45490, _24325);
        goto L1C; // [579] 635
L1D: 

        /** 			elsif TOSX then*/
        if (_36TOSX_14729 == 0)
        {
            goto L1E; // [586] 598
        }
        else{
        }

        /** 				l_flags &= " -lresolv"*/
        Concat((object_ptr)&_l_flags_45490, _l_flags_45490, _24327);
        goto L1C; // [595] 635
L1E: 

        /** 			elsif TWINDOWS then*/
        if (_36TWINDOWS_14721 == 0)
        {
            goto L1F; // [602] 634
        }
        else{
        }

        /** 				if mno_cygwin then*/
        if (_54mno_cygwin_45338 == 0)
        {
            goto L20; // [609] 619
        }
        else{
        }

        /** 					l_flags &= " -mno-cygwin"*/
        Concat((object_ptr)&_l_flags_45490, _l_flags_45490, _24315);
L20: 

        /** 				if not con_option then*/
        if (_56con_option_42656 != 0)
        goto L21; // [623] 633

        /** 					l_flags &= " -mwindows"*/
        Concat((object_ptr)&_l_flags_45490, _l_flags_45490, _24331);
L21: 
L1F: 
L1C: 

        /** 			rc_comp = "windres -DSRCDIR=\"" & adjust_for_build_file(current_dir()) & "\" [1] -O coff -o [2]"*/
        _24334 = _9current_dir();
        _24335 = _54adjust_for_build_file(_24334);
        _24334 = NOVALUE;
        {
            int concat_list[3];

            concat_list[0] = _24336;
            concat_list[1] = _24335;
            concat_list[2] = _24333;
            Concat_N((object_ptr)&_rc_comp_45494, concat_list, 3);
        }
        DeRef(_24335);
        _24335 = NOVALUE;
        goto L22; // [650] 849

        /** 		case COMPILER_WATCOM then*/
        case 2:

        /** 			c_exe = "wcc386"*/
        RefDS(_24338);
        DeRefi(_c_exe_45487);
        _c_exe_45487 = _24338;

        /** 			l_exe = "wlink"*/
        RefDS(_24339);
        DeRefi(_l_exe_45489);
        _l_exe_45489 = _24339;

        /** 			obj_ext = "obj"*/
        RefDS(_24340);
        DeRefi(_obj_ext_45491);
        _obj_ext_45491 = _24340;

        /** 			if debug_option then*/
        if (_56debug_option_42664 == 0)
        {
            goto L23; // [681] 698
        }
        else{
        }

        /** 				c_flags = " /d3"*/
        RefDS(_24341);
        DeRef(_c_flags_45488);
        _c_flags_45488 = _24341;

        /** 				l_flags_begin &= " DEBUG ALL "*/
        Concat((object_ptr)&_l_flags_begin_45493, _l_flags_begin_45493, _24342);
L23: 

        /** 			l_flags &= sprintf(" OPTION STACK=%d ", { total_stack_size })*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _56total_stack_size_42677;
        _24345 = MAKE_SEQ(_1);
        _24346 = EPrintf(-9999999, _24344, _24345);
        DeRefDS(_24345);
        _24345 = NOVALUE;
        Concat((object_ptr)&_l_flags_45490, _l_flags_45490, _24346);
        DeRefDS(_24346);
        _24346 = NOVALUE;

        /** 			l_flags &= sprintf(" COMMIT STACK=%d ", { total_stack_size })*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _56total_stack_size_42677;
        _24349 = MAKE_SEQ(_1);
        _24350 = EPrintf(-9999999, _24348, _24349);
        DeRefDS(_24349);
        _24349 = NOVALUE;
        Concat((object_ptr)&_l_flags_45490, _l_flags_45490, _24350);
        DeRefDS(_24350);
        _24350 = NOVALUE;

        /** 			l_flags &= " OPTION QUIET OPTION ELIMINATE OPTION CASEEXACT"*/
        Concat((object_ptr)&_l_flags_45490, _l_flags_45490, _24352);

        /** 			if dll_option then*/
        if (_56dll_option_42654 == 0)
        {
            goto L24; // [740] 766
        }
        else{
        }

        /** 				c_flags &= " /bd /bt=nt /mf /w0 /zq /j /zp4 /fp5 /fpi87 /5r /otimra /s /I" & adjust_for_build_file(compile_dir) */
        Ref(_compile_dir_45574);
        _24355 = _54adjust_for_build_file(_compile_dir_45574);
        if (IS_SEQUENCE(_24354) && IS_ATOM(_24355)) {
            Ref(_24355);
            Append(&_24356, _24354, _24355);
        }
        else if (IS_ATOM(_24354) && IS_SEQUENCE(_24355)) {
        }
        else {
            Concat((object_ptr)&_24356, _24354, _24355);
        }
        DeRef(_24355);
        _24355 = NOVALUE;
        Concat((object_ptr)&_c_flags_45488, _c_flags_45488, _24356);
        DeRefDS(_24356);
        _24356 = NOVALUE;

        /** 				l_flags &= " SYSTEM NT_DLL initinstance terminstance"*/
        Concat((object_ptr)&_l_flags_45490, _l_flags_45490, _24358);
        goto L25; // [763] 804
L24: 

        /** 				c_flags &= " /bt=nt /mf /w0 /zq /j /zp4 /fp5 /fpi87 /5r /otimra /s /I" & adjust_for_build_file(compile_dir)*/
        Ref(_compile_dir_45574);
        _24361 = _54adjust_for_build_file(_compile_dir_45574);
        if (IS_SEQUENCE(_24360) && IS_ATOM(_24361)) {
            Ref(_24361);
            Append(&_24362, _24360, _24361);
        }
        else if (IS_ATOM(_24360) && IS_SEQUENCE(_24361)) {
        }
        else {
            Concat((object_ptr)&_24362, _24360, _24361);
        }
        DeRef(_24361);
        _24361 = NOVALUE;
        Concat((object_ptr)&_c_flags_45488, _c_flags_45488, _24362);
        DeRefDS(_24362);
        _24362 = NOVALUE;

        /** 				if con_option then*/
        if (_56con_option_42656 == 0)
        {
            goto L26; // [784] 796
        }
        else{
        }

        /** 					l_flags = " SYSTEM NT" & l_flags*/
        Concat((object_ptr)&_l_flags_45490, _24364, _l_flags_45490);
        goto L27; // [793] 803
L26: 

        /** 					l_flags = " SYSTEM NT_WIN RUNTIME WINDOWS=4.0" & l_flags*/
        Concat((object_ptr)&_l_flags_45490, _24366, _l_flags_45490);
L27: 
L25: 

        /** 			l_flags &= sprintf(" FILE %s", { (user_library) })*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_56user_library_42666);
        *((int *)(_2+4)) = _56user_library_42666;
        _24369 = MAKE_SEQ(_1);
        _24370 = EPrintf(-9999999, _24368, _24369);
        DeRefDS(_24369);
        _24369 = NOVALUE;
        Concat((object_ptr)&_l_flags_45490, _l_flags_45490, _24370);
        DeRefDS(_24370);
        _24370 = NOVALUE;

        /** 			rc_comp = "wrc -DSRCDIR=\"" & adjust_for_build_file(current_dir()) & "\" -q -fo=[2] -ad [1] [3]"*/
        _24373 = _9current_dir();
        _24374 = _54adjust_for_build_file(_24373);
        _24373 = NOVALUE;
        {
            int concat_list[3];

            concat_list[0] = _24375;
            concat_list[1] = _24374;
            concat_list[2] = _24372;
            Concat_N((object_ptr)&_rc_comp_45494, concat_list, 3);
        }
        DeRef(_24374);
        _24374 = NOVALUE;
        goto L22; // [835] 849

        /** 		case else*/
        default:

        /** 			CompileErr(43)*/
        RefDS(_22682);
        _43CompileErr(43, _22682, 0);
    ;}L22: 

    /** 	if length(cflags) then*/
    if (IS_SEQUENCE(_54cflags_45334)){
            _24377 = SEQ_PTR(_54cflags_45334)->length;
    }
    else {
        _24377 = 1;
    }
    if (_24377 == 0)
    {
        _24377 = NOVALUE;
        goto L28; // [856] 869
    }
    else{
        _24377 = NOVALUE;
    }

    /** 		c_flags = cflags*/
    RefDS(_54cflags_45334);
    DeRef(_c_flags_45488);
    _c_flags_45488 = _54cflags_45334;
L28: 

    /** 	if length(lflags) then*/
    if (IS_SEQUENCE(_54lflags_45335)){
            _24378 = SEQ_PTR(_54lflags_45335)->length;
    }
    else {
        _24378 = 1;
    }
    if (_24378 == 0)
    {
        _24378 = NOVALUE;
        goto L29; // [876] 896
    }
    else{
        _24378 = NOVALUE;
    }

    /** 		l_flags = lflags*/
    RefDS(_54lflags_45335);
    DeRef(_l_flags_45490);
    _l_flags_45490 = _54lflags_45335;

    /** 		l_flags_begin = ""*/
    RefDS(_22682);
    DeRefi(_l_flags_begin_45493);
    _l_flags_begin_45493 = _22682;
L29: 

    /** 	return { */
    _1 = NewS1(8);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_c_exe_45487);
    *((int *)(_2+4)) = _c_exe_45487;
    RefDS(_c_flags_45488);
    *((int *)(_2+8)) = _c_flags_45488;
    RefDS(_l_exe_45489);
    *((int *)(_2+12)) = _l_exe_45489;
    RefDS(_l_flags_45490);
    *((int *)(_2+16)) = _l_flags_45490;
    RefDS(_obj_ext_45491);
    *((int *)(_2+20)) = _obj_ext_45491;
    RefDS(_exe_ext_45492);
    *((int *)(_2+24)) = _exe_ext_45492;
    RefDS(_l_flags_begin_45493);
    *((int *)(_2+28)) = _l_flags_begin_45493;
    RefDS(_rc_comp_45494);
    *((int *)(_2+32)) = _rc_comp_45494;
    _24379 = MAKE_SEQ(_1);
    DeRefDSi(_c_exe_45487);
    DeRefDS(_c_flags_45488);
    DeRefDSi(_l_exe_45489);
    DeRefDS(_l_flags_45490);
    DeRefDSi(_obj_ext_45491);
    DeRefDSi(_exe_ext_45492);
    DeRefDSi(_l_flags_begin_45493);
    DeRefDS(_rc_comp_45494);
    DeRef(_l_names_45495);
    DeRefi(_l_ext_45496);
    DeRefi(_t_slash_45497);
    DeRef(_compile_dir_45574);
    return _24379;
    ;
}


void _54ensure_exename(int _ext_45710)
{
    int _24386 = NOVALUE;
    int _24385 = NOVALUE;
    int _24384 = NOVALUE;
    int _24383 = NOVALUE;
    int _24381 = NOVALUE;
    int _24380 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(exe_name[D_ALTNAME]) = 0 then*/
    _2 = (int)SEQ_PTR(_54exe_name_45319);
    _24380 = (int)*(((s1_ptr)_2)->base + 11);
    if (IS_SEQUENCE(_24380)){
            _24381 = SEQ_PTR(_24380)->length;
    }
    else {
        _24381 = 1;
    }
    _24380 = NOVALUE;
    if (_24381 != 0)
    goto L1; // [16] 67

    /** 		exe_name[D_NAME] = current_dir() & SLASH & file0 & ext*/
    _24383 = _9current_dir();
    {
        int concat_list[4];

        concat_list[0] = _ext_45710;
        concat_list[1] = _56file0_44491;
        concat_list[2] = 92;
        concat_list[3] = _24383;
        Concat_N((object_ptr)&_24384, concat_list, 4);
    }
    DeRef(_24383);
    _24383 = NOVALUE;
    _2 = (int)SEQ_PTR(_54exe_name_45319);
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _24384;
    if( _1 != _24384 ){
        DeRef(_1);
    }
    _24384 = NOVALUE;

    /** 		exe_name[D_ALTNAME] = adjust_for_command_line_passing(exe_name[D_NAME])*/
    _2 = (int)SEQ_PTR(_54exe_name_45319);
    _24385 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_24385);
    _24386 = _54adjust_for_command_line_passing(_24385);
    _24385 = NOVALUE;
    _2 = (int)SEQ_PTR(_54exe_name_45319);
    _2 = (int)(((s1_ptr)_2)->base + 11);
    _1 = *(int *)_2;
    *(int *)_2 = _24386;
    if( _1 != _24386 ){
        DeRef(_1);
    }
    _24386 = NOVALUE;
L1: 

    /** end procedure*/
    DeRefDS(_ext_45710);
    _24380 = NOVALUE;
    return;
    ;
}


void _54write_objlink_file()
{
    int _settings_45728 = NOVALUE;
    int _fh_45730 = NOVALUE;
    int _s_45778 = NOVALUE;
    int _24435 = NOVALUE;
    int _24433 = NOVALUE;
    int _24432 = NOVALUE;
    int _24431 = NOVALUE;
    int _24430 = NOVALUE;
    int _24429 = NOVALUE;
    int _24428 = NOVALUE;
    int _24427 = NOVALUE;
    int _24426 = NOVALUE;
    int _24425 = NOVALUE;
    int _24424 = NOVALUE;
    int _24423 = NOVALUE;
    int _24422 = NOVALUE;
    int _24420 = NOVALUE;
    int _24419 = NOVALUE;
    int _24418 = NOVALUE;
    int _24417 = NOVALUE;
    int _24415 = NOVALUE;
    int _24414 = NOVALUE;
    int _24413 = NOVALUE;
    int _24412 = NOVALUE;
    int _24411 = NOVALUE;
    int _24410 = NOVALUE;
    int _24409 = NOVALUE;
    int _24408 = NOVALUE;
    int _24407 = NOVALUE;
    int _24404 = NOVALUE;
    int _24403 = NOVALUE;
    int _24400 = NOVALUE;
    int _24399 = NOVALUE;
    int _24398 = NOVALUE;
    int _24397 = NOVALUE;
    int _24396 = NOVALUE;
    int _24394 = NOVALUE;
    int _24393 = NOVALUE;
    int _24392 = NOVALUE;
    int _24389 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence settings = setup_build()*/
    _0 = _settings_45728;
    _settings_45728 = _54setup_build();
    DeRef(_0);

    /** 	integer fh = open(output_dir & file0 & ".lnk", "wb")*/
    {
        int concat_list[3];

        concat_list[0] = _24388;
        concat_list[1] = _56file0_44491;
        concat_list[2] = _56output_dir_42676;
        Concat_N((object_ptr)&_24389, concat_list, 3);
    }
    _fh_45730 = EOpen(_24389, _24390, 0);
    DeRefDS(_24389);
    _24389 = NOVALUE;

    /** 	ensure_exename(settings[SETUP_EXE_EXT])*/
    _2 = (int)SEQ_PTR(_settings_45728);
    _24392 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_24392);
    _54ensure_exename(_24392);
    _24392 = NOVALUE;

    /** 	if length(settings[SETUP_LFLAGS_BEGIN]) > 0 then*/
    _2 = (int)SEQ_PTR(_settings_45728);
    _24393 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_24393)){
            _24394 = SEQ_PTR(_24393)->length;
    }
    else {
        _24394 = 1;
    }
    _24393 = NOVALUE;
    if (_24394 <= 0)
    goto L1; // [47] 69

    /** 		puts(fh, settings[SETUP_LFLAGS_BEGIN] & HOSTNL)*/
    _2 = (int)SEQ_PTR(_settings_45728);
    _24396 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_24396) && IS_ATOM(_36HOSTNL_14736)) {
    }
    else if (IS_ATOM(_24396) && IS_SEQUENCE(_36HOSTNL_14736)) {
        Ref(_24396);
        Prepend(&_24397, _36HOSTNL_14736, _24396);
    }
    else {
        Concat((object_ptr)&_24397, _24396, _36HOSTNL_14736);
        _24396 = NOVALUE;
    }
    _24396 = NOVALUE;
    EPuts(_fh_45730, _24397); // DJP 
    DeRefDS(_24397);
    _24397 = NOVALUE;
L1: 

    /** 	for i = 1 to length(generated_files) do*/
    if (IS_SEQUENCE(_56generated_files_42658)){
            _24398 = SEQ_PTR(_56generated_files_42658)->length;
    }
    else {
        _24398 = 1;
    }
    {
        int _i_45746;
        _i_45746 = 1;
L2: 
        if (_i_45746 > _24398){
            goto L3; // [76] 140
        }

        /** 		if match(".o", generated_files[i]) then*/
        _2 = (int)SEQ_PTR(_56generated_files_42658);
        _24399 = (int)*(((s1_ptr)_2)->base + _i_45746);
        _24400 = e_match_from(_23728, _24399, 1);
        _24399 = NOVALUE;
        if (_24400 == 0)
        {
            _24400 = NOVALUE;
            goto L4; // [96] 133
        }
        else{
            _24400 = NOVALUE;
        }

        /** 			if compiler_type = COMPILER_WATCOM then*/
        if (_54compiler_type_45317 != 2)
        goto L5; // [105] 115

        /** 				puts(fh, "FILE ")*/
        EPuts(_fh_45730, _24402); // DJP 
L5: 

        /** 			puts(fh, generated_files[i] & HOSTNL)*/
        _2 = (int)SEQ_PTR(_56generated_files_42658);
        _24403 = (int)*(((s1_ptr)_2)->base + _i_45746);
        Concat((object_ptr)&_24404, _24403, _36HOSTNL_14736);
        _24403 = NOVALUE;
        _24403 = NOVALUE;
        EPuts(_fh_45730, _24404); // DJP 
        DeRefDS(_24404);
        _24404 = NOVALUE;
L4: 

        /** 	end for*/
        _i_45746 = _i_45746 + 1;
        goto L2; // [135] 83
L3: 
        ;
    }

    /** 	if compiler_type = COMPILER_WATCOM then*/
    if (_54compiler_type_45317 != 2)
    goto L6; // [146] 175

    /** 		printf(fh, "NAME '%s'" & HOSTNL, { exe_name[D_ALTNAME] })*/
    Concat((object_ptr)&_24407, _24406, _36HOSTNL_14736);
    _2 = (int)SEQ_PTR(_54exe_name_45319);
    _24408 = (int)*(((s1_ptr)_2)->base + 11);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24408);
    *((int *)(_2+4)) = _24408;
    _24409 = MAKE_SEQ(_1);
    _24408 = NOVALUE;
    EPrintf(_fh_45730, _24407, _24409);
    DeRefDS(_24407);
    _24407 = NOVALUE;
    DeRefDS(_24409);
    _24409 = NOVALUE;
L6: 

    /** 	puts(fh, trim(settings[SETUP_LFLAGS] & HOSTNL))*/
    _2 = (int)SEQ_PTR(_settings_45728);
    _24410 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_SEQUENCE(_24410) && IS_ATOM(_36HOSTNL_14736)) {
    }
    else if (IS_ATOM(_24410) && IS_SEQUENCE(_36HOSTNL_14736)) {
        Ref(_24410);
        Prepend(&_24411, _36HOSTNL_14736, _24410);
    }
    else {
        Concat((object_ptr)&_24411, _24410, _36HOSTNL_14736);
        _24410 = NOVALUE;
    }
    _24410 = NOVALUE;
    RefDS(_4443);
    _24412 = _4trim(_24411, _4443, 0);
    _24411 = NOVALUE;
    EPuts(_fh_45730, _24412); // DJP 
    DeRef(_24412);
    _24412 = NOVALUE;

    /** 	if compiler_type = COMPILER_WATCOM and dll_option then*/
    _24413 = (_54compiler_type_45317 == 2);
    if (_24413 == 0) {
        goto L7; // [208] 375
    }
    if (_56dll_option_42654 == 0)
    {
        goto L7; // [215] 375
    }
    else{
    }

    /** 		puts(fh, HOSTNL)*/
    EPuts(_fh_45730, _36HOSTNL_14736); // DJP 

    /** 		object s = SymTab[TopLevelSub][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _24415 = (int)*(((s1_ptr)_2)->base + _25TopLevelSub_12269);
    DeRef(_s_45778);
    _2 = (int)SEQ_PTR(_24415);
    _s_45778 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_s_45778);
    _24415 = NOVALUE;

    /** 		while s do*/
L8: 
    if (_s_45778 <= 0) {
        if (_s_45778 == 0) {
            goto L9; // [246] 374
        }
        else {
            if (!IS_ATOM_INT(_s_45778) && DBL_PTR(_s_45778)->dbl == 0.0){
                goto L9; // [246] 374
            }
        }
    }

    /** 			if eu:find(SymTab[s][S_TOKEN], RTN_TOKS) then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_s_45778)){
        _24417 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_s_45778)->dbl));
    }
    else{
        _24417 = (int)*(((s1_ptr)_2)->base + _s_45778);
    }
    _2 = (int)SEQ_PTR(_24417);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _24418 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _24418 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _24417 = NOVALUE;
    _24419 = find_from(_24418, _28RTN_TOKS_11857, 1);
    _24418 = NOVALUE;
    if (_24419 == 0)
    {
        _24419 = NOVALUE;
        goto LA; // [270] 355
    }
    else{
        _24419 = NOVALUE;
    }

    /** 				if is_exported( s ) then*/
    Ref(_s_45778);
    _24420 = _56is_exported(_s_45778);
    if (_24420 == 0) {
        DeRef(_24420);
        _24420 = NOVALUE;
        goto LB; // [279] 354
    }
    else {
        if (!IS_ATOM_INT(_24420) && DBL_PTR(_24420)->dbl == 0.0){
            DeRef(_24420);
            _24420 = NOVALUE;
            goto LB; // [279] 354
        }
        DeRef(_24420);
        _24420 = NOVALUE;
    }
    DeRef(_24420);
    _24420 = NOVALUE;

    /** 					printf(fh, "EXPORT %s='__%d%s@%d'" & HOSTNL,*/
    Concat((object_ptr)&_24422, _24421, _36HOSTNL_14736);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_s_45778)){
        _24423 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_s_45778)->dbl));
    }
    else{
        _24423 = (int)*(((s1_ptr)_2)->base + _s_45778);
    }
    _2 = (int)SEQ_PTR(_24423);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _24424 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _24424 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _24423 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_s_45778)){
        _24425 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_s_45778)->dbl));
    }
    else{
        _24425 = (int)*(((s1_ptr)_2)->base + _s_45778);
    }
    _2 = (int)SEQ_PTR(_24425);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _24426 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _24426 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _24425 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_s_45778)){
        _24427 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_s_45778)->dbl));
    }
    else{
        _24427 = (int)*(((s1_ptr)_2)->base + _s_45778);
    }
    _2 = (int)SEQ_PTR(_24427);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _24428 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _24428 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _24427 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_s_45778)){
        _24429 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_s_45778)->dbl));
    }
    else{
        _24429 = (int)*(((s1_ptr)_2)->base + _s_45778);
    }
    _2 = (int)SEQ_PTR(_24429);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _24430 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _24430 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    _24429 = NOVALUE;
    if (IS_ATOM_INT(_24430)) {
        if (_24430 == (short)_24430)
        _24431 = _24430 * 4;
        else
        _24431 = NewDouble(_24430 * (double)4);
    }
    else {
        _24431 = binary_op(MULTIPLY, _24430, 4);
    }
    _24430 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24424);
    *((int *)(_2+4)) = _24424;
    Ref(_24426);
    *((int *)(_2+8)) = _24426;
    Ref(_24428);
    *((int *)(_2+12)) = _24428;
    *((int *)(_2+16)) = _24431;
    _24432 = MAKE_SEQ(_1);
    _24431 = NOVALUE;
    _24428 = NOVALUE;
    _24426 = NOVALUE;
    _24424 = NOVALUE;
    EPrintf(_fh_45730, _24422, _24432);
    DeRefDS(_24422);
    _24422 = NOVALUE;
    DeRefDS(_24432);
    _24432 = NOVALUE;
LB: 
LA: 

    /** 			s = SymTab[s][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_s_45778)){
        _24433 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_s_45778)->dbl));
    }
    else{
        _24433 = (int)*(((s1_ptr)_2)->base + _s_45778);
    }
    DeRef(_s_45778);
    _2 = (int)SEQ_PTR(_24433);
    _s_45778 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_s_45778);
    _24433 = NOVALUE;

    /** 		end while*/
    goto L8; // [371] 246
L9: 
L7: 
    DeRef(_s_45778);
    _s_45778 = NOVALUE;

    /** 	close(fh)*/
    EClose(_fh_45730);

    /** 	generated_files = append(generated_files, file0 & ".lnk")*/
    Concat((object_ptr)&_24435, _56file0_44491, _24388);
    RefDS(_24435);
    Append(&_56generated_files_42658, _56generated_files_42658, _24435);
    DeRefDS(_24435);
    _24435 = NOVALUE;

    /** end procedure*/
    DeRef(_settings_45728);
    _24393 = NOVALUE;
    DeRef(_24413);
    _24413 = NOVALUE;
    return;
    ;
}


void _54write_makefile_srcobj_list(int _fh_45827)
{
    int _24460 = NOVALUE;
    int _24459 = NOVALUE;
    int _24458 = NOVALUE;
    int _24457 = NOVALUE;
    int _24456 = NOVALUE;
    int _24454 = NOVALUE;
    int _24453 = NOVALUE;
    int _24452 = NOVALUE;
    int _24451 = NOVALUE;
    int _24450 = NOVALUE;
    int _24449 = NOVALUE;
    int _24448 = NOVALUE;
    int _24446 = NOVALUE;
    int _24445 = NOVALUE;
    int _24443 = NOVALUE;
    int _24442 = NOVALUE;
    int _24441 = NOVALUE;
    int _24440 = NOVALUE;
    int _24439 = NOVALUE;
    int _24438 = NOVALUE;
    int _0, _1, _2;
    

    /** 	printf(fh, "%s_SOURCES =", { upper(file0) })*/
    RefDS(_56file0_44491);
    _24438 = _4upper(_56file0_44491);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _24438;
    _24439 = MAKE_SEQ(_1);
    _24438 = NOVALUE;
    EPrintf(_fh_45827, _24437, _24439);
    DeRefDS(_24439);
    _24439 = NOVALUE;

    /** 	for i = 1 to length(generated_files) do*/
    if (IS_SEQUENCE(_56generated_files_42658)){
            _24440 = SEQ_PTR(_56generated_files_42658)->length;
    }
    else {
        _24440 = 1;
    }
    {
        int _i_45834;
        _i_45834 = 1;
L1: 
        if (_i_45834 > _24440){
            goto L2; // [26] 75
        }

        /** 		if generated_files[i][$] = 'c' then*/
        _2 = (int)SEQ_PTR(_56generated_files_42658);
        _24441 = (int)*(((s1_ptr)_2)->base + _i_45834);
        if (IS_SEQUENCE(_24441)){
                _24442 = SEQ_PTR(_24441)->length;
        }
        else {
            _24442 = 1;
        }
        _2 = (int)SEQ_PTR(_24441);
        _24443 = (int)*(((s1_ptr)_2)->base + _24442);
        _24441 = NOVALUE;
        if (binary_op_a(NOTEQ, _24443, 99)){
            _24443 = NOVALUE;
            goto L3; // [48] 68
        }
        _24443 = NOVALUE;

        /** 			puts(fh, " " & generated_files[i])*/
        _2 = (int)SEQ_PTR(_56generated_files_42658);
        _24445 = (int)*(((s1_ptr)_2)->base + _i_45834);
        Concat((object_ptr)&_24446, _23968, _24445);
        _24445 = NOVALUE;
        EPuts(_fh_45827, _24446); // DJP 
        DeRefDS(_24446);
        _24446 = NOVALUE;
L3: 

        /** 	end for*/
        _i_45834 = _i_45834 + 1;
        goto L1; // [70] 33
L2: 
        ;
    }

    /** 	puts(fh, HOSTNL)*/
    EPuts(_fh_45827, _36HOSTNL_14736); // DJP 

    /** 	printf(fh, "%s_OBJECTS =", { upper(file0) })*/
    RefDS(_56file0_44491);
    _24448 = _4upper(_56file0_44491);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _24448;
    _24449 = MAKE_SEQ(_1);
    _24448 = NOVALUE;
    EPrintf(_fh_45827, _24447, _24449);
    DeRefDS(_24449);
    _24449 = NOVALUE;

    /** 	for i = 1 to length(generated_files) do*/
    if (IS_SEQUENCE(_56generated_files_42658)){
            _24450 = SEQ_PTR(_56generated_files_42658)->length;
    }
    else {
        _24450 = 1;
    }
    {
        int _i_45853;
        _i_45853 = 1;
L4: 
        if (_i_45853 > _24450){
            goto L5; // [105] 151
        }

        /** 		if match(".o", generated_files[i]) then*/
        _2 = (int)SEQ_PTR(_56generated_files_42658);
        _24451 = (int)*(((s1_ptr)_2)->base + _i_45853);
        _24452 = e_match_from(_23728, _24451, 1);
        _24451 = NOVALUE;
        if (_24452 == 0)
        {
            _24452 = NOVALUE;
            goto L6; // [125] 144
        }
        else{
            _24452 = NOVALUE;
        }

        /** 			puts(fh, " " & generated_files[i])*/
        _2 = (int)SEQ_PTR(_56generated_files_42658);
        _24453 = (int)*(((s1_ptr)_2)->base + _i_45853);
        Concat((object_ptr)&_24454, _23968, _24453);
        _24453 = NOVALUE;
        EPuts(_fh_45827, _24454); // DJP 
        DeRefDS(_24454);
        _24454 = NOVALUE;
L6: 

        /** 	end for*/
        _i_45853 = _i_45853 + 1;
        goto L4; // [146] 112
L5: 
        ;
    }

    /** 	puts(fh, HOSTNL)*/
    EPuts(_fh_45827, _36HOSTNL_14736); // DJP 

    /** 	printf(fh, "%s_GENERATED_FILES = ", { upper(file0) })*/
    RefDS(_56file0_44491);
    _24456 = _4upper(_56file0_44491);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _24456;
    _24457 = MAKE_SEQ(_1);
    _24456 = NOVALUE;
    EPrintf(_fh_45827, _24455, _24457);
    DeRefDS(_24457);
    _24457 = NOVALUE;

    /** 	for i = 1 to length(generated_files) do*/
    if (IS_SEQUENCE(_56generated_files_42658)){
            _24458 = SEQ_PTR(_56generated_files_42658)->length;
    }
    else {
        _24458 = 1;
    }
    {
        int _i_45870;
        _i_45870 = 1;
L7: 
        if (_i_45870 > _24458){
            goto L8; // [181] 210
        }

        /** 		puts(fh, " " & generated_files[i])*/
        _2 = (int)SEQ_PTR(_56generated_files_42658);
        _24459 = (int)*(((s1_ptr)_2)->base + _i_45870);
        Concat((object_ptr)&_24460, _23968, _24459);
        _24459 = NOVALUE;
        EPuts(_fh_45827, _24460); // DJP 
        DeRefDS(_24460);
        _24460 = NOVALUE;

        /** 	end for*/
        _i_45870 = _i_45870 + 1;
        goto L7; // [205] 188
L8: 
        ;
    }

    /** 	puts(fh, HOSTNL)*/
    EPuts(_fh_45827, _36HOSTNL_14736); // DJP 

    /** end procedure*/
    return;
    ;
}


int _54windows_to_mingw_path(int _s_45879)
{
    int _24462 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef TEST_FOR_WIN9X_ON_MING then*/

    /** 	return search:find_replace('\\',s,'/')*/
    RefDS(_s_45879);
    _24462 = _7find_replace(92, _s_45879, 47, 0);
    DeRefDS(_s_45879);
    return _24462;
    ;
}


void _54write_makefile_full()
{
    int _settings_45884 = NOVALUE;
    int _fh_45887 = NOVALUE;
    int _24586 = NOVALUE;
    int _24584 = NOVALUE;
    int _24582 = NOVALUE;
    int _24581 = NOVALUE;
    int _24580 = NOVALUE;
    int _24579 = NOVALUE;
    int _24578 = NOVALUE;
    int _24576 = NOVALUE;
    int _24575 = NOVALUE;
    int _24573 = NOVALUE;
    int _24572 = NOVALUE;
    int _24571 = NOVALUE;
    int _24570 = NOVALUE;
    int _24568 = NOVALUE;
    int _24567 = NOVALUE;
    int _24565 = NOVALUE;
    int _24564 = NOVALUE;
    int _24562 = NOVALUE;
    int _24561 = NOVALUE;
    int _24560 = NOVALUE;
    int _24559 = NOVALUE;
    int _24558 = NOVALUE;
    int _24557 = NOVALUE;
    int _24556 = NOVALUE;
    int _24555 = NOVALUE;
    int _24553 = NOVALUE;
    int _24552 = NOVALUE;
    int _24551 = NOVALUE;
    int _24550 = NOVALUE;
    int _24549 = NOVALUE;
    int _24548 = NOVALUE;
    int _24547 = NOVALUE;
    int _24546 = NOVALUE;
    int _24545 = NOVALUE;
    int _24544 = NOVALUE;
    int _24543 = NOVALUE;
    int _24542 = NOVALUE;
    int _24541 = NOVALUE;
    int _24539 = NOVALUE;
    int _24537 = NOVALUE;
    int _24535 = NOVALUE;
    int _24534 = NOVALUE;
    int _24533 = NOVALUE;
    int _24532 = NOVALUE;
    int _24531 = NOVALUE;
    int _24530 = NOVALUE;
    int _24529 = NOVALUE;
    int _24528 = NOVALUE;
    int _24527 = NOVALUE;
    int _24526 = NOVALUE;
    int _24525 = NOVALUE;
    int _24524 = NOVALUE;
    int _24523 = NOVALUE;
    int _24522 = NOVALUE;
    int _24520 = NOVALUE;
    int _24519 = NOVALUE;
    int _24518 = NOVALUE;
    int _24517 = NOVALUE;
    int _24516 = NOVALUE;
    int _24515 = NOVALUE;
    int _24514 = NOVALUE;
    int _24513 = NOVALUE;
    int _24512 = NOVALUE;
    int _24510 = NOVALUE;
    int _24509 = NOVALUE;
    int _24508 = NOVALUE;
    int _24507 = NOVALUE;
    int _24505 = NOVALUE;
    int _24504 = NOVALUE;
    int _24503 = NOVALUE;
    int _24502 = NOVALUE;
    int _24501 = NOVALUE;
    int _24500 = NOVALUE;
    int _24498 = NOVALUE;
    int _24497 = NOVALUE;
    int _24496 = NOVALUE;
    int _24495 = NOVALUE;
    int _24494 = NOVALUE;
    int _24493 = NOVALUE;
    int _24492 = NOVALUE;
    int _24490 = NOVALUE;
    int _24489 = NOVALUE;
    int _24488 = NOVALUE;
    int _24487 = NOVALUE;
    int _24484 = NOVALUE;
    int _24483 = NOVALUE;
    int _24482 = NOVALUE;
    int _24479 = NOVALUE;
    int _24478 = NOVALUE;
    int _24477 = NOVALUE;
    int _24475 = NOVALUE;
    int _24474 = NOVALUE;
    int _24473 = NOVALUE;
    int _24471 = NOVALUE;
    int _24470 = NOVALUE;
    int _24469 = NOVALUE;
    int _24466 = NOVALUE;
    int _24464 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence settings = setup_build()*/
    _0 = _settings_45884;
    _settings_45884 = _54setup_build();
    DeRef(_0);

    /** 	ensure_exename(settings[SETUP_EXE_EXT])*/
    _2 = (int)SEQ_PTR(_settings_45884);
    _24464 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_24464);
    _54ensure_exename(_24464);
    _24464 = NOVALUE;

    /** 	integer fh = open(output_dir & file0 & ".mak", "wb")*/
    {
        int concat_list[3];

        concat_list[0] = _24465;
        concat_list[1] = _56file0_44491;
        concat_list[2] = _56output_dir_42676;
        Concat_N((object_ptr)&_24466, concat_list, 3);
    }
    _fh_45887 = EOpen(_24466, _24390, 0);
    DeRefDS(_24466);
    _24466 = NOVALUE;

    /** 	printf(fh, "CC     = %s" & HOSTNL, { settings[SETUP_CEXE] })*/
    Concat((object_ptr)&_24469, _24468, _36HOSTNL_14736);
    _2 = (int)SEQ_PTR(_settings_45884);
    _24470 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24470);
    *((int *)(_2+4)) = _24470;
    _24471 = MAKE_SEQ(_1);
    _24470 = NOVALUE;
    EPrintf(_fh_45887, _24469, _24471);
    DeRefDS(_24469);
    _24469 = NOVALUE;
    DeRefDS(_24471);
    _24471 = NOVALUE;

    /** 	printf(fh, "CFLAGS = %s" & HOSTNL, { settings[SETUP_CFLAGS] })*/
    Concat((object_ptr)&_24473, _24472, _36HOSTNL_14736);
    _2 = (int)SEQ_PTR(_settings_45884);
    _24474 = (int)*(((s1_ptr)_2)->base + 2);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24474);
    *((int *)(_2+4)) = _24474;
    _24475 = MAKE_SEQ(_1);
    _24474 = NOVALUE;
    EPrintf(_fh_45887, _24473, _24475);
    DeRefDS(_24473);
    _24473 = NOVALUE;
    DeRefDS(_24475);
    _24475 = NOVALUE;

    /** 	printf(fh, "LINKER = %s" & HOSTNL, { settings[SETUP_LEXE] })*/
    Concat((object_ptr)&_24477, _24476, _36HOSTNL_14736);
    _2 = (int)SEQ_PTR(_settings_45884);
    _24478 = (int)*(((s1_ptr)_2)->base + 3);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24478);
    *((int *)(_2+4)) = _24478;
    _24479 = MAKE_SEQ(_1);
    _24478 = NOVALUE;
    EPrintf(_fh_45887, _24477, _24479);
    DeRefDS(_24477);
    _24477 = NOVALUE;
    DeRefDS(_24479);
    _24479 = NOVALUE;

    /** 	if compiler_type = COMPILER_GCC then*/
    if (_54compiler_type_45317 != 1)
    goto L1; // [108] 137

    /** 		printf(fh, "LFLAGS = %s" & HOSTNL, { settings[SETUP_LFLAGS] })*/
    Concat((object_ptr)&_24482, _24481, _36HOSTNL_14736);
    _2 = (int)SEQ_PTR(_settings_45884);
    _24483 = (int)*(((s1_ptr)_2)->base + 4);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24483);
    *((int *)(_2+4)) = _24483;
    _24484 = MAKE_SEQ(_1);
    _24483 = NOVALUE;
    EPrintf(_fh_45887, _24482, _24484);
    DeRefDS(_24482);
    _24482 = NOVALUE;
    DeRefDS(_24484);
    _24484 = NOVALUE;
    goto L2; // [134] 142
L1: 

    /** 		write_objlink_file()*/
    _54write_objlink_file();
L2: 

    /** 	write_makefile_srcobj_list(fh)*/
    _54write_makefile_srcobj_list(_fh_45887);

    /** 	puts(fh, HOSTNL)*/
    EPuts(_fh_45887, _36HOSTNL_14736); // DJP 

    /** 	if compiler_type = COMPILER_WATCOM then*/
    if (_54compiler_type_45317 != 2)
    goto L3; // [160] 593

    /** 		printf(fh, "\"%s\" : $(%s_OBJECTS) %s" & HOSTNL, { */
    Concat((object_ptr)&_24487, _24486, _36HOSTNL_14736);
    _2 = (int)SEQ_PTR(_54exe_name_45319);
    _24488 = (int)*(((s1_ptr)_2)->base + 11);
    RefDS(_56file0_44491);
    _24489 = _4upper(_56file0_44491);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24488);
    *((int *)(_2+4)) = _24488;
    *((int *)(_2+8)) = _24489;
    RefDS(_56user_library_42666);
    *((int *)(_2+12)) = _56user_library_42666;
    _24490 = MAKE_SEQ(_1);
    _24489 = NOVALUE;
    _24488 = NOVALUE;
    EPrintf(_fh_45887, _24487, _24490);
    DeRefDS(_24487);
    _24487 = NOVALUE;
    DeRefDS(_24490);
    _24490 = NOVALUE;

    /** 		printf(fh, "\t$(LINKER) @%s.lnk" & HOSTNL, { file0 })*/
    Concat((object_ptr)&_24492, _24491, _36HOSTNL_14736);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_56file0_44491);
    *((int *)(_2+4)) = _56file0_44491;
    _24493 = MAKE_SEQ(_1);
    EPrintf(_fh_45887, _24492, _24493);
    DeRefDS(_24492);
    _24492 = NOVALUE;
    DeRefDS(_24493);
    _24493 = NOVALUE;

    /** 		if length(rc_file[D_ALTNAME]) and length(settings[SETUP_RC_COMPILER]) then*/
    _2 = (int)SEQ_PTR(_54rc_file_45325);
    _24494 = (int)*(((s1_ptr)_2)->base + 11);
    if (IS_SEQUENCE(_24494)){
            _24495 = SEQ_PTR(_24494)->length;
    }
    else {
        _24495 = 1;
    }
    _24494 = NOVALUE;
    if (_24495 == 0) {
        goto L4; // [229] 295
    }
    _2 = (int)SEQ_PTR(_settings_45884);
    _24497 = (int)*(((s1_ptr)_2)->base + 8);
    if (IS_SEQUENCE(_24497)){
            _24498 = SEQ_PTR(_24497)->length;
    }
    else {
        _24498 = 1;
    }
    _24497 = NOVALUE;
    if (_24498 == 0)
    {
        _24498 = NOVALUE;
        goto L4; // [243] 295
    }
    else{
        _24498 = NOVALUE;
    }

    /** 			writef(fh, "\t" & settings[SETUP_RC_COMPILER], { rc_file[D_ALTNAME], res_file[D_ALTNAME], exe_name[D_ALTNAME] })*/
    _2 = (int)SEQ_PTR(_settings_45884);
    _24500 = (int)*(((s1_ptr)_2)->base + 8);
    if (IS_SEQUENCE(_24499) && IS_ATOM(_24500)) {
        Ref(_24500);
        Append(&_24501, _24499, _24500);
    }
    else if (IS_ATOM(_24499) && IS_SEQUENCE(_24500)) {
    }
    else {
        Concat((object_ptr)&_24501, _24499, _24500);
    }
    _24500 = NOVALUE;
    _2 = (int)SEQ_PTR(_54rc_file_45325);
    _24502 = (int)*(((s1_ptr)_2)->base + 11);
    _2 = (int)SEQ_PTR(_54res_file_45331);
    _24503 = (int)*(((s1_ptr)_2)->base + 11);
    _2 = (int)SEQ_PTR(_54exe_name_45319);
    _24504 = (int)*(((s1_ptr)_2)->base + 11);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24502);
    *((int *)(_2+4)) = _24502;
    Ref(_24503);
    *((int *)(_2+8)) = _24503;
    Ref(_24504);
    *((int *)(_2+12)) = _24504;
    _24505 = MAKE_SEQ(_1);
    _24504 = NOVALUE;
    _24503 = NOVALUE;
    _24502 = NOVALUE;
    _16writef(_fh_45887, _24501, _24505, 0);
    _24501 = NOVALUE;
    _24505 = NOVALUE;
L4: 

    /** 		puts(fh, HOSTNL)*/
    EPuts(_fh_45887, _36HOSTNL_14736); // DJP 

    /** 		printf(fh, "%s-clean : .SYMBOLIC" & HOSTNL, { file0 })*/
    Concat((object_ptr)&_24507, _24506, _36HOSTNL_14736);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_56file0_44491);
    *((int *)(_2+4)) = _56file0_44491;
    _24508 = MAKE_SEQ(_1);
    EPrintf(_fh_45887, _24507, _24508);
    DeRefDS(_24507);
    _24507 = NOVALUE;
    DeRefDS(_24508);
    _24508 = NOVALUE;

    /** 		if length(res_file[D_ALTNAME]) then*/
    _2 = (int)SEQ_PTR(_54res_file_45331);
    _24509 = (int)*(((s1_ptr)_2)->base + 11);
    if (IS_SEQUENCE(_24509)){
            _24510 = SEQ_PTR(_24509)->length;
    }
    else {
        _24510 = 1;
    }
    _24509 = NOVALUE;
    if (_24510 == 0)
    {
        _24510 = NOVALUE;
        goto L5; // [333] 361
    }
    else{
        _24510 = NOVALUE;
    }

    /** 			printf(fh, "\tdel \"%s\"" & HOSTNL, { res_file[D_ALTNAME] })*/
    Concat((object_ptr)&_24512, _24511, _36HOSTNL_14736);
    _2 = (int)SEQ_PTR(_54res_file_45331);
    _24513 = (int)*(((s1_ptr)_2)->base + 11);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24513);
    *((int *)(_2+4)) = _24513;
    _24514 = MAKE_SEQ(_1);
    _24513 = NOVALUE;
    EPrintf(_fh_45887, _24512, _24514);
    DeRefDS(_24512);
    _24512 = NOVALUE;
    DeRefDS(_24514);
    _24514 = NOVALUE;
L5: 

    /** 		for i = 1 to length(generated_files) do*/
    if (IS_SEQUENCE(_56generated_files_42658)){
            _24515 = SEQ_PTR(_56generated_files_42658)->length;
    }
    else {
        _24515 = 1;
    }
    {
        int _i_45969;
        _i_45969 = 1;
L6: 
        if (_i_45969 > _24515){
            goto L7; // [368] 421
        }

        /** 			if match(".o", generated_files[i]) then*/
        _2 = (int)SEQ_PTR(_56generated_files_42658);
        _24516 = (int)*(((s1_ptr)_2)->base + _i_45969);
        _24517 = e_match_from(_23728, _24516, 1);
        _24516 = NOVALUE;
        if (_24517 == 0)
        {
            _24517 = NOVALUE;
            goto L8; // [388] 414
        }
        else{
            _24517 = NOVALUE;
        }

        /** 				printf(fh, "\tdel \"%s\"" & HOSTNL, { generated_files[i] })*/
        Concat((object_ptr)&_24518, _24511, _36HOSTNL_14736);
        _2 = (int)SEQ_PTR(_56generated_files_42658);
        _24519 = (int)*(((s1_ptr)_2)->base + _i_45969);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_24519);
        *((int *)(_2+4)) = _24519;
        _24520 = MAKE_SEQ(_1);
        _24519 = NOVALUE;
        EPrintf(_fh_45887, _24518, _24520);
        DeRefDS(_24518);
        _24518 = NOVALUE;
        DeRefDS(_24520);
        _24520 = NOVALUE;
L8: 

        /** 		end for*/
        _i_45969 = _i_45969 + 1;
        goto L6; // [416] 375
L7: 
        ;
    }

    /** 		puts(fh, HOSTNL)*/
    EPuts(_fh_45887, _36HOSTNL_14736); // DJP 

    /** 		printf(fh, "%s-clean-all : .SYMBOLIC" & HOSTNL, { file0 })*/
    Concat((object_ptr)&_24522, _24521, _36HOSTNL_14736);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_56file0_44491);
    *((int *)(_2+4)) = _56file0_44491;
    _24523 = MAKE_SEQ(_1);
    EPrintf(_fh_45887, _24522, _24523);
    DeRefDS(_24522);
    _24522 = NOVALUE;
    DeRefDS(_24523);
    _24523 = NOVALUE;

    /** 		printf(fh, "\tdel \"%s\"" & HOSTNL, { exe_name[D_ALTNAME] })*/
    Concat((object_ptr)&_24524, _24511, _36HOSTNL_14736);
    _2 = (int)SEQ_PTR(_54exe_name_45319);
    _24525 = (int)*(((s1_ptr)_2)->base + 11);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24525);
    *((int *)(_2+4)) = _24525;
    _24526 = MAKE_SEQ(_1);
    _24525 = NOVALUE;
    EPrintf(_fh_45887, _24524, _24526);
    DeRefDS(_24524);
    _24524 = NOVALUE;
    DeRefDS(_24526);
    _24526 = NOVALUE;

    /** 		if length(res_file[D_ALTNAME]) then*/
    _2 = (int)SEQ_PTR(_54res_file_45331);
    _24527 = (int)*(((s1_ptr)_2)->base + 11);
    if (IS_SEQUENCE(_24527)){
            _24528 = SEQ_PTR(_24527)->length;
    }
    else {
        _24528 = 1;
    }
    _24527 = NOVALUE;
    if (_24528 == 0)
    {
        _24528 = NOVALUE;
        goto L9; // [483] 511
    }
    else{
        _24528 = NOVALUE;
    }

    /** 			printf(fh, "\tdel \"%s\"" & HOSTNL, { res_file[D_ALTNAME] })*/
    Concat((object_ptr)&_24529, _24511, _36HOSTNL_14736);
    _2 = (int)SEQ_PTR(_54res_file_45331);
    _24530 = (int)*(((s1_ptr)_2)->base + 11);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24530);
    *((int *)(_2+4)) = _24530;
    _24531 = MAKE_SEQ(_1);
    _24530 = NOVALUE;
    EPrintf(_fh_45887, _24529, _24531);
    DeRefDS(_24529);
    _24529 = NOVALUE;
    DeRefDS(_24531);
    _24531 = NOVALUE;
L9: 

    /** 		for i = 1 to length(generated_files) do*/
    if (IS_SEQUENCE(_56generated_files_42658)){
            _24532 = SEQ_PTR(_56generated_files_42658)->length;
    }
    else {
        _24532 = 1;
    }
    {
        int _i_46002;
        _i_46002 = 1;
LA: 
        if (_i_46002 > _24532){
            goto LB; // [518] 554
        }

        /** 			printf(fh, "\tdel \"%s\"" & HOSTNL, { generated_files[i] })*/
        Concat((object_ptr)&_24533, _24511, _36HOSTNL_14736);
        _2 = (int)SEQ_PTR(_56generated_files_42658);
        _24534 = (int)*(((s1_ptr)_2)->base + _i_46002);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_24534);
        *((int *)(_2+4)) = _24534;
        _24535 = MAKE_SEQ(_1);
        _24534 = NOVALUE;
        EPrintf(_fh_45887, _24533, _24535);
        DeRefDS(_24533);
        _24533 = NOVALUE;
        DeRefDS(_24535);
        _24535 = NOVALUE;

        /** 		end for*/
        _i_46002 = _i_46002 + 1;
        goto LA; // [549] 525
LB: 
        ;
    }

    /** 		puts(fh, HOSTNL)*/
    EPuts(_fh_45887, _36HOSTNL_14736); // DJP 

    /** 		puts(fh, ".c.obj : .autodepend" & HOSTNL)*/
    Concat((object_ptr)&_24537, _24536, _36HOSTNL_14736);
    EPuts(_fh_45887, _24537); // DJP 
    DeRefDS(_24537);
    _24537 = NOVALUE;

    /** 		puts(fh, "\t$(CC) $(CFLAGS) $<" & HOSTNL)*/
    Concat((object_ptr)&_24539, _24538, _36HOSTNL_14736);
    EPuts(_fh_45887, _24539); // DJP 
    DeRefDS(_24539);
    _24539 = NOVALUE;

    /** 		puts(fh, HOSTNL)*/
    EPuts(_fh_45887, _36HOSTNL_14736); // DJP 
    goto LC; // [590] 942
L3: 

    /** 		printf(fh, "%s: $(%s_OBJECTS) %s %s" & HOSTNL, { adjust_for_build_file(exe_name[D_ALTNAME]), upper(file0), user_library, rc_file[D_ALTNAME] })*/
    Concat((object_ptr)&_24541, _24540, _36HOSTNL_14736);
    _2 = (int)SEQ_PTR(_54exe_name_45319);
    _24542 = (int)*(((s1_ptr)_2)->base + 11);
    Ref(_24542);
    _24543 = _54adjust_for_build_file(_24542);
    _24542 = NOVALUE;
    RefDS(_56file0_44491);
    _24544 = _4upper(_56file0_44491);
    _2 = (int)SEQ_PTR(_54rc_file_45325);
    _24545 = (int)*(((s1_ptr)_2)->base + 11);
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _24543;
    *((int *)(_2+8)) = _24544;
    RefDS(_56user_library_42666);
    *((int *)(_2+12)) = _56user_library_42666;
    Ref(_24545);
    *((int *)(_2+16)) = _24545;
    _24546 = MAKE_SEQ(_1);
    _24545 = NOVALUE;
    _24544 = NOVALUE;
    _24543 = NOVALUE;
    EPrintf(_fh_45887, _24541, _24546);
    DeRefDS(_24541);
    _24541 = NOVALUE;
    DeRefDS(_24546);
    _24546 = NOVALUE;

    /** 		if length(rc_file[D_ALTNAME]) then*/
    _2 = (int)SEQ_PTR(_54rc_file_45325);
    _24547 = (int)*(((s1_ptr)_2)->base + 11);
    if (IS_SEQUENCE(_24547)){
            _24548 = SEQ_PTR(_24547)->length;
    }
    else {
        _24548 = 1;
    }
    _24547 = NOVALUE;
    if (_24548 == 0)
    {
        _24548 = NOVALUE;
        goto LD; // [653] 699
    }
    else{
        _24548 = NOVALUE;
    }

    /** 			writef(fh, "\t" & settings[SETUP_RC_COMPILER] & HOSTNL, { rc_file[D_ALTNAME], res_file[D_ALTNAME] })*/
    _2 = (int)SEQ_PTR(_settings_45884);
    _24549 = (int)*(((s1_ptr)_2)->base + 8);
    {
        int concat_list[3];

        concat_list[0] = _36HOSTNL_14736;
        concat_list[1] = _24549;
        concat_list[2] = _24499;
        Concat_N((object_ptr)&_24550, concat_list, 3);
    }
    _24549 = NOVALUE;
    _2 = (int)SEQ_PTR(_54rc_file_45325);
    _24551 = (int)*(((s1_ptr)_2)->base + 11);
    _2 = (int)SEQ_PTR(_54res_file_45331);
    _24552 = (int)*(((s1_ptr)_2)->base + 11);
    Ref(_24552);
    Ref(_24551);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24551;
    ((int *)_2)[2] = _24552;
    _24553 = MAKE_SEQ(_1);
    _24552 = NOVALUE;
    _24551 = NOVALUE;
    _16writef(_fh_45887, _24550, _24553, 0);
    _24550 = NOVALUE;
    _24553 = NOVALUE;
LD: 

    /** 		printf(fh, "\t$(LINKER) -o %s $(%s_OBJECTS) %s $(LFLAGS)" & HOSTNL, {*/
    Concat((object_ptr)&_24555, _24554, _36HOSTNL_14736);
    _2 = (int)SEQ_PTR(_54exe_name_45319);
    _24556 = (int)*(((s1_ptr)_2)->base + 11);
    RefDS(_56file0_44491);
    _24557 = _4upper(_56file0_44491);
    _2 = (int)SEQ_PTR(_54res_file_45331);
    _24558 = (int)*(((s1_ptr)_2)->base + 11);
    if (IS_SEQUENCE(_24558)){
            _24559 = SEQ_PTR(_24558)->length;
    }
    else {
        _24559 = 1;
    }
    _24558 = NOVALUE;
    _2 = (int)SEQ_PTR(_54res_file_45331);
    _24560 = (int)*(((s1_ptr)_2)->base + 11);
    Ref(_24560);
    RefDS(_22682);
    _24561 = _55iif(_24559, _24560, _22682);
    _24559 = NOVALUE;
    _24560 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24556);
    *((int *)(_2+4)) = _24556;
    *((int *)(_2+8)) = _24557;
    *((int *)(_2+12)) = _24561;
    _24562 = MAKE_SEQ(_1);
    _24561 = NOVALUE;
    _24557 = NOVALUE;
    _24556 = NOVALUE;
    EPrintf(_fh_45887, _24555, _24562);
    DeRefDS(_24555);
    _24555 = NOVALUE;
    DeRefDS(_24562);
    _24562 = NOVALUE;

    /** 		puts(fh, HOSTNL)*/
    EPuts(_fh_45887, _36HOSTNL_14736); // DJP 

    /** 		printf(fh, ".PHONY: %s-clean %s-clean-all" & HOSTNL, { file0, file0 })*/
    Concat((object_ptr)&_24564, _24563, _36HOSTNL_14736);
    RefDS(_56file0_44491);
    RefDS(_56file0_44491);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _56file0_44491;
    ((int *)_2)[2] = _56file0_44491;
    _24565 = MAKE_SEQ(_1);
    EPrintf(_fh_45887, _24564, _24565);
    DeRefDS(_24564);
    _24564 = NOVALUE;
    DeRefDS(_24565);
    _24565 = NOVALUE;

    /** 		puts(fh, HOSTNL)*/
    EPuts(_fh_45887, _36HOSTNL_14736); // DJP 

    /** 		printf(fh, "%s-clean:" & HOSTNL, { file0 })*/
    Concat((object_ptr)&_24567, _24566, _36HOSTNL_14736);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_56file0_44491);
    *((int *)(_2+4)) = _56file0_44491;
    _24568 = MAKE_SEQ(_1);
    EPrintf(_fh_45887, _24567, _24568);
    DeRefDS(_24567);
    _24567 = NOVALUE;
    DeRefDS(_24568);
    _24568 = NOVALUE;

    /** 		printf(fh, "\trm -rf $(%s_OBJECTS) %s" & HOSTNL, { upper(file0), res_file[D_ALTNAME] })*/
    Concat((object_ptr)&_24570, _24569, _36HOSTNL_14736);
    RefDS(_56file0_44491);
    _24571 = _4upper(_56file0_44491);
    _2 = (int)SEQ_PTR(_54res_file_45331);
    _24572 = (int)*(((s1_ptr)_2)->base + 11);
    Ref(_24572);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24571;
    ((int *)_2)[2] = _24572;
    _24573 = MAKE_SEQ(_1);
    _24572 = NOVALUE;
    _24571 = NOVALUE;
    EPrintf(_fh_45887, _24570, _24573);
    DeRefDS(_24570);
    _24570 = NOVALUE;
    DeRefDS(_24573);
    _24573 = NOVALUE;

    /** 		puts(fh, HOSTNL)*/
    EPuts(_fh_45887, _36HOSTNL_14736); // DJP 

    /** 		printf(fh, "%s-clean-all: %s-clean" & HOSTNL, { file0, file0 })*/
    Concat((object_ptr)&_24575, _24574, _36HOSTNL_14736);
    RefDS(_56file0_44491);
    RefDS(_56file0_44491);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _56file0_44491;
    ((int *)_2)[2] = _56file0_44491;
    _24576 = MAKE_SEQ(_1);
    EPrintf(_fh_45887, _24575, _24576);
    DeRefDS(_24575);
    _24575 = NOVALUE;
    DeRefDS(_24576);
    _24576 = NOVALUE;

    /** 		printf(fh, "\trm -rf $(%s_SOURCES) %s %s" & HOSTNL, { upper(file0), res_file[D_ALTNAME], exe_name[D_ALTNAME] })*/
    Concat((object_ptr)&_24578, _24577, _36HOSTNL_14736);
    RefDS(_56file0_44491);
    _24579 = _4upper(_56file0_44491);
    _2 = (int)SEQ_PTR(_54res_file_45331);
    _24580 = (int)*(((s1_ptr)_2)->base + 11);
    _2 = (int)SEQ_PTR(_54exe_name_45319);
    _24581 = (int)*(((s1_ptr)_2)->base + 11);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _24579;
    Ref(_24580);
    *((int *)(_2+8)) = _24580;
    Ref(_24581);
    *((int *)(_2+12)) = _24581;
    _24582 = MAKE_SEQ(_1);
    _24581 = NOVALUE;
    _24580 = NOVALUE;
    _24579 = NOVALUE;
    EPrintf(_fh_45887, _24578, _24582);
    DeRefDS(_24578);
    _24578 = NOVALUE;
    DeRefDS(_24582);
    _24582 = NOVALUE;

    /** 		puts(fh, HOSTNL)*/
    EPuts(_fh_45887, _36HOSTNL_14736); // DJP 

    /** 		puts(fh, "%.o: %.c" & HOSTNL)*/
    Concat((object_ptr)&_24584, _24583, _36HOSTNL_14736);
    EPuts(_fh_45887, _24584); // DJP 
    DeRefDS(_24584);
    _24584 = NOVALUE;

    /** 		puts(fh, "\t$(CC) $(CFLAGS) $*.c -o $*.o" & HOSTNL)*/
    Concat((object_ptr)&_24586, _24585, _36HOSTNL_14736);
    EPuts(_fh_45887, _24586); // DJP 
    DeRefDS(_24586);
    _24586 = NOVALUE;

    /** 		puts(fh, HOSTNL)*/
    EPuts(_fh_45887, _36HOSTNL_14736); // DJP 
LC: 

    /** 	close(fh)*/
    EClose(_fh_45887);

    /** end procedure*/
    DeRef(_settings_45884);
    _24494 = NOVALUE;
    _24497 = NOVALUE;
    _24509 = NOVALUE;
    _24527 = NOVALUE;
    _24547 = NOVALUE;
    _24558 = NOVALUE;
    return;
    ;
}


void _54write_makefile_partial()
{
    int _settings_46111 = NOVALUE;
    int _fh_46113 = NOVALUE;
    int _24588 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence settings = setup_build()*/
    _0 = _settings_46111;
    _settings_46111 = _54setup_build();
    DeRef(_0);

    /** 	integer fh = open(output_dir & file0 & ".mak", "wb")*/
    {
        int concat_list[3];

        concat_list[0] = _24465;
        concat_list[1] = _56file0_44491;
        concat_list[2] = _56output_dir_42676;
        Concat_N((object_ptr)&_24588, concat_list, 3);
    }
    _fh_46113 = EOpen(_24588, _24390, 0);
    DeRefDS(_24588);
    _24588 = NOVALUE;

    /** 	write_makefile_srcobj_list(fh)*/
    _54write_makefile_srcobj_list(_fh_46113);

    /** 	close(fh)*/
    EClose(_fh_46113);

    /** end procedure*/
    DeRefDS(_settings_46111);
    return;
    ;
}


void _54build_direct(int _link_only_46120, int _the_file0_46121)
{
    int _cmd_46127 = NOVALUE;
    int _objs_46128 = NOVALUE;
    int _settings_46129 = NOVALUE;
    int _cwd_46131 = NOVALUE;
    int _status_46134 = NOVALUE;
    int _link_files_46163 = NOVALUE;
    int _pdone_46189 = NOVALUE;
    int _files_46235 = NOVALUE;
    int _32387 = NOVALUE;
    int _32386 = NOVALUE;
    int _32385 = NOVALUE;
    int _32384 = NOVALUE;
    int _32383 = NOVALUE;
    int _32381 = NOVALUE;
    int _24732 = NOVALUE;
    int _24731 = NOVALUE;
    int _24730 = NOVALUE;
    int _24729 = NOVALUE;
    int _24728 = NOVALUE;
    int _24727 = NOVALUE;
    int _24726 = NOVALUE;
    int _24724 = NOVALUE;
    int _24723 = NOVALUE;
    int _24722 = NOVALUE;
    int _24721 = NOVALUE;
    int _24717 = NOVALUE;
    int _24716 = NOVALUE;
    int _24715 = NOVALUE;
    int _24714 = NOVALUE;
    int _24713 = NOVALUE;
    int _24712 = NOVALUE;
    int _24711 = NOVALUE;
    int _24710 = NOVALUE;
    int _24709 = NOVALUE;
    int _24708 = NOVALUE;
    int _24707 = NOVALUE;
    int _24706 = NOVALUE;
    int _24705 = NOVALUE;
    int _24704 = NOVALUE;
    int _24703 = NOVALUE;
    int _24700 = NOVALUE;
    int _24699 = NOVALUE;
    int _24698 = NOVALUE;
    int _24697 = NOVALUE;
    int _24694 = NOVALUE;
    int _24692 = NOVALUE;
    int _24691 = NOVALUE;
    int _24690 = NOVALUE;
    int _24689 = NOVALUE;
    int _24688 = NOVALUE;
    int _24687 = NOVALUE;
    int _24684 = NOVALUE;
    int _24683 = NOVALUE;
    int _24679 = NOVALUE;
    int _24678 = NOVALUE;
    int _24677 = NOVALUE;
    int _24673 = NOVALUE;
    int _24672 = NOVALUE;
    int _24671 = NOVALUE;
    int _24670 = NOVALUE;
    int _24669 = NOVALUE;
    int _24668 = NOVALUE;
    int _24667 = NOVALUE;
    int _24666 = NOVALUE;
    int _24665 = NOVALUE;
    int _24664 = NOVALUE;
    int _24663 = NOVALUE;
    int _24662 = NOVALUE;
    int _24660 = NOVALUE;
    int _24659 = NOVALUE;
    int _24658 = NOVALUE;
    int _24657 = NOVALUE;
    int _24656 = NOVALUE;
    int _24654 = NOVALUE;
    int _24653 = NOVALUE;
    int _24652 = NOVALUE;
    int _24651 = NOVALUE;
    int _24650 = NOVALUE;
    int _24648 = NOVALUE;
    int _24646 = NOVALUE;
    int _24645 = NOVALUE;
    int _24644 = NOVALUE;
    int _24643 = NOVALUE;
    int _24641 = NOVALUE;
    int _24640 = NOVALUE;
    int _24639 = NOVALUE;
    int _24636 = NOVALUE;
    int _24635 = NOVALUE;
    int _24634 = NOVALUE;
    int _24633 = NOVALUE;
    int _24632 = NOVALUE;
    int _24631 = NOVALUE;
    int _24630 = NOVALUE;
    int _24629 = NOVALUE;
    int _24628 = NOVALUE;
    int _24627 = NOVALUE;
    int _24624 = NOVALUE;
    int _24623 = NOVALUE;
    int _24620 = NOVALUE;
    int _24618 = NOVALUE;
    int _24617 = NOVALUE;
    int _24616 = NOVALUE;
    int _24615 = NOVALUE;
    int _24612 = NOVALUE;
    int _24611 = NOVALUE;
    int _24610 = NOVALUE;
    int _24609 = NOVALUE;
    int _24607 = NOVALUE;
    int _24606 = NOVALUE;
    int _24605 = NOVALUE;
    int _24604 = NOVALUE;
    int _24603 = NOVALUE;
    int _24600 = NOVALUE;
    int _24594 = NOVALUE;
    int _24590 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_link_only_46120)) {
        _1 = (long)(DBL_PTR(_link_only_46120)->dbl);
        if (UNIQUE(DBL_PTR(_link_only_46120)) && (DBL_PTR(_link_only_46120)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_link_only_46120);
        _link_only_46120 = _1;
    }

    /** 	if length(the_file0) then*/
    if (IS_SEQUENCE(_the_file0_46121)){
            _24590 = SEQ_PTR(_the_file0_46121)->length;
    }
    else {
        _24590 = 1;
    }
    if (_24590 == 0)
    {
        _24590 = NOVALUE;
        goto L1; // [10] 22
    }
    else{
        _24590 = NOVALUE;
    }

    /** 		file0 = filebase(the_file0)*/
    RefDS(_the_file0_46121);
    _0 = _9filebase(_the_file0_46121);
    DeRef(_56file0_44491);
    _56file0_44491 = _0;
L1: 

    /** 	sequence cmd, objs = "", settings = setup_build(), cwd = current_dir()*/
    RefDS(_22682);
    DeRef(_objs_46128);
    _objs_46128 = _22682;
    _0 = _settings_46129;
    _settings_46129 = _54setup_build();
    DeRef(_0);
    _0 = _cwd_46131;
    _cwd_46131 = _9current_dir();
    DeRef(_0);

    /** 	integer status*/

    /** 	ensure_exename(settings[SETUP_EXE_EXT])*/
    _2 = (int)SEQ_PTR(_settings_46129);
    _24594 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_24594);
    _54ensure_exename(_24594);
    _24594 = NOVALUE;

    /** 	if not link_only then*/
    if (_link_only_46120 != 0)
    goto L2; // [54] 122

    /** 		switch compiler_type do*/
    _0 = _54compiler_type_45317;
    switch ( _0 ){ 

        /** 			case COMPILER_GCC then*/
        case 1:

        /** 				if not silent then*/
        if (_25silent_12398 != 0)
        goto L3; // [74] 121

        /** 					ShowMsg(1, 176, {"GCC"})*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_24599);
        *((int *)(_2+4)) = _24599;
        _24600 = MAKE_SEQ(_1);
        _44ShowMsg(1, 176, _24600, 1);
        _24600 = NOVALUE;
        goto L3; // [90] 121

        /** 			case COMPILER_WATCOM then*/
        case 2:

        /** 				write_objlink_file()*/
        _54write_objlink_file();

        /** 				if not silent then*/
        if (_25silent_12398 != 0)
        goto L4; // [104] 120

        /** 					ShowMsg(1, 176, {"Watcom"})*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_24602);
        *((int *)(_2+4)) = _24602;
        _24603 = MAKE_SEQ(_1);
        _44ShowMsg(1, 176, _24603, 1);
        _24603 = NOVALUE;
L4: 
    ;}L3: 
L2: 

    /** 	if sequence(output_dir) and length(output_dir) > 0 then*/
    _24604 = 1;
    if (_24604 == 0) {
        goto L5; // [129] 157
    }
    if (IS_SEQUENCE(_56output_dir_42676)){
            _24606 = SEQ_PTR(_56output_dir_42676)->length;
    }
    else {
        _24606 = 1;
    }
    _24607 = (_24606 > 0);
    _24606 = NOVALUE;
    if (_24607 == 0)
    {
        DeRef(_24607);
        _24607 = NOVALUE;
        goto L5; // [143] 157
    }
    else{
        DeRef(_24607);
        _24607 = NOVALUE;
    }

    /** 		chdir(output_dir)*/
    RefDS(_56output_dir_42676);
    _32387 = _9chdir(_56output_dir_42676);
    DeRef(_32387);
    _32387 = NOVALUE;
L5: 

    /** 	sequence link_files = {}*/
    RefDS(_22682);
    DeRef(_link_files_46163);
    _link_files_46163 = _22682;

    /** 	if not link_only then*/
    if (_link_only_46120 != 0)
    goto L6; // [166] 474

    /** 		for i = 1 to length(generated_files) do*/
    if (IS_SEQUENCE(_56generated_files_42658)){
            _24609 = SEQ_PTR(_56generated_files_42658)->length;
    }
    else {
        _24609 = 1;
    }
    {
        int _i_46167;
        _i_46167 = 1;
L7: 
        if (_i_46167 > _24609){
            goto L8; // [176] 471
        }

        /** 			if generated_files[i][$] = 'c' then*/
        _2 = (int)SEQ_PTR(_56generated_files_42658);
        _24610 = (int)*(((s1_ptr)_2)->base + _i_46167);
        if (IS_SEQUENCE(_24610)){
                _24611 = SEQ_PTR(_24610)->length;
        }
        else {
            _24611 = 1;
        }
        _2 = (int)SEQ_PTR(_24610);
        _24612 = (int)*(((s1_ptr)_2)->base + _24611);
        _24610 = NOVALUE;
        if (binary_op_a(NOTEQ, _24612, 99)){
            _24612 = NOVALUE;
            goto L9; // [198] 430
        }
        _24612 = NOVALUE;

        /** 				cmd = sprintf("%s %s %s", { settings[SETUP_CEXE], settings[SETUP_CFLAGS],*/
        _2 = (int)SEQ_PTR(_settings_46129);
        _24615 = (int)*(((s1_ptr)_2)->base + 1);
        _2 = (int)SEQ_PTR(_settings_46129);
        _24616 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_56generated_files_42658);
        _24617 = (int)*(((s1_ptr)_2)->base + _i_46167);
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_24615);
        *((int *)(_2+4)) = _24615;
        Ref(_24616);
        *((int *)(_2+8)) = _24616;
        RefDS(_24617);
        *((int *)(_2+12)) = _24617;
        _24618 = MAKE_SEQ(_1);
        _24617 = NOVALUE;
        _24616 = NOVALUE;
        _24615 = NOVALUE;
        DeRef(_cmd_46127);
        _cmd_46127 = EPrintf(-9999999, _24614, _24618);
        DeRefDS(_24618);
        _24618 = NOVALUE;

        /** 				link_files = append(link_files, generated_files[i])*/
        _2 = (int)SEQ_PTR(_56generated_files_42658);
        _24620 = (int)*(((s1_ptr)_2)->base + _i_46167);
        RefDS(_24620);
        Append(&_link_files_46163, _link_files_46163, _24620);
        _24620 = NOVALUE;

        /** 				if not silent then*/
        if (_25silent_12398 != 0)
        goto LA; // [248] 370

        /** 					atom pdone = 100 * (i / length(generated_files))*/
        if (IS_SEQUENCE(_56generated_files_42658)){
                _24623 = SEQ_PTR(_56generated_files_42658)->length;
        }
        else {
            _24623 = 1;
        }
        _24624 = (_i_46167 % _24623) ? NewDouble((double)_i_46167 / _24623) : (_i_46167 / _24623);
        _24623 = NOVALUE;
        DeRef(_pdone_46189);
        if (IS_ATOM_INT(_24624)) {
            if (_24624 <= INT15 && _24624 >= -INT15)
            _pdone_46189 = 100 * _24624;
            else
            _pdone_46189 = NewDouble(100 * (double)_24624);
        }
        else {
            _pdone_46189 = NewDouble((double)100 * DBL_PTR(_24624)->dbl);
        }
        DeRef(_24624);
        _24624 = NOVALUE;

        /** 					if not verbose then*/
        if (_25verbose_12401 != 0)
        goto LB; // [270] 356

        /** 						if 0 and outdated_files[i] = 0 and force_build = 0 then*/
        if (0 == 0) {
            _24627 = 0;
            goto LC; // [275] 293
        }
        _2 = (int)SEQ_PTR(_56outdated_files_42659);
        _24628 = (int)*(((s1_ptr)_2)->base + _i_46167);
        if (IS_ATOM_INT(_24628)) {
            _24629 = (_24628 == 0);
        }
        else {
            _24629 = binary_op(EQUALS, _24628, 0);
        }
        _24628 = NOVALUE;
        if (IS_ATOM_INT(_24629))
        _24627 = (_24629 != 0);
        else
        _24627 = DBL_PTR(_24629)->dbl != 0.0;
LC: 
        if (_24627 == 0) {
            goto LD; // [293] 334
        }
        _24631 = (_54force_build_45336 == 0);
        if (_24631 == 0)
        {
            DeRef(_24631);
            _24631 = NOVALUE;
            goto LD; // [304] 334
        }
        else{
            DeRef(_24631);
            _24631 = NOVALUE;
        }

        /** 							ShowMsg(1, 325, { pdone, generated_files[i] })*/
        _2 = (int)SEQ_PTR(_56generated_files_42658);
        _24632 = (int)*(((s1_ptr)_2)->base + _i_46167);
        RefDS(_24632);
        Ref(_pdone_46189);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _pdone_46189;
        ((int *)_2)[2] = _24632;
        _24633 = MAKE_SEQ(_1);
        _24632 = NOVALUE;
        _44ShowMsg(1, 325, _24633, 1);
        _24633 = NOVALUE;

        /** 							continue*/
        DeRef(_pdone_46189);
        _pdone_46189 = NOVALUE;
        goto LE; // [329] 466
        goto LF; // [331] 369
LD: 

        /** 							ShowMsg(1, 163, { pdone, generated_files[i] })*/
        _2 = (int)SEQ_PTR(_56generated_files_42658);
        _24634 = (int)*(((s1_ptr)_2)->base + _i_46167);
        RefDS(_24634);
        Ref(_pdone_46189);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _pdone_46189;
        ((int *)_2)[2] = _24634;
        _24635 = MAKE_SEQ(_1);
        _24634 = NOVALUE;
        _44ShowMsg(1, 163, _24635, 1);
        _24635 = NOVALUE;
        goto LF; // [353] 369
LB: 

        /** 						ShowMsg(1, 163, { pdone, cmd })*/
        RefDS(_cmd_46127);
        Ref(_pdone_46189);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _pdone_46189;
        ((int *)_2)[2] = _cmd_46127;
        _24636 = MAKE_SEQ(_1);
        _44ShowMsg(1, 163, _24636, 1);
        _24636 = NOVALUE;
LF: 
LA: 
        DeRef(_pdone_46189);
        _pdone_46189 = NOVALUE;

        /** 				status = system_exec(cmd, 0)*/
        _status_46134 = system_exec_call(_cmd_46127, 0);

        /** 				if status != 0 then*/
        if (_status_46134 == 0)
        goto L10; // [380] 464

        /** 					ShowMsg(2, 164, { generated_files[i] })*/
        _2 = (int)SEQ_PTR(_56generated_files_42658);
        _24639 = (int)*(((s1_ptr)_2)->base + _i_46167);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_24639);
        *((int *)(_2+4)) = _24639;
        _24640 = MAKE_SEQ(_1);
        _24639 = NOVALUE;
        _44ShowMsg(2, 164, _24640, 1);
        _24640 = NOVALUE;

        /** 					ShowMsg(2, 165, { status, cmd })*/
        RefDS(_cmd_46127);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _status_46134;
        ((int *)_2)[2] = _cmd_46127;
        _24641 = MAKE_SEQ(_1);
        _44ShowMsg(2, 165, _24641, 1);
        _24641 = NOVALUE;

        /** 					goto "build_direct_cleanup"*/
        goto G11;
        goto L10; // [427] 464
L9: 

        /** 			elsif match(".o", generated_files[i]) then*/
        _2 = (int)SEQ_PTR(_56generated_files_42658);
        _24643 = (int)*(((s1_ptr)_2)->base + _i_46167);
        _24644 = e_match_from(_23728, _24643, 1);
        _24643 = NOVALUE;
        if (_24644 == 0)
        {
            _24644 = NOVALUE;
            goto L12; // [443] 463
        }
        else{
            _24644 = NOVALUE;
        }

        /** 				objs &= " " & generated_files[i]*/
        _2 = (int)SEQ_PTR(_56generated_files_42658);
        _24645 = (int)*(((s1_ptr)_2)->base + _i_46167);
        Concat((object_ptr)&_24646, _23968, _24645);
        _24645 = NOVALUE;
        Concat((object_ptr)&_objs_46128, _objs_46128, _24646);
        DeRefDS(_24646);
        _24646 = NOVALUE;
L12: 
L10: 

        /** 		end for*/
LE: 
        _i_46167 = _i_46167 + 1;
        goto L7; // [466] 183
L8: 
        ;
    }
    goto L13; // [471] 535
L6: 

    /** 		object files = read_lines(file0 & ".bld")*/
    Concat((object_ptr)&_24648, _56file0_44491, _24159);
    _0 = _files_46235;
    _files_46235 = _16read_lines(_24648);
    DeRef(_0);
    _24648 = NOVALUE;

    /** 		for i = 1 to length(files) do*/
    if (IS_SEQUENCE(_files_46235)){
            _24650 = SEQ_PTR(_files_46235)->length;
    }
    else {
        _24650 = 1;
    }
    {
        int _i_46241;
        _i_46241 = 1;
L14: 
        if (_i_46241 > _24650){
            goto L15; // [491] 532
        }

        /** 			objs &= " " & filebase(files[i]) & "." & settings[SETUP_OBJ_EXT]*/
        _2 = (int)SEQ_PTR(_files_46235);
        _24651 = (int)*(((s1_ptr)_2)->base + _i_46241);
        Ref(_24651);
        _24652 = _9filebase(_24651);
        _24651 = NOVALUE;
        _2 = (int)SEQ_PTR(_settings_46129);
        _24653 = (int)*(((s1_ptr)_2)->base + 5);
        {
            int concat_list[4];

            concat_list[0] = _24653;
            concat_list[1] = _23767;
            concat_list[2] = _24652;
            concat_list[3] = _23968;
            Concat_N((object_ptr)&_24654, concat_list, 4);
        }
        _24653 = NOVALUE;
        DeRef(_24652);
        _24652 = NOVALUE;
        Concat((object_ptr)&_objs_46128, _objs_46128, _24654);
        DeRefDS(_24654);
        _24654 = NOVALUE;

        /** 		end for*/
        _i_46241 = _i_46241 + 1;
        goto L14; // [527] 498
L15: 
        ;
    }
    DeRef(_files_46235);
    _files_46235 = NOVALUE;
L13: 

    /** 	if keep and not link_only and length(link_files) then*/
    if (_56keep_42661 == 0) {
        _24656 = 0;
        goto L16; // [539] 550
    }
    _24657 = (_link_only_46120 == 0);
    _24656 = (_24657 != 0);
L16: 
    if (_24656 == 0) {
        goto L17; // [550] 579
    }
    if (IS_SEQUENCE(_link_files_46163)){
            _24659 = SEQ_PTR(_link_files_46163)->length;
    }
    else {
        _24659 = 1;
    }
    if (_24659 == 0)
    {
        _24659 = NOVALUE;
        goto L17; // [558] 579
    }
    else{
        _24659 = NOVALUE;
    }

    /** 		write_lines(file0 & ".bld", link_files)*/
    Concat((object_ptr)&_24660, _56file0_44491, _24159);
    RefDS(_link_files_46163);
    _32386 = _16write_lines(_24660, _link_files_46163);
    _24660 = NOVALUE;
    DeRef(_32386);
    _32386 = NOVALUE;
    goto L18; // [576] 603
L17: 

    /** 	elsif keep = 0 then*/
    if (_56keep_42661 != 0)
    goto L19; // [583] 602

    /** 		delete_file(file0 & ".bld")*/
    Concat((object_ptr)&_24662, _56file0_44491, _24159);
    _32385 = _9delete_file(_24662);
    _24662 = NOVALUE;
    DeRef(_32385);
    _32385 = NOVALUE;
L19: 
L18: 

    /** 	if length(rc_file[D_ALTNAME]) and length(settings[SETUP_RC_COMPILER]) and compiler_type = COMPILER_GCC then*/
    _2 = (int)SEQ_PTR(_54rc_file_45325);
    _24663 = (int)*(((s1_ptr)_2)->base + 11);
    if (IS_SEQUENCE(_24663)){
            _24664 = SEQ_PTR(_24663)->length;
    }
    else {
        _24664 = 1;
    }
    _24663 = NOVALUE;
    if (_24664 == 0) {
        _24665 = 0;
        goto L1A; // [616] 633
    }
    _2 = (int)SEQ_PTR(_settings_46129);
    _24666 = (int)*(((s1_ptr)_2)->base + 8);
    if (IS_SEQUENCE(_24666)){
            _24667 = SEQ_PTR(_24666)->length;
    }
    else {
        _24667 = 1;
    }
    _24666 = NOVALUE;
    _24665 = (_24667 != 0);
L1A: 
    if (_24665 == 0) {
        goto L1B; // [633] 738
    }
    _24669 = (_54compiler_type_45317 == 1);
    if (_24669 == 0)
    {
        DeRef(_24669);
        _24669 = NOVALUE;
        goto L1B; // [646] 738
    }
    else{
        DeRef(_24669);
        _24669 = NOVALUE;
    }

    /** 		cmd = text:format(settings[SETUP_RC_COMPILER], { rc_file[D_ALTNAME], res_file[D_ALTNAME] })*/
    _2 = (int)SEQ_PTR(_settings_46129);
    _24670 = (int)*(((s1_ptr)_2)->base + 8);
    _2 = (int)SEQ_PTR(_54rc_file_45325);
    _24671 = (int)*(((s1_ptr)_2)->base + 11);
    _2 = (int)SEQ_PTR(_54res_file_45331);
    _24672 = (int)*(((s1_ptr)_2)->base + 11);
    Ref(_24672);
    Ref(_24671);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24671;
    ((int *)_2)[2] = _24672;
    _24673 = MAKE_SEQ(_1);
    _24672 = NOVALUE;
    _24671 = NOVALUE;
    Ref(_24670);
    _0 = _cmd_46127;
    _cmd_46127 = _4format(_24670, _24673);
    DeRef(_0);
    _24670 = NOVALUE;
    _24673 = NOVALUE;

    /** 		status = system_exec(cmd, 0)*/
    _status_46134 = system_exec_call(_cmd_46127, 0);

    /** 		if status != 0 then*/
    if (_status_46134 == 0)
    goto L1C; // [692] 737

    /** 			ShowMsg(2, 350, { rc_file[D_NAME] })*/
    _2 = (int)SEQ_PTR(_54rc_file_45325);
    _24677 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24677);
    *((int *)(_2+4)) = _24677;
    _24678 = MAKE_SEQ(_1);
    _24677 = NOVALUE;
    _44ShowMsg(2, 350, _24678, 1);
    _24678 = NOVALUE;

    /** 			ShowMsg(2, 169, { status, cmd })*/
    RefDS(_cmd_46127);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _status_46134;
    ((int *)_2)[2] = _cmd_46127;
    _24679 = MAKE_SEQ(_1);
    _44ShowMsg(2, 169, _24679, 1);
    _24679 = NOVALUE;

    /** 			goto "build_direct_cleanup"*/
    goto G11;
L1C: 
L1B: 

    /** 	switch compiler_type do*/
    _0 = _54compiler_type_45317;
    switch ( _0 ){ 

        /** 		case COMPILER_WATCOM then*/
        case 2:

        /** 			cmd = sprintf("%s @%s.lnk", { settings[SETUP_LEXE], file0 })*/
        _2 = (int)SEQ_PTR(_settings_46129);
        _24683 = (int)*(((s1_ptr)_2)->base + 3);
        RefDS(_56file0_44491);
        Ref(_24683);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _24683;
        ((int *)_2)[2] = _56file0_44491;
        _24684 = MAKE_SEQ(_1);
        _24683 = NOVALUE;
        DeRef(_cmd_46127);
        _cmd_46127 = EPrintf(-9999999, _24682, _24684);
        DeRefDS(_24684);
        _24684 = NOVALUE;
        goto L1D; // [769] 848

        /** 		case COMPILER_GCC then*/
        case 1:

        /** 			cmd = sprintf("%s -o %s %s %s %s", { */
        _2 = (int)SEQ_PTR(_settings_46129);
        _24687 = (int)*(((s1_ptr)_2)->base + 3);
        _2 = (int)SEQ_PTR(_54exe_name_45319);
        _24688 = (int)*(((s1_ptr)_2)->base + 11);
        Ref(_24688);
        _24689 = _54adjust_for_build_file(_24688);
        _24688 = NOVALUE;
        _2 = (int)SEQ_PTR(_54res_file_45331);
        _24690 = (int)*(((s1_ptr)_2)->base + 11);
        _2 = (int)SEQ_PTR(_settings_46129);
        _24691 = (int)*(((s1_ptr)_2)->base + 4);
        _1 = NewS1(5);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_24687);
        *((int *)(_2+4)) = _24687;
        *((int *)(_2+8)) = _24689;
        RefDS(_objs_46128);
        *((int *)(_2+12)) = _objs_46128;
        Ref(_24690);
        *((int *)(_2+16)) = _24690;
        Ref(_24691);
        *((int *)(_2+20)) = _24691;
        _24692 = MAKE_SEQ(_1);
        _24691 = NOVALUE;
        _24690 = NOVALUE;
        _24689 = NOVALUE;
        _24687 = NOVALUE;
        DeRef(_cmd_46127);
        _cmd_46127 = EPrintf(-9999999, _24686, _24692);
        DeRefDS(_24692);
        _24692 = NOVALUE;
        goto L1D; // [821] 848

        /** 		case else*/
        default:

        /** 			ShowMsg(2, 167, { compiler_type })*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _54compiler_type_45317;
        _24694 = MAKE_SEQ(_1);
        _44ShowMsg(2, 167, _24694, 1);
        _24694 = NOVALUE;

        /** 			goto "build_direct_cleanup"*/
        goto G11;
    ;}L1D: 

    /** 	if not silent then*/
    if (_25silent_12398 != 0)
    goto L1E; // [852] 906

    /** 		if not verbose then*/
    if (_25verbose_12401 != 0)
    goto L1F; // [859] 890

    /** 			ShowMsg(1, 166, { abbreviate_path(exe_name[D_NAME]) })*/
    _2 = (int)SEQ_PTR(_54exe_name_45319);
    _24697 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_24697);
    RefDS(_22682);
    _24698 = _9abbreviate_path(_24697, _22682);
    _24697 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _24698;
    _24699 = MAKE_SEQ(_1);
    _24698 = NOVALUE;
    _44ShowMsg(1, 166, _24699, 1);
    _24699 = NOVALUE;
    goto L20; // [887] 905
L1F: 

    /** 			ShowMsg(1, 166, { cmd })*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_cmd_46127);
    *((int *)(_2+4)) = _cmd_46127;
    _24700 = MAKE_SEQ(_1);
    _44ShowMsg(1, 166, _24700, 1);
    _24700 = NOVALUE;
L20: 
L1E: 

    /** 	status = system_exec(cmd, 0)*/
    _status_46134 = system_exec_call(_cmd_46127, 0);

    /** 	if status != 0 then*/
    if (_status_46134 == 0)
    goto L21; // [916] 959

    /** 		ShowMsg(2, 168, { exe_name[D_NAME] })*/
    _2 = (int)SEQ_PTR(_54exe_name_45319);
    _24703 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24703);
    *((int *)(_2+4)) = _24703;
    _24704 = MAKE_SEQ(_1);
    _24703 = NOVALUE;
    _44ShowMsg(2, 168, _24704, 1);
    _24704 = NOVALUE;

    /** 		ShowMsg(2, 169, { status, cmd })*/
    RefDS(_cmd_46127);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _status_46134;
    ((int *)_2)[2] = _cmd_46127;
    _24705 = MAKE_SEQ(_1);
    _44ShowMsg(2, 169, _24705, 1);
    _24705 = NOVALUE;

    /** 		goto "build_direct_cleanup"*/
    goto G11;
L21: 

    /** 	if length(rc_file[D_ALTNAME]) and length(settings[SETUP_RC_COMPILER]) and compiler_type = COMPILER_WATCOM then*/
    _2 = (int)SEQ_PTR(_54rc_file_45325);
    _24706 = (int)*(((s1_ptr)_2)->base + 11);
    if (IS_SEQUENCE(_24706)){
            _24707 = SEQ_PTR(_24706)->length;
    }
    else {
        _24707 = 1;
    }
    _24706 = NOVALUE;
    if (_24707 == 0) {
        _24708 = 0;
        goto L22; // [972] 989
    }
    _2 = (int)SEQ_PTR(_settings_46129);
    _24709 = (int)*(((s1_ptr)_2)->base + 8);
    if (IS_SEQUENCE(_24709)){
            _24710 = SEQ_PTR(_24709)->length;
    }
    else {
        _24710 = 1;
    }
    _24709 = NOVALUE;
    _24708 = (_24710 != 0);
L22: 
    if (_24708 == 0) {
        goto L23; // [989] 1112
    }
    _24712 = (_54compiler_type_45317 == 2);
    if (_24712 == 0)
    {
        DeRef(_24712);
        _24712 = NOVALUE;
        goto L23; // [1002] 1112
    }
    else{
        DeRef(_24712);
        _24712 = NOVALUE;
    }

    /** 		cmd = text:format(settings[SETUP_RC_COMPILER], { rc_file[D_ALTNAME], res_file[D_ALTNAME], exe_name[D_ALTNAME] })*/
    _2 = (int)SEQ_PTR(_settings_46129);
    _24713 = (int)*(((s1_ptr)_2)->base + 8);
    _2 = (int)SEQ_PTR(_54rc_file_45325);
    _24714 = (int)*(((s1_ptr)_2)->base + 11);
    _2 = (int)SEQ_PTR(_54res_file_45331);
    _24715 = (int)*(((s1_ptr)_2)->base + 11);
    _2 = (int)SEQ_PTR(_54exe_name_45319);
    _24716 = (int)*(((s1_ptr)_2)->base + 11);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24714);
    *((int *)(_2+4)) = _24714;
    Ref(_24715);
    *((int *)(_2+8)) = _24715;
    Ref(_24716);
    *((int *)(_2+12)) = _24716;
    _24717 = MAKE_SEQ(_1);
    _24716 = NOVALUE;
    _24715 = NOVALUE;
    _24714 = NOVALUE;
    Ref(_24713);
    _0 = _cmd_46127;
    _cmd_46127 = _4format(_24713, _24717);
    DeRef(_0);
    _24713 = NOVALUE;
    _24717 = NOVALUE;

    /** 		status = system_exec(cmd, 0)*/
    _status_46134 = system_exec_call(_cmd_46127, 0);

    /** 		if status != 0 then*/
    if (_status_46134 == 0)
    goto L24; // [1058] 1111

    /** 			ShowMsg(2, 187, { rc_file[D_NAME], exe_name[D_NAME] })*/
    _2 = (int)SEQ_PTR(_54rc_file_45325);
    _24721 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_54exe_name_45319);
    _24722 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_24722);
    Ref(_24721);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24721;
    ((int *)_2)[2] = _24722;
    _24723 = MAKE_SEQ(_1);
    _24722 = NOVALUE;
    _24721 = NOVALUE;
    _44ShowMsg(2, 187, _24723, 1);
    _24723 = NOVALUE;

    /** 			ShowMsg(2, 169, { status, cmd })*/
    RefDS(_cmd_46127);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _status_46134;
    ((int *)_2)[2] = _cmd_46127;
    _24724 = MAKE_SEQ(_1);
    _44ShowMsg(2, 169, _24724, 1);
    _24724 = NOVALUE;

    /** 			goto "build_direct_cleanup"*/
    goto G11;
L24: 
L23: 

    /** label "build_direct_cleanup"*/
G11:

    /** 	if keep = 0 then*/
    if (_56keep_42661 != 0)
    goto L25; // [1120] 1267

    /** 		for i = 1 to length(generated_files) do*/
    if (IS_SEQUENCE(_56generated_files_42658)){
            _24726 = SEQ_PTR(_56generated_files_42658)->length;
    }
    else {
        _24726 = 1;
    }
    {
        int _i_46368;
        _i_46368 = 1;
L26: 
        if (_i_46368 > _24726){
            goto L27; // [1131] 1185
        }

        /** 			if verbose then*/
        if (_25verbose_12401 == 0)
        {
            goto L28; // [1142] 1164
        }
        else{
        }

        /** 				ShowMsg(1, 347, { generated_files[i] })*/
        _2 = (int)SEQ_PTR(_56generated_files_42658);
        _24727 = (int)*(((s1_ptr)_2)->base + _i_46368);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_24727);
        *((int *)(_2+4)) = _24727;
        _24728 = MAKE_SEQ(_1);
        _24727 = NOVALUE;
        _44ShowMsg(1, 347, _24728, 1);
        _24728 = NOVALUE;
L28: 

        /** 			delete_file(generated_files[i])*/
        _2 = (int)SEQ_PTR(_56generated_files_42658);
        _24729 = (int)*(((s1_ptr)_2)->base + _i_46368);
        RefDS(_24729);
        _32384 = _9delete_file(_24729);
        _24729 = NOVALUE;
        DeRef(_32384);
        _32384 = NOVALUE;

        /** 		end for*/
        _i_46368 = _i_46368 + 1;
        goto L26; // [1180] 1138
L27: 
        ;
    }

    /** 		if length(res_file[D_ALTNAME]) then*/
    _2 = (int)SEQ_PTR(_54res_file_45331);
    _24730 = (int)*(((s1_ptr)_2)->base + 11);
    if (IS_SEQUENCE(_24730)){
            _24731 = SEQ_PTR(_24730)->length;
    }
    else {
        _24731 = 1;
    }
    _24730 = NOVALUE;
    if (_24731 == 0)
    {
        _24731 = NOVALUE;
        goto L29; // [1198] 1218
    }
    else{
        _24731 = NOVALUE;
    }

    /** 			delete_file(res_file[D_ALTNAME])*/
    _2 = (int)SEQ_PTR(_54res_file_45331);
    _24732 = (int)*(((s1_ptr)_2)->base + 11);
    Ref(_24732);
    _32383 = _9delete_file(_24732);
    _24732 = NOVALUE;
    DeRef(_32383);
    _32383 = NOVALUE;
L29: 

    /** 		if remove_output_dir then*/
L25: 

    /** 	chdir(cwd)*/
    RefDS(_cwd_46131);
    _32381 = _9chdir(_cwd_46131);
    DeRef(_32381);
    _32381 = NOVALUE;

    /** end procedure*/
    DeRefDS(_the_file0_46121);
    DeRef(_cmd_46127);
    DeRef(_objs_46128);
    DeRef(_settings_46129);
    DeRefDS(_cwd_46131);
    DeRef(_link_files_46163);
    DeRef(_24657);
    _24657 = NOVALUE;
    DeRef(_24629);
    _24629 = NOVALUE;
    _24663 = NOVALUE;
    _24666 = NOVALUE;
    _24706 = NOVALUE;
    _24709 = NOVALUE;
    _24730 = NOVALUE;
    return;
    ;
}


void _54write_buildfile()
{
    int _make_command_46408 = NOVALUE;
    int _settings_46448 = NOVALUE;
    int _24759 = NOVALUE;
    int _24758 = NOVALUE;
    int _24754 = NOVALUE;
    int _24753 = NOVALUE;
    int _24752 = NOVALUE;
    int _24750 = NOVALUE;
    int _24749 = NOVALUE;
    int _24748 = NOVALUE;
    int _24747 = NOVALUE;
    int _24746 = NOVALUE;
    int _24745 = NOVALUE;
    int _24744 = NOVALUE;
    int _24743 = NOVALUE;
    int _0, _1, _2;
    

    /** 	switch build_system_type do*/
    _0 = _54build_system_type_45311;
    switch ( _0 ){ 

        /** 		case BUILD_MAKEFILE_FULL then*/
        case 2:

        /** 			write_makefile_full()*/
        _54write_makefile_full();

        /** 			if not silent then*/
        if (_25silent_12398 != 0)
        goto L1; // [22] 138

        /** 				sequence make_command*/

        /** 				if compiler_type = COMPILER_WATCOM then*/
        if (_54compiler_type_45317 != 2)
        goto L2; // [33] 47

        /** 					make_command = "wmake /f "*/
        RefDS(_24741);
        DeRefi(_make_command_46408);
        _make_command_46408 = _24741;
        goto L3; // [44] 55
L2: 

        /** 					make_command = "make -f "*/
        RefDS(_24742);
        DeRefi(_make_command_46408);
        _make_command_46408 = _24742;
L3: 

        /** 				ShowMsg(1, 170, { cfile_count + 2 })*/
        _24743 = _25cfile_count_12344 + 2;
        if ((long)((unsigned long)_24743 + (unsigned long)HIGH_BITS) >= 0) 
        _24743 = NewDouble((double)_24743);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _24743;
        _24744 = MAKE_SEQ(_1);
        _24743 = NOVALUE;
        _44ShowMsg(1, 170, _24744, 1);
        _24744 = NOVALUE;

        /** 				if sequence(output_dir) and length(output_dir) > 0 then*/
        _24745 = 1;
        if (_24745 == 0) {
            goto L4; // [80] 120
        }
        if (IS_SEQUENCE(_56output_dir_42676)){
                _24747 = SEQ_PTR(_56output_dir_42676)->length;
        }
        else {
            _24747 = 1;
        }
        _24748 = (_24747 > 0);
        _24747 = NOVALUE;
        if (_24748 == 0)
        {
            DeRef(_24748);
            _24748 = NOVALUE;
            goto L4; // [94] 120
        }
        else{
            DeRef(_24748);
            _24748 = NOVALUE;
        }

        /** 					ShowMsg(1, 174, { output_dir, make_command, file0 })*/
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_56output_dir_42676);
        *((int *)(_2+4)) = _56output_dir_42676;
        RefDS(_make_command_46408);
        *((int *)(_2+8)) = _make_command_46408;
        RefDS(_56file0_44491);
        *((int *)(_2+12)) = _56file0_44491;
        _24749 = MAKE_SEQ(_1);
        _44ShowMsg(1, 174, _24749, 1);
        _24749 = NOVALUE;
        goto L5; // [117] 137
L4: 

        /** 					ShowMsg(1, 172, { make_command, file0 })*/
        RefDS(_56file0_44491);
        RefDS(_make_command_46408);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _make_command_46408;
        ((int *)_2)[2] = _56file0_44491;
        _24750 = MAKE_SEQ(_1);
        _44ShowMsg(1, 172, _24750, 1);
        _24750 = NOVALUE;
L5: 
L1: 
        DeRefi(_make_command_46408);
        _make_command_46408 = NOVALUE;
        goto L6; // [140] 265

        /** 		case BUILD_MAKEFILE_PARTIAL then*/
        case 1:

        /** 			write_makefile_partial()*/
        _54write_makefile_partial();

        /** 			if not silent then*/
        if (_25silent_12398 != 0)
        goto L6; // [154] 265

        /** 				ShowMsg(1, 170, { cfile_count + 2 })*/
        _24752 = _25cfile_count_12344 + 2;
        if ((long)((unsigned long)_24752 + (unsigned long)HIGH_BITS) >= 0) 
        _24752 = NewDouble((double)_24752);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _24752;
        _24753 = MAKE_SEQ(_1);
        _24752 = NOVALUE;
        _44ShowMsg(1, 170, _24753, 1);
        _24753 = NOVALUE;

        /** 				ShowMsg(1, 173, { file0 })*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_56file0_44491);
        *((int *)(_2+4)) = _56file0_44491;
        _24754 = MAKE_SEQ(_1);
        _44ShowMsg(1, 173, _24754, 1);
        _24754 = NOVALUE;
        goto L6; // [190] 265

        /** 		case BUILD_DIRECT then*/
        case 3:

        /** 			build_direct()*/
        RefDS(_22682);
        _54build_direct(0, _22682);

        /** 			if not silent then*/
        if (_25silent_12398 != 0)
        goto L7; // [206] 217

        /** 				sequence settings = setup_build()*/
        _0 = _settings_46448;
        _settings_46448 = _54setup_build();
        DeRef(_0);
L7: 
        DeRef(_settings_46448);
        _settings_46448 = NOVALUE;
        goto L6; // [219] 265

        /** 		case BUILD_NONE then*/
        case 0:

        /** 			if not silent then*/
        if (_25silent_12398 != 0)
        goto L6; // [229] 265

        /** 				ShowMsg(1, 170, { cfile_count + 2 })*/
        _24758 = _25cfile_count_12344 + 2;
        if ((long)((unsigned long)_24758 + (unsigned long)HIGH_BITS) >= 0) 
        _24758 = NewDouble((double)_24758);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _24758;
        _24759 = MAKE_SEQ(_1);
        _24758 = NOVALUE;
        _44ShowMsg(1, 170, _24759, 1);
        _24759 = NOVALUE;
        goto L6; // [251] 265

        /** 		case else*/
        default:

        /** 			CompileErr(151)*/
        RefDS(_22682);
        _43CompileErr(151, _22682, 0);
    ;}L6: 

    /** end procedure*/
    return;
    ;
}



// 0xFC816FB2
