// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _12allocate(int _n_1791, int _cleanup_1792)
{
    int _iaddr_1793 = NOVALUE;
    int _eaddr_1794 = NOVALUE;
    int _805 = NOVALUE;
    int _804 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef DATA_EXECUTE then*/

    /** 		iaddr = eu:machine_func( memconst:M_ALLOC, n + memory:BORDER_SPACE * 2)*/
    _804 = 0;
    _805 = _n_1791 + 0;
    _804 = NOVALUE;
    DeRef(_iaddr_1793);
    _iaddr_1793 = machine(16, _805);
    _805 = NOVALUE;

    /** 		eaddr = memory:prepare_block( iaddr, n, PAGE_READ_WRITE )*/
    Ref(_iaddr_1793);
    _0 = _eaddr_1794;
    _eaddr_1794 = _14prepare_block(_iaddr_1793, _n_1791, 4);
    DeRef(_0);

    /** 	if cleanup then*/

    /** 	return eaddr*/
    DeRef(_iaddr_1793);
    return _eaddr_1794;
    ;
}


int _12allocate_data(int _n_1804, int _cleanup_1805)
{
    int _a_1806 = NOVALUE;
    int _sla_1808 = NOVALUE;
    int _810 = NOVALUE;
    int _809 = NOVALUE;
    int _0, _1, _2;
    

    /** 	a = eu:machine_func( memconst:M_ALLOC, n+BORDER_SPACE*2)*/
    _809 = 0;
    _810 = 4;
    _809 = NOVALUE;
    DeRef(_a_1806);
    _a_1806 = machine(16, 4);
    _810 = NOVALUE;

    /** 	sla = memory:prepare_block(a, n, PAGE_READ_WRITE )*/
    Ref(_a_1806);
    _0 = _sla_1808;
    _sla_1808 = _14prepare_block(_a_1806, 4, 4);
    DeRef(_0);

    /** 	if cleanup then*/

    /** 		return sla*/
    DeRef(_a_1806);
    return _sla_1808;
    ;
}


int _12VirtualAlloc(int _addr_1987, int _size_1988, int _allocation_type_1989, int _protect__1990)
{
    int _r1_1991 = NOVALUE;
    int _893 = NOVALUE;
    int _0, _1, _2;
    

    /** 		r1 = c_func( VirtualAlloc_rid, {addr, size, allocation_type, protect_ } )*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 1;
    Ref(_allocation_type_1989);
    *((int *)(_2+12)) = _allocation_type_1989;
    *((int *)(_2+16)) = 64;
    _893 = MAKE_SEQ(_1);
    DeRef(_r1_1991);
    _r1_1991 = call_c(1, _12VirtualAlloc_rid_1904, _893);
    DeRefDS(_893);
    _893 = NOVALUE;

    /** 		return r1*/
    DeRef(_allocation_type_1989);
    return _r1_1991;
    ;
}


int _12allocate_code(int _data_2003, int _wordsize_2004)
{
    int _895 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return allocate_protect( data, wordsize, PAGE_EXECUTE )*/
    RefDS(_data_2003);
    _895 = _12allocate_protect(_data_2003, 1, 16);
    DeRefDSi(_data_2003);
    return _895;
    ;
}


int _12allocate_string(int _s_2010, int _cleanup_2011)
{
    int _mem_2012 = NOVALUE;
    int _900 = NOVALUE;
    int _899 = NOVALUE;
    int _897 = NOVALUE;
    int _896 = NOVALUE;
    int _0, _1, _2;
    

    /** 	mem = allocate( length(s) + 1) -- Thanks to Igor*/
    if (IS_SEQUENCE(_s_2010)){
            _896 = SEQ_PTR(_s_2010)->length;
    }
    else {
        _896 = 1;
    }
    _897 = _896 + 1;
    _896 = NOVALUE;
    _0 = _mem_2012;
    _mem_2012 = _12allocate(_897, 0);
    DeRef(_0);
    _897 = NOVALUE;

    /** 	if mem then*/
    if (_mem_2012 == 0) {
        goto L1; // [19] 54
    }
    else {
        if (!IS_ATOM_INT(_mem_2012) && DBL_PTR(_mem_2012)->dbl == 0.0){
            goto L1; // [19] 54
        }
    }

    /** 		poke(mem, s)*/
    if (IS_ATOM_INT(_mem_2012)){
        poke_addr = (unsigned char *)_mem_2012;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_mem_2012)->dbl);
    }
    _1 = (int)SEQ_PTR(_s_2010);
    _1 = (int)((s1_ptr)_1)->base;
    while (1) {
        _1 += 4;
        _2 = *((int *)_1);
        if (IS_ATOM_INT(_2))
        *poke_addr++ = (unsigned char)_2;
        else if (_2 == NOVALUE)
        break;
        else {
            _0 = (signed char)DBL_PTR(_2)->dbl;
            *poke_addr++ = (signed char)_0;
        }
    }

    /** 		poke(mem+length(s), 0)  -- Thanks to Aku*/
    if (IS_SEQUENCE(_s_2010)){
            _899 = SEQ_PTR(_s_2010)->length;
    }
    else {
        _899 = 1;
    }
    if (IS_ATOM_INT(_mem_2012)) {
        _900 = _mem_2012 + _899;
        if ((long)((unsigned long)_900 + (unsigned long)HIGH_BITS) >= 0) 
        _900 = NewDouble((double)_900);
    }
    else {
        _900 = NewDouble(DBL_PTR(_mem_2012)->dbl + (double)_899);
    }
    _899 = NOVALUE;
    if (IS_ATOM_INT(_900)){
        poke_addr = (unsigned char *)_900;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_900)->dbl);
    }
    *poke_addr = (unsigned char)0;
    DeRef(_900);
    _900 = NOVALUE;

    /** 		if cleanup then*/
L1: 

    /** 	return mem*/
    DeRefDS(_s_2010);
    return _mem_2012;
    ;
}


int _12allocate_protect(int _data_2023, int _wordsize_2024, int _protection_2026)
{
    int _iaddr_2027 = NOVALUE;
    int _eaddr_2029 = NOVALUE;
    int _size_2030 = NOVALUE;
    int _first_protection_2032 = NOVALUE;
    int _true_protection_2034 = NOVALUE;
    int _prepare_block_inlined_prepare_block_at_104_2049 = NOVALUE;
    int _msg_inlined_crash_at_186_2062 = NOVALUE;
    int _921 = NOVALUE;
    int _920 = NOVALUE;
    int _918 = NOVALUE;
    int _917 = NOVALUE;
    int _916 = NOVALUE;
    int _912 = NOVALUE;
    int _910 = NOVALUE;
    int _907 = NOVALUE;
    int _906 = NOVALUE;
    int _904 = NOVALUE;
    int _902 = NOVALUE;
    int _0, _1, _2;
    

    /** 	atom iaddr = 0*/
    DeRef(_iaddr_2027);
    _iaddr_2027 = 0;

    /** 	integer size*/

    /** 	valid_memory_protection_constant true_protection = protection*/
    _true_protection_2034 = 16;

    /** 	ifdef SAFE then	*/

    /** 	if atom(data) then*/
    _902 = 0;
    if (_902 == 0)
    {
        _902 = NOVALUE;
        goto L1; // [20] 39
    }
    else{
        _902 = NOVALUE;
    }

    /** 		size = data * wordsize*/
    _size_2030 = binary_op(MULTIPLY, _data_2023, 1);
    if (!IS_ATOM_INT(_size_2030)) {
        _1 = (long)(DBL_PTR(_size_2030)->dbl);
        if (UNIQUE(DBL_PTR(_size_2030)) && (DBL_PTR(_size_2030)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_2030);
        _size_2030 = _1;
    }

    /** 		first_protection = true_protection*/
    _first_protection_2032 = 16;
    goto L2; // [36] 58
L1: 

    /** 		size = length(data) * wordsize*/
    if (IS_SEQUENCE(_data_2023)){
            _904 = SEQ_PTR(_data_2023)->length;
    }
    else {
        _904 = 1;
    }
    _size_2030 = _904 * _wordsize_2024;
    _904 = NOVALUE;

    /** 		first_protection = PAGE_READ_WRITE*/
    _first_protection_2032 = 4;
L2: 

    /** 	iaddr = local_allocate_protected_memory( size + memory:BORDER_SPACE * 2, first_protection )*/
    _906 = 0;
    _907 = _size_2030 + 0;
    _906 = NOVALUE;
    _0 = _iaddr_2027;
    _iaddr_2027 = _12local_allocate_protected_memory(_907, _first_protection_2032);
    DeRef(_0);
    _907 = NOVALUE;

    /** 	if iaddr = 0 then*/
    if (binary_op_a(NOTEQ, _iaddr_2027, 0)){
        goto L3; // [79] 90
    }

    /** 		return 0*/
    DeRefi(_data_2023);
    DeRef(_iaddr_2027);
    DeRef(_eaddr_2029);
    return 0;
L3: 

    /** 	eaddr = memory:prepare_block( iaddr, size, protection )*/
    if (!IS_ATOM_INT(_protection_2026)) {
        _1 = (long)(DBL_PTR(_protection_2026)->dbl);
        if (UNIQUE(DBL_PTR(_protection_2026)) && (DBL_PTR(_protection_2026)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_protection_2026);
        _protection_2026 = _1;
    }

    /** 	return addr*/
    Ref(_iaddr_2027);
    DeRef(_eaddr_2029);
    _eaddr_2029 = _iaddr_2027;

    /** 	if eaddr = 0 or atom( data ) then*/
    if (IS_ATOM_INT(_eaddr_2029)) {
        _910 = (_eaddr_2029 == 0);
    }
    else {
        _910 = (DBL_PTR(_eaddr_2029)->dbl == (double)0);
    }
    if (_910 != 0) {
        goto L4; // [106] 118
    }
    _912 = IS_ATOM(_data_2023);
    if (_912 == 0)
    {
        _912 = NOVALUE;
        goto L5; // [114] 125
    }
    else{
        _912 = NOVALUE;
    }
L4: 

    /** 		return eaddr*/
    DeRefi(_data_2023);
    DeRef(_iaddr_2027);
    DeRef(_910);
    _910 = NOVALUE;
    return _eaddr_2029;
L5: 

    /** 	switch wordsize do*/
    _0 = _wordsize_2024;
    switch ( _0 ){ 

        /** 		case 1 then*/
        case 1:

        /** 			eu:poke( eaddr, data )*/
        if (IS_ATOM_INT(_eaddr_2029)){
            poke_addr = (unsigned char *)_eaddr_2029;
        }
        else {
            poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_eaddr_2029)->dbl);
        }
        if (IS_ATOM_INT(_data_2023)) {
            *poke_addr = (unsigned char)_data_2023;
        }
        else if (IS_ATOM(_data_2023)) {
            _1 = (signed char)DBL_PTR(_data_2023)->dbl;
            *poke_addr = _1;
        }
        else {
            _1 = (int)SEQ_PTR(_data_2023);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *poke_addr++ = (unsigned char)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (signed char)DBL_PTR(_2)->dbl;
                    *poke_addr++ = (signed char)_0;
                }
            }
        }
        goto L6; // [141] 190

        /** 		case 2 then*/
        case 2:

        /** 			eu:poke2( eaddr, data )*/
        if (IS_ATOM_INT(_eaddr_2029)){
            poke2_addr = (unsigned short *)_eaddr_2029;
        }
        else {
            poke2_addr = (unsigned short *)(unsigned long)(DBL_PTR(_eaddr_2029)->dbl);
        }
        if (IS_ATOM_INT(_data_2023)) {
            *poke2_addr = (unsigned short)_data_2023;
        }
        else if (IS_ATOM(_data_2023)) {
            _1 = (signed char)DBL_PTR(_data_2023)->dbl;
            *poke_addr = _1;
        }
        else {
            _1 = (int)SEQ_PTR(_data_2023);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *poke2_addr++ = (unsigned short)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned short)DBL_PTR(_2)->dbl;
                    *poke2_addr++ = (unsigned short)_0;
                }
            }
        }
        goto L6; // [152] 190

        /** 		case 4 then*/
        case 4:

        /** 			eu:poke4( eaddr, data )*/
        if (IS_ATOM_INT(_eaddr_2029)){
            poke4_addr = (unsigned long *)_eaddr_2029;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_eaddr_2029)->dbl);
        }
        if (IS_ATOM_INT(_data_2023)) {
            *poke4_addr = (unsigned long)_data_2023;
        }
        else if (IS_ATOM(_data_2023)) {
            _1 = (unsigned long)DBL_PTR(_data_2023)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_data_2023);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }
        goto L6; // [163] 190

        /** 		case else*/
        default:

        /** 			error:crash("Parameter error: Wrong word size %d in allocate_protect().", wordsize)*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_186_2062);
        _msg_inlined_crash_at_186_2062 = EPrintf(-9999999, _915, _wordsize_2024);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_186_2062);

        /** end procedure*/
        goto L7; // [184] 187
L7: 
        DeRefi(_msg_inlined_crash_at_186_2062);
        _msg_inlined_crash_at_186_2062 = NOVALUE;
    ;}L6: 

    /** 	ifdef SAFE then*/

    /** 	if local_change_protection_on_protected_memory( iaddr, size + memory:BORDER_SPACE * 2, true_protection ) = -1 then*/
    _916 = 0;
    _917 = _size_2030 + 0;
    _916 = NOVALUE;
    Ref(_iaddr_2027);
    _918 = _12local_change_protection_on_protected_memory(_iaddr_2027, _917, _true_protection_2034);
    _917 = NOVALUE;
    if (binary_op_a(NOTEQ, _918, -1)){
        DeRef(_918);
        _918 = NOVALUE;
        goto L8; // [208] 232
    }
    DeRef(_918);
    _918 = NOVALUE;

    /** 		local_free_protected_memory( iaddr, size + memory:BORDER_SPACE * 2 )*/
    _920 = 0;
    _921 = _size_2030 + 0;
    _920 = NOVALUE;
    Ref(_iaddr_2027);
    _12local_free_protected_memory(_iaddr_2027, _921);
    _921 = NOVALUE;

    /** 		eaddr = 0*/
    DeRef(_eaddr_2029);
    _eaddr_2029 = 0;
L8: 

    /** 	return eaddr*/
    DeRefi(_data_2023);
    DeRef(_iaddr_2027);
    DeRef(_910);
    _910 = NOVALUE;
    return _eaddr_2029;
    ;
}


int _12local_allocate_protected_memory(int _s_2077, int _first_protection_2078)
{
    int _927 = NOVALUE;
    int _926 = NOVALUE;
    int _925 = NOVALUE;
    int _924 = NOVALUE;
    int _923 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		if dep_works() then*/
    _923 = _14dep_works();
    if (_923 == 0) {
        DeRef(_923);
        _923 = NOVALUE;
        goto L1; // [12] 46
    }
    else {
        if (!IS_ATOM_INT(_923) && DBL_PTR(_923)->dbl == 0.0){
            DeRef(_923);
            _923 = NOVALUE;
            goto L1; // [12] 46
        }
        DeRef(_923);
        _923 = NOVALUE;
    }
    DeRef(_923);
    _923 = NOVALUE;

    /** 			return eu:c_func(VirtualAlloc_rid, */
    {unsigned long tu;
         tu = (unsigned long)8192 | (unsigned long)4096;
         _924 = MAKE_UINT(tu);
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = _s_2077;
    *((int *)(_2+12)) = _924;
    *((int *)(_2+16)) = _first_protection_2078;
    _925 = MAKE_SEQ(_1);
    _924 = NOVALUE;
    _926 = call_c(1, _12VirtualAlloc_rid_1904, _925);
    DeRefDS(_925);
    _925 = NOVALUE;
    return _926;
    goto L2; // [43] 61
L1: 

    /** 			return machine_func(M_ALLOC, PAGE_SIZE)*/
    _927 = machine(16, _12PAGE_SIZE_1984);
    DeRef(_926);
    _926 = NOVALUE;
    return _927;
L2: 
    ;
}


int _12local_change_protection_on_protected_memory(int _p_2092, int _s_2093, int _new_protection_2094)
{
    int _930 = NOVALUE;
    int _929 = NOVALUE;
    int _928 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		if dep_works() then*/
    _928 = _14dep_works();
    if (_928 == 0) {
        DeRef(_928);
        _928 = NOVALUE;
        goto L1; // [12] 45
    }
    else {
        if (!IS_ATOM_INT(_928) && DBL_PTR(_928)->dbl == 0.0){
            DeRef(_928);
            _928 = NOVALUE;
            goto L1; // [12] 45
        }
        DeRef(_928);
        _928 = NOVALUE;
    }
    DeRef(_928);
    _928 = NOVALUE;

    /** 			if eu:c_func( VirtualProtect_rid, { p, s, new_protection , oldprotptr } ) = 0 then*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_p_2092);
    *((int *)(_2+4)) = _p_2092;
    *((int *)(_2+8)) = _s_2093;
    *((int *)(_2+12)) = _new_protection_2094;
    Ref(_12oldprotptr_2073);
    *((int *)(_2+16)) = _12oldprotptr_2073;
    _929 = MAKE_SEQ(_1);
    _930 = call_c(1, _12VirtualProtect_rid_1905, _929);
    DeRefDS(_929);
    _929 = NOVALUE;
    if (binary_op_a(NOTEQ, _930, 0)){
        DeRef(_930);
        _930 = NOVALUE;
        goto L2; // [33] 44
    }
    DeRef(_930);
    _930 = NOVALUE;

    /** 				return -1*/
    DeRef(_p_2092);
    return -1;
L2: 
L1: 

    /** 		return 0*/
    DeRef(_p_2092);
    return 0;
    ;
}


void _12local_free_protected_memory(int _p_2104, int _s_2105)
{
    int _936 = NOVALUE;
    int _935 = NOVALUE;
    int _934 = NOVALUE;
    int _933 = NOVALUE;
    int _932 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		if dep_works() then*/
    _932 = _14dep_works();
    if (_932 == 0) {
        DeRef(_932);
        _932 = NOVALUE;
        goto L1; // [10] 33
    }
    else {
        if (!IS_ATOM_INT(_932) && DBL_PTR(_932)->dbl == 0.0){
            DeRef(_932);
            _932 = NOVALUE;
            goto L1; // [10] 33
        }
        DeRef(_932);
        _932 = NOVALUE;
    }
    DeRef(_932);
    _932 = NOVALUE;

    /** 			c_func(VirtualFree_rid, { p, s, MEM_RELEASE })*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_p_2104);
    *((int *)(_2+4)) = _p_2104;
    *((int *)(_2+8)) = _s_2105;
    *((int *)(_2+12)) = 32768;
    _933 = MAKE_SEQ(_1);
    _934 = call_c(1, _14VirtualFree_rid_1773, _933);
    DeRefDS(_933);
    _933 = NOVALUE;
    goto L2; // [30] 46
L1: 

    /** 			machine_func(M_FREE, {p})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_p_2104);
    *((int *)(_2+4)) = _p_2104;
    _935 = MAKE_SEQ(_1);
    _936 = machine(17, _935);
    DeRefDS(_935);
    _935 = NOVALUE;
L2: 

    /** end procedure*/
    DeRef(_p_2104);
    DeRef(_934);
    _934 = NOVALUE;
    DeRef(_936);
    _936 = NOVALUE;
    return;
    ;
}


void _12free(int _addr_2119)
{
    int _msg_inlined_crash_at_27_2128 = NOVALUE;
    int _data_inlined_crash_at_24_2127 = NOVALUE;
    int _addr_inlined_deallocate_at_64_2134 = NOVALUE;
    int _msg_inlined_crash_at_106_2139 = NOVALUE;
    int _943 = NOVALUE;
    int _942 = NOVALUE;
    int _941 = NOVALUE;
    int _940 = NOVALUE;
    int _938 = NOVALUE;
    int _937 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if types:number_array (addr) then*/
    Ref(_addr_2119);
    _937 = _5number_array(_addr_2119);
    if (_937 == 0) {
        DeRef(_937);
        _937 = NOVALUE;
        goto L1; // [7] 97
    }
    else {
        if (!IS_ATOM_INT(_937) && DBL_PTR(_937)->dbl == 0.0){
            DeRef(_937);
            _937 = NOVALUE;
            goto L1; // [7] 97
        }
        DeRef(_937);
        _937 = NOVALUE;
    }
    DeRef(_937);
    _937 = NOVALUE;

    /** 		if types:ascii_string(addr) then*/
    Ref(_addr_2119);
    _938 = _5ascii_string(_addr_2119);
    if (_938 == 0) {
        DeRef(_938);
        _938 = NOVALUE;
        goto L2; // [16] 47
    }
    else {
        if (!IS_ATOM_INT(_938) && DBL_PTR(_938)->dbl == 0.0){
            DeRef(_938);
            _938 = NOVALUE;
            goto L2; // [16] 47
        }
        DeRef(_938);
        _938 = NOVALUE;
    }
    DeRef(_938);
    _938 = NOVALUE;

    /** 			error:crash("free(\"%s\") is not a valid address", {addr})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_addr_2119);
    *((int *)(_2+4)) = _addr_2119;
    _940 = MAKE_SEQ(_1);
    DeRef(_data_inlined_crash_at_24_2127);
    _data_inlined_crash_at_24_2127 = _940;
    _940 = NOVALUE;

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_27_2128);
    _msg_inlined_crash_at_27_2128 = EPrintf(-9999999, _939, _data_inlined_crash_at_24_2127);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_27_2128);

    /** end procedure*/
    goto L3; // [41] 44
L3: 
    DeRef(_data_inlined_crash_at_24_2127);
    _data_inlined_crash_at_24_2127 = NOVALUE;
    DeRefi(_msg_inlined_crash_at_27_2128);
    _msg_inlined_crash_at_27_2128 = NOVALUE;
L2: 

    /** 		for i = 1 to length(addr) do*/
    if (IS_SEQUENCE(_addr_2119)){
            _941 = SEQ_PTR(_addr_2119)->length;
    }
    else {
        _941 = 1;
    }
    {
        int _i_2130;
        _i_2130 = 1;
L4: 
        if (_i_2130 > _941){
            goto L5; // [52] 89
        }

        /** 			memory:deallocate( addr[i] )*/
        _2 = (int)SEQ_PTR(_addr_2119);
        _942 = (int)*(((s1_ptr)_2)->base + _i_2130);
        Ref(_942);
        DeRef(_addr_inlined_deallocate_at_64_2134);
        _addr_inlined_deallocate_at_64_2134 = _942;
        _942 = NOVALUE;

        /** 	ifdef DATA_EXECUTE and WINDOWS then*/

        /**    	machine_proc( memconst:M_FREE, addr)*/
        machine(17, _addr_inlined_deallocate_at_64_2134);

        /** end procedure*/
        goto L6; // [77] 80
L6: 
        DeRef(_addr_inlined_deallocate_at_64_2134);
        _addr_inlined_deallocate_at_64_2134 = NOVALUE;

        /** 		end for*/
        _i_2130 = _i_2130 + 1;
        goto L4; // [84] 59
L5: 
        ;
    }

    /** 		return*/
    DeRef(_addr_2119);
    return;
    goto L7; // [94] 127
L1: 

    /** 	elsif sequence(addr) then*/
    _943 = IS_SEQUENCE(_addr_2119);
    if (_943 == 0)
    {
        _943 = NOVALUE;
        goto L8; // [102] 126
    }
    else{
        _943 = NOVALUE;
    }

    /** 		error:crash("free() called with nested sequence")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_106_2139);
    _msg_inlined_crash_at_106_2139 = EPrintf(-9999999, _944, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_106_2139);

    /** end procedure*/
    goto L9; // [120] 123
L9: 
    DeRefi(_msg_inlined_crash_at_106_2139);
    _msg_inlined_crash_at_106_2139 = NOVALUE;
L8: 
L7: 

    /** 	if addr = 0 then*/
    if (binary_op_a(NOTEQ, _addr_2119, 0)){
        goto LA; // [129] 139
    }

    /** 		return*/
    DeRef(_addr_2119);
    return;
LA: 

    /** 	memory:deallocate( addr )*/

    /** 	ifdef DATA_EXECUTE and WINDOWS then*/

    /**    	machine_proc( memconst:M_FREE, addr)*/
    machine(17, _addr_2119);

    /** end procedure*/
    goto LB; // [150] 153
LB: 

    /** end procedure*/
    DeRef(_addr_2119);
    return;
    ;
}


void _12free_pointer_array(int _pointers_array_2147)
{
    int _saved_2148 = NOVALUE;
    int _ptr_2149 = NOVALUE;
    int _0, _1, _2;
    

    /** 	atom saved = pointers_array*/
    Ref(_pointers_array_2147);
    DeRef(_saved_2148);
    _saved_2148 = _pointers_array_2147;

    /** 	while ptr with entry do*/
    goto L1; // [8] 39
L2: 
    if (_ptr_2149 <= 0) {
        if (_ptr_2149 == 0) {
            goto L3; // [13] 49
        }
        else {
            if (!IS_ATOM_INT(_ptr_2149) && DBL_PTR(_ptr_2149)->dbl == 0.0){
                goto L3; // [13] 49
            }
        }
    }

    /** 		memory:deallocate( ptr )*/

    /** 	ifdef DATA_EXECUTE and WINDOWS then*/

    /**    	machine_proc( memconst:M_FREE, addr)*/
    machine(17, _ptr_2149);

    /** end procedure*/
    goto L4; // [27] 30
L4: 

    /** 		pointers_array += ADDRESS_LENGTH*/
    _0 = _pointers_array_2147;
    if (IS_ATOM_INT(_pointers_array_2147)) {
        _pointers_array_2147 = _pointers_array_2147 + 4;
        if ((long)((unsigned long)_pointers_array_2147 + (unsigned long)HIGH_BITS) >= 0) 
        _pointers_array_2147 = NewDouble((double)_pointers_array_2147);
    }
    else {
        _pointers_array_2147 = NewDouble(DBL_PTR(_pointers_array_2147)->dbl + (double)4);
    }
    DeRef(_0);

    /** 	entry*/
L1: 

    /** 		ptr = peek4u(pointers_array)*/
    DeRef(_ptr_2149);
    if (IS_ATOM_INT(_pointers_array_2147)) {
        _ptr_2149 = *(unsigned long *)_pointers_array_2147;
        if ((unsigned)_ptr_2149 > (unsigned)MAXINT)
        _ptr_2149 = NewDouble((double)(unsigned long)_ptr_2149);
    }
    else {
        _ptr_2149 = *(unsigned long *)(unsigned long)(DBL_PTR(_pointers_array_2147)->dbl);
        if ((unsigned)_ptr_2149 > (unsigned)MAXINT)
        _ptr_2149 = NewDouble((double)(unsigned long)_ptr_2149);
    }

    /** 	end while*/
    goto L2; // [46] 11
L3: 

    /** 	free(saved)*/
    Ref(_saved_2148);
    _12free(_saved_2148);

    /** end procedure*/
    DeRef(_pointers_array_2147);
    DeRef(_saved_2148);
    DeRef(_ptr_2149);
    return;
    ;
}



// 0x44217176
