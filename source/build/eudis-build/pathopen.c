// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _38convert_from_OEM(int _s_50578)
{
    int _ls_50579 = NOVALUE;
    int _rc_50580 = NOVALUE;
    int _26598 = NOVALUE;
    int _26597 = NOVALUE;
    int _26595 = NOVALUE;
    int _26594 = NOVALUE;
    int _26591 = NOVALUE;
    int _26590 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ls=length(s)*/
    if (IS_SEQUENCE(_s_50578)){
            _ls_50579 = SEQ_PTR(_s_50578)->length;
    }
    else {
        _ls_50579 = 1;
    }

    /** 	if ls>convert_length then*/
    if (_ls_50579 <= _38convert_length_50558)
    goto L1; // [12] 47

    /** 		free(convert_buffer)*/
    Ref(_38convert_buffer_50557);
    _12free(_38convert_buffer_50557);

    /** 		convert_length=and_bits(ls+15,-16)+1*/
    _26590 = _ls_50579 + 15;
    {unsigned long tu;
         tu = (unsigned long)_26590 & (unsigned long)-16;
         _26591 = MAKE_UINT(tu);
    }
    _26590 = NOVALUE;
    if (IS_ATOM_INT(_26591)) {
        _38convert_length_50558 = _26591 + 1;
    }
    else
    { // coercing _38convert_length_50558 to an integer 1
        _38convert_length_50558 = 1+(long)(DBL_PTR(_26591)->dbl);
        if( !IS_ATOM_INT(_38convert_length_50558) ){
            _38convert_length_50558 = (object)DBL_PTR(_38convert_length_50558)->dbl;
        }
    }
    DeRef(_26591);
    _26591 = NOVALUE;

    /** 		convert_buffer=allocate(convert_length)*/
    _0 = _12allocate(_38convert_length_50558, 0);
    DeRef(_38convert_buffer_50557);
    _38convert_buffer_50557 = _0;
L1: 

    /** 	poke(convert_buffer,s)*/
    if (IS_ATOM_INT(_38convert_buffer_50557)){
        poke_addr = (unsigned char *)_38convert_buffer_50557;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_38convert_buffer_50557)->dbl);
    }
    _1 = (int)SEQ_PTR(_s_50578);
    _1 = (int)((s1_ptr)_1)->base;
    while (1) {
        _1 += 4;
        _2 = *((int *)_1);
        if (IS_ATOM_INT(_2))
        *poke_addr++ = (unsigned char)_2;
        else if (_2 == NOVALUE)
        break;
        else {
            _0 = (signed char)DBL_PTR(_2)->dbl;
            *poke_addr++ = (signed char)_0;
        }
    }

    /** 	poke(convert_buffer+ls,0)*/
    if (IS_ATOM_INT(_38convert_buffer_50557)) {
        _26594 = _38convert_buffer_50557 + _ls_50579;
        if ((long)((unsigned long)_26594 + (unsigned long)HIGH_BITS) >= 0) 
        _26594 = NewDouble((double)_26594);
    }
    else {
        _26594 = NewDouble(DBL_PTR(_38convert_buffer_50557)->dbl + (double)_ls_50579);
    }
    if (IS_ATOM_INT(_26594)){
        poke_addr = (unsigned char *)_26594;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_26594)->dbl);
    }
    *poke_addr = (unsigned char)0;
    DeRef(_26594);
    _26594 = NOVALUE;

    /** 	rc=c_func(oem2char,{convert_buffer,convert_buffer}) -- always nonzero*/
    Ref(_38convert_buffer_50557);
    Ref(_38convert_buffer_50557);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _38convert_buffer_50557;
    ((int *)_2)[2] = _38convert_buffer_50557;
    _26595 = MAKE_SEQ(_1);
    _rc_50580 = call_c(1, _38oem2char_50556, _26595);
    DeRefDS(_26595);
    _26595 = NOVALUE;
    if (!IS_ATOM_INT(_rc_50580)) {
        _1 = (long)(DBL_PTR(_rc_50580)->dbl);
        if (UNIQUE(DBL_PTR(_rc_50580)) && (DBL_PTR(_rc_50580)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_rc_50580);
        _rc_50580 = _1;
    }

    /** 	return peek({convert_buffer,ls}) */
    Ref(_38convert_buffer_50557);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _38convert_buffer_50557;
    ((int *)_2)[2] = _ls_50579;
    _26597 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_26597);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _26598 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_26597);
    _26597 = NOVALUE;
    DeRefDS(_s_50578);
    return _26598;
    ;
}


int _38exe_path()
{
    int _26619 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(exe_path_cache) then*/
    _26619 = IS_SEQUENCE(_38exe_path_cache_50634);
    if (_26619 == 0)
    {
        _26619 = NOVALUE;
        goto L1; // [8] 20
    }
    else{
        _26619 = NOVALUE;
    }

    /** 		return exe_path_cache*/
    Ref(_38exe_path_cache_50634);
    return _38exe_path_cache_50634;
L1: 

    /** 	exe_path_cache = command_line()*/
    DeRef(_38exe_path_cache_50634);
    _38exe_path_cache_50634 = Command_Line();

    /** 	exe_path_cache = exe_path_cache[1]*/
    _0 = _38exe_path_cache_50634;
    _2 = (int)SEQ_PTR(_38exe_path_cache_50634);
    _38exe_path_cache_50634 = (int)*(((s1_ptr)_2)->base + 1);
    RefDS(_38exe_path_cache_50634);
    DeRefDS(_0);

    /** 	return exe_path_cache*/
    RefDS(_38exe_path_cache_50634);
    return _38exe_path_cache_50634;
    ;
}


int _38check_cache(int _env_50646, int _inc_path_50647)
{
    int _delim_50648 = NOVALUE;
    int _pos_50649 = NOVALUE;
    int _26670 = NOVALUE;
    int _26669 = NOVALUE;
    int _26668 = NOVALUE;
    int _26667 = NOVALUE;
    int _26665 = NOVALUE;
    int _26664 = NOVALUE;
    int _26663 = NOVALUE;
    int _26662 = NOVALUE;
    int _26661 = NOVALUE;
    int _26660 = NOVALUE;
    int _26659 = NOVALUE;
    int _26658 = NOVALUE;
    int _26657 = NOVALUE;
    int _26656 = NOVALUE;
    int _26655 = NOVALUE;
    int _26651 = NOVALUE;
    int _26650 = NOVALUE;
    int _26649 = NOVALUE;
    int _26648 = NOVALUE;
    int _26647 = NOVALUE;
    int _26646 = NOVALUE;
    int _26645 = NOVALUE;
    int _26644 = NOVALUE;
    int _26642 = NOVALUE;
    int _26641 = NOVALUE;
    int _26640 = NOVALUE;
    int _26639 = NOVALUE;
    int _26638 = NOVALUE;
    int _26637 = NOVALUE;
    int _26635 = NOVALUE;
    int _26634 = NOVALUE;
    int _26633 = NOVALUE;
    int _26632 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not num_var then -- first time the var is accessed, add cache entry*/
    if (_38num_var_50599 != 0)
    goto L1; // [9] 94

    /** 		cache_vars = append(cache_vars,env)*/
    RefDS(_env_50646);
    Append(&_38cache_vars_50600, _38cache_vars_50600, _env_50646);

    /** 		cache_strings = append(cache_strings,inc_path)*/
    RefDS(_inc_path_50647);
    Append(&_38cache_strings_50601, _38cache_strings_50601, _inc_path_50647);

    /** 		cache_substrings = append(cache_substrings,{})*/
    RefDS(_22682);
    Append(&_38cache_substrings_50602, _38cache_substrings_50602, _22682);

    /** 		cache_starts = append(cache_starts,{})*/
    RefDS(_22682);
    Append(&_38cache_starts_50603, _38cache_starts_50603, _22682);

    /** 		cache_ends = append(cache_ends,{})*/
    RefDS(_22682);
    Append(&_38cache_ends_50604, _38cache_ends_50604, _22682);

    /** 		ifdef WINDOWS then*/

    /** 			cache_converted = append(cache_converted,{})*/
    RefDS(_22682);
    Append(&_38cache_converted_50605, _38cache_converted_50605, _22682);

    /** 		num_var = length(cache_vars)*/
    if (IS_SEQUENCE(_38cache_vars_50600)){
            _38num_var_50599 = SEQ_PTR(_38cache_vars_50600)->length;
    }
    else {
        _38num_var_50599 = 1;
    }

    /** 		cache_complete &= 0*/
    Append(&_38cache_complete_50606, _38cache_complete_50606, 0);

    /** 		cache_delims &= 0*/
    Append(&_38cache_delims_50607, _38cache_delims_50607, 0);

    /** 		return 0*/
    DeRefDS(_env_50646);
    DeRefDSi(_inc_path_50647);
    return 0;
    goto L2; // [91] 456
L1: 

    /** 		if compare(inc_path,cache_strings[num_var]) then*/
    _2 = (int)SEQ_PTR(_38cache_strings_50601);
    _26632 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
    if (IS_ATOM_INT(_inc_path_50647) && IS_ATOM_INT(_26632)){
        _26633 = (_inc_path_50647 < _26632) ? -1 : (_inc_path_50647 > _26632);
    }
    else{
        _26633 = compare(_inc_path_50647, _26632);
    }
    _26632 = NOVALUE;
    if (_26633 == 0)
    {
        _26633 = NOVALUE;
        goto L3; // [108] 455
    }
    else{
        _26633 = NOVALUE;
    }

    /** 			cache_strings[num_var] = inc_path*/
    RefDS(_inc_path_50647);
    _2 = (int)SEQ_PTR(_38cache_strings_50601);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _38cache_strings_50601 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
    _1 = *(int *)_2;
    *(int *)_2 = _inc_path_50647;
    DeRefDS(_1);

    /** 			cache_complete[num_var] = 0*/
    _2 = (int)SEQ_PTR(_38cache_complete_50606);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _38cache_complete_50606 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
    *(int *)_2 = 0;

    /** 			if match(cache_strings[num_var],inc_path)!=1 then -- try to salvage what we can*/
    _2 = (int)SEQ_PTR(_38cache_strings_50601);
    _26634 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
    _26635 = e_match_from(_26634, _inc_path_50647, 1);
    _26634 = NOVALUE;
    if (_26635 == 1)
    goto L4; // [146] 454

    /** 				pos = -1*/
    _pos_50649 = -1;

    /** 				for i=1 to length(cache_strings[num_var]) do*/
    _2 = (int)SEQ_PTR(_38cache_strings_50601);
    _26637 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
    if (IS_SEQUENCE(_26637)){
            _26638 = SEQ_PTR(_26637)->length;
    }
    else {
        _26638 = 1;
    }
    _26637 = NOVALUE;
    {
        int _i_50670;
        _i_50670 = 1;
L5: 
        if (_i_50670 > _26638){
            goto L6; // [168] 453
        }

        /** 					if cache_ends[num_var][i] > length(inc_path) or */
        _2 = (int)SEQ_PTR(_38cache_ends_50604);
        _26639 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        _2 = (int)SEQ_PTR(_26639);
        _26640 = (int)*(((s1_ptr)_2)->base + _i_50670);
        _26639 = NOVALUE;
        if (IS_SEQUENCE(_inc_path_50647)){
                _26641 = SEQ_PTR(_inc_path_50647)->length;
        }
        else {
            _26641 = 1;
        }
        if (IS_ATOM_INT(_26640)) {
            _26642 = (_26640 > _26641);
        }
        else {
            _26642 = binary_op(GREATER, _26640, _26641);
        }
        _26640 = NOVALUE;
        _26641 = NOVALUE;
        if (IS_ATOM_INT(_26642)) {
            if (_26642 != 0) {
                goto L7; // [196] 250
            }
        }
        else {
            if (DBL_PTR(_26642)->dbl != 0.0) {
                goto L7; // [196] 250
            }
        }
        _2 = (int)SEQ_PTR(_38cache_substrings_50602);
        _26644 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        _2 = (int)SEQ_PTR(_26644);
        _26645 = (int)*(((s1_ptr)_2)->base + _i_50670);
        _26644 = NOVALUE;
        _2 = (int)SEQ_PTR(_38cache_starts_50603);
        _26646 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        _2 = (int)SEQ_PTR(_26646);
        _26647 = (int)*(((s1_ptr)_2)->base + _i_50670);
        _26646 = NOVALUE;
        _2 = (int)SEQ_PTR(_38cache_ends_50604);
        _26648 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        _2 = (int)SEQ_PTR(_26648);
        _26649 = (int)*(((s1_ptr)_2)->base + _i_50670);
        _26648 = NOVALUE;
        rhs_slice_target = (object_ptr)&_26650;
        RHS_Slice(_inc_path_50647, _26647, _26649);
        if (IS_ATOM_INT(_26645) && IS_ATOM_INT(_26650)){
            _26651 = (_26645 < _26650) ? -1 : (_26645 > _26650);
        }
        else{
            _26651 = compare(_26645, _26650);
        }
        _26645 = NOVALUE;
        DeRefDS(_26650);
        _26650 = NOVALUE;
        if (_26651 == 0)
        {
            _26651 = NOVALUE;
            goto L8; // [246] 261
        }
        else{
            _26651 = NOVALUE;
        }
L7: 

        /** 						pos = i-1*/
        _pos_50649 = _i_50670 - 1;

        /** 						exit*/
        goto L6; // [258] 453
L8: 

        /** 					if pos = 0 then*/
        if (_pos_50649 != 0)
        goto L9; // [263] 276

        /** 						return 0*/
        DeRefDS(_env_50646);
        DeRefDSi(_inc_path_50647);
        _26637 = NOVALUE;
        DeRef(_26642);
        _26642 = NOVALUE;
        _26647 = NOVALUE;
        _26649 = NOVALUE;
        return 0;
        goto LA; // [273] 446
L9: 

        /** 					elsif pos >0 then -- crop cache data*/
        if (_pos_50649 <= 0)
        goto LB; // [278] 445

        /** 						cache_substrings[num_var] = cache_substrings[num_var][1..pos]*/
        _2 = (int)SEQ_PTR(_38cache_substrings_50602);
        _26655 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        rhs_slice_target = (object_ptr)&_26656;
        RHS_Slice(_26655, 1, _pos_50649);
        _26655 = NOVALUE;
        _2 = (int)SEQ_PTR(_38cache_substrings_50602);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_substrings_50602 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
        _1 = *(int *)_2;
        *(int *)_2 = _26656;
        if( _1 != _26656 ){
            DeRefDS(_1);
        }
        _26656 = NOVALUE;

        /** 						cache_starts[num_var] = cache_starts[num_var][1..pos]*/
        _2 = (int)SEQ_PTR(_38cache_starts_50603);
        _26657 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        rhs_slice_target = (object_ptr)&_26658;
        RHS_Slice(_26657, 1, _pos_50649);
        _26657 = NOVALUE;
        _2 = (int)SEQ_PTR(_38cache_starts_50603);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_starts_50603 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
        _1 = *(int *)_2;
        *(int *)_2 = _26658;
        if( _1 != _26658 ){
            DeRef(_1);
        }
        _26658 = NOVALUE;

        /** 						cache_ends[num_var] = cache_ends[num_var][1..pos]*/
        _2 = (int)SEQ_PTR(_38cache_ends_50604);
        _26659 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        rhs_slice_target = (object_ptr)&_26660;
        RHS_Slice(_26659, 1, _pos_50649);
        _26659 = NOVALUE;
        _2 = (int)SEQ_PTR(_38cache_ends_50604);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_ends_50604 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
        _1 = *(int *)_2;
        *(int *)_2 = _26660;
        if( _1 != _26660 ){
            DeRef(_1);
        }
        _26660 = NOVALUE;

        /** 						ifdef WINDOWS then*/

        /** 							cache_converted[num_var] = cache_converted[num_var][1..pos]*/
        _2 = (int)SEQ_PTR(_38cache_converted_50605);
        _26661 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        rhs_slice_target = (object_ptr)&_26662;
        RHS_Slice(_26661, 1, _pos_50649);
        _26661 = NOVALUE;
        _2 = (int)SEQ_PTR(_38cache_converted_50605);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_converted_50605 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
        _1 = *(int *)_2;
        *(int *)_2 = _26662;
        if( _1 != _26662 ){
            DeRef(_1);
        }
        _26662 = NOVALUE;

        /** 						delim = cache_ends[num_var][$]+1*/
        _2 = (int)SEQ_PTR(_38cache_ends_50604);
        _26663 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        if (IS_SEQUENCE(_26663)){
                _26664 = SEQ_PTR(_26663)->length;
        }
        else {
            _26664 = 1;
        }
        _2 = (int)SEQ_PTR(_26663);
        _26665 = (int)*(((s1_ptr)_2)->base + _26664);
        _26663 = NOVALUE;
        if (IS_ATOM_INT(_26665)) {
            _delim_50648 = _26665 + 1;
        }
        else
        { // coercing _delim_50648 to an integer 1
            _delim_50648 = 1+(long)(DBL_PTR(_26665)->dbl);
            if( !IS_ATOM_INT(_delim_50648) ){
                _delim_50648 = (object)DBL_PTR(_delim_50648)->dbl;
            }
        }
        _26665 = NOVALUE;

        /** 						while delim <= length(inc_path) and delim != PATH_SEPARATOR do*/
LC: 
        if (IS_SEQUENCE(_inc_path_50647)){
                _26667 = SEQ_PTR(_inc_path_50647)->length;
        }
        else {
            _26667 = 1;
        }
        _26668 = (_delim_50648 <= _26667);
        _26667 = NOVALUE;
        if (_26668 == 0) {
            goto LD; // [409] 434
        }
        _26670 = (_delim_50648 != 59);
        if (_26670 == 0)
        {
            DeRef(_26670);
            _26670 = NOVALUE;
            goto LD; // [420] 434
        }
        else{
            DeRef(_26670);
            _26670 = NOVALUE;
        }

        /** 							delim+=1*/
        _delim_50648 = _delim_50648 + 1;

        /** 						end while*/
        goto LC; // [431] 402
LD: 

        /** 						cache_delims[num_var] = delim*/
        _2 = (int)SEQ_PTR(_38cache_delims_50607);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_delims_50607 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
        *(int *)_2 = _delim_50648;
LB: 
LA: 

        /** 				end for*/
        _i_50670 = _i_50670 + 1;
        goto L5; // [448] 175
L6: 
        ;
    }
L4: 
L3: 
L2: 

    /** 	return 1*/
    DeRefDS(_env_50646);
    DeRefDSi(_inc_path_50647);
    _26637 = NOVALUE;
    DeRef(_26668);
    _26668 = NOVALUE;
    DeRef(_26642);
    _26642 = NOVALUE;
    _26647 = NOVALUE;
    _26649 = NOVALUE;
    return 1;
    ;
}


int _38get_conf_dirs()
{
    int _delimiter_50713 = NOVALUE;
    int _dirs_50714 = NOVALUE;
    int _26675 = NOVALUE;
    int _26673 = NOVALUE;
    int _26672 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef UNIX then*/

    /** 		delimiter = ';'*/
    _delimiter_50713 = 59;

    /** 	dirs = ""*/
    RefDS(_22682);
    DeRef(_dirs_50714);
    _dirs_50714 = _22682;

    /** 	for i = 1 to length(config_inc_paths) do*/
    if (IS_SEQUENCE(_38config_inc_paths_50608)){
            _26672 = SEQ_PTR(_38config_inc_paths_50608)->length;
    }
    else {
        _26672 = 1;
    }
    {
        int _i_50716;
        _i_50716 = 1;
L1: 
        if (_i_50716 > _26672){
            goto L2; // [22] 68
        }

        /** 		dirs &= config_inc_paths[i]*/
        _2 = (int)SEQ_PTR(_38config_inc_paths_50608);
        _26673 = (int)*(((s1_ptr)_2)->base + _i_50716);
        Concat((object_ptr)&_dirs_50714, _dirs_50714, _26673);
        _26673 = NOVALUE;

        /** 		if i != length(config_inc_paths) then*/
        if (IS_SEQUENCE(_38config_inc_paths_50608)){
                _26675 = SEQ_PTR(_38config_inc_paths_50608)->length;
        }
        else {
            _26675 = 1;
        }
        if (_i_50716 == _26675)
        goto L3; // [48] 61

        /** 			dirs &= delimiter*/
        Append(&_dirs_50714, _dirs_50714, _delimiter_50713);
L3: 

        /** 	end for*/
        _i_50716 = _i_50716 + 1;
        goto L1; // [63] 29
L2: 
        ;
    }

    /** 	return dirs*/
    return _dirs_50714;
    ;
}


int _38strip_file_from_path(int _full_path_50726)
{
    int _26681 = NOVALUE;
    int _26679 = NOVALUE;
    int _26678 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = length(full_path) to 1 by -1 do*/
    if (IS_SEQUENCE(_full_path_50726)){
            _26678 = SEQ_PTR(_full_path_50726)->length;
    }
    else {
        _26678 = 1;
    }
    {
        int _i_50728;
        _i_50728 = _26678;
L1: 
        if (_i_50728 < 1){
            goto L2; // [8] 46
        }

        /** 		if full_path[i] = SLASH then*/
        _2 = (int)SEQ_PTR(_full_path_50726);
        _26679 = (int)*(((s1_ptr)_2)->base + _i_50728);
        if (binary_op_a(NOTEQ, _26679, 92)){
            _26679 = NOVALUE;
            goto L3; // [23] 39
        }
        _26679 = NOVALUE;

        /** 			return full_path[1..i]*/
        rhs_slice_target = (object_ptr)&_26681;
        RHS_Slice(_full_path_50726, 1, _i_50728);
        DeRefDS(_full_path_50726);
        return _26681;
L3: 

        /** 	end for*/
        _i_50728 = _i_50728 + -1;
        goto L1; // [41] 15
L2: 
        ;
    }

    /** 	return ""*/
    RefDS(_22682);
    DeRefDS(_full_path_50726);
    DeRef(_26681);
    _26681 = NOVALUE;
    return _22682;
    ;
}


int _38expand_path(int _path_50737, int _prefix_50738)
{
    int _absolute_50739 = NOVALUE;
    int _26696 = NOVALUE;
    int _26695 = NOVALUE;
    int _26694 = NOVALUE;
    int _26693 = NOVALUE;
    int _26692 = NOVALUE;
    int _26691 = NOVALUE;
    int _26687 = NOVALUE;
    int _26686 = NOVALUE;
    int _26685 = NOVALUE;
    int _26682 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length(path) then*/
    if (IS_SEQUENCE(_path_50737)){
            _26682 = SEQ_PTR(_path_50737)->length;
    }
    else {
        _26682 = 1;
    }
    if (_26682 != 0)
    goto L1; // [10] 22
    _26682 = NOVALUE;

    /** 		return pwd*/
    RefDS(_38pwd_50635);
    DeRefDS(_path_50737);
    DeRefDS(_prefix_50738);
    return _38pwd_50635;
L1: 

    /** 	ifdef UNIX then*/

    /** 		absolute = find(path[1], SLASH_CHARS) or find(':', path)*/
    _2 = (int)SEQ_PTR(_path_50737);
    _26685 = (int)*(((s1_ptr)_2)->base + 1);
    _26686 = find_from(_26685, _36SLASH_CHARS_14735, 1);
    _26685 = NOVALUE;
    _26687 = find_from(58, _path_50737, 1);
    _absolute_50739 = (_26686 != 0 || _26687 != 0);
    _26686 = NOVALUE;
    _26687 = NOVALUE;

    /** 	if not absolute then*/
    if (_absolute_50739 != 0)
    goto L2; // [50] 64

    /** 		path = prefix & SLASH & path*/
    {
        int concat_list[3];

        concat_list[0] = _path_50737;
        concat_list[1] = 92;
        concat_list[2] = _prefix_50738;
        Concat_N((object_ptr)&_path_50737, concat_list, 3);
    }
L2: 

    /** 	if length(path) and not find(path[$], SLASH_CHARS) then*/
    if (IS_SEQUENCE(_path_50737)){
            _26691 = SEQ_PTR(_path_50737)->length;
    }
    else {
        _26691 = 1;
    }
    if (_26691 == 0) {
        goto L3; // [69] 103
    }
    if (IS_SEQUENCE(_path_50737)){
            _26693 = SEQ_PTR(_path_50737)->length;
    }
    else {
        _26693 = 1;
    }
    _2 = (int)SEQ_PTR(_path_50737);
    _26694 = (int)*(((s1_ptr)_2)->base + _26693);
    _26695 = find_from(_26694, _36SLASH_CHARS_14735, 1);
    _26694 = NOVALUE;
    _26696 = (_26695 == 0);
    _26695 = NOVALUE;
    if (_26696 == 0)
    {
        DeRef(_26696);
        _26696 = NOVALUE;
        goto L3; // [91] 103
    }
    else{
        DeRef(_26696);
        _26696 = NOVALUE;
    }

    /** 		path &= SLASH*/
    Append(&_path_50737, _path_50737, 92);
L3: 

    /** 	return path*/
    DeRefDS(_prefix_50738);
    return _path_50737;
    ;
}


void _38add_include_directory(int _path_50765)
{
    int _26699 = NOVALUE;
    int _0, _1, _2;
    

    /** 	path = expand_path( path, pwd )*/
    RefDS(_path_50765);
    RefDS(_38pwd_50635);
    _0 = _path_50765;
    _path_50765 = _38expand_path(_path_50765, _38pwd_50635);
    DeRefDS(_0);

    /** 	if not find( path, config_inc_paths ) then*/
    _26699 = find_from(_path_50765, _38config_inc_paths_50608, 1);
    if (_26699 != 0)
    goto L1; // [23] 35
    _26699 = NOVALUE;

    /** 		config_inc_paths = append( config_inc_paths, path )*/
    RefDS(_path_50765);
    Append(&_38config_inc_paths_50608, _38config_inc_paths_50608, _path_50765);
L1: 

    /** end procedure*/
    DeRefDS(_path_50765);
    return;
    ;
}


int _38load_euphoria_config(int _file_50774)
{
    int _fn_50775 = NOVALUE;
    int _in_50776 = NOVALUE;
    int _spos_50777 = NOVALUE;
    int _epos_50778 = NOVALUE;
    int _conf_path_50779 = NOVALUE;
    int _new_args_50780 = NOVALUE;
    int _arg_50781 = NOVALUE;
    int _parm_50782 = NOVALUE;
    int _section_50783 = NOVALUE;
    int _needed_50880 = NOVALUE;
    int _26797 = NOVALUE;
    int _26796 = NOVALUE;
    int _26793 = NOVALUE;
    int _26792 = NOVALUE;
    int _26790 = NOVALUE;
    int _26789 = NOVALUE;
    int _26767 = NOVALUE;
    int _26764 = NOVALUE;
    int _26763 = NOVALUE;
    int _26761 = NOVALUE;
    int _26757 = NOVALUE;
    int _26755 = NOVALUE;
    int _26753 = NOVALUE;
    int _26751 = NOVALUE;
    int _26749 = NOVALUE;
    int _26747 = NOVALUE;
    int _26746 = NOVALUE;
    int _26745 = NOVALUE;
    int _26744 = NOVALUE;
    int _26743 = NOVALUE;
    int _26742 = NOVALUE;
    int _26741 = NOVALUE;
    int _26740 = NOVALUE;
    int _26738 = NOVALUE;
    int _26736 = NOVALUE;
    int _26734 = NOVALUE;
    int _26730 = NOVALUE;
    int _26729 = NOVALUE;
    int _26724 = NOVALUE;
    int _26722 = NOVALUE;
    int _26720 = NOVALUE;
    int _26719 = NOVALUE;
    int _26711 = NOVALUE;
    int _26705 = NOVALUE;
    int _26704 = NOVALUE;
    int _26702 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence new_args = {}*/
    RefDS(_22682);
    DeRef(_new_args_50780);
    _new_args_50780 = _22682;

    /** 	if file_type(file) = FILETYPE_DIRECTORY then*/
    RefDS(_file_50774);
    _26702 = _9file_type(_file_50774);
    if (binary_op_a(NOTEQ, _26702, 2)){
        DeRef(_26702);
        _26702 = NOVALUE;
        goto L1; // [18] 53
    }
    DeRef(_26702);
    _26702 = NOVALUE;

    /** 		if file[$] != SLASH then*/
    if (IS_SEQUENCE(_file_50774)){
            _26704 = SEQ_PTR(_file_50774)->length;
    }
    else {
        _26704 = 1;
    }
    _2 = (int)SEQ_PTR(_file_50774);
    _26705 = (int)*(((s1_ptr)_2)->base + _26704);
    if (binary_op_a(EQUALS, _26705, 92)){
        _26705 = NOVALUE;
        goto L2; // [33] 46
    }
    _26705 = NOVALUE;

    /** 			file &= SLASH*/
    Append(&_file_50774, _file_50774, 92);
L2: 

    /** 		file &= "eu.cfg"*/
    Concat((object_ptr)&_file_50774, _file_50774, _26708);
L1: 

    /** 	conf_path = canonical_path( file,,CORRECT )*/
    RefDS(_file_50774);
    _0 = _conf_path_50779;
    _conf_path_50779 = _9canonical_path(_file_50774, 0, 2);
    DeRef(_0);

    /** 	if find(conf_path, seen_conf) != 0 then*/
    _26711 = find_from(_conf_path_50779, _38seen_conf_50771, 1);
    if (_26711 == 0)
    goto L3; // [74] 85

    /** 		return {}*/
    RefDS(_22682);
    DeRefDS(_file_50774);
    DeRefi(_in_50776);
    DeRefDS(_conf_path_50779);
    DeRef(_new_args_50780);
    DeRefi(_arg_50781);
    DeRefi(_parm_50782);
    DeRef(_section_50783);
    return _22682;
L3: 

    /** 	seen_conf = append(seen_conf, conf_path)*/
    RefDS(_conf_path_50779);
    Append(&_38seen_conf_50771, _38seen_conf_50771, _conf_path_50779);

    /** 	section = "all"*/
    RefDS(_26714);
    DeRef(_section_50783);
    _section_50783 = _26714;

    /** 	fn = open( conf_path, "r" )*/
    _fn_50775 = EOpen(_conf_path_50779, _26715, 0);

    /** 	if fn = -1 then return {} end if*/
    if (_fn_50775 != -1)
    goto L4; // [109] 118
    RefDS(_22682);
    DeRefDS(_file_50774);
    DeRefi(_in_50776);
    DeRefDS(_conf_path_50779);
    DeRef(_new_args_50780);
    DeRefi(_arg_50781);
    DeRefi(_parm_50782);
    DeRefDSi(_section_50783);
    return _22682;
L4: 

    /** 	in = gets( fn )*/
    DeRefi(_in_50776);
    _in_50776 = EGets(_fn_50775);

    /** 	while sequence( in ) do*/
L5: 
    _26719 = IS_SEQUENCE(_in_50776);
    if (_26719 == 0)
    {
        _26719 = NOVALUE;
        goto L6; // [131] 771
    }
    else{
        _26719 = NOVALUE;
    }

    /** 		spos = 1*/
    _spos_50777 = 1;

    /** 		while spos <= length(in) do*/
L7: 
    if (IS_SEQUENCE(_in_50776)){
            _26720 = SEQ_PTR(_in_50776)->length;
    }
    else {
        _26720 = 1;
    }
    if (_spos_50777 > _26720)
    goto L8; // [147] 182

    /** 			if find( in[spos], "\n\r \t" ) = 0 then*/
    _2 = (int)SEQ_PTR(_in_50776);
    _26722 = (int)*(((s1_ptr)_2)->base + _spos_50777);
    _26724 = find_from(_26722, _26723, 1);
    _26722 = NOVALUE;
    if (_26724 != 0)
    goto L9; // [162] 171

    /** 				exit*/
    goto L8; // [168] 182
L9: 

    /** 			spos += 1*/
    _spos_50777 = _spos_50777 + 1;

    /** 		end while*/
    goto L7; // [179] 144
L8: 

    /** 		epos = length(in)*/
    if (IS_SEQUENCE(_in_50776)){
            _epos_50778 = SEQ_PTR(_in_50776)->length;
    }
    else {
        _epos_50778 = 1;
    }

    /** 		while epos >= spos do*/
LA: 
    if (_epos_50778 < _spos_50777)
    goto LB; // [192] 227

    /** 			if find( in[epos], "\n\r \t" ) = 0 then*/
    _2 = (int)SEQ_PTR(_in_50776);
    _26729 = (int)*(((s1_ptr)_2)->base + _epos_50778);
    _26730 = find_from(_26729, _26723, 1);
    _26729 = NOVALUE;
    if (_26730 != 0)
    goto LC; // [207] 216

    /** 				exit*/
    goto LB; // [213] 227
LC: 

    /** 			epos -= 1*/
    _epos_50778 = _epos_50778 - 1;

    /** 		end while*/
    goto LA; // [224] 192
LB: 

    /** 		in = in[spos .. epos]		*/
    rhs_slice_target = (object_ptr)&_in_50776;
    RHS_Slice(_in_50776, _spos_50777, _epos_50778);

    /** 		arg = ""*/
    RefDS(_22682);
    DeRefi(_arg_50781);
    _arg_50781 = _22682;

    /** 		parm = ""*/
    RefDS(_22682);
    DeRefi(_parm_50782);
    _parm_50782 = _22682;

    /** 		if length(in) > 0 then*/
    if (IS_SEQUENCE(_in_50776)){
            _26734 = SEQ_PTR(_in_50776)->length;
    }
    else {
        _26734 = 1;
    }
    if (_26734 <= 0)
    goto LD; // [253] 477

    /** 			if in[1] = '[' then*/
    _2 = (int)SEQ_PTR(_in_50776);
    _26736 = (int)*(((s1_ptr)_2)->base + 1);
    if (_26736 != 91)
    goto LE; // [263] 354

    /** 				section = in[2..$]*/
    if (IS_SEQUENCE(_in_50776)){
            _26738 = SEQ_PTR(_in_50776)->length;
    }
    else {
        _26738 = 1;
    }
    rhs_slice_target = (object_ptr)&_section_50783;
    RHS_Slice(_in_50776, 2, _26738);

    /** 				if length(section) > 0 and section[$] = ']' then*/
    if (IS_SEQUENCE(_section_50783)){
            _26740 = SEQ_PTR(_section_50783)->length;
    }
    else {
        _26740 = 1;
    }
    _26741 = (_26740 > 0);
    _26740 = NOVALUE;
    if (_26741 == 0) {
        goto LF; // [286] 320
    }
    if (IS_SEQUENCE(_section_50783)){
            _26743 = SEQ_PTR(_section_50783)->length;
    }
    else {
        _26743 = 1;
    }
    _2 = (int)SEQ_PTR(_section_50783);
    _26744 = (int)*(((s1_ptr)_2)->base + _26743);
    _26745 = (_26744 == 93);
    _26744 = NOVALUE;
    if (_26745 == 0)
    {
        DeRef(_26745);
        _26745 = NOVALUE;
        goto LF; // [302] 320
    }
    else{
        DeRef(_26745);
        _26745 = NOVALUE;
    }

    /** 					section = section[1..$-1]*/
    if (IS_SEQUENCE(_section_50783)){
            _26746 = SEQ_PTR(_section_50783)->length;
    }
    else {
        _26746 = 1;
    }
    _26747 = _26746 - 1;
    _26746 = NOVALUE;
    rhs_slice_target = (object_ptr)&_section_50783;
    RHS_Slice(_section_50783, 1, _26747);
LF: 

    /** 				section = lower(trim(section))*/
    RefDS(_section_50783);
    RefDS(_4443);
    _26749 = _4trim(_section_50783, _4443, 0);
    _0 = _section_50783;
    _section_50783 = _4lower(_26749);
    DeRefDS(_0);
    _26749 = NOVALUE;

    /** 				if length(section) = 0 then*/
    if (IS_SEQUENCE(_section_50783)){
            _26751 = SEQ_PTR(_section_50783)->length;
    }
    else {
        _26751 = 1;
    }
    if (_26751 != 0)
    goto L10; // [339] 476

    /** 					section = "all"*/
    RefDS(_26714);
    DeRefDS(_section_50783);
    _section_50783 = _26714;
    goto L10; // [351] 476
LE: 

    /** 			elsif length(in) > 2 then*/
    if (IS_SEQUENCE(_in_50776)){
            _26753 = SEQ_PTR(_in_50776)->length;
    }
    else {
        _26753 = 1;
    }
    if (_26753 <= 2)
    goto L11; // [359] 461

    /** 				if in[1] = '-' then*/
    _2 = (int)SEQ_PTR(_in_50776);
    _26755 = (int)*(((s1_ptr)_2)->base + 1);
    if (_26755 != 45)
    goto L12; // [369] 443

    /** 					if in[2] != '-' then*/
    _2 = (int)SEQ_PTR(_in_50776);
    _26757 = (int)*(((s1_ptr)_2)->base + 2);
    if (_26757 == 45)
    goto L10; // [379] 476

    /** 						spos = find(' ', in)*/
    _spos_50777 = find_from(32, _in_50776, 1);

    /** 						if spos = 0 then*/
    if (_spos_50777 != 0)
    goto L13; // [392] 413

    /** 							arg = in*/
    Ref(_in_50776);
    DeRefi(_arg_50781);
    _arg_50781 = _in_50776;

    /** 							parm = ""*/
    RefDS(_22682);
    DeRefi(_parm_50782);
    _parm_50782 = _22682;
    goto L10; // [410] 476
L13: 

    /** 							arg = in[1..spos - 1]*/
    _26761 = _spos_50777 - 1;
    rhs_slice_target = (object_ptr)&_arg_50781;
    RHS_Slice(_in_50776, 1, _26761);

    /** 							parm = in[spos + 1 .. $]*/
    _26763 = _spos_50777 + 1;
    if (_26763 > MAXINT){
        _26763 = NewDouble((double)_26763);
    }
    if (IS_SEQUENCE(_in_50776)){
            _26764 = SEQ_PTR(_in_50776)->length;
    }
    else {
        _26764 = 1;
    }
    rhs_slice_target = (object_ptr)&_parm_50782;
    RHS_Slice(_in_50776, _26763, _26764);
    goto L10; // [440] 476
L12: 

    /** 					arg = "-i"*/
    RefDS(_26766);
    DeRefi(_arg_50781);
    _arg_50781 = _26766;

    /** 					parm = in*/
    Ref(_in_50776);
    DeRefi(_parm_50782);
    _parm_50782 = _in_50776;
    goto L10; // [458] 476
L11: 

    /** 				arg = "-i"*/
    RefDS(_26766);
    DeRefi(_arg_50781);
    _arg_50781 = _26766;

    /** 				parm = in*/
    Ref(_in_50776);
    DeRefi(_parm_50782);
    _parm_50782 = _in_50776;
L10: 
LD: 

    /** 		if length(arg) > 0 then*/
    if (IS_SEQUENCE(_arg_50781)){
            _26767 = SEQ_PTR(_arg_50781)->length;
    }
    else {
        _26767 = 1;
    }
    if (_26767 <= 0)
    goto L14; // [482] 759

    /** 			integer needed = 0*/
    _needed_50880 = 0;

    /** 			switch section do*/
    _1 = find(_section_50783, _26769);
    switch ( _1 ){ 

        /** 				case "all" then*/
        case 1:

        /** 					needed = 1*/
        _needed_50880 = 1;
        goto L15; // [507] 691

        /** 				case "windows" then*/
        case 2:

        /** 					needed = TWINDOWS*/
        _needed_50880 = _36TWINDOWS_14721;
        goto L15; // [522] 691

        /** 				case "unix" then*/
        case 3:

        /** 					needed = TUNIX*/
        _needed_50880 = _36TUNIX_14725;
        goto L15; // [537] 691

        /** 				case "translate" then*/
        case 4:

        /** 					needed = TRANSLATE*/
        _needed_50880 = _25TRANSLATE_11874;
        goto L15; // [552] 691

        /** 				case "translate:windows" then*/
        case 5:

        /** 					needed = TRANSLATE and TWINDOWS*/
        _needed_50880 = (_25TRANSLATE_11874 != 0 && _36TWINDOWS_14721 != 0);
        goto L15; // [570] 691

        /** 				case "translate:unix" then*/
        case 6:

        /** 					needed = TRANSLATE and TUNIX*/
        _needed_50880 = (_25TRANSLATE_11874 != 0 && _36TUNIX_14725 != 0);
        goto L15; // [588] 691

        /** 				case "interpret" then*/
        case 7:

        /** 					needed = INTERPRET*/
        _needed_50880 = _25INTERPRET_11871;
        goto L15; // [603] 691

        /** 				case "interpret:windows" then*/
        case 8:

        /** 					needed = INTERPRET and TWINDOWS*/
        _needed_50880 = (_25INTERPRET_11871 != 0 && _36TWINDOWS_14721 != 0);
        goto L15; // [621] 691

        /** 				case "interpret:unix" then*/
        case 9:

        /** 					needed = INTERPRET and TUNIX*/
        _needed_50880 = (_25INTERPRET_11871 != 0 && _36TUNIX_14725 != 0);
        goto L15; // [639] 691

        /** 				case "bind" then*/
        case 10:

        /** 					needed = BIND*/
        _needed_50880 = _25BIND_11877;
        goto L15; // [654] 691

        /** 				case "bind:windows" then*/
        case 11:

        /** 					needed = BIND and TWINDOWS*/
        _needed_50880 = (_25BIND_11877 != 0 && _36TWINDOWS_14721 != 0);
        goto L15; // [672] 691

        /** 				case "bind:unix" then*/
        case 12:

        /** 					needed = BIND and TUNIX*/
        _needed_50880 = (_25BIND_11877 != 0 && _36TUNIX_14725 != 0);
    ;}L15: 

    /** 			if needed then*/
    if (_needed_50880 == 0)
    {
        goto L16; // [693] 758
    }
    else{
    }

    /** 				if equal(arg, "-c") then*/
    if (_arg_50781 == _26788)
    _26789 = 1;
    else if (IS_ATOM_INT(_arg_50781) && IS_ATOM_INT(_26788))
    _26789 = 0;
    else
    _26789 = (compare(_arg_50781, _26788) == 0);
    if (_26789 == 0)
    {
        _26789 = NOVALUE;
        goto L17; // [702] 731
    }
    else{
        _26789 = NOVALUE;
    }

    /** 					if length(parm) > 0 then*/
    if (IS_SEQUENCE(_parm_50782)){
            _26790 = SEQ_PTR(_parm_50782)->length;
    }
    else {
        _26790 = 1;
    }
    if (_26790 <= 0)
    goto L18; // [710] 757

    /** 						new_args &= load_euphoria_config(parm)*/
    RefDS(_parm_50782);
    DeRef(_26792);
    _26792 = _parm_50782;
    _26793 = _38load_euphoria_config(_26792);
    _26792 = NOVALUE;
    if (IS_SEQUENCE(_new_args_50780) && IS_ATOM(_26793)) {
        Ref(_26793);
        Append(&_new_args_50780, _new_args_50780, _26793);
    }
    else if (IS_ATOM(_new_args_50780) && IS_SEQUENCE(_26793)) {
    }
    else {
        Concat((object_ptr)&_new_args_50780, _new_args_50780, _26793);
    }
    DeRef(_26793);
    _26793 = NOVALUE;
    goto L18; // [728] 757
L17: 

    /** 					new_args = append(new_args, arg)*/
    RefDS(_arg_50781);
    Append(&_new_args_50780, _new_args_50780, _arg_50781);

    /** 					if length(parm > 0) then*/
    _26796 = binary_op(GREATER, _parm_50782, 0);
    if (IS_SEQUENCE(_26796)){
            _26797 = SEQ_PTR(_26796)->length;
    }
    else {
        _26797 = 1;
    }
    DeRefDS(_26796);
    _26796 = NOVALUE;
    if (_26797 == 0)
    {
        _26797 = NOVALUE;
        goto L19; // [746] 756
    }
    else{
        _26797 = NOVALUE;
    }

    /** 						new_args = append(new_args, parm)*/
    RefDS(_parm_50782);
    Append(&_new_args_50780, _new_args_50780, _parm_50782);
L19: 
L18: 
L16: 
L14: 

    /** 		in = gets( fn )*/
    DeRefi(_in_50776);
    _in_50776 = EGets(_fn_50775);

    /** 	end while*/
    goto L5; // [768] 128
L6: 

    /** 	close(fn)*/
    EClose(_fn_50775);

    /** 	return new_args*/
    DeRefDS(_file_50774);
    DeRefi(_in_50776);
    DeRef(_conf_path_50779);
    DeRefi(_arg_50781);
    DeRefi(_parm_50782);
    DeRef(_section_50783);
    _26736 = NOVALUE;
    DeRef(_26741);
    _26741 = NOVALUE;
    DeRef(_26747);
    _26747 = NOVALUE;
    _26755 = NOVALUE;
    _26757 = NOVALUE;
    DeRef(_26761);
    _26761 = NOVALUE;
    DeRef(_26763);
    _26763 = NOVALUE;
    _26796 = NOVALUE;
    return _new_args_50780;
    ;
}


int _38GetDefaultArgs(int _user_files_50948)
{
    int _env_50949 = NOVALUE;
    int _default_args_50950 = NOVALUE;
    int _conf_file_50951 = NOVALUE;
    int _cmd_options_50953 = NOVALUE;
    int _user_config_50959 = NOVALUE;
    int _26840 = NOVALUE;
    int _26839 = NOVALUE;
    int _26838 = NOVALUE;
    int _26835 = NOVALUE;
    int _26834 = NOVALUE;
    int _26833 = NOVALUE;
    int _26832 = NOVALUE;
    int _26829 = NOVALUE;
    int _26828 = NOVALUE;
    int _26827 = NOVALUE;
    int _26826 = NOVALUE;
    int _26822 = NOVALUE;
    int _26821 = NOVALUE;
    int _26820 = NOVALUE;
    int _26818 = NOVALUE;
    int _26812 = NOVALUE;
    int _26811 = NOVALUE;
    int _26809 = NOVALUE;
    int _26807 = NOVALUE;
    int _26806 = NOVALUE;
    int _26802 = NOVALUE;
    int _26801 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence default_args = {}*/
    RefDS(_22682);
    DeRef(_default_args_50950);
    _default_args_50950 = _22682;

    /** 	sequence conf_file = "eu.cfg"*/
    RefDS(_26708);
    DeRefi(_conf_file_50951);
    _conf_file_50951 = _26708;

    /** 	if loaded_config_inc_paths then return "" end if*/
    if (_38loaded_config_inc_paths_50633 == 0)
    {
        goto L1; // [21] 29
    }
    else{
    }
    RefDS(_22682);
    DeRefDS(_user_files_50948);
    DeRef(_env_50949);
    DeRefDS(_default_args_50950);
    DeRefDSi(_conf_file_50951);
    DeRef(_cmd_options_50953);
    return _22682;
L1: 

    /** 	loaded_config_inc_paths = 1*/
    _38loaded_config_inc_paths_50633 = 1;

    /** 	sequence cmd_options = get_options()*/
    _0 = _cmd_options_50953;
    _cmd_options_50953 = _39get_options();
    DeRef(_0);

    /** 	default_args = {}*/
    RefDS(_22682);
    DeRef(_default_args_50950);
    _default_args_50950 = _22682;

    /** 	for i = 1 to length( user_files ) do*/
    if (IS_SEQUENCE(_user_files_50948)){
            _26801 = SEQ_PTR(_user_files_50948)->length;
    }
    else {
        _26801 = 1;
    }
    {
        int _i_50957;
        _i_50957 = 1;
L2: 
        if (_i_50957 > _26801){
            goto L3; // [53] 92
        }

        /** 		sequence user_config = load_euphoria_config( user_files[i] )*/
        _2 = (int)SEQ_PTR(_user_files_50948);
        _26802 = (int)*(((s1_ptr)_2)->base + _i_50957);
        Ref(_26802);
        _0 = _user_config_50959;
        _user_config_50959 = _38load_euphoria_config(_26802);
        DeRef(_0);
        _26802 = NOVALUE;

        /** 		default_args = merge_parameters( user_config, default_args, cmd_options, 1 )*/
        RefDS(_user_config_50959);
        RefDS(_default_args_50950);
        RefDS(_cmd_options_50953);
        _0 = _default_args_50950;
        _default_args_50950 = _39merge_parameters(_user_config_50959, _default_args_50950, _cmd_options_50953, 1);
        DeRefDS(_0);
        DeRefDS(_user_config_50959);
        _user_config_50959 = NOVALUE;

        /** 	end for*/
        _i_50957 = _i_50957 + 1;
        goto L2; // [87] 60
L3: 
        ;
    }

    /** 	default_args = merge_parameters( load_euphoria_config("./" & conf_file), default_args, cmd_options, 1 )*/
    Concat((object_ptr)&_26806, _26805, _conf_file_50951);
    _26807 = _38load_euphoria_config(_26806);
    _26806 = NOVALUE;
    RefDS(_default_args_50950);
    RefDS(_cmd_options_50953);
    _0 = _default_args_50950;
    _default_args_50950 = _39merge_parameters(_26807, _default_args_50950, _cmd_options_50953, 1);
    DeRefDS(_0);
    _26807 = NOVALUE;

    /** 	env = strip_file_from_path( exe_path() )*/
    _26809 = _38exe_path();
    _0 = _env_50949;
    _env_50949 = _38strip_file_from_path(_26809);
    DeRef(_0);
    _26809 = NOVALUE;

    /** 	default_args = merge_parameters( load_euphoria_config( env & conf_file ), default_args, cmd_options, 1 )*/
    if (IS_SEQUENCE(_env_50949) && IS_ATOM(_conf_file_50951)) {
    }
    else if (IS_ATOM(_env_50949) && IS_SEQUENCE(_conf_file_50951)) {
        Ref(_env_50949);
        Prepend(&_26811, _conf_file_50951, _env_50949);
    }
    else {
        Concat((object_ptr)&_26811, _env_50949, _conf_file_50951);
    }
    _26812 = _38load_euphoria_config(_26811);
    _26811 = NOVALUE;
    RefDS(_default_args_50950);
    RefDS(_cmd_options_50953);
    _0 = _default_args_50950;
    _default_args_50950 = _39merge_parameters(_26812, _default_args_50950, _cmd_options_50953, 1);
    DeRefDS(_0);
    _26812 = NOVALUE;

    /** 	ifdef UNIX then*/

    /** 		env = getenv( "ALLUSERSPROFILE" )*/
    DeRef(_env_50949);
    _env_50949 = EGetEnv(_26816);

    /** 		if sequence(env) then*/
    _26818 = IS_SEQUENCE(_env_50949);
    if (_26818 == 0)
    {
        _26818 = NOVALUE;
        goto L4; // [151] 179
    }
    else{
        _26818 = NOVALUE;
    }

    /** 			default_args = merge_parameters( load_euphoria_config( expand_path( "euphoria", env ) & conf_file ), default_args, cmd_options, 1 )*/
    RefDS(_26819);
    Ref(_env_50949);
    _26820 = _38expand_path(_26819, _env_50949);
    if (IS_SEQUENCE(_26820) && IS_ATOM(_conf_file_50951)) {
    }
    else if (IS_ATOM(_26820) && IS_SEQUENCE(_conf_file_50951)) {
        Ref(_26820);
        Prepend(&_26821, _conf_file_50951, _26820);
    }
    else {
        Concat((object_ptr)&_26821, _26820, _conf_file_50951);
        DeRef(_26820);
        _26820 = NOVALUE;
    }
    DeRef(_26820);
    _26820 = NOVALUE;
    _26822 = _38load_euphoria_config(_26821);
    _26821 = NOVALUE;
    RefDS(_default_args_50950);
    RefDS(_cmd_options_50953);
    _0 = _default_args_50950;
    _default_args_50950 = _39merge_parameters(_26822, _default_args_50950, _cmd_options_50953, 1);
    DeRefDS(_0);
    _26822 = NOVALUE;
L4: 

    /** 		env = getenv( "APPDATA" )*/
    DeRef(_env_50949);
    _env_50949 = EGetEnv(_26824);

    /** 		if sequence(env) then*/
    _26826 = IS_SEQUENCE(_env_50949);
    if (_26826 == 0)
    {
        _26826 = NOVALUE;
        goto L5; // [189] 217
    }
    else{
        _26826 = NOVALUE;
    }

    /** 			default_args = merge_parameters( load_euphoria_config( expand_path( "euphoria", env ) & conf_file ), default_args, cmd_options, 1 )*/
    RefDS(_26819);
    Ref(_env_50949);
    _26827 = _38expand_path(_26819, _env_50949);
    if (IS_SEQUENCE(_26827) && IS_ATOM(_conf_file_50951)) {
    }
    else if (IS_ATOM(_26827) && IS_SEQUENCE(_conf_file_50951)) {
        Ref(_26827);
        Prepend(&_26828, _conf_file_50951, _26827);
    }
    else {
        Concat((object_ptr)&_26828, _26827, _conf_file_50951);
        DeRef(_26827);
        _26827 = NOVALUE;
    }
    DeRef(_26827);
    _26827 = NOVALUE;
    _26829 = _38load_euphoria_config(_26828);
    _26828 = NOVALUE;
    RefDS(_default_args_50950);
    RefDS(_cmd_options_50953);
    _0 = _default_args_50950;
    _default_args_50950 = _39merge_parameters(_26829, _default_args_50950, _cmd_options_50953, 1);
    DeRefDS(_0);
    _26829 = NOVALUE;
L5: 

    /** 		env = getenv( "HOMEPATH" )*/
    DeRef(_env_50949);
    _env_50949 = EGetEnv(_26604);

    /** 		if sequence(env) then*/
    _26832 = IS_SEQUENCE(_env_50949);
    if (_26832 == 0)
    {
        _26832 = NOVALUE;
        goto L6; // [227] 256
    }
    else{
        _26832 = NOVALUE;
    }

    /** 			default_args = merge_parameters( load_euphoria_config( getenv( "HOMEDRIVE" ) & env & "\\" & conf_file ), default_args, cmd_options, 1 )*/
    _26833 = EGetEnv(_26606);
    {
        int concat_list[4];

        concat_list[0] = _conf_file_50951;
        concat_list[1] = _23009;
        concat_list[2] = _env_50949;
        concat_list[3] = _26833;
        Concat_N((object_ptr)&_26834, concat_list, 4);
    }
    DeRef(_26833);
    _26833 = NOVALUE;
    _26835 = _38load_euphoria_config(_26834);
    _26834 = NOVALUE;
    RefDS(_default_args_50950);
    RefDS(_cmd_options_50953);
    _0 = _default_args_50950;
    _default_args_50950 = _39merge_parameters(_26835, _default_args_50950, _cmd_options_50953, 1);
    DeRefDS(_0);
    _26835 = NOVALUE;
L6: 

    /** 	env = get_eudir()*/
    _0 = _env_50949;
    _env_50949 = _26get_eudir();
    DeRef(_0);

    /** 	if sequence(env) then*/
    _26838 = IS_SEQUENCE(_env_50949);
    if (_26838 == 0)
    {
        _26838 = NOVALUE;
        goto L7; // [266] 291
    }
    else{
        _26838 = NOVALUE;
    }

    /** 		default_args = merge_parameters( load_euphoria_config(env & "/" & conf_file), default_args, cmd_options, 1 )*/
    {
        int concat_list[3];

        concat_list[0] = _conf_file_50951;
        concat_list[1] = _24138;
        concat_list[2] = _env_50949;
        Concat_N((object_ptr)&_26839, concat_list, 3);
    }
    _26840 = _38load_euphoria_config(_26839);
    _26839 = NOVALUE;
    RefDS(_default_args_50950);
    RefDS(_cmd_options_50953);
    _0 = _default_args_50950;
    _default_args_50950 = _39merge_parameters(_26840, _default_args_50950, _cmd_options_50953, 1);
    DeRefDS(_0);
    _26840 = NOVALUE;
L7: 

    /** 	return default_args*/
    DeRefDS(_user_files_50948);
    DeRef(_env_50949);
    DeRefi(_conf_file_50951);
    DeRef(_cmd_options_50953);
    return _default_args_50950;
    ;
}


int _38ConfPath(int _file_name_51014)
{
    int _file_path_51015 = NOVALUE;
    int _try_51016 = NOVALUE;
    int _26847 = NOVALUE;
    int _26843 = NOVALUE;
    int _26842 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(config_inc_paths) do*/
    if (IS_SEQUENCE(_38config_inc_paths_50608)){
            _26842 = SEQ_PTR(_38config_inc_paths_50608)->length;
    }
    else {
        _26842 = 1;
    }
    {
        int _i_51018;
        _i_51018 = 1;
L1: 
        if (_i_51018 > _26842){
            goto L2; // [10] 60
        }

        /** 		file_path = config_inc_paths[i] & file_name*/
        _2 = (int)SEQ_PTR(_38config_inc_paths_50608);
        _26843 = (int)*(((s1_ptr)_2)->base + _i_51018);
        Concat((object_ptr)&_file_path_51015, _26843, _file_name_51014);
        _26843 = NOVALUE;
        _26843 = NOVALUE;

        /** 		try = open( file_path, "r" )*/
        _try_51016 = EOpen(_file_path_51015, _26715, 0);

        /** 		if try != -1 then*/
        if (_try_51016 == -1)
        goto L3; // [38] 53

        /** 			return {file_path, try}*/
        RefDS(_file_path_51015);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _file_path_51015;
        ((int *)_2)[2] = _try_51016;
        _26847 = MAKE_SEQ(_1);
        DeRefDS(_file_name_51014);
        DeRefDS(_file_path_51015);
        return _26847;
L3: 

        /** 	end for*/
        _i_51018 = _i_51018 + 1;
        goto L1; // [55] 17
L2: 
        ;
    }

    /** 	return -1*/
    DeRefDS(_file_name_51014);
    DeRef(_file_path_51015);
    DeRef(_26847);
    _26847 = NOVALUE;
    return -1;
    ;
}


int _38ScanPath(int _file_name_51028, int _env_51029, int _flag_51030)
{
    int _inc_path_51031 = NOVALUE;
    int _full_path_51032 = NOVALUE;
    int _file_path_51033 = NOVALUE;
    int _strings_51034 = NOVALUE;
    int _end_path_51035 = NOVALUE;
    int _start_path_51036 = NOVALUE;
    int _try_51037 = NOVALUE;
    int _use_cache_51038 = NOVALUE;
    int _pos_51039 = NOVALUE;
    int _26925 = NOVALUE;
    int _26924 = NOVALUE;
    int _26923 = NOVALUE;
    int _26922 = NOVALUE;
    int _26921 = NOVALUE;
    int _26920 = NOVALUE;
    int _26919 = NOVALUE;
    int _26918 = NOVALUE;
    int _26917 = NOVALUE;
    int _26916 = NOVALUE;
    int _26915 = NOVALUE;
    int _26914 = NOVALUE;
    int _26913 = NOVALUE;
    int _26912 = NOVALUE;
    int _26911 = NOVALUE;
    int _26910 = NOVALUE;
    int _26905 = NOVALUE;
    int _26904 = NOVALUE;
    int _26903 = NOVALUE;
    int _26902 = NOVALUE;
    int _26901 = NOVALUE;
    int _26897 = NOVALUE;
    int _26896 = NOVALUE;
    int _26895 = NOVALUE;
    int _26894 = NOVALUE;
    int _26893 = NOVALUE;
    int _26892 = NOVALUE;
    int _26890 = NOVALUE;
    int _26888 = NOVALUE;
    int _26887 = NOVALUE;
    int _26885 = NOVALUE;
    int _26884 = NOVALUE;
    int _26883 = NOVALUE;
    int _26880 = NOVALUE;
    int _26879 = NOVALUE;
    int _26877 = NOVALUE;
    int _26876 = NOVALUE;
    int _26875 = NOVALUE;
    int _26873 = NOVALUE;
    int _26871 = NOVALUE;
    int _26866 = NOVALUE;
    int _26865 = NOVALUE;
    int _26864 = NOVALUE;
    int _26863 = NOVALUE;
    int _26862 = NOVALUE;
    int _26857 = NOVALUE;
    int _26849 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_flag_51030)) {
        _1 = (long)(DBL_PTR(_flag_51030)->dbl);
        if (UNIQUE(DBL_PTR(_flag_51030)) && (DBL_PTR(_flag_51030)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_flag_51030);
        _flag_51030 = _1;
    }

    /** 	inc_path = getenv(env)*/
    DeRefi(_inc_path_51031);
    _inc_path_51031 = EGetEnv(_env_51029);

    /** 	if compare(inc_path,{})!=1 then -- nothing to do, just fail*/
    if (IS_ATOM_INT(_inc_path_51031) && IS_ATOM_INT(_22682)){
        _26849 = (_inc_path_51031 < _22682) ? -1 : (_inc_path_51031 > _22682);
    }
    else{
        _26849 = compare(_inc_path_51031, _22682);
    }
    if (_26849 == 1)
    goto L1; // [18] 29

    /** 		return -1*/
    DeRefDS(_file_name_51028);
    DeRefDS(_env_51029);
    DeRefi(_inc_path_51031);
    DeRef(_full_path_51032);
    DeRef(_file_path_51033);
    DeRef(_strings_51034);
    return -1;
L1: 

    /** 	num_var = find(env,cache_vars)*/
    _38num_var_50599 = find_from(_env_51029, _38cache_vars_50600, 1);

    /** 	use_cache = check_cache(env,inc_path)*/
    RefDS(_env_51029);
    Ref(_inc_path_51031);
    _use_cache_51038 = _38check_cache(_env_51029, _inc_path_51031);
    if (!IS_ATOM_INT(_use_cache_51038)) {
        _1 = (long)(DBL_PTR(_use_cache_51038)->dbl);
        if (UNIQUE(DBL_PTR(_use_cache_51038)) && (DBL_PTR(_use_cache_51038)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_use_cache_51038);
        _use_cache_51038 = _1;
    }

    /** 	inc_path = append(inc_path, PATH_SEPARATOR)*/
    Append(&_inc_path_51031, _inc_path_51031, 59);

    /** 	file_name = SLASH & file_name*/
    Prepend(&_file_name_51028, _file_name_51028, 92);

    /** 	if flag then*/
    if (_flag_51030 == 0)
    {
        goto L2; // [65] 77
    }
    else{
    }

    /** 		file_name = include_subfolder & file_name*/
    Concat((object_ptr)&_file_name_51028, _38include_subfolder_50595, _file_name_51028);
L2: 

    /** 	strings = cache_substrings[num_var]*/
    DeRef(_strings_51034);
    _2 = (int)SEQ_PTR(_38cache_substrings_50602);
    _strings_51034 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
    RefDS(_strings_51034);

    /** 	if use_cache then*/
    if (_use_cache_51038 == 0)
    {
        goto L3; // [91] 292
    }
    else{
    }

    /** 		for i=1 to length(strings) do*/
    if (IS_SEQUENCE(_strings_51034)){
            _26857 = SEQ_PTR(_strings_51034)->length;
    }
    else {
        _26857 = 1;
    }
    {
        int _i_51055;
        _i_51055 = 1;
L4: 
        if (_i_51055 > _26857){
            goto L5; // [99] 252
        }

        /** 			full_path = strings[i]*/
        DeRef(_full_path_51032);
        _2 = (int)SEQ_PTR(_strings_51034);
        _full_path_51032 = (int)*(((s1_ptr)_2)->base + _i_51055);
        Ref(_full_path_51032);

        /** 			file_path = full_path & file_name*/
        Concat((object_ptr)&_file_path_51033, _full_path_51032, _file_name_51028);

        /** 			try = open_locked(file_path)    */
        RefDS(_file_path_51033);
        _try_51037 = _26open_locked(_file_path_51033);
        if (!IS_ATOM_INT(_try_51037)) {
            _1 = (long)(DBL_PTR(_try_51037)->dbl);
            if (UNIQUE(DBL_PTR(_try_51037)) && (DBL_PTR(_try_51037)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_try_51037);
            _try_51037 = _1;
        }

        /** 			if try != -1 then*/
        if (_try_51037 == -1)
        goto L6; // [130] 145

        /** 				return {file_path,try}*/
        RefDS(_file_path_51033);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _file_path_51033;
        ((int *)_2)[2] = _try_51037;
        _26862 = MAKE_SEQ(_1);
        DeRefDS(_file_name_51028);
        DeRefDS(_env_51029);
        DeRefi(_inc_path_51031);
        DeRefDS(_full_path_51032);
        DeRefDS(_file_path_51033);
        DeRefDS(_strings_51034);
        return _26862;
L6: 

        /** 			ifdef WINDOWS then */

        /** 				if sequence(cache_converted[num_var][i]) then*/
        _2 = (int)SEQ_PTR(_38cache_converted_50605);
        _26863 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        _2 = (int)SEQ_PTR(_26863);
        _26864 = (int)*(((s1_ptr)_2)->base + _i_51055);
        _26863 = NOVALUE;
        _26865 = IS_SEQUENCE(_26864);
        _26864 = NOVALUE;
        if (_26865 == 0)
        {
            _26865 = NOVALUE;
            goto L7; // [164] 245
        }
        else{
            _26865 = NOVALUE;
        }

        /** 					full_path = cache_converted[num_var][i]*/
        _2 = (int)SEQ_PTR(_38cache_converted_50605);
        _26866 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        DeRef(_full_path_51032);
        _2 = (int)SEQ_PTR(_26866);
        _full_path_51032 = (int)*(((s1_ptr)_2)->base + _i_51055);
        Ref(_full_path_51032);
        _26866 = NOVALUE;

        /** 					file_path = full_path & file_name*/
        Concat((object_ptr)&_file_path_51033, _full_path_51032, _file_name_51028);

        /** 					try = open_locked(file_path)*/
        RefDS(_file_path_51033);
        _try_51037 = _26open_locked(_file_path_51033);
        if (!IS_ATOM_INT(_try_51037)) {
            _1 = (long)(DBL_PTR(_try_51037)->dbl);
            if (UNIQUE(DBL_PTR(_try_51037)) && (DBL_PTR(_try_51037)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_try_51037);
            _try_51037 = _1;
        }

        /** 					if try != -1 then*/
        if (_try_51037 == -1)
        goto L8; // [199] 244

        /** 						cache_converted[num_var][i] = 0*/
        _2 = (int)SEQ_PTR(_38cache_converted_50605);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_converted_50605 = MAKE_SEQ(_2);
        }
        _3 = (int)(_38num_var_50599 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_51055);
        _1 = *(int *)_2;
        *(int *)_2 = 0;
        DeRef(_1);
        _26871 = NOVALUE;

        /** 						cache_substrings[num_var][i] = full_path*/
        _2 = (int)SEQ_PTR(_38cache_substrings_50602);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_substrings_50602 = MAKE_SEQ(_2);
        }
        _3 = (int)(_38num_var_50599 + ((s1_ptr)_2)->base);
        RefDS(_full_path_51032);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_51055);
        _1 = *(int *)_2;
        *(int *)_2 = _full_path_51032;
        DeRef(_1);
        _26873 = NOVALUE;

        /** 						return {file_path,try}*/
        RefDS(_file_path_51033);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _file_path_51033;
        ((int *)_2)[2] = _try_51037;
        _26875 = MAKE_SEQ(_1);
        DeRefDS(_file_name_51028);
        DeRefDS(_env_51029);
        DeRefi(_inc_path_51031);
        DeRefDS(_full_path_51032);
        DeRefDS(_file_path_51033);
        DeRef(_strings_51034);
        DeRef(_26862);
        _26862 = NOVALUE;
        return _26875;
L8: 
L7: 

        /** 		end for*/
        _i_51055 = _i_51055 + 1;
        goto L4; // [247] 106
L5: 
        ;
    }

    /** 		if cache_complete[num_var] then -- nothing to scan*/
    _2 = (int)SEQ_PTR(_38cache_complete_50606);
    _26876 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
    if (_26876 == 0)
    {
        _26876 = NOVALUE;
        goto L9; // [262] 274
    }
    else{
        _26876 = NOVALUE;
    }

    /** 			return -1*/
    DeRefDS(_file_name_51028);
    DeRefDS(_env_51029);
    DeRefi(_inc_path_51031);
    DeRef(_full_path_51032);
    DeRef(_file_path_51033);
    DeRef(_strings_51034);
    DeRef(_26862);
    _26862 = NOVALUE;
    DeRef(_26875);
    _26875 = NOVALUE;
    return -1;
    goto LA; // [271] 298
L9: 

    /** 			pos = cache_delims[num_var]+1 -- scan remainder, starting from as far sa possible*/
    _2 = (int)SEQ_PTR(_38cache_delims_50607);
    _26877 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
    _pos_51039 = _26877 + 1;
    _26877 = NOVALUE;
    goto LA; // [289] 298
L3: 

    /** 		pos = 1*/
    _pos_51039 = 1;
LA: 

    /** 	start_path = 0*/
    _start_path_51036 = 0;

    /** 	for p = pos to length(inc_path) do*/
    if (IS_SEQUENCE(_inc_path_51031)){
            _26879 = SEQ_PTR(_inc_path_51031)->length;
    }
    else {
        _26879 = 1;
    }
    {
        int _p_51087;
        _p_51087 = _pos_51039;
LB: 
        if (_p_51087 > _26879){
            goto LC; // [310] 716
        }

        /** 		if inc_path[p] = PATH_SEPARATOR then*/
        _2 = (int)SEQ_PTR(_inc_path_51031);
        _26880 = (int)*(((s1_ptr)_2)->base + _p_51087);
        if (_26880 != 59)
        goto LD; // [325] 665

        /** 			cache_delims[num_var] = p*/
        _2 = (int)SEQ_PTR(_38cache_delims_50607);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_delims_50607 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
        *(int *)_2 = _p_51087;

        /** 			end_path = p-1*/
        _end_path_51035 = _p_51087 - 1;

        /** 			while end_path >= start_path and find(inc_path[end_path], " \t" & SLASH_CHARS) do*/
LE: 
        _26883 = (_end_path_51035 >= _start_path_51036);
        if (_26883 == 0) {
            goto LF; // [354] 388
        }
        _2 = (int)SEQ_PTR(_inc_path_51031);
        _26885 = (int)*(((s1_ptr)_2)->base + _end_path_51035);
        Concat((object_ptr)&_26887, _26886, _36SLASH_CHARS_14735);
        _26888 = find_from(_26885, _26887, 1);
        _26885 = NOVALUE;
        DeRefDS(_26887);
        _26887 = NOVALUE;
        if (_26888 == 0)
        {
            _26888 = NOVALUE;
            goto LF; // [374] 388
        }
        else{
            _26888 = NOVALUE;
        }

        /** 				end_path-=1*/
        _end_path_51035 = _end_path_51035 - 1;

        /** 			end while*/
        goto LE; // [385] 350
LF: 

        /** 			if start_path and end_path then*/
        if (_start_path_51036 == 0) {
            goto L10; // [390] 709
        }
        if (_end_path_51035 == 0)
        {
            goto L10; // [395] 709
        }
        else{
        }

        /** 				full_path = inc_path[start_path..end_path]*/
        rhs_slice_target = (object_ptr)&_full_path_51032;
        RHS_Slice(_inc_path_51031, _start_path_51036, _end_path_51035);

        /** 				cache_substrings[num_var] = append(cache_substrings[num_var],full_path)*/
        _2 = (int)SEQ_PTR(_38cache_substrings_50602);
        _26892 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        RefDS(_full_path_51032);
        Append(&_26893, _26892, _full_path_51032);
        _26892 = NOVALUE;
        _2 = (int)SEQ_PTR(_38cache_substrings_50602);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_substrings_50602 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
        _1 = *(int *)_2;
        *(int *)_2 = _26893;
        if( _1 != _26893 ){
            DeRefDS(_1);
        }
        _26893 = NOVALUE;

        /** 				cache_starts[num_var] &= start_path*/
        _2 = (int)SEQ_PTR(_38cache_starts_50603);
        _26894 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        if (IS_SEQUENCE(_26894) && IS_ATOM(_start_path_51036)) {
            Append(&_26895, _26894, _start_path_51036);
        }
        else if (IS_ATOM(_26894) && IS_SEQUENCE(_start_path_51036)) {
        }
        else {
            Concat((object_ptr)&_26895, _26894, _start_path_51036);
            _26894 = NOVALUE;
        }
        _26894 = NOVALUE;
        _2 = (int)SEQ_PTR(_38cache_starts_50603);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_starts_50603 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
        _1 = *(int *)_2;
        *(int *)_2 = _26895;
        if( _1 != _26895 ){
            DeRef(_1);
        }
        _26895 = NOVALUE;

        /** 				cache_ends[num_var] &= end_path*/
        _2 = (int)SEQ_PTR(_38cache_ends_50604);
        _26896 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        if (IS_SEQUENCE(_26896) && IS_ATOM(_end_path_51035)) {
            Append(&_26897, _26896, _end_path_51035);
        }
        else if (IS_ATOM(_26896) && IS_SEQUENCE(_end_path_51035)) {
        }
        else {
            Concat((object_ptr)&_26897, _26896, _end_path_51035);
            _26896 = NOVALUE;
        }
        _26896 = NOVALUE;
        _2 = (int)SEQ_PTR(_38cache_ends_50604);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_ends_50604 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
        _1 = *(int *)_2;
        *(int *)_2 = _26897;
        if( _1 != _26897 ){
            DeRef(_1);
        }
        _26897 = NOVALUE;

        /** 				file_path = full_path & file_name  */
        Concat((object_ptr)&_file_path_51033, _full_path_51032, _file_name_51028);

        /** 				try = open_locked(file_path)*/
        RefDS(_file_path_51033);
        _try_51037 = _26open_locked(_file_path_51033);
        if (!IS_ATOM_INT(_try_51037)) {
            _1 = (long)(DBL_PTR(_try_51037)->dbl);
            if (UNIQUE(DBL_PTR(_try_51037)) && (DBL_PTR(_try_51037)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_try_51037);
            _try_51037 = _1;
        }

        /** 				if try != -1 then -- valid path, no point trying to convert*/
        if (_try_51037 == -1)
        goto L11; // [479] 514

        /** 					ifdef WINDOWS then*/

        /** 						cache_converted[num_var] &= 0*/
        _2 = (int)SEQ_PTR(_38cache_converted_50605);
        _26901 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        if (IS_SEQUENCE(_26901) && IS_ATOM(0)) {
            Append(&_26902, _26901, 0);
        }
        else if (IS_ATOM(_26901) && IS_SEQUENCE(0)) {
        }
        else {
            Concat((object_ptr)&_26902, _26901, 0);
            _26901 = NOVALUE;
        }
        _26901 = NOVALUE;
        _2 = (int)SEQ_PTR(_38cache_converted_50605);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_converted_50605 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
        _1 = *(int *)_2;
        *(int *)_2 = _26902;
        if( _1 != _26902 ){
            DeRef(_1);
        }
        _26902 = NOVALUE;

        /** 					return {file_path,try}*/
        RefDS(_file_path_51033);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _file_path_51033;
        ((int *)_2)[2] = _try_51037;
        _26903 = MAKE_SEQ(_1);
        DeRefDS(_file_name_51028);
        DeRefDS(_env_51029);
        DeRefi(_inc_path_51031);
        DeRefDSi(_full_path_51032);
        DeRefDS(_file_path_51033);
        DeRef(_strings_51034);
        DeRef(_26862);
        _26862 = NOVALUE;
        DeRef(_26875);
        _26875 = NOVALUE;
        _26880 = NOVALUE;
        DeRef(_26883);
        _26883 = NOVALUE;
        return _26903;
L11: 

        /** 				ifdef WINDOWS then*/

        /** 					if find(1, full_path>=128) then*/
        _26904 = binary_op(GREATEREQ, _full_path_51032, 128);
        _26905 = find_from(1, _26904, 1);
        DeRefDS(_26904);
        _26904 = NOVALUE;
        if (_26905 == 0)
        {
            _26905 = NOVALUE;
            goto L12; // [527] 637
        }
        else{
            _26905 = NOVALUE;
        }

        /** 						full_path = convert_from_OEM(full_path)*/
        RefDS(_full_path_51032);
        _0 = _full_path_51032;
        _full_path_51032 = _38convert_from_OEM(_full_path_51032);
        DeRefDS(_0);

        /** 						file_path = full_path & file_name*/
        Concat((object_ptr)&_file_path_51033, _full_path_51032, _file_name_51028);

        /** 						try = open_locked(file_path)*/
        RefDS(_file_path_51033);
        _try_51037 = _26open_locked(_file_path_51033);
        if (!IS_ATOM_INT(_try_51037)) {
            _1 = (long)(DBL_PTR(_try_51037)->dbl);
            if (UNIQUE(DBL_PTR(_try_51037)) && (DBL_PTR(_try_51037)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_try_51037);
            _try_51037 = _1;
        }

        /** 						if try != -1 then -- that was it; record translation as the valid path*/
        if (_try_51037 == -1)
        goto L13; // [554] 611

        /** 							cache_converted[num_var] &= 0*/
        _2 = (int)SEQ_PTR(_38cache_converted_50605);
        _26910 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        if (IS_SEQUENCE(_26910) && IS_ATOM(0)) {
            Append(&_26911, _26910, 0);
        }
        else if (IS_ATOM(_26910) && IS_SEQUENCE(0)) {
        }
        else {
            Concat((object_ptr)&_26911, _26910, 0);
            _26910 = NOVALUE;
        }
        _26910 = NOVALUE;
        _2 = (int)SEQ_PTR(_38cache_converted_50605);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_converted_50605 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
        _1 = *(int *)_2;
        *(int *)_2 = _26911;
        if( _1 != _26911 ){
            DeRef(_1);
        }
        _26911 = NOVALUE;

        /** 							cache_substrings[num_var] = append(cache_substrings[num_var],full_path)*/
        _2 = (int)SEQ_PTR(_38cache_substrings_50602);
        _26912 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        RefDS(_full_path_51032);
        Append(&_26913, _26912, _full_path_51032);
        _26912 = NOVALUE;
        _2 = (int)SEQ_PTR(_38cache_substrings_50602);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_substrings_50602 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
        _1 = *(int *)_2;
        *(int *)_2 = _26913;
        if( _1 != _26913 ){
            DeRefDS(_1);
        }
        _26913 = NOVALUE;

        /** 							return {file_path,try}*/
        RefDS(_file_path_51033);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _file_path_51033;
        ((int *)_2)[2] = _try_51037;
        _26914 = MAKE_SEQ(_1);
        DeRefDS(_file_name_51028);
        DeRefDS(_env_51029);
        DeRefi(_inc_path_51031);
        DeRefDS(_full_path_51032);
        DeRefDS(_file_path_51033);
        DeRef(_strings_51034);
        DeRef(_26862);
        _26862 = NOVALUE;
        DeRef(_26875);
        _26875 = NOVALUE;
        _26880 = NOVALUE;
        DeRef(_26883);
        _26883 = NOVALUE;
        DeRef(_26903);
        _26903 = NOVALUE;
        return _26914;
        goto L14; // [608] 656
L13: 

        /** 							cache_converted[num_var] = append(cache_converted[num_var],full_path)*/
        _2 = (int)SEQ_PTR(_38cache_converted_50605);
        _26915 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        RefDS(_full_path_51032);
        Append(&_26916, _26915, _full_path_51032);
        _26915 = NOVALUE;
        _2 = (int)SEQ_PTR(_38cache_converted_50605);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_converted_50605 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
        _1 = *(int *)_2;
        *(int *)_2 = _26916;
        if( _1 != _26916 ){
            DeRef(_1);
        }
        _26916 = NOVALUE;
        goto L14; // [634] 656
L12: 

        /** 						cache_converted[num_var] &= 0*/
        _2 = (int)SEQ_PTR(_38cache_converted_50605);
        _26917 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        if (IS_SEQUENCE(_26917) && IS_ATOM(0)) {
            Append(&_26918, _26917, 0);
        }
        else if (IS_ATOM(_26917) && IS_SEQUENCE(0)) {
        }
        else {
            Concat((object_ptr)&_26918, _26917, 0);
            _26917 = NOVALUE;
        }
        _26917 = NOVALUE;
        _2 = (int)SEQ_PTR(_38cache_converted_50605);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_converted_50605 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
        _1 = *(int *)_2;
        *(int *)_2 = _26918;
        if( _1 != _26918 ){
            DeRef(_1);
        }
        _26918 = NOVALUE;
L14: 

        /** 				start_path = 0*/
        _start_path_51036 = 0;
        goto L10; // [662] 709
LD: 

        /** 		elsif not start_path and (inc_path[p] != ' ' and inc_path[p] != '\t') then*/
        _26919 = (_start_path_51036 == 0);
        if (_26919 == 0) {
            goto L15; // [670] 708
        }
        _2 = (int)SEQ_PTR(_inc_path_51031);
        _26921 = (int)*(((s1_ptr)_2)->base + _p_51087);
        _26922 = (_26921 != 32);
        _26921 = NOVALUE;
        if (_26922 == 0) {
            DeRef(_26923);
            _26923 = 0;
            goto L16; // [682] 698
        }
        _2 = (int)SEQ_PTR(_inc_path_51031);
        _26924 = (int)*(((s1_ptr)_2)->base + _p_51087);
        _26925 = (_26924 != 9);
        _26924 = NOVALUE;
        _26923 = (_26925 != 0);
L16: 
        if (_26923 == 0)
        {
            _26923 = NOVALUE;
            goto L15; // [699] 708
        }
        else{
            _26923 = NOVALUE;
        }

        /** 			start_path = p*/
        _start_path_51036 = _p_51087;
L15: 
L10: 

        /** 	end for*/
        _p_51087 = _p_51087 + 1;
        goto LB; // [711] 317
LC: 
        ;
    }

    /** 	cache_complete[num_var] = 1*/
    _2 = (int)SEQ_PTR(_38cache_complete_50606);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _38cache_complete_50606 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
    *(int *)_2 = 1;

    /** 	return -1*/
    DeRefDS(_file_name_51028);
    DeRefDS(_env_51029);
    DeRefi(_inc_path_51031);
    DeRef(_full_path_51032);
    DeRef(_file_path_51033);
    DeRef(_strings_51034);
    DeRef(_26862);
    _26862 = NOVALUE;
    DeRef(_26875);
    _26875 = NOVALUE;
    _26880 = NOVALUE;
    DeRef(_26883);
    _26883 = NOVALUE;
    DeRef(_26903);
    _26903 = NOVALUE;
    DeRef(_26914);
    _26914 = NOVALUE;
    DeRef(_26919);
    _26919 = NOVALUE;
    DeRef(_26922);
    _26922 = NOVALUE;
    DeRef(_26925);
    _26925 = NOVALUE;
    return -1;
    ;
}


int _38Include_paths(int _add_converted_51151)
{
    int _status_51152 = NOVALUE;
    int _pos_51153 = NOVALUE;
    int _inc_path_51154 = NOVALUE;
    int _full_path_51155 = NOVALUE;
    int _start_path_51156 = NOVALUE;
    int _end_path_51157 = NOVALUE;
    int _eudir_path_51173 = NOVALUE;
    int _26986 = NOVALUE;
    int _26985 = NOVALUE;
    int _26984 = NOVALUE;
    int _26983 = NOVALUE;
    int _26982 = NOVALUE;
    int _26981 = NOVALUE;
    int _26980 = NOVALUE;
    int _26978 = NOVALUE;
    int _26977 = NOVALUE;
    int _26976 = NOVALUE;
    int _26975 = NOVALUE;
    int _26974 = NOVALUE;
    int _26973 = NOVALUE;
    int _26972 = NOVALUE;
    int _26971 = NOVALUE;
    int _26970 = NOVALUE;
    int _26969 = NOVALUE;
    int _26968 = NOVALUE;
    int _26967 = NOVALUE;
    int _26966 = NOVALUE;
    int _26965 = NOVALUE;
    int _26964 = NOVALUE;
    int _26963 = NOVALUE;
    int _26962 = NOVALUE;
    int _26961 = NOVALUE;
    int _26960 = NOVALUE;
    int _26959 = NOVALUE;
    int _26958 = NOVALUE;
    int _26956 = NOVALUE;
    int _26954 = NOVALUE;
    int _26953 = NOVALUE;
    int _26952 = NOVALUE;
    int _26951 = NOVALUE;
    int _26950 = NOVALUE;
    int _26947 = NOVALUE;
    int _26946 = NOVALUE;
    int _26944 = NOVALUE;
    int _26942 = NOVALUE;
    int _26940 = NOVALUE;
    int _26939 = NOVALUE;
    int _26937 = NOVALUE;
    int _26934 = NOVALUE;
    int _26932 = NOVALUE;
    int _26927 = NOVALUE;
    int _26926 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_add_converted_51151)) {
        _1 = (long)(DBL_PTR(_add_converted_51151)->dbl);
        if (UNIQUE(DBL_PTR(_add_converted_51151)) && (DBL_PTR(_add_converted_51151)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_add_converted_51151);
        _add_converted_51151 = _1;
    }

    /** 	if length(include_Paths) then*/
    if (IS_SEQUENCE(_38include_Paths_51148)){
            _26926 = SEQ_PTR(_38include_Paths_51148)->length;
    }
    else {
        _26926 = 1;
    }
    if (_26926 == 0)
    {
        _26926 = NOVALUE;
        goto L1; // [10] 22
    }
    else{
        _26926 = NOVALUE;
    }

    /** 		return include_Paths*/
    RefDS(_38include_Paths_51148);
    DeRefi(_inc_path_51154);
    DeRefi(_full_path_51155);
    DeRef(_eudir_path_51173);
    return _38include_Paths_51148;
L1: 

    /** 	include_Paths = append(config_inc_paths, current_dir())*/
    _26927 = _9current_dir();
    Ref(_26927);
    Append(&_38include_Paths_51148, _38config_inc_paths_50608, _26927);
    DeRef(_26927);
    _26927 = NOVALUE;

    /** 	num_var = find("EUINC", cache_vars)*/
    _38num_var_50599 = find_from(_26929, _38cache_vars_50600, 1);

    /** 	inc_path = getenv("EUINC")*/
    DeRefi(_inc_path_51154);
    _inc_path_51154 = EGetEnv(_26929);

    /** 	if atom(inc_path) then*/
    _26932 = IS_ATOM(_inc_path_51154);
    if (_26932 == 0)
    {
        _26932 = NOVALUE;
        goto L2; // [52] 61
    }
    else{
        _26932 = NOVALUE;
    }

    /** 		inc_path = ""*/
    RefDS(_22682);
    DeRefi(_inc_path_51154);
    _inc_path_51154 = _22682;
L2: 

    /** 	status = check_cache("EUINC", inc_path)*/
    RefDS(_26929);
    Ref(_inc_path_51154);
    _status_51152 = _38check_cache(_26929, _inc_path_51154);
    if (!IS_ATOM_INT(_status_51152)) {
        _1 = (long)(DBL_PTR(_status_51152)->dbl);
        if (UNIQUE(DBL_PTR(_status_51152)) && (DBL_PTR(_status_51152)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_status_51152);
        _status_51152 = _1;
    }

    /** 	if length(inc_path) then*/
    if (IS_SEQUENCE(_inc_path_51154)){
            _26934 = SEQ_PTR(_inc_path_51154)->length;
    }
    else {
        _26934 = 1;
    }
    if (_26934 == 0)
    {
        _26934 = NOVALUE;
        goto L3; // [75] 87
    }
    else{
        _26934 = NOVALUE;
    }

    /** 		inc_path = append(inc_path, PATH_SEPARATOR)*/
    Append(&_inc_path_51154, _inc_path_51154, 59);
L3: 

    /** 	object eudir_path = get_eudir()*/
    _0 = _eudir_path_51173;
    _eudir_path_51173 = _26get_eudir();
    DeRef(_0);

    /** 	if sequence(eudir_path) then*/
    _26937 = IS_SEQUENCE(_eudir_path_51173);
    if (_26937 == 0)
    {
        _26937 = NOVALUE;
        goto L4; // [97] 117
    }
    else{
        _26937 = NOVALUE;
    }

    /** 		include_Paths = append(include_Paths, sprintf("%s/include", { eudir_path }))*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_eudir_path_51173);
    *((int *)(_2+4)) = _eudir_path_51173;
    _26939 = MAKE_SEQ(_1);
    _26940 = EPrintf(-9999999, _26938, _26939);
    DeRefDS(_26939);
    _26939 = NOVALUE;
    RefDS(_26940);
    Append(&_38include_Paths_51148, _38include_Paths_51148, _26940);
    DeRefDS(_26940);
    _26940 = NOVALUE;
L4: 

    /** 	if status then*/
    if (_status_51152 == 0)
    {
        goto L5; // [119] 161
    }
    else{
    }

    /** 		if cache_complete[num_var] then*/
    _2 = (int)SEQ_PTR(_38cache_complete_50606);
    _26942 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
    if (_26942 == 0)
    {
        _26942 = NOVALUE;
        goto L6; // [132] 144
    }
    else{
        _26942 = NOVALUE;
    }

    /** 			goto "cache done"*/
    goto G7;
L6: 

    /** 		pos = cache_delims[num_var]+1*/
    _2 = (int)SEQ_PTR(_38cache_delims_50607);
    _26944 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
    _pos_51153 = _26944 + 1;
    _26944 = NOVALUE;
    goto L8; // [158] 167
L5: 

    /**         pos = 1*/
    _pos_51153 = 1;
L8: 

    /** 	start_path = 0*/
    _start_path_51156 = 0;

    /** 	for p = pos to length(inc_path) do*/
    if (IS_SEQUENCE(_inc_path_51154)){
            _26946 = SEQ_PTR(_inc_path_51154)->length;
    }
    else {
        _26946 = 1;
    }
    {
        int _p_51190;
        _p_51190 = _pos_51153;
L9: 
        if (_p_51190 > _26946){
            goto LA; // [179] 456
        }

        /** 		if inc_path[p] = PATH_SEPARATOR then*/
        _2 = (int)SEQ_PTR(_inc_path_51154);
        _26947 = (int)*(((s1_ptr)_2)->base + _p_51190);
        if (_26947 != 59)
        goto LB; // [194] 405

        /** 			cache_delims[num_var] = p*/
        _2 = (int)SEQ_PTR(_38cache_delims_50607);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_delims_50607 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
        *(int *)_2 = _p_51190;

        /** 			end_path = p-1*/
        _end_path_51157 = _p_51190 - 1;

        /** 			while end_path >= start_path and find(inc_path[end_path]," \t" & SLASH_CHARS) do*/
LC: 
        _26950 = (_end_path_51157 >= _start_path_51156);
        if (_26950 == 0) {
            goto LD; // [223] 257
        }
        _2 = (int)SEQ_PTR(_inc_path_51154);
        _26952 = (int)*(((s1_ptr)_2)->base + _end_path_51157);
        Concat((object_ptr)&_26953, _26886, _36SLASH_CHARS_14735);
        _26954 = find_from(_26952, _26953, 1);
        _26952 = NOVALUE;
        DeRefDS(_26953);
        _26953 = NOVALUE;
        if (_26954 == 0)
        {
            _26954 = NOVALUE;
            goto LD; // [243] 257
        }
        else{
            _26954 = NOVALUE;
        }

        /** 				end_path -= 1*/
        _end_path_51157 = _end_path_51157 - 1;

        /** 			end while*/
        goto LC; // [254] 219
LD: 

        /** 			if start_path and end_path then*/
        if (_start_path_51156 == 0) {
            goto LE; // [259] 449
        }
        if (_end_path_51157 == 0)
        {
            goto LE; // [264] 449
        }
        else{
        }

        /** 				full_path = inc_path[start_path..end_path]*/
        rhs_slice_target = (object_ptr)&_full_path_51155;
        RHS_Slice(_inc_path_51154, _start_path_51156, _end_path_51157);

        /** 				cache_substrings[num_var] = append(cache_substrings[num_var],full_path)*/
        _2 = (int)SEQ_PTR(_38cache_substrings_50602);
        _26958 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        RefDS(_full_path_51155);
        Append(&_26959, _26958, _full_path_51155);
        _26958 = NOVALUE;
        _2 = (int)SEQ_PTR(_38cache_substrings_50602);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_substrings_50602 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
        _1 = *(int *)_2;
        *(int *)_2 = _26959;
        if( _1 != _26959 ){
            DeRefDS(_1);
        }
        _26959 = NOVALUE;

        /** 				cache_starts[num_var] &= start_path*/
        _2 = (int)SEQ_PTR(_38cache_starts_50603);
        _26960 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        if (IS_SEQUENCE(_26960) && IS_ATOM(_start_path_51156)) {
            Append(&_26961, _26960, _start_path_51156);
        }
        else if (IS_ATOM(_26960) && IS_SEQUENCE(_start_path_51156)) {
        }
        else {
            Concat((object_ptr)&_26961, _26960, _start_path_51156);
            _26960 = NOVALUE;
        }
        _26960 = NOVALUE;
        _2 = (int)SEQ_PTR(_38cache_starts_50603);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_starts_50603 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
        _1 = *(int *)_2;
        *(int *)_2 = _26961;
        if( _1 != _26961 ){
            DeRef(_1);
        }
        _26961 = NOVALUE;

        /** 				cache_ends[num_var] &= end_path*/
        _2 = (int)SEQ_PTR(_38cache_ends_50604);
        _26962 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        if (IS_SEQUENCE(_26962) && IS_ATOM(_end_path_51157)) {
            Append(&_26963, _26962, _end_path_51157);
        }
        else if (IS_ATOM(_26962) && IS_SEQUENCE(_end_path_51157)) {
        }
        else {
            Concat((object_ptr)&_26963, _26962, _end_path_51157);
            _26962 = NOVALUE;
        }
        _26962 = NOVALUE;
        _2 = (int)SEQ_PTR(_38cache_ends_50604);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_ends_50604 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
        _1 = *(int *)_2;
        *(int *)_2 = _26963;
        if( _1 != _26963 ){
            DeRef(_1);
        }
        _26963 = NOVALUE;

        /** 				ifdef WINDOWS then*/

        /** 					if find(1, full_path>=128) then*/
        _26964 = binary_op(GREATEREQ, _full_path_51155, 128);
        _26965 = find_from(1, _26964, 1);
        DeRefDS(_26964);
        _26964 = NOVALUE;
        if (_26965 == 0)
        {
            _26965 = NOVALUE;
            goto LF; // [345] 377
        }
        else{
            _26965 = NOVALUE;
        }

        /** 						cache_converted[num_var] = append(cache_converted[num_var], */
        _2 = (int)SEQ_PTR(_38cache_converted_50605);
        _26966 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        RefDS(_full_path_51155);
        _26967 = _38convert_from_OEM(_full_path_51155);
        Ref(_26967);
        Append(&_26968, _26966, _26967);
        _26966 = NOVALUE;
        DeRef(_26967);
        _26967 = NOVALUE;
        _2 = (int)SEQ_PTR(_38cache_converted_50605);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_converted_50605 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
        _1 = *(int *)_2;
        *(int *)_2 = _26968;
        if( _1 != _26968 ){
            DeRef(_1);
        }
        _26968 = NOVALUE;
        goto L10; // [374] 396
LF: 

        /** 						cache_converted[num_var] &= 0*/
        _2 = (int)SEQ_PTR(_38cache_converted_50605);
        _26969 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        if (IS_SEQUENCE(_26969) && IS_ATOM(0)) {
            Append(&_26970, _26969, 0);
        }
        else if (IS_ATOM(_26969) && IS_SEQUENCE(0)) {
        }
        else {
            Concat((object_ptr)&_26970, _26969, 0);
            _26969 = NOVALUE;
        }
        _26969 = NOVALUE;
        _2 = (int)SEQ_PTR(_38cache_converted_50605);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38cache_converted_50605 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
        _1 = *(int *)_2;
        *(int *)_2 = _26970;
        if( _1 != _26970 ){
            DeRef(_1);
        }
        _26970 = NOVALUE;
L10: 

        /** 				start_path = 0*/
        _start_path_51156 = 0;
        goto LE; // [402] 449
LB: 

        /** 		elsif not start_path and (inc_path[p] != ' ' and inc_path[p] != '\t') then*/
        _26971 = (_start_path_51156 == 0);
        if (_26971 == 0) {
            goto L11; // [410] 448
        }
        _2 = (int)SEQ_PTR(_inc_path_51154);
        _26973 = (int)*(((s1_ptr)_2)->base + _p_51190);
        _26974 = (_26973 != 32);
        _26973 = NOVALUE;
        if (_26974 == 0) {
            DeRef(_26975);
            _26975 = 0;
            goto L12; // [422] 438
        }
        _2 = (int)SEQ_PTR(_inc_path_51154);
        _26976 = (int)*(((s1_ptr)_2)->base + _p_51190);
        _26977 = (_26976 != 9);
        _26976 = NOVALUE;
        _26975 = (_26977 != 0);
L12: 
        if (_26975 == 0)
        {
            _26975 = NOVALUE;
            goto L11; // [439] 448
        }
        else{
            _26975 = NOVALUE;
        }

        /** 			start_path = p*/
        _start_path_51156 = _p_51190;
L11: 
LE: 

        /** 	end for*/
        _p_51190 = _p_51190 + 1;
        goto L9; // [451] 186
LA: 
        ;
    }

    /** label "cache done"*/
G7:

    /** 	include_Paths &= cache_substrings[num_var]*/
    _2 = (int)SEQ_PTR(_38cache_substrings_50602);
    _26978 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
    Concat((object_ptr)&_38include_Paths_51148, _38include_Paths_51148, _26978);
    _26978 = NOVALUE;

    /** 	cache_complete[num_var] = 1*/
    _2 = (int)SEQ_PTR(_38cache_complete_50606);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _38cache_complete_50606 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _38num_var_50599);
    *(int *)_2 = 1;

    /** 	ifdef WINDOWS then*/

    /** 		if add_converted then*/
    if (_add_converted_51151 == 0)
    {
        goto L13; // [490] 562
    }
    else{
    }

    /** 	    	for i=1 to length(cache_converted[num_var]) do*/
    _2 = (int)SEQ_PTR(_38cache_converted_50605);
    _26980 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
    if (IS_SEQUENCE(_26980)){
            _26981 = SEQ_PTR(_26980)->length;
    }
    else {
        _26981 = 1;
    }
    _26980 = NOVALUE;
    {
        int _i_51235;
        _i_51235 = 1;
L14: 
        if (_i_51235 > _26981){
            goto L15; // [506] 561
        }

        /** 	        	if sequence(cache_converted[num_var][i]) then*/
        _2 = (int)SEQ_PTR(_38cache_converted_50605);
        _26982 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        _2 = (int)SEQ_PTR(_26982);
        _26983 = (int)*(((s1_ptr)_2)->base + _i_51235);
        _26982 = NOVALUE;
        _26984 = IS_SEQUENCE(_26983);
        _26983 = NOVALUE;
        if (_26984 == 0)
        {
            _26984 = NOVALUE;
            goto L16; // [530] 554
        }
        else{
            _26984 = NOVALUE;
        }

        /** 		        	include_Paths = append(include_Paths, cache_converted[num_var][i])*/
        _2 = (int)SEQ_PTR(_38cache_converted_50605);
        _26985 = (int)*(((s1_ptr)_2)->base + _38num_var_50599);
        _2 = (int)SEQ_PTR(_26985);
        _26986 = (int)*(((s1_ptr)_2)->base + _i_51235);
        _26985 = NOVALUE;
        Ref(_26986);
        Append(&_38include_Paths_51148, _38include_Paths_51148, _26986);
        _26986 = NOVALUE;
L16: 

        /** 			end for*/
        _i_51235 = _i_51235 + 1;
        goto L14; // [556] 513
L15: 
        ;
    }
L13: 

    /** 	return include_Paths*/
    RefDS(_38include_Paths_51148);
    DeRefi(_inc_path_51154);
    DeRefi(_full_path_51155);
    DeRef(_eudir_path_51173);
    _26947 = NOVALUE;
    DeRef(_26950);
    _26950 = NOVALUE;
    DeRef(_26971);
    _26971 = NOVALUE;
    _26980 = NOVALUE;
    DeRef(_26974);
    _26974 = NOVALUE;
    DeRef(_26977);
    _26977 = NOVALUE;
    return _38include_Paths_51148;
    ;
}


int _38e_path_find(int _name_51247)
{
    int _scan_result_51248 = NOVALUE;
    int _26996 = NOVALUE;
    int _26995 = NOVALUE;
    int _26994 = NOVALUE;
    int _26991 = NOVALUE;
    int _26990 = NOVALUE;
    int _26989 = NOVALUE;
    int _26988 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if file_exists(name) then*/
    RefDS(_name_51247);
    _26988 = _9file_exists(_name_51247);
    if (_26988 == 0) {
        DeRef(_26988);
        _26988 = NOVALUE;
        goto L1; // [9] 19
    }
    else {
        if (!IS_ATOM_INT(_26988) && DBL_PTR(_26988)->dbl == 0.0){
            DeRef(_26988);
            _26988 = NOVALUE;
            goto L1; // [9] 19
        }
        DeRef(_26988);
        _26988 = NOVALUE;
    }
    DeRef(_26988);
    _26988 = NOVALUE;

    /** 		return name*/
    DeRef(_scan_result_51248);
    return _name_51247;
L1: 

    /** 	for i = 1 to length(SLASH_CHARS) do*/
    if (IS_SEQUENCE(_36SLASH_CHARS_14735)){
            _26989 = SEQ_PTR(_36SLASH_CHARS_14735)->length;
    }
    else {
        _26989 = 1;
    }
    {
        int _i_51253;
        _i_51253 = 1;
L2: 
        if (_i_51253 > _26989){
            goto L3; // [26] 63
        }

        /** 		if find(SLASH_CHARS[i], name) then*/
        _2 = (int)SEQ_PTR(_36SLASH_CHARS_14735);
        _26990 = (int)*(((s1_ptr)_2)->base + _i_51253);
        _26991 = find_from(_26990, _name_51247, 1);
        _26990 = NOVALUE;
        if (_26991 == 0)
        {
            _26991 = NOVALUE;
            goto L4; // [46] 56
        }
        else{
            _26991 = NOVALUE;
        }

        /** 			return -1*/
        DeRefDS(_name_51247);
        DeRef(_scan_result_51248);
        return -1;
L4: 

        /** 	end for*/
        _i_51253 = _i_51253 + 1;
        goto L2; // [58] 33
L3: 
        ;
    }

    /** 	scan_result = ScanPath(name, "PATH", 0)*/
    RefDS(_name_51247);
    RefDS(_26992);
    _0 = _scan_result_51248;
    _scan_result_51248 = _38ScanPath(_name_51247, _26992, 0);
    DeRef(_0);

    /** 	if sequence(scan_result) then*/
    _26994 = IS_SEQUENCE(_scan_result_51248);
    if (_26994 == 0)
    {
        _26994 = NOVALUE;
        goto L5; // [76] 98
    }
    else{
        _26994 = NOVALUE;
    }

    /** 		close(scan_result[2])*/
    _2 = (int)SEQ_PTR(_scan_result_51248);
    _26995 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_26995))
    EClose(_26995);
    else
    EClose((int)DBL_PTR(_26995)->dbl);
    _26995 = NOVALUE;

    /** 		return scan_result[1]*/
    _2 = (int)SEQ_PTR(_scan_result_51248);
    _26996 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_26996);
    DeRefDS(_name_51247);
    DeRef(_scan_result_51248);
    return _26996;
L5: 

    /** 	return -1*/
    DeRefDS(_name_51247);
    DeRef(_scan_result_51248);
    _26996 = NOVALUE;
    return -1;
    ;
}



// 0xB13B8286
