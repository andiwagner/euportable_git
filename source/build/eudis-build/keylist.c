// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _63find_category(int _tokid_24423)
{
    int _catname_24424 = NOVALUE;
    int _14204 = NOVALUE;
    int _14203 = NOVALUE;
    int _14201 = NOVALUE;
    int _14200 = NOVALUE;
    int _14199 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_tokid_24423)) {
        _1 = (long)(DBL_PTR(_tokid_24423)->dbl);
        if (UNIQUE(DBL_PTR(_tokid_24423)) && (DBL_PTR(_tokid_24423)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_tokid_24423);
        _tokid_24423 = _1;
    }

    /** 	sequence catname = "reserved word"*/
    RefDS(_14198);
    DeRef(_catname_24424);
    _catname_24424 = _14198;

    /** 	for i = 1 to length(token_category) do*/
    _14199 = 73;
    {
        int _i_24427;
        _i_24427 = 1;
L1: 
        if (_i_24427 > 73){
            goto L2; // [17] 72
        }

        /** 		if token_category[i][1] = tokid then*/
        _2 = (int)SEQ_PTR(_28token_category_11782);
        _14200 = (int)*(((s1_ptr)_2)->base + _i_24427);
        _2 = (int)SEQ_PTR(_14200);
        _14201 = (int)*(((s1_ptr)_2)->base + 1);
        _14200 = NOVALUE;
        if (binary_op_a(NOTEQ, _14201, _tokid_24423)){
            _14201 = NOVALUE;
            goto L3; // [36] 65
        }
        _14201 = NOVALUE;

        /** 			catname = token_catname[token_category[i][2]]*/
        _2 = (int)SEQ_PTR(_28token_category_11782);
        _14203 = (int)*(((s1_ptr)_2)->base + _i_24427);
        _2 = (int)SEQ_PTR(_14203);
        _14204 = (int)*(((s1_ptr)_2)->base + 2);
        _14203 = NOVALUE;
        DeRef(_catname_24424);
        _2 = (int)SEQ_PTR(_28token_catname_11769);
        if (!IS_ATOM_INT(_14204)){
            _catname_24424 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14204)->dbl));
        }
        else{
            _catname_24424 = (int)*(((s1_ptr)_2)->base + _14204);
        }
        RefDS(_catname_24424);

        /** 			exit*/
        goto L2; // [62] 72
L3: 

        /** 	end for*/
        _i_24427 = _i_24427 + 1;
        goto L1; // [67] 24
L2: 
        ;
    }

    /** 	return catname*/
    _14204 = NOVALUE;
    return _catname_24424;
    ;
}


int _63find_token_text(int _tokid_24442)
{
    int _14213 = NOVALUE;
    int _14211 = NOVALUE;
    int _14210 = NOVALUE;
    int _14208 = NOVALUE;
    int _14207 = NOVALUE;
    int _14206 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_tokid_24442)) {
        _1 = (long)(DBL_PTR(_tokid_24442)->dbl);
        if (UNIQUE(DBL_PTR(_tokid_24442)) && (DBL_PTR(_tokid_24442)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_tokid_24442);
        _tokid_24442 = _1;
    }

    /** 	for i = 1 to length(keylist) do*/
    if (IS_SEQUENCE(_63keylist_23599)){
            _14206 = SEQ_PTR(_63keylist_23599)->length;
    }
    else {
        _14206 = 1;
    }
    {
        int _i_24444;
        _i_24444 = 1;
L1: 
        if (_i_24444 > _14206){
            goto L2; // [10] 57
        }

        /** 		if keylist[i][3] = tokid then*/
        _2 = (int)SEQ_PTR(_63keylist_23599);
        _14207 = (int)*(((s1_ptr)_2)->base + _i_24444);
        _2 = (int)SEQ_PTR(_14207);
        _14208 = (int)*(((s1_ptr)_2)->base + 3);
        _14207 = NOVALUE;
        if (binary_op_a(NOTEQ, _14208, _tokid_24442)){
            _14208 = NOVALUE;
            goto L3; // [29] 50
        }
        _14208 = NOVALUE;

        /** 			return keylist[i][1]*/
        _2 = (int)SEQ_PTR(_63keylist_23599);
        _14210 = (int)*(((s1_ptr)_2)->base + _i_24444);
        _2 = (int)SEQ_PTR(_14210);
        _14211 = (int)*(((s1_ptr)_2)->base + 1);
        _14210 = NOVALUE;
        Ref(_14211);
        return _14211;
L3: 

        /** 	end for*/
        _i_24444 = _i_24444 + 1;
        goto L1; // [52] 17
L2: 
        ;
    }

    /** 	return LexName(tokid, "unknown word")*/
    RefDS(_14212);
    _14213 = _37LexName(_tokid_24442, _14212);
    _14211 = NOVALUE;
    return _14213;
    ;
}



// 0x2F083DDA
