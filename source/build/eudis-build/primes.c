// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _34calc_primes(int _approx_limit_12619, int _time_limit_p_12620)
{
    int _result__12621 = NOVALUE;
    int _candidate__12622 = NOVALUE;
    int _pos__12623 = NOVALUE;
    int _time_out__12624 = NOVALUE;
    int _maxp__12625 = NOVALUE;
    int _maxf__12626 = NOVALUE;
    int _maxf_idx_12627 = NOVALUE;
    int _next_trigger_12628 = NOVALUE;
    int _growth_12629 = NOVALUE;
    int _7002 = NOVALUE;
    int _6999 = NOVALUE;
    int _6997 = NOVALUE;
    int _6994 = NOVALUE;
    int _6991 = NOVALUE;
    int _6988 = NOVALUE;
    int _6981 = NOVALUE;
    int _6979 = NOVALUE;
    int _6976 = NOVALUE;
    int _6973 = NOVALUE;
    int _6969 = NOVALUE;
    int _6968 = NOVALUE;
    int _6964 = NOVALUE;
    int _6958 = NOVALUE;
    int _6956 = NOVALUE;
    int _6954 = NOVALUE;
    int _6949 = NOVALUE;
    int _6948 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if approx_limit <= list_of_primes[$] then*/
    if (IS_SEQUENCE(_34list_of_primes_12615)){
            _6948 = SEQ_PTR(_34list_of_primes_12615)->length;
    }
    else {
        _6948 = 1;
    }
    _2 = (int)SEQ_PTR(_34list_of_primes_12615);
    _6949 = (int)*(((s1_ptr)_2)->base + _6948);
    if (binary_op_a(GREATER, _approx_limit_12619, _6949)){
        _6949 = NOVALUE;
        goto L1; // [14] 59
    }
    _6949 = NOVALUE;

    /** 		pos_ = search:binary_search(approx_limit, list_of_primes)*/
    RefDS(_34list_of_primes_12615);
    _pos__12623 = _7binary_search(_approx_limit_12619, _34list_of_primes_12615, 1, 0);
    if (!IS_ATOM_INT(_pos__12623)) {
        _1 = (long)(DBL_PTR(_pos__12623)->dbl);
        if (UNIQUE(DBL_PTR(_pos__12623)) && (DBL_PTR(_pos__12623)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos__12623);
        _pos__12623 = _1;
    }

    /** 		if pos_ < 0 then*/
    if (_pos__12623 >= 0)
    goto L2; // [33] 45

    /** 			pos_ = (-pos_)*/
    _pos__12623 = - _pos__12623;
L2: 

    /** 		return list_of_primes[1..pos_]*/
    rhs_slice_target = (object_ptr)&_6954;
    RHS_Slice(_34list_of_primes_12615, 1, _pos__12623);
    DeRef(_result__12621);
    DeRef(_time_out__12624);
    return _6954;
L1: 

    /** 	pos_ = length(list_of_primes)*/
    if (IS_SEQUENCE(_34list_of_primes_12615)){
            _pos__12623 = SEQ_PTR(_34list_of_primes_12615)->length;
    }
    else {
        _pos__12623 = 1;
    }

    /** 	candidate_ = list_of_primes[$]*/
    if (IS_SEQUENCE(_34list_of_primes_12615)){
            _6956 = SEQ_PTR(_34list_of_primes_12615)->length;
    }
    else {
        _6956 = 1;
    }
    _2 = (int)SEQ_PTR(_34list_of_primes_12615);
    _candidate__12622 = (int)*(((s1_ptr)_2)->base + _6956);
    if (!IS_ATOM_INT(_candidate__12622))
    _candidate__12622 = (long)DBL_PTR(_candidate__12622)->dbl;

    /** 	maxf_ = floor(power(candidate_, 0.5))*/
    temp_d.dbl = (double)_candidate__12622;
    _6958 = Dpower(&temp_d, DBL_PTR(_2254));
    _maxf__12626 = unary_op(FLOOR, _6958);
    DeRefDS(_6958);
    _6958 = NOVALUE;
    if (!IS_ATOM_INT(_maxf__12626)) {
        _1 = (long)(DBL_PTR(_maxf__12626)->dbl);
        if (UNIQUE(DBL_PTR(_maxf__12626)) && (DBL_PTR(_maxf__12626)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_maxf__12626);
        _maxf__12626 = _1;
    }

    /** 	maxf_idx = search:binary_search(maxf_, list_of_primes)*/
    RefDS(_34list_of_primes_12615);
    _maxf_idx_12627 = _7binary_search(_maxf__12626, _34list_of_primes_12615, 1, 0);
    if (!IS_ATOM_INT(_maxf_idx_12627)) {
        _1 = (long)(DBL_PTR(_maxf_idx_12627)->dbl);
        if (UNIQUE(DBL_PTR(_maxf_idx_12627)) && (DBL_PTR(_maxf_idx_12627)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_maxf_idx_12627);
        _maxf_idx_12627 = _1;
    }

    /** 	if maxf_idx < 0 then*/
    if (_maxf_idx_12627 >= 0)
    goto L3; // [103] 123

    /** 		maxf_idx = (-maxf_idx)*/
    _maxf_idx_12627 = - _maxf_idx_12627;

    /** 		maxf_ = list_of_primes[maxf_idx]*/
    _2 = (int)SEQ_PTR(_34list_of_primes_12615);
    _maxf__12626 = (int)*(((s1_ptr)_2)->base + _maxf_idx_12627);
    if (!IS_ATOM_INT(_maxf__12626))
    _maxf__12626 = (long)DBL_PTR(_maxf__12626)->dbl;
L3: 

    /** 	next_trigger = list_of_primes[maxf_idx+1]*/
    _6964 = _maxf_idx_12627 + 1;
    _2 = (int)SEQ_PTR(_34list_of_primes_12615);
    _next_trigger_12628 = (int)*(((s1_ptr)_2)->base + _6964);
    if (!IS_ATOM_INT(_next_trigger_12628))
    _next_trigger_12628 = (long)DBL_PTR(_next_trigger_12628)->dbl;

    /** 	next_trigger *= next_trigger*/
    _next_trigger_12628 = _next_trigger_12628 * _next_trigger_12628;

    /** 	growth = floor(approx_limit  / 3.5) - length(list_of_primes)*/
    _2 = binary_op(DIVIDE, _approx_limit_12619, _6967);
    _6968 = unary_op(FLOOR, _2);
    DeRef(_2);
    if (IS_SEQUENCE(_34list_of_primes_12615)){
            _6969 = SEQ_PTR(_34list_of_primes_12615)->length;
    }
    else {
        _6969 = 1;
    }
    if (IS_ATOM_INT(_6968)) {
        _growth_12629 = _6968 - _6969;
    }
    else {
        _growth_12629 = NewDouble(DBL_PTR(_6968)->dbl - (double)_6969);
    }
    DeRef(_6968);
    _6968 = NOVALUE;
    _6969 = NOVALUE;
    if (!IS_ATOM_INT(_growth_12629)) {
        _1 = (long)(DBL_PTR(_growth_12629)->dbl);
        if (UNIQUE(DBL_PTR(_growth_12629)) && (DBL_PTR(_growth_12629)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_growth_12629);
        _growth_12629 = _1;
    }

    /** 	if growth <= 0 then*/
    if (_growth_12629 > 0)
    goto L4; // [162] 174

    /** 		growth = length(list_of_primes)*/
    if (IS_SEQUENCE(_34list_of_primes_12615)){
            _growth_12629 = SEQ_PTR(_34list_of_primes_12615)->length;
    }
    else {
        _growth_12629 = 1;
    }
L4: 

    /** 	result_ = list_of_primes & repeat(0, growth)*/
    _6973 = Repeat(0, _growth_12629);
    Concat((object_ptr)&_result__12621, _34list_of_primes_12615, _6973);
    DeRefDS(_6973);
    _6973 = NOVALUE;

    /** 	if time_limit_p < 0 then*/
    if (_time_limit_p_12620 >= 0)
    goto L5; // [188] 203

    /** 		time_out_ = time() + 100_000_000*/
    DeRef(_6976);
    _6976 = NewDouble(current_time());
    DeRef(_time_out__12624);
    _time_out__12624 = NewDouble(DBL_PTR(_6976)->dbl + (double)100000000);
    DeRefDS(_6976);
    _6976 = NOVALUE;
    goto L6; // [200] 212
L5: 

    /** 		time_out_ = time() + time_limit_p*/
    DeRef(_6979);
    _6979 = NewDouble(current_time());
    DeRef(_time_out__12624);
    _time_out__12624 = NewDouble(DBL_PTR(_6979)->dbl + (double)_time_limit_p_12620);
    DeRefDS(_6979);
    _6979 = NOVALUE;
L6: 

    /** 	while time_out_ >= time()  label "MW" do*/
L7: 
    DeRef(_6981);
    _6981 = NewDouble(current_time());
    if (binary_op_a(LESS, _time_out__12624, _6981)){
        DeRefDS(_6981);
        _6981 = NOVALUE;
        goto L8; // [221] 370
    }
    DeRef(_6981);
    _6981 = NOVALUE;

    /** 		task_yield()*/
    task_yield();

    /** 		candidate_ += 2*/
    _candidate__12622 = _candidate__12622 + 2;

    /** 		if candidate_ >= next_trigger then*/
    if (_candidate__12622 < _next_trigger_12628)
    goto L9; // [236] 271

    /** 			maxf_idx += 1*/
    _maxf_idx_12627 = _maxf_idx_12627 + 1;

    /** 			maxf_ = result_[maxf_idx]*/
    _2 = (int)SEQ_PTR(_result__12621);
    _maxf__12626 = (int)*(((s1_ptr)_2)->base + _maxf_idx_12627);
    if (!IS_ATOM_INT(_maxf__12626))
    _maxf__12626 = (long)DBL_PTR(_maxf__12626)->dbl;

    /** 			next_trigger = result_[maxf_idx+1]*/
    _6988 = _maxf_idx_12627 + 1;
    _2 = (int)SEQ_PTR(_result__12621);
    _next_trigger_12628 = (int)*(((s1_ptr)_2)->base + _6988);
    if (!IS_ATOM_INT(_next_trigger_12628))
    _next_trigger_12628 = (long)DBL_PTR(_next_trigger_12628)->dbl;

    /** 			next_trigger *= next_trigger*/
    _next_trigger_12628 = _next_trigger_12628 * _next_trigger_12628;
L9: 

    /** 		for i = 2 to pos_ do*/
    _6991 = _pos__12623;
    {
        int _i_12682;
        _i_12682 = 2;
LA: 
        if (_i_12682 > _6991){
            goto LB; // [276] 322
        }

        /** 			maxp_ = result_[i]*/
        _2 = (int)SEQ_PTR(_result__12621);
        _maxp__12625 = (int)*(((s1_ptr)_2)->base + _i_12682);
        if (!IS_ATOM_INT(_maxp__12625))
        _maxp__12625 = (long)DBL_PTR(_maxp__12625)->dbl;

        /** 			if maxp_ > maxf_ then*/
        if (_maxp__12625 <= _maxf__12626)
        goto LC; // [291] 300

        /** 				exit*/
        goto LB; // [297] 322
LC: 

        /** 			if remainder(candidate_, maxp_) = 0 then*/
        _6994 = (_candidate__12622 % _maxp__12625);
        if (_6994 != 0)
        goto LD; // [306] 315

        /** 				continue "MW"*/
        goto L7; // [312] 217
LD: 

        /** 		end for*/
        _i_12682 = _i_12682 + 1;
        goto LA; // [317] 283
LB: 
        ;
    }

    /** 		pos_ += 1*/
    _pos__12623 = _pos__12623 + 1;

    /** 		if pos_ >= length(result_) then*/
    if (IS_SEQUENCE(_result__12621)){
            _6997 = SEQ_PTR(_result__12621)->length;
    }
    else {
        _6997 = 1;
    }
    if (_pos__12623 < _6997)
    goto LE; // [333] 348

    /** 			result_ &= repeat(0, 1000)*/
    _6999 = Repeat(0, 1000);
    Concat((object_ptr)&_result__12621, _result__12621, _6999);
    DeRefDS(_6999);
    _6999 = NOVALUE;
LE: 

    /** 		result_[pos_] = candidate_*/
    _2 = (int)SEQ_PTR(_result__12621);
    _2 = (int)(((s1_ptr)_2)->base + _pos__12623);
    _1 = *(int *)_2;
    *(int *)_2 = _candidate__12622;
    DeRef(_1);

    /** 		if candidate_ >= approx_limit then*/
    if (_candidate__12622 < _approx_limit_12619)
    goto L7; // [356] 217

    /** 			exit*/
    goto L8; // [362] 370

    /** 	end while*/
    goto L7; // [367] 217
L8: 

    /** 	return result_[1..pos_]*/
    rhs_slice_target = (object_ptr)&_7002;
    RHS_Slice(_result__12621, 1, _pos__12623);
    DeRefDS(_result__12621);
    DeRef(_time_out__12624);
    DeRef(_6954);
    _6954 = NOVALUE;
    DeRef(_6964);
    _6964 = NOVALUE;
    DeRef(_6988);
    _6988 = NOVALUE;
    DeRef(_6994);
    _6994 = NOVALUE;
    return _7002;
    ;
}


int _34next_prime(int _n_12701, int _fail_signal_p_12702, int _time_out_p_12703)
{
    int _i_12704 = NOVALUE;
    int _7021 = NOVALUE;
    int _7016 = NOVALUE;
    int _7015 = NOVALUE;
    int _7014 = NOVALUE;
    int _7013 = NOVALUE;
    int _7012 = NOVALUE;
    int _7009 = NOVALUE;
    int _7008 = NOVALUE;
    int _7005 = NOVALUE;
    int _7004 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if n < 0 then*/
    if (_n_12701 >= 0)
    goto L1; // [5] 16

    /** 		return fail_signal_p*/
    return _fail_signal_p_12702;
L1: 

    /** 	if list_of_primes[$] < n then*/
    if (IS_SEQUENCE(_34list_of_primes_12615)){
            _7004 = SEQ_PTR(_34list_of_primes_12615)->length;
    }
    else {
        _7004 = 1;
    }
    _2 = (int)SEQ_PTR(_34list_of_primes_12615);
    _7005 = (int)*(((s1_ptr)_2)->base + _7004);
    if (binary_op_a(GREATEREQ, _7005, _n_12701)){
        _7005 = NOVALUE;
        goto L2; // [27] 41
    }
    _7005 = NOVALUE;

    /** 		list_of_primes = calc_primes(n,time_out_p)*/
    _0 = _34calc_primes(_n_12701, _time_out_p_12703);
    DeRefDS(_34list_of_primes_12615);
    _34list_of_primes_12615 = _0;
L2: 

    /** 	if n > list_of_primes[$] then*/
    if (IS_SEQUENCE(_34list_of_primes_12615)){
            _7008 = SEQ_PTR(_34list_of_primes_12615)->length;
    }
    else {
        _7008 = 1;
    }
    _2 = (int)SEQ_PTR(_34list_of_primes_12615);
    _7009 = (int)*(((s1_ptr)_2)->base + _7008);
    if (binary_op_a(LESSEQ, _n_12701, _7009)){
        _7009 = NOVALUE;
        goto L3; // [52] 63
    }
    _7009 = NOVALUE;

    /** 		return fail_signal_p*/
    return _fail_signal_p_12702;
L3: 

    /** 	if n < 1009 and 1009 <= list_of_primes[$] then*/
    _7012 = (_n_12701 < 1009);
    if (_7012 == 0) {
        goto L4; // [69] 106
    }
    if (IS_SEQUENCE(_34list_of_primes_12615)){
            _7014 = SEQ_PTR(_34list_of_primes_12615)->length;
    }
    else {
        _7014 = 1;
    }
    _2 = (int)SEQ_PTR(_34list_of_primes_12615);
    _7015 = (int)*(((s1_ptr)_2)->base + _7014);
    if (IS_ATOM_INT(_7015)) {
        _7016 = (1009 <= _7015);
    }
    else {
        _7016 = binary_op(LESSEQ, 1009, _7015);
    }
    _7015 = NOVALUE;
    if (_7016 == 0) {
        DeRef(_7016);
        _7016 = NOVALUE;
        goto L4; // [87] 106
    }
    else {
        if (!IS_ATOM_INT(_7016) && DBL_PTR(_7016)->dbl == 0.0){
            DeRef(_7016);
            _7016 = NOVALUE;
            goto L4; // [87] 106
        }
        DeRef(_7016);
        _7016 = NOVALUE;
    }
    DeRef(_7016);
    _7016 = NOVALUE;

    /** 		i = search:binary_search(n, list_of_primes, ,169)*/
    RefDS(_34list_of_primes_12615);
    _i_12704 = _7binary_search(_n_12701, _34list_of_primes_12615, 1, 169);
    if (!IS_ATOM_INT(_i_12704)) {
        _1 = (long)(DBL_PTR(_i_12704)->dbl);
        if (UNIQUE(DBL_PTR(_i_12704)) && (DBL_PTR(_i_12704)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_12704);
        _i_12704 = _1;
    }
    goto L5; // [103] 120
L4: 

    /** 		i = search:binary_search(n, list_of_primes)*/
    RefDS(_34list_of_primes_12615);
    _i_12704 = _7binary_search(_n_12701, _34list_of_primes_12615, 1, 0);
    if (!IS_ATOM_INT(_i_12704)) {
        _1 = (long)(DBL_PTR(_i_12704)->dbl);
        if (UNIQUE(DBL_PTR(_i_12704)) && (DBL_PTR(_i_12704)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_12704);
        _i_12704 = _1;
    }
L5: 

    /** 	if i < 0 then*/
    if (_i_12704 >= 0)
    goto L6; // [124] 136

    /** 		i = (-i)*/
    _i_12704 = - _i_12704;
L6: 

    /** 	return list_of_primes[i]*/
    _2 = (int)SEQ_PTR(_34list_of_primes_12615);
    _7021 = (int)*(((s1_ptr)_2)->base + _i_12704);
    Ref(_7021);
    DeRef(_fail_signal_p_12702);
    DeRef(_7012);
    _7012 = NOVALUE;
    return _7021;
    ;
}



// 0xFEE27426
