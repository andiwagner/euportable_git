// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _6int_to_bytes(int _x_1181)
{
    int _a_1182 = NOVALUE;
    int _b_1183 = NOVALUE;
    int _c_1184 = NOVALUE;
    int _d_1185 = NOVALUE;
    int _546 = NOVALUE;
    int _0, _1, _2;
    

    /** 	a = remainder(x, #100)*/
    if (IS_ATOM_INT(_x_1181)) {
        _a_1182 = (_x_1181 % 256);
    }
    else {
        temp_d.dbl = (double)256;
        _a_1182 = Dremainder(DBL_PTR(_x_1181), &temp_d);
    }
    if (!IS_ATOM_INT(_a_1182)) {
        _1 = (long)(DBL_PTR(_a_1182)->dbl);
        if (UNIQUE(DBL_PTR(_a_1182)) && (DBL_PTR(_a_1182)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_a_1182);
        _a_1182 = _1;
    }

    /** 	x = floor(x / #100)*/
    _0 = _x_1181;
    if (IS_ATOM_INT(_x_1181)) {
        if (256 > 0 && _x_1181 >= 0) {
            _x_1181 = _x_1181 / 256;
        }
        else {
            temp_dbl = floor((double)_x_1181 / (double)256);
            if (_x_1181 != MININT)
            _x_1181 = (long)temp_dbl;
            else
            _x_1181 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_1181, 256);
        _x_1181 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_0);

    /** 	b = remainder(x, #100)*/
    if (IS_ATOM_INT(_x_1181)) {
        _b_1183 = (_x_1181 % 256);
    }
    else {
        temp_d.dbl = (double)256;
        _b_1183 = Dremainder(DBL_PTR(_x_1181), &temp_d);
    }
    if (!IS_ATOM_INT(_b_1183)) {
        _1 = (long)(DBL_PTR(_b_1183)->dbl);
        if (UNIQUE(DBL_PTR(_b_1183)) && (DBL_PTR(_b_1183)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_b_1183);
        _b_1183 = _1;
    }

    /** 	x = floor(x / #100)*/
    _0 = _x_1181;
    if (IS_ATOM_INT(_x_1181)) {
        if (256 > 0 && _x_1181 >= 0) {
            _x_1181 = _x_1181 / 256;
        }
        else {
            temp_dbl = floor((double)_x_1181 / (double)256);
            if (_x_1181 != MININT)
            _x_1181 = (long)temp_dbl;
            else
            _x_1181 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_1181, 256);
        _x_1181 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_0);

    /** 	c = remainder(x, #100)*/
    if (IS_ATOM_INT(_x_1181)) {
        _c_1184 = (_x_1181 % 256);
    }
    else {
        temp_d.dbl = (double)256;
        _c_1184 = Dremainder(DBL_PTR(_x_1181), &temp_d);
    }
    if (!IS_ATOM_INT(_c_1184)) {
        _1 = (long)(DBL_PTR(_c_1184)->dbl);
        if (UNIQUE(DBL_PTR(_c_1184)) && (DBL_PTR(_c_1184)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_1184);
        _c_1184 = _1;
    }

    /** 	x = floor(x / #100)*/
    _0 = _x_1181;
    if (IS_ATOM_INT(_x_1181)) {
        if (256 > 0 && _x_1181 >= 0) {
            _x_1181 = _x_1181 / 256;
        }
        else {
            temp_dbl = floor((double)_x_1181 / (double)256);
            if (_x_1181 != MININT)
            _x_1181 = (long)temp_dbl;
            else
            _x_1181 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_1181, 256);
        _x_1181 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_0);

    /** 	d = remainder(x, #100)*/
    if (IS_ATOM_INT(_x_1181)) {
        _d_1185 = (_x_1181 % 256);
    }
    else {
        temp_d.dbl = (double)256;
        _d_1185 = Dremainder(DBL_PTR(_x_1181), &temp_d);
    }
    if (!IS_ATOM_INT(_d_1185)) {
        _1 = (long)(DBL_PTR(_d_1185)->dbl);
        if (UNIQUE(DBL_PTR(_d_1185)) && (DBL_PTR(_d_1185)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_d_1185);
        _d_1185 = _1;
    }

    /** 	return {a,b,c,d}*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _a_1182;
    *((int *)(_2+8)) = _b_1183;
    *((int *)(_2+12)) = _c_1184;
    *((int *)(_2+16)) = _d_1185;
    _546 = MAKE_SEQ(_1);
    DeRef(_x_1181);
    return _546;
    ;
}


int _6int_to_bits(int _x_1223, int _nbits_1224)
{
    int _bits_1225 = NOVALUE;
    int _mask_1226 = NOVALUE;
    int _572 = NOVALUE;
    int _571 = NOVALUE;
    int _569 = NOVALUE;
    int _566 = NOVALUE;
    int _565 = NOVALUE;
    int _564 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if nbits < 1 then*/

    /** 	bits = repeat(0, nbits)*/
    DeRef(_bits_1225);
    _bits_1225 = Repeat(0, _nbits_1224);

    /** 	if nbits <= 32 then*/

    /** 		mask = 1*/
    DeRef(_mask_1226);
    _mask_1226 = 1;

    /** 		for i = 1 to nbits do*/
    _564 = _nbits_1224;
    {
        int _i_1233;
        _i_1233 = 1;
L1: 
        if (_i_1233 > _564){
            goto L2; // [38] 72
        }

        /** 			bits[i] = and_bits(x, mask) and 1*/
        if (IS_ATOM_INT(_x_1223) && IS_ATOM_INT(_mask_1226)) {
            {unsigned long tu;
                 tu = (unsigned long)_x_1223 & (unsigned long)_mask_1226;
                 _565 = MAKE_UINT(tu);
            }
        }
        else {
            if (IS_ATOM_INT(_x_1223)) {
                temp_d.dbl = (double)_x_1223;
                _565 = Dand_bits(&temp_d, DBL_PTR(_mask_1226));
            }
            else {
                if (IS_ATOM_INT(_mask_1226)) {
                    temp_d.dbl = (double)_mask_1226;
                    _565 = Dand_bits(DBL_PTR(_x_1223), &temp_d);
                }
                else
                _565 = Dand_bits(DBL_PTR(_x_1223), DBL_PTR(_mask_1226));
            }
        }
        if (IS_ATOM_INT(_565)) {
            _566 = (_565 != 0 && 1 != 0);
        }
        else {
            temp_d.dbl = (double)1;
            _566 = Dand(DBL_PTR(_565), &temp_d);
        }
        DeRef(_565);
        _565 = NOVALUE;
        _2 = (int)SEQ_PTR(_bits_1225);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _bits_1225 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_1233);
        _1 = *(int *)_2;
        *(int *)_2 = _566;
        if( _1 != _566 ){
            DeRef(_1);
        }
        _566 = NOVALUE;

        /** 			mask *= 2*/
        _0 = _mask_1226;
        if (IS_ATOM_INT(_mask_1226) && IS_ATOM_INT(_mask_1226)) {
            _mask_1226 = _mask_1226 + _mask_1226;
            if ((long)((unsigned long)_mask_1226 + (unsigned long)HIGH_BITS) >= 0) 
            _mask_1226 = NewDouble((double)_mask_1226);
        }
        else {
            if (IS_ATOM_INT(_mask_1226)) {
                _mask_1226 = NewDouble((double)_mask_1226 + DBL_PTR(_mask_1226)->dbl);
            }
            else {
                if (IS_ATOM_INT(_mask_1226)) {
                    _mask_1226 = NewDouble(DBL_PTR(_mask_1226)->dbl + (double)_mask_1226);
                }
                else
                _mask_1226 = NewDouble(DBL_PTR(_mask_1226)->dbl + DBL_PTR(_mask_1226)->dbl);
            }
        }
        DeRef(_0);

        /** 		end for*/
        _i_1233 = _i_1233 + 1;
        goto L1; // [67] 45
L2: 
        ;
    }
    goto L3; // [72] 128

    /** 		if x < 0 then*/
    if (binary_op_a(GREATEREQ, _x_1223, 0)){
        goto L4; // [77] 92
    }

    /** 			x += power(2, nbits) -- for 2's complement bit pattern*/
    _569 = power(2, _nbits_1224);
    _0 = _x_1223;
    if (IS_ATOM_INT(_x_1223) && IS_ATOM_INT(_569)) {
        _x_1223 = _x_1223 + _569;
        if ((long)((unsigned long)_x_1223 + (unsigned long)HIGH_BITS) >= 0) 
        _x_1223 = NewDouble((double)_x_1223);
    }
    else {
        if (IS_ATOM_INT(_x_1223)) {
            _x_1223 = NewDouble((double)_x_1223 + DBL_PTR(_569)->dbl);
        }
        else {
            if (IS_ATOM_INT(_569)) {
                _x_1223 = NewDouble(DBL_PTR(_x_1223)->dbl + (double)_569);
            }
            else
            _x_1223 = NewDouble(DBL_PTR(_x_1223)->dbl + DBL_PTR(_569)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_569);
    _569 = NOVALUE;
L4: 

    /** 		for i = 1 to nbits do*/
    _571 = _nbits_1224;
    {
        int _i_1244;
        _i_1244 = 1;
L5: 
        if (_i_1244 > _571){
            goto L6; // [97] 127
        }

        /** 			bits[i] = remainder(x, 2)*/
        if (IS_ATOM_INT(_x_1223)) {
            _572 = (_x_1223 % 2);
        }
        else {
            temp_d.dbl = (double)2;
            _572 = Dremainder(DBL_PTR(_x_1223), &temp_d);
        }
        _2 = (int)SEQ_PTR(_bits_1225);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _bits_1225 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_1244);
        _1 = *(int *)_2;
        *(int *)_2 = _572;
        if( _1 != _572 ){
            DeRef(_1);
        }
        _572 = NOVALUE;

        /** 			x = floor(x / 2)*/
        _0 = _x_1223;
        if (IS_ATOM_INT(_x_1223)) {
            _x_1223 = _x_1223 >> 1;
        }
        else {
            _1 = binary_op(DIVIDE, _x_1223, 2);
            _x_1223 = unary_op(FLOOR, _1);
            DeRef(_1);
        }
        DeRef(_0);

        /** 		end for*/
        _i_1244 = _i_1244 + 1;
        goto L5; // [122] 104
L6: 
        ;
    }
L3: 

    /** 	return bits*/
    DeRef(_x_1223);
    DeRef(_mask_1226);
    return _bits_1225;
    ;
}


int _6bits_to_int(int _bits_1250)
{
    int _value_1251 = NOVALUE;
    int _p_1252 = NOVALUE;
    int _575 = NOVALUE;
    int _574 = NOVALUE;
    int _0, _1, _2;
    

    /** 	value = 0*/
    DeRef(_value_1251);
    _value_1251 = 0;

    /** 	p = 1*/
    DeRef(_p_1252);
    _p_1252 = 1;

    /** 	for i = 1 to length(bits) do*/
    if (IS_SEQUENCE(_bits_1250)){
            _574 = SEQ_PTR(_bits_1250)->length;
    }
    else {
        _574 = 1;
    }
    {
        int _i_1254;
        _i_1254 = 1;
L1: 
        if (_i_1254 > _574){
            goto L2; // [18] 54
        }

        /** 		if bits[i] then*/
        _2 = (int)SEQ_PTR(_bits_1250);
        _575 = (int)*(((s1_ptr)_2)->base + _i_1254);
        if (_575 == 0) {
            _575 = NOVALUE;
            goto L3; // [31] 41
        }
        else {
            if (!IS_ATOM_INT(_575) && DBL_PTR(_575)->dbl == 0.0){
                _575 = NOVALUE;
                goto L3; // [31] 41
            }
            _575 = NOVALUE;
        }
        _575 = NOVALUE;

        /** 			value += p*/
        _0 = _value_1251;
        if (IS_ATOM_INT(_value_1251) && IS_ATOM_INT(_p_1252)) {
            _value_1251 = _value_1251 + _p_1252;
            if ((long)((unsigned long)_value_1251 + (unsigned long)HIGH_BITS) >= 0) 
            _value_1251 = NewDouble((double)_value_1251);
        }
        else {
            if (IS_ATOM_INT(_value_1251)) {
                _value_1251 = NewDouble((double)_value_1251 + DBL_PTR(_p_1252)->dbl);
            }
            else {
                if (IS_ATOM_INT(_p_1252)) {
                    _value_1251 = NewDouble(DBL_PTR(_value_1251)->dbl + (double)_p_1252);
                }
                else
                _value_1251 = NewDouble(DBL_PTR(_value_1251)->dbl + DBL_PTR(_p_1252)->dbl);
            }
        }
        DeRef(_0);
L3: 

        /** 		p += p*/
        _0 = _p_1252;
        if (IS_ATOM_INT(_p_1252) && IS_ATOM_INT(_p_1252)) {
            _p_1252 = _p_1252 + _p_1252;
            if ((long)((unsigned long)_p_1252 + (unsigned long)HIGH_BITS) >= 0) 
            _p_1252 = NewDouble((double)_p_1252);
        }
        else {
            if (IS_ATOM_INT(_p_1252)) {
                _p_1252 = NewDouble((double)_p_1252 + DBL_PTR(_p_1252)->dbl);
            }
            else {
                if (IS_ATOM_INT(_p_1252)) {
                    _p_1252 = NewDouble(DBL_PTR(_p_1252)->dbl + (double)_p_1252);
                }
                else
                _p_1252 = NewDouble(DBL_PTR(_p_1252)->dbl + DBL_PTR(_p_1252)->dbl);
            }
        }
        DeRef(_0);

        /** 	end for*/
        _i_1254 = _i_1254 + 1;
        goto L1; // [49] 25
L2: 
        ;
    }

    /** 	return value*/
    DeRefDS(_bits_1250);
    DeRef(_p_1252);
    return _value_1251;
    ;
}


int _6atom_to_float64(int _a_1262)
{
    int _578 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_A_TO_F64, a)*/
    _578 = machine(46, _a_1262);
    DeRef(_a_1262);
    return _578;
    ;
}


int _6atom_to_float32(int _a_1266)
{
    int _579 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_A_TO_F32, a)*/
    _579 = machine(48, _a_1266);
    DeRef(_a_1266);
    return _579;
    ;
}


int _6float64_to_atom(int _ieee64_1270)
{
    int _580 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_F64_TO_A, ieee64)*/
    _580 = machine(47, _ieee64_1270);
    DeRefDS(_ieee64_1270);
    return _580;
    ;
}


int _6float32_to_atom(int _ieee32_1274)
{
    int _581 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_F32_TO_A, ieee32)*/
    _581 = machine(49, _ieee32_1274);
    DeRefDS(_ieee32_1274);
    return _581;
    ;
}


int _6hex_text(int _text_1278)
{
    int _res_1279 = NOVALUE;
    int _fp_1280 = NOVALUE;
    int _div_1281 = NOVALUE;
    int _pos_1282 = NOVALUE;
    int _sign_1283 = NOVALUE;
    int _n_1284 = NOVALUE;
    int _609 = NOVALUE;
    int _607 = NOVALUE;
    int _598 = NOVALUE;
    int _597 = NOVALUE;
    int _596 = NOVALUE;
    int _595 = NOVALUE;
    int _592 = NOVALUE;
    int _589 = NOVALUE;
    int _585 = NOVALUE;
    int _583 = NOVALUE;
    int _582 = NOVALUE;
    int _0, _1, _2;
    

    /** 	res = 0*/
    DeRef(_res_1279);
    _res_1279 = 0;

    /** 	fp = 0*/
    DeRef(_fp_1280);
    _fp_1280 = 0;

    /** 	div = 0*/
    _div_1281 = 0;

    /** 	sign = 0*/
    _sign_1283 = 0;

    /** 	n = 0*/
    _n_1284 = 0;

    /** 	for i = 1 to length(text) do*/
    if (IS_SEQUENCE(_text_1278)){
            _582 = SEQ_PTR(_text_1278)->length;
    }
    else {
        _582 = 1;
    }
    {
        int _i_1286;
        _i_1286 = 1;
L1: 
        if (_i_1286 > _582){
            goto L2; // [33] 254
        }

        /** 		if text[i] = '_' then*/
        _2 = (int)SEQ_PTR(_text_1278);
        _583 = (int)*(((s1_ptr)_2)->base + _i_1286);
        if (binary_op_a(NOTEQ, _583, 95)){
            _583 = NOVALUE;
            goto L3; // [46] 55
        }
        _583 = NOVALUE;

        /** 			continue*/
        goto L4; // [52] 249
L3: 

        /** 		if text[i] = '#' then*/
        _2 = (int)SEQ_PTR(_text_1278);
        _585 = (int)*(((s1_ptr)_2)->base + _i_1286);
        if (binary_op_a(NOTEQ, _585, 35)){
            _585 = NOVALUE;
            goto L5; // [61] 84
        }
        _585 = NOVALUE;

        /** 			if n = 0 then*/
        if (_n_1284 != 0)
        goto L2; // [67] 254

        /** 				continue*/
        goto L4; // [73] 249
        goto L6; // [75] 83

        /** 				exit*/
        goto L2; // [80] 254
L6: 
L5: 

        /** 		if text[i] = '.' then*/
        _2 = (int)SEQ_PTR(_text_1278);
        _589 = (int)*(((s1_ptr)_2)->base + _i_1286);
        if (binary_op_a(NOTEQ, _589, 46)){
            _589 = NOVALUE;
            goto L7; // [90] 118
        }
        _589 = NOVALUE;

        /** 			if div = 0 then*/
        if (_div_1281 != 0)
        goto L2; // [96] 254

        /** 				div = 1*/
        _div_1281 = 1;

        /** 				continue*/
        goto L4; // [107] 249
        goto L8; // [109] 117

        /** 				exit*/
        goto L2; // [114] 254
L8: 
L7: 

        /** 		if text[i] = '-' then*/
        _2 = (int)SEQ_PTR(_text_1278);
        _592 = (int)*(((s1_ptr)_2)->base + _i_1286);
        if (binary_op_a(NOTEQ, _592, 45)){
            _592 = NOVALUE;
            goto L9; // [124] 164
        }
        _592 = NOVALUE;

        /** 			if sign = 0 and n = 0 then*/
        _595 = (_sign_1283 == 0);
        if (_595 == 0) {
            goto L2; // [134] 254
        }
        _597 = (_n_1284 == 0);
        if (_597 == 0)
        {
            DeRef(_597);
            _597 = NOVALUE;
            goto L2; // [143] 254
        }
        else{
            DeRef(_597);
            _597 = NOVALUE;
        }

        /** 				sign = -1*/
        _sign_1283 = -1;

        /** 				continue*/
        goto L4; // [153] 249
        goto LA; // [155] 163

        /** 				exit*/
        goto L2; // [160] 254
LA: 
L9: 

        /** 		pos = eu:find(text[i], "0123456789abcdefABCDEF")*/
        _2 = (int)SEQ_PTR(_text_1278);
        _598 = (int)*(((s1_ptr)_2)->base + _i_1286);
        _pos_1282 = find_from(_598, _599, 1);
        _598 = NOVALUE;

        /** 		if pos = 0 then*/
        if (_pos_1282 != 0)
        goto LB; // [177] 186

        /** 			exit*/
        goto L2; // [183] 254
LB: 

        /** 		if pos > 16 then*/
        if (_pos_1282 <= 16)
        goto LC; // [188] 199

        /** 			pos -= 6*/
        _pos_1282 = _pos_1282 - 6;
LC: 

        /** 		pos -= 1*/
        _pos_1282 = _pos_1282 - 1;

        /** 		if div = 0 then*/
        if (_div_1281 != 0)
        goto LD; // [207] 224

        /** 			res = res * 16 + pos*/
        if (IS_ATOM_INT(_res_1279)) {
            if (_res_1279 == (short)_res_1279)
            _607 = _res_1279 * 16;
            else
            _607 = NewDouble(_res_1279 * (double)16);
        }
        else {
            _607 = NewDouble(DBL_PTR(_res_1279)->dbl * (double)16);
        }
        DeRef(_res_1279);
        if (IS_ATOM_INT(_607)) {
            _res_1279 = _607 + _pos_1282;
            if ((long)((unsigned long)_res_1279 + (unsigned long)HIGH_BITS) >= 0) 
            _res_1279 = NewDouble((double)_res_1279);
        }
        else {
            _res_1279 = NewDouble(DBL_PTR(_607)->dbl + (double)_pos_1282);
        }
        DeRef(_607);
        _607 = NOVALUE;
        goto LE; // [221] 241
LD: 

        /** 		    fp = fp * 16 + pos*/
        if (IS_ATOM_INT(_fp_1280)) {
            if (_fp_1280 == (short)_fp_1280)
            _609 = _fp_1280 * 16;
            else
            _609 = NewDouble(_fp_1280 * (double)16);
        }
        else {
            _609 = NewDouble(DBL_PTR(_fp_1280)->dbl * (double)16);
        }
        DeRef(_fp_1280);
        if (IS_ATOM_INT(_609)) {
            _fp_1280 = _609 + _pos_1282;
            if ((long)((unsigned long)_fp_1280 + (unsigned long)HIGH_BITS) >= 0) 
            _fp_1280 = NewDouble((double)_fp_1280);
        }
        else {
            _fp_1280 = NewDouble(DBL_PTR(_609)->dbl + (double)_pos_1282);
        }
        DeRef(_609);
        _609 = NOVALUE;

        /** 		    div += 1*/
        _div_1281 = _div_1281 + 1;
LE: 

        /** 		n += 1*/
        _n_1284 = _n_1284 + 1;

        /** 	end for*/
L4: 
        _i_1286 = _i_1286 + 1;
        goto L1; // [249] 40
L2: 
        ;
    }

    /** 	while div > 1 do*/
LF: 
    if (_div_1281 <= 1)
    goto L10; // [259] 280

    /** 		fp /= 16*/
    _0 = _fp_1280;
    if (IS_ATOM_INT(_fp_1280)) {
        _fp_1280 = (_fp_1280 % 16) ? NewDouble((double)_fp_1280 / 16) : (_fp_1280 / 16);
    }
    else {
        _fp_1280 = NewDouble(DBL_PTR(_fp_1280)->dbl / (double)16);
    }
    DeRef(_0);

    /** 		div -= 1*/
    _div_1281 = _div_1281 - 1;

    /** 	end while*/
    goto LF; // [277] 259
L10: 

    /** 	res += fp*/
    _0 = _res_1279;
    if (IS_ATOM_INT(_res_1279) && IS_ATOM_INT(_fp_1280)) {
        _res_1279 = _res_1279 + _fp_1280;
        if ((long)((unsigned long)_res_1279 + (unsigned long)HIGH_BITS) >= 0) 
        _res_1279 = NewDouble((double)_res_1279);
    }
    else {
        if (IS_ATOM_INT(_res_1279)) {
            _res_1279 = NewDouble((double)_res_1279 + DBL_PTR(_fp_1280)->dbl);
        }
        else {
            if (IS_ATOM_INT(_fp_1280)) {
                _res_1279 = NewDouble(DBL_PTR(_res_1279)->dbl + (double)_fp_1280);
            }
            else
            _res_1279 = NewDouble(DBL_PTR(_res_1279)->dbl + DBL_PTR(_fp_1280)->dbl);
        }
    }
    DeRef(_0);

    /** 	if sign != 0 then*/
    if (_sign_1283 == 0)
    goto L11; // [288] 298

    /** 		res = -res*/
    _0 = _res_1279;
    if (IS_ATOM_INT(_res_1279)) {
        if ((unsigned long)_res_1279 == 0xC0000000)
        _res_1279 = (int)NewDouble((double)-0xC0000000);
        else
        _res_1279 = - _res_1279;
    }
    else {
        _res_1279 = unary_op(UMINUS, _res_1279);
    }
    DeRef(_0);
L11: 

    /** 	return res*/
    DeRefDS(_text_1278);
    DeRef(_fp_1280);
    DeRef(_595);
    _595 = NOVALUE;
    return _res_1279;
    ;
}


int _6to_number(int _text_in_1354, int _return_bad_pos_1355)
{
    int _lDotFound_1356 = NOVALUE;
    int _lSignFound_1357 = NOVALUE;
    int _lCharValue_1358 = NOVALUE;
    int _lBadPos_1359 = NOVALUE;
    int _lLeftSize_1360 = NOVALUE;
    int _lRightSize_1361 = NOVALUE;
    int _lLeftValue_1362 = NOVALUE;
    int _lRightValue_1363 = NOVALUE;
    int _lBase_1364 = NOVALUE;
    int _lPercent_1366 = NOVALUE;
    int _lResult_1367 = NOVALUE;
    int _lDigitCount_1368 = NOVALUE;
    int _lCurrencyFound_1369 = NOVALUE;
    int _lLastDigit_1370 = NOVALUE;
    int _lChar_1371 = NOVALUE;
    int _717 = NOVALUE;
    int _716 = NOVALUE;
    int _709 = NOVALUE;
    int _707 = NOVALUE;
    int _706 = NOVALUE;
    int _701 = NOVALUE;
    int _700 = NOVALUE;
    int _699 = NOVALUE;
    int _698 = NOVALUE;
    int _697 = NOVALUE;
    int _696 = NOVALUE;
    int _692 = NOVALUE;
    int _688 = NOVALUE;
    int _679 = NOVALUE;
    int _660 = NOVALUE;
    int _659 = NOVALUE;
    int _652 = NOVALUE;
    int _650 = NOVALUE;
    int _643 = NOVALUE;
    int _642 = NOVALUE;
    int _641 = NOVALUE;
    int _640 = NOVALUE;
    int _639 = NOVALUE;
    int _638 = NOVALUE;
    int _636 = NOVALUE;
    int _635 = NOVALUE;
    int _634 = NOVALUE;
    int _626 = NOVALUE;
    int _625 = NOVALUE;
    int _624 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer lDotFound = 0*/
    _lDotFound_1356 = 0;

    /** 	integer lSignFound = 2*/
    _lSignFound_1357 = 2;

    /** 	integer lBadPos = 0*/
    _lBadPos_1359 = 0;

    /** 	atom    lLeftSize = 0*/
    DeRef(_lLeftSize_1360);
    _lLeftSize_1360 = 0;

    /** 	atom    lRightSize = 1*/
    DeRef(_lRightSize_1361);
    _lRightSize_1361 = 1;

    /** 	atom    lLeftValue = 0*/
    DeRef(_lLeftValue_1362);
    _lLeftValue_1362 = 0;

    /** 	atom    lRightValue = 0*/
    DeRef(_lRightValue_1363);
    _lRightValue_1363 = 0;

    /** 	integer lBase = 10*/
    _lBase_1364 = 10;

    /** 	integer lPercent = 1*/
    _lPercent_1366 = 1;

    /** 	integer lDigitCount = 0*/
    _lDigitCount_1368 = 0;

    /** 	integer lCurrencyFound = 0*/
    _lCurrencyFound_1369 = 0;

    /** 	integer lLastDigit = 0*/
    _lLastDigit_1370 = 0;

    /** 	for i = 1 to length(text_in) do*/
    if (IS_SEQUENCE(_text_in_1354)){
            _624 = SEQ_PTR(_text_in_1354)->length;
    }
    else {
        _624 = 1;
    }
    {
        int _i_1373;
        _i_1373 = 1;
L1: 
        if (_i_1373 > _624){
            goto L2; // [70] 672
        }

        /** 		if not integer(text_in[i]) then*/
        _2 = (int)SEQ_PTR(_text_in_1354);
        _625 = (int)*(((s1_ptr)_2)->base + _i_1373);
        if (IS_ATOM_INT(_625))
        _626 = 1;
        else if (IS_ATOM_DBL(_625))
        _626 = IS_ATOM_INT(DoubleToInt(_625));
        else
        _626 = 0;
        _625 = NOVALUE;
        if (_626 != 0)
        goto L3; // [86] 94
        _626 = NOVALUE;

        /** 			exit*/
        goto L2; // [91] 672
L3: 

        /** 		lChar = text_in[i]*/
        _2 = (int)SEQ_PTR(_text_in_1354);
        _lChar_1371 = (int)*(((s1_ptr)_2)->base + _i_1373);
        if (!IS_ATOM_INT(_lChar_1371))
        _lChar_1371 = (long)DBL_PTR(_lChar_1371)->dbl;

        /** 		switch lChar do*/
        _0 = _lChar_1371;
        switch ( _0 ){ 

            /** 			case '-' then*/
            case 45:

            /** 				if lSignFound = 2 then*/
            if (_lSignFound_1357 != 2)
            goto L4; // [113] 130

            /** 					lSignFound = -1*/
            _lSignFound_1357 = -1;

            /** 					lLastDigit = lDigitCount*/
            _lLastDigit_1370 = _lDigitCount_1368;
            goto L5; // [127] 654
L4: 

            /** 					lBadPos = i*/
            _lBadPos_1359 = _i_1373;
            goto L5; // [136] 654

            /** 			case '+' then*/
            case 43:

            /** 				if lSignFound = 2 then*/
            if (_lSignFound_1357 != 2)
            goto L6; // [144] 161

            /** 					lSignFound = 1*/
            _lSignFound_1357 = 1;

            /** 					lLastDigit = lDigitCount*/
            _lLastDigit_1370 = _lDigitCount_1368;
            goto L5; // [158] 654
L6: 

            /** 					lBadPos = i*/
            _lBadPos_1359 = _i_1373;
            goto L5; // [167] 654

            /** 			case '#' then*/
            case 35:

            /** 				if lDigitCount = 0 and lBase = 10 then*/
            _634 = (_lDigitCount_1368 == 0);
            if (_634 == 0) {
                goto L7; // [179] 199
            }
            _636 = (_lBase_1364 == 10);
            if (_636 == 0)
            {
                DeRef(_636);
                _636 = NOVALUE;
                goto L7; // [188] 199
            }
            else{
                DeRef(_636);
                _636 = NOVALUE;
            }

            /** 					lBase = 16*/
            _lBase_1364 = 16;
            goto L5; // [196] 654
L7: 

            /** 					lBadPos = i*/
            _lBadPos_1359 = _i_1373;
            goto L5; // [205] 654

            /** 			case '@' then*/
            case 64:

            /** 				if lDigitCount = 0  and lBase = 10 then*/
            _638 = (_lDigitCount_1368 == 0);
            if (_638 == 0) {
                goto L8; // [217] 237
            }
            _640 = (_lBase_1364 == 10);
            if (_640 == 0)
            {
                DeRef(_640);
                _640 = NOVALUE;
                goto L8; // [226] 237
            }
            else{
                DeRef(_640);
                _640 = NOVALUE;
            }

            /** 					lBase = 8*/
            _lBase_1364 = 8;
            goto L5; // [234] 654
L8: 

            /** 					lBadPos = i*/
            _lBadPos_1359 = _i_1373;
            goto L5; // [243] 654

            /** 			case '!' then*/
            case 33:

            /** 				if lDigitCount = 0  and lBase = 10 then*/
            _641 = (_lDigitCount_1368 == 0);
            if (_641 == 0) {
                goto L9; // [255] 275
            }
            _643 = (_lBase_1364 == 10);
            if (_643 == 0)
            {
                DeRef(_643);
                _643 = NOVALUE;
                goto L9; // [264] 275
            }
            else{
                DeRef(_643);
                _643 = NOVALUE;
            }

            /** 					lBase = 2*/
            _lBase_1364 = 2;
            goto L5; // [272] 654
L9: 

            /** 					lBadPos = i*/
            _lBadPos_1359 = _i_1373;
            goto L5; // [281] 654

            /** 			case '$', '�', '�', '�', '�' then*/
            case 36:
            case 163:
            case 164:
            case 165:
            case 128:

            /** 				if lCurrencyFound = 0 then*/
            if (_lCurrencyFound_1369 != 0)
            goto LA; // [297] 314

            /** 					lCurrencyFound = 1*/
            _lCurrencyFound_1369 = 1;

            /** 					lLastDigit = lDigitCount*/
            _lLastDigit_1370 = _lDigitCount_1368;
            goto L5; // [311] 654
LA: 

            /** 					lBadPos = i*/
            _lBadPos_1359 = _i_1373;
            goto L5; // [320] 654

            /** 			case '_' then -- grouping character*/
            case 95:

            /** 				if lDigitCount = 0 or lLastDigit != 0 then*/
            _650 = (_lDigitCount_1368 == 0);
            if (_650 != 0) {
                goto LB; // [332] 345
            }
            _652 = (_lLastDigit_1370 != 0);
            if (_652 == 0)
            {
                DeRef(_652);
                _652 = NOVALUE;
                goto L5; // [341] 654
            }
            else{
                DeRef(_652);
                _652 = NOVALUE;
            }
LB: 

            /** 					lBadPos = i*/
            _lBadPos_1359 = _i_1373;
            goto L5; // [351] 654

            /** 			case '.', ',' then*/
            case 46:
            case 44:

            /** 				if lLastDigit = 0 then*/
            if (_lLastDigit_1370 != 0)
            goto LC; // [361] 400

            /** 					if decimal_mark = lChar then*/
            if (46 != _lChar_1371)
            goto L5; // [369] 654

            /** 						if lDotFound = 0 then*/
            if (_lDotFound_1356 != 0)
            goto LD; // [375] 387

            /** 							lDotFound = 1*/
            _lDotFound_1356 = 1;
            goto L5; // [384] 654
LD: 

            /** 							lBadPos = i*/
            _lBadPos_1359 = _i_1373;
            goto L5; // [393] 654
            goto L5; // [397] 654
LC: 

            /** 					lBadPos = i*/
            _lBadPos_1359 = _i_1373;
            goto L5; // [406] 654

            /** 			case '%' then*/
            case 37:

            /** 				lLastDigit = lDigitCount*/
            _lLastDigit_1370 = _lDigitCount_1368;

            /** 				if lPercent = 1 then*/
            if (_lPercent_1366 != 1)
            goto LE; // [419] 431

            /** 					lPercent = 100*/
            _lPercent_1366 = 100;
            goto L5; // [428] 654
LE: 

            /** 					if text_in[i-1] = '%' then*/
            _659 = _i_1373 - 1;
            _2 = (int)SEQ_PTR(_text_in_1354);
            _660 = (int)*(((s1_ptr)_2)->base + _659);
            if (binary_op_a(NOTEQ, _660, 37)){
                _660 = NOVALUE;
                goto LF; // [441] 456
            }
            _660 = NOVALUE;

            /** 						lPercent *= 10 -- Yes ten not one hundred.*/
            _lPercent_1366 = _lPercent_1366 * 10;
            goto L5; // [453] 654
LF: 

            /** 						lBadPos = i*/
            _lBadPos_1359 = _i_1373;
            goto L5; // [463] 654

            /** 			case '\t', ' ', #A0 then*/
            case 9:
            case 32:
            case 160:

            /** 				if lDigitCount = 0 then*/
            if (_lDigitCount_1368 != 0)
            goto L10; // [475] 482
            goto L5; // [479] 654
L10: 

            /** 					lLastDigit = i*/
            _lLastDigit_1370 = _i_1373;
            goto L5; // [488] 654

            /** 			case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',*/
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 65:
            case 66:
            case 67:
            case 68:
            case 69:
            case 70:
            case 97:
            case 98:
            case 99:
            case 100:
            case 101:
            case 102:

            /** 	            lCharValue = find(lChar, vDigits) - 1*/
            _679 = find_from(_lChar_1371, _6vDigits_1340, 1);
            _lCharValue_1358 = _679 - 1;
            _679 = NOVALUE;

            /** 	            if lCharValue > 15 then*/
            if (_lCharValue_1358 <= 15)
            goto L11; // [549] 560

            /** 	            	lCharValue -= 6*/
            _lCharValue_1358 = _lCharValue_1358 - 6;
L11: 

            /** 	            if lCharValue >= lBase then*/
            if (_lCharValue_1358 < _lBase_1364)
            goto L12; // [562] 574

            /** 	                lBadPos = i*/
            _lBadPos_1359 = _i_1373;
            goto L5; // [571] 654
L12: 

            /** 	            elsif lLastDigit != 0 then  -- shouldn't be any more digits*/
            if (_lLastDigit_1370 == 0)
            goto L13; // [576] 588

            /** 					lBadPos = i*/
            _lBadPos_1359 = _i_1373;
            goto L5; // [585] 654
L13: 

            /** 				elsif lDotFound = 1 then*/
            if (_lDotFound_1356 != 1)
            goto L14; // [590] 619

            /** 					lRightSize *= lBase*/
            _0 = _lRightSize_1361;
            if (IS_ATOM_INT(_lRightSize_1361)) {
                if (_lRightSize_1361 == (short)_lRightSize_1361 && _lBase_1364 <= INT15 && _lBase_1364 >= -INT15)
                _lRightSize_1361 = _lRightSize_1361 * _lBase_1364;
                else
                _lRightSize_1361 = NewDouble(_lRightSize_1361 * (double)_lBase_1364);
            }
            else {
                _lRightSize_1361 = NewDouble(DBL_PTR(_lRightSize_1361)->dbl * (double)_lBase_1364);
            }
            DeRef(_0);

            /** 					lRightValue = (lRightValue * lBase) + lCharValue*/
            if (IS_ATOM_INT(_lRightValue_1363)) {
                if (_lRightValue_1363 == (short)_lRightValue_1363 && _lBase_1364 <= INT15 && _lBase_1364 >= -INT15)
                _688 = _lRightValue_1363 * _lBase_1364;
                else
                _688 = NewDouble(_lRightValue_1363 * (double)_lBase_1364);
            }
            else {
                _688 = NewDouble(DBL_PTR(_lRightValue_1363)->dbl * (double)_lBase_1364);
            }
            DeRef(_lRightValue_1363);
            if (IS_ATOM_INT(_688)) {
                _lRightValue_1363 = _688 + _lCharValue_1358;
                if ((long)((unsigned long)_lRightValue_1363 + (unsigned long)HIGH_BITS) >= 0) 
                _lRightValue_1363 = NewDouble((double)_lRightValue_1363);
            }
            else {
                _lRightValue_1363 = NewDouble(DBL_PTR(_688)->dbl + (double)_lCharValue_1358);
            }
            DeRef(_688);
            _688 = NOVALUE;

            /** 					lDigitCount += 1*/
            _lDigitCount_1368 = _lDigitCount_1368 + 1;
            goto L5; // [616] 654
L14: 

            /** 					lLeftSize += 1*/
            _0 = _lLeftSize_1360;
            if (IS_ATOM_INT(_lLeftSize_1360)) {
                _lLeftSize_1360 = _lLeftSize_1360 + 1;
                if (_lLeftSize_1360 > MAXINT){
                    _lLeftSize_1360 = NewDouble((double)_lLeftSize_1360);
                }
            }
            else
            _lLeftSize_1360 = binary_op(PLUS, 1, _lLeftSize_1360);
            DeRef(_0);

            /** 					lLeftValue = (lLeftValue * lBase) + lCharValue*/
            if (IS_ATOM_INT(_lLeftValue_1362)) {
                if (_lLeftValue_1362 == (short)_lLeftValue_1362 && _lBase_1364 <= INT15 && _lBase_1364 >= -INT15)
                _692 = _lLeftValue_1362 * _lBase_1364;
                else
                _692 = NewDouble(_lLeftValue_1362 * (double)_lBase_1364);
            }
            else {
                _692 = NewDouble(DBL_PTR(_lLeftValue_1362)->dbl * (double)_lBase_1364);
            }
            DeRef(_lLeftValue_1362);
            if (IS_ATOM_INT(_692)) {
                _lLeftValue_1362 = _692 + _lCharValue_1358;
                if ((long)((unsigned long)_lLeftValue_1362 + (unsigned long)HIGH_BITS) >= 0) 
                _lLeftValue_1362 = NewDouble((double)_lLeftValue_1362);
            }
            else {
                _lLeftValue_1362 = NewDouble(DBL_PTR(_692)->dbl + (double)_lCharValue_1358);
            }
            DeRef(_692);
            _692 = NOVALUE;

            /** 					lDigitCount += 1*/
            _lDigitCount_1368 = _lDigitCount_1368 + 1;
            goto L5; // [642] 654

            /** 			case else*/
            default:

            /** 				lBadPos = i*/
            _lBadPos_1359 = _i_1373;
        ;}L5: 

        /** 		if lBadPos != 0 then*/
        if (_lBadPos_1359 == 0)
        goto L15; // [656] 665

        /** 			exit*/
        goto L2; // [662] 672
L15: 

        /** 	end for*/
        _i_1373 = _i_1373 + 1;
        goto L1; // [667] 77
L2: 
        ;
    }

    /** 	if lBadPos = 0 and lDigitCount = 0 then*/
    _696 = (_lBadPos_1359 == 0);
    if (_696 == 0) {
        goto L16; // [678] 696
    }
    _698 = (_lDigitCount_1368 == 0);
    if (_698 == 0)
    {
        DeRef(_698);
        _698 = NOVALUE;
        goto L16; // [687] 696
    }
    else{
        DeRef(_698);
        _698 = NOVALUE;
    }

    /** 		lBadPos = 1*/
    _lBadPos_1359 = 1;
L16: 

    /** 	if return_bad_pos = 0 and lBadPos != 0 then*/
    _699 = (_return_bad_pos_1355 == 0);
    if (_699 == 0) {
        goto L17; // [702] 721
    }
    _701 = (_lBadPos_1359 != 0);
    if (_701 == 0)
    {
        DeRef(_701);
        _701 = NOVALUE;
        goto L17; // [711] 721
    }
    else{
        DeRef(_701);
        _701 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_text_in_1354);
    DeRef(_lLeftSize_1360);
    DeRef(_lRightSize_1361);
    DeRef(_lLeftValue_1362);
    DeRef(_lRightValue_1363);
    DeRef(_lResult_1367);
    DeRef(_634);
    _634 = NOVALUE;
    DeRef(_638);
    _638 = NOVALUE;
    DeRef(_641);
    _641 = NOVALUE;
    DeRef(_650);
    _650 = NOVALUE;
    DeRef(_659);
    _659 = NOVALUE;
    DeRef(_696);
    _696 = NOVALUE;
    DeRef(_699);
    _699 = NOVALUE;
    return 0;
L17: 

    /** 	if lRightValue = 0 then*/
    if (binary_op_a(NOTEQ, _lRightValue_1363, 0)){
        goto L18; // [723] 751
    }

    /** 	    if lPercent != 1 then*/
    if (_lPercent_1366 == 1)
    goto L19; // [729] 742

    /** 			lResult = (lLeftValue / lPercent)*/
    DeRef(_lResult_1367);
    if (IS_ATOM_INT(_lLeftValue_1362)) {
        _lResult_1367 = (_lLeftValue_1362 % _lPercent_1366) ? NewDouble((double)_lLeftValue_1362 / _lPercent_1366) : (_lLeftValue_1362 / _lPercent_1366);
    }
    else {
        _lResult_1367 = NewDouble(DBL_PTR(_lLeftValue_1362)->dbl / (double)_lPercent_1366);
    }
    goto L1A; // [739] 786
L19: 

    /** 	        lResult = lLeftValue*/
    Ref(_lLeftValue_1362);
    DeRef(_lResult_1367);
    _lResult_1367 = _lLeftValue_1362;
    goto L1A; // [748] 786
L18: 

    /** 	    if lPercent != 1 then*/
    if (_lPercent_1366 == 1)
    goto L1B; // [753] 774

    /** 	        lResult = (lLeftValue  + (lRightValue / (lRightSize))) / lPercent*/
    if (IS_ATOM_INT(_lRightValue_1363) && IS_ATOM_INT(_lRightSize_1361)) {
        _706 = (_lRightValue_1363 % _lRightSize_1361) ? NewDouble((double)_lRightValue_1363 / _lRightSize_1361) : (_lRightValue_1363 / _lRightSize_1361);
    }
    else {
        if (IS_ATOM_INT(_lRightValue_1363)) {
            _706 = NewDouble((double)_lRightValue_1363 / DBL_PTR(_lRightSize_1361)->dbl);
        }
        else {
            if (IS_ATOM_INT(_lRightSize_1361)) {
                _706 = NewDouble(DBL_PTR(_lRightValue_1363)->dbl / (double)_lRightSize_1361);
            }
            else
            _706 = NewDouble(DBL_PTR(_lRightValue_1363)->dbl / DBL_PTR(_lRightSize_1361)->dbl);
        }
    }
    if (IS_ATOM_INT(_lLeftValue_1362) && IS_ATOM_INT(_706)) {
        _707 = _lLeftValue_1362 + _706;
        if ((long)((unsigned long)_707 + (unsigned long)HIGH_BITS) >= 0) 
        _707 = NewDouble((double)_707);
    }
    else {
        if (IS_ATOM_INT(_lLeftValue_1362)) {
            _707 = NewDouble((double)_lLeftValue_1362 + DBL_PTR(_706)->dbl);
        }
        else {
            if (IS_ATOM_INT(_706)) {
                _707 = NewDouble(DBL_PTR(_lLeftValue_1362)->dbl + (double)_706);
            }
            else
            _707 = NewDouble(DBL_PTR(_lLeftValue_1362)->dbl + DBL_PTR(_706)->dbl);
        }
    }
    DeRef(_706);
    _706 = NOVALUE;
    DeRef(_lResult_1367);
    if (IS_ATOM_INT(_707)) {
        _lResult_1367 = (_707 % _lPercent_1366) ? NewDouble((double)_707 / _lPercent_1366) : (_707 / _lPercent_1366);
    }
    else {
        _lResult_1367 = NewDouble(DBL_PTR(_707)->dbl / (double)_lPercent_1366);
    }
    DeRef(_707);
    _707 = NOVALUE;
    goto L1C; // [771] 785
L1B: 

    /** 	        lResult = lLeftValue + (lRightValue / lRightSize)*/
    if (IS_ATOM_INT(_lRightValue_1363) && IS_ATOM_INT(_lRightSize_1361)) {
        _709 = (_lRightValue_1363 % _lRightSize_1361) ? NewDouble((double)_lRightValue_1363 / _lRightSize_1361) : (_lRightValue_1363 / _lRightSize_1361);
    }
    else {
        if (IS_ATOM_INT(_lRightValue_1363)) {
            _709 = NewDouble((double)_lRightValue_1363 / DBL_PTR(_lRightSize_1361)->dbl);
        }
        else {
            if (IS_ATOM_INT(_lRightSize_1361)) {
                _709 = NewDouble(DBL_PTR(_lRightValue_1363)->dbl / (double)_lRightSize_1361);
            }
            else
            _709 = NewDouble(DBL_PTR(_lRightValue_1363)->dbl / DBL_PTR(_lRightSize_1361)->dbl);
        }
    }
    DeRef(_lResult_1367);
    if (IS_ATOM_INT(_lLeftValue_1362) && IS_ATOM_INT(_709)) {
        _lResult_1367 = _lLeftValue_1362 + _709;
        if ((long)((unsigned long)_lResult_1367 + (unsigned long)HIGH_BITS) >= 0) 
        _lResult_1367 = NewDouble((double)_lResult_1367);
    }
    else {
        if (IS_ATOM_INT(_lLeftValue_1362)) {
            _lResult_1367 = NewDouble((double)_lLeftValue_1362 + DBL_PTR(_709)->dbl);
        }
        else {
            if (IS_ATOM_INT(_709)) {
                _lResult_1367 = NewDouble(DBL_PTR(_lLeftValue_1362)->dbl + (double)_709);
            }
            else
            _lResult_1367 = NewDouble(DBL_PTR(_lLeftValue_1362)->dbl + DBL_PTR(_709)->dbl);
        }
    }
    DeRef(_709);
    _709 = NOVALUE;
L1C: 
L1A: 

    /** 	if lSignFound < 0 then*/
    if (_lSignFound_1357 >= 0)
    goto L1D; // [788] 800

    /** 		lResult = -lResult*/
    _0 = _lResult_1367;
    if (IS_ATOM_INT(_lResult_1367)) {
        if ((unsigned long)_lResult_1367 == 0xC0000000)
        _lResult_1367 = (int)NewDouble((double)-0xC0000000);
        else
        _lResult_1367 = - _lResult_1367;
    }
    else {
        _lResult_1367 = unary_op(UMINUS, _lResult_1367);
    }
    DeRef(_0);
L1D: 

    /** 	if return_bad_pos = 0 then*/
    if (_return_bad_pos_1355 != 0)
    goto L1E; // [802] 815

    /** 		return lResult*/
    DeRefDS(_text_in_1354);
    DeRef(_lLeftSize_1360);
    DeRef(_lRightSize_1361);
    DeRef(_lLeftValue_1362);
    DeRef(_lRightValue_1363);
    DeRef(_634);
    _634 = NOVALUE;
    DeRef(_638);
    _638 = NOVALUE;
    DeRef(_641);
    _641 = NOVALUE;
    DeRef(_650);
    _650 = NOVALUE;
    DeRef(_659);
    _659 = NOVALUE;
    DeRef(_696);
    _696 = NOVALUE;
    DeRef(_699);
    _699 = NOVALUE;
    return _lResult_1367;
L1E: 

    /** 	if return_bad_pos = -1 then*/
    if (_return_bad_pos_1355 != -1)
    goto L1F; // [817] 850

    /** 		if lBadPos = 0 then*/
    if (_lBadPos_1359 != 0)
    goto L20; // [823] 838

    /** 			return lResult*/
    DeRefDS(_text_in_1354);
    DeRef(_lLeftSize_1360);
    DeRef(_lRightSize_1361);
    DeRef(_lLeftValue_1362);
    DeRef(_lRightValue_1363);
    DeRef(_634);
    _634 = NOVALUE;
    DeRef(_638);
    _638 = NOVALUE;
    DeRef(_641);
    _641 = NOVALUE;
    DeRef(_650);
    _650 = NOVALUE;
    DeRef(_659);
    _659 = NOVALUE;
    DeRef(_696);
    _696 = NOVALUE;
    DeRef(_699);
    _699 = NOVALUE;
    return _lResult_1367;
    goto L21; // [835] 849
L20: 

    /** 			return {lBadPos}	*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _lBadPos_1359;
    _716 = MAKE_SEQ(_1);
    DeRefDS(_text_in_1354);
    DeRef(_lLeftSize_1360);
    DeRef(_lRightSize_1361);
    DeRef(_lLeftValue_1362);
    DeRef(_lRightValue_1363);
    DeRef(_lResult_1367);
    DeRef(_634);
    _634 = NOVALUE;
    DeRef(_638);
    _638 = NOVALUE;
    DeRef(_641);
    _641 = NOVALUE;
    DeRef(_650);
    _650 = NOVALUE;
    DeRef(_659);
    _659 = NOVALUE;
    DeRef(_696);
    _696 = NOVALUE;
    DeRef(_699);
    _699 = NOVALUE;
    return _716;
L21: 
L1F: 

    /** 	return {lResult, lBadPos}*/
    Ref(_lResult_1367);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _lResult_1367;
    ((int *)_2)[2] = _lBadPos_1359;
    _717 = MAKE_SEQ(_1);
    DeRefDS(_text_in_1354);
    DeRef(_lLeftSize_1360);
    DeRef(_lRightSize_1361);
    DeRef(_lLeftValue_1362);
    DeRef(_lRightValue_1363);
    DeRef(_lResult_1367);
    DeRef(_634);
    _634 = NOVALUE;
    DeRef(_638);
    _638 = NOVALUE;
    DeRef(_641);
    _641 = NOVALUE;
    DeRef(_650);
    _650 = NOVALUE;
    DeRef(_659);
    _659 = NOVALUE;
    DeRef(_696);
    _696 = NOVALUE;
    DeRef(_699);
    _699 = NOVALUE;
    DeRef(_716);
    _716 = NOVALUE;
    return _717;
    ;
}



// 0xE67BC17B
