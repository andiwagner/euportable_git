// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _9find_first_wildcard(int _name_6982, int _from_6983)
{
    int _asterisk_at_6984 = NOVALUE;
    int _question_at_6986 = NOVALUE;
    int _first_wildcard_at_6988 = NOVALUE;
    int _3672 = NOVALUE;
    int _3671 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer asterisk_at = eu:find('*', name, from)*/
    _asterisk_at_6984 = find_from(42, _name_6982, 1);

    /** 	integer question_at = eu:find('?', name, from)*/
    _question_at_6986 = find_from(63, _name_6982, 1);

    /** 	integer first_wildcard_at = asterisk_at*/
    _first_wildcard_at_6988 = _asterisk_at_6984;

    /** 	if asterisk_at or question_at then*/
    if (_asterisk_at_6984 != 0) {
        goto L1; // [26] 35
    }
    if (_question_at_6986 == 0)
    {
        goto L2; // [31] 56
    }
    else{
    }
L1: 

    /** 		if question_at and question_at < asterisk_at then*/
    if (_question_at_6986 == 0) {
        goto L3; // [37] 55
    }
    _3672 = (_question_at_6986 < _asterisk_at_6984);
    if (_3672 == 0)
    {
        DeRef(_3672);
        _3672 = NOVALUE;
        goto L3; // [46] 55
    }
    else{
        DeRef(_3672);
        _3672 = NOVALUE;
    }

    /** 			first_wildcard_at = question_at*/
    _first_wildcard_at_6988 = _question_at_6986;
L3: 
L2: 

    /** 	return first_wildcard_at*/
    DeRefDS(_name_6982);
    return _first_wildcard_at_6988;
    ;
}


int _9dir(int _name_6996)
{
    int _3673 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    _3673 = machine(22, _name_6996);
    DeRefDS(_name_6996);
    return _3673;
    ;
}


int _9current_dir()
{
    int _3675 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_CURRENT_DIR, 0)*/
    _3675 = machine(23, 0);
    return _3675;
    ;
}


int _9chdir(int _newdir_7004)
{
    int _3676 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_CHDIR, newdir)*/
    _3676 = machine(63, _newdir_7004);
    DeRefDS(_newdir_7004);
    return _3676;
    ;
}


int _9walk_dir(int _path_name_7020, int _your_function_7021, int _scan_subdirs_7022, int _dir_source_7023)
{
    int _d_7024 = NOVALUE;
    int _abort_now_7025 = NOVALUE;
    int _orig_func_7026 = NOVALUE;
    int _user_data_7027 = NOVALUE;
    int _source_orig_func_7029 = NOVALUE;
    int _source_user_data_7030 = NOVALUE;
    int _3726 = NOVALUE;
    int _3725 = NOVALUE;
    int _3724 = NOVALUE;
    int _3723 = NOVALUE;
    int _3722 = NOVALUE;
    int _3720 = NOVALUE;
    int _3719 = NOVALUE;
    int _3718 = NOVALUE;
    int _3717 = NOVALUE;
    int _3716 = NOVALUE;
    int _3715 = NOVALUE;
    int _3714 = NOVALUE;
    int _3713 = NOVALUE;
    int _3712 = NOVALUE;
    int _3710 = NOVALUE;
    int _3708 = NOVALUE;
    int _3707 = NOVALUE;
    int _3706 = NOVALUE;
    int _3704 = NOVALUE;
    int _3703 = NOVALUE;
    int _3702 = NOVALUE;
    int _3698 = NOVALUE;
    int _3696 = NOVALUE;
    int _3692 = NOVALUE;
    int _3690 = NOVALUE;
    int _3689 = NOVALUE;
    int _3687 = NOVALUE;
    int _3684 = NOVALUE;
    int _3681 = NOVALUE;
    int _3680 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_scan_subdirs_7022)) {
        _1 = (long)(DBL_PTR(_scan_subdirs_7022)->dbl);
        if (UNIQUE(DBL_PTR(_scan_subdirs_7022)) && (DBL_PTR(_scan_subdirs_7022)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_scan_subdirs_7022);
        _scan_subdirs_7022 = _1;
    }

    /** 	sequence user_data = {path_name, 0}*/
    RefDS(_path_name_7020);
    DeRef(_user_data_7027);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _path_name_7020;
    ((int *)_2)[2] = 0;
    _user_data_7027 = MAKE_SEQ(_1);

    /** 	object source_user_data = ""*/
    RefDS(_5);
    DeRef(_source_user_data_7030);
    _source_user_data_7030 = _5;

    /** 	orig_func = your_function*/
    Ref(_your_function_7021);
    DeRef(_orig_func_7026);
    _orig_func_7026 = _your_function_7021;

    /** 	if sequence(your_function) then*/
    _3680 = IS_SEQUENCE(_your_function_7021);
    if (_3680 == 0)
    {
        _3680 = NOVALUE;
        goto L1; // [26] 46
    }
    else{
        _3680 = NOVALUE;
    }

    /** 		user_data = append(user_data, your_function[2])*/
    _2 = (int)SEQ_PTR(_your_function_7021);
    _3681 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_3681);
    Append(&_user_data_7027, _user_data_7027, _3681);
    _3681 = NOVALUE;

    /** 		your_function = your_function[1]*/
    _0 = _your_function_7021;
    _2 = (int)SEQ_PTR(_your_function_7021);
    _your_function_7021 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_your_function_7021);
    DeRef(_0);
L1: 

    /** 	source_orig_func = dir_source*/
    Ref(_dir_source_7023);
    DeRef(_source_orig_func_7029);
    _source_orig_func_7029 = _dir_source_7023;

    /** 	if sequence(dir_source) then*/
    _3684 = IS_SEQUENCE(_dir_source_7023);
    if (_3684 == 0)
    {
        _3684 = NOVALUE;
        goto L2; // [56] 72
    }
    else{
        _3684 = NOVALUE;
    }

    /** 		source_user_data = dir_source[2]*/
    DeRef(_source_user_data_7030);
    _2 = (int)SEQ_PTR(_dir_source_7023);
    _source_user_data_7030 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_source_user_data_7030);

    /** 		dir_source = dir_source[1]*/
    _0 = _dir_source_7023;
    _2 = (int)SEQ_PTR(_dir_source_7023);
    _dir_source_7023 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_dir_source_7023);
    DeRef(_0);
L2: 

    /** 	if not equal(dir_source, types:NO_ROUTINE_ID) then*/
    if (_dir_source_7023 == -99999)
    _3687 = 1;
    else if (IS_ATOM_INT(_dir_source_7023) && IS_ATOM_INT(-99999))
    _3687 = 0;
    else
    _3687 = (compare(_dir_source_7023, -99999) == 0);
    if (_3687 != 0)
    goto L3; // [78] 116
    _3687 = NOVALUE;

    /** 		if atom(source_orig_func) then*/
    _3689 = IS_ATOM(_source_orig_func_7029);
    if (_3689 == 0)
    {
        _3689 = NOVALUE;
        goto L4; // [86] 102
    }
    else{
        _3689 = NOVALUE;
    }

    /** 			d = call_func(dir_source, {path_name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_path_name_7020);
    *((int *)(_2+4)) = _path_name_7020;
    _3690 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_3690);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_dir_source_7023].addr;
    Ref(*(int *)(_2+4));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4)
                         );
    DeRef(_d_7024);
    _d_7024 = _1;
    DeRefDS(_3690);
    _3690 = NOVALUE;
    goto L5; // [99] 146
L4: 

    /** 			d = call_func(dir_source, {path_name, source_user_data})*/
    Ref(_source_user_data_7030);
    RefDS(_path_name_7020);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _path_name_7020;
    ((int *)_2)[2] = _source_user_data_7030;
    _3692 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_3692);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_dir_source_7023].addr;
    Ref(*(int *)(_2+4));
    Ref(*(int *)(_2+8));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4), 
                        *(int *)(_2+8)
                         );
    DeRef(_d_7024);
    _d_7024 = _1;
    DeRefDS(_3692);
    _3692 = NOVALUE;
    goto L5; // [113] 146
L3: 

    /** 	elsif my_dir = DEFAULT_DIR_SOURCE then*/

    /** 		d = machine_func(M_DIR, path_name)*/
    DeRef(_d_7024);
    _d_7024 = machine(22, _path_name_7020);
    goto L5; // [130] 146

    /** 		d = call_func(my_dir, {path_name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_path_name_7020);
    *((int *)(_2+4)) = _path_name_7020;
    _3696 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_3696);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[-2].addr;
    Ref(*(int *)(_2+4));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4)
                         );
    DeRef(_d_7024);
    _d_7024 = _1;
    DeRefDS(_3696);
    _3696 = NOVALUE;
L5: 

    /** 	if atom(d) then*/
    _3698 = IS_ATOM(_d_7024);
    if (_3698 == 0)
    {
        _3698 = NOVALUE;
        goto L6; // [153] 163
    }
    else{
        _3698 = NOVALUE;
    }

    /** 		return W_BAD_PATH*/
    DeRefDS(_path_name_7020);
    DeRef(_your_function_7021);
    DeRef(_dir_source_7023);
    DeRef(_d_7024);
    DeRef(_abort_now_7025);
    DeRef(_orig_func_7026);
    DeRef(_user_data_7027);
    DeRef(_source_orig_func_7029);
    DeRef(_source_user_data_7030);
    return -1;
L6: 

    /** 	ifdef not UNIX then*/

    /** 		path_name = match_replace('/', path_name, '\\')*/
    RefDS(_path_name_7020);
    _0 = _path_name_7020;
    _path_name_7020 = _7match_replace(47, _path_name_7020, 92, 0);
    DeRefDS(_0);

    /** 	path_name = text:trim_tail(path_name, {' ', SLASH, '\n'})*/
    RefDS(_path_name_7020);
    RefDS(_3700);
    _0 = _path_name_7020;
    _path_name_7020 = _4trim_tail(_path_name_7020, _3700, 0);
    DeRefDS(_0);

    /** 	user_data[1] = path_name*/
    RefDS(_path_name_7020);
    _2 = (int)SEQ_PTR(_user_data_7027);
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _path_name_7020;
    DeRef(_1);

    /** 	for i = 1 to length(d) do*/
    if (IS_SEQUENCE(_d_7024)){
            _3702 = SEQ_PTR(_d_7024)->length;
    }
    else {
        _3702 = 1;
    }
    {
        int _i_7064;
        _i_7064 = 1;
L7: 
        if (_i_7064 > _3702){
            goto L8; // [197] 364
        }

        /** 		if eu:find(d[i][D_NAME], {".", ".."}) then*/
        _2 = (int)SEQ_PTR(_d_7024);
        _3703 = (int)*(((s1_ptr)_2)->base + _i_7064);
        _2 = (int)SEQ_PTR(_3703);
        _3704 = (int)*(((s1_ptr)_2)->base + 1);
        _3703 = NOVALUE;
        RefDS(_3705);
        RefDS(_3674);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _3674;
        ((int *)_2)[2] = _3705;
        _3706 = MAKE_SEQ(_1);
        _3707 = find_from(_3704, _3706, 1);
        _3704 = NOVALUE;
        DeRefDS(_3706);
        _3706 = NOVALUE;
        if (_3707 == 0)
        {
            _3707 = NOVALUE;
            goto L9; // [225] 233
        }
        else{
            _3707 = NOVALUE;
        }

        /** 			continue*/
        goto LA; // [230] 359
L9: 

        /** 		user_data[2] = d[i]*/
        _2 = (int)SEQ_PTR(_d_7024);
        _3708 = (int)*(((s1_ptr)_2)->base + _i_7064);
        Ref(_3708);
        _2 = (int)SEQ_PTR(_user_data_7027);
        _2 = (int)(((s1_ptr)_2)->base + 2);
        _1 = *(int *)_2;
        *(int *)_2 = _3708;
        if( _1 != _3708 ){
            DeRef(_1);
        }
        _3708 = NOVALUE;

        /** 		abort_now = call_func(your_function, user_data)*/
        _1 = (int)SEQ_PTR(_user_data_7027);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_your_function_7021].addr;
        switch(((s1_ptr)_1)->length) {
            case 0:
                _1 = (*(int (*)())_0)(
                                     );
                break;
            case 1:
                Ref(*(int *)(_2+4));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4)
                                     );
                break;
            case 2:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8)
                                     );
                break;
            case 3:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12)
                                     );
                break;
            case 4:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16)
                                     );
                break;
            case 5:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20)
                                     );
                break;
            case 6:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24)
                                     );
                break;
            case 7:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28)
                                     );
                break;
            case 8:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32)
                                     );
                break;
            case 9:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36)
                                     );
                break;
            case 10:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40)
                                     );
                break;
            case 11:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44)
                                     );
                break;
            case 12:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48)
                                     );
                break;
            case 13:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52)
                                     );
                break;
            case 14:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                Ref(*(int *)(_2+56));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52), 
                                    *(int *)(_2+56)
                                     );
                break;
            case 15:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                Ref(*(int *)(_2+56));
                Ref(*(int *)(_2+60));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52), 
                                    *(int *)(_2+56), 
                                    *(int *)(_2+60)
                                     );
                break;
            case 16:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                Ref(*(int *)(_2+56));
                Ref(*(int *)(_2+60));
                Ref(*(int *)(_2+64));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52), 
                                    *(int *)(_2+56), 
                                    *(int *)(_2+60), 
                                    *(int *)(_2+64)
                                     );
                break;
            case 17:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                Ref(*(int *)(_2+56));
                Ref(*(int *)(_2+60));
                Ref(*(int *)(_2+64));
                Ref(*(int *)(_2+68));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52), 
                                    *(int *)(_2+56), 
                                    *(int *)(_2+60), 
                                    *(int *)(_2+64), 
                                    *(int *)(_2+68)
                                     );
                break;
            case 18:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                Ref(*(int *)(_2+56));
                Ref(*(int *)(_2+60));
                Ref(*(int *)(_2+64));
                Ref(*(int *)(_2+68));
                Ref(*(int *)(_2+72));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52), 
                                    *(int *)(_2+56), 
                                    *(int *)(_2+60), 
                                    *(int *)(_2+64), 
                                    *(int *)(_2+68), 
                                    *(int *)(_2+72)
                                     );
                break;
            case 19:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                Ref(*(int *)(_2+56));
                Ref(*(int *)(_2+60));
                Ref(*(int *)(_2+64));
                Ref(*(int *)(_2+68));
                Ref(*(int *)(_2+72));
                Ref(*(int *)(_2+76));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52), 
                                    *(int *)(_2+56), 
                                    *(int *)(_2+60), 
                                    *(int *)(_2+64), 
                                    *(int *)(_2+68), 
                                    *(int *)(_2+72), 
                                    *(int *)(_2+76)
                                     );
                break;
            case 20:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                Ref(*(int *)(_2+56));
                Ref(*(int *)(_2+60));
                Ref(*(int *)(_2+64));
                Ref(*(int *)(_2+68));
                Ref(*(int *)(_2+72));
                Ref(*(int *)(_2+76));
                Ref(*(int *)(_2+80));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52), 
                                    *(int *)(_2+56), 
                                    *(int *)(_2+60), 
                                    *(int *)(_2+64), 
                                    *(int *)(_2+68), 
                                    *(int *)(_2+72), 
                                    *(int *)(_2+76), 
                                    *(int *)(_2+80)
                                     );
                break;
        }
        DeRef(_abort_now_7025);
        _abort_now_7025 = _1;

        /** 		if not equal(abort_now, 0) then*/
        if (_abort_now_7025 == 0)
        _3710 = 1;
        else if (IS_ATOM_INT(_abort_now_7025) && IS_ATOM_INT(0))
        _3710 = 0;
        else
        _3710 = (compare(_abort_now_7025, 0) == 0);
        if (_3710 != 0)
        goto LB; // [255] 265
        _3710 = NOVALUE;

        /** 			return abort_now*/
        DeRefDS(_path_name_7020);
        DeRef(_your_function_7021);
        DeRef(_dir_source_7023);
        DeRef(_d_7024);
        DeRef(_orig_func_7026);
        DeRefDS(_user_data_7027);
        DeRef(_source_orig_func_7029);
        DeRef(_source_user_data_7030);
        return _abort_now_7025;
LB: 

        /** 		if eu:find('d', d[i][D_ATTRIBUTES]) then*/
        _2 = (int)SEQ_PTR(_d_7024);
        _3712 = (int)*(((s1_ptr)_2)->base + _i_7064);
        _2 = (int)SEQ_PTR(_3712);
        _3713 = (int)*(((s1_ptr)_2)->base + 2);
        _3712 = NOVALUE;
        _3714 = find_from(100, _3713, 1);
        _3713 = NOVALUE;
        if (_3714 == 0)
        {
            _3714 = NOVALUE;
            goto LC; // [282] 357
        }
        else{
            _3714 = NOVALUE;
        }

        /** 			if scan_subdirs then*/
        if (_scan_subdirs_7022 == 0)
        {
            goto LD; // [287] 356
        }
        else{
        }

        /** 				abort_now = walk_dir(path_name & SLASH & d[i][D_NAME],*/
        _2 = (int)SEQ_PTR(_d_7024);
        _3715 = (int)*(((s1_ptr)_2)->base + _i_7064);
        _2 = (int)SEQ_PTR(_3715);
        _3716 = (int)*(((s1_ptr)_2)->base + 1);
        _3715 = NOVALUE;
        {
            int concat_list[3];

            concat_list[0] = _3716;
            concat_list[1] = 92;
            concat_list[2] = _path_name_7020;
            Concat_N((object_ptr)&_3717, concat_list, 3);
        }
        _3716 = NOVALUE;
        Ref(_orig_func_7026);
        DeRef(_3718);
        _3718 = _orig_func_7026;
        DeRef(_3719);
        _3719 = _scan_subdirs_7022;
        Ref(_source_orig_func_7029);
        DeRef(_3720);
        _3720 = _source_orig_func_7029;
        _0 = _abort_now_7025;
        _abort_now_7025 = _9walk_dir(_3717, _3718, _3719, _3720);
        DeRef(_0);
        _3717 = NOVALUE;
        _3718 = NOVALUE;
        _3719 = NOVALUE;
        _3720 = NOVALUE;

        /** 				if not equal(abort_now, 0) and */
        if (_abort_now_7025 == 0)
        _3722 = 1;
        else if (IS_ATOM_INT(_abort_now_7025) && IS_ATOM_INT(0))
        _3722 = 0;
        else
        _3722 = (compare(_abort_now_7025, 0) == 0);
        _3723 = (_3722 == 0);
        _3722 = NOVALUE;
        if (_3723 == 0) {
            goto LE; // [333] 355
        }
        if (_abort_now_7025 == -1)
        _3725 = 1;
        else if (IS_ATOM_INT(_abort_now_7025) && IS_ATOM_INT(-1))
        _3725 = 0;
        else
        _3725 = (compare(_abort_now_7025, -1) == 0);
        _3726 = (_3725 == 0);
        _3725 = NOVALUE;
        if (_3726 == 0)
        {
            DeRef(_3726);
            _3726 = NOVALUE;
            goto LE; // [345] 355
        }
        else{
            DeRef(_3726);
            _3726 = NOVALUE;
        }

        /** 					return abort_now*/
        DeRefDS(_path_name_7020);
        DeRef(_your_function_7021);
        DeRef(_dir_source_7023);
        DeRef(_d_7024);
        DeRef(_orig_func_7026);
        DeRef(_user_data_7027);
        DeRef(_source_orig_func_7029);
        DeRef(_source_user_data_7030);
        DeRef(_3723);
        _3723 = NOVALUE;
        return _abort_now_7025;
LE: 
LD: 
LC: 

        /** 	end for*/
LA: 
        _i_7064 = _i_7064 + 1;
        goto L7; // [359] 204
L8: 
        ;
    }

    /** 	return 0*/
    DeRefDS(_path_name_7020);
    DeRef(_your_function_7021);
    DeRef(_dir_source_7023);
    DeRef(_d_7024);
    DeRef(_abort_now_7025);
    DeRef(_orig_func_7026);
    DeRef(_user_data_7027);
    DeRef(_source_orig_func_7029);
    DeRef(_source_user_data_7030);
    DeRef(_3723);
    _3723 = NOVALUE;
    return 0;
    ;
}


int _9create_directory(int _name_7097, int _mode_7098, int _mkparent_7100)
{
    int _pname_7101 = NOVALUE;
    int _ret_7102 = NOVALUE;
    int _pos_7103 = NOVALUE;
    int _3746 = NOVALUE;
    int _3743 = NOVALUE;
    int _3742 = NOVALUE;
    int _3741 = NOVALUE;
    int _3740 = NOVALUE;
    int _3737 = NOVALUE;
    int _3734 = NOVALUE;
    int _3733 = NOVALUE;
    int _3731 = NOVALUE;
    int _3730 = NOVALUE;
    int _3728 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_mode_7098)) {
        _1 = (long)(DBL_PTR(_mode_7098)->dbl);
        if (UNIQUE(DBL_PTR(_mode_7098)) && (DBL_PTR(_mode_7098)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mode_7098);
        _mode_7098 = _1;
    }
    if (!IS_ATOM_INT(_mkparent_7100)) {
        _1 = (long)(DBL_PTR(_mkparent_7100)->dbl);
        if (UNIQUE(DBL_PTR(_mkparent_7100)) && (DBL_PTR(_mkparent_7100)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mkparent_7100);
        _mkparent_7100 = _1;
    }

    /** 	if length(name) = 0 then*/
    if (IS_SEQUENCE(_name_7097)){
            _3728 = SEQ_PTR(_name_7097)->length;
    }
    else {
        _3728 = 1;
    }
    if (_3728 != 0)
    goto L1; // [12] 23

    /** 		return 0 -- failed*/
    DeRefDS(_name_7097);
    DeRef(_pname_7101);
    DeRef(_ret_7102);
    return 0;
L1: 

    /** 	if name[$] = SLASH then*/
    if (IS_SEQUENCE(_name_7097)){
            _3730 = SEQ_PTR(_name_7097)->length;
    }
    else {
        _3730 = 1;
    }
    _2 = (int)SEQ_PTR(_name_7097);
    _3731 = (int)*(((s1_ptr)_2)->base + _3730);
    if (binary_op_a(NOTEQ, _3731, 92)){
        _3731 = NOVALUE;
        goto L2; // [32] 51
    }
    _3731 = NOVALUE;

    /** 		name = name[1 .. $-1]*/
    if (IS_SEQUENCE(_name_7097)){
            _3733 = SEQ_PTR(_name_7097)->length;
    }
    else {
        _3733 = 1;
    }
    _3734 = _3733 - 1;
    _3733 = NOVALUE;
    rhs_slice_target = (object_ptr)&_name_7097;
    RHS_Slice(_name_7097, 1, _3734);
L2: 

    /** 	if mkparent != 0 then*/
    if (_mkparent_7100 == 0)
    goto L3; // [53] 101

    /** 		pos = search:rfind(SLASH, name)*/
    if (IS_SEQUENCE(_name_7097)){
            _3737 = SEQ_PTR(_name_7097)->length;
    }
    else {
        _3737 = 1;
    }
    RefDS(_name_7097);
    _pos_7103 = _7rfind(92, _name_7097, _3737);
    _3737 = NOVALUE;
    if (!IS_ATOM_INT(_pos_7103)) {
        _1 = (long)(DBL_PTR(_pos_7103)->dbl);
        if (UNIQUE(DBL_PTR(_pos_7103)) && (DBL_PTR(_pos_7103)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_7103);
        _pos_7103 = _1;
    }

    /** 		if pos != 0 then*/
    if (_pos_7103 == 0)
    goto L4; // [72] 100

    /** 			ret = create_directory(name[1.. pos-1], mode, mkparent)*/
    _3740 = _pos_7103 - 1;
    rhs_slice_target = (object_ptr)&_3741;
    RHS_Slice(_name_7097, 1, _3740);
    DeRef(_3742);
    _3742 = _mode_7098;
    DeRef(_3743);
    _3743 = _mkparent_7100;
    _0 = _ret_7102;
    _ret_7102 = _9create_directory(_3741, _3742, _3743);
    DeRef(_0);
    _3741 = NOVALUE;
    _3742 = NOVALUE;
    _3743 = NOVALUE;
L4: 
L3: 

    /** 	pname = machine:allocate_string(name)*/
    RefDS(_name_7097);
    _0 = _pname_7101;
    _pname_7101 = _12allocate_string(_name_7097, 0);
    DeRef(_0);

    /** 	ifdef UNIX then*/

    /** 		ret = c_func(xCreateDirectory, {pname, 0})*/
    Ref(_pname_7101);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _pname_7101;
    ((int *)_2)[2] = 0;
    _3746 = MAKE_SEQ(_1);
    DeRef(_ret_7102);
    _ret_7102 = call_c(1, _9xCreateDirectory_6925, _3746);
    DeRefDS(_3746);
    _3746 = NOVALUE;

    /** 		mode = mode -- get rid of not used warning*/
    _mode_7098 = _mode_7098;

    /** 	return ret*/
    DeRefDS(_name_7097);
    DeRef(_pname_7101);
    DeRef(_3734);
    _3734 = NOVALUE;
    DeRef(_3740);
    _3740 = NOVALUE;
    return _ret_7102;
    ;
}


int _9create_file(int _name_7130)
{
    int _fh_7131 = NOVALUE;
    int _ret_7133 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer fh = open(name, "wb")*/
    _fh_7131 = EOpen(_name_7130, _1183, 0);

    /** 	integer ret = (fh != -1)*/
    _ret_7133 = (_fh_7131 != -1);

    /** 	if ret then*/
    if (_ret_7133 == 0)
    {
        goto L1; // [18] 26
    }
    else{
    }

    /** 		close(fh)*/
    EClose(_fh_7131);
L1: 

    /** 	return ret*/
    DeRefDS(_name_7130);
    return _ret_7133;
    ;
}


int _9delete_file(int _name_7138)
{
    int _pfilename_7139 = NOVALUE;
    int _success_7141 = NOVALUE;
    int _3751 = NOVALUE;
    int _0, _1, _2;
    

    /** 	atom pfilename = machine:allocate_string(name)*/
    RefDS(_name_7138);
    _0 = _pfilename_7139;
    _pfilename_7139 = _12allocate_string(_name_7138, 0);
    DeRef(_0);

    /** 	integer success = c_func(xDeleteFile, {pfilename})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pfilename_7139);
    *((int *)(_2+4)) = _pfilename_7139;
    _3751 = MAKE_SEQ(_1);
    _success_7141 = call_c(1, _9xDeleteFile_6921, _3751);
    DeRefDS(_3751);
    _3751 = NOVALUE;
    if (!IS_ATOM_INT(_success_7141)) {
        _1 = (long)(DBL_PTR(_success_7141)->dbl);
        if (UNIQUE(DBL_PTR(_success_7141)) && (DBL_PTR(_success_7141)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_success_7141);
        _success_7141 = _1;
    }

    /** 	ifdef UNIX then*/

    /** 	machine:free(pfilename)*/
    Ref(_pfilename_7139);
    _12free(_pfilename_7139);

    /** 	return success*/
    DeRefDS(_name_7138);
    DeRef(_pfilename_7139);
    return _success_7141;
    ;
}


int _9curdir(int _drive_id_7146)
{
    int _lCurDir_7147 = NOVALUE;
    int _lOrigDir_7148 = NOVALUE;
    int _lDrive_7149 = NOVALUE;
    int _current_dir_inlined_current_dir_at_25_7154 = NOVALUE;
    int _chdir_inlined_chdir_at_55_7157 = NOVALUE;
    int _current_dir_inlined_current_dir_at_77_7160 = NOVALUE;
    int _chdir_inlined_chdir_at_109_7167 = NOVALUE;
    int _newdir_inlined_chdir_at_106_7166 = NOVALUE;
    int _3759 = NOVALUE;
    int _3758 = NOVALUE;
    int _3757 = NOVALUE;
    int _3755 = NOVALUE;
    int _3753 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_drive_id_7146)) {
        _1 = (long)(DBL_PTR(_drive_id_7146)->dbl);
        if (UNIQUE(DBL_PTR(_drive_id_7146)) && (DBL_PTR(_drive_id_7146)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_drive_id_7146);
        _drive_id_7146 = _1;
    }

    /** 	ifdef not LINUX then*/

    /** 	    sequence lOrigDir = ""*/
    RefDS(_5);
    DeRefi(_lOrigDir_7148);
    _lOrigDir_7148 = _5;

    /** 	    sequence lDrive*/

    /** 	    if t_alpha(drive_id) then*/
    _3753 = _5t_alpha(_drive_id_7146);
    if (_3753 == 0) {
        DeRef(_3753);
        _3753 = NOVALUE;
        goto L1; // [20] 75
    }
    else {
        if (!IS_ATOM_INT(_3753) && DBL_PTR(_3753)->dbl == 0.0){
            DeRef(_3753);
            _3753 = NOVALUE;
            goto L1; // [20] 75
        }
        DeRef(_3753);
        _3753 = NOVALUE;
    }
    DeRef(_3753);
    _3753 = NOVALUE;

    /** 		    lOrigDir =  current_dir()*/

    /** 	return machine_func(M_CURRENT_DIR, 0)*/
    DeRefDSi(_lOrigDir_7148);
    _lOrigDir_7148 = machine(23, 0);

    /** 		    lDrive = "  "*/
    RefDS(_112);
    DeRefi(_lDrive_7149);
    _lDrive_7149 = _112;

    /** 		    lDrive[1] = drive_id*/
    _2 = (int)SEQ_PTR(_lDrive_7149);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _lDrive_7149 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    *(int *)_2 = _drive_id_7146;

    /** 		    lDrive[2] = ':'*/
    _2 = (int)SEQ_PTR(_lDrive_7149);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _lDrive_7149 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    *(int *)_2 = 58;

    /** 		    if chdir(lDrive) = 0 then*/

    /** 	return machine_func(M_CHDIR, newdir)*/
    _chdir_inlined_chdir_at_55_7157 = machine(63, _lDrive_7149);
    if (_chdir_inlined_chdir_at_55_7157 != 0)
    goto L2; // [62] 74

    /** 		    	lOrigDir = ""*/
    RefDS(_5);
    DeRefi(_lOrigDir_7148);
    _lOrigDir_7148 = _5;
L2: 
L1: 

    /**     lCurDir = current_dir()*/

    /** 	return machine_func(M_CURRENT_DIR, 0)*/
    DeRefi(_lCurDir_7147);
    _lCurDir_7147 = machine(23, 0);

    /** 	ifdef not LINUX then*/

    /** 		if length(lOrigDir) > 0 then*/
    if (IS_SEQUENCE(_lOrigDir_7148)){
            _3755 = SEQ_PTR(_lOrigDir_7148)->length;
    }
    else {
        _3755 = 1;
    }
    if (_3755 <= 0)
    goto L3; // [95] 121

    /** 	    	chdir(lOrigDir[1..2])*/
    rhs_slice_target = (object_ptr)&_3757;
    RHS_Slice(_lOrigDir_7148, 1, 2);
    DeRefi(_newdir_inlined_chdir_at_106_7166);
    _newdir_inlined_chdir_at_106_7166 = _3757;
    _3757 = NOVALUE;

    /** 	return machine_func(M_CHDIR, newdir)*/
    _chdir_inlined_chdir_at_109_7167 = machine(63, _newdir_inlined_chdir_at_106_7166);
    DeRefi(_newdir_inlined_chdir_at_106_7166);
    _newdir_inlined_chdir_at_106_7166 = NOVALUE;
L3: 

    /** 	if (lCurDir[$] != SLASH) then*/
    if (IS_SEQUENCE(_lCurDir_7147)){
            _3758 = SEQ_PTR(_lCurDir_7147)->length;
    }
    else {
        _3758 = 1;
    }
    _2 = (int)SEQ_PTR(_lCurDir_7147);
    _3759 = (int)*(((s1_ptr)_2)->base + _3758);
    if (_3759 == 92)
    goto L4; // [130] 141

    /** 		lCurDir &= SLASH*/
    Append(&_lCurDir_7147, _lCurDir_7147, 92);
L4: 

    /** 	return lCurDir*/
    DeRefi(_lOrigDir_7148);
    DeRefi(_lDrive_7149);
    _3759 = NOVALUE;
    return _lCurDir_7147;
    ;
}


int _9init_curdir()
{
    int _0, _1, _2;
    

    /** 	return InitCurDir*/
    RefDS(_9InitCurDir_7173);
    return _9InitCurDir_7173;
    ;
}


int _9clear_directory(int _path_7179, int _recurse_7180)
{
    int _files_7181 = NOVALUE;
    int _ret_7182 = NOVALUE;
    int _dir_inlined_dir_at_89_7203 = NOVALUE;
    int _cnt_7228 = NOVALUE;
    int _3803 = NOVALUE;
    int _3802 = NOVALUE;
    int _3801 = NOVALUE;
    int _3800 = NOVALUE;
    int _3796 = NOVALUE;
    int _3795 = NOVALUE;
    int _3794 = NOVALUE;
    int _3793 = NOVALUE;
    int _3792 = NOVALUE;
    int _3791 = NOVALUE;
    int _3790 = NOVALUE;
    int _3789 = NOVALUE;
    int _3786 = NOVALUE;
    int _3785 = NOVALUE;
    int _3784 = NOVALUE;
    int _3782 = NOVALUE;
    int _3781 = NOVALUE;
    int _3780 = NOVALUE;
    int _3778 = NOVALUE;
    int _3777 = NOVALUE;
    int _3775 = NOVALUE;
    int _3773 = NOVALUE;
    int _3771 = NOVALUE;
    int _3769 = NOVALUE;
    int _3768 = NOVALUE;
    int _3766 = NOVALUE;
    int _3765 = NOVALUE;
    int _3763 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_recurse_7180)) {
        _1 = (long)(DBL_PTR(_recurse_7180)->dbl);
        if (UNIQUE(DBL_PTR(_recurse_7180)) && (DBL_PTR(_recurse_7180)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_recurse_7180);
        _recurse_7180 = _1;
    }

    /** 	if length(path) > 0 then*/
    if (IS_SEQUENCE(_path_7179)){
            _3763 = SEQ_PTR(_path_7179)->length;
    }
    else {
        _3763 = 1;
    }
    if (_3763 <= 0)
    goto L1; // [10] 43

    /** 		if path[$] = SLASH then*/
    if (IS_SEQUENCE(_path_7179)){
            _3765 = SEQ_PTR(_path_7179)->length;
    }
    else {
        _3765 = 1;
    }
    _2 = (int)SEQ_PTR(_path_7179);
    _3766 = (int)*(((s1_ptr)_2)->base + _3765);
    if (binary_op_a(NOTEQ, _3766, 92)){
        _3766 = NOVALUE;
        goto L2; // [23] 42
    }
    _3766 = NOVALUE;

    /** 			path = path[1 .. $-1]*/
    if (IS_SEQUENCE(_path_7179)){
            _3768 = SEQ_PTR(_path_7179)->length;
    }
    else {
        _3768 = 1;
    }
    _3769 = _3768 - 1;
    _3768 = NOVALUE;
    rhs_slice_target = (object_ptr)&_path_7179;
    RHS_Slice(_path_7179, 1, _3769);
L2: 
L1: 

    /** 	if length(path) = 0 then*/
    if (IS_SEQUENCE(_path_7179)){
            _3771 = SEQ_PTR(_path_7179)->length;
    }
    else {
        _3771 = 1;
    }
    if (_3771 != 0)
    goto L3; // [48] 59

    /** 		return 0 -- Nothing specified to clear. Not safe to assume anything.*/
    DeRefDS(_path_7179);
    DeRef(_files_7181);
    DeRef(_3769);
    _3769 = NOVALUE;
    return 0;
L3: 

    /** 	ifdef WINDOWS then*/

    /** 		if length(path) = 2 then*/
    if (IS_SEQUENCE(_path_7179)){
            _3773 = SEQ_PTR(_path_7179)->length;
    }
    else {
        _3773 = 1;
    }
    if (_3773 != 2)
    goto L4; // [66] 88

    /** 			if path[2] = ':' then*/
    _2 = (int)SEQ_PTR(_path_7179);
    _3775 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _3775, 58)){
        _3775 = NOVALUE;
        goto L5; // [76] 87
    }
    _3775 = NOVALUE;

    /** 				return 0 -- nothing specified to delete*/
    DeRefDS(_path_7179);
    DeRef(_files_7181);
    DeRef(_3769);
    _3769 = NOVALUE;
    return 0;
L5: 
L4: 

    /** 	files = dir(path)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_files_7181);
    _files_7181 = machine(22, _path_7179);

    /** 	if atom(files) then*/
    _3777 = IS_ATOM(_files_7181);
    if (_3777 == 0)
    {
        _3777 = NOVALUE;
        goto L6; // [104] 114
    }
    else{
        _3777 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_path_7179);
    DeRef(_files_7181);
    DeRef(_3769);
    _3769 = NOVALUE;
    return 0;
L6: 

    /** 	ifdef WINDOWS then*/

    /** 		if length( files ) < 3 then*/
    if (IS_SEQUENCE(_files_7181)){
            _3778 = SEQ_PTR(_files_7181)->length;
    }
    else {
        _3778 = 1;
    }
    if (_3778 >= 3)
    goto L7; // [121] 132

    /** 			return 0 -- Supplied name was not a directory*/
    DeRefDS(_path_7179);
    DeRef(_files_7181);
    DeRef(_3769);
    _3769 = NOVALUE;
    return 0;
L7: 

    /** 		if not equal(files[1][D_NAME], ".") then*/
    _2 = (int)SEQ_PTR(_files_7181);
    _3780 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3780);
    _3781 = (int)*(((s1_ptr)_2)->base + 1);
    _3780 = NOVALUE;
    if (_3781 == _3674)
    _3782 = 1;
    else if (IS_ATOM_INT(_3781) && IS_ATOM_INT(_3674))
    _3782 = 0;
    else
    _3782 = (compare(_3781, _3674) == 0);
    _3781 = NOVALUE;
    if (_3782 != 0)
    goto L8; // [148] 158
    _3782 = NOVALUE;

    /** 			return 0 -- Supplied name was not a directory*/
    DeRefDS(_path_7179);
    DeRef(_files_7181);
    DeRef(_3769);
    _3769 = NOVALUE;
    return 0;
L8: 

    /** 		if not eu:find('d', files[1][D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_files_7181);
    _3784 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3784);
    _3785 = (int)*(((s1_ptr)_2)->base + 2);
    _3784 = NOVALUE;
    _3786 = find_from(100, _3785, 1);
    _3785 = NOVALUE;
    if (_3786 != 0)
    goto L9; // [175] 185
    _3786 = NOVALUE;

    /** 			return 0 -- Supplied name was not a directory*/
    DeRefDS(_path_7179);
    DeRef(_files_7181);
    DeRef(_3769);
    _3769 = NOVALUE;
    return 0;
L9: 

    /** 	ret = 1*/
    _ret_7182 = 1;

    /** 	path &= SLASH*/
    Append(&_path_7179, _path_7179, 92);

    /** 	ifdef WINDOWS then*/

    /** 		for i = 3 to length(files) do*/
    if (IS_SEQUENCE(_files_7181)){
            _3789 = SEQ_PTR(_files_7181)->length;
    }
    else {
        _3789 = 1;
    }
    {
        int _i_7221;
        _i_7221 = 3;
LA: 
        if (_i_7221 > _3789){
            goto LB; // [203] 338
        }

        /** 			if eu:find('d', files[i][D_ATTRIBUTES]) then*/
        _2 = (int)SEQ_PTR(_files_7181);
        _3790 = (int)*(((s1_ptr)_2)->base + _i_7221);
        _2 = (int)SEQ_PTR(_3790);
        _3791 = (int)*(((s1_ptr)_2)->base + 2);
        _3790 = NOVALUE;
        _3792 = find_from(100, _3791, 1);
        _3791 = NOVALUE;
        if (_3792 == 0)
        {
            _3792 = NOVALUE;
            goto LC; // [227] 293
        }
        else{
            _3792 = NOVALUE;
        }

        /** 				if recurse then*/
        if (_recurse_7180 == 0)
        {
            goto LD; // [232] 333
        }
        else{
        }

        /** 					integer cnt = clear_directory(path & files[i][D_NAME], recurse)*/
        _2 = (int)SEQ_PTR(_files_7181);
        _3793 = (int)*(((s1_ptr)_2)->base + _i_7221);
        _2 = (int)SEQ_PTR(_3793);
        _3794 = (int)*(((s1_ptr)_2)->base + 1);
        _3793 = NOVALUE;
        if (IS_SEQUENCE(_path_7179) && IS_ATOM(_3794)) {
            Ref(_3794);
            Append(&_3795, _path_7179, _3794);
        }
        else if (IS_ATOM(_path_7179) && IS_SEQUENCE(_3794)) {
        }
        else {
            Concat((object_ptr)&_3795, _path_7179, _3794);
        }
        _3794 = NOVALUE;
        DeRef(_3796);
        _3796 = _recurse_7180;
        _cnt_7228 = _9clear_directory(_3795, _3796);
        _3795 = NOVALUE;
        _3796 = NOVALUE;
        if (!IS_ATOM_INT(_cnt_7228)) {
            _1 = (long)(DBL_PTR(_cnt_7228)->dbl);
            if (UNIQUE(DBL_PTR(_cnt_7228)) && (DBL_PTR(_cnt_7228)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_cnt_7228);
            _cnt_7228 = _1;
        }

        /** 					if cnt = 0 then*/
        if (_cnt_7228 != 0)
        goto LE; // [263] 274

        /** 						return 0*/
        DeRefDS(_path_7179);
        DeRef(_files_7181);
        DeRef(_3769);
        _3769 = NOVALUE;
        return 0;
LE: 

        /** 					ret += cnt*/
        _ret_7182 = _ret_7182 + _cnt_7228;
        goto LF; // [282] 331

        /** 					continue*/
        goto LD; // [287] 333
        goto LF; // [290] 331
LC: 

        /** 				if delete_file(path & files[i][D_NAME]) = 0 then*/
        _2 = (int)SEQ_PTR(_files_7181);
        _3800 = (int)*(((s1_ptr)_2)->base + _i_7221);
        _2 = (int)SEQ_PTR(_3800);
        _3801 = (int)*(((s1_ptr)_2)->base + 1);
        _3800 = NOVALUE;
        if (IS_SEQUENCE(_path_7179) && IS_ATOM(_3801)) {
            Ref(_3801);
            Append(&_3802, _path_7179, _3801);
        }
        else if (IS_ATOM(_path_7179) && IS_SEQUENCE(_3801)) {
        }
        else {
            Concat((object_ptr)&_3802, _path_7179, _3801);
        }
        _3801 = NOVALUE;
        _3803 = _9delete_file(_3802);
        _3802 = NOVALUE;
        if (binary_op_a(NOTEQ, _3803, 0)){
            DeRef(_3803);
            _3803 = NOVALUE;
            goto L10; // [313] 324
        }
        DeRef(_3803);
        _3803 = NOVALUE;

        /** 					return 0*/
        DeRefDS(_path_7179);
        DeRef(_files_7181);
        DeRef(_3769);
        _3769 = NOVALUE;
        return 0;
L10: 

        /** 				ret += 1*/
        _ret_7182 = _ret_7182 + 1;
LF: 

        /** 		end for*/
LD: 
        _i_7221 = _i_7221 + 1;
        goto LA; // [333] 210
LB: 
        ;
    }

    /** 	return ret*/
    DeRefDS(_path_7179);
    DeRef(_files_7181);
    DeRef(_3769);
    _3769 = NOVALUE;
    return _ret_7182;
    ;
}


int _9remove_directory(int _dir_name_7248, int _force_7249)
{
    int _pname_7250 = NOVALUE;
    int _ret_7251 = NOVALUE;
    int _files_7252 = NOVALUE;
    int _D_NAME_7253 = NOVALUE;
    int _D_ATTRIBUTES_7254 = NOVALUE;
    int _dir_inlined_dir_at_97_7275 = NOVALUE;
    int _3850 = NOVALUE;
    int _3846 = NOVALUE;
    int _3845 = NOVALUE;
    int _3844 = NOVALUE;
    int _3842 = NOVALUE;
    int _3841 = NOVALUE;
    int _3840 = NOVALUE;
    int _3839 = NOVALUE;
    int _3838 = NOVALUE;
    int _3837 = NOVALUE;
    int _3836 = NOVALUE;
    int _3835 = NOVALUE;
    int _3831 = NOVALUE;
    int _3829 = NOVALUE;
    int _3828 = NOVALUE;
    int _3827 = NOVALUE;
    int _3825 = NOVALUE;
    int _3824 = NOVALUE;
    int _3823 = NOVALUE;
    int _3821 = NOVALUE;
    int _3820 = NOVALUE;
    int _3818 = NOVALUE;
    int _3816 = NOVALUE;
    int _3814 = NOVALUE;
    int _3812 = NOVALUE;
    int _3811 = NOVALUE;
    int _3809 = NOVALUE;
    int _3808 = NOVALUE;
    int _3806 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_force_7249)) {
        _1 = (long)(DBL_PTR(_force_7249)->dbl);
        if (UNIQUE(DBL_PTR(_force_7249)) && (DBL_PTR(_force_7249)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_force_7249);
        _force_7249 = _1;
    }

    /** 	integer D_NAME = 1, D_ATTRIBUTES = 2*/
    _D_NAME_7253 = 1;
    _D_ATTRIBUTES_7254 = 2;

    /**  	if length(dir_name) > 0 then*/
    if (IS_SEQUENCE(_dir_name_7248)){
            _3806 = SEQ_PTR(_dir_name_7248)->length;
    }
    else {
        _3806 = 1;
    }
    if (_3806 <= 0)
    goto L1; // [18] 51

    /** 		if dir_name[$] = SLASH then*/
    if (IS_SEQUENCE(_dir_name_7248)){
            _3808 = SEQ_PTR(_dir_name_7248)->length;
    }
    else {
        _3808 = 1;
    }
    _2 = (int)SEQ_PTR(_dir_name_7248);
    _3809 = (int)*(((s1_ptr)_2)->base + _3808);
    if (binary_op_a(NOTEQ, _3809, 92)){
        _3809 = NOVALUE;
        goto L2; // [31] 50
    }
    _3809 = NOVALUE;

    /** 			dir_name = dir_name[1 .. $-1]*/
    if (IS_SEQUENCE(_dir_name_7248)){
            _3811 = SEQ_PTR(_dir_name_7248)->length;
    }
    else {
        _3811 = 1;
    }
    _3812 = _3811 - 1;
    _3811 = NOVALUE;
    rhs_slice_target = (object_ptr)&_dir_name_7248;
    RHS_Slice(_dir_name_7248, 1, _3812);
L2: 
L1: 

    /** 	if length(dir_name) = 0 then*/
    if (IS_SEQUENCE(_dir_name_7248)){
            _3814 = SEQ_PTR(_dir_name_7248)->length;
    }
    else {
        _3814 = 1;
    }
    if (_3814 != 0)
    goto L3; // [56] 67

    /** 		return 0	-- nothing specified to delete.*/
    DeRefDS(_dir_name_7248);
    DeRef(_pname_7250);
    DeRef(_ret_7251);
    DeRef(_files_7252);
    DeRef(_3812);
    _3812 = NOVALUE;
    return 0;
L3: 

    /** 	ifdef WINDOWS then*/

    /** 		if length(dir_name) = 2 then*/
    if (IS_SEQUENCE(_dir_name_7248)){
            _3816 = SEQ_PTR(_dir_name_7248)->length;
    }
    else {
        _3816 = 1;
    }
    if (_3816 != 2)
    goto L4; // [74] 96

    /** 			if dir_name[2] = ':' then*/
    _2 = (int)SEQ_PTR(_dir_name_7248);
    _3818 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _3818, 58)){
        _3818 = NOVALUE;
        goto L5; // [84] 95
    }
    _3818 = NOVALUE;

    /** 				return 0 -- nothing specified to delete*/
    DeRefDS(_dir_name_7248);
    DeRef(_pname_7250);
    DeRef(_ret_7251);
    DeRef(_files_7252);
    DeRef(_3812);
    _3812 = NOVALUE;
    return 0;
L5: 
L4: 

    /** 	files = dir(dir_name)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_files_7252);
    _files_7252 = machine(22, _dir_name_7248);

    /** 	if atom(files) then*/
    _3820 = IS_ATOM(_files_7252);
    if (_3820 == 0)
    {
        _3820 = NOVALUE;
        goto L6; // [112] 122
    }
    else{
        _3820 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_dir_name_7248);
    DeRef(_pname_7250);
    DeRef(_ret_7251);
    DeRef(_files_7252);
    DeRef(_3812);
    _3812 = NOVALUE;
    return 0;
L6: 

    /** 	if length( files ) < 2 then*/
    if (IS_SEQUENCE(_files_7252)){
            _3821 = SEQ_PTR(_files_7252)->length;
    }
    else {
        _3821 = 1;
    }
    if (_3821 >= 2)
    goto L7; // [127] 138

    /** 		return 0	-- Supplied dir_name was not a directory*/
    DeRefDS(_dir_name_7248);
    DeRef(_pname_7250);
    DeRef(_ret_7251);
    DeRef(_files_7252);
    DeRef(_3812);
    _3812 = NOVALUE;
    return 0;
L7: 

    /** 	ifdef WINDOWS then*/

    /** 		if not equal(files[1][D_NAME], ".") then*/
    _2 = (int)SEQ_PTR(_files_7252);
    _3823 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3823);
    _3824 = (int)*(((s1_ptr)_2)->base + _D_NAME_7253);
    _3823 = NOVALUE;
    if (_3824 == _3674)
    _3825 = 1;
    else if (IS_ATOM_INT(_3824) && IS_ATOM_INT(_3674))
    _3825 = 0;
    else
    _3825 = (compare(_3824, _3674) == 0);
    _3824 = NOVALUE;
    if (_3825 != 0)
    goto L8; // [154] 164
    _3825 = NOVALUE;

    /** 			return 0 -- Supplied name was not a directory*/
    DeRefDS(_dir_name_7248);
    DeRef(_pname_7250);
    DeRef(_ret_7251);
    DeRef(_files_7252);
    DeRef(_3812);
    _3812 = NOVALUE;
    return 0;
L8: 

    /** 		if not eu:find('d', files[1][D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_files_7252);
    _3827 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3827);
    _3828 = (int)*(((s1_ptr)_2)->base + _D_ATTRIBUTES_7254);
    _3827 = NOVALUE;
    _3829 = find_from(100, _3828, 1);
    _3828 = NOVALUE;
    if (_3829 != 0)
    goto L9; // [179] 189
    _3829 = NOVALUE;

    /** 			return 0 -- Supplied name was not a directory*/
    DeRefDS(_dir_name_7248);
    DeRef(_pname_7250);
    DeRef(_ret_7251);
    DeRef(_files_7252);
    DeRef(_3812);
    _3812 = NOVALUE;
    return 0;
L9: 

    /** 		if length(files) > 2 then*/
    if (IS_SEQUENCE(_files_7252)){
            _3831 = SEQ_PTR(_files_7252)->length;
    }
    else {
        _3831 = 1;
    }
    if (_3831 <= 2)
    goto LA; // [194] 211

    /** 			if not force then*/
    if (_force_7249 != 0)
    goto LB; // [200] 210

    /** 				return 0 -- Directory is not already emptied.*/
    DeRefDS(_dir_name_7248);
    DeRef(_pname_7250);
    DeRef(_ret_7251);
    DeRef(_files_7252);
    DeRef(_3812);
    _3812 = NOVALUE;
    return 0;
LB: 
LA: 

    /** 	dir_name &= SLASH*/
    Append(&_dir_name_7248, _dir_name_7248, 92);

    /** 	ifdef WINDOWS then*/

    /** 		for i = 3 to length(files) do*/
    if (IS_SEQUENCE(_files_7252)){
            _3835 = SEQ_PTR(_files_7252)->length;
    }
    else {
        _3835 = 1;
    }
    {
        int _i_7298;
        _i_7298 = 3;
LC: 
        if (_i_7298 > _3835){
            goto LD; // [224] 316
        }

        /** 			if eu:find('d', files[i][D_ATTRIBUTES]) then*/
        _2 = (int)SEQ_PTR(_files_7252);
        _3836 = (int)*(((s1_ptr)_2)->base + _i_7298);
        _2 = (int)SEQ_PTR(_3836);
        _3837 = (int)*(((s1_ptr)_2)->base + _D_ATTRIBUTES_7254);
        _3836 = NOVALUE;
        _3838 = find_from(100, _3837, 1);
        _3837 = NOVALUE;
        if (_3838 == 0)
        {
            _3838 = NOVALUE;
            goto LE; // [246] 276
        }
        else{
            _3838 = NOVALUE;
        }

        /** 				ret = remove_directory(dir_name & files[i][D_NAME] & SLASH, force)*/
        _2 = (int)SEQ_PTR(_files_7252);
        _3839 = (int)*(((s1_ptr)_2)->base + _i_7298);
        _2 = (int)SEQ_PTR(_3839);
        _3840 = (int)*(((s1_ptr)_2)->base + _D_NAME_7253);
        _3839 = NOVALUE;
        {
            int concat_list[3];

            concat_list[0] = 92;
            concat_list[1] = _3840;
            concat_list[2] = _dir_name_7248;
            Concat_N((object_ptr)&_3841, concat_list, 3);
        }
        _3840 = NOVALUE;
        DeRef(_3842);
        _3842 = _force_7249;
        _0 = _ret_7251;
        _ret_7251 = _9remove_directory(_3841, _3842);
        DeRef(_0);
        _3841 = NOVALUE;
        _3842 = NOVALUE;
        goto LF; // [273] 295
LE: 

        /** 				ret = delete_file(dir_name & files[i][D_NAME])*/
        _2 = (int)SEQ_PTR(_files_7252);
        _3844 = (int)*(((s1_ptr)_2)->base + _i_7298);
        _2 = (int)SEQ_PTR(_3844);
        _3845 = (int)*(((s1_ptr)_2)->base + _D_NAME_7253);
        _3844 = NOVALUE;
        if (IS_SEQUENCE(_dir_name_7248) && IS_ATOM(_3845)) {
            Ref(_3845);
            Append(&_3846, _dir_name_7248, _3845);
        }
        else if (IS_ATOM(_dir_name_7248) && IS_SEQUENCE(_3845)) {
        }
        else {
            Concat((object_ptr)&_3846, _dir_name_7248, _3845);
        }
        _3845 = NOVALUE;
        _0 = _ret_7251;
        _ret_7251 = _9delete_file(_3846);
        DeRef(_0);
        _3846 = NOVALUE;
LF: 

        /** 			if not ret then*/
        if (IS_ATOM_INT(_ret_7251)) {
            if (_ret_7251 != 0){
                goto L10; // [299] 309
            }
        }
        else {
            if (DBL_PTR(_ret_7251)->dbl != 0.0){
                goto L10; // [299] 309
            }
        }

        /** 				return 0*/
        DeRefDS(_dir_name_7248);
        DeRef(_pname_7250);
        DeRef(_ret_7251);
        DeRef(_files_7252);
        DeRef(_3812);
        _3812 = NOVALUE;
        return 0;
L10: 

        /** 		end for*/
        _i_7298 = _i_7298 + 1;
        goto LC; // [311] 231
LD: 
        ;
    }

    /** 	pname = machine:allocate_string(dir_name)*/
    RefDS(_dir_name_7248);
    _0 = _pname_7250;
    _pname_7250 = _12allocate_string(_dir_name_7248, 0);
    DeRef(_0);

    /** 	ret = c_func(xRemoveDirectory, {pname})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pname_7250);
    *((int *)(_2+4)) = _pname_7250;
    _3850 = MAKE_SEQ(_1);
    DeRef(_ret_7251);
    _ret_7251 = call_c(1, _9xRemoveDirectory_6932, _3850);
    DeRefDS(_3850);
    _3850 = NOVALUE;

    /** 	ifdef UNIX then*/

    /** 	machine:free(pname)*/
    Ref(_pname_7250);
    _12free(_pname_7250);

    /** 	return ret*/
    DeRefDS(_dir_name_7248);
    DeRef(_pname_7250);
    DeRef(_files_7252);
    DeRef(_3812);
    _3812 = NOVALUE;
    return _ret_7251;
    ;
}


int _9pathinfo(int _path_7330, int _std_slash_7331)
{
    int _slash_7332 = NOVALUE;
    int _period_7333 = NOVALUE;
    int _ch_7334 = NOVALUE;
    int _dir_name_7335 = NOVALUE;
    int _file_name_7336 = NOVALUE;
    int _file_ext_7337 = NOVALUE;
    int _file_full_7338 = NOVALUE;
    int _drive_id_7339 = NOVALUE;
    int _from_slash_7379 = NOVALUE;
    int _3888 = NOVALUE;
    int _3881 = NOVALUE;
    int _3880 = NOVALUE;
    int _3877 = NOVALUE;
    int _3876 = NOVALUE;
    int _3874 = NOVALUE;
    int _3873 = NOVALUE;
    int _3870 = NOVALUE;
    int _3869 = NOVALUE;
    int _3867 = NOVALUE;
    int _3863 = NOVALUE;
    int _3861 = NOVALUE;
    int _3860 = NOVALUE;
    int _3859 = NOVALUE;
    int _3858 = NOVALUE;
    int _3856 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_std_slash_7331)) {
        _1 = (long)(DBL_PTR(_std_slash_7331)->dbl);
        if (UNIQUE(DBL_PTR(_std_slash_7331)) && (DBL_PTR(_std_slash_7331)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_std_slash_7331);
        _std_slash_7331 = _1;
    }

    /** 	dir_name  = ""*/
    RefDS(_5);
    DeRef(_dir_name_7335);
    _dir_name_7335 = _5;

    /** 	file_name = ""*/
    RefDS(_5);
    DeRef(_file_name_7336);
    _file_name_7336 = _5;

    /** 	file_ext  = ""*/
    RefDS(_5);
    DeRef(_file_ext_7337);
    _file_ext_7337 = _5;

    /** 	file_full = ""*/
    RefDS(_5);
    DeRef(_file_full_7338);
    _file_full_7338 = _5;

    /** 	drive_id  = ""*/
    RefDS(_5);
    DeRef(_drive_id_7339);
    _drive_id_7339 = _5;

    /** 	slash = 0*/
    _slash_7332 = 0;

    /** 	period = 0*/
    _period_7333 = 0;

    /** 	for i = length(path) to 1 by -1 do*/
    if (IS_SEQUENCE(_path_7330)){
            _3856 = SEQ_PTR(_path_7330)->length;
    }
    else {
        _3856 = 1;
    }
    {
        int _i_7341;
        _i_7341 = _3856;
L1: 
        if (_i_7341 < 1){
            goto L2; // [55] 122
        }

        /** 		ch = path[i]*/
        _2 = (int)SEQ_PTR(_path_7330);
        _ch_7334 = (int)*(((s1_ptr)_2)->base + _i_7341);
        if (!IS_ATOM_INT(_ch_7334))
        _ch_7334 = (long)DBL_PTR(_ch_7334)->dbl;

        /** 		if period = 0 and ch = '.' then*/
        _3858 = (_period_7333 == 0);
        if (_3858 == 0) {
            goto L3; // [74] 94
        }
        _3860 = (_ch_7334 == 46);
        if (_3860 == 0)
        {
            DeRef(_3860);
            _3860 = NOVALUE;
            goto L3; // [83] 94
        }
        else{
            DeRef(_3860);
            _3860 = NOVALUE;
        }

        /** 			period = i*/
        _period_7333 = _i_7341;
        goto L4; // [91] 115
L3: 

        /** 		elsif eu:find(ch, SLASHES) then*/
        _3861 = find_from(_ch_7334, _9SLASHES_6949, 1);
        if (_3861 == 0)
        {
            _3861 = NOVALUE;
            goto L5; // [101] 114
        }
        else{
            _3861 = NOVALUE;
        }

        /** 			slash = i*/
        _slash_7332 = _i_7341;

        /** 			exit*/
        goto L2; // [111] 122
L5: 
L4: 

        /** 	end for*/
        _i_7341 = _i_7341 + -1;
        goto L1; // [117] 62
L2: 
        ;
    }

    /** 	if slash > 0 then*/
    if (_slash_7332 <= 0)
    goto L6; // [124] 181

    /** 		dir_name = path[1..slash-1]*/
    _3863 = _slash_7332 - 1;
    rhs_slice_target = (object_ptr)&_dir_name_7335;
    RHS_Slice(_path_7330, 1, _3863);

    /** 		ifdef not UNIX then*/

    /** 			ch = eu:find(':', dir_name)*/
    _ch_7334 = find_from(58, _dir_name_7335, 1);

    /** 			if ch != 0 then*/
    if (_ch_7334 == 0)
    goto L7; // [150] 180

    /** 				drive_id = dir_name[1..ch-1]*/
    _3867 = _ch_7334 - 1;
    rhs_slice_target = (object_ptr)&_drive_id_7339;
    RHS_Slice(_dir_name_7335, 1, _3867);

    /** 				dir_name = dir_name[ch+1..$]*/
    _3869 = _ch_7334 + 1;
    if (IS_SEQUENCE(_dir_name_7335)){
            _3870 = SEQ_PTR(_dir_name_7335)->length;
    }
    else {
        _3870 = 1;
    }
    rhs_slice_target = (object_ptr)&_dir_name_7335;
    RHS_Slice(_dir_name_7335, _3869, _3870);
L7: 
L6: 

    /** 	if period > 0 then*/
    if (_period_7333 <= 0)
    goto L8; // [183] 227

    /** 		file_name = path[slash+1..period-1]*/
    _3873 = _slash_7332 + 1;
    if (_3873 > MAXINT){
        _3873 = NewDouble((double)_3873);
    }
    _3874 = _period_7333 - 1;
    rhs_slice_target = (object_ptr)&_file_name_7336;
    RHS_Slice(_path_7330, _3873, _3874);

    /** 		file_ext = path[period+1..$]*/
    _3876 = _period_7333 + 1;
    if (_3876 > MAXINT){
        _3876 = NewDouble((double)_3876);
    }
    if (IS_SEQUENCE(_path_7330)){
            _3877 = SEQ_PTR(_path_7330)->length;
    }
    else {
        _3877 = 1;
    }
    rhs_slice_target = (object_ptr)&_file_ext_7337;
    RHS_Slice(_path_7330, _3876, _3877);

    /** 		file_full = file_name & '.' & file_ext*/
    {
        int concat_list[3];

        concat_list[0] = _file_ext_7337;
        concat_list[1] = 46;
        concat_list[2] = _file_name_7336;
        Concat_N((object_ptr)&_file_full_7338, concat_list, 3);
    }
    goto L9; // [224] 249
L8: 

    /** 		file_name = path[slash+1..$]*/
    _3880 = _slash_7332 + 1;
    if (_3880 > MAXINT){
        _3880 = NewDouble((double)_3880);
    }
    if (IS_SEQUENCE(_path_7330)){
            _3881 = SEQ_PTR(_path_7330)->length;
    }
    else {
        _3881 = 1;
    }
    rhs_slice_target = (object_ptr)&_file_name_7336;
    RHS_Slice(_path_7330, _3880, _3881);

    /** 		file_full = file_name*/
    RefDS(_file_name_7336);
    DeRef(_file_full_7338);
    _file_full_7338 = _file_name_7336;
L9: 

    /** 	if std_slash != 0 then*/
    if (_std_slash_7331 == 0)
    goto LA; // [251] 317

    /** 		if std_slash < 0 then*/
    if (_std_slash_7331 >= 0)
    goto LB; // [257] 293

    /** 			std_slash = SLASH*/
    _std_slash_7331 = 92;

    /** 			ifdef UNIX then*/

    /** 			sequence from_slash = "/"*/
    RefDS(_3651);
    DeRefi(_from_slash_7379);
    _from_slash_7379 = _3651;

    /** 			dir_name = search:match_replace(from_slash, dir_name, std_slash)*/
    RefDS(_from_slash_7379);
    RefDS(_dir_name_7335);
    _0 = _dir_name_7335;
    _dir_name_7335 = _7match_replace(_from_slash_7379, _dir_name_7335, 92, 0);
    DeRefDS(_0);
    DeRefDSi(_from_slash_7379);
    _from_slash_7379 = NOVALUE;
    goto LC; // [290] 316
LB: 

    /** 			dir_name = search:match_replace("\\", dir_name, std_slash)*/
    RefDS(_731);
    RefDS(_dir_name_7335);
    _0 = _dir_name_7335;
    _dir_name_7335 = _7match_replace(_731, _dir_name_7335, _std_slash_7331, 0);
    DeRefDS(_0);

    /** 			dir_name = search:match_replace("/", dir_name, std_slash)*/
    RefDS(_3651);
    RefDS(_dir_name_7335);
    _0 = _dir_name_7335;
    _dir_name_7335 = _7match_replace(_3651, _dir_name_7335, _std_slash_7331, 0);
    DeRefDS(_0);
LC: 
LA: 

    /** 	return {dir_name, file_full, file_name, file_ext, drive_id}*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_dir_name_7335);
    *((int *)(_2+4)) = _dir_name_7335;
    RefDS(_file_full_7338);
    *((int *)(_2+8)) = _file_full_7338;
    RefDS(_file_name_7336);
    *((int *)(_2+12)) = _file_name_7336;
    RefDS(_file_ext_7337);
    *((int *)(_2+16)) = _file_ext_7337;
    RefDS(_drive_id_7339);
    *((int *)(_2+20)) = _drive_id_7339;
    _3888 = MAKE_SEQ(_1);
    DeRefDS(_path_7330);
    DeRefDS(_dir_name_7335);
    DeRefDS(_file_name_7336);
    DeRefDS(_file_ext_7337);
    DeRefDS(_file_full_7338);
    DeRefDS(_drive_id_7339);
    DeRef(_3858);
    _3858 = NOVALUE;
    DeRef(_3863);
    _3863 = NOVALUE;
    DeRef(_3867);
    _3867 = NOVALUE;
    DeRef(_3869);
    _3869 = NOVALUE;
    DeRef(_3873);
    _3873 = NOVALUE;
    DeRef(_3874);
    _3874 = NOVALUE;
    DeRef(_3876);
    _3876 = NOVALUE;
    DeRef(_3880);
    _3880 = NOVALUE;
    return _3888;
    ;
}


int _9dirname(int _path_7387, int _pcd_7388)
{
    int _data_7389 = NOVALUE;
    int _3893 = NOVALUE;
    int _3891 = NOVALUE;
    int _3890 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_pcd_7388)) {
        _1 = (long)(DBL_PTR(_pcd_7388)->dbl);
        if (UNIQUE(DBL_PTR(_pcd_7388)) && (DBL_PTR(_pcd_7388)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pcd_7388);
        _pcd_7388 = _1;
    }

    /** 	data = pathinfo(path)*/
    RefDS(_path_7387);
    _0 = _data_7389;
    _data_7389 = _9pathinfo(_path_7387, 0);
    DeRef(_0);

    /** 	if pcd then*/
    if (_pcd_7388 == 0)
    {
        goto L1; // [16] 40
    }
    else{
    }

    /** 		if length(data[1]) = 0 then*/
    _2 = (int)SEQ_PTR(_data_7389);
    _3890 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_3890)){
            _3891 = SEQ_PTR(_3890)->length;
    }
    else {
        _3891 = 1;
    }
    _3890 = NOVALUE;
    if (_3891 != 0)
    goto L2; // [28] 39

    /** 			return "."*/
    RefDS(_3674);
    DeRefDS(_path_7387);
    DeRefDS(_data_7389);
    _3890 = NOVALUE;
    return _3674;
L2: 
L1: 

    /** 	return data[1]*/
    _2 = (int)SEQ_PTR(_data_7389);
    _3893 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_3893);
    DeRefDS(_path_7387);
    DeRefDS(_data_7389);
    _3890 = NOVALUE;
    return _3893;
    ;
}


int _9pathname(int _path_7399)
{
    int _data_7400 = NOVALUE;
    int _stop_7401 = NOVALUE;
    int _3898 = NOVALUE;
    int _3897 = NOVALUE;
    int _3895 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = canonical_path(path)*/
    RefDS(_path_7399);
    _0 = _data_7400;
    _data_7400 = _9canonical_path(_path_7399, 0, 0);
    DeRef(_0);

    /** 	stop = search:rfind(SLASH, data)*/
    if (IS_SEQUENCE(_data_7400)){
            _3895 = SEQ_PTR(_data_7400)->length;
    }
    else {
        _3895 = 1;
    }
    RefDS(_data_7400);
    _stop_7401 = _7rfind(92, _data_7400, _3895);
    _3895 = NOVALUE;
    if (!IS_ATOM_INT(_stop_7401)) {
        _1 = (long)(DBL_PTR(_stop_7401)->dbl);
        if (UNIQUE(DBL_PTR(_stop_7401)) && (DBL_PTR(_stop_7401)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_stop_7401);
        _stop_7401 = _1;
    }

    /** 	return data[1 .. stop - 1]*/
    _3897 = _stop_7401 - 1;
    rhs_slice_target = (object_ptr)&_3898;
    RHS_Slice(_data_7400, 1, _3897);
    DeRefDS(_path_7399);
    DeRefDS(_data_7400);
    _3897 = NOVALUE;
    return _3898;
    ;
}


int _9filename(int _path_7410)
{
    int _data_7411 = NOVALUE;
    int _3900 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = pathinfo(path)*/
    RefDS(_path_7410);
    _0 = _data_7411;
    _data_7411 = _9pathinfo(_path_7410, 0);
    DeRef(_0);

    /** 	return data[2]*/
    _2 = (int)SEQ_PTR(_data_7411);
    _3900 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_3900);
    DeRefDS(_path_7410);
    DeRefDS(_data_7411);
    return _3900;
    ;
}


int _9filebase(int _path_7416)
{
    int _data_7417 = NOVALUE;
    int _3902 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = pathinfo(path)*/
    RefDS(_path_7416);
    _0 = _data_7417;
    _data_7417 = _9pathinfo(_path_7416, 0);
    DeRef(_0);

    /** 	return data[3]*/
    _2 = (int)SEQ_PTR(_data_7417);
    _3902 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_3902);
    DeRefDS(_path_7416);
    DeRefDS(_data_7417);
    return _3902;
    ;
}


int _9fileext(int _path_7422)
{
    int _data_7423 = NOVALUE;
    int _3904 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = pathinfo(path)*/
    RefDS(_path_7422);
    _0 = _data_7423;
    _data_7423 = _9pathinfo(_path_7422, 0);
    DeRef(_0);

    /** 	return data[4]*/
    _2 = (int)SEQ_PTR(_data_7423);
    _3904 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_3904);
    DeRefDS(_path_7422);
    DeRefDS(_data_7423);
    return _3904;
    ;
}


int _9driveid(int _path_7428)
{
    int _data_7429 = NOVALUE;
    int _3906 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = pathinfo(path)*/
    RefDS(_path_7428);
    _0 = _data_7429;
    _data_7429 = _9pathinfo(_path_7428, 0);
    DeRef(_0);

    /** 	return data[5]*/
    _2 = (int)SEQ_PTR(_data_7429);
    _3906 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_3906);
    DeRefDS(_path_7428);
    DeRefDS(_data_7429);
    return _3906;
    ;
}


int _9defaultext(int _path_7434, int _defext_7435)
{
    int _3919 = NOVALUE;
    int _3916 = NOVALUE;
    int _3914 = NOVALUE;
    int _3913 = NOVALUE;
    int _3912 = NOVALUE;
    int _3910 = NOVALUE;
    int _3909 = NOVALUE;
    int _3907 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(defext) = 0 then*/
    if (IS_SEQUENCE(_defext_7435)){
            _3907 = SEQ_PTR(_defext_7435)->length;
    }
    else {
        _3907 = 1;
    }
    if (_3907 != 0)
    goto L1; // [10] 21

    /** 		return path*/
    DeRefDS(_defext_7435);
    return _path_7434;
L1: 

    /** 	for i = length(path) to 1 by -1 do*/
    if (IS_SEQUENCE(_path_7434)){
            _3909 = SEQ_PTR(_path_7434)->length;
    }
    else {
        _3909 = 1;
    }
    {
        int _i_7440;
        _i_7440 = _3909;
L2: 
        if (_i_7440 < 1){
            goto L3; // [26] 95
        }

        /** 		if path[i] = '.' then*/
        _2 = (int)SEQ_PTR(_path_7434);
        _3910 = (int)*(((s1_ptr)_2)->base + _i_7440);
        if (binary_op_a(NOTEQ, _3910, 46)){
            _3910 = NOVALUE;
            goto L4; // [39] 50
        }
        _3910 = NOVALUE;

        /** 			return path*/
        DeRefDS(_defext_7435);
        return _path_7434;
L4: 

        /** 		if find(path[i], SLASHES) then*/
        _2 = (int)SEQ_PTR(_path_7434);
        _3912 = (int)*(((s1_ptr)_2)->base + _i_7440);
        _3913 = find_from(_3912, _9SLASHES_6949, 1);
        _3912 = NOVALUE;
        if (_3913 == 0)
        {
            _3913 = NOVALUE;
            goto L5; // [61] 88
        }
        else{
            _3913 = NOVALUE;
        }

        /** 			if i = length(path) then*/
        if (IS_SEQUENCE(_path_7434)){
                _3914 = SEQ_PTR(_path_7434)->length;
        }
        else {
            _3914 = 1;
        }
        if (_i_7440 != _3914)
        goto L3; // [69] 95

        /** 				return path*/
        DeRefDS(_defext_7435);
        return _path_7434;
        goto L6; // [79] 87

        /** 				exit*/
        goto L3; // [84] 95
L6: 
L5: 

        /** 	end for*/
        _i_7440 = _i_7440 + -1;
        goto L2; // [90] 33
L3: 
        ;
    }

    /** 	if defext[1] != '.' then*/
    _2 = (int)SEQ_PTR(_defext_7435);
    _3916 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _3916, 46)){
        _3916 = NOVALUE;
        goto L7; // [101] 112
    }
    _3916 = NOVALUE;

    /** 		path &= '.'*/
    Append(&_path_7434, _path_7434, 46);
L7: 

    /** 	return path & defext*/
    Concat((object_ptr)&_3919, _path_7434, _defext_7435);
    DeRefDS(_path_7434);
    DeRefDS(_defext_7435);
    return _3919;
    ;
}


int _9absolute_path(int _filename_7459)
{
    int _3931 = NOVALUE;
    int _3930 = NOVALUE;
    int _3928 = NOVALUE;
    int _3926 = NOVALUE;
    int _3924 = NOVALUE;
    int _3923 = NOVALUE;
    int _3922 = NOVALUE;
    int _3920 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(filename) = 0 then*/
    if (IS_SEQUENCE(_filename_7459)){
            _3920 = SEQ_PTR(_filename_7459)->length;
    }
    else {
        _3920 = 1;
    }
    if (_3920 != 0)
    goto L1; // [8] 19

    /** 		return 0*/
    DeRefDS(_filename_7459);
    return 0;
L1: 

    /** 	if eu:find(filename[1], SLASHES) then*/
    _2 = (int)SEQ_PTR(_filename_7459);
    _3922 = (int)*(((s1_ptr)_2)->base + 1);
    _3923 = find_from(_3922, _9SLASHES_6949, 1);
    _3922 = NOVALUE;
    if (_3923 == 0)
    {
        _3923 = NOVALUE;
        goto L2; // [30] 40
    }
    else{
        _3923 = NOVALUE;
    }

    /** 		return 1*/
    DeRefDS(_filename_7459);
    return 1;
L2: 

    /** 	ifdef WINDOWS then*/

    /** 		if length(filename) = 1 then*/
    if (IS_SEQUENCE(_filename_7459)){
            _3924 = SEQ_PTR(_filename_7459)->length;
    }
    else {
        _3924 = 1;
    }
    if (_3924 != 1)
    goto L3; // [47] 58

    /** 			return 0*/
    DeRefDS(_filename_7459);
    return 0;
L3: 

    /** 		if filename[2] != ':' then*/
    _2 = (int)SEQ_PTR(_filename_7459);
    _3926 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(EQUALS, _3926, 58)){
        _3926 = NOVALUE;
        goto L4; // [64] 75
    }
    _3926 = NOVALUE;

    /** 			return 0*/
    DeRefDS(_filename_7459);
    return 0;
L4: 

    /** 		if length(filename) < 3 then*/
    if (IS_SEQUENCE(_filename_7459)){
            _3928 = SEQ_PTR(_filename_7459)->length;
    }
    else {
        _3928 = 1;
    }
    if (_3928 >= 3)
    goto L5; // [80] 91

    /** 			return 0*/
    DeRefDS(_filename_7459);
    return 0;
L5: 

    /** 		if eu:find(filename[3], SLASHES) then*/
    _2 = (int)SEQ_PTR(_filename_7459);
    _3930 = (int)*(((s1_ptr)_2)->base + 3);
    _3931 = find_from(_3930, _9SLASHES_6949, 1);
    _3930 = NOVALUE;
    if (_3931 == 0)
    {
        _3931 = NOVALUE;
        goto L6; // [102] 112
    }
    else{
        _3931 = NOVALUE;
    }

    /** 			return 1*/
    DeRefDS(_filename_7459);
    return 1;
L6: 

    /** 	return 0*/
    DeRefDS(_filename_7459);
    return 0;
    ;
}


int _9case_flagset_type(int _x_7491)
{
    int _3940 = NOVALUE;
    int _3939 = NOVALUE;
    int _3938 = NOVALUE;
    int _3937 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_7491)) {
        _1 = (long)(DBL_PTR(_x_7491)->dbl);
        if (UNIQUE(DBL_PTR(_x_7491)) && (DBL_PTR(_x_7491)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_7491);
        _x_7491 = _1;
    }

    /** 	return x >= AS_IS and x < 2*TO_SHORT*/
    _3937 = (_x_7491 >= 0);
    _3938 = 8;
    _3939 = (_x_7491 < 8);
    _3938 = NOVALUE;
    _3940 = (_3937 != 0 && _3939 != 0);
    _3937 = NOVALUE;
    _3939 = NOVALUE;
    return _3940;
    ;
}


int _9canonical_path(int _path_in_7498, int _directory_given_7499, int _case_flags_7500)
{
    int _lPath_7501 = NOVALUE;
    int _lPosA_7502 = NOVALUE;
    int _lPosB_7503 = NOVALUE;
    int _lLevel_7504 = NOVALUE;
    int _lHome_7505 = NOVALUE;
    int _lDrive_7506 = NOVALUE;
    int _current_dir_inlined_current_dir_at_300_7566 = NOVALUE;
    int _driveid_inlined_driveid_at_307_7569 = NOVALUE;
    int _data_inlined_driveid_at_307_7568 = NOVALUE;
    int _wildcard_suffix_7571 = NOVALUE;
    int _first_wildcard_at_7572 = NOVALUE;
    int _last_slash_7575 = NOVALUE;
    int _sl_7647 = NOVALUE;
    int _short_name_7650 = NOVALUE;
    int _correct_name_7653 = NOVALUE;
    int _lower_name_7656 = NOVALUE;
    int _part_7672 = NOVALUE;
    int _list_7677 = NOVALUE;
    int _dir_inlined_dir_at_899_7681 = NOVALUE;
    int _name_inlined_dir_at_896_7680 = NOVALUE;
    int _supplied_name_7682 = NOVALUE;
    int _read_name_7701 = NOVALUE;
    int _read_name_7726 = NOVALUE;
    int _4153 = NOVALUE;
    int _4150 = NOVALUE;
    int _4146 = NOVALUE;
    int _4145 = NOVALUE;
    int _4143 = NOVALUE;
    int _4142 = NOVALUE;
    int _4141 = NOVALUE;
    int _4140 = NOVALUE;
    int _4139 = NOVALUE;
    int _4137 = NOVALUE;
    int _4136 = NOVALUE;
    int _4135 = NOVALUE;
    int _4134 = NOVALUE;
    int _4133 = NOVALUE;
    int _4132 = NOVALUE;
    int _4131 = NOVALUE;
    int _4130 = NOVALUE;
    int _4128 = NOVALUE;
    int _4127 = NOVALUE;
    int _4126 = NOVALUE;
    int _4125 = NOVALUE;
    int _4124 = NOVALUE;
    int _4123 = NOVALUE;
    int _4122 = NOVALUE;
    int _4121 = NOVALUE;
    int _4120 = NOVALUE;
    int _4118 = NOVALUE;
    int _4117 = NOVALUE;
    int _4116 = NOVALUE;
    int _4115 = NOVALUE;
    int _4114 = NOVALUE;
    int _4113 = NOVALUE;
    int _4112 = NOVALUE;
    int _4111 = NOVALUE;
    int _4110 = NOVALUE;
    int _4109 = NOVALUE;
    int _4108 = NOVALUE;
    int _4107 = NOVALUE;
    int _4106 = NOVALUE;
    int _4105 = NOVALUE;
    int _4104 = NOVALUE;
    int _4102 = NOVALUE;
    int _4101 = NOVALUE;
    int _4100 = NOVALUE;
    int _4099 = NOVALUE;
    int _4098 = NOVALUE;
    int _4096 = NOVALUE;
    int _4095 = NOVALUE;
    int _4094 = NOVALUE;
    int _4093 = NOVALUE;
    int _4092 = NOVALUE;
    int _4091 = NOVALUE;
    int _4090 = NOVALUE;
    int _4089 = NOVALUE;
    int _4088 = NOVALUE;
    int _4087 = NOVALUE;
    int _4086 = NOVALUE;
    int _4085 = NOVALUE;
    int _4084 = NOVALUE;
    int _4082 = NOVALUE;
    int _4081 = NOVALUE;
    int _4079 = NOVALUE;
    int _4078 = NOVALUE;
    int _4077 = NOVALUE;
    int _4076 = NOVALUE;
    int _4075 = NOVALUE;
    int _4073 = NOVALUE;
    int _4072 = NOVALUE;
    int _4071 = NOVALUE;
    int _4070 = NOVALUE;
    int _4069 = NOVALUE;
    int _4068 = NOVALUE;
    int _4066 = NOVALUE;
    int _4065 = NOVALUE;
    int _4064 = NOVALUE;
    int _4062 = NOVALUE;
    int _4061 = NOVALUE;
    int _4059 = NOVALUE;
    int _4058 = NOVALUE;
    int _4057 = NOVALUE;
    int _4055 = NOVALUE;
    int _4054 = NOVALUE;
    int _4052 = NOVALUE;
    int _4050 = NOVALUE;
    int _4048 = NOVALUE;
    int _4041 = NOVALUE;
    int _4038 = NOVALUE;
    int _4037 = NOVALUE;
    int _4036 = NOVALUE;
    int _4035 = NOVALUE;
    int _4029 = NOVALUE;
    int _4025 = NOVALUE;
    int _4024 = NOVALUE;
    int _4023 = NOVALUE;
    int _4022 = NOVALUE;
    int _4021 = NOVALUE;
    int _4019 = NOVALUE;
    int _4016 = NOVALUE;
    int _4015 = NOVALUE;
    int _4014 = NOVALUE;
    int _4013 = NOVALUE;
    int _4012 = NOVALUE;
    int _4011 = NOVALUE;
    int _4009 = NOVALUE;
    int _4008 = NOVALUE;
    int _4006 = NOVALUE;
    int _4004 = NOVALUE;
    int _4003 = NOVALUE;
    int _4002 = NOVALUE;
    int _4000 = NOVALUE;
    int _3999 = NOVALUE;
    int _3998 = NOVALUE;
    int _3997 = NOVALUE;
    int _3995 = NOVALUE;
    int _3993 = NOVALUE;
    int _3988 = NOVALUE;
    int _3986 = NOVALUE;
    int _3985 = NOVALUE;
    int _3984 = NOVALUE;
    int _3983 = NOVALUE;
    int _3982 = NOVALUE;
    int _3980 = NOVALUE;
    int _3979 = NOVALUE;
    int _3977 = NOVALUE;
    int _3976 = NOVALUE;
    int _3975 = NOVALUE;
    int _3974 = NOVALUE;
    int _3973 = NOVALUE;
    int _3972 = NOVALUE;
    int _3971 = NOVALUE;
    int _3968 = NOVALUE;
    int _3967 = NOVALUE;
    int _3965 = NOVALUE;
    int _3963 = NOVALUE;
    int _3961 = NOVALUE;
    int _3958 = NOVALUE;
    int _3957 = NOVALUE;
    int _3956 = NOVALUE;
    int _3955 = NOVALUE;
    int _3954 = NOVALUE;
    int _3952 = NOVALUE;
    int _3951 = NOVALUE;
    int _3950 = NOVALUE;
    int _3949 = NOVALUE;
    int _3948 = NOVALUE;
    int _3947 = NOVALUE;
    int _3946 = NOVALUE;
    int _3945 = NOVALUE;
    int _3944 = NOVALUE;
    int _3943 = NOVALUE;
    int _3942 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_directory_given_7499)) {
        _1 = (long)(DBL_PTR(_directory_given_7499)->dbl);
        if (UNIQUE(DBL_PTR(_directory_given_7499)) && (DBL_PTR(_directory_given_7499)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_directory_given_7499);
        _directory_given_7499 = _1;
    }
    if (!IS_ATOM_INT(_case_flags_7500)) {
        _1 = (long)(DBL_PTR(_case_flags_7500)->dbl);
        if (UNIQUE(DBL_PTR(_case_flags_7500)) && (DBL_PTR(_case_flags_7500)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_case_flags_7500);
        _case_flags_7500 = _1;
    }

    /**     sequence lPath = ""*/
    RefDS(_5);
    DeRef(_lPath_7501);
    _lPath_7501 = _5;

    /**     integer lPosA = -1*/
    _lPosA_7502 = -1;

    /**     integer lPosB = -1*/
    _lPosB_7503 = -1;

    /**     sequence lLevel = ""*/
    RefDS(_5);
    DeRefi(_lLevel_7504);
    _lLevel_7504 = _5;

    /**     path_in = path_in*/
    RefDS(_path_in_7498);
    DeRefDS(_path_in_7498);
    _path_in_7498 = _path_in_7498;

    /** 	ifdef UNIX then*/

    /** 	    sequence lDrive*/

    /** 	    lPath = match_replace("/", path_in, SLASH)*/
    RefDS(_3651);
    RefDS(_path_in_7498);
    _0 = _lPath_7501;
    _lPath_7501 = _7match_replace(_3651, _path_in_7498, 92, 0);
    DeRefDS(_0);

    /**     if (length(lPath) > 2 and lPath[1] = '"' and lPath[$] = '"') then*/
    if (IS_SEQUENCE(_lPath_7501)){
            _3942 = SEQ_PTR(_lPath_7501)->length;
    }
    else {
        _3942 = 1;
    }
    _3943 = (_3942 > 2);
    _3942 = NOVALUE;
    if (_3943 == 0) {
        _3944 = 0;
        goto L1; // [62] 78
    }
    _2 = (int)SEQ_PTR(_lPath_7501);
    _3945 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_3945)) {
        _3946 = (_3945 == 34);
    }
    else {
        _3946 = binary_op(EQUALS, _3945, 34);
    }
    _3945 = NOVALUE;
    if (IS_ATOM_INT(_3946))
    _3944 = (_3946 != 0);
    else
    _3944 = DBL_PTR(_3946)->dbl != 0.0;
L1: 
    if (_3944 == 0) {
        DeRef(_3947);
        _3947 = 0;
        goto L2; // [78] 97
    }
    if (IS_SEQUENCE(_lPath_7501)){
            _3948 = SEQ_PTR(_lPath_7501)->length;
    }
    else {
        _3948 = 1;
    }
    _2 = (int)SEQ_PTR(_lPath_7501);
    _3949 = (int)*(((s1_ptr)_2)->base + _3948);
    if (IS_ATOM_INT(_3949)) {
        _3950 = (_3949 == 34);
    }
    else {
        _3950 = binary_op(EQUALS, _3949, 34);
    }
    _3949 = NOVALUE;
    if (IS_ATOM_INT(_3950))
    _3947 = (_3950 != 0);
    else
    _3947 = DBL_PTR(_3950)->dbl != 0.0;
L2: 
    if (_3947 == 0)
    {
        _3947 = NOVALUE;
        goto L3; // [97] 115
    }
    else{
        _3947 = NOVALUE;
    }

    /**         lPath = lPath[2..$-1]*/
    if (IS_SEQUENCE(_lPath_7501)){
            _3951 = SEQ_PTR(_lPath_7501)->length;
    }
    else {
        _3951 = 1;
    }
    _3952 = _3951 - 1;
    _3951 = NOVALUE;
    rhs_slice_target = (object_ptr)&_lPath_7501;
    RHS_Slice(_lPath_7501, 2, _3952);
L3: 

    /**     if (length(lPath) > 0 and lPath[1] = '~') then*/
    if (IS_SEQUENCE(_lPath_7501)){
            _3954 = SEQ_PTR(_lPath_7501)->length;
    }
    else {
        _3954 = 1;
    }
    _3955 = (_3954 > 0);
    _3954 = NOVALUE;
    if (_3955 == 0) {
        DeRef(_3956);
        _3956 = 0;
        goto L4; // [124] 140
    }
    _2 = (int)SEQ_PTR(_lPath_7501);
    _3957 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_3957)) {
        _3958 = (_3957 == 126);
    }
    else {
        _3958 = binary_op(EQUALS, _3957, 126);
    }
    _3957 = NOVALUE;
    if (IS_ATOM_INT(_3958))
    _3956 = (_3958 != 0);
    else
    _3956 = DBL_PTR(_3958)->dbl != 0.0;
L4: 
    if (_3956 == 0)
    {
        _3956 = NOVALUE;
        goto L5; // [140] 249
    }
    else{
        _3956 = NOVALUE;
    }

    /** 		lHome = getenv("HOME")*/
    DeRef(_lHome_7505);
    _lHome_7505 = EGetEnv(_3959);

    /** 		ifdef WINDOWS then*/

    /** 			if atom(lHome) then*/
    _3961 = IS_ATOM(_lHome_7505);
    if (_3961 == 0)
    {
        _3961 = NOVALUE;
        goto L6; // [155] 171
    }
    else{
        _3961 = NOVALUE;
    }

    /** 				lHome = getenv("HOMEDRIVE") & getenv("HOMEPATH")*/
    _3963 = EGetEnv(_3962);
    _3965 = EGetEnv(_3964);
    if (IS_SEQUENCE(_3963) && IS_ATOM(_3965)) {
        Ref(_3965);
        Append(&_lHome_7505, _3963, _3965);
    }
    else if (IS_ATOM(_3963) && IS_SEQUENCE(_3965)) {
        Ref(_3963);
        Prepend(&_lHome_7505, _3965, _3963);
    }
    else {
        Concat((object_ptr)&_lHome_7505, _3963, _3965);
        DeRef(_3963);
        _3963 = NOVALUE;
    }
    DeRef(_3963);
    _3963 = NOVALUE;
    DeRef(_3965);
    _3965 = NOVALUE;
L6: 

    /** 		if lHome[$] != SLASH then*/
    if (IS_SEQUENCE(_lHome_7505)){
            _3967 = SEQ_PTR(_lHome_7505)->length;
    }
    else {
        _3967 = 1;
    }
    _2 = (int)SEQ_PTR(_lHome_7505);
    _3968 = (int)*(((s1_ptr)_2)->base + _3967);
    if (binary_op_a(EQUALS, _3968, 92)){
        _3968 = NOVALUE;
        goto L7; // [180] 191
    }
    _3968 = NOVALUE;

    /** 			lHome &= SLASH*/
    if (IS_SEQUENCE(_lHome_7505) && IS_ATOM(92)) {
        Append(&_lHome_7505, _lHome_7505, 92);
    }
    else if (IS_ATOM(_lHome_7505) && IS_SEQUENCE(92)) {
    }
    else {
        Concat((object_ptr)&_lHome_7505, _lHome_7505, 92);
    }
L7: 

    /** 		if length(lPath) > 1 and lPath[2] = SLASH then*/
    if (IS_SEQUENCE(_lPath_7501)){
            _3971 = SEQ_PTR(_lPath_7501)->length;
    }
    else {
        _3971 = 1;
    }
    _3972 = (_3971 > 1);
    _3971 = NOVALUE;
    if (_3972 == 0) {
        goto L8; // [200] 233
    }
    _2 = (int)SEQ_PTR(_lPath_7501);
    _3974 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_3974)) {
        _3975 = (_3974 == 92);
    }
    else {
        _3975 = binary_op(EQUALS, _3974, 92);
    }
    _3974 = NOVALUE;
    if (_3975 == 0) {
        DeRef(_3975);
        _3975 = NOVALUE;
        goto L8; // [213] 233
    }
    else {
        if (!IS_ATOM_INT(_3975) && DBL_PTR(_3975)->dbl == 0.0){
            DeRef(_3975);
            _3975 = NOVALUE;
            goto L8; // [213] 233
        }
        DeRef(_3975);
        _3975 = NOVALUE;
    }
    DeRef(_3975);
    _3975 = NOVALUE;

    /** 			lPath = lHome & lPath[3 .. $]*/
    if (IS_SEQUENCE(_lPath_7501)){
            _3976 = SEQ_PTR(_lPath_7501)->length;
    }
    else {
        _3976 = 1;
    }
    rhs_slice_target = (object_ptr)&_3977;
    RHS_Slice(_lPath_7501, 3, _3976);
    if (IS_SEQUENCE(_lHome_7505) && IS_ATOM(_3977)) {
    }
    else if (IS_ATOM(_lHome_7505) && IS_SEQUENCE(_3977)) {
        Ref(_lHome_7505);
        Prepend(&_lPath_7501, _3977, _lHome_7505);
    }
    else {
        Concat((object_ptr)&_lPath_7501, _lHome_7505, _3977);
    }
    DeRefDS(_3977);
    _3977 = NOVALUE;
    goto L9; // [230] 248
L8: 

    /** 			lPath = lHome & lPath[2 .. $]*/
    if (IS_SEQUENCE(_lPath_7501)){
            _3979 = SEQ_PTR(_lPath_7501)->length;
    }
    else {
        _3979 = 1;
    }
    rhs_slice_target = (object_ptr)&_3980;
    RHS_Slice(_lPath_7501, 2, _3979);
    if (IS_SEQUENCE(_lHome_7505) && IS_ATOM(_3980)) {
    }
    else if (IS_ATOM(_lHome_7505) && IS_SEQUENCE(_3980)) {
        Ref(_lHome_7505);
        Prepend(&_lPath_7501, _3980, _lHome_7505);
    }
    else {
        Concat((object_ptr)&_lPath_7501, _lHome_7505, _3980);
    }
    DeRefDS(_3980);
    _3980 = NOVALUE;
L9: 
L5: 

    /** 	ifdef WINDOWS then*/

    /** 	    if ( (length(lPath) > 1) and (lPath[2] = ':' ) ) then*/
    if (IS_SEQUENCE(_lPath_7501)){
            _3982 = SEQ_PTR(_lPath_7501)->length;
    }
    else {
        _3982 = 1;
    }
    _3983 = (_3982 > 1);
    _3982 = NOVALUE;
    if (_3983 == 0) {
        DeRef(_3984);
        _3984 = 0;
        goto LA; // [260] 276
    }
    _2 = (int)SEQ_PTR(_lPath_7501);
    _3985 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_3985)) {
        _3986 = (_3985 == 58);
    }
    else {
        _3986 = binary_op(EQUALS, _3985, 58);
    }
    _3985 = NOVALUE;
    if (IS_ATOM_INT(_3986))
    _3984 = (_3986 != 0);
    else
    _3984 = DBL_PTR(_3986)->dbl != 0.0;
LA: 
    if (_3984 == 0)
    {
        _3984 = NOVALUE;
        goto LB; // [276] 299
    }
    else{
        _3984 = NOVALUE;
    }

    /** 			lDrive = lPath[1..2]*/
    rhs_slice_target = (object_ptr)&_lDrive_7506;
    RHS_Slice(_lPath_7501, 1, 2);

    /** 			lPath = lPath[3..$]*/
    if (IS_SEQUENCE(_lPath_7501)){
            _3988 = SEQ_PTR(_lPath_7501)->length;
    }
    else {
        _3988 = 1;
    }
    rhs_slice_target = (object_ptr)&_lPath_7501;
    RHS_Slice(_lPath_7501, 3, _3988);
    goto LC; // [296] 333
LB: 

    /** 			lDrive = driveid(current_dir()) & ':'*/

    /** 	return machine_func(M_CURRENT_DIR, 0)*/
    DeRefi(_current_dir_inlined_current_dir_at_300_7566);
    _current_dir_inlined_current_dir_at_300_7566 = machine(23, 0);

    /** 	data = pathinfo(path)*/
    RefDS(_current_dir_inlined_current_dir_at_300_7566);
    _0 = _data_inlined_driveid_at_307_7568;
    _data_inlined_driveid_at_307_7568 = _9pathinfo(_current_dir_inlined_current_dir_at_300_7566, 0);
    DeRef(_0);

    /** 	return data[5]*/
    DeRef(_driveid_inlined_driveid_at_307_7569);
    _2 = (int)SEQ_PTR(_data_inlined_driveid_at_307_7568);
    _driveid_inlined_driveid_at_307_7569 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_driveid_inlined_driveid_at_307_7569);
    DeRef(_data_inlined_driveid_at_307_7568);
    _data_inlined_driveid_at_307_7568 = NOVALUE;
    if (IS_SEQUENCE(_driveid_inlined_driveid_at_307_7569) && IS_ATOM(58)) {
        Append(&_lDrive_7506, _driveid_inlined_driveid_at_307_7569, 58);
    }
    else if (IS_ATOM(_driveid_inlined_driveid_at_307_7569) && IS_SEQUENCE(58)) {
    }
    else {
        Concat((object_ptr)&_lDrive_7506, _driveid_inlined_driveid_at_307_7569, 58);
    }
LC: 

    /** 	sequence wildcard_suffix*/

    /** 	integer first_wildcard_at = find_first_wildcard( lPath )*/
    RefDS(_lPath_7501);
    _first_wildcard_at_7572 = _9find_first_wildcard(_lPath_7501, 1);
    if (!IS_ATOM_INT(_first_wildcard_at_7572)) {
        _1 = (long)(DBL_PTR(_first_wildcard_at_7572)->dbl);
        if (UNIQUE(DBL_PTR(_first_wildcard_at_7572)) && (DBL_PTR(_first_wildcard_at_7572)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_first_wildcard_at_7572);
        _first_wildcard_at_7572 = _1;
    }

    /** 	if first_wildcard_at then*/
    if (_first_wildcard_at_7572 == 0)
    {
        goto LD; // [346] 407
    }
    else{
    }

    /** 		integer last_slash = search:rfind( SLASH, lPath, first_wildcard_at )*/
    RefDS(_lPath_7501);
    _last_slash_7575 = _7rfind(92, _lPath_7501, _first_wildcard_at_7572);
    if (!IS_ATOM_INT(_last_slash_7575)) {
        _1 = (long)(DBL_PTR(_last_slash_7575)->dbl);
        if (UNIQUE(DBL_PTR(_last_slash_7575)) && (DBL_PTR(_last_slash_7575)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_last_slash_7575);
        _last_slash_7575 = _1;
    }

    /** 		if last_slash then*/
    if (_last_slash_7575 == 0)
    {
        goto LE; // [361] 387
    }
    else{
    }

    /** 			wildcard_suffix = lPath[last_slash..$]*/
    if (IS_SEQUENCE(_lPath_7501)){
            _3993 = SEQ_PTR(_lPath_7501)->length;
    }
    else {
        _3993 = 1;
    }
    rhs_slice_target = (object_ptr)&_wildcard_suffix_7571;
    RHS_Slice(_lPath_7501, _last_slash_7575, _3993);

    /** 			lPath = remove( lPath, last_slash, length( lPath ) )*/
    if (IS_SEQUENCE(_lPath_7501)){
            _3995 = SEQ_PTR(_lPath_7501)->length;
    }
    else {
        _3995 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_lPath_7501);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_last_slash_7575)) ? _last_slash_7575 : (long)(DBL_PTR(_last_slash_7575)->dbl);
        int stop = (IS_ATOM_INT(_3995)) ? _3995 : (long)(DBL_PTR(_3995)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_lPath_7501), start, &_lPath_7501 );
            }
            else Tail(SEQ_PTR(_lPath_7501), stop+1, &_lPath_7501);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_lPath_7501), start, &_lPath_7501);
        }
        else {
            assign_slice_seq = &assign_space;
            _lPath_7501 = Remove_elements(start, stop, (SEQ_PTR(_lPath_7501)->ref == 1));
        }
    }
    _3995 = NOVALUE;
    goto LF; // [384] 402
LE: 

    /** 			wildcard_suffix = lPath*/
    RefDS(_lPath_7501);
    DeRef(_wildcard_suffix_7571);
    _wildcard_suffix_7571 = _lPath_7501;

    /** 			lPath = ""*/
    RefDS(_5);
    DeRefDS(_lPath_7501);
    _lPath_7501 = _5;
LF: 
    goto L10; // [404] 415
LD: 

    /** 		wildcard_suffix = ""*/
    RefDS(_5);
    DeRef(_wildcard_suffix_7571);
    _wildcard_suffix_7571 = _5;
L10: 

    /** 	if ((length(lPath) = 0) or not find(lPath[1], "/\\")) then*/
    if (IS_SEQUENCE(_lPath_7501)){
            _3997 = SEQ_PTR(_lPath_7501)->length;
    }
    else {
        _3997 = 1;
    }
    _3998 = (_3997 == 0);
    _3997 = NOVALUE;
    if (_3998 != 0) {
        DeRef(_3999);
        _3999 = 1;
        goto L11; // [424] 444
    }
    _2 = (int)SEQ_PTR(_lPath_7501);
    _4000 = (int)*(((s1_ptr)_2)->base + 1);
    _4002 = find_from(_4000, _4001, 1);
    _4000 = NOVALUE;
    _4003 = (_4002 == 0);
    _4002 = NOVALUE;
    _3999 = (_4003 != 0);
L11: 
    if (_3999 == 0)
    {
        _3999 = NOVALUE;
        goto L12; // [444] 545
    }
    else{
        _3999 = NOVALUE;
    }

    /** 		ifdef UNIX then*/

    /** 			if (length(lDrive) = 0) then*/
    if (IS_SEQUENCE(_lDrive_7506)){
            _4004 = SEQ_PTR(_lDrive_7506)->length;
    }
    else {
        _4004 = 1;
    }
    if (_4004 != 0)
    goto L13; // [456] 473

    /** 				lPath = curdir() & lPath*/
    _4006 = _9curdir(0);
    if (IS_SEQUENCE(_4006) && IS_ATOM(_lPath_7501)) {
    }
    else if (IS_ATOM(_4006) && IS_SEQUENCE(_lPath_7501)) {
        Ref(_4006);
        Prepend(&_lPath_7501, _lPath_7501, _4006);
    }
    else {
        Concat((object_ptr)&_lPath_7501, _4006, _lPath_7501);
        DeRef(_4006);
        _4006 = NOVALUE;
    }
    DeRef(_4006);
    _4006 = NOVALUE;
    goto L14; // [470] 488
L13: 

    /** 				lPath = curdir(lDrive[1]) & lPath*/
    _2 = (int)SEQ_PTR(_lDrive_7506);
    _4008 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_4008);
    _4009 = _9curdir(_4008);
    _4008 = NOVALUE;
    if (IS_SEQUENCE(_4009) && IS_ATOM(_lPath_7501)) {
    }
    else if (IS_ATOM(_4009) && IS_SEQUENCE(_lPath_7501)) {
        Ref(_4009);
        Prepend(&_lPath_7501, _lPath_7501, _4009);
    }
    else {
        Concat((object_ptr)&_lPath_7501, _4009, _lPath_7501);
        DeRef(_4009);
        _4009 = NOVALUE;
    }
    DeRef(_4009);
    _4009 = NOVALUE;
L14: 

    /** 			if ( (length(lPath) > 1) and (lPath[2] = ':' ) ) then*/
    if (IS_SEQUENCE(_lPath_7501)){
            _4011 = SEQ_PTR(_lPath_7501)->length;
    }
    else {
        _4011 = 1;
    }
    _4012 = (_4011 > 1);
    _4011 = NOVALUE;
    if (_4012 == 0) {
        DeRef(_4013);
        _4013 = 0;
        goto L15; // [497] 513
    }
    _2 = (int)SEQ_PTR(_lPath_7501);
    _4014 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_4014)) {
        _4015 = (_4014 == 58);
    }
    else {
        _4015 = binary_op(EQUALS, _4014, 58);
    }
    _4014 = NOVALUE;
    if (IS_ATOM_INT(_4015))
    _4013 = (_4015 != 0);
    else
    _4013 = DBL_PTR(_4015)->dbl != 0.0;
L15: 
    if (_4013 == 0)
    {
        _4013 = NOVALUE;
        goto L16; // [513] 544
    }
    else{
        _4013 = NOVALUE;
    }

    /** 				if (length(lDrive) = 0) then*/
    if (IS_SEQUENCE(_lDrive_7506)){
            _4016 = SEQ_PTR(_lDrive_7506)->length;
    }
    else {
        _4016 = 1;
    }
    if (_4016 != 0)
    goto L17; // [521] 533

    /** 					lDrive = lPath[1..2]*/
    rhs_slice_target = (object_ptr)&_lDrive_7506;
    RHS_Slice(_lPath_7501, 1, 2);
L17: 

    /** 				lPath = lPath[3..$]*/
    if (IS_SEQUENCE(_lPath_7501)){
            _4019 = SEQ_PTR(_lPath_7501)->length;
    }
    else {
        _4019 = 1;
    }
    rhs_slice_target = (object_ptr)&_lPath_7501;
    RHS_Slice(_lPath_7501, 3, _4019);
L16: 
L12: 

    /** 	if ((directory_given != 0) and (lPath[$] != SLASH) ) then*/
    _4021 = (_directory_given_7499 != 0);
    if (_4021 == 0) {
        DeRef(_4022);
        _4022 = 0;
        goto L18; // [551] 570
    }
    if (IS_SEQUENCE(_lPath_7501)){
            _4023 = SEQ_PTR(_lPath_7501)->length;
    }
    else {
        _4023 = 1;
    }
    _2 = (int)SEQ_PTR(_lPath_7501);
    _4024 = (int)*(((s1_ptr)_2)->base + _4023);
    if (IS_ATOM_INT(_4024)) {
        _4025 = (_4024 != 92);
    }
    else {
        _4025 = binary_op(NOTEQ, _4024, 92);
    }
    _4024 = NOVALUE;
    if (IS_ATOM_INT(_4025))
    _4022 = (_4025 != 0);
    else
    _4022 = DBL_PTR(_4025)->dbl != 0.0;
L18: 
    if (_4022 == 0)
    {
        _4022 = NOVALUE;
        goto L19; // [570] 580
    }
    else{
        _4022 = NOVALUE;
    }

    /** 		lPath &= SLASH*/
    Append(&_lPath_7501, _lPath_7501, 92);
L19: 

    /** 	lLevel = SLASH & '.' & SLASH*/
    {
        int concat_list[3];

        concat_list[0] = 92;
        concat_list[1] = 46;
        concat_list[2] = 92;
        Concat_N((object_ptr)&_lLevel_7504, concat_list, 3);
    }

    /** 	lPosA = 1*/
    _lPosA_7502 = 1;

    /** 	while( lPosA != 0 ) with entry do*/
    goto L1A; // [595] 616
L1B: 
    if (_lPosA_7502 == 0)
    goto L1C; // [598] 628

    /** 		lPath = eu:remove(lPath, lPosA, lPosA + 1)*/
    _4029 = _lPosA_7502 + 1;
    if (_4029 > MAXINT){
        _4029 = NewDouble((double)_4029);
    }
    {
        s1_ptr assign_space = SEQ_PTR(_lPath_7501);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_lPosA_7502)) ? _lPosA_7502 : (long)(DBL_PTR(_lPosA_7502)->dbl);
        int stop = (IS_ATOM_INT(_4029)) ? _4029 : (long)(DBL_PTR(_4029)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_lPath_7501), start, &_lPath_7501 );
            }
            else Tail(SEQ_PTR(_lPath_7501), stop+1, &_lPath_7501);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_lPath_7501), start, &_lPath_7501);
        }
        else {
            assign_slice_seq = &assign_space;
            _lPath_7501 = Remove_elements(start, stop, (SEQ_PTR(_lPath_7501)->ref == 1));
        }
    }
    DeRef(_4029);
    _4029 = NOVALUE;

    /** 	  entry*/
L1A: 

    /** 		lPosA = match(lLevel, lPath, lPosA )*/
    _lPosA_7502 = e_match_from(_lLevel_7504, _lPath_7501, _lPosA_7502);

    /** 	end while*/
    goto L1B; // [625] 598
L1C: 

    /** 	lLevel = SLASH & ".." & SLASH*/
    {
        int concat_list[3];

        concat_list[0] = 92;
        concat_list[1] = _3705;
        concat_list[2] = 92;
        Concat_N((object_ptr)&_lLevel_7504, concat_list, 3);
    }

    /** 	lPosB = 1*/
    _lPosB_7503 = 1;

    /** 	while( lPosA != 0 ) with entry do*/
    goto L1D; // [643] 721
L1E: 
    if (_lPosA_7502 == 0)
    goto L1F; // [646] 733

    /** 		lPosB = lPosA-1*/
    _lPosB_7503 = _lPosA_7502 - 1;

    /** 		while((lPosB > 0) and (lPath[lPosB] != SLASH)) do*/
L20: 
    _4035 = (_lPosB_7503 > 0);
    if (_4035 == 0) {
        DeRef(_4036);
        _4036 = 0;
        goto L21; // [665] 681
    }
    _2 = (int)SEQ_PTR(_lPath_7501);
    _4037 = (int)*(((s1_ptr)_2)->base + _lPosB_7503);
    if (IS_ATOM_INT(_4037)) {
        _4038 = (_4037 != 92);
    }
    else {
        _4038 = binary_op(NOTEQ, _4037, 92);
    }
    _4037 = NOVALUE;
    if (IS_ATOM_INT(_4038))
    _4036 = (_4038 != 0);
    else
    _4036 = DBL_PTR(_4038)->dbl != 0.0;
L21: 
    if (_4036 == 0)
    {
        _4036 = NOVALUE;
        goto L22; // [681] 695
    }
    else{
        _4036 = NOVALUE;
    }

    /** 			lPosB -= 1*/
    _lPosB_7503 = _lPosB_7503 - 1;

    /** 		end while*/
    goto L20; // [692] 661
L22: 

    /** 		if (lPosB <= 0) then*/
    if (_lPosB_7503 > 0)
    goto L23; // [697] 707

    /** 			lPosB = 1*/
    _lPosB_7503 = 1;
L23: 

    /** 		lPath = eu:remove(lPath, lPosB, lPosA + 2)*/
    _4041 = _lPosA_7502 + 2;
    if ((long)((unsigned long)_4041 + (unsigned long)HIGH_BITS) >= 0) 
    _4041 = NewDouble((double)_4041);
    {
        s1_ptr assign_space = SEQ_PTR(_lPath_7501);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_lPosB_7503)) ? _lPosB_7503 : (long)(DBL_PTR(_lPosB_7503)->dbl);
        int stop = (IS_ATOM_INT(_4041)) ? _4041 : (long)(DBL_PTR(_4041)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_lPath_7501), start, &_lPath_7501 );
            }
            else Tail(SEQ_PTR(_lPath_7501), stop+1, &_lPath_7501);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_lPath_7501), start, &_lPath_7501);
        }
        else {
            assign_slice_seq = &assign_space;
            _lPath_7501 = Remove_elements(start, stop, (SEQ_PTR(_lPath_7501)->ref == 1));
        }
    }
    DeRef(_4041);
    _4041 = NOVALUE;

    /** 	  entry*/
L1D: 

    /** 		lPosA = match(lLevel, lPath, lPosB )*/
    _lPosA_7502 = e_match_from(_lLevel_7504, _lPath_7501, _lPosB_7503);

    /** 	end while*/
    goto L1E; // [730] 646
L1F: 

    /** 	if case_flags = TO_LOWER then*/
    if (_case_flags_7500 != 1)
    goto L24; // [737] 752

    /** 		lPath = lower( lPath )*/
    RefDS(_lPath_7501);
    _0 = _lPath_7501;
    _lPath_7501 = _4lower(_lPath_7501);
    DeRefDS(_0);
    goto L25; // [749] 1407
L24: 

    /** 	elsif case_flags != AS_IS then*/
    if (_case_flags_7500 == 0)
    goto L26; // [756] 1404

    /** 		sequence sl = find_all(SLASH,lPath) -- split apart lPath*/
    RefDS(_lPath_7501);
    _0 = _sl_7647;
    _sl_7647 = _7find_all(92, _lPath_7501, 1);
    DeRef(_0);

    /** 		integer short_name = and_bits(TO_SHORT,case_flags)=TO_SHORT*/
    {unsigned long tu;
         tu = (unsigned long)4 & (unsigned long)_case_flags_7500;
         _4048 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_4048)) {
        _short_name_7650 = (_4048 == 4);
    }
    else {
        _short_name_7650 = (DBL_PTR(_4048)->dbl == (double)4);
    }
    DeRef(_4048);
    _4048 = NOVALUE;

    /** 		integer correct_name = and_bits(case_flags,CORRECT)=CORRECT*/
    {unsigned long tu;
         tu = (unsigned long)_case_flags_7500 & (unsigned long)2;
         _4050 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_4050)) {
        _correct_name_7653 = (_4050 == 2);
    }
    else {
        _correct_name_7653 = (DBL_PTR(_4050)->dbl == (double)2);
    }
    DeRef(_4050);
    _4050 = NOVALUE;

    /** 		integer lower_name = and_bits(TO_LOWER,case_flags)=TO_LOWER*/
    {unsigned long tu;
         tu = (unsigned long)1 & (unsigned long)_case_flags_7500;
         _4052 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_4052)) {
        _lower_name_7656 = (_4052 == 1);
    }
    else {
        _lower_name_7656 = (DBL_PTR(_4052)->dbl == (double)1);
    }
    DeRef(_4052);
    _4052 = NOVALUE;

    /** 		if lPath[$] != SLASH then*/
    if (IS_SEQUENCE(_lPath_7501)){
            _4054 = SEQ_PTR(_lPath_7501)->length;
    }
    else {
        _4054 = 1;
    }
    _2 = (int)SEQ_PTR(_lPath_7501);
    _4055 = (int)*(((s1_ptr)_2)->base + _4054);
    if (binary_op_a(EQUALS, _4055, 92)){
        _4055 = NOVALUE;
        goto L27; // [827] 849
    }
    _4055 = NOVALUE;

    /** 			sl = sl & {length(lPath)+1}*/
    if (IS_SEQUENCE(_lPath_7501)){
            _4057 = SEQ_PTR(_lPath_7501)->length;
    }
    else {
        _4057 = 1;
    }
    _4058 = _4057 + 1;
    _4057 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _4058;
    _4059 = MAKE_SEQ(_1);
    _4058 = NOVALUE;
    Concat((object_ptr)&_sl_7647, _sl_7647, _4059);
    DeRefDS(_4059);
    _4059 = NOVALUE;
L27: 

    /** 		for i = length(sl)-1 to 1 by -1 label "partloop" do*/
    if (IS_SEQUENCE(_sl_7647)){
            _4061 = SEQ_PTR(_sl_7647)->length;
    }
    else {
        _4061 = 1;
    }
    _4062 = _4061 - 1;
    _4061 = NOVALUE;
    {
        int _i_7668;
        _i_7668 = _4062;
L28: 
        if (_i_7668 < 1){
            goto L29; // [858] 1363
        }

        /** 			ifdef WINDOWS then*/

        /** 				sequence part = lDrive & lPath[1..sl[i]-1]*/
        _2 = (int)SEQ_PTR(_sl_7647);
        _4064 = (int)*(((s1_ptr)_2)->base + _i_7668);
        if (IS_ATOM_INT(_4064)) {
            _4065 = _4064 - 1;
        }
        else {
            _4065 = binary_op(MINUS, _4064, 1);
        }
        _4064 = NOVALUE;
        rhs_slice_target = (object_ptr)&_4066;
        RHS_Slice(_lPath_7501, 1, _4065);
        Concat((object_ptr)&_part_7672, _lDrive_7506, _4066);
        DeRefDS(_4066);
        _4066 = NOVALUE;

        /** 			object list = dir( part & SLASH )*/
        Append(&_4068, _part_7672, 92);
        DeRef(_name_inlined_dir_at_896_7680);
        _name_inlined_dir_at_896_7680 = _4068;
        _4068 = NOVALUE;

        /** 	ifdef WINDOWS then*/

        /** 		return machine_func(M_DIR, name)*/
        DeRef(_list_7677);
        _list_7677 = machine(22, _name_inlined_dir_at_896_7680);
        DeRef(_name_inlined_dir_at_896_7680);
        _name_inlined_dir_at_896_7680 = NOVALUE;

        /** 			sequence supplied_name = lPath[sl[i]+1..sl[i+1]-1]*/
        _2 = (int)SEQ_PTR(_sl_7647);
        _4069 = (int)*(((s1_ptr)_2)->base + _i_7668);
        if (IS_ATOM_INT(_4069)) {
            _4070 = _4069 + 1;
            if (_4070 > MAXINT){
                _4070 = NewDouble((double)_4070);
            }
        }
        else
        _4070 = binary_op(PLUS, 1, _4069);
        _4069 = NOVALUE;
        _4071 = _i_7668 + 1;
        _2 = (int)SEQ_PTR(_sl_7647);
        _4072 = (int)*(((s1_ptr)_2)->base + _4071);
        if (IS_ATOM_INT(_4072)) {
            _4073 = _4072 - 1;
        }
        else {
            _4073 = binary_op(MINUS, _4072, 1);
        }
        _4072 = NOVALUE;
        rhs_slice_target = (object_ptr)&_supplied_name_7682;
        RHS_Slice(_lPath_7501, _4070, _4073);

        /** 			if atom(list) then*/
        _4075 = IS_ATOM(_list_7677);
        if (_4075 == 0)
        {
            _4075 = NOVALUE;
            goto L2A; // [944] 982
        }
        else{
            _4075 = NOVALUE;
        }

        /** 				if lower_name then*/
        if (_lower_name_7656 == 0)
        {
            goto L2B; // [949] 975
        }
        else{
        }

        /** 					lPath = part & lower(lPath[sl[i]..$])*/
        _2 = (int)SEQ_PTR(_sl_7647);
        _4076 = (int)*(((s1_ptr)_2)->base + _i_7668);
        if (IS_SEQUENCE(_lPath_7501)){
                _4077 = SEQ_PTR(_lPath_7501)->length;
        }
        else {
            _4077 = 1;
        }
        rhs_slice_target = (object_ptr)&_4078;
        RHS_Slice(_lPath_7501, _4076, _4077);
        _4079 = _4lower(_4078);
        _4078 = NOVALUE;
        if (IS_SEQUENCE(_part_7672) && IS_ATOM(_4079)) {
            Ref(_4079);
            Append(&_lPath_7501, _part_7672, _4079);
        }
        else if (IS_ATOM(_part_7672) && IS_SEQUENCE(_4079)) {
        }
        else {
            Concat((object_ptr)&_lPath_7501, _part_7672, _4079);
        }
        DeRef(_4079);
        _4079 = NOVALUE;
L2B: 

        /** 				continue*/
        DeRef(_part_7672);
        _part_7672 = NOVALUE;
        DeRef(_list_7677);
        _list_7677 = NOVALUE;
        DeRef(_supplied_name_7682);
        _supplied_name_7682 = NOVALUE;
        goto L2C; // [979] 1358
L2A: 

        /** 			for j = 1 to length(list) do*/
        if (IS_SEQUENCE(_list_7677)){
                _4081 = SEQ_PTR(_list_7677)->length;
        }
        else {
            _4081 = 1;
        }
        {
            int _j_7699;
            _j_7699 = 1;
L2D: 
            if (_j_7699 > _4081){
                goto L2E; // [987] 1118
            }

            /** 				sequence read_name = list[j][D_NAME]*/
            _2 = (int)SEQ_PTR(_list_7677);
            _4082 = (int)*(((s1_ptr)_2)->base + _j_7699);
            DeRef(_read_name_7701);
            _2 = (int)SEQ_PTR(_4082);
            _read_name_7701 = (int)*(((s1_ptr)_2)->base + 1);
            Ref(_read_name_7701);
            _4082 = NOVALUE;

            /** 				if equal(read_name, supplied_name) then*/
            if (_read_name_7701 == _supplied_name_7682)
            _4084 = 1;
            else if (IS_ATOM_INT(_read_name_7701) && IS_ATOM_INT(_supplied_name_7682))
            _4084 = 0;
            else
            _4084 = (compare(_read_name_7701, _supplied_name_7682) == 0);
            if (_4084 == 0)
            {
                _4084 = NOVALUE;
                goto L2F; // [1014] 1109
            }
            else{
                _4084 = NOVALUE;
            }

            /** 					if short_name and sequence(list[j][D_ALTNAME]) then*/
            if (_short_name_7650 == 0) {
                goto L30; // [1019] 1100
            }
            _2 = (int)SEQ_PTR(_list_7677);
            _4086 = (int)*(((s1_ptr)_2)->base + _j_7699);
            _2 = (int)SEQ_PTR(_4086);
            _4087 = (int)*(((s1_ptr)_2)->base + 11);
            _4086 = NOVALUE;
            _4088 = IS_SEQUENCE(_4087);
            _4087 = NOVALUE;
            if (_4088 == 0)
            {
                _4088 = NOVALUE;
                goto L30; // [1037] 1100
            }
            else{
                _4088 = NOVALUE;
            }

            /** 						lPath = lPath[1..sl[i]] & list[j][D_ALTNAME] & lPath[sl[i+1]..$]*/
            _2 = (int)SEQ_PTR(_sl_7647);
            _4089 = (int)*(((s1_ptr)_2)->base + _i_7668);
            rhs_slice_target = (object_ptr)&_4090;
            RHS_Slice(_lPath_7501, 1, _4089);
            _2 = (int)SEQ_PTR(_list_7677);
            _4091 = (int)*(((s1_ptr)_2)->base + _j_7699);
            _2 = (int)SEQ_PTR(_4091);
            _4092 = (int)*(((s1_ptr)_2)->base + 11);
            _4091 = NOVALUE;
            _4093 = _i_7668 + 1;
            _2 = (int)SEQ_PTR(_sl_7647);
            _4094 = (int)*(((s1_ptr)_2)->base + _4093);
            if (IS_SEQUENCE(_lPath_7501)){
                    _4095 = SEQ_PTR(_lPath_7501)->length;
            }
            else {
                _4095 = 1;
            }
            rhs_slice_target = (object_ptr)&_4096;
            RHS_Slice(_lPath_7501, _4094, _4095);
            {
                int concat_list[3];

                concat_list[0] = _4096;
                concat_list[1] = _4092;
                concat_list[2] = _4090;
                Concat_N((object_ptr)&_lPath_7501, concat_list, 3);
            }
            DeRefDS(_4096);
            _4096 = NOVALUE;
            _4092 = NOVALUE;
            DeRefDS(_4090);
            _4090 = NOVALUE;

            /** 						sl[$] = length(lPath)+1*/
            if (IS_SEQUENCE(_sl_7647)){
                    _4098 = SEQ_PTR(_sl_7647)->length;
            }
            else {
                _4098 = 1;
            }
            if (IS_SEQUENCE(_lPath_7501)){
                    _4099 = SEQ_PTR(_lPath_7501)->length;
            }
            else {
                _4099 = 1;
            }
            _4100 = _4099 + 1;
            _4099 = NOVALUE;
            _2 = (int)SEQ_PTR(_sl_7647);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _sl_7647 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _4098);
            _1 = *(int *)_2;
            *(int *)_2 = _4100;
            if( _1 != _4100 ){
                DeRef(_1);
            }
            _4100 = NOVALUE;
L30: 

            /** 					continue "partloop"*/
            DeRef(_read_name_7701);
            _read_name_7701 = NOVALUE;
            DeRef(_part_7672);
            _part_7672 = NOVALUE;
            DeRef(_list_7677);
            _list_7677 = NOVALUE;
            DeRef(_supplied_name_7682);
            _supplied_name_7682 = NOVALUE;
            goto L2C; // [1106] 1358
L2F: 
            DeRef(_read_name_7701);
            _read_name_7701 = NOVALUE;

            /** 			end for*/
            _j_7699 = _j_7699 + 1;
            goto L2D; // [1113] 994
L2E: 
            ;
        }

        /** 			for j = 1 to length(list) do*/
        if (IS_SEQUENCE(_list_7677)){
                _4101 = SEQ_PTR(_list_7677)->length;
        }
        else {
            _4101 = 1;
        }
        {
            int _j_7724;
            _j_7724 = 1;
L31: 
            if (_j_7724 > _4101){
                goto L32; // [1123] 1301
            }

            /** 				sequence read_name = list[j][D_NAME]*/
            _2 = (int)SEQ_PTR(_list_7677);
            _4102 = (int)*(((s1_ptr)_2)->base + _j_7724);
            DeRef(_read_name_7726);
            _2 = (int)SEQ_PTR(_4102);
            _read_name_7726 = (int)*(((s1_ptr)_2)->base + 1);
            Ref(_read_name_7726);
            _4102 = NOVALUE;

            /** 				if equal(lower(read_name), lower(supplied_name)) then*/
            RefDS(_read_name_7726);
            _4104 = _4lower(_read_name_7726);
            RefDS(_supplied_name_7682);
            _4105 = _4lower(_supplied_name_7682);
            if (_4104 == _4105)
            _4106 = 1;
            else if (IS_ATOM_INT(_4104) && IS_ATOM_INT(_4105))
            _4106 = 0;
            else
            _4106 = (compare(_4104, _4105) == 0);
            DeRef(_4104);
            _4104 = NOVALUE;
            DeRef(_4105);
            _4105 = NOVALUE;
            if (_4106 == 0)
            {
                _4106 = NOVALUE;
                goto L33; // [1158] 1292
            }
            else{
                _4106 = NOVALUE;
            }

            /** 					if short_name and sequence(list[j][D_ALTNAME]) then*/
            if (_short_name_7650 == 0) {
                goto L34; // [1163] 1244
            }
            _2 = (int)SEQ_PTR(_list_7677);
            _4108 = (int)*(((s1_ptr)_2)->base + _j_7724);
            _2 = (int)SEQ_PTR(_4108);
            _4109 = (int)*(((s1_ptr)_2)->base + 11);
            _4108 = NOVALUE;
            _4110 = IS_SEQUENCE(_4109);
            _4109 = NOVALUE;
            if (_4110 == 0)
            {
                _4110 = NOVALUE;
                goto L34; // [1181] 1244
            }
            else{
                _4110 = NOVALUE;
            }

            /** 						lPath = lPath[1..sl[i]] & list[j][D_ALTNAME] & lPath[sl[i+1]..$]*/
            _2 = (int)SEQ_PTR(_sl_7647);
            _4111 = (int)*(((s1_ptr)_2)->base + _i_7668);
            rhs_slice_target = (object_ptr)&_4112;
            RHS_Slice(_lPath_7501, 1, _4111);
            _2 = (int)SEQ_PTR(_list_7677);
            _4113 = (int)*(((s1_ptr)_2)->base + _j_7724);
            _2 = (int)SEQ_PTR(_4113);
            _4114 = (int)*(((s1_ptr)_2)->base + 11);
            _4113 = NOVALUE;
            _4115 = _i_7668 + 1;
            _2 = (int)SEQ_PTR(_sl_7647);
            _4116 = (int)*(((s1_ptr)_2)->base + _4115);
            if (IS_SEQUENCE(_lPath_7501)){
                    _4117 = SEQ_PTR(_lPath_7501)->length;
            }
            else {
                _4117 = 1;
            }
            rhs_slice_target = (object_ptr)&_4118;
            RHS_Slice(_lPath_7501, _4116, _4117);
            {
                int concat_list[3];

                concat_list[0] = _4118;
                concat_list[1] = _4114;
                concat_list[2] = _4112;
                Concat_N((object_ptr)&_lPath_7501, concat_list, 3);
            }
            DeRefDS(_4118);
            _4118 = NOVALUE;
            _4114 = NOVALUE;
            DeRefDS(_4112);
            _4112 = NOVALUE;

            /** 						sl[$] = length(lPath)+1*/
            if (IS_SEQUENCE(_sl_7647)){
                    _4120 = SEQ_PTR(_sl_7647)->length;
            }
            else {
                _4120 = 1;
            }
            if (IS_SEQUENCE(_lPath_7501)){
                    _4121 = SEQ_PTR(_lPath_7501)->length;
            }
            else {
                _4121 = 1;
            }
            _4122 = _4121 + 1;
            _4121 = NOVALUE;
            _2 = (int)SEQ_PTR(_sl_7647);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _sl_7647 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _4120);
            _1 = *(int *)_2;
            *(int *)_2 = _4122;
            if( _1 != _4122 ){
                DeRef(_1);
            }
            _4122 = NOVALUE;
L34: 

            /** 					if correct_name then*/
            if (_correct_name_7653 == 0)
            {
                goto L35; // [1246] 1283
            }
            else{
            }

            /** 						lPath = lPath[1..sl[i]] & read_name & lPath[sl[i+1]..$]*/
            _2 = (int)SEQ_PTR(_sl_7647);
            _4123 = (int)*(((s1_ptr)_2)->base + _i_7668);
            rhs_slice_target = (object_ptr)&_4124;
            RHS_Slice(_lPath_7501, 1, _4123);
            _4125 = _i_7668 + 1;
            _2 = (int)SEQ_PTR(_sl_7647);
            _4126 = (int)*(((s1_ptr)_2)->base + _4125);
            if (IS_SEQUENCE(_lPath_7501)){
                    _4127 = SEQ_PTR(_lPath_7501)->length;
            }
            else {
                _4127 = 1;
            }
            rhs_slice_target = (object_ptr)&_4128;
            RHS_Slice(_lPath_7501, _4126, _4127);
            {
                int concat_list[3];

                concat_list[0] = _4128;
                concat_list[1] = _read_name_7726;
                concat_list[2] = _4124;
                Concat_N((object_ptr)&_lPath_7501, concat_list, 3);
            }
            DeRefDS(_4128);
            _4128 = NOVALUE;
            DeRefDS(_4124);
            _4124 = NOVALUE;
L35: 

            /** 					continue "partloop"*/
            DeRef(_read_name_7726);
            _read_name_7726 = NOVALUE;
            DeRef(_part_7672);
            _part_7672 = NOVALUE;
            DeRef(_list_7677);
            _list_7677 = NOVALUE;
            DeRef(_supplied_name_7682);
            _supplied_name_7682 = NOVALUE;
            goto L2C; // [1289] 1358
L33: 
            DeRef(_read_name_7726);
            _read_name_7726 = NOVALUE;

            /** 			end for*/
            _j_7724 = _j_7724 + 1;
            goto L31; // [1296] 1130
L32: 
            ;
        }

        /** 			if and_bits(TO_LOWER,case_flags) then*/
        {unsigned long tu;
             tu = (unsigned long)1 & (unsigned long)_case_flags_7500;
             _4130 = MAKE_UINT(tu);
        }
        if (_4130 == 0) {
            DeRef(_4130);
            _4130 = NOVALUE;
            goto L36; // [1309] 1348
        }
        else {
            if (!IS_ATOM_INT(_4130) && DBL_PTR(_4130)->dbl == 0.0){
                DeRef(_4130);
                _4130 = NOVALUE;
                goto L36; // [1309] 1348
            }
            DeRef(_4130);
            _4130 = NOVALUE;
        }
        DeRef(_4130);
        _4130 = NOVALUE;

        /** 				lPath = lPath[1..sl[i]-1] & lower(lPath[sl[i]..$])*/
        _2 = (int)SEQ_PTR(_sl_7647);
        _4131 = (int)*(((s1_ptr)_2)->base + _i_7668);
        if (IS_ATOM_INT(_4131)) {
            _4132 = _4131 - 1;
        }
        else {
            _4132 = binary_op(MINUS, _4131, 1);
        }
        _4131 = NOVALUE;
        rhs_slice_target = (object_ptr)&_4133;
        RHS_Slice(_lPath_7501, 1, _4132);
        _2 = (int)SEQ_PTR(_sl_7647);
        _4134 = (int)*(((s1_ptr)_2)->base + _i_7668);
        if (IS_SEQUENCE(_lPath_7501)){
                _4135 = SEQ_PTR(_lPath_7501)->length;
        }
        else {
            _4135 = 1;
        }
        rhs_slice_target = (object_ptr)&_4136;
        RHS_Slice(_lPath_7501, _4134, _4135);
        _4137 = _4lower(_4136);
        _4136 = NOVALUE;
        if (IS_SEQUENCE(_4133) && IS_ATOM(_4137)) {
            Ref(_4137);
            Append(&_lPath_7501, _4133, _4137);
        }
        else if (IS_ATOM(_4133) && IS_SEQUENCE(_4137)) {
        }
        else {
            Concat((object_ptr)&_lPath_7501, _4133, _4137);
            DeRefDS(_4133);
            _4133 = NOVALUE;
        }
        DeRef(_4133);
        _4133 = NOVALUE;
        DeRef(_4137);
        _4137 = NOVALUE;
L36: 

        /** 			exit*/
        DeRef(_part_7672);
        _part_7672 = NOVALUE;
        DeRef(_list_7677);
        _list_7677 = NOVALUE;
        DeRef(_supplied_name_7682);
        _supplied_name_7682 = NOVALUE;
        goto L29; // [1352] 1363

        /** 		end for*/
L2C: 
        _i_7668 = _i_7668 + -1;
        goto L28; // [1358] 865
L29: 
        ;
    }

    /** 		if and_bits(case_flags,or_bits(CORRECT,TO_LOWER))=TO_LOWER and length(lPath) then*/
    {unsigned long tu;
         tu = (unsigned long)2 | (unsigned long)1;
         _4139 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_4139)) {
        {unsigned long tu;
             tu = (unsigned long)_case_flags_7500 & (unsigned long)_4139;
             _4140 = MAKE_UINT(tu);
        }
    }
    else {
        temp_d.dbl = (double)_case_flags_7500;
        _4140 = Dand_bits(&temp_d, DBL_PTR(_4139));
    }
    DeRef(_4139);
    _4139 = NOVALUE;
    if (IS_ATOM_INT(_4140)) {
        _4141 = (_4140 == 1);
    }
    else {
        _4141 = (DBL_PTR(_4140)->dbl == (double)1);
    }
    DeRef(_4140);
    _4140 = NOVALUE;
    if (_4141 == 0) {
        goto L37; // [1383] 1403
    }
    if (IS_SEQUENCE(_lPath_7501)){
            _4143 = SEQ_PTR(_lPath_7501)->length;
    }
    else {
        _4143 = 1;
    }
    if (_4143 == 0)
    {
        _4143 = NOVALUE;
        goto L37; // [1391] 1403
    }
    else{
        _4143 = NOVALUE;
    }

    /** 			lPath = lower(lPath)*/
    RefDS(_lPath_7501);
    _0 = _lPath_7501;
    _lPath_7501 = _4lower(_lPath_7501);
    DeRefDS(_0);
L37: 
L26: 
    DeRef(_sl_7647);
    _sl_7647 = NOVALUE;
L25: 

    /** 	ifdef WINDOWS then*/

    /** 		if and_bits(CORRECT,case_flags) then*/
    {unsigned long tu;
         tu = (unsigned long)2 & (unsigned long)_case_flags_7500;
         _4145 = MAKE_UINT(tu);
    }
    if (_4145 == 0) {
        DeRef(_4145);
        _4145 = NOVALUE;
        goto L38; // [1417] 1459
    }
    else {
        if (!IS_ATOM_INT(_4145) && DBL_PTR(_4145)->dbl == 0.0){
            DeRef(_4145);
            _4145 = NOVALUE;
            goto L38; // [1417] 1459
        }
        DeRef(_4145);
        _4145 = NOVALUE;
    }
    DeRef(_4145);
    _4145 = NOVALUE;

    /** 			if or_bits(system_drive_case,'A') = 'a' then*/
    if (IS_ATOM_INT(_9system_drive_case_7484)) {
        {unsigned long tu;
             tu = (unsigned long)_9system_drive_case_7484 | (unsigned long)65;
             _4146 = MAKE_UINT(tu);
        }
    }
    else {
        temp_d.dbl = (double)65;
        _4146 = Dor_bits(DBL_PTR(_9system_drive_case_7484), &temp_d);
    }
    if (binary_op_a(NOTEQ, _4146, 97)){
        DeRef(_4146);
        _4146 = NOVALUE;
        goto L39; // [1428] 1445
    }
    DeRef(_4146);
    _4146 = NOVALUE;

    /** 				lDrive = lower(lDrive)*/
    RefDS(_lDrive_7506);
    _0 = _lDrive_7506;
    _lDrive_7506 = _4lower(_lDrive_7506);
    DeRefDS(_0);
    goto L3A; // [1442] 1482
L39: 

    /** 				lDrive = upper(lDrive)*/
    RefDS(_lDrive_7506);
    _0 = _lDrive_7506;
    _lDrive_7506 = _4upper(_lDrive_7506);
    DeRefDS(_0);
    goto L3A; // [1456] 1482
L38: 

    /** 		elsif and_bits(TO_LOWER,case_flags) then*/
    {unsigned long tu;
         tu = (unsigned long)1 & (unsigned long)_case_flags_7500;
         _4150 = MAKE_UINT(tu);
    }
    if (_4150 == 0) {
        DeRef(_4150);
        _4150 = NOVALUE;
        goto L3B; // [1467] 1481
    }
    else {
        if (!IS_ATOM_INT(_4150) && DBL_PTR(_4150)->dbl == 0.0){
            DeRef(_4150);
            _4150 = NOVALUE;
            goto L3B; // [1467] 1481
        }
        DeRef(_4150);
        _4150 = NOVALUE;
    }
    DeRef(_4150);
    _4150 = NOVALUE;

    /** 			lDrive = lower(lDrive)*/
    RefDS(_lDrive_7506);
    _0 = _lDrive_7506;
    _lDrive_7506 = _4lower(_lDrive_7506);
    DeRefDS(_0);
L3B: 
L3A: 

    /** 		lPath = lDrive & lPath*/
    Concat((object_ptr)&_lPath_7501, _lDrive_7506, _lPath_7501);

    /** 	return lPath & wildcard_suffix*/
    Concat((object_ptr)&_4153, _lPath_7501, _wildcard_suffix_7571);
    DeRefDS(_path_in_7498);
    DeRefDS(_lPath_7501);
    DeRefi(_lLevel_7504);
    DeRef(_lHome_7505);
    DeRefDS(_lDrive_7506);
    DeRefDS(_wildcard_suffix_7571);
    DeRef(_3958);
    _3958 = NOVALUE;
    DeRef(_4125);
    _4125 = NOVALUE;
    DeRef(_4012);
    _4012 = NOVALUE;
    DeRef(_4062);
    _4062 = NOVALUE;
    DeRef(_3952);
    _3952 = NOVALUE;
    DeRef(_3946);
    _3946 = NOVALUE;
    DeRef(_4065);
    _4065 = NOVALUE;
    _4111 = NOVALUE;
    DeRef(_4115);
    _4115 = NOVALUE;
    _4116 = NOVALUE;
    _4094 = NOVALUE;
    DeRef(_3955);
    _3955 = NOVALUE;
    DeRef(_3998);
    _3998 = NOVALUE;
    DeRef(_4035);
    _4035 = NOVALUE;
    DeRef(_3983);
    _3983 = NOVALUE;
    DeRef(_3986);
    _3986 = NOVALUE;
    _4089 = NOVALUE;
    DeRef(_4093);
    _4093 = NOVALUE;
    DeRef(_4071);
    _4071 = NOVALUE;
    _4134 = NOVALUE;
    _4076 = NOVALUE;
    DeRef(_3972);
    _3972 = NOVALUE;
    _4123 = NOVALUE;
    _4126 = NOVALUE;
    DeRef(_3943);
    _3943 = NOVALUE;
    DeRef(_4025);
    _4025 = NOVALUE;
    DeRef(_4073);
    _4073 = NOVALUE;
    DeRef(_4015);
    _4015 = NOVALUE;
    DeRef(_4038);
    _4038 = NOVALUE;
    DeRef(_4003);
    _4003 = NOVALUE;
    DeRef(_3950);
    _3950 = NOVALUE;
    DeRef(_4021);
    _4021 = NOVALUE;
    DeRef(_4132);
    _4132 = NOVALUE;
    DeRef(_4141);
    _4141 = NOVALUE;
    DeRef(_4070);
    _4070 = NOVALUE;
    return _4153;
    ;
}


int _9fs_case(int _s_7797)
{
    int _4154 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		return lower(s)*/
    RefDS(_s_7797);
    _4154 = _4lower(_s_7797);
    DeRefDS(_s_7797);
    return _4154;
    ;
}


int _9abbreviate_path(int _orig_path_7802, int _base_paths_7803)
{
    int _expanded_path_7804 = NOVALUE;
    int _lowered_expanded_path_7814 = NOVALUE;
    int _4199 = NOVALUE;
    int _4198 = NOVALUE;
    int _4195 = NOVALUE;
    int _4194 = NOVALUE;
    int _4193 = NOVALUE;
    int _4192 = NOVALUE;
    int _4191 = NOVALUE;
    int _4189 = NOVALUE;
    int _4188 = NOVALUE;
    int _4187 = NOVALUE;
    int _4186 = NOVALUE;
    int _4185 = NOVALUE;
    int _4184 = NOVALUE;
    int _4183 = NOVALUE;
    int _4182 = NOVALUE;
    int _4181 = NOVALUE;
    int _4178 = NOVALUE;
    int _4177 = NOVALUE;
    int _4175 = NOVALUE;
    int _4174 = NOVALUE;
    int _4173 = NOVALUE;
    int _4172 = NOVALUE;
    int _4171 = NOVALUE;
    int _4170 = NOVALUE;
    int _4169 = NOVALUE;
    int _4168 = NOVALUE;
    int _4167 = NOVALUE;
    int _4166 = NOVALUE;
    int _4165 = NOVALUE;
    int _4164 = NOVALUE;
    int _4163 = NOVALUE;
    int _4160 = NOVALUE;
    int _4159 = NOVALUE;
    int _4158 = NOVALUE;
    int _4156 = NOVALUE;
    int _0, _1, _2;
    

    /** 	expanded_path = canonical_path(orig_path)*/
    RefDS(_orig_path_7802);
    _0 = _expanded_path_7804;
    _expanded_path_7804 = _9canonical_path(_orig_path_7802, 0, 0);
    DeRef(_0);

    /** 	base_paths = append(base_paths, curdir())*/
    _4156 = _9curdir(0);
    Ref(_4156);
    Append(&_base_paths_7803, _base_paths_7803, _4156);
    DeRef(_4156);
    _4156 = NOVALUE;

    /** 	for i = 1 to length(base_paths) do*/
    if (IS_SEQUENCE(_base_paths_7803)){
            _4158 = SEQ_PTR(_base_paths_7803)->length;
    }
    else {
        _4158 = 1;
    }
    {
        int _i_7809;
        _i_7809 = 1;
L1: 
        if (_i_7809 > _4158){
            goto L2; // [32] 64
        }

        /** 		base_paths[i] = canonical_path(base_paths[i], 1) -- assume each base path is meant to be a directory.*/
        _2 = (int)SEQ_PTR(_base_paths_7803);
        _4159 = (int)*(((s1_ptr)_2)->base + _i_7809);
        Ref(_4159);
        _4160 = _9canonical_path(_4159, 1, 0);
        _4159 = NOVALUE;
        _2 = (int)SEQ_PTR(_base_paths_7803);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _base_paths_7803 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_7809);
        _1 = *(int *)_2;
        *(int *)_2 = _4160;
        if( _1 != _4160 ){
            DeRef(_1);
        }
        _4160 = NOVALUE;

        /** 	end for*/
        _i_7809 = _i_7809 + 1;
        goto L1; // [59] 39
L2: 
        ;
    }

    /** 	base_paths = fs_case(base_paths)*/
    RefDS(_base_paths_7803);
    _0 = _base_paths_7803;
    _base_paths_7803 = _9fs_case(_base_paths_7803);
    DeRefDS(_0);

    /** 	sequence lowered_expanded_path = fs_case(expanded_path)*/
    RefDS(_expanded_path_7804);
    _0 = _lowered_expanded_path_7814;
    _lowered_expanded_path_7814 = _9fs_case(_expanded_path_7804);
    DeRef(_0);

    /** 	for i = 1 to length(base_paths) do*/
    if (IS_SEQUENCE(_base_paths_7803)){
            _4163 = SEQ_PTR(_base_paths_7803)->length;
    }
    else {
        _4163 = 1;
    }
    {
        int _i_7817;
        _i_7817 = 1;
L3: 
        if (_i_7817 > _4163){
            goto L4; // [85] 139
        }

        /** 		if search:begins(base_paths[i], lowered_expanded_path) then*/
        _2 = (int)SEQ_PTR(_base_paths_7803);
        _4164 = (int)*(((s1_ptr)_2)->base + _i_7817);
        Ref(_4164);
        RefDS(_lowered_expanded_path_7814);
        _4165 = _7begins(_4164, _lowered_expanded_path_7814);
        _4164 = NOVALUE;
        if (_4165 == 0) {
            DeRef(_4165);
            _4165 = NOVALUE;
            goto L5; // [103] 132
        }
        else {
            if (!IS_ATOM_INT(_4165) && DBL_PTR(_4165)->dbl == 0.0){
                DeRef(_4165);
                _4165 = NOVALUE;
                goto L5; // [103] 132
            }
            DeRef(_4165);
            _4165 = NOVALUE;
        }
        DeRef(_4165);
        _4165 = NOVALUE;

        /** 			return expanded_path[length(base_paths[i]) + 1 .. $]*/
        _2 = (int)SEQ_PTR(_base_paths_7803);
        _4166 = (int)*(((s1_ptr)_2)->base + _i_7817);
        if (IS_SEQUENCE(_4166)){
                _4167 = SEQ_PTR(_4166)->length;
        }
        else {
            _4167 = 1;
        }
        _4166 = NOVALUE;
        _4168 = _4167 + 1;
        _4167 = NOVALUE;
        if (IS_SEQUENCE(_expanded_path_7804)){
                _4169 = SEQ_PTR(_expanded_path_7804)->length;
        }
        else {
            _4169 = 1;
        }
        rhs_slice_target = (object_ptr)&_4170;
        RHS_Slice(_expanded_path_7804, _4168, _4169);
        DeRefDS(_orig_path_7802);
        DeRefDS(_base_paths_7803);
        DeRefDS(_expanded_path_7804);
        DeRefDS(_lowered_expanded_path_7814);
        _4166 = NOVALUE;
        _4168 = NOVALUE;
        return _4170;
L5: 

        /** 	end for*/
        _i_7817 = _i_7817 + 1;
        goto L3; // [134] 92
L4: 
        ;
    }

    /** 	ifdef WINDOWS then*/

    /** 		if not equal(base_paths[$][1], lowered_expanded_path[1]) then*/
    if (IS_SEQUENCE(_base_paths_7803)){
            _4171 = SEQ_PTR(_base_paths_7803)->length;
    }
    else {
        _4171 = 1;
    }
    _2 = (int)SEQ_PTR(_base_paths_7803);
    _4172 = (int)*(((s1_ptr)_2)->base + _4171);
    _2 = (int)SEQ_PTR(_4172);
    _4173 = (int)*(((s1_ptr)_2)->base + 1);
    _4172 = NOVALUE;
    _2 = (int)SEQ_PTR(_lowered_expanded_path_7814);
    _4174 = (int)*(((s1_ptr)_2)->base + 1);
    if (_4173 == _4174)
    _4175 = 1;
    else if (IS_ATOM_INT(_4173) && IS_ATOM_INT(_4174))
    _4175 = 0;
    else
    _4175 = (compare(_4173, _4174) == 0);
    _4173 = NOVALUE;
    _4174 = NOVALUE;
    if (_4175 != 0)
    goto L6; // [162] 172
    _4175 = NOVALUE;

    /** 			return orig_path*/
    DeRefDS(_base_paths_7803);
    DeRef(_expanded_path_7804);
    DeRefDS(_lowered_expanded_path_7814);
    _4166 = NOVALUE;
    DeRef(_4168);
    _4168 = NOVALUE;
    DeRef(_4170);
    _4170 = NOVALUE;
    return _orig_path_7802;
L6: 

    /** 	base_paths = stdseq:split(base_paths[$], SLASH)*/
    if (IS_SEQUENCE(_base_paths_7803)){
            _4177 = SEQ_PTR(_base_paths_7803)->length;
    }
    else {
        _4177 = 1;
    }
    _2 = (int)SEQ_PTR(_base_paths_7803);
    _4178 = (int)*(((s1_ptr)_2)->base + _4177);
    Ref(_4178);
    _0 = _base_paths_7803;
    _base_paths_7803 = _21split(_4178, 92, 0, 0);
    DeRefDS(_0);
    _4178 = NOVALUE;

    /** 	expanded_path = stdseq:split(expanded_path, SLASH)*/
    RefDS(_expanded_path_7804);
    _0 = _expanded_path_7804;
    _expanded_path_7804 = _21split(_expanded_path_7804, 92, 0, 0);
    DeRefDS(_0);

    /** 	lowered_expanded_path = ""*/
    RefDS(_5);
    DeRef(_lowered_expanded_path_7814);
    _lowered_expanded_path_7814 = _5;

    /** 	for i = 1 to math:min({length(expanded_path), length(base_paths) - 1}) do*/
    if (IS_SEQUENCE(_expanded_path_7804)){
            _4181 = SEQ_PTR(_expanded_path_7804)->length;
    }
    else {
        _4181 = 1;
    }
    if (IS_SEQUENCE(_base_paths_7803)){
            _4182 = SEQ_PTR(_base_paths_7803)->length;
    }
    else {
        _4182 = 1;
    }
    _4183 = _4182 - 1;
    _4182 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _4181;
    ((int *)_2)[2] = _4183;
    _4184 = MAKE_SEQ(_1);
    _4183 = NOVALUE;
    _4181 = NOVALUE;
    _4185 = _18min(_4184);
    _4184 = NOVALUE;
    {
        int _i_7839;
        _i_7839 = 1;
L7: 
        if (binary_op_a(GREATER, _i_7839, _4185)){
            goto L8; // [228] 321
        }

        /** 		if not equal(fs_case(expanded_path[i]), base_paths[i]) then*/
        _2 = (int)SEQ_PTR(_expanded_path_7804);
        if (!IS_ATOM_INT(_i_7839)){
            _4186 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_7839)->dbl));
        }
        else{
            _4186 = (int)*(((s1_ptr)_2)->base + _i_7839);
        }
        Ref(_4186);
        _4187 = _9fs_case(_4186);
        _4186 = NOVALUE;
        _2 = (int)SEQ_PTR(_base_paths_7803);
        if (!IS_ATOM_INT(_i_7839)){
            _4188 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_7839)->dbl));
        }
        else{
            _4188 = (int)*(((s1_ptr)_2)->base + _i_7839);
        }
        if (_4187 == _4188)
        _4189 = 1;
        else if (IS_ATOM_INT(_4187) && IS_ATOM_INT(_4188))
        _4189 = 0;
        else
        _4189 = (compare(_4187, _4188) == 0);
        DeRef(_4187);
        _4187 = NOVALUE;
        _4188 = NOVALUE;
        if (_4189 != 0)
        goto L9; // [253] 314
        _4189 = NOVALUE;

        /** 			expanded_path = repeat("..", length(base_paths) - i) & expanded_path[i .. $]*/
        if (IS_SEQUENCE(_base_paths_7803)){
                _4191 = SEQ_PTR(_base_paths_7803)->length;
        }
        else {
            _4191 = 1;
        }
        if (IS_ATOM_INT(_i_7839)) {
            _4192 = _4191 - _i_7839;
        }
        else {
            _4192 = NewDouble((double)_4191 - DBL_PTR(_i_7839)->dbl);
        }
        _4191 = NOVALUE;
        _4193 = Repeat(_3705, _4192);
        DeRef(_4192);
        _4192 = NOVALUE;
        if (IS_SEQUENCE(_expanded_path_7804)){
                _4194 = SEQ_PTR(_expanded_path_7804)->length;
        }
        else {
            _4194 = 1;
        }
        rhs_slice_target = (object_ptr)&_4195;
        RHS_Slice(_expanded_path_7804, _i_7839, _4194);
        Concat((object_ptr)&_expanded_path_7804, _4193, _4195);
        DeRefDS(_4193);
        _4193 = NOVALUE;
        DeRef(_4193);
        _4193 = NOVALUE;
        DeRefDS(_4195);
        _4195 = NOVALUE;

        /** 			expanded_path = stdseq:join(expanded_path, SLASH)*/
        RefDS(_expanded_path_7804);
        _0 = _expanded_path_7804;
        _expanded_path_7804 = _21join(_expanded_path_7804, 92);
        DeRefDS(_0);

        /** 			if length(expanded_path) < length(orig_path) then*/
        if (IS_SEQUENCE(_expanded_path_7804)){
                _4198 = SEQ_PTR(_expanded_path_7804)->length;
        }
        else {
            _4198 = 1;
        }
        if (IS_SEQUENCE(_orig_path_7802)){
                _4199 = SEQ_PTR(_orig_path_7802)->length;
        }
        else {
            _4199 = 1;
        }
        if (_4198 >= _4199)
        goto L8; // [298] 321

        /** 		  		return expanded_path*/
        DeRef(_i_7839);
        DeRefDS(_orig_path_7802);
        DeRefDS(_base_paths_7803);
        DeRef(_lowered_expanded_path_7814);
        _4166 = NOVALUE;
        DeRef(_4168);
        _4168 = NOVALUE;
        DeRef(_4170);
        _4170 = NOVALUE;
        DeRef(_4185);
        _4185 = NOVALUE;
        return _expanded_path_7804;

        /** 			exit*/
        goto L8; // [311] 321
L9: 

        /** 	end for*/
        _0 = _i_7839;
        if (IS_ATOM_INT(_i_7839)) {
            _i_7839 = _i_7839 + 1;
            if ((long)((unsigned long)_i_7839 +(unsigned long) HIGH_BITS) >= 0){
                _i_7839 = NewDouble((double)_i_7839);
            }
        }
        else {
            _i_7839 = binary_op_a(PLUS, _i_7839, 1);
        }
        DeRef(_0);
        goto L7; // [316] 235
L8: 
        ;
        DeRef(_i_7839);
    }

    /** 	return orig_path*/
    DeRefDS(_base_paths_7803);
    DeRef(_expanded_path_7804);
    DeRef(_lowered_expanded_path_7814);
    _4166 = NOVALUE;
    DeRef(_4168);
    _4168 = NOVALUE;
    DeRef(_4170);
    _4170 = NOVALUE;
    DeRef(_4185);
    _4185 = NOVALUE;
    return _orig_path_7802;
    ;
}


int _9split_path(int _fname_7864)
{
    int _4201 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return stdseq:split(fname, SLASH, 1)*/
    RefDS(_fname_7864);
    _4201 = _21split(_fname_7864, 92, 1, 0);
    DeRefDS(_fname_7864);
    return _4201;
    ;
}


int _9join_path(int _path_elements_7868)
{
    int _fname_7869 = NOVALUE;
    int _elem_7873 = NOVALUE;
    int _4215 = NOVALUE;
    int _4214 = NOVALUE;
    int _4213 = NOVALUE;
    int _4212 = NOVALUE;
    int _4211 = NOVALUE;
    int _4210 = NOVALUE;
    int _4208 = NOVALUE;
    int _4207 = NOVALUE;
    int _4205 = NOVALUE;
    int _4204 = NOVALUE;
    int _4202 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence fname = ""*/
    RefDS(_5);
    DeRef(_fname_7869);
    _fname_7869 = _5;

    /** 	for i = 1 to length(path_elements) do*/
    if (IS_SEQUENCE(_path_elements_7868)){
            _4202 = SEQ_PTR(_path_elements_7868)->length;
    }
    else {
        _4202 = 1;
    }
    {
        int _i_7871;
        _i_7871 = 1;
L1: 
        if (_i_7871 > _4202){
            goto L2; // [15] 117
        }

        /** 		sequence elem = path_elements[i]*/
        DeRef(_elem_7873);
        _2 = (int)SEQ_PTR(_path_elements_7868);
        _elem_7873 = (int)*(((s1_ptr)_2)->base + _i_7871);
        Ref(_elem_7873);

        /** 		if elem[$] = SLASH then*/
        if (IS_SEQUENCE(_elem_7873)){
                _4204 = SEQ_PTR(_elem_7873)->length;
        }
        else {
            _4204 = 1;
        }
        _2 = (int)SEQ_PTR(_elem_7873);
        _4205 = (int)*(((s1_ptr)_2)->base + _4204);
        if (binary_op_a(NOTEQ, _4205, 92)){
            _4205 = NOVALUE;
            goto L3; // [39] 58
        }
        _4205 = NOVALUE;

        /** 			elem = elem[1..$ - 1]*/
        if (IS_SEQUENCE(_elem_7873)){
                _4207 = SEQ_PTR(_elem_7873)->length;
        }
        else {
            _4207 = 1;
        }
        _4208 = _4207 - 1;
        _4207 = NOVALUE;
        rhs_slice_target = (object_ptr)&_elem_7873;
        RHS_Slice(_elem_7873, 1, _4208);
L3: 

        /** 		if length(elem) and elem[1] != SLASH then*/
        if (IS_SEQUENCE(_elem_7873)){
                _4210 = SEQ_PTR(_elem_7873)->length;
        }
        else {
            _4210 = 1;
        }
        if (_4210 == 0) {
            goto L4; // [63] 102
        }
        _2 = (int)SEQ_PTR(_elem_7873);
        _4212 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_4212)) {
            _4213 = (_4212 != 92);
        }
        else {
            _4213 = binary_op(NOTEQ, _4212, 92);
        }
        _4212 = NOVALUE;
        if (_4213 == 0) {
            DeRef(_4213);
            _4213 = NOVALUE;
            goto L4; // [76] 102
        }
        else {
            if (!IS_ATOM_INT(_4213) && DBL_PTR(_4213)->dbl == 0.0){
                DeRef(_4213);
                _4213 = NOVALUE;
                goto L4; // [76] 102
            }
            DeRef(_4213);
            _4213 = NOVALUE;
        }
        DeRef(_4213);
        _4213 = NOVALUE;

        /** 			ifdef WINDOWS then*/

        /** 				if elem[$] != ':' then*/
        if (IS_SEQUENCE(_elem_7873)){
                _4214 = SEQ_PTR(_elem_7873)->length;
        }
        else {
            _4214 = 1;
        }
        _2 = (int)SEQ_PTR(_elem_7873);
        _4215 = (int)*(((s1_ptr)_2)->base + _4214);
        if (binary_op_a(EQUALS, _4215, 58)){
            _4215 = NOVALUE;
            goto L5; // [90] 101
        }
        _4215 = NOVALUE;

        /** 					elem = SLASH & elem*/
        Prepend(&_elem_7873, _elem_7873, 92);
L5: 
L4: 

        /** 		fname &= elem*/
        Concat((object_ptr)&_fname_7869, _fname_7869, _elem_7873);
        DeRefDS(_elem_7873);
        _elem_7873 = NOVALUE;

        /** 	end for*/
        _i_7871 = _i_7871 + 1;
        goto L1; // [112] 22
L2: 
        ;
    }

    /** 	return fname*/
    DeRefDS(_path_elements_7868);
    DeRef(_4208);
    _4208 = NOVALUE;
    return _fname_7869;
    ;
}


int _9file_type(int _filename_7902)
{
    int _dirfil_7903 = NOVALUE;
    int _dir_inlined_dir_at_66_7916 = NOVALUE;
    int _4243 = NOVALUE;
    int _4242 = NOVALUE;
    int _4241 = NOVALUE;
    int _4240 = NOVALUE;
    int _4239 = NOVALUE;
    int _4237 = NOVALUE;
    int _4236 = NOVALUE;
    int _4235 = NOVALUE;
    int _4234 = NOVALUE;
    int _4233 = NOVALUE;
    int _4232 = NOVALUE;
    int _4231 = NOVALUE;
    int _4229 = NOVALUE;
    int _4228 = NOVALUE;
    int _4227 = NOVALUE;
    int _4226 = NOVALUE;
    int _4225 = NOVALUE;
    int _4224 = NOVALUE;
    int _4222 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if eu:find('*', filename) or eu:find('?', filename) then return FILETYPE_UNDEFINED end if*/
    _4222 = find_from(42, _filename_7902, 1);
    if (_4222 != 0) {
        goto L1; // [10] 24
    }
    _4224 = find_from(63, _filename_7902, 1);
    if (_4224 == 0)
    {
        _4224 = NOVALUE;
        goto L2; // [20] 31
    }
    else{
        _4224 = NOVALUE;
    }
L1: 
    DeRefDS(_filename_7902);
    DeRef(_dirfil_7903);
    return -1;
L2: 

    /** 	ifdef WINDOWS then*/

    /** 		if length(filename) = 2 and filename[2] = ':' then*/
    if (IS_SEQUENCE(_filename_7902)){
            _4225 = SEQ_PTR(_filename_7902)->length;
    }
    else {
        _4225 = 1;
    }
    _4226 = (_4225 == 2);
    _4225 = NOVALUE;
    if (_4226 == 0) {
        goto L3; // [42] 65
    }
    _2 = (int)SEQ_PTR(_filename_7902);
    _4228 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_4228)) {
        _4229 = (_4228 == 58);
    }
    else {
        _4229 = binary_op(EQUALS, _4228, 58);
    }
    _4228 = NOVALUE;
    if (_4229 == 0) {
        DeRef(_4229);
        _4229 = NOVALUE;
        goto L3; // [55] 65
    }
    else {
        if (!IS_ATOM_INT(_4229) && DBL_PTR(_4229)->dbl == 0.0){
            DeRef(_4229);
            _4229 = NOVALUE;
            goto L3; // [55] 65
        }
        DeRef(_4229);
        _4229 = NOVALUE;
    }
    DeRef(_4229);
    _4229 = NOVALUE;

    /** 			filename &= "\\"*/
    Concat((object_ptr)&_filename_7902, _filename_7902, _731);
L3: 

    /** 	dirfil = dir(filename)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_dirfil_7903);
    _dirfil_7903 = machine(22, _filename_7902);

    /** 	if sequence(dirfil) then*/
    _4231 = IS_SEQUENCE(_dirfil_7903);
    if (_4231 == 0)
    {
        _4231 = NOVALUE;
        goto L4; // [81] 169
    }
    else{
        _4231 = NOVALUE;
    }

    /** 		if length( dirfil ) > 1 or eu:find('d', dirfil[1][2]) or (length(filename)=3 and filename[2]=':') then*/
    if (IS_SEQUENCE(_dirfil_7903)){
            _4232 = SEQ_PTR(_dirfil_7903)->length;
    }
    else {
        _4232 = 1;
    }
    _4233 = (_4232 > 1);
    _4232 = NOVALUE;
    if (_4233 != 0) {
        _4234 = 1;
        goto L5; // [93] 114
    }
    _2 = (int)SEQ_PTR(_dirfil_7903);
    _4235 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4235);
    _4236 = (int)*(((s1_ptr)_2)->base + 2);
    _4235 = NOVALUE;
    _4237 = find_from(100, _4236, 1);
    _4236 = NOVALUE;
    _4234 = (_4237 != 0);
L5: 
    if (_4234 != 0) {
        goto L6; // [114] 146
    }
    if (IS_SEQUENCE(_filename_7902)){
            _4239 = SEQ_PTR(_filename_7902)->length;
    }
    else {
        _4239 = 1;
    }
    _4240 = (_4239 == 3);
    _4239 = NOVALUE;
    if (_4240 == 0) {
        DeRef(_4241);
        _4241 = 0;
        goto L7; // [125] 141
    }
    _2 = (int)SEQ_PTR(_filename_7902);
    _4242 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_4242)) {
        _4243 = (_4242 == 58);
    }
    else {
        _4243 = binary_op(EQUALS, _4242, 58);
    }
    _4242 = NOVALUE;
    if (IS_ATOM_INT(_4243))
    _4241 = (_4243 != 0);
    else
    _4241 = DBL_PTR(_4243)->dbl != 0.0;
L7: 
    if (_4241 == 0)
    {
        _4241 = NOVALUE;
        goto L8; // [142] 157
    }
    else{
        _4241 = NOVALUE;
    }
L6: 

    /** 			return FILETYPE_DIRECTORY*/
    DeRefDS(_filename_7902);
    DeRef(_dirfil_7903);
    DeRef(_4226);
    _4226 = NOVALUE;
    DeRef(_4233);
    _4233 = NOVALUE;
    DeRef(_4240);
    _4240 = NOVALUE;
    DeRef(_4243);
    _4243 = NOVALUE;
    return 2;
    goto L9; // [154] 178
L8: 

    /** 			return FILETYPE_FILE*/
    DeRefDS(_filename_7902);
    DeRef(_dirfil_7903);
    DeRef(_4226);
    _4226 = NOVALUE;
    DeRef(_4233);
    _4233 = NOVALUE;
    DeRef(_4240);
    _4240 = NOVALUE;
    DeRef(_4243);
    _4243 = NOVALUE;
    return 1;
    goto L9; // [166] 178
L4: 

    /** 		return FILETYPE_NOT_FOUND*/
    DeRefDS(_filename_7902);
    DeRef(_dirfil_7903);
    DeRef(_4226);
    _4226 = NOVALUE;
    DeRef(_4233);
    _4233 = NOVALUE;
    DeRef(_4240);
    _4240 = NOVALUE;
    DeRef(_4243);
    _4243 = NOVALUE;
    return 0;
L9: 
    ;
}


int _9file_exists(int _name_7960)
{
    int _pName_7963 = NOVALUE;
    int _r_7966 = NOVALUE;
    int _4258 = NOVALUE;
    int _4256 = NOVALUE;
    int _4254 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(name) then*/
    _4254 = IS_ATOM(_name_7960);
    if (_4254 == 0)
    {
        _4254 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _4254 = NOVALUE;
    }

    /** 		return 0*/
    DeRef(_name_7960);
    DeRef(_pName_7963);
    DeRef(_r_7966);
    return 0;
L1: 

    /** 	ifdef WINDOWS then*/

    /** 		atom pName = allocate_string(name)*/
    Ref(_name_7960);
    _0 = _pName_7963;
    _pName_7963 = _12allocate_string(_name_7960, 0);
    DeRef(_0);

    /** 		atom r = c_func(xGetFileAttributes, {pName})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pName_7963);
    *((int *)(_2+4)) = _pName_7963;
    _4256 = MAKE_SEQ(_1);
    DeRef(_r_7966);
    _r_7966 = call_c(1, _9xGetFileAttributes_6936, _4256);
    DeRefDS(_4256);
    _4256 = NOVALUE;

    /** 		free(pName)*/
    Ref(_pName_7963);
    _12free(_pName_7963);

    /** 		return r > 0*/
    if (IS_ATOM_INT(_r_7966)) {
        _4258 = (_r_7966 > 0);
    }
    else {
        _4258 = (DBL_PTR(_r_7966)->dbl > (double)0);
    }
    DeRef(_name_7960);
    DeRef(_pName_7963);
    DeRef(_r_7966);
    return _4258;
    ;
}


int _9file_timestamp(int _fname_7973)
{
    int _d_7974 = NOVALUE;
    int _dir_inlined_dir_at_4_7976 = NOVALUE;
    int _4272 = NOVALUE;
    int _4271 = NOVALUE;
    int _4270 = NOVALUE;
    int _4269 = NOVALUE;
    int _4268 = NOVALUE;
    int _4267 = NOVALUE;
    int _4266 = NOVALUE;
    int _4265 = NOVALUE;
    int _4264 = NOVALUE;
    int _4263 = NOVALUE;
    int _4262 = NOVALUE;
    int _4261 = NOVALUE;
    int _4260 = NOVALUE;
    int _4259 = NOVALUE;
    int _0, _1, _2;
    

    /** 	object d = dir(fname)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_d_7974);
    _d_7974 = machine(22, _fname_7973);

    /** 	if atom(d) then return -1 end if*/
    _4259 = IS_ATOM(_d_7974);
    if (_4259 == 0)
    {
        _4259 = NOVALUE;
        goto L1; // [19] 27
    }
    else{
        _4259 = NOVALUE;
    }
    DeRefDS(_fname_7973);
    DeRef(_d_7974);
    return -1;
L1: 

    /** 	return datetime:new(d[1][D_YEAR], d[1][D_MONTH], d[1][D_DAY],*/
    _2 = (int)SEQ_PTR(_d_7974);
    _4260 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4260);
    _4261 = (int)*(((s1_ptr)_2)->base + 4);
    _4260 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_7974);
    _4262 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4262);
    _4263 = (int)*(((s1_ptr)_2)->base + 5);
    _4262 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_7974);
    _4264 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4264);
    _4265 = (int)*(((s1_ptr)_2)->base + 6);
    _4264 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_7974);
    _4266 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4266);
    _4267 = (int)*(((s1_ptr)_2)->base + 7);
    _4266 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_7974);
    _4268 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4268);
    _4269 = (int)*(((s1_ptr)_2)->base + 8);
    _4268 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_7974);
    _4270 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4270);
    _4271 = (int)*(((s1_ptr)_2)->base + 9);
    _4270 = NOVALUE;
    Ref(_4261);
    Ref(_4263);
    Ref(_4265);
    Ref(_4267);
    Ref(_4269);
    Ref(_4271);
    _4272 = _10new(_4261, _4263, _4265, _4267, _4269, _4271);
    _4261 = NOVALUE;
    _4263 = NOVALUE;
    _4265 = NOVALUE;
    _4267 = NOVALUE;
    _4269 = NOVALUE;
    _4271 = NOVALUE;
    DeRefDS(_fname_7973);
    DeRef(_d_7974);
    return _4272;
    ;
}


int _9copy_file(int _src_7994, int _dest_7995, int _overwrite_7996)
{
    int _info_8007 = NOVALUE;
    int _psrc_8011 = NOVALUE;
    int _pdest_8014 = NOVALUE;
    int _success_8017 = NOVALUE;
    int _4288 = NOVALUE;
    int _4286 = NOVALUE;
    int _4285 = NOVALUE;
    int _4281 = NOVALUE;
    int _4277 = NOVALUE;
    int _4276 = NOVALUE;
    int _4274 = NOVALUE;
    int _4273 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_overwrite_7996)) {
        _1 = (long)(DBL_PTR(_overwrite_7996)->dbl);
        if (UNIQUE(DBL_PTR(_overwrite_7996)) && (DBL_PTR(_overwrite_7996)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_overwrite_7996);
        _overwrite_7996 = _1;
    }

    /** 	if length(dest) then*/
    if (IS_SEQUENCE(_dest_7995)){
            _4273 = SEQ_PTR(_dest_7995)->length;
    }
    else {
        _4273 = 1;
    }
    if (_4273 == 0)
    {
        _4273 = NOVALUE;
        goto L1; // [12] 72
    }
    else{
        _4273 = NOVALUE;
    }

    /** 		if file_type( dest ) = FILETYPE_DIRECTORY then*/
    RefDS(_dest_7995);
    _4274 = _9file_type(_dest_7995);
    if (binary_op_a(NOTEQ, _4274, 2)){
        DeRef(_4274);
        _4274 = NOVALUE;
        goto L2; // [23] 69
    }
    DeRef(_4274);
    _4274 = NOVALUE;

    /** 			if dest[$] != SLASH then*/
    if (IS_SEQUENCE(_dest_7995)){
            _4276 = SEQ_PTR(_dest_7995)->length;
    }
    else {
        _4276 = 1;
    }
    _2 = (int)SEQ_PTR(_dest_7995);
    _4277 = (int)*(((s1_ptr)_2)->base + _4276);
    if (binary_op_a(EQUALS, _4277, 92)){
        _4277 = NOVALUE;
        goto L3; // [36] 47
    }
    _4277 = NOVALUE;

    /** 				dest &= SLASH*/
    Append(&_dest_7995, _dest_7995, 92);
L3: 

    /** 			sequence info = pathinfo( src )*/
    RefDS(_src_7994);
    _0 = _info_8007;
    _info_8007 = _9pathinfo(_src_7994, 0);
    DeRef(_0);

    /** 			dest &= info[PATH_FILENAME]*/
    _2 = (int)SEQ_PTR(_info_8007);
    _4281 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_dest_7995) && IS_ATOM(_4281)) {
        Ref(_4281);
        Append(&_dest_7995, _dest_7995, _4281);
    }
    else if (IS_ATOM(_dest_7995) && IS_SEQUENCE(_4281)) {
    }
    else {
        Concat((object_ptr)&_dest_7995, _dest_7995, _4281);
    }
    _4281 = NOVALUE;
L2: 
    DeRef(_info_8007);
    _info_8007 = NOVALUE;
L1: 

    /** 	ifdef WINDOWS then*/

    /** 		atom psrc = allocate_string(src)*/
    RefDS(_src_7994);
    _0 = _psrc_8011;
    _psrc_8011 = _12allocate_string(_src_7994, 0);
    DeRef(_0);

    /** 		atom pdest = allocate_string(dest)*/
    RefDS(_dest_7995);
    _0 = _pdest_8014;
    _pdest_8014 = _12allocate_string(_dest_7995, 0);
    DeRef(_0);

    /** 		integer success = c_func(xCopyFile, {psrc, pdest, not overwrite})*/
    _4285 = (_overwrite_7996 == 0);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_psrc_8011);
    *((int *)(_2+4)) = _psrc_8011;
    Ref(_pdest_8014);
    *((int *)(_2+8)) = _pdest_8014;
    *((int *)(_2+12)) = _4285;
    _4286 = MAKE_SEQ(_1);
    _4285 = NOVALUE;
    _success_8017 = call_c(1, _9xCopyFile_6912, _4286);
    DeRefDS(_4286);
    _4286 = NOVALUE;
    if (!IS_ATOM_INT(_success_8017)) {
        _1 = (long)(DBL_PTR(_success_8017)->dbl);
        if (UNIQUE(DBL_PTR(_success_8017)) && (DBL_PTR(_success_8017)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_success_8017);
        _success_8017 = _1;
    }

    /** 		free({pdest, psrc})*/
    Ref(_psrc_8011);
    Ref(_pdest_8014);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _pdest_8014;
    ((int *)_2)[2] = _psrc_8011;
    _4288 = MAKE_SEQ(_1);
    _12free(_4288);
    _4288 = NOVALUE;

    /** 	return success*/
    DeRefDS(_src_7994);
    DeRefDS(_dest_7995);
    DeRef(_psrc_8011);
    DeRef(_pdest_8014);
    return _success_8017;
    ;
}


int _9rename_file(int _old_name_8025, int _new_name_8026, int _overwrite_8027)
{
    int _psrc_8028 = NOVALUE;
    int _pdest_8029 = NOVALUE;
    int _ret_8030 = NOVALUE;
    int _tempfile_8031 = NOVALUE;
    int _4303 = NOVALUE;
    int _4300 = NOVALUE;
    int _4298 = NOVALUE;
    int _4296 = NOVALUE;
    int _4291 = NOVALUE;
    int _4290 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_overwrite_8027)) {
        _1 = (long)(DBL_PTR(_overwrite_8027)->dbl);
        if (UNIQUE(DBL_PTR(_overwrite_8027)) && (DBL_PTR(_overwrite_8027)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_overwrite_8027);
        _overwrite_8027 = _1;
    }

    /** 	sequence tempfile = ""*/
    RefDS(_5);
    DeRef(_tempfile_8031);
    _tempfile_8031 = _5;

    /** 	if not overwrite then*/
    if (_overwrite_8027 != 0)
    goto L1; // [16] 38

    /** 		if file_exists(new_name) then*/
    RefDS(_new_name_8026);
    _4290 = _9file_exists(_new_name_8026);
    if (_4290 == 0) {
        DeRef(_4290);
        _4290 = NOVALUE;
        goto L2; // [25] 68
    }
    else {
        if (!IS_ATOM_INT(_4290) && DBL_PTR(_4290)->dbl == 0.0){
            DeRef(_4290);
            _4290 = NOVALUE;
            goto L2; // [25] 68
        }
        DeRef(_4290);
        _4290 = NOVALUE;
    }
    DeRef(_4290);
    _4290 = NOVALUE;

    /** 			return 0*/
    DeRefDS(_old_name_8025);
    DeRefDS(_new_name_8026);
    DeRef(_psrc_8028);
    DeRef(_pdest_8029);
    DeRef(_ret_8030);
    DeRefDS(_tempfile_8031);
    return 0;
    goto L2; // [35] 68
L1: 

    /** 		if file_exists(new_name) then*/
    RefDS(_new_name_8026);
    _4291 = _9file_exists(_new_name_8026);
    if (_4291 == 0) {
        DeRef(_4291);
        _4291 = NOVALUE;
        goto L3; // [44] 67
    }
    else {
        if (!IS_ATOM_INT(_4291) && DBL_PTR(_4291)->dbl == 0.0){
            DeRef(_4291);
            _4291 = NOVALUE;
            goto L3; // [44] 67
        }
        DeRef(_4291);
        _4291 = NOVALUE;
    }
    DeRef(_4291);
    _4291 = NOVALUE;

    /** 			tempfile = temp_file(new_name)*/
    RefDS(_new_name_8026);
    RefDS(_5);
    RefDS(_4524);
    _0 = _tempfile_8031;
    _tempfile_8031 = _9temp_file(_new_name_8026, _5, _4524, 0);
    DeRef(_0);

    /** 			ret = move_file(new_name, tempfile)*/
    RefDS(_new_name_8026);
    RefDS(_tempfile_8031);
    _0 = _ret_8030;
    _ret_8030 = _9move_file(_new_name_8026, _tempfile_8031, 0);
    DeRef(_0);
L3: 
L2: 

    /** 	psrc = machine:allocate_string(old_name)*/
    RefDS(_old_name_8025);
    _0 = _psrc_8028;
    _psrc_8028 = _12allocate_string(_old_name_8025, 0);
    DeRef(_0);

    /** 	pdest = machine:allocate_string(new_name)*/
    RefDS(_new_name_8026);
    _0 = _pdest_8029;
    _pdest_8029 = _12allocate_string(_new_name_8026, 0);
    DeRef(_0);

    /** 	ret = c_func(xMoveFile, {psrc, pdest})*/
    Ref(_pdest_8029);
    Ref(_psrc_8028);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _psrc_8028;
    ((int *)_2)[2] = _pdest_8029;
    _4296 = MAKE_SEQ(_1);
    DeRef(_ret_8030);
    _ret_8030 = call_c(1, _9xMoveFile_6917, _4296);
    DeRefDS(_4296);
    _4296 = NOVALUE;

    /** 	ifdef UNIX then*/

    /** 	machine:free({pdest, psrc})*/
    Ref(_psrc_8028);
    Ref(_pdest_8029);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _pdest_8029;
    ((int *)_2)[2] = _psrc_8028;
    _4298 = MAKE_SEQ(_1);
    _12free(_4298);
    _4298 = NOVALUE;

    /** 	if overwrite then*/
    if (_overwrite_8027 == 0)
    {
        goto L4; // [108] 142
    }
    else{
    }

    /** 		if not ret then*/
    if (IS_ATOM_INT(_ret_8030)) {
        if (_ret_8030 != 0){
            goto L5; // [113] 135
        }
    }
    else {
        if (DBL_PTR(_ret_8030)->dbl != 0.0){
            goto L5; // [113] 135
        }
    }

    /** 			if length(tempfile) > 0 then*/
    if (IS_SEQUENCE(_tempfile_8031)){
            _4300 = SEQ_PTR(_tempfile_8031)->length;
    }
    else {
        _4300 = 1;
    }
    if (_4300 <= 0)
    goto L6; // [121] 134

    /** 				ret = move_file(tempfile, new_name)*/
    RefDS(_tempfile_8031);
    RefDS(_new_name_8026);
    _0 = _ret_8030;
    _ret_8030 = _9move_file(_tempfile_8031, _new_name_8026, 0);
    DeRef(_0);
L6: 
L5: 

    /** 		delete_file(tempfile)*/
    RefDS(_tempfile_8031);
    _4303 = _9delete_file(_tempfile_8031);
L4: 

    /** 	return ret*/
    DeRefDS(_old_name_8025);
    DeRefDS(_new_name_8026);
    DeRef(_psrc_8028);
    DeRef(_pdest_8029);
    DeRef(_tempfile_8031);
    DeRef(_4303);
    _4303 = NOVALUE;
    return _ret_8030;
    ;
}


int _9move_file(int _src_8059, int _dest_8060, int _overwrite_8061)
{
    int _psrc_8062 = NOVALUE;
    int _pdest_8063 = NOVALUE;
    int _ret_8064 = NOVALUE;
    int _tempfile_8065 = NOVALUE;
    int _4322 = NOVALUE;
    int _4321 = NOVALUE;
    int _4320 = NOVALUE;
    int _4319 = NOVALUE;
    int _4317 = NOVALUE;
    int _4315 = NOVALUE;
    int _4314 = NOVALUE;
    int _4313 = NOVALUE;
    int _4312 = NOVALUE;
    int _4307 = NOVALUE;
    int _4304 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_overwrite_8061)) {
        _1 = (long)(DBL_PTR(_overwrite_8061)->dbl);
        if (UNIQUE(DBL_PTR(_overwrite_8061)) && (DBL_PTR(_overwrite_8061)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_overwrite_8061);
        _overwrite_8061 = _1;
    }

    /** 	atom psrc = 0, pdest = 0, ret*/
    DeRef(_psrc_8062);
    _psrc_8062 = 0;
    DeRef(_pdest_8063);
    _pdest_8063 = 0;

    /** 	sequence tempfile = ""*/
    RefDS(_5);
    DeRef(_tempfile_8065);
    _tempfile_8065 = _5;

    /** 	if not file_exists(src) then*/
    RefDS(_src_8059);
    _4304 = _9file_exists(_src_8059);
    if (IS_ATOM_INT(_4304)) {
        if (_4304 != 0){
            DeRef(_4304);
            _4304 = NOVALUE;
            goto L1; // [28] 38
        }
    }
    else {
        if (DBL_PTR(_4304)->dbl != 0.0){
            DeRef(_4304);
            _4304 = NOVALUE;
            goto L1; // [28] 38
        }
    }
    DeRef(_4304);
    _4304 = NOVALUE;

    /** 		return 0*/
    DeRefDS(_src_8059);
    DeRefDS(_dest_8060);
    DeRef(_ret_8064);
    DeRefDS(_tempfile_8065);
    return 0;
L1: 

    /** 	if not overwrite then*/
    if (_overwrite_8061 != 0)
    goto L2; // [40] 60

    /** 		if file_exists( dest ) then*/
    RefDS(_dest_8060);
    _4307 = _9file_exists(_dest_8060);
    if (_4307 == 0) {
        DeRef(_4307);
        _4307 = NOVALUE;
        goto L3; // [49] 59
    }
    else {
        if (!IS_ATOM_INT(_4307) && DBL_PTR(_4307)->dbl == 0.0){
            DeRef(_4307);
            _4307 = NOVALUE;
            goto L3; // [49] 59
        }
        DeRef(_4307);
        _4307 = NOVALUE;
    }
    DeRef(_4307);
    _4307 = NOVALUE;

    /** 			return 0*/
    DeRefDS(_src_8059);
    DeRefDS(_dest_8060);
    DeRef(_psrc_8062);
    DeRef(_pdest_8063);
    DeRef(_ret_8064);
    DeRef(_tempfile_8065);
    return 0;
L3: 
L2: 

    /** 	ifdef UNIX then*/

    /** 	ifdef LINUX then*/

    /** 	ifdef UNIX then*/

    /** 		psrc  = machine:allocate_string(src)*/
    RefDS(_src_8059);
    _0 = _psrc_8062;
    _psrc_8062 = _12allocate_string(_src_8059, 0);
    DeRef(_0);

    /** 		pdest = machine:allocate_string(dest)*/
    RefDS(_dest_8060);
    _0 = _pdest_8063;
    _pdest_8063 = _12allocate_string(_dest_8060, 0);
    DeRef(_0);

    /** 	if overwrite then*/
    if (_overwrite_8061 == 0)
    {
        goto L4; // [82] 111
    }
    else{
    }

    /** 		tempfile = temp_file(dest)*/
    RefDS(_dest_8060);
    RefDS(_5);
    RefDS(_4524);
    _0 = _tempfile_8065;
    _tempfile_8065 = _9temp_file(_dest_8060, _5, _4524, 0);
    DeRef(_0);

    /** 		move_file(dest, tempfile)*/
    RefDS(_dest_8060);
    DeRef(_4312);
    _4312 = _dest_8060;
    RefDS(_tempfile_8065);
    DeRef(_4313);
    _4313 = _tempfile_8065;
    _4314 = _9move_file(_4312, _4313, 0);
    _4312 = NOVALUE;
    _4313 = NOVALUE;
L4: 

    /** 	ret = c_func(xMoveFile, {psrc, pdest})*/
    Ref(_pdest_8063);
    Ref(_psrc_8062);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _psrc_8062;
    ((int *)_2)[2] = _pdest_8063;
    _4315 = MAKE_SEQ(_1);
    DeRef(_ret_8064);
    _ret_8064 = call_c(1, _9xMoveFile_6917, _4315);
    DeRefDS(_4315);
    _4315 = NOVALUE;

    /** 	ifdef UNIX then*/

    /** 	machine:free({pdest, psrc})*/
    Ref(_psrc_8062);
    Ref(_pdest_8063);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _pdest_8063;
    ((int *)_2)[2] = _psrc_8062;
    _4317 = MAKE_SEQ(_1);
    _12free(_4317);
    _4317 = NOVALUE;

    /** 	if overwrite then*/
    if (_overwrite_8061 == 0)
    {
        goto L5; // [137] 167
    }
    else{
    }

    /** 		if not ret then*/
    if (IS_ATOM_INT(_ret_8064)) {
        if (_ret_8064 != 0){
            goto L6; // [142] 160
        }
    }
    else {
        if (DBL_PTR(_ret_8064)->dbl != 0.0){
            goto L6; // [142] 160
        }
    }

    /** 			move_file(tempfile, dest)*/
    RefDS(_tempfile_8065);
    DeRef(_4319);
    _4319 = _tempfile_8065;
    RefDS(_dest_8060);
    DeRef(_4320);
    _4320 = _dest_8060;
    _4321 = _9move_file(_4319, _4320, 0);
    _4319 = NOVALUE;
    _4320 = NOVALUE;
L6: 

    /** 		delete_file(tempfile)*/
    RefDS(_tempfile_8065);
    _4322 = _9delete_file(_tempfile_8065);
L5: 

    /** 	return ret*/
    DeRefDS(_src_8059);
    DeRefDS(_dest_8060);
    DeRef(_psrc_8062);
    DeRef(_pdest_8063);
    DeRef(_tempfile_8065);
    DeRef(_4314);
    _4314 = NOVALUE;
    DeRef(_4321);
    _4321 = NOVALUE;
    DeRef(_4322);
    _4322 = NOVALUE;
    return _ret_8064;
    ;
}


int _9file_length(int _filename_8094)
{
    int _list_8095 = NOVALUE;
    int _dir_inlined_dir_at_4_8097 = NOVALUE;
    int _4328 = NOVALUE;
    int _4327 = NOVALUE;
    int _4326 = NOVALUE;
    int _4325 = NOVALUE;
    int _4323 = NOVALUE;
    int _0, _1, _2;
    

    /** 	list = dir(filename)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_list_8095);
    _list_8095 = machine(22, _filename_8094);

    /** 	if atom(list) or length(list) = 0 then*/
    _4323 = IS_ATOM(_list_8095);
    if (_4323 != 0) {
        goto L1; // [19] 35
    }
    if (IS_SEQUENCE(_list_8095)){
            _4325 = SEQ_PTR(_list_8095)->length;
    }
    else {
        _4325 = 1;
    }
    _4326 = (_4325 == 0);
    _4325 = NOVALUE;
    if (_4326 == 0)
    {
        DeRef(_4326);
        _4326 = NOVALUE;
        goto L2; // [31] 42
    }
    else{
        DeRef(_4326);
        _4326 = NOVALUE;
    }
L1: 

    /** 		return -1*/
    DeRefDS(_filename_8094);
    DeRef(_list_8095);
    return -1;
L2: 

    /** 	return list[1][D_SIZE]*/
    _2 = (int)SEQ_PTR(_list_8095);
    _4327 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4327);
    _4328 = (int)*(((s1_ptr)_2)->base + 3);
    _4327 = NOVALUE;
    Ref(_4328);
    DeRefDS(_filename_8094);
    DeRef(_list_8095);
    return _4328;
    ;
}


int _9locate_file(int _filename_8107, int _search_list_8108, int _subdir_8109)
{
    int _extra_paths_8110 = NOVALUE;
    int _this_path_8111 = NOVALUE;
    int _4412 = NOVALUE;
    int _4411 = NOVALUE;
    int _4409 = NOVALUE;
    int _4407 = NOVALUE;
    int _4405 = NOVALUE;
    int _4404 = NOVALUE;
    int _4403 = NOVALUE;
    int _4401 = NOVALUE;
    int _4400 = NOVALUE;
    int _4399 = NOVALUE;
    int _4397 = NOVALUE;
    int _4396 = NOVALUE;
    int _4395 = NOVALUE;
    int _4392 = NOVALUE;
    int _4391 = NOVALUE;
    int _4389 = NOVALUE;
    int _4387 = NOVALUE;
    int _4386 = NOVALUE;
    int _4383 = NOVALUE;
    int _4378 = NOVALUE;
    int _4374 = NOVALUE;
    int _4365 = NOVALUE;
    int _4362 = NOVALUE;
    int _4359 = NOVALUE;
    int _4358 = NOVALUE;
    int _4354 = NOVALUE;
    int _4351 = NOVALUE;
    int _4349 = NOVALUE;
    int _4345 = NOVALUE;
    int _4343 = NOVALUE;
    int _4342 = NOVALUE;
    int _4340 = NOVALUE;
    int _4339 = NOVALUE;
    int _4336 = NOVALUE;
    int _4335 = NOVALUE;
    int _4332 = NOVALUE;
    int _4330 = NOVALUE;
    int _4329 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if absolute_path(filename) then*/
    RefDS(_filename_8107);
    _4329 = _9absolute_path(_filename_8107);
    if (_4329 == 0) {
        DeRef(_4329);
        _4329 = NOVALUE;
        goto L1; // [13] 23
    }
    else {
        if (!IS_ATOM_INT(_4329) && DBL_PTR(_4329)->dbl == 0.0){
            DeRef(_4329);
            _4329 = NOVALUE;
            goto L1; // [13] 23
        }
        DeRef(_4329);
        _4329 = NOVALUE;
    }
    DeRef(_4329);
    _4329 = NOVALUE;

    /** 		return filename*/
    DeRefDS(_search_list_8108);
    DeRefDS(_subdir_8109);
    DeRef(_extra_paths_8110);
    DeRef(_this_path_8111);
    return _filename_8107;
L1: 

    /** 	if length(search_list) = 0 then*/
    if (IS_SEQUENCE(_search_list_8108)){
            _4330 = SEQ_PTR(_search_list_8108)->length;
    }
    else {
        _4330 = 1;
    }
    if (_4330 != 0)
    goto L2; // [28] 281

    /** 		search_list = append(search_list, "." & SLASH)*/
    Append(&_4332, _3674, 92);
    RefDS(_4332);
    Append(&_search_list_8108, _search_list_8108, _4332);
    DeRefDS(_4332);
    _4332 = NOVALUE;

    /** 		extra_paths = command_line()*/
    DeRef(_extra_paths_8110);
    _extra_paths_8110 = Command_Line();

    /** 		extra_paths = canonical_path(dirname(extra_paths[2]), 1)*/
    _2 = (int)SEQ_PTR(_extra_paths_8110);
    _4335 = (int)*(((s1_ptr)_2)->base + 2);
    RefDS(_4335);
    _4336 = _9dirname(_4335, 0);
    _4335 = NOVALUE;
    _0 = _extra_paths_8110;
    _extra_paths_8110 = _9canonical_path(_4336, 1, 0);
    DeRefDS(_0);
    _4336 = NOVALUE;

    /** 		search_list = append(search_list, extra_paths)*/
    Ref(_extra_paths_8110);
    Append(&_search_list_8108, _search_list_8108, _extra_paths_8110);

    /** 		ifdef UNIX then*/

    /** 			extra_paths = getenv("HOMEDRIVE") & getenv("HOMEPATH")*/
    _4339 = EGetEnv(_3962);
    _4340 = EGetEnv(_3964);
    if (IS_SEQUENCE(_4339) && IS_ATOM(_4340)) {
        Ref(_4340);
        Append(&_extra_paths_8110, _4339, _4340);
    }
    else if (IS_ATOM(_4339) && IS_SEQUENCE(_4340)) {
        Ref(_4339);
        Prepend(&_extra_paths_8110, _4340, _4339);
    }
    else {
        Concat((object_ptr)&_extra_paths_8110, _4339, _4340);
        DeRef(_4339);
        _4339 = NOVALUE;
    }
    DeRef(_4339);
    _4339 = NOVALUE;
    DeRef(_4340);
    _4340 = NOVALUE;

    /** 		if sequence(extra_paths) then*/
    _4342 = 1;
    if (_4342 == 0)
    {
        _4342 = NOVALUE;
        goto L3; // [90] 104
    }
    else{
        _4342 = NOVALUE;
    }

    /** 			search_list = append(search_list, extra_paths & SLASH)*/
    Append(&_4343, _extra_paths_8110, 92);
    RefDS(_4343);
    Append(&_search_list_8108, _search_list_8108, _4343);
    DeRefDS(_4343);
    _4343 = NOVALUE;
L3: 

    /** 		search_list = append(search_list, ".." & SLASH)*/
    Append(&_4345, _3705, 92);
    RefDS(_4345);
    Append(&_search_list_8108, _search_list_8108, _4345);
    DeRefDS(_4345);
    _4345 = NOVALUE;

    /** 		extra_paths = getenv("EUDIR")*/
    DeRef(_extra_paths_8110);
    _extra_paths_8110 = EGetEnv(_4347);

    /** 		if sequence(extra_paths) then*/
    _4349 = IS_SEQUENCE(_extra_paths_8110);
    if (_4349 == 0)
    {
        _4349 = NOVALUE;
        goto L4; // [124] 154
    }
    else{
        _4349 = NOVALUE;
    }

    /** 			search_list = append(search_list, extra_paths & SLASH & "bin" & SLASH)*/
    {
        int concat_list[4];

        concat_list[0] = 92;
        concat_list[1] = _4350;
        concat_list[2] = 92;
        concat_list[3] = _extra_paths_8110;
        Concat_N((object_ptr)&_4351, concat_list, 4);
    }
    RefDS(_4351);
    Append(&_search_list_8108, _search_list_8108, _4351);
    DeRefDS(_4351);
    _4351 = NOVALUE;

    /** 			search_list = append(search_list, extra_paths & SLASH & "docs" & SLASH)*/
    {
        int concat_list[4];

        concat_list[0] = 92;
        concat_list[1] = _4353;
        concat_list[2] = 92;
        concat_list[3] = _extra_paths_8110;
        Concat_N((object_ptr)&_4354, concat_list, 4);
    }
    RefDS(_4354);
    Append(&_search_list_8108, _search_list_8108, _4354);
    DeRefDS(_4354);
    _4354 = NOVALUE;
L4: 

    /** 		extra_paths = getenv("EUDIST")*/
    DeRef(_extra_paths_8110);
    _extra_paths_8110 = EGetEnv(_4356);

    /** 		if sequence(extra_paths) then*/
    _4358 = IS_SEQUENCE(_extra_paths_8110);
    if (_4358 == 0)
    {
        _4358 = NOVALUE;
        goto L5; // [164] 204
    }
    else{
        _4358 = NOVALUE;
    }

    /** 			search_list = append(search_list, extra_paths & SLASH)*/
    if (IS_SEQUENCE(_extra_paths_8110) && IS_ATOM(92)) {
        Append(&_4359, _extra_paths_8110, 92);
    }
    else if (IS_ATOM(_extra_paths_8110) && IS_SEQUENCE(92)) {
    }
    else {
        Concat((object_ptr)&_4359, _extra_paths_8110, 92);
    }
    RefDS(_4359);
    Append(&_search_list_8108, _search_list_8108, _4359);
    DeRefDS(_4359);
    _4359 = NOVALUE;

    /** 			search_list = append(search_list, extra_paths & SLASH & "etc" & SLASH)*/
    {
        int concat_list[4];

        concat_list[0] = 92;
        concat_list[1] = _4361;
        concat_list[2] = 92;
        concat_list[3] = _extra_paths_8110;
        Concat_N((object_ptr)&_4362, concat_list, 4);
    }
    RefDS(_4362);
    Append(&_search_list_8108, _search_list_8108, _4362);
    DeRefDS(_4362);
    _4362 = NOVALUE;

    /** 			search_list = append(search_list, extra_paths & SLASH & "data" & SLASH)*/
    {
        int concat_list[4];

        concat_list[0] = 92;
        concat_list[1] = _4364;
        concat_list[2] = 92;
        concat_list[3] = _extra_paths_8110;
        Concat_N((object_ptr)&_4365, concat_list, 4);
    }
    RefDS(_4365);
    Append(&_search_list_8108, _search_list_8108, _4365);
    DeRefDS(_4365);
    _4365 = NOVALUE;
L5: 

    /** 		ifdef UNIX then*/

    /** 		search_list &= include_paths(1)*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_4373);
    *((int *)(_2+4)) = _4373;
    RefDS(_4372);
    *((int *)(_2+8)) = _4372;
    RefDS(_4371);
    *((int *)(_2+12)) = _4371;
    RefDS(_4370);
    *((int *)(_2+16)) = _4370;
    RefDS(_4369);
    *((int *)(_2+20)) = _4369;
    _4374 = MAKE_SEQ(_1);
    Concat((object_ptr)&_search_list_8108, _search_list_8108, _4374);
    DeRefDS(_4374);
    _4374 = NOVALUE;

    /** 		extra_paths = getenv("USERPATH")*/
    DeRef(_extra_paths_8110);
    _extra_paths_8110 = EGetEnv(_4376);

    /** 		if sequence(extra_paths) then*/
    _4378 = IS_SEQUENCE(_extra_paths_8110);
    if (_4378 == 0)
    {
        _4378 = NOVALUE;
        goto L6; // [230] 249
    }
    else{
        _4378 = NOVALUE;
    }

    /** 			extra_paths = stdseq:split(extra_paths, PATHSEP)*/
    Ref(_extra_paths_8110);
    _0 = _extra_paths_8110;
    _extra_paths_8110 = _21split(_extra_paths_8110, 59, 0, 0);
    DeRefi(_0);

    /** 			search_list &= extra_paths*/
    if (IS_SEQUENCE(_search_list_8108) && IS_ATOM(_extra_paths_8110)) {
        Ref(_extra_paths_8110);
        Append(&_search_list_8108, _search_list_8108, _extra_paths_8110);
    }
    else if (IS_ATOM(_search_list_8108) && IS_SEQUENCE(_extra_paths_8110)) {
    }
    else {
        Concat((object_ptr)&_search_list_8108, _search_list_8108, _extra_paths_8110);
    }
L6: 

    /** 		extra_paths = getenv("PATH")*/
    DeRef(_extra_paths_8110);
    _extra_paths_8110 = EGetEnv(_4381);

    /** 		if sequence(extra_paths) then*/
    _4383 = IS_SEQUENCE(_extra_paths_8110);
    if (_4383 == 0)
    {
        _4383 = NOVALUE;
        goto L7; // [259] 306
    }
    else{
        _4383 = NOVALUE;
    }

    /** 			extra_paths = stdseq:split(extra_paths, PATHSEP)*/
    Ref(_extra_paths_8110);
    _0 = _extra_paths_8110;
    _extra_paths_8110 = _21split(_extra_paths_8110, 59, 0, 0);
    DeRefi(_0);

    /** 			search_list &= extra_paths*/
    if (IS_SEQUENCE(_search_list_8108) && IS_ATOM(_extra_paths_8110)) {
        Ref(_extra_paths_8110);
        Append(&_search_list_8108, _search_list_8108, _extra_paths_8110);
    }
    else if (IS_ATOM(_search_list_8108) && IS_SEQUENCE(_extra_paths_8110)) {
    }
    else {
        Concat((object_ptr)&_search_list_8108, _search_list_8108, _extra_paths_8110);
    }
    goto L7; // [278] 306
L2: 

    /** 		if integer(search_list[1]) then*/
    _2 = (int)SEQ_PTR(_search_list_8108);
    _4386 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_4386))
    _4387 = 1;
    else if (IS_ATOM_DBL(_4386))
    _4387 = IS_ATOM_INT(DoubleToInt(_4386));
    else
    _4387 = 0;
    _4386 = NOVALUE;
    if (_4387 == 0)
    {
        _4387 = NOVALUE;
        goto L8; // [290] 305
    }
    else{
        _4387 = NOVALUE;
    }

    /** 			search_list = stdseq:split(search_list, PATHSEP)*/
    RefDS(_search_list_8108);
    _0 = _search_list_8108;
    _search_list_8108 = _21split(_search_list_8108, 59, 0, 0);
    DeRefDS(_0);
L8: 
L7: 

    /** 	if length(subdir) > 0 then*/
    if (IS_SEQUENCE(_subdir_8109)){
            _4389 = SEQ_PTR(_subdir_8109)->length;
    }
    else {
        _4389 = 1;
    }
    if (_4389 <= 0)
    goto L9; // [311] 336

    /** 		if subdir[$] != SLASH then*/
    if (IS_SEQUENCE(_subdir_8109)){
            _4391 = SEQ_PTR(_subdir_8109)->length;
    }
    else {
        _4391 = 1;
    }
    _2 = (int)SEQ_PTR(_subdir_8109);
    _4392 = (int)*(((s1_ptr)_2)->base + _4391);
    if (binary_op_a(EQUALS, _4392, 92)){
        _4392 = NOVALUE;
        goto LA; // [324] 335
    }
    _4392 = NOVALUE;

    /** 			subdir &= SLASH*/
    Append(&_subdir_8109, _subdir_8109, 92);
LA: 
L9: 

    /** 	for i = 1 to length(search_list) do*/
    if (IS_SEQUENCE(_search_list_8108)){
            _4395 = SEQ_PTR(_search_list_8108)->length;
    }
    else {
        _4395 = 1;
    }
    {
        int _i_8190;
        _i_8190 = 1;
LB: 
        if (_i_8190 > _4395){
            goto LC; // [341] 466
        }

        /** 		if length(search_list[i]) = 0 then*/
        _2 = (int)SEQ_PTR(_search_list_8108);
        _4396 = (int)*(((s1_ptr)_2)->base + _i_8190);
        if (IS_SEQUENCE(_4396)){
                _4397 = SEQ_PTR(_4396)->length;
        }
        else {
            _4397 = 1;
        }
        _4396 = NOVALUE;
        if (_4397 != 0)
        goto LD; // [357] 366

        /** 			continue*/
        goto LE; // [363] 461
LD: 

        /** 		if search_list[i][$] != SLASH then*/
        _2 = (int)SEQ_PTR(_search_list_8108);
        _4399 = (int)*(((s1_ptr)_2)->base + _i_8190);
        if (IS_SEQUENCE(_4399)){
                _4400 = SEQ_PTR(_4399)->length;
        }
        else {
            _4400 = 1;
        }
        _2 = (int)SEQ_PTR(_4399);
        _4401 = (int)*(((s1_ptr)_2)->base + _4400);
        _4399 = NOVALUE;
        if (binary_op_a(EQUALS, _4401, 92)){
            _4401 = NOVALUE;
            goto LF; // [379] 398
        }
        _4401 = NOVALUE;

        /** 			search_list[i] &= SLASH*/
        _2 = (int)SEQ_PTR(_search_list_8108);
        _4403 = (int)*(((s1_ptr)_2)->base + _i_8190);
        if (IS_SEQUENCE(_4403) && IS_ATOM(92)) {
            Append(&_4404, _4403, 92);
        }
        else if (IS_ATOM(_4403) && IS_SEQUENCE(92)) {
        }
        else {
            Concat((object_ptr)&_4404, _4403, 92);
            _4403 = NOVALUE;
        }
        _4403 = NOVALUE;
        _2 = (int)SEQ_PTR(_search_list_8108);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _search_list_8108 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_8190);
        _1 = *(int *)_2;
        *(int *)_2 = _4404;
        if( _1 != _4404 ){
            DeRef(_1);
        }
        _4404 = NOVALUE;
LF: 

        /** 		if length(subdir) > 0 then*/
        if (IS_SEQUENCE(_subdir_8109)){
                _4405 = SEQ_PTR(_subdir_8109)->length;
        }
        else {
            _4405 = 1;
        }
        if (_4405 <= 0)
        goto L10; // [403] 422

        /** 			this_path = search_list[i] & subdir & filename*/
        _2 = (int)SEQ_PTR(_search_list_8108);
        _4407 = (int)*(((s1_ptr)_2)->base + _i_8190);
        {
            int concat_list[3];

            concat_list[0] = _filename_8107;
            concat_list[1] = _subdir_8109;
            concat_list[2] = _4407;
            Concat_N((object_ptr)&_this_path_8111, concat_list, 3);
        }
        _4407 = NOVALUE;
        goto L11; // [419] 433
L10: 

        /** 			this_path = search_list[i] & filename*/
        _2 = (int)SEQ_PTR(_search_list_8108);
        _4409 = (int)*(((s1_ptr)_2)->base + _i_8190);
        if (IS_SEQUENCE(_4409) && IS_ATOM(_filename_8107)) {
        }
        else if (IS_ATOM(_4409) && IS_SEQUENCE(_filename_8107)) {
            Ref(_4409);
            Prepend(&_this_path_8111, _filename_8107, _4409);
        }
        else {
            Concat((object_ptr)&_this_path_8111, _4409, _filename_8107);
            _4409 = NOVALUE;
        }
        _4409 = NOVALUE;
L11: 

        /** 		if file_exists(this_path) then*/
        RefDS(_this_path_8111);
        _4411 = _9file_exists(_this_path_8111);
        if (_4411 == 0) {
            DeRef(_4411);
            _4411 = NOVALUE;
            goto L12; // [441] 459
        }
        else {
            if (!IS_ATOM_INT(_4411) && DBL_PTR(_4411)->dbl == 0.0){
                DeRef(_4411);
                _4411 = NOVALUE;
                goto L12; // [441] 459
            }
            DeRef(_4411);
            _4411 = NOVALUE;
        }
        DeRef(_4411);
        _4411 = NOVALUE;

        /** 			return canonical_path(this_path)*/
        RefDS(_this_path_8111);
        _4412 = _9canonical_path(_this_path_8111, 0, 0);
        DeRefDS(_filename_8107);
        DeRefDS(_search_list_8108);
        DeRefDS(_subdir_8109);
        DeRef(_extra_paths_8110);
        DeRefDS(_this_path_8111);
        _4396 = NOVALUE;
        return _4412;
L12: 

        /** 	end for*/
LE: 
        _i_8190 = _i_8190 + 1;
        goto LB; // [461] 348
LC: 
        ;
    }

    /** 	return filename*/
    DeRefDS(_search_list_8108);
    DeRefDS(_subdir_8109);
    DeRef(_extra_paths_8110);
    DeRef(_this_path_8111);
    _4396 = NOVALUE;
    DeRef(_4412);
    _4412 = NOVALUE;
    return _filename_8107;
    ;
}


int _9disk_metrics(int _disk_path_8216)
{
    int _result_8217 = NOVALUE;
    int _path_addr_8219 = NOVALUE;
    int _metric_addr_8220 = NOVALUE;
    int _4425 = NOVALUE;
    int _4423 = NOVALUE;
    int _4422 = NOVALUE;
    int _4421 = NOVALUE;
    int _4420 = NOVALUE;
    int _4419 = NOVALUE;
    int _4418 = NOVALUE;
    int _4417 = NOVALUE;
    int _4414 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence result = {0, 0, 0, 0} */
    _0 = _result_8217;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 0;
    _result_8217 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 	atom path_addr = 0*/
    DeRef(_path_addr_8219);
    _path_addr_8219 = 0;

    /** 	atom metric_addr = 0*/
    DeRef(_metric_addr_8220);
    _metric_addr_8220 = 0;

    /** 	ifdef WINDOWS then*/

    /** 		if sequence(disk_path) then */
    _4414 = IS_SEQUENCE(_disk_path_8216);
    if (_4414 == 0)
    {
        _4414 = NOVALUE;
        goto L1; // [27] 40
    }
    else{
        _4414 = NOVALUE;
    }

    /** 			path_addr = allocate_string(disk_path) */
    Ref(_disk_path_8216);
    _path_addr_8219 = _12allocate_string(_disk_path_8216, 0);
    goto L2; // [37] 46
L1: 

    /** 			path_addr = 0 */
    DeRef(_path_addr_8219);
    _path_addr_8219 = 0;
L2: 

    /** 		metric_addr = allocate(16) */
    _0 = _metric_addr_8220;
    _metric_addr_8220 = _12allocate(16, 0);
    DeRef(_0);

    /** 		if c_func(xGetDiskFreeSpace, {path_addr, */
    if (IS_ATOM_INT(_metric_addr_8220)) {
        _4417 = _metric_addr_8220 + 0;
        if ((long)((unsigned long)_4417 + (unsigned long)HIGH_BITS) >= 0) 
        _4417 = NewDouble((double)_4417);
    }
    else {
        _4417 = NewDouble(DBL_PTR(_metric_addr_8220)->dbl + (double)0);
    }
    if (IS_ATOM_INT(_metric_addr_8220)) {
        _4418 = _metric_addr_8220 + 4;
        if ((long)((unsigned long)_4418 + (unsigned long)HIGH_BITS) >= 0) 
        _4418 = NewDouble((double)_4418);
    }
    else {
        _4418 = NewDouble(DBL_PTR(_metric_addr_8220)->dbl + (double)4);
    }
    if (IS_ATOM_INT(_metric_addr_8220)) {
        _4419 = _metric_addr_8220 + 8;
        if ((long)((unsigned long)_4419 + (unsigned long)HIGH_BITS) >= 0) 
        _4419 = NewDouble((double)_4419);
    }
    else {
        _4419 = NewDouble(DBL_PTR(_metric_addr_8220)->dbl + (double)8);
    }
    if (IS_ATOM_INT(_metric_addr_8220)) {
        _4420 = _metric_addr_8220 + 12;
        if ((long)((unsigned long)_4420 + (unsigned long)HIGH_BITS) >= 0) 
        _4420 = NewDouble((double)_4420);
    }
    else {
        _4420 = NewDouble(DBL_PTR(_metric_addr_8220)->dbl + (double)12);
    }
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_path_addr_8219);
    *((int *)(_2+4)) = _path_addr_8219;
    *((int *)(_2+8)) = _4417;
    *((int *)(_2+12)) = _4418;
    *((int *)(_2+16)) = _4419;
    *((int *)(_2+20)) = _4420;
    _4421 = MAKE_SEQ(_1);
    _4420 = NOVALUE;
    _4419 = NOVALUE;
    _4418 = NOVALUE;
    _4417 = NOVALUE;
    _4422 = call_c(1, _9xGetDiskFreeSpace_6940, _4421);
    DeRefDS(_4421);
    _4421 = NOVALUE;
    if (_4422 == 0) {
        DeRef(_4422);
        _4422 = NOVALUE;
        goto L3; // [86] 101
    }
    else {
        if (!IS_ATOM_INT(_4422) && DBL_PTR(_4422)->dbl == 0.0){
            DeRef(_4422);
            _4422 = NOVALUE;
            goto L3; // [86] 101
        }
        DeRef(_4422);
        _4422 = NOVALUE;
    }
    DeRef(_4422);
    _4422 = NOVALUE;

    /** 			result = peek4s({metric_addr, 4}) */
    Ref(_metric_addr_8220);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _metric_addr_8220;
    ((int *)_2)[2] = 4;
    _4423 = MAKE_SEQ(_1);
    DeRef(_result_8217);
    _1 = (int)SEQ_PTR(_4423);
    peek4_addr = (unsigned long *)get_pos_int("peek4s/peek4u", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _result_8217 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)*peek4_addr++;
        if (_1 < MININT || _1 > MAXINT)
        _1 = NewDouble((double)(long)_1);
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_4423);
    _4423 = NOVALUE;
L3: 

    /** 		free({path_addr, metric_addr}) */
    Ref(_metric_addr_8220);
    Ref(_path_addr_8219);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _path_addr_8219;
    ((int *)_2)[2] = _metric_addr_8220;
    _4425 = MAKE_SEQ(_1);
    _12free(_4425);
    _4425 = NOVALUE;

    /** 	return result */
    DeRef(_disk_path_8216);
    DeRef(_path_addr_8219);
    DeRef(_metric_addr_8220);
    return _result_8217;
    ;
}


int _9disk_size(int _disk_path_8242)
{
    int _disk_size_8243 = NOVALUE;
    int _result_8245 = NOVALUE;
    int _bytes_per_cluster_8246 = NOVALUE;
    int _4438 = NOVALUE;
    int _4437 = NOVALUE;
    int _4436 = NOVALUE;
    int _4435 = NOVALUE;
    int _4434 = NOVALUE;
    int _4433 = NOVALUE;
    int _4432 = NOVALUE;
    int _4430 = NOVALUE;
    int _4429 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence disk_size = {0,0,0, disk_path}*/
    _0 = _disk_size_8243;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    Ref(_disk_path_8242);
    *((int *)(_2+16)) = _disk_path_8242;
    _disk_size_8243 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 	ifdef WINDOWS then*/

    /** 		sequence result */

    /** 		atom bytes_per_cluster*/

    /** 		result = disk_metrics(disk_path) */
    Ref(_disk_path_8242);
    _0 = _result_8245;
    _result_8245 = _9disk_metrics(_disk_path_8242);
    DeRef(_0);

    /** 		bytes_per_cluster = result[BYTES_PER_SECTOR] * result[SECTORS_PER_CLUSTER]*/
    _2 = (int)SEQ_PTR(_result_8245);
    _4429 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_result_8245);
    _4430 = (int)*(((s1_ptr)_2)->base + 1);
    DeRef(_bytes_per_cluster_8246);
    if (IS_ATOM_INT(_4429) && IS_ATOM_INT(_4430)) {
        if (_4429 == (short)_4429 && _4430 <= INT15 && _4430 >= -INT15)
        _bytes_per_cluster_8246 = _4429 * _4430;
        else
        _bytes_per_cluster_8246 = NewDouble(_4429 * (double)_4430);
    }
    else {
        _bytes_per_cluster_8246 = binary_op(MULTIPLY, _4429, _4430);
    }
    _4429 = NOVALUE;
    _4430 = NOVALUE;

    /** 		disk_size[TOTAL_BYTES] = bytes_per_cluster * result[TOTAL_NUMBER_OF_CLUSTERS] */
    _2 = (int)SEQ_PTR(_result_8245);
    _4432 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_bytes_per_cluster_8246) && IS_ATOM_INT(_4432)) {
        if (_bytes_per_cluster_8246 == (short)_bytes_per_cluster_8246 && _4432 <= INT15 && _4432 >= -INT15)
        _4433 = _bytes_per_cluster_8246 * _4432;
        else
        _4433 = NewDouble(_bytes_per_cluster_8246 * (double)_4432);
    }
    else {
        _4433 = binary_op(MULTIPLY, _bytes_per_cluster_8246, _4432);
    }
    _4432 = NOVALUE;
    _2 = (int)SEQ_PTR(_disk_size_8243);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _disk_size_8243 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _4433;
    if( _1 != _4433 ){
        DeRef(_1);
    }
    _4433 = NOVALUE;

    /** 		disk_size[FREE_BYTES]  = bytes_per_cluster * result[NUMBER_OF_FREE_CLUSTERS] */
    _2 = (int)SEQ_PTR(_result_8245);
    _4434 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_bytes_per_cluster_8246) && IS_ATOM_INT(_4434)) {
        if (_bytes_per_cluster_8246 == (short)_bytes_per_cluster_8246 && _4434 <= INT15 && _4434 >= -INT15)
        _4435 = _bytes_per_cluster_8246 * _4434;
        else
        _4435 = NewDouble(_bytes_per_cluster_8246 * (double)_4434);
    }
    else {
        _4435 = binary_op(MULTIPLY, _bytes_per_cluster_8246, _4434);
    }
    _4434 = NOVALUE;
    _2 = (int)SEQ_PTR(_disk_size_8243);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _disk_size_8243 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _4435;
    if( _1 != _4435 ){
        DeRef(_1);
    }
    _4435 = NOVALUE;

    /** 		disk_size[USED_BYTES]  = disk_size[TOTAL_BYTES] - disk_size[FREE_BYTES] */
    _2 = (int)SEQ_PTR(_disk_size_8243);
    _4436 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_disk_size_8243);
    _4437 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_4436) && IS_ATOM_INT(_4437)) {
        _4438 = _4436 - _4437;
        if ((long)((unsigned long)_4438 +(unsigned long) HIGH_BITS) >= 0){
            _4438 = NewDouble((double)_4438);
        }
    }
    else {
        _4438 = binary_op(MINUS, _4436, _4437);
    }
    _4436 = NOVALUE;
    _4437 = NOVALUE;
    _2 = (int)SEQ_PTR(_disk_size_8243);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _disk_size_8243 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _4438;
    if( _1 != _4438 ){
        DeRef(_1);
    }
    _4438 = NOVALUE;

    /** 	return disk_size */
    DeRef(_disk_path_8242);
    DeRefDS(_result_8245);
    DeRef(_bytes_per_cluster_8246);
    return _disk_size_8243;
    ;
}


int _9count_files(int _orig_path_8267, int _dir_info_8268, int _inst_8269)
{
    int _pos_8270 = NOVALUE;
    int _ext_8271 = NOVALUE;
    int _fileext_inlined_fileext_at_243_8314 = NOVALUE;
    int _data_inlined_fileext_at_243_8313 = NOVALUE;
    int _path_inlined_fileext_at_240_8312 = NOVALUE;
    int _4508 = NOVALUE;
    int _4507 = NOVALUE;
    int _4506 = NOVALUE;
    int _4504 = NOVALUE;
    int _4503 = NOVALUE;
    int _4502 = NOVALUE;
    int _4501 = NOVALUE;
    int _4499 = NOVALUE;
    int _4498 = NOVALUE;
    int _4496 = NOVALUE;
    int _4495 = NOVALUE;
    int _4494 = NOVALUE;
    int _4493 = NOVALUE;
    int _4492 = NOVALUE;
    int _4491 = NOVALUE;
    int _4490 = NOVALUE;
    int _4488 = NOVALUE;
    int _4487 = NOVALUE;
    int _4485 = NOVALUE;
    int _4484 = NOVALUE;
    int _4483 = NOVALUE;
    int _4482 = NOVALUE;
    int _4481 = NOVALUE;
    int _4480 = NOVALUE;
    int _4479 = NOVALUE;
    int _4478 = NOVALUE;
    int _4477 = NOVALUE;
    int _4476 = NOVALUE;
    int _4475 = NOVALUE;
    int _4474 = NOVALUE;
    int _4473 = NOVALUE;
    int _4472 = NOVALUE;
    int _4470 = NOVALUE;
    int _4469 = NOVALUE;
    int _4468 = NOVALUE;
    int _4467 = NOVALUE;
    int _4465 = NOVALUE;
    int _4464 = NOVALUE;
    int _4463 = NOVALUE;
    int _4462 = NOVALUE;
    int _4461 = NOVALUE;
    int _4460 = NOVALUE;
    int _4459 = NOVALUE;
    int _4457 = NOVALUE;
    int _4456 = NOVALUE;
    int _4455 = NOVALUE;
    int _4454 = NOVALUE;
    int _4453 = NOVALUE;
    int _4452 = NOVALUE;
    int _4449 = NOVALUE;
    int _4448 = NOVALUE;
    int _4447 = NOVALUE;
    int _4446 = NOVALUE;
    int _4445 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	integer pos = 0*/
    _pos_8270 = 0;

    /** 	orig_path = orig_path*/
    RefDS(_orig_path_8267);
    DeRefDS(_orig_path_8267);
    _orig_path_8267 = _orig_path_8267;

    /** 	if equal(dir_info[D_NAME], ".") then*/
    _2 = (int)SEQ_PTR(_dir_info_8268);
    _4445 = (int)*(((s1_ptr)_2)->base + 1);
    if (_4445 == _3674)
    _4446 = 1;
    else if (IS_ATOM_INT(_4445) && IS_ATOM_INT(_3674))
    _4446 = 0;
    else
    _4446 = (compare(_4445, _3674) == 0);
    _4445 = NOVALUE;
    if (_4446 == 0)
    {
        _4446 = NOVALUE;
        goto L1; // [31] 41
    }
    else{
        _4446 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_orig_path_8267);
    DeRefDS(_dir_info_8268);
    DeRefDS(_inst_8269);
    DeRef(_ext_8271);
    return 0;
L1: 

    /** 	if equal(dir_info[D_NAME], "..") then*/
    _2 = (int)SEQ_PTR(_dir_info_8268);
    _4447 = (int)*(((s1_ptr)_2)->base + 1);
    if (_4447 == _3705)
    _4448 = 1;
    else if (IS_ATOM_INT(_4447) && IS_ATOM_INT(_3705))
    _4448 = 0;
    else
    _4448 = (compare(_4447, _3705) == 0);
    _4447 = NOVALUE;
    if (_4448 == 0)
    {
        _4448 = NOVALUE;
        goto L2; // [53] 63
    }
    else{
        _4448 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_orig_path_8267);
    DeRefDS(_dir_info_8268);
    DeRefDS(_inst_8269);
    DeRef(_ext_8271);
    return 0;
L2: 

    /** 	if inst[1] = 0 then -- count all is false*/
    _2 = (int)SEQ_PTR(_inst_8269);
    _4449 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _4449, 0)){
        _4449 = NOVALUE;
        goto L3; // [69] 120
    }
    _4449 = NOVALUE;

    /** 		if find('h', dir_info[D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_dir_info_8268);
    _4452 = (int)*(((s1_ptr)_2)->base + 2);
    _4453 = find_from(104, _4452, 1);
    _4452 = NOVALUE;
    if (_4453 == 0)
    {
        _4453 = NOVALUE;
        goto L4; // [86] 96
    }
    else{
        _4453 = NOVALUE;
    }

    /** 			return 0*/
    DeRefDS(_orig_path_8267);
    DeRefDS(_dir_info_8268);
    DeRefDS(_inst_8269);
    DeRef(_ext_8271);
    return 0;
L4: 

    /** 		if find('s', dir_info[D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_dir_info_8268);
    _4454 = (int)*(((s1_ptr)_2)->base + 2);
    _4455 = find_from(115, _4454, 1);
    _4454 = NOVALUE;
    if (_4455 == 0)
    {
        _4455 = NOVALUE;
        goto L5; // [109] 119
    }
    else{
        _4455 = NOVALUE;
    }

    /** 			return 0*/
    DeRefDS(_orig_path_8267);
    DeRefDS(_dir_info_8268);
    DeRefDS(_inst_8269);
    DeRef(_ext_8271);
    return 0;
L5: 
L3: 

    /** 	file_counters[inst[2]][COUNT_SIZE] += dir_info[D_SIZE]*/
    _2 = (int)SEQ_PTR(_inst_8269);
    _4456 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_9file_counters_8264);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _9file_counters_8264 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_4456))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_4456)->dbl));
    else
    _3 = (int)(_4456 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_dir_info_8268);
    _4459 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _4460 = (int)*(((s1_ptr)_2)->base + 3);
    _4457 = NOVALUE;
    if (IS_ATOM_INT(_4460) && IS_ATOM_INT(_4459)) {
        _4461 = _4460 + _4459;
        if ((long)((unsigned long)_4461 + (unsigned long)HIGH_BITS) >= 0) 
        _4461 = NewDouble((double)_4461);
    }
    else {
        _4461 = binary_op(PLUS, _4460, _4459);
    }
    _4460 = NOVALUE;
    _4459 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _4461;
    if( _1 != _4461 ){
        DeRef(_1);
    }
    _4461 = NOVALUE;
    _4457 = NOVALUE;

    /** 	if find('d', dir_info[D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_dir_info_8268);
    _4462 = (int)*(((s1_ptr)_2)->base + 2);
    _4463 = find_from(100, _4462, 1);
    _4462 = NOVALUE;
    if (_4463 == 0)
    {
        _4463 = NOVALUE;
        goto L6; // [166] 199
    }
    else{
        _4463 = NOVALUE;
    }

    /** 		file_counters[inst[2]][COUNT_DIRS] += 1*/
    _2 = (int)SEQ_PTR(_inst_8269);
    _4464 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_9file_counters_8264);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _9file_counters_8264 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_4464))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_4464)->dbl));
    else
    _3 = (int)(_4464 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _4467 = (int)*(((s1_ptr)_2)->base + 1);
    _4465 = NOVALUE;
    if (IS_ATOM_INT(_4467)) {
        _4468 = _4467 + 1;
        if (_4468 > MAXINT){
            _4468 = NewDouble((double)_4468);
        }
    }
    else
    _4468 = binary_op(PLUS, 1, _4467);
    _4467 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _4468;
    if( _1 != _4468 ){
        DeRef(_1);
    }
    _4468 = NOVALUE;
    _4465 = NOVALUE;
    goto L7; // [196] 504
L6: 

    /** 		file_counters[inst[2]][COUNT_FILES] += 1*/
    _2 = (int)SEQ_PTR(_inst_8269);
    _4469 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_9file_counters_8264);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _9file_counters_8264 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_4469))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_4469)->dbl));
    else
    _3 = (int)(_4469 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _4472 = (int)*(((s1_ptr)_2)->base + 2);
    _4470 = NOVALUE;
    if (IS_ATOM_INT(_4472)) {
        _4473 = _4472 + 1;
        if (_4473 > MAXINT){
            _4473 = NewDouble((double)_4473);
        }
    }
    else
    _4473 = binary_op(PLUS, 1, _4472);
    _4472 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _4473;
    if( _1 != _4473 ){
        DeRef(_1);
    }
    _4473 = NOVALUE;
    _4470 = NOVALUE;

    /** 		ifdef not UNIX then*/

    /** 			ext = fileext(lower(dir_info[D_NAME]))*/
    _2 = (int)SEQ_PTR(_dir_info_8268);
    _4474 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_4474);
    _4475 = _4lower(_4474);
    _4474 = NOVALUE;
    DeRef(_path_inlined_fileext_at_240_8312);
    _path_inlined_fileext_at_240_8312 = _4475;
    _4475 = NOVALUE;

    /** 	data = pathinfo(path)*/
    Ref(_path_inlined_fileext_at_240_8312);
    _0 = _data_inlined_fileext_at_243_8313;
    _data_inlined_fileext_at_243_8313 = _9pathinfo(_path_inlined_fileext_at_240_8312, 0);
    DeRef(_0);

    /** 	return data[4]*/
    DeRef(_ext_8271);
    _2 = (int)SEQ_PTR(_data_inlined_fileext_at_243_8313);
    _ext_8271 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_ext_8271);
    DeRef(_path_inlined_fileext_at_240_8312);
    _path_inlined_fileext_at_240_8312 = NOVALUE;
    DeRef(_data_inlined_fileext_at_243_8313);
    _data_inlined_fileext_at_243_8313 = NOVALUE;

    /** 		pos = 0*/
    _pos_8270 = 0;

    /** 		for i = 1 to length(file_counters[inst[2]][COUNT_TYPES]) do*/
    _2 = (int)SEQ_PTR(_inst_8269);
    _4476 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_9file_counters_8264);
    if (!IS_ATOM_INT(_4476)){
        _4477 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_4476)->dbl));
    }
    else{
        _4477 = (int)*(((s1_ptr)_2)->base + _4476);
    }
    _2 = (int)SEQ_PTR(_4477);
    _4478 = (int)*(((s1_ptr)_2)->base + 4);
    _4477 = NOVALUE;
    if (IS_SEQUENCE(_4478)){
            _4479 = SEQ_PTR(_4478)->length;
    }
    else {
        _4479 = 1;
    }
    _4478 = NOVALUE;
    {
        int _i_8316;
        _i_8316 = 1;
L8: 
        if (_i_8316 > _4479){
            goto L9; // [291] 352
        }

        /** 			if equal(file_counters[inst[2]][COUNT_TYPES][i][EXT_NAME], ext) then*/
        _2 = (int)SEQ_PTR(_inst_8269);
        _4480 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_9file_counters_8264);
        if (!IS_ATOM_INT(_4480)){
            _4481 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_4480)->dbl));
        }
        else{
            _4481 = (int)*(((s1_ptr)_2)->base + _4480);
        }
        _2 = (int)SEQ_PTR(_4481);
        _4482 = (int)*(((s1_ptr)_2)->base + 4);
        _4481 = NOVALUE;
        _2 = (int)SEQ_PTR(_4482);
        _4483 = (int)*(((s1_ptr)_2)->base + _i_8316);
        _4482 = NOVALUE;
        _2 = (int)SEQ_PTR(_4483);
        _4484 = (int)*(((s1_ptr)_2)->base + 1);
        _4483 = NOVALUE;
        if (_4484 == _ext_8271)
        _4485 = 1;
        else if (IS_ATOM_INT(_4484) && IS_ATOM_INT(_ext_8271))
        _4485 = 0;
        else
        _4485 = (compare(_4484, _ext_8271) == 0);
        _4484 = NOVALUE;
        if (_4485 == 0)
        {
            _4485 = NOVALUE;
            goto LA; // [332] 345
        }
        else{
            _4485 = NOVALUE;
        }

        /** 				pos = i*/
        _pos_8270 = _i_8316;

        /** 				exit*/
        goto L9; // [342] 352
LA: 

        /** 		end for*/
        _i_8316 = _i_8316 + 1;
        goto L8; // [347] 298
L9: 
        ;
    }

    /** 		if pos = 0 then*/
    if (_pos_8270 != 0)
    goto LB; // [354] 419

    /** 			file_counters[inst[2]][COUNT_TYPES] &= {{ext, 0, 0}}*/
    _2 = (int)SEQ_PTR(_inst_8269);
    _4487 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_9file_counters_8264);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _9file_counters_8264 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_4487))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_4487)->dbl));
    else
    _3 = (int)(_4487 + ((s1_ptr)_2)->base);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_ext_8271);
    *((int *)(_2+4)) = _ext_8271;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    _4490 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _4490;
    _4491 = MAKE_SEQ(_1);
    _4490 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    _4492 = (int)*(((s1_ptr)_2)->base + 4);
    _4488 = NOVALUE;
    if (IS_SEQUENCE(_4492) && IS_ATOM(_4491)) {
    }
    else if (IS_ATOM(_4492) && IS_SEQUENCE(_4491)) {
        Ref(_4492);
        Prepend(&_4493, _4491, _4492);
    }
    else {
        Concat((object_ptr)&_4493, _4492, _4491);
        _4492 = NOVALUE;
    }
    _4492 = NOVALUE;
    DeRefDS(_4491);
    _4491 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _4493;
    if( _1 != _4493 ){
        DeRef(_1);
    }
    _4493 = NOVALUE;
    _4488 = NOVALUE;

    /** 			pos = length(file_counters[inst[2]][COUNT_TYPES])*/
    _2 = (int)SEQ_PTR(_inst_8269);
    _4494 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_9file_counters_8264);
    if (!IS_ATOM_INT(_4494)){
        _4495 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_4494)->dbl));
    }
    else{
        _4495 = (int)*(((s1_ptr)_2)->base + _4494);
    }
    _2 = (int)SEQ_PTR(_4495);
    _4496 = (int)*(((s1_ptr)_2)->base + 4);
    _4495 = NOVALUE;
    if (IS_SEQUENCE(_4496)){
            _pos_8270 = SEQ_PTR(_4496)->length;
    }
    else {
        _pos_8270 = 1;
    }
    _4496 = NOVALUE;
LB: 

    /** 		file_counters[inst[2]][COUNT_TYPES][pos][EXT_COUNT] += 1*/
    _2 = (int)SEQ_PTR(_inst_8269);
    _4498 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_9file_counters_8264);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _9file_counters_8264 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_4498))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_4498)->dbl));
    else
    _3 = (int)(_4498 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    _3 = (int)(4 + ((s1_ptr)_2)->base);
    _4499 = NOVALUE;
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    _3 = (int)(_pos_8270 + ((s1_ptr)_2)->base);
    _4499 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    _4501 = (int)*(((s1_ptr)_2)->base + 2);
    _4499 = NOVALUE;
    if (IS_ATOM_INT(_4501)) {
        _4502 = _4501 + 1;
        if (_4502 > MAXINT){
            _4502 = NewDouble((double)_4502);
        }
    }
    else
    _4502 = binary_op(PLUS, 1, _4501);
    _4501 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _4502;
    if( _1 != _4502 ){
        DeRef(_1);
    }
    _4502 = NOVALUE;
    _4499 = NOVALUE;

    /** 		file_counters[inst[2]][COUNT_TYPES][pos][EXT_SIZE] += dir_info[D_SIZE]*/
    _2 = (int)SEQ_PTR(_inst_8269);
    _4503 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_9file_counters_8264);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _9file_counters_8264 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_4503))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_4503)->dbl));
    else
    _3 = (int)(_4503 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    _3 = (int)(4 + ((s1_ptr)_2)->base);
    _4504 = NOVALUE;
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    _3 = (int)(_pos_8270 + ((s1_ptr)_2)->base);
    _4504 = NOVALUE;
    _2 = (int)SEQ_PTR(_dir_info_8268);
    _4506 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _4507 = (int)*(((s1_ptr)_2)->base + 3);
    _4504 = NOVALUE;
    if (IS_ATOM_INT(_4507) && IS_ATOM_INT(_4506)) {
        _4508 = _4507 + _4506;
        if ((long)((unsigned long)_4508 + (unsigned long)HIGH_BITS) >= 0) 
        _4508 = NewDouble((double)_4508);
    }
    else {
        _4508 = binary_op(PLUS, _4507, _4506);
    }
    _4507 = NOVALUE;
    _4506 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _4508;
    if( _1 != _4508 ){
        DeRef(_1);
    }
    _4508 = NOVALUE;
    _4504 = NOVALUE;
L7: 

    /** 	return 0*/
    DeRefDS(_orig_path_8267);
    DeRefDS(_dir_info_8268);
    DeRefDS(_inst_8269);
    DeRef(_ext_8271);
    _4456 = NOVALUE;
    _4464 = NOVALUE;
    _4469 = NOVALUE;
    _4476 = NOVALUE;
    _4478 = NOVALUE;
    _4480 = NOVALUE;
    _4487 = NOVALUE;
    _4494 = NOVALUE;
    _4496 = NOVALUE;
    _4498 = NOVALUE;
    _4503 = NOVALUE;
    return 0;
    ;
}


int _9dir_size(int _dir_path_8354, int _count_all_8355)
{
    int _fc_8356 = NOVALUE;
    int _4523 = NOVALUE;
    int _4522 = NOVALUE;
    int _4520 = NOVALUE;
    int _4519 = NOVALUE;
    int _4517 = NOVALUE;
    int _4516 = NOVALUE;
    int _4515 = NOVALUE;
    int _4514 = NOVALUE;
    int _4513 = NOVALUE;
    int _4512 = NOVALUE;
    int _4509 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_count_all_8355)) {
        _1 = (long)(DBL_PTR(_count_all_8355)->dbl);
        if (UNIQUE(DBL_PTR(_count_all_8355)) && (DBL_PTR(_count_all_8355)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_count_all_8355);
        _count_all_8355 = _1;
    }

    /** 	file_counters = append(file_counters, {0,0,0,{}})*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    RefDS(_5);
    *((int *)(_2+16)) = _5;
    _4509 = MAKE_SEQ(_1);
    RefDS(_4509);
    Append(&_9file_counters_8264, _9file_counters_8264, _4509);
    DeRefDS(_4509);
    _4509 = NOVALUE;

    /** 	walk_dir(dir_path, {routine_id("count_files"), {count_all, length(file_counters)}}, 0)*/
    _4512 = CRoutineId(357, 9, _4511);
    if (IS_SEQUENCE(_9file_counters_8264)){
            _4513 = SEQ_PTR(_9file_counters_8264)->length;
    }
    else {
        _4513 = 1;
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _count_all_8355;
    ((int *)_2)[2] = _4513;
    _4514 = MAKE_SEQ(_1);
    _4513 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _4512;
    ((int *)_2)[2] = _4514;
    _4515 = MAKE_SEQ(_1);
    _4514 = NOVALUE;
    _4512 = NOVALUE;
    RefDS(_dir_path_8354);
    _4516 = _9walk_dir(_dir_path_8354, _4515, 0, -99999);
    _4515 = NOVALUE;

    /** 	fc = file_counters[$]*/
    if (IS_SEQUENCE(_9file_counters_8264)){
            _4517 = SEQ_PTR(_9file_counters_8264)->length;
    }
    else {
        _4517 = 1;
    }
    DeRef(_fc_8356);
    _2 = (int)SEQ_PTR(_9file_counters_8264);
    _fc_8356 = (int)*(((s1_ptr)_2)->base + _4517);
    RefDS(_fc_8356);

    /** 	file_counters = file_counters[1 .. $-1]*/
    if (IS_SEQUENCE(_9file_counters_8264)){
            _4519 = SEQ_PTR(_9file_counters_8264)->length;
    }
    else {
        _4519 = 1;
    }
    _4520 = _4519 - 1;
    _4519 = NOVALUE;
    rhs_slice_target = (object_ptr)&_9file_counters_8264;
    RHS_Slice(_9file_counters_8264, 1, _4520);

    /** 	fc[COUNT_TYPES] = stdsort:sort(fc[COUNT_TYPES])*/
    _2 = (int)SEQ_PTR(_fc_8356);
    _4522 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_4522);
    _4523 = _22sort(_4522, 1);
    _4522 = NOVALUE;
    _2 = (int)SEQ_PTR(_fc_8356);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _fc_8356 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _4523;
    if( _1 != _4523 ){
        DeRef(_1);
    }
    _4523 = NOVALUE;

    /** 	return fc*/
    DeRefDS(_dir_path_8354);
    _4520 = NOVALUE;
    DeRef(_4516);
    _4516 = NOVALUE;
    return _fc_8356;
    ;
}


int _9temp_file(int _temp_location_8374, int _temp_prefix_8375, int _temp_extn_8376, int _reserve_temp_8378)
{
    int _randname_8379 = NOVALUE;
    int _envtmp_8383 = NOVALUE;
    int _tdir_8402 = NOVALUE;
    int _4562 = NOVALUE;
    int _4560 = NOVALUE;
    int _4558 = NOVALUE;
    int _4556 = NOVALUE;
    int _4554 = NOVALUE;
    int _4553 = NOVALUE;
    int _4552 = NOVALUE;
    int _4548 = NOVALUE;
    int _4547 = NOVALUE;
    int _4546 = NOVALUE;
    int _4545 = NOVALUE;
    int _4542 = NOVALUE;
    int _4541 = NOVALUE;
    int _4540 = NOVALUE;
    int _4535 = NOVALUE;
    int _4532 = NOVALUE;
    int _4529 = NOVALUE;
    int _4525 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_reserve_temp_8378)) {
        _1 = (long)(DBL_PTR(_reserve_temp_8378)->dbl);
        if (UNIQUE(DBL_PTR(_reserve_temp_8378)) && (DBL_PTR(_reserve_temp_8378)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_reserve_temp_8378);
        _reserve_temp_8378 = _1;
    }

    /** 	if length(temp_location) = 0 then*/
    if (IS_SEQUENCE(_temp_location_8374)){
            _4525 = SEQ_PTR(_temp_location_8374)->length;
    }
    else {
        _4525 = 1;
    }
    if (_4525 != 0)
    goto L1; // [14] 67

    /** 		object envtmp*/

    /** 		envtmp = getenv("TEMP")*/
    DeRefi(_envtmp_8383);
    _envtmp_8383 = EGetEnv(_4527);

    /** 		if atom(envtmp) then*/
    _4529 = IS_ATOM(_envtmp_8383);
    if (_4529 == 0)
    {
        _4529 = NOVALUE;
        goto L2; // [30] 39
    }
    else{
        _4529 = NOVALUE;
    }

    /** 			envtmp = getenv("TMP")*/
    DeRefi(_envtmp_8383);
    _envtmp_8383 = EGetEnv(_4530);
L2: 

    /** 		ifdef WINDOWS then			*/

    /** 			if atom(envtmp) then*/
    _4532 = IS_ATOM(_envtmp_8383);
    if (_4532 == 0)
    {
        _4532 = NOVALUE;
        goto L3; // [46] 55
    }
    else{
        _4532 = NOVALUE;
    }

    /** 				envtmp = "C:\\temp\\"*/
    RefDS(_4533);
    DeRefi(_envtmp_8383);
    _envtmp_8383 = _4533;
L3: 

    /** 		temp_location = envtmp*/
    Ref(_envtmp_8383);
    DeRefDS(_temp_location_8374);
    _temp_location_8374 = _envtmp_8383;
    DeRefi(_envtmp_8383);
    _envtmp_8383 = NOVALUE;
    goto L4; // [64] 161
L1: 

    /** 		switch file_type(temp_location) do*/
    RefDS(_temp_location_8374);
    _4535 = _9file_type(_temp_location_8374);
    if (IS_SEQUENCE(_4535) ){
        goto L5; // [73] 150
    }
    if(!IS_ATOM_INT(_4535)){
        if( (DBL_PTR(_4535)->dbl != (double) ((int) DBL_PTR(_4535)->dbl) ) ){
            goto L5; // [73] 150
        }
        _0 = (int) DBL_PTR(_4535)->dbl;
    }
    else {
        _0 = _4535;
    };
    DeRef(_4535);
    _4535 = NOVALUE;
    switch ( _0 ){ 

        /** 			case FILETYPE_FILE then*/
        case 1:

        /** 				temp_location = dirname(temp_location, 1)*/
        RefDS(_temp_location_8374);
        _0 = _temp_location_8374;
        _temp_location_8374 = _9dirname(_temp_location_8374, 1);
        DeRefDS(_0);
        goto L6; // [91] 160

        /** 			case FILETYPE_DIRECTORY then*/
        case 2:

        /** 				temp_location = temp_location*/
        RefDS(_temp_location_8374);
        DeRefDS(_temp_location_8374);
        _temp_location_8374 = _temp_location_8374;
        goto L6; // [104] 160

        /** 			case FILETYPE_NOT_FOUND then*/
        case 0:

        /** 				object tdir = dirname(temp_location, 1)*/
        RefDS(_temp_location_8374);
        _0 = _tdir_8402;
        _tdir_8402 = _9dirname(_temp_location_8374, 1);
        DeRef(_0);

        /** 				if file_exists(tdir) then*/
        Ref(_tdir_8402);
        _4540 = _9file_exists(_tdir_8402);
        if (_4540 == 0) {
            DeRef(_4540);
            _4540 = NOVALUE;
            goto L7; // [123] 136
        }
        else {
            if (!IS_ATOM_INT(_4540) && DBL_PTR(_4540)->dbl == 0.0){
                DeRef(_4540);
                _4540 = NOVALUE;
                goto L7; // [123] 136
            }
            DeRef(_4540);
            _4540 = NOVALUE;
        }
        DeRef(_4540);
        _4540 = NOVALUE;

        /** 					temp_location = tdir*/
        Ref(_tdir_8402);
        DeRefDS(_temp_location_8374);
        _temp_location_8374 = _tdir_8402;
        goto L8; // [133] 144
L7: 

        /** 					temp_location = "."*/
        RefDS(_3674);
        DeRefDS(_temp_location_8374);
        _temp_location_8374 = _3674;
L8: 
        DeRef(_tdir_8402);
        _tdir_8402 = NOVALUE;
        goto L6; // [146] 160

        /** 			case else*/
        default:
L5: 

        /** 				temp_location = "."*/
        RefDS(_3674);
        DeRefDS(_temp_location_8374);
        _temp_location_8374 = _3674;
    ;}L6: 
L4: 

    /** 	if temp_location[$] != SLASH then*/
    if (IS_SEQUENCE(_temp_location_8374)){
            _4541 = SEQ_PTR(_temp_location_8374)->length;
    }
    else {
        _4541 = 1;
    }
    _2 = (int)SEQ_PTR(_temp_location_8374);
    _4542 = (int)*(((s1_ptr)_2)->base + _4541);
    if (binary_op_a(EQUALS, _4542, 92)){
        _4542 = NOVALUE;
        goto L9; // [170] 181
    }
    _4542 = NOVALUE;

    /** 		temp_location &= SLASH*/
    Append(&_temp_location_8374, _temp_location_8374, 92);
L9: 

    /** 	if length(temp_extn) and temp_extn[1] != '.' then*/
    if (IS_SEQUENCE(_temp_extn_8376)){
            _4545 = SEQ_PTR(_temp_extn_8376)->length;
    }
    else {
        _4545 = 1;
    }
    if (_4545 == 0) {
        goto LA; // [186] 209
    }
    _2 = (int)SEQ_PTR(_temp_extn_8376);
    _4547 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_4547)) {
        _4548 = (_4547 != 46);
    }
    else {
        _4548 = binary_op(NOTEQ, _4547, 46);
    }
    _4547 = NOVALUE;
    if (_4548 == 0) {
        DeRef(_4548);
        _4548 = NOVALUE;
        goto LA; // [199] 209
    }
    else {
        if (!IS_ATOM_INT(_4548) && DBL_PTR(_4548)->dbl == 0.0){
            DeRef(_4548);
            _4548 = NOVALUE;
            goto LA; // [199] 209
        }
        DeRef(_4548);
        _4548 = NOVALUE;
    }
    DeRef(_4548);
    _4548 = NOVALUE;

    /** 		temp_extn = '.' & temp_extn*/
    Prepend(&_temp_extn_8376, _temp_extn_8376, 46);
LA: 

    /** 	while 1 do*/
LB: 

    /** 		randname = sprintf("%s%s%06d%s", {temp_location, temp_prefix, rand(1_000_000) - 1, temp_extn})*/
    _4552 = good_rand() % ((unsigned)1000000) + 1;
    _4553 = _4552 - 1;
    _4552 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_temp_location_8374);
    *((int *)(_2+4)) = _temp_location_8374;
    RefDS(_temp_prefix_8375);
    *((int *)(_2+8)) = _temp_prefix_8375;
    *((int *)(_2+12)) = _4553;
    RefDS(_temp_extn_8376);
    *((int *)(_2+16)) = _temp_extn_8376;
    _4554 = MAKE_SEQ(_1);
    _4553 = NOVALUE;
    DeRefi(_randname_8379);
    _randname_8379 = EPrintf(-9999999, _4550, _4554);
    DeRefDS(_4554);
    _4554 = NOVALUE;

    /** 		if not file_exists( randname ) then*/
    RefDS(_randname_8379);
    _4556 = _9file_exists(_randname_8379);
    if (IS_ATOM_INT(_4556)) {
        if (_4556 != 0){
            DeRef(_4556);
            _4556 = NOVALUE;
            goto LB; // [240] 214
        }
    }
    else {
        if (DBL_PTR(_4556)->dbl != 0.0){
            DeRef(_4556);
            _4556 = NOVALUE;
            goto LB; // [240] 214
        }
    }
    DeRef(_4556);
    _4556 = NOVALUE;

    /** 			exit*/
    goto LC; // [245] 253

    /** 	end while*/
    goto LB; // [250] 214
LC: 

    /** 	if reserve_temp then*/
    if (_reserve_temp_8378 == 0)
    {
        goto LD; // [255] 300
    }
    else{
    }

    /** 		if not file_exists(temp_location) then*/
    RefDS(_temp_location_8374);
    _4558 = _9file_exists(_temp_location_8374);
    if (IS_ATOM_INT(_4558)) {
        if (_4558 != 0){
            DeRef(_4558);
            _4558 = NOVALUE;
            goto LE; // [264] 287
        }
    }
    else {
        if (DBL_PTR(_4558)->dbl != 0.0){
            DeRef(_4558);
            _4558 = NOVALUE;
            goto LE; // [264] 287
        }
    }
    DeRef(_4558);
    _4558 = NOVALUE;

    /** 			if create_directory(temp_location) = 0 then*/
    RefDS(_temp_location_8374);
    _4560 = _9create_directory(_temp_location_8374, 448, 1);
    if (binary_op_a(NOTEQ, _4560, 0)){
        DeRef(_4560);
        _4560 = NOVALUE;
        goto LF; // [275] 286
    }
    DeRef(_4560);
    _4560 = NOVALUE;

    /** 				return ""*/
    RefDS(_5);
    DeRefDS(_temp_location_8374);
    DeRefDS(_temp_prefix_8375);
    DeRefDS(_temp_extn_8376);
    DeRefi(_randname_8379);
    return _5;
LF: 
LE: 

    /** 		io:write_file(randname, "")*/
    RefDS(_randname_8379);
    RefDS(_5);
    _4562 = _16write_file(_randname_8379, _5, 1);
LD: 

    /** 	return randname*/
    DeRefDS(_temp_location_8374);
    DeRefDS(_temp_prefix_8375);
    DeRefDS(_temp_extn_8376);
    DeRef(_4562);
    _4562 = NOVALUE;
    return _randname_8379;
    ;
}


int _9checksum(int _filename_8439, int _size_8440, int _usename_8441, int _return_text_8442)
{
    int _fn_8443 = NOVALUE;
    int _cs_8444 = NOVALUE;
    int _hits_8445 = NOVALUE;
    int _ix_8446 = NOVALUE;
    int _jx_8447 = NOVALUE;
    int _fx_8448 = NOVALUE;
    int _data_8449 = NOVALUE;
    int _nhit_8450 = NOVALUE;
    int _nmiss_8451 = NOVALUE;
    int _cs_text_8523 = NOVALUE;
    int _4622 = NOVALUE;
    int _4620 = NOVALUE;
    int _4619 = NOVALUE;
    int _4617 = NOVALUE;
    int _4615 = NOVALUE;
    int _4614 = NOVALUE;
    int _4613 = NOVALUE;
    int _4612 = NOVALUE;
    int _4611 = NOVALUE;
    int _4609 = NOVALUE;
    int _4607 = NOVALUE;
    int _4605 = NOVALUE;
    int _4604 = NOVALUE;
    int _4603 = NOVALUE;
    int _4602 = NOVALUE;
    int _4601 = NOVALUE;
    int _4600 = NOVALUE;
    int _4599 = NOVALUE;
    int _4597 = NOVALUE;
    int _4594 = NOVALUE;
    int _4592 = NOVALUE;
    int _4591 = NOVALUE;
    int _4590 = NOVALUE;
    int _4589 = NOVALUE;
    int _4588 = NOVALUE;
    int _4585 = NOVALUE;
    int _4584 = NOVALUE;
    int _4583 = NOVALUE;
    int _4582 = NOVALUE;
    int _4580 = NOVALUE;
    int _4577 = NOVALUE;
    int _4575 = NOVALUE;
    int _4571 = NOVALUE;
    int _4570 = NOVALUE;
    int _4569 = NOVALUE;
    int _4568 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_size_8440)) {
        _1 = (long)(DBL_PTR(_size_8440)->dbl);
        if (UNIQUE(DBL_PTR(_size_8440)) && (DBL_PTR(_size_8440)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_8440);
        _size_8440 = _1;
    }
    if (!IS_ATOM_INT(_usename_8441)) {
        _1 = (long)(DBL_PTR(_usename_8441)->dbl);
        if (UNIQUE(DBL_PTR(_usename_8441)) && (DBL_PTR(_usename_8441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_usename_8441);
        _usename_8441 = _1;
    }
    if (!IS_ATOM_INT(_return_text_8442)) {
        _1 = (long)(DBL_PTR(_return_text_8442)->dbl);
        if (UNIQUE(DBL_PTR(_return_text_8442)) && (DBL_PTR(_return_text_8442)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_return_text_8442);
        _return_text_8442 = _1;
    }

    /** 	if size <= 0 then*/
    if (_size_8440 > 0)
    goto L1; // [11] 22

    /** 		return {}*/
    RefDS(_5);
    DeRefDS(_filename_8439);
    DeRef(_cs_8444);
    DeRef(_hits_8445);
    DeRef(_jx_8447);
    DeRef(_fx_8448);
    DeRefi(_data_8449);
    return _5;
L1: 

    /** 	fn = open(filename, "rb")*/
    _fn_8443 = EOpen(_filename_8439, _1138, 0);

    /** 	if fn = -1 then*/
    if (_fn_8443 != -1)
    goto L2; // [31] 42

    /** 		return {}*/
    RefDS(_5);
    DeRefDS(_filename_8439);
    DeRef(_cs_8444);
    DeRef(_hits_8445);
    DeRef(_jx_8447);
    DeRef(_fx_8448);
    DeRefi(_data_8449);
    return _5;
L2: 

    /** 	jx = file_length(filename)*/
    RefDS(_filename_8439);
    _0 = _jx_8447;
    _jx_8447 = _9file_length(_filename_8439);
    DeRef(_0);

    /** 	cs = repeat(jx, size)*/
    DeRef(_cs_8444);
    _cs_8444 = Repeat(_jx_8447, _size_8440);

    /** 	for i = 1 to size do*/
    _4568 = _size_8440;
    {
        int _i_8460;
        _i_8460 = 1;
L3: 
        if (_i_8460 > _4568){
            goto L4; // [59] 91
        }

        /** 		cs[i] = hash(i + size, cs[i])*/
        _4569 = _i_8460 + _size_8440;
        if ((long)((unsigned long)_4569 + (unsigned long)HIGH_BITS) >= 0) 
        _4569 = NewDouble((double)_4569);
        _2 = (int)SEQ_PTR(_cs_8444);
        _4570 = (int)*(((s1_ptr)_2)->base + _i_8460);
        _4571 = calc_hash(_4569, _4570);
        DeRef(_4569);
        _4569 = NOVALUE;
        _4570 = NOVALUE;
        _2 = (int)SEQ_PTR(_cs_8444);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _cs_8444 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_8460);
        _1 = *(int *)_2;
        *(int *)_2 = _4571;
        if( _1 != _4571 ){
            DeRef(_1);
        }
        _4571 = NOVALUE;

        /** 	end for*/
        _i_8460 = _i_8460 + 1;
        goto L3; // [86] 66
L4: 
        ;
    }

    /** 	if usename != 0 then*/
    if (_usename_8441 == 0)
    goto L5; // [93] 216

    /** 		nhit = 0*/
    _nhit_8450 = 0;

    /** 		nmiss = 0*/
    _nmiss_8451 = 0;

    /** 		hits = {0,0}*/
    DeRef(_hits_8445);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 0;
    _hits_8445 = MAKE_SEQ(_1);

    /** 		fx = hash(filename, stdhash:HSIEH32) -- Get a hash value for the whole name.*/
    DeRef(_fx_8448);
    _fx_8448 = calc_hash(_filename_8439, -5);

    /** 		while find(0, hits) do*/
L6: 
    _4575 = find_from(0, _hits_8445, 1);
    if (_4575 == 0)
    {
        _4575 = NOVALUE;
        goto L7; // [131] 215
    }
    else{
        _4575 = NOVALUE;
    }

    /** 			nhit += 1*/
    _nhit_8450 = _nhit_8450 + 1;

    /** 			if nhit > length(filename) then*/
    if (IS_SEQUENCE(_filename_8439)){
            _4577 = SEQ_PTR(_filename_8439)->length;
    }
    else {
        _4577 = 1;
    }
    if (_nhit_8450 <= _4577)
    goto L8; // [145] 161

    /** 				nhit = 1*/
    _nhit_8450 = 1;

    /** 				hits[1] = 1*/
    _2 = (int)SEQ_PTR(_hits_8445);
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
L8: 

    /** 			nmiss += 1*/
    _nmiss_8451 = _nmiss_8451 + 1;

    /** 			if nmiss > length(cs) then*/
    if (IS_SEQUENCE(_cs_8444)){
            _4580 = SEQ_PTR(_cs_8444)->length;
    }
    else {
        _4580 = 1;
    }
    if (_nmiss_8451 <= _4580)
    goto L9; // [172] 188

    /** 				nmiss = 1*/
    _nmiss_8451 = 1;

    /** 				hits[2] = 1*/
    _2 = (int)SEQ_PTR(_hits_8445);
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
L9: 

    /** 			cs[nmiss] = hash(filename[nhit], xor_bits(fx, cs[nmiss]))*/
    _2 = (int)SEQ_PTR(_filename_8439);
    _4582 = (int)*(((s1_ptr)_2)->base + _nhit_8450);
    _2 = (int)SEQ_PTR(_cs_8444);
    _4583 = (int)*(((s1_ptr)_2)->base + _nmiss_8451);
    if (IS_ATOM_INT(_fx_8448) && IS_ATOM_INT(_4583)) {
        {unsigned long tu;
             tu = (unsigned long)_fx_8448 ^ (unsigned long)_4583;
             _4584 = MAKE_UINT(tu);
        }
    }
    else {
        _4584 = binary_op(XOR_BITS, _fx_8448, _4583);
    }
    _4583 = NOVALUE;
    _4585 = calc_hash(_4582, _4584);
    _4582 = NOVALUE;
    DeRef(_4584);
    _4584 = NOVALUE;
    _2 = (int)SEQ_PTR(_cs_8444);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _cs_8444 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _nmiss_8451);
    _1 = *(int *)_2;
    *(int *)_2 = _4585;
    if( _1 != _4585 ){
        DeRef(_1);
    }
    _4585 = NOVALUE;

    /** 		end while -- repeat until every bucket and every character has been used.*/
    goto L6; // [212] 126
L7: 
L5: 

    /** 	hits = repeat(0, size)*/
    DeRef(_hits_8445);
    _hits_8445 = Repeat(0, _size_8440);

    /** 	if jx != 0 then*/
    if (binary_op_a(EQUALS, _jx_8447, 0)){
        goto LA; // [224] 464
    }

    /** 		data = repeat(0, remainder( hash(jx * jx / size , stdhash:HSIEH32), 8) + 7)*/
    if (IS_ATOM_INT(_jx_8447) && IS_ATOM_INT(_jx_8447)) {
        if (_jx_8447 == (short)_jx_8447 && _jx_8447 <= INT15 && _jx_8447 >= -INT15)
        _4588 = _jx_8447 * _jx_8447;
        else
        _4588 = NewDouble(_jx_8447 * (double)_jx_8447);
    }
    else {
        if (IS_ATOM_INT(_jx_8447)) {
            _4588 = NewDouble((double)_jx_8447 * DBL_PTR(_jx_8447)->dbl);
        }
        else {
            if (IS_ATOM_INT(_jx_8447)) {
                _4588 = NewDouble(DBL_PTR(_jx_8447)->dbl * (double)_jx_8447);
            }
            else
            _4588 = NewDouble(DBL_PTR(_jx_8447)->dbl * DBL_PTR(_jx_8447)->dbl);
        }
    }
    if (IS_ATOM_INT(_4588)) {
        _4589 = (_4588 % _size_8440) ? NewDouble((double)_4588 / _size_8440) : (_4588 / _size_8440);
    }
    else {
        _4589 = NewDouble(DBL_PTR(_4588)->dbl / (double)_size_8440);
    }
    DeRef(_4588);
    _4588 = NOVALUE;
    _4590 = calc_hash(_4589, -5);
    DeRef(_4589);
    _4589 = NOVALUE;
    if (IS_ATOM_INT(_4590)) {
        _4591 = (_4590 % 8);
    }
    else {
        temp_d.dbl = (double)8;
        _4591 = Dremainder(DBL_PTR(_4590), &temp_d);
    }
    DeRef(_4590);
    _4590 = NOVALUE;
    if (IS_ATOM_INT(_4591)) {
        _4592 = _4591 + 7;
    }
    else {
        _4592 = NewDouble(DBL_PTR(_4591)->dbl + (double)7);
    }
    DeRef(_4591);
    _4591 = NOVALUE;
    DeRefi(_data_8449);
    _data_8449 = Repeat(0, _4592);
    DeRef(_4592);
    _4592 = NOVALUE;

    /** 		while data[1] != -1 with entry do*/
    goto LB; // [258] 322
LC: 
    _2 = (int)SEQ_PTR(_data_8449);
    _4594 = (int)*(((s1_ptr)_2)->base + 1);
    if (_4594 == -1)
    goto LD; // [265] 355

    /** 			jx = hash(jx, data)*/
    _0 = _jx_8447;
    _jx_8447 = calc_hash(_jx_8447, _data_8449);
    DeRef(_0);

    /** 			ix = remainder(jx, size) + 1*/
    if (IS_ATOM_INT(_jx_8447)) {
        _4597 = (_jx_8447 % _size_8440);
    }
    else {
        temp_d.dbl = (double)_size_8440;
        _4597 = Dremainder(DBL_PTR(_jx_8447), &temp_d);
    }
    if (IS_ATOM_INT(_4597)) {
        _ix_8446 = _4597 + 1;
    }
    else
    { // coercing _ix_8446 to an integer 1
        _ix_8446 = 1+(long)(DBL_PTR(_4597)->dbl);
        if( !IS_ATOM_INT(_ix_8446) ){
            _ix_8446 = (object)DBL_PTR(_ix_8446)->dbl;
        }
    }
    DeRef(_4597);
    _4597 = NOVALUE;

    /** 			cs[ix] = xor_bits(cs[ix], hash(data, stdhash:HSIEH32))*/
    _2 = (int)SEQ_PTR(_cs_8444);
    _4599 = (int)*(((s1_ptr)_2)->base + _ix_8446);
    _4600 = calc_hash(_data_8449, -5);
    if (IS_ATOM_INT(_4599) && IS_ATOM_INT(_4600)) {
        {unsigned long tu;
             tu = (unsigned long)_4599 ^ (unsigned long)_4600;
             _4601 = MAKE_UINT(tu);
        }
    }
    else {
        _4601 = binary_op(XOR_BITS, _4599, _4600);
    }
    _4599 = NOVALUE;
    DeRef(_4600);
    _4600 = NOVALUE;
    _2 = (int)SEQ_PTR(_cs_8444);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _cs_8444 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _ix_8446);
    _1 = *(int *)_2;
    *(int *)_2 = _4601;
    if( _1 != _4601 ){
        DeRef(_1);
    }
    _4601 = NOVALUE;

    /** 			hits[ix] += 1*/
    _2 = (int)SEQ_PTR(_hits_8445);
    _4602 = (int)*(((s1_ptr)_2)->base + _ix_8446);
    if (IS_ATOM_INT(_4602)) {
        _4603 = _4602 + 1;
        if (_4603 > MAXINT){
            _4603 = NewDouble((double)_4603);
        }
    }
    else
    _4603 = binary_op(PLUS, 1, _4602);
    _4602 = NOVALUE;
    _2 = (int)SEQ_PTR(_hits_8445);
    _2 = (int)(((s1_ptr)_2)->base + _ix_8446);
    _1 = *(int *)_2;
    *(int *)_2 = _4603;
    if( _1 != _4603 ){
        DeRef(_1);
    }
    _4603 = NOVALUE;

    /** 		entry*/
LB: 

    /** 			for i = 1 to length(data) do*/
    if (IS_SEQUENCE(_data_8449)){
            _4604 = SEQ_PTR(_data_8449)->length;
    }
    else {
        _4604 = 1;
    }
    {
        int _i_8504;
        _i_8504 = 1;
LE: 
        if (_i_8504 > _4604){
            goto LF; // [327] 350
        }

        /** 				data[i] = getc(fn)*/
        if (_fn_8443 != last_r_file_no) {
            last_r_file_ptr = which_file(_fn_8443, EF_READ);
            last_r_file_no = _fn_8443;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4605 = getKBchar();
            }
            else
            _4605 = getc(last_r_file_ptr);
        }
        else
        _4605 = getc(last_r_file_ptr);
        _2 = (int)SEQ_PTR(_data_8449);
        _2 = (int)(((s1_ptr)_2)->base + _i_8504);
        *(int *)_2 = _4605;
        if( _1 != _4605 ){
        }
        _4605 = NOVALUE;

        /** 			end for*/
        _i_8504 = _i_8504 + 1;
        goto LE; // [345] 334
LF: 
        ;
    }

    /** 		end while*/
    goto LC; // [352] 261
LD: 

    /** 		nhit = 0*/
    _nhit_8450 = 0;

    /** 		while nmiss with entry do*/
    goto L10; // [362] 451
L11: 
    if (_nmiss_8451 == 0)
    {
        goto L12; // [367] 463
    }
    else{
    }

    /** 			while 1 do*/
L13: 

    /** 				nhit += 1*/
    _nhit_8450 = _nhit_8450 + 1;

    /** 				if nhit > length(hits) then*/
    if (IS_SEQUENCE(_hits_8445)){
            _4607 = SEQ_PTR(_hits_8445)->length;
    }
    else {
        _4607 = 1;
    }
    if (_nhit_8450 <= _4607)
    goto L14; // [386] 396

    /** 					nhit = 1*/
    _nhit_8450 = 1;
L14: 

    /** 				if hits[nhit] != 0 then*/
    _2 = (int)SEQ_PTR(_hits_8445);
    _4609 = (int)*(((s1_ptr)_2)->base + _nhit_8450);
    if (binary_op_a(EQUALS, _4609, 0)){
        _4609 = NOVALUE;
        goto L13; // [402] 375
    }
    _4609 = NOVALUE;

    /** 					exit*/
    goto L15; // [408] 416

    /** 			end while*/
    goto L13; // [413] 375
L15: 

    /** 			cs[nmiss] = hash(cs[nmiss], cs[nhit])*/
    _2 = (int)SEQ_PTR(_cs_8444);
    _4611 = (int)*(((s1_ptr)_2)->base + _nmiss_8451);
    _2 = (int)SEQ_PTR(_cs_8444);
    _4612 = (int)*(((s1_ptr)_2)->base + _nhit_8450);
    _4613 = calc_hash(_4611, _4612);
    _4611 = NOVALUE;
    _4612 = NOVALUE;
    _2 = (int)SEQ_PTR(_cs_8444);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _cs_8444 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _nmiss_8451);
    _1 = *(int *)_2;
    *(int *)_2 = _4613;
    if( _1 != _4613 ){
        DeRef(_1);
    }
    _4613 = NOVALUE;

    /** 			hits[nmiss] += 1*/
    _2 = (int)SEQ_PTR(_hits_8445);
    _4614 = (int)*(((s1_ptr)_2)->base + _nmiss_8451);
    if (IS_ATOM_INT(_4614)) {
        _4615 = _4614 + 1;
        if (_4615 > MAXINT){
            _4615 = NewDouble((double)_4615);
        }
    }
    else
    _4615 = binary_op(PLUS, 1, _4614);
    _4614 = NOVALUE;
    _2 = (int)SEQ_PTR(_hits_8445);
    _2 = (int)(((s1_ptr)_2)->base + _nmiss_8451);
    _1 = *(int *)_2;
    *(int *)_2 = _4615;
    if( _1 != _4615 ){
        DeRef(_1);
    }
    _4615 = NOVALUE;

    /** 		entry*/
L10: 

    /** 			nmiss = find(0, hits)	*/
    _nmiss_8451 = find_from(0, _hits_8445, 1);

    /** 		end while*/
    goto L11; // [460] 365
L12: 
LA: 

    /** 	close(fn)*/
    EClose(_fn_8443);

    /** 	if return_text then*/
    if (_return_text_8442 == 0)
    {
        goto L16; // [470] 541
    }
    else{
    }

    /** 		sequence cs_text = ""*/
    RefDS(_5);
    DeRef(_cs_text_8523);
    _cs_text_8523 = _5;

    /** 		for i = 1 to length(cs) do*/
    if (IS_SEQUENCE(_cs_8444)){
            _4617 = SEQ_PTR(_cs_8444)->length;
    }
    else {
        _4617 = 1;
    }
    {
        int _i_8525;
        _i_8525 = 1;
L17: 
        if (_i_8525 > _4617){
            goto L18; // [485] 530
        }

        /** 			cs_text &= text:format("[:08X]", cs[i])*/
        _2 = (int)SEQ_PTR(_cs_8444);
        _4619 = (int)*(((s1_ptr)_2)->base + _i_8525);
        RefDS(_4618);
        Ref(_4619);
        _4620 = _4format(_4618, _4619);
        _4619 = NOVALUE;
        if (IS_SEQUENCE(_cs_text_8523) && IS_ATOM(_4620)) {
            Ref(_4620);
            Append(&_cs_text_8523, _cs_text_8523, _4620);
        }
        else if (IS_ATOM(_cs_text_8523) && IS_SEQUENCE(_4620)) {
        }
        else {
            Concat((object_ptr)&_cs_text_8523, _cs_text_8523, _4620);
        }
        DeRef(_4620);
        _4620 = NOVALUE;

        /** 			if i != length(cs) then*/
        if (IS_SEQUENCE(_cs_8444)){
                _4622 = SEQ_PTR(_cs_8444)->length;
        }
        else {
            _4622 = 1;
        }
        if (_i_8525 == _4622)
        goto L19; // [512] 523

        /** 				cs_text &= ' '*/
        Append(&_cs_text_8523, _cs_text_8523, 32);
L19: 

        /** 		end for*/
        _i_8525 = _i_8525 + 1;
        goto L17; // [525] 492
L18: 
        ;
    }

    /** 		return cs_text*/
    DeRefDS(_filename_8439);
    DeRef(_cs_8444);
    DeRef(_hits_8445);
    DeRef(_jx_8447);
    DeRef(_fx_8448);
    DeRefi(_data_8449);
    _4594 = NOVALUE;
    return _cs_text_8523;
    DeRefDS(_cs_text_8523);
    _cs_text_8523 = NOVALUE;
    goto L1A; // [538] 548
L16: 

    /** 		return cs*/
    DeRefDS(_filename_8439);
    DeRef(_hits_8445);
    DeRef(_jx_8447);
    DeRef(_fx_8448);
    DeRefi(_data_8449);
    _4594 = NOVALUE;
    return _cs_8444;
L1A: 
    ;
}



// 0x92ADB815
