// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _50regex(int _o_21990)
{
    int _12850 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return sequence(o)*/
    _12850 = IS_SEQUENCE(_o_21990);
    DeRef(_o_21990);
    return _12850;
    ;
}


int _50new(int _pattern_22030, int _options_22031)
{
    int _12870 = NOVALUE;
    int _12869 = NOVALUE;
    int _12867 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(options) then */
    _12867 = 0;
    if (_12867 == 0)
    {
        _12867 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _12867 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    _options_22031 = _18or_all(_options_22031);
L1: 

    /** 	return machine_func(M_PCRE_COMPILE, { pattern, options })*/
    Ref(_options_22031);
    Ref(_pattern_22030);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _pattern_22030;
    ((int *)_2)[2] = _options_22031;
    _12869 = MAKE_SEQ(_1);
    _12870 = machine(68, _12869);
    DeRefDS(_12869);
    _12869 = NOVALUE;
    DeRef(_pattern_22030);
    DeRef(_options_22031);
    return _12870;
    ;
}


int _50get_ovector_size(int _ex_22050, int _maxsize_22051)
{
    int _m_22052 = NOVALUE;
    int _12878 = NOVALUE;
    int _12875 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer m = machine_func(M_PCRE_GET_OVECTOR_SIZE, {ex})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_ex_22050);
    *((int *)(_2+4)) = _ex_22050;
    _12875 = MAKE_SEQ(_1);
    _m_22052 = machine(97, _12875);
    DeRefDS(_12875);
    _12875 = NOVALUE;
    if (!IS_ATOM_INT(_m_22052)) {
        _1 = (long)(DBL_PTR(_m_22052)->dbl);
        if (UNIQUE(DBL_PTR(_m_22052)) && (DBL_PTR(_m_22052)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_m_22052);
        _m_22052 = _1;
    }

    /** 	if (m > maxsize) then*/
    if (_m_22052 <= 30)
    goto L1; // [19] 30

    /** 		return maxsize*/
    DeRef(_ex_22050);
    return 30;
L1: 

    /** 	return m+1*/
    _12878 = _m_22052 + 1;
    if (_12878 > MAXINT){
        _12878 = NewDouble((double)_12878);
    }
    DeRef(_ex_22050);
    return _12878;
    ;
}


int _50find(int _re_22060, int _haystack_22062, int _from_22063, int _options_22064, int _size_22065)
{
    int _12885 = NOVALUE;
    int _12884 = NOVALUE;
    int _12883 = NOVALUE;
    int _12880 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_size_22065)) {
        _1 = (long)(DBL_PTR(_size_22065)->dbl);
        if (UNIQUE(DBL_PTR(_size_22065)) && (DBL_PTR(_size_22065)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_22065);
        _size_22065 = _1;
    }

    /** 	if sequence(options) then */
    _12880 = IS_SEQUENCE(_options_22064);
    if (_12880 == 0)
    {
        _12880 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _12880 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_22064);
    _0 = _options_22064;
    _options_22064 = _18or_all(_options_22064);
    DeRef(_0);
L1: 

    /** 	if size < 0 then*/
    if (_size_22065 >= 0)
    goto L2; // [22] 32

    /** 		size = 0*/
    _size_22065 = 0;
L2: 

    /** 	return machine_func(M_PCRE_EXEC, { re, haystack, length(haystack), options, from, size })*/
    if (IS_SEQUENCE(_haystack_22062)){
            _12883 = SEQ_PTR(_haystack_22062)->length;
    }
    else {
        _12883 = 1;
    }
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_re_22060);
    *((int *)(_2+4)) = _re_22060;
    Ref(_haystack_22062);
    *((int *)(_2+8)) = _haystack_22062;
    *((int *)(_2+12)) = _12883;
    Ref(_options_22064);
    *((int *)(_2+16)) = _options_22064;
    *((int *)(_2+20)) = _from_22063;
    *((int *)(_2+24)) = _size_22065;
    _12884 = MAKE_SEQ(_1);
    _12883 = NOVALUE;
    _12885 = machine(70, _12884);
    DeRefDS(_12884);
    _12884 = NOVALUE;
    DeRef(_re_22060);
    DeRef(_haystack_22062);
    DeRef(_options_22064);
    return _12885;
    ;
}


int _50find_all(int _re_22077, int _haystack_22079, int _from_22080, int _options_22081, int _size_22082)
{
    int _result_22089 = NOVALUE;
    int _results_22090 = NOVALUE;
    int _pHaystack_22091 = NOVALUE;
    int _12898 = NOVALUE;
    int _12897 = NOVALUE;
    int _12895 = NOVALUE;
    int _12893 = NOVALUE;
    int _12891 = NOVALUE;
    int _12887 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_size_22082)) {
        _1 = (long)(DBL_PTR(_size_22082)->dbl);
        if (UNIQUE(DBL_PTR(_size_22082)) && (DBL_PTR(_size_22082)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_22082);
        _size_22082 = _1;
    }

    /** 	if sequence(options) then */
    _12887 = IS_SEQUENCE(_options_22081);
    if (_12887 == 0)
    {
        _12887 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _12887 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_22081);
    _0 = _options_22081;
    _options_22081 = _18or_all(_options_22081);
    DeRef(_0);
L1: 

    /** 	if size < 0 then*/
    if (_size_22082 >= 0)
    goto L2; // [22] 32

    /** 		size = 0*/
    _size_22082 = 0;
L2: 

    /** 	object result*/

    /** 	sequence results = {}*/
    RefDS(_5);
    DeRef(_results_22090);
    _results_22090 = _5;

    /** 	atom pHaystack = machine:allocate_string(haystack)*/
    Ref(_haystack_22079);
    _0 = _pHaystack_22091;
    _pHaystack_22091 = _12allocate_string(_haystack_22079, 0);
    DeRef(_0);

    /** 	while sequence(result) with entry do*/
    goto L3; // [50] 94
L4: 
    _12891 = IS_SEQUENCE(_result_22089);
    if (_12891 == 0)
    {
        _12891 = NOVALUE;
        goto L5; // [58] 119
    }
    else{
        _12891 = NOVALUE;
    }

    /** 		results = append(results, result)*/
    Ref(_result_22089);
    Append(&_results_22090, _results_22090, _result_22089);

    /** 		from = math:max(result) + 1*/
    Ref(_result_22089);
    _12893 = _18max(_result_22089);
    if (IS_ATOM_INT(_12893)) {
        _from_22080 = _12893 + 1;
    }
    else
    { // coercing _from_22080 to an integer 1
        _from_22080 = 1+(long)(DBL_PTR(_12893)->dbl);
        if( !IS_ATOM_INT(_from_22080) ){
            _from_22080 = (object)DBL_PTR(_from_22080)->dbl;
        }
    }
    DeRef(_12893);
    _12893 = NOVALUE;

    /** 		if from > length(haystack) then*/
    if (IS_SEQUENCE(_haystack_22079)){
            _12895 = SEQ_PTR(_haystack_22079)->length;
    }
    else {
        _12895 = 1;
    }
    if (_from_22080 <= _12895)
    goto L6; // [82] 91

    /** 			exit*/
    goto L5; // [88] 119
L6: 

    /** 	entry*/
L3: 

    /** 		result = machine_func(M_PCRE_EXEC, { re, pHaystack, length(haystack), options, from, size })*/
    if (IS_SEQUENCE(_haystack_22079)){
            _12897 = SEQ_PTR(_haystack_22079)->length;
    }
    else {
        _12897 = 1;
    }
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_re_22077);
    *((int *)(_2+4)) = _re_22077;
    Ref(_pHaystack_22091);
    *((int *)(_2+8)) = _pHaystack_22091;
    *((int *)(_2+12)) = _12897;
    Ref(_options_22081);
    *((int *)(_2+16)) = _options_22081;
    *((int *)(_2+20)) = _from_22080;
    *((int *)(_2+24)) = _size_22082;
    _12898 = MAKE_SEQ(_1);
    _12897 = NOVALUE;
    DeRef(_result_22089);
    _result_22089 = machine(70, _12898);
    DeRefDS(_12898);
    _12898 = NOVALUE;

    /** 	end while*/
    goto L4; // [116] 53
L5: 

    /** 	machine:free(pHaystack)*/
    Ref(_pHaystack_22091);
    _12free(_pHaystack_22091);

    /** 	return results*/
    DeRef(_re_22077);
    DeRef(_haystack_22079);
    DeRef(_options_22081);
    DeRef(_result_22089);
    DeRef(_pHaystack_22091);
    return _results_22090;
    ;
}


int _50has_match(int _re_22106, int _haystack_22108, int _from_22109, int _options_22110)
{
    int _12902 = NOVALUE;
    int _12901 = NOVALUE;
    int _12900 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return sequence(find(re, haystack, from, options))*/
    Ref(_re_22106);
    _12900 = _50get_ovector_size(_re_22106, 30);
    Ref(_re_22106);
    Ref(_haystack_22108);
    _12901 = _50find(_re_22106, _haystack_22108, 1, 0, _12900);
    _12900 = NOVALUE;
    _12902 = IS_SEQUENCE(_12901);
    DeRef(_12901);
    _12901 = NOVALUE;
    DeRef(_re_22106);
    DeRef(_haystack_22108);
    return _12902;
    ;
}


int _50matches(int _re_22140, int _haystack_22142, int _from_22143, int _options_22144)
{
    int _str_offsets_22148 = NOVALUE;
    int _match_data_22150 = NOVALUE;
    int _tmp_22160 = NOVALUE;
    int _12939 = NOVALUE;
    int _12938 = NOVALUE;
    int _12937 = NOVALUE;
    int _12936 = NOVALUE;
    int _12935 = NOVALUE;
    int _12933 = NOVALUE;
    int _12932 = NOVALUE;
    int _12931 = NOVALUE;
    int _12930 = NOVALUE;
    int _12928 = NOVALUE;
    int _12927 = NOVALUE;
    int _12926 = NOVALUE;
    int _12925 = NOVALUE;
    int _12923 = NOVALUE;
    int _12922 = NOVALUE;
    int _12921 = NOVALUE;
    int _12918 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(options) then */
    _12918 = 0;
    if (_12918 == 0)
    {
        _12918 = NOVALUE;
        goto L1; // [8] 18
    }
    else{
        _12918 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    _options_22144 = _18or_all(0);
L1: 

    /** 	integer str_offsets = and_bits(STRING_OFFSETS, options)*/
    if (IS_ATOM_INT(_options_22144)) {
        {unsigned long tu;
             tu = (unsigned long)201326592 & (unsigned long)_options_22144;
             _str_offsets_22148 = MAKE_UINT(tu);
        }
    }
    else {
        _str_offsets_22148 = binary_op(AND_BITS, 201326592, _options_22144);
    }
    if (!IS_ATOM_INT(_str_offsets_22148)) {
        _1 = (long)(DBL_PTR(_str_offsets_22148)->dbl);
        if (UNIQUE(DBL_PTR(_str_offsets_22148)) && (DBL_PTR(_str_offsets_22148)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_str_offsets_22148);
        _str_offsets_22148 = _1;
    }

    /** 	object match_data = find(re, haystack, from, and_bits(options, not_bits(STRING_OFFSETS)))*/
    _12921 = not_bits(201326592);
    if (IS_ATOM_INT(_options_22144) && IS_ATOM_INT(_12921)) {
        {unsigned long tu;
             tu = (unsigned long)_options_22144 & (unsigned long)_12921;
             _12922 = MAKE_UINT(tu);
        }
    }
    else {
        _12922 = binary_op(AND_BITS, _options_22144, _12921);
    }
    DeRef(_12921);
    _12921 = NOVALUE;
    Ref(_re_22140);
    _12923 = _50get_ovector_size(_re_22140, 30);
    Ref(_re_22140);
    Ref(_haystack_22142);
    _0 = _match_data_22150;
    _match_data_22150 = _50find(_re_22140, _haystack_22142, _from_22143, _12922, _12923);
    DeRef(_0);
    _12922 = NOVALUE;
    _12923 = NOVALUE;

    /** 	if atom(match_data) then */
    _12925 = IS_ATOM(_match_data_22150);
    if (_12925 == 0)
    {
        _12925 = NOVALUE;
        goto L2; // [53] 63
    }
    else{
        _12925 = NOVALUE;
    }

    /** 		return ERROR_NOMATCH */
    DeRef(_re_22140);
    DeRef(_haystack_22142);
    DeRef(_options_22144);
    DeRef(_match_data_22150);
    return -1;
L2: 

    /** 	for i = 1 to length(match_data) do*/
    if (IS_SEQUENCE(_match_data_22150)){
            _12926 = SEQ_PTR(_match_data_22150)->length;
    }
    else {
        _12926 = 1;
    }
    {
        int _i_22158;
        _i_22158 = 1;
L3: 
        if (_i_22158 > _12926){
            goto L4; // [68] 181
        }

        /** 		sequence tmp*/

        /** 		if match_data[i][1] = 0 then*/
        _2 = (int)SEQ_PTR(_match_data_22150);
        _12927 = (int)*(((s1_ptr)_2)->base + _i_22158);
        _2 = (int)SEQ_PTR(_12927);
        _12928 = (int)*(((s1_ptr)_2)->base + 1);
        _12927 = NOVALUE;
        if (binary_op_a(NOTEQ, _12928, 0)){
            _12928 = NOVALUE;
            goto L5; // [87] 101
        }
        _12928 = NOVALUE;

        /** 			tmp = ""*/
        RefDS(_5);
        DeRef(_tmp_22160);
        _tmp_22160 = _5;
        goto L6; // [98] 125
L5: 

        /** 			tmp = haystack[match_data[i][1]..match_data[i][2]]*/
        _2 = (int)SEQ_PTR(_match_data_22150);
        _12930 = (int)*(((s1_ptr)_2)->base + _i_22158);
        _2 = (int)SEQ_PTR(_12930);
        _12931 = (int)*(((s1_ptr)_2)->base + 1);
        _12930 = NOVALUE;
        _2 = (int)SEQ_PTR(_match_data_22150);
        _12932 = (int)*(((s1_ptr)_2)->base + _i_22158);
        _2 = (int)SEQ_PTR(_12932);
        _12933 = (int)*(((s1_ptr)_2)->base + 2);
        _12932 = NOVALUE;
        rhs_slice_target = (object_ptr)&_tmp_22160;
        RHS_Slice(_haystack_22142, _12931, _12933);
L6: 

        /** 		if str_offsets then*/
        if (_str_offsets_22148 == 0)
        {
            goto L7; // [127] 163
        }
        else{
        }

        /** 			match_data[i] = { tmp, match_data[i][1], match_data[i][2] }*/
        _2 = (int)SEQ_PTR(_match_data_22150);
        _12935 = (int)*(((s1_ptr)_2)->base + _i_22158);
        _2 = (int)SEQ_PTR(_12935);
        _12936 = (int)*(((s1_ptr)_2)->base + 1);
        _12935 = NOVALUE;
        _2 = (int)SEQ_PTR(_match_data_22150);
        _12937 = (int)*(((s1_ptr)_2)->base + _i_22158);
        _2 = (int)SEQ_PTR(_12937);
        _12938 = (int)*(((s1_ptr)_2)->base + 2);
        _12937 = NOVALUE;
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_tmp_22160);
        *((int *)(_2+4)) = _tmp_22160;
        Ref(_12936);
        *((int *)(_2+8)) = _12936;
        Ref(_12938);
        *((int *)(_2+12)) = _12938;
        _12939 = MAKE_SEQ(_1);
        _12938 = NOVALUE;
        _12936 = NOVALUE;
        _2 = (int)SEQ_PTR(_match_data_22150);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _match_data_22150 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_22158);
        _1 = *(int *)_2;
        *(int *)_2 = _12939;
        if( _1 != _12939 ){
            DeRef(_1);
        }
        _12939 = NOVALUE;
        goto L8; // [160] 172
L7: 

        /** 			match_data[i] = tmp*/
        RefDS(_tmp_22160);
        _2 = (int)SEQ_PTR(_match_data_22150);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _match_data_22150 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_22158);
        _1 = *(int *)_2;
        *(int *)_2 = _tmp_22160;
        DeRef(_1);
L8: 
        DeRef(_tmp_22160);
        _tmp_22160 = NOVALUE;

        /** 	end for*/
        _i_22158 = _i_22158 + 1;
        goto L3; // [176] 75
L4: 
        ;
    }

    /** 	return match_data*/
    DeRef(_re_22140);
    DeRef(_haystack_22142);
    DeRef(_options_22144);
    _12931 = NOVALUE;
    _12933 = NOVALUE;
    return _match_data_22150;
    ;
}


int _50split(int _re_22227, int _text_22229, int _from_22230, int _options_22231)
{
    int _12965 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return split_limit(re, text, 0, from, options)*/
    Ref(_re_22227);
    RefDS(_text_22229);
    _12965 = _50split_limit(_re_22227, _text_22229, 0, 1, 0);
    DeRef(_re_22227);
    DeRefDS(_text_22229);
    return _12965;
    ;
}


int _50split_limit(int _re_22236, int _text_22238, int _limit_22239, int _from_22240, int _options_22241)
{
    int _match_data_22245 = NOVALUE;
    int _result_22248 = NOVALUE;
    int _last_22249 = NOVALUE;
    int _a_22260 = NOVALUE;
    int _12991 = NOVALUE;
    int _12990 = NOVALUE;
    int _12989 = NOVALUE;
    int _12987 = NOVALUE;
    int _12985 = NOVALUE;
    int _12984 = NOVALUE;
    int _12983 = NOVALUE;
    int _12982 = NOVALUE;
    int _12981 = NOVALUE;
    int _12978 = NOVALUE;
    int _12977 = NOVALUE;
    int _12976 = NOVALUE;
    int _12973 = NOVALUE;
    int _12972 = NOVALUE;
    int _12970 = NOVALUE;
    int _12968 = NOVALUE;
    int _12966 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(options) then */
    _12966 = 0;
    if (_12966 == 0)
    {
        _12966 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _12966 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    _options_22241 = _18or_all(0);
L1: 

    /** 	sequence match_data = find_all(re, text, from, options), result*/
    Ref(_re_22236);
    _12968 = _50get_ovector_size(_re_22236, 30);
    Ref(_re_22236);
    Ref(_text_22238);
    Ref(_options_22241);
    _0 = _match_data_22245;
    _match_data_22245 = _50find_all(_re_22236, _text_22238, _from_22240, _options_22241, _12968);
    DeRef(_0);
    _12968 = NOVALUE;

    /** 	integer last = 1*/
    _last_22249 = 1;

    /** 	if limit = 0 or limit > length(match_data) then*/
    _12970 = (_limit_22239 == 0);
    if (_12970 != 0) {
        goto L2; // [48] 64
    }
    if (IS_SEQUENCE(_match_data_22245)){
            _12972 = SEQ_PTR(_match_data_22245)->length;
    }
    else {
        _12972 = 1;
    }
    _12973 = (_limit_22239 > _12972);
    _12972 = NOVALUE;
    if (_12973 == 0)
    {
        DeRef(_12973);
        _12973 = NOVALUE;
        goto L3; // [60] 70
    }
    else{
        DeRef(_12973);
        _12973 = NOVALUE;
    }
L2: 

    /** 		limit = length(match_data)*/
    if (IS_SEQUENCE(_match_data_22245)){
            _limit_22239 = SEQ_PTR(_match_data_22245)->length;
    }
    else {
        _limit_22239 = 1;
    }
L3: 

    /** 	result = repeat(0, limit)*/
    DeRef(_result_22248);
    _result_22248 = Repeat(0, _limit_22239);

    /** 	for i = 1 to limit do*/
    _12976 = _limit_22239;
    {
        int _i_22258;
        _i_22258 = 1;
L4: 
        if (_i_22258 > _12976){
            goto L5; // [81] 164
        }

        /** 		integer a*/

        /** 		a = match_data[i][1][1]*/
        _2 = (int)SEQ_PTR(_match_data_22245);
        _12977 = (int)*(((s1_ptr)_2)->base + _i_22258);
        _2 = (int)SEQ_PTR(_12977);
        _12978 = (int)*(((s1_ptr)_2)->base + 1);
        _12977 = NOVALUE;
        _2 = (int)SEQ_PTR(_12978);
        _a_22260 = (int)*(((s1_ptr)_2)->base + 1);
        if (!IS_ATOM_INT(_a_22260)){
            _a_22260 = (long)DBL_PTR(_a_22260)->dbl;
        }
        _12978 = NOVALUE;

        /** 		if a = 0 then*/
        if (_a_22260 != 0)
        goto L6; // [108] 121

        /** 			result[i] = ""*/
        RefDS(_5);
        _2 = (int)SEQ_PTR(_result_22248);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _result_22248 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_22258);
        _1 = *(int *)_2;
        *(int *)_2 = _5;
        DeRef(_1);
        goto L7; // [118] 155
L6: 

        /** 			result[i] = text[last..a - 1]*/
        _12981 = _a_22260 - 1;
        rhs_slice_target = (object_ptr)&_12982;
        RHS_Slice(_text_22238, _last_22249, _12981);
        _2 = (int)SEQ_PTR(_result_22248);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _result_22248 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_22258);
        _1 = *(int *)_2;
        *(int *)_2 = _12982;
        if( _1 != _12982 ){
            DeRef(_1);
        }
        _12982 = NOVALUE;

        /** 			last = match_data[i][1][2] + 1*/
        _2 = (int)SEQ_PTR(_match_data_22245);
        _12983 = (int)*(((s1_ptr)_2)->base + _i_22258);
        _2 = (int)SEQ_PTR(_12983);
        _12984 = (int)*(((s1_ptr)_2)->base + 1);
        _12983 = NOVALUE;
        _2 = (int)SEQ_PTR(_12984);
        _12985 = (int)*(((s1_ptr)_2)->base + 2);
        _12984 = NOVALUE;
        if (IS_ATOM_INT(_12985)) {
            _last_22249 = _12985 + 1;
        }
        else
        { // coercing _last_22249 to an integer 1
            _last_22249 = 1+(long)(DBL_PTR(_12985)->dbl);
            if( !IS_ATOM_INT(_last_22249) ){
                _last_22249 = (object)DBL_PTR(_last_22249)->dbl;
            }
        }
        _12985 = NOVALUE;
L7: 

        /** 	end for*/
        _i_22258 = _i_22258 + 1;
        goto L4; // [159] 88
L5: 
        ;
    }

    /** 	if last < length(text) then*/
    if (IS_SEQUENCE(_text_22238)){
            _12987 = SEQ_PTR(_text_22238)->length;
    }
    else {
        _12987 = 1;
    }
    if (_last_22249 >= _12987)
    goto L8; // [169] 192

    /** 		result &= { text[last..$] }*/
    if (IS_SEQUENCE(_text_22238)){
            _12989 = SEQ_PTR(_text_22238)->length;
    }
    else {
        _12989 = 1;
    }
    rhs_slice_target = (object_ptr)&_12990;
    RHS_Slice(_text_22238, _last_22249, _12989);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _12990;
    _12991 = MAKE_SEQ(_1);
    _12990 = NOVALUE;
    Concat((object_ptr)&_result_22248, _result_22248, _12991);
    DeRefDS(_12991);
    _12991 = NOVALUE;
L8: 

    /** 	return result*/
    DeRef(_re_22236);
    DeRef(_text_22238);
    DeRef(_options_22241);
    DeRef(_match_data_22245);
    DeRef(_12970);
    _12970 = NOVALUE;
    DeRef(_12981);
    _12981 = NOVALUE;
    return _result_22248;
    ;
}


int _50find_replace(int _ex_22282, int _text_22284, int _replacement_22285, int _from_22286, int _options_22287)
{
    int _12993 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return find_replace_limit(ex, text, replacement, -1, from, options)*/
    Ref(_ex_22282);
    RefDS(_text_22284);
    RefDS(_replacement_22285);
    _12993 = _50find_replace_limit(_ex_22282, _text_22284, _replacement_22285, -1, 1, 0);
    DeRef(_ex_22282);
    DeRefDS(_text_22284);
    DeRefDS(_replacement_22285);
    return _12993;
    ;
}


int _50find_replace_limit(int _ex_22292, int _text_22294, int _replacement_22295, int _limit_22296, int _from_22297, int _options_22298)
{
    int _12997 = NOVALUE;
    int _12996 = NOVALUE;
    int _12994 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(options) then */
    _12994 = 0;
    if (_12994 == 0)
    {
        _12994 = NOVALUE;
        goto L1; // [12] 22
    }
    else{
        _12994 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    _options_22298 = _18or_all(0);
L1: 

    /**     return machine_func(M_PCRE_REPLACE, { ex, text, replacement, options, */
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_ex_22292);
    *((int *)(_2+4)) = _ex_22292;
    Ref(_text_22294);
    *((int *)(_2+8)) = _text_22294;
    RefDS(_replacement_22295);
    *((int *)(_2+12)) = _replacement_22295;
    Ref(_options_22298);
    *((int *)(_2+16)) = _options_22298;
    *((int *)(_2+20)) = _from_22297;
    *((int *)(_2+24)) = _limit_22296;
    _12996 = MAKE_SEQ(_1);
    _12997 = machine(71, _12996);
    DeRefDS(_12996);
    _12996 = NOVALUE;
    DeRef(_ex_22292);
    DeRef(_text_22294);
    DeRefDS(_replacement_22295);
    DeRef(_options_22298);
    return _12997;
    ;
}



// 0x3AEA24D5
