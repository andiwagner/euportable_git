// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _68intoptions()
{
    int _pause_msg_64350 = NOVALUE;
    int _opts_array_64360 = NOVALUE;
    int _opts_64365 = NOVALUE;
    int _opt_keys_64374 = NOVALUE;
    int _option_w_64376 = NOVALUE;
    int _key_64380 = NOVALUE;
    int _val_64382 = NOVALUE;
    int _32442 = NOVALUE;
    int _32440 = NOVALUE;
    int _32439 = NOVALUE;
    int _32438 = NOVALUE;
    int _32436 = NOVALUE;
    int _32435 = NOVALUE;
    int _32434 = NOVALUE;
    int _32433 = NOVALUE;
    int _32428 = NOVALUE;
    int _32425 = NOVALUE;
    int _32423 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence pause_msg = GetMsgText(278, 0)*/
    RefDS(_22682);
    _0 = _pause_msg_64350;
    _pause_msg_64350 = _44GetMsgText(278, 0, _22682);
    DeRef(_0);

    /** 	Argv = expand_config_options(Argv)*/
    RefDS(_25Argv_12273);
    _0 = _39expand_config_options(_25Argv_12273);
    DeRefDS(_25Argv_12273);
    _25Argv_12273 = _0;

    /** 	Argc = length(Argv)*/
    if (IS_SEQUENCE(_25Argv_12273)){
            _25Argc_12272 = SEQ_PTR(_25Argv_12273)->length;
    }
    else {
        _25Argc_12272 = 1;
    }

    /** 	sequence opts_array = sort( get_options() )*/
    _32423 = _39get_options();
    _0 = _opts_array_64360;
    _opts_array_64360 = _22sort(_32423, 1);
    DeRef(_0);
    _32423 = NOVALUE;

    /** 	m:map opts = cmd_parse( opts_array, */
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 10;
    *((int *)(_2+8)) = 4;
    *((int *)(_2+12)) = 8;
    RefDS(_pause_msg_64350);
    *((int *)(_2+16)) = _pause_msg_64350;
    _32425 = MAKE_SEQ(_1);
    RefDS(_opts_array_64360);
    RefDS(_25Argv_12273);
    _0 = _opts_64365;
    _opts_64365 = _40cmd_parse(_opts_array_64360, _32425, _25Argv_12273);
    DeRef(_0);
    _32425 = NOVALUE;

    /** 	handle_common_options(opts)*/
    Ref(_opts_64365);
    _39handle_common_options(_opts_64365);

    /** 	sequence opt_keys = map:keys(opts)*/
    Ref(_opts_64365);
    _0 = _opt_keys_64374;
    _opt_keys_64374 = _32keys(_opts_64365, 0);
    DeRef(_0);

    /** 	integer option_w = 0*/
    _option_w_64376 = 0;

    /** 	for idx = 1 to length(opt_keys) do*/
    if (IS_SEQUENCE(_opt_keys_64374)){
            _32428 = SEQ_PTR(_opt_keys_64374)->length;
    }
    else {
        _32428 = 1;
    }
    {
        int _idx_64378;
        _idx_64378 = 1;
L1: 
        if (_idx_64378 > _32428){
            goto L2; // [89] 193
        }

        /** 		sequence key = opt_keys[idx]*/
        DeRef(_key_64380);
        _2 = (int)SEQ_PTR(_opt_keys_64374);
        _key_64380 = (int)*(((s1_ptr)_2)->base + _idx_64378);
        Ref(_key_64380);

        /** 		object val = map:get(opts, key)*/
        Ref(_opts_64365);
        RefDS(_key_64380);
        _0 = _val_64382;
        _val_64382 = _32get(_opts_64365, _key_64380, 0);
        DeRef(_0);

        /** 		switch key do*/
        _1 = find(_key_64380, _32431);
        switch ( _1 ){ 

            /** 			case "coverage" then*/
            case 1:

            /** 				for i = 1 to length( val ) do*/
            if (IS_SEQUENCE(_val_64382)){
                    _32433 = SEQ_PTR(_val_64382)->length;
            }
            else {
                _32433 = 1;
            }
            {
                int _i_64388;
                _i_64388 = 1;
L3: 
                if (_i_64388 > _32433){
                    goto L4; // [128] 151
                }

                /** 					add_coverage( val[i] )*/
                _2 = (int)SEQ_PTR(_val_64382);
                _32434 = (int)*(((s1_ptr)_2)->base + _i_64388);
                Ref(_32434);
                _49add_coverage(_32434);
                _32434 = NOVALUE;

                /** 				end for*/
                _i_64388 = _i_64388 + 1;
                goto L3; // [146] 135
L4: 
                ;
            }
            goto L5; // [151] 184

            /** 			case "coverage-db" then*/
            case 2:

            /** 				coverage_db( val )*/
            Ref(_val_64382);
            _49coverage_db(_val_64382);
            goto L5; // [162] 184

            /** 			case "coverage-erase" then*/
            case 3:

            /** 				new_coverage_db()*/
            _49new_coverage_db();
            goto L5; // [172] 184

            /** 			case "coverage-exclude" then*/
            case 4:

            /** 				coverage_exclude( val )*/
            Ref(_val_64382);
            _49coverage_exclude(_val_64382);
        ;}L5: 
        DeRef(_key_64380);
        _key_64380 = NOVALUE;
        DeRef(_val_64382);
        _val_64382 = NOVALUE;

        /** 	end for*/
        _idx_64378 = _idx_64378 + 1;
        goto L1; // [188] 96
L2: 
        ;
    }

    /** 	if length(m:get(opts, cmdline:EXTRAS)) = 0 then*/
    Ref(_opts_64365);
    RefDS(_40EXTRAS_15909);
    _32435 = _32get(_opts_64365, _40EXTRAS_15909, 0);
    if (IS_SEQUENCE(_32435)){
            _32436 = SEQ_PTR(_32435)->length;
    }
    else {
        _32436 = 1;
    }
    DeRef(_32435);
    _32435 = NOVALUE;
    if (_32436 != 0)
    goto L6; // [206] 254

    /** 		show_banner()*/
    _39show_banner();

    /** 		ShowMsg(2, 249)*/
    RefDS(_22682);
    _44ShowMsg(2, 249, _22682, 1);

    /** 		if not batch_job and not test_only then*/
    _32438 = (_25batch_job_12275 == 0);
    if (_32438 == 0) {
        goto L7; // [229] 249
    }
    _32440 = (_25test_only_12274 == 0);
    if (_32440 == 0)
    {
        DeRef(_32440);
        _32440 = NOVALUE;
        goto L7; // [239] 249
    }
    else{
        DeRef(_32440);
        _32440 = NOVALUE;
    }

    /** 			maybe_any_key(pause_msg)*/
    RefDS(_pause_msg_64350);
    _41maybe_any_key(_pause_msg_64350, 1);
L7: 

    /** 		abort(1)*/
    UserCleanup(1);
L6: 

    /** 	OpDefines &= { "EUI" }*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_32441);
    *((int *)(_2+4)) = _32441;
    _32442 = MAKE_SEQ(_1);
    Concat((object_ptr)&_25OpDefines_12336, _25OpDefines_12336, _32442);
    DeRefDS(_32442);
    _32442 = NOVALUE;

    /** 	finalize_command_line(opts)*/
    Ref(_opts_64365);
    _39finalize_command_line(_opts_64365);

    /** end procedure*/
    DeRef(_pause_msg_64350);
    DeRef(_opts_array_64360);
    DeRef(_opts_64365);
    DeRef(_opt_keys_64374);
    _32435 = NOVALUE;
    DeRef(_32438);
    _32438 = NOVALUE;
    return;
    ;
}



// 0x11CD25E9
