// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _19rand_range(int _lo_4208, int _hi_4209)
{
    int _temp_4212 = NOVALUE;
    int _2112 = NOVALUE;
    int _2110 = NOVALUE;
    int _2107 = NOVALUE;
    int _2106 = NOVALUE;
    int _2105 = NOVALUE;
    int _2104 = NOVALUE;
    int _2102 = NOVALUE;
    int _2101 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if lo > hi then*/
    if (binary_op_a(LESSEQ, _lo_4208, _hi_4209)){
        goto L1; // [3] 23
    }

    /** 		atom temp = hi*/
    Ref(_hi_4209);
    DeRef(_temp_4212);
    _temp_4212 = _hi_4209;

    /** 		hi = lo*/
    Ref(_lo_4208);
    DeRef(_hi_4209);
    _hi_4209 = _lo_4208;

    /** 		lo = temp*/
    Ref(_temp_4212);
    DeRef(_lo_4208);
    _lo_4208 = _temp_4212;
L1: 
    DeRef(_temp_4212);
    _temp_4212 = NOVALUE;

    /** 	if not integer(lo) or not integer(hi) then*/
    if (IS_ATOM_INT(_lo_4208))
    _2101 = 1;
    else if (IS_ATOM_DBL(_lo_4208))
    _2101 = IS_ATOM_INT(DoubleToInt(_lo_4208));
    else
    _2101 = 0;
    _2102 = (_2101 == 0);
    _2101 = NOVALUE;
    if (_2102 != 0) {
        goto L2; // [33] 48
    }
    if (IS_ATOM_INT(_hi_4209))
    _2104 = 1;
    else if (IS_ATOM_DBL(_hi_4209))
    _2104 = IS_ATOM_INT(DoubleToInt(_hi_4209));
    else
    _2104 = 0;
    _2105 = (_2104 == 0);
    _2104 = NOVALUE;
    if (_2105 == 0)
    {
        DeRef(_2105);
        _2105 = NOVALUE;
        goto L3; // [44] 64
    }
    else{
        DeRef(_2105);
        _2105 = NOVALUE;
    }
L2: 

    /**    		hi = rnd() * (hi - lo)*/
    _2106 = _19rnd();
    if (IS_ATOM_INT(_hi_4209) && IS_ATOM_INT(_lo_4208)) {
        _2107 = _hi_4209 - _lo_4208;
        if ((long)((unsigned long)_2107 +(unsigned long) HIGH_BITS) >= 0){
            _2107 = NewDouble((double)_2107);
        }
    }
    else {
        if (IS_ATOM_INT(_hi_4209)) {
            _2107 = NewDouble((double)_hi_4209 - DBL_PTR(_lo_4208)->dbl);
        }
        else {
            if (IS_ATOM_INT(_lo_4208)) {
                _2107 = NewDouble(DBL_PTR(_hi_4209)->dbl - (double)_lo_4208);
            }
            else
            _2107 = NewDouble(DBL_PTR(_hi_4209)->dbl - DBL_PTR(_lo_4208)->dbl);
        }
    }
    DeRef(_hi_4209);
    if (IS_ATOM_INT(_2106) && IS_ATOM_INT(_2107)) {
        if (_2106 == (short)_2106 && _2107 <= INT15 && _2107 >= -INT15)
        _hi_4209 = _2106 * _2107;
        else
        _hi_4209 = NewDouble(_2106 * (double)_2107);
    }
    else {
        _hi_4209 = binary_op(MULTIPLY, _2106, _2107);
    }
    DeRef(_2106);
    _2106 = NOVALUE;
    DeRef(_2107);
    _2107 = NOVALUE;
    goto L4; // [61] 80
L3: 

    /** 		lo -= 1*/
    _0 = _lo_4208;
    if (IS_ATOM_INT(_lo_4208)) {
        _lo_4208 = _lo_4208 - 1;
        if ((long)((unsigned long)_lo_4208 +(unsigned long) HIGH_BITS) >= 0){
            _lo_4208 = NewDouble((double)_lo_4208);
        }
    }
    else {
        _lo_4208 = NewDouble(DBL_PTR(_lo_4208)->dbl - (double)1);
    }
    DeRef(_0);

    /**    		hi = rand(hi - lo)*/
    if (IS_ATOM_INT(_hi_4209) && IS_ATOM_INT(_lo_4208)) {
        _2110 = _hi_4209 - _lo_4208;
        if ((long)((unsigned long)_2110 +(unsigned long) HIGH_BITS) >= 0){
            _2110 = NewDouble((double)_2110);
        }
    }
    else {
        if (IS_ATOM_INT(_hi_4209)) {
            _2110 = NewDouble((double)_hi_4209 - DBL_PTR(_lo_4208)->dbl);
        }
        else {
            if (IS_ATOM_INT(_lo_4208)) {
                _2110 = NewDouble(DBL_PTR(_hi_4209)->dbl - (double)_lo_4208);
            }
            else
            _2110 = NewDouble(DBL_PTR(_hi_4209)->dbl - DBL_PTR(_lo_4208)->dbl);
        }
    }
    DeRef(_hi_4209);
    if (IS_ATOM_INT(_2110)) {
        _hi_4209 = good_rand() % ((unsigned)_2110) + 1;
    }
    else {
        _hi_4209 = unary_op(RAND, _2110);
    }
    DeRef(_2110);
    _2110 = NOVALUE;
L4: 

    /**    	return lo + hi*/
    if (IS_ATOM_INT(_lo_4208) && IS_ATOM_INT(_hi_4209)) {
        _2112 = _lo_4208 + _hi_4209;
        if ((long)((unsigned long)_2112 + (unsigned long)HIGH_BITS) >= 0) 
        _2112 = NewDouble((double)_2112);
    }
    else {
        if (IS_ATOM_INT(_lo_4208)) {
            _2112 = NewDouble((double)_lo_4208 + DBL_PTR(_hi_4209)->dbl);
        }
        else {
            if (IS_ATOM_INT(_hi_4209)) {
                _2112 = NewDouble(DBL_PTR(_lo_4208)->dbl + (double)_hi_4209);
            }
            else
            _2112 = NewDouble(DBL_PTR(_lo_4208)->dbl + DBL_PTR(_hi_4209)->dbl);
        }
    }
    DeRef(_lo_4208);
    DeRef(_hi_4209);
    DeRef(_2102);
    _2102 = NOVALUE;
    return _2112;
    ;
}


int _19rnd()
{
    int _a_4232 = NOVALUE;
    int _b_4233 = NOVALUE;
    int _r_4234 = NOVALUE;
    int _0, _1, _2;
    

    /** 	 a = rand(#FFFFFFFF)*/
    DeRef(_a_4232);
    _a_4232 = unary_op(RAND, _2113);

    /** 	 if a = 1 then return 0 end if*/
    if (binary_op_a(NOTEQ, _a_4232, 1)){
        goto L1; // [8] 17
    }
    DeRef(_a_4232);
    DeRef(_b_4233);
    DeRef(_r_4234);
    return 0;
L1: 

    /** 	 b = rand(#FFFFFFFF)*/
    DeRef(_b_4233);
    _b_4233 = unary_op(RAND, _2113);

    /** 	 if b = 1 then return 0 end if*/
    if (binary_op_a(NOTEQ, _b_4233, 1)){
        goto L2; // [24] 33
    }
    DeRef(_a_4232);
    DeRef(_b_4233);
    DeRef(_r_4234);
    return 0;
L2: 

    /** 	 if a > b then*/
    if (binary_op_a(LESSEQ, _a_4232, _b_4233)){
        goto L3; // [35] 48
    }

    /** 	 	r = b / a*/
    DeRef(_r_4234);
    if (IS_ATOM_INT(_b_4233) && IS_ATOM_INT(_a_4232)) {
        _r_4234 = (_b_4233 % _a_4232) ? NewDouble((double)_b_4233 / _a_4232) : (_b_4233 / _a_4232);
    }
    else {
        if (IS_ATOM_INT(_b_4233)) {
            _r_4234 = NewDouble((double)_b_4233 / DBL_PTR(_a_4232)->dbl);
        }
        else {
            if (IS_ATOM_INT(_a_4232)) {
                _r_4234 = NewDouble(DBL_PTR(_b_4233)->dbl / (double)_a_4232);
            }
            else
            _r_4234 = NewDouble(DBL_PTR(_b_4233)->dbl / DBL_PTR(_a_4232)->dbl);
        }
    }
    goto L4; // [45] 55
L3: 

    /** 	 	r = a / b*/
    DeRef(_r_4234);
    if (IS_ATOM_INT(_a_4232) && IS_ATOM_INT(_b_4233)) {
        _r_4234 = (_a_4232 % _b_4233) ? NewDouble((double)_a_4232 / _b_4233) : (_a_4232 / _b_4233);
    }
    else {
        if (IS_ATOM_INT(_a_4232)) {
            _r_4234 = NewDouble((double)_a_4232 / DBL_PTR(_b_4233)->dbl);
        }
        else {
            if (IS_ATOM_INT(_b_4233)) {
                _r_4234 = NewDouble(DBL_PTR(_a_4232)->dbl / (double)_b_4233);
            }
            else
            _r_4234 = NewDouble(DBL_PTR(_a_4232)->dbl / DBL_PTR(_b_4233)->dbl);
        }
    }
L4: 

    /** 	 return r*/
    DeRef(_a_4232);
    DeRef(_b_4233);
    return _r_4234;
    ;
}


int _19rnd_1()
{
    int _r_4249 = NOVALUE;
    int _0, _1, _2;
    

    /** 	while r >= 1.0 with entry do*/
    goto L1; // [3] 15
L2: 
    if (binary_op_a(LESS, _r_4249, _2121)){
        goto L3; // [8] 25
    }

    /** 	entry*/
L1: 

    /** 		r = rnd()*/
    _0 = _r_4249;
    _r_4249 = _19rnd();
    DeRef(_0);

    /** 	end while	 */
    goto L2; // [22] 6
L3: 

    /** 	return r*/
    return _r_4249;
    ;
}


void _19set_rand(int _seed_4256)
{
    int _0, _1, _2;
    

    /** 	machine_proc(M_SET_RAND, seed)*/
    machine(35, _seed_4256);

    /** end procedure*/
    DeRef(_seed_4256);
    return;
    ;
}


int _19get_rand()
{
    int _2124 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_GET_RAND, {})*/
    _2124 = machine(98, _5);
    return _2124;
    ;
}


int _19chance(int _my_limit_4262, int _top_limit_4263)
{
    int _2127 = NOVALUE;
    int _2126 = NOVALUE;
    int _2125 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return (rnd_1() * top_limit) <= my_limit*/
    _2125 = _19rnd_1();
    if (IS_ATOM_INT(_2125) && IS_ATOM_INT(_top_limit_4263)) {
        if (_2125 == (short)_2125 && _top_limit_4263 <= INT15 && _top_limit_4263 >= -INT15)
        _2126 = _2125 * _top_limit_4263;
        else
        _2126 = NewDouble(_2125 * (double)_top_limit_4263);
    }
    else {
        _2126 = binary_op(MULTIPLY, _2125, _top_limit_4263);
    }
    DeRef(_2125);
    _2125 = NOVALUE;
    if (IS_ATOM_INT(_2126) && IS_ATOM_INT(_my_limit_4262)) {
        _2127 = (_2126 <= _my_limit_4262);
    }
    else {
        _2127 = binary_op(LESSEQ, _2126, _my_limit_4262);
    }
    DeRef(_2126);
    _2126 = NOVALUE;
    DeRef(_my_limit_4262);
    DeRef(_top_limit_4263);
    return _2127;
    ;
}


int _19roll(int _desired_4269, int _sides_4270)
{
    int _rolled_4271 = NOVALUE;
    int _2132 = NOVALUE;
    int _2129 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sides_4270)) {
        _1 = (long)(DBL_PTR(_sides_4270)->dbl);
        if (UNIQUE(DBL_PTR(_sides_4270)) && (DBL_PTR(_sides_4270)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sides_4270);
        _sides_4270 = _1;
    }

    /** 	if sides < 2 then*/
    if (_sides_4270 >= 2)
    goto L1; // [5] 16

    /** 		return 0*/
    DeRef(_desired_4269);
    return 0;
L1: 

    /** 	if atom(desired) then*/
    _2129 = IS_ATOM(_desired_4269);
    if (_2129 == 0)
    {
        _2129 = NOVALUE;
        goto L2; // [21] 31
    }
    else{
        _2129 = NOVALUE;
    }

    /** 		desired = {desired}*/
    _0 = _desired_4269;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_desired_4269);
    *((int *)(_2+4)) = _desired_4269;
    _desired_4269 = MAKE_SEQ(_1);
    DeRef(_0);
L2: 

    /** 	rolled =  rand(sides)*/
    _rolled_4271 = good_rand() % ((unsigned)_sides_4270) + 1;

    /** 	if find(rolled, desired) then*/
    _2132 = find_from(_rolled_4271, _desired_4269, 1);
    if (_2132 == 0)
    {
        _2132 = NOVALUE;
        goto L3; // [43] 55
    }
    else{
        _2132 = NOVALUE;
    }

    /** 		return rolled*/
    DeRef(_desired_4269);
    return _rolled_4271;
    goto L4; // [52] 62
L3: 

    /** 		return 0*/
    DeRef(_desired_4269);
    return 0;
L4: 
    ;
}


int _19sample(int _population_4283, int _sample_size_4284, int _sampling_method_4285)
{
    int _lResult_4286 = NOVALUE;
    int _lIdx_4287 = NOVALUE;
    int _lChoice_4288 = NOVALUE;
    int _2150 = NOVALUE;
    int _2146 = NOVALUE;
    int _2143 = NOVALUE;
    int _2139 = NOVALUE;
    int _2138 = NOVALUE;
    int _2137 = NOVALUE;
    int _2136 = NOVALUE;
    int _2135 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sample_size_4284)) {
        _1 = (long)(DBL_PTR(_sample_size_4284)->dbl);
        if (UNIQUE(DBL_PTR(_sample_size_4284)) && (DBL_PTR(_sample_size_4284)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sample_size_4284);
        _sample_size_4284 = _1;
    }
    if (!IS_ATOM_INT(_sampling_method_4285)) {
        _1 = (long)(DBL_PTR(_sampling_method_4285)->dbl);
        if (UNIQUE(DBL_PTR(_sampling_method_4285)) && (DBL_PTR(_sampling_method_4285)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sampling_method_4285);
        _sampling_method_4285 = _1;
    }

    /** 	if sample_size < 1 then*/
    if (_sample_size_4284 >= 1)
    goto L1; // [9] 40

    /** 		if sampling_method > 0 then*/
    if (_sampling_method_4285 <= 0)
    goto L2; // [15] 32

    /** 			return {{}, population}	*/
    RefDS(_population_4283);
    RefDS(_5);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5;
    ((int *)_2)[2] = _population_4283;
    _2135 = MAKE_SEQ(_1);
    DeRefDS(_population_4283);
    DeRef(_lResult_4286);
    return _2135;
    goto L3; // [29] 39
L2: 

    /** 			return {}*/
    RefDS(_5);
    DeRefDS(_population_4283);
    DeRef(_lResult_4286);
    DeRef(_2135);
    _2135 = NOVALUE;
    return _5;
L3: 
L1: 

    /** 	if sampling_method >= 0 and sample_size >= length(population) then*/
    _2136 = (_sampling_method_4285 >= 0);
    if (_2136 == 0) {
        goto L4; // [46] 67
    }
    if (IS_SEQUENCE(_population_4283)){
            _2138 = SEQ_PTR(_population_4283)->length;
    }
    else {
        _2138 = 1;
    }
    _2139 = (_sample_size_4284 >= _2138);
    _2138 = NOVALUE;
    if (_2139 == 0)
    {
        DeRef(_2139);
        _2139 = NOVALUE;
        goto L4; // [58] 67
    }
    else{
        DeRef(_2139);
        _2139 = NOVALUE;
    }

    /** 		sample_size = length(population)*/
    if (IS_SEQUENCE(_population_4283)){
            _sample_size_4284 = SEQ_PTR(_population_4283)->length;
    }
    else {
        _sample_size_4284 = 1;
    }
L4: 

    /** 	lResult = repeat(0, sample_size)*/
    DeRef(_lResult_4286);
    _lResult_4286 = Repeat(0, _sample_size_4284);

    /** 	lIdx = 0*/
    _lIdx_4287 = 0;

    /** 	while lIdx < sample_size do*/
L5: 
    if (_lIdx_4287 >= _sample_size_4284)
    goto L6; // [83] 130

    /** 		lChoice = rand(length(population))*/
    if (IS_SEQUENCE(_population_4283)){
            _2143 = SEQ_PTR(_population_4283)->length;
    }
    else {
        _2143 = 1;
    }
    _lChoice_4288 = good_rand() % ((unsigned)_2143) + 1;
    _2143 = NOVALUE;

    /** 		lIdx += 1*/
    _lIdx_4287 = _lIdx_4287 + 1;

    /** 		lResult[lIdx] = population[lChoice]*/
    _2 = (int)SEQ_PTR(_population_4283);
    _2146 = (int)*(((s1_ptr)_2)->base + _lChoice_4288);
    Ref(_2146);
    _2 = (int)SEQ_PTR(_lResult_4286);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _lResult_4286 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _lIdx_4287);
    _1 = *(int *)_2;
    *(int *)_2 = _2146;
    if( _1 != _2146 ){
        DeRef(_1);
    }
    _2146 = NOVALUE;

    /** 		if sampling_method >= 0 then*/
    if (_sampling_method_4285 < 0)
    goto L5; // [113] 83

    /** 			population = remove(population, lChoice)*/
    {
        s1_ptr assign_space = SEQ_PTR(_population_4283);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_lChoice_4288)) ? _lChoice_4288 : (long)(DBL_PTR(_lChoice_4288)->dbl);
        int stop = (IS_ATOM_INT(_lChoice_4288)) ? _lChoice_4288 : (long)(DBL_PTR(_lChoice_4288)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_population_4283), start, &_population_4283 );
            }
            else Tail(SEQ_PTR(_population_4283), stop+1, &_population_4283);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_population_4283), start, &_population_4283);
        }
        else {
            assign_slice_seq = &assign_space;
            _population_4283 = Remove_elements(start, stop, (SEQ_PTR(_population_4283)->ref == 1));
        }
    }

    /** 	end while*/
    goto L5; // [127] 83
L6: 

    /** 	if sampling_method > 0 then*/
    if (_sampling_method_4285 <= 0)
    goto L7; // [132] 149

    /** 		return {lResult, population}	*/
    RefDS(_population_4283);
    RefDS(_lResult_4286);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _lResult_4286;
    ((int *)_2)[2] = _population_4283;
    _2150 = MAKE_SEQ(_1);
    DeRefDS(_population_4283);
    DeRefDS(_lResult_4286);
    DeRef(_2135);
    _2135 = NOVALUE;
    DeRef(_2136);
    _2136 = NOVALUE;
    return _2150;
    goto L8; // [146] 156
L7: 

    /** 		return lResult*/
    DeRefDS(_population_4283);
    DeRef(_2135);
    _2135 = NOVALUE;
    DeRef(_2136);
    _2136 = NOVALUE;
    DeRef(_2150);
    _2150 = NOVALUE;
    return _lResult_4286;
L8: 
    ;
}



// 0xEF817CFD
