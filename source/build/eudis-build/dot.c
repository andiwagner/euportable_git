// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _71edges(int _call_map_65030, int _proc_65031, int _files_65032, int _direction_65033)
{
    int _file_65034 = NOVALUE;
    int _proc_map_65039 = NOVALUE;
    int _from_files_65044 = NOVALUE;
    int _lines_65046 = NOVALUE;
    int _file_map_65055 = NOVALUE;
    int _procs_65060 = NOVALUE;
    int _edge_lines_65062 = NOVALUE;
    int _edge_count_65063 = NOVALUE;
    int _called_proc_65067 = NOVALUE;
    int _sym_ent_65071 = NOVALUE;
    int _caller_65074 = NOVALUE;
    int _callee_65075 = NOVALUE;
    int _count_65105 = NOVALUE;
    int _32733 = NOVALUE;
    int _32728 = NOVALUE;
    int _32727 = NOVALUE;
    int _32725 = NOVALUE;
    int _32724 = NOVALUE;
    int _32723 = NOVALUE;
    int _32722 = NOVALUE;
    int _32720 = NOVALUE;
    int _32718 = NOVALUE;
    int _32716 = NOVALUE;
    int _32714 = NOVALUE;
    int _32710 = NOVALUE;
    int _32707 = NOVALUE;
    int _32706 = NOVALUE;
    int _32705 = NOVALUE;
    int _32704 = NOVALUE;
    int _32702 = NOVALUE;
    int _32701 = NOVALUE;
    int _32698 = NOVALUE;
    int _32697 = NOVALUE;
    int _32695 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_proc_65031)) {
        _1 = (long)(DBL_PTR(_proc_65031)->dbl);
        if (UNIQUE(DBL_PTR(_proc_65031)) && (DBL_PTR(_proc_65031)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_proc_65031);
        _proc_65031 = _1;
    }

    /** 	integer file = SymTab[proc][S_FILE_NO]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _32695 = (int)*(((s1_ptr)_2)->base + _proc_65031);
    _2 = (int)SEQ_PTR(_32695);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _file_65034 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _file_65034 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    if (!IS_ATOM_INT(_file_65034)){
        _file_65034 = (long)DBL_PTR(_file_65034)->dbl;
    }
    _32695 = NOVALUE;

    /** 	map:map proc_map = new_extra( map:nested_get( call_map, { file, proc } ) )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _file_65034;
    ((int *)_2)[2] = _proc_65031;
    _32697 = MAKE_SEQ(_1);
    Ref(_call_map_65030);
    _32698 = _32nested_get(_call_map_65030, _32697, 0);
    _32697 = NOVALUE;
    _0 = _proc_map_65039;
    _proc_map_65039 = _32new_extra(_32698, 690);
    DeRef(_0);
    _32698 = NOVALUE;

    /** 	sequence from_files = map:keys( proc_map )*/
    Ref(_proc_map_65039);
    _0 = _from_files_65044;
    _from_files_65044 = _32keys(_proc_map_65039, 0);
    DeRef(_0);

    /** 	sequence lines = {}*/
    RefDS(_22682);
    DeRefi(_lines_65046);
    _lines_65046 = _22682;

    /** 	for i = 1 to length( from_files ) do*/
    if (IS_SEQUENCE(_from_files_65044)){
            _32701 = SEQ_PTR(_from_files_65044)->length;
    }
    else {
        _32701 = 1;
    }
    {
        int _i_65048;
        _i_65048 = 1;
L1: 
        if (_i_65048 > _32701){
            goto L2; // [61] 356
        }

        /** 		if and_bits( INTER_FILE, files ) or file = from_files[i] then*/
        {unsigned long tu;
             tu = (unsigned long)1 & (unsigned long)_files_65032;
             _32702 = MAKE_UINT(tu);
        }
        if (IS_ATOM_INT(_32702)) {
            if (_32702 != 0) {
                goto L3; // [74] 91
            }
        }
        else {
            if (DBL_PTR(_32702)->dbl != 0.0) {
                goto L3; // [74] 91
            }
        }
        _2 = (int)SEQ_PTR(_from_files_65044);
        _32704 = (int)*(((s1_ptr)_2)->base + _i_65048);
        if (IS_ATOM_INT(_32704)) {
            _32705 = (_file_65034 == _32704);
        }
        else {
            _32705 = binary_op(EQUALS, _file_65034, _32704);
        }
        _32704 = NOVALUE;
        if (_32705 == 0) {
            DeRef(_32705);
            _32705 = NOVALUE;
            goto L4; // [87] 347
        }
        else {
            if (!IS_ATOM_INT(_32705) && DBL_PTR(_32705)->dbl == 0.0){
                DeRef(_32705);
                _32705 = NOVALUE;
                goto L4; // [87] 347
            }
            DeRef(_32705);
            _32705 = NOVALUE;
        }
        DeRef(_32705);
        _32705 = NOVALUE;
L3: 

        /** 			map:map file_map = new_extra( map:get( proc_map, from_files[i]) )*/
        _2 = (int)SEQ_PTR(_from_files_65044);
        _32706 = (int)*(((s1_ptr)_2)->base + _i_65048);
        Ref(_proc_map_65039);
        Ref(_32706);
        _32707 = _32get(_proc_map_65039, _32706, 0);
        _32706 = NOVALUE;
        _0 = _file_map_65055;
        _file_map_65055 = _32new_extra(_32707, 690);
        DeRef(_0);
        _32707 = NOVALUE;

        /** 			sequence procs = map:keys( file_map )*/
        Ref(_file_map_65055);
        _0 = _procs_65060;
        _procs_65060 = _32keys(_file_map_65055, 0);
        DeRef(_0);

        /** 			sequence edge_lines = ""*/
        RefDS(_22682);
        DeRefi(_edge_lines_65062);
        _edge_lines_65062 = _22682;

        /** 			integer edge_count = 0*/
        _edge_count_65063 = 0;

        /** 			for j = 1 to length( procs ) do*/
        if (IS_SEQUENCE(_procs_65060)){
                _32710 = SEQ_PTR(_procs_65060)->length;
        }
        else {
            _32710 = 1;
        }
        {
            int _j_65065;
            _j_65065 = 1;
L5: 
            if (_j_65065 > _32710){
                goto L6; // [134] 340
            }

            /** 				integer called_proc = procs[j]*/
            _2 = (int)SEQ_PTR(_procs_65060);
            _called_proc_65067 = (int)*(((s1_ptr)_2)->base + _j_65065);
            if (!IS_ATOM_INT(_called_proc_65067))
            _called_proc_65067 = (long)DBL_PTR(_called_proc_65067)->dbl;

            /** 				if called_proc then*/
            if (_called_proc_65067 == 0)
            {
                goto L7; // [149] 329
            }
            else{
            }

            /** 					symtab_entry sym_ent = SymTab[called_proc]*/
            DeRef(_sym_ent_65071);
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _sym_ent_65071 = (int)*(((s1_ptr)_2)->base + _called_proc_65067);
            Ref(_sym_ent_65071);

            /** 					sequence caller, callee*/

            /** 					if direction = CALL_FROM then*/
            if (_direction_65033 != 2)
            goto L8; // [166] 205

            /** 						caller = SymTab[proc][S_NAME]*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _32714 = (int)*(((s1_ptr)_2)->base + _proc_65031);
            DeRef(_caller_65074);
            _2 = (int)SEQ_PTR(_32714);
            if (!IS_ATOM_INT(_25S_NAME_11913)){
                _caller_65074 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
            }
            else{
                _caller_65074 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
            }
            Ref(_caller_65074);
            _32714 = NOVALUE;

            /** 						callee = SymTab[called_proc][S_NAME]*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _32716 = (int)*(((s1_ptr)_2)->base + _called_proc_65067);
            DeRef(_callee_65075);
            _2 = (int)SEQ_PTR(_32716);
            if (!IS_ATOM_INT(_25S_NAME_11913)){
                _callee_65075 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
            }
            else{
                _callee_65075 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
            }
            Ref(_callee_65075);
            _32716 = NOVALUE;
            goto L9; // [202] 238
L8: 

            /** 						callee = SymTab[proc][S_NAME]*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _32718 = (int)*(((s1_ptr)_2)->base + _proc_65031);
            DeRef(_callee_65075);
            _2 = (int)SEQ_PTR(_32718);
            if (!IS_ATOM_INT(_25S_NAME_11913)){
                _callee_65075 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
            }
            else{
                _callee_65075 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
            }
            Ref(_callee_65075);
            _32718 = NOVALUE;

            /** 						caller = SymTab[called_proc][S_NAME]*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _32720 = (int)*(((s1_ptr)_2)->base + _called_proc_65067);
            DeRef(_caller_65074);
            _2 = (int)SEQ_PTR(_32720);
            if (!IS_ATOM_INT(_25S_NAME_11913)){
                _caller_65074 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
            }
            else{
                _caller_65074 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
            }
            Ref(_caller_65074);
            _32720 = NOVALUE;
L9: 

            /** 					map:nested_put( cluster, {direction, from_files[i], sym_ent[S_TOKEN], sym_ent[S_SCOPE], called_proc}, 0 )*/
            _2 = (int)SEQ_PTR(_from_files_65044);
            _32722 = (int)*(((s1_ptr)_2)->base + _i_65048);
            _2 = (int)SEQ_PTR(_sym_ent_65071);
            if (!IS_ATOM_INT(_25S_TOKEN_11918)){
                _32723 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
            }
            else{
                _32723 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
            }
            _2 = (int)SEQ_PTR(_sym_ent_65071);
            _32724 = (int)*(((s1_ptr)_2)->base + 4);
            _1 = NewS1(5);
            _2 = (int)((s1_ptr)_1)->base;
            *((int *)(_2+4)) = _direction_65033;
            Ref(_32722);
            *((int *)(_2+8)) = _32722;
            Ref(_32723);
            *((int *)(_2+12)) = _32723;
            Ref(_32724);
            *((int *)(_2+16)) = _32724;
            *((int *)(_2+20)) = _called_proc_65067;
            _32725 = MAKE_SEQ(_1);
            _32724 = NOVALUE;
            _32723 = NOVALUE;
            _32722 = NOVALUE;
            Ref(_71cluster_65015);
            _32nested_put(_71cluster_65015, _32725, 0, 1, _32threshold_size_13318);
            _32725 = NOVALUE;

            /** 					edge_lines &= sprintf("\t\"%s\" -> \"%s\"\n", { caller, callee } )*/
            RefDS(_callee_65075);
            RefDS(_caller_65074);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _caller_65074;
            ((int *)_2)[2] = _callee_65075;
            _32727 = MAKE_SEQ(_1);
            _32728 = EPrintf(-9999999, _32726, _32727);
            DeRefDS(_32727);
            _32727 = NOVALUE;
            Concat((object_ptr)&_edge_lines_65062, _edge_lines_65062, _32728);
            DeRefDS(_32728);
            _32728 = NOVALUE;

            /** 					integer count = map:get( file_map, called_proc, 1 )*/
            Ref(_file_map_65055);
            _count_65105 = _32get(_file_map_65055, _called_proc_65067, 1);
            if (!IS_ATOM_INT(_count_65105)) {
                _1 = (long)(DBL_PTR(_count_65105)->dbl);
                if (UNIQUE(DBL_PTR(_count_65105)) && (DBL_PTR(_count_65105)->cleanup != 0))
                RTFatal("Cannot assign value with a destructor to an integer");                DeRefDS(_count_65105);
                _count_65105 = _1;
            }

            /** 					if count > 1 then*/
            if (_count_65105 <= 1)
            goto LA; // [307] 322

            /** 						edge_lines &= sprintf("\t[label=\"%d\"]\n", count )*/
            _32733 = EPrintf(-9999999, _32732, _count_65105);
            Concat((object_ptr)&_edge_lines_65062, _edge_lines_65062, _32733);
            DeRefDS(_32733);
            _32733 = NOVALUE;
LA: 

            /** 					edge_count += 1*/
            _edge_count_65063 = _edge_count_65063 + 1;
L7: 
            DeRef(_sym_ent_65071);
            _sym_ent_65071 = NOVALUE;
            DeRef(_caller_65074);
            _caller_65074 = NOVALUE;
            DeRef(_callee_65075);
            _callee_65075 = NOVALUE;

            /** 			end for*/
            _j_65065 = _j_65065 + 1;
            goto L5; // [335] 141
L6: 
            ;
        }

        /** 			lines &= edge_lines*/
        Concat((object_ptr)&_lines_65046, _lines_65046, _edge_lines_65062);
L4: 
        DeRef(_file_map_65055);
        _file_map_65055 = NOVALUE;
        DeRef(_procs_65060);
        _procs_65060 = NOVALUE;
        DeRefi(_edge_lines_65062);
        _edge_lines_65062 = NOVALUE;

        /** 	end for*/
        _i_65048 = _i_65048 + 1;
        goto L1; // [351] 68
L2: 
        ;
    }

    /** 	return lines*/
    DeRef(_call_map_65030);
    DeRef(_proc_map_65039);
    DeRef(_from_files_65044);
    DeRef(_32702);
    _32702 = NOVALUE;
    return _lines_65046;
    ;
}


int _71clusters()
{
    int _lines_65116 = NOVALUE;
    int _call_type_65117 = NOVALUE;
    int _file_map_65122 = NOVALUE;
    int _files_65127 = NOVALUE;
    int _fn_65132 = NOVALUE;
    int _token_map_65150 = NOVALUE;
    int _tokens_65154 = NOVALUE;
    int _fill_65159 = NOVALUE;
    int _token_65160 = NOVALUE;
    int _scope_map_65168 = NOVALUE;
    int _scopes_65172 = NOVALUE;
    int _scope_65177 = NOVALUE;
    int _shape_65179 = NOVALUE;
    int _proc_map_65190 = NOVALUE;
    int _procs_65194 = NOVALUE;
    int _proc_65204 = NOVALUE;
    int _32788 = NOVALUE;
    int _32787 = NOVALUE;
    int _32786 = NOVALUE;
    int _32785 = NOVALUE;
    int _32782 = NOVALUE;
    int _32780 = NOVALUE;
    int _32779 = NOVALUE;
    int _32775 = NOVALUE;
    int _32768 = NOVALUE;
    int _32765 = NOVALUE;
    int _32760 = NOVALUE;
    int _32757 = NOVALUE;
    int _32755 = NOVALUE;
    int _32754 = NOVALUE;
    int _32753 = NOVALUE;
    int _32750 = NOVALUE;
    int _32749 = NOVALUE;
    int _32748 = NOVALUE;
    int _32747 = NOVALUE;
    int _32745 = NOVALUE;
    int _32743 = NOVALUE;
    int _32740 = NOVALUE;
    int _32739 = NOVALUE;
    int _32738 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence lines = {}*/
    RefDS(_22682);
    DeRefi(_lines_65116);
    _lines_65116 = _22682;

    /** 	sequence call_type = map:keys( cluster )*/
    Ref(_71cluster_65015);
    _0 = _call_type_65117;
    _call_type_65117 = _32keys(_71cluster_65015, 0);
    DeRef(_0);

    /** 	for ct = 1 to length( call_type ) do*/
    if (IS_SEQUENCE(_call_type_65117)){
            _32738 = SEQ_PTR(_call_type_65117)->length;
    }
    else {
        _32738 = 1;
    }
    {
        int _ct_65120;
        _ct_65120 = 1;
L1: 
        if (_ct_65120 > _32738){
            goto L2; // [24] 427
        }

        /** 		map:map file_map = new_extra( map:get( cluster, call_type[ct]) )*/
        _2 = (int)SEQ_PTR(_call_type_65117);
        _32739 = (int)*(((s1_ptr)_2)->base + _ct_65120);
        Ref(_71cluster_65015);
        Ref(_32739);
        _32740 = _32get(_71cluster_65015, _32739, 0);
        _32739 = NOVALUE;
        _0 = _file_map_65122;
        _file_map_65122 = _32new_extra(_32740, 690);
        DeRef(_0);
        _32740 = NOVALUE;

        /** 		sequence files = map:keys( file_map )*/
        Ref(_file_map_65122);
        _0 = _files_65127;
        _files_65127 = _32keys(_file_map_65122, 0);
        DeRef(_0);

        /** 		for f = 1 to length( files ) do*/
        if (IS_SEQUENCE(_files_65127)){
                _32743 = SEQ_PTR(_files_65127)->length;
        }
        else {
            _32743 = 1;
        }
        {
            int _f_65130;
            _f_65130 = 1;
L3: 
            if (_f_65130 > _32743){
                goto L4; // [64] 418
            }

            /** 			integer fn = files[f]*/
            _2 = (int)SEQ_PTR(_files_65127);
            _fn_65132 = (int)*(((s1_ptr)_2)->base + _f_65130);
            if (!IS_ATOM_INT(_fn_65132))
            _fn_65132 = (long)DBL_PTR(_fn_65132)->dbl;

            /** 			if call_type[ct] then*/
            _2 = (int)SEQ_PTR(_call_type_65117);
            _32745 = (int)*(((s1_ptr)_2)->base + _ct_65120);
            if (_32745 == 0) {
                _32745 = NOVALUE;
                goto L5; // [83] 115
            }
            else {
                if (!IS_ATOM_INT(_32745) && DBL_PTR(_32745)->dbl == 0.0){
                    _32745 = NOVALUE;
                    goto L5; // [83] 115
                }
                _32745 = NOVALUE;
            }
            _32745 = NOVALUE;

            /** 				lines &= sprintf("\tsubgraph \"cluster_%d_%d\" {\n\t\tlabel = \"%s\"\n\t\tcolor = blue\n", */
            _2 = (int)SEQ_PTR(_call_type_65117);
            _32747 = (int)*(((s1_ptr)_2)->base + _ct_65120);
            _2 = (int)SEQ_PTR(_26known_files_11139);
            _32748 = (int)*(((s1_ptr)_2)->base + _fn_65132);
            _1 = NewS1(3);
            _2 = (int)((s1_ptr)_1)->base;
            Ref(_32747);
            *((int *)(_2+4)) = _32747;
            *((int *)(_2+8)) = _fn_65132;
            Ref(_32748);
            *((int *)(_2+12)) = _32748;
            _32749 = MAKE_SEQ(_1);
            _32748 = NOVALUE;
            _32747 = NOVALUE;
            _32750 = EPrintf(-9999999, _32746, _32749);
            DeRefDS(_32749);
            _32749 = NOVALUE;
            Concat((object_ptr)&_lines_65116, _lines_65116, _32750);
            DeRefDS(_32750);
            _32750 = NOVALUE;
            goto L6; // [112] 136
L5: 

            /** 				lines &= sprintf("\tsubgraph \"cluster_target\" {\n\t\tlabel = \"%s\"\n\t\tcolor = blue\n", */
            _2 = (int)SEQ_PTR(_26known_files_11139);
            _32753 = (int)*(((s1_ptr)_2)->base + _fn_65132);
            _1 = NewS1(1);
            _2 = (int)((s1_ptr)_1)->base;
            Ref(_32753);
            *((int *)(_2+4)) = _32753;
            _32754 = MAKE_SEQ(_1);
            _32753 = NOVALUE;
            _32755 = EPrintf(-9999999, _32752, _32754);
            DeRefDS(_32754);
            _32754 = NOVALUE;
            Concat((object_ptr)&_lines_65116, _lines_65116, _32755);
            DeRefDS(_32755);
            _32755 = NOVALUE;
L6: 

            /** 			map:map token_map = new_extra( map:get( file_map, fn) )*/
            Ref(_file_map_65122);
            _32757 = _32get(_file_map_65122, _fn_65132, 0);
            _0 = _token_map_65150;
            _token_map_65150 = _32new_extra(_32757, 690);
            DeRef(_0);
            _32757 = NOVALUE;

            /** 			sequence tokens = map:keys( token_map )*/
            Ref(_token_map_65150);
            _0 = _tokens_65154;
            _tokens_65154 = _32keys(_token_map_65150, 0);
            DeRef(_0);

            /** 			for t = 1 to length( tokens ) do*/
            if (IS_SEQUENCE(_tokens_65154)){
                    _32760 = SEQ_PTR(_tokens_65154)->length;
            }
            else {
                _32760 = 1;
            }
            {
                int _t_65157;
                _t_65157 = 1;
L7: 
                if (_t_65157 > _32760){
                    goto L8; // [163] 403
                }

                /** 				sequence fill*/

                /** 				integer token = tokens[t]*/
                _2 = (int)SEQ_PTR(_tokens_65154);
                _token_65160 = (int)*(((s1_ptr)_2)->base + _t_65157);
                if (!IS_ATOM_INT(_token_65160))
                _token_65160 = (long)DBL_PTR(_token_65160)->dbl;

                /** 				if token = PROC then*/
                if (_token_65160 != 27)
                goto L9; // [182] 196

                /** 					fill = ",style=filled,color=lightgrey"*/
                RefDS(_32763);
                DeRefi(_fill_65159);
                _fill_65159 = _32763;
                goto LA; // [193] 204
L9: 

                /** 					fill = ",style=\"\""*/
                RefDS(_32764);
                DeRefi(_fill_65159);
                _fill_65159 = _32764;
LA: 

                /** 				map:map scope_map = new_extra( map:get( token_map, token) )*/
                Ref(_token_map_65150);
                _32765 = _32get(_token_map_65150, _token_65160, 0);
                _0 = _scope_map_65168;
                _scope_map_65168 = _32new_extra(_32765, 690);
                DeRef(_0);
                _32765 = NOVALUE;

                /** 				sequence scopes = map:keys( scope_map )*/
                Ref(_scope_map_65168);
                _0 = _scopes_65172;
                _scopes_65172 = _32keys(_scope_map_65168, 0);
                DeRef(_0);

                /** 				for s = 1 to length( scopes ) do*/
                if (IS_SEQUENCE(_scopes_65172)){
                        _32768 = SEQ_PTR(_scopes_65172)->length;
                }
                else {
                    _32768 = 1;
                }
                {
                    int _s_65175;
                    _s_65175 = 1;
LB: 
                    if (_s_65175 > _32768){
                        goto LC; // [231] 394
                    }

                    /** 					integer scope = scopes[s]*/
                    _2 = (int)SEQ_PTR(_scopes_65172);
                    _scope_65177 = (int)*(((s1_ptr)_2)->base + _s_65175);
                    if (!IS_ATOM_INT(_scope_65177))
                    _scope_65177 = (long)DBL_PTR(_scope_65177)->dbl;

                    /** 					sequence shape*/

                    /** 					if scope = SC_LOCAL then*/
                    if (_scope_65177 != 5)
                    goto LD; // [250] 264

                    /** 						shape = "shape=ellipse"*/
                    RefDS(_32771);
                    DeRefi(_shape_65179);
                    _shape_65179 = _32771;
                    goto LE; // [261] 290
LD: 

                    /** 					elsif scope = SC_EXPORT then*/
                    if (_scope_65177 != 11)
                    goto LF; // [268] 282

                    /** 						shape = "shape=diamond"*/
                    RefDS(_32773);
                    DeRefi(_shape_65179);
                    _shape_65179 = _32773;
                    goto LE; // [279] 290
LF: 

                    /** 						shape = "shape=box"*/
                    RefDS(_32774);
                    DeRefi(_shape_65179);
                    _shape_65179 = _32774;
LE: 

                    /** 					map:map proc_map = new_extra( map:get( scope_map, scope) )*/
                    Ref(_scope_map_65168);
                    _32775 = _32get(_scope_map_65168, _scope_65177, 0);
                    _0 = _proc_map_65190;
                    _proc_map_65190 = _32new_extra(_32775, 690);
                    DeRef(_0);
                    _32775 = NOVALUE;

                    /** 					sequence procs = map:keys( proc_map )*/
                    Ref(_proc_map_65190);
                    _0 = _procs_65194;
                    _procs_65194 = _32keys(_proc_map_65190, 0);
                    DeRef(_0);

                    /** 					lines &= sprintf( "\t\t\tnode [%s%s]\n", {shape, fill})*/
                    RefDS(_fill_65159);
                    RefDS(_shape_65179);
                    _1 = NewS1(2);
                    _2 = (int)((s1_ptr)_1)->base;
                    ((int *)_2)[1] = _shape_65179;
                    ((int *)_2)[2] = _fill_65159;
                    _32779 = MAKE_SEQ(_1);
                    _32780 = EPrintf(-9999999, _32778, _32779);
                    DeRefDS(_32779);
                    _32779 = NOVALUE;
                    Concat((object_ptr)&_lines_65116, _lines_65116, _32780);
                    DeRefDS(_32780);
                    _32780 = NOVALUE;

                    /** 					for p = 1 to length( procs ) do*/
                    if (IS_SEQUENCE(_procs_65194)){
                            _32782 = SEQ_PTR(_procs_65194)->length;
                    }
                    else {
                        _32782 = 1;
                    }
                    {
                        int _p_65201;
                        _p_65201 = 1;
L10: 
                        if (_p_65201 > _32782){
                            goto L11; // [335] 385
                        }

                        /** 						symtab_index proc = procs[p]*/
                        _2 = (int)SEQ_PTR(_procs_65194);
                        _proc_65204 = (int)*(((s1_ptr)_2)->base + _p_65201);
                        if (!IS_ATOM_INT(_proc_65204)){
                            _proc_65204 = (long)DBL_PTR(_proc_65204)->dbl;
                        }

                        /** 						lines &= sprintf( "\t\t\t\"%s\"\n", {SymTab[proc][S_NAME]} )*/
                        _2 = (int)SEQ_PTR(_26SymTab_11138);
                        _32785 = (int)*(((s1_ptr)_2)->base + _proc_65204);
                        _2 = (int)SEQ_PTR(_32785);
                        if (!IS_ATOM_INT(_25S_NAME_11913)){
                            _32786 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
                        }
                        else{
                            _32786 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
                        }
                        _32785 = NOVALUE;
                        _1 = NewS1(1);
                        _2 = (int)((s1_ptr)_1)->base;
                        Ref(_32786);
                        *((int *)(_2+4)) = _32786;
                        _32787 = MAKE_SEQ(_1);
                        _32786 = NOVALUE;
                        _32788 = EPrintf(-9999999, _32784, _32787);
                        DeRefDS(_32787);
                        _32787 = NOVALUE;
                        Concat((object_ptr)&_lines_65116, _lines_65116, _32788);
                        DeRefDS(_32788);
                        _32788 = NOVALUE;

                        /** 					end for -- procs*/
                        _p_65201 = _p_65201 + 1;
                        goto L10; // [380] 342
L11: 
                        ;
                    }
                    DeRefi(_shape_65179);
                    _shape_65179 = NOVALUE;
                    DeRef(_proc_map_65190);
                    _proc_map_65190 = NOVALUE;
                    DeRef(_procs_65194);
                    _procs_65194 = NOVALUE;

                    /** 				end for -- scopes*/
                    _s_65175 = _s_65175 + 1;
                    goto LB; // [389] 238
LC: 
                    ;
                }
                DeRefi(_fill_65159);
                _fill_65159 = NOVALUE;
                DeRef(_scope_map_65168);
                _scope_map_65168 = NOVALUE;
                DeRef(_scopes_65172);
                _scopes_65172 = NOVALUE;

                /** 			end for  -- tokens*/
                _t_65157 = _t_65157 + 1;
                goto L7; // [398] 170
L8: 
                ;
            }

            /** 			lines &= "\t}\n" -- end of the subgraph*/
            Concat((object_ptr)&_lines_65116, _lines_65116, _32790);
            DeRef(_token_map_65150);
            _token_map_65150 = NOVALUE;
            DeRef(_tokens_65154);
            _tokens_65154 = NOVALUE;

            /** 		end for -- files*/
            _f_65130 = _f_65130 + 1;
            goto L3; // [413] 71
L4: 
            ;
        }
        DeRef(_file_map_65122);
        _file_map_65122 = NOVALUE;
        DeRef(_files_65127);
        _files_65127 = NOVALUE;

        /** 	end for -- call types*/
        _ct_65120 = _ct_65120 + 1;
        goto L1; // [422] 31
L2: 
        ;
    }

    /** 	return  lines*/
    DeRef(_call_type_65117);
    return _lines_65116;
    ;
}


int _71diagram_routine(int _proc_65218, int _files_65219, int _calls_65220)
{
    int _same_file_65221 = NOVALUE;
    int _other_file_65223 = NOVALUE;
    int _lines_65231 = NOVALUE;
    int _fn_65232 = NOVALUE;
    int _32817 = NOVALUE;
    int _32815 = NOVALUE;
    int _32814 = NOVALUE;
    int _32813 = NOVALUE;
    int _32812 = NOVALUE;
    int _32811 = NOVALUE;
    int _32808 = NOVALUE;
    int _32807 = NOVALUE;
    int _32805 = NOVALUE;
    int _32804 = NOVALUE;
    int _32803 = NOVALUE;
    int _32802 = NOVALUE;
    int _32801 = NOVALUE;
    int _32800 = NOVALUE;
    int _32799 = NOVALUE;
    int _32797 = NOVALUE;
    int _32794 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_files_65219)) {
        _1 = (long)(DBL_PTR(_files_65219)->dbl);
        if (UNIQUE(DBL_PTR(_files_65219)) && (DBL_PTR(_files_65219)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_files_65219);
        _files_65219 = _1;
    }
    if (!IS_ATOM_INT(_calls_65220)) {
        _1 = (long)(DBL_PTR(_calls_65220)->dbl);
        if (UNIQUE(DBL_PTR(_calls_65220)) && (DBL_PTR(_calls_65220)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_calls_65220);
        _calls_65220 = _1;
    }

    /** 	integer same_file  = and_bits( INTRA_FILE, files )*/
    {unsigned long tu;
         tu = (unsigned long)2 & (unsigned long)_files_65219;
         _same_file_65221 = MAKE_UINT(tu);
    }

    /** 	integer other_file = and_bits( INTER_FILE, files )*/
    {unsigned long tu;
         tu = (unsigned long)1 & (unsigned long)_files_65219;
         _other_file_65223 = MAKE_UINT(tu);
    }

    /** 	new_diagram()*/

    /** 	cluster = map:new()*/
    _0 = _32new(690);
    DeRef(_71cluster_65015);
    _71cluster_65015 = _0;

    /** end procedure*/
    goto L1; // [27] 30
L1: 

    /** 	if sequence( proc ) then*/
    _32794 = IS_SEQUENCE(_proc_65218);
    if (_32794 == 0)
    {
        _32794 = NOVALUE;
        goto L2; // [35] 49
    }
    else{
        _32794 = NOVALUE;
    }

    /** 		proc = map:get( proc_names, proc, 0 )*/
    Ref(_71proc_names_65013);
    Ref(_proc_65218);
    _0 = _proc_65218;
    _proc_65218 = _32get(_71proc_names_65013, _proc_65218, 0);
    DeRef(_0);
L2: 

    /** 	if not proc then*/
    if (IS_ATOM_INT(_proc_65218)) {
        if (_proc_65218 != 0){
            goto L3; // [51] 61
        }
    }
    else {
        if (DBL_PTR(_proc_65218)->dbl != 0.0){
            goto L3; // [51] 61
        }
    }

    /** 		return ""*/
    RefDS(_22682);
    DeRef(_proc_65218);
    DeRef(_lines_65231);
    return _22682;
L3: 

    /** 	sequence lines = {}*/
    RefDS(_22682);
    DeRef(_lines_65231);
    _lines_65231 = _22682;

    /** 	lines = ""*/
    RefDS(_22682);
    DeRefDS(_lines_65231);
    _lines_65231 = _22682;

    /** 	integer fn = SymTab[proc][S_FILE_NO]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_proc_65218)){
        _32797 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_proc_65218)->dbl));
    }
    else{
        _32797 = (int)*(((s1_ptr)_2)->base + _proc_65218);
    }
    _2 = (int)SEQ_PTR(_32797);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _fn_65232 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _fn_65232 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    if (!IS_ATOM_INT(_fn_65232)){
        _fn_65232 = (long)DBL_PTR(_fn_65232)->dbl;
    }
    _32797 = NOVALUE;

    /** 	map:nested_put( cluster, {0, fn, SymTab[proc][S_TOKEN], SymTab[proc][S_SCOPE], proc}, 0 )*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_proc_65218)){
        _32799 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_proc_65218)->dbl));
    }
    else{
        _32799 = (int)*(((s1_ptr)_2)->base + _proc_65218);
    }
    _2 = (int)SEQ_PTR(_32799);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _32800 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _32800 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _32799 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_proc_65218)){
        _32801 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_proc_65218)->dbl));
    }
    else{
        _32801 = (int)*(((s1_ptr)_2)->base + _proc_65218);
    }
    _2 = (int)SEQ_PTR(_32801);
    _32802 = (int)*(((s1_ptr)_2)->base + 4);
    _32801 = NOVALUE;
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = _fn_65232;
    Ref(_32800);
    *((int *)(_2+12)) = _32800;
    Ref(_32802);
    *((int *)(_2+16)) = _32802;
    Ref(_proc_65218);
    *((int *)(_2+20)) = _proc_65218;
    _32803 = MAKE_SEQ(_1);
    _32802 = NOVALUE;
    _32800 = NOVALUE;
    Ref(_71cluster_65015);
    _32nested_put(_71cluster_65015, _32803, 0, 1, _32threshold_size_13318);
    _32803 = NOVALUE;

    /** 	if and_bits( calls, CALL_BY ) then*/
    {unsigned long tu;
         tu = (unsigned long)_calls_65220 & (unsigned long)1;
         _32804 = MAKE_UINT(tu);
    }
    if (_32804 == 0) {
        DeRef(_32804);
        _32804 = NOVALUE;
        goto L4; // [144] 163
    }
    else {
        if (!IS_ATOM_INT(_32804) && DBL_PTR(_32804)->dbl == 0.0){
            DeRef(_32804);
            _32804 = NOVALUE;
            goto L4; // [144] 163
        }
        DeRef(_32804);
        _32804 = NOVALUE;
    }
    DeRef(_32804);
    _32804 = NOVALUE;

    /** 		lines &= edges( called_by, proc, files, CALL_BY )*/
    Ref(_71called_by_65011);
    Ref(_proc_65218);
    _32805 = _71edges(_71called_by_65011, _proc_65218, _files_65219, 1);
    if (IS_SEQUENCE(_lines_65231) && IS_ATOM(_32805)) {
        Ref(_32805);
        Append(&_lines_65231, _lines_65231, _32805);
    }
    else if (IS_ATOM(_lines_65231) && IS_SEQUENCE(_32805)) {
    }
    else {
        Concat((object_ptr)&_lines_65231, _lines_65231, _32805);
    }
    DeRef(_32805);
    _32805 = NOVALUE;
L4: 

    /** 	if and_bits( calls, CALL_FROM ) then*/
    {unsigned long tu;
         tu = (unsigned long)_calls_65220 & (unsigned long)2;
         _32807 = MAKE_UINT(tu);
    }
    if (_32807 == 0) {
        DeRef(_32807);
        _32807 = NOVALUE;
        goto L5; // [169] 188
    }
    else {
        if (!IS_ATOM_INT(_32807) && DBL_PTR(_32807)->dbl == 0.0){
            DeRef(_32807);
            _32807 = NOVALUE;
            goto L5; // [169] 188
        }
        DeRef(_32807);
        _32807 = NOVALUE;
    }
    DeRef(_32807);
    _32807 = NOVALUE;

    /** 		lines &= edges( called_from, proc, files, CALL_FROM )*/
    Ref(_71called_from_65009);
    Ref(_proc_65218);
    _32808 = _71edges(_71called_from_65009, _proc_65218, _files_65219, 2);
    if (IS_SEQUENCE(_lines_65231) && IS_ATOM(_32808)) {
        Ref(_32808);
        Append(&_lines_65231, _lines_65231, _32808);
    }
    else if (IS_ATOM(_lines_65231) && IS_SEQUENCE(_32808)) {
    }
    else {
        Concat((object_ptr)&_lines_65231, _lines_65231, _32808);
    }
    DeRef(_32808);
    _32808 = NOVALUE;
L5: 

    /** 	return sprintf("digraph %s { rankdir=BT ratio=auto \n", {SymTab[proc][S_NAME]}) & clusters() & lines & "}\n"*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_proc_65218)){
        _32811 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_proc_65218)->dbl));
    }
    else{
        _32811 = (int)*(((s1_ptr)_2)->base + _proc_65218);
    }
    _2 = (int)SEQ_PTR(_32811);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _32812 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _32812 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _32811 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_32812);
    *((int *)(_2+4)) = _32812;
    _32813 = MAKE_SEQ(_1);
    _32812 = NOVALUE;
    _32814 = EPrintf(-9999999, _32810, _32813);
    DeRefDS(_32813);
    _32813 = NOVALUE;
    _32815 = _71clusters();
    {
        int concat_list[4];

        concat_list[0] = _32816;
        concat_list[1] = _lines_65231;
        concat_list[2] = _32815;
        concat_list[3] = _32814;
        Concat_N((object_ptr)&_32817, concat_list, 4);
    }
    DeRef(_32815);
    _32815 = NOVALUE;
    DeRefDS(_32814);
    _32814 = NOVALUE;
    DeRef(_proc_65218);
    DeRefDS(_lines_65231);
    return _32817;
    ;
}


void _71short_files()
{
    int _name_65270 = NOVALUE;
    int _32834 = NOVALUE;
    int _32833 = NOVALUE;
    int _32832 = NOVALUE;
    int _32831 = NOVALUE;
    int _32829 = NOVALUE;
    int _32827 = NOVALUE;
    int _32825 = NOVALUE;
    int _32824 = NOVALUE;
    int _32822 = NOVALUE;
    int _32821 = NOVALUE;
    int _32818 = NOVALUE;
    int _0, _1, _2;
    

    /** 	short_names = known_files*/
    RefDS(_26known_files_11139);
    DeRef(_71short_names_65016);
    _71short_names_65016 = _26known_files_11139;

    /** 	for f = 1 to length( short_names ) do*/
    if (IS_SEQUENCE(_71short_names_65016)){
            _32818 = SEQ_PTR(_71short_names_65016)->length;
    }
    else {
        _32818 = 1;
    }
    {
        int _f_65268;
        _f_65268 = 1;
L1: 
        if (_f_65268 > _32818){
            goto L2; // [17] 118
        }

        /** 		sequence name = short_names[f]*/
        DeRef(_name_65270);
        _2 = (int)SEQ_PTR(_71short_names_65016);
        _name_65270 = (int)*(((s1_ptr)_2)->base + _f_65268);
        Ref(_name_65270);

        /** 		name = match_replace( '\\', name, '/' )*/
        RefDS(_name_65270);
        _0 = _name_65270;
        _name_65270 = _7match_replace(92, _name_65270, 47, 0);
        DeRefDS(_0);

        /** 		known_files[f] = name*/
        RefDS(_name_65270);
        _2 = (int)SEQ_PTR(_26known_files_11139);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26known_files_11139 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _f_65268);
        _1 = *(int *)_2;
        *(int *)_2 = _name_65270;
        DeRef(_1);

        /** 		for r = length( name ) to 1 by -1 do*/
        if (IS_SEQUENCE(_name_65270)){
                _32821 = SEQ_PTR(_name_65270)->length;
        }
        else {
            _32821 = 1;
        }
        {
            int _r_65276;
            _r_65276 = _32821;
L3: 
            if (_r_65276 < 1){
                goto L4; // [58] 101
            }

            /** 			if name[r] = '/' then*/
            _2 = (int)SEQ_PTR(_name_65270);
            _32822 = (int)*(((s1_ptr)_2)->base + _r_65276);
            if (binary_op_a(NOTEQ, _32822, 47)){
                _32822 = NOVALUE;
                goto L5; // [71] 94
            }
            _32822 = NOVALUE;

            /** 				name = name[r+1..$]*/
            _32824 = _r_65276 + 1;
            if (IS_SEQUENCE(_name_65270)){
                    _32825 = SEQ_PTR(_name_65270)->length;
            }
            else {
                _32825 = 1;
            }
            rhs_slice_target = (object_ptr)&_name_65270;
            RHS_Slice(_name_65270, _32824, _32825);

            /** 				exit*/
            goto L4; // [91] 101
L5: 

            /** 		end for*/
            _r_65276 = _r_65276 + -1;
            goto L3; // [96] 65
L4: 
            ;
        }

        /** 		short_names[f] = name*/
        RefDS(_name_65270);
        _2 = (int)SEQ_PTR(_71short_names_65016);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _71short_names_65016 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _f_65268);
        _1 = *(int *)_2;
        *(int *)_2 = _name_65270;
        DeRef(_1);
        DeRefDS(_name_65270);
        _name_65270 = NOVALUE;

        /** 	end for*/
        _f_65268 = _f_65268 + 1;
        goto L1; // [113] 24
L2: 
        ;
    }

    /** 	std_libs = repeat( 0, length( short_names ) )*/
    if (IS_SEQUENCE(_71short_names_65016)){
            _32827 = SEQ_PTR(_71short_names_65016)->length;
    }
    else {
        _32827 = 1;
    }
    DeRefi(_71std_libs_65017);
    _71std_libs_65017 = Repeat(0, _32827);
    _32827 = NOVALUE;

    /** 	for i = 1 to length( known_files ) do*/
    if (IS_SEQUENCE(_26known_files_11139)){
            _32829 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _32829 = 1;
    }
    {
        int _i_65287;
        _i_65287 = 1;
L6: 
        if (_i_65287 > _32829){
            goto L7; // [136] 201
        }

        /** 		if match( "std/", known_files[i]) then*/
        _2 = (int)SEQ_PTR(_26known_files_11139);
        _32831 = (int)*(((s1_ptr)_2)->base + _i_65287);
        _32832 = e_match_from(_32830, _32831, 1);
        _32831 = NOVALUE;
        if (_32832 == 0)
        {
            _32832 = NOVALUE;
            goto L8; // [156] 194
        }
        else{
            _32832 = NOVALUE;
        }

        /** 			short_names[i] = "std/" & short_names[i]*/
        _2 = (int)SEQ_PTR(_71short_names_65016);
        _32833 = (int)*(((s1_ptr)_2)->base + _i_65287);
        if (IS_SEQUENCE(_32830) && IS_ATOM(_32833)) {
            Ref(_32833);
            Append(&_32834, _32830, _32833);
        }
        else if (IS_ATOM(_32830) && IS_SEQUENCE(_32833)) {
        }
        else {
            Concat((object_ptr)&_32834, _32830, _32833);
        }
        _32833 = NOVALUE;
        _2 = (int)SEQ_PTR(_71short_names_65016);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _71short_names_65016 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_65287);
        _1 = *(int *)_2;
        *(int *)_2 = _32834;
        if( _1 != _32834 ){
            DeRef(_1);
        }
        _32834 = NOVALUE;

        /** 			if not show_stdlib then*/
        if (_71show_stdlib_65018 != 0)
        goto L9; // [181] 193

        /** 				std_libs[i] = 1*/
        _2 = (int)SEQ_PTR(_71std_libs_65017);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _71std_libs_65017 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_65287);
        *(int *)_2 = 1;
L9: 
L8: 

        /** 	end for*/
        _i_65287 = _i_65287 + 1;
        goto L6; // [196] 143
L7: 
        ;
    }

    /** end procedure*/
    DeRef(_32824);
    _32824 = NOVALUE;
    return;
    ;
}


int _71diagram_includes(int _show_all_65301, int _stdlib_65302)
{
    int _lines_65303 = NOVALUE;
    int _included_65321 = NOVALUE;
    int _file_65331 = NOVALUE;
    int _32868 = NOVALUE;
    int _32865 = NOVALUE;
    int _32864 = NOVALUE;
    int _32863 = NOVALUE;
    int _32862 = NOVALUE;
    int _32861 = NOVALUE;
    int _32860 = NOVALUE;
    int _32859 = NOVALUE;
    int _32857 = NOVALUE;
    int _32856 = NOVALUE;
    int _32853 = NOVALUE;
    int _32852 = NOVALUE;
    int _32851 = NOVALUE;
    int _32850 = NOVALUE;
    int _32849 = NOVALUE;
    int _32846 = NOVALUE;
    int _32845 = NOVALUE;
    int _32844 = NOVALUE;
    int _32841 = NOVALUE;
    int _32840 = NOVALUE;
    int _32839 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_show_all_65301)) {
        _1 = (long)(DBL_PTR(_show_all_65301)->dbl);
        if (UNIQUE(DBL_PTR(_show_all_65301)) && (DBL_PTR(_show_all_65301)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_show_all_65301);
        _show_all_65301 = _1;
    }
    if (!IS_ATOM_INT(_stdlib_65302)) {
        _1 = (long)(DBL_PTR(_stdlib_65302)->dbl);
        if (UNIQUE(DBL_PTR(_stdlib_65302)) && (DBL_PTR(_stdlib_65302)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_stdlib_65302);
        _stdlib_65302 = _1;
    }

    /** 	sequence lines = "digraph include { rankdir=TB ranksep=1.5 style=filled color=lightgrey node [shape=box] \n"*/
    RefDS(_32836);
    DeRefi(_lines_65303);
    _lines_65303 = _32836;

    /** 	lines &= "\tsubgraph \"cluster_stdlib\"{ rank=max\n"*/
    Concat((object_ptr)&_lines_65303, _lines_65303, _32837);

    /** 	for i = 1 to length(short_names) do*/
    if (IS_SEQUENCE(_71short_names_65016)){
            _32839 = SEQ_PTR(_71short_names_65016)->length;
    }
    else {
        _32839 = 1;
    }
    {
        int _i_65308;
        _i_65308 = 1;
L1: 
        if (_i_65308 > _32839){
            goto L2; // [25] 83
        }

        /** 		if match( "std/", short_names[i] ) = 1 then*/
        _2 = (int)SEQ_PTR(_71short_names_65016);
        _32840 = (int)*(((s1_ptr)_2)->base + _i_65308);
        _32841 = e_match_from(_32830, _32840, 1);
        _32840 = NOVALUE;
        if (_32841 != 1)
        goto L3; // [45] 76

        /** 			if stdlib then*/
        if (_stdlib_65302 == 0)
        {
            goto L4; // [51] 75
        }
        else{
        }

        /** 				lines &= sprintf( "\"%s\"\n", {short_names[i]})*/
        _2 = (int)SEQ_PTR(_71short_names_65016);
        _32844 = (int)*(((s1_ptr)_2)->base + _i_65308);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_32844);
        *((int *)(_2+4)) = _32844;
        _32845 = MAKE_SEQ(_1);
        _32844 = NOVALUE;
        _32846 = EPrintf(-9999999, _32843, _32845);
        DeRefDS(_32845);
        _32845 = NOVALUE;
        Concat((object_ptr)&_lines_65303, _lines_65303, _32846);
        DeRefDS(_32846);
        _32846 = NOVALUE;
L4: 
L3: 

        /** 	end for*/
        _i_65308 = _i_65308 + 1;
        goto L1; // [78] 32
L2: 
        ;
    }

    /** 	lines &= "\t}\n"*/
    Concat((object_ptr)&_lines_65303, _lines_65303, _32790);

    /** 	sequence included = {}*/
    RefDS(_22682);
    DeRefi(_included_65321);
    _included_65321 = _22682;

    /** 	for fi = 1 to length( file_include ) do*/
    if (IS_SEQUENCE(_26file_include_11143)){
            _32849 = SEQ_PTR(_26file_include_11143)->length;
    }
    else {
        _32849 = 1;
    }
    {
        int _fi_65323;
        _fi_65323 = 1;
L5: 
        if (_fi_65323 > _32849){
            goto L6; // [103] 241
        }

        /** 		for i = 1 to length( file_include[fi] ) do*/
        _2 = (int)SEQ_PTR(_26file_include_11143);
        _32850 = (int)*(((s1_ptr)_2)->base + _fi_65323);
        if (IS_SEQUENCE(_32850)){
                _32851 = SEQ_PTR(_32850)->length;
        }
        else {
            _32851 = 1;
        }
        _32850 = NOVALUE;
        {
            int _i_65327;
            _i_65327 = 1;
L7: 
            if (_i_65327 > _32851){
                goto L8; // [121] 234
            }

            /** 			integer file = abs( file_include[fi][i])*/
            _2 = (int)SEQ_PTR(_26file_include_11143);
            _32852 = (int)*(((s1_ptr)_2)->base + _fi_65323);
            _2 = (int)SEQ_PTR(_32852);
            _32853 = (int)*(((s1_ptr)_2)->base + _i_65327);
            _32852 = NOVALUE;
            Ref(_32853);
            _file_65331 = _18abs(_32853);
            _32853 = NOVALUE;
            if (!IS_ATOM_INT(_file_65331)) {
                _1 = (long)(DBL_PTR(_file_65331)->dbl);
                if (UNIQUE(DBL_PTR(_file_65331)) && (DBL_PTR(_file_65331)->cleanup != 0))
                RTFatal("Cannot assign value with a destructor to an integer");                DeRefDS(_file_65331);
                _file_65331 = _1;
            }

            /** 			if show_all or not find( file, included ) then*/
            if (_show_all_65301 != 0) {
                goto L9; // [148] 165
            }
            _32856 = find_from(_file_65331, _included_65321, 1);
            _32857 = (_32856 == 0);
            _32856 = NOVALUE;
            if (_32857 == 0)
            {
                DeRef(_32857);
                _32857 = NOVALUE;
                goto LA; // [161] 225
            }
            else{
                DeRef(_32857);
                _32857 = NOVALUE;
            }
L9: 

            /** 				if stdlib or match( "std/", short_names[file] ) != 1 then*/
            if (_stdlib_65302 != 0) {
                goto LB; // [167] 191
            }
            _2 = (int)SEQ_PTR(_71short_names_65016);
            _32859 = (int)*(((s1_ptr)_2)->base + _file_65331);
            _32860 = e_match_from(_32830, _32859, 1);
            _32859 = NOVALUE;
            _32861 = (_32860 != 1);
            _32860 = NOVALUE;
            if (_32861 == 0)
            {
                DeRef(_32861);
                _32861 = NOVALUE;
                goto LC; // [187] 224
            }
            else{
                DeRef(_32861);
                _32861 = NOVALUE;
            }
LB: 

            /** 					lines &= sprintf("\t\"%s\" -> \"%s\"\n", {short_names[fi], short_names[file]})*/
            _2 = (int)SEQ_PTR(_71short_names_65016);
            _32862 = (int)*(((s1_ptr)_2)->base + _fi_65323);
            _2 = (int)SEQ_PTR(_71short_names_65016);
            _32863 = (int)*(((s1_ptr)_2)->base + _file_65331);
            Ref(_32863);
            Ref(_32862);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _32862;
            ((int *)_2)[2] = _32863;
            _32864 = MAKE_SEQ(_1);
            _32863 = NOVALUE;
            _32862 = NOVALUE;
            _32865 = EPrintf(-9999999, _32726, _32864);
            DeRefDS(_32864);
            _32864 = NOVALUE;
            Concat((object_ptr)&_lines_65303, _lines_65303, _32865);
            DeRefDS(_32865);
            _32865 = NOVALUE;

            /** 					included = append( included, file )*/
            Append(&_included_65321, _included_65321, _file_65331);
LC: 
LA: 

            /** 		end for*/
            _i_65327 = _i_65327 + 1;
            goto L7; // [229] 128
L8: 
            ;
        }

        /** 	end for*/
        _fi_65323 = _fi_65323 + 1;
        goto L5; // [236] 110
L6: 
        ;
    }

    /** 	return lines & "}\n"*/
    Concat((object_ptr)&_32868, _lines_65303, _32816);
    DeRefDSi(_lines_65303);
    DeRefi(_included_65321);
    _32850 = NOVALUE;
    return _32868;
    ;
}


int _71diagram_file_deps(int _fn_65355)
{
    int _lines_65356 = NOVALUE;
    int _32906 = NOVALUE;
    int _32904 = NOVALUE;
    int _32903 = NOVALUE;
    int _32902 = NOVALUE;
    int _32901 = NOVALUE;
    int _32900 = NOVALUE;
    int _32899 = NOVALUE;
    int _32898 = NOVALUE;
    int _32897 = NOVALUE;
    int _32896 = NOVALUE;
    int _32894 = NOVALUE;
    int _32893 = NOVALUE;
    int _32892 = NOVALUE;
    int _32891 = NOVALUE;
    int _32890 = NOVALUE;
    int _32889 = NOVALUE;
    int _32888 = NOVALUE;
    int _32887 = NOVALUE;
    int _32886 = NOVALUE;
    int _32885 = NOVALUE;
    int _32883 = NOVALUE;
    int _32880 = NOVALUE;
    int _32879 = NOVALUE;
    int _32878 = NOVALUE;
    int _32877 = NOVALUE;
    int _32876 = NOVALUE;
    int _32873 = NOVALUE;
    int _32872 = NOVALUE;
    int _32871 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_fn_65355)) {
        _1 = (long)(DBL_PTR(_fn_65355)->dbl);
        if (UNIQUE(DBL_PTR(_fn_65355)) && (DBL_PTR(_fn_65355)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fn_65355);
        _fn_65355 = _1;
    }

    /** 	sequence lines = "digraph filedep { rankdir=TB \n"*/
    RefDS(_32869);
    DeRefi(_lines_65356);
    _lines_65356 = _32869;

    /** 	lines &= sprintf( "\tnode [shape=box,style=filled,color=lightblue] \"%s\"\n\tnode [shape=box,style=\"\",color=black]\n", {short_names[fn]} )*/
    _2 = (int)SEQ_PTR(_71short_names_65016);
    _32871 = (int)*(((s1_ptr)_2)->base + _fn_65355);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_32871);
    *((int *)(_2+4)) = _32871;
    _32872 = MAKE_SEQ(_1);
    _32871 = NOVALUE;
    _32873 = EPrintf(-9999999, _32870, _32872);
    DeRefDS(_32872);
    _32872 = NOVALUE;
    Concat((object_ptr)&_lines_65356, _lines_65356, _32873);
    DeRefDS(_32873);
    _32873 = NOVALUE;

    /** 	if show_stdlib then*/
    if (_71show_stdlib_65018 == 0)
    {
        goto L1; // [34] 103
    }
    else{
    }

    /** 		lines &= "\tsubgraph \"cluster_stdlib\"{ rank=max\n"*/
    Concat((object_ptr)&_lines_65356, _lines_65356, _32837);

    /** 		for i = 1 to length(short_names) do*/
    if (IS_SEQUENCE(_71short_names_65016)){
            _32876 = SEQ_PTR(_71short_names_65016)->length;
    }
    else {
        _32876 = 1;
    }
    {
        int _i_65366;
        _i_65366 = 1;
L2: 
        if (_i_65366 > _32876){
            goto L3; // [50] 96
        }

        /** 			if std_libs[i] then*/
        _2 = (int)SEQ_PTR(_71std_libs_65017);
        _32877 = (int)*(((s1_ptr)_2)->base + _i_65366);
        if (_32877 == 0)
        {
            _32877 = NOVALUE;
            goto L4; // [65] 89
        }
        else{
            _32877 = NOVALUE;
        }

        /** 				lines &= sprintf( "\"%s\"\n", {short_names[i]})*/
        _2 = (int)SEQ_PTR(_71short_names_65016);
        _32878 = (int)*(((s1_ptr)_2)->base + _i_65366);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_32878);
        *((int *)(_2+4)) = _32878;
        _32879 = MAKE_SEQ(_1);
        _32878 = NOVALUE;
        _32880 = EPrintf(-9999999, _32843, _32879);
        DeRefDS(_32879);
        _32879 = NOVALUE;
        Concat((object_ptr)&_lines_65356, _lines_65356, _32880);
        DeRefDS(_32880);
        _32880 = NOVALUE;
L4: 

        /** 		end for*/
        _i_65366 = _i_65366 + 1;
        goto L2; // [91] 57
L3: 
        ;
    }

    /** 		lines &= "\t}\n"*/
    Concat((object_ptr)&_lines_65356, _lines_65356, _32790);
L1: 

    /** 	for f = 1 to length( short_names ) do*/
    if (IS_SEQUENCE(_71short_names_65016)){
            _32883 = SEQ_PTR(_71short_names_65016)->length;
    }
    else {
        _32883 = 1;
    }
    {
        int _f_65376;
        _f_65376 = 1;
L5: 
        if (_f_65376 > _32883){
            goto L6; // [110] 266
        }

        /** 		if f != fn then*/
        if (_f_65376 == _fn_65355)
        goto L7; // [119] 193

        /** 			if find( fn, file_include[f] ) and (show_stdlib or not std_libs[f]) then*/
        _2 = (int)SEQ_PTR(_26file_include_11143);
        _32885 = (int)*(((s1_ptr)_2)->base + _f_65376);
        _32886 = find_from(_fn_65355, _32885, 1);
        _32885 = NOVALUE;
        if (_32886 == 0) {
            goto L8; // [136] 259
        }
        if (_71show_stdlib_65018 != 0) {
            DeRef(_32888);
            _32888 = 1;
            goto L9; // [142] 159
        }
        _2 = (int)SEQ_PTR(_71std_libs_65017);
        _32889 = (int)*(((s1_ptr)_2)->base + _f_65376);
        _32890 = (_32889 == 0);
        _32889 = NOVALUE;
        _32888 = (_32890 != 0);
L9: 
        if (_32888 == 0)
        {
            _32888 = NOVALUE;
            goto L8; // [160] 259
        }
        else{
            _32888 = NOVALUE;
        }

        /** 				lines &= sprintf("\t\"%s\" -> \"%s\"\n", { short_names[f], short_names[fn] } )*/
        _2 = (int)SEQ_PTR(_71short_names_65016);
        _32891 = (int)*(((s1_ptr)_2)->base + _f_65376);
        _2 = (int)SEQ_PTR(_71short_names_65016);
        _32892 = (int)*(((s1_ptr)_2)->base + _fn_65355);
        Ref(_32892);
        Ref(_32891);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _32891;
        ((int *)_2)[2] = _32892;
        _32893 = MAKE_SEQ(_1);
        _32892 = NOVALUE;
        _32891 = NOVALUE;
        _32894 = EPrintf(-9999999, _32726, _32893);
        DeRefDS(_32893);
        _32893 = NOVALUE;
        Concat((object_ptr)&_lines_65356, _lines_65356, _32894);
        DeRefDS(_32894);
        _32894 = NOVALUE;
        goto L8; // [190] 259
L7: 

        /** 			for i = 1 to length( file_include[f] ) do*/
        _2 = (int)SEQ_PTR(_26file_include_11143);
        _32896 = (int)*(((s1_ptr)_2)->base + _f_65376);
        if (IS_SEQUENCE(_32896)){
                _32897 = SEQ_PTR(_32896)->length;
        }
        else {
            _32897 = 1;
        }
        _32896 = NOVALUE;
        {
            int _i_65395;
            _i_65395 = 1;
LA: 
            if (_i_65395 > _32897){
                goto LB; // [204] 258
            }

            /** 				lines &= sprintf("\t\"%s\" -> \"%s\"\n", { short_names[fn], short_names[abs(file_include[f][i])] } )*/
            _2 = (int)SEQ_PTR(_71short_names_65016);
            _32898 = (int)*(((s1_ptr)_2)->base + _fn_65355);
            _2 = (int)SEQ_PTR(_26file_include_11143);
            _32899 = (int)*(((s1_ptr)_2)->base + _f_65376);
            _2 = (int)SEQ_PTR(_32899);
            _32900 = (int)*(((s1_ptr)_2)->base + _i_65395);
            _32899 = NOVALUE;
            Ref(_32900);
            _32901 = _18abs(_32900);
            _32900 = NOVALUE;
            _2 = (int)SEQ_PTR(_71short_names_65016);
            if (!IS_ATOM_INT(_32901)){
                _32902 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32901)->dbl));
            }
            else{
                _32902 = (int)*(((s1_ptr)_2)->base + _32901);
            }
            Ref(_32902);
            Ref(_32898);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _32898;
            ((int *)_2)[2] = _32902;
            _32903 = MAKE_SEQ(_1);
            _32902 = NOVALUE;
            _32898 = NOVALUE;
            _32904 = EPrintf(-9999999, _32726, _32903);
            DeRefDS(_32903);
            _32903 = NOVALUE;
            Concat((object_ptr)&_lines_65356, _lines_65356, _32904);
            DeRefDS(_32904);
            _32904 = NOVALUE;

            /** 			end for*/
            _i_65395 = _i_65395 + 1;
            goto LA; // [253] 211
LB: 
            ;
        }
L8: 

        /** 	end for*/
        _f_65376 = _f_65376 + 1;
        goto L5; // [261] 117
L6: 
        ;
    }

    /** 	return lines & "}\n"*/
    Concat((object_ptr)&_32906, _lines_65356, _32816);
    DeRefDSi(_lines_65356);
    DeRef(_32890);
    _32890 = NOVALUE;
    _32896 = NOVALUE;
    DeRef(_32901);
    _32901 = NOVALUE;
    return _32906;
    ;
}



// 0x79FD492B
