// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _73GetSourceName()
{
    int _real_name_68860 = NOVALUE;
    int _fh_68861 = NOVALUE;
    int _has_extension_68863 = NOVALUE;
    int _34846 = NOVALUE;
    int _34844 = NOVALUE;
    int _34843 = NOVALUE;
    int _34840 = NOVALUE;
    int _34839 = NOVALUE;
    int _34838 = NOVALUE;
    int _34837 = NOVALUE;
    int _34836 = NOVALUE;
    int _34835 = NOVALUE;
    int _34832 = NOVALUE;
    int _34831 = NOVALUE;
    int _34829 = NOVALUE;
    int _34828 = NOVALUE;
    int _34827 = NOVALUE;
    int _34826 = NOVALUE;
    int _34825 = NOVALUE;
    int _34824 = NOVALUE;
    int _34821 = NOVALUE;
    int _34820 = NOVALUE;
    int _34818 = NOVALUE;
    int _34817 = NOVALUE;
    int _34812 = NOVALUE;
    int _0, _1, _2;
    

    /** 	boolean has_extension = FALSE*/
    _has_extension_68863 = _5FALSE_242;

    /** 	if length(src_name) = 0 then*/
    if (IS_SEQUENCE(_39src_name_49878)){
            _34812 = SEQ_PTR(_39src_name_49878)->length;
    }
    else {
        _34812 = 1;
    }
    if (_34812 != 0)
    goto L1; // [15] 30

    /** 		show_banner()*/
    _39show_banner();

    /** 		return -2 -- No source file*/
    DeRef(_real_name_68860);
    return -2;
L1: 

    /** 	ifdef WINDOWS then*/

    /** 		src_name = match_replace("/", src_name, "\\")*/
    RefDS(_34814);
    RefDS(_39src_name_49878);
    RefDS(_34815);
    _0 = _7match_replace(_34814, _39src_name_49878, _34815, 0);
    DeRefDS(_39src_name_49878);
    _39src_name_49878 = _0;

    /** 	for p = length(src_name) to 1 by -1 do*/
    if (IS_SEQUENCE(_39src_name_49878)){
            _34817 = SEQ_PTR(_39src_name_49878)->length;
    }
    else {
        _34817 = 1;
    }
    {
        int _p_68877;
        _p_68877 = _34817;
L2: 
        if (_p_68877 < 1){
            goto L3; // [52] 116
        }

        /** 		if src_name[p] = '.' then*/
        _2 = (int)SEQ_PTR(_39src_name_49878);
        _34818 = (int)*(((s1_ptr)_2)->base + _p_68877);
        if (binary_op_a(NOTEQ, _34818, 46)){
            _34818 = NOVALUE;
            goto L4; // [67] 85
        }
        _34818 = NOVALUE;

        /** 		   has_extension = TRUE*/
        _has_extension_68863 = _5TRUE_244;

        /** 		   exit*/
        goto L3; // [80] 116
        goto L5; // [82] 109
L4: 

        /** 		elsif find(src_name[p], SLASH_CHARS) then*/
        _2 = (int)SEQ_PTR(_39src_name_49878);
        _34820 = (int)*(((s1_ptr)_2)->base + _p_68877);
        _34821 = find_from(_34820, _36SLASH_CHARS_14735, 1);
        _34820 = NOVALUE;
        if (_34821 == 0)
        {
            _34821 = NOVALUE;
            goto L6; // [100] 108
        }
        else{
            _34821 = NOVALUE;
        }

        /** 		   exit*/
        goto L3; // [105] 116
L6: 
L5: 

        /** 	end for*/
        _p_68877 = _p_68877 + -1;
        goto L2; // [111] 59
L3: 
        ;
    }

    /** 	if not has_extension then*/
    if (_has_extension_68863 != 0)
    goto L7; // [118] 223

    /** 		known_files = append(known_files, "")*/
    RefDS(_22682);
    Append(&_26known_files_11139, _26known_files_11139, _22682);

    /** 		for i = 1 to length( DEFAULT_EXTS ) do*/
    _34824 = 4;
    {
        int _i_68896;
        _i_68896 = 1;
L8: 
        if (_i_68896 > 4){
            goto L9; // [138] 203
        }

        /** 			known_files[$] = src_name & DEFAULT_EXTS[i]*/
        if (IS_SEQUENCE(_26known_files_11139)){
                _34825 = SEQ_PTR(_26known_files_11139)->length;
        }
        else {
            _34825 = 1;
        }
        _2 = (int)SEQ_PTR(_36DEFAULT_EXTS_14716);
        _34826 = (int)*(((s1_ptr)_2)->base + _i_68896);
        Concat((object_ptr)&_34827, _39src_name_49878, _34826);
        _34826 = NOVALUE;
        _2 = (int)SEQ_PTR(_26known_files_11139);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26known_files_11139 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _34825);
        _1 = *(int *)_2;
        *(int *)_2 = _34827;
        if( _1 != _34827 ){
            DeRef(_1);
        }
        _34827 = NOVALUE;

        /** 			real_name = e_path_find(known_files[$])*/
        if (IS_SEQUENCE(_26known_files_11139)){
                _34828 = SEQ_PTR(_26known_files_11139)->length;
        }
        else {
            _34828 = 1;
        }
        _2 = (int)SEQ_PTR(_26known_files_11139);
        _34829 = (int)*(((s1_ptr)_2)->base + _34828);
        Ref(_34829);
        _0 = _real_name_68860;
        _real_name_68860 = _38e_path_find(_34829);
        DeRef(_0);
        _34829 = NOVALUE;

        /** 			if sequence(real_name) then*/
        _34831 = IS_SEQUENCE(_real_name_68860);
        if (_34831 == 0)
        {
            _34831 = NOVALUE;
            goto LA; // [188] 196
        }
        else{
            _34831 = NOVALUE;
        }

        /** 				exit*/
        goto L9; // [193] 203
LA: 

        /** 		end for*/
        _i_68896 = _i_68896 + 1;
        goto L8; // [198] 145
L9: 
        ;
    }

    /** 		if atom(real_name) then*/
    _34832 = IS_ATOM(_real_name_68860);
    if (_34832 == 0)
    {
        _34832 = NOVALUE;
        goto LB; // [210] 259
    }
    else{
        _34832 = NOVALUE;
    }

    /** 			return -1*/
    DeRef(_real_name_68860);
    return -1;
    goto LB; // [220] 259
L7: 

    /** 		known_files = append(known_files, src_name)*/
    RefDS(_39src_name_49878);
    Append(&_26known_files_11139, _26known_files_11139, _39src_name_49878);

    /** 		real_name = e_path_find(src_name)*/
    RefDS(_39src_name_49878);
    _0 = _real_name_68860;
    _real_name_68860 = _38e_path_find(_39src_name_49878);
    DeRef(_0);

    /** 		if atom(real_name) then*/
    _34835 = IS_ATOM(_real_name_68860);
    if (_34835 == 0)
    {
        _34835 = NOVALUE;
        goto LC; // [248] 258
    }
    else{
        _34835 = NOVALUE;
    }

    /** 			return -1*/
    DeRef(_real_name_68860);
    return -1;
LC: 
LB: 

    /** 	known_files[$] = canonical_path(real_name,,CORRECT)*/
    if (IS_SEQUENCE(_26known_files_11139)){
            _34836 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _34836 = 1;
    }
    Ref(_real_name_68860);
    _34837 = _9canonical_path(_real_name_68860, 0, 2);
    _2 = (int)SEQ_PTR(_26known_files_11139);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26known_files_11139 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _34836);
    _1 = *(int *)_2;
    *(int *)_2 = _34837;
    if( _1 != _34837 ){
        DeRef(_1);
    }
    _34837 = NOVALUE;

    /** 	known_files_hash &= hash(known_files[$], stdhash:HSIEH32)*/
    if (IS_SEQUENCE(_26known_files_11139)){
            _34838 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _34838 = 1;
    }
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _34839 = (int)*(((s1_ptr)_2)->base + _34838);
    _34840 = calc_hash(_34839, -5);
    _34839 = NOVALUE;
    Ref(_34840);
    Append(&_26known_files_hash_11140, _26known_files_hash_11140, _34840);
    DeRef(_34840);
    _34840 = NOVALUE;

    /** 	finished_files &= 0*/
    Append(&_26finished_files_11141, _26finished_files_11141, 0);

    /** 	file_include_depend = append( file_include_depend, { length( known_files ) } )*/
    if (IS_SEQUENCE(_26known_files_11139)){
            _34843 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _34843 = 1;
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _34843;
    _34844 = MAKE_SEQ(_1);
    _34843 = NOVALUE;
    RefDS(_34844);
    Append(&_26file_include_depend_11142, _26file_include_depend_11142, _34844);
    DeRefDS(_34844);
    _34844 = NOVALUE;

    /** 	if file_exists(real_name) then*/
    Ref(_real_name_68860);
    _34846 = _9file_exists(_real_name_68860);
    if (_34846 == 0) {
        DeRef(_34846);
        _34846 = NOVALUE;
        goto LD; // [340] 364
    }
    else {
        if (!IS_ATOM_INT(_34846) && DBL_PTR(_34846)->dbl == 0.0){
            DeRef(_34846);
            _34846 = NOVALUE;
            goto LD; // [340] 364
        }
        DeRef(_34846);
        _34846 = NOVALUE;
    }
    DeRef(_34846);
    _34846 = NOVALUE;

    /** 		real_name = maybe_preprocess(real_name)*/
    Ref(_real_name_68860);
    _0 = _real_name_68860;
    _real_name_68860 = _64maybe_preprocess(_real_name_68860);
    DeRef(_0);

    /** 		fh = open_locked(real_name)*/
    Ref(_real_name_68860);
    _fh_68861 = _26open_locked(_real_name_68860);
    if (!IS_ATOM_INT(_fh_68861)) {
        _1 = (long)(DBL_PTR(_fh_68861)->dbl);
        if (UNIQUE(DBL_PTR(_fh_68861)) && (DBL_PTR(_fh_68861)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fh_68861);
        _fh_68861 = _1;
    }

    /** 		return fh*/
    DeRef(_real_name_68860);
    return _fh_68861;
LD: 

    /** 	return -1*/
    DeRef(_real_name_68860);
    return -1;
    ;
}


void _73main()
{
    int _argc_68965 = NOVALUE;
    int _argv_68966 = NOVALUE;
    int _34877 = NOVALUE;
    int _34876 = NOVALUE;
    int _34873 = NOVALUE;
    int _34871 = NOVALUE;
    int _34869 = NOVALUE;
    int _34868 = NOVALUE;
    int _34867 = NOVALUE;
    int _34866 = NOVALUE;
    int _34865 = NOVALUE;
    int _34864 = NOVALUE;
    int _34863 = NOVALUE;
    int _34862 = NOVALUE;
    int _34858 = NOVALUE;
    int _0, _1, _2;
    

    /** 	argv = command_line()*/
    DeRef(_argv_68966);
    _argv_68966 = Command_Line();

    /** 	if BIND then*/
    if (_25BIND_11877 == 0)
    {
        goto L1; // [9] 21
    }
    else{
    }

    /** 		argv = extract_options(argv)*/
    RefDS(_argv_68966);
    _0 = _argv_68966;
    _argv_68966 = _2extract_options(_argv_68966);
    DeRefDS(_0);
L1: 

    /** 	argc = length(argv)*/
    if (IS_SEQUENCE(_argv_68966)){
            _argc_68965 = SEQ_PTR(_argv_68966)->length;
    }
    else {
        _argc_68965 = 1;
    }

    /** 	Argv = argv*/
    RefDS(_argv_68966);
    DeRef(_25Argv_12273);
    _25Argv_12273 = _argv_68966;

    /** 	Argc = argc*/
    _25Argc_12272 = _argc_68965;

    /** 	TempErrName = "ex.err"*/
    RefDS(_34857);
    DeRefi(_43TempErrName_49530);
    _43TempErrName_49530 = _34857;

    /** 	TempWarningName = STDERR*/
    DeRef(_25TempWarningName_12276);
    _25TempWarningName_12276 = 2;

    /** 	display_warnings = 1*/
    _43display_warnings_49531 = 1;

    /** 	InitGlobals()*/
    _30InitGlobals();

    /** 	if TRANSLATE or BIND or INTERPRET then*/
    if (_25TRANSLATE_11874 != 0) {
        _34858 = 1;
        goto L2; // [69] 79
    }
    _34858 = (_25BIND_11877 != 0);
L2: 
    if (_34858 != 0) {
        goto L3; // [79] 90
    }
    if (_25INTERPRET_11871 == 0)
    {
        goto L4; // [86] 96
    }
    else{
    }
L3: 

    /** 		InitBackEnd(0)*/
    _2InitBackEnd(0);
L4: 

    /** 	src_file = GetSourceName()*/
    _0 = _73GetSourceName();
    _25src_file_12404 = _0;
    if (!IS_ATOM_INT(_25src_file_12404)) {
        _1 = (long)(DBL_PTR(_25src_file_12404)->dbl);
        if (UNIQUE(DBL_PTR(_25src_file_12404)) && (DBL_PTR(_25src_file_12404)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_25src_file_12404);
        _25src_file_12404 = _1;
    }

    /** 	if src_file = -1 then*/
    if (_25src_file_12404 != -1)
    goto L5; // [107] 181

    /** 		screen_output(STDERR, GetMsgText(51, 0, {known_files[$]}))*/
    if (IS_SEQUENCE(_26known_files_11139)){
            _34862 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _34862 = 1;
    }
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _34863 = (int)*(((s1_ptr)_2)->base + _34862);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_34863);
    *((int *)(_2+4)) = _34863;
    _34864 = MAKE_SEQ(_1);
    _34863 = NOVALUE;
    _34865 = _44GetMsgText(51, 0, _34864);
    _34864 = NOVALUE;
    _43screen_output(2, _34865);
    _34865 = NOVALUE;

    /** 		if not batch_job and not test_only then*/
    _34866 = (_25batch_job_12275 == 0);
    if (_34866 == 0) {
        goto L6; // [145] 173
    }
    _34868 = (_25test_only_12274 == 0);
    if (_34868 == 0)
    {
        DeRef(_34868);
        _34868 = NOVALUE;
        goto L6; // [155] 173
    }
    else{
        DeRef(_34868);
        _34868 = NOVALUE;
    }

    /** 			maybe_any_key(GetMsgText(277,0), STDERR)*/
    RefDS(_22682);
    _34869 = _44GetMsgText(277, 0, _22682);
    _41maybe_any_key(_34869, 2);
    _34869 = NOVALUE;
L6: 

    /** 		Cleanup(1)*/
    _43Cleanup(1);
    goto L7; // [178] 226
L5: 

    /** 	elsif src_file >= 0 then*/
    if (_25src_file_12404 < 0)
    goto L8; // [185] 225

    /** 		main_path = known_files[$]*/
    if (IS_SEQUENCE(_26known_files_11139)){
            _34871 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _34871 = 1;
    }
    DeRef(_25main_path_12403);
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _25main_path_12403 = (int)*(((s1_ptr)_2)->base + _34871);
    Ref(_25main_path_12403);

    /** 		if length(main_path) = 0 then*/
    if (IS_SEQUENCE(_25main_path_12403)){
            _34873 = SEQ_PTR(_25main_path_12403)->length;
    }
    else {
        _34873 = 1;
    }
    if (_34873 != 0)
    goto L9; // [209] 224

    /** 			main_path = '.' & SLASH*/
    Concat((object_ptr)&_25main_path_12403, 46, 92);
L9: 
L8: 
L7: 

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto LA; // [230] 239
    }
    else{
    }

    /** 		InitBackEnd(1)*/
    _2InitBackEnd(1);
LA: 

    /** 	CheckPlatform()*/
    _2CheckPlatform();

    /** 	InitSymTab()*/
    _52InitSymTab();

    /** 	InitEmit()*/
    _37InitEmit();

    /** 	InitLex()*/
    _60InitLex();

    /** 	InitParser()*/
    _30InitParser();

    /** 	eu_namespace()*/
    _60eu_namespace();

    /** 	ifdef TRANSLATOR then*/

    /** 	main_file()*/
    _60main_file();

    /** 	check_coverage()*/
    _49check_coverage();

    /** 	parser()*/
    _30parser();

    /** 	init_coverage()*/
    _49init_coverage();

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto LB; // [285] 296
    }
    else{
    }

    /** 		BackEnd(0) -- translate IL to C*/
    _2BackEnd(0);
    goto LC; // [293] 336
LB: 

    /** 	elsif BIND then*/
    if (_25BIND_11877 == 0)
    {
        goto LD; // [300] 310
    }
    else{
    }

    /** 		OutputIL()*/
    _2OutputIL();
    goto LC; // [307] 336
LD: 

    /** 	elsif INTERPRET and not test_only then*/
    if (_25INTERPRET_11871 == 0) {
        goto LE; // [314] 335
    }
    _34877 = (_25test_only_12274 == 0);
    if (_34877 == 0)
    {
        DeRef(_34877);
        _34877 = NOVALUE;
        goto LE; // [324] 335
    }
    else{
        DeRef(_34877);
        _34877 = NOVALUE;
    }

    /** 		ifdef not STDDEBUG then*/

    /** 			BackEnd(0) -- execute IL using Euphoria-coded back-end*/
    _2BackEnd(0);
LE: 
LC: 

    /** 	Cleanup(0) -- does warnings*/
    _43Cleanup(0);

    /** end procedure*/
    DeRef(_argv_68966);
    DeRef(_34866);
    _34866 = NOVALUE;
    return;
    ;
}



// 0x16005008
