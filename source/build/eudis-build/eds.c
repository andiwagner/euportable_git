// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _48fatal(int _errcode_17870, int _msg_17871, int _routine_name_17872, int _parms_17873)
{
    int _10271 = NOVALUE;
    int _0, _1, _2;
    

    /** 	vLastErrors = append(vLastErrors, {errcode, msg, routine_name, parms})*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _errcode_17870;
    RefDS(_msg_17871);
    *((int *)(_2+8)) = _msg_17871;
    RefDS(_routine_name_17872);
    *((int *)(_2+12)) = _routine_name_17872;
    RefDS(_parms_17873);
    *((int *)(_2+16)) = _parms_17873;
    _10271 = MAKE_SEQ(_1);
    RefDS(_10271);
    Append(&_48vLastErrors_17867, _48vLastErrors_17867, _10271);
    DeRefDS(_10271);
    _10271 = NOVALUE;

    /** 	if db_fatal_id >= 0 then*/

    /** end procedure*/
    DeRefDSi(_msg_17871);
    DeRefDSi(_routine_name_17872);
    DeRefDS(_parms_17873);
    return;
    ;
}


int _48get4()
{
    int _10287 = NOVALUE;
    int _10286 = NOVALUE;
    int _10285 = NOVALUE;
    int _10284 = NOVALUE;
    int _10283 = NOVALUE;
    int _0, _1, _2;
    

    /** 	poke(mem0, getc(current_db))*/
    if (_48current_db_17843 != last_r_file_no) {
        last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
        last_r_file_no = _48current_db_17843;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _10283 = getKBchar();
        }
        else
        _10283 = getc(last_r_file_ptr);
    }
    else
    _10283 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_48mem0_17885)){
        poke_addr = (unsigned char *)_48mem0_17885;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    *poke_addr = (unsigned char)_10283;
    _10283 = NOVALUE;

    /** 	poke(mem1, getc(current_db))*/
    if (_48current_db_17843 != last_r_file_no) {
        last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
        last_r_file_no = _48current_db_17843;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _10284 = getKBchar();
        }
        else
        _10284 = getc(last_r_file_ptr);
    }
    else
    _10284 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_48mem1_17886)){
        poke_addr = (unsigned char *)_48mem1_17886;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_48mem1_17886)->dbl);
    }
    *poke_addr = (unsigned char)_10284;
    _10284 = NOVALUE;

    /** 	poke(mem2, getc(current_db))*/
    if (_48current_db_17843 != last_r_file_no) {
        last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
        last_r_file_no = _48current_db_17843;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _10285 = getKBchar();
        }
        else
        _10285 = getc(last_r_file_ptr);
    }
    else
    _10285 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_48mem2_17887)){
        poke_addr = (unsigned char *)_48mem2_17887;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_48mem2_17887)->dbl);
    }
    *poke_addr = (unsigned char)_10285;
    _10285 = NOVALUE;

    /** 	poke(mem3, getc(current_db))*/
    if (_48current_db_17843 != last_r_file_no) {
        last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
        last_r_file_no = _48current_db_17843;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _10286 = getKBchar();
        }
        else
        _10286 = getc(last_r_file_ptr);
    }
    else
    _10286 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_48mem3_17888)){
        poke_addr = (unsigned char *)_48mem3_17888;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_48mem3_17888)->dbl);
    }
    *poke_addr = (unsigned char)_10286;
    _10286 = NOVALUE;

    /** 	return peek4u(mem0)*/
    if (IS_ATOM_INT(_48mem0_17885)) {
        _10287 = *(unsigned long *)_48mem0_17885;
        if ((unsigned)_10287 > (unsigned)MAXINT)
        _10287 = NewDouble((double)(unsigned long)_10287);
    }
    else {
        _10287 = *(unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
        if ((unsigned)_10287 > (unsigned)MAXINT)
        _10287 = NewDouble((double)(unsigned long)_10287);
    }
    return _10287;
    ;
}


int _48get_string()
{
    int _where_inlined_where_at_33_17913 = NOVALUE;
    int _s_17902 = NOVALUE;
    int _c_17903 = NOVALUE;
    int _i_17904 = NOVALUE;
    int _10300 = NOVALUE;
    int _10297 = NOVALUE;
    int _10295 = NOVALUE;
    int _10293 = NOVALUE;
    int _0, _1, _2;
    

    /** 	s = repeat(0, 256)*/
    DeRefi(_s_17902);
    _s_17902 = Repeat(0, 256);

    /** 	i = 0*/
    _i_17904 = 0;

    /** 	while c with entry do*/
    goto L1; // [14] 91
L2: 
    if (_c_17903 == 0)
    {
        goto L3; // [19] 103
    }
    else{
    }

    /** 		if c = -1 then*/
    if (_c_17903 != -1)
    goto L4; // [24] 56

    /** 			fatal(MISSING_END, "string is missing 0 terminator", "get_string", {io:where(current_db)})*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_33_17913);
    _where_inlined_where_at_33_17913 = machine(20, _48current_db_17843);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_where_inlined_where_at_33_17913);
    *((int *)(_2+4)) = _where_inlined_where_at_33_17913;
    _10293 = MAKE_SEQ(_1);
    RefDS(_10291);
    RefDS(_10292);
    _48fatal(900, _10291, _10292, _10293);
    _10293 = NOVALUE;

    /** 			exit*/
    goto L3; // [53] 103
L4: 

    /** 		i += 1*/
    _i_17904 = _i_17904 + 1;

    /** 		if i > length(s) then*/
    if (IS_SEQUENCE(_s_17902)){
            _10295 = SEQ_PTR(_s_17902)->length;
    }
    else {
        _10295 = 1;
    }
    if (_i_17904 <= _10295)
    goto L5; // [67] 82

    /** 			s &= repeat(0, 256)*/
    _10297 = Repeat(0, 256);
    Concat((object_ptr)&_s_17902, _s_17902, _10297);
    DeRefDS(_10297);
    _10297 = NOVALUE;
L5: 

    /** 		s[i] = c*/
    _2 = (int)SEQ_PTR(_s_17902);
    _2 = (int)(((s1_ptr)_2)->base + _i_17904);
    *(int *)_2 = _c_17903;

    /** 	  entry*/
L1: 

    /** 		c = getc(current_db)*/
    if (_48current_db_17843 != last_r_file_no) {
        last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
        last_r_file_no = _48current_db_17843;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_17903 = getKBchar();
        }
        else
        _c_17903 = getc(last_r_file_ptr);
    }
    else
    _c_17903 = getc(last_r_file_ptr);

    /** 	end while*/
    goto L2; // [100] 17
L3: 

    /** 	return s[1..i]*/
    rhs_slice_target = (object_ptr)&_10300;
    RHS_Slice(_s_17902, 1, _i_17904);
    DeRefDSi(_s_17902);
    return _10300;
    ;
}


int _48equal_string(int _target_17925)
{
    int _c_17926 = NOVALUE;
    int _i_17927 = NOVALUE;
    int _where_inlined_where_at_29_17933 = NOVALUE;
    int _10311 = NOVALUE;
    int _10310 = NOVALUE;
    int _10307 = NOVALUE;
    int _10305 = NOVALUE;
    int _10303 = NOVALUE;
    int _0, _1, _2;
    

    /** 	i = 0*/
    _i_17927 = 0;

    /** 	while c with entry do*/
    goto L1; // [10] 98
L2: 
    if (_c_17926 == 0)
    {
        goto L3; // [15] 110
    }
    else{
    }

    /** 		if c = -1 then*/
    if (_c_17926 != -1)
    goto L4; // [20] 56

    /** 			fatal(MISSING_END, "string is missing 0 terminator", "equal_string", {io:where(current_db)})*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_29_17933);
    _where_inlined_where_at_29_17933 = machine(20, _48current_db_17843);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_where_inlined_where_at_29_17933);
    *((int *)(_2+4)) = _where_inlined_where_at_29_17933;
    _10303 = MAKE_SEQ(_1);
    RefDS(_10291);
    RefDS(_10302);
    _48fatal(900, _10291, _10302, _10303);
    _10303 = NOVALUE;

    /** 			return DB_FATAL_FAIL*/
    DeRefDS(_target_17925);
    return -404;
L4: 

    /** 		i += 1*/
    _i_17927 = _i_17927 + 1;

    /** 		if i > length(target) then*/
    if (IS_SEQUENCE(_target_17925)){
            _10305 = SEQ_PTR(_target_17925)->length;
    }
    else {
        _10305 = 1;
    }
    if (_i_17927 <= _10305)
    goto L5; // [67] 78

    /** 			return 0*/
    DeRefDS(_target_17925);
    return 0;
L5: 

    /** 		if target[i] != c then*/
    _2 = (int)SEQ_PTR(_target_17925);
    _10307 = (int)*(((s1_ptr)_2)->base + _i_17927);
    if (binary_op_a(EQUALS, _10307, _c_17926)){
        _10307 = NOVALUE;
        goto L6; // [84] 95
    }
    _10307 = NOVALUE;

    /** 			return 0*/
    DeRefDS(_target_17925);
    return 0;
L6: 

    /** 	  entry*/
L1: 

    /** 		c = getc(current_db)*/
    if (_48current_db_17843 != last_r_file_no) {
        last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
        last_r_file_no = _48current_db_17843;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_17926 = getKBchar();
        }
        else
        _c_17926 = getc(last_r_file_ptr);
    }
    else
    _c_17926 = getc(last_r_file_ptr);

    /** 	end while*/
    goto L2; // [107] 13
L3: 

    /** 	return (i = length(target))*/
    if (IS_SEQUENCE(_target_17925)){
            _10310 = SEQ_PTR(_target_17925)->length;
    }
    else {
        _10310 = 1;
    }
    _10311 = (_i_17927 == _10310);
    _10310 = NOVALUE;
    DeRefDS(_target_17925);
    return _10311;
    ;
}


int _48decompress(int _c_17984)
{
    int _s_17985 = NOVALUE;
    int _len_17986 = NOVALUE;
    int _float32_to_atom_inlined_float32_to_atom_at_176_18022 = NOVALUE;
    int _ieee32_inlined_float32_to_atom_at_173_18021 = NOVALUE;
    int _float64_to_atom_inlined_float64_to_atom_at_251_18035 = NOVALUE;
    int _ieee64_inlined_float64_to_atom_at_248_18034 = NOVALUE;
    int _10380 = NOVALUE;
    int _10379 = NOVALUE;
    int _10378 = NOVALUE;
    int _10375 = NOVALUE;
    int _10370 = NOVALUE;
    int _10369 = NOVALUE;
    int _10368 = NOVALUE;
    int _10367 = NOVALUE;
    int _10366 = NOVALUE;
    int _10365 = NOVALUE;
    int _10364 = NOVALUE;
    int _10363 = NOVALUE;
    int _10362 = NOVALUE;
    int _10361 = NOVALUE;
    int _10360 = NOVALUE;
    int _10359 = NOVALUE;
    int _10358 = NOVALUE;
    int _10357 = NOVALUE;
    int _10356 = NOVALUE;
    int _10355 = NOVALUE;
    int _10354 = NOVALUE;
    int _10353 = NOVALUE;
    int _10352 = NOVALUE;
    int _10351 = NOVALUE;
    int _10349 = NOVALUE;
    int _10348 = NOVALUE;
    int _10347 = NOVALUE;
    int _10346 = NOVALUE;
    int _10345 = NOVALUE;
    int _10344 = NOVALUE;
    int _10343 = NOVALUE;
    int _10342 = NOVALUE;
    int _10341 = NOVALUE;
    int _10338 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if c = 0 then*/
    if (_c_17984 != 0)
    goto L1; // [5] 34

    /** 		c = getc(current_db)*/
    if (_48current_db_17843 != last_r_file_no) {
        last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
        last_r_file_no = _48current_db_17843;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_17984 = getKBchar();
        }
        else
        _c_17984 = getc(last_r_file_ptr);
    }
    else
    _c_17984 = getc(last_r_file_ptr);

    /** 		if c < I2B then*/
    if (_c_17984 >= 249)
    goto L2; // [18] 33

    /** 			return c + MIN1B*/
    _10338 = _c_17984 + -9;
    DeRef(_s_17985);
    return _10338;
L2: 
L1: 

    /** 	switch c with fallthru do*/
    _0 = _c_17984;
    switch ( _0 ){ 

        /** 		case I2B then*/
        case 249:

        /** 			return getc(current_db) +*/
        if (_48current_db_17843 != last_r_file_no) {
            last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
            last_r_file_no = _48current_db_17843;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10341 = getKBchar();
            }
            else
            _10341 = getc(last_r_file_ptr);
        }
        else
        _10341 = getc(last_r_file_ptr);
        if (_48current_db_17843 != last_r_file_no) {
            last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
            last_r_file_no = _48current_db_17843;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10342 = getKBchar();
            }
            else
            _10342 = getc(last_r_file_ptr);
        }
        else
        _10342 = getc(last_r_file_ptr);
        _10343 = 256 * _10342;
        _10342 = NOVALUE;
        _10344 = _10341 + _10343;
        _10341 = NOVALUE;
        _10343 = NOVALUE;
        _10345 = _10344 + _48MIN2B_17964;
        if ((long)((unsigned long)_10345 + (unsigned long)HIGH_BITS) >= 0) 
        _10345 = NewDouble((double)_10345);
        _10344 = NOVALUE;
        DeRef(_s_17985);
        DeRef(_10338);
        _10338 = NOVALUE;
        return _10345;

        /** 		case I3B then*/
        case 250:

        /** 			return getc(current_db) +*/
        if (_48current_db_17843 != last_r_file_no) {
            last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
            last_r_file_no = _48current_db_17843;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10346 = getKBchar();
            }
            else
            _10346 = getc(last_r_file_ptr);
        }
        else
        _10346 = getc(last_r_file_ptr);
        if (_48current_db_17843 != last_r_file_no) {
            last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
            last_r_file_no = _48current_db_17843;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10347 = getKBchar();
            }
            else
            _10347 = getc(last_r_file_ptr);
        }
        else
        _10347 = getc(last_r_file_ptr);
        _10348 = 256 * _10347;
        _10347 = NOVALUE;
        _10349 = _10346 + _10348;
        _10346 = NOVALUE;
        _10348 = NOVALUE;
        if (_48current_db_17843 != last_r_file_no) {
            last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
            last_r_file_no = _48current_db_17843;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10351 = getKBchar();
            }
            else
            _10351 = getc(last_r_file_ptr);
        }
        else
        _10351 = getc(last_r_file_ptr);
        _10352 = 65536 * _10351;
        _10351 = NOVALUE;
        _10353 = _10349 + _10352;
        _10349 = NOVALUE;
        _10352 = NOVALUE;
        _10354 = _10353 + _48MIN3B_17971;
        if ((long)((unsigned long)_10354 + (unsigned long)HIGH_BITS) >= 0) 
        _10354 = NewDouble((double)_10354);
        _10353 = NOVALUE;
        DeRef(_s_17985);
        DeRef(_10338);
        _10338 = NOVALUE;
        DeRef(_10345);
        _10345 = NOVALUE;
        return _10354;

        /** 		case I4B then*/
        case 251:

        /** 			return get4() + MIN4B*/
        _10355 = _48get4();
        if (IS_ATOM_INT(_10355) && IS_ATOM_INT(_48MIN4B_17978)) {
            _10356 = _10355 + _48MIN4B_17978;
            if ((long)((unsigned long)_10356 + (unsigned long)HIGH_BITS) >= 0) 
            _10356 = NewDouble((double)_10356);
        }
        else {
            _10356 = binary_op(PLUS, _10355, _48MIN4B_17978);
        }
        DeRef(_10355);
        _10355 = NOVALUE;
        DeRef(_s_17985);
        DeRef(_10338);
        _10338 = NOVALUE;
        DeRef(_10345);
        _10345 = NOVALUE;
        DeRef(_10354);
        _10354 = NOVALUE;
        return _10356;

        /** 		case F4B then*/
        case 252:

        /** 			return convert:float32_to_atom({getc(current_db), getc(current_db),*/
        if (_48current_db_17843 != last_r_file_no) {
            last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
            last_r_file_no = _48current_db_17843;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10357 = getKBchar();
            }
            else
            _10357 = getc(last_r_file_ptr);
        }
        else
        _10357 = getc(last_r_file_ptr);
        if (_48current_db_17843 != last_r_file_no) {
            last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
            last_r_file_no = _48current_db_17843;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10358 = getKBchar();
            }
            else
            _10358 = getc(last_r_file_ptr);
        }
        else
        _10358 = getc(last_r_file_ptr);
        if (_48current_db_17843 != last_r_file_no) {
            last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
            last_r_file_no = _48current_db_17843;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10359 = getKBchar();
            }
            else
            _10359 = getc(last_r_file_ptr);
        }
        else
        _10359 = getc(last_r_file_ptr);
        if (_48current_db_17843 != last_r_file_no) {
            last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
            last_r_file_no = _48current_db_17843;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10360 = getKBchar();
            }
            else
            _10360 = getc(last_r_file_ptr);
        }
        else
        _10360 = getc(last_r_file_ptr);
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _10357;
        *((int *)(_2+8)) = _10358;
        *((int *)(_2+12)) = _10359;
        *((int *)(_2+16)) = _10360;
        _10361 = MAKE_SEQ(_1);
        _10360 = NOVALUE;
        _10359 = NOVALUE;
        _10358 = NOVALUE;
        _10357 = NOVALUE;
        DeRefi(_ieee32_inlined_float32_to_atom_at_173_18021);
        _ieee32_inlined_float32_to_atom_at_173_18021 = _10361;
        _10361 = NOVALUE;

        /** 	return machine_func(M_F32_TO_A, ieee32)*/
        DeRef(_float32_to_atom_inlined_float32_to_atom_at_176_18022);
        _float32_to_atom_inlined_float32_to_atom_at_176_18022 = machine(49, _ieee32_inlined_float32_to_atom_at_173_18021);
        DeRefi(_ieee32_inlined_float32_to_atom_at_173_18021);
        _ieee32_inlined_float32_to_atom_at_173_18021 = NOVALUE;
        DeRef(_s_17985);
        DeRef(_10338);
        _10338 = NOVALUE;
        DeRef(_10345);
        _10345 = NOVALUE;
        DeRef(_10354);
        _10354 = NOVALUE;
        DeRef(_10356);
        _10356 = NOVALUE;
        return _float32_to_atom_inlined_float32_to_atom_at_176_18022;

        /** 		case F8B then*/
        case 253:

        /** 			return convert:float64_to_atom({getc(current_db), getc(current_db),*/
        if (_48current_db_17843 != last_r_file_no) {
            last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
            last_r_file_no = _48current_db_17843;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10362 = getKBchar();
            }
            else
            _10362 = getc(last_r_file_ptr);
        }
        else
        _10362 = getc(last_r_file_ptr);
        if (_48current_db_17843 != last_r_file_no) {
            last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
            last_r_file_no = _48current_db_17843;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10363 = getKBchar();
            }
            else
            _10363 = getc(last_r_file_ptr);
        }
        else
        _10363 = getc(last_r_file_ptr);
        if (_48current_db_17843 != last_r_file_no) {
            last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
            last_r_file_no = _48current_db_17843;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10364 = getKBchar();
            }
            else
            _10364 = getc(last_r_file_ptr);
        }
        else
        _10364 = getc(last_r_file_ptr);
        if (_48current_db_17843 != last_r_file_no) {
            last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
            last_r_file_no = _48current_db_17843;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10365 = getKBchar();
            }
            else
            _10365 = getc(last_r_file_ptr);
        }
        else
        _10365 = getc(last_r_file_ptr);
        if (_48current_db_17843 != last_r_file_no) {
            last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
            last_r_file_no = _48current_db_17843;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10366 = getKBchar();
            }
            else
            _10366 = getc(last_r_file_ptr);
        }
        else
        _10366 = getc(last_r_file_ptr);
        if (_48current_db_17843 != last_r_file_no) {
            last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
            last_r_file_no = _48current_db_17843;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10367 = getKBchar();
            }
            else
            _10367 = getc(last_r_file_ptr);
        }
        else
        _10367 = getc(last_r_file_ptr);
        if (_48current_db_17843 != last_r_file_no) {
            last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
            last_r_file_no = _48current_db_17843;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10368 = getKBchar();
            }
            else
            _10368 = getc(last_r_file_ptr);
        }
        else
        _10368 = getc(last_r_file_ptr);
        if (_48current_db_17843 != last_r_file_no) {
            last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
            last_r_file_no = _48current_db_17843;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10369 = getKBchar();
            }
            else
            _10369 = getc(last_r_file_ptr);
        }
        else
        _10369 = getc(last_r_file_ptr);
        _1 = NewS1(8);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _10362;
        *((int *)(_2+8)) = _10363;
        *((int *)(_2+12)) = _10364;
        *((int *)(_2+16)) = _10365;
        *((int *)(_2+20)) = _10366;
        *((int *)(_2+24)) = _10367;
        *((int *)(_2+28)) = _10368;
        *((int *)(_2+32)) = _10369;
        _10370 = MAKE_SEQ(_1);
        _10369 = NOVALUE;
        _10368 = NOVALUE;
        _10367 = NOVALUE;
        _10366 = NOVALUE;
        _10365 = NOVALUE;
        _10364 = NOVALUE;
        _10363 = NOVALUE;
        _10362 = NOVALUE;
        DeRefi(_ieee64_inlined_float64_to_atom_at_248_18034);
        _ieee64_inlined_float64_to_atom_at_248_18034 = _10370;
        _10370 = NOVALUE;

        /** 	return machine_func(M_F64_TO_A, ieee64)*/
        DeRef(_float64_to_atom_inlined_float64_to_atom_at_251_18035);
        _float64_to_atom_inlined_float64_to_atom_at_251_18035 = machine(47, _ieee64_inlined_float64_to_atom_at_248_18034);
        DeRefi(_ieee64_inlined_float64_to_atom_at_248_18034);
        _ieee64_inlined_float64_to_atom_at_248_18034 = NOVALUE;
        DeRef(_s_17985);
        DeRef(_10338);
        _10338 = NOVALUE;
        DeRef(_10345);
        _10345 = NOVALUE;
        DeRef(_10354);
        _10354 = NOVALUE;
        DeRef(_10356);
        _10356 = NOVALUE;
        return _float64_to_atom_inlined_float64_to_atom_at_251_18035;

        /** 		case else*/
        default:

        /** 			if c = S1B then*/
        if (_c_17984 != 254)
        goto L3; // [273] 287

        /** 				len = getc(current_db)*/
        if (_48current_db_17843 != last_r_file_no) {
            last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
            last_r_file_no = _48current_db_17843;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _len_17986 = getKBchar();
            }
            else
            _len_17986 = getc(last_r_file_ptr);
        }
        else
        _len_17986 = getc(last_r_file_ptr);
        goto L4; // [284] 295
L3: 

        /** 				len = get4()*/
        _len_17986 = _48get4();
        if (!IS_ATOM_INT(_len_17986)) {
            _1 = (long)(DBL_PTR(_len_17986)->dbl);
            if (UNIQUE(DBL_PTR(_len_17986)) && (DBL_PTR(_len_17986)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_len_17986);
            _len_17986 = _1;
        }
L4: 

        /** 			s = repeat(0, len)*/
        DeRef(_s_17985);
        _s_17985 = Repeat(0, _len_17986);

        /** 			for i = 1 to len do*/
        _10375 = _len_17986;
        {
            int _i_18044;
            _i_18044 = 1;
L5: 
            if (_i_18044 > _10375){
                goto L6; // [308] 362
            }

            /** 				c = getc(current_db)*/
            if (_48current_db_17843 != last_r_file_no) {
                last_r_file_ptr = which_file(_48current_db_17843, EF_READ);
                last_r_file_no = _48current_db_17843;
            }
            if (last_r_file_ptr == xstdin) {
                show_console();
                if (in_from_keyb) {
                    _c_17984 = getKBchar();
                }
                else
                _c_17984 = getc(last_r_file_ptr);
            }
            else
            _c_17984 = getc(last_r_file_ptr);

            /** 				if c < I2B then*/
            if (_c_17984 >= 249)
            goto L7; // [324] 341

            /** 					s[i] = c + MIN1B*/
            _10378 = _c_17984 + -9;
            _2 = (int)SEQ_PTR(_s_17985);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_17985 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_18044);
            _1 = *(int *)_2;
            *(int *)_2 = _10378;
            if( _1 != _10378 ){
                DeRef(_1);
            }
            _10378 = NOVALUE;
            goto L8; // [338] 355
L7: 

            /** 					s[i] = decompress(c)*/
            DeRef(_10379);
            _10379 = _c_17984;
            _10380 = _48decompress(_10379);
            _10379 = NOVALUE;
            _2 = (int)SEQ_PTR(_s_17985);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_17985 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_18044);
            _1 = *(int *)_2;
            *(int *)_2 = _10380;
            if( _1 != _10380 ){
                DeRef(_1);
            }
            _10380 = NOVALUE;
L8: 

            /** 			end for*/
            _i_18044 = _i_18044 + 1;
            goto L5; // [357] 315
L6: 
            ;
        }

        /** 			return s*/
        DeRef(_10338);
        _10338 = NOVALUE;
        DeRef(_10345);
        _10345 = NOVALUE;
        DeRef(_10354);
        _10354 = NOVALUE;
        DeRef(_10356);
        _10356 = NOVALUE;
        return _s_17985;
    ;}    ;
}


int _48compress(int _x_18055)
{
    int _x4_18056 = NOVALUE;
    int _s_18057 = NOVALUE;
    int _atom_to_float32_inlined_atom_to_float32_at_192_18091 = NOVALUE;
    int _float32_to_atom_inlined_float32_to_atom_at_203_18094 = NOVALUE;
    int _atom_to_float64_inlined_atom_to_float64_at_229_18099 = NOVALUE;
    int _10419 = NOVALUE;
    int _10418 = NOVALUE;
    int _10417 = NOVALUE;
    int _10415 = NOVALUE;
    int _10414 = NOVALUE;
    int _10412 = NOVALUE;
    int _10410 = NOVALUE;
    int _10409 = NOVALUE;
    int _10408 = NOVALUE;
    int _10406 = NOVALUE;
    int _10405 = NOVALUE;
    int _10404 = NOVALUE;
    int _10403 = NOVALUE;
    int _10402 = NOVALUE;
    int _10401 = NOVALUE;
    int _10400 = NOVALUE;
    int _10399 = NOVALUE;
    int _10398 = NOVALUE;
    int _10396 = NOVALUE;
    int _10395 = NOVALUE;
    int _10394 = NOVALUE;
    int _10393 = NOVALUE;
    int _10392 = NOVALUE;
    int _10391 = NOVALUE;
    int _10389 = NOVALUE;
    int _10388 = NOVALUE;
    int _10387 = NOVALUE;
    int _10386 = NOVALUE;
    int _10385 = NOVALUE;
    int _10384 = NOVALUE;
    int _10383 = NOVALUE;
    int _10382 = NOVALUE;
    int _10381 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(x) then*/
    if (IS_ATOM_INT(_x_18055))
    _10381 = 1;
    else if (IS_ATOM_DBL(_x_18055))
    _10381 = IS_ATOM_INT(DoubleToInt(_x_18055));
    else
    _10381 = 0;
    if (_10381 == 0)
    {
        _10381 = NOVALUE;
        goto L1; // [6] 183
    }
    else{
        _10381 = NOVALUE;
    }

    /** 		if x >= MIN1B and x <= MAX1B then*/
    if (IS_ATOM_INT(_x_18055)) {
        _10382 = (_x_18055 >= -9);
    }
    else {
        _10382 = binary_op(GREATEREQ, _x_18055, -9);
    }
    if (IS_ATOM_INT(_10382)) {
        if (_10382 == 0) {
            goto L2; // [15] 44
        }
    }
    else {
        if (DBL_PTR(_10382)->dbl == 0.0) {
            goto L2; // [15] 44
        }
    }
    if (IS_ATOM_INT(_x_18055)) {
        _10384 = (_x_18055 <= 239);
    }
    else {
        _10384 = binary_op(LESSEQ, _x_18055, 239);
    }
    if (_10384 == 0) {
        DeRef(_10384);
        _10384 = NOVALUE;
        goto L2; // [24] 44
    }
    else {
        if (!IS_ATOM_INT(_10384) && DBL_PTR(_10384)->dbl == 0.0){
            DeRef(_10384);
            _10384 = NOVALUE;
            goto L2; // [24] 44
        }
        DeRef(_10384);
        _10384 = NOVALUE;
    }
    DeRef(_10384);
    _10384 = NOVALUE;

    /** 			return {x - MIN1B}*/
    if (IS_ATOM_INT(_x_18055)) {
        _10385 = _x_18055 - -9;
        if ((long)((unsigned long)_10385 +(unsigned long) HIGH_BITS) >= 0){
            _10385 = NewDouble((double)_10385);
        }
    }
    else {
        _10385 = binary_op(MINUS, _x_18055, -9);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _10385;
    _10386 = MAKE_SEQ(_1);
    _10385 = NOVALUE;
    DeRef(_x_18055);
    DeRefi(_x4_18056);
    DeRef(_s_18057);
    DeRef(_10382);
    _10382 = NOVALUE;
    return _10386;
    goto L3; // [41] 328
L2: 

    /** 		elsif x >= MIN2B and x <= MAX2B then*/
    if (IS_ATOM_INT(_x_18055)) {
        _10387 = (_x_18055 >= _48MIN2B_17964);
    }
    else {
        _10387 = binary_op(GREATEREQ, _x_18055, _48MIN2B_17964);
    }
    if (IS_ATOM_INT(_10387)) {
        if (_10387 == 0) {
            goto L4; // [52] 97
        }
    }
    else {
        if (DBL_PTR(_10387)->dbl == 0.0) {
            goto L4; // [52] 97
        }
    }
    if (IS_ATOM_INT(_x_18055)) {
        _10389 = (_x_18055 <= 32767);
    }
    else {
        _10389 = binary_op(LESSEQ, _x_18055, 32767);
    }
    if (_10389 == 0) {
        DeRef(_10389);
        _10389 = NOVALUE;
        goto L4; // [63] 97
    }
    else {
        if (!IS_ATOM_INT(_10389) && DBL_PTR(_10389)->dbl == 0.0){
            DeRef(_10389);
            _10389 = NOVALUE;
            goto L4; // [63] 97
        }
        DeRef(_10389);
        _10389 = NOVALUE;
    }
    DeRef(_10389);
    _10389 = NOVALUE;

    /** 			x -= MIN2B*/
    _0 = _x_18055;
    if (IS_ATOM_INT(_x_18055)) {
        _x_18055 = _x_18055 - _48MIN2B_17964;
        if ((long)((unsigned long)_x_18055 +(unsigned long) HIGH_BITS) >= 0){
            _x_18055 = NewDouble((double)_x_18055);
        }
    }
    else {
        _x_18055 = binary_op(MINUS, _x_18055, _48MIN2B_17964);
    }
    DeRef(_0);

    /** 			return {I2B, and_bits(x, #FF), floor(x / #100)}*/
    if (IS_ATOM_INT(_x_18055)) {
        {unsigned long tu;
             tu = (unsigned long)_x_18055 & (unsigned long)255;
             _10391 = MAKE_UINT(tu);
        }
    }
    else {
        _10391 = binary_op(AND_BITS, _x_18055, 255);
    }
    if (IS_ATOM_INT(_x_18055)) {
        if (256 > 0 && _x_18055 >= 0) {
            _10392 = _x_18055 / 256;
        }
        else {
            temp_dbl = floor((double)_x_18055 / (double)256);
            if (_x_18055 != MININT)
            _10392 = (long)temp_dbl;
            else
            _10392 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_18055, 256);
        _10392 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 249;
    *((int *)(_2+8)) = _10391;
    *((int *)(_2+12)) = _10392;
    _10393 = MAKE_SEQ(_1);
    _10392 = NOVALUE;
    _10391 = NOVALUE;
    DeRef(_x_18055);
    DeRefi(_x4_18056);
    DeRef(_s_18057);
    DeRef(_10382);
    _10382 = NOVALUE;
    DeRef(_10386);
    _10386 = NOVALUE;
    DeRef(_10387);
    _10387 = NOVALUE;
    return _10393;
    goto L3; // [94] 328
L4: 

    /** 		elsif x >= MIN3B and x <= MAX3B then*/
    if (IS_ATOM_INT(_x_18055)) {
        _10394 = (_x_18055 >= _48MIN3B_17971);
    }
    else {
        _10394 = binary_op(GREATEREQ, _x_18055, _48MIN3B_17971);
    }
    if (IS_ATOM_INT(_10394)) {
        if (_10394 == 0) {
            goto L5; // [105] 159
        }
    }
    else {
        if (DBL_PTR(_10394)->dbl == 0.0) {
            goto L5; // [105] 159
        }
    }
    if (IS_ATOM_INT(_x_18055)) {
        _10396 = (_x_18055 <= 8388607);
    }
    else {
        _10396 = binary_op(LESSEQ, _x_18055, 8388607);
    }
    if (_10396 == 0) {
        DeRef(_10396);
        _10396 = NOVALUE;
        goto L5; // [116] 159
    }
    else {
        if (!IS_ATOM_INT(_10396) && DBL_PTR(_10396)->dbl == 0.0){
            DeRef(_10396);
            _10396 = NOVALUE;
            goto L5; // [116] 159
        }
        DeRef(_10396);
        _10396 = NOVALUE;
    }
    DeRef(_10396);
    _10396 = NOVALUE;

    /** 			x -= MIN3B*/
    _0 = _x_18055;
    if (IS_ATOM_INT(_x_18055)) {
        _x_18055 = _x_18055 - _48MIN3B_17971;
        if ((long)((unsigned long)_x_18055 +(unsigned long) HIGH_BITS) >= 0){
            _x_18055 = NewDouble((double)_x_18055);
        }
    }
    else {
        _x_18055 = binary_op(MINUS, _x_18055, _48MIN3B_17971);
    }
    DeRef(_0);

    /** 			return {I3B, and_bits(x, #FF), and_bits(floor(x / #100), #FF), floor(x / #10000)}*/
    if (IS_ATOM_INT(_x_18055)) {
        {unsigned long tu;
             tu = (unsigned long)_x_18055 & (unsigned long)255;
             _10398 = MAKE_UINT(tu);
        }
    }
    else {
        _10398 = binary_op(AND_BITS, _x_18055, 255);
    }
    if (IS_ATOM_INT(_x_18055)) {
        if (256 > 0 && _x_18055 >= 0) {
            _10399 = _x_18055 / 256;
        }
        else {
            temp_dbl = floor((double)_x_18055 / (double)256);
            if (_x_18055 != MININT)
            _10399 = (long)temp_dbl;
            else
            _10399 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_18055, 256);
        _10399 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    if (IS_ATOM_INT(_10399)) {
        {unsigned long tu;
             tu = (unsigned long)_10399 & (unsigned long)255;
             _10400 = MAKE_UINT(tu);
        }
    }
    else {
        _10400 = binary_op(AND_BITS, _10399, 255);
    }
    DeRef(_10399);
    _10399 = NOVALUE;
    if (IS_ATOM_INT(_x_18055)) {
        if (65536 > 0 && _x_18055 >= 0) {
            _10401 = _x_18055 / 65536;
        }
        else {
            temp_dbl = floor((double)_x_18055 / (double)65536);
            if (_x_18055 != MININT)
            _10401 = (long)temp_dbl;
            else
            _10401 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_18055, 65536);
        _10401 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 250;
    *((int *)(_2+8)) = _10398;
    *((int *)(_2+12)) = _10400;
    *((int *)(_2+16)) = _10401;
    _10402 = MAKE_SEQ(_1);
    _10401 = NOVALUE;
    _10400 = NOVALUE;
    _10398 = NOVALUE;
    DeRef(_x_18055);
    DeRefi(_x4_18056);
    DeRef(_s_18057);
    DeRef(_10382);
    _10382 = NOVALUE;
    DeRef(_10386);
    _10386 = NOVALUE;
    DeRef(_10387);
    _10387 = NOVALUE;
    DeRef(_10393);
    _10393 = NOVALUE;
    DeRef(_10394);
    _10394 = NOVALUE;
    return _10402;
    goto L3; // [156] 328
L5: 

    /** 			return I4B & convert:int_to_bytes(x-MIN4B)*/
    if (IS_ATOM_INT(_x_18055) && IS_ATOM_INT(_48MIN4B_17978)) {
        _10403 = _x_18055 - _48MIN4B_17978;
        if ((long)((unsigned long)_10403 +(unsigned long) HIGH_BITS) >= 0){
            _10403 = NewDouble((double)_10403);
        }
    }
    else {
        _10403 = binary_op(MINUS, _x_18055, _48MIN4B_17978);
    }
    _10404 = _6int_to_bytes(_10403);
    _10403 = NOVALUE;
    if (IS_SEQUENCE(251) && IS_ATOM(_10404)) {
    }
    else if (IS_ATOM(251) && IS_SEQUENCE(_10404)) {
        Prepend(&_10405, _10404, 251);
    }
    else {
        Concat((object_ptr)&_10405, 251, _10404);
    }
    DeRef(_10404);
    _10404 = NOVALUE;
    DeRef(_x_18055);
    DeRefi(_x4_18056);
    DeRef(_s_18057);
    DeRef(_10382);
    _10382 = NOVALUE;
    DeRef(_10386);
    _10386 = NOVALUE;
    DeRef(_10387);
    _10387 = NOVALUE;
    DeRef(_10393);
    _10393 = NOVALUE;
    DeRef(_10394);
    _10394 = NOVALUE;
    DeRef(_10402);
    _10402 = NOVALUE;
    return _10405;
    goto L3; // [180] 328
L1: 

    /** 	elsif atom(x) then*/
    _10406 = IS_ATOM(_x_18055);
    if (_10406 == 0)
    {
        _10406 = NOVALUE;
        goto L6; // [188] 249
    }
    else{
        _10406 = NOVALUE;
    }

    /** 		x4 = convert:atom_to_float32(x)*/

    /** 	return machine_func(M_A_TO_F32, a)*/
    DeRefi(_x4_18056);
    _x4_18056 = machine(48, _x_18055);

    /** 		if x = convert:float32_to_atom(x4) then*/

    /** 	return machine_func(M_F32_TO_A, ieee32)*/
    DeRef(_float32_to_atom_inlined_float32_to_atom_at_203_18094);
    _float32_to_atom_inlined_float32_to_atom_at_203_18094 = machine(49, _x4_18056);
    if (binary_op_a(NOTEQ, _x_18055, _float32_to_atom_inlined_float32_to_atom_at_203_18094)){
        goto L7; // [211] 228
    }

    /** 			return F4B & x4*/
    Prepend(&_10408, _x4_18056, 252);
    DeRef(_x_18055);
    DeRefDSi(_x4_18056);
    DeRef(_s_18057);
    DeRef(_10382);
    _10382 = NOVALUE;
    DeRef(_10386);
    _10386 = NOVALUE;
    DeRef(_10387);
    _10387 = NOVALUE;
    DeRef(_10393);
    _10393 = NOVALUE;
    DeRef(_10394);
    _10394 = NOVALUE;
    DeRef(_10402);
    _10402 = NOVALUE;
    DeRef(_10405);
    _10405 = NOVALUE;
    return _10408;
    goto L3; // [225] 328
L7: 

    /** 			return F8B & convert:atom_to_float64(x)*/

    /** 	return machine_func(M_A_TO_F64, a)*/
    DeRefi(_atom_to_float64_inlined_atom_to_float64_at_229_18099);
    _atom_to_float64_inlined_atom_to_float64_at_229_18099 = machine(46, _x_18055);
    Prepend(&_10409, _atom_to_float64_inlined_atom_to_float64_at_229_18099, 253);
    DeRef(_x_18055);
    DeRefi(_x4_18056);
    DeRef(_s_18057);
    DeRef(_10382);
    _10382 = NOVALUE;
    DeRef(_10386);
    _10386 = NOVALUE;
    DeRef(_10387);
    _10387 = NOVALUE;
    DeRef(_10393);
    _10393 = NOVALUE;
    DeRef(_10394);
    _10394 = NOVALUE;
    DeRef(_10402);
    _10402 = NOVALUE;
    DeRef(_10405);
    _10405 = NOVALUE;
    DeRef(_10408);
    _10408 = NOVALUE;
    return _10409;
    goto L3; // [246] 328
L6: 

    /** 		if length(x) <= 255 then*/
    if (IS_SEQUENCE(_x_18055)){
            _10410 = SEQ_PTR(_x_18055)->length;
    }
    else {
        _10410 = 1;
    }
    if (_10410 > 255)
    goto L8; // [254] 270

    /** 			s = {S1B, length(x)}*/
    if (IS_SEQUENCE(_x_18055)){
            _10412 = SEQ_PTR(_x_18055)->length;
    }
    else {
        _10412 = 1;
    }
    DeRef(_s_18057);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 254;
    ((int *)_2)[2] = _10412;
    _s_18057 = MAKE_SEQ(_1);
    _10412 = NOVALUE;
    goto L9; // [267] 284
L8: 

    /** 			s = S4B & convert:int_to_bytes(length(x))*/
    if (IS_SEQUENCE(_x_18055)){
            _10414 = SEQ_PTR(_x_18055)->length;
    }
    else {
        _10414 = 1;
    }
    _10415 = _6int_to_bytes(_10414);
    _10414 = NOVALUE;
    if (IS_SEQUENCE(255) && IS_ATOM(_10415)) {
    }
    else if (IS_ATOM(255) && IS_SEQUENCE(_10415)) {
        Prepend(&_s_18057, _10415, 255);
    }
    else {
        Concat((object_ptr)&_s_18057, 255, _10415);
    }
    DeRef(_10415);
    _10415 = NOVALUE;
L9: 

    /** 		for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_18055)){
            _10417 = SEQ_PTR(_x_18055)->length;
    }
    else {
        _10417 = 1;
    }
    {
        int _i_18112;
        _i_18112 = 1;
LA: 
        if (_i_18112 > _10417){
            goto LB; // [289] 319
        }

        /** 			s &= compress(x[i])*/
        _2 = (int)SEQ_PTR(_x_18055);
        _10418 = (int)*(((s1_ptr)_2)->base + _i_18112);
        Ref(_10418);
        _10419 = _48compress(_10418);
        _10418 = NOVALUE;
        if (IS_SEQUENCE(_s_18057) && IS_ATOM(_10419)) {
            Ref(_10419);
            Append(&_s_18057, _s_18057, _10419);
        }
        else if (IS_ATOM(_s_18057) && IS_SEQUENCE(_10419)) {
        }
        else {
            Concat((object_ptr)&_s_18057, _s_18057, _10419);
        }
        DeRef(_10419);
        _10419 = NOVALUE;

        /** 		end for*/
        _i_18112 = _i_18112 + 1;
        goto LA; // [314] 296
LB: 
        ;
    }

    /** 		return s*/
    DeRef(_x_18055);
    DeRefi(_x4_18056);
    DeRef(_10382);
    _10382 = NOVALUE;
    DeRef(_10386);
    _10386 = NOVALUE;
    DeRef(_10387);
    _10387 = NOVALUE;
    DeRef(_10393);
    _10393 = NOVALUE;
    DeRef(_10394);
    _10394 = NOVALUE;
    DeRef(_10402);
    _10402 = NOVALUE;
    DeRef(_10405);
    _10405 = NOVALUE;
    DeRef(_10408);
    _10408 = NOVALUE;
    DeRef(_10409);
    _10409 = NOVALUE;
    return _s_18057;
L3: 
    ;
}


int _48db_allocate(int _n_18496)
{
    int _free_list_18497 = NOVALUE;
    int _size_18498 = NOVALUE;
    int _size_ptr_18499 = NOVALUE;
    int _addr_18500 = NOVALUE;
    int _free_count_18501 = NOVALUE;
    int _remaining_18502 = NOVALUE;
    int _seek_1__tmp_at4_18505 = NOVALUE;
    int _seek_inlined_seek_at_4_18504 = NOVALUE;
    int _seek_1__tmp_at39_18512 = NOVALUE;
    int _seek_inlined_seek_at_39_18511 = NOVALUE;
    int _seek_1__tmp_at111_18529 = NOVALUE;
    int _seek_inlined_seek_at_111_18528 = NOVALUE;
    int _pos_inlined_seek_at_108_18527 = NOVALUE;
    int _put4_1__tmp_at137_18534 = NOVALUE;
    int _x_inlined_put4_at_134_18533 = NOVALUE;
    int _seek_1__tmp_at167_18537 = NOVALUE;
    int _seek_inlined_seek_at_167_18536 = NOVALUE;
    int _put4_1__tmp_at193_18542 = NOVALUE;
    int _x_inlined_put4_at_190_18541 = NOVALUE;
    int _seek_1__tmp_at244_18550 = NOVALUE;
    int _seek_inlined_seek_at_244_18549 = NOVALUE;
    int _pos_inlined_seek_at_241_18548 = NOVALUE;
    int _put4_1__tmp_at266_18554 = NOVALUE;
    int _x_inlined_put4_at_263_18553 = NOVALUE;
    int _seek_1__tmp_at333_18565 = NOVALUE;
    int _seek_inlined_seek_at_333_18564 = NOVALUE;
    int _pos_inlined_seek_at_330_18563 = NOVALUE;
    int _seek_1__tmp_at364_18569 = NOVALUE;
    int _seek_inlined_seek_at_364_18568 = NOVALUE;
    int _put4_1__tmp_at386_18573 = NOVALUE;
    int _x_inlined_put4_at_383_18572 = NOVALUE;
    int _seek_1__tmp_at423_18578 = NOVALUE;
    int _seek_inlined_seek_at_423_18577 = NOVALUE;
    int _pos_inlined_seek_at_420_18576 = NOVALUE;
    int _put4_1__tmp_at438_18580 = NOVALUE;
    int _seek_1__tmp_at490_18584 = NOVALUE;
    int _seek_inlined_seek_at_490_18583 = NOVALUE;
    int _put4_1__tmp_at512_18588 = NOVALUE;
    int _x_inlined_put4_at_509_18587 = NOVALUE;
    int _where_inlined_where_at_542_18590 = NOVALUE;
    int _10631 = NOVALUE;
    int _10629 = NOVALUE;
    int _10628 = NOVALUE;
    int _10627 = NOVALUE;
    int _10626 = NOVALUE;
    int _10625 = NOVALUE;
    int _10623 = NOVALUE;
    int _10622 = NOVALUE;
    int _10621 = NOVALUE;
    int _10620 = NOVALUE;
    int _10618 = NOVALUE;
    int _10617 = NOVALUE;
    int _10616 = NOVALUE;
    int _10615 = NOVALUE;
    int _10614 = NOVALUE;
    int _10613 = NOVALUE;
    int _10612 = NOVALUE;
    int _10610 = NOVALUE;
    int _10608 = NOVALUE;
    int _10605 = NOVALUE;
    int _0, _1, _2;
    

    /** 	io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at4_18505);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at4_18505 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_4_18504 = machine(19, _seek_1__tmp_at4_18505);
    DeRefi(_seek_1__tmp_at4_18505);
    _seek_1__tmp_at4_18505 = NOVALUE;

    /** 	free_count = get4()*/
    _free_count_18501 = _48get4();
    if (!IS_ATOM_INT(_free_count_18501)) {
        _1 = (long)(DBL_PTR(_free_count_18501)->dbl);
        if (UNIQUE(DBL_PTR(_free_count_18501)) && (DBL_PTR(_free_count_18501)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_free_count_18501);
        _free_count_18501 = _1;
    }

    /** 	if free_count > 0 then*/
    if (_free_count_18501 <= 0)
    goto L1; // [27] 487

    /** 		free_list = get4()*/
    _0 = _free_list_18497;
    _free_list_18497 = _48get4();
    DeRef(_0);

    /** 		io:seek(current_db, free_list)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_free_list_18497);
    DeRef(_seek_1__tmp_at39_18512);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _free_list_18497;
    _seek_1__tmp_at39_18512 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_39_18511 = machine(19, _seek_1__tmp_at39_18512);
    DeRef(_seek_1__tmp_at39_18512);
    _seek_1__tmp_at39_18512 = NOVALUE;

    /** 		size_ptr = free_list + 4*/
    DeRef(_size_ptr_18499);
    if (IS_ATOM_INT(_free_list_18497)) {
        _size_ptr_18499 = _free_list_18497 + 4;
        if ((long)((unsigned long)_size_ptr_18499 + (unsigned long)HIGH_BITS) >= 0) 
        _size_ptr_18499 = NewDouble((double)_size_ptr_18499);
    }
    else {
        _size_ptr_18499 = NewDouble(DBL_PTR(_free_list_18497)->dbl + (double)4);
    }

    /** 		for i = 1 to free_count do*/
    _10605 = _free_count_18501;
    {
        int _i_18515;
        _i_18515 = 1;
L2: 
        if (_i_18515 > _10605){
            goto L3; // [64] 486
        }

        /** 			addr = get4()*/
        _0 = _addr_18500;
        _addr_18500 = _48get4();
        DeRef(_0);

        /** 			size = get4()*/
        _0 = _size_18498;
        _size_18498 = _48get4();
        DeRef(_0);

        /** 			if size >= n+4 then*/
        if (IS_ATOM_INT(_n_18496)) {
            _10608 = _n_18496 + 4;
            if ((long)((unsigned long)_10608 + (unsigned long)HIGH_BITS) >= 0) 
            _10608 = NewDouble((double)_10608);
        }
        else {
            _10608 = NewDouble(DBL_PTR(_n_18496)->dbl + (double)4);
        }
        if (binary_op_a(LESS, _size_18498, _10608)){
            DeRef(_10608);
            _10608 = NOVALUE;
            goto L4; // [87] 473
        }
        DeRef(_10608);
        _10608 = NOVALUE;

        /** 				if size >= n+16 then*/
        if (IS_ATOM_INT(_n_18496)) {
            _10610 = _n_18496 + 16;
            if ((long)((unsigned long)_10610 + (unsigned long)HIGH_BITS) >= 0) 
            _10610 = NewDouble((double)_10610);
        }
        else {
            _10610 = NewDouble(DBL_PTR(_n_18496)->dbl + (double)16);
        }
        if (binary_op_a(LESS, _size_18498, _10610)){
            DeRef(_10610);
            _10610 = NOVALUE;
            goto L5; // [97] 296
        }
        DeRef(_10610);
        _10610 = NOVALUE;

        /** 					io:seek(current_db, addr - 4)*/
        if (IS_ATOM_INT(_addr_18500)) {
            _10612 = _addr_18500 - 4;
            if ((long)((unsigned long)_10612 +(unsigned long) HIGH_BITS) >= 0){
                _10612 = NewDouble((double)_10612);
            }
        }
        else {
            _10612 = NewDouble(DBL_PTR(_addr_18500)->dbl - (double)4);
        }
        DeRef(_pos_inlined_seek_at_108_18527);
        _pos_inlined_seek_at_108_18527 = _10612;
        _10612 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_108_18527);
        DeRef(_seek_1__tmp_at111_18529);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _48current_db_17843;
        ((int *)_2)[2] = _pos_inlined_seek_at_108_18527;
        _seek_1__tmp_at111_18529 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_111_18528 = machine(19, _seek_1__tmp_at111_18529);
        DeRef(_pos_inlined_seek_at_108_18527);
        _pos_inlined_seek_at_108_18527 = NOVALUE;
        DeRef(_seek_1__tmp_at111_18529);
        _seek_1__tmp_at111_18529 = NOVALUE;

        /** 					put4(size-n-4) -- shrink the block*/
        if (IS_ATOM_INT(_size_18498) && IS_ATOM_INT(_n_18496)) {
            _10613 = _size_18498 - _n_18496;
            if ((long)((unsigned long)_10613 +(unsigned long) HIGH_BITS) >= 0){
                _10613 = NewDouble((double)_10613);
            }
        }
        else {
            if (IS_ATOM_INT(_size_18498)) {
                _10613 = NewDouble((double)_size_18498 - DBL_PTR(_n_18496)->dbl);
            }
            else {
                if (IS_ATOM_INT(_n_18496)) {
                    _10613 = NewDouble(DBL_PTR(_size_18498)->dbl - (double)_n_18496);
                }
                else
                _10613 = NewDouble(DBL_PTR(_size_18498)->dbl - DBL_PTR(_n_18496)->dbl);
            }
        }
        if (IS_ATOM_INT(_10613)) {
            _10614 = _10613 - 4;
            if ((long)((unsigned long)_10614 +(unsigned long) HIGH_BITS) >= 0){
                _10614 = NewDouble((double)_10614);
            }
        }
        else {
            _10614 = NewDouble(DBL_PTR(_10613)->dbl - (double)4);
        }
        DeRef(_10613);
        _10613 = NOVALUE;
        DeRef(_x_inlined_put4_at_134_18533);
        _x_inlined_put4_at_134_18533 = _10614;
        _10614 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_48mem0_17885)){
            poke4_addr = (unsigned long *)_48mem0_17885;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_134_18533)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_134_18533;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_134_18533)->dbl;
            *poke4_addr = (unsigned long)_1;
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at137_18534);
        _1 = (int)SEQ_PTR(_48memseq_18120);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at137_18534 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_48current_db_17843, _put4_1__tmp_at137_18534); // DJP 

        /** end procedure*/
        goto L6; // [159] 162
L6: 
        DeRef(_x_inlined_put4_at_134_18533);
        _x_inlined_put4_at_134_18533 = NOVALUE;
        DeRefi(_put4_1__tmp_at137_18534);
        _put4_1__tmp_at137_18534 = NOVALUE;

        /** 					io:seek(current_db, size_ptr)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_size_ptr_18499);
        DeRef(_seek_1__tmp_at167_18537);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _48current_db_17843;
        ((int *)_2)[2] = _size_ptr_18499;
        _seek_1__tmp_at167_18537 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_167_18536 = machine(19, _seek_1__tmp_at167_18537);
        DeRef(_seek_1__tmp_at167_18537);
        _seek_1__tmp_at167_18537 = NOVALUE;

        /** 					put4(size-n-4) -- update size on free list too*/
        if (IS_ATOM_INT(_size_18498) && IS_ATOM_INT(_n_18496)) {
            _10615 = _size_18498 - _n_18496;
            if ((long)((unsigned long)_10615 +(unsigned long) HIGH_BITS) >= 0){
                _10615 = NewDouble((double)_10615);
            }
        }
        else {
            if (IS_ATOM_INT(_size_18498)) {
                _10615 = NewDouble((double)_size_18498 - DBL_PTR(_n_18496)->dbl);
            }
            else {
                if (IS_ATOM_INT(_n_18496)) {
                    _10615 = NewDouble(DBL_PTR(_size_18498)->dbl - (double)_n_18496);
                }
                else
                _10615 = NewDouble(DBL_PTR(_size_18498)->dbl - DBL_PTR(_n_18496)->dbl);
            }
        }
        if (IS_ATOM_INT(_10615)) {
            _10616 = _10615 - 4;
            if ((long)((unsigned long)_10616 +(unsigned long) HIGH_BITS) >= 0){
                _10616 = NewDouble((double)_10616);
            }
        }
        else {
            _10616 = NewDouble(DBL_PTR(_10615)->dbl - (double)4);
        }
        DeRef(_10615);
        _10615 = NOVALUE;
        DeRef(_x_inlined_put4_at_190_18541);
        _x_inlined_put4_at_190_18541 = _10616;
        _10616 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_48mem0_17885)){
            poke4_addr = (unsigned long *)_48mem0_17885;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_190_18541)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_190_18541;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_190_18541)->dbl;
            *poke4_addr = (unsigned long)_1;
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at193_18542);
        _1 = (int)SEQ_PTR(_48memseq_18120);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at193_18542 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_48current_db_17843, _put4_1__tmp_at193_18542); // DJP 

        /** end procedure*/
        goto L7; // [215] 218
L7: 
        DeRef(_x_inlined_put4_at_190_18541);
        _x_inlined_put4_at_190_18541 = NOVALUE;
        DeRefi(_put4_1__tmp_at193_18542);
        _put4_1__tmp_at193_18542 = NOVALUE;

        /** 					addr += size-n-4*/
        if (IS_ATOM_INT(_size_18498) && IS_ATOM_INT(_n_18496)) {
            _10617 = _size_18498 - _n_18496;
            if ((long)((unsigned long)_10617 +(unsigned long) HIGH_BITS) >= 0){
                _10617 = NewDouble((double)_10617);
            }
        }
        else {
            if (IS_ATOM_INT(_size_18498)) {
                _10617 = NewDouble((double)_size_18498 - DBL_PTR(_n_18496)->dbl);
            }
            else {
                if (IS_ATOM_INT(_n_18496)) {
                    _10617 = NewDouble(DBL_PTR(_size_18498)->dbl - (double)_n_18496);
                }
                else
                _10617 = NewDouble(DBL_PTR(_size_18498)->dbl - DBL_PTR(_n_18496)->dbl);
            }
        }
        if (IS_ATOM_INT(_10617)) {
            _10618 = _10617 - 4;
            if ((long)((unsigned long)_10618 +(unsigned long) HIGH_BITS) >= 0){
                _10618 = NewDouble((double)_10618);
            }
        }
        else {
            _10618 = NewDouble(DBL_PTR(_10617)->dbl - (double)4);
        }
        DeRef(_10617);
        _10617 = NOVALUE;
        _0 = _addr_18500;
        if (IS_ATOM_INT(_addr_18500) && IS_ATOM_INT(_10618)) {
            _addr_18500 = _addr_18500 + _10618;
            if ((long)((unsigned long)_addr_18500 + (unsigned long)HIGH_BITS) >= 0) 
            _addr_18500 = NewDouble((double)_addr_18500);
        }
        else {
            if (IS_ATOM_INT(_addr_18500)) {
                _addr_18500 = NewDouble((double)_addr_18500 + DBL_PTR(_10618)->dbl);
            }
            else {
                if (IS_ATOM_INT(_10618)) {
                    _addr_18500 = NewDouble(DBL_PTR(_addr_18500)->dbl + (double)_10618);
                }
                else
                _addr_18500 = NewDouble(DBL_PTR(_addr_18500)->dbl + DBL_PTR(_10618)->dbl);
            }
        }
        DeRef(_0);
        DeRef(_10618);
        _10618 = NOVALUE;

        /** 					io:seek(current_db, addr - 4) */
        if (IS_ATOM_INT(_addr_18500)) {
            _10620 = _addr_18500 - 4;
            if ((long)((unsigned long)_10620 +(unsigned long) HIGH_BITS) >= 0){
                _10620 = NewDouble((double)_10620);
            }
        }
        else {
            _10620 = NewDouble(DBL_PTR(_addr_18500)->dbl - (double)4);
        }
        DeRef(_pos_inlined_seek_at_241_18548);
        _pos_inlined_seek_at_241_18548 = _10620;
        _10620 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_241_18548);
        DeRef(_seek_1__tmp_at244_18550);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _48current_db_17843;
        ((int *)_2)[2] = _pos_inlined_seek_at_241_18548;
        _seek_1__tmp_at244_18550 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_244_18549 = machine(19, _seek_1__tmp_at244_18550);
        DeRef(_pos_inlined_seek_at_241_18548);
        _pos_inlined_seek_at_241_18548 = NOVALUE;
        DeRef(_seek_1__tmp_at244_18550);
        _seek_1__tmp_at244_18550 = NOVALUE;

        /** 					put4(n+4)*/
        if (IS_ATOM_INT(_n_18496)) {
            _10621 = _n_18496 + 4;
            if ((long)((unsigned long)_10621 + (unsigned long)HIGH_BITS) >= 0) 
            _10621 = NewDouble((double)_10621);
        }
        else {
            _10621 = NewDouble(DBL_PTR(_n_18496)->dbl + (double)4);
        }
        DeRef(_x_inlined_put4_at_263_18553);
        _x_inlined_put4_at_263_18553 = _10621;
        _10621 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_48mem0_17885)){
            poke4_addr = (unsigned long *)_48mem0_17885;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_263_18553)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_263_18553;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_263_18553)->dbl;
            *poke4_addr = (unsigned long)_1;
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at266_18554);
        _1 = (int)SEQ_PTR(_48memseq_18120);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at266_18554 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_48current_db_17843, _put4_1__tmp_at266_18554); // DJP 

        /** end procedure*/
        goto L8; // [288] 291
L8: 
        DeRef(_x_inlined_put4_at_263_18553);
        _x_inlined_put4_at_263_18553 = NOVALUE;
        DeRefi(_put4_1__tmp_at266_18554);
        _put4_1__tmp_at266_18554 = NOVALUE;
        goto L9; // [293] 466
L5: 

        /** 					remaining = io:get_bytes(current_db, (free_count-i) * 8)*/
        _10622 = _free_count_18501 - _i_18515;
        if ((long)((unsigned long)_10622 +(unsigned long) HIGH_BITS) >= 0){
            _10622 = NewDouble((double)_10622);
        }
        if (IS_ATOM_INT(_10622)) {
            if (_10622 == (short)_10622)
            _10623 = _10622 * 8;
            else
            _10623 = NewDouble(_10622 * (double)8);
        }
        else {
            _10623 = NewDouble(DBL_PTR(_10622)->dbl * (double)8);
        }
        DeRef(_10622);
        _10622 = NOVALUE;
        _0 = _remaining_18502;
        _remaining_18502 = _16get_bytes(_48current_db_17843, _10623);
        DeRef(_0);
        _10623 = NOVALUE;

        /** 					io:seek(current_db, free_list+8*(i-1))*/
        _10625 = _i_18515 - 1;
        if (_10625 <= INT15)
        _10626 = 8 * _10625;
        else
        _10626 = NewDouble(8 * (double)_10625);
        _10625 = NOVALUE;
        if (IS_ATOM_INT(_free_list_18497) && IS_ATOM_INT(_10626)) {
            _10627 = _free_list_18497 + _10626;
            if ((long)((unsigned long)_10627 + (unsigned long)HIGH_BITS) >= 0) 
            _10627 = NewDouble((double)_10627);
        }
        else {
            if (IS_ATOM_INT(_free_list_18497)) {
                _10627 = NewDouble((double)_free_list_18497 + DBL_PTR(_10626)->dbl);
            }
            else {
                if (IS_ATOM_INT(_10626)) {
                    _10627 = NewDouble(DBL_PTR(_free_list_18497)->dbl + (double)_10626);
                }
                else
                _10627 = NewDouble(DBL_PTR(_free_list_18497)->dbl + DBL_PTR(_10626)->dbl);
            }
        }
        DeRef(_10626);
        _10626 = NOVALUE;
        DeRef(_pos_inlined_seek_at_330_18563);
        _pos_inlined_seek_at_330_18563 = _10627;
        _10627 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_330_18563);
        DeRef(_seek_1__tmp_at333_18565);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _48current_db_17843;
        ((int *)_2)[2] = _pos_inlined_seek_at_330_18563;
        _seek_1__tmp_at333_18565 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_333_18564 = machine(19, _seek_1__tmp_at333_18565);
        DeRef(_pos_inlined_seek_at_330_18563);
        _pos_inlined_seek_at_330_18563 = NOVALUE;
        DeRef(_seek_1__tmp_at333_18565);
        _seek_1__tmp_at333_18565 = NOVALUE;

        /** 					putn(remaining)*/

        /** 	puts(current_db, s)*/
        EPuts(_48current_db_17843, _remaining_18502); // DJP 

        /** end procedure*/
        goto LA; // [358] 361
LA: 

        /** 					io:seek(current_db, FREE_COUNT)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        DeRefi(_seek_1__tmp_at364_18569);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _48current_db_17843;
        ((int *)_2)[2] = 7;
        _seek_1__tmp_at364_18569 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_364_18568 = machine(19, _seek_1__tmp_at364_18569);
        DeRefi(_seek_1__tmp_at364_18569);
        _seek_1__tmp_at364_18569 = NOVALUE;

        /** 					put4(free_count-1)*/
        _10628 = _free_count_18501 - 1;
        if ((long)((unsigned long)_10628 +(unsigned long) HIGH_BITS) >= 0){
            _10628 = NewDouble((double)_10628);
        }
        DeRef(_x_inlined_put4_at_383_18572);
        _x_inlined_put4_at_383_18572 = _10628;
        _10628 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_48mem0_17885)){
            poke4_addr = (unsigned long *)_48mem0_17885;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_383_18572)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_383_18572;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_383_18572)->dbl;
            *poke4_addr = (unsigned long)_1;
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at386_18573);
        _1 = (int)SEQ_PTR(_48memseq_18120);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at386_18573 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_48current_db_17843, _put4_1__tmp_at386_18573); // DJP 

        /** end procedure*/
        goto LB; // [408] 411
LB: 
        DeRef(_x_inlined_put4_at_383_18572);
        _x_inlined_put4_at_383_18572 = NOVALUE;
        DeRefi(_put4_1__tmp_at386_18573);
        _put4_1__tmp_at386_18573 = NOVALUE;

        /** 					io:seek(current_db, addr - 4)*/
        if (IS_ATOM_INT(_addr_18500)) {
            _10629 = _addr_18500 - 4;
            if ((long)((unsigned long)_10629 +(unsigned long) HIGH_BITS) >= 0){
                _10629 = NewDouble((double)_10629);
            }
        }
        else {
            _10629 = NewDouble(DBL_PTR(_addr_18500)->dbl - (double)4);
        }
        DeRef(_pos_inlined_seek_at_420_18576);
        _pos_inlined_seek_at_420_18576 = _10629;
        _10629 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_420_18576);
        DeRef(_seek_1__tmp_at423_18578);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _48current_db_17843;
        ((int *)_2)[2] = _pos_inlined_seek_at_420_18576;
        _seek_1__tmp_at423_18578 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_423_18577 = machine(19, _seek_1__tmp_at423_18578);
        DeRef(_pos_inlined_seek_at_420_18576);
        _pos_inlined_seek_at_420_18576 = NOVALUE;
        DeRef(_seek_1__tmp_at423_18578);
        _seek_1__tmp_at423_18578 = NOVALUE;

        /** 					put4(size) -- in case size was not updated by db_free()*/

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_48mem0_17885)){
            poke4_addr = (unsigned long *)_48mem0_17885;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
        }
        if (IS_ATOM_INT(_size_18498)) {
            *poke4_addr = (unsigned long)_size_18498;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_size_18498)->dbl;
            *poke4_addr = (unsigned long)_1;
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at438_18580);
        _1 = (int)SEQ_PTR(_48memseq_18120);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at438_18580 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_48current_db_17843, _put4_1__tmp_at438_18580); // DJP 

        /** end procedure*/
        goto LC; // [460] 463
LC: 
        DeRefi(_put4_1__tmp_at438_18580);
        _put4_1__tmp_at438_18580 = NOVALUE;
L9: 

        /** 				return addr*/
        DeRef(_n_18496);
        DeRef(_free_list_18497);
        DeRef(_size_18498);
        DeRef(_size_ptr_18499);
        DeRef(_remaining_18502);
        return _addr_18500;
L4: 

        /** 			size_ptr += 8*/
        _0 = _size_ptr_18499;
        if (IS_ATOM_INT(_size_ptr_18499)) {
            _size_ptr_18499 = _size_ptr_18499 + 8;
            if ((long)((unsigned long)_size_ptr_18499 + (unsigned long)HIGH_BITS) >= 0) 
            _size_ptr_18499 = NewDouble((double)_size_ptr_18499);
        }
        else {
            _size_ptr_18499 = NewDouble(DBL_PTR(_size_ptr_18499)->dbl + (double)8);
        }
        DeRef(_0);

        /** 		end for*/
        _i_18515 = _i_18515 + 1;
        goto L2; // [481] 71
L3: 
        ;
    }
L1: 

    /** 	io:seek(current_db, -1)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at490_18584);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = -1;
    _seek_1__tmp_at490_18584 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_490_18583 = machine(19, _seek_1__tmp_at490_18584);
    DeRefi(_seek_1__tmp_at490_18584);
    _seek_1__tmp_at490_18584 = NOVALUE;

    /** 	put4(n+4)*/
    if (IS_ATOM_INT(_n_18496)) {
        _10631 = _n_18496 + 4;
        if ((long)((unsigned long)_10631 + (unsigned long)HIGH_BITS) >= 0) 
        _10631 = NewDouble((double)_10631);
    }
    else {
        _10631 = NewDouble(DBL_PTR(_n_18496)->dbl + (double)4);
    }
    DeRef(_x_inlined_put4_at_509_18587);
    _x_inlined_put4_at_509_18587 = _10631;
    _10631 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_509_18587)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_509_18587;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_509_18587)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at512_18588);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at512_18588 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at512_18588); // DJP 

    /** end procedure*/
    goto LD; // [534] 537
LD: 
    DeRef(_x_inlined_put4_at_509_18587);
    _x_inlined_put4_at_509_18587 = NOVALUE;
    DeRefi(_put4_1__tmp_at512_18588);
    _put4_1__tmp_at512_18588 = NOVALUE;

    /** 	return io:where(current_db)*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_542_18590);
    _where_inlined_where_at_542_18590 = machine(20, _48current_db_17843);
    DeRef(_n_18496);
    DeRef(_free_list_18497);
    DeRef(_size_18498);
    DeRef(_size_ptr_18499);
    DeRef(_addr_18500);
    DeRef(_remaining_18502);
    return _where_inlined_where_at_542_18590;
    ;
}


void _48db_free(int _p_18593)
{
    int _psize_18594 = NOVALUE;
    int _i_18595 = NOVALUE;
    int _size_18596 = NOVALUE;
    int _addr_18597 = NOVALUE;
    int _free_list_18598 = NOVALUE;
    int _free_list_space_18599 = NOVALUE;
    int _new_space_18600 = NOVALUE;
    int _to_be_freed_18601 = NOVALUE;
    int _prev_addr_18602 = NOVALUE;
    int _prev_size_18603 = NOVALUE;
    int _free_count_18604 = NOVALUE;
    int _remaining_18605 = NOVALUE;
    int _seek_1__tmp_at11_18610 = NOVALUE;
    int _seek_inlined_seek_at_11_18609 = NOVALUE;
    int _pos_inlined_seek_at_8_18608 = NOVALUE;
    int _seek_1__tmp_at33_18614 = NOVALUE;
    int _seek_inlined_seek_at_33_18613 = NOVALUE;
    int _seek_1__tmp_at69_18621 = NOVALUE;
    int _seek_inlined_seek_at_69_18620 = NOVALUE;
    int _pos_inlined_seek_at_66_18619 = NOVALUE;
    int _seek_1__tmp_at133_18634 = NOVALUE;
    int _seek_inlined_seek_at_133_18633 = NOVALUE;
    int _seek_1__tmp_at157_18638 = NOVALUE;
    int _seek_inlined_seek_at_157_18637 = NOVALUE;
    int _put4_1__tmp_at172_18640 = NOVALUE;
    int _seek_1__tmp_at202_18643 = NOVALUE;
    int _seek_inlined_seek_at_202_18642 = NOVALUE;
    int _seek_1__tmp_at234_18648 = NOVALUE;
    int _seek_inlined_seek_at_234_18647 = NOVALUE;
    int _s_inlined_putn_at_274_18654 = NOVALUE;
    int _seek_1__tmp_at297_18657 = NOVALUE;
    int _seek_inlined_seek_at_297_18656 = NOVALUE;
    int _seek_1__tmp_at430_18678 = NOVALUE;
    int _seek_inlined_seek_at_430_18677 = NOVALUE;
    int _pos_inlined_seek_at_427_18676 = NOVALUE;
    int _put4_1__tmp_at482_18688 = NOVALUE;
    int _x_inlined_put4_at_479_18687 = NOVALUE;
    int _seek_1__tmp_at523_18694 = NOVALUE;
    int _seek_inlined_seek_at_523_18693 = NOVALUE;
    int _pos_inlined_seek_at_520_18692 = NOVALUE;
    int _seek_1__tmp_at574_18704 = NOVALUE;
    int _seek_inlined_seek_at_574_18703 = NOVALUE;
    int _pos_inlined_seek_at_571_18702 = NOVALUE;
    int _seek_1__tmp_at611_18709 = NOVALUE;
    int _seek_inlined_seek_at_611_18708 = NOVALUE;
    int _put4_1__tmp_at626_18711 = NOVALUE;
    int _put4_1__tmp_at664_18716 = NOVALUE;
    int _x_inlined_put4_at_661_18715 = NOVALUE;
    int _seek_1__tmp_at737_18728 = NOVALUE;
    int _seek_inlined_seek_at_737_18727 = NOVALUE;
    int _pos_inlined_seek_at_734_18726 = NOVALUE;
    int _put4_1__tmp_at752_18730 = NOVALUE;
    int _put4_1__tmp_at789_18734 = NOVALUE;
    int _x_inlined_put4_at_786_18733 = NOVALUE;
    int _seek_1__tmp_at837_18742 = NOVALUE;
    int _seek_inlined_seek_at_837_18741 = NOVALUE;
    int _pos_inlined_seek_at_834_18740 = NOVALUE;
    int _seek_1__tmp_at883_18750 = NOVALUE;
    int _seek_inlined_seek_at_883_18749 = NOVALUE;
    int _put4_1__tmp_at898_18752 = NOVALUE;
    int _seek_1__tmp_at943_18759 = NOVALUE;
    int _seek_inlined_seek_at_943_18758 = NOVALUE;
    int _pos_inlined_seek_at_940_18757 = NOVALUE;
    int _put4_1__tmp_at958_18761 = NOVALUE;
    int _put4_1__tmp_at986_18763 = NOVALUE;
    int _10700 = NOVALUE;
    int _10699 = NOVALUE;
    int _10698 = NOVALUE;
    int _10697 = NOVALUE;
    int _10694 = NOVALUE;
    int _10693 = NOVALUE;
    int _10692 = NOVALUE;
    int _10691 = NOVALUE;
    int _10690 = NOVALUE;
    int _10689 = NOVALUE;
    int _10688 = NOVALUE;
    int _10687 = NOVALUE;
    int _10686 = NOVALUE;
    int _10685 = NOVALUE;
    int _10684 = NOVALUE;
    int _10683 = NOVALUE;
    int _10682 = NOVALUE;
    int _10681 = NOVALUE;
    int _10680 = NOVALUE;
    int _10678 = NOVALUE;
    int _10677 = NOVALUE;
    int _10676 = NOVALUE;
    int _10674 = NOVALUE;
    int _10673 = NOVALUE;
    int _10672 = NOVALUE;
    int _10671 = NOVALUE;
    int _10670 = NOVALUE;
    int _10669 = NOVALUE;
    int _10668 = NOVALUE;
    int _10667 = NOVALUE;
    int _10666 = NOVALUE;
    int _10665 = NOVALUE;
    int _10664 = NOVALUE;
    int _10663 = NOVALUE;
    int _10662 = NOVALUE;
    int _10661 = NOVALUE;
    int _10660 = NOVALUE;
    int _10659 = NOVALUE;
    int _10658 = NOVALUE;
    int _10657 = NOVALUE;
    int _10651 = NOVALUE;
    int _10650 = NOVALUE;
    int _10649 = NOVALUE;
    int _10647 = NOVALUE;
    int _10643 = NOVALUE;
    int _10642 = NOVALUE;
    int _10640 = NOVALUE;
    int _10639 = NOVALUE;
    int _10637 = NOVALUE;
    int _10636 = NOVALUE;
    int _10632 = NOVALUE;
    int _0, _1, _2;
    

    /** 	io:seek(current_db, p-4)*/
    if (IS_ATOM_INT(_p_18593)) {
        _10632 = _p_18593 - 4;
        if ((long)((unsigned long)_10632 +(unsigned long) HIGH_BITS) >= 0){
            _10632 = NewDouble((double)_10632);
        }
    }
    else {
        _10632 = NewDouble(DBL_PTR(_p_18593)->dbl - (double)4);
    }
    DeRef(_pos_inlined_seek_at_8_18608);
    _pos_inlined_seek_at_8_18608 = _10632;
    _10632 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_8_18608);
    DeRef(_seek_1__tmp_at11_18610);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_8_18608;
    _seek_1__tmp_at11_18610 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_11_18609 = machine(19, _seek_1__tmp_at11_18610);
    DeRef(_pos_inlined_seek_at_8_18608);
    _pos_inlined_seek_at_8_18608 = NOVALUE;
    DeRef(_seek_1__tmp_at11_18610);
    _seek_1__tmp_at11_18610 = NOVALUE;

    /** 	psize = get4()*/
    _0 = _psize_18594;
    _psize_18594 = _48get4();
    DeRef(_0);

    /** 	io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at33_18614);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at33_18614 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_33_18613 = machine(19, _seek_1__tmp_at33_18614);
    DeRefi(_seek_1__tmp_at33_18614);
    _seek_1__tmp_at33_18614 = NOVALUE;

    /** 	free_count = get4()*/
    _free_count_18604 = _48get4();
    if (!IS_ATOM_INT(_free_count_18604)) {
        _1 = (long)(DBL_PTR(_free_count_18604)->dbl);
        if (UNIQUE(DBL_PTR(_free_count_18604)) && (DBL_PTR(_free_count_18604)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_free_count_18604);
        _free_count_18604 = _1;
    }

    /** 	free_list = get4()*/
    _0 = _free_list_18598;
    _free_list_18598 = _48get4();
    DeRef(_0);

    /** 	io:seek(current_db, free_list - 4)*/
    if (IS_ATOM_INT(_free_list_18598)) {
        _10636 = _free_list_18598 - 4;
        if ((long)((unsigned long)_10636 +(unsigned long) HIGH_BITS) >= 0){
            _10636 = NewDouble((double)_10636);
        }
    }
    else {
        _10636 = NewDouble(DBL_PTR(_free_list_18598)->dbl - (double)4);
    }
    DeRef(_pos_inlined_seek_at_66_18619);
    _pos_inlined_seek_at_66_18619 = _10636;
    _10636 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_66_18619);
    DeRef(_seek_1__tmp_at69_18621);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_66_18619;
    _seek_1__tmp_at69_18621 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_69_18620 = machine(19, _seek_1__tmp_at69_18621);
    DeRef(_pos_inlined_seek_at_66_18619);
    _pos_inlined_seek_at_66_18619 = NOVALUE;
    DeRef(_seek_1__tmp_at69_18621);
    _seek_1__tmp_at69_18621 = NOVALUE;

    /** 	free_list_space = get4()-4*/
    _10637 = _48get4();
    DeRef(_free_list_space_18599);
    if (IS_ATOM_INT(_10637)) {
        _free_list_space_18599 = _10637 - 4;
        if ((long)((unsigned long)_free_list_space_18599 +(unsigned long) HIGH_BITS) >= 0){
            _free_list_space_18599 = NewDouble((double)_free_list_space_18599);
        }
    }
    else {
        _free_list_space_18599 = binary_op(MINUS, _10637, 4);
    }
    DeRef(_10637);
    _10637 = NOVALUE;

    /** 	if free_list_space < 8 * (free_count+1) then*/
    _10639 = _free_count_18604 + 1;
    if (_10639 > MAXINT){
        _10639 = NewDouble((double)_10639);
    }
    if (IS_ATOM_INT(_10639)) {
        if (_10639 <= INT15 && _10639 >= -INT15)
        _10640 = 8 * _10639;
        else
        _10640 = NewDouble(8 * (double)_10639);
    }
    else {
        _10640 = NewDouble((double)8 * DBL_PTR(_10639)->dbl);
    }
    DeRef(_10639);
    _10639 = NOVALUE;
    if (binary_op_a(GREATEREQ, _free_list_space_18599, _10640)){
        DeRef(_10640);
        _10640 = NOVALUE;
        goto L1; // [102] 314
    }
    DeRef(_10640);
    _10640 = NOVALUE;

    /** 		new_space = floor(free_list_space + free_list_space / 2)*/
    if (IS_ATOM_INT(_free_list_space_18599)) {
        if (_free_list_space_18599 & 1) {
            _10642 = NewDouble((_free_list_space_18599 >> 1) + 0.5);
        }
        else
        _10642 = _free_list_space_18599 >> 1;
    }
    else {
        _10642 = binary_op(DIVIDE, _free_list_space_18599, 2);
    }
    if (IS_ATOM_INT(_free_list_space_18599) && IS_ATOM_INT(_10642)) {
        _10643 = _free_list_space_18599 + _10642;
        if ((long)((unsigned long)_10643 + (unsigned long)HIGH_BITS) >= 0) 
        _10643 = NewDouble((double)_10643);
    }
    else {
        if (IS_ATOM_INT(_free_list_space_18599)) {
            _10643 = NewDouble((double)_free_list_space_18599 + DBL_PTR(_10642)->dbl);
        }
        else {
            if (IS_ATOM_INT(_10642)) {
                _10643 = NewDouble(DBL_PTR(_free_list_space_18599)->dbl + (double)_10642);
            }
            else
            _10643 = NewDouble(DBL_PTR(_free_list_space_18599)->dbl + DBL_PTR(_10642)->dbl);
        }
    }
    DeRef(_10642);
    _10642 = NOVALUE;
    DeRef(_new_space_18600);
    if (IS_ATOM_INT(_10643))
    _new_space_18600 = e_floor(_10643);
    else
    _new_space_18600 = unary_op(FLOOR, _10643);
    DeRef(_10643);
    _10643 = NOVALUE;

    /** 		to_be_freed = free_list*/
    Ref(_free_list_18598);
    DeRef(_to_be_freed_18601);
    _to_be_freed_18601 = _free_list_18598;

    /** 		free_list = db_allocate(new_space)*/
    Ref(_new_space_18600);
    _0 = _free_list_18598;
    _free_list_18598 = _48db_allocate(_new_space_18600);
    DeRef(_0);

    /** 		io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at133_18634);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at133_18634 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_133_18633 = machine(19, _seek_1__tmp_at133_18634);
    DeRefi(_seek_1__tmp_at133_18634);
    _seek_1__tmp_at133_18634 = NOVALUE;

    /** 		free_count = get4() -- db_allocate may have changed it*/
    _free_count_18604 = _48get4();
    if (!IS_ATOM_INT(_free_count_18604)) {
        _1 = (long)(DBL_PTR(_free_count_18604)->dbl);
        if (UNIQUE(DBL_PTR(_free_count_18604)) && (DBL_PTR(_free_count_18604)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_free_count_18604);
        _free_count_18604 = _1;
    }

    /** 		io:seek(current_db, FREE_LIST)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at157_18638);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = 11;
    _seek_1__tmp_at157_18638 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_157_18637 = machine(19, _seek_1__tmp_at157_18638);
    DeRefi(_seek_1__tmp_at157_18638);
    _seek_1__tmp_at157_18638 = NOVALUE;

    /** 		put4(free_list)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_free_list_18598)) {
        *poke4_addr = (unsigned long)_free_list_18598;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_free_list_18598)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at172_18640);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at172_18640 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at172_18640); // DJP 

    /** end procedure*/
    goto L2; // [194] 197
L2: 
    DeRefi(_put4_1__tmp_at172_18640);
    _put4_1__tmp_at172_18640 = NOVALUE;

    /** 		io:seek(current_db, to_be_freed)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_to_be_freed_18601);
    DeRef(_seek_1__tmp_at202_18643);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _to_be_freed_18601;
    _seek_1__tmp_at202_18643 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_202_18642 = machine(19, _seek_1__tmp_at202_18643);
    DeRef(_seek_1__tmp_at202_18643);
    _seek_1__tmp_at202_18643 = NOVALUE;

    /** 		remaining = io:get_bytes(current_db, 8*free_count)*/
    if (_free_count_18604 <= INT15 && _free_count_18604 >= -INT15)
    _10647 = 8 * _free_count_18604;
    else
    _10647 = NewDouble(8 * (double)_free_count_18604);
    _0 = _remaining_18605;
    _remaining_18605 = _16get_bytes(_48current_db_17843, _10647);
    DeRef(_0);
    _10647 = NOVALUE;

    /** 		io:seek(current_db, free_list)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_free_list_18598);
    DeRef(_seek_1__tmp_at234_18648);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _free_list_18598;
    _seek_1__tmp_at234_18648 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_234_18647 = machine(19, _seek_1__tmp_at234_18648);
    DeRef(_seek_1__tmp_at234_18648);
    _seek_1__tmp_at234_18648 = NOVALUE;

    /** 		putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_48current_db_17843, _remaining_18605); // DJP 

    /** end procedure*/
    goto L3; // [259] 262
L3: 

    /** 		putn(repeat(0, new_space-length(remaining)))*/
    if (IS_SEQUENCE(_remaining_18605)){
            _10649 = SEQ_PTR(_remaining_18605)->length;
    }
    else {
        _10649 = 1;
    }
    if (IS_ATOM_INT(_new_space_18600)) {
        _10650 = _new_space_18600 - _10649;
    }
    else {
        _10650 = NewDouble(DBL_PTR(_new_space_18600)->dbl - (double)_10649);
    }
    _10649 = NOVALUE;
    _10651 = Repeat(0, _10650);
    DeRef(_10650);
    _10650 = NOVALUE;
    DeRefi(_s_inlined_putn_at_274_18654);
    _s_inlined_putn_at_274_18654 = _10651;
    _10651 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_48current_db_17843, _s_inlined_putn_at_274_18654); // DJP 

    /** end procedure*/
    goto L4; // [289] 292
L4: 
    DeRefi(_s_inlined_putn_at_274_18654);
    _s_inlined_putn_at_274_18654 = NOVALUE;

    /** 		io:seek(current_db, free_list)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_free_list_18598);
    DeRef(_seek_1__tmp_at297_18657);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _free_list_18598;
    _seek_1__tmp_at297_18657 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_297_18656 = machine(19, _seek_1__tmp_at297_18657);
    DeRef(_seek_1__tmp_at297_18657);
    _seek_1__tmp_at297_18657 = NOVALUE;
    goto L5; // [311] 320
L1: 

    /** 		new_space = 0*/
    DeRef(_new_space_18600);
    _new_space_18600 = 0;
L5: 

    /** 	i = 1*/
    DeRef(_i_18595);
    _i_18595 = 1;

    /** 	prev_addr = 0*/
    DeRef(_prev_addr_18602);
    _prev_addr_18602 = 0;

    /** 	prev_size = 0*/
    DeRef(_prev_size_18603);
    _prev_size_18603 = 0;

    /** 	while i <= free_count do*/
L6: 
    if (binary_op_a(GREATER, _i_18595, _free_count_18604)){
        goto L7; // [340] 386
    }

    /** 		addr = get4()*/
    _0 = _addr_18597;
    _addr_18597 = _48get4();
    DeRef(_0);

    /** 		size = get4()*/
    _0 = _size_18596;
    _size_18596 = _48get4();
    DeRef(_0);

    /** 		if p < addr then*/
    if (binary_op_a(GREATEREQ, _p_18593, _addr_18597)){
        goto L8; // [356] 365
    }

    /** 			exit*/
    goto L7; // [362] 386
L8: 

    /** 		prev_addr = addr*/
    Ref(_addr_18597);
    DeRef(_prev_addr_18602);
    _prev_addr_18602 = _addr_18597;

    /** 		prev_size = size*/
    Ref(_size_18596);
    DeRef(_prev_size_18603);
    _prev_size_18603 = _size_18596;

    /** 		i += 1*/
    _0 = _i_18595;
    if (IS_ATOM_INT(_i_18595)) {
        _i_18595 = _i_18595 + 1;
        if (_i_18595 > MAXINT){
            _i_18595 = NewDouble((double)_i_18595);
        }
    }
    else
    _i_18595 = binary_op(PLUS, 1, _i_18595);
    DeRef(_0);

    /** 	end while*/
    goto L6; // [383] 340
L7: 

    /** 	if i > 1 and prev_addr + prev_size = p then*/
    if (IS_ATOM_INT(_i_18595)) {
        _10657 = (_i_18595 > 1);
    }
    else {
        _10657 = (DBL_PTR(_i_18595)->dbl > (double)1);
    }
    if (_10657 == 0) {
        goto L9; // [392] 695
    }
    if (IS_ATOM_INT(_prev_addr_18602) && IS_ATOM_INT(_prev_size_18603)) {
        _10659 = _prev_addr_18602 + _prev_size_18603;
        if ((long)((unsigned long)_10659 + (unsigned long)HIGH_BITS) >= 0) 
        _10659 = NewDouble((double)_10659);
    }
    else {
        if (IS_ATOM_INT(_prev_addr_18602)) {
            _10659 = NewDouble((double)_prev_addr_18602 + DBL_PTR(_prev_size_18603)->dbl);
        }
        else {
            if (IS_ATOM_INT(_prev_size_18603)) {
                _10659 = NewDouble(DBL_PTR(_prev_addr_18602)->dbl + (double)_prev_size_18603);
            }
            else
            _10659 = NewDouble(DBL_PTR(_prev_addr_18602)->dbl + DBL_PTR(_prev_size_18603)->dbl);
        }
    }
    if (IS_ATOM_INT(_10659) && IS_ATOM_INT(_p_18593)) {
        _10660 = (_10659 == _p_18593);
    }
    else {
        if (IS_ATOM_INT(_10659)) {
            _10660 = ((double)_10659 == DBL_PTR(_p_18593)->dbl);
        }
        else {
            if (IS_ATOM_INT(_p_18593)) {
                _10660 = (DBL_PTR(_10659)->dbl == (double)_p_18593);
            }
            else
            _10660 = (DBL_PTR(_10659)->dbl == DBL_PTR(_p_18593)->dbl);
        }
    }
    DeRef(_10659);
    _10659 = NOVALUE;
    if (_10660 == 0)
    {
        DeRef(_10660);
        _10660 = NOVALUE;
        goto L9; // [405] 695
    }
    else{
        DeRef(_10660);
        _10660 = NOVALUE;
    }

    /** 		io:seek(current_db, free_list+(i-2)*8+4)*/
    if (IS_ATOM_INT(_i_18595)) {
        _10661 = _i_18595 - 2;
        if ((long)((unsigned long)_10661 +(unsigned long) HIGH_BITS) >= 0){
            _10661 = NewDouble((double)_10661);
        }
    }
    else {
        _10661 = NewDouble(DBL_PTR(_i_18595)->dbl - (double)2);
    }
    if (IS_ATOM_INT(_10661)) {
        if (_10661 == (short)_10661)
        _10662 = _10661 * 8;
        else
        _10662 = NewDouble(_10661 * (double)8);
    }
    else {
        _10662 = NewDouble(DBL_PTR(_10661)->dbl * (double)8);
    }
    DeRef(_10661);
    _10661 = NOVALUE;
    if (IS_ATOM_INT(_free_list_18598) && IS_ATOM_INT(_10662)) {
        _10663 = _free_list_18598 + _10662;
        if ((long)((unsigned long)_10663 + (unsigned long)HIGH_BITS) >= 0) 
        _10663 = NewDouble((double)_10663);
    }
    else {
        if (IS_ATOM_INT(_free_list_18598)) {
            _10663 = NewDouble((double)_free_list_18598 + DBL_PTR(_10662)->dbl);
        }
        else {
            if (IS_ATOM_INT(_10662)) {
                _10663 = NewDouble(DBL_PTR(_free_list_18598)->dbl + (double)_10662);
            }
            else
            _10663 = NewDouble(DBL_PTR(_free_list_18598)->dbl + DBL_PTR(_10662)->dbl);
        }
    }
    DeRef(_10662);
    _10662 = NOVALUE;
    if (IS_ATOM_INT(_10663)) {
        _10664 = _10663 + 4;
        if ((long)((unsigned long)_10664 + (unsigned long)HIGH_BITS) >= 0) 
        _10664 = NewDouble((double)_10664);
    }
    else {
        _10664 = NewDouble(DBL_PTR(_10663)->dbl + (double)4);
    }
    DeRef(_10663);
    _10663 = NOVALUE;
    DeRef(_pos_inlined_seek_at_427_18676);
    _pos_inlined_seek_at_427_18676 = _10664;
    _10664 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_427_18676);
    DeRef(_seek_1__tmp_at430_18678);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_427_18676;
    _seek_1__tmp_at430_18678 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_430_18677 = machine(19, _seek_1__tmp_at430_18678);
    DeRef(_pos_inlined_seek_at_427_18676);
    _pos_inlined_seek_at_427_18676 = NOVALUE;
    DeRef(_seek_1__tmp_at430_18678);
    _seek_1__tmp_at430_18678 = NOVALUE;

    /** 		if i < free_count and p + psize = addr then*/
    if (IS_ATOM_INT(_i_18595)) {
        _10665 = (_i_18595 < _free_count_18604);
    }
    else {
        _10665 = (DBL_PTR(_i_18595)->dbl < (double)_free_count_18604);
    }
    if (_10665 == 0) {
        goto LA; // [450] 656
    }
    if (IS_ATOM_INT(_p_18593) && IS_ATOM_INT(_psize_18594)) {
        _10667 = _p_18593 + _psize_18594;
        if ((long)((unsigned long)_10667 + (unsigned long)HIGH_BITS) >= 0) 
        _10667 = NewDouble((double)_10667);
    }
    else {
        if (IS_ATOM_INT(_p_18593)) {
            _10667 = NewDouble((double)_p_18593 + DBL_PTR(_psize_18594)->dbl);
        }
        else {
            if (IS_ATOM_INT(_psize_18594)) {
                _10667 = NewDouble(DBL_PTR(_p_18593)->dbl + (double)_psize_18594);
            }
            else
            _10667 = NewDouble(DBL_PTR(_p_18593)->dbl + DBL_PTR(_psize_18594)->dbl);
        }
    }
    if (IS_ATOM_INT(_10667) && IS_ATOM_INT(_addr_18597)) {
        _10668 = (_10667 == _addr_18597);
    }
    else {
        if (IS_ATOM_INT(_10667)) {
            _10668 = ((double)_10667 == DBL_PTR(_addr_18597)->dbl);
        }
        else {
            if (IS_ATOM_INT(_addr_18597)) {
                _10668 = (DBL_PTR(_10667)->dbl == (double)_addr_18597);
            }
            else
            _10668 = (DBL_PTR(_10667)->dbl == DBL_PTR(_addr_18597)->dbl);
        }
    }
    DeRef(_10667);
    _10667 = NOVALUE;
    if (_10668 == 0)
    {
        DeRef(_10668);
        _10668 = NOVALUE;
        goto LA; // [465] 656
    }
    else{
        DeRef(_10668);
        _10668 = NOVALUE;
    }

    /** 			put4(prev_size+psize+size) -- update size on free list (only)*/
    if (IS_ATOM_INT(_prev_size_18603) && IS_ATOM_INT(_psize_18594)) {
        _10669 = _prev_size_18603 + _psize_18594;
        if ((long)((unsigned long)_10669 + (unsigned long)HIGH_BITS) >= 0) 
        _10669 = NewDouble((double)_10669);
    }
    else {
        if (IS_ATOM_INT(_prev_size_18603)) {
            _10669 = NewDouble((double)_prev_size_18603 + DBL_PTR(_psize_18594)->dbl);
        }
        else {
            if (IS_ATOM_INT(_psize_18594)) {
                _10669 = NewDouble(DBL_PTR(_prev_size_18603)->dbl + (double)_psize_18594);
            }
            else
            _10669 = NewDouble(DBL_PTR(_prev_size_18603)->dbl + DBL_PTR(_psize_18594)->dbl);
        }
    }
    if (IS_ATOM_INT(_10669) && IS_ATOM_INT(_size_18596)) {
        _10670 = _10669 + _size_18596;
        if ((long)((unsigned long)_10670 + (unsigned long)HIGH_BITS) >= 0) 
        _10670 = NewDouble((double)_10670);
    }
    else {
        if (IS_ATOM_INT(_10669)) {
            _10670 = NewDouble((double)_10669 + DBL_PTR(_size_18596)->dbl);
        }
        else {
            if (IS_ATOM_INT(_size_18596)) {
                _10670 = NewDouble(DBL_PTR(_10669)->dbl + (double)_size_18596);
            }
            else
            _10670 = NewDouble(DBL_PTR(_10669)->dbl + DBL_PTR(_size_18596)->dbl);
        }
    }
    DeRef(_10669);
    _10669 = NOVALUE;
    DeRef(_x_inlined_put4_at_479_18687);
    _x_inlined_put4_at_479_18687 = _10670;
    _10670 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_479_18687)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_479_18687;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_479_18687)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at482_18688);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at482_18688 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at482_18688); // DJP 

    /** end procedure*/
    goto LB; // [504] 507
LB: 
    DeRef(_x_inlined_put4_at_479_18687);
    _x_inlined_put4_at_479_18687 = NOVALUE;
    DeRefi(_put4_1__tmp_at482_18688);
    _put4_1__tmp_at482_18688 = NOVALUE;

    /** 			io:seek(current_db, free_list+i*8)*/
    if (IS_ATOM_INT(_i_18595)) {
        if (_i_18595 == (short)_i_18595)
        _10671 = _i_18595 * 8;
        else
        _10671 = NewDouble(_i_18595 * (double)8);
    }
    else {
        _10671 = NewDouble(DBL_PTR(_i_18595)->dbl * (double)8);
    }
    if (IS_ATOM_INT(_free_list_18598) && IS_ATOM_INT(_10671)) {
        _10672 = _free_list_18598 + _10671;
        if ((long)((unsigned long)_10672 + (unsigned long)HIGH_BITS) >= 0) 
        _10672 = NewDouble((double)_10672);
    }
    else {
        if (IS_ATOM_INT(_free_list_18598)) {
            _10672 = NewDouble((double)_free_list_18598 + DBL_PTR(_10671)->dbl);
        }
        else {
            if (IS_ATOM_INT(_10671)) {
                _10672 = NewDouble(DBL_PTR(_free_list_18598)->dbl + (double)_10671);
            }
            else
            _10672 = NewDouble(DBL_PTR(_free_list_18598)->dbl + DBL_PTR(_10671)->dbl);
        }
    }
    DeRef(_10671);
    _10671 = NOVALUE;
    DeRef(_pos_inlined_seek_at_520_18692);
    _pos_inlined_seek_at_520_18692 = _10672;
    _10672 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_520_18692);
    DeRef(_seek_1__tmp_at523_18694);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_520_18692;
    _seek_1__tmp_at523_18694 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_523_18693 = machine(19, _seek_1__tmp_at523_18694);
    DeRef(_pos_inlined_seek_at_520_18692);
    _pos_inlined_seek_at_520_18692 = NOVALUE;
    DeRef(_seek_1__tmp_at523_18694);
    _seek_1__tmp_at523_18694 = NOVALUE;

    /** 			remaining = io:get_bytes(current_db, (free_count-i)*8)*/
    if (IS_ATOM_INT(_i_18595)) {
        _10673 = _free_count_18604 - _i_18595;
        if ((long)((unsigned long)_10673 +(unsigned long) HIGH_BITS) >= 0){
            _10673 = NewDouble((double)_10673);
        }
    }
    else {
        _10673 = NewDouble((double)_free_count_18604 - DBL_PTR(_i_18595)->dbl);
    }
    if (IS_ATOM_INT(_10673)) {
        if (_10673 == (short)_10673)
        _10674 = _10673 * 8;
        else
        _10674 = NewDouble(_10673 * (double)8);
    }
    else {
        _10674 = NewDouble(DBL_PTR(_10673)->dbl * (double)8);
    }
    DeRef(_10673);
    _10673 = NOVALUE;
    _0 = _remaining_18605;
    _remaining_18605 = _16get_bytes(_48current_db_17843, _10674);
    DeRef(_0);
    _10674 = NOVALUE;

    /** 			io:seek(current_db, free_list+(i-1)*8)*/
    if (IS_ATOM_INT(_i_18595)) {
        _10676 = _i_18595 - 1;
        if ((long)((unsigned long)_10676 +(unsigned long) HIGH_BITS) >= 0){
            _10676 = NewDouble((double)_10676);
        }
    }
    else {
        _10676 = NewDouble(DBL_PTR(_i_18595)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_10676)) {
        if (_10676 == (short)_10676)
        _10677 = _10676 * 8;
        else
        _10677 = NewDouble(_10676 * (double)8);
    }
    else {
        _10677 = NewDouble(DBL_PTR(_10676)->dbl * (double)8);
    }
    DeRef(_10676);
    _10676 = NOVALUE;
    if (IS_ATOM_INT(_free_list_18598) && IS_ATOM_INT(_10677)) {
        _10678 = _free_list_18598 + _10677;
        if ((long)((unsigned long)_10678 + (unsigned long)HIGH_BITS) >= 0) 
        _10678 = NewDouble((double)_10678);
    }
    else {
        if (IS_ATOM_INT(_free_list_18598)) {
            _10678 = NewDouble((double)_free_list_18598 + DBL_PTR(_10677)->dbl);
        }
        else {
            if (IS_ATOM_INT(_10677)) {
                _10678 = NewDouble(DBL_PTR(_free_list_18598)->dbl + (double)_10677);
            }
            else
            _10678 = NewDouble(DBL_PTR(_free_list_18598)->dbl + DBL_PTR(_10677)->dbl);
        }
    }
    DeRef(_10677);
    _10677 = NOVALUE;
    DeRef(_pos_inlined_seek_at_571_18702);
    _pos_inlined_seek_at_571_18702 = _10678;
    _10678 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_571_18702);
    DeRef(_seek_1__tmp_at574_18704);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_571_18702;
    _seek_1__tmp_at574_18704 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_574_18703 = machine(19, _seek_1__tmp_at574_18704);
    DeRef(_pos_inlined_seek_at_571_18702);
    _pos_inlined_seek_at_571_18702 = NOVALUE;
    DeRef(_seek_1__tmp_at574_18704);
    _seek_1__tmp_at574_18704 = NOVALUE;

    /** 			putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_48current_db_17843, _remaining_18605); // DJP 

    /** end procedure*/
    goto LC; // [599] 602
LC: 

    /** 			free_count -= 1*/
    _free_count_18604 = _free_count_18604 - 1;

    /** 			io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at611_18709);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at611_18709 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_611_18708 = machine(19, _seek_1__tmp_at611_18709);
    DeRefi(_seek_1__tmp_at611_18709);
    _seek_1__tmp_at611_18709 = NOVALUE;

    /** 			put4(free_count)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    *poke4_addr = (unsigned long)_free_count_18604;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at626_18711);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at626_18711 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at626_18711); // DJP 

    /** end procedure*/
    goto LD; // [648] 651
LD: 
    DeRefi(_put4_1__tmp_at626_18711);
    _put4_1__tmp_at626_18711 = NOVALUE;
    goto LE; // [653] 1028
LA: 

    /** 			put4(prev_size+psize) -- increase previous size on free list (only)*/
    if (IS_ATOM_INT(_prev_size_18603) && IS_ATOM_INT(_psize_18594)) {
        _10680 = _prev_size_18603 + _psize_18594;
        if ((long)((unsigned long)_10680 + (unsigned long)HIGH_BITS) >= 0) 
        _10680 = NewDouble((double)_10680);
    }
    else {
        if (IS_ATOM_INT(_prev_size_18603)) {
            _10680 = NewDouble((double)_prev_size_18603 + DBL_PTR(_psize_18594)->dbl);
        }
        else {
            if (IS_ATOM_INT(_psize_18594)) {
                _10680 = NewDouble(DBL_PTR(_prev_size_18603)->dbl + (double)_psize_18594);
            }
            else
            _10680 = NewDouble(DBL_PTR(_prev_size_18603)->dbl + DBL_PTR(_psize_18594)->dbl);
        }
    }
    DeRef(_x_inlined_put4_at_661_18715);
    _x_inlined_put4_at_661_18715 = _10680;
    _10680 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_661_18715)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_661_18715;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_661_18715)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at664_18716);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at664_18716 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at664_18716); // DJP 

    /** end procedure*/
    goto LF; // [686] 689
LF: 
    DeRef(_x_inlined_put4_at_661_18715);
    _x_inlined_put4_at_661_18715 = NOVALUE;
    DeRefi(_put4_1__tmp_at664_18716);
    _put4_1__tmp_at664_18716 = NOVALUE;
    goto LE; // [692] 1028
L9: 

    /** 	elsif i < free_count and p + psize = addr then*/
    if (IS_ATOM_INT(_i_18595)) {
        _10681 = (_i_18595 < _free_count_18604);
    }
    else {
        _10681 = (DBL_PTR(_i_18595)->dbl < (double)_free_count_18604);
    }
    if (_10681 == 0) {
        goto L10; // [701] 819
    }
    if (IS_ATOM_INT(_p_18593) && IS_ATOM_INT(_psize_18594)) {
        _10683 = _p_18593 + _psize_18594;
        if ((long)((unsigned long)_10683 + (unsigned long)HIGH_BITS) >= 0) 
        _10683 = NewDouble((double)_10683);
    }
    else {
        if (IS_ATOM_INT(_p_18593)) {
            _10683 = NewDouble((double)_p_18593 + DBL_PTR(_psize_18594)->dbl);
        }
        else {
            if (IS_ATOM_INT(_psize_18594)) {
                _10683 = NewDouble(DBL_PTR(_p_18593)->dbl + (double)_psize_18594);
            }
            else
            _10683 = NewDouble(DBL_PTR(_p_18593)->dbl + DBL_PTR(_psize_18594)->dbl);
        }
    }
    if (IS_ATOM_INT(_10683) && IS_ATOM_INT(_addr_18597)) {
        _10684 = (_10683 == _addr_18597);
    }
    else {
        if (IS_ATOM_INT(_10683)) {
            _10684 = ((double)_10683 == DBL_PTR(_addr_18597)->dbl);
        }
        else {
            if (IS_ATOM_INT(_addr_18597)) {
                _10684 = (DBL_PTR(_10683)->dbl == (double)_addr_18597);
            }
            else
            _10684 = (DBL_PTR(_10683)->dbl == DBL_PTR(_addr_18597)->dbl);
        }
    }
    DeRef(_10683);
    _10683 = NOVALUE;
    if (_10684 == 0)
    {
        DeRef(_10684);
        _10684 = NOVALUE;
        goto L10; // [716] 819
    }
    else{
        DeRef(_10684);
        _10684 = NOVALUE;
    }

    /** 		io:seek(current_db, free_list+(i-1)*8)*/
    if (IS_ATOM_INT(_i_18595)) {
        _10685 = _i_18595 - 1;
        if ((long)((unsigned long)_10685 +(unsigned long) HIGH_BITS) >= 0){
            _10685 = NewDouble((double)_10685);
        }
    }
    else {
        _10685 = NewDouble(DBL_PTR(_i_18595)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_10685)) {
        if (_10685 == (short)_10685)
        _10686 = _10685 * 8;
        else
        _10686 = NewDouble(_10685 * (double)8);
    }
    else {
        _10686 = NewDouble(DBL_PTR(_10685)->dbl * (double)8);
    }
    DeRef(_10685);
    _10685 = NOVALUE;
    if (IS_ATOM_INT(_free_list_18598) && IS_ATOM_INT(_10686)) {
        _10687 = _free_list_18598 + _10686;
        if ((long)((unsigned long)_10687 + (unsigned long)HIGH_BITS) >= 0) 
        _10687 = NewDouble((double)_10687);
    }
    else {
        if (IS_ATOM_INT(_free_list_18598)) {
            _10687 = NewDouble((double)_free_list_18598 + DBL_PTR(_10686)->dbl);
        }
        else {
            if (IS_ATOM_INT(_10686)) {
                _10687 = NewDouble(DBL_PTR(_free_list_18598)->dbl + (double)_10686);
            }
            else
            _10687 = NewDouble(DBL_PTR(_free_list_18598)->dbl + DBL_PTR(_10686)->dbl);
        }
    }
    DeRef(_10686);
    _10686 = NOVALUE;
    DeRef(_pos_inlined_seek_at_734_18726);
    _pos_inlined_seek_at_734_18726 = _10687;
    _10687 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_734_18726);
    DeRef(_seek_1__tmp_at737_18728);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_734_18726;
    _seek_1__tmp_at737_18728 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_737_18727 = machine(19, _seek_1__tmp_at737_18728);
    DeRef(_pos_inlined_seek_at_734_18726);
    _pos_inlined_seek_at_734_18726 = NOVALUE;
    DeRef(_seek_1__tmp_at737_18728);
    _seek_1__tmp_at737_18728 = NOVALUE;

    /** 		put4(p)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_p_18593)) {
        *poke4_addr = (unsigned long)_p_18593;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_p_18593)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at752_18730);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at752_18730 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at752_18730); // DJP 

    /** end procedure*/
    goto L11; // [774] 777
L11: 
    DeRefi(_put4_1__tmp_at752_18730);
    _put4_1__tmp_at752_18730 = NOVALUE;

    /** 		put4(psize+size)*/
    if (IS_ATOM_INT(_psize_18594) && IS_ATOM_INT(_size_18596)) {
        _10688 = _psize_18594 + _size_18596;
        if ((long)((unsigned long)_10688 + (unsigned long)HIGH_BITS) >= 0) 
        _10688 = NewDouble((double)_10688);
    }
    else {
        if (IS_ATOM_INT(_psize_18594)) {
            _10688 = NewDouble((double)_psize_18594 + DBL_PTR(_size_18596)->dbl);
        }
        else {
            if (IS_ATOM_INT(_size_18596)) {
                _10688 = NewDouble(DBL_PTR(_psize_18594)->dbl + (double)_size_18596);
            }
            else
            _10688 = NewDouble(DBL_PTR(_psize_18594)->dbl + DBL_PTR(_size_18596)->dbl);
        }
    }
    DeRef(_x_inlined_put4_at_786_18733);
    _x_inlined_put4_at_786_18733 = _10688;
    _10688 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_786_18733)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_786_18733;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_786_18733)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at789_18734);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at789_18734 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at789_18734); // DJP 

    /** end procedure*/
    goto L12; // [811] 814
L12: 
    DeRef(_x_inlined_put4_at_786_18733);
    _x_inlined_put4_at_786_18733 = NOVALUE;
    DeRefi(_put4_1__tmp_at789_18734);
    _put4_1__tmp_at789_18734 = NOVALUE;
    goto LE; // [816] 1028
L10: 

    /** 		io:seek(current_db, free_list+(i-1)*8)*/
    if (IS_ATOM_INT(_i_18595)) {
        _10689 = _i_18595 - 1;
        if ((long)((unsigned long)_10689 +(unsigned long) HIGH_BITS) >= 0){
            _10689 = NewDouble((double)_10689);
        }
    }
    else {
        _10689 = NewDouble(DBL_PTR(_i_18595)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_10689)) {
        if (_10689 == (short)_10689)
        _10690 = _10689 * 8;
        else
        _10690 = NewDouble(_10689 * (double)8);
    }
    else {
        _10690 = NewDouble(DBL_PTR(_10689)->dbl * (double)8);
    }
    DeRef(_10689);
    _10689 = NOVALUE;
    if (IS_ATOM_INT(_free_list_18598) && IS_ATOM_INT(_10690)) {
        _10691 = _free_list_18598 + _10690;
        if ((long)((unsigned long)_10691 + (unsigned long)HIGH_BITS) >= 0) 
        _10691 = NewDouble((double)_10691);
    }
    else {
        if (IS_ATOM_INT(_free_list_18598)) {
            _10691 = NewDouble((double)_free_list_18598 + DBL_PTR(_10690)->dbl);
        }
        else {
            if (IS_ATOM_INT(_10690)) {
                _10691 = NewDouble(DBL_PTR(_free_list_18598)->dbl + (double)_10690);
            }
            else
            _10691 = NewDouble(DBL_PTR(_free_list_18598)->dbl + DBL_PTR(_10690)->dbl);
        }
    }
    DeRef(_10690);
    _10690 = NOVALUE;
    DeRef(_pos_inlined_seek_at_834_18740);
    _pos_inlined_seek_at_834_18740 = _10691;
    _10691 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_834_18740);
    DeRef(_seek_1__tmp_at837_18742);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_834_18740;
    _seek_1__tmp_at837_18742 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_837_18741 = machine(19, _seek_1__tmp_at837_18742);
    DeRef(_pos_inlined_seek_at_834_18740);
    _pos_inlined_seek_at_834_18740 = NOVALUE;
    DeRef(_seek_1__tmp_at837_18742);
    _seek_1__tmp_at837_18742 = NOVALUE;

    /** 		remaining = io:get_bytes(current_db, (free_count-i+1)*8)*/
    if (IS_ATOM_INT(_i_18595)) {
        _10692 = _free_count_18604 - _i_18595;
        if ((long)((unsigned long)_10692 +(unsigned long) HIGH_BITS) >= 0){
            _10692 = NewDouble((double)_10692);
        }
    }
    else {
        _10692 = NewDouble((double)_free_count_18604 - DBL_PTR(_i_18595)->dbl);
    }
    if (IS_ATOM_INT(_10692)) {
        _10693 = _10692 + 1;
        if (_10693 > MAXINT){
            _10693 = NewDouble((double)_10693);
        }
    }
    else
    _10693 = binary_op(PLUS, 1, _10692);
    DeRef(_10692);
    _10692 = NOVALUE;
    if (IS_ATOM_INT(_10693)) {
        if (_10693 == (short)_10693)
        _10694 = _10693 * 8;
        else
        _10694 = NewDouble(_10693 * (double)8);
    }
    else {
        _10694 = NewDouble(DBL_PTR(_10693)->dbl * (double)8);
    }
    DeRef(_10693);
    _10693 = NOVALUE;
    _0 = _remaining_18605;
    _remaining_18605 = _16get_bytes(_48current_db_17843, _10694);
    DeRef(_0);
    _10694 = NOVALUE;

    /** 		free_count += 1*/
    _free_count_18604 = _free_count_18604 + 1;

    /** 		io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at883_18750);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at883_18750 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_883_18749 = machine(19, _seek_1__tmp_at883_18750);
    DeRefi(_seek_1__tmp_at883_18750);
    _seek_1__tmp_at883_18750 = NOVALUE;

    /** 		put4(free_count)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    *poke4_addr = (unsigned long)_free_count_18604;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at898_18752);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at898_18752 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at898_18752); // DJP 

    /** end procedure*/
    goto L13; // [920] 923
L13: 
    DeRefi(_put4_1__tmp_at898_18752);
    _put4_1__tmp_at898_18752 = NOVALUE;

    /** 		io:seek(current_db, free_list+(i-1)*8)*/
    if (IS_ATOM_INT(_i_18595)) {
        _10697 = _i_18595 - 1;
        if ((long)((unsigned long)_10697 +(unsigned long) HIGH_BITS) >= 0){
            _10697 = NewDouble((double)_10697);
        }
    }
    else {
        _10697 = NewDouble(DBL_PTR(_i_18595)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_10697)) {
        if (_10697 == (short)_10697)
        _10698 = _10697 * 8;
        else
        _10698 = NewDouble(_10697 * (double)8);
    }
    else {
        _10698 = NewDouble(DBL_PTR(_10697)->dbl * (double)8);
    }
    DeRef(_10697);
    _10697 = NOVALUE;
    if (IS_ATOM_INT(_free_list_18598) && IS_ATOM_INT(_10698)) {
        _10699 = _free_list_18598 + _10698;
        if ((long)((unsigned long)_10699 + (unsigned long)HIGH_BITS) >= 0) 
        _10699 = NewDouble((double)_10699);
    }
    else {
        if (IS_ATOM_INT(_free_list_18598)) {
            _10699 = NewDouble((double)_free_list_18598 + DBL_PTR(_10698)->dbl);
        }
        else {
            if (IS_ATOM_INT(_10698)) {
                _10699 = NewDouble(DBL_PTR(_free_list_18598)->dbl + (double)_10698);
            }
            else
            _10699 = NewDouble(DBL_PTR(_free_list_18598)->dbl + DBL_PTR(_10698)->dbl);
        }
    }
    DeRef(_10698);
    _10698 = NOVALUE;
    DeRef(_pos_inlined_seek_at_940_18757);
    _pos_inlined_seek_at_940_18757 = _10699;
    _10699 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_940_18757);
    DeRef(_seek_1__tmp_at943_18759);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_940_18757;
    _seek_1__tmp_at943_18759 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_943_18758 = machine(19, _seek_1__tmp_at943_18759);
    DeRef(_pos_inlined_seek_at_940_18757);
    _pos_inlined_seek_at_940_18757 = NOVALUE;
    DeRef(_seek_1__tmp_at943_18759);
    _seek_1__tmp_at943_18759 = NOVALUE;

    /** 		put4(p)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_p_18593)) {
        *poke4_addr = (unsigned long)_p_18593;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_p_18593)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at958_18761);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at958_18761 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at958_18761); // DJP 

    /** end procedure*/
    goto L14; // [980] 983
L14: 
    DeRefi(_put4_1__tmp_at958_18761);
    _put4_1__tmp_at958_18761 = NOVALUE;

    /** 		put4(psize)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_psize_18594)) {
        *poke4_addr = (unsigned long)_psize_18594;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_psize_18594)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at986_18763);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at986_18763 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at986_18763); // DJP 

    /** end procedure*/
    goto L15; // [1008] 1011
L15: 
    DeRefi(_put4_1__tmp_at986_18763);
    _put4_1__tmp_at986_18763 = NOVALUE;

    /** 		putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_48current_db_17843, _remaining_18605); // DJP 

    /** end procedure*/
    goto L16; // [1024] 1027
L16: 
LE: 

    /** 	if new_space then*/
    if (_new_space_18600 == 0) {
        goto L17; // [1032] 1046
    }
    else {
        if (!IS_ATOM_INT(_new_space_18600) && DBL_PTR(_new_space_18600)->dbl == 0.0){
            goto L17; // [1032] 1046
        }
    }

    /** 		db_free(to_be_freed) -- free the old space*/
    Ref(_to_be_freed_18601);
    DeRef(_10700);
    _10700 = _to_be_freed_18601;
    _48db_free(_10700);
    _10700 = NOVALUE;
L17: 

    /** end procedure*/
    DeRef(_p_18593);
    DeRef(_psize_18594);
    DeRef(_i_18595);
    DeRef(_size_18596);
    DeRef(_addr_18597);
    DeRef(_free_list_18598);
    DeRef(_free_list_space_18599);
    DeRef(_new_space_18600);
    DeRef(_to_be_freed_18601);
    DeRef(_prev_addr_18602);
    DeRef(_prev_size_18603);
    DeRef(_remaining_18605);
    DeRef(_10657);
    _10657 = NOVALUE;
    DeRef(_10665);
    _10665 = NOVALUE;
    DeRef(_10681);
    _10681 = NOVALUE;
    return;
    ;
}


void _48save_keys()
{
    int _k_18769 = NOVALUE;
    int _10707 = NOVALUE;
    int _10703 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if caching_option = 1 then*/

    /** 		if current_table_pos > 0 then*/
    if (binary_op_a(LESSEQ, _48current_table_pos_17844, 0)){
        goto L1; // [13] 81
    }

    /** 			k = eu:find({current_db, current_table_pos}, cache_index)*/
    Ref(_48current_table_pos_17844);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _48current_table_pos_17844;
    _10703 = MAKE_SEQ(_1);
    _k_18769 = find_from(_10703, _48cache_index_17852, 1);
    DeRefDS(_10703);
    _10703 = NOVALUE;

    /** 			if k != 0 then*/
    if (_k_18769 == 0)
    goto L2; // [36] 53

    /** 				key_cache[k] = key_pointers*/
    RefDS(_48key_pointers_17850);
    _2 = (int)SEQ_PTR(_48key_cache_17851);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _48key_cache_17851 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _k_18769);
    _1 = *(int *)_2;
    *(int *)_2 = _48key_pointers_17850;
    DeRef(_1);
    goto L3; // [50] 80
L2: 

    /** 				key_cache = append(key_cache, key_pointers)*/
    RefDS(_48key_pointers_17850);
    Append(&_48key_cache_17851, _48key_cache_17851, _48key_pointers_17850);

    /** 				cache_index = append(cache_index, {current_db, current_table_pos})*/
    Ref(_48current_table_pos_17844);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _48current_table_pos_17844;
    _10707 = MAKE_SEQ(_1);
    RefDS(_10707);
    Append(&_48cache_index_17852, _48cache_index_17852, _10707);
    DeRefDS(_10707);
    _10707 = NOVALUE;
L3: 
L1: 

    /** end procedure*/
    return;
    ;
}


int _48db_create(int _path_18866, int _lock_method_18867, int _init_tables_18868, int _init_free_18869)
{
    int _db_18870 = NOVALUE;
    int _lock_file_1__tmp_at246_18911 = NOVALUE;
    int _lock_file_inlined_lock_file_at_246_18910 = NOVALUE;
    int _put4_1__tmp_at368_18920 = NOVALUE;
    int _put4_1__tmp_at396_18922 = NOVALUE;
    int _put4_1__tmp_at439_18928 = NOVALUE;
    int _x_inlined_put4_at_436_18927 = NOVALUE;
    int _put4_1__tmp_at478_18933 = NOVALUE;
    int _x_inlined_put4_at_475_18932 = NOVALUE;
    int _put4_1__tmp_at506_18935 = NOVALUE;
    int _s_inlined_putn_at_542_18939 = NOVALUE;
    int _put4_1__tmp_at574_18944 = NOVALUE;
    int _x_inlined_put4_at_571_18943 = NOVALUE;
    int _s_inlined_putn_at_610_18948 = NOVALUE;
    int _10807 = NOVALUE;
    int _10806 = NOVALUE;
    int _10805 = NOVALUE;
    int _10804 = NOVALUE;
    int _10803 = NOVALUE;
    int _10802 = NOVALUE;
    int _10801 = NOVALUE;
    int _10800 = NOVALUE;
    int _10799 = NOVALUE;
    int _10798 = NOVALUE;
    int _10797 = NOVALUE;
    int _10778 = NOVALUE;
    int _10776 = NOVALUE;
    int _10775 = NOVALUE;
    int _10773 = NOVALUE;
    int _10772 = NOVALUE;
    int _10770 = NOVALUE;
    int _10769 = NOVALUE;
    int _10767 = NOVALUE;
    int _0, _1, _2;
    

    /** 	db = find(path, Known_Aliases)*/
    _db_18870 = find_from(_path_18866, _48Known_Aliases_17864, 1);

    /** 	if db then*/
    if (_db_18870 == 0)
    {
        goto L1; // [20] 100
    }
    else{
    }

    /** 		path = Alias_Details[db][1]*/
    _2 = (int)SEQ_PTR(_48Alias_Details_17865);
    _10767 = (int)*(((s1_ptr)_2)->base + _db_18870);
    DeRefDS(_path_18866);
    _2 = (int)SEQ_PTR(_10767);
    _path_18866 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_path_18866);
    _10767 = NOVALUE;

    /** 		lock_method = Alias_Details[db][2][CONNECT_LOCK]*/
    _2 = (int)SEQ_PTR(_48Alias_Details_17865);
    _10769 = (int)*(((s1_ptr)_2)->base + _db_18870);
    _2 = (int)SEQ_PTR(_10769);
    _10770 = (int)*(((s1_ptr)_2)->base + 2);
    _10769 = NOVALUE;
    _2 = (int)SEQ_PTR(_10770);
    _lock_method_18867 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_lock_method_18867)){
        _lock_method_18867 = (long)DBL_PTR(_lock_method_18867)->dbl;
    }
    _10770 = NOVALUE;

    /** 		init_tables = Alias_Details[db][2][CONNECT_TABLES]*/
    _2 = (int)SEQ_PTR(_48Alias_Details_17865);
    _10772 = (int)*(((s1_ptr)_2)->base + _db_18870);
    _2 = (int)SEQ_PTR(_10772);
    _10773 = (int)*(((s1_ptr)_2)->base + 2);
    _10772 = NOVALUE;
    _2 = (int)SEQ_PTR(_10773);
    _init_tables_18868 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_init_tables_18868)){
        _init_tables_18868 = (long)DBL_PTR(_init_tables_18868)->dbl;
    }
    _10773 = NOVALUE;

    /** 		init_free = Alias_Details[db][2][CONNECT_FREE]*/
    _2 = (int)SEQ_PTR(_48Alias_Details_17865);
    _10775 = (int)*(((s1_ptr)_2)->base + _db_18870);
    _2 = (int)SEQ_PTR(_10775);
    _10776 = (int)*(((s1_ptr)_2)->base + 2);
    _10775 = NOVALUE;
    _2 = (int)SEQ_PTR(_10776);
    _init_free_18869 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_init_free_18869)){
        _init_free_18869 = (long)DBL_PTR(_init_free_18869)->dbl;
    }
    _10776 = NOVALUE;
    goto L2; // [97] 142
L1: 

    /** 		path = filesys:canonical_path( defaultext(path, "edb") )*/
    RefDS(_path_18866);
    RefDS(_10761);
    _10778 = _9defaultext(_path_18866, _10761);
    _0 = _path_18866;
    _path_18866 = _9canonical_path(_10778, 0, 0);
    DeRefDS(_0);
    _10778 = NOVALUE;

    /** 		if init_tables < 1 then*/
    if (_init_tables_18868 >= 1)
    goto L3; // [119] 129

    /** 			init_tables = 1*/
    _init_tables_18868 = 1;
L3: 

    /** 		if init_free < 0 then*/
    if (_init_free_18869 >= 0)
    goto L4; // [131] 141

    /** 			init_free = 0*/
    _init_free_18869 = 0;
L4: 
L2: 

    /** 	db = open(path, "rb")*/
    _db_18870 = EOpen(_path_18866, _10782, 0);

    /** 	if db != -1 then*/
    if (_db_18870 == -1)
    goto L5; // [151] 168

    /** 		close(db)*/
    EClose(_db_18870);

    /** 		return DB_EXISTS_ALREADY*/
    DeRefDS(_path_18866);
    return -2;
L5: 

    /** 	db = open(path, "wb")*/
    _db_18870 = EOpen(_path_18866, _10785, 0);

    /** 	if db = -1 then*/
    if (_db_18870 != -1)
    goto L6; // [177] 190

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_path_18866);
    return -1;
L6: 

    /** 	close(db)*/
    EClose(_db_18870);

    /** 	db = open(path, "ub")*/
    _db_18870 = EOpen(_path_18866, _10788, 0);

    /** 	if db = -1 then*/
    if (_db_18870 != -1)
    goto L7; // [203] 216

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_path_18866);
    return -1;
L7: 

    /** 	if lock_method = DB_LOCK_SHARED then*/
    if (_lock_method_18867 != 1)
    goto L8; // [220] 234

    /** 		lock_method = DB_LOCK_NO*/
    _lock_method_18867 = 0;
L8: 

    /** 	if lock_method = DB_LOCK_EXCLUSIVE then*/
    if (_lock_method_18867 != 2)
    goto L9; // [238] 274

    /** 		if not io:lock_file(db, io:LOCK_EXCLUSIVE, {}) then*/

    /** 	return machine_func(M_LOCK_FILE, {fn, t, r})*/
    _0 = _lock_file_1__tmp_at246_18911;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _db_18870;
    *((int *)(_2+8)) = 2;
    RefDS(_5);
    *((int *)(_2+12)) = _5;
    _lock_file_1__tmp_at246_18911 = MAKE_SEQ(_1);
    DeRef(_0);
    _lock_file_inlined_lock_file_at_246_18910 = machine(61, _lock_file_1__tmp_at246_18911);
    DeRef(_lock_file_1__tmp_at246_18911);
    _lock_file_1__tmp_at246_18911 = NOVALUE;
    if (_lock_file_inlined_lock_file_at_246_18910 != 0)
    goto LA; // [261] 273

    /** 			return DB_LOCK_FAIL*/
    DeRefDS(_path_18866);
    return -3;
LA: 
L9: 

    /** 	save_keys()*/
    _48save_keys();

    /** 	current_db = db*/
    _48current_db_17843 = _db_18870;

    /** 	current_lock = lock_method*/
    _48current_lock_17849 = _lock_method_18867;

    /** 	current_table_pos = -1*/
    DeRef(_48current_table_pos_17844);
    _48current_table_pos_17844 = -1;

    /** 	current_table_name = ""*/
    RefDS(_5);
    DeRef(_48current_table_name_17845);
    _48current_table_name_17845 = _5;

    /** 	db_names = append(db_names, path)*/
    RefDS(_path_18866);
    Append(&_48db_names_17846, _48db_names_17846, _path_18866);

    /** 	db_lock_methods = append(db_lock_methods, lock_method)*/
    Append(&_48db_lock_methods_17848, _48db_lock_methods_17848, _lock_method_18867);

    /** 	db_file_nums = append(db_file_nums, db)*/
    Append(&_48db_file_nums_17847, _48db_file_nums_17847, _db_18870);

    /** 	put1(DB_MAGIC) -- so we know what type of file it is*/

    /** 	puts(current_db, x)*/
    EPuts(_48current_db_17843, 77); // DJP 

    /** end procedure*/
    goto LB; // [335] 338
LB: 

    /** 	put1(DB_MAJOR) -- major version*/

    /** 	puts(current_db, x)*/
    EPuts(_48current_db_17843, 4); // DJP 

    /** end procedure*/
    goto LC; // [349] 352
LC: 

    /** 	put1(DB_MINOR) -- minor version*/

    /** 	puts(current_db, x)*/
    EPuts(_48current_db_17843, 0); // DJP 

    /** end procedure*/
    goto LD; // [363] 366
LD: 

    /** 	put4(19)  -- pointer to tables*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    *poke4_addr = (unsigned long)19;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at368_18920);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at368_18920 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at368_18920); // DJP 

    /** end procedure*/
    goto LE; // [389] 392
LE: 
    DeRefi(_put4_1__tmp_at368_18920);
    _put4_1__tmp_at368_18920 = NOVALUE;

    /** 	put4(0)   -- number of free blocks*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    *poke4_addr = (unsigned long)0;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at396_18922);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at396_18922 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at396_18922); // DJP 

    /** end procedure*/
    goto LF; // [417] 420
LF: 
    DeRefi(_put4_1__tmp_at396_18922);
    _put4_1__tmp_at396_18922 = NOVALUE;

    /** 	put4(23 + init_tables * SIZEOF_TABLE_HEADER + 4)   -- pointer to free list*/
    if (_init_tables_18868 == (short)_init_tables_18868)
    _10797 = _init_tables_18868 * 16;
    else
    _10797 = NewDouble(_init_tables_18868 * (double)16);
    if (IS_ATOM_INT(_10797)) {
        _10798 = 23 + _10797;
        if ((long)((unsigned long)_10798 + (unsigned long)HIGH_BITS) >= 0) 
        _10798 = NewDouble((double)_10798);
    }
    else {
        _10798 = NewDouble((double)23 + DBL_PTR(_10797)->dbl);
    }
    DeRef(_10797);
    _10797 = NOVALUE;
    if (IS_ATOM_INT(_10798)) {
        _10799 = _10798 + 4;
        if ((long)((unsigned long)_10799 + (unsigned long)HIGH_BITS) >= 0) 
        _10799 = NewDouble((double)_10799);
    }
    else {
        _10799 = NewDouble(DBL_PTR(_10798)->dbl + (double)4);
    }
    DeRef(_10798);
    _10798 = NOVALUE;
    DeRef(_x_inlined_put4_at_436_18927);
    _x_inlined_put4_at_436_18927 = _10799;
    _10799 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_436_18927)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_436_18927;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_436_18927)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at439_18928);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at439_18928 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at439_18928); // DJP 

    /** end procedure*/
    goto L10; // [460] 463
L10: 
    DeRef(_x_inlined_put4_at_436_18927);
    _x_inlined_put4_at_436_18927 = NOVALUE;
    DeRefi(_put4_1__tmp_at439_18928);
    _put4_1__tmp_at439_18928 = NOVALUE;

    /** 	put4( 8 + init_tables * SIZEOF_TABLE_HEADER)  -- allocated size*/
    if (_init_tables_18868 == (short)_init_tables_18868)
    _10800 = _init_tables_18868 * 16;
    else
    _10800 = NewDouble(_init_tables_18868 * (double)16);
    if (IS_ATOM_INT(_10800)) {
        _10801 = 8 + _10800;
        if ((long)((unsigned long)_10801 + (unsigned long)HIGH_BITS) >= 0) 
        _10801 = NewDouble((double)_10801);
    }
    else {
        _10801 = NewDouble((double)8 + DBL_PTR(_10800)->dbl);
    }
    DeRef(_10800);
    _10800 = NOVALUE;
    DeRef(_x_inlined_put4_at_475_18932);
    _x_inlined_put4_at_475_18932 = _10801;
    _10801 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_475_18932)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_475_18932;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_475_18932)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at478_18933);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at478_18933 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at478_18933); // DJP 

    /** end procedure*/
    goto L11; // [499] 502
L11: 
    DeRef(_x_inlined_put4_at_475_18932);
    _x_inlined_put4_at_475_18932 = NOVALUE;
    DeRefi(_put4_1__tmp_at478_18933);
    _put4_1__tmp_at478_18933 = NOVALUE;

    /** 	put4(0)   -- number of tables that currently exist*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    *poke4_addr = (unsigned long)0;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at506_18935);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at506_18935 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at506_18935); // DJP 

    /** end procedure*/
    goto L12; // [527] 530
L12: 
    DeRefi(_put4_1__tmp_at506_18935);
    _put4_1__tmp_at506_18935 = NOVALUE;

    /** 	putn(repeat(0, init_tables * SIZEOF_TABLE_HEADER))*/
    _10802 = _init_tables_18868 * 16;
    _10803 = Repeat(0, _10802);
    _10802 = NOVALUE;
    DeRefi(_s_inlined_putn_at_542_18939);
    _s_inlined_putn_at_542_18939 = _10803;
    _10803 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_48current_db_17843, _s_inlined_putn_at_542_18939); // DJP 

    /** end procedure*/
    goto L13; // [556] 559
L13: 
    DeRefi(_s_inlined_putn_at_542_18939);
    _s_inlined_putn_at_542_18939 = NOVALUE;

    /** 	put4(4+init_free*8)   -- allocated size*/
    if (_init_free_18869 == (short)_init_free_18869)
    _10804 = _init_free_18869 * 8;
    else
    _10804 = NewDouble(_init_free_18869 * (double)8);
    if (IS_ATOM_INT(_10804)) {
        _10805 = 4 + _10804;
        if ((long)((unsigned long)_10805 + (unsigned long)HIGH_BITS) >= 0) 
        _10805 = NewDouble((double)_10805);
    }
    else {
        _10805 = NewDouble((double)4 + DBL_PTR(_10804)->dbl);
    }
    DeRef(_10804);
    _10804 = NOVALUE;
    DeRef(_x_inlined_put4_at_571_18943);
    _x_inlined_put4_at_571_18943 = _10805;
    _10805 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_571_18943)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_571_18943;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_571_18943)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at574_18944);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at574_18944 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at574_18944); // DJP 

    /** end procedure*/
    goto L14; // [595] 598
L14: 
    DeRef(_x_inlined_put4_at_571_18943);
    _x_inlined_put4_at_571_18943 = NOVALUE;
    DeRefi(_put4_1__tmp_at574_18944);
    _put4_1__tmp_at574_18944 = NOVALUE;

    /** 	putn(repeat(0, init_free * 8))*/
    _10806 = _init_free_18869 * 8;
    _10807 = Repeat(0, _10806);
    _10806 = NOVALUE;
    DeRefi(_s_inlined_putn_at_610_18948);
    _s_inlined_putn_at_610_18948 = _10807;
    _10807 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_48current_db_17843, _s_inlined_putn_at_610_18948); // DJP 

    /** end procedure*/
    goto L15; // [624] 627
L15: 
    DeRefi(_s_inlined_putn_at_610_18948);
    _s_inlined_putn_at_610_18948 = NOVALUE;

    /** 	return DB_OK*/
    DeRefDS(_path_18866);
    return 0;
    ;
}


int _48db_open(int _path_18951, int _lock_method_18952)
{
    int _db_18953 = NOVALUE;
    int _magic_18954 = NOVALUE;
    int _lock_file_1__tmp_at161_18981 = NOVALUE;
    int _lock_file_inlined_lock_file_at_161_18980 = NOVALUE;
    int _lock_file_1__tmp_at207_18988 = NOVALUE;
    int _lock_file_inlined_lock_file_at_207_18987 = NOVALUE;
    int _10818 = NOVALUE;
    int _10816 = NOVALUE;
    int _10814 = NOVALUE;
    int _10812 = NOVALUE;
    int _10811 = NOVALUE;
    int _10809 = NOVALUE;
    int _0, _1, _2;
    

    /** 	db = find(path, Known_Aliases)*/
    _db_18953 = find_from(_path_18951, _48Known_Aliases_17864, 1);

    /** 	if db then*/
    if (_db_18953 == 0)
    {
        goto L1; // [16] 56
    }
    else{
    }

    /** 		path = Alias_Details[db][1]*/
    _2 = (int)SEQ_PTR(_48Alias_Details_17865);
    _10809 = (int)*(((s1_ptr)_2)->base + _db_18953);
    DeRefDS(_path_18951);
    _2 = (int)SEQ_PTR(_10809);
    _path_18951 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_path_18951);
    _10809 = NOVALUE;

    /** 		lock_method = Alias_Details[db][2][CONNECT_LOCK]*/
    _2 = (int)SEQ_PTR(_48Alias_Details_17865);
    _10811 = (int)*(((s1_ptr)_2)->base + _db_18953);
    _2 = (int)SEQ_PTR(_10811);
    _10812 = (int)*(((s1_ptr)_2)->base + 2);
    _10811 = NOVALUE;
    _2 = (int)SEQ_PTR(_10812);
    _lock_method_18952 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_lock_method_18952)){
        _lock_method_18952 = (long)DBL_PTR(_lock_method_18952)->dbl;
    }
    _10812 = NOVALUE;
    goto L2; // [53] 74
L1: 

    /** 		path = filesys:canonical_path( filesys:defaultext(path, "edb") )*/
    RefDS(_path_18951);
    RefDS(_10761);
    _10814 = _9defaultext(_path_18951, _10761);
    _0 = _path_18951;
    _path_18951 = _9canonical_path(_10814, 0, 0);
    DeRefDS(_0);
    _10814 = NOVALUE;
L2: 

    /** 	if lock_method = DB_LOCK_NO or*/
    _10816 = (_lock_method_18952 == 0);
    if (_10816 != 0) {
        goto L3; // [82] 97
    }
    _10818 = (_lock_method_18952 == 2);
    if (_10818 == 0)
    {
        DeRef(_10818);
        _10818 = NOVALUE;
        goto L4; // [93] 107
    }
    else{
        DeRef(_10818);
        _10818 = NOVALUE;
    }
L3: 

    /** 		db = open(path, "ub")*/
    _db_18953 = EOpen(_path_18951, _10788, 0);
    goto L5; // [104] 115
L4: 

    /** 		db = open(path, "rb")*/
    _db_18953 = EOpen(_path_18951, _10782, 0);
L5: 

    /** ifdef WINDOWS then*/

    /** 	if lock_method = DB_LOCK_SHARED then*/
    if (_lock_method_18952 != 1)
    goto L6; // [121] 135

    /** 		lock_method = DB_LOCK_EXCLUSIVE*/
    _lock_method_18952 = 2;
L6: 

    /** 	if db = -1 then*/
    if (_db_18953 != -1)
    goto L7; // [137] 150

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_path_18951);
    DeRef(_10816);
    _10816 = NOVALUE;
    return -1;
L7: 

    /** 	if lock_method = DB_LOCK_EXCLUSIVE then*/
    if (_lock_method_18952 != 2)
    goto L8; // [154] 196

    /** 		if not io:lock_file(db, io:LOCK_EXCLUSIVE, {}) then*/

    /** 	return machine_func(M_LOCK_FILE, {fn, t, r})*/
    _0 = _lock_file_1__tmp_at161_18981;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _db_18953;
    *((int *)(_2+8)) = 2;
    RefDS(_5);
    *((int *)(_2+12)) = _5;
    _lock_file_1__tmp_at161_18981 = MAKE_SEQ(_1);
    DeRef(_0);
    _lock_file_inlined_lock_file_at_161_18980 = machine(61, _lock_file_1__tmp_at161_18981);
    DeRef(_lock_file_1__tmp_at161_18981);
    _lock_file_1__tmp_at161_18981 = NOVALUE;
    if (_lock_file_inlined_lock_file_at_161_18980 != 0)
    goto L9; // [177] 241

    /** 			close(db)*/
    EClose(_db_18953);

    /** 			return DB_LOCK_FAIL*/
    DeRefDS(_path_18951);
    DeRef(_10816);
    _10816 = NOVALUE;
    return -3;
    goto L9; // [193] 241
L8: 

    /** 	elsif lock_method = DB_LOCK_SHARED then*/
    if (_lock_method_18952 != 1)
    goto LA; // [200] 240

    /** 		if not io:lock_file(db, io:LOCK_SHARED, {}) then*/

    /** 	return machine_func(M_LOCK_FILE, {fn, t, r})*/
    _0 = _lock_file_1__tmp_at207_18988;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _db_18953;
    *((int *)(_2+8)) = 1;
    RefDS(_5);
    *((int *)(_2+12)) = _5;
    _lock_file_1__tmp_at207_18988 = MAKE_SEQ(_1);
    DeRef(_0);
    _lock_file_inlined_lock_file_at_207_18987 = machine(61, _lock_file_1__tmp_at207_18988);
    DeRef(_lock_file_1__tmp_at207_18988);
    _lock_file_1__tmp_at207_18988 = NOVALUE;
    if (_lock_file_inlined_lock_file_at_207_18987 != 0)
    goto LB; // [223] 239

    /** 			close(db)*/
    EClose(_db_18953);

    /** 			return DB_LOCK_FAIL*/
    DeRefDS(_path_18951);
    DeRef(_10816);
    _10816 = NOVALUE;
    return -3;
LB: 
LA: 
L9: 

    /** 	magic = getc(db)*/
    if (_db_18953 != last_r_file_no) {
        last_r_file_ptr = which_file(_db_18953, EF_READ);
        last_r_file_no = _db_18953;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _magic_18954 = getKBchar();
        }
        else
        _magic_18954 = getc(last_r_file_ptr);
    }
    else
    _magic_18954 = getc(last_r_file_ptr);

    /** 	if magic != DB_MAGIC then*/
    if (_magic_18954 == 77)
    goto LC; // [248] 265

    /** 		close(db)*/
    EClose(_db_18953);

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_path_18951);
    DeRef(_10816);
    _10816 = NOVALUE;
    return -1;
LC: 

    /** 	save_keys()*/
    _48save_keys();

    /** 	current_db = db */
    _48current_db_17843 = _db_18953;

    /** 	current_table_pos = -1*/
    DeRef(_48current_table_pos_17844);
    _48current_table_pos_17844 = -1;

    /** 	current_table_name = ""*/
    RefDS(_5);
    DeRef(_48current_table_name_17845);
    _48current_table_name_17845 = _5;

    /** 	current_lock = lock_method*/
    _48current_lock_17849 = _lock_method_18952;

    /** 	db_names = append(db_names, path)*/
    RefDS(_path_18951);
    Append(&_48db_names_17846, _48db_names_17846, _path_18951);

    /** 	db_lock_methods = append(db_lock_methods, lock_method)*/
    Append(&_48db_lock_methods_17848, _48db_lock_methods_17848, _lock_method_18952);

    /** 	db_file_nums = append(db_file_nums, db)*/
    Append(&_48db_file_nums_17847, _48db_file_nums_17847, _db_18953);

    /** 	return DB_OK*/
    DeRefDS(_path_18951);
    DeRef(_10816);
    _10816 = NOVALUE;
    return 0;
    ;
}


int _48db_select(int _path_18998, int _lock_method_18999)
{
    int _index_19000 = NOVALUE;
    int _10838 = NOVALUE;
    int _10836 = NOVALUE;
    int _10835 = NOVALUE;
    int _10833 = NOVALUE;
    int _0, _1, _2;
    

    /** 	index = find(path, Known_Aliases)*/
    _index_19000 = find_from(_path_18998, _48Known_Aliases_17864, 1);

    /** 	if index then*/
    if (_index_19000 == 0)
    {
        goto L1; // [16] 56
    }
    else{
    }

    /** 		path = Alias_Details[index][1]*/
    _2 = (int)SEQ_PTR(_48Alias_Details_17865);
    _10833 = (int)*(((s1_ptr)_2)->base + _index_19000);
    DeRefDS(_path_18998);
    _2 = (int)SEQ_PTR(_10833);
    _path_18998 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_path_18998);
    _10833 = NOVALUE;

    /** 		lock_method = Alias_Details[index][2][CONNECT_LOCK]*/
    _2 = (int)SEQ_PTR(_48Alias_Details_17865);
    _10835 = (int)*(((s1_ptr)_2)->base + _index_19000);
    _2 = (int)SEQ_PTR(_10835);
    _10836 = (int)*(((s1_ptr)_2)->base + 2);
    _10835 = NOVALUE;
    _2 = (int)SEQ_PTR(_10836);
    _lock_method_18999 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_lock_method_18999)){
        _lock_method_18999 = (long)DBL_PTR(_lock_method_18999)->dbl;
    }
    _10836 = NOVALUE;
    goto L2; // [53] 74
L1: 

    /** 		path = filesys:canonical_path( filesys:defaultext(path, "edb") )*/
    RefDS(_path_18998);
    RefDS(_10761);
    _10838 = _9defaultext(_path_18998, _10761);
    _0 = _path_18998;
    _path_18998 = _9canonical_path(_10838, 0, 0);
    DeRefDS(_0);
    _10838 = NOVALUE;
L2: 

    /** 	index = eu:find(path, db_names)*/
    _index_19000 = find_from(_path_18998, _48db_names_17846, 1);

    /** 	if index = 0 then*/
    if (_index_19000 != 0)
    goto L3; // [85] 138

    /** 		if lock_method = -1 then*/
    if (_lock_method_18999 != -1)
    goto L4; // [91] 104

    /** 			return DB_OPEN_FAIL*/
    DeRefDS(_path_18998);
    return -1;
L4: 

    /** 		index = db_open(path, lock_method)*/
    RefDS(_path_18998);
    _index_19000 = _48db_open(_path_18998, _lock_method_18999);
    if (!IS_ATOM_INT(_index_19000)) {
        _1 = (long)(DBL_PTR(_index_19000)->dbl);
        if (UNIQUE(DBL_PTR(_index_19000)) && (DBL_PTR(_index_19000)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_index_19000);
        _index_19000 = _1;
    }

    /** 		if index != DB_OK then*/
    if (_index_19000 == 0)
    goto L5; // [117] 128

    /** 			return index*/
    DeRefDS(_path_18998);
    return _index_19000;
L5: 

    /** 		index = eu:find(path, db_names)*/
    _index_19000 = find_from(_path_18998, _48db_names_17846, 1);
L3: 

    /** 	save_keys()*/
    _48save_keys();

    /** 	current_db = db_file_nums[index]*/
    _2 = (int)SEQ_PTR(_48db_file_nums_17847);
    _48current_db_17843 = (int)*(((s1_ptr)_2)->base + _index_19000);
    if (!IS_ATOM_INT(_48current_db_17843))
    _48current_db_17843 = (long)DBL_PTR(_48current_db_17843)->dbl;

    /** 	current_lock = db_lock_methods[index]*/
    _2 = (int)SEQ_PTR(_48db_lock_methods_17848);
    _48current_lock_17849 = (int)*(((s1_ptr)_2)->base + _index_19000);
    if (!IS_ATOM_INT(_48current_lock_17849))
    _48current_lock_17849 = (long)DBL_PTR(_48current_lock_17849)->dbl;

    /** 	current_table_pos = -1*/
    DeRef(_48current_table_pos_17844);
    _48current_table_pos_17844 = -1;

    /** 	current_table_name = ""*/
    RefDS(_5);
    DeRef(_48current_table_name_17845);
    _48current_table_name_17845 = _5;

    /** 	key_pointers = {}*/
    RefDS(_5);
    DeRef(_48key_pointers_17850);
    _48key_pointers_17850 = _5;

    /** 	return DB_OK*/
    DeRefDS(_path_18998);
    return 0;
    ;
}


void _48db_close()
{
    int _unlock_file_1__tmp_at25_19029 = NOVALUE;
    int _index_19024 = NOVALUE;
    int _10855 = NOVALUE;
    int _10854 = NOVALUE;
    int _10853 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if current_db = -1 then*/
    if (_48current_db_17843 != -1)
    goto L1; // [5] 15

    /** 		return*/
    return;
L1: 

    /** 	if current_lock then*/
    if (_48current_lock_17849 == 0)
    {
        goto L2; // [19] 43
    }
    else{
    }

    /** 		io:unlock_file(current_db, {})*/

    /** 	machine_proc(M_UNLOCK_FILE, {fn, r})*/
    RefDS(_5);
    DeRef(_unlock_file_1__tmp_at25_19029);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _5;
    _unlock_file_1__tmp_at25_19029 = MAKE_SEQ(_1);
    machine(62, _unlock_file_1__tmp_at25_19029);

    /** end procedure*/
    goto L3; // [37] 40
L3: 
    DeRef(_unlock_file_1__tmp_at25_19029);
    _unlock_file_1__tmp_at25_19029 = NOVALUE;
L2: 

    /** 	close(current_db)*/
    EClose(_48current_db_17843);

    /** 	index = eu:find(current_db, db_file_nums)*/
    _index_19024 = find_from(_48current_db_17843, _48db_file_nums_17847, 1);

    /** 	db_names = remove(db_names, index)*/
    {
        s1_ptr assign_space = SEQ_PTR(_48db_names_17846);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_index_19024)) ? _index_19024 : (long)(DBL_PTR(_index_19024)->dbl);
        int stop = (IS_ATOM_INT(_index_19024)) ? _index_19024 : (long)(DBL_PTR(_index_19024)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_48db_names_17846), start, &_48db_names_17846 );
            }
            else Tail(SEQ_PTR(_48db_names_17846), stop+1, &_48db_names_17846);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_48db_names_17846), start, &_48db_names_17846);
        }
        else {
            assign_slice_seq = &assign_space;
            _48db_names_17846 = Remove_elements(start, stop, (SEQ_PTR(_48db_names_17846)->ref == 1));
        }
    }

    /** 	db_file_nums = remove(db_file_nums, index)*/
    {
        s1_ptr assign_space = SEQ_PTR(_48db_file_nums_17847);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_index_19024)) ? _index_19024 : (long)(DBL_PTR(_index_19024)->dbl);
        int stop = (IS_ATOM_INT(_index_19024)) ? _index_19024 : (long)(DBL_PTR(_index_19024)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_48db_file_nums_17847), start, &_48db_file_nums_17847 );
            }
            else Tail(SEQ_PTR(_48db_file_nums_17847), stop+1, &_48db_file_nums_17847);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_48db_file_nums_17847), start, &_48db_file_nums_17847);
        }
        else {
            assign_slice_seq = &assign_space;
            _48db_file_nums_17847 = Remove_elements(start, stop, (SEQ_PTR(_48db_file_nums_17847)->ref == 1));
        }
    }

    /** 	db_lock_methods = remove(db_lock_methods, index)*/
    {
        s1_ptr assign_space = SEQ_PTR(_48db_lock_methods_17848);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_index_19024)) ? _index_19024 : (long)(DBL_PTR(_index_19024)->dbl);
        int stop = (IS_ATOM_INT(_index_19024)) ? _index_19024 : (long)(DBL_PTR(_index_19024)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_48db_lock_methods_17848), start, &_48db_lock_methods_17848 );
            }
            else Tail(SEQ_PTR(_48db_lock_methods_17848), stop+1, &_48db_lock_methods_17848);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_48db_lock_methods_17848), start, &_48db_lock_methods_17848);
        }
        else {
            assign_slice_seq = &assign_space;
            _48db_lock_methods_17848 = Remove_elements(start, stop, (SEQ_PTR(_48db_lock_methods_17848)->ref == 1));
        }
    }

    /** 	for i = length(cache_index) to 1 by -1 do*/
    if (IS_SEQUENCE(_48cache_index_17852)){
            _10853 = SEQ_PTR(_48cache_index_17852)->length;
    }
    else {
        _10853 = 1;
    }
    {
        int _i_19035;
        _i_19035 = _10853;
L4: 
        if (_i_19035 < 1){
            goto L5; // [94] 145
        }

        /** 		if cache_index[i][1] = current_db then*/
        _2 = (int)SEQ_PTR(_48cache_index_17852);
        _10854 = (int)*(((s1_ptr)_2)->base + _i_19035);
        _2 = (int)SEQ_PTR(_10854);
        _10855 = (int)*(((s1_ptr)_2)->base + 1);
        _10854 = NOVALUE;
        if (binary_op_a(NOTEQ, _10855, _48current_db_17843)){
            _10855 = NOVALUE;
            goto L6; // [115] 138
        }
        _10855 = NOVALUE;

        /** 			cache_index = remove(cache_index, i)*/
        {
            s1_ptr assign_space = SEQ_PTR(_48cache_index_17852);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_i_19035)) ? _i_19035 : (long)(DBL_PTR(_i_19035)->dbl);
            int stop = (IS_ATOM_INT(_i_19035)) ? _i_19035 : (long)(DBL_PTR(_i_19035)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_48cache_index_17852), start, &_48cache_index_17852 );
                }
                else Tail(SEQ_PTR(_48cache_index_17852), stop+1, &_48cache_index_17852);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_48cache_index_17852), start, &_48cache_index_17852);
            }
            else {
                assign_slice_seq = &assign_space;
                _48cache_index_17852 = Remove_elements(start, stop, (SEQ_PTR(_48cache_index_17852)->ref == 1));
            }
        }

        /** 			key_cache = remove(key_cache, i)*/
        {
            s1_ptr assign_space = SEQ_PTR(_48key_cache_17851);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_i_19035)) ? _i_19035 : (long)(DBL_PTR(_i_19035)->dbl);
            int stop = (IS_ATOM_INT(_i_19035)) ? _i_19035 : (long)(DBL_PTR(_i_19035)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_48key_cache_17851), start, &_48key_cache_17851 );
                }
                else Tail(SEQ_PTR(_48key_cache_17851), stop+1, &_48key_cache_17851);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_48key_cache_17851), start, &_48key_cache_17851);
            }
            else {
                assign_slice_seq = &assign_space;
                _48key_cache_17851 = Remove_elements(start, stop, (SEQ_PTR(_48key_cache_17851)->ref == 1));
            }
        }
L6: 

        /** 	end for*/
        _i_19035 = _i_19035 + -1;
        goto L4; // [140] 101
L5: 
        ;
    }

    /** 	current_table_pos = -1*/
    DeRef(_48current_table_pos_17844);
    _48current_table_pos_17844 = -1;

    /** 	current_table_name = ""	*/
    RefDS(_5);
    DeRef(_48current_table_name_17845);
    _48current_table_name_17845 = _5;

    /** 	current_db = -1*/
    _48current_db_17843 = -1;

    /** 	key_pointers = {}*/
    RefDS(_5);
    DeRef(_48key_pointers_17850);
    _48key_pointers_17850 = _5;

    /** end procedure*/
    return;
    ;
}


int _48table_find(int _name_19045)
{
    int _tables_19046 = NOVALUE;
    int _nt_19047 = NOVALUE;
    int _t_header_19048 = NOVALUE;
    int _name_ptr_19049 = NOVALUE;
    int _seek_1__tmp_at6_19052 = NOVALUE;
    int _seek_inlined_seek_at_6_19051 = NOVALUE;
    int _seek_1__tmp_at44_19059 = NOVALUE;
    int _seek_inlined_seek_at_44_19058 = NOVALUE;
    int _seek_1__tmp_at84_19067 = NOVALUE;
    int _seek_inlined_seek_at_84_19066 = NOVALUE;
    int _seek_1__tmp_at106_19071 = NOVALUE;
    int _seek_inlined_seek_at_106_19070 = NOVALUE;
    int _10866 = NOVALUE;
    int _10864 = NOVALUE;
    int _10859 = NOVALUE;
    int _0, _1, _2;
    

    /** 	io:seek(current_db, TABLE_HEADERS)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at6_19052);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = 3;
    _seek_1__tmp_at6_19052 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_6_19051 = machine(19, _seek_1__tmp_at6_19052);
    DeRefi(_seek_1__tmp_at6_19052);
    _seek_1__tmp_at6_19052 = NOVALUE;

    /** 	if length(vLastErrors) > 0 then return -1 end if*/
    if (IS_SEQUENCE(_48vLastErrors_17867)){
            _10859 = SEQ_PTR(_48vLastErrors_17867)->length;
    }
    else {
        _10859 = 1;
    }
    if (_10859 <= 0)
    goto L1; // [27] 36
    DeRefDS(_name_19045);
    DeRef(_tables_19046);
    DeRef(_nt_19047);
    DeRef(_t_header_19048);
    DeRef(_name_ptr_19049);
    return -1;
L1: 

    /** 	tables = get4()*/
    _0 = _tables_19046;
    _tables_19046 = _48get4();
    DeRef(_0);

    /** 	io:seek(current_db, tables)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_tables_19046);
    DeRef(_seek_1__tmp_at44_19059);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _tables_19046;
    _seek_1__tmp_at44_19059 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_44_19058 = machine(19, _seek_1__tmp_at44_19059);
    DeRef(_seek_1__tmp_at44_19059);
    _seek_1__tmp_at44_19059 = NOVALUE;

    /** 	nt = get4()*/
    _0 = _nt_19047;
    _nt_19047 = _48get4();
    DeRef(_0);

    /** 	t_header = tables+4*/
    DeRef(_t_header_19048);
    if (IS_ATOM_INT(_tables_19046)) {
        _t_header_19048 = _tables_19046 + 4;
        if ((long)((unsigned long)_t_header_19048 + (unsigned long)HIGH_BITS) >= 0) 
        _t_header_19048 = NewDouble((double)_t_header_19048);
    }
    else {
        _t_header_19048 = NewDouble(DBL_PTR(_tables_19046)->dbl + (double)4);
    }

    /** 	for i = 1 to nt do*/
    Ref(_nt_19047);
    DeRef(_10864);
    _10864 = _nt_19047;
    {
        int _i_19063;
        _i_19063 = 1;
L2: 
        if (binary_op_a(GREATER, _i_19063, _10864)){
            goto L3; // [74] 150
        }

        /** 		io:seek(current_db, t_header)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_t_header_19048);
        DeRef(_seek_1__tmp_at84_19067);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _48current_db_17843;
        ((int *)_2)[2] = _t_header_19048;
        _seek_1__tmp_at84_19067 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_84_19066 = machine(19, _seek_1__tmp_at84_19067);
        DeRef(_seek_1__tmp_at84_19067);
        _seek_1__tmp_at84_19067 = NOVALUE;

        /** 		name_ptr = get4()*/
        _0 = _name_ptr_19049;
        _name_ptr_19049 = _48get4();
        DeRef(_0);

        /** 		io:seek(current_db, name_ptr)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_name_ptr_19049);
        DeRef(_seek_1__tmp_at106_19071);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _48current_db_17843;
        ((int *)_2)[2] = _name_ptr_19049;
        _seek_1__tmp_at106_19071 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_106_19070 = machine(19, _seek_1__tmp_at106_19071);
        DeRef(_seek_1__tmp_at106_19071);
        _seek_1__tmp_at106_19071 = NOVALUE;

        /** 		if equal_string(name) > 0 then*/
        RefDS(_name_19045);
        _10866 = _48equal_string(_name_19045);
        if (binary_op_a(LESSEQ, _10866, 0)){
            DeRef(_10866);
            _10866 = NOVALUE;
            goto L4; // [126] 137
        }
        DeRef(_10866);
        _10866 = NOVALUE;

        /** 			return t_header*/
        DeRef(_i_19063);
        DeRefDS(_name_19045);
        DeRef(_tables_19046);
        DeRef(_nt_19047);
        DeRef(_name_ptr_19049);
        return _t_header_19048;
L4: 

        /** 		t_header += SIZEOF_TABLE_HEADER*/
        _0 = _t_header_19048;
        if (IS_ATOM_INT(_t_header_19048)) {
            _t_header_19048 = _t_header_19048 + 16;
            if ((long)((unsigned long)_t_header_19048 + (unsigned long)HIGH_BITS) >= 0) 
            _t_header_19048 = NewDouble((double)_t_header_19048);
        }
        else {
            _t_header_19048 = NewDouble(DBL_PTR(_t_header_19048)->dbl + (double)16);
        }
        DeRef(_0);

        /** 	end for*/
        _0 = _i_19063;
        if (IS_ATOM_INT(_i_19063)) {
            _i_19063 = _i_19063 + 1;
            if ((long)((unsigned long)_i_19063 +(unsigned long) HIGH_BITS) >= 0){
                _i_19063 = NewDouble((double)_i_19063);
            }
        }
        else {
            _i_19063 = binary_op_a(PLUS, _i_19063, 1);
        }
        DeRef(_0);
        goto L2; // [145] 81
L3: 
        ;
        DeRef(_i_19063);
    }

    /** 	return -1*/
    DeRefDS(_name_19045);
    DeRef(_tables_19046);
    DeRef(_nt_19047);
    DeRef(_t_header_19048);
    DeRef(_name_ptr_19049);
    return -1;
    ;
}


int _48db_select_table(int _name_19078)
{
    int _table_19079 = NOVALUE;
    int _nkeys_19080 = NOVALUE;
    int _index_19081 = NOVALUE;
    int _block_ptr_19082 = NOVALUE;
    int _block_size_19083 = NOVALUE;
    int _blocks_19084 = NOVALUE;
    int _k_19085 = NOVALUE;
    int _seek_1__tmp_at124_19104 = NOVALUE;
    int _seek_inlined_seek_at_124_19103 = NOVALUE;
    int _pos_inlined_seek_at_121_19102 = NOVALUE;
    int _seek_1__tmp_at182_19114 = NOVALUE;
    int _seek_inlined_seek_at_182_19113 = NOVALUE;
    int _seek_1__tmp_at209_19119 = NOVALUE;
    int _seek_inlined_seek_at_209_19118 = NOVALUE;
    int _10887 = NOVALUE;
    int _10886 = NOVALUE;
    int _10883 = NOVALUE;
    int _10878 = NOVALUE;
    int _10873 = NOVALUE;
    int _10869 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if equal(current_table_name, name) then*/
    if (_48current_table_name_17845 == _name_19078)
    _10869 = 1;
    else if (IS_ATOM_INT(_48current_table_name_17845) && IS_ATOM_INT(_name_19078))
    _10869 = 0;
    else
    _10869 = (compare(_48current_table_name_17845, _name_19078) == 0);
    if (_10869 == 0)
    {
        _10869 = NOVALUE;
        goto L1; // [11] 23
    }
    else{
        _10869 = NOVALUE;
    }

    /** 		return DB_OK*/
    DeRefDS(_name_19078);
    DeRef(_table_19079);
    DeRef(_nkeys_19080);
    DeRef(_index_19081);
    DeRef(_block_ptr_19082);
    DeRef(_block_size_19083);
    return 0;
L1: 

    /** 	table = table_find(name)*/
    RefDS(_name_19078);
    _0 = _table_19079;
    _table_19079 = _48table_find(_name_19078);
    DeRef(_0);

    /** 	if table = -1 then*/
    if (binary_op_a(NOTEQ, _table_19079, -1)){
        goto L2; // [31] 44
    }

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_name_19078);
    DeRef(_table_19079);
    DeRef(_nkeys_19080);
    DeRef(_index_19081);
    DeRef(_block_ptr_19082);
    DeRef(_block_size_19083);
    return -1;
L2: 

    /** 	save_keys()*/
    _48save_keys();

    /** 	current_table_pos = table*/
    Ref(_table_19079);
    DeRef(_48current_table_pos_17844);
    _48current_table_pos_17844 = _table_19079;

    /** 	current_table_name = name*/
    RefDS(_name_19078);
    DeRef(_48current_table_name_17845);
    _48current_table_name_17845 = _name_19078;

    /** 	k = 0*/
    _k_19085 = 0;

    /** 	if caching_option = 1 then*/

    /** 		k = eu:find({current_db, current_table_pos}, cache_index)*/
    Ref(_48current_table_pos_17844);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _48current_table_pos_17844;
    _10873 = MAKE_SEQ(_1);
    _k_19085 = find_from(_10873, _48cache_index_17852, 1);
    DeRefDS(_10873);
    _10873 = NOVALUE;

    /** 		if k != 0 then*/
    if (_k_19085 == 0)
    goto L3; // [92] 107

    /** 			key_pointers = key_cache[k]*/
    DeRef(_48key_pointers_17850);
    _2 = (int)SEQ_PTR(_48key_cache_17851);
    _48key_pointers_17850 = (int)*(((s1_ptr)_2)->base + _k_19085);
    Ref(_48key_pointers_17850);
L3: 

    /** 	if k = 0 then*/
    if (_k_19085 != 0)
    goto L4; // [110] 273

    /** 		io:seek(current_db, table+4)*/
    if (IS_ATOM_INT(_table_19079)) {
        _10878 = _table_19079 + 4;
        if ((long)((unsigned long)_10878 + (unsigned long)HIGH_BITS) >= 0) 
        _10878 = NewDouble((double)_10878);
    }
    else {
        _10878 = NewDouble(DBL_PTR(_table_19079)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_121_19102);
    _pos_inlined_seek_at_121_19102 = _10878;
    _10878 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_121_19102);
    DeRef(_seek_1__tmp_at124_19104);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_121_19102;
    _seek_1__tmp_at124_19104 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_124_19103 = machine(19, _seek_1__tmp_at124_19104);
    DeRef(_pos_inlined_seek_at_121_19102);
    _pos_inlined_seek_at_121_19102 = NOVALUE;
    DeRef(_seek_1__tmp_at124_19104);
    _seek_1__tmp_at124_19104 = NOVALUE;

    /** 		nkeys = get4()*/
    _0 = _nkeys_19080;
    _nkeys_19080 = _48get4();
    DeRef(_0);

    /** 		blocks = get4()*/
    _blocks_19084 = _48get4();
    if (!IS_ATOM_INT(_blocks_19084)) {
        _1 = (long)(DBL_PTR(_blocks_19084)->dbl);
        if (UNIQUE(DBL_PTR(_blocks_19084)) && (DBL_PTR(_blocks_19084)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_blocks_19084);
        _blocks_19084 = _1;
    }

    /** 		index = get4()*/
    _0 = _index_19081;
    _index_19081 = _48get4();
    DeRef(_0);

    /** 		key_pointers = repeat(0, nkeys)*/
    DeRef(_48key_pointers_17850);
    _48key_pointers_17850 = Repeat(0, _nkeys_19080);

    /** 		k = 1*/
    _k_19085 = 1;

    /** 		for b = 0 to blocks-1 do*/
    _10883 = _blocks_19084 - 1;
    if ((long)((unsigned long)_10883 +(unsigned long) HIGH_BITS) >= 0){
        _10883 = NewDouble((double)_10883);
    }
    {
        int _b_19110;
        _b_19110 = 0;
L5: 
        if (binary_op_a(GREATER, _b_19110, _10883)){
            goto L6; // [172] 272
        }

        /** 			io:seek(current_db, index)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_index_19081);
        DeRef(_seek_1__tmp_at182_19114);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _48current_db_17843;
        ((int *)_2)[2] = _index_19081;
        _seek_1__tmp_at182_19114 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_182_19113 = machine(19, _seek_1__tmp_at182_19114);
        DeRef(_seek_1__tmp_at182_19114);
        _seek_1__tmp_at182_19114 = NOVALUE;

        /** 			block_size = get4()*/
        _0 = _block_size_19083;
        _block_size_19083 = _48get4();
        DeRef(_0);

        /** 			block_ptr = get4()*/
        _0 = _block_ptr_19082;
        _block_ptr_19082 = _48get4();
        DeRef(_0);

        /** 			io:seek(current_db, block_ptr)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_block_ptr_19082);
        DeRef(_seek_1__tmp_at209_19119);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _48current_db_17843;
        ((int *)_2)[2] = _block_ptr_19082;
        _seek_1__tmp_at209_19119 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_209_19118 = machine(19, _seek_1__tmp_at209_19119);
        DeRef(_seek_1__tmp_at209_19119);
        _seek_1__tmp_at209_19119 = NOVALUE;

        /** 			for j = 1 to block_size do*/
        Ref(_block_size_19083);
        DeRef(_10886);
        _10886 = _block_size_19083;
        {
            int _j_19121;
            _j_19121 = 1;
L7: 
            if (binary_op_a(GREATER, _j_19121, _10886)){
                goto L8; // [228] 259
            }

            /** 				key_pointers[k] = get4()*/
            _10887 = _48get4();
            _2 = (int)SEQ_PTR(_48key_pointers_17850);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _48key_pointers_17850 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _k_19085);
            _1 = *(int *)_2;
            *(int *)_2 = _10887;
            if( _1 != _10887 ){
                DeRef(_1);
            }
            _10887 = NOVALUE;

            /** 				k += 1*/
            _k_19085 = _k_19085 + 1;

            /** 			end for*/
            _0 = _j_19121;
            if (IS_ATOM_INT(_j_19121)) {
                _j_19121 = _j_19121 + 1;
                if ((long)((unsigned long)_j_19121 +(unsigned long) HIGH_BITS) >= 0){
                    _j_19121 = NewDouble((double)_j_19121);
                }
            }
            else {
                _j_19121 = binary_op_a(PLUS, _j_19121, 1);
            }
            DeRef(_0);
            goto L7; // [254] 235
L8: 
            ;
            DeRef(_j_19121);
        }

        /** 			index += 8*/
        _0 = _index_19081;
        if (IS_ATOM_INT(_index_19081)) {
            _index_19081 = _index_19081 + 8;
            if ((long)((unsigned long)_index_19081 + (unsigned long)HIGH_BITS) >= 0) 
            _index_19081 = NewDouble((double)_index_19081);
        }
        else {
            _index_19081 = NewDouble(DBL_PTR(_index_19081)->dbl + (double)8);
        }
        DeRef(_0);

        /** 		end for*/
        _0 = _b_19110;
        if (IS_ATOM_INT(_b_19110)) {
            _b_19110 = _b_19110 + 1;
            if ((long)((unsigned long)_b_19110 +(unsigned long) HIGH_BITS) >= 0){
                _b_19110 = NewDouble((double)_b_19110);
            }
        }
        else {
            _b_19110 = binary_op_a(PLUS, _b_19110, 1);
        }
        DeRef(_0);
        goto L5; // [267] 179
L6: 
        ;
        DeRef(_b_19110);
    }
L4: 

    /** 	return DB_OK*/
    DeRefDS(_name_19078);
    DeRef(_table_19079);
    DeRef(_nkeys_19080);
    DeRef(_index_19081);
    DeRef(_block_ptr_19082);
    DeRef(_block_size_19083);
    DeRef(_10883);
    _10883 = NOVALUE;
    return 0;
    ;
}


int _48db_create_table(int _name_19130, int _init_records_19131)
{
    int _name_ptr_19132 = NOVALUE;
    int _nt_19133 = NOVALUE;
    int _tables_19134 = NOVALUE;
    int _newtables_19135 = NOVALUE;
    int _table_19136 = NOVALUE;
    int _records_ptr_19137 = NOVALUE;
    int _size_19138 = NOVALUE;
    int _newsize_19139 = NOVALUE;
    int _index_ptr_19140 = NOVALUE;
    int _remaining_19141 = NOVALUE;
    int _init_index_19142 = NOVALUE;
    int _seek_1__tmp_at72_19156 = NOVALUE;
    int _seek_inlined_seek_at_72_19155 = NOVALUE;
    int _seek_1__tmp_at101_19162 = NOVALUE;
    int _seek_inlined_seek_at_101_19161 = NOVALUE;
    int _pos_inlined_seek_at_98_19160 = NOVALUE;
    int _put4_1__tmp_at163_19175 = NOVALUE;
    int _seek_1__tmp_at200_19180 = NOVALUE;
    int _seek_inlined_seek_at_200_19179 = NOVALUE;
    int _pos_inlined_seek_at_197_19178 = NOVALUE;
    int _seek_1__tmp_at243_19188 = NOVALUE;
    int _seek_inlined_seek_at_243_19187 = NOVALUE;
    int _pos_inlined_seek_at_240_19186 = NOVALUE;
    int _s_inlined_putn_at_292_19196 = NOVALUE;
    int _seek_1__tmp_at320_19199 = NOVALUE;
    int _seek_inlined_seek_at_320_19198 = NOVALUE;
    int _put4_1__tmp_at335_19201 = NOVALUE;
    int _seek_1__tmp_at373_19205 = NOVALUE;
    int _seek_inlined_seek_at_373_19204 = NOVALUE;
    int _put4_1__tmp_at388_19207 = NOVALUE;
    int _s_inlined_putn_at_435_19213 = NOVALUE;
    int _put4_1__tmp_at466_19217 = NOVALUE;
    int _put4_1__tmp_at494_19219 = NOVALUE;
    int _s_inlined_putn_at_534_19224 = NOVALUE;
    int _s_inlined_putn_at_572_19230 = NOVALUE;
    int _seek_1__tmp_at614_19238 = NOVALUE;
    int _seek_inlined_seek_at_614_19237 = NOVALUE;
    int _pos_inlined_seek_at_611_19236 = NOVALUE;
    int _put4_1__tmp_at629_19240 = NOVALUE;
    int _put4_1__tmp_at657_19242 = NOVALUE;
    int _put4_1__tmp_at685_19244 = NOVALUE;
    int _put4_1__tmp_at713_19246 = NOVALUE;
    int _10936 = NOVALUE;
    int _10935 = NOVALUE;
    int _10934 = NOVALUE;
    int _10933 = NOVALUE;
    int _10932 = NOVALUE;
    int _10931 = NOVALUE;
    int _10929 = NOVALUE;
    int _10928 = NOVALUE;
    int _10927 = NOVALUE;
    int _10926 = NOVALUE;
    int _10925 = NOVALUE;
    int _10923 = NOVALUE;
    int _10922 = NOVALUE;
    int _10921 = NOVALUE;
    int _10919 = NOVALUE;
    int _10918 = NOVALUE;
    int _10917 = NOVALUE;
    int _10916 = NOVALUE;
    int _10915 = NOVALUE;
    int _10914 = NOVALUE;
    int _10913 = NOVALUE;
    int _10911 = NOVALUE;
    int _10910 = NOVALUE;
    int _10909 = NOVALUE;
    int _10906 = NOVALUE;
    int _10905 = NOVALUE;
    int _10903 = NOVALUE;
    int _10902 = NOVALUE;
    int _10900 = NOVALUE;
    int _10898 = NOVALUE;
    int _10895 = NOVALUE;
    int _10890 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not cstring(name) then*/
    RefDS(_name_19130);
    _10890 = _5cstring(_name_19130);
    if (IS_ATOM_INT(_10890)) {
        if (_10890 != 0){
            DeRef(_10890);
            _10890 = NOVALUE;
            goto L1; // [11] 23
        }
    }
    else {
        if (DBL_PTR(_10890)->dbl != 0.0){
            DeRef(_10890);
            _10890 = NOVALUE;
            goto L1; // [11] 23
        }
    }
    DeRef(_10890);
    _10890 = NOVALUE;

    /** 		return DB_BAD_NAME*/
    DeRefDS(_name_19130);
    DeRef(_name_ptr_19132);
    DeRef(_nt_19133);
    DeRef(_tables_19134);
    DeRef(_newtables_19135);
    DeRef(_table_19136);
    DeRef(_records_ptr_19137);
    DeRef(_size_19138);
    DeRef(_newsize_19139);
    DeRef(_index_ptr_19140);
    DeRef(_remaining_19141);
    return -4;
L1: 

    /** 	table = table_find(name)*/
    RefDS(_name_19130);
    _0 = _table_19136;
    _table_19136 = _48table_find(_name_19130);
    DeRef(_0);

    /** 	if table != -1 then*/
    if (binary_op_a(EQUALS, _table_19136, -1)){
        goto L2; // [31] 44
    }

    /** 		return DB_EXISTS_ALREADY*/
    DeRefDS(_name_19130);
    DeRef(_name_ptr_19132);
    DeRef(_nt_19133);
    DeRef(_tables_19134);
    DeRef(_newtables_19135);
    DeRef(_table_19136);
    DeRef(_records_ptr_19137);
    DeRef(_size_19138);
    DeRef(_newsize_19139);
    DeRef(_index_ptr_19140);
    DeRef(_remaining_19141);
    return -2;
L2: 

    /** 	if init_records < 1 then*/
    if (_init_records_19131 >= 1)
    goto L3; // [46] 56

    /** 		init_records = 1*/
    _init_records_19131 = 1;
L3: 

    /** 	init_index = math:min({init_records, MAX_INDEX})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _init_records_19131;
    ((int *)_2)[2] = 10;
    _10895 = MAKE_SEQ(_1);
    _init_index_19142 = _18min(_10895);
    _10895 = NOVALUE;
    if (!IS_ATOM_INT(_init_index_19142)) {
        _1 = (long)(DBL_PTR(_init_index_19142)->dbl);
        if (UNIQUE(DBL_PTR(_init_index_19142)) && (DBL_PTR(_init_index_19142)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_init_index_19142);
        _init_index_19142 = _1;
    }

    /** 	io:seek(current_db, TABLE_HEADERS)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at72_19156);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = 3;
    _seek_1__tmp_at72_19156 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_72_19155 = machine(19, _seek_1__tmp_at72_19156);
    DeRefi(_seek_1__tmp_at72_19156);
    _seek_1__tmp_at72_19156 = NOVALUE;

    /** 	tables = get4()*/
    _0 = _tables_19134;
    _tables_19134 = _48get4();
    DeRef(_0);

    /** 	io:seek(current_db, tables-4)*/
    if (IS_ATOM_INT(_tables_19134)) {
        _10898 = _tables_19134 - 4;
        if ((long)((unsigned long)_10898 +(unsigned long) HIGH_BITS) >= 0){
            _10898 = NewDouble((double)_10898);
        }
    }
    else {
        _10898 = NewDouble(DBL_PTR(_tables_19134)->dbl - (double)4);
    }
    DeRef(_pos_inlined_seek_at_98_19160);
    _pos_inlined_seek_at_98_19160 = _10898;
    _10898 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_98_19160);
    DeRef(_seek_1__tmp_at101_19162);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_98_19160;
    _seek_1__tmp_at101_19162 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_101_19161 = machine(19, _seek_1__tmp_at101_19162);
    DeRef(_pos_inlined_seek_at_98_19160);
    _pos_inlined_seek_at_98_19160 = NOVALUE;
    DeRef(_seek_1__tmp_at101_19162);
    _seek_1__tmp_at101_19162 = NOVALUE;

    /** 	size = get4()*/
    _0 = _size_19138;
    _size_19138 = _48get4();
    DeRef(_0);

    /** 	nt = get4()+1*/
    _10900 = _48get4();
    DeRef(_nt_19133);
    if (IS_ATOM_INT(_10900)) {
        _nt_19133 = _10900 + 1;
        if (_nt_19133 > MAXINT){
            _nt_19133 = NewDouble((double)_nt_19133);
        }
    }
    else
    _nt_19133 = binary_op(PLUS, 1, _10900);
    DeRef(_10900);
    _10900 = NOVALUE;

    /** 	if nt*SIZEOF_TABLE_HEADER + 8 > size then*/
    if (IS_ATOM_INT(_nt_19133)) {
        if (_nt_19133 == (short)_nt_19133)
        _10902 = _nt_19133 * 16;
        else
        _10902 = NewDouble(_nt_19133 * (double)16);
    }
    else {
        _10902 = NewDouble(DBL_PTR(_nt_19133)->dbl * (double)16);
    }
    if (IS_ATOM_INT(_10902)) {
        _10903 = _10902 + 8;
        if ((long)((unsigned long)_10903 + (unsigned long)HIGH_BITS) >= 0) 
        _10903 = NewDouble((double)_10903);
    }
    else {
        _10903 = NewDouble(DBL_PTR(_10902)->dbl + (double)8);
    }
    DeRef(_10902);
    _10902 = NOVALUE;
    if (binary_op_a(LESSEQ, _10903, _size_19138)){
        DeRef(_10903);
        _10903 = NOVALUE;
        goto L4; // [138] 369
    }
    DeRef(_10903);
    _10903 = NOVALUE;

    /** 		newsize = floor(size + size / 2)*/
    if (IS_ATOM_INT(_size_19138)) {
        if (_size_19138 & 1) {
            _10905 = NewDouble((_size_19138 >> 1) + 0.5);
        }
        else
        _10905 = _size_19138 >> 1;
    }
    else {
        _10905 = binary_op(DIVIDE, _size_19138, 2);
    }
    if (IS_ATOM_INT(_size_19138) && IS_ATOM_INT(_10905)) {
        _10906 = _size_19138 + _10905;
        if ((long)((unsigned long)_10906 + (unsigned long)HIGH_BITS) >= 0) 
        _10906 = NewDouble((double)_10906);
    }
    else {
        if (IS_ATOM_INT(_size_19138)) {
            _10906 = NewDouble((double)_size_19138 + DBL_PTR(_10905)->dbl);
        }
        else {
            if (IS_ATOM_INT(_10905)) {
                _10906 = NewDouble(DBL_PTR(_size_19138)->dbl + (double)_10905);
            }
            else
            _10906 = NewDouble(DBL_PTR(_size_19138)->dbl + DBL_PTR(_10905)->dbl);
        }
    }
    DeRef(_10905);
    _10905 = NOVALUE;
    DeRef(_newsize_19139);
    if (IS_ATOM_INT(_10906))
    _newsize_19139 = e_floor(_10906);
    else
    _newsize_19139 = unary_op(FLOOR, _10906);
    DeRef(_10906);
    _10906 = NOVALUE;

    /** 		newtables = db_allocate(newsize)*/
    Ref(_newsize_19139);
    _0 = _newtables_19135;
    _newtables_19135 = _48db_allocate(_newsize_19139);
    DeRef(_0);

    /** 		put4(nt)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_nt_19133)) {
        *poke4_addr = (unsigned long)_nt_19133;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_nt_19133)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at163_19175);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at163_19175 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at163_19175); // DJP 

    /** end procedure*/
    goto L5; // [184] 187
L5: 
    DeRefi(_put4_1__tmp_at163_19175);
    _put4_1__tmp_at163_19175 = NOVALUE;

    /** 		io:seek(current_db, tables+4)*/
    if (IS_ATOM_INT(_tables_19134)) {
        _10909 = _tables_19134 + 4;
        if ((long)((unsigned long)_10909 + (unsigned long)HIGH_BITS) >= 0) 
        _10909 = NewDouble((double)_10909);
    }
    else {
        _10909 = NewDouble(DBL_PTR(_tables_19134)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_197_19178);
    _pos_inlined_seek_at_197_19178 = _10909;
    _10909 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_197_19178);
    DeRef(_seek_1__tmp_at200_19180);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_197_19178;
    _seek_1__tmp_at200_19180 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_200_19179 = machine(19, _seek_1__tmp_at200_19180);
    DeRef(_pos_inlined_seek_at_197_19178);
    _pos_inlined_seek_at_197_19178 = NOVALUE;
    DeRef(_seek_1__tmp_at200_19180);
    _seek_1__tmp_at200_19180 = NOVALUE;

    /** 		remaining = io:get_bytes(current_db, (nt-1)*SIZEOF_TABLE_HEADER)*/
    if (IS_ATOM_INT(_nt_19133)) {
        _10910 = _nt_19133 - 1;
        if ((long)((unsigned long)_10910 +(unsigned long) HIGH_BITS) >= 0){
            _10910 = NewDouble((double)_10910);
        }
    }
    else {
        _10910 = NewDouble(DBL_PTR(_nt_19133)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_10910)) {
        if (_10910 == (short)_10910)
        _10911 = _10910 * 16;
        else
        _10911 = NewDouble(_10910 * (double)16);
    }
    else {
        _10911 = NewDouble(DBL_PTR(_10910)->dbl * (double)16);
    }
    DeRef(_10910);
    _10910 = NOVALUE;
    _0 = _remaining_19141;
    _remaining_19141 = _16get_bytes(_48current_db_17843, _10911);
    DeRef(_0);
    _10911 = NOVALUE;

    /** 		io:seek(current_db, newtables+4)*/
    if (IS_ATOM_INT(_newtables_19135)) {
        _10913 = _newtables_19135 + 4;
        if ((long)((unsigned long)_10913 + (unsigned long)HIGH_BITS) >= 0) 
        _10913 = NewDouble((double)_10913);
    }
    else {
        _10913 = NewDouble(DBL_PTR(_newtables_19135)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_240_19186);
    _pos_inlined_seek_at_240_19186 = _10913;
    _10913 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_240_19186);
    DeRef(_seek_1__tmp_at243_19188);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_240_19186;
    _seek_1__tmp_at243_19188 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_243_19187 = machine(19, _seek_1__tmp_at243_19188);
    DeRef(_pos_inlined_seek_at_240_19186);
    _pos_inlined_seek_at_240_19186 = NOVALUE;
    DeRef(_seek_1__tmp_at243_19188);
    _seek_1__tmp_at243_19188 = NOVALUE;

    /** 		putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_48current_db_17843, _remaining_19141); // DJP 

    /** end procedure*/
    goto L6; // [267] 270
L6: 

    /** 		putn(repeat(0, newsize - 4 - (nt-1)*SIZEOF_TABLE_HEADER))*/
    if (IS_ATOM_INT(_newsize_19139)) {
        _10914 = _newsize_19139 - 4;
        if ((long)((unsigned long)_10914 +(unsigned long) HIGH_BITS) >= 0){
            _10914 = NewDouble((double)_10914);
        }
    }
    else {
        _10914 = NewDouble(DBL_PTR(_newsize_19139)->dbl - (double)4);
    }
    if (IS_ATOM_INT(_nt_19133)) {
        _10915 = _nt_19133 - 1;
        if ((long)((unsigned long)_10915 +(unsigned long) HIGH_BITS) >= 0){
            _10915 = NewDouble((double)_10915);
        }
    }
    else {
        _10915 = NewDouble(DBL_PTR(_nt_19133)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_10915)) {
        if (_10915 == (short)_10915)
        _10916 = _10915 * 16;
        else
        _10916 = NewDouble(_10915 * (double)16);
    }
    else {
        _10916 = NewDouble(DBL_PTR(_10915)->dbl * (double)16);
    }
    DeRef(_10915);
    _10915 = NOVALUE;
    if (IS_ATOM_INT(_10914) && IS_ATOM_INT(_10916)) {
        _10917 = _10914 - _10916;
    }
    else {
        if (IS_ATOM_INT(_10914)) {
            _10917 = NewDouble((double)_10914 - DBL_PTR(_10916)->dbl);
        }
        else {
            if (IS_ATOM_INT(_10916)) {
                _10917 = NewDouble(DBL_PTR(_10914)->dbl - (double)_10916);
            }
            else
            _10917 = NewDouble(DBL_PTR(_10914)->dbl - DBL_PTR(_10916)->dbl);
        }
    }
    DeRef(_10914);
    _10914 = NOVALUE;
    DeRef(_10916);
    _10916 = NOVALUE;
    _10918 = Repeat(0, _10917);
    DeRef(_10917);
    _10917 = NOVALUE;
    DeRefi(_s_inlined_putn_at_292_19196);
    _s_inlined_putn_at_292_19196 = _10918;
    _10918 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_48current_db_17843, _s_inlined_putn_at_292_19196); // DJP 

    /** end procedure*/
    goto L7; // [306] 309
L7: 
    DeRefi(_s_inlined_putn_at_292_19196);
    _s_inlined_putn_at_292_19196 = NOVALUE;

    /** 		db_free(tables)*/
    Ref(_tables_19134);
    _48db_free(_tables_19134);

    /** 		io:seek(current_db, TABLE_HEADERS)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at320_19199);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = 3;
    _seek_1__tmp_at320_19199 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_320_19198 = machine(19, _seek_1__tmp_at320_19199);
    DeRefi(_seek_1__tmp_at320_19199);
    _seek_1__tmp_at320_19199 = NOVALUE;

    /** 		put4(newtables)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_newtables_19135)) {
        *poke4_addr = (unsigned long)_newtables_19135;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_newtables_19135)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at335_19201);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at335_19201 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at335_19201); // DJP 

    /** end procedure*/
    goto L8; // [356] 359
L8: 
    DeRefi(_put4_1__tmp_at335_19201);
    _put4_1__tmp_at335_19201 = NOVALUE;

    /** 		tables = newtables*/
    Ref(_newtables_19135);
    DeRef(_tables_19134);
    _tables_19134 = _newtables_19135;
    goto L9; // [366] 415
L4: 

    /** 		io:seek(current_db, tables)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_tables_19134);
    DeRef(_seek_1__tmp_at373_19205);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _tables_19134;
    _seek_1__tmp_at373_19205 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_373_19204 = machine(19, _seek_1__tmp_at373_19205);
    DeRef(_seek_1__tmp_at373_19205);
    _seek_1__tmp_at373_19205 = NOVALUE;

    /** 		put4(nt)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_nt_19133)) {
        *poke4_addr = (unsigned long)_nt_19133;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_nt_19133)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at388_19207);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at388_19207 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at388_19207); // DJP 

    /** end procedure*/
    goto LA; // [409] 412
LA: 
    DeRefi(_put4_1__tmp_at388_19207);
    _put4_1__tmp_at388_19207 = NOVALUE;
L9: 

    /** 	records_ptr = db_allocate(init_records * 4)*/
    if (_init_records_19131 == (short)_init_records_19131)
    _10919 = _init_records_19131 * 4;
    else
    _10919 = NewDouble(_init_records_19131 * (double)4);
    _0 = _records_ptr_19137;
    _records_ptr_19137 = _48db_allocate(_10919);
    DeRef(_0);
    _10919 = NOVALUE;

    /** 	putn(repeat(0, init_records * 4))*/
    _10921 = _init_records_19131 * 4;
    _10922 = Repeat(0, _10921);
    _10921 = NOVALUE;
    DeRefi(_s_inlined_putn_at_435_19213);
    _s_inlined_putn_at_435_19213 = _10922;
    _10922 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_48current_db_17843, _s_inlined_putn_at_435_19213); // DJP 

    /** end procedure*/
    goto LB; // [449] 452
LB: 
    DeRefi(_s_inlined_putn_at_435_19213);
    _s_inlined_putn_at_435_19213 = NOVALUE;

    /** 	index_ptr = db_allocate(init_index * 8)*/
    if (_init_index_19142 == (short)_init_index_19142)
    _10923 = _init_index_19142 * 8;
    else
    _10923 = NewDouble(_init_index_19142 * (double)8);
    _0 = _index_ptr_19140;
    _index_ptr_19140 = _48db_allocate(_10923);
    DeRef(_0);
    _10923 = NOVALUE;

    /** 	put4(0)  -- 0 records*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    *poke4_addr = (unsigned long)0;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at466_19217);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at466_19217 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at466_19217); // DJP 

    /** end procedure*/
    goto LC; // [487] 490
LC: 
    DeRefi(_put4_1__tmp_at466_19217);
    _put4_1__tmp_at466_19217 = NOVALUE;

    /** 	put4(records_ptr) -- point to 1st block*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_records_ptr_19137)) {
        *poke4_addr = (unsigned long)_records_ptr_19137;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_records_ptr_19137)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at494_19219);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at494_19219 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at494_19219); // DJP 

    /** end procedure*/
    goto LD; // [515] 518
LD: 
    DeRefi(_put4_1__tmp_at494_19219);
    _put4_1__tmp_at494_19219 = NOVALUE;

    /** 	putn(repeat(0, (init_index-1) * 8))*/
    _10925 = _init_index_19142 - 1;
    if ((long)((unsigned long)_10925 +(unsigned long) HIGH_BITS) >= 0){
        _10925 = NewDouble((double)_10925);
    }
    if (IS_ATOM_INT(_10925)) {
        _10926 = _10925 * 8;
    }
    else {
        _10926 = NewDouble(DBL_PTR(_10925)->dbl * (double)8);
    }
    DeRef(_10925);
    _10925 = NOVALUE;
    _10927 = Repeat(0, _10926);
    DeRef(_10926);
    _10926 = NOVALUE;
    DeRefi(_s_inlined_putn_at_534_19224);
    _s_inlined_putn_at_534_19224 = _10927;
    _10927 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_48current_db_17843, _s_inlined_putn_at_534_19224); // DJP 

    /** end procedure*/
    goto LE; // [548] 551
LE: 
    DeRefi(_s_inlined_putn_at_534_19224);
    _s_inlined_putn_at_534_19224 = NOVALUE;

    /** 	name_ptr = db_allocate(length(name)+1)*/
    if (IS_SEQUENCE(_name_19130)){
            _10928 = SEQ_PTR(_name_19130)->length;
    }
    else {
        _10928 = 1;
    }
    _10929 = _10928 + 1;
    _10928 = NOVALUE;
    _0 = _name_ptr_19132;
    _name_ptr_19132 = _48db_allocate(_10929);
    DeRef(_0);
    _10929 = NOVALUE;

    /** 	putn(name & 0)*/
    Append(&_10931, _name_19130, 0);
    DeRef(_s_inlined_putn_at_572_19230);
    _s_inlined_putn_at_572_19230 = _10931;
    _10931 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_48current_db_17843, _s_inlined_putn_at_572_19230); // DJP 

    /** end procedure*/
    goto LF; // [586] 589
LF: 
    DeRef(_s_inlined_putn_at_572_19230);
    _s_inlined_putn_at_572_19230 = NOVALUE;

    /** 	io:seek(current_db, tables+4+(nt-1)*SIZEOF_TABLE_HEADER)*/
    if (IS_ATOM_INT(_tables_19134)) {
        _10932 = _tables_19134 + 4;
        if ((long)((unsigned long)_10932 + (unsigned long)HIGH_BITS) >= 0) 
        _10932 = NewDouble((double)_10932);
    }
    else {
        _10932 = NewDouble(DBL_PTR(_tables_19134)->dbl + (double)4);
    }
    if (IS_ATOM_INT(_nt_19133)) {
        _10933 = _nt_19133 - 1;
        if ((long)((unsigned long)_10933 +(unsigned long) HIGH_BITS) >= 0){
            _10933 = NewDouble((double)_10933);
        }
    }
    else {
        _10933 = NewDouble(DBL_PTR(_nt_19133)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_10933)) {
        if (_10933 == (short)_10933)
        _10934 = _10933 * 16;
        else
        _10934 = NewDouble(_10933 * (double)16);
    }
    else {
        _10934 = NewDouble(DBL_PTR(_10933)->dbl * (double)16);
    }
    DeRef(_10933);
    _10933 = NOVALUE;
    if (IS_ATOM_INT(_10932) && IS_ATOM_INT(_10934)) {
        _10935 = _10932 + _10934;
        if ((long)((unsigned long)_10935 + (unsigned long)HIGH_BITS) >= 0) 
        _10935 = NewDouble((double)_10935);
    }
    else {
        if (IS_ATOM_INT(_10932)) {
            _10935 = NewDouble((double)_10932 + DBL_PTR(_10934)->dbl);
        }
        else {
            if (IS_ATOM_INT(_10934)) {
                _10935 = NewDouble(DBL_PTR(_10932)->dbl + (double)_10934);
            }
            else
            _10935 = NewDouble(DBL_PTR(_10932)->dbl + DBL_PTR(_10934)->dbl);
        }
    }
    DeRef(_10932);
    _10932 = NOVALUE;
    DeRef(_10934);
    _10934 = NOVALUE;
    DeRef(_pos_inlined_seek_at_611_19236);
    _pos_inlined_seek_at_611_19236 = _10935;
    _10935 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_611_19236);
    DeRef(_seek_1__tmp_at614_19238);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_611_19236;
    _seek_1__tmp_at614_19238 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_614_19237 = machine(19, _seek_1__tmp_at614_19238);
    DeRef(_pos_inlined_seek_at_611_19236);
    _pos_inlined_seek_at_611_19236 = NOVALUE;
    DeRef(_seek_1__tmp_at614_19238);
    _seek_1__tmp_at614_19238 = NOVALUE;

    /** 	put4(name_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_name_ptr_19132)) {
        *poke4_addr = (unsigned long)_name_ptr_19132;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_name_ptr_19132)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at629_19240);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at629_19240 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at629_19240); // DJP 

    /** end procedure*/
    goto L10; // [650] 653
L10: 
    DeRefi(_put4_1__tmp_at629_19240);
    _put4_1__tmp_at629_19240 = NOVALUE;

    /** 	put4(0)  -- start with 0 records total*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    *poke4_addr = (unsigned long)0;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at657_19242);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at657_19242 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at657_19242); // DJP 

    /** end procedure*/
    goto L11; // [678] 681
L11: 
    DeRefi(_put4_1__tmp_at657_19242);
    _put4_1__tmp_at657_19242 = NOVALUE;

    /** 	put4(1)  -- start with 1 block of records in index*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    *poke4_addr = (unsigned long)1;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at685_19244);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at685_19244 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at685_19244); // DJP 

    /** end procedure*/
    goto L12; // [706] 709
L12: 
    DeRefi(_put4_1__tmp_at685_19244);
    _put4_1__tmp_at685_19244 = NOVALUE;

    /** 	put4(index_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_index_ptr_19140)) {
        *poke4_addr = (unsigned long)_index_ptr_19140;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_index_ptr_19140)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at713_19246);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at713_19246 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at713_19246); // DJP 

    /** end procedure*/
    goto L13; // [734] 737
L13: 
    DeRefi(_put4_1__tmp_at713_19246);
    _put4_1__tmp_at713_19246 = NOVALUE;

    /** 	if db_select_table(name) then*/
    RefDS(_name_19130);
    _10936 = _48db_select_table(_name_19130);
    if (_10936 == 0) {
        DeRef(_10936);
        _10936 = NOVALUE;
        goto L14; // [745] 749
    }
    else {
        if (!IS_ATOM_INT(_10936) && DBL_PTR(_10936)->dbl == 0.0){
            DeRef(_10936);
            _10936 = NOVALUE;
            goto L14; // [745] 749
        }
        DeRef(_10936);
        _10936 = NOVALUE;
    }
    DeRef(_10936);
    _10936 = NOVALUE;
L14: 

    /** 	return DB_OK*/
    DeRefDS(_name_19130);
    DeRef(_name_ptr_19132);
    DeRef(_nt_19133);
    DeRef(_tables_19134);
    DeRef(_newtables_19135);
    DeRef(_table_19136);
    DeRef(_records_ptr_19137);
    DeRef(_size_19138);
    DeRef(_newsize_19139);
    DeRef(_index_ptr_19140);
    DeRef(_remaining_19141);
    return 0;
    ;
}


int _48db_table_list()
{
    int _seek_1__tmp_at120_19505 = NOVALUE;
    int _seek_inlined_seek_at_120_19504 = NOVALUE;
    int _seek_1__tmp_at98_19501 = NOVALUE;
    int _seek_inlined_seek_at_98_19500 = NOVALUE;
    int _pos_inlined_seek_at_95_19499 = NOVALUE;
    int _seek_1__tmp_at42_19489 = NOVALUE;
    int _seek_inlined_seek_at_42_19488 = NOVALUE;
    int _seek_1__tmp_at4_19482 = NOVALUE;
    int _seek_inlined_seek_at_4_19481 = NOVALUE;
    int _table_names_19476 = NOVALUE;
    int _tables_19477 = NOVALUE;
    int _nt_19478 = NOVALUE;
    int _name_19479 = NOVALUE;
    int _11035 = NOVALUE;
    int _11034 = NOVALUE;
    int _11032 = NOVALUE;
    int _11031 = NOVALUE;
    int _11030 = NOVALUE;
    int _11029 = NOVALUE;
    int _11024 = NOVALUE;
    int _0, _1, _2;
    

    /** 	io:seek(current_db, TABLE_HEADERS)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at4_19482);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = 3;
    _seek_1__tmp_at4_19482 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_4_19481 = machine(19, _seek_1__tmp_at4_19482);
    DeRefi(_seek_1__tmp_at4_19482);
    _seek_1__tmp_at4_19482 = NOVALUE;

    /** 	if length(vLastErrors) > 0 then return {} end if*/
    if (IS_SEQUENCE(_48vLastErrors_17867)){
            _11024 = SEQ_PTR(_48vLastErrors_17867)->length;
    }
    else {
        _11024 = 1;
    }
    if (_11024 <= 0)
    goto L1; // [25] 34
    RefDS(_5);
    DeRef(_table_names_19476);
    DeRef(_tables_19477);
    DeRef(_nt_19478);
    DeRef(_name_19479);
    return _5;
L1: 

    /** 	tables = get4()*/
    _0 = _tables_19477;
    _tables_19477 = _48get4();
    DeRef(_0);

    /** 	io:seek(current_db, tables)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_tables_19477);
    DeRef(_seek_1__tmp_at42_19489);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _tables_19477;
    _seek_1__tmp_at42_19489 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_42_19488 = machine(19, _seek_1__tmp_at42_19489);
    DeRef(_seek_1__tmp_at42_19489);
    _seek_1__tmp_at42_19489 = NOVALUE;

    /** 	nt = get4()*/
    _0 = _nt_19478;
    _nt_19478 = _48get4();
    DeRef(_0);

    /** 	table_names = repeat(0, nt)*/
    DeRef(_table_names_19476);
    _table_names_19476 = Repeat(0, _nt_19478);

    /** 	for i = 0 to nt-1 do*/
    if (IS_ATOM_INT(_nt_19478)) {
        _11029 = _nt_19478 - 1;
        if ((long)((unsigned long)_11029 +(unsigned long) HIGH_BITS) >= 0){
            _11029 = NewDouble((double)_11029);
        }
    }
    else {
        _11029 = NewDouble(DBL_PTR(_nt_19478)->dbl - (double)1);
    }
    {
        int _i_19493;
        _i_19493 = 0;
L2: 
        if (binary_op_a(GREATER, _i_19493, _11029)){
            goto L3; // [73] 154
        }

        /** 		io:seek(current_db, tables + 4 + i*SIZEOF_TABLE_HEADER)*/
        if (IS_ATOM_INT(_tables_19477)) {
            _11030 = _tables_19477 + 4;
            if ((long)((unsigned long)_11030 + (unsigned long)HIGH_BITS) >= 0) 
            _11030 = NewDouble((double)_11030);
        }
        else {
            _11030 = NewDouble(DBL_PTR(_tables_19477)->dbl + (double)4);
        }
        if (IS_ATOM_INT(_i_19493)) {
            if (_i_19493 == (short)_i_19493)
            _11031 = _i_19493 * 16;
            else
            _11031 = NewDouble(_i_19493 * (double)16);
        }
        else {
            _11031 = NewDouble(DBL_PTR(_i_19493)->dbl * (double)16);
        }
        if (IS_ATOM_INT(_11030) && IS_ATOM_INT(_11031)) {
            _11032 = _11030 + _11031;
            if ((long)((unsigned long)_11032 + (unsigned long)HIGH_BITS) >= 0) 
            _11032 = NewDouble((double)_11032);
        }
        else {
            if (IS_ATOM_INT(_11030)) {
                _11032 = NewDouble((double)_11030 + DBL_PTR(_11031)->dbl);
            }
            else {
                if (IS_ATOM_INT(_11031)) {
                    _11032 = NewDouble(DBL_PTR(_11030)->dbl + (double)_11031);
                }
                else
                _11032 = NewDouble(DBL_PTR(_11030)->dbl + DBL_PTR(_11031)->dbl);
            }
        }
        DeRef(_11030);
        _11030 = NOVALUE;
        DeRef(_11031);
        _11031 = NOVALUE;
        DeRef(_pos_inlined_seek_at_95_19499);
        _pos_inlined_seek_at_95_19499 = _11032;
        _11032 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_95_19499);
        DeRef(_seek_1__tmp_at98_19501);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _48current_db_17843;
        ((int *)_2)[2] = _pos_inlined_seek_at_95_19499;
        _seek_1__tmp_at98_19501 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_98_19500 = machine(19, _seek_1__tmp_at98_19501);
        DeRef(_pos_inlined_seek_at_95_19499);
        _pos_inlined_seek_at_95_19499 = NOVALUE;
        DeRef(_seek_1__tmp_at98_19501);
        _seek_1__tmp_at98_19501 = NOVALUE;

        /** 		name = get4()*/
        _0 = _name_19479;
        _name_19479 = _48get4();
        DeRef(_0);

        /** 		io:seek(current_db, name)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_name_19479);
        DeRef(_seek_1__tmp_at120_19505);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _48current_db_17843;
        ((int *)_2)[2] = _name_19479;
        _seek_1__tmp_at120_19505 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_120_19504 = machine(19, _seek_1__tmp_at120_19505);
        DeRef(_seek_1__tmp_at120_19505);
        _seek_1__tmp_at120_19505 = NOVALUE;

        /** 		table_names[i+1] = get_string()*/
        if (IS_ATOM_INT(_i_19493)) {
            _11034 = _i_19493 + 1;
            if (_11034 > MAXINT){
                _11034 = NewDouble((double)_11034);
            }
        }
        else
        _11034 = binary_op(PLUS, 1, _i_19493);
        _11035 = _48get_string();
        _2 = (int)SEQ_PTR(_table_names_19476);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _table_names_19476 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_11034))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_11034)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _11034);
        _1 = *(int *)_2;
        *(int *)_2 = _11035;
        if( _1 != _11035 ){
            DeRef(_1);
        }
        _11035 = NOVALUE;

        /** 	end for*/
        _0 = _i_19493;
        if (IS_ATOM_INT(_i_19493)) {
            _i_19493 = _i_19493 + 1;
            if ((long)((unsigned long)_i_19493 +(unsigned long) HIGH_BITS) >= 0){
                _i_19493 = NewDouble((double)_i_19493);
            }
        }
        else {
            _i_19493 = binary_op_a(PLUS, _i_19493, 1);
        }
        DeRef(_0);
        goto L2; // [149] 80
L3: 
        ;
        DeRef(_i_19493);
    }

    /** 	return table_names*/
    DeRef(_tables_19477);
    DeRef(_nt_19478);
    DeRef(_name_19479);
    DeRef(_11029);
    _11029 = NOVALUE;
    DeRef(_11034);
    _11034 = NOVALUE;
    return _table_names_19476;
    ;
}


int _48key_value(int _ptr_19510)
{
    int _seek_1__tmp_at11_19515 = NOVALUE;
    int _seek_inlined_seek_at_11_19514 = NOVALUE;
    int _pos_inlined_seek_at_8_19513 = NOVALUE;
    int _11037 = NOVALUE;
    int _11036 = NOVALUE;
    int _0, _1, _2;
    

    /** 	io:seek(current_db, ptr+4) -- skip ptr to data*/
    if (IS_ATOM_INT(_ptr_19510)) {
        _11036 = _ptr_19510 + 4;
        if ((long)((unsigned long)_11036 + (unsigned long)HIGH_BITS) >= 0) 
        _11036 = NewDouble((double)_11036);
    }
    else {
        _11036 = NewDouble(DBL_PTR(_ptr_19510)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_8_19513);
    _pos_inlined_seek_at_8_19513 = _11036;
    _11036 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_8_19513);
    DeRef(_seek_1__tmp_at11_19515);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_8_19513;
    _seek_1__tmp_at11_19515 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_11_19514 = machine(19, _seek_1__tmp_at11_19515);
    DeRef(_pos_inlined_seek_at_8_19513);
    _pos_inlined_seek_at_8_19513 = NOVALUE;
    DeRef(_seek_1__tmp_at11_19515);
    _seek_1__tmp_at11_19515 = NOVALUE;

    /** 	return decompress(0)*/
    _11037 = _48decompress(0);
    DeRef(_ptr_19510);
    return _11037;
    ;
}


int _48db_find_key(int _key_19519, int _table_name_19520)
{
    int _lo_19521 = NOVALUE;
    int _hi_19522 = NOVALUE;
    int _mid_19523 = NOVALUE;
    int _c_19524 = NOVALUE;
    int _11061 = NOVALUE;
    int _11053 = NOVALUE;
    int _11052 = NOVALUE;
    int _11050 = NOVALUE;
    int _11047 = NOVALUE;
    int _11044 = NOVALUE;
    int _11040 = NOVALUE;
    int _11038 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_19520 == _48current_table_name_17845)
    _11038 = 1;
    else if (IS_ATOM_INT(_table_name_19520) && IS_ATOM_INT(_48current_table_name_17845))
    _11038 = 0;
    else
    _11038 = (compare(_table_name_19520, _48current_table_name_17845) == 0);
    if (_11038 != 0)
    goto L1; // [9] 46
    _11038 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    RefDS(_table_name_19520);
    _11040 = _48db_select_table(_table_name_19520);
    if (binary_op_a(EQUALS, _11040, 0)){
        DeRef(_11040);
        _11040 = NOVALUE;
        goto L2; // [20] 45
    }
    DeRef(_11040);
    _11040 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_find_key", {key, table_name})*/
    RefDS(_table_name_19520);
    Ref(_key_19519);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_19519;
    ((int *)_2)[2] = _table_name_19520;
    _11044 = MAKE_SEQ(_1);
    RefDS(_11042);
    RefDS(_11043);
    _48fatal(903, _11042, _11043, _11044);
    _11044 = NOVALUE;

    /** 			return 0*/
    DeRef(_key_19519);
    DeRefDS(_table_name_19520);
    return 0;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _48current_table_pos_17844, -1)){
        goto L3; // [50] 75
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_find_key", {key, table_name})*/
    Ref(_table_name_19520);
    Ref(_key_19519);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_19519;
    ((int *)_2)[2] = _table_name_19520;
    _11047 = MAKE_SEQ(_1);
    RefDS(_11046);
    RefDS(_11043);
    _48fatal(903, _11046, _11043, _11047);
    _11047 = NOVALUE;

    /** 		return 0*/
    DeRef(_key_19519);
    DeRef(_table_name_19520);
    return 0;
L3: 

    /** 	lo = 1*/
    _lo_19521 = 1;

    /** 	hi = length(key_pointers)*/
    if (IS_SEQUENCE(_48key_pointers_17850)){
            _hi_19522 = SEQ_PTR(_48key_pointers_17850)->length;
    }
    else {
        _hi_19522 = 1;
    }

    /** 	mid = 1*/
    _mid_19523 = 1;

    /** 	c = 0*/
    _c_19524 = 0;

    /** 	while lo <= hi do*/
L4: 
    if (_lo_19521 > _hi_19522)
    goto L5; // [102] 176

    /** 		mid = floor((lo + hi) / 2)*/
    _11050 = _lo_19521 + _hi_19522;
    if ((long)((unsigned long)_11050 + (unsigned long)HIGH_BITS) >= 0) 
    _11050 = NewDouble((double)_11050);
    if (IS_ATOM_INT(_11050)) {
        _mid_19523 = _11050 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _11050, 2);
        _mid_19523 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    DeRef(_11050);
    _11050 = NOVALUE;
    if (!IS_ATOM_INT(_mid_19523)) {
        _1 = (long)(DBL_PTR(_mid_19523)->dbl);
        if (UNIQUE(DBL_PTR(_mid_19523)) && (DBL_PTR(_mid_19523)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mid_19523);
        _mid_19523 = _1;
    }

    /** 		c = eu:compare(key, key_value(key_pointers[mid]))*/
    _2 = (int)SEQ_PTR(_48key_pointers_17850);
    _11052 = (int)*(((s1_ptr)_2)->base + _mid_19523);
    Ref(_11052);
    _11053 = _48key_value(_11052);
    _11052 = NOVALUE;
    if (IS_ATOM_INT(_key_19519) && IS_ATOM_INT(_11053)){
        _c_19524 = (_key_19519 < _11053) ? -1 : (_key_19519 > _11053);
    }
    else{
        _c_19524 = compare(_key_19519, _11053);
    }
    DeRef(_11053);
    _11053 = NOVALUE;

    /** 		if c < 0 then*/
    if (_c_19524 >= 0)
    goto L6; // [136] 149

    /** 			hi = mid - 1*/
    _hi_19522 = _mid_19523 - 1;
    goto L4; // [146] 102
L6: 

    /** 		elsif c > 0 then*/
    if (_c_19524 <= 0)
    goto L7; // [151] 164

    /** 			lo = mid + 1*/
    _lo_19521 = _mid_19523 + 1;
    goto L4; // [161] 102
L7: 

    /** 			return mid*/
    DeRef(_key_19519);
    DeRef(_table_name_19520);
    return _mid_19523;

    /** 	end while*/
    goto L4; // [173] 102
L5: 

    /** 	if c > 0 then*/
    if (_c_19524 <= 0)
    goto L8; // [178] 189

    /** 		mid += 1*/
    _mid_19523 = _mid_19523 + 1;
L8: 

    /** 	return -mid*/
    if ((unsigned long)_mid_19523 == 0xC0000000)
    _11061 = (int)NewDouble((double)-0xC0000000);
    else
    _11061 = - _mid_19523;
    DeRef(_key_19519);
    DeRef(_table_name_19520);
    return _11061;
    ;
}


int _48db_insert(int _key_19559, int _data_19560, int _table_name_19561)
{
    int _key_string_19562 = NOVALUE;
    int _data_string_19563 = NOVALUE;
    int _last_part_19564 = NOVALUE;
    int _remaining_19565 = NOVALUE;
    int _key_ptr_19566 = NOVALUE;
    int _data_ptr_19567 = NOVALUE;
    int _records_ptr_19568 = NOVALUE;
    int _nrecs_19569 = NOVALUE;
    int _current_block_19570 = NOVALUE;
    int _size_19571 = NOVALUE;
    int _new_size_19572 = NOVALUE;
    int _key_location_19573 = NOVALUE;
    int _new_block_19574 = NOVALUE;
    int _index_ptr_19575 = NOVALUE;
    int _new_index_ptr_19576 = NOVALUE;
    int _total_recs_19577 = NOVALUE;
    int _r_19578 = NOVALUE;
    int _blocks_19579 = NOVALUE;
    int _new_recs_19580 = NOVALUE;
    int _n_19581 = NOVALUE;
    int _put4_1__tmp_at81_19595 = NOVALUE;
    int _seek_1__tmp_at134_19601 = NOVALUE;
    int _seek_inlined_seek_at_134_19600 = NOVALUE;
    int _pos_inlined_seek_at_131_19599 = NOVALUE;
    int _seek_1__tmp_at176_19609 = NOVALUE;
    int _seek_inlined_seek_at_176_19608 = NOVALUE;
    int _pos_inlined_seek_at_173_19607 = NOVALUE;
    int _put4_1__tmp_at191_19611 = NOVALUE;
    int _seek_1__tmp_at319_19629 = NOVALUE;
    int _seek_inlined_seek_at_319_19628 = NOVALUE;
    int _pos_inlined_seek_at_316_19627 = NOVALUE;
    int _seek_1__tmp_at341_19633 = NOVALUE;
    int _seek_inlined_seek_at_341_19632 = NOVALUE;
    int _where_inlined_where_at_406_19642 = NOVALUE;
    int _seek_1__tmp_at450_19652 = NOVALUE;
    int _seek_inlined_seek_at_450_19651 = NOVALUE;
    int _pos_inlined_seek_at_447_19650 = NOVALUE;
    int _put4_1__tmp_at495_19661 = NOVALUE;
    int _x_inlined_put4_at_492_19660 = NOVALUE;
    int _seek_1__tmp_at532_19664 = NOVALUE;
    int _seek_inlined_seek_at_532_19663 = NOVALUE;
    int _put4_1__tmp_at553_19667 = NOVALUE;
    int _seek_1__tmp_at590_19672 = NOVALUE;
    int _seek_inlined_seek_at_590_19671 = NOVALUE;
    int _pos_inlined_seek_at_587_19670 = NOVALUE;
    int _seek_1__tmp_at692_19697 = NOVALUE;
    int _seek_inlined_seek_at_692_19696 = NOVALUE;
    int _pos_inlined_seek_at_689_19695 = NOVALUE;
    int _s_inlined_putn_at_753_19706 = NOVALUE;
    int _seek_1__tmp_at776_19709 = NOVALUE;
    int _seek_inlined_seek_at_776_19708 = NOVALUE;
    int _put4_1__tmp_at798_19713 = NOVALUE;
    int _x_inlined_put4_at_795_19712 = NOVALUE;
    int _seek_1__tmp_at835_19718 = NOVALUE;
    int _seek_inlined_seek_at_835_19717 = NOVALUE;
    int _pos_inlined_seek_at_832_19716 = NOVALUE;
    int _seek_1__tmp_at886_19728 = NOVALUE;
    int _seek_inlined_seek_at_886_19727 = NOVALUE;
    int _pos_inlined_seek_at_883_19726 = NOVALUE;
    int _put4_1__tmp_at901_19730 = NOVALUE;
    int _put4_1__tmp_at929_19732 = NOVALUE;
    int _seek_1__tmp_at982_19738 = NOVALUE;
    int _seek_inlined_seek_at_982_19737 = NOVALUE;
    int _pos_inlined_seek_at_979_19736 = NOVALUE;
    int _put4_1__tmp_at1003_19741 = NOVALUE;
    int _seek_1__tmp_at1040_19746 = NOVALUE;
    int _seek_inlined_seek_at_1040_19745 = NOVALUE;
    int _pos_inlined_seek_at_1037_19744 = NOVALUE;
    int _s_inlined_putn_at_1138_19764 = NOVALUE;
    int _seek_1__tmp_at1175_19769 = NOVALUE;
    int _seek_inlined_seek_at_1175_19768 = NOVALUE;
    int _pos_inlined_seek_at_1172_19767 = NOVALUE;
    int _put4_1__tmp_at1190_19771 = NOVALUE;
    int _11157 = NOVALUE;
    int _11156 = NOVALUE;
    int _11155 = NOVALUE;
    int _11154 = NOVALUE;
    int _11151 = NOVALUE;
    int _11150 = NOVALUE;
    int _11148 = NOVALUE;
    int _11146 = NOVALUE;
    int _11145 = NOVALUE;
    int _11143 = NOVALUE;
    int _11142 = NOVALUE;
    int _11140 = NOVALUE;
    int _11139 = NOVALUE;
    int _11137 = NOVALUE;
    int _11136 = NOVALUE;
    int _11135 = NOVALUE;
    int _11134 = NOVALUE;
    int _11133 = NOVALUE;
    int _11132 = NOVALUE;
    int _11131 = NOVALUE;
    int _11130 = NOVALUE;
    int _11129 = NOVALUE;
    int _11126 = NOVALUE;
    int _11125 = NOVALUE;
    int _11124 = NOVALUE;
    int _11123 = NOVALUE;
    int _11120 = NOVALUE;
    int _11117 = NOVALUE;
    int _11116 = NOVALUE;
    int _11115 = NOVALUE;
    int _11114 = NOVALUE;
    int _11110 = NOVALUE;
    int _11109 = NOVALUE;
    int _11107 = NOVALUE;
    int _11106 = NOVALUE;
    int _11104 = NOVALUE;
    int _11103 = NOVALUE;
    int _11102 = NOVALUE;
    int _11101 = NOVALUE;
    int _11100 = NOVALUE;
    int _11099 = NOVALUE;
    int _11098 = NOVALUE;
    int _11096 = NOVALUE;
    int _11093 = NOVALUE;
    int _11088 = NOVALUE;
    int _11086 = NOVALUE;
    int _11085 = NOVALUE;
    int _11083 = NOVALUE;
    int _11082 = NOVALUE;
    int _11081 = NOVALUE;
    int _11078 = NOVALUE;
    int _11076 = NOVALUE;
    int _11073 = NOVALUE;
    int _11072 = NOVALUE;
    int _11070 = NOVALUE;
    int _11069 = NOVALUE;
    int _11067 = NOVALUE;
    int _0, _1, _2;
    

    /** 	key_location = db_find_key(key, table_name) -- Let it set the current table if necessary*/
    Ref(_key_19559);
    RefDS(_table_name_19561);
    _0 = _key_location_19573;
    _key_location_19573 = _48db_find_key(_key_19559, _table_name_19561);
    DeRef(_0);

    /** 	if key_location > 0 then*/
    if (binary_op_a(LESSEQ, _key_location_19573, 0)){
        goto L1; // [10] 23
    }

    /** 		return DB_EXISTS_ALREADY*/
    DeRef(_key_19559);
    DeRefDS(_table_name_19561);
    DeRef(_key_string_19562);
    DeRef(_data_string_19563);
    DeRef(_last_part_19564);
    DeRef(_remaining_19565);
    DeRef(_key_ptr_19566);
    DeRef(_data_ptr_19567);
    DeRef(_records_ptr_19568);
    DeRef(_nrecs_19569);
    DeRef(_current_block_19570);
    DeRef(_size_19571);
    DeRef(_new_size_19572);
    DeRef(_key_location_19573);
    DeRef(_new_block_19574);
    DeRef(_index_ptr_19575);
    DeRef(_new_index_ptr_19576);
    DeRef(_total_recs_19577);
    return -2;
L1: 

    /** 	key_location = -key_location*/
    _0 = _key_location_19573;
    if (IS_ATOM_INT(_key_location_19573)) {
        if ((unsigned long)_key_location_19573 == 0xC0000000)
        _key_location_19573 = (int)NewDouble((double)-0xC0000000);
        else
        _key_location_19573 = - _key_location_19573;
    }
    else {
        _key_location_19573 = unary_op(UMINUS, _key_location_19573);
    }
    DeRef(_0);

    /** 	data_string = compress(data)*/
    _0 = _data_string_19563;
    _data_string_19563 = _48compress(_data_19560);
    DeRef(_0);

    /** 	key_string  = compress(key)*/
    Ref(_key_19559);
    _0 = _key_string_19562;
    _key_string_19562 = _48compress(_key_19559);
    DeRef(_0);

    /** 	data_ptr = db_allocate(length(data_string))*/
    if (IS_SEQUENCE(_data_string_19563)){
            _11067 = SEQ_PTR(_data_string_19563)->length;
    }
    else {
        _11067 = 1;
    }
    _0 = _data_ptr_19567;
    _data_ptr_19567 = _48db_allocate(_11067);
    DeRef(_0);
    _11067 = NOVALUE;

    /** 	putn(data_string)*/

    /** 	puts(current_db, s)*/
    EPuts(_48current_db_17843, _data_string_19563); // DJP 

    /** end procedure*/
    goto L2; // [64] 67
L2: 

    /** 	key_ptr = db_allocate(4+length(key_string))*/
    if (IS_SEQUENCE(_key_string_19562)){
            _11069 = SEQ_PTR(_key_string_19562)->length;
    }
    else {
        _11069 = 1;
    }
    _11070 = 4 + _11069;
    _11069 = NOVALUE;
    _0 = _key_ptr_19566;
    _key_ptr_19566 = _48db_allocate(_11070);
    DeRef(_0);
    _11070 = NOVALUE;

    /** 	put4(data_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_data_ptr_19567)) {
        *poke4_addr = (unsigned long)_data_ptr_19567;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_data_ptr_19567)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at81_19595);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at81_19595 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at81_19595); // DJP 

    /** end procedure*/
    goto L3; // [103] 106
L3: 
    DeRefi(_put4_1__tmp_at81_19595);
    _put4_1__tmp_at81_19595 = NOVALUE;

    /** 	putn(key_string)*/

    /** 	puts(current_db, s)*/
    EPuts(_48current_db_17843, _key_string_19562); // DJP 

    /** end procedure*/
    goto L4; // [119] 122
L4: 

    /** 	io:seek(current_db, current_table_pos+4)*/
    if (IS_ATOM_INT(_48current_table_pos_17844)) {
        _11072 = _48current_table_pos_17844 + 4;
        if ((long)((unsigned long)_11072 + (unsigned long)HIGH_BITS) >= 0) 
        _11072 = NewDouble((double)_11072);
    }
    else {
        _11072 = NewDouble(DBL_PTR(_48current_table_pos_17844)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_131_19599);
    _pos_inlined_seek_at_131_19599 = _11072;
    _11072 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_131_19599);
    DeRef(_seek_1__tmp_at134_19601);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_131_19599;
    _seek_1__tmp_at134_19601 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_134_19600 = machine(19, _seek_1__tmp_at134_19601);
    DeRef(_pos_inlined_seek_at_131_19599);
    _pos_inlined_seek_at_131_19599 = NOVALUE;
    DeRef(_seek_1__tmp_at134_19601);
    _seek_1__tmp_at134_19601 = NOVALUE;

    /** 	total_recs = get4()+1*/
    _11073 = _48get4();
    DeRef(_total_recs_19577);
    if (IS_ATOM_INT(_11073)) {
        _total_recs_19577 = _11073 + 1;
        if (_total_recs_19577 > MAXINT){
            _total_recs_19577 = NewDouble((double)_total_recs_19577);
        }
    }
    else
    _total_recs_19577 = binary_op(PLUS, 1, _11073);
    DeRef(_11073);
    _11073 = NOVALUE;

    /** 	blocks = get4()*/
    _blocks_19579 = _48get4();
    if (!IS_ATOM_INT(_blocks_19579)) {
        _1 = (long)(DBL_PTR(_blocks_19579)->dbl);
        if (UNIQUE(DBL_PTR(_blocks_19579)) && (DBL_PTR(_blocks_19579)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_blocks_19579);
        _blocks_19579 = _1;
    }

    /** 	io:seek(current_db, current_table_pos+4)*/
    if (IS_ATOM_INT(_48current_table_pos_17844)) {
        _11076 = _48current_table_pos_17844 + 4;
        if ((long)((unsigned long)_11076 + (unsigned long)HIGH_BITS) >= 0) 
        _11076 = NewDouble((double)_11076);
    }
    else {
        _11076 = NewDouble(DBL_PTR(_48current_table_pos_17844)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_173_19607);
    _pos_inlined_seek_at_173_19607 = _11076;
    _11076 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_173_19607);
    DeRef(_seek_1__tmp_at176_19609);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_173_19607;
    _seek_1__tmp_at176_19609 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_176_19608 = machine(19, _seek_1__tmp_at176_19609);
    DeRef(_pos_inlined_seek_at_173_19607);
    _pos_inlined_seek_at_173_19607 = NOVALUE;
    DeRef(_seek_1__tmp_at176_19609);
    _seek_1__tmp_at176_19609 = NOVALUE;

    /** 	put4(total_recs)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_total_recs_19577)) {
        *poke4_addr = (unsigned long)_total_recs_19577;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_total_recs_19577)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at191_19611);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at191_19611 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at191_19611); // DJP 

    /** end procedure*/
    goto L5; // [213] 216
L5: 
    DeRefi(_put4_1__tmp_at191_19611);
    _put4_1__tmp_at191_19611 = NOVALUE;

    /** 	n = length(key_pointers)*/
    if (IS_SEQUENCE(_48key_pointers_17850)){
            _n_19581 = SEQ_PTR(_48key_pointers_17850)->length;
    }
    else {
        _n_19581 = 1;
    }

    /** 	if key_location >= floor(n/2) then*/
    _11078 = _n_19581 >> 1;
    if (binary_op_a(LESS, _key_location_19573, _11078)){
        _11078 = NOVALUE;
        goto L6; // [231] 270
    }
    DeRef(_11078);
    _11078 = NOVALUE;

    /** 		key_pointers = append(key_pointers, 0)*/
    Append(&_48key_pointers_17850, _48key_pointers_17850, 0);

    /** 		key_pointers[key_location+1..n+1] = key_pointers[key_location..n]*/
    if (IS_ATOM_INT(_key_location_19573)) {
        _11081 = _key_location_19573 + 1;
        if (_11081 > MAXINT){
            _11081 = NewDouble((double)_11081);
        }
    }
    else
    _11081 = binary_op(PLUS, 1, _key_location_19573);
    _11082 = _n_19581 + 1;
    rhs_slice_target = (object_ptr)&_11083;
    RHS_Slice(_48key_pointers_17850, _key_location_19573, _n_19581);
    assign_slice_seq = (s1_ptr *)&_48key_pointers_17850;
    AssignSlice(_11081, _11082, _11083);
    DeRef(_11081);
    _11081 = NOVALUE;
    _11082 = NOVALUE;
    DeRefDS(_11083);
    _11083 = NOVALUE;
    goto L7; // [267] 299
L6: 

    /** 		key_pointers = prepend(key_pointers, 0)*/
    Prepend(&_48key_pointers_17850, _48key_pointers_17850, 0);

    /** 		key_pointers[1..key_location-1] = key_pointers[2..key_location]*/
    if (IS_ATOM_INT(_key_location_19573)) {
        _11085 = _key_location_19573 - 1;
        if ((long)((unsigned long)_11085 +(unsigned long) HIGH_BITS) >= 0){
            _11085 = NewDouble((double)_11085);
        }
    }
    else {
        _11085 = NewDouble(DBL_PTR(_key_location_19573)->dbl - (double)1);
    }
    rhs_slice_target = (object_ptr)&_11086;
    RHS_Slice(_48key_pointers_17850, 2, _key_location_19573);
    assign_slice_seq = (s1_ptr *)&_48key_pointers_17850;
    AssignSlice(1, _11085, _11086);
    DeRef(_11085);
    _11085 = NOVALUE;
    DeRefDS(_11086);
    _11086 = NOVALUE;
L7: 

    /** 	key_pointers[key_location] = key_ptr*/
    Ref(_key_ptr_19566);
    _2 = (int)SEQ_PTR(_48key_pointers_17850);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _48key_pointers_17850 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_key_location_19573))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_key_location_19573)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _key_location_19573);
    _1 = *(int *)_2;
    *(int *)_2 = _key_ptr_19566;
    DeRef(_1);

    /** 	io:seek(current_db, current_table_pos+12) -- get after put - seek is necessary*/
    if (IS_ATOM_INT(_48current_table_pos_17844)) {
        _11088 = _48current_table_pos_17844 + 12;
        if ((long)((unsigned long)_11088 + (unsigned long)HIGH_BITS) >= 0) 
        _11088 = NewDouble((double)_11088);
    }
    else {
        _11088 = NewDouble(DBL_PTR(_48current_table_pos_17844)->dbl + (double)12);
    }
    DeRef(_pos_inlined_seek_at_316_19627);
    _pos_inlined_seek_at_316_19627 = _11088;
    _11088 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_316_19627);
    DeRef(_seek_1__tmp_at319_19629);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_316_19627;
    _seek_1__tmp_at319_19629 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_319_19628 = machine(19, _seek_1__tmp_at319_19629);
    DeRef(_pos_inlined_seek_at_316_19627);
    _pos_inlined_seek_at_316_19627 = NOVALUE;
    DeRef(_seek_1__tmp_at319_19629);
    _seek_1__tmp_at319_19629 = NOVALUE;

    /** 	index_ptr = get4()*/
    _0 = _index_ptr_19575;
    _index_ptr_19575 = _48get4();
    DeRef(_0);

    /** 	io:seek(current_db, index_ptr)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_index_ptr_19575);
    DeRef(_seek_1__tmp_at341_19633);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _index_ptr_19575;
    _seek_1__tmp_at341_19633 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_341_19632 = machine(19, _seek_1__tmp_at341_19633);
    DeRef(_seek_1__tmp_at341_19633);
    _seek_1__tmp_at341_19633 = NOVALUE;

    /** 	r = 0*/
    _r_19578 = 0;

    /** 	while TRUE do*/
L8: 

    /** 		nrecs = get4()*/
    _0 = _nrecs_19569;
    _nrecs_19569 = _48get4();
    DeRef(_0);

    /** 		records_ptr = get4()*/
    _0 = _records_ptr_19568;
    _records_ptr_19568 = _48get4();
    DeRef(_0);

    /** 		r += nrecs*/
    if (IS_ATOM_INT(_nrecs_19569)) {
        _r_19578 = _r_19578 + _nrecs_19569;
    }
    else {
        _r_19578 = NewDouble((double)_r_19578 + DBL_PTR(_nrecs_19569)->dbl);
    }
    if (!IS_ATOM_INT(_r_19578)) {
        _1 = (long)(DBL_PTR(_r_19578)->dbl);
        if (UNIQUE(DBL_PTR(_r_19578)) && (DBL_PTR(_r_19578)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_r_19578);
        _r_19578 = _1;
    }

    /** 		if r + 1 >= key_location then*/
    _11093 = _r_19578 + 1;
    if (_11093 > MAXINT){
        _11093 = NewDouble((double)_11093);
    }
    if (binary_op_a(LESS, _11093, _key_location_19573)){
        DeRef(_11093);
        _11093 = NOVALUE;
        goto L8; // [389] 365
    }
    DeRef(_11093);
    _11093 = NOVALUE;

    /** 			exit*/
    goto L9; // [395] 403

    /** 	end while*/
    goto L8; // [400] 365
L9: 

    /** 	current_block = io:where(current_db)-8*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_406_19642);
    _where_inlined_where_at_406_19642 = machine(20, _48current_db_17843);
    DeRef(_current_block_19570);
    if (IS_ATOM_INT(_where_inlined_where_at_406_19642)) {
        _current_block_19570 = _where_inlined_where_at_406_19642 - 8;
        if ((long)((unsigned long)_current_block_19570 +(unsigned long) HIGH_BITS) >= 0){
            _current_block_19570 = NewDouble((double)_current_block_19570);
        }
    }
    else {
        _current_block_19570 = NewDouble(DBL_PTR(_where_inlined_where_at_406_19642)->dbl - (double)8);
    }

    /** 	key_location -= (r-nrecs)*/
    if (IS_ATOM_INT(_nrecs_19569)) {
        _11096 = _r_19578 - _nrecs_19569;
        if ((long)((unsigned long)_11096 +(unsigned long) HIGH_BITS) >= 0){
            _11096 = NewDouble((double)_11096);
        }
    }
    else {
        _11096 = NewDouble((double)_r_19578 - DBL_PTR(_nrecs_19569)->dbl);
    }
    _0 = _key_location_19573;
    if (IS_ATOM_INT(_key_location_19573) && IS_ATOM_INT(_11096)) {
        _key_location_19573 = _key_location_19573 - _11096;
        if ((long)((unsigned long)_key_location_19573 +(unsigned long) HIGH_BITS) >= 0){
            _key_location_19573 = NewDouble((double)_key_location_19573);
        }
    }
    else {
        if (IS_ATOM_INT(_key_location_19573)) {
            _key_location_19573 = NewDouble((double)_key_location_19573 - DBL_PTR(_11096)->dbl);
        }
        else {
            if (IS_ATOM_INT(_11096)) {
                _key_location_19573 = NewDouble(DBL_PTR(_key_location_19573)->dbl - (double)_11096);
            }
            else
            _key_location_19573 = NewDouble(DBL_PTR(_key_location_19573)->dbl - DBL_PTR(_11096)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_11096);
    _11096 = NOVALUE;

    /** 	io:seek(current_db, records_ptr+4*(key_location-1))*/
    if (IS_ATOM_INT(_key_location_19573)) {
        _11098 = _key_location_19573 - 1;
        if ((long)((unsigned long)_11098 +(unsigned long) HIGH_BITS) >= 0){
            _11098 = NewDouble((double)_11098);
        }
    }
    else {
        _11098 = NewDouble(DBL_PTR(_key_location_19573)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_11098)) {
        if (_11098 <= INT15 && _11098 >= -INT15)
        _11099 = 4 * _11098;
        else
        _11099 = NewDouble(4 * (double)_11098);
    }
    else {
        _11099 = NewDouble((double)4 * DBL_PTR(_11098)->dbl);
    }
    DeRef(_11098);
    _11098 = NOVALUE;
    if (IS_ATOM_INT(_records_ptr_19568) && IS_ATOM_INT(_11099)) {
        _11100 = _records_ptr_19568 + _11099;
        if ((long)((unsigned long)_11100 + (unsigned long)HIGH_BITS) >= 0) 
        _11100 = NewDouble((double)_11100);
    }
    else {
        if (IS_ATOM_INT(_records_ptr_19568)) {
            _11100 = NewDouble((double)_records_ptr_19568 + DBL_PTR(_11099)->dbl);
        }
        else {
            if (IS_ATOM_INT(_11099)) {
                _11100 = NewDouble(DBL_PTR(_records_ptr_19568)->dbl + (double)_11099);
            }
            else
            _11100 = NewDouble(DBL_PTR(_records_ptr_19568)->dbl + DBL_PTR(_11099)->dbl);
        }
    }
    DeRef(_11099);
    _11099 = NOVALUE;
    DeRef(_pos_inlined_seek_at_447_19650);
    _pos_inlined_seek_at_447_19650 = _11100;
    _11100 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_447_19650);
    DeRef(_seek_1__tmp_at450_19652);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_447_19650;
    _seek_1__tmp_at450_19652 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_450_19651 = machine(19, _seek_1__tmp_at450_19652);
    DeRef(_pos_inlined_seek_at_447_19650);
    _pos_inlined_seek_at_447_19650 = NOVALUE;
    DeRef(_seek_1__tmp_at450_19652);
    _seek_1__tmp_at450_19652 = NOVALUE;

    /** 	for i = key_location to nrecs+1 do*/
    if (IS_ATOM_INT(_nrecs_19569)) {
        _11101 = _nrecs_19569 + 1;
        if (_11101 > MAXINT){
            _11101 = NewDouble((double)_11101);
        }
    }
    else
    _11101 = binary_op(PLUS, 1, _nrecs_19569);
    {
        int _i_19654;
        Ref(_key_location_19573);
        _i_19654 = _key_location_19573;
LA: 
        if (binary_op_a(GREATER, _i_19654, _11101)){
            goto LB; // [470] 529
        }

        /** 		put4(key_pointers[i+r-nrecs])*/
        if (IS_ATOM_INT(_i_19654)) {
            _11102 = _i_19654 + _r_19578;
            if ((long)((unsigned long)_11102 + (unsigned long)HIGH_BITS) >= 0) 
            _11102 = NewDouble((double)_11102);
        }
        else {
            _11102 = NewDouble(DBL_PTR(_i_19654)->dbl + (double)_r_19578);
        }
        if (IS_ATOM_INT(_11102) && IS_ATOM_INT(_nrecs_19569)) {
            _11103 = _11102 - _nrecs_19569;
        }
        else {
            if (IS_ATOM_INT(_11102)) {
                _11103 = NewDouble((double)_11102 - DBL_PTR(_nrecs_19569)->dbl);
            }
            else {
                if (IS_ATOM_INT(_nrecs_19569)) {
                    _11103 = NewDouble(DBL_PTR(_11102)->dbl - (double)_nrecs_19569);
                }
                else
                _11103 = NewDouble(DBL_PTR(_11102)->dbl - DBL_PTR(_nrecs_19569)->dbl);
            }
        }
        DeRef(_11102);
        _11102 = NOVALUE;
        _2 = (int)SEQ_PTR(_48key_pointers_17850);
        if (!IS_ATOM_INT(_11103)){
            _11104 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_11103)->dbl));
        }
        else{
            _11104 = (int)*(((s1_ptr)_2)->base + _11103);
        }
        Ref(_11104);
        DeRef(_x_inlined_put4_at_492_19660);
        _x_inlined_put4_at_492_19660 = _11104;
        _11104 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_48mem0_17885)){
            poke4_addr = (unsigned long *)_48mem0_17885;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_492_19660)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_492_19660;
        }
        else if (IS_ATOM(_x_inlined_put4_at_492_19660)) {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_492_19660)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_x_inlined_put4_at_492_19660);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at495_19661);
        _1 = (int)SEQ_PTR(_48memseq_18120);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at495_19661 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_48current_db_17843, _put4_1__tmp_at495_19661); // DJP 

        /** end procedure*/
        goto LC; // [517] 520
LC: 
        DeRef(_x_inlined_put4_at_492_19660);
        _x_inlined_put4_at_492_19660 = NOVALUE;
        DeRefi(_put4_1__tmp_at495_19661);
        _put4_1__tmp_at495_19661 = NOVALUE;

        /** 	end for*/
        _0 = _i_19654;
        if (IS_ATOM_INT(_i_19654)) {
            _i_19654 = _i_19654 + 1;
            if ((long)((unsigned long)_i_19654 +(unsigned long) HIGH_BITS) >= 0){
                _i_19654 = NewDouble((double)_i_19654);
            }
        }
        else {
            _i_19654 = binary_op_a(PLUS, _i_19654, 1);
        }
        DeRef(_0);
        goto LA; // [524] 477
LB: 
        ;
        DeRef(_i_19654);
    }

    /** 	io:seek(current_db, current_block)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_current_block_19570);
    DeRef(_seek_1__tmp_at532_19664);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _current_block_19570;
    _seek_1__tmp_at532_19664 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_532_19663 = machine(19, _seek_1__tmp_at532_19664);
    DeRef(_seek_1__tmp_at532_19664);
    _seek_1__tmp_at532_19664 = NOVALUE;

    /** 	nrecs += 1*/
    _0 = _nrecs_19569;
    if (IS_ATOM_INT(_nrecs_19569)) {
        _nrecs_19569 = _nrecs_19569 + 1;
        if (_nrecs_19569 > MAXINT){
            _nrecs_19569 = NewDouble((double)_nrecs_19569);
        }
    }
    else
    _nrecs_19569 = binary_op(PLUS, 1, _nrecs_19569);
    DeRef(_0);

    /** 	put4(nrecs)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_nrecs_19569)) {
        *poke4_addr = (unsigned long)_nrecs_19569;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_nrecs_19569)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at553_19667);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at553_19667 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at553_19667); // DJP 

    /** end procedure*/
    goto LD; // [575] 578
LD: 
    DeRefi(_put4_1__tmp_at553_19667);
    _put4_1__tmp_at553_19667 = NOVALUE;

    /** 	io:seek(current_db, records_ptr - 4)*/
    if (IS_ATOM_INT(_records_ptr_19568)) {
        _11106 = _records_ptr_19568 - 4;
        if ((long)((unsigned long)_11106 +(unsigned long) HIGH_BITS) >= 0){
            _11106 = NewDouble((double)_11106);
        }
    }
    else {
        _11106 = NewDouble(DBL_PTR(_records_ptr_19568)->dbl - (double)4);
    }
    DeRef(_pos_inlined_seek_at_587_19670);
    _pos_inlined_seek_at_587_19670 = _11106;
    _11106 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_587_19670);
    DeRef(_seek_1__tmp_at590_19672);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_587_19670;
    _seek_1__tmp_at590_19672 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_590_19671 = machine(19, _seek_1__tmp_at590_19672);
    DeRef(_pos_inlined_seek_at_587_19670);
    _pos_inlined_seek_at_587_19670 = NOVALUE;
    DeRef(_seek_1__tmp_at590_19672);
    _seek_1__tmp_at590_19672 = NOVALUE;

    /** 	size = get4() - 4*/
    _11107 = _48get4();
    DeRef(_size_19571);
    if (IS_ATOM_INT(_11107)) {
        _size_19571 = _11107 - 4;
        if ((long)((unsigned long)_size_19571 +(unsigned long) HIGH_BITS) >= 0){
            _size_19571 = NewDouble((double)_size_19571);
        }
    }
    else {
        _size_19571 = binary_op(MINUS, _11107, 4);
    }
    DeRef(_11107);
    _11107 = NOVALUE;

    /** 	if nrecs*4 > size-4 then*/
    if (IS_ATOM_INT(_nrecs_19569)) {
        if (_nrecs_19569 == (short)_nrecs_19569)
        _11109 = _nrecs_19569 * 4;
        else
        _11109 = NewDouble(_nrecs_19569 * (double)4);
    }
    else {
        _11109 = NewDouble(DBL_PTR(_nrecs_19569)->dbl * (double)4);
    }
    if (IS_ATOM_INT(_size_19571)) {
        _11110 = _size_19571 - 4;
        if ((long)((unsigned long)_11110 +(unsigned long) HIGH_BITS) >= 0){
            _11110 = NewDouble((double)_11110);
        }
    }
    else {
        _11110 = NewDouble(DBL_PTR(_size_19571)->dbl - (double)4);
    }
    if (binary_op_a(LESSEQ, _11109, _11110)){
        DeRef(_11109);
        _11109 = NOVALUE;
        DeRef(_11110);
        _11110 = NOVALUE;
        goto LE; // [623] 1219
    }
    DeRef(_11109);
    _11109 = NOVALUE;
    DeRef(_11110);
    _11110 = NOVALUE;

    /** 		new_size = 8 * (20 + floor(sqrt(1.5 * total_recs)))*/
    if (IS_ATOM_INT(_total_recs_19577)) {
        _11114 = NewDouble(DBL_PTR(_11113)->dbl * (double)_total_recs_19577);
    }
    else
    _11114 = NewDouble(DBL_PTR(_11113)->dbl * DBL_PTR(_total_recs_19577)->dbl);
    _11115 = unary_op(SQRT, _11114);
    DeRefDS(_11114);
    _11114 = NOVALUE;
    _11116 = unary_op(FLOOR, _11115);
    DeRefDS(_11115);
    _11115 = NOVALUE;
    if (IS_ATOM_INT(_11116)) {
        _11117 = 20 + _11116;
        if ((long)((unsigned long)_11117 + (unsigned long)HIGH_BITS) >= 0) 
        _11117 = NewDouble((double)_11117);
    }
    else {
        _11117 = binary_op(PLUS, 20, _11116);
    }
    DeRef(_11116);
    _11116 = NOVALUE;
    DeRef(_new_size_19572);
    if (IS_ATOM_INT(_11117)) {
        if (_11117 <= INT15 && _11117 >= -INT15)
        _new_size_19572 = 8 * _11117;
        else
        _new_size_19572 = NewDouble(8 * (double)_11117);
    }
    else {
        _new_size_19572 = binary_op(MULTIPLY, 8, _11117);
    }
    DeRef(_11117);
    _11117 = NOVALUE;

    /** 		new_recs = floor(new_size/8)*/
    if (IS_ATOM_INT(_new_size_19572)) {
        if (8 > 0 && _new_size_19572 >= 0) {
            _new_recs_19580 = _new_size_19572 / 8;
        }
        else {
            temp_dbl = floor((double)_new_size_19572 / (double)8);
            _new_recs_19580 = (long)temp_dbl;
        }
    }
    else {
        _2 = binary_op(DIVIDE, _new_size_19572, 8);
        _new_recs_19580 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    if (!IS_ATOM_INT(_new_recs_19580)) {
        _1 = (long)(DBL_PTR(_new_recs_19580)->dbl);
        if (UNIQUE(DBL_PTR(_new_recs_19580)) && (DBL_PTR(_new_recs_19580)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_new_recs_19580);
        _new_recs_19580 = _1;
    }

    /** 		if new_recs > floor(nrecs/2) then*/
    if (IS_ATOM_INT(_nrecs_19569)) {
        _11120 = _nrecs_19569 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _nrecs_19569, 2);
        _11120 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    if (binary_op_a(LESSEQ, _new_recs_19580, _11120)){
        DeRef(_11120);
        _11120 = NOVALUE;
        goto LF; // [661] 674
    }
    DeRef(_11120);
    _11120 = NOVALUE;

    /** 			new_recs = floor(nrecs/2)*/
    if (IS_ATOM_INT(_nrecs_19569)) {
        _new_recs_19580 = _nrecs_19569 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _nrecs_19569, 2);
        _new_recs_19580 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    if (!IS_ATOM_INT(_new_recs_19580)) {
        _1 = (long)(DBL_PTR(_new_recs_19580)->dbl);
        if (UNIQUE(DBL_PTR(_new_recs_19580)) && (DBL_PTR(_new_recs_19580)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_new_recs_19580);
        _new_recs_19580 = _1;
    }
LF: 

    /** 		io:seek(current_db, records_ptr + (nrecs-new_recs)*4)*/
    if (IS_ATOM_INT(_nrecs_19569)) {
        _11123 = _nrecs_19569 - _new_recs_19580;
        if ((long)((unsigned long)_11123 +(unsigned long) HIGH_BITS) >= 0){
            _11123 = NewDouble((double)_11123);
        }
    }
    else {
        _11123 = NewDouble(DBL_PTR(_nrecs_19569)->dbl - (double)_new_recs_19580);
    }
    if (IS_ATOM_INT(_11123)) {
        if (_11123 == (short)_11123)
        _11124 = _11123 * 4;
        else
        _11124 = NewDouble(_11123 * (double)4);
    }
    else {
        _11124 = NewDouble(DBL_PTR(_11123)->dbl * (double)4);
    }
    DeRef(_11123);
    _11123 = NOVALUE;
    if (IS_ATOM_INT(_records_ptr_19568) && IS_ATOM_INT(_11124)) {
        _11125 = _records_ptr_19568 + _11124;
        if ((long)((unsigned long)_11125 + (unsigned long)HIGH_BITS) >= 0) 
        _11125 = NewDouble((double)_11125);
    }
    else {
        if (IS_ATOM_INT(_records_ptr_19568)) {
            _11125 = NewDouble((double)_records_ptr_19568 + DBL_PTR(_11124)->dbl);
        }
        else {
            if (IS_ATOM_INT(_11124)) {
                _11125 = NewDouble(DBL_PTR(_records_ptr_19568)->dbl + (double)_11124);
            }
            else
            _11125 = NewDouble(DBL_PTR(_records_ptr_19568)->dbl + DBL_PTR(_11124)->dbl);
        }
    }
    DeRef(_11124);
    _11124 = NOVALUE;
    DeRef(_pos_inlined_seek_at_689_19695);
    _pos_inlined_seek_at_689_19695 = _11125;
    _11125 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_689_19695);
    DeRef(_seek_1__tmp_at692_19697);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_689_19695;
    _seek_1__tmp_at692_19697 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_692_19696 = machine(19, _seek_1__tmp_at692_19697);
    DeRef(_pos_inlined_seek_at_689_19695);
    _pos_inlined_seek_at_689_19695 = NOVALUE;
    DeRef(_seek_1__tmp_at692_19697);
    _seek_1__tmp_at692_19697 = NOVALUE;

    /** 		last_part = io:get_bytes(current_db, new_recs*4)*/
    if (_new_recs_19580 == (short)_new_recs_19580)
    _11126 = _new_recs_19580 * 4;
    else
    _11126 = NewDouble(_new_recs_19580 * (double)4);
    _0 = _last_part_19564;
    _last_part_19564 = _16get_bytes(_48current_db_17843, _11126);
    DeRef(_0);
    _11126 = NOVALUE;

    /** 		new_block = db_allocate(new_size)*/
    Ref(_new_size_19572);
    _0 = _new_block_19574;
    _new_block_19574 = _48db_allocate(_new_size_19572);
    DeRef(_0);

    /** 		putn(last_part)*/

    /** 	puts(current_db, s)*/
    EPuts(_48current_db_17843, _last_part_19564); // DJP 

    /** end procedure*/
    goto L10; // [738] 741
L10: 

    /** 		putn(repeat(0, new_size-length(last_part)))*/
    if (IS_SEQUENCE(_last_part_19564)){
            _11129 = SEQ_PTR(_last_part_19564)->length;
    }
    else {
        _11129 = 1;
    }
    if (IS_ATOM_INT(_new_size_19572)) {
        _11130 = _new_size_19572 - _11129;
    }
    else {
        _11130 = NewDouble(DBL_PTR(_new_size_19572)->dbl - (double)_11129);
    }
    _11129 = NOVALUE;
    _11131 = Repeat(0, _11130);
    DeRef(_11130);
    _11130 = NOVALUE;
    DeRefi(_s_inlined_putn_at_753_19706);
    _s_inlined_putn_at_753_19706 = _11131;
    _11131 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_48current_db_17843, _s_inlined_putn_at_753_19706); // DJP 

    /** end procedure*/
    goto L11; // [768] 771
L11: 
    DeRefi(_s_inlined_putn_at_753_19706);
    _s_inlined_putn_at_753_19706 = NOVALUE;

    /** 		io:seek(current_db, current_block)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_current_block_19570);
    DeRef(_seek_1__tmp_at776_19709);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _current_block_19570;
    _seek_1__tmp_at776_19709 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_776_19708 = machine(19, _seek_1__tmp_at776_19709);
    DeRef(_seek_1__tmp_at776_19709);
    _seek_1__tmp_at776_19709 = NOVALUE;

    /** 		put4(nrecs-new_recs)*/
    if (IS_ATOM_INT(_nrecs_19569)) {
        _11132 = _nrecs_19569 - _new_recs_19580;
        if ((long)((unsigned long)_11132 +(unsigned long) HIGH_BITS) >= 0){
            _11132 = NewDouble((double)_11132);
        }
    }
    else {
        _11132 = NewDouble(DBL_PTR(_nrecs_19569)->dbl - (double)_new_recs_19580);
    }
    DeRef(_x_inlined_put4_at_795_19712);
    _x_inlined_put4_at_795_19712 = _11132;
    _11132 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_795_19712)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_795_19712;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_795_19712)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at798_19713);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at798_19713 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at798_19713); // DJP 

    /** end procedure*/
    goto L12; // [820] 823
L12: 
    DeRef(_x_inlined_put4_at_795_19712);
    _x_inlined_put4_at_795_19712 = NOVALUE;
    DeRefi(_put4_1__tmp_at798_19713);
    _put4_1__tmp_at798_19713 = NOVALUE;

    /** 		io:seek(current_db, current_block+8)*/
    if (IS_ATOM_INT(_current_block_19570)) {
        _11133 = _current_block_19570 + 8;
        if ((long)((unsigned long)_11133 + (unsigned long)HIGH_BITS) >= 0) 
        _11133 = NewDouble((double)_11133);
    }
    else {
        _11133 = NewDouble(DBL_PTR(_current_block_19570)->dbl + (double)8);
    }
    DeRef(_pos_inlined_seek_at_832_19716);
    _pos_inlined_seek_at_832_19716 = _11133;
    _11133 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_832_19716);
    DeRef(_seek_1__tmp_at835_19718);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_832_19716;
    _seek_1__tmp_at835_19718 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_835_19717 = machine(19, _seek_1__tmp_at835_19718);
    DeRef(_pos_inlined_seek_at_832_19716);
    _pos_inlined_seek_at_832_19716 = NOVALUE;
    DeRef(_seek_1__tmp_at835_19718);
    _seek_1__tmp_at835_19718 = NOVALUE;

    /** 		remaining = io:get_bytes(current_db, index_ptr+blocks*8-(current_block+8))*/
    if (_blocks_19579 == (short)_blocks_19579)
    _11134 = _blocks_19579 * 8;
    else
    _11134 = NewDouble(_blocks_19579 * (double)8);
    if (IS_ATOM_INT(_index_ptr_19575) && IS_ATOM_INT(_11134)) {
        _11135 = _index_ptr_19575 + _11134;
        if ((long)((unsigned long)_11135 + (unsigned long)HIGH_BITS) >= 0) 
        _11135 = NewDouble((double)_11135);
    }
    else {
        if (IS_ATOM_INT(_index_ptr_19575)) {
            _11135 = NewDouble((double)_index_ptr_19575 + DBL_PTR(_11134)->dbl);
        }
        else {
            if (IS_ATOM_INT(_11134)) {
                _11135 = NewDouble(DBL_PTR(_index_ptr_19575)->dbl + (double)_11134);
            }
            else
            _11135 = NewDouble(DBL_PTR(_index_ptr_19575)->dbl + DBL_PTR(_11134)->dbl);
        }
    }
    DeRef(_11134);
    _11134 = NOVALUE;
    if (IS_ATOM_INT(_current_block_19570)) {
        _11136 = _current_block_19570 + 8;
        if ((long)((unsigned long)_11136 + (unsigned long)HIGH_BITS) >= 0) 
        _11136 = NewDouble((double)_11136);
    }
    else {
        _11136 = NewDouble(DBL_PTR(_current_block_19570)->dbl + (double)8);
    }
    if (IS_ATOM_INT(_11135) && IS_ATOM_INT(_11136)) {
        _11137 = _11135 - _11136;
        if ((long)((unsigned long)_11137 +(unsigned long) HIGH_BITS) >= 0){
            _11137 = NewDouble((double)_11137);
        }
    }
    else {
        if (IS_ATOM_INT(_11135)) {
            _11137 = NewDouble((double)_11135 - DBL_PTR(_11136)->dbl);
        }
        else {
            if (IS_ATOM_INT(_11136)) {
                _11137 = NewDouble(DBL_PTR(_11135)->dbl - (double)_11136);
            }
            else
            _11137 = NewDouble(DBL_PTR(_11135)->dbl - DBL_PTR(_11136)->dbl);
        }
    }
    DeRef(_11135);
    _11135 = NOVALUE;
    DeRef(_11136);
    _11136 = NOVALUE;
    _0 = _remaining_19565;
    _remaining_19565 = _16get_bytes(_48current_db_17843, _11137);
    DeRef(_0);
    _11137 = NOVALUE;

    /** 		io:seek(current_db, current_block+8)*/
    if (IS_ATOM_INT(_current_block_19570)) {
        _11139 = _current_block_19570 + 8;
        if ((long)((unsigned long)_11139 + (unsigned long)HIGH_BITS) >= 0) 
        _11139 = NewDouble((double)_11139);
    }
    else {
        _11139 = NewDouble(DBL_PTR(_current_block_19570)->dbl + (double)8);
    }
    DeRef(_pos_inlined_seek_at_883_19726);
    _pos_inlined_seek_at_883_19726 = _11139;
    _11139 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_883_19726);
    DeRef(_seek_1__tmp_at886_19728);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_883_19726;
    _seek_1__tmp_at886_19728 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_886_19727 = machine(19, _seek_1__tmp_at886_19728);
    DeRef(_pos_inlined_seek_at_883_19726);
    _pos_inlined_seek_at_883_19726 = NOVALUE;
    DeRef(_seek_1__tmp_at886_19728);
    _seek_1__tmp_at886_19728 = NOVALUE;

    /** 		put4(new_recs)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    *poke4_addr = (unsigned long)_new_recs_19580;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at901_19730);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at901_19730 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at901_19730); // DJP 

    /** end procedure*/
    goto L13; // [923] 926
L13: 
    DeRefi(_put4_1__tmp_at901_19730);
    _put4_1__tmp_at901_19730 = NOVALUE;

    /** 		put4(new_block)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_new_block_19574)) {
        *poke4_addr = (unsigned long)_new_block_19574;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_new_block_19574)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at929_19732);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at929_19732 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at929_19732); // DJP 

    /** end procedure*/
    goto L14; // [951] 954
L14: 
    DeRefi(_put4_1__tmp_at929_19732);
    _put4_1__tmp_at929_19732 = NOVALUE;

    /** 		putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_48current_db_17843, _remaining_19565); // DJP 

    /** end procedure*/
    goto L15; // [967] 970
L15: 

    /** 		io:seek(current_db, current_table_pos+8)*/
    if (IS_ATOM_INT(_48current_table_pos_17844)) {
        _11140 = _48current_table_pos_17844 + 8;
        if ((long)((unsigned long)_11140 + (unsigned long)HIGH_BITS) >= 0) 
        _11140 = NewDouble((double)_11140);
    }
    else {
        _11140 = NewDouble(DBL_PTR(_48current_table_pos_17844)->dbl + (double)8);
    }
    DeRef(_pos_inlined_seek_at_979_19736);
    _pos_inlined_seek_at_979_19736 = _11140;
    _11140 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_979_19736);
    DeRef(_seek_1__tmp_at982_19738);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_979_19736;
    _seek_1__tmp_at982_19738 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_982_19737 = machine(19, _seek_1__tmp_at982_19738);
    DeRef(_pos_inlined_seek_at_979_19736);
    _pos_inlined_seek_at_979_19736 = NOVALUE;
    DeRef(_seek_1__tmp_at982_19738);
    _seek_1__tmp_at982_19738 = NOVALUE;

    /** 		blocks += 1*/
    _blocks_19579 = _blocks_19579 + 1;

    /** 		put4(blocks)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    *poke4_addr = (unsigned long)_blocks_19579;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at1003_19741);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at1003_19741 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at1003_19741); // DJP 

    /** end procedure*/
    goto L16; // [1025] 1028
L16: 
    DeRefi(_put4_1__tmp_at1003_19741);
    _put4_1__tmp_at1003_19741 = NOVALUE;

    /** 		io:seek(current_db, index_ptr-4)*/
    if (IS_ATOM_INT(_index_ptr_19575)) {
        _11142 = _index_ptr_19575 - 4;
        if ((long)((unsigned long)_11142 +(unsigned long) HIGH_BITS) >= 0){
            _11142 = NewDouble((double)_11142);
        }
    }
    else {
        _11142 = NewDouble(DBL_PTR(_index_ptr_19575)->dbl - (double)4);
    }
    DeRef(_pos_inlined_seek_at_1037_19744);
    _pos_inlined_seek_at_1037_19744 = _11142;
    _11142 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_1037_19744);
    DeRef(_seek_1__tmp_at1040_19746);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_1037_19744;
    _seek_1__tmp_at1040_19746 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_1040_19745 = machine(19, _seek_1__tmp_at1040_19746);
    DeRef(_pos_inlined_seek_at_1037_19744);
    _pos_inlined_seek_at_1037_19744 = NOVALUE;
    DeRef(_seek_1__tmp_at1040_19746);
    _seek_1__tmp_at1040_19746 = NOVALUE;

    /** 		size = get4() - 4*/
    _11143 = _48get4();
    DeRef(_size_19571);
    if (IS_ATOM_INT(_11143)) {
        _size_19571 = _11143 - 4;
        if ((long)((unsigned long)_size_19571 +(unsigned long) HIGH_BITS) >= 0){
            _size_19571 = NewDouble((double)_size_19571);
        }
    }
    else {
        _size_19571 = binary_op(MINUS, _11143, 4);
    }
    DeRef(_11143);
    _11143 = NOVALUE;

    /** 		if blocks*8 > size-8 then*/
    if (_blocks_19579 == (short)_blocks_19579)
    _11145 = _blocks_19579 * 8;
    else
    _11145 = NewDouble(_blocks_19579 * (double)8);
    if (IS_ATOM_INT(_size_19571)) {
        _11146 = _size_19571 - 8;
        if ((long)((unsigned long)_11146 +(unsigned long) HIGH_BITS) >= 0){
            _11146 = NewDouble((double)_11146);
        }
    }
    else {
        _11146 = NewDouble(DBL_PTR(_size_19571)->dbl - (double)8);
    }
    if (binary_op_a(LESSEQ, _11145, _11146)){
        DeRef(_11145);
        _11145 = NOVALUE;
        DeRef(_11146);
        _11146 = NOVALUE;
        goto L17; // [1073] 1218
    }
    DeRef(_11145);
    _11145 = NOVALUE;
    DeRef(_11146);
    _11146 = NOVALUE;

    /** 			remaining = io:get_bytes(current_db, blocks*8)*/
    if (_blocks_19579 == (short)_blocks_19579)
    _11148 = _blocks_19579 * 8;
    else
    _11148 = NewDouble(_blocks_19579 * (double)8);
    _0 = _remaining_19565;
    _remaining_19565 = _16get_bytes(_48current_db_17843, _11148);
    DeRef(_0);
    _11148 = NOVALUE;

    /** 			new_size = floor(size + size/2)*/
    if (IS_ATOM_INT(_size_19571)) {
        if (_size_19571 & 1) {
            _11150 = NewDouble((_size_19571 >> 1) + 0.5);
        }
        else
        _11150 = _size_19571 >> 1;
    }
    else {
        _11150 = binary_op(DIVIDE, _size_19571, 2);
    }
    if (IS_ATOM_INT(_size_19571) && IS_ATOM_INT(_11150)) {
        _11151 = _size_19571 + _11150;
        if ((long)((unsigned long)_11151 + (unsigned long)HIGH_BITS) >= 0) 
        _11151 = NewDouble((double)_11151);
    }
    else {
        if (IS_ATOM_INT(_size_19571)) {
            _11151 = NewDouble((double)_size_19571 + DBL_PTR(_11150)->dbl);
        }
        else {
            if (IS_ATOM_INT(_11150)) {
                _11151 = NewDouble(DBL_PTR(_size_19571)->dbl + (double)_11150);
            }
            else
            _11151 = NewDouble(DBL_PTR(_size_19571)->dbl + DBL_PTR(_11150)->dbl);
        }
    }
    DeRef(_11150);
    _11150 = NOVALUE;
    DeRef(_new_size_19572);
    if (IS_ATOM_INT(_11151))
    _new_size_19572 = e_floor(_11151);
    else
    _new_size_19572 = unary_op(FLOOR, _11151);
    DeRef(_11151);
    _11151 = NOVALUE;

    /** 			new_index_ptr = db_allocate(new_size)*/
    Ref(_new_size_19572);
    _0 = _new_index_ptr_19576;
    _new_index_ptr_19576 = _48db_allocate(_new_size_19572);
    DeRef(_0);

    /** 			putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_48current_db_17843, _remaining_19565); // DJP 

    /** end procedure*/
    goto L18; // [1122] 1125
L18: 

    /** 			putn(repeat(0, new_size-blocks*8))*/
    if (_blocks_19579 == (short)_blocks_19579)
    _11154 = _blocks_19579 * 8;
    else
    _11154 = NewDouble(_blocks_19579 * (double)8);
    if (IS_ATOM_INT(_new_size_19572) && IS_ATOM_INT(_11154)) {
        _11155 = _new_size_19572 - _11154;
    }
    else {
        if (IS_ATOM_INT(_new_size_19572)) {
            _11155 = NewDouble((double)_new_size_19572 - DBL_PTR(_11154)->dbl);
        }
        else {
            if (IS_ATOM_INT(_11154)) {
                _11155 = NewDouble(DBL_PTR(_new_size_19572)->dbl - (double)_11154);
            }
            else
            _11155 = NewDouble(DBL_PTR(_new_size_19572)->dbl - DBL_PTR(_11154)->dbl);
        }
    }
    DeRef(_11154);
    _11154 = NOVALUE;
    _11156 = Repeat(0, _11155);
    DeRef(_11155);
    _11155 = NOVALUE;
    DeRefi(_s_inlined_putn_at_1138_19764);
    _s_inlined_putn_at_1138_19764 = _11156;
    _11156 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_48current_db_17843, _s_inlined_putn_at_1138_19764); // DJP 

    /** end procedure*/
    goto L19; // [1153] 1156
L19: 
    DeRefi(_s_inlined_putn_at_1138_19764);
    _s_inlined_putn_at_1138_19764 = NOVALUE;

    /** 			db_free(index_ptr)*/
    Ref(_index_ptr_19575);
    _48db_free(_index_ptr_19575);

    /** 			io:seek(current_db, current_table_pos+12)*/
    if (IS_ATOM_INT(_48current_table_pos_17844)) {
        _11157 = _48current_table_pos_17844 + 12;
        if ((long)((unsigned long)_11157 + (unsigned long)HIGH_BITS) >= 0) 
        _11157 = NewDouble((double)_11157);
    }
    else {
        _11157 = NewDouble(DBL_PTR(_48current_table_pos_17844)->dbl + (double)12);
    }
    DeRef(_pos_inlined_seek_at_1172_19767);
    _pos_inlined_seek_at_1172_19767 = _11157;
    _11157 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_1172_19767);
    DeRef(_seek_1__tmp_at1175_19769);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_1172_19767;
    _seek_1__tmp_at1175_19769 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_1175_19768 = machine(19, _seek_1__tmp_at1175_19769);
    DeRef(_pos_inlined_seek_at_1172_19767);
    _pos_inlined_seek_at_1172_19767 = NOVALUE;
    DeRef(_seek_1__tmp_at1175_19769);
    _seek_1__tmp_at1175_19769 = NOVALUE;

    /** 			put4(new_index_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_new_index_ptr_19576)) {
        *poke4_addr = (unsigned long)_new_index_ptr_19576;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_new_index_ptr_19576)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at1190_19771);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at1190_19771 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at1190_19771); // DJP 

    /** end procedure*/
    goto L1A; // [1212] 1215
L1A: 
    DeRefi(_put4_1__tmp_at1190_19771);
    _put4_1__tmp_at1190_19771 = NOVALUE;
L17: 
LE: 

    /** 	return DB_OK*/
    DeRef(_key_19559);
    DeRef(_table_name_19561);
    DeRef(_key_string_19562);
    DeRef(_data_string_19563);
    DeRef(_last_part_19564);
    DeRef(_remaining_19565);
    DeRef(_key_ptr_19566);
    DeRef(_data_ptr_19567);
    DeRef(_records_ptr_19568);
    DeRef(_nrecs_19569);
    DeRef(_current_block_19570);
    DeRef(_size_19571);
    DeRef(_new_size_19572);
    DeRef(_key_location_19573);
    DeRef(_new_block_19574);
    DeRef(_index_ptr_19575);
    DeRef(_new_index_ptr_19576);
    DeRef(_total_recs_19577);
    DeRef(_11101);
    _11101 = NOVALUE;
    DeRef(_11103);
    _11103 = NOVALUE;
    return 0;
    ;
}


void _48db_replace_data(int _key_location_19906, int _data_19907, int _table_name_19908)
{
    int _11231 = NOVALUE;
    int _11230 = NOVALUE;
    int _11229 = NOVALUE;
    int _11228 = NOVALUE;
    int _11226 = NOVALUE;
    int _11225 = NOVALUE;
    int _11223 = NOVALUE;
    int _11220 = NOVALUE;
    int _11218 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_19908 == _48current_table_name_17845)
    _11218 = 1;
    else if (IS_ATOM_INT(_table_name_19908) && IS_ATOM_INT(_48current_table_name_17845))
    _11218 = 0;
    else
    _11218 = (compare(_table_name_19908, _48current_table_name_17845) == 0);
    if (_11218 != 0)
    goto L1; // [11] 49
    _11218 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    RefDS(_table_name_19908);
    _11220 = _48db_select_table(_table_name_19908);
    if (binary_op_a(EQUALS, _11220, 0)){
        DeRef(_11220);
        _11220 = NOVALUE;
        goto L2; // [22] 48
    }
    DeRef(_11220);
    _11220 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_replace_data", {key_location, data, table_name})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _key_location_19906;
    *((int *)(_2+8)) = _data_19907;
    RefDS(_table_name_19908);
    *((int *)(_2+12)) = _table_name_19908;
    _11223 = MAKE_SEQ(_1);
    RefDS(_11042);
    RefDS(_11222);
    _48fatal(903, _11042, _11222, _11223);
    _11223 = NOVALUE;

    /** 			return*/
    DeRefDS(_table_name_19908);
    return;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _48current_table_pos_17844, -1)){
        goto L3; // [53] 79
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_replace_data", {key_location, data, table_name})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _key_location_19906;
    *((int *)(_2+8)) = _data_19907;
    Ref(_table_name_19908);
    *((int *)(_2+12)) = _table_name_19908;
    _11225 = MAKE_SEQ(_1);
    RefDS(_11046);
    RefDS(_11222);
    _48fatal(903, _11046, _11222, _11225);
    _11225 = NOVALUE;

    /** 		return*/
    DeRef(_table_name_19908);
    return;
L3: 

    /** 	if key_location < 1 or key_location > length(key_pointers) then*/
    _11226 = (_key_location_19906 < 1);
    if (_11226 != 0) {
        goto L4; // [85] 103
    }
    if (IS_SEQUENCE(_48key_pointers_17850)){
            _11228 = SEQ_PTR(_48key_pointers_17850)->length;
    }
    else {
        _11228 = 1;
    }
    _11229 = (_key_location_19906 > _11228);
    _11228 = NOVALUE;
    if (_11229 == 0)
    {
        DeRef(_11229);
        _11229 = NOVALUE;
        goto L5; // [99] 125
    }
    else{
        DeRef(_11229);
        _11229 = NOVALUE;
    }
L4: 

    /** 		fatal(BAD_RECNO, "bad record number", "db_replace_data", {key_location, data, table_name})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _key_location_19906;
    *((int *)(_2+8)) = _data_19907;
    Ref(_table_name_19908);
    *((int *)(_2+12)) = _table_name_19908;
    _11230 = MAKE_SEQ(_1);
    RefDS(_11170);
    RefDS(_11222);
    _48fatal(905, _11170, _11222, _11230);
    _11230 = NOVALUE;

    /** 		return*/
    DeRef(_table_name_19908);
    DeRef(_11226);
    _11226 = NOVALUE;
    return;
L5: 

    /** 	db_replace_recid(key_pointers[key_location], data)*/
    _2 = (int)SEQ_PTR(_48key_pointers_17850);
    _11231 = (int)*(((s1_ptr)_2)->base + _key_location_19906);
    Ref(_11231);
    _48db_replace_recid(_11231, _data_19907);
    _11231 = NOVALUE;

    /** end procedure*/
    DeRef(_table_name_19908);
    DeRef(_11226);
    _11226 = NOVALUE;
    return;
    ;
}


int _48db_table_size(int _table_name_19930)
{
    int _11240 = NOVALUE;
    int _11239 = NOVALUE;
    int _11237 = NOVALUE;
    int _11234 = NOVALUE;
    int _11232 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_19930 == _48current_table_name_17845)
    _11232 = 1;
    else if (IS_ATOM_INT(_table_name_19930) && IS_ATOM_INT(_48current_table_name_17845))
    _11232 = 0;
    else
    _11232 = (compare(_table_name_19930, _48current_table_name_17845) == 0);
    if (_11232 != 0)
    goto L1; // [9] 46
    _11232 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    RefDS(_table_name_19930);
    _11234 = _48db_select_table(_table_name_19930);
    if (binary_op_a(EQUALS, _11234, 0)){
        DeRef(_11234);
        _11234 = NOVALUE;
        goto L2; // [20] 45
    }
    DeRef(_11234);
    _11234 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_table_size", {table_name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_table_name_19930);
    *((int *)(_2+4)) = _table_name_19930;
    _11237 = MAKE_SEQ(_1);
    RefDS(_11042);
    RefDS(_11236);
    _48fatal(903, _11042, _11236, _11237);
    _11237 = NOVALUE;

    /** 			return -1*/
    DeRefDS(_table_name_19930);
    return -1;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _48current_table_pos_17844, -1)){
        goto L3; // [50] 75
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_table_size", {table_name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_table_name_19930);
    *((int *)(_2+4)) = _table_name_19930;
    _11239 = MAKE_SEQ(_1);
    RefDS(_11046);
    RefDS(_11236);
    _48fatal(903, _11046, _11236, _11239);
    _11239 = NOVALUE;

    /** 		return -1*/
    DeRef(_table_name_19930);
    return -1;
L3: 

    /** 	return length(key_pointers)*/
    if (IS_SEQUENCE(_48key_pointers_17850)){
            _11240 = SEQ_PTR(_48key_pointers_17850)->length;
    }
    else {
        _11240 = 1;
    }
    DeRef(_table_name_19930);
    return _11240;
    ;
}


int _48db_record_data(int _key_location_19945, int _table_name_19946)
{
    int _data_ptr_19947 = NOVALUE;
    int _data_value_19948 = NOVALUE;
    int _seek_1__tmp_at134_19970 = NOVALUE;
    int _seek_inlined_seek_at_134_19969 = NOVALUE;
    int _pos_inlined_seek_at_131_19968 = NOVALUE;
    int _seek_1__tmp_at172_19977 = NOVALUE;
    int _seek_inlined_seek_at_172_19976 = NOVALUE;
    int _11255 = NOVALUE;
    int _11254 = NOVALUE;
    int _11253 = NOVALUE;
    int _11252 = NOVALUE;
    int _11251 = NOVALUE;
    int _11249 = NOVALUE;
    int _11248 = NOVALUE;
    int _11246 = NOVALUE;
    int _11243 = NOVALUE;
    int _11241 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_key_location_19945)) {
        _1 = (long)(DBL_PTR(_key_location_19945)->dbl);
        if (UNIQUE(DBL_PTR(_key_location_19945)) && (DBL_PTR(_key_location_19945)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_key_location_19945);
        _key_location_19945 = _1;
    }

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_19946 == _48current_table_name_17845)
    _11241 = 1;
    else if (IS_ATOM_INT(_table_name_19946) && IS_ATOM_INT(_48current_table_name_17845))
    _11241 = 0;
    else
    _11241 = (compare(_table_name_19946, _48current_table_name_17845) == 0);
    if (_11241 != 0)
    goto L1; // [11] 48
    _11241 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    RefDS(_table_name_19946);
    _11243 = _48db_select_table(_table_name_19946);
    if (binary_op_a(EQUALS, _11243, 0)){
        DeRef(_11243);
        _11243 = NOVALUE;
        goto L2; // [22] 47
    }
    DeRef(_11243);
    _11243 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_record_data", {key_location, table_name})*/
    RefDS(_table_name_19946);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_19945;
    ((int *)_2)[2] = _table_name_19946;
    _11246 = MAKE_SEQ(_1);
    RefDS(_11042);
    RefDS(_11245);
    _48fatal(903, _11042, _11245, _11246);
    _11246 = NOVALUE;

    /** 			return -1*/
    DeRefDS(_table_name_19946);
    DeRef(_data_ptr_19947);
    DeRef(_data_value_19948);
    return -1;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _48current_table_pos_17844, -1)){
        goto L3; // [52] 77
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_record_data", {key_location, table_name})*/
    Ref(_table_name_19946);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_19945;
    ((int *)_2)[2] = _table_name_19946;
    _11248 = MAKE_SEQ(_1);
    RefDS(_11046);
    RefDS(_11245);
    _48fatal(903, _11046, _11245, _11248);
    _11248 = NOVALUE;

    /** 		return -1*/
    DeRef(_table_name_19946);
    DeRef(_data_ptr_19947);
    DeRef(_data_value_19948);
    return -1;
L3: 

    /** 	if key_location < 1 or key_location > length(key_pointers) then*/
    _11249 = (_key_location_19945 < 1);
    if (_11249 != 0) {
        goto L4; // [83] 101
    }
    if (IS_SEQUENCE(_48key_pointers_17850)){
            _11251 = SEQ_PTR(_48key_pointers_17850)->length;
    }
    else {
        _11251 = 1;
    }
    _11252 = (_key_location_19945 > _11251);
    _11251 = NOVALUE;
    if (_11252 == 0)
    {
        DeRef(_11252);
        _11252 = NOVALUE;
        goto L5; // [97] 122
    }
    else{
        DeRef(_11252);
        _11252 = NOVALUE;
    }
L4: 

    /** 		fatal(BAD_RECNO, "bad record number", "db_record_data", {key_location, table_name})*/
    Ref(_table_name_19946);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_19945;
    ((int *)_2)[2] = _table_name_19946;
    _11253 = MAKE_SEQ(_1);
    RefDS(_11170);
    RefDS(_11245);
    _48fatal(905, _11170, _11245, _11253);
    _11253 = NOVALUE;

    /** 		return -1*/
    DeRef(_table_name_19946);
    DeRef(_data_ptr_19947);
    DeRef(_data_value_19948);
    DeRef(_11249);
    _11249 = NOVALUE;
    return -1;
L5: 

    /** 	io:seek(current_db, key_pointers[key_location])*/
    _2 = (int)SEQ_PTR(_48key_pointers_17850);
    _11254 = (int)*(((s1_ptr)_2)->base + _key_location_19945);
    Ref(_11254);
    DeRef(_pos_inlined_seek_at_131_19968);
    _pos_inlined_seek_at_131_19968 = _11254;
    _11254 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_131_19968);
    DeRef(_seek_1__tmp_at134_19970);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _pos_inlined_seek_at_131_19968;
    _seek_1__tmp_at134_19970 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_134_19969 = machine(19, _seek_1__tmp_at134_19970);
    DeRef(_pos_inlined_seek_at_131_19968);
    _pos_inlined_seek_at_131_19968 = NOVALUE;
    DeRef(_seek_1__tmp_at134_19970);
    _seek_1__tmp_at134_19970 = NOVALUE;

    /** 	if length(vLastErrors) > 0 then return -1 end if*/
    if (IS_SEQUENCE(_48vLastErrors_17867)){
            _11255 = SEQ_PTR(_48vLastErrors_17867)->length;
    }
    else {
        _11255 = 1;
    }
    if (_11255 <= 0)
    goto L6; // [155] 164
    DeRef(_table_name_19946);
    DeRef(_data_ptr_19947);
    DeRef(_data_value_19948);
    DeRef(_11249);
    _11249 = NOVALUE;
    return -1;
L6: 

    /** 	data_ptr = get4()*/
    _0 = _data_ptr_19947;
    _data_ptr_19947 = _48get4();
    DeRef(_0);

    /** 	io:seek(current_db, data_ptr)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_data_ptr_19947);
    DeRef(_seek_1__tmp_at172_19977);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _48current_db_17843;
    ((int *)_2)[2] = _data_ptr_19947;
    _seek_1__tmp_at172_19977 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_172_19976 = machine(19, _seek_1__tmp_at172_19977);
    DeRef(_seek_1__tmp_at172_19977);
    _seek_1__tmp_at172_19977 = NOVALUE;

    /** 	data_value = decompress(0)*/
    _0 = _data_value_19948;
    _data_value_19948 = _48decompress(0);
    DeRef(_0);

    /** 	return data_value*/
    DeRef(_table_name_19946);
    DeRef(_data_ptr_19947);
    DeRef(_11249);
    _11249 = NOVALUE;
    return _data_value_19948;
    ;
}


int _48db_fetch_record(int _key_19981, int _table_name_19982)
{
    int _pos_19983 = NOVALUE;
    int _11261 = NOVALUE;
    int _0, _1, _2;
    

    /** 	pos = db_find_key(key, table_name)*/
    Ref(_key_19981);
    RefDS(_table_name_19982);
    _pos_19983 = _48db_find_key(_key_19981, _table_name_19982);
    if (!IS_ATOM_INT(_pos_19983)) {
        _1 = (long)(DBL_PTR(_pos_19983)->dbl);
        if (UNIQUE(DBL_PTR(_pos_19983)) && (DBL_PTR(_pos_19983)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_19983);
        _pos_19983 = _1;
    }

    /** 	if pos > 0 then*/
    if (_pos_19983 <= 0)
    goto L1; // [12] 30

    /** 		return db_record_data(pos, table_name)*/
    RefDS(_table_name_19982);
    _11261 = _48db_record_data(_pos_19983, _table_name_19982);
    DeRef(_key_19981);
    DeRefDS(_table_name_19982);
    return _11261;
    goto L2; // [27] 37
L1: 

    /** 		return pos*/
    DeRef(_key_19981);
    DeRef(_table_name_19982);
    DeRef(_11261);
    _11261 = NOVALUE;
    return _pos_19983;
L2: 
    ;
}


int _48db_record_key(int _key_location_19991, int _table_name_19992)
{
    int _11276 = NOVALUE;
    int _11275 = NOVALUE;
    int _11274 = NOVALUE;
    int _11273 = NOVALUE;
    int _11272 = NOVALUE;
    int _11270 = NOVALUE;
    int _11269 = NOVALUE;
    int _11267 = NOVALUE;
    int _11264 = NOVALUE;
    int _11262 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_key_location_19991)) {
        _1 = (long)(DBL_PTR(_key_location_19991)->dbl);
        if (UNIQUE(DBL_PTR(_key_location_19991)) && (DBL_PTR(_key_location_19991)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_key_location_19991);
        _key_location_19991 = _1;
    }

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_19992 == _48current_table_name_17845)
    _11262 = 1;
    else if (IS_ATOM_INT(_table_name_19992) && IS_ATOM_INT(_48current_table_name_17845))
    _11262 = 0;
    else
    _11262 = (compare(_table_name_19992, _48current_table_name_17845) == 0);
    if (_11262 != 0)
    goto L1; // [11] 48
    _11262 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    RefDS(_table_name_19992);
    _11264 = _48db_select_table(_table_name_19992);
    if (binary_op_a(EQUALS, _11264, 0)){
        DeRef(_11264);
        _11264 = NOVALUE;
        goto L2; // [22] 47
    }
    DeRef(_11264);
    _11264 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_record_key", {key_location, table_name})*/
    RefDS(_table_name_19992);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_19991;
    ((int *)_2)[2] = _table_name_19992;
    _11267 = MAKE_SEQ(_1);
    RefDS(_11042);
    RefDS(_11266);
    _48fatal(903, _11042, _11266, _11267);
    _11267 = NOVALUE;

    /** 			return -1*/
    DeRefDS(_table_name_19992);
    return -1;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _48current_table_pos_17844, -1)){
        goto L3; // [52] 77
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_record_key", {key_location, table_name})*/
    Ref(_table_name_19992);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_19991;
    ((int *)_2)[2] = _table_name_19992;
    _11269 = MAKE_SEQ(_1);
    RefDS(_11046);
    RefDS(_11266);
    _48fatal(903, _11046, _11266, _11269);
    _11269 = NOVALUE;

    /** 		return -1*/
    DeRef(_table_name_19992);
    return -1;
L3: 

    /** 	if key_location < 1 or key_location > length(key_pointers) then*/
    _11270 = (_key_location_19991 < 1);
    if (_11270 != 0) {
        goto L4; // [83] 101
    }
    if (IS_SEQUENCE(_48key_pointers_17850)){
            _11272 = SEQ_PTR(_48key_pointers_17850)->length;
    }
    else {
        _11272 = 1;
    }
    _11273 = (_key_location_19991 > _11272);
    _11272 = NOVALUE;
    if (_11273 == 0)
    {
        DeRef(_11273);
        _11273 = NOVALUE;
        goto L5; // [97] 122
    }
    else{
        DeRef(_11273);
        _11273 = NOVALUE;
    }
L4: 

    /** 		fatal(BAD_RECNO, "bad record number", "db_record_key", {key_location, table_name})*/
    Ref(_table_name_19992);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_19991;
    ((int *)_2)[2] = _table_name_19992;
    _11274 = MAKE_SEQ(_1);
    RefDS(_11170);
    RefDS(_11266);
    _48fatal(905, _11170, _11266, _11274);
    _11274 = NOVALUE;

    /** 		return -1*/
    DeRef(_table_name_19992);
    DeRef(_11270);
    _11270 = NOVALUE;
    return -1;
L5: 

    /** 	return key_value(key_pointers[key_location])*/
    _2 = (int)SEQ_PTR(_48key_pointers_17850);
    _11275 = (int)*(((s1_ptr)_2)->base + _key_location_19991);
    Ref(_11275);
    _11276 = _48key_value(_11275);
    _11275 = NOVALUE;
    DeRef(_table_name_19992);
    DeRef(_11270);
    _11270 = NOVALUE;
    return _11276;
    ;
}


void _48db_replace_recid(int _recid_20105, int _data_20106)
{
    int _old_size_20107 = NOVALUE;
    int _new_size_20108 = NOVALUE;
    int _data_ptr_20109 = NOVALUE;
    int _data_string_20110 = NOVALUE;
    int _put4_1__tmp_at111_20130 = NOVALUE;
    int _11375 = NOVALUE;
    int _11374 = NOVALUE;
    int _11373 = NOVALUE;
    int _11372 = NOVALUE;
    int _11371 = NOVALUE;
    int _11343 = NOVALUE;
    int _11341 = NOVALUE;
    int _11340 = NOVALUE;
    int _11339 = NOVALUE;
    int _11338 = NOVALUE;
    int _11337 = NOVALUE;
    int _11333 = NOVALUE;
    int _11332 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_recid_20105)) {
        _1 = (long)(DBL_PTR(_recid_20105)->dbl);
        if (UNIQUE(DBL_PTR(_recid_20105)) && (DBL_PTR(_recid_20105)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_recid_20105);
        _recid_20105 = _1;
    }

    /** 	seek(current_db, recid)*/
    _11375 = _16seek(_48current_db_17843, _recid_20105);
    DeRef(_11375);
    _11375 = NOVALUE;

    /** 	data_ptr = get4()*/
    _0 = _data_ptr_20109;
    _data_ptr_20109 = _48get4();
    DeRef(_0);

    /** 	seek(current_db, data_ptr-4)*/
    if (IS_ATOM_INT(_data_ptr_20109)) {
        _11332 = _data_ptr_20109 - 4;
        if ((long)((unsigned long)_11332 +(unsigned long) HIGH_BITS) >= 0){
            _11332 = NewDouble((double)_11332);
        }
    }
    else {
        _11332 = NewDouble(DBL_PTR(_data_ptr_20109)->dbl - (double)4);
    }
    _11374 = _16seek(_48current_db_17843, _11332);
    _11332 = NOVALUE;
    DeRef(_11374);
    _11374 = NOVALUE;

    /** 	old_size = get4()-4*/
    _11333 = _48get4();
    DeRef(_old_size_20107);
    if (IS_ATOM_INT(_11333)) {
        _old_size_20107 = _11333 - 4;
        if ((long)((unsigned long)_old_size_20107 +(unsigned long) HIGH_BITS) >= 0){
            _old_size_20107 = NewDouble((double)_old_size_20107);
        }
    }
    else {
        _old_size_20107 = binary_op(MINUS, _11333, 4);
    }
    DeRef(_11333);
    _11333 = NOVALUE;

    /** 	data_string = compress(data)*/
    _0 = _data_string_20110;
    _data_string_20110 = _48compress(_data_20106);
    DeRef(_0);

    /** 	new_size = length(data_string)*/
    if (IS_SEQUENCE(_data_string_20110)){
            _new_size_20108 = SEQ_PTR(_data_string_20110)->length;
    }
    else {
        _new_size_20108 = 1;
    }

    /** 	if new_size <= old_size and*/
    if (IS_ATOM_INT(_old_size_20107)) {
        _11337 = (_new_size_20108 <= _old_size_20107);
    }
    else {
        _11337 = ((double)_new_size_20108 <= DBL_PTR(_old_size_20107)->dbl);
    }
    if (_11337 == 0) {
        goto L1; // [62] 92
    }
    if (IS_ATOM_INT(_old_size_20107)) {
        _11339 = _old_size_20107 - 16;
        if ((long)((unsigned long)_11339 +(unsigned long) HIGH_BITS) >= 0){
            _11339 = NewDouble((double)_11339);
        }
    }
    else {
        _11339 = NewDouble(DBL_PTR(_old_size_20107)->dbl - (double)16);
    }
    if (IS_ATOM_INT(_11339)) {
        _11340 = (_new_size_20108 >= _11339);
    }
    else {
        _11340 = ((double)_new_size_20108 >= DBL_PTR(_11339)->dbl);
    }
    DeRef(_11339);
    _11339 = NOVALUE;
    if (_11340 == 0)
    {
        DeRef(_11340);
        _11340 = NOVALUE;
        goto L1; // [75] 92
    }
    else{
        DeRef(_11340);
        _11340 = NOVALUE;
    }

    /** 		seek(current_db, data_ptr)*/
    Ref(_data_ptr_20109);
    _11373 = _16seek(_48current_db_17843, _data_ptr_20109);
    DeRef(_11373);
    _11373 = NOVALUE;
    goto L2; // [89] 168
L1: 

    /** 		db_free(data_ptr)*/
    Ref(_data_ptr_20109);
    _48db_free(_data_ptr_20109);

    /** 		data_ptr = db_allocate(new_size + 8)*/
    _11341 = _new_size_20108 + 8;
    if ((long)((unsigned long)_11341 + (unsigned long)HIGH_BITS) >= 0) 
    _11341 = NewDouble((double)_11341);
    _0 = _data_ptr_20109;
    _data_ptr_20109 = _48db_allocate(_11341);
    DeRef(_0);
    _11341 = NOVALUE;

    /** 		seek(current_db, recid)*/
    _11372 = _16seek(_48current_db_17843, _recid_20105);
    DeRef(_11372);
    _11372 = NOVALUE;

    /** 		put4(data_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_48mem0_17885)){
        poke4_addr = (unsigned long *)_48mem0_17885;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_48mem0_17885)->dbl);
    }
    if (IS_ATOM_INT(_data_ptr_20109)) {
        *poke4_addr = (unsigned long)_data_ptr_20109;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_data_ptr_20109)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at111_20130);
    _1 = (int)SEQ_PTR(_48memseq_18120);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at111_20130 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_48current_db_17843, _put4_1__tmp_at111_20130); // DJP 

    /** end procedure*/
    goto L3; // [141] 144
L3: 
    DeRefi(_put4_1__tmp_at111_20130);
    _put4_1__tmp_at111_20130 = NOVALUE;

    /** 		seek(current_db, data_ptr)*/
    Ref(_data_ptr_20109);
    _11371 = _16seek(_48current_db_17843, _data_ptr_20109);
    DeRef(_11371);
    _11371 = NOVALUE;

    /** 		data_string &= repeat( 0, 8 )*/
    _11343 = Repeat(0, 8);
    Concat((object_ptr)&_data_string_20110, _data_string_20110, _11343);
    DeRefDS(_11343);
    _11343 = NOVALUE;
L2: 

    /** 	putn(data_string)*/

    /** 	puts(current_db, s)*/
    EPuts(_48current_db_17843, _data_string_20110); // DJP 

    /** end procedure*/
    goto L4; // [179] 182
L4: 

    /** end procedure*/
    DeRef(_old_size_20107);
    DeRef(_data_ptr_20109);
    DeRef(_data_string_20110);
    DeRef(_11337);
    _11337 = NOVALUE;
    return;
    ;
}



// 0x9A4E78D4
