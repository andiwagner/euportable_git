// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _39add_options(int _new_options_49986)
{
    int _0, _1, _2;
    

    /** 	options = splice(options, new_options, COMMON_OPTIONS_SPLICE_IDX)*/
    {
        s1_ptr assign_space;
        insert_pos = 15;
        if (insert_pos <= 0) {
            Concat(&_39options_49982,_new_options_49986,_39options_49982);
        }
        else if (insert_pos > SEQ_PTR(_39options_49982)->length){
            Concat(&_39options_49982,_39options_49982,_new_options_49986);
        }
        else if (IS_SEQUENCE(_new_options_49986)) {
            if( _39options_49982 != _39options_49982 || SEQ_PTR( _39options_49982 )->ref != 1 ){
                DeRef( _39options_49982 );
                RefDS( _39options_49982 );
            }
            assign_space = Add_internal_space( _39options_49982, insert_pos,((s1_ptr)SEQ_PTR(_new_options_49986))->length);
            assign_slice_seq = &assign_space;
            assign_space = Copy_elements( insert_pos, SEQ_PTR(_new_options_49986), _39options_49982 == _39options_49982 );
            _39options_49982 = MAKE_SEQ( assign_space );
        }
        else {
            if( _39options_49982 == _39options_49982 && SEQ_PTR( _39options_49982 )->ref == 1 ){
                _39options_49982 = Insert( _39options_49982, _new_options_49986, insert_pos);
            }
            else {
                DeRef( _39options_49982 );
                RefDS( _39options_49982 );
                _39options_49982 = Insert( _39options_49982, _new_options_49986, insert_pos);
            }
        }
    }

    /** end procedure*/
    DeRefDS(_new_options_49986);
    return;
    ;
}


int _39get_options()
{
    int _0, _1, _2;
    

    /** 	return options*/
    RefDS(_39options_49982);
    return _39options_49982;
    ;
}


int _39get_common_options()
{
    int _0, _1, _2;
    

    /** 	return COMMON_OPTIONS*/
    RefDS(_39COMMON_OPTIONS_49880);
    return _39COMMON_OPTIONS_49880;
    ;
}


int _39get_switches()
{
    int _0, _1, _2;
    

    /** 	return switches*/
    RefDS(_39switches_49879);
    return _39switches_49879;
    ;
}


void _39show_copyrights()
{
    int _notices_49996 = NOVALUE;
    int _26292 = NOVALUE;
    int _26291 = NOVALUE;
    int _26289 = NOVALUE;
    int _26288 = NOVALUE;
    int _26287 = NOVALUE;
    int _26286 = NOVALUE;
    int _26284 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence notices = all_copyrights()*/
    _0 = _notices_49996;
    _notices_49996 = _31all_copyrights();
    DeRef(_0);

    /** 	for i = 1 to length(notices) do*/
    if (IS_SEQUENCE(_notices_49996)){
            _26284 = SEQ_PTR(_notices_49996)->length;
    }
    else {
        _26284 = 1;
    }
    {
        int _i_50000;
        _i_50000 = 1;
L1: 
        if (_i_50000 > _26284){
            goto L2; // [13] 60
        }

        /** 		printf(2, "%s\n  %s\n\n", { notices[i][1], match_replace("\n", notices[i][2], "\n  ") })*/
        _2 = (int)SEQ_PTR(_notices_49996);
        _26286 = (int)*(((s1_ptr)_2)->base + _i_50000);
        _2 = (int)SEQ_PTR(_26286);
        _26287 = (int)*(((s1_ptr)_2)->base + 1);
        _26286 = NOVALUE;
        _2 = (int)SEQ_PTR(_notices_49996);
        _26288 = (int)*(((s1_ptr)_2)->base + _i_50000);
        _2 = (int)SEQ_PTR(_26288);
        _26289 = (int)*(((s1_ptr)_2)->base + 2);
        _26288 = NOVALUE;
        RefDS(_22834);
        Ref(_26289);
        RefDS(_26290);
        _26291 = _7match_replace(_22834, _26289, _26290, 0);
        _26289 = NOVALUE;
        Ref(_26287);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _26287;
        ((int *)_2)[2] = _26291;
        _26292 = MAKE_SEQ(_1);
        _26291 = NOVALUE;
        _26287 = NOVALUE;
        EPrintf(2, _26285, _26292);
        DeRefDS(_26292);
        _26292 = NOVALUE;

        /** 	end for*/
        _i_50000 = _i_50000 + 1;
        goto L1; // [55] 20
L2: 
        ;
    }

    /** end procedure*/
    DeRef(_notices_49996);
    return;
    ;
}


void _39show_banner()
{
    int _version_type_inlined_version_type_at_212_50063 = NOVALUE;
    int _version_string_short_1__tmp_at192_50061 = NOVALUE;
    int _version_string_short_inlined_version_string_short_at_192_50060 = NOVALUE;
    int _version_revision_inlined_version_revision_at_121_50042 = NOVALUE;
    int _platform_name_inlined_platform_name_at_83_50034 = NOVALUE;
    int _prod_name_50013 = NOVALUE;
    int _memory_type_50014 = NOVALUE;
    int _misc_info_50032 = NOVALUE;
    int _EuConsole_50046 = NOVALUE;
    int _26316 = NOVALUE;
    int _26315 = NOVALUE;
    int _26314 = NOVALUE;
    int _26311 = NOVALUE;
    int _26310 = NOVALUE;
    int _26306 = NOVALUE;
    int _26305 = NOVALUE;
    int _26304 = NOVALUE;
    int _26302 = NOVALUE;
    int _26300 = NOVALUE;
    int _26299 = NOVALUE;
    int _26294 = NOVALUE;
    int _26293 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if INTERPRET and not BIND then*/
    if (_25INTERPRET_11871 == 0) {
        goto L1; // [5] 31
    }
    _26294 = (_25BIND_11877 == 0);
    if (_26294 == 0)
    {
        DeRef(_26294);
        _26294 = NOVALUE;
        goto L1; // [15] 31
    }
    else{
        DeRef(_26294);
        _26294 = NOVALUE;
    }

    /** 		prod_name = GetMsgText(270,0)*/
    RefDS(_22682);
    _0 = _prod_name_50013;
    _prod_name_50013 = _44GetMsgText(270, 0, _22682);
    DeRef(_0);
    goto L2; // [28] 70
L1: 

    /** 	elsif TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L3; // [35] 51
    }
    else{
    }

    /** 		prod_name = GetMsgText(271,0)*/
    RefDS(_22682);
    _0 = _prod_name_50013;
    _prod_name_50013 = _44GetMsgText(271, 0, _22682);
    DeRef(_0);
    goto L2; // [48] 70
L3: 

    /** 	elsif BIND then*/
    if (_25BIND_11877 == 0)
    {
        goto L4; // [55] 69
    }
    else{
    }

    /** 		prod_name = GetMsgText(272,0)*/
    RefDS(_22682);
    _0 = _prod_name_50013;
    _prod_name_50013 = _44GetMsgText(272, 0, _22682);
    DeRef(_0);
L4: 
L2: 

    /** 	ifdef EU_MANAGED_MEM then*/

    /** 		memory_type = GetMsgText(274,0)*/
    RefDS(_22682);
    _0 = _memory_type_50014;
    _memory_type_50014 = _44GetMsgText(274, 0, _22682);
    DeRef(_0);

    /** 	sequence misc_info = {*/

    /** 	ifdef WINDOWS then*/

    /** 		return "Windows"*/
    RefDS(_6625);
    DeRefi(_platform_name_inlined_platform_name_at_83_50034);
    _platform_name_inlined_platform_name_at_83_50034 = _6625;
    _26299 = _31version_date(0);
    _26300 = _31version_node(0);
    _0 = _misc_info_50032;
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_platform_name_inlined_platform_name_at_83_50034);
    *((int *)(_2+4)) = _platform_name_inlined_platform_name_at_83_50034;
    RefDS(_memory_type_50014);
    *((int *)(_2+8)) = _memory_type_50014;
    RefDS(_22682);
    *((int *)(_2+12)) = _22682;
    *((int *)(_2+16)) = _26299;
    *((int *)(_2+20)) = _26300;
    _misc_info_50032 = MAKE_SEQ(_1);
    DeRef(_0);
    _26300 = NOVALUE;
    _26299 = NOVALUE;

    /** 	if info:is_developmental then*/
    if (_31is_developmental_12432 == 0)
    {
        goto L5; // [114] 150
    }
    else{
    }

    /** 		misc_info[$] = sprintf("%d:%s", { info:version_revision(), info:version_node() })*/
    _26302 = 5;

    /** 	return version_info[REVISION]*/
    DeRef(_version_revision_inlined_version_revision_at_121_50042);
    _2 = (int)SEQ_PTR(_31version_info_12430);
    _version_revision_inlined_version_revision_at_121_50042 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_version_revision_inlined_version_revision_at_121_50042);
    _26304 = _31version_node(0);
    Ref(_version_revision_inlined_version_revision_at_121_50042);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _version_revision_inlined_version_revision_at_121_50042;
    ((int *)_2)[2] = _26304;
    _26305 = MAKE_SEQ(_1);
    _26304 = NOVALUE;
    _26306 = EPrintf(-9999999, _26303, _26305);
    DeRefDS(_26305);
    _26305 = NOVALUE;
    _2 = (int)SEQ_PTR(_misc_info_50032);
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _26306;
    if( _1 != _26306 ){
        DeRef(_1);
    }
    _26306 = NOVALUE;
L5: 

    /** 	object EuConsole = getenv("EUCONS")*/
    DeRefi(_EuConsole_50046);
    _EuConsole_50046 = EGetEnv(_26307);

    /** 	if equal(EuConsole, "1") then*/
    if (_EuConsole_50046 == _26309)
    _26310 = 1;
    else if (IS_ATOM_INT(_EuConsole_50046) && IS_ATOM_INT(_26309))
    _26310 = 0;
    else
    _26310 = (compare(_EuConsole_50046, _26309) == 0);
    if (_26310 == 0)
    {
        _26310 = NOVALUE;
        goto L6; // [161] 179
    }
    else{
        _26310 = NOVALUE;
    }

    /** 		misc_info[3] = GetMsgText(275,0)*/
    RefDS(_22682);
    _26311 = _44GetMsgText(275, 0, _22682);
    _2 = (int)SEQ_PTR(_misc_info_50032);
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _26311;
    if( _1 != _26311 ){
        DeRef(_1);
    }
    _26311 = NOVALUE;
    goto L7; // [176] 187
L6: 

    /** 		misc_info = remove(misc_info, 3)*/
    {
        s1_ptr assign_space = SEQ_PTR(_misc_info_50032);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(3)) ? 3 : (long)(DBL_PTR(3)->dbl);
        int stop = (IS_ATOM_INT(3)) ? 3 : (long)(DBL_PTR(3)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_misc_info_50032), start, &_misc_info_50032 );
            }
            else Tail(SEQ_PTR(_misc_info_50032), stop+1, &_misc_info_50032);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_misc_info_50032), start, &_misc_info_50032);
        }
        else {
            assign_slice_seq = &assign_space;
            _misc_info_50032 = Remove_elements(start, stop, (SEQ_PTR(_misc_info_50032)->ref == 1));
        }
    }
L7: 

    /** 	screen_output(STDERR, sprintf("%s v%s %s\n   %s, %s\n   Revision Date: %s, Id: %s\n", {*/

    /** 	return sprintf("%d.%d.%d", version_info[MAJ_VER..PAT_VER])*/
    rhs_slice_target = (object_ptr)&_version_string_short_1__tmp_at192_50061;
    RHS_Slice(_31version_info_12430, 1, 3);
    DeRefi(_version_string_short_inlined_version_string_short_at_192_50060);
    _version_string_short_inlined_version_string_short_at_192_50060 = EPrintf(-9999999, _6905, _version_string_short_1__tmp_at192_50061);
    DeRef(_version_string_short_1__tmp_at192_50061);
    _version_string_short_1__tmp_at192_50061 = NOVALUE;

    /** 	return version_info[VER_TYPE]*/
    DeRef(_version_type_inlined_version_type_at_212_50063);
    _2 = (int)SEQ_PTR(_31version_info_12430);
    _version_type_inlined_version_type_at_212_50063 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_version_type_inlined_version_type_at_212_50063);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_prod_name_50013);
    *((int *)(_2+4)) = _prod_name_50013;
    RefDS(_version_string_short_inlined_version_string_short_at_192_50060);
    *((int *)(_2+8)) = _version_string_short_inlined_version_string_short_at_192_50060;
    Ref(_version_type_inlined_version_type_at_212_50063);
    *((int *)(_2+12)) = _version_type_inlined_version_type_at_212_50063;
    _26314 = MAKE_SEQ(_1);
    Concat((object_ptr)&_26315, _26314, _misc_info_50032);
    DeRefDS(_26314);
    _26314 = NOVALUE;
    DeRef(_26314);
    _26314 = NOVALUE;
    _26316 = EPrintf(-9999999, _26313, _26315);
    DeRefDS(_26315);
    _26315 = NOVALUE;
    _43screen_output(2, _26316);
    _26316 = NOVALUE;

    /** end procedure*/
    DeRefDS(_prod_name_50013);
    DeRef(_memory_type_50014);
    DeRefDS(_misc_info_50032);
    DeRefi(_EuConsole_50046);
    return;
    ;
}


int _39find_opt(int _name_type_50075, int _opt_50076, int _opts_50077)
{
    int _o_50081 = NOVALUE;
    int _has_case_50083 = NOVALUE;
    int _26329 = NOVALUE;
    int _26328 = NOVALUE;
    int _26327 = NOVALUE;
    int _26326 = NOVALUE;
    int _26325 = NOVALUE;
    int _26324 = NOVALUE;
    int _26323 = NOVALUE;
    int _26322 = NOVALUE;
    int _26321 = NOVALUE;
    int _26319 = NOVALUE;
    int _26317 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(opts) do*/
    if (IS_SEQUENCE(_opts_50077)){
            _26317 = SEQ_PTR(_opts_50077)->length;
    }
    else {
        _26317 = 1;
    }
    {
        int _i_50079;
        _i_50079 = 1;
L1: 
        if (_i_50079 > _26317){
            goto L2; // [12] 115
        }

        /** 		sequence o = opts[i]		*/
        DeRef(_o_50081);
        _2 = (int)SEQ_PTR(_opts_50077);
        _o_50081 = (int)*(((s1_ptr)_2)->base + _i_50079);
        Ref(_o_50081);

        /** 		integer has_case = find(HAS_CASE, o[OPTIONS])*/
        _2 = (int)SEQ_PTR(_o_50081);
        _26319 = (int)*(((s1_ptr)_2)->base + 4);
        _has_case_50083 = find_from(99, _26319, 1);
        _26319 = NOVALUE;

        /** 		if has_case and equal(o[name_type], opt) then*/
        if (_has_case_50083 == 0) {
            goto L3; // [44] 69
        }
        _2 = (int)SEQ_PTR(_o_50081);
        _26322 = (int)*(((s1_ptr)_2)->base + _name_type_50075);
        if (_26322 == _opt_50076)
        _26323 = 1;
        else if (IS_ATOM_INT(_26322) && IS_ATOM_INT(_opt_50076))
        _26323 = 0;
        else
        _26323 = (compare(_26322, _opt_50076) == 0);
        _26322 = NOVALUE;
        if (_26323 == 0)
        {
            _26323 = NOVALUE;
            goto L3; // [57] 69
        }
        else{
            _26323 = NOVALUE;
        }

        /** 			return o*/
        DeRefDS(_opt_50076);
        DeRefDS(_opts_50077);
        return _o_50081;
        goto L4; // [66] 106
L3: 

        /** 		elsif not has_case and equal(text:lower(o[name_type]), text:lower(opt)) then*/
        _26324 = (_has_case_50083 == 0);
        if (_26324 == 0) {
            goto L5; // [74] 105
        }
        _2 = (int)SEQ_PTR(_o_50081);
        _26326 = (int)*(((s1_ptr)_2)->base + _name_type_50075);
        Ref(_26326);
        _26327 = _4lower(_26326);
        _26326 = NOVALUE;
        RefDS(_opt_50076);
        _26328 = _4lower(_opt_50076);
        if (_26327 == _26328)
        _26329 = 1;
        else if (IS_ATOM_INT(_26327) && IS_ATOM_INT(_26328))
        _26329 = 0;
        else
        _26329 = (compare(_26327, _26328) == 0);
        DeRef(_26327);
        _26327 = NOVALUE;
        DeRef(_26328);
        _26328 = NOVALUE;
        if (_26329 == 0)
        {
            _26329 = NOVALUE;
            goto L5; // [95] 105
        }
        else{
            _26329 = NOVALUE;
        }

        /** 			return o*/
        DeRefDS(_opt_50076);
        DeRefDS(_opts_50077);
        DeRef(_26324);
        _26324 = NOVALUE;
        return _o_50081;
L5: 
L4: 
        DeRef(_o_50081);
        _o_50081 = NOVALUE;

        /** 	end for*/
        _i_50079 = _i_50079 + 1;
        goto L1; // [110] 19
L2: 
        ;
    }

    /** 	return {}*/
    RefDS(_22682);
    DeRefDS(_opt_50076);
    DeRefDS(_opts_50077);
    DeRef(_26324);
    _26324 = NOVALUE;
    return _22682;
    ;
}


int _39merge_parameters(int _a_50100, int _b_50101, int _opts_50102, int _dedupe_50103)
{
    int _i_50104 = NOVALUE;
    int _opt_50108 = NOVALUE;
    int _this_opt_50114 = NOVALUE;
    int _bi_50115 = NOVALUE;
    int _beginLen_50175 = NOVALUE;
    int _first_extra_50197 = NOVALUE;
    int _opt_50201 = NOVALUE;
    int _this_opt_50206 = NOVALUE;
    int _26423 = NOVALUE;
    int _26422 = NOVALUE;
    int _26419 = NOVALUE;
    int _26418 = NOVALUE;
    int _26417 = NOVALUE;
    int _26415 = NOVALUE;
    int _26414 = NOVALUE;
    int _26413 = NOVALUE;
    int _26412 = NOVALUE;
    int _26410 = NOVALUE;
    int _26409 = NOVALUE;
    int _26407 = NOVALUE;
    int _26406 = NOVALUE;
    int _26405 = NOVALUE;
    int _26404 = NOVALUE;
    int _26403 = NOVALUE;
    int _26402 = NOVALUE;
    int _26401 = NOVALUE;
    int _26399 = NOVALUE;
    int _26396 = NOVALUE;
    int _26395 = NOVALUE;
    int _26390 = NOVALUE;
    int _26388 = NOVALUE;
    int _26387 = NOVALUE;
    int _26386 = NOVALUE;
    int _26385 = NOVALUE;
    int _26384 = NOVALUE;
    int _26383 = NOVALUE;
    int _26382 = NOVALUE;
    int _26381 = NOVALUE;
    int _26377 = NOVALUE;
    int _26376 = NOVALUE;
    int _26375 = NOVALUE;
    int _26374 = NOVALUE;
    int _26373 = NOVALUE;
    int _26372 = NOVALUE;
    int _26371 = NOVALUE;
    int _26370 = NOVALUE;
    int _26369 = NOVALUE;
    int _26368 = NOVALUE;
    int _26367 = NOVALUE;
    int _26366 = NOVALUE;
    int _26365 = NOVALUE;
    int _26364 = NOVALUE;
    int _26363 = NOVALUE;
    int _26361 = NOVALUE;
    int _26360 = NOVALUE;
    int _26359 = NOVALUE;
    int _26358 = NOVALUE;
    int _26357 = NOVALUE;
    int _26356 = NOVALUE;
    int _26355 = NOVALUE;
    int _26354 = NOVALUE;
    int _26352 = NOVALUE;
    int _26351 = NOVALUE;
    int _26350 = NOVALUE;
    int _26349 = NOVALUE;
    int _26347 = NOVALUE;
    int _26346 = NOVALUE;
    int _26345 = NOVALUE;
    int _26344 = NOVALUE;
    int _26343 = NOVALUE;
    int _26342 = NOVALUE;
    int _26341 = NOVALUE;
    int _26339 = NOVALUE;
    int _26338 = NOVALUE;
    int _26336 = NOVALUE;
    int _26333 = NOVALUE;
    int _26330 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_dedupe_50103)) {
        _1 = (long)(DBL_PTR(_dedupe_50103)->dbl);
        if (UNIQUE(DBL_PTR(_dedupe_50103)) && (DBL_PTR(_dedupe_50103)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_dedupe_50103);
        _dedupe_50103 = _1;
    }

    /** 	integer i = 1*/
    _i_50104 = 1;

    /** 	while i <= length(a) do*/
L1: 
    if (IS_SEQUENCE(_a_50100)){
            _26330 = SEQ_PTR(_a_50100)->length;
    }
    else {
        _26330 = 1;
    }
    if (_i_50104 > _26330)
    goto L2; // [22] 473

    /** 		sequence opt = a[i]*/
    DeRef(_opt_50108);
    _2 = (int)SEQ_PTR(_a_50100);
    _opt_50108 = (int)*(((s1_ptr)_2)->base + _i_50104);
    Ref(_opt_50108);

    /** 		if length(opt) < 2 then*/
    if (IS_SEQUENCE(_opt_50108)){
            _26333 = SEQ_PTR(_opt_50108)->length;
    }
    else {
        _26333 = 1;
    }
    if (_26333 >= 2)
    goto L3; // [39] 56

    /** 			i += 1*/
    _i_50104 = _i_50104 + 1;

    /** 			continue*/
    DeRefDS(_opt_50108);
    _opt_50108 = NOVALUE;
    DeRef(_this_opt_50114);
    _this_opt_50114 = NOVALUE;
    goto L1; // [53] 19
L3: 

    /** 		sequence this_opt = {}*/
    RefDS(_22682);
    DeRef(_this_opt_50114);
    _this_opt_50114 = _22682;

    /** 		integer bi = 0*/
    _bi_50115 = 0;

    /** 		if opt[2] = '-' then*/
    _2 = (int)SEQ_PTR(_opt_50108);
    _26336 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _26336, 45)){
        _26336 = NOVALUE;
        goto L4; // [74] 151
    }
    _26336 = NOVALUE;

    /** 			this_opt = find_opt(LONGNAME, opt[3..$], opts)*/
    if (IS_SEQUENCE(_opt_50108)){
            _26338 = SEQ_PTR(_opt_50108)->length;
    }
    else {
        _26338 = 1;
    }
    rhs_slice_target = (object_ptr)&_26339;
    RHS_Slice(_opt_50108, 3, _26338);
    RefDS(_opts_50102);
    _0 = _this_opt_50114;
    _this_opt_50114 = _39find_opt(2, _26339, _opts_50102);
    DeRefDS(_0);
    _26339 = NOVALUE;

    /** 			for j = 1 to length(b) do*/
    if (IS_SEQUENCE(_b_50101)){
            _26341 = SEQ_PTR(_b_50101)->length;
    }
    else {
        _26341 = 1;
    }
    {
        int _j_50123;
        _j_50123 = 1;
L5: 
        if (_j_50123 > _26341){
            goto L6; // [103] 148
        }

        /** 				if equal(text:lower(b[j]), text:lower(opt)) then*/
        _2 = (int)SEQ_PTR(_b_50101);
        _26342 = (int)*(((s1_ptr)_2)->base + _j_50123);
        Ref(_26342);
        _26343 = _4lower(_26342);
        _26342 = NOVALUE;
        RefDS(_opt_50108);
        _26344 = _4lower(_opt_50108);
        if (_26343 == _26344)
        _26345 = 1;
        else if (IS_ATOM_INT(_26343) && IS_ATOM_INT(_26344))
        _26345 = 0;
        else
        _26345 = (compare(_26343, _26344) == 0);
        DeRef(_26343);
        _26343 = NOVALUE;
        DeRef(_26344);
        _26344 = NOVALUE;
        if (_26345 == 0)
        {
            _26345 = NOVALUE;
            goto L7; // [128] 141
        }
        else{
            _26345 = NOVALUE;
        }

        /** 					bi = j*/
        _bi_50115 = _j_50123;

        /** 					exit*/
        goto L6; // [138] 148
L7: 

        /** 			end for*/
        _j_50123 = _j_50123 + 1;
        goto L5; // [143] 110
L6: 
        ;
    }
    goto L8; // [148] 296
L4: 

    /** 		elsif opt[1] = '-' or opt[1] = '/' then*/
    _2 = (int)SEQ_PTR(_opt_50108);
    _26346 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_26346)) {
        _26347 = (_26346 == 45);
    }
    else {
        _26347 = binary_op(EQUALS, _26346, 45);
    }
    _26346 = NOVALUE;
    if (IS_ATOM_INT(_26347)) {
        if (_26347 != 0) {
            goto L9; // [161] 178
        }
    }
    else {
        if (DBL_PTR(_26347)->dbl != 0.0) {
            goto L9; // [161] 178
        }
    }
    _2 = (int)SEQ_PTR(_opt_50108);
    _26349 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_26349)) {
        _26350 = (_26349 == 47);
    }
    else {
        _26350 = binary_op(EQUALS, _26349, 47);
    }
    _26349 = NOVALUE;
    if (_26350 == 0) {
        DeRef(_26350);
        _26350 = NOVALUE;
        goto LA; // [174] 295
    }
    else {
        if (!IS_ATOM_INT(_26350) && DBL_PTR(_26350)->dbl == 0.0){
            DeRef(_26350);
            _26350 = NOVALUE;
            goto LA; // [174] 295
        }
        DeRef(_26350);
        _26350 = NOVALUE;
    }
    DeRef(_26350);
    _26350 = NOVALUE;
L9: 

    /** 			this_opt = find_opt(SHORTNAME, opt[2..$], opts)*/
    if (IS_SEQUENCE(_opt_50108)){
            _26351 = SEQ_PTR(_opt_50108)->length;
    }
    else {
        _26351 = 1;
    }
    rhs_slice_target = (object_ptr)&_26352;
    RHS_Slice(_opt_50108, 2, _26351);
    RefDS(_opts_50102);
    _0 = _this_opt_50114;
    _this_opt_50114 = _39find_opt(1, _26352, _opts_50102);
    DeRef(_0);
    _26352 = NOVALUE;

    /** 			for j = 1 to length(b) do*/
    if (IS_SEQUENCE(_b_50101)){
            _26354 = SEQ_PTR(_b_50101)->length;
    }
    else {
        _26354 = 1;
    }
    {
        int _j_50140;
        _j_50140 = 1;
LB: 
        if (_j_50140 > _26354){
            goto LC; // [203] 294
        }

        /** 				if equal(text:lower(b[j]), '-' & text:lower(opt[2..$])) or */
        _2 = (int)SEQ_PTR(_b_50101);
        _26355 = (int)*(((s1_ptr)_2)->base + _j_50140);
        Ref(_26355);
        _26356 = _4lower(_26355);
        _26355 = NOVALUE;
        if (IS_SEQUENCE(_opt_50108)){
                _26357 = SEQ_PTR(_opt_50108)->length;
        }
        else {
            _26357 = 1;
        }
        rhs_slice_target = (object_ptr)&_26358;
        RHS_Slice(_opt_50108, 2, _26357);
        _26359 = _4lower(_26358);
        _26358 = NOVALUE;
        if (IS_SEQUENCE(45) && IS_ATOM(_26359)) {
        }
        else if (IS_ATOM(45) && IS_SEQUENCE(_26359)) {
            Prepend(&_26360, _26359, 45);
        }
        else {
            Concat((object_ptr)&_26360, 45, _26359);
        }
        DeRef(_26359);
        _26359 = NOVALUE;
        if (_26356 == _26360)
        _26361 = 1;
        else if (IS_ATOM_INT(_26356) && IS_ATOM_INT(_26360))
        _26361 = 0;
        else
        _26361 = (compare(_26356, _26360) == 0);
        DeRef(_26356);
        _26356 = NOVALUE;
        DeRefDS(_26360);
        _26360 = NOVALUE;
        if (_26361 != 0) {
            goto LD; // [240] 277
        }
        _2 = (int)SEQ_PTR(_b_50101);
        _26363 = (int)*(((s1_ptr)_2)->base + _j_50140);
        Ref(_26363);
        _26364 = _4lower(_26363);
        _26363 = NOVALUE;
        if (IS_SEQUENCE(_opt_50108)){
                _26365 = SEQ_PTR(_opt_50108)->length;
        }
        else {
            _26365 = 1;
        }
        rhs_slice_target = (object_ptr)&_26366;
        RHS_Slice(_opt_50108, 2, _26365);
        _26367 = _4lower(_26366);
        _26366 = NOVALUE;
        if (IS_SEQUENCE(47) && IS_ATOM(_26367)) {
        }
        else if (IS_ATOM(47) && IS_SEQUENCE(_26367)) {
            Prepend(&_26368, _26367, 47);
        }
        else {
            Concat((object_ptr)&_26368, 47, _26367);
        }
        DeRef(_26367);
        _26367 = NOVALUE;
        if (_26364 == _26368)
        _26369 = 1;
        else if (IS_ATOM_INT(_26364) && IS_ATOM_INT(_26368))
        _26369 = 0;
        else
        _26369 = (compare(_26364, _26368) == 0);
        DeRef(_26364);
        _26364 = NOVALUE;
        DeRefDS(_26368);
        _26368 = NOVALUE;
        if (_26369 == 0)
        {
            _26369 = NOVALUE;
            goto LE; // [273] 287
        }
        else{
            _26369 = NOVALUE;
        }
LD: 

        /** 					bi = j*/
        _bi_50115 = _j_50140;

        /** 					exit*/
        goto LC; // [284] 294
LE: 

        /** 			end for*/
        _j_50140 = _j_50140 + 1;
        goto LB; // [289] 210
LC: 
        ;
    }
LA: 
L8: 

    /** 		if length(this_opt) and not find(MULTIPLE, this_opt[OPTIONS]) then*/
    if (IS_SEQUENCE(_this_opt_50114)){
            _26370 = SEQ_PTR(_this_opt_50114)->length;
    }
    else {
        _26370 = 1;
    }
    if (_26370 == 0) {
        goto LF; // [301] 459
    }
    _2 = (int)SEQ_PTR(_this_opt_50114);
    _26372 = (int)*(((s1_ptr)_2)->base + 4);
    _26373 = find_from(42, _26372, 1);
    _26372 = NOVALUE;
    _26374 = (_26373 == 0);
    _26373 = NOVALUE;
    if (_26374 == 0)
    {
        DeRef(_26374);
        _26374 = NOVALUE;
        goto LF; // [322] 459
    }
    else{
        DeRef(_26374);
        _26374 = NOVALUE;
    }

    /** 			if bi then*/
    if (_bi_50115 == 0)
    {
        goto L10; // [327] 373
    }
    else{
    }

    /** 				if find(HAS_PARAMETER, this_opt[OPTIONS]) then*/
    _2 = (int)SEQ_PTR(_this_opt_50114);
    _26375 = (int)*(((s1_ptr)_2)->base + 4);
    _26376 = find_from(112, _26375, 1);
    _26375 = NOVALUE;
    if (_26376 == 0)
    {
        _26376 = NOVALUE;
        goto L11; // [345] 362
    }
    else{
        _26376 = NOVALUE;
    }

    /** 					a = remove(a, i, i + 1)*/
    _26377 = _i_50104 + 1;
    if (_26377 > MAXINT){
        _26377 = NewDouble((double)_26377);
    }
    {
        s1_ptr assign_space = SEQ_PTR(_a_50100);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_i_50104)) ? _i_50104 : (long)(DBL_PTR(_i_50104)->dbl);
        int stop = (IS_ATOM_INT(_26377)) ? _26377 : (long)(DBL_PTR(_26377)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_a_50100), start, &_a_50100 );
            }
            else Tail(SEQ_PTR(_a_50100), stop+1, &_a_50100);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_a_50100), start, &_a_50100);
        }
        else {
            assign_slice_seq = &assign_space;
            _a_50100 = Remove_elements(start, stop, (SEQ_PTR(_a_50100)->ref == 1));
        }
    }
    DeRef(_26377);
    _26377 = NOVALUE;
    goto L12; // [359] 466
L11: 

    /** 					a = remove(a, i)*/
    {
        s1_ptr assign_space = SEQ_PTR(_a_50100);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_i_50104)) ? _i_50104 : (long)(DBL_PTR(_i_50104)->dbl);
        int stop = (IS_ATOM_INT(_i_50104)) ? _i_50104 : (long)(DBL_PTR(_i_50104)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_a_50100), start, &_a_50100 );
            }
            else Tail(SEQ_PTR(_a_50100), stop+1, &_a_50100);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_a_50100), start, &_a_50100);
        }
        else {
            assign_slice_seq = &assign_space;
            _a_50100 = Remove_elements(start, stop, (SEQ_PTR(_a_50100)->ref == 1));
        }
    }
    goto L12; // [370] 466
L10: 

    /** 				integer beginLen = length(a)*/
    if (IS_SEQUENCE(_a_50100)){
            _beginLen_50175 = SEQ_PTR(_a_50100)->length;
    }
    else {
        _beginLen_50175 = 1;
    }

    /** 				if dedupe = 0 and i < beginLen then*/
    _26381 = (_dedupe_50103 == 0);
    if (_26381 == 0) {
        goto L13; // [384] 446
    }
    _26383 = (_i_50104 < _beginLen_50175);
    if (_26383 == 0)
    {
        DeRef(_26383);
        _26383 = NOVALUE;
        goto L13; // [393] 446
    }
    else{
        DeRef(_26383);
        _26383 = NOVALUE;
    }

    /** 					a = merge_parameters( a[i + 1..$], a[1..i], opts, 1)*/
    _26384 = _i_50104 + 1;
    if (_26384 > MAXINT){
        _26384 = NewDouble((double)_26384);
    }
    if (IS_SEQUENCE(_a_50100)){
            _26385 = SEQ_PTR(_a_50100)->length;
    }
    else {
        _26385 = 1;
    }
    rhs_slice_target = (object_ptr)&_26386;
    RHS_Slice(_a_50100, _26384, _26385);
    rhs_slice_target = (object_ptr)&_26387;
    RHS_Slice(_a_50100, 1, _i_50104);
    RefDS(_opts_50102);
    DeRef(_26388);
    _26388 = _opts_50102;
    _0 = _a_50100;
    _a_50100 = _39merge_parameters(_26386, _26387, _26388, 1);
    DeRefDS(_0);
    _26386 = NOVALUE;
    _26387 = NOVALUE;
    _26388 = NOVALUE;

    /** 					if beginLen = length(a) then*/
    if (IS_SEQUENCE(_a_50100)){
            _26390 = SEQ_PTR(_a_50100)->length;
    }
    else {
        _26390 = 1;
    }
    if (_beginLen_50175 != _26390)
    goto L14; // [432] 453

    /** 						i += 1*/
    _i_50104 = _i_50104 + 1;
    goto L14; // [443] 453
L13: 

    /** 					i += 1*/
    _i_50104 = _i_50104 + 1;
L14: 
    goto L12; // [456] 466
LF: 

    /** 			i += 1*/
    _i_50104 = _i_50104 + 1;
L12: 
    DeRef(_opt_50108);
    _opt_50108 = NOVALUE;
    DeRef(_this_opt_50114);
    _this_opt_50114 = NOVALUE;

    /** 	end while*/
    goto L1; // [470] 19
L2: 

    /** 	if dedupe then*/
    if (_dedupe_50103 == 0)
    {
        goto L15; // [475] 489
    }
    else{
    }

    /** 		return b & a*/
    Concat((object_ptr)&_26395, _b_50101, _a_50100);
    DeRefDS(_a_50100);
    DeRefDS(_b_50101);
    DeRefDS(_opts_50102);
    DeRef(_26381);
    _26381 = NOVALUE;
    DeRef(_26347);
    _26347 = NOVALUE;
    DeRef(_26384);
    _26384 = NOVALUE;
    return _26395;
L15: 

    /** 	integer first_extra = 0*/
    _first_extra_50197 = 0;

    /** 	i = 1*/
    _i_50104 = 1;

    /** 	while i <= length(b) do*/
L16: 
    if (IS_SEQUENCE(_b_50101)){
            _26396 = SEQ_PTR(_b_50101)->length;
    }
    else {
        _26396 = 1;
    }
    if (_i_50104 > _26396)
    goto L17; // [507] 706

    /** 		sequence opt = b[i]*/
    DeRef(_opt_50201);
    _2 = (int)SEQ_PTR(_b_50101);
    _opt_50201 = (int)*(((s1_ptr)_2)->base + _i_50104);
    Ref(_opt_50201);

    /** 		if length(opt) <= 1 then*/
    if (IS_SEQUENCE(_opt_50201)){
            _26399 = SEQ_PTR(_opt_50201)->length;
    }
    else {
        _26399 = 1;
    }
    if (_26399 > 1)
    goto L18; // [524] 540

    /** 			first_extra = i*/
    _first_extra_50197 = _i_50104;

    /** 			exit*/
    DeRefDS(_opt_50201);
    _opt_50201 = NOVALUE;
    DeRef(_this_opt_50206);
    _this_opt_50206 = NOVALUE;
    goto L17; // [537] 706
L18: 

    /** 		sequence this_opt = {}*/
    RefDS(_22682);
    DeRef(_this_opt_50206);
    _this_opt_50206 = _22682;

    /** 		if opt[2] = '-' and opt[1] = '-' then*/
    _2 = (int)SEQ_PTR(_opt_50201);
    _26401 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_26401)) {
        _26402 = (_26401 == 45);
    }
    else {
        _26402 = binary_op(EQUALS, _26401, 45);
    }
    _26401 = NOVALUE;
    if (IS_ATOM_INT(_26402)) {
        if (_26402 == 0) {
            goto L19; // [557] 596
        }
    }
    else {
        if (DBL_PTR(_26402)->dbl == 0.0) {
            goto L19; // [557] 596
        }
    }
    _2 = (int)SEQ_PTR(_opt_50201);
    _26404 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_26404)) {
        _26405 = (_26404 == 45);
    }
    else {
        _26405 = binary_op(EQUALS, _26404, 45);
    }
    _26404 = NOVALUE;
    if (_26405 == 0) {
        DeRef(_26405);
        _26405 = NOVALUE;
        goto L19; // [570] 596
    }
    else {
        if (!IS_ATOM_INT(_26405) && DBL_PTR(_26405)->dbl == 0.0){
            DeRef(_26405);
            _26405 = NOVALUE;
            goto L19; // [570] 596
        }
        DeRef(_26405);
        _26405 = NOVALUE;
    }
    DeRef(_26405);
    _26405 = NOVALUE;

    /** 			this_opt = find_opt(LONGNAME, opt[3..$], opts)*/
    if (IS_SEQUENCE(_opt_50201)){
            _26406 = SEQ_PTR(_opt_50201)->length;
    }
    else {
        _26406 = 1;
    }
    rhs_slice_target = (object_ptr)&_26407;
    RHS_Slice(_opt_50201, 3, _26406);
    RefDS(_opts_50102);
    _0 = _this_opt_50206;
    _this_opt_50206 = _39find_opt(2, _26407, _opts_50102);
    DeRef(_0);
    _26407 = NOVALUE;
    goto L1A; // [593] 645
L19: 

    /** 		elsif opt[1] = '-' or opt[1] = '/' then*/
    _2 = (int)SEQ_PTR(_opt_50201);
    _26409 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_26409)) {
        _26410 = (_26409 == 45);
    }
    else {
        _26410 = binary_op(EQUALS, _26409, 45);
    }
    _26409 = NOVALUE;
    if (IS_ATOM_INT(_26410)) {
        if (_26410 != 0) {
            goto L1B; // [606] 623
        }
    }
    else {
        if (DBL_PTR(_26410)->dbl != 0.0) {
            goto L1B; // [606] 623
        }
    }
    _2 = (int)SEQ_PTR(_opt_50201);
    _26412 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_26412)) {
        _26413 = (_26412 == 47);
    }
    else {
        _26413 = binary_op(EQUALS, _26412, 47);
    }
    _26412 = NOVALUE;
    if (_26413 == 0) {
        DeRef(_26413);
        _26413 = NOVALUE;
        goto L1C; // [619] 644
    }
    else {
        if (!IS_ATOM_INT(_26413) && DBL_PTR(_26413)->dbl == 0.0){
            DeRef(_26413);
            _26413 = NOVALUE;
            goto L1C; // [619] 644
        }
        DeRef(_26413);
        _26413 = NOVALUE;
    }
    DeRef(_26413);
    _26413 = NOVALUE;
L1B: 

    /** 			this_opt = find_opt(SHORTNAME, opt[2..$], opts)*/
    if (IS_SEQUENCE(_opt_50201)){
            _26414 = SEQ_PTR(_opt_50201)->length;
    }
    else {
        _26414 = 1;
    }
    rhs_slice_target = (object_ptr)&_26415;
    RHS_Slice(_opt_50201, 2, _26414);
    RefDS(_opts_50102);
    _0 = _this_opt_50206;
    _this_opt_50206 = _39find_opt(1, _26415, _opts_50102);
    DeRef(_0);
    _26415 = NOVALUE;
L1C: 
L1A: 

    /** 		if length(this_opt) then*/
    if (IS_SEQUENCE(_this_opt_50206)){
            _26417 = SEQ_PTR(_this_opt_50206)->length;
    }
    else {
        _26417 = 1;
    }
    if (_26417 == 0)
    {
        _26417 = NOVALUE;
        goto L1D; // [650] 681
    }
    else{
        _26417 = NOVALUE;
    }

    /** 			if find(HAS_PARAMETER, this_opt[OPTIONS]) then*/
    _2 = (int)SEQ_PTR(_this_opt_50206);
    _26418 = (int)*(((s1_ptr)_2)->base + 4);
    _26419 = find_from(112, _26418, 1);
    _26418 = NOVALUE;
    if (_26419 == 0)
    {
        _26419 = NOVALUE;
        goto L1E; // [668] 693
    }
    else{
        _26419 = NOVALUE;
    }

    /** 				i += 1*/
    _i_50104 = _i_50104 + 1;
    goto L1E; // [678] 693
L1D: 

    /** 			first_extra = i*/
    _first_extra_50197 = _i_50104;

    /** 			exit*/
    DeRef(_opt_50201);
    _opt_50201 = NOVALUE;
    DeRef(_this_opt_50206);
    _this_opt_50206 = NOVALUE;
    goto L17; // [690] 706
L1E: 

    /** 		i += 1*/
    _i_50104 = _i_50104 + 1;
    DeRef(_opt_50201);
    _opt_50201 = NOVALUE;
    DeRef(_this_opt_50206);
    _this_opt_50206 = NOVALUE;

    /** 	end while*/
    goto L16; // [703] 504
L17: 

    /** 	if first_extra then*/
    if (_first_extra_50197 == 0)
    {
        goto L1F; // [708] 723
    }
    else{
    }

    /** 		return splice(b, a, first_extra)*/
    {
        s1_ptr assign_space;
        insert_pos = _first_extra_50197;
        if (insert_pos <= 0) {
            Concat(&_26422,_a_50100,_b_50101);
        }
        else if (insert_pos > SEQ_PTR(_b_50101)->length){
            Concat(&_26422,_b_50101,_a_50100);
        }
        else if (IS_SEQUENCE(_a_50100)) {
            if( _26422 != _b_50101 || SEQ_PTR( _b_50101 )->ref != 1 ){
                DeRef( _26422 );
                RefDS( _b_50101 );
            }
            assign_space = Add_internal_space( _b_50101, insert_pos,((s1_ptr)SEQ_PTR(_a_50100))->length);
            assign_slice_seq = &assign_space;
            assign_space = Copy_elements( insert_pos, SEQ_PTR(_a_50100), _b_50101 == _26422 );
            _26422 = MAKE_SEQ( assign_space );
        }
        else {
            if( _26422 == _b_50101 && SEQ_PTR( _b_50101 )->ref == 1 ){
                _26422 = Insert( _b_50101, _a_50100, insert_pos);
            }
            else {
                DeRef( _26422 );
                RefDS( _b_50101 );
                _26422 = Insert( _b_50101, _a_50100, insert_pos);
            }
        }
    }
    DeRefDS(_a_50100);
    DeRefDS(_b_50101);
    DeRefDS(_opts_50102);
    DeRef(_26381);
    _26381 = NOVALUE;
    DeRef(_26347);
    _26347 = NOVALUE;
    DeRef(_26384);
    _26384 = NOVALUE;
    DeRef(_26395);
    _26395 = NOVALUE;
    DeRef(_26402);
    _26402 = NOVALUE;
    DeRef(_26410);
    _26410 = NOVALUE;
    return _26422;
L1F: 

    /** 	return b & a*/
    Concat((object_ptr)&_26423, _b_50101, _a_50100);
    DeRefDS(_a_50100);
    DeRefDS(_b_50101);
    DeRefDS(_opts_50102);
    DeRef(_26381);
    _26381 = NOVALUE;
    DeRef(_26347);
    _26347 = NOVALUE;
    DeRef(_26384);
    _26384 = NOVALUE;
    DeRef(_26395);
    _26395 = NOVALUE;
    DeRef(_26422);
    _26422 = NOVALUE;
    DeRef(_26402);
    _26402 = NOVALUE;
    DeRef(_26410);
    _26410 = NOVALUE;
    return _26423;
    ;
}


int _39validate_opt(int _opt_type_50239, int _arg_50240, int _args_50241, int _ix_50242)
{
    int _opt_50243 = NOVALUE;
    int _this_opt_50251 = NOVALUE;
    int _26442 = NOVALUE;
    int _26441 = NOVALUE;
    int _26440 = NOVALUE;
    int _26439 = NOVALUE;
    int _26438 = NOVALUE;
    int _26436 = NOVALUE;
    int _26435 = NOVALUE;
    int _26434 = NOVALUE;
    int _26433 = NOVALUE;
    int _26432 = NOVALUE;
    int _26430 = NOVALUE;
    int _26427 = NOVALUE;
    int _26425 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if opt_type = SHORTNAME then*/
    if (_opt_type_50239 != 1)
    goto L1; // [13] 30

    /** 		opt = arg[2..$]*/
    if (IS_SEQUENCE(_arg_50240)){
            _26425 = SEQ_PTR(_arg_50240)->length;
    }
    else {
        _26425 = 1;
    }
    rhs_slice_target = (object_ptr)&_opt_50243;
    RHS_Slice(_arg_50240, 2, _26425);
    goto L2; // [27] 41
L1: 

    /** 		opt = arg[3..$]*/
    if (IS_SEQUENCE(_arg_50240)){
            _26427 = SEQ_PTR(_arg_50240)->length;
    }
    else {
        _26427 = 1;
    }
    rhs_slice_target = (object_ptr)&_opt_50243;
    RHS_Slice(_arg_50240, 3, _26427);
L2: 

    /** 	sequence this_opt = find_opt( opt_type, opt, options )*/
    RefDS(_opt_50243);
    RefDS(_39options_49982);
    _0 = _this_opt_50251;
    _this_opt_50251 = _39find_opt(_opt_type_50239, _opt_50243, _39options_49982);
    DeRef(_0);

    /** 	if not length( this_opt ) then*/
    if (IS_SEQUENCE(_this_opt_50251)){
            _26430 = SEQ_PTR(_this_opt_50251)->length;
    }
    else {
        _26430 = 1;
    }
    if (_26430 != 0)
    goto L3; // [60] 74
    _26430 = NOVALUE;

    /** 		return { 0, 0 }*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 0;
    _26432 = MAKE_SEQ(_1);
    DeRefDS(_arg_50240);
    DeRefDS(_args_50241);
    DeRefDS(_opt_50243);
    DeRefDS(_this_opt_50251);
    return _26432;
L3: 

    /** 	if find( HAS_PARAMETER, this_opt[OPTIONS] ) then*/
    _2 = (int)SEQ_PTR(_this_opt_50251);
    _26433 = (int)*(((s1_ptr)_2)->base + 4);
    _26434 = find_from(112, _26433, 1);
    _26433 = NOVALUE;
    if (_26434 == 0)
    {
        _26434 = NOVALUE;
        goto L4; // [89] 139
    }
    else{
        _26434 = NOVALUE;
    }

    /** 		if ix = length( args ) - 1 then*/
    if (IS_SEQUENCE(_args_50241)){
            _26435 = SEQ_PTR(_args_50241)->length;
    }
    else {
        _26435 = 1;
    }
    _26436 = _26435 - 1;
    _26435 = NOVALUE;
    if (_ix_50242 != _26436)
    goto L5; // [101] 121

    /** 			CompileErr( MISSING_CMD_PARAMETER, { arg } )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_arg_50240);
    *((int *)(_2+4)) = _arg_50240;
    _26438 = MAKE_SEQ(_1);
    _43CompileErr(353, _26438, 0);
    _26438 = NOVALUE;
    goto L6; // [118] 154
L5: 

    /** 			return { ix, ix + 2 }*/
    _26439 = _ix_50242 + 2;
    if ((long)((unsigned long)_26439 + (unsigned long)HIGH_BITS) >= 0) 
    _26439 = NewDouble((double)_26439);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _ix_50242;
    ((int *)_2)[2] = _26439;
    _26440 = MAKE_SEQ(_1);
    _26439 = NOVALUE;
    DeRefDS(_arg_50240);
    DeRefDS(_args_50241);
    DeRef(_opt_50243);
    DeRef(_this_opt_50251);
    DeRef(_26432);
    _26432 = NOVALUE;
    DeRef(_26436);
    _26436 = NOVALUE;
    return _26440;
    goto L6; // [136] 154
L4: 

    /** 		return { ix, ix + 1 }*/
    _26441 = _ix_50242 + 1;
    if (_26441 > MAXINT){
        _26441 = NewDouble((double)_26441);
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _ix_50242;
    ((int *)_2)[2] = _26441;
    _26442 = MAKE_SEQ(_1);
    _26441 = NOVALUE;
    DeRefDS(_arg_50240);
    DeRefDS(_args_50241);
    DeRef(_opt_50243);
    DeRef(_this_opt_50251);
    DeRef(_26432);
    _26432 = NOVALUE;
    DeRef(_26436);
    _26436 = NOVALUE;
    DeRef(_26440);
    _26440 = NOVALUE;
    return _26442;
L6: 
    ;
}


int _39find_next_opt(int _ix_50276, int _args_50277)
{
    int _arg_50281 = NOVALUE;
    int _26464 = NOVALUE;
    int _26463 = NOVALUE;
    int _26461 = NOVALUE;
    int _26460 = NOVALUE;
    int _26459 = NOVALUE;
    int _26458 = NOVALUE;
    int _26457 = NOVALUE;
    int _26456 = NOVALUE;
    int _26455 = NOVALUE;
    int _26454 = NOVALUE;
    int _26452 = NOVALUE;
    int _26450 = NOVALUE;
    int _26448 = NOVALUE;
    int _26446 = NOVALUE;
    int _26443 = NOVALUE;
    int _0, _1, _2;
    

    /** 	while ix < length( args ) do*/
L1: 
    if (IS_SEQUENCE(_args_50277)){
            _26443 = SEQ_PTR(_args_50277)->length;
    }
    else {
        _26443 = 1;
    }
    if (_ix_50276 >= _26443)
    goto L2; // [13] 161

    /** 		sequence arg = args[ix]*/
    DeRef(_arg_50281);
    _2 = (int)SEQ_PTR(_args_50277);
    _arg_50281 = (int)*(((s1_ptr)_2)->base + _ix_50276);
    Ref(_arg_50281);

    /** 		if length( arg ) > 1 then*/
    if (IS_SEQUENCE(_arg_50281)){
            _26446 = SEQ_PTR(_arg_50281)->length;
    }
    else {
        _26446 = 1;
    }
    if (_26446 <= 1)
    goto L3; // [30] 133

    /** 			if arg[1] = '-' then*/
    _2 = (int)SEQ_PTR(_arg_50281);
    _26448 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _26448, 45)){
        _26448 = NOVALUE;
        goto L4; // [40] 115
    }
    _26448 = NOVALUE;

    /** 				if arg[2] = '-' then*/
    _2 = (int)SEQ_PTR(_arg_50281);
    _26450 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _26450, 45)){
        _26450 = NOVALUE;
        goto L5; // [50] 96
    }
    _26450 = NOVALUE;

    /** 					if length( arg ) = 2 then*/
    if (IS_SEQUENCE(_arg_50281)){
            _26452 = SEQ_PTR(_arg_50281)->length;
    }
    else {
        _26452 = 1;
    }
    if (_26452 != 2)
    goto L6; // [59] 78

    /** 						return { 0, ix - 1 }*/
    _26454 = _ix_50276 - 1;
    if ((long)((unsigned long)_26454 +(unsigned long) HIGH_BITS) >= 0){
        _26454 = NewDouble((double)_26454);
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _26454;
    _26455 = MAKE_SEQ(_1);
    _26454 = NOVALUE;
    DeRefDS(_arg_50281);
    DeRefDS(_args_50277);
    return _26455;
L6: 

    /** 					return validate_opt( LONGNAME, arg, args, ix )*/
    RefDS(_arg_50281);
    RefDS(_args_50277);
    _26456 = _39validate_opt(2, _arg_50281, _args_50277, _ix_50276);
    DeRefDS(_arg_50281);
    DeRefDS(_args_50277);
    DeRef(_26455);
    _26455 = NOVALUE;
    return _26456;
    goto L7; // [93] 148
L5: 

    /** 					return validate_opt( SHORTNAME, arg, args, ix )*/
    RefDS(_arg_50281);
    RefDS(_args_50277);
    _26457 = _39validate_opt(1, _arg_50281, _args_50277, _ix_50276);
    DeRefDS(_arg_50281);
    DeRefDS(_args_50277);
    DeRef(_26455);
    _26455 = NOVALUE;
    DeRef(_26456);
    _26456 = NOVALUE;
    return _26457;
    goto L7; // [112] 148
L4: 

    /** 				return {0, ix-1}*/
    _26458 = _ix_50276 - 1;
    if ((long)((unsigned long)_26458 +(unsigned long) HIGH_BITS) >= 0){
        _26458 = NewDouble((double)_26458);
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _26458;
    _26459 = MAKE_SEQ(_1);
    _26458 = NOVALUE;
    DeRef(_arg_50281);
    DeRefDS(_args_50277);
    DeRef(_26455);
    _26455 = NOVALUE;
    DeRef(_26456);
    _26456 = NOVALUE;
    DeRef(_26457);
    _26457 = NOVALUE;
    return _26459;
    goto L7; // [130] 148
L3: 

    /** 			return { 0, ix-1 }*/
    _26460 = _ix_50276 - 1;
    if ((long)((unsigned long)_26460 +(unsigned long) HIGH_BITS) >= 0){
        _26460 = NewDouble((double)_26460);
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _26460;
    _26461 = MAKE_SEQ(_1);
    _26460 = NOVALUE;
    DeRef(_arg_50281);
    DeRefDS(_args_50277);
    DeRef(_26455);
    _26455 = NOVALUE;
    DeRef(_26456);
    _26456 = NOVALUE;
    DeRef(_26457);
    _26457 = NOVALUE;
    DeRef(_26459);
    _26459 = NOVALUE;
    return _26461;
L7: 

    /** 		ix += 1*/
    _ix_50276 = _ix_50276 + 1;
    DeRef(_arg_50281);
    _arg_50281 = NOVALUE;

    /** 	end while*/
    goto L1; // [158] 10
L2: 

    /** 	return {0, ix-1}*/
    _26463 = _ix_50276 - 1;
    if ((long)((unsigned long)_26463 +(unsigned long) HIGH_BITS) >= 0){
        _26463 = NewDouble((double)_26463);
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _26463;
    _26464 = MAKE_SEQ(_1);
    _26463 = NOVALUE;
    DeRefDS(_args_50277);
    DeRef(_26455);
    _26455 = NOVALUE;
    DeRef(_26456);
    _26456 = NOVALUE;
    DeRef(_26457);
    _26457 = NOVALUE;
    DeRef(_26459);
    _26459 = NOVALUE;
    DeRef(_26461);
    _26461 = NOVALUE;
    return _26464;
    ;
}


int _39expand_config_options(int _args_50311)
{
    int _idx_50312 = NOVALUE;
    int _next_idx_50313 = NOVALUE;
    int _files_50314 = NOVALUE;
    int _cmd_1_2_50315 = NOVALUE;
    int _26487 = NOVALUE;
    int _26486 = NOVALUE;
    int _26485 = NOVALUE;
    int _26484 = NOVALUE;
    int _26483 = NOVALUE;
    int _26482 = NOVALUE;
    int _26481 = NOVALUE;
    int _26480 = NOVALUE;
    int _26479 = NOVALUE;
    int _26474 = NOVALUE;
    int _26472 = NOVALUE;
    int _26471 = NOVALUE;
    int _26470 = NOVALUE;
    int _26468 = NOVALUE;
    int _26467 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer idx = 1*/
    _idx_50312 = 1;

    /** 	sequence files = {}*/
    RefDS(_22682);
    DeRef(_files_50314);
    _files_50314 = _22682;

    /** 	sequence cmd_1_2 = args[1..2]*/
    rhs_slice_target = (object_ptr)&_cmd_1_2_50315;
    RHS_Slice(_args_50311, 1, 2);

    /** 	args = remove( args, 1, 2 )*/
    {
        s1_ptr assign_space = SEQ_PTR(_args_50311);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(1)) ? 1 : (long)(DBL_PTR(1)->dbl);
        int stop = (IS_ATOM_INT(2)) ? 2 : (long)(DBL_PTR(2)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_args_50311), start, &_args_50311 );
            }
            else Tail(SEQ_PTR(_args_50311), stop+1, &_args_50311);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_args_50311), start, &_args_50311);
        }
        else {
            assign_slice_seq = &assign_space;
            _args_50311 = Remove_elements(start, stop, (SEQ_PTR(_args_50311)->ref == 1));
        }
    }

    /** 	while idx with entry do*/
    goto L1; // [31] 94
L2: 
    if (_idx_50312 == 0)
    {
        goto L3; // [34] 114
    }
    else{
    }

    /** 		if equal(upper(args[idx]), "-C") then*/
    _2 = (int)SEQ_PTR(_args_50311);
    _26467 = (int)*(((s1_ptr)_2)->base + _idx_50312);
    Ref(_26467);
    _26468 = _4upper(_26467);
    _26467 = NOVALUE;
    if (_26468 == _26469)
    _26470 = 1;
    else if (IS_ATOM_INT(_26468) && IS_ATOM_INT(_26469))
    _26470 = 0;
    else
    _26470 = (compare(_26468, _26469) == 0);
    DeRef(_26468);
    _26468 = NOVALUE;
    if (_26470 == 0)
    {
        _26470 = NOVALUE;
        goto L4; // [51] 82
    }
    else{
        _26470 = NOVALUE;
    }

    /** 			files = append( files, args[idx+1] )*/
    _26471 = _idx_50312 + 1;
    _2 = (int)SEQ_PTR(_args_50311);
    _26472 = (int)*(((s1_ptr)_2)->base + _26471);
    Ref(_26472);
    Append(&_files_50314, _files_50314, _26472);
    _26472 = NOVALUE;

    /** 			args = remove( args, idx, idx + 1 )*/
    _26474 = _idx_50312 + 1;
    if (_26474 > MAXINT){
        _26474 = NewDouble((double)_26474);
    }
    {
        s1_ptr assign_space = SEQ_PTR(_args_50311);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_idx_50312)) ? _idx_50312 : (long)(DBL_PTR(_idx_50312)->dbl);
        int stop = (IS_ATOM_INT(_26474)) ? _26474 : (long)(DBL_PTR(_26474)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_args_50311), start, &_args_50311 );
            }
            else Tail(SEQ_PTR(_args_50311), stop+1, &_args_50311);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_args_50311), start, &_args_50311);
        }
        else {
            assign_slice_seq = &assign_space;
            _args_50311 = Remove_elements(start, stop, (SEQ_PTR(_args_50311)->ref == 1));
        }
    }
    DeRef(_26474);
    _26474 = NOVALUE;
    goto L5; // [79] 91
L4: 

    /** 			idx = next_idx[2]*/
    _2 = (int)SEQ_PTR(_next_idx_50313);
    _idx_50312 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_idx_50312))
    _idx_50312 = (long)DBL_PTR(_idx_50312)->dbl;
L5: 

    /** 	entry*/
L1: 

    /** 		next_idx = find_next_opt( idx, args )*/
    RefDS(_args_50311);
    _0 = _next_idx_50313;
    _next_idx_50313 = _39find_next_opt(_idx_50312, _args_50311);
    DeRef(_0);

    /** 		idx = next_idx[1]*/
    _2 = (int)SEQ_PTR(_next_idx_50313);
    _idx_50312 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_idx_50312))
    _idx_50312 = (long)DBL_PTR(_idx_50312)->dbl;

    /** 	end while*/
    goto L2; // [111] 34
L3: 

    /** 	return cmd_1_2 & merge_parameters( GetDefaultArgs( files ), args[1..next_idx[2]], options, 1 ) & args[next_idx[2]+1..$]*/
    RefDS(_files_50314);
    _26479 = _38GetDefaultArgs(_files_50314);
    _2 = (int)SEQ_PTR(_next_idx_50313);
    _26480 = (int)*(((s1_ptr)_2)->base + 2);
    rhs_slice_target = (object_ptr)&_26481;
    RHS_Slice(_args_50311, 1, _26480);
    RefDS(_39options_49982);
    _26482 = _39merge_parameters(_26479, _26481, _39options_49982, 1);
    _26479 = NOVALUE;
    _26481 = NOVALUE;
    _2 = (int)SEQ_PTR(_next_idx_50313);
    _26483 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_26483)) {
        _26484 = _26483 + 1;
        if (_26484 > MAXINT){
            _26484 = NewDouble((double)_26484);
        }
    }
    else
    _26484 = binary_op(PLUS, 1, _26483);
    _26483 = NOVALUE;
    if (IS_SEQUENCE(_args_50311)){
            _26485 = SEQ_PTR(_args_50311)->length;
    }
    else {
        _26485 = 1;
    }
    rhs_slice_target = (object_ptr)&_26486;
    RHS_Slice(_args_50311, _26484, _26485);
    {
        int concat_list[3];

        concat_list[0] = _26486;
        concat_list[1] = _26482;
        concat_list[2] = _cmd_1_2_50315;
        Concat_N((object_ptr)&_26487, concat_list, 3);
    }
    DeRefDS(_26486);
    _26486 = NOVALUE;
    DeRef(_26482);
    _26482 = NOVALUE;
    DeRefDS(_args_50311);
    DeRefDS(_next_idx_50313);
    DeRefDS(_files_50314);
    DeRefDS(_cmd_1_2_50315);
    DeRef(_26471);
    _26471 = NOVALUE;
    _26480 = NOVALUE;
    DeRef(_26484);
    _26484 = NOVALUE;
    return _26487;
    ;
}


void _39handle_common_options(int _opts_50346)
{
    int _opt_keys_50347 = NOVALUE;
    int _option_w_50349 = NOVALUE;
    int _key_50353 = NOVALUE;
    int _val_50355 = NOVALUE;
    int _this_warn_50401 = NOVALUE;
    int _auto_add_warn_50403 = NOVALUE;
    int _n_50409 = NOVALUE;
    int _this_warn_50432 = NOVALUE;
    int _auto_add_warn_50434 = NOVALUE;
    int _n_50440 = NOVALUE;
    int _has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_618_50476 = NOVALUE;
    int _prompt_inlined_maybe_any_key_at_615_50475 = NOVALUE;
    int _has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_692_50488 = NOVALUE;
    int _prompt_inlined_maybe_any_key_at_689_50487 = NOVALUE;
    int _26540 = NOVALUE;
    int _26539 = NOVALUE;
    int _26538 = NOVALUE;
    int _26537 = NOVALUE;
    int _26536 = NOVALUE;
    int _26535 = NOVALUE;
    int _26534 = NOVALUE;
    int _26533 = NOVALUE;
    int _26532 = NOVALUE;
    int _26530 = NOVALUE;
    int _26528 = NOVALUE;
    int _26527 = NOVALUE;
    int _26526 = NOVALUE;
    int _26521 = NOVALUE;
    int _26519 = NOVALUE;
    int _26517 = NOVALUE;
    int _26514 = NOVALUE;
    int _26513 = NOVALUE;
    int _26508 = NOVALUE;
    int _26506 = NOVALUE;
    int _26504 = NOVALUE;
    int _26502 = NOVALUE;
    int _26501 = NOVALUE;
    int _26500 = NOVALUE;
    int _26499 = NOVALUE;
    int _26498 = NOVALUE;
    int _26497 = NOVALUE;
    int _26495 = NOVALUE;
    int _26494 = NOVALUE;
    int _26489 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence opt_keys = m:keys(opts)*/
    Ref(_opts_50346);
    _0 = _opt_keys_50347;
    _opt_keys_50347 = _32keys(_opts_50346, 0);
    DeRef(_0);

    /** 	integer option_w = 0*/
    _option_w_50349 = 0;

    /** 	for idx = 1 to length(opt_keys) do*/
    if (IS_SEQUENCE(_opt_keys_50347)){
            _26489 = SEQ_PTR(_opt_keys_50347)->length;
    }
    else {
        _26489 = 1;
    }
    {
        int _idx_50351;
        _idx_50351 = 1;
L1: 
        if (_idx_50351 > _26489){
            goto L2; // [20] 731
        }

        /** 		sequence key = opt_keys[idx]*/
        DeRef(_key_50353);
        _2 = (int)SEQ_PTR(_opt_keys_50347);
        _key_50353 = (int)*(((s1_ptr)_2)->base + _idx_50351);
        Ref(_key_50353);

        /** 		object val = m:get(opts, key)*/
        Ref(_opts_50346);
        RefDS(_key_50353);
        _0 = _val_50355;
        _val_50355 = _32get(_opts_50346, _key_50353, 0);
        DeRef(_0);

        /** 		switch key do*/
        _1 = find(_key_50353, _26492);
        switch ( _1 ){ 

            /** 			case "i" then*/
            case 1:

            /** 				for i = 1 to length(val) do*/
            if (IS_SEQUENCE(_val_50355)){
                    _26494 = SEQ_PTR(_val_50355)->length;
            }
            else {
                _26494 = 1;
            }
            {
                int _i_50361;
                _i_50361 = 1;
L3: 
                if (_i_50361 > _26494){
                    goto L4; // [59] 82
                }

                /** 					add_include_directory(val[i])*/
                _2 = (int)SEQ_PTR(_val_50355);
                _26495 = (int)*(((s1_ptr)_2)->base + _i_50361);
                Ref(_26495);
                _38add_include_directory(_26495);
                _26495 = NOVALUE;

                /** 				end for*/
                _i_50361 = _i_50361 + 1;
                goto L3; // [77] 66
L4: 
                ;
            }
            goto L5; // [82] 722

            /** 			case "d" then*/
            case 2:

            /** 				OpDefines &= val*/
            if (IS_SEQUENCE(_25OpDefines_12336) && IS_ATOM(_val_50355)) {
                Ref(_val_50355);
                Append(&_25OpDefines_12336, _25OpDefines_12336, _val_50355);
            }
            else if (IS_ATOM(_25OpDefines_12336) && IS_SEQUENCE(_val_50355)) {
            }
            else {
                Concat((object_ptr)&_25OpDefines_12336, _25OpDefines_12336, _val_50355);
            }
            goto L5; // [98] 722

            /** 			case "batch" then*/
            case 3:

            /** 				batch_job = 1*/
            _25batch_job_12275 = 1;
            goto L5; // [111] 722

            /** 			case "test" then*/
            case 4:

            /** 				test_only = 1*/
            _25test_only_12274 = 1;
            goto L5; // [124] 722

            /** 			case "strict" then*/
            case 5:

            /** 				Strict_is_on = 1*/
            _25Strict_is_on_12328 = 1;
            goto L5; // [137] 722

            /** 			case "p" then*/
            case 6:

            /** 				for i = 1 to length(val) do*/
            if (IS_SEQUENCE(_val_50355)){
                    _26497 = SEQ_PTR(_val_50355)->length;
            }
            else {
                _26497 = 1;
            }
            {
                int _i_50376;
                _i_50376 = 1;
L6: 
                if (_i_50376 > _26497){
                    goto L7; // [148] 173
                }

                /** 					add_preprocessor(val[i])*/
                _2 = (int)SEQ_PTR(_val_50355);
                _26498 = (int)*(((s1_ptr)_2)->base + _i_50376);
                Ref(_26498);
                _64add_preprocessor(_26498, 0, 0);
                _26498 = NOVALUE;

                /** 				end for*/
                _i_50376 = _i_50376 + 1;
                goto L6; // [168] 155
L7: 
                ;
            }
            goto L5; // [173] 722

            /** 			case "pf" then*/
            case 7:

            /** 				force_preprocessor = 1*/
            _26force_preprocessor_11158 = 1;
            goto L5; // [186] 722

            /** 			case "l" then*/
            case 8:

            /** 				for i = 1 to length(val) do*/
            if (IS_SEQUENCE(_val_50355)){
                    _26499 = SEQ_PTR(_val_50355)->length;
            }
            else {
                _26499 = 1;
            }
            {
                int _i_50384;
                _i_50384 = 1;
L8: 
                if (_i_50384 > _26499){
                    goto L9; // [197] 238
                }

                /** 					LocalizeQual = append(LocalizeQual, (filter(lower(val[i]), STDFLTR_ALPHA)))*/
                _2 = (int)SEQ_PTR(_val_50355);
                _26500 = (int)*(((s1_ptr)_2)->base + _i_50384);
                Ref(_26500);
                _26501 = _4lower(_26500);
                _26500 = NOVALUE;
                RefDS(_22682);
                RefDS(_5);
                _26502 = _21filter(_26501, _21STDFLTR_ALPHA_6154, _22682, _5);
                _26501 = NOVALUE;
                Ref(_26502);
                Append(&_26LocalizeQual_11159, _26LocalizeQual_11159, _26502);
                DeRef(_26502);
                _26502 = NOVALUE;

                /** 				end for*/
                _i_50384 = _i_50384 + 1;
                goto L8; // [233] 204
L9: 
                ;
            }
            goto L5; // [238] 722

            /** 			case "ldb" then*/
            case 9:

            /** 				LocalDB = val*/
            Ref(_val_50355);
            DeRef(_26LocalDB_11160);
            _26LocalDB_11160 = _val_50355;
            goto L5; // [251] 722

            /** 			case "w" then*/
            case 10:

            /** 				for i = 1 to length(val) do*/
            if (IS_SEQUENCE(_val_50355)){
                    _26504 = SEQ_PTR(_val_50355)->length;
            }
            else {
                _26504 = 1;
            }
            {
                int _i_50399;
                _i_50399 = 1;
LA: 
                if (_i_50399 > _26504){
                    goto LB; // [262] 392
                }

                /** 					sequence this_warn = val[i]*/
                DeRef(_this_warn_50401);
                _2 = (int)SEQ_PTR(_val_50355);
                _this_warn_50401 = (int)*(((s1_ptr)_2)->base + _i_50399);
                Ref(_this_warn_50401);

                /** 					integer auto_add_warn = 0*/
                _auto_add_warn_50403 = 0;

                /** 					if this_warn[1] = '+' then*/
                _2 = (int)SEQ_PTR(_this_warn_50401);
                _26506 = (int)*(((s1_ptr)_2)->base + 1);
                if (binary_op_a(NOTEQ, _26506, 43)){
                    _26506 = NOVALUE;
                    goto LC; // [288] 308
                }
                _26506 = NOVALUE;

                /** 						auto_add_warn = 1*/
                _auto_add_warn_50403 = 1;

                /** 						this_warn = this_warn[2 .. $]*/
                if (IS_SEQUENCE(_this_warn_50401)){
                        _26508 = SEQ_PTR(_this_warn_50401)->length;
                }
                else {
                    _26508 = 1;
                }
                rhs_slice_target = (object_ptr)&_this_warn_50401;
                RHS_Slice(_this_warn_50401, 2, _26508);
LC: 

                /** 					integer n = find(this_warn, warning_names)*/
                _n_50409 = find_from(_this_warn_50401, _25warning_names_12307, 1);

                /** 					if n != 0 then*/
                if (_n_50409 == 0)
                goto LD; // [319] 383

                /** 						if auto_add_warn or option_w = 1 then*/
                if (_auto_add_warn_50403 != 0) {
                    goto LE; // [325] 338
                }
                _26513 = (_option_w_50349 == 1);
                if (_26513 == 0)
                {
                    DeRef(_26513);
                    _26513 = NOVALUE;
                    goto LF; // [334] 357
                }
                else{
                    DeRef(_26513);
                    _26513 = NOVALUE;
                }
LE: 

                /** 							OpWarning = or_bits(OpWarning, warning_flags[n])*/
                _2 = (int)SEQ_PTR(_25warning_flags_12305);
                _26514 = (int)*(((s1_ptr)_2)->base + _n_50409);
                {unsigned long tu;
                     tu = (unsigned long)_25OpWarning_12330 | (unsigned long)_26514;
                     _25OpWarning_12330 = MAKE_UINT(tu);
                }
                _26514 = NOVALUE;
                if (!IS_ATOM_INT(_25OpWarning_12330)) {
                    _1 = (long)(DBL_PTR(_25OpWarning_12330)->dbl);
                    if (UNIQUE(DBL_PTR(_25OpWarning_12330)) && (DBL_PTR(_25OpWarning_12330)->cleanup != 0))
                    RTFatal("Cannot assign value with a destructor to an integer");                    DeRefDS(_25OpWarning_12330);
                    _25OpWarning_12330 = _1;
                }
                goto L10; // [354] 373
LF: 

                /** 							option_w = 1*/
                _option_w_50349 = 1;

                /** 							OpWarning = warning_flags[n]*/
                _2 = (int)SEQ_PTR(_25warning_flags_12305);
                _25OpWarning_12330 = (int)*(((s1_ptr)_2)->base + _n_50409);
L10: 

                /** 						prev_OpWarning = OpWarning*/
                _25prev_OpWarning_12331 = _25OpWarning_12330;
LD: 
                DeRef(_this_warn_50401);
                _this_warn_50401 = NOVALUE;

                /** 				end for*/
                _i_50399 = _i_50399 + 1;
                goto LA; // [387] 269
LB: 
                ;
            }
            goto L5; // [392] 722

            /** 			case "x" then*/
            case 11:

            /** 				for i = 1 to length(val) do*/
            if (IS_SEQUENCE(_val_50355)){
                    _26517 = SEQ_PTR(_val_50355)->length;
            }
            else {
                _26517 = 1;
            }
            {
                int _i_50430;
                _i_50430 = 1;
L11: 
                if (_i_50430 > _26517){
                    goto L12; // [403] 542
                }

                /** 					sequence this_warn = val[i]*/
                DeRef(_this_warn_50432);
                _2 = (int)SEQ_PTR(_val_50355);
                _this_warn_50432 = (int)*(((s1_ptr)_2)->base + _i_50430);
                Ref(_this_warn_50432);

                /** 					integer auto_add_warn = 0*/
                _auto_add_warn_50434 = 0;

                /** 					if this_warn[1] = '+' then*/
                _2 = (int)SEQ_PTR(_this_warn_50432);
                _26519 = (int)*(((s1_ptr)_2)->base + 1);
                if (binary_op_a(NOTEQ, _26519, 43)){
                    _26519 = NOVALUE;
                    goto L13; // [429] 449
                }
                _26519 = NOVALUE;

                /** 						auto_add_warn = 1*/
                _auto_add_warn_50434 = 1;

                /** 						this_warn = this_warn[2 .. $]*/
                if (IS_SEQUENCE(_this_warn_50432)){
                        _26521 = SEQ_PTR(_this_warn_50432)->length;
                }
                else {
                    _26521 = 1;
                }
                rhs_slice_target = (object_ptr)&_this_warn_50432;
                RHS_Slice(_this_warn_50432, 2, _26521);
L13: 

                /** 					integer n = find(this_warn, warning_names)*/
                _n_50440 = find_from(_this_warn_50432, _25warning_names_12307, 1);

                /** 					if n != 0 then*/
                if (_n_50440 == 0)
                goto L14; // [460] 533

                /** 						if auto_add_warn or option_w = -1 then*/
                if (_auto_add_warn_50434 != 0) {
                    goto L15; // [466] 479
                }
                _26526 = (_option_w_50349 == -1);
                if (_26526 == 0)
                {
                    DeRef(_26526);
                    _26526 = NOVALUE;
                    goto L16; // [475] 501
                }
                else{
                    DeRef(_26526);
                    _26526 = NOVALUE;
                }
L15: 

                /** 							OpWarning = and_bits(OpWarning, not_bits(warning_flags[n]))*/
                _2 = (int)SEQ_PTR(_25warning_flags_12305);
                _26527 = (int)*(((s1_ptr)_2)->base + _n_50440);
                _26528 = not_bits(_26527);
                _26527 = NOVALUE;
                if (IS_ATOM_INT(_26528)) {
                    {unsigned long tu;
                         tu = (unsigned long)_25OpWarning_12330 & (unsigned long)_26528;
                         _25OpWarning_12330 = MAKE_UINT(tu);
                    }
                }
                else {
                    temp_d.dbl = (double)_25OpWarning_12330;
                    _25OpWarning_12330 = Dand_bits(&temp_d, DBL_PTR(_26528));
                }
                DeRef(_26528);
                _26528 = NOVALUE;
                if (!IS_ATOM_INT(_25OpWarning_12330)) {
                    _1 = (long)(DBL_PTR(_25OpWarning_12330)->dbl);
                    if (UNIQUE(DBL_PTR(_25OpWarning_12330)) && (DBL_PTR(_25OpWarning_12330)->cleanup != 0))
                    RTFatal("Cannot assign value with a destructor to an integer");                    DeRefDS(_25OpWarning_12330);
                    _25OpWarning_12330 = _1;
                }
                goto L17; // [498] 523
L16: 

                /** 							option_w = -1*/
                _option_w_50349 = -1;

                /** 							OpWarning = all_warning_flag - warning_flags[n]*/
                _2 = (int)SEQ_PTR(_25warning_flags_12305);
                _26530 = (int)*(((s1_ptr)_2)->base + _n_50440);
                _25OpWarning_12330 = 32767 - _26530;
                _26530 = NOVALUE;
L17: 

                /** 						prev_OpWarning = OpWarning*/
                _25prev_OpWarning_12331 = _25OpWarning_12330;
L14: 
                DeRef(_this_warn_50432);
                _this_warn_50432 = NOVALUE;

                /** 				end for*/
                _i_50430 = _i_50430 + 1;
                goto L11; // [537] 410
L12: 
                ;
            }
            goto L5; // [542] 722

            /** 			case "wf" then*/
            case 12:

            /** 				TempWarningName = val*/
            Ref(_val_50355);
            DeRef(_25TempWarningName_12276);
            _25TempWarningName_12276 = _val_50355;

            /** 			  	error:warning_file(TempWarningName)*/
            Ref(_25TempWarningName_12276);
            _8warning_file(_25TempWarningName_12276);
            goto L5; // [560] 722

            /** 			case "v", "version" then*/
            case 13:
            case 14:

            /** 				show_banner()*/
            _39show_banner();

            /** 				if not batch_job and not test_only then*/
            _26532 = (_25batch_job_12275 == 0);
            if (_26532 == 0) {
                goto L18; // [579] 632
            }
            _26534 = (_25test_only_12274 == 0);
            if (_26534 == 0)
            {
                DeRef(_26534);
                _26534 = NOVALUE;
                goto L18; // [589] 632
            }
            else{
                DeRef(_26534);
                _26534 = NOVALUE;
            }

            /** 					console:maybe_any_key(GetMsgText(278,0), 2)*/
            RefDS(_22682);
            _26535 = _44GetMsgText(278, 0, _22682);
            DeRef(_prompt_inlined_maybe_any_key_at_615_50475);
            _prompt_inlined_maybe_any_key_at_615_50475 = _26535;
            _26535 = NOVALUE;

            /** 	if not has_console() then*/

            /** 	return machine_func(M_HAS_CONSOLE, 0)*/
            DeRef(_has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_618_50476);
            _has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_618_50476 = machine(99, 0);
            if (IS_ATOM_INT(_has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_618_50476)) {
                if (_has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_618_50476 != 0){
                    goto L19; // [614] 629
                }
            }
            else {
                if (DBL_PTR(_has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_618_50476)->dbl != 0.0){
                    goto L19; // [614] 629
                }
            }

            /** 		any_key(prompt, con)*/
            Ref(_prompt_inlined_maybe_any_key_at_615_50475);
            _41any_key(_prompt_inlined_maybe_any_key_at_615_50475, 2);

            /** end procedure*/
            goto L19; // [626] 629
L19: 
            DeRef(_prompt_inlined_maybe_any_key_at_615_50475);
            _prompt_inlined_maybe_any_key_at_615_50475 = NOVALUE;
            DeRef(_has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_618_50476);
            _has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_618_50476 = NOVALUE;
L18: 

            /** 				abort(0)*/
            UserCleanup(0);
            goto L5; // [636] 722

            /** 			case "copyright" then*/
            case 15:

            /** 				show_copyrights()*/
            _39show_copyrights();

            /** 				if not batch_job and not test_only then*/
            _26536 = (_25batch_job_12275 == 0);
            if (_26536 == 0) {
                goto L1A; // [653] 706
            }
            _26538 = (_25test_only_12274 == 0);
            if (_26538 == 0)
            {
                DeRef(_26538);
                _26538 = NOVALUE;
                goto L1A; // [663] 706
            }
            else{
                DeRef(_26538);
                _26538 = NOVALUE;
            }

            /** 					console:maybe_any_key(GetMsgText(278,0), 2)*/
            RefDS(_22682);
            _26539 = _44GetMsgText(278, 0, _22682);
            DeRef(_prompt_inlined_maybe_any_key_at_689_50487);
            _prompt_inlined_maybe_any_key_at_689_50487 = _26539;
            _26539 = NOVALUE;

            /** 	if not has_console() then*/

            /** 	return machine_func(M_HAS_CONSOLE, 0)*/
            DeRef(_has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_692_50488);
            _has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_692_50488 = machine(99, 0);
            if (IS_ATOM_INT(_has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_692_50488)) {
                if (_has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_692_50488 != 0){
                    goto L1B; // [688] 703
                }
            }
            else {
                if (DBL_PTR(_has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_692_50488)->dbl != 0.0){
                    goto L1B; // [688] 703
                }
            }

            /** 		any_key(prompt, con)*/
            Ref(_prompt_inlined_maybe_any_key_at_689_50487);
            _41any_key(_prompt_inlined_maybe_any_key_at_689_50487, 2);

            /** end procedure*/
            goto L1B; // [700] 703
L1B: 
            DeRef(_prompt_inlined_maybe_any_key_at_689_50487);
            _prompt_inlined_maybe_any_key_at_689_50487 = NOVALUE;
            DeRef(_has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_692_50488);
            _has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_692_50488 = NOVALUE;
L1A: 

            /** 				abort(0)*/
            UserCleanup(0);
            goto L5; // [710] 722

            /** 			case "eudir" then*/
            case 16:

            /** 				set_eudir( val )*/
            Ref(_val_50355);
            _26set_eudir(_val_50355);
        ;}L5: 
        DeRef(_key_50353);
        _key_50353 = NOVALUE;
        DeRef(_val_50355);
        _val_50355 = NOVALUE;

        /** 	end for*/
        _idx_50351 = _idx_50351 + 1;
        goto L1; // [726] 27
L2: 
        ;
    }

    /** 	if length(LocalizeQual) = 0 then*/
    if (IS_SEQUENCE(_26LocalizeQual_11159)){
            _26540 = SEQ_PTR(_26LocalizeQual_11159)->length;
    }
    else {
        _26540 = 1;
    }
    if (_26540 != 0)
    goto L1C; // [738] 751

    /** 		LocalizeQual = {"en"}*/
    _0 = _26LocalizeQual_11159;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_26542);
    *((int *)(_2+4)) = _26542;
    _26LocalizeQual_11159 = MAKE_SEQ(_1);
    DeRefDS(_0);
L1C: 

    /** end procedure*/
    DeRef(_opts_50346);
    DeRef(_opt_keys_50347);
    DeRef(_26532);
    _26532 = NOVALUE;
    DeRef(_26536);
    _26536 = NOVALUE;
    return;
    ;
}


void _39finalize_command_line(int _opts_50500)
{
    int _extras_50507 = NOVALUE;
    int _pairs_50512 = NOVALUE;
    int _pair_50517 = NOVALUE;
    int _26572 = NOVALUE;
    int _26570 = NOVALUE;
    int _26567 = NOVALUE;
    int _26566 = NOVALUE;
    int _26565 = NOVALUE;
    int _26564 = NOVALUE;
    int _26563 = NOVALUE;
    int _26562 = NOVALUE;
    int _26561 = NOVALUE;
    int _26560 = NOVALUE;
    int _26559 = NOVALUE;
    int _26558 = NOVALUE;
    int _26557 = NOVALUE;
    int _26556 = NOVALUE;
    int _26555 = NOVALUE;
    int _26554 = NOVALUE;
    int _26553 = NOVALUE;
    int _26552 = NOVALUE;
    int _26551 = NOVALUE;
    int _26550 = NOVALUE;
    int _26548 = NOVALUE;
    int _26545 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if Strict_is_on then -- overrides any -W/-X switches*/
    if (_25Strict_is_on_12328 == 0)
    {
        goto L1; // [5] 27
    }
    else{
    }

    /** 		OpWarning = all_warning_flag*/
    _25OpWarning_12330 = 32767;

    /** 		prev_OpWarning = OpWarning*/
    _25prev_OpWarning_12331 = 32767;
L1: 

    /** 	sequence extras = m:get(opts, cmdline:EXTRAS)*/
    Ref(_opts_50500);
    RefDS(_40EXTRAS_15909);
    _0 = _extras_50507;
    _extras_50507 = _32get(_opts_50500, _40EXTRAS_15909, 0);
    DeRef(_0);

    /** 	if length(extras) > 0 then*/
    if (IS_SEQUENCE(_extras_50507)){
            _26545 = SEQ_PTR(_extras_50507)->length;
    }
    else {
        _26545 = 1;
    }
    if (_26545 <= 0)
    goto L2; // [44] 270

    /** 		sequence pairs = m:pairs( opts )*/
    Ref(_opts_50500);
    _0 = _pairs_50512;
    _pairs_50512 = _32pairs(_opts_50500, 0);
    DeRef(_0);

    /** 		for i = 1 to length( pairs ) do*/
    if (IS_SEQUENCE(_pairs_50512)){
            _26548 = SEQ_PTR(_pairs_50512)->length;
    }
    else {
        _26548 = 1;
    }
    {
        int _i_50515;
        _i_50515 = 1;
L3: 
        if (_i_50515 > _26548){
            goto L4; // [62] 237
        }

        /** 			sequence pair = pairs[i]*/
        DeRef(_pair_50517);
        _2 = (int)SEQ_PTR(_pairs_50512);
        _pair_50517 = (int)*(((s1_ptr)_2)->base + _i_50515);
        Ref(_pair_50517);

        /** 			if equal( pair[1], cmdline:EXTRAS ) then*/
        _2 = (int)SEQ_PTR(_pair_50517);
        _26550 = (int)*(((s1_ptr)_2)->base + 1);
        if (_26550 == _40EXTRAS_15909)
        _26551 = 1;
        else if (IS_ATOM_INT(_26550) && IS_ATOM_INT(_40EXTRAS_15909))
        _26551 = 0;
        else
        _26551 = (compare(_26550, _40EXTRAS_15909) == 0);
        _26550 = NOVALUE;
        if (_26551 == 0)
        {
            _26551 = NOVALUE;
            goto L5; // [89] 99
        }
        else{
            _26551 = NOVALUE;
        }

        /** 				continue*/
        DeRefDS(_pair_50517);
        _pair_50517 = NOVALUE;
        goto L6; // [96] 232
L5: 

        /** 			pair[1] = prepend( pair[1], '-' )*/
        _2 = (int)SEQ_PTR(_pair_50517);
        _26552 = (int)*(((s1_ptr)_2)->base + 1);
        Prepend(&_26553, _26552, 45);
        _26552 = NOVALUE;
        _2 = (int)SEQ_PTR(_pair_50517);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _pair_50517 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 1);
        _1 = *(int *)_2;
        *(int *)_2 = _26553;
        if( _1 != _26553 ){
            DeRef(_1);
        }
        _26553 = NOVALUE;

        /** 			if sequence( pair[2] ) then*/
        _2 = (int)SEQ_PTR(_pair_50517);
        _26554 = (int)*(((s1_ptr)_2)->base + 2);
        _26555 = IS_SEQUENCE(_26554);
        _26554 = NOVALUE;
        if (_26555 == 0)
        {
            _26555 = NOVALUE;
            goto L7; // [122] 215
        }
        else{
            _26555 = NOVALUE;
        }

        /** 				if length( pair[2] ) and sequence( pair[2][1] ) then*/
        _2 = (int)SEQ_PTR(_pair_50517);
        _26556 = (int)*(((s1_ptr)_2)->base + 2);
        if (IS_SEQUENCE(_26556)){
                _26557 = SEQ_PTR(_26556)->length;
        }
        else {
            _26557 = 1;
        }
        _26556 = NOVALUE;
        if (_26557 == 0) {
            goto L8; // [134] 203
        }
        _2 = (int)SEQ_PTR(_pair_50517);
        _26559 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_26559);
        _26560 = (int)*(((s1_ptr)_2)->base + 1);
        _26559 = NOVALUE;
        _26561 = IS_SEQUENCE(_26560);
        _26560 = NOVALUE;
        if (_26561 == 0)
        {
            _26561 = NOVALUE;
            goto L8; // [150] 203
        }
        else{
            _26561 = NOVALUE;
        }

        /** 					for j = 1 to length( pair[2] ) do*/
        _2 = (int)SEQ_PTR(_pair_50517);
        _26562 = (int)*(((s1_ptr)_2)->base + 2);
        if (IS_SEQUENCE(_26562)){
                _26563 = SEQ_PTR(_26562)->length;
        }
        else {
            _26563 = 1;
        }
        _26562 = NOVALUE;
        {
            int _j_50535;
            _j_50535 = 1;
L9: 
            if (_j_50535 > _26563){
                goto LA; // [162] 200
            }

            /** 						switches &= { pair[1], pair[2][j] }*/
            _2 = (int)SEQ_PTR(_pair_50517);
            _26564 = (int)*(((s1_ptr)_2)->base + 1);
            _2 = (int)SEQ_PTR(_pair_50517);
            _26565 = (int)*(((s1_ptr)_2)->base + 2);
            _2 = (int)SEQ_PTR(_26565);
            _26566 = (int)*(((s1_ptr)_2)->base + _j_50535);
            _26565 = NOVALUE;
            Ref(_26566);
            Ref(_26564);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _26564;
            ((int *)_2)[2] = _26566;
            _26567 = MAKE_SEQ(_1);
            _26566 = NOVALUE;
            _26564 = NOVALUE;
            Concat((object_ptr)&_39switches_49879, _39switches_49879, _26567);
            DeRefDS(_26567);
            _26567 = NOVALUE;

            /** 					end for*/
            _j_50535 = _j_50535 + 1;
            goto L9; // [195] 169
LA: 
            ;
        }
        goto LB; // [200] 228
L8: 

        /** 					switches &= pair*/
        Concat((object_ptr)&_39switches_49879, _39switches_49879, _pair_50517);
        goto LB; // [212] 228
L7: 

        /** 				switches = append( switches, pair[1] )*/
        _2 = (int)SEQ_PTR(_pair_50517);
        _26570 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_26570);
        Append(&_39switches_49879, _39switches_49879, _26570);
        _26570 = NOVALUE;
LB: 
        DeRef(_pair_50517);
        _pair_50517 = NOVALUE;

        /** 		end for*/
L6: 
        _i_50515 = _i_50515 + 1;
        goto L3; // [232] 69
L4: 
        ;
    }

    /** 		Argv = Argv[2..3] & extras*/
    rhs_slice_target = (object_ptr)&_26572;
    RHS_Slice(_25Argv_12273, 2, 3);
    Concat((object_ptr)&_25Argv_12273, _26572, _extras_50507);
    DeRefDS(_26572);
    _26572 = NOVALUE;
    DeRef(_26572);
    _26572 = NOVALUE;

    /** 		Argc = length(Argv)*/
    if (IS_SEQUENCE(_25Argv_12273)){
            _25Argc_12272 = SEQ_PTR(_25Argv_12273)->length;
    }
    else {
        _25Argc_12272 = 1;
    }

    /** 		src_name = extras[1]*/
    DeRef(_39src_name_49878);
    _2 = (int)SEQ_PTR(_extras_50507);
    _39src_name_49878 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_39src_name_49878);
L2: 
    DeRef(_pairs_50512);
    _pairs_50512 = NOVALUE;

    /** end procedure*/
    DeRef(_opts_50500);
    DeRef(_extras_50507);
    _26556 = NOVALUE;
    _26562 = NOVALUE;
    return;
    ;
}



// 0xF6E2AB58
