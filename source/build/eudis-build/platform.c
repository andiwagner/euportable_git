// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _36host_platform()
{
    int _0, _1, _2;
    

    /** 	return ihost_platform*/
    return _36ihost_platform_14737;
    ;
}


void _36set_host_platform(int _plat_14744)
{
    int _8196 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ihost_platform = floor(plat)*/
    _36ihost_platform_14737 = e_floor(_plat_14744);

    /** 	TUNIX    = (find(ihost_platform, unices) != 0) */
    _8196 = find_from(_36ihost_platform_14737, _36unices_14740, 1);
    _36TUNIX_14725 = (_8196 != 0);
    _8196 = NOVALUE;

    /** 	TWINDOWS = (ihost_platform = WIN32)*/
    _36TWINDOWS_14721 = (_36ihost_platform_14737 == 2);

    /** 	TBSD     = (ihost_platform = UFREEBSD)*/
    _36TBSD_14727 = (_36ihost_platform_14737 == 8);

    /** 	TOSX     = (ihost_platform = UOSX)*/
    _36TOSX_14729 = (_36ihost_platform_14737 == 4);

    /** 	TLINUX   = (ihost_platform = ULINUX)*/
    _36TLINUX_14723 = (_36ihost_platform_14737 == 3);

    /** 	TOPENBSD = (ihost_platform = UOPENBSD)*/
    _36TOPENBSD_14731 = (_36ihost_platform_14737 == 6);

    /** 	TNETBSD  = (ihost_platform = UNETBSD)*/
    _36TNETBSD_14733 = (_36ihost_platform_14737 == 7);

    /** 	IUNIX    = TUNIX*/
    _36IUNIX_14724 = _36TUNIX_14725;

    /** 	IWINDOWS = TWINDOWS*/
    _36IWINDOWS_14720 = _36TWINDOWS_14721;

    /** 	IBSD     = TBSD*/
    _36IBSD_14726 = _36TBSD_14727;

    /** 	IOSX     = TOSX*/
    _36IOSX_14728 = _36TOSX_14729;

    /** 	ILINUX   = TLINUX*/
    _36ILINUX_14722 = _36TLINUX_14723;

    /** 	IOPENBSD = TOPENBSD*/
    _36IOPENBSD_14730 = _36TOPENBSD_14731;

    /** 	INETBSD  = TNETBSD*/
    _36INETBSD_14732 = _36TNETBSD_14733;

    /** 	if TUNIX then*/
    if (_36TUNIX_14725 == 0)
    {
        goto L1; // [148] 161
    }
    else{
    }

    /** 		HOSTNL = "\n"*/
    RefDS(_1155);
    DeRefi(_36HOSTNL_14736);
    _36HOSTNL_14736 = _1155;
    goto L2; // [158] 169
L1: 

    /** 		HOSTNL = "\r\n"*/
    RefDS(_1154);
    DeRefi(_36HOSTNL_14736);
    _36HOSTNL_14736 = _1154;
L2: 

    /** end procedure*/
    return;
    ;
}


int _36GetPlatformDefines(int _for_translator_14759)
{
    int _local_defines_14760 = NOVALUE;
    int _lcmds_14770 = NOVALUE;
    int _fh_14772 = NOVALUE;
    int _sk_14792 = NOVALUE;
    int _8297 = NOVALUE;
    int _8296 = NOVALUE;
    int _8294 = NOVALUE;
    int _8291 = NOVALUE;
    int _8289 = NOVALUE;
    int _8287 = NOVALUE;
    int _8286 = NOVALUE;
    int _8284 = NOVALUE;
    int _8282 = NOVALUE;
    int _8280 = NOVALUE;
    int _8279 = NOVALUE;
    int _8277 = NOVALUE;
    int _8275 = NOVALUE;
    int _8273 = NOVALUE;
    int _8272 = NOVALUE;
    int _8270 = NOVALUE;
    int _8267 = NOVALUE;
    int _8265 = NOVALUE;
    int _8264 = NOVALUE;
    int _8262 = NOVALUE;
    int _8260 = NOVALUE;
    int _8258 = NOVALUE;
    int _8257 = NOVALUE;
    int _8254 = NOVALUE;
    int _8252 = NOVALUE;
    int _8249 = NOVALUE;
    int _8245 = NOVALUE;
    int _8244 = NOVALUE;
    int _8239 = NOVALUE;
    int _8238 = NOVALUE;
    int _8225 = NOVALUE;
    int _8222 = NOVALUE;
    int _8219 = NOVALUE;
    int _8218 = NOVALUE;
    int _8217 = NOVALUE;
    int _8213 = NOVALUE;
    int _8210 = NOVALUE;
    int _8207 = NOVALUE;
    int _8205 = NOVALUE;
    int _8204 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence local_defines = {}*/
    RefDS(_5);
    DeRef(_local_defines_14760);
    _local_defines_14760 = _5;

    /** 	if (IWINDOWS and not for_translator) or (TWINDOWS and for_translator) then*/
    if (_36IWINDOWS_14720 == 0) {
        _8204 = 0;
        goto L1; // [14] 25
    }
    _8205 = (_for_translator_14759 == 0);
    _8204 = (_8205 != 0);
L1: 
    if (_8204 != 0) {
        goto L2; // [25] 44
    }
    if (_36TWINDOWS_14721 == 0) {
        DeRef(_8207);
        _8207 = 0;
        goto L3; // [31] 39
    }
    _8207 = (_for_translator_14759 != 0);
L3: 
    if (_8207 == 0)
    {
        _8207 = NOVALUE;
        goto L4; // [40] 326
    }
    else{
        _8207 = NOVALUE;
    }
L2: 

    /** 		local_defines &= {"WINDOWS", "WIN32"}*/
    RefDS(_8209);
    RefDS(_8208);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _8208;
    ((int *)_2)[2] = _8209;
    _8210 = MAKE_SEQ(_1);
    Concat((object_ptr)&_local_defines_14760, _local_defines_14760, _8210);
    DeRefDS(_8210);
    _8210 = NOVALUE;

    /** 		sequence lcmds = command_line()*/
    DeRef(_lcmds_14770);
    _lcmds_14770 = Command_Line();

    /** 		integer fh*/

    /** 		fh = open(lcmds[1], "rb")*/
    _2 = (int)SEQ_PTR(_lcmds_14770);
    _8213 = (int)*(((s1_ptr)_2)->base + 1);
    _fh_14772 = EOpen(_8213, _1138, 0);
    _8213 = NOVALUE;

    /** 		if fh = -1 then*/
    if (_fh_14772 != -1)
    goto L5; // [73] 123

    /**  			if match("euiw", lower(lcmds[1])) != 0 then*/
    _2 = (int)SEQ_PTR(_lcmds_14770);
    _8217 = (int)*(((s1_ptr)_2)->base + 1);
    RefDS(_8217);
    _8218 = _4lower(_8217);
    _8217 = NOVALUE;
    _8219 = e_match_from(_8216, _8218, 1);
    DeRef(_8218);
    _8218 = NOVALUE;
    if (_8219 == 0)
    goto L6; // [92] 109

    /**  				local_defines &= { "GUI" }*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_8221);
    *((int *)(_2+4)) = _8221;
    _8222 = MAKE_SEQ(_1);
    Concat((object_ptr)&_local_defines_14760, _local_defines_14760, _8222);
    DeRefDS(_8222);
    _8222 = NOVALUE;
    goto L7; // [106] 321
L6: 

    /**  				local_defines &= { "CONSOLE" }*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_8224);
    *((int *)(_2+4)) = _8224;
    _8225 = MAKE_SEQ(_1);
    Concat((object_ptr)&_local_defines_14760, _local_defines_14760, _8225);
    DeRefDS(_8225);
    _8225 = NOVALUE;
    goto L7; // [120] 321
L5: 

    /** 			atom sk*/

    /** 			sk = seek(fh, #18) -- Fixed location of relocation table.*/
    _0 = _sk_14792;
    _sk_14792 = _16seek(_fh_14772, 24);
    DeRef(_0);

    /** 			sk = get_integer16(fh)*/
    _0 = _sk_14792;
    _sk_14792 = _16get_integer16(_fh_14772);
    DeRef(_0);

    /** 			if sk = #40 then*/
    if (binary_op_a(NOTEQ, _sk_14792, 64)){
        goto L8; // [140] 259
    }

    /** 				sk = seek(fh, #3C) -- Fixed location of COFF signature offset.*/
    _0 = _sk_14792;
    _sk_14792 = _16seek(_fh_14772, 60);
    DeRef(_0);

    /** 				sk = get_integer32(fh)*/
    _0 = _sk_14792;
    _sk_14792 = _16get_integer32(_fh_14772);
    DeRef(_0);

    /** 				sk = seek(fh, sk)*/
    Ref(_sk_14792);
    _0 = _sk_14792;
    _sk_14792 = _16seek(_fh_14772, _sk_14792);
    DeRef(_0);

    /** 				sk = get_integer16(fh)*/
    _0 = _sk_14792;
    _sk_14792 = _16get_integer16(_fh_14772);
    DeRef(_0);

    /** 				if sk = #4550 then -- "PE" in intel endian*/
    if (binary_op_a(NOTEQ, _sk_14792, 17744)){
        goto L9; // [172] 221
    }

    /** 					sk = get_integer16(fh)*/
    _0 = _sk_14792;
    _sk_14792 = _16get_integer16(_fh_14772);
    DeRef(_0);

    /** 					if sk = 0 then*/
    if (binary_op_a(NOTEQ, _sk_14792, 0)){
        goto LA; // [184] 212
    }

    /** 						sk = seek(fh, where(fh) + 88 )*/
    _8238 = _16where(_fh_14772);
    if (IS_ATOM_INT(_8238)) {
        _8239 = _8238 + 88;
        if ((long)((unsigned long)_8239 + (unsigned long)HIGH_BITS) >= 0) 
        _8239 = NewDouble((double)_8239);
    }
    else {
        _8239 = binary_op(PLUS, _8238, 88);
    }
    DeRef(_8238);
    _8238 = NOVALUE;
    _0 = _sk_14792;
    _sk_14792 = _16seek(_fh_14772, _8239);
    DeRef(_0);
    _8239 = NOVALUE;

    /** 						sk = get_integer16(fh)*/
    _0 = _sk_14792;
    _sk_14792 = _16get_integer16(_fh_14772);
    DeRef(_0);
    goto LB; // [209] 265
LA: 

    /** 						sk = 0	-- Don't know this format.*/
    DeRef(_sk_14792);
    _sk_14792 = 0;
    goto LB; // [218] 265
L9: 

    /** 				elsif sk = #454E then -- "NE" in intel endian*/
    if (binary_op_a(NOTEQ, _sk_14792, 17742)){
        goto LC; // [223] 250
    }

    /** 					sk = seek(fh, where(fh) + 54 )*/
    _8244 = _16where(_fh_14772);
    if (IS_ATOM_INT(_8244)) {
        _8245 = _8244 + 54;
        if ((long)((unsigned long)_8245 + (unsigned long)HIGH_BITS) >= 0) 
        _8245 = NewDouble((double)_8245);
    }
    else {
        _8245 = binary_op(PLUS, _8244, 54);
    }
    DeRef(_8244);
    _8244 = NOVALUE;
    _0 = _sk_14792;
    _sk_14792 = _16seek(_fh_14772, _8245);
    DeRef(_0);
    _8245 = NOVALUE;

    /** 					sk = getc(fh)*/
    DeRef(_sk_14792);
    if (_fh_14772 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_14772, EF_READ);
        last_r_file_no = _fh_14772;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _sk_14792 = getKBchar();
        }
        else
        _sk_14792 = getc(last_r_file_ptr);
    }
    else
    _sk_14792 = getc(last_r_file_ptr);
    goto LB; // [247] 265
LC: 

    /** 					sk = 0*/
    DeRef(_sk_14792);
    _sk_14792 = 0;
    goto LB; // [256] 265
L8: 

    /** 				sk = 0*/
    DeRef(_sk_14792);
    _sk_14792 = 0;
LB: 

    /** 			if sk = 2 then*/
    if (binary_op_a(NOTEQ, _sk_14792, 2)){
        goto LD; // [267] 284
    }

    /** 				local_defines &= { "GUI" }*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_8221);
    *((int *)(_2+4)) = _8221;
    _8249 = MAKE_SEQ(_1);
    Concat((object_ptr)&_local_defines_14760, _local_defines_14760, _8249);
    DeRefDS(_8249);
    _8249 = NOVALUE;
    goto LE; // [281] 314
LD: 

    /** 			elsif sk = 3 then*/
    if (binary_op_a(NOTEQ, _sk_14792, 3)){
        goto LF; // [286] 303
    }

    /** 				local_defines &= { "CONSOLE" }*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_8224);
    *((int *)(_2+4)) = _8224;
    _8252 = MAKE_SEQ(_1);
    Concat((object_ptr)&_local_defines_14760, _local_defines_14760, _8252);
    DeRefDS(_8252);
    _8252 = NOVALUE;
    goto LE; // [300] 314
LF: 

    /** 				local_defines &= { "UNKNOWN" }*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_6267);
    *((int *)(_2+4)) = _6267;
    _8254 = MAKE_SEQ(_1);
    Concat((object_ptr)&_local_defines_14760, _local_defines_14760, _8254);
    DeRefDS(_8254);
    _8254 = NOVALUE;
LE: 

    /** 			close(fh)*/
    EClose(_fh_14772);
    DeRef(_sk_14792);
    _sk_14792 = NOVALUE;
L7: 
    DeRef(_lcmds_14770);
    _lcmds_14770 = NOVALUE;
    goto L10; // [323] 575
L4: 

    /** 		local_defines = append( local_defines, "CONSOLE" )*/
    RefDS(_8224);
    Append(&_local_defines_14760, _local_defines_14760, _8224);

    /** 		if (ILINUX and not for_translator) or (TLINUX and for_translator) then*/
    if (_36ILINUX_14722 == 0) {
        _8257 = 0;
        goto L11; // [336] 347
    }
    _8258 = (_for_translator_14759 == 0);
    _8257 = (_8258 != 0);
L11: 
    if (_8257 != 0) {
        goto L12; // [347] 366
    }
    if (_36TLINUX_14723 == 0) {
        DeRef(_8260);
        _8260 = 0;
        goto L13; // [353] 361
    }
    _8260 = (_for_translator_14759 != 0);
L13: 
    if (_8260 == 0)
    {
        _8260 = NOVALUE;
        goto L14; // [362] 379
    }
    else{
        _8260 = NOVALUE;
    }
L12: 

    /** 			local_defines &= {"UNIX", "LINUX"}*/
    RefDS(_8261);
    RefDS(_6631);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6631;
    ((int *)_2)[2] = _8261;
    _8262 = MAKE_SEQ(_1);
    Concat((object_ptr)&_local_defines_14760, _local_defines_14760, _8262);
    DeRefDS(_8262);
    _8262 = NOVALUE;
    goto L15; // [376] 574
L14: 

    /** 		elsif (IOSX and not for_translator) or (TOSX and for_translator) then*/
    if (_36IOSX_14728 == 0) {
        _8264 = 0;
        goto L16; // [383] 394
    }
    _8265 = (_for_translator_14759 == 0);
    _8264 = (_8265 != 0);
L16: 
    if (_8264 != 0) {
        goto L17; // [394] 413
    }
    if (_36TOSX_14729 == 0) {
        DeRef(_8267);
        _8267 = 0;
        goto L18; // [400] 408
    }
    _8267 = (_for_translator_14759 != 0);
L18: 
    if (_8267 == 0)
    {
        _8267 = NOVALUE;
        goto L19; // [409] 428
    }
    else{
        _8267 = NOVALUE;
    }
L17: 

    /** 			local_defines &= {"UNIX", "BSD", "OSX"}*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_6631);
    *((int *)(_2+4)) = _6631;
    RefDS(_8268);
    *((int *)(_2+8)) = _8268;
    RefDS(_8269);
    *((int *)(_2+12)) = _8269;
    _8270 = MAKE_SEQ(_1);
    Concat((object_ptr)&_local_defines_14760, _local_defines_14760, _8270);
    DeRefDS(_8270);
    _8270 = NOVALUE;
    goto L15; // [425] 574
L19: 

    /** 		elsif (IOPENBSD and not for_translator) or (TOPENBSD and for_translator) then*/
    if (_36IOPENBSD_14730 == 0) {
        _8272 = 0;
        goto L1A; // [432] 443
    }
    _8273 = (_for_translator_14759 == 0);
    _8272 = (_8273 != 0);
L1A: 
    if (_8272 != 0) {
        goto L1B; // [443] 462
    }
    if (_36TOPENBSD_14731 == 0) {
        DeRef(_8275);
        _8275 = 0;
        goto L1C; // [449] 457
    }
    _8275 = (_for_translator_14759 != 0);
L1C: 
    if (_8275 == 0)
    {
        _8275 = NOVALUE;
        goto L1D; // [458] 477
    }
    else{
        _8275 = NOVALUE;
    }
L1B: 

    /** 			local_defines &= { "UNIX", "BSD", "OPENBSD"}*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_6631);
    *((int *)(_2+4)) = _6631;
    RefDS(_8268);
    *((int *)(_2+8)) = _8268;
    RefDS(_8276);
    *((int *)(_2+12)) = _8276;
    _8277 = MAKE_SEQ(_1);
    Concat((object_ptr)&_local_defines_14760, _local_defines_14760, _8277);
    DeRefDS(_8277);
    _8277 = NOVALUE;
    goto L15; // [474] 574
L1D: 

    /** 		elsif (INETBSD and not for_translator) or (TNETBSD and for_translator) then*/
    if (_36INETBSD_14732 == 0) {
        _8279 = 0;
        goto L1E; // [481] 492
    }
    _8280 = (_for_translator_14759 == 0);
    _8279 = (_8280 != 0);
L1E: 
    if (_8279 != 0) {
        goto L1F; // [492] 511
    }
    if (_36TNETBSD_14733 == 0) {
        DeRef(_8282);
        _8282 = 0;
        goto L20; // [498] 506
    }
    _8282 = (_for_translator_14759 != 0);
L20: 
    if (_8282 == 0)
    {
        _8282 = NOVALUE;
        goto L21; // [507] 526
    }
    else{
        _8282 = NOVALUE;
    }
L1F: 

    /** 			local_defines &= { "UNIX", "BSD", "NETBSD"}*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_6631);
    *((int *)(_2+4)) = _6631;
    RefDS(_8268);
    *((int *)(_2+8)) = _8268;
    RefDS(_8283);
    *((int *)(_2+12)) = _8283;
    _8284 = MAKE_SEQ(_1);
    Concat((object_ptr)&_local_defines_14760, _local_defines_14760, _8284);
    DeRefDS(_8284);
    _8284 = NOVALUE;
    goto L15; // [523] 574
L21: 

    /** 		elsif (IBSD and not for_translator) or (TBSD and for_translator) then*/
    if (_36IBSD_14726 == 0) {
        _8286 = 0;
        goto L22; // [530] 541
    }
    _8287 = (_for_translator_14759 == 0);
    _8286 = (_8287 != 0);
L22: 
    if (_8286 != 0) {
        goto L23; // [541] 560
    }
    if (_36TBSD_14727 == 0) {
        DeRef(_8289);
        _8289 = 0;
        goto L24; // [547] 555
    }
    _8289 = (_for_translator_14759 != 0);
L24: 
    if (_8289 == 0)
    {
        _8289 = NOVALUE;
        goto L25; // [556] 573
    }
    else{
        _8289 = NOVALUE;
    }
L23: 

    /** 			local_defines &= {"UNIX", "BSD", "FREEBSD"}*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_6631);
    *((int *)(_2+4)) = _6631;
    RefDS(_8268);
    *((int *)(_2+8)) = _8268;
    RefDS(_8290);
    *((int *)(_2+12)) = _8290;
    _8291 = MAKE_SEQ(_1);
    Concat((object_ptr)&_local_defines_14760, _local_defines_14760, _8291);
    DeRefDS(_8291);
    _8291 = NOVALUE;
L25: 
L15: 
L10: 

    /** 	return { "_PLAT_START" } & local_defines & { "_PLAT_STOP" }*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_8293);
    *((int *)(_2+4)) = _8293;
    _8294 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_8295);
    *((int *)(_2+4)) = _8295;
    _8296 = MAKE_SEQ(_1);
    {
        int concat_list[3];

        concat_list[0] = _8296;
        concat_list[1] = _local_defines_14760;
        concat_list[2] = _8294;
        Concat_N((object_ptr)&_8297, concat_list, 3);
    }
    DeRefDS(_8296);
    _8296 = NOVALUE;
    DeRefDS(_8294);
    _8294 = NOVALUE;
    DeRefDS(_local_defines_14760);
    DeRef(_8205);
    _8205 = NOVALUE;
    DeRef(_8258);
    _8258 = NOVALUE;
    DeRef(_8265);
    _8265 = NOVALUE;
    DeRef(_8273);
    _8273 = NOVALUE;
    DeRef(_8280);
    _8280 = NOVALUE;
    DeRef(_8287);
    _8287 = NOVALUE;
    return _8297;
    ;
}



// 0x77CE2FD9
