// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _5char_test(int _test_data_289, int _char_set_290)
{
    int _lChr_291 = NOVALUE;
    int _87 = NOVALUE;
    int _86 = NOVALUE;
    int _85 = NOVALUE;
    int _84 = NOVALUE;
    int _83 = NOVALUE;
    int _82 = NOVALUE;
    int _81 = NOVALUE;
    int _80 = NOVALUE;
    int _79 = NOVALUE;
    int _78 = NOVALUE;
    int _77 = NOVALUE;
    int _74 = NOVALUE;
    int _73 = NOVALUE;
    int _72 = NOVALUE;
    int _71 = NOVALUE;
    int _69 = NOVALUE;
    int _67 = NOVALUE;
    int _66 = NOVALUE;
    int _65 = NOVALUE;
    int _64 = NOVALUE;
    int _63 = NOVALUE;
    int _62 = NOVALUE;
    int _61 = NOVALUE;
    int _60 = NOVALUE;
    int _59 = NOVALUE;
    int _58 = NOVALUE;
    int _57 = NOVALUE;
    int _56 = NOVALUE;
    int _55 = NOVALUE;
    int _54 = NOVALUE;
    int _53 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(test_data) then*/
    if (IS_ATOM_INT(_test_data_289))
    _53 = 1;
    else if (IS_ATOM_DBL(_test_data_289))
    _53 = IS_ATOM_INT(DoubleToInt(_test_data_289));
    else
    _53 = 0;
    if (_53 == 0)
    {
        _53 = NOVALUE;
        goto L1; // [8] 115
    }
    else{
        _53 = NOVALUE;
    }

    /** 		if sequence(char_set[1]) then*/
    _2 = (int)SEQ_PTR(_char_set_290);
    _54 = (int)*(((s1_ptr)_2)->base + 1);
    _55 = IS_SEQUENCE(_54);
    _54 = NOVALUE;
    if (_55 == 0)
    {
        _55 = NOVALUE;
        goto L2; // [20] 96
    }
    else{
        _55 = NOVALUE;
    }

    /** 			for j = 1 to length(char_set) do*/
    if (IS_SEQUENCE(_char_set_290)){
            _56 = SEQ_PTR(_char_set_290)->length;
    }
    else {
        _56 = 1;
    }
    {
        int _j_298;
        _j_298 = 1;
L3: 
        if (_j_298 > _56){
            goto L4; // [28] 85
        }

        /** 				if test_data >= char_set[j][1] and test_data <= char_set[j][2] then */
        _2 = (int)SEQ_PTR(_char_set_290);
        _57 = (int)*(((s1_ptr)_2)->base + _j_298);
        _2 = (int)SEQ_PTR(_57);
        _58 = (int)*(((s1_ptr)_2)->base + 1);
        _57 = NOVALUE;
        if (IS_ATOM_INT(_test_data_289) && IS_ATOM_INT(_58)) {
            _59 = (_test_data_289 >= _58);
        }
        else {
            _59 = binary_op(GREATEREQ, _test_data_289, _58);
        }
        _58 = NOVALUE;
        if (IS_ATOM_INT(_59)) {
            if (_59 == 0) {
                goto L5; // [49] 78
            }
        }
        else {
            if (DBL_PTR(_59)->dbl == 0.0) {
                goto L5; // [49] 78
            }
        }
        _2 = (int)SEQ_PTR(_char_set_290);
        _61 = (int)*(((s1_ptr)_2)->base + _j_298);
        _2 = (int)SEQ_PTR(_61);
        _62 = (int)*(((s1_ptr)_2)->base + 2);
        _61 = NOVALUE;
        if (IS_ATOM_INT(_test_data_289) && IS_ATOM_INT(_62)) {
            _63 = (_test_data_289 <= _62);
        }
        else {
            _63 = binary_op(LESSEQ, _test_data_289, _62);
        }
        _62 = NOVALUE;
        if (_63 == 0) {
            DeRef(_63);
            _63 = NOVALUE;
            goto L5; // [66] 78
        }
        else {
            if (!IS_ATOM_INT(_63) && DBL_PTR(_63)->dbl == 0.0){
                DeRef(_63);
                _63 = NOVALUE;
                goto L5; // [66] 78
            }
            DeRef(_63);
            _63 = NOVALUE;
        }
        DeRef(_63);
        _63 = NOVALUE;

        /** 					return TRUE */
        DeRef(_test_data_289);
        DeRefDS(_char_set_290);
        DeRef(_59);
        _59 = NOVALUE;
        return _5TRUE_244;
L5: 

        /** 			end for*/
        _j_298 = _j_298 + 1;
        goto L3; // [80] 35
L4: 
        ;
    }

    /** 			return FALSE*/
    DeRef(_test_data_289);
    DeRefDS(_char_set_290);
    DeRef(_59);
    _59 = NOVALUE;
    return _5FALSE_242;
    goto L6; // [93] 328
L2: 

    /** 			return find(test_data, char_set) > 0*/
    _64 = find_from(_test_data_289, _char_set_290, 1);
    _65 = (_64 > 0);
    _64 = NOVALUE;
    DeRef(_test_data_289);
    DeRefDS(_char_set_290);
    DeRef(_59);
    _59 = NOVALUE;
    return _65;
    goto L6; // [112] 328
L1: 

    /** 	elsif sequence(test_data) then*/
    _66 = IS_SEQUENCE(_test_data_289);
    if (_66 == 0)
    {
        _66 = NOVALUE;
        goto L7; // [120] 319
    }
    else{
        _66 = NOVALUE;
    }

    /** 		if length(test_data) = 0 then */
    if (IS_SEQUENCE(_test_data_289)){
            _67 = SEQ_PTR(_test_data_289)->length;
    }
    else {
        _67 = 1;
    }
    if (_67 != 0)
    goto L8; // [128] 141

    /** 			return FALSE */
    DeRef(_test_data_289);
    DeRefDS(_char_set_290);
    DeRef(_65);
    _65 = NOVALUE;
    DeRef(_59);
    _59 = NOVALUE;
    return _5FALSE_242;
L8: 

    /** 		for i = 1 to length(test_data) label "NXTCHR" do*/
    if (IS_SEQUENCE(_test_data_289)){
            _69 = SEQ_PTR(_test_data_289)->length;
    }
    else {
        _69 = 1;
    }
    {
        int _i_317;
        _i_317 = 1;
L9: 
        if (_i_317 > _69){
            goto LA; // [146] 308
        }

        /** 			if sequence(test_data[i]) then */
        _2 = (int)SEQ_PTR(_test_data_289);
        _71 = (int)*(((s1_ptr)_2)->base + _i_317);
        _72 = IS_SEQUENCE(_71);
        _71 = NOVALUE;
        if (_72 == 0)
        {
            _72 = NOVALUE;
            goto LB; // [162] 174
        }
        else{
            _72 = NOVALUE;
        }

        /** 				return FALSE*/
        DeRef(_test_data_289);
        DeRefDS(_char_set_290);
        DeRef(_65);
        _65 = NOVALUE;
        DeRef(_59);
        _59 = NOVALUE;
        return _5FALSE_242;
LB: 

        /** 			if not integer(test_data[i]) then */
        _2 = (int)SEQ_PTR(_test_data_289);
        _73 = (int)*(((s1_ptr)_2)->base + _i_317);
        if (IS_ATOM_INT(_73))
        _74 = 1;
        else if (IS_ATOM_DBL(_73))
        _74 = IS_ATOM_INT(DoubleToInt(_73));
        else
        _74 = 0;
        _73 = NOVALUE;
        if (_74 != 0)
        goto LC; // [183] 195
        _74 = NOVALUE;

        /** 				return FALSE*/
        DeRef(_test_data_289);
        DeRefDS(_char_set_290);
        DeRef(_65);
        _65 = NOVALUE;
        DeRef(_59);
        _59 = NOVALUE;
        return _5FALSE_242;
LC: 

        /** 			lChr = test_data[i]*/
        _2 = (int)SEQ_PTR(_test_data_289);
        _lChr_291 = (int)*(((s1_ptr)_2)->base + _i_317);
        if (!IS_ATOM_INT(_lChr_291)){
            _lChr_291 = (long)DBL_PTR(_lChr_291)->dbl;
        }

        /** 			if sequence(char_set[1]) then*/
        _2 = (int)SEQ_PTR(_char_set_290);
        _77 = (int)*(((s1_ptr)_2)->base + 1);
        _78 = IS_SEQUENCE(_77);
        _77 = NOVALUE;
        if (_78 == 0)
        {
            _78 = NOVALUE;
            goto LD; // [212] 276
        }
        else{
            _78 = NOVALUE;
        }

        /** 				for j = 1 to length(char_set) do*/
        if (IS_SEQUENCE(_char_set_290)){
                _79 = SEQ_PTR(_char_set_290)->length;
        }
        else {
            _79 = 1;
        }
        {
            int _j_332;
            _j_332 = 1;
LE: 
            if (_j_332 > _79){
                goto LF; // [220] 273
            }

            /** 					if lChr >= char_set[j][1] and lChr <= char_set[j][2] then*/
            _2 = (int)SEQ_PTR(_char_set_290);
            _80 = (int)*(((s1_ptr)_2)->base + _j_332);
            _2 = (int)SEQ_PTR(_80);
            _81 = (int)*(((s1_ptr)_2)->base + 1);
            _80 = NOVALUE;
            if (IS_ATOM_INT(_81)) {
                _82 = (_lChr_291 >= _81);
            }
            else {
                _82 = binary_op(GREATEREQ, _lChr_291, _81);
            }
            _81 = NOVALUE;
            if (IS_ATOM_INT(_82)) {
                if (_82 == 0) {
                    goto L10; // [241] 266
                }
            }
            else {
                if (DBL_PTR(_82)->dbl == 0.0) {
                    goto L10; // [241] 266
                }
            }
            _2 = (int)SEQ_PTR(_char_set_290);
            _84 = (int)*(((s1_ptr)_2)->base + _j_332);
            _2 = (int)SEQ_PTR(_84);
            _85 = (int)*(((s1_ptr)_2)->base + 2);
            _84 = NOVALUE;
            if (IS_ATOM_INT(_85)) {
                _86 = (_lChr_291 <= _85);
            }
            else {
                _86 = binary_op(LESSEQ, _lChr_291, _85);
            }
            _85 = NOVALUE;
            if (_86 == 0) {
                DeRef(_86);
                _86 = NOVALUE;
                goto L10; // [258] 266
            }
            else {
                if (!IS_ATOM_INT(_86) && DBL_PTR(_86)->dbl == 0.0){
                    DeRef(_86);
                    _86 = NOVALUE;
                    goto L10; // [258] 266
                }
                DeRef(_86);
                _86 = NOVALUE;
            }
            DeRef(_86);
            _86 = NOVALUE;

            /** 						continue "NXTCHR" */
            goto L11; // [263] 303
L10: 

            /** 				end for*/
            _j_332 = _j_332 + 1;
            goto LE; // [268] 227
LF: 
            ;
        }
        goto L12; // [273] 293
LD: 

        /** 				if find(lChr, char_set) > 0 then*/
        _87 = find_from(_lChr_291, _char_set_290, 1);
        if (_87 <= 0)
        goto L13; // [283] 292

        /** 					continue "NXTCHR"*/
        goto L11; // [289] 303
L13: 
L12: 

        /** 			return FALSE*/
        DeRef(_test_data_289);
        DeRefDS(_char_set_290);
        DeRef(_65);
        _65 = NOVALUE;
        DeRef(_59);
        _59 = NOVALUE;
        DeRef(_82);
        _82 = NOVALUE;
        return _5FALSE_242;

        /** 		end for*/
L11: 
        _i_317 = _i_317 + 1;
        goto L9; // [303] 153
LA: 
        ;
    }

    /** 		return TRUE*/
    DeRef(_test_data_289);
    DeRefDS(_char_set_290);
    DeRef(_65);
    _65 = NOVALUE;
    DeRef(_59);
    _59 = NOVALUE;
    DeRef(_82);
    _82 = NOVALUE;
    return _5TRUE_244;
    goto L6; // [316] 328
L7: 

    /** 		return FALSE*/
    DeRef(_test_data_289);
    DeRefDS(_char_set_290);
    DeRef(_65);
    _65 = NOVALUE;
    DeRef(_59);
    _59 = NOVALUE;
    DeRef(_82);
    _82 = NOVALUE;
    return _5FALSE_242;
L6: 
    ;
}


void _5set_default_charsets()
{
    int _158 = NOVALUE;
    int _156 = NOVALUE;
    int _155 = NOVALUE;
    int _153 = NOVALUE;
    int _150 = NOVALUE;
    int _149 = NOVALUE;
    int _148 = NOVALUE;
    int _147 = NOVALUE;
    int _144 = NOVALUE;
    int _142 = NOVALUE;
    int _131 = NOVALUE;
    int _124 = NOVALUE;
    int _120 = NOVALUE;
    int _111 = NOVALUE;
    int _107 = NOVALUE;
    int _106 = NOVALUE;
    int _105 = NOVALUE;
    int _102 = NOVALUE;
    int _98 = NOVALUE;
    int _90 = NOVALUE;
    int _89 = NOVALUE;
    int _0, _1, _2;
    

    /** 	Defined_Sets = repeat(0, CS_LAST - CS_FIRST - 1)*/
    _89 = 20;
    _90 = 19;
    _89 = NOVALUE;
    DeRef(_5Defined_Sets_347);
    _5Defined_Sets_347 = Repeat(0, 19);
    _90 = NOVALUE;

    /** 	Defined_Sets[CS_Alphabetic	] = {{'a', 'z'}, {'A', 'Z'}}*/
    RefDS(_97);
    RefDS(_94);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _94;
    ((int *)_2)[2] = _97;
    _98 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _2 = (int)(((s1_ptr)_2)->base + 12);
    *(int *)_2 = _98;
    if( _1 != _98 ){
    }
    _98 = NOVALUE;

    /** 	Defined_Sets[CS_Alphanumeric] = {{'0', '9'}, {'a', 'z'}, {'A', 'Z'}}*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_101);
    *((int *)(_2+4)) = _101;
    RefDS(_94);
    *((int *)(_2+8)) = _94;
    RefDS(_97);
    *((int *)(_2+12)) = _97;
    _102 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _2 = (int)(((s1_ptr)_2)->base + 10);
    _1 = *(int *)_2;
    *(int *)_2 = _102;
    if( _1 != _102 ){
        DeRef(_1);
    }
    _102 = NOVALUE;

    /** 	Defined_Sets[CS_Identifier]   = {{'0', '9'}, {'a', 'z'}, {'A', 'Z'}, {'_', '_'}}*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_101);
    *((int *)(_2+4)) = _101;
    RefDS(_94);
    *((int *)(_2+8)) = _94;
    RefDS(_97);
    *((int *)(_2+12)) = _97;
    RefDS(_104);
    *((int *)(_2+16)) = _104;
    _105 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _2 = (int)(((s1_ptr)_2)->base + 11);
    _1 = *(int *)_2;
    *(int *)_2 = _105;
    if( _1 != _105 ){
        DeRef(_1);
    }
    _105 = NOVALUE;

    /** 	Defined_Sets[CS_Uppercase 	] = {{'A', 'Z'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_97);
    *((int *)(_2+4)) = _97;
    _106 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _2 = (int)(((s1_ptr)_2)->base + 9);
    _1 = *(int *)_2;
    *(int *)_2 = _106;
    if( _1 != _106 ){
        DeRef(_1);
    }
    _106 = NOVALUE;

    /** 	Defined_Sets[CS_Lowercase 	] = {{'a', 'z'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_94);
    *((int *)(_2+4)) = _94;
    _107 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _2 = (int)(((s1_ptr)_2)->base + 8);
    _1 = *(int *)_2;
    *(int *)_2 = _107;
    if( _1 != _107 ){
        DeRef(_1);
    }
    _107 = NOVALUE;

    /** 	Defined_Sets[CS_Printable 	] = {{' ', '~'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_110);
    *((int *)(_2+4)) = _110;
    _111 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _111;
    if( _1 != _111 ){
        DeRef(_1);
    }
    _111 = NOVALUE;

    /** 	Defined_Sets[CS_Displayable ] = {{' ', '~'}, "  ", "\t\t", "\n\n", "\r\r", {8,8}, {7,7} }*/
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_110);
    *((int *)(_2+4)) = _110;
    RefDS(_112);
    *((int *)(_2+8)) = _112;
    RefDS(_113);
    *((int *)(_2+12)) = _113;
    RefDS(_114);
    *((int *)(_2+16)) = _114;
    RefDS(_115);
    *((int *)(_2+20)) = _115;
    RefDS(_117);
    *((int *)(_2+24)) = _117;
    RefDS(_119);
    *((int *)(_2+28)) = _119;
    _120 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _2 = (int)(((s1_ptr)_2)->base + 7);
    _1 = *(int *)_2;
    *(int *)_2 = _120;
    if( _1 != _120 ){
        DeRef(_1);
    }
    _120 = NOVALUE;

    /** 	Defined_Sets[CS_Whitespace 	] = " \t\n\r" & 11 & 160*/
    {
        int concat_list[3];

        concat_list[0] = 160;
        concat_list[1] = 11;
        concat_list[2] = _121;
        Concat_N((object_ptr)&_124, concat_list, 3);
    }
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _124;
    if( _1 != _124 ){
        DeRef(_1);
    }
    _124 = NOVALUE;

    /** 	Defined_Sets[CS_Consonant 	] = "bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ"*/
    RefDS(_125);
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _125;
    DeRef(_1);

    /** 	Defined_Sets[CS_Vowel 		] = "aeiouAEIOU"*/
    RefDS(_126);
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _126;
    DeRef(_1);

    /** 	Defined_Sets[CS_Hexadecimal ] = {{'0', '9'}, {'A', 'F'},{'a', 'f'}}*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_101);
    *((int *)(_2+4)) = _101;
    RefDS(_128);
    *((int *)(_2+8)) = _128;
    RefDS(_130);
    *((int *)(_2+12)) = _130;
    _131 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _131;
    if( _1 != _131 ){
        DeRef(_1);
    }
    _131 = NOVALUE;

    /** 	Defined_Sets[CS_Punctuation ] = {{' ', '/'}, {':', '?'}, {'[', '`'}, {'{', '~'}}*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_133);
    *((int *)(_2+4)) = _133;
    RefDS(_136);
    *((int *)(_2+8)) = _136;
    RefDS(_139);
    *((int *)(_2+12)) = _139;
    RefDS(_141);
    *((int *)(_2+16)) = _141;
    _142 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _142;
    if( _1 != _142 ){
        DeRef(_1);
    }
    _142 = NOVALUE;

    /** 	Defined_Sets[CS_Control 	] = {{0, 31}, {127, 127}}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 31;
    _144 = MAKE_SEQ(_1);
    RefDS(_146);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _144;
    ((int *)_2)[2] = _146;
    _147 = MAKE_SEQ(_1);
    _144 = NOVALUE;
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _2 = (int)(((s1_ptr)_2)->base + 14);
    _1 = *(int *)_2;
    *(int *)_2 = _147;
    if( _1 != _147 ){
        DeRef(_1);
    }
    _147 = NOVALUE;

    /** 	Defined_Sets[CS_ASCII 		] = {{0, 127}}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 127;
    _148 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _148;
    _149 = MAKE_SEQ(_1);
    _148 = NOVALUE;
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _2 = (int)(((s1_ptr)_2)->base + 13);
    _1 = *(int *)_2;
    *(int *)_2 = _149;
    if( _1 != _149 ){
        DeRef(_1);
    }
    _149 = NOVALUE;

    /** 	Defined_Sets[CS_Digit 		] = {{'0', '9'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_101);
    *((int *)(_2+4)) = _101;
    _150 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _2 = (int)(((s1_ptr)_2)->base + 15);
    _1 = *(int *)_2;
    *(int *)_2 = _150;
    if( _1 != _150 ){
        DeRef(_1);
    }
    _150 = NOVALUE;

    /** 	Defined_Sets[CS_Graphic 	] = {{'!', '~'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_152);
    *((int *)(_2+4)) = _152;
    _153 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _2 = (int)(((s1_ptr)_2)->base + 16);
    _1 = *(int *)_2;
    *(int *)_2 = _153;
    if( _1 != _153 ){
        DeRef(_1);
    }
    _153 = NOVALUE;

    /** 	Defined_Sets[CS_Bytes	 	] = {{0, 255}}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 255;
    _155 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _155;
    _156 = MAKE_SEQ(_1);
    _155 = NOVALUE;
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _2 = (int)(((s1_ptr)_2)->base + 17);
    _1 = *(int *)_2;
    *(int *)_2 = _156;
    if( _1 != _156 ){
        DeRef(_1);
    }
    _156 = NOVALUE;

    /** 	Defined_Sets[CS_SpecWord 	] = "_"*/
    RefDS(_157);
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _2 = (int)(((s1_ptr)_2)->base + 18);
    _1 = *(int *)_2;
    *(int *)_2 = _157;
    DeRef(_1);

    /** 	Defined_Sets[CS_Boolean     ] = {TRUE,FALSE}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5TRUE_244;
    ((int *)_2)[2] = _5FALSE_242;
    _158 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _2 = (int)(((s1_ptr)_2)->base + 19);
    _1 = *(int *)_2;
    *(int *)_2 = _158;
    if( _1 != _158 ){
        DeRef(_1);
    }
    _158 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


int _5get_charsets()
{
    int _result__422 = NOVALUE;
    int _162 = NOVALUE;
    int _161 = NOVALUE;
    int _160 = NOVALUE;
    int _159 = NOVALUE;
    int _0, _1, _2;
    

    /** 	result_ = {}*/
    RefDS(_5);
    DeRef(_result__422);
    _result__422 = _5;

    /** 	for i = CS_FIRST + 1 to CS_LAST - 1 do*/
    _159 = 1;
    _160 = 19;
    {
        int _i_424;
        _i_424 = 1;
L1: 
        if (_i_424 > 19){
            goto L2; // [22] 52
        }

        /** 		result_ = append(result_, {i, Defined_Sets[i]} )*/
        _2 = (int)SEQ_PTR(_5Defined_Sets_347);
        _161 = (int)*(((s1_ptr)_2)->base + _i_424);
        Ref(_161);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _i_424;
        ((int *)_2)[2] = _161;
        _162 = MAKE_SEQ(_1);
        _161 = NOVALUE;
        RefDS(_162);
        Append(&_result__422, _result__422, _162);
        DeRefDS(_162);
        _162 = NOVALUE;

        /** 	end for*/
        _i_424 = _i_424 + 1;
        goto L1; // [47] 29
L2: 
        ;
    }

    /** 	return result_*/
    DeRef(_159);
    _159 = NOVALUE;
    DeRef(_160);
    _160 = NOVALUE;
    return _result__422;
    ;
}


void _5set_charsets(int _charset_list_432)
{
    int _185 = NOVALUE;
    int _184 = NOVALUE;
    int _183 = NOVALUE;
    int _182 = NOVALUE;
    int _181 = NOVALUE;
    int _180 = NOVALUE;
    int _179 = NOVALUE;
    int _178 = NOVALUE;
    int _177 = NOVALUE;
    int _176 = NOVALUE;
    int _175 = NOVALUE;
    int _174 = NOVALUE;
    int _173 = NOVALUE;
    int _172 = NOVALUE;
    int _171 = NOVALUE;
    int _170 = NOVALUE;
    int _169 = NOVALUE;
    int _168 = NOVALUE;
    int _167 = NOVALUE;
    int _166 = NOVALUE;
    int _165 = NOVALUE;
    int _164 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(charset_list) do*/
    if (IS_SEQUENCE(_charset_list_432)){
            _164 = SEQ_PTR(_charset_list_432)->length;
    }
    else {
        _164 = 1;
    }
    {
        int _i_434;
        _i_434 = 1;
L1: 
        if (_i_434 > _164){
            goto L2; // [8] 133
        }

        /** 		if sequence(charset_list[i]) and length(charset_list[i]) = 2 then*/
        _2 = (int)SEQ_PTR(_charset_list_432);
        _165 = (int)*(((s1_ptr)_2)->base + _i_434);
        _166 = IS_SEQUENCE(_165);
        _165 = NOVALUE;
        if (_166 == 0) {
            goto L3; // [24] 126
        }
        _2 = (int)SEQ_PTR(_charset_list_432);
        _168 = (int)*(((s1_ptr)_2)->base + _i_434);
        if (IS_SEQUENCE(_168)){
                _169 = SEQ_PTR(_168)->length;
        }
        else {
            _169 = 1;
        }
        _168 = NOVALUE;
        _170 = (_169 == 2);
        _169 = NOVALUE;
        if (_170 == 0)
        {
            DeRef(_170);
            _170 = NOVALUE;
            goto L3; // [40] 126
        }
        else{
            DeRef(_170);
            _170 = NOVALUE;
        }

        /** 			if integer(charset_list[i][1]) and charset_list[i][1] > CS_FIRST and charset_list[i][1] < CS_LAST then*/
        _2 = (int)SEQ_PTR(_charset_list_432);
        _171 = (int)*(((s1_ptr)_2)->base + _i_434);
        _2 = (int)SEQ_PTR(_171);
        _172 = (int)*(((s1_ptr)_2)->base + 1);
        _171 = NOVALUE;
        if (IS_ATOM_INT(_172))
        _173 = 1;
        else if (IS_ATOM_DBL(_172))
        _173 = IS_ATOM_INT(DoubleToInt(_172));
        else
        _173 = 0;
        _172 = NOVALUE;
        if (_173 == 0) {
            _174 = 0;
            goto L4; // [56] 78
        }
        _2 = (int)SEQ_PTR(_charset_list_432);
        _175 = (int)*(((s1_ptr)_2)->base + _i_434);
        _2 = (int)SEQ_PTR(_175);
        _176 = (int)*(((s1_ptr)_2)->base + 1);
        _175 = NOVALUE;
        if (IS_ATOM_INT(_176)) {
            _177 = (_176 > 0);
        }
        else {
            _177 = binary_op(GREATER, _176, 0);
        }
        _176 = NOVALUE;
        if (IS_ATOM_INT(_177))
        _174 = (_177 != 0);
        else
        _174 = DBL_PTR(_177)->dbl != 0.0;
L4: 
        if (_174 == 0) {
            goto L5; // [78] 125
        }
        _2 = (int)SEQ_PTR(_charset_list_432);
        _179 = (int)*(((s1_ptr)_2)->base + _i_434);
        _2 = (int)SEQ_PTR(_179);
        _180 = (int)*(((s1_ptr)_2)->base + 1);
        _179 = NOVALUE;
        if (IS_ATOM_INT(_180)) {
            _181 = (_180 < 20);
        }
        else {
            _181 = binary_op(LESS, _180, 20);
        }
        _180 = NOVALUE;
        if (_181 == 0) {
            DeRef(_181);
            _181 = NOVALUE;
            goto L5; // [97] 125
        }
        else {
            if (!IS_ATOM_INT(_181) && DBL_PTR(_181)->dbl == 0.0){
                DeRef(_181);
                _181 = NOVALUE;
                goto L5; // [97] 125
            }
            DeRef(_181);
            _181 = NOVALUE;
        }
        DeRef(_181);
        _181 = NOVALUE;

        /** 				Defined_Sets[charset_list[i][1]] = charset_list[i][2]*/
        _2 = (int)SEQ_PTR(_charset_list_432);
        _182 = (int)*(((s1_ptr)_2)->base + _i_434);
        _2 = (int)SEQ_PTR(_182);
        _183 = (int)*(((s1_ptr)_2)->base + 1);
        _182 = NOVALUE;
        _2 = (int)SEQ_PTR(_charset_list_432);
        _184 = (int)*(((s1_ptr)_2)->base + _i_434);
        _2 = (int)SEQ_PTR(_184);
        _185 = (int)*(((s1_ptr)_2)->base + 2);
        _184 = NOVALUE;
        Ref(_185);
        _2 = (int)SEQ_PTR(_5Defined_Sets_347);
        if (!IS_ATOM_INT(_183))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_183)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _183);
        _1 = *(int *)_2;
        *(int *)_2 = _185;
        if( _1 != _185 ){
            DeRef(_1);
        }
        _185 = NOVALUE;
L5: 
L3: 

        /** 	end for*/
        _i_434 = _i_434 + 1;
        goto L1; // [128] 15
L2: 
        ;
    }

    /** end procedure*/
    DeRefDS(_charset_list_432);
    _168 = NOVALUE;
    _183 = NOVALUE;
    DeRef(_177);
    _177 = NOVALUE;
    return;
    ;
}


int _5boolean(int _test_data_461)
{
    int _188 = NOVALUE;
    int _187 = NOVALUE;
    int _186 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return find(test_data,{1,0}) != 0*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _186 = MAKE_SEQ(_1);
    _187 = find_from(_test_data_461, _186, 1);
    DeRefDS(_186);
    _186 = NOVALUE;
    _188 = (_187 != 0);
    _187 = NOVALUE;
    DeRef(_test_data_461);
    return _188;
    ;
}


int _5t_boolean(int _test_data_467)
{
    int _190 = NOVALUE;
    int _189 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Boolean])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _189 = (int)*(((s1_ptr)_2)->base + 19);
    Ref(_test_data_467);
    Ref(_189);
    _190 = _5char_test(_test_data_467, _189);
    _189 = NOVALUE;
    DeRef(_test_data_467);
    return _190;
    ;
}


int _5t_alnum(int _test_data_472)
{
    int _192 = NOVALUE;
    int _191 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Alphanumeric])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _191 = (int)*(((s1_ptr)_2)->base + 10);
    Ref(_test_data_472);
    Ref(_191);
    _192 = _5char_test(_test_data_472, _191);
    _191 = NOVALUE;
    DeRef(_test_data_472);
    return _192;
    ;
}


int _5t_identifier(int _test_data_477)
{
    int _202 = NOVALUE;
    int _201 = NOVALUE;
    int _200 = NOVALUE;
    int _199 = NOVALUE;
    int _198 = NOVALUE;
    int _197 = NOVALUE;
    int _196 = NOVALUE;
    int _195 = NOVALUE;
    int _194 = NOVALUE;
    int _193 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if t_digit(test_data) then*/
    Ref(_test_data_477);
    _193 = _5t_digit(_test_data_477);
    if (_193 == 0) {
        DeRef(_193);
        _193 = NOVALUE;
        goto L1; // [7] 19
    }
    else {
        if (!IS_ATOM_INT(_193) && DBL_PTR(_193)->dbl == 0.0){
            DeRef(_193);
            _193 = NOVALUE;
            goto L1; // [7] 19
        }
        DeRef(_193);
        _193 = NOVALUE;
    }
    DeRef(_193);
    _193 = NOVALUE;

    /** 		return 0*/
    DeRef(_test_data_477);
    return 0;
    goto L2; // [16] 63
L1: 

    /** 	elsif sequence(test_data) and length(test_data) > 0 and t_digit(test_data[1]) then*/
    _194 = IS_SEQUENCE(_test_data_477);
    if (_194 == 0) {
        _195 = 0;
        goto L3; // [24] 39
    }
    if (IS_SEQUENCE(_test_data_477)){
            _196 = SEQ_PTR(_test_data_477)->length;
    }
    else {
        _196 = 1;
    }
    _197 = (_196 > 0);
    _196 = NOVALUE;
    _195 = (_197 != 0);
L3: 
    if (_195 == 0) {
        goto L4; // [39] 62
    }
    _2 = (int)SEQ_PTR(_test_data_477);
    _199 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_199);
    _200 = _5t_digit(_199);
    _199 = NOVALUE;
    if (_200 == 0) {
        DeRef(_200);
        _200 = NOVALUE;
        goto L4; // [52] 62
    }
    else {
        if (!IS_ATOM_INT(_200) && DBL_PTR(_200)->dbl == 0.0){
            DeRef(_200);
            _200 = NOVALUE;
            goto L4; // [52] 62
        }
        DeRef(_200);
        _200 = NOVALUE;
    }
    DeRef(_200);
    _200 = NOVALUE;

    /** 		return 0*/
    DeRef(_test_data_477);
    DeRef(_197);
    _197 = NOVALUE;
    return 0;
L4: 
L2: 

    /** 	return char_test(test_data, Defined_Sets[CS_Identifier])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _201 = (int)*(((s1_ptr)_2)->base + 11);
    Ref(_test_data_477);
    Ref(_201);
    _202 = _5char_test(_test_data_477, _201);
    _201 = NOVALUE;
    DeRef(_test_data_477);
    DeRef(_197);
    _197 = NOVALUE;
    return _202;
    ;
}


int _5t_alpha(int _test_data_494)
{
    int _204 = NOVALUE;
    int _203 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Alphabetic])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _203 = (int)*(((s1_ptr)_2)->base + 12);
    Ref(_test_data_494);
    Ref(_203);
    _204 = _5char_test(_test_data_494, _203);
    _203 = NOVALUE;
    DeRef(_test_data_494);
    return _204;
    ;
}


int _5t_ascii(int _test_data_499)
{
    int _206 = NOVALUE;
    int _205 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_ASCII])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _205 = (int)*(((s1_ptr)_2)->base + 13);
    Ref(_test_data_499);
    Ref(_205);
    _206 = _5char_test(_test_data_499, _205);
    _205 = NOVALUE;
    DeRef(_test_data_499);
    return _206;
    ;
}


int _5t_cntrl(int _test_data_504)
{
    int _208 = NOVALUE;
    int _207 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Control])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _207 = (int)*(((s1_ptr)_2)->base + 14);
    Ref(_test_data_504);
    Ref(_207);
    _208 = _5char_test(_test_data_504, _207);
    _207 = NOVALUE;
    DeRef(_test_data_504);
    return _208;
    ;
}


int _5t_digit(int _test_data_509)
{
    int _210 = NOVALUE;
    int _209 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Digit])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _209 = (int)*(((s1_ptr)_2)->base + 15);
    Ref(_test_data_509);
    Ref(_209);
    _210 = _5char_test(_test_data_509, _209);
    _209 = NOVALUE;
    DeRef(_test_data_509);
    return _210;
    ;
}


int _5t_graph(int _test_data_514)
{
    int _212 = NOVALUE;
    int _211 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Graphic])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _211 = (int)*(((s1_ptr)_2)->base + 16);
    Ref(_test_data_514);
    Ref(_211);
    _212 = _5char_test(_test_data_514, _211);
    _211 = NOVALUE;
    DeRef(_test_data_514);
    return _212;
    ;
}


int _5t_specword(int _test_data_519)
{
    int _214 = NOVALUE;
    int _213 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_SpecWord])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _213 = (int)*(((s1_ptr)_2)->base + 18);
    Ref(_test_data_519);
    Ref(_213);
    _214 = _5char_test(_test_data_519, _213);
    _213 = NOVALUE;
    DeRef(_test_data_519);
    return _214;
    ;
}


int _5t_bytearray(int _test_data_524)
{
    int _216 = NOVALUE;
    int _215 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Bytes])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _215 = (int)*(((s1_ptr)_2)->base + 17);
    Ref(_test_data_524);
    Ref(_215);
    _216 = _5char_test(_test_data_524, _215);
    _215 = NOVALUE;
    DeRef(_test_data_524);
    return _216;
    ;
}


int _5t_lower(int _test_data_529)
{
    int _218 = NOVALUE;
    int _217 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Lowercase])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _217 = (int)*(((s1_ptr)_2)->base + 8);
    Ref(_test_data_529);
    Ref(_217);
    _218 = _5char_test(_test_data_529, _217);
    _217 = NOVALUE;
    DeRef(_test_data_529);
    return _218;
    ;
}


int _5t_print(int _test_data_534)
{
    int _220 = NOVALUE;
    int _219 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Printable])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _219 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_test_data_534);
    Ref(_219);
    _220 = _5char_test(_test_data_534, _219);
    _219 = NOVALUE;
    DeRef(_test_data_534);
    return _220;
    ;
}


int _5t_display(int _test_data_539)
{
    int _222 = NOVALUE;
    int _221 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Displayable])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _221 = (int)*(((s1_ptr)_2)->base + 7);
    Ref(_test_data_539);
    Ref(_221);
    _222 = _5char_test(_test_data_539, _221);
    _221 = NOVALUE;
    DeRef(_test_data_539);
    return _222;
    ;
}


int _5t_punct(int _test_data_544)
{
    int _224 = NOVALUE;
    int _223 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Punctuation])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _223 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_test_data_544);
    Ref(_223);
    _224 = _5char_test(_test_data_544, _223);
    _223 = NOVALUE;
    DeRef(_test_data_544);
    return _224;
    ;
}


int _5t_space(int _test_data_549)
{
    int _226 = NOVALUE;
    int _225 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Whitespace])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _225 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_test_data_549);
    Ref(_225);
    _226 = _5char_test(_test_data_549, _225);
    _225 = NOVALUE;
    DeRef(_test_data_549);
    return _226;
    ;
}


int _5t_upper(int _test_data_554)
{
    int _228 = NOVALUE;
    int _227 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Uppercase])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _227 = (int)*(((s1_ptr)_2)->base + 9);
    Ref(_test_data_554);
    Ref(_227);
    _228 = _5char_test(_test_data_554, _227);
    _227 = NOVALUE;
    DeRef(_test_data_554);
    return _228;
    ;
}


int _5t_xdigit(int _test_data_559)
{
    int _230 = NOVALUE;
    int _229 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Hexadecimal])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _229 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_test_data_559);
    Ref(_229);
    _230 = _5char_test(_test_data_559, _229);
    _229 = NOVALUE;
    DeRef(_test_data_559);
    return _230;
    ;
}


int _5t_vowel(int _test_data_564)
{
    int _232 = NOVALUE;
    int _231 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Vowel])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _231 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_test_data_564);
    Ref(_231);
    _232 = _5char_test(_test_data_564, _231);
    _231 = NOVALUE;
    DeRef(_test_data_564);
    return _232;
    ;
}


int _5t_consonant(int _test_data_569)
{
    int _234 = NOVALUE;
    int _233 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Consonant])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_347);
    _233 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_test_data_569);
    Ref(_233);
    _234 = _5char_test(_test_data_569, _233);
    _233 = NOVALUE;
    DeRef(_test_data_569);
    return _234;
    ;
}


int _5integer_array(int _x_574)
{
    int _239 = NOVALUE;
    int _238 = NOVALUE;
    int _237 = NOVALUE;
    int _235 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _235 = IS_SEQUENCE(_x_574);
    if (_235 != 0)
    goto L1; // [6] 16
    _235 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_574);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_574)){
            _237 = SEQ_PTR(_x_574)->length;
    }
    else {
        _237 = 1;
    }
    {
        int _i_579;
        _i_579 = 1;
L2: 
        if (_i_579 > _237){
            goto L3; // [21] 54
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_574);
        _238 = (int)*(((s1_ptr)_2)->base + _i_579);
        if (IS_ATOM_INT(_238))
        _239 = 1;
        else if (IS_ATOM_DBL(_238))
        _239 = IS_ATOM_INT(DoubleToInt(_238));
        else
        _239 = 0;
        _238 = NOVALUE;
        if (_239 != 0)
        goto L4; // [37] 47
        _239 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_574);
        return 0;
L4: 

        /** 	end for*/
        _i_579 = _i_579 + 1;
        goto L2; // [49] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_574);
    return 1;
    ;
}


int _5t_text(int _x_587)
{
    int _247 = NOVALUE;
    int _245 = NOVALUE;
    int _244 = NOVALUE;
    int _243 = NOVALUE;
    int _241 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _241 = IS_SEQUENCE(_x_587);
    if (_241 != 0)
    goto L1; // [6] 16
    _241 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_587);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_587)){
            _243 = SEQ_PTR(_x_587)->length;
    }
    else {
        _243 = 1;
    }
    {
        int _i_592;
        _i_592 = 1;
L2: 
        if (_i_592 > _243){
            goto L3; // [21] 71
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_587);
        _244 = (int)*(((s1_ptr)_2)->base + _i_592);
        if (IS_ATOM_INT(_244))
        _245 = 1;
        else if (IS_ATOM_DBL(_244))
        _245 = IS_ATOM_INT(DoubleToInt(_244));
        else
        _245 = 0;
        _244 = NOVALUE;
        if (_245 != 0)
        goto L4; // [37] 47
        _245 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_587);
        return 0;
L4: 

        /** 		if x[i] < 0 then*/
        _2 = (int)SEQ_PTR(_x_587);
        _247 = (int)*(((s1_ptr)_2)->base + _i_592);
        if (binary_op_a(GREATEREQ, _247, 0)){
            _247 = NOVALUE;
            goto L5; // [53] 64
        }
        _247 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_587);
        return 0;
L5: 

        /** 	end for*/
        _i_592 = _i_592 + 1;
        goto L2; // [66] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_587);
    return 1;
    ;
}


int _5number_array(int _x_603)
{
    int _253 = NOVALUE;
    int _252 = NOVALUE;
    int _251 = NOVALUE;
    int _249 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _249 = IS_SEQUENCE(_x_603);
    if (_249 != 0)
    goto L1; // [6] 16
    _249 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_603);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_603)){
            _251 = SEQ_PTR(_x_603)->length;
    }
    else {
        _251 = 1;
    }
    {
        int _i_608;
        _i_608 = 1;
L2: 
        if (_i_608 > _251){
            goto L3; // [21] 54
        }

        /** 		if not atom(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_603);
        _252 = (int)*(((s1_ptr)_2)->base + _i_608);
        _253 = IS_ATOM(_252);
        _252 = NOVALUE;
        if (_253 != 0)
        goto L4; // [37] 47
        _253 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_603);
        return 0;
L4: 

        /** 	end for*/
        _i_608 = _i_608 + 1;
        goto L2; // [49] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_603);
    return 1;
    ;
}


int _5sequence_array(int _x_616)
{
    int _259 = NOVALUE;
    int _258 = NOVALUE;
    int _257 = NOVALUE;
    int _255 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _255 = IS_SEQUENCE(_x_616);
    if (_255 != 0)
    goto L1; // [6] 16
    _255 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_616);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_616)){
            _257 = SEQ_PTR(_x_616)->length;
    }
    else {
        _257 = 1;
    }
    {
        int _i_621;
        _i_621 = 1;
L2: 
        if (_i_621 > _257){
            goto L3; // [21] 54
        }

        /** 		if not sequence(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_616);
        _258 = (int)*(((s1_ptr)_2)->base + _i_621);
        _259 = IS_SEQUENCE(_258);
        _258 = NOVALUE;
        if (_259 != 0)
        goto L4; // [37] 47
        _259 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_616);
        return 0;
L4: 

        /** 	end for*/
        _i_621 = _i_621 + 1;
        goto L2; // [49] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_616);
    return 1;
    ;
}


int _5ascii_string(int _x_629)
{
    int _269 = NOVALUE;
    int _267 = NOVALUE;
    int _265 = NOVALUE;
    int _264 = NOVALUE;
    int _263 = NOVALUE;
    int _261 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _261 = IS_SEQUENCE(_x_629);
    if (_261 != 0)
    goto L1; // [6] 16
    _261 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_629);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_629)){
            _263 = SEQ_PTR(_x_629)->length;
    }
    else {
        _263 = 1;
    }
    {
        int _i_634;
        _i_634 = 1;
L2: 
        if (_i_634 > _263){
            goto L3; // [21] 88
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_629);
        _264 = (int)*(((s1_ptr)_2)->base + _i_634);
        if (IS_ATOM_INT(_264))
        _265 = 1;
        else if (IS_ATOM_DBL(_264))
        _265 = IS_ATOM_INT(DoubleToInt(_264));
        else
        _265 = 0;
        _264 = NOVALUE;
        if (_265 != 0)
        goto L4; // [37] 47
        _265 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_629);
        return 0;
L4: 

        /** 		if x[i] < 0 then*/
        _2 = (int)SEQ_PTR(_x_629);
        _267 = (int)*(((s1_ptr)_2)->base + _i_634);
        if (binary_op_a(GREATEREQ, _267, 0)){
            _267 = NOVALUE;
            goto L5; // [53] 64
        }
        _267 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_629);
        return 0;
L5: 

        /** 		if x[i] > 127 then*/
        _2 = (int)SEQ_PTR(_x_629);
        _269 = (int)*(((s1_ptr)_2)->base + _i_634);
        if (binary_op_a(LESSEQ, _269, 127)){
            _269 = NOVALUE;
            goto L6; // [70] 81
        }
        _269 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_629);
        return 0;
L6: 

        /** 	end for*/
        _i_634 = _i_634 + 1;
        goto L2; // [83] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_629);
    return 1;
    ;
}


int _5string(int _x_648)
{
    int _279 = NOVALUE;
    int _277 = NOVALUE;
    int _275 = NOVALUE;
    int _274 = NOVALUE;
    int _273 = NOVALUE;
    int _271 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _271 = IS_SEQUENCE(_x_648);
    if (_271 != 0)
    goto L1; // [6] 16
    _271 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_648);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_648)){
            _273 = SEQ_PTR(_x_648)->length;
    }
    else {
        _273 = 1;
    }
    {
        int _i_653;
        _i_653 = 1;
L2: 
        if (_i_653 > _273){
            goto L3; // [21] 88
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_648);
        _274 = (int)*(((s1_ptr)_2)->base + _i_653);
        if (IS_ATOM_INT(_274))
        _275 = 1;
        else if (IS_ATOM_DBL(_274))
        _275 = IS_ATOM_INT(DoubleToInt(_274));
        else
        _275 = 0;
        _274 = NOVALUE;
        if (_275 != 0)
        goto L4; // [37] 47
        _275 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_648);
        return 0;
L4: 

        /** 		if x[i] < 0 then*/
        _2 = (int)SEQ_PTR(_x_648);
        _277 = (int)*(((s1_ptr)_2)->base + _i_653);
        if (binary_op_a(GREATEREQ, _277, 0)){
            _277 = NOVALUE;
            goto L5; // [53] 64
        }
        _277 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_648);
        return 0;
L5: 

        /** 		if x[i] > 255 then*/
        _2 = (int)SEQ_PTR(_x_648);
        _279 = (int)*(((s1_ptr)_2)->base + _i_653);
        if (binary_op_a(LESSEQ, _279, 255)){
            _279 = NOVALUE;
            goto L6; // [70] 81
        }
        _279 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_648);
        return 0;
L6: 

        /** 	end for*/
        _i_653 = _i_653 + 1;
        goto L2; // [83] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_648);
    return 1;
    ;
}


int _5cstring(int _x_667)
{
    int _289 = NOVALUE;
    int _287 = NOVALUE;
    int _285 = NOVALUE;
    int _284 = NOVALUE;
    int _283 = NOVALUE;
    int _281 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _281 = IS_SEQUENCE(_x_667);
    if (_281 != 0)
    goto L1; // [6] 16
    _281 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_667);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_667)){
            _283 = SEQ_PTR(_x_667)->length;
    }
    else {
        _283 = 1;
    }
    {
        int _i_672;
        _i_672 = 1;
L2: 
        if (_i_672 > _283){
            goto L3; // [21] 88
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_667);
        _284 = (int)*(((s1_ptr)_2)->base + _i_672);
        if (IS_ATOM_INT(_284))
        _285 = 1;
        else if (IS_ATOM_DBL(_284))
        _285 = IS_ATOM_INT(DoubleToInt(_284));
        else
        _285 = 0;
        _284 = NOVALUE;
        if (_285 != 0)
        goto L4; // [37] 47
        _285 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_667);
        return 0;
L4: 

        /** 		if x[i] <= 0 then*/
        _2 = (int)SEQ_PTR(_x_667);
        _287 = (int)*(((s1_ptr)_2)->base + _i_672);
        if (binary_op_a(GREATER, _287, 0)){
            _287 = NOVALUE;
            goto L5; // [53] 64
        }
        _287 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_667);
        return 0;
L5: 

        /** 		if x[i] > 255 then*/
        _2 = (int)SEQ_PTR(_x_667);
        _289 = (int)*(((s1_ptr)_2)->base + _i_672);
        if (binary_op_a(LESSEQ, _289, 255)){
            _289 = NOVALUE;
            goto L6; // [70] 81
        }
        _289 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_667);
        return 0;
L6: 

        /** 	end for*/
        _i_672 = _i_672 + 1;
        goto L2; // [83] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_667);
    return 1;
    ;
}



// 0x673FC5C0
