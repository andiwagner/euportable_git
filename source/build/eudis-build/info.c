// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _31version_major()
{
    int _6866 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return version_info[MAJ_VER]*/
    _2 = (int)SEQ_PTR(_31version_info_12430);
    _6866 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_6866);
    return _6866;
    ;
}


int _31version_minor()
{
    int _6867 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return version_info[MIN_VER]*/
    _2 = (int)SEQ_PTR(_31version_info_12430);
    _6867 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_6867);
    return _6867;
    ;
}


int _31version_patch()
{
    int _6868 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return version_info[PAT_VER]*/
    _2 = (int)SEQ_PTR(_31version_info_12430);
    _6868 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_6868);
    return _6868;
    ;
}


int _31version_node(int _full_12463)
{
    int _6875 = NOVALUE;
    int _6874 = NOVALUE;
    int _6873 = NOVALUE;
    int _6872 = NOVALUE;
    int _6871 = NOVALUE;
    int _6870 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if full or length(version_info[NODE]) < 12 then*/
    if (0 != 0) {
        goto L1; // [5] 29
    }
    _2 = (int)SEQ_PTR(_31version_info_12430);
    _6870 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_6870)){
            _6871 = SEQ_PTR(_6870)->length;
    }
    else {
        _6871 = 1;
    }
    _6870 = NOVALUE;
    _6872 = (_6871 < 12);
    _6871 = NOVALUE;
    if (_6872 == 0)
    {
        DeRef(_6872);
        _6872 = NOVALUE;
        goto L2; // [25] 44
    }
    else{
        DeRef(_6872);
        _6872 = NOVALUE;
    }
L1: 

    /** 		return version_info[NODE]*/
    _2 = (int)SEQ_PTR(_31version_info_12430);
    _6873 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_6873);
    _6870 = NOVALUE;
    return _6873;
L2: 

    /** 	return version_info[NODE][1..12]*/
    _2 = (int)SEQ_PTR(_31version_info_12430);
    _6874 = (int)*(((s1_ptr)_2)->base + 5);
    rhs_slice_target = (object_ptr)&_6875;
    RHS_Slice(_6874, 1, 12);
    _6874 = NOVALUE;
    _6870 = NOVALUE;
    _6873 = NOVALUE;
    return _6875;
    ;
}


int _31version_date(int _full_12477)
{
    int _6884 = NOVALUE;
    int _6883 = NOVALUE;
    int _6882 = NOVALUE;
    int _6881 = NOVALUE;
    int _6880 = NOVALUE;
    int _6879 = NOVALUE;
    int _6877 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if full or is_developmental or length(version_info[REVISION_DATE]) < 10 then*/
    if (_full_12477 != 0) {
        _6877 = 1;
        goto L1; // [5] 15
    }
    _6877 = (_31is_developmental_12432 != 0);
L1: 
    if (_6877 != 0) {
        goto L2; // [15] 39
    }
    _2 = (int)SEQ_PTR(_31version_info_12430);
    _6879 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_6879)){
            _6880 = SEQ_PTR(_6879)->length;
    }
    else {
        _6880 = 1;
    }
    _6879 = NOVALUE;
    _6881 = (_6880 < 10);
    _6880 = NOVALUE;
    if (_6881 == 0)
    {
        DeRef(_6881);
        _6881 = NOVALUE;
        goto L3; // [35] 54
    }
    else{
        DeRef(_6881);
        _6881 = NOVALUE;
    }
L2: 

    /** 		return version_info[REVISION_DATE]*/
    _2 = (int)SEQ_PTR(_31version_info_12430);
    _6882 = (int)*(((s1_ptr)_2)->base + 7);
    Ref(_6882);
    _6879 = NOVALUE;
    return _6882;
L3: 

    /** 	return version_info[REVISION_DATE][1..10]*/
    _2 = (int)SEQ_PTR(_31version_info_12430);
    _6883 = (int)*(((s1_ptr)_2)->base + 7);
    rhs_slice_target = (object_ptr)&_6884;
    RHS_Slice(_6883, 1, 10);
    _6883 = NOVALUE;
    _6879 = NOVALUE;
    _6882 = NOVALUE;
    return _6884;
    ;
}


int _31version_string(int _full_12492)
{
    int _version_revision_inlined_version_revision_at_49_12501 = NOVALUE;
    int _6904 = NOVALUE;
    int _6903 = NOVALUE;
    int _6902 = NOVALUE;
    int _6901 = NOVALUE;
    int _6900 = NOVALUE;
    int _6899 = NOVALUE;
    int _6898 = NOVALUE;
    int _6897 = NOVALUE;
    int _6895 = NOVALUE;
    int _6894 = NOVALUE;
    int _6893 = NOVALUE;
    int _6892 = NOVALUE;
    int _6891 = NOVALUE;
    int _6890 = NOVALUE;
    int _6889 = NOVALUE;
    int _6888 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if full or is_developmental then*/
    if (0 != 0) {
        goto L1; // [5] 16
    }
    if (_31is_developmental_12432 == 0)
    {
        goto L2; // [12] 90
    }
    else{
    }
L1: 

    /** 		return sprintf("%d.%d.%d %s (%d:%s, %s)", {*/
    _2 = (int)SEQ_PTR(_31version_info_12430);
    _6888 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_31version_info_12430);
    _6889 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_31version_info_12430);
    _6890 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_31version_info_12430);
    _6891 = (int)*(((s1_ptr)_2)->base + 4);

    /** 	return version_info[REVISION]*/
    DeRef(_version_revision_inlined_version_revision_at_49_12501);
    _2 = (int)SEQ_PTR(_31version_info_12430);
    _version_revision_inlined_version_revision_at_49_12501 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_version_revision_inlined_version_revision_at_49_12501);
    _6892 = _31version_node(0);
    _6893 = _31version_date(_full_12492);
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_6888);
    *((int *)(_2+4)) = _6888;
    Ref(_6889);
    *((int *)(_2+8)) = _6889;
    Ref(_6890);
    *((int *)(_2+12)) = _6890;
    Ref(_6891);
    *((int *)(_2+16)) = _6891;
    Ref(_version_revision_inlined_version_revision_at_49_12501);
    *((int *)(_2+20)) = _version_revision_inlined_version_revision_at_49_12501;
    *((int *)(_2+24)) = _6892;
    *((int *)(_2+28)) = _6893;
    _6894 = MAKE_SEQ(_1);
    _6893 = NOVALUE;
    _6892 = NOVALUE;
    _6891 = NOVALUE;
    _6890 = NOVALUE;
    _6889 = NOVALUE;
    _6888 = NOVALUE;
    _6895 = EPrintf(-9999999, _6887, _6894);
    DeRefDS(_6894);
    _6894 = NOVALUE;
    return _6895;
    goto L3; // [87] 150
L2: 

    /** 		return sprintf("%d.%d.%d %s (%s, %s)", {*/
    _2 = (int)SEQ_PTR(_31version_info_12430);
    _6897 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_31version_info_12430);
    _6898 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_31version_info_12430);
    _6899 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_31version_info_12430);
    _6900 = (int)*(((s1_ptr)_2)->base + 4);
    _6901 = _31version_node(0);
    _6902 = _31version_date(_full_12492);
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_6897);
    *((int *)(_2+4)) = _6897;
    Ref(_6898);
    *((int *)(_2+8)) = _6898;
    Ref(_6899);
    *((int *)(_2+12)) = _6899;
    Ref(_6900);
    *((int *)(_2+16)) = _6900;
    *((int *)(_2+20)) = _6901;
    *((int *)(_2+24)) = _6902;
    _6903 = MAKE_SEQ(_1);
    _6902 = NOVALUE;
    _6901 = NOVALUE;
    _6900 = NOVALUE;
    _6899 = NOVALUE;
    _6898 = NOVALUE;
    _6897 = NOVALUE;
    _6904 = EPrintf(-9999999, _6896, _6903);
    DeRefDS(_6903);
    _6903 = NOVALUE;
    DeRef(_6895);
    _6895 = NOVALUE;
    return _6904;
L3: 
    ;
}


int _31euphoria_copyright()
{
    int _version_string_long_1__tmp_at2_12535 = NOVALUE;
    int _version_string_long_inlined_version_string_long_at_2_12534 = NOVALUE;
    int _platform_name_inlined_platform_name_at_8_inlined_version_string_long_at_2_12533 = NOVALUE;
    int _6914 = NOVALUE;
    int _6912 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return {*/

    /** 	return version_string(full) & " for " & platform_name()*/
    _0 = _version_string_long_1__tmp_at2_12535;
    _version_string_long_1__tmp_at2_12535 = _31version_string(0);
    DeRef(_0);

    /** 	ifdef WINDOWS then*/

    /** 		return "Windows"*/
    RefDS(_6625);
    DeRefi(_platform_name_inlined_platform_name_at_8_inlined_version_string_long_at_2_12533);
    _platform_name_inlined_platform_name_at_8_inlined_version_string_long_at_2_12533 = _6625;
    {
        int concat_list[3];

        concat_list[0] = _platform_name_inlined_platform_name_at_8_inlined_version_string_long_at_2_12533;
        concat_list[1] = _6909;
        concat_list[2] = _version_string_long_1__tmp_at2_12535;
        Concat_N((object_ptr)&_version_string_long_inlined_version_string_long_at_2_12534, concat_list, 3);
    }
    DeRefi(_platform_name_inlined_platform_name_at_8_inlined_version_string_long_at_2_12533);
    _platform_name_inlined_platform_name_at_8_inlined_version_string_long_at_2_12533 = NOVALUE;
    DeRef(_version_string_long_1__tmp_at2_12535);
    _version_string_long_1__tmp_at2_12535 = NOVALUE;
    Concat((object_ptr)&_6912, _6911, _version_string_long_inlined_version_string_long_at_2_12534);
    RefDS(_6913);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6912;
    ((int *)_2)[2] = _6913;
    _6914 = MAKE_SEQ(_1);
    _6912 = NOVALUE;
    return _6914;
    ;
}


int _31all_copyrights()
{
    int _pcre_copyright_inlined_pcre_copyright_at_5_12548 = NOVALUE;
    int _6919 = NOVALUE;
    int _6918 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return {*/
    _6918 = _31euphoria_copyright();

    /** 	return {*/
    RefDS(_6916);
    RefDS(_6915);
    DeRef(_pcre_copyright_inlined_pcre_copyright_at_5_12548);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6915;
    ((int *)_2)[2] = _6916;
    _pcre_copyright_inlined_pcre_copyright_at_5_12548 = MAKE_SEQ(_1);
    RefDS(_pcre_copyright_inlined_pcre_copyright_at_5_12548);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6918;
    ((int *)_2)[2] = _pcre_copyright_inlined_pcre_copyright_at_5_12548;
    _6919 = MAKE_SEQ(_1);
    _6918 = NOVALUE;
    return _6919;
    ;
}



// 0x32FC0F7D
