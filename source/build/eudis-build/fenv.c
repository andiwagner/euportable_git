// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _62e_to_c(int _ee_23031)
{
    int _ce_23032 = NOVALUE;
    int _13490 = NOVALUE;
    int _13489 = NOVALUE;
    int _13488 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer ce = 0*/
    _ce_23032 = 0;

    /** 	for i = 1 to length(ee) do*/
    if (IS_SEQUENCE(_ee_23031)){
            _13488 = SEQ_PTR(_ee_23031)->length;
    }
    else {
        _13488 = 1;
    }
    {
        int _i_23034;
        _i_23034 = 1;
L1: 
        if (_i_23034 > _13488){
            goto L2; // [11] 43
        }

        /** 		ce = or_bits(ce, conv_table[ee[i]])*/
        _2 = (int)SEQ_PTR(_ee_23031);
        _13489 = (int)*(((s1_ptr)_2)->base + _i_23034);
        _2 = (int)SEQ_PTR(_62conv_table_23025);
        if (!IS_ATOM_INT(_13489)){
            _13490 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_13489)->dbl));
        }
        else{
            _13490 = (int)*(((s1_ptr)_2)->base + _13489);
        }
        {unsigned long tu;
             tu = (unsigned long)_ce_23032 | (unsigned long)_13490;
             _ce_23032 = MAKE_UINT(tu);
        }
        _13490 = NOVALUE;
        if (!IS_ATOM_INT(_ce_23032)) {
            _1 = (long)(DBL_PTR(_ce_23032)->dbl);
            if (UNIQUE(DBL_PTR(_ce_23032)) && (DBL_PTR(_ce_23032)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_ce_23032);
            _ce_23032 = _1;
        }

        /** 	end for*/
        _i_23034 = _i_23034 + 1;
        goto L1; // [38] 18
L2: 
        ;
    }

    /** 	return ce*/
    DeRef(_ee_23031);
    _13489 = NOVALUE;
    return _ce_23032;
    ;
}


int _62c_to_e(int _ce_23041)
{
    int _ee_23042 = NOVALUE;
    int _i_23043 = NOVALUE;
    int _13495 = NOVALUE;
    int _13493 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence ee = {}*/
    RefDS(_5);
    DeRefi(_ee_23042);
    _ee_23042 = _5;

    /** 	integer i = 1*/
    _i_23043 = 1;

    /** 	while i <= C_FE_INEXACT do*/
L1: 
    if (_i_23043 > 32)
    goto L2; // [22] 60

    /** 		if and_bits(ce, i) = i then*/
    {unsigned long tu;
         tu = (unsigned long)_ce_23041 & (unsigned long)_i_23043;
         _13493 = MAKE_UINT(tu);
    }
    if (_13493 != _i_23043)
    goto L3; // [32] 49

    /** 			ee = append(ee, rev[i])*/
    _2 = (int)SEQ_PTR(_62rev_23027);
    _13495 = (int)*(((s1_ptr)_2)->base + _i_23043);
    Append(&_ee_23042, _ee_23042, _13495);
    _13495 = NOVALUE;
L3: 

    /** 		i *= 2*/
    _i_23043 = _i_23043 + _i_23043;

    /** 	end while*/
    goto L1; // [57] 20
L2: 

    /** 	return ee*/
    DeRef(_13493);
    _13493 = NOVALUE;
    return _ee_23042;
    ;
}


int _62clear(int _e_p_23054)
{
    int _ee_23055 = NOVALUE;
    int _13503 = NOVALUE;
    int _13502 = NOVALUE;
    int _13501 = NOVALUE;
    int _13498 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(e_p) then*/
    if (IS_ATOM_INT(_e_p_23054))
    _13498 = 1;
    else if (IS_ATOM_DBL(_e_p_23054))
    _13498 = IS_ATOM_INT(DoubleToInt(_e_p_23054));
    else
    _13498 = 0;
    if (_13498 == 0)
    {
        _13498 = NOVALUE;
        goto L1; // [6] 18
    }
    else{
        _13498 = NOVALUE;
    }

    /** 		ee = {e_p}*/
    _0 = _ee_23055;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_e_p_23054);
    *((int *)(_2+4)) = _e_p_23054;
    _ee_23055 = MAKE_SEQ(_1);
    DeRef(_0);
    goto L2; // [15] 24
L1: 

    /** 		ee = e_p*/
    Ref(_e_p_23054);
    DeRef(_ee_23055);
    _ee_23055 = _e_p_23054;
L2: 

    /** 	if feclearexcept != -1 then*/
    if (binary_op_a(EQUALS, _62feclearexcept_22957, -1)){
        goto L3; // [28] 58
    }

    /** 		return c_func(feclearexcept, {e_to_c(ee)})*/
    Ref(_ee_23055);
    _13501 = _62e_to_c(_ee_23055);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _13501;
    _13502 = MAKE_SEQ(_1);
    _13501 = NOVALUE;
    _13503 = call_c(1, _62feclearexcept_22957, _13502);
    DeRefDS(_13502);
    _13502 = NOVALUE;
    DeRef(_e_p_23054);
    DeRef(_ee_23055);
    return _13503;
    goto L4; // [55] 65
L3: 

    /** 		return -1*/
    DeRef(_e_p_23054);
    DeRef(_ee_23055);
    DeRef(_13503);
    _13503 = NOVALUE;
    return -1;
L4: 
    ;
}


int _62raise(int _e_p_23068)
{
    int _ee_23069 = NOVALUE;
    int _13509 = NOVALUE;
    int _13508 = NOVALUE;
    int _13507 = NOVALUE;
    int _13504 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(e_p) then*/
    _13504 = 1;
    if (_13504 == 0)
    {
        _13504 = NOVALUE;
        goto L1; // [6] 18
    }
    else{
        _13504 = NOVALUE;
    }

    /** 		ee = {e_p}*/
    _0 = _ee_23069;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _e_p_23068;
    _ee_23069 = MAKE_SEQ(_1);
    DeRefi(_0);
    goto L2; // [15] 24
L1: 

    /** 		ee = e_p*/
    DeRefi(_ee_23069);
    _ee_23069 = _e_p_23068;
L2: 

    /** 	if feraiseexcept = -1 then*/
    if (binary_op_a(NOTEQ, _62feraiseexcept_22966, -1)){
        goto L3; // [28] 39
    }

    /** 		return -1*/
    DeRefi(_ee_23069);
    return -1;
L3: 

    /** 	return c_func(feraiseexcept, {e_to_c(ee)})*/
    Ref(_ee_23069);
    _13507 = _62e_to_c(_ee_23069);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _13507;
    _13508 = MAKE_SEQ(_1);
    _13507 = NOVALUE;
    _13509 = call_c(1, _62feraiseexcept_22966, _13508);
    DeRefDS(_13508);
    _13508 = NOVALUE;
    DeRefi(_ee_23069);
    return _13509;
    ;
}


int _62test(int _e_p_23081)
{
    int _ee_23082 = NOVALUE;
    int _ce_23089 = NOVALUE;
    int _ans_23093 = NOVALUE;
    int _13517 = NOVALUE;
    int _13514 = NOVALUE;
    int _13513 = NOVALUE;
    int _13510 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(e_p) then*/
    _13510 = 1;
    if (_13510 == 0)
    {
        _13510 = NOVALUE;
        goto L1; // [6] 18
    }
    else{
        _13510 = NOVALUE;
    }

    /** 		ee = {e_p}*/
    _0 = _ee_23082;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _e_p_23081;
    _ee_23082 = MAKE_SEQ(_1);
    DeRefi(_0);
    goto L2; // [15] 24
L1: 

    /** 		ee = e_p*/
    DeRefi(_ee_23082);
    _ee_23082 = _e_p_23081;
L2: 

    /** 	if fetestexcept = -1 then*/
    if (binary_op_a(NOTEQ, _62fetestexcept_22975, -1)){
        goto L3; // [28] 39
    }

    /** 		return 0*/
    DeRefi(_ee_23082);
    DeRef(_ans_23093);
    return 0;
L3: 

    /** 	integer ce = c_func(fetestexcept, {e_to_c(ee)})*/
    Ref(_ee_23082);
    _13513 = _62e_to_c(_ee_23082);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _13513;
    _13514 = MAKE_SEQ(_1);
    _13513 = NOVALUE;
    _ce_23089 = call_c(1, _62fetestexcept_22975, _13514);
    DeRefDS(_13514);
    _13514 = NOVALUE;
    if (!IS_ATOM_INT(_ce_23089)) {
        _1 = (long)(DBL_PTR(_ce_23089)->dbl);
        if (UNIQUE(DBL_PTR(_ce_23089)) && (DBL_PTR(_ce_23089)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ce_23089);
        _ce_23089 = _1;
    }

    /** 	sequence ans = c_to_e(ce)*/
    _0 = _ans_23093;
    _ans_23093 = _62c_to_e(_ce_23089);
    DeRef(_0);

    /** 	if integer(e_p) then*/
    _13517 = 1;
    if (_13517 == 0)
    {
        _13517 = NOVALUE;
        goto L4; // [73] 83
    }
    else{
        _13517 = NOVALUE;
    }

    /** 		return ce*/
    DeRefi(_ee_23082);
    DeRefDS(_ans_23093);
    return _ce_23089;
L4: 

    /** 	return ans*/
    DeRefi(_ee_23082);
    return _ans_23093;
    ;
}



// 0x660082A5
