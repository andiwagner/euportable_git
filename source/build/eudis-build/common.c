// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _26open_locked(int _file_path_11182)
{
    int _fh_11183 = NOVALUE;
    int _0, _1, _2;
    

    /** 	fh = open(file_path, "u")*/
    _fh_11183 = EOpen(_file_path_11182, _6294, 0);

    /** 	if fh = -1 then*/
    if (_fh_11183 != -1)
    goto L1; // [14] 28

    /** 		fh = open(file_path, "r")*/
    _fh_11183 = EOpen(_file_path_11182, _1081, 0);
L1: 

    /** 	return fh*/
    DeRefDS(_file_path_11182);
    return _fh_11183;
    ;
}


int _26get_eudir()
{
    int _possible_paths_11200 = NOVALUE;
    int _homepath_11205 = NOVALUE;
    int _homedrive_11207 = NOVALUE;
    int _possible_path_11230 = NOVALUE;
    int _possible_path_11244 = NOVALUE;
    int _file_check_11258 = NOVALUE;
    int _6345 = NOVALUE;
    int _6344 = NOVALUE;
    int _6343 = NOVALUE;
    int _6341 = NOVALUE;
    int _6339 = NOVALUE;
    int _6337 = NOVALUE;
    int _6336 = NOVALUE;
    int _6335 = NOVALUE;
    int _6334 = NOVALUE;
    int _6333 = NOVALUE;
    int _6331 = NOVALUE;
    int _6329 = NOVALUE;
    int _6328 = NOVALUE;
    int _6324 = NOVALUE;
    int _6322 = NOVALUE;
    int _6319 = NOVALUE;
    int _6318 = NOVALUE;
    int _6317 = NOVALUE;
    int _6316 = NOVALUE;
    int _6315 = NOVALUE;
    int _6314 = NOVALUE;
    int _6313 = NOVALUE;
    int _6312 = NOVALUE;
    int _6311 = NOVALUE;
    int _6300 = NOVALUE;
    int _6298 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(eudir) then*/
    _6298 = IS_SEQUENCE(_26eudir_11166);
    if (_6298 == 0)
    {
        _6298 = NOVALUE;
        goto L1; // [8] 20
    }
    else{
        _6298 = NOVALUE;
    }

    /** 		return eudir*/
    Ref(_26eudir_11166);
    DeRef(_possible_paths_11200);
    DeRefi(_homepath_11205);
    DeRefi(_homedrive_11207);
    return _26eudir_11166;
L1: 

    /** 	eudir = getenv("EUDIR")*/
    DeRef(_26eudir_11166);
    _26eudir_11166 = EGetEnv(_4347);

    /** 	if sequence(eudir) then*/
    _6300 = IS_SEQUENCE(_26eudir_11166);
    if (_6300 == 0)
    {
        _6300 = NOVALUE;
        goto L2; // [32] 44
    }
    else{
        _6300 = NOVALUE;
    }

    /** 		return eudir*/
    Ref(_26eudir_11166);
    DeRef(_possible_paths_11200);
    DeRefi(_homepath_11205);
    DeRefi(_homedrive_11207);
    return _26eudir_11166;
L2: 

    /** 	ifdef UNIX then*/

    /** 		sequence possible_paths = {*/
    _0 = _possible_paths_11200;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_6305);
    *((int *)(_2+4)) = _6305;
    RefDS(_6306);
    *((int *)(_2+8)) = _6306;
    RefDS(_6307);
    *((int *)(_2+12)) = _6307;
    _possible_paths_11200 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 		object homepath = getenv("HOMEPATH")*/
    DeRefi(_homepath_11205);
    _homepath_11205 = EGetEnv(_3964);

    /** 		object homedrive = getenv("HOMEDRIVE")*/
    DeRefi(_homedrive_11207);
    _homedrive_11207 = EGetEnv(_3962);

    /** 		if sequence(homepath) and sequence(homedrive) then*/
    _6311 = IS_SEQUENCE(_homepath_11205);
    if (_6311 == 0) {
        goto L3; // [69] 134
    }
    _6313 = IS_SEQUENCE(_homedrive_11207);
    if (_6313 == 0)
    {
        _6313 = NOVALUE;
        goto L3; // [77] 134
    }
    else{
        _6313 = NOVALUE;
    }

    /** 			if length(homepath) and not equal(homepath[$], SLASH) then*/
    if (IS_SEQUENCE(_homepath_11205)){
            _6314 = SEQ_PTR(_homepath_11205)->length;
    }
    else {
        _6314 = 1;
    }
    if (_6314 == 0) {
        goto L4; // [85] 118
    }
    if (IS_SEQUENCE(_homepath_11205)){
            _6316 = SEQ_PTR(_homepath_11205)->length;
    }
    else {
        _6316 = 1;
    }
    _2 = (int)SEQ_PTR(_homepath_11205);
    _6317 = (int)*(((s1_ptr)_2)->base + _6316);
    if (_6317 == 92)
    _6318 = 1;
    else if (IS_ATOM_INT(_6317) && IS_ATOM_INT(92))
    _6318 = 0;
    else
    _6318 = (compare(_6317, 92) == 0);
    _6317 = NOVALUE;
    _6319 = (_6318 == 0);
    _6318 = NOVALUE;
    if (_6319 == 0)
    {
        DeRef(_6319);
        _6319 = NOVALUE;
        goto L4; // [106] 118
    }
    else{
        DeRef(_6319);
        _6319 = NOVALUE;
    }

    /** 				homepath &= SLASH*/
    if (IS_SEQUENCE(_homepath_11205) && IS_ATOM(92)) {
        Append(&_homepath_11205, _homepath_11205, 92);
    }
    else if (IS_ATOM(_homepath_11205) && IS_SEQUENCE(92)) {
    }
    else {
        Concat((object_ptr)&_homepath_11205, _homepath_11205, 92);
    }
L4: 

    /** 			possible_paths = append(possible_paths, homedrive & SLASH & homepath & "euphoria")*/
    {
        int concat_list[4];

        concat_list[0] = _6321;
        concat_list[1] = _homepath_11205;
        concat_list[2] = 92;
        concat_list[3] = _homedrive_11207;
        Concat_N((object_ptr)&_6322, concat_list, 4);
    }
    RefDS(_6322);
    Append(&_possible_paths_11200, _possible_paths_11200, _6322);
    DeRefDS(_6322);
    _6322 = NOVALUE;
L3: 

    /** 	for i = 1 to length(possible_paths) do*/
    if (IS_SEQUENCE(_possible_paths_11200)){
            _6324 = SEQ_PTR(_possible_paths_11200)->length;
    }
    else {
        _6324 = 1;
    }
    {
        int _i_11228;
        _i_11228 = 1;
L5: 
        if (_i_11228 > _6324){
            goto L6; // [141] 200
        }

        /** 		sequence possible_path = possible_paths[i]*/
        DeRef(_possible_path_11230);
        _2 = (int)SEQ_PTR(_possible_paths_11200);
        _possible_path_11230 = (int)*(((s1_ptr)_2)->base + _i_11228);
        RefDS(_possible_path_11230);

        /** 		if file_exists(possible_path & SLASH & "include" & SLASH & "euphoria.h") then*/
        {
            int concat_list[5];

            concat_list[0] = _6327;
            concat_list[1] = 92;
            concat_list[2] = _6326;
            concat_list[3] = 92;
            concat_list[4] = _possible_path_11230;
            Concat_N((object_ptr)&_6328, concat_list, 5);
        }
        _6329 = _9file_exists(_6328);
        _6328 = NOVALUE;
        if (_6329 == 0) {
            DeRef(_6329);
            _6329 = NOVALUE;
            goto L7; // [174] 191
        }
        else {
            if (!IS_ATOM_INT(_6329) && DBL_PTR(_6329)->dbl == 0.0){
                DeRef(_6329);
                _6329 = NOVALUE;
                goto L7; // [174] 191
            }
            DeRef(_6329);
            _6329 = NOVALUE;
        }
        DeRef(_6329);
        _6329 = NOVALUE;

        /** 			eudir = possible_path*/
        RefDS(_possible_path_11230);
        DeRef(_26eudir_11166);
        _26eudir_11166 = _possible_path_11230;

        /** 			return eudir*/
        RefDS(_26eudir_11166);
        DeRefDS(_possible_path_11230);
        DeRefDS(_possible_paths_11200);
        DeRefi(_homepath_11205);
        DeRefi(_homedrive_11207);
        return _26eudir_11166;
L7: 
        DeRef(_possible_path_11230);
        _possible_path_11230 = NOVALUE;

        /** 	end for*/
        _i_11228 = _i_11228 + 1;
        goto L5; // [195] 148
L6: 
        ;
    }

    /** 	possible_paths = include_paths(0)*/
    _0 = _possible_paths_11200;
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_4373);
    *((int *)(_2+4)) = _4373;
    RefDS(_4372);
    *((int *)(_2+8)) = _4372;
    RefDS(_4371);
    *((int *)(_2+12)) = _4371;
    RefDS(_4370);
    *((int *)(_2+16)) = _4370;
    RefDS(_4369);
    *((int *)(_2+20)) = _4369;
    _possible_paths_11200 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 	for i = 1 to length(possible_paths) do*/
    _6331 = 5;
    {
        int _i_11242;
        _i_11242 = 1;
L8: 
        if (_i_11242 > 5){
            goto L9; // [215] 340
        }

        /** 		sequence possible_path = possible_paths[i]*/
        DeRef(_possible_path_11244);
        _2 = (int)SEQ_PTR(_possible_paths_11200);
        _possible_path_11244 = (int)*(((s1_ptr)_2)->base + _i_11242);
        RefDS(_possible_path_11244);

        /** 		if equal(possible_path[$], SLASH) then*/
        if (IS_SEQUENCE(_possible_path_11244)){
                _6333 = SEQ_PTR(_possible_path_11244)->length;
        }
        else {
            _6333 = 1;
        }
        _2 = (int)SEQ_PTR(_possible_path_11244);
        _6334 = (int)*(((s1_ptr)_2)->base + _6333);
        if (_6334 == 92)
        _6335 = 1;
        else if (IS_ATOM_INT(_6334) && IS_ATOM_INT(92))
        _6335 = 0;
        else
        _6335 = (compare(_6334, 92) == 0);
        _6334 = NOVALUE;
        if (_6335 == 0)
        {
            _6335 = NOVALUE;
            goto LA; // [245] 263
        }
        else{
            _6335 = NOVALUE;
        }

        /** 			possible_path = possible_path[1..$-1]*/
        if (IS_SEQUENCE(_possible_path_11244)){
                _6336 = SEQ_PTR(_possible_path_11244)->length;
        }
        else {
            _6336 = 1;
        }
        _6337 = _6336 - 1;
        _6336 = NOVALUE;
        rhs_slice_target = (object_ptr)&_possible_path_11244;
        RHS_Slice(_possible_path_11244, 1, _6337);
LA: 

        /** 		if not ends("include", possible_path) then*/
        RefDS(_6326);
        RefDS(_possible_path_11244);
        _6339 = _7ends(_6326, _possible_path_11244);
        if (IS_ATOM_INT(_6339)) {
            if (_6339 != 0){
                DeRef(_6339);
                _6339 = NOVALUE;
                goto LB; // [270] 280
            }
        }
        else {
            if (DBL_PTR(_6339)->dbl != 0.0){
                DeRef(_6339);
                _6339 = NOVALUE;
                goto LB; // [270] 280
            }
        }
        DeRef(_6339);
        _6339 = NOVALUE;

        /** 			continue*/
        DeRefDS(_possible_path_11244);
        _possible_path_11244 = NOVALUE;
        DeRef(_file_check_11258);
        _file_check_11258 = NOVALUE;
        goto LC; // [277] 335
LB: 

        /** 		sequence file_check = possible_path*/
        RefDS(_possible_path_11244);
        DeRef(_file_check_11258);
        _file_check_11258 = _possible_path_11244;

        /** 		file_check &= SLASH & "euphoria.h"*/
        Prepend(&_6341, _6327, 92);
        Concat((object_ptr)&_file_check_11258, _file_check_11258, _6341);
        DeRefDS(_6341);
        _6341 = NOVALUE;

        /** 		if file_exists(file_check) then*/
        RefDS(_file_check_11258);
        _6343 = _9file_exists(_file_check_11258);
        if (_6343 == 0) {
            DeRef(_6343);
            _6343 = NOVALUE;
            goto LD; // [305] 331
        }
        else {
            if (!IS_ATOM_INT(_6343) && DBL_PTR(_6343)->dbl == 0.0){
                DeRef(_6343);
                _6343 = NOVALUE;
                goto LD; // [305] 331
            }
            DeRef(_6343);
            _6343 = NOVALUE;
        }
        DeRef(_6343);
        _6343 = NOVALUE;

        /** 			eudir = possible_path[1..$-8] -- strip SLASH & "include"*/
        if (IS_SEQUENCE(_possible_path_11244)){
                _6344 = SEQ_PTR(_possible_path_11244)->length;
        }
        else {
            _6344 = 1;
        }
        _6345 = _6344 - 8;
        _6344 = NOVALUE;
        rhs_slice_target = (object_ptr)&_26eudir_11166;
        RHS_Slice(_possible_path_11244, 1, _6345);

        /** 			return eudir*/
        RefDS(_26eudir_11166);
        DeRefDS(_possible_path_11244);
        DeRefDS(_file_check_11258);
        DeRef(_possible_paths_11200);
        DeRefi(_homepath_11205);
        DeRefi(_homedrive_11207);
        DeRef(_6337);
        _6337 = NOVALUE;
        _6345 = NOVALUE;
        return _26eudir_11166;
LD: 
        DeRef(_possible_path_11244);
        _possible_path_11244 = NOVALUE;
        DeRef(_file_check_11258);
        _file_check_11258 = NOVALUE;

        /** 	end for*/
LC: 
        _i_11242 = _i_11242 + 1;
        goto L8; // [335] 222
L9: 
        ;
    }

    /** 	return ""*/
    RefDS(_5);
    DeRef(_possible_paths_11200);
    DeRefi(_homepath_11205);
    DeRefi(_homedrive_11207);
    DeRef(_6337);
    _6337 = NOVALUE;
    DeRef(_6345);
    _6345 = NOVALUE;
    return _5;
    ;
}


void _26set_eudir(int _new_eudir_11270)
{
    int _0, _1, _2;
    

    /** 	eudir = new_eudir*/
    RefDS(_new_eudir_11270);
    DeRef(_26eudir_11166);
    _26eudir_11166 = _new_eudir_11270;

    /** 	cmdline_eudir = 1*/
    _26cmdline_eudir_11179 = 1;

    /** end procedure*/
    DeRefDS(_new_eudir_11270);
    return;
    ;
}


int _26is_eudir_from_cmdline()
{
    int _0, _1, _2;
    

    /** 	return cmdline_eudir*/
    return _26cmdline_eudir_11179;
    ;
}



// 0xBC130FA2
