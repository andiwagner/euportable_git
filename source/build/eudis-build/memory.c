// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _14positive_int(int _x_1706)
{
    int _783 = NOVALUE;
    int _781 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not integer(x) then*/
    if (IS_ATOM_INT(_x_1706))
    _781 = 1;
    else if (IS_ATOM_DBL(_x_1706))
    _781 = IS_ATOM_INT(DoubleToInt(_x_1706));
    else
    _781 = 0;
    if (_781 != 0)
    goto L1; // [6] 16
    _781 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_1706);
    return 0;
L1: 

    /**     return x >= 1*/
    if (IS_ATOM_INT(_x_1706)) {
        _783 = (_x_1706 >= 1);
    }
    else {
        _783 = binary_op(GREATEREQ, _x_1706, 1);
    }
    DeRef(_x_1706);
    return _783;
    ;
}


int _14machine_addr(int _a_1713)
{
    int _792 = NOVALUE;
    int _791 = NOVALUE;
    int _790 = NOVALUE;
    int _788 = NOVALUE;
    int _786 = NOVALUE;
    int _784 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not atom(a) then*/
    _784 = IS_ATOM(_a_1713);
    if (_784 != 0)
    goto L1; // [6] 16
    _784 = NOVALUE;

    /** 		return 0*/
    DeRef(_a_1713);
    return 0;
L1: 

    /** 	if not integer(a)then*/
    if (IS_ATOM_INT(_a_1713))
    _786 = 1;
    else if (IS_ATOM_DBL(_a_1713))
    _786 = IS_ATOM_INT(DoubleToInt(_a_1713));
    else
    _786 = 0;
    if (_786 != 0)
    goto L2; // [21] 41
    _786 = NOVALUE;

    /** 		if floor(a) != a then*/
    if (IS_ATOM_INT(_a_1713))
    _788 = e_floor(_a_1713);
    else
    _788 = unary_op(FLOOR, _a_1713);
    if (binary_op_a(EQUALS, _788, _a_1713)){
        DeRef(_788);
        _788 = NOVALUE;
        goto L3; // [29] 40
    }
    DeRef(_788);
    _788 = NOVALUE;

    /** 			return 0*/
    DeRef(_a_1713);
    return 0;
L3: 
L2: 

    /** 	return a > 0 and a <= MAX_ADDR*/
    if (IS_ATOM_INT(_a_1713)) {
        _790 = (_a_1713 > 0);
    }
    else {
        _790 = binary_op(GREATER, _a_1713, 0);
    }
    if (IS_ATOM_INT(_a_1713) && IS_ATOM_INT(_14MAX_ADDR_1699)) {
        _791 = (_a_1713 <= _14MAX_ADDR_1699);
    }
    else {
        _791 = binary_op(LESSEQ, _a_1713, _14MAX_ADDR_1699);
    }
    if (IS_ATOM_INT(_790) && IS_ATOM_INT(_791)) {
        _792 = (_790 != 0 && _791 != 0);
    }
    else {
        _792 = binary_op(AND, _790, _791);
    }
    DeRef(_790);
    _790 = NOVALUE;
    DeRef(_791);
    _791 = NOVALUE;
    DeRef(_a_1713);
    return _792;
    ;
}


void _14deallocate(int _addr_1728)
{
    int _0, _1, _2;
    

    /** 	ifdef DATA_EXECUTE and WINDOWS then*/

    /**    	machine_proc( memconst:M_FREE, addr)*/
    machine(17, _addr_1728);

    /** end procedure*/
    DeRef(_addr_1728);
    return;
    ;
}


void _14register_block(int _block_addr_1735, int _block_len_1736, int _protection_1737)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_protection_1737)) {
        _1 = (long)(DBL_PTR(_protection_1737)->dbl);
        if (UNIQUE(DBL_PTR(_protection_1737)) && (DBL_PTR(_protection_1737)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_protection_1737);
        _protection_1737 = _1;
    }

    /** end procedure*/
    return;
    ;
}


void _14unregister_block(int _block_addr_1741)
{
    int _0, _1, _2;
    

    /** end procedure*/
    return;
    ;
}


int _14safe_address(int _start_1745, int _len_1746, int _action_1747)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_len_1746)) {
        _1 = (long)(DBL_PTR(_len_1746)->dbl);
        if (UNIQUE(DBL_PTR(_len_1746)) && (DBL_PTR(_len_1746)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_len_1746);
        _len_1746 = _1;
    }

    /** 	return 1*/
    return 1;
    ;
}


void _14check_all_blocks()
{
    int _0, _1, _2;
    

    /** end procedure*/
    return;
    ;
}


int _14prepare_block(int _addr_1754, int _a_1755, int _protection_1756)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_a_1755)) {
        _1 = (long)(DBL_PTR(_a_1755)->dbl);
        if (UNIQUE(DBL_PTR(_a_1755)) && (DBL_PTR(_a_1755)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_a_1755);
        _a_1755 = _1;
    }
    if (!IS_ATOM_INT(_protection_1756)) {
        _1 = (long)(DBL_PTR(_protection_1756)->dbl);
        if (UNIQUE(DBL_PTR(_protection_1756)) && (DBL_PTR(_protection_1756)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_protection_1756);
        _protection_1756 = _1;
    }

    /** 	return addr*/
    return _addr_1754;
    ;
}


int _14bordered_address(int _addr_1764)
{
    int _797 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not atom(addr) then*/
    _797 = IS_ATOM(_addr_1764);
    if (_797 != 0)
    goto L1; // [6] 16
    _797 = NOVALUE;

    /** 		return 0*/
    DeRef(_addr_1764);
    return 0;
L1: 

    /** 	return 1*/
    DeRef(_addr_1764);
    return 1;
    ;
}


int _14dep_works()
{
    int _799 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		return (DEP_really_works and use_DEP)*/
    _799 = (_13DEP_really_works_1671 != 0 && 1 != 0);
    return _799;

    /** 	return 1*/
    _799 = NOVALUE;
    return 1;
    ;
}


void _14free_code(int _addr_1776, int _size_1777, int _wordsize_1779)
{
    int _803 = NOVALUE;
    int _802 = NOVALUE;
    int _801 = NOVALUE;
    int _800 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_size_1777)) {
        _1 = (long)(DBL_PTR(_size_1777)->dbl);
        if (UNIQUE(DBL_PTR(_size_1777)) && (DBL_PTR(_size_1777)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_1777);
        _size_1777 = _1;
    }

    /** 		if dep_works() then*/
    _800 = _14dep_works();
    if (_800 == 0) {
        DeRef(_800);
        _800 = NOVALUE;
        goto L1; // [8] 35
    }
    else {
        if (!IS_ATOM_INT(_800) && DBL_PTR(_800)->dbl == 0.0){
            DeRef(_800);
            _800 = NOVALUE;
            goto L1; // [8] 35
        }
        DeRef(_800);
        _800 = NOVALUE;
    }
    DeRef(_800);
    _800 = NOVALUE;

    /** 			c_func(VirtualFree_rid, { addr, size*wordsize, MEM_RELEASE })*/
    if (IS_ATOM_INT(_wordsize_1779)) {
        if (_size_1777 == (short)_size_1777 && _wordsize_1779 <= INT15 && _wordsize_1779 >= -INT15)
        _801 = _size_1777 * _wordsize_1779;
        else
        _801 = NewDouble(_size_1777 * (double)_wordsize_1779);
    }
    else {
        _801 = binary_op(MULTIPLY, _size_1777, _wordsize_1779);
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_addr_1776);
    *((int *)(_2+4)) = _addr_1776;
    *((int *)(_2+8)) = _801;
    *((int *)(_2+12)) = 32768;
    _802 = MAKE_SEQ(_1);
    _801 = NOVALUE;
    _803 = call_c(1, _14VirtualFree_rid_1773, _802);
    DeRefDS(_802);
    _802 = NOVALUE;
    goto L2; // [32] 41
L1: 

    /** 			machine_proc( memconst:M_FREE, addr)*/
    machine(17, _addr_1776);
L2: 

    /** end procedure*/
    DeRef(_addr_1776);
    DeRef(_wordsize_1779);
    DeRef(_803);
    _803 = NOVALUE;
    return;
    ;
}



// 0x36049002
