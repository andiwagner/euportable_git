// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _70RTInternal(int _msg_65974)
{
    int _0, _1, _2;
    

    /** 	machine_proc(67, msg)*/
    machine(67, _msg_65974);

    /** end procedure*/
    DeRefDS(_msg_65974);
    return;
    ;
}


int _70name_or_literal(int _sym_65981)
{
    int _33220 = NOVALUE;
    int _33219 = NOVALUE;
    int _33218 = NOVALUE;
    int _33217 = NOVALUE;
    int _33216 = NOVALUE;
    int _33214 = NOVALUE;
    int _33212 = NOVALUE;
    int _33211 = NOVALUE;
    int _33210 = NOVALUE;
    int _33209 = NOVALUE;
    int _33208 = NOVALUE;
    int _33207 = NOVALUE;
    int _33206 = NOVALUE;
    int _33205 = NOVALUE;
    int _33204 = NOVALUE;
    int _33203 = NOVALUE;
    int _33202 = NOVALUE;
    int _33199 = NOVALUE;
    int _33198 = NOVALUE;
    int _33197 = NOVALUE;
    int _33194 = NOVALUE;
    int _33193 = NOVALUE;
    int _33192 = NOVALUE;
    int _33189 = NOVALUE;
    int _33188 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_65981)) {
        _1 = (long)(DBL_PTR(_sym_65981)->dbl);
        if (UNIQUE(DBL_PTR(_sym_65981)) && (DBL_PTR(_sym_65981)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_65981);
        _sym_65981 = _1;
    }

    /** 	if not sym then*/
    if (_sym_65981 != 0)
    goto L1; // [5] 17

    /** 		return "[0: ???]"*/
    RefDS(_33185);
    return _33185;
    goto L2; // [14] 227
L1: 

    /** 	elsif sym < 0 then*/
    if (_sym_65981 >= 0)
    goto L3; // [19] 36

    /** 		return sprintf("[UNRESOLVED FORWARD REFERENCE: %d]", sym )*/
    _33188 = EPrintf(-9999999, _33187, _sym_65981);
    return _33188;
    goto L2; // [33] 227
L3: 

    /** 	elsif sym > length(SymTab) then*/
    if (IS_SEQUENCE(_26SymTab_11138)){
            _33189 = SEQ_PTR(_26SymTab_11138)->length;
    }
    else {
        _33189 = 1;
    }
    if (_sym_65981 <= _33189)
    goto L4; // [43] 60

    /** 		return sprintf("[_invalid_:%d]", sym )*/
    _33192 = EPrintf(-9999999, _33191, _sym_65981);
    DeRef(_33188);
    _33188 = NOVALUE;
    return _33192;
    goto L2; // [57] 227
L4: 

    /** 	elsif length(SymTab[sym]) = 1 then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _33193 = (int)*(((s1_ptr)_2)->base + _sym_65981);
    if (IS_SEQUENCE(_33193)){
            _33194 = SEQ_PTR(_33193)->length;
    }
    else {
        _33194 = 1;
    }
    _33193 = NOVALUE;
    if (_33194 != 1)
    goto L5; // [71] 88

    /** 		return sprintf("[_deleted_:%d]", sym)*/
    _33197 = EPrintf(-9999999, _33196, _sym_65981);
    DeRef(_33188);
    _33188 = NOVALUE;
    DeRef(_33192);
    _33192 = NOVALUE;
    _33193 = NOVALUE;
    return _33197;
    goto L2; // [85] 227
L5: 

    /** 	elsif length(SymTab[sym]) >= SIZEOF_VAR_ENTRY then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _33198 = (int)*(((s1_ptr)_2)->base + _sym_65981);
    if (IS_SEQUENCE(_33198)){
            _33199 = SEQ_PTR(_33198)->length;
    }
    else {
        _33199 = 1;
    }
    _33198 = NOVALUE;
    if (_33199 < _25SIZEOF_VAR_ENTRY_12041)
    goto L6; // [101] 134

    /** 		return sprintf("[%s:%d]", {SymTab[sym][S_NAME],sym})*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _33202 = (int)*(((s1_ptr)_2)->base + _sym_65981);
    _2 = (int)SEQ_PTR(_33202);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _33203 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _33203 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _33202 = NOVALUE;
    Ref(_33203);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33203;
    ((int *)_2)[2] = _sym_65981;
    _33204 = MAKE_SEQ(_1);
    _33203 = NOVALUE;
    _33205 = EPrintf(-9999999, _33201, _33204);
    DeRefDS(_33204);
    _33204 = NOVALUE;
    DeRef(_33188);
    _33188 = NOVALUE;
    DeRef(_33192);
    _33192 = NOVALUE;
    _33193 = NOVALUE;
    DeRef(_33197);
    _33197 = NOVALUE;
    _33198 = NOVALUE;
    return _33205;
    goto L2; // [131] 227
L6: 

    /** 	elsif SymTab[sym][S_MODE] = M_TEMP and equal( SymTab[sym][S_OBJ], NOVALUE ) then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _33206 = (int)*(((s1_ptr)_2)->base + _sym_65981);
    _2 = (int)SEQ_PTR(_33206);
    _33207 = (int)*(((s1_ptr)_2)->base + 3);
    _33206 = NOVALUE;
    if (IS_ATOM_INT(_33207)) {
        _33208 = (_33207 == 3);
    }
    else {
        _33208 = binary_op(EQUALS, _33207, 3);
    }
    _33207 = NOVALUE;
    if (IS_ATOM_INT(_33208)) {
        if (_33208 == 0) {
            goto L7; // [154] 193
        }
    }
    else {
        if (DBL_PTR(_33208)->dbl == 0.0) {
            goto L7; // [154] 193
        }
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _33210 = (int)*(((s1_ptr)_2)->base + _sym_65981);
    _2 = (int)SEQ_PTR(_33210);
    _33211 = (int)*(((s1_ptr)_2)->base + 1);
    _33210 = NOVALUE;
    if (_33211 == _25NOVALUE_12115)
    _33212 = 1;
    else if (IS_ATOM_INT(_33211) && IS_ATOM_INT(_25NOVALUE_12115))
    _33212 = 0;
    else
    _33212 = (compare(_33211, _25NOVALUE_12115) == 0);
    _33211 = NOVALUE;
    if (_33212 == 0)
    {
        _33212 = NOVALUE;
        goto L7; // [177] 193
    }
    else{
        _33212 = NOVALUE;
    }

    /** 		return sprintf("[_temp_:%d]", sym)*/
    _33214 = EPrintf(-9999999, _33213, _sym_65981);
    DeRef(_33188);
    _33188 = NOVALUE;
    DeRef(_33192);
    _33192 = NOVALUE;
    _33193 = NOVALUE;
    DeRef(_33197);
    _33197 = NOVALUE;
    _33198 = NOVALUE;
    DeRef(_33205);
    _33205 = NOVALUE;
    DeRef(_33208);
    _33208 = NOVALUE;
    return _33214;
    goto L2; // [190] 227
L7: 

    /** 		return sprintf("[LIT %s:%d]",{ pretty_sprint(SymTab[sym][S_OBJ], ps_options), sym})*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _33216 = (int)*(((s1_ptr)_2)->base + _sym_65981);
    _2 = (int)SEQ_PTR(_33216);
    _33217 = (int)*(((s1_ptr)_2)->base + 1);
    _33216 = NOVALUE;
    Ref(_33217);
    RefDS(_70ps_options_65975);
    _33218 = _23pretty_sprint(_33217, _70ps_options_65975);
    _33217 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33218;
    ((int *)_2)[2] = _sym_65981;
    _33219 = MAKE_SEQ(_1);
    _33218 = NOVALUE;
    _33220 = EPrintf(-9999999, _33215, _33219);
    DeRefDS(_33219);
    _33219 = NOVALUE;
    DeRef(_33188);
    _33188 = NOVALUE;
    DeRef(_33192);
    _33192 = NOVALUE;
    _33193 = NOVALUE;
    DeRef(_33197);
    _33197 = NOVALUE;
    _33198 = NOVALUE;
    DeRef(_33205);
    _33205 = NOVALUE;
    DeRef(_33214);
    _33214 = NOVALUE;
    DeRef(_33208);
    _33208 = NOVALUE;
    return _33220;
L2: 
    ;
}


int _70names(int _n_66043)
{
    int _nl_66044 = NOVALUE;
    int _33224 = NOVALUE;
    int _33223 = NOVALUE;
    int _33222 = NOVALUE;
    int _33221 = NOVALUE;
    int _0, _1, _2;
    

    /** 	nl = {}*/
    RefDS(_22682);
    DeRef(_nl_66044);
    _nl_66044 = _22682;

    /** 	for i = 1 to length(n) do*/
    if (IS_SEQUENCE(_n_66043)){
            _33221 = SEQ_PTR(_n_66043)->length;
    }
    else {
        _33221 = 1;
    }
    {
        int _i_66046;
        _i_66046 = 1;
L1: 
        if (_i_66046 > _33221){
            goto L2; // [15] 62
        }

        /** 		if n[i] then*/
        _2 = (int)SEQ_PTR(_n_66043);
        _33222 = (int)*(((s1_ptr)_2)->base + _i_66046);
        if (_33222 == 0) {
            _33222 = NOVALUE;
            goto L3; // [28] 48
        }
        else {
            if (!IS_ATOM_INT(_33222) && DBL_PTR(_33222)->dbl == 0.0){
                _33222 = NOVALUE;
                goto L3; // [28] 48
            }
            _33222 = NOVALUE;
        }
        _33222 = NOVALUE;

        /** 			nl = append( nl, name_or_literal(n[i]))*/
        _2 = (int)SEQ_PTR(_n_66043);
        _33223 = (int)*(((s1_ptr)_2)->base + _i_66046);
        Ref(_33223);
        _33224 = _70name_or_literal(_33223);
        _33223 = NOVALUE;
        Ref(_33224);
        Append(&_nl_66044, _nl_66044, _33224);
        DeRef(_33224);
        _33224 = NOVALUE;
        goto L4; // [45] 55
L3: 

        /** 			nl = append( nl, "[0]" )*/
        RefDS(_33226);
        Append(&_nl_66044, _nl_66044, _33226);
L4: 

        /** 	end for*/
        _i_66046 = _i_66046 + 1;
        goto L1; // [57] 22
L2: 
        ;
    }

    /** 	return nl*/
    DeRefDS(_n_66043);
    return _nl_66044;
    ;
}


void _70il(int _dsm_66058, int _len_66059)
{
    int _format_66060 = NOVALUE;
    int _code_66061 = NOVALUE;
    int _line_66062 = NOVALUE;
    int _space_66063 = NOVALUE;
    int _33279 = NOVALUE;
    int _33278 = NOVALUE;
    int _33277 = NOVALUE;
    int _33275 = NOVALUE;
    int _33271 = NOVALUE;
    int _33270 = NOVALUE;
    int _33269 = NOVALUE;
    int _33268 = NOVALUE;
    int _33267 = NOVALUE;
    int _33265 = NOVALUE;
    int _33262 = NOVALUE;
    int _33261 = NOVALUE;
    int _33259 = NOVALUE;
    int _33258 = NOVALUE;
    int _33257 = NOVALUE;
    int _33256 = NOVALUE;
    int _33254 = NOVALUE;
    int _33252 = NOVALUE;
    int _33251 = NOVALUE;
    int _33249 = NOVALUE;
    int _33248 = NOVALUE;
    int _33246 = NOVALUE;
    int _33245 = NOVALUE;
    int _33244 = NOVALUE;
    int _33238 = NOVALUE;
    int _33236 = NOVALUE;
    int _33234 = NOVALUE;
    int _33233 = NOVALUE;
    int _33232 = NOVALUE;
    int _33229 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_len_66059)) {
        _1 = (long)(DBL_PTR(_len_66059)->dbl);
        if (UNIQUE(DBL_PTR(_len_66059)) && (DBL_PTR(_len_66059)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_len_66059);
        _len_66059 = _1;
    }

    /** 	format = "%6d: %03d"*/
    RefDS(_33228);
    DeRefi(_format_66060);
    _format_66060 = _33228;

    /** 	for i = 1 to len do*/
    _33229 = _len_66059;
    {
        int _i_66066;
        _i_66066 = 1;
L1: 
        if (_i_66066 > _33229){
            goto L2; // [17] 37
        }

        /** 		format &= " %d"*/
        Concat((object_ptr)&_format_66060, _format_66060, _33230);

        /** 	end for*/
        _i_66066 = _i_66066 + 1;
        goto L1; // [32] 24
L2: 
        ;
    }

    /** 	code = sprintf( format, pc & Code[pc..$] )*/
    if (IS_SEQUENCE(_25Code_12355)){
            _33232 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _33232 = 1;
    }
    rhs_slice_target = (object_ptr)&_33233;
    RHS_Slice(_25Code_12355, _70pc_65963, _33232);
    Prepend(&_33234, _33233, _70pc_65963);
    DeRefDS(_33233);
    _33233 = NOVALUE;
    DeRefi(_code_66061);
    _code_66061 = EPrintf(-9999999, _format_66060, _33234);
    DeRefDS(_33234);
    _33234 = NOVALUE;

    /** 	line = 1*/
    _line_66062 = 1;

    /** 	while length(code) > 40 do*/
L3: 
    if (IS_SEQUENCE(_code_66061)){
            _33236 = SEQ_PTR(_code_66061)->length;
    }
    else {
        _33236 = 1;
    }
    if (_33236 <= 40)
    goto L4; // [74] 188

    /** 		for c = 40 to 1 by -1 do*/
    {
        int _c_66079;
        _c_66079 = 40;
L5: 
        if (_c_66079 < 1){
            goto L6; // [80] 183
        }

        /** 			if code[c] = 32 then*/
        _2 = (int)SEQ_PTR(_code_66061);
        _33238 = (int)*(((s1_ptr)_2)->base + _c_66079);
        if (_33238 != 32)
        goto L7; // [93] 176

        /** 				if line > 1 then*/
        if (_line_66062 <= 1)
        goto L8; // [99] 133

        /** 					code = "        " & code*/
        Concat((object_ptr)&_code_66061, _33241, _code_66061);

        /** 					printf( out, "\n%-40s # ", {"        " & code[1..c]})*/
        rhs_slice_target = (object_ptr)&_33244;
        RHS_Slice(_code_66061, 1, _c_66079);
        Concat((object_ptr)&_33245, _33241, _33244);
        DeRefDS(_33244);
        _33244 = NOVALUE;
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _33245;
        _33246 = MAKE_SEQ(_1);
        _33245 = NOVALUE;
        EPrintf(_70out_65962, _33243, _33246);
        DeRefDS(_33246);
        _33246 = NOVALUE;
        goto L9; // [130] 151
L8: 

        /** 					printf( out, "%-40s # ", {code[1..c]})*/
        rhs_slice_target = (object_ptr)&_33248;
        RHS_Slice(_code_66061, 1, _c_66079);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _33248;
        _33249 = MAKE_SEQ(_1);
        _33248 = NOVALUE;
        EPrintf(_70out_65962, _33247, _33249);
        DeRefDS(_33249);
        _33249 = NOVALUE;
L9: 

        /** 				line += 1*/
        _line_66062 = _line_66062 + 1;

        /** 				code = code[c+1..$]*/
        _33251 = _c_66079 + 1;
        if (IS_SEQUENCE(_code_66061)){
                _33252 = SEQ_PTR(_code_66061)->length;
        }
        else {
            _33252 = 1;
        }
        rhs_slice_target = (object_ptr)&_code_66061;
        RHS_Slice(_code_66061, _33251, _33252);

        /** 				exit*/
        goto L6; // [173] 183
L7: 

        /** 		end for*/
        _c_66079 = _c_66079 + -1;
        goto L5; // [178] 87
L6: 
        ;
    }

    /** 	end while*/
    goto L3; // [185] 71
L4: 

    /** 	if length(code) then*/
    if (IS_SEQUENCE(_code_66061)){
            _33254 = SEQ_PTR(_code_66061)->length;
    }
    else {
        _33254 = 1;
    }
    if (_33254 == 0)
    {
        _33254 = NOVALUE;
        goto LA; // [193] 235
    }
    else{
        _33254 = NOVALUE;
    }

    /** 		if line > 1 then*/
    if (_line_66062 <= 1)
    goto LB; // [198] 221

    /** 			printf( out, "\n%-40s # ", {"        " & code})*/
    Concat((object_ptr)&_33256, _33241, _code_66061);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _33256;
    _33257 = MAKE_SEQ(_1);
    _33256 = NOVALUE;
    EPrintf(_70out_65962, _33243, _33257);
    DeRefDS(_33257);
    _33257 = NOVALUE;
    goto LC; // [218] 234
LB: 

    /** 			printf( out, "%-40s # ", {code})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_code_66061);
    *((int *)(_2+4)) = _code_66061;
    _33258 = MAKE_SEQ(_1);
    EPrintf(_70out_65962, _33247, _33258);
    DeRefDS(_33258);
    _33258 = NOVALUE;
LC: 
LA: 

    /** 	line = 1*/
    _line_66062 = 1;

    /** 	while length(dsm) > 60 do*/
LD: 
    if (IS_SEQUENCE(_dsm_66058)){
            _33259 = SEQ_PTR(_dsm_66058)->length;
    }
    else {
        _33259 = 1;
    }
    if (_33259 <= 60)
    goto LE; // [248] 369

    /** 		space = 0*/
    _space_66063 = 0;

    /** 		for i = 60 to length(dsm) do*/
    if (IS_SEQUENCE(_dsm_66058)){
            _33261 = SEQ_PTR(_dsm_66058)->length;
    }
    else {
        _33261 = 1;
    }
    {
        int _i_66111;
        _i_66111 = 60;
LF: 
        if (_i_66111 > _33261){
            goto L10; // [262] 354
        }

        /** 			if dsm[i] = 32 then*/
        _2 = (int)SEQ_PTR(_dsm_66058);
        _33262 = (int)*(((s1_ptr)_2)->base + _i_66111);
        if (binary_op_a(NOTEQ, _33262, 32)){
            _33262 = NOVALUE;
            goto L11; // [275] 347
        }
        _33262 = NOVALUE;

        /** 				if line > 1 then*/
        if (_line_66062 <= 1)
        goto L12; // [281] 301

        /** 					puts( out, repeat( 32, 41 ) & "#     " )*/
        _33265 = Repeat(32, 41);
        Concat((object_ptr)&_33267, _33265, _33266);
        DeRefDS(_33265);
        _33265 = NOVALUE;
        DeRef(_33265);
        _33265 = NOVALUE;
        EPuts(_70out_65962, _33267); // DJP 
        DeRefDS(_33267);
        _33267 = NOVALUE;
L12: 

        /** 				puts( out, dsm[1..i] & "\n")*/
        rhs_slice_target = (object_ptr)&_33268;
        RHS_Slice(_dsm_66058, 1, _i_66111);
        Concat((object_ptr)&_33269, _33268, _22834);
        DeRefDS(_33268);
        _33268 = NOVALUE;
        DeRef(_33268);
        _33268 = NOVALUE;
        EPuts(_70out_65962, _33269); // DJP 
        DeRefDS(_33269);
        _33269 = NOVALUE;

        /** 				dsm = dsm[i+1..$]*/
        _33270 = _i_66111 + 1;
        if (IS_SEQUENCE(_dsm_66058)){
                _33271 = SEQ_PTR(_dsm_66058)->length;
        }
        else {
            _33271 = 1;
        }
        rhs_slice_target = (object_ptr)&_dsm_66058;
        RHS_Slice(_dsm_66058, _33270, _33271);

        /** 				line += 1*/
        _line_66062 = _line_66062 + 1;

        /** 				space = 1*/
        _space_66063 = 1;

        /** 				exit*/
        goto L10; // [344] 354
L11: 

        /** 		end for*/
        _i_66111 = _i_66111 + 1;
        goto LF; // [349] 269
L10: 
        ;
    }

    /** 		if not space then*/
    if (_space_66063 != 0)
    goto LD; // [356] 245

    /** 			exit*/
    goto LE; // [361] 369

    /** 	end while*/
    goto LD; // [366] 245
LE: 

    /** 	if length(dsm) then*/
    if (IS_SEQUENCE(_dsm_66058)){
            _33275 = SEQ_PTR(_dsm_66058)->length;
    }
    else {
        _33275 = 1;
    }
    if (_33275 == 0)
    {
        _33275 = NOVALUE;
        goto L13; // [374] 411
    }
    else{
        _33275 = NOVALUE;
    }

    /** 		if line > 1 then*/
    if (_line_66062 <= 1)
    goto L14; // [379] 399

    /** 			puts( out, repeat( 32, 41 ) & "#     " )*/
    _33277 = Repeat(32, 41);
    Concat((object_ptr)&_33278, _33277, _33266);
    DeRefDS(_33277);
    _33277 = NOVALUE;
    DeRef(_33277);
    _33277 = NOVALUE;
    EPuts(_70out_65962, _33278); // DJP 
    DeRefDS(_33278);
    _33278 = NOVALUE;
L14: 

    /** 		puts( out, dsm & "\n")*/
    Concat((object_ptr)&_33279, _dsm_66058, _22834);
    EPuts(_70out_65962, _33279); // DJP 
    DeRefDS(_33279);
    _33279 = NOVALUE;
L13: 

    /** end procedure*/
    DeRefDS(_dsm_66058);
    DeRefi(_format_66060);
    DeRefi(_code_66061);
    _33238 = NOVALUE;
    DeRef(_33251);
    _33251 = NOVALUE;
    DeRef(_33270);
    _33270 = NOVALUE;
    return;
    ;
}


void _70quadary()
{
    int _33289 = NOVALUE;
    int _33288 = NOVALUE;
    int _33287 = NOVALUE;
    int _33286 = NOVALUE;
    int _33285 = NOVALUE;
    int _33284 = NOVALUE;
    int _33283 = NOVALUE;
    int _33282 = NOVALUE;
    int _33281 = NOVALUE;
    int _0, _1, _2;
    

    /** 	il( sprintf( "%s: %s, %s, %s, %s => %s", {opnames[Code[pc]]} & names(Code[pc+1..pc+5])), 5)*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33281 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33281)){
        _33282 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33281)->dbl));
    }
    else{
        _33282 = (int)*(((s1_ptr)_2)->base + _33281);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_33282);
    *((int *)(_2+4)) = _33282;
    _33283 = MAKE_SEQ(_1);
    _33282 = NOVALUE;
    _33284 = _70pc_65963 + 1;
    if (_33284 > MAXINT){
        _33284 = NewDouble((double)_33284);
    }
    _33285 = _70pc_65963 + 5;
    rhs_slice_target = (object_ptr)&_33286;
    RHS_Slice(_25Code_12355, _33284, _33285);
    _33287 = _70names(_33286);
    _33286 = NOVALUE;
    if (IS_SEQUENCE(_33283) && IS_ATOM(_33287)) {
        Ref(_33287);
        Append(&_33288, _33283, _33287);
    }
    else if (IS_ATOM(_33283) && IS_SEQUENCE(_33287)) {
    }
    else {
        Concat((object_ptr)&_33288, _33283, _33287);
        DeRefDS(_33283);
        _33283 = NOVALUE;
    }
    DeRef(_33283);
    _33283 = NOVALUE;
    DeRef(_33287);
    _33287 = NOVALUE;
    _33289 = EPrintf(-9999999, _33280, _33288);
    DeRefDS(_33288);
    _33288 = NOVALUE;
    _70il(_33289, 5);
    _33289 = NOVALUE;

    /** 	pc += 6*/
    _70pc_65963 = _70pc_65963 + 6;

    /** end procedure*/
    _33281 = NOVALUE;
    DeRef(_33284);
    _33284 = NOVALUE;
    _33285 = NOVALUE;
    return;
    ;
}


void _70pquadary()
{
    int _33300 = NOVALUE;
    int _33299 = NOVALUE;
    int _33298 = NOVALUE;
    int _33297 = NOVALUE;
    int _33296 = NOVALUE;
    int _33295 = NOVALUE;
    int _33294 = NOVALUE;
    int _33293 = NOVALUE;
    int _33292 = NOVALUE;
    int _0, _1, _2;
    

    /** 	il( sprintf( "%s: %s, %s, %s %s", {opnames[Code[pc]]} & names(Code[pc+1..pc+4])), 4)*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33292 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33292)){
        _33293 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33292)->dbl));
    }
    else{
        _33293 = (int)*(((s1_ptr)_2)->base + _33292);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_33293);
    *((int *)(_2+4)) = _33293;
    _33294 = MAKE_SEQ(_1);
    _33293 = NOVALUE;
    _33295 = _70pc_65963 + 1;
    if (_33295 > MAXINT){
        _33295 = NewDouble((double)_33295);
    }
    _33296 = _70pc_65963 + 4;
    rhs_slice_target = (object_ptr)&_33297;
    RHS_Slice(_25Code_12355, _33295, _33296);
    _33298 = _70names(_33297);
    _33297 = NOVALUE;
    if (IS_SEQUENCE(_33294) && IS_ATOM(_33298)) {
        Ref(_33298);
        Append(&_33299, _33294, _33298);
    }
    else if (IS_ATOM(_33294) && IS_SEQUENCE(_33298)) {
    }
    else {
        Concat((object_ptr)&_33299, _33294, _33298);
        DeRefDS(_33294);
        _33294 = NOVALUE;
    }
    DeRef(_33294);
    _33294 = NOVALUE;
    DeRef(_33298);
    _33298 = NOVALUE;
    _33300 = EPrintf(-9999999, _33291, _33299);
    DeRefDS(_33299);
    _33299 = NOVALUE;
    _70il(_33300, 4);
    _33300 = NOVALUE;

    /** 	pc += 5*/
    _70pc_65963 = _70pc_65963 + 5;

    /** end procedure*/
    _33292 = NOVALUE;
    DeRef(_33295);
    _33295 = NOVALUE;
    _33296 = NOVALUE;
    return;
    ;
}


void _70trinary()
{
    int _33311 = NOVALUE;
    int _33310 = NOVALUE;
    int _33309 = NOVALUE;
    int _33308 = NOVALUE;
    int _33307 = NOVALUE;
    int _33306 = NOVALUE;
    int _33305 = NOVALUE;
    int _33304 = NOVALUE;
    int _33303 = NOVALUE;
    int _0, _1, _2;
    

    /** 	il( sprintf( "%s: %s, %s, %s => %s", {opnames[Code[pc]]} & names(Code[pc+1..pc+4])), 4)*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33303 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33303)){
        _33304 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33303)->dbl));
    }
    else{
        _33304 = (int)*(((s1_ptr)_2)->base + _33303);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_33304);
    *((int *)(_2+4)) = _33304;
    _33305 = MAKE_SEQ(_1);
    _33304 = NOVALUE;
    _33306 = _70pc_65963 + 1;
    if (_33306 > MAXINT){
        _33306 = NewDouble((double)_33306);
    }
    _33307 = _70pc_65963 + 4;
    rhs_slice_target = (object_ptr)&_33308;
    RHS_Slice(_25Code_12355, _33306, _33307);
    _33309 = _70names(_33308);
    _33308 = NOVALUE;
    if (IS_SEQUENCE(_33305) && IS_ATOM(_33309)) {
        Ref(_33309);
        Append(&_33310, _33305, _33309);
    }
    else if (IS_ATOM(_33305) && IS_SEQUENCE(_33309)) {
    }
    else {
        Concat((object_ptr)&_33310, _33305, _33309);
        DeRefDS(_33305);
        _33305 = NOVALUE;
    }
    DeRef(_33305);
    _33305 = NOVALUE;
    DeRef(_33309);
    _33309 = NOVALUE;
    _33311 = EPrintf(-9999999, _33302, _33310);
    DeRefDS(_33310);
    _33310 = NOVALUE;
    _70il(_33311, 4);
    _33311 = NOVALUE;

    /** 	pc += 5*/
    _70pc_65963 = _70pc_65963 + 5;

    /** end procedure*/
    _33303 = NOVALUE;
    DeRef(_33306);
    _33306 = NOVALUE;
    _33307 = NOVALUE;
    return;
    ;
}


void _70ptrinary()
{
    int _33322 = NOVALUE;
    int _33321 = NOVALUE;
    int _33320 = NOVALUE;
    int _33319 = NOVALUE;
    int _33318 = NOVALUE;
    int _33317 = NOVALUE;
    int _33316 = NOVALUE;
    int _33315 = NOVALUE;
    int _33314 = NOVALUE;
    int _0, _1, _2;
    

    /** 	il( sprintf( "%s: %s, %s, %s", {opnames[Code[pc]]} & names(Code[pc+1..pc+3])), 3)*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33314 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33314)){
        _33315 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33314)->dbl));
    }
    else{
        _33315 = (int)*(((s1_ptr)_2)->base + _33314);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_33315);
    *((int *)(_2+4)) = _33315;
    _33316 = MAKE_SEQ(_1);
    _33315 = NOVALUE;
    _33317 = _70pc_65963 + 1;
    if (_33317 > MAXINT){
        _33317 = NewDouble((double)_33317);
    }
    _33318 = _70pc_65963 + 3;
    rhs_slice_target = (object_ptr)&_33319;
    RHS_Slice(_25Code_12355, _33317, _33318);
    _33320 = _70names(_33319);
    _33319 = NOVALUE;
    if (IS_SEQUENCE(_33316) && IS_ATOM(_33320)) {
        Ref(_33320);
        Append(&_33321, _33316, _33320);
    }
    else if (IS_ATOM(_33316) && IS_SEQUENCE(_33320)) {
    }
    else {
        Concat((object_ptr)&_33321, _33316, _33320);
        DeRefDS(_33316);
        _33316 = NOVALUE;
    }
    DeRef(_33316);
    _33316 = NOVALUE;
    DeRef(_33320);
    _33320 = NOVALUE;
    _33322 = EPrintf(-9999999, _33313, _33321);
    DeRefDS(_33321);
    _33321 = NOVALUE;
    _70il(_33322, 3);
    _33322 = NOVALUE;

    /** 	pc += 4*/
    _70pc_65963 = _70pc_65963 + 4;

    /** end procedure*/
    _33314 = NOVALUE;
    DeRef(_33317);
    _33317 = NOVALUE;
    _33318 = NOVALUE;
    return;
    ;
}


void _70binary()
{
    int _33333 = NOVALUE;
    int _33332 = NOVALUE;
    int _33331 = NOVALUE;
    int _33330 = NOVALUE;
    int _33329 = NOVALUE;
    int _33328 = NOVALUE;
    int _33327 = NOVALUE;
    int _33326 = NOVALUE;
    int _33325 = NOVALUE;
    int _0, _1, _2;
    

    /** 	il( sprintf( "%s: %s, %s => %s", {opnames[Code[pc]]} & names(Code[pc+1..pc+3])), 3)*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33325 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33325)){
        _33326 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33325)->dbl));
    }
    else{
        _33326 = (int)*(((s1_ptr)_2)->base + _33325);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_33326);
    *((int *)(_2+4)) = _33326;
    _33327 = MAKE_SEQ(_1);
    _33326 = NOVALUE;
    _33328 = _70pc_65963 + 1;
    if (_33328 > MAXINT){
        _33328 = NewDouble((double)_33328);
    }
    _33329 = _70pc_65963 + 3;
    rhs_slice_target = (object_ptr)&_33330;
    RHS_Slice(_25Code_12355, _33328, _33329);
    _33331 = _70names(_33330);
    _33330 = NOVALUE;
    if (IS_SEQUENCE(_33327) && IS_ATOM(_33331)) {
        Ref(_33331);
        Append(&_33332, _33327, _33331);
    }
    else if (IS_ATOM(_33327) && IS_SEQUENCE(_33331)) {
    }
    else {
        Concat((object_ptr)&_33332, _33327, _33331);
        DeRefDS(_33327);
        _33327 = NOVALUE;
    }
    DeRef(_33327);
    _33327 = NOVALUE;
    DeRef(_33331);
    _33331 = NOVALUE;
    _33333 = EPrintf(-9999999, _33324, _33332);
    DeRefDS(_33332);
    _33332 = NOVALUE;
    _70il(_33333, 3);
    _33333 = NOVALUE;

    /** 	pc += 4*/
    _70pc_65963 = _70pc_65963 + 4;

    /** end procedure*/
    _33325 = NOVALUE;
    DeRef(_33328);
    _33328 = NOVALUE;
    _33329 = NOVALUE;
    return;
    ;
}


void _70pbinary()
{
    int _33344 = NOVALUE;
    int _33343 = NOVALUE;
    int _33342 = NOVALUE;
    int _33341 = NOVALUE;
    int _33340 = NOVALUE;
    int _33339 = NOVALUE;
    int _33338 = NOVALUE;
    int _33337 = NOVALUE;
    int _33336 = NOVALUE;
    int _0, _1, _2;
    

    /** 	il( sprintf( "%s: %s, %s", {opnames[Code[pc]]} & names(Code[pc+1..pc+2])), 2)*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33336 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33336)){
        _33337 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33336)->dbl));
    }
    else{
        _33337 = (int)*(((s1_ptr)_2)->base + _33336);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_33337);
    *((int *)(_2+4)) = _33337;
    _33338 = MAKE_SEQ(_1);
    _33337 = NOVALUE;
    _33339 = _70pc_65963 + 1;
    if (_33339 > MAXINT){
        _33339 = NewDouble((double)_33339);
    }
    _33340 = _70pc_65963 + 2;
    rhs_slice_target = (object_ptr)&_33341;
    RHS_Slice(_25Code_12355, _33339, _33340);
    _33342 = _70names(_33341);
    _33341 = NOVALUE;
    if (IS_SEQUENCE(_33338) && IS_ATOM(_33342)) {
        Ref(_33342);
        Append(&_33343, _33338, _33342);
    }
    else if (IS_ATOM(_33338) && IS_SEQUENCE(_33342)) {
    }
    else {
        Concat((object_ptr)&_33343, _33338, _33342);
        DeRefDS(_33338);
        _33338 = NOVALUE;
    }
    DeRef(_33338);
    _33338 = NOVALUE;
    DeRef(_33342);
    _33342 = NOVALUE;
    _33344 = EPrintf(-9999999, _33335, _33343);
    DeRefDS(_33343);
    _33343 = NOVALUE;
    _70il(_33344, 2);
    _33344 = NOVALUE;

    /** 	pc += 3*/
    _70pc_65963 = _70pc_65963 + 3;

    /** end procedure*/
    _33336 = NOVALUE;
    DeRef(_33339);
    _33339 = NOVALUE;
    _33340 = NOVALUE;
    return;
    ;
}


void _70unary()
{
    int _33355 = NOVALUE;
    int _33354 = NOVALUE;
    int _33353 = NOVALUE;
    int _33352 = NOVALUE;
    int _33351 = NOVALUE;
    int _33350 = NOVALUE;
    int _33349 = NOVALUE;
    int _33348 = NOVALUE;
    int _33347 = NOVALUE;
    int _0, _1, _2;
    

    /** 	il( sprintf( "%s: %s => %s", {opnames[Code[pc]]} & names(Code[pc+1..pc+2])), 2)*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33347 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33347)){
        _33348 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33347)->dbl));
    }
    else{
        _33348 = (int)*(((s1_ptr)_2)->base + _33347);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_33348);
    *((int *)(_2+4)) = _33348;
    _33349 = MAKE_SEQ(_1);
    _33348 = NOVALUE;
    _33350 = _70pc_65963 + 1;
    if (_33350 > MAXINT){
        _33350 = NewDouble((double)_33350);
    }
    _33351 = _70pc_65963 + 2;
    rhs_slice_target = (object_ptr)&_33352;
    RHS_Slice(_25Code_12355, _33350, _33351);
    _33353 = _70names(_33352);
    _33352 = NOVALUE;
    if (IS_SEQUENCE(_33349) && IS_ATOM(_33353)) {
        Ref(_33353);
        Append(&_33354, _33349, _33353);
    }
    else if (IS_ATOM(_33349) && IS_SEQUENCE(_33353)) {
    }
    else {
        Concat((object_ptr)&_33354, _33349, _33353);
        DeRefDS(_33349);
        _33349 = NOVALUE;
    }
    DeRef(_33349);
    _33349 = NOVALUE;
    DeRef(_33353);
    _33353 = NOVALUE;
    _33355 = EPrintf(-9999999, _33346, _33354);
    DeRefDS(_33354);
    _33354 = NOVALUE;
    _70il(_33355, 2);
    _33355 = NOVALUE;

    /** 	pc += 3*/
    _70pc_65963 = _70pc_65963 + 3;

    /** end procedure*/
    _33347 = NOVALUE;
    DeRef(_33350);
    _33350 = NOVALUE;
    _33351 = NOVALUE;
    return;
    ;
}


void _70punary()
{
    int _33366 = NOVALUE;
    int _33365 = NOVALUE;
    int _33364 = NOVALUE;
    int _33363 = NOVALUE;
    int _33362 = NOVALUE;
    int _33361 = NOVALUE;
    int _33360 = NOVALUE;
    int _33359 = NOVALUE;
    int _33358 = NOVALUE;
    int _0, _1, _2;
    

    /** 	il( sprintf( "%s: %s ", {opnames[Code[pc]]} & names(Code[pc+1..pc+1])), 1)*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33358 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33358)){
        _33359 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33358)->dbl));
    }
    else{
        _33359 = (int)*(((s1_ptr)_2)->base + _33358);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_33359);
    *((int *)(_2+4)) = _33359;
    _33360 = MAKE_SEQ(_1);
    _33359 = NOVALUE;
    _33361 = _70pc_65963 + 1;
    if (_33361 > MAXINT){
        _33361 = NewDouble((double)_33361);
    }
    _33362 = _70pc_65963 + 1;
    rhs_slice_target = (object_ptr)&_33363;
    RHS_Slice(_25Code_12355, _33361, _33362);
    _33364 = _70names(_33363);
    _33363 = NOVALUE;
    if (IS_SEQUENCE(_33360) && IS_ATOM(_33364)) {
        Ref(_33364);
        Append(&_33365, _33360, _33364);
    }
    else if (IS_ATOM(_33360) && IS_SEQUENCE(_33364)) {
    }
    else {
        Concat((object_ptr)&_33365, _33360, _33364);
        DeRefDS(_33360);
        _33360 = NOVALUE;
    }
    DeRef(_33360);
    _33360 = NOVALUE;
    DeRef(_33364);
    _33364 = NOVALUE;
    _33366 = EPrintf(-9999999, _33357, _33365);
    DeRefDS(_33365);
    _33365 = NOVALUE;
    _70il(_33366, 1);
    _33366 = NOVALUE;

    /** 	pc += 2*/
    _70pc_65963 = _70pc_65963 + 2;

    /** end procedure*/
    _33358 = NOVALUE;
    DeRef(_33361);
    _33361 = NOVALUE;
    _33362 = NOVALUE;
    return;
    ;
}


void _70nonary()
{
    int _33375 = NOVALUE;
    int _33374 = NOVALUE;
    int _33373 = NOVALUE;
    int _33372 = NOVALUE;
    int _33371 = NOVALUE;
    int _33370 = NOVALUE;
    int _33369 = NOVALUE;
    int _0, _1, _2;
    

    /** 	il( sprintf( "%s: %s", {opnames[Code[pc]], name_or_literal( Code[pc+1])}), 1)*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33369 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33369)){
        _33370 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33369)->dbl));
    }
    else{
        _33370 = (int)*(((s1_ptr)_2)->base + _33369);
    }
    _33371 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33372 = (int)*(((s1_ptr)_2)->base + _33371);
    Ref(_33372);
    _33373 = _70name_or_literal(_33372);
    _33372 = NOVALUE;
    RefDS(_33370);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33370;
    ((int *)_2)[2] = _33373;
    _33374 = MAKE_SEQ(_1);
    _33373 = NOVALUE;
    _33370 = NOVALUE;
    _33375 = EPrintf(-9999999, _33368, _33374);
    DeRefDS(_33374);
    _33374 = NOVALUE;
    _70il(_33375, 1);
    _33375 = NOVALUE;

    /** 	pc += 2*/
    _70pc_65963 = _70pc_65963 + 2;

    /** end procedure*/
    _33369 = NOVALUE;
    _33371 = NOVALUE;
    return;
    ;
}


void _70pnonary()
{
    int _33381 = NOVALUE;
    int _33380 = NOVALUE;
    int _33379 = NOVALUE;
    int _33378 = NOVALUE;
    int _0, _1, _2;
    

    /** 	il( sprintf( "%s:", {opnames[Code[pc]]}), 0)*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33378 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33378)){
        _33379 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33378)->dbl));
    }
    else{
        _33379 = (int)*(((s1_ptr)_2)->base + _33378);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_33379);
    *((int *)(_2+4)) = _33379;
    _33380 = MAKE_SEQ(_1);
    _33379 = NOVALUE;
    _33381 = EPrintf(-9999999, _33377, _33380);
    DeRefDS(_33380);
    _33380 = NOVALUE;
    _70il(_33381, 0);
    _33381 = NOVALUE;

    /** 	pc += 1*/
    _70pc_65963 = _70pc_65963 + 1;

    /** end procedure*/
    _33378 = NOVALUE;
    return;
    ;
}


void _70opCOVERAGE_LINE()
{
    int _line_66290 = NOVALUE;
    int _entry__66291 = NOVALUE;
    int _lx_66292 = NOVALUE;
    int _sub_66297 = NOVALUE;
    int _33418 = NOVALUE;
    int _33417 = NOVALUE;
    int _33416 = NOVALUE;
    int _33415 = NOVALUE;
    int _33414 = NOVALUE;
    int _33413 = NOVALUE;
    int _33412 = NOVALUE;
    int _33409 = NOVALUE;
    int _33408 = NOVALUE;
    int _33406 = NOVALUE;
    int _33405 = NOVALUE;
    int _33404 = NOVALUE;
    int _33403 = NOVALUE;
    int _33400 = NOVALUE;
    int _33399 = NOVALUE;
    int _33398 = NOVALUE;
    int _33397 = NOVALUE;
    int _33396 = NOVALUE;
    int _33394 = NOVALUE;
    int _33393 = NOVALUE;
    int _33392 = NOVALUE;
    int _33391 = NOVALUE;
    int _33389 = NOVALUE;
    int _33388 = NOVALUE;
    int _33387 = NOVALUE;
    int _33385 = NOVALUE;
    int _33383 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	lx = Code[pc+1]*/
    _33383 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _lx_66292 = (int)*(((s1_ptr)_2)->base + _33383);
    if (!IS_ATOM_INT(_lx_66292)){
        _lx_66292 = (long)DBL_PTR(_lx_66292)->dbl;
    }

    /** 	symtab_index sub = Code[pc+2]*/
    _33385 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _sub_66297 = (int)*(((s1_ptr)_2)->base + _33385);
    if (!IS_ATOM_INT(_sub_66297)){
        _sub_66297 = (long)DBL_PTR(_sub_66297)->dbl;
    }

    /** 	if atom(slist[$]) then*/
    if (IS_SEQUENCE(_25slist_12357)){
            _33387 = SEQ_PTR(_25slist_12357)->length;
    }
    else {
        _33387 = 1;
    }
    _2 = (int)SEQ_PTR(_25slist_12357);
    _33388 = (int)*(((s1_ptr)_2)->base + _33387);
    _33389 = IS_ATOM(_33388);
    _33388 = NOVALUE;
    if (_33389 == 0)
    {
        _33389 = NOVALUE;
        goto L1; // [47] 61
    }
    else{
        _33389 = NOVALUE;
    }

    /** 		slist = s_expand( slist )*/
    RefDS(_25slist_12357);
    _0 = _60s_expand(_25slist_12357);
    DeRefDS(_25slist_12357);
    _25slist_12357 = _0;
L1: 

    /** 	if atom(slist[lx][SRC]) then*/
    _2 = (int)SEQ_PTR(_25slist_12357);
    _33391 = (int)*(((s1_ptr)_2)->base + _lx_66292);
    _2 = (int)SEQ_PTR(_33391);
    _33392 = (int)*(((s1_ptr)_2)->base + 1);
    _33391 = NOVALUE;
    _33393 = IS_ATOM(_33392);
    _33392 = NOVALUE;
    if (_33393 == 0)
    {
        _33393 = NOVALUE;
        goto L2; // [78] 113
    }
    else{
        _33393 = NOVALUE;
    }

    /** 		slist[lx][SRC] = fetch_line(slist[lx][SRC])*/
    _2 = (int)SEQ_PTR(_25slist_12357);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _25slist_12357 = MAKE_SEQ(_2);
    }
    _3 = (int)(_lx_66292 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_25slist_12357);
    _33396 = (int)*(((s1_ptr)_2)->base + _lx_66292);
    _2 = (int)SEQ_PTR(_33396);
    _33397 = (int)*(((s1_ptr)_2)->base + 1);
    _33396 = NOVALUE;
    Ref(_33397);
    _33398 = _60fetch_line(_33397);
    _33397 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _33398;
    if( _1 != _33398 ){
        DeRef(_1);
    }
    _33398 = NOVALUE;
    _33394 = NOVALUE;
L2: 

    /** 	entry_ = slist[Code[pc+1]]*/
    _33399 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33400 = (int)*(((s1_ptr)_2)->base + _33399);
    DeRef(_entry__66291);
    _2 = (int)SEQ_PTR(_25slist_12357);
    if (!IS_ATOM_INT(_33400)){
        _entry__66291 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33400)->dbl));
    }
    else{
        _entry__66291 = (int)*(((s1_ptr)_2)->base + _33400);
    }
    Ref(_entry__66291);

    /** 	line = entry_[SRC]*/
    DeRef(_line_66290);
    _2 = (int)SEQ_PTR(_entry__66291);
    _line_66290 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_line_66290);

    /** 	if atom(line) then*/
    _33403 = IS_ATOM(_line_66290);
    if (_33403 == 0)
    {
        _33403 = NOVALUE;
        goto L3; // [148] 159
    }
    else{
        _33403 = NOVALUE;
    }

    /** 		line = ""*/
    RefDS(_22682);
    DeRef(_line_66290);
    _line_66290 = _22682;
    goto L4; // [156] 200
L3: 

    /** 		while length(line) and find( line[1], "\t " ) do*/
L5: 
    if (IS_SEQUENCE(_line_66290)){
            _33404 = SEQ_PTR(_line_66290)->length;
    }
    else {
        _33404 = 1;
    }
    if (_33404 == 0) {
        goto L6; // [167] 199
    }
    _2 = (int)SEQ_PTR(_line_66290);
    _33406 = (int)*(((s1_ptr)_2)->base + 1);
    _33408 = find_from(_33406, _33407, 1);
    _33406 = NOVALUE;
    if (_33408 == 0)
    {
        _33408 = NOVALUE;
        goto L6; // [181] 199
    }
    else{
        _33408 = NOVALUE;
    }

    /** 			line = line[2..$]*/
    if (IS_SEQUENCE(_line_66290)){
            _33409 = SEQ_PTR(_line_66290)->length;
    }
    else {
        _33409 = 1;
    }
    rhs_slice_target = (object_ptr)&_line_66290;
    RHS_Slice(_line_66290, 2, _33409);

    /** 		end while*/
    goto L5; // [196] 164
L6: 
L4: 

    /** 	il( sprintf( "%s: %s:(%d)<<%s>>", {opnames[Code[pc]],  known_files[entry_[LOCAL_FILE_NO]], entry_[LINE] ,line}), 1)*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33412 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33412)){
        _33413 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33412)->dbl));
    }
    else{
        _33413 = (int)*(((s1_ptr)_2)->base + _33412);
    }
    _2 = (int)SEQ_PTR(_entry__66291);
    _33414 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_26known_files_11139);
    if (!IS_ATOM_INT(_33414)){
        _33415 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33414)->dbl));
    }
    else{
        _33415 = (int)*(((s1_ptr)_2)->base + _33414);
    }
    _2 = (int)SEQ_PTR(_entry__66291);
    _33416 = (int)*(((s1_ptr)_2)->base + 2);
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_33413);
    *((int *)(_2+4)) = _33413;
    Ref(_33415);
    *((int *)(_2+8)) = _33415;
    Ref(_33416);
    *((int *)(_2+12)) = _33416;
    Ref(_line_66290);
    *((int *)(_2+16)) = _line_66290;
    _33417 = MAKE_SEQ(_1);
    _33416 = NOVALUE;
    _33415 = NOVALUE;
    _33413 = NOVALUE;
    _33418 = EPrintf(-9999999, _33411, _33417);
    DeRefDS(_33417);
    _33417 = NOVALUE;
    _70il(_33418, 1);
    _33418 = NOVALUE;

    /** 	pc += 2*/
    _70pc_65963 = _70pc_65963 + 2;

    /** end procedure*/
    DeRef(_line_66290);
    DeRefDS(_entry__66291);
    DeRef(_33383);
    _33383 = NOVALUE;
    DeRef(_33385);
    _33385 = NOVALUE;
    DeRef(_33399);
    _33399 = NOVALUE;
    _33400 = NOVALUE;
    _33412 = NOVALUE;
    _33414 = NOVALUE;
    return;
    ;
}


void _70opCOVERAGE_ROUTINE()
{
    int _33433 = NOVALUE;
    int _33432 = NOVALUE;
    int _33431 = NOVALUE;
    int _33430 = NOVALUE;
    int _33429 = NOVALUE;
    int _33428 = NOVALUE;
    int _33427 = NOVALUE;
    int _33426 = NOVALUE;
    int _33425 = NOVALUE;
    int _33424 = NOVALUE;
    int _33423 = NOVALUE;
    int _33422 = NOVALUE;
    int _33421 = NOVALUE;
    int _0, _1, _2;
    

    /** 	il( sprintf( "%s: %s:%s\n",*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33421 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33421)){
        _33422 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33421)->dbl));
    }
    else{
        _33422 = (int)*(((s1_ptr)_2)->base + _33421);
    }
    _33423 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33424 = (int)*(((s1_ptr)_2)->base + _33423);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_33424)){
        _33425 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33424)->dbl));
    }
    else{
        _33425 = (int)*(((s1_ptr)_2)->base + _33424);
    }
    _2 = (int)SEQ_PTR(_33425);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _33426 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _33426 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _33425 = NOVALUE;
    _2 = (int)SEQ_PTR(_26known_files_11139);
    if (!IS_ATOM_INT(_33426)){
        _33427 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33426)->dbl));
    }
    else{
        _33427 = (int)*(((s1_ptr)_2)->base + _33426);
    }
    Ref(_33427);
    _33428 = _9canonical_path(_33427, 0, 0);
    _33427 = NOVALUE;
    _33429 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33430 = (int)*(((s1_ptr)_2)->base + _33429);
    Ref(_33430);
    _33431 = _52sym_name(_33430);
    _33430 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_33422);
    *((int *)(_2+4)) = _33422;
    *((int *)(_2+8)) = _33428;
    *((int *)(_2+12)) = _33431;
    _33432 = MAKE_SEQ(_1);
    _33431 = NOVALUE;
    _33428 = NOVALUE;
    _33422 = NOVALUE;
    _33433 = EPrintf(-9999999, _33420, _33432);
    DeRefDS(_33432);
    _33432 = NOVALUE;
    _70il(_33433, 1);
    _33433 = NOVALUE;

    /** 	pc += 2*/
    _70pc_65963 = _70pc_65963 + 2;

    /** end procedure*/
    _33421 = NOVALUE;
    _33423 = NOVALUE;
    _33424 = NOVALUE;
    _33426 = NOVALUE;
    _33429 = NOVALUE;
    return;
    ;
}


void _70opSTARTLINE()
{
    int _line_66386 = NOVALUE;
    int _entry__66387 = NOVALUE;
    int _lx_66388 = NOVALUE;
    int _33467 = NOVALUE;
    int _33466 = NOVALUE;
    int _33465 = NOVALUE;
    int _33464 = NOVALUE;
    int _33463 = NOVALUE;
    int _33462 = NOVALUE;
    int _33461 = NOVALUE;
    int _33458 = NOVALUE;
    int _33457 = NOVALUE;
    int _33456 = NOVALUE;
    int _33455 = NOVALUE;
    int _33454 = NOVALUE;
    int _33453 = NOVALUE;
    int _33450 = NOVALUE;
    int _33449 = NOVALUE;
    int _33448 = NOVALUE;
    int _33447 = NOVALUE;
    int _33446 = NOVALUE;
    int _33444 = NOVALUE;
    int _33443 = NOVALUE;
    int _33442 = NOVALUE;
    int _33441 = NOVALUE;
    int _33439 = NOVALUE;
    int _33438 = NOVALUE;
    int _33437 = NOVALUE;
    int _33435 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	lx = Code[pc+1]*/
    _33435 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _lx_66388 = (int)*(((s1_ptr)_2)->base + _33435);
    if (!IS_ATOM_INT(_lx_66388)){
        _lx_66388 = (long)DBL_PTR(_lx_66388)->dbl;
    }

    /** 	if atom(slist[$]) then*/
    if (IS_SEQUENCE(_25slist_12357)){
            _33437 = SEQ_PTR(_25slist_12357)->length;
    }
    else {
        _33437 = 1;
    }
    _2 = (int)SEQ_PTR(_25slist_12357);
    _33438 = (int)*(((s1_ptr)_2)->base + _33437);
    _33439 = IS_ATOM(_33438);
    _33438 = NOVALUE;
    if (_33439 == 0)
    {
        _33439 = NOVALUE;
        goto L1; // [31] 45
    }
    else{
        _33439 = NOVALUE;
    }

    /** 		slist = s_expand( slist )*/
    RefDS(_25slist_12357);
    _0 = _60s_expand(_25slist_12357);
    DeRefDS(_25slist_12357);
    _25slist_12357 = _0;
L1: 

    /** 	if atom(slist[lx][SRC]) then*/
    _2 = (int)SEQ_PTR(_25slist_12357);
    _33441 = (int)*(((s1_ptr)_2)->base + _lx_66388);
    _2 = (int)SEQ_PTR(_33441);
    _33442 = (int)*(((s1_ptr)_2)->base + 1);
    _33441 = NOVALUE;
    _33443 = IS_ATOM(_33442);
    _33442 = NOVALUE;
    if (_33443 == 0)
    {
        _33443 = NOVALUE;
        goto L2; // [62] 97
    }
    else{
        _33443 = NOVALUE;
    }

    /** 		slist[lx][SRC] = fetch_line(slist[lx][SRC])*/
    _2 = (int)SEQ_PTR(_25slist_12357);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _25slist_12357 = MAKE_SEQ(_2);
    }
    _3 = (int)(_lx_66388 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_25slist_12357);
    _33446 = (int)*(((s1_ptr)_2)->base + _lx_66388);
    _2 = (int)SEQ_PTR(_33446);
    _33447 = (int)*(((s1_ptr)_2)->base + 1);
    _33446 = NOVALUE;
    Ref(_33447);
    _33448 = _60fetch_line(_33447);
    _33447 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _33448;
    if( _1 != _33448 ){
        DeRef(_1);
    }
    _33448 = NOVALUE;
    _33444 = NOVALUE;
L2: 

    /** 	entry_ = slist[Code[pc+1]]*/
    _33449 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33450 = (int)*(((s1_ptr)_2)->base + _33449);
    DeRef(_entry__66387);
    _2 = (int)SEQ_PTR(_25slist_12357);
    if (!IS_ATOM_INT(_33450)){
        _entry__66387 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33450)->dbl));
    }
    else{
        _entry__66387 = (int)*(((s1_ptr)_2)->base + _33450);
    }
    Ref(_entry__66387);

    /** 	line = entry_[SRC]*/
    DeRef(_line_66386);
    _2 = (int)SEQ_PTR(_entry__66387);
    _line_66386 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_line_66386);

    /** 	if atom(line) then*/
    _33453 = IS_ATOM(_line_66386);
    if (_33453 == 0)
    {
        _33453 = NOVALUE;
        goto L3; // [132] 143
    }
    else{
        _33453 = NOVALUE;
    }

    /** 		line = ""*/
    RefDS(_22682);
    DeRef(_line_66386);
    _line_66386 = _22682;
    goto L4; // [140] 184
L3: 

    /** 		while length(line) and find( line[1], "\t " ) do*/
L5: 
    if (IS_SEQUENCE(_line_66386)){
            _33454 = SEQ_PTR(_line_66386)->length;
    }
    else {
        _33454 = 1;
    }
    if (_33454 == 0) {
        goto L6; // [151] 183
    }
    _2 = (int)SEQ_PTR(_line_66386);
    _33456 = (int)*(((s1_ptr)_2)->base + 1);
    _33457 = find_from(_33456, _33407, 1);
    _33456 = NOVALUE;
    if (_33457 == 0)
    {
        _33457 = NOVALUE;
        goto L6; // [165] 183
    }
    else{
        _33457 = NOVALUE;
    }

    /** 			line = line[2..$]*/
    if (IS_SEQUENCE(_line_66386)){
            _33458 = SEQ_PTR(_line_66386)->length;
    }
    else {
        _33458 = 1;
    }
    rhs_slice_target = (object_ptr)&_line_66386;
    RHS_Slice(_line_66386, 2, _33458);

    /** 		end while*/
    goto L5; // [180] 148
L6: 
L4: 

    /** 	il( sprintf( "%s: %s(%d)<<%s>>", {opnames[Code[pc]], known_files[entry_[LOCAL_FILE_NO]],entry_[LINE] ,line}), 1)*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33461 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33461)){
        _33462 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33461)->dbl));
    }
    else{
        _33462 = (int)*(((s1_ptr)_2)->base + _33461);
    }
    _2 = (int)SEQ_PTR(_entry__66387);
    _33463 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_26known_files_11139);
    if (!IS_ATOM_INT(_33463)){
        _33464 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33463)->dbl));
    }
    else{
        _33464 = (int)*(((s1_ptr)_2)->base + _33463);
    }
    _2 = (int)SEQ_PTR(_entry__66387);
    _33465 = (int)*(((s1_ptr)_2)->base + 2);
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_33462);
    *((int *)(_2+4)) = _33462;
    Ref(_33464);
    *((int *)(_2+8)) = _33464;
    Ref(_33465);
    *((int *)(_2+12)) = _33465;
    Ref(_line_66386);
    *((int *)(_2+16)) = _line_66386;
    _33466 = MAKE_SEQ(_1);
    _33465 = NOVALUE;
    _33464 = NOVALUE;
    _33462 = NOVALUE;
    _33467 = EPrintf(-9999999, _33460, _33466);
    DeRefDS(_33466);
    _33466 = NOVALUE;
    _70il(_33467, 1);
    _33467 = NOVALUE;

    /** 	pc += 2*/
    _70pc_65963 = _70pc_65963 + 2;

    /** end procedure*/
    DeRef(_line_66386);
    DeRefDS(_entry__66387);
    DeRef(_33435);
    _33435 = NOVALUE;
    DeRef(_33449);
    _33449 = NOVALUE;
    _33450 = NOVALUE;
    _33461 = NOVALUE;
    _33463 = NOVALUE;
    return;
    ;
}


void _70opREF_TEMP()
{
    int _0, _1, _2;
    

    /** 	punary()*/
    _70punary();

    /** end procedure*/
    return;
    ;
}


void _70opDEREF_TEMP()
{
    int _0, _1, _2;
    

    /** 	punary()*/
    _70punary();

    /** end procedure*/
    return;
    ;
}


void _70opNOVALUE_TEMP()
{
    int _0, _1, _2;
    

    /** 	punary()*/
    _70punary();

    /** end procedure*/
    return;
    ;
}


void _70opTASK_YIELD()
{
    int _0, _1, _2;
    

    /** 	pnonary()*/
    _70pnonary();

    /** end procedure*/
    return;
    ;
}


void _70opTASK_STATUS()
{
    int _0, _1, _2;
    

    /** 	unary()*/
    _70unary();

    /** end procedure*/
    return;
    ;
}


void _70opTASK_LIST()
{
    int _0, _1, _2;
    

    /** 	nonary()*/
    _70nonary();

    /** end procedure*/
    return;
    ;
}


void _70opTASK_SELF()
{
    int _0, _1, _2;
    

    /** 	nonary()*/
    _70nonary();

    /** end procedure*/
    return;
    ;
}


void _70opTASK_CLOCK_STOP()
{
    int _0, _1, _2;
    

    /** 	pnonary()*/
    _70pnonary();

    /** end procedure*/
    return;
    ;
}


void _70opTASK_CLOCK_START()
{
    int _0, _1, _2;
    

    /** 	pnonary()*/
    _70pnonary();

    /** end procedure*/
    return;
    ;
}


void _70opTASK_SUSPEND()
{
    int _0, _1, _2;
    

    /** 	punary()*/
    _70punary();

    /** end procedure*/
    return;
    ;
}


void _70opTASK_CREATE()
{
    int _0, _1, _2;
    

    /** 	binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opTASK_SCHEDULE()
{
    int _0, _1, _2;
    

    /** 	pbinary()*/
    _70pbinary();

    /** end procedure*/
    return;
    ;
}


void _70opEMBEDDED_PROCEDURE_CALL()
{
    int _0, _1, _2;
    

    /** 	pbinary()*/
    _70pbinary();

    /** end procedure*/
    return;
    ;
}


void _70opEMBEDDED_FUNCTION_CALL()
{
    int _0, _1, _2;
    

    /** 	binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


int _70find_line(int _sub_66480, int _pc_66481, int _file_only_66482)
{
    int _linetab_66483 = NOVALUE;
    int _line_66484 = NOVALUE;
    int _gline_66485 = NOVALUE;
    int _33503 = NOVALUE;
    int _33502 = NOVALUE;
    int _33501 = NOVALUE;
    int _33500 = NOVALUE;
    int _33499 = NOVALUE;
    int _33498 = NOVALUE;
    int _33497 = NOVALUE;
    int _33496 = NOVALUE;
    int _33495 = NOVALUE;
    int _33492 = NOVALUE;
    int _33490 = NOVALUE;
    int _33489 = NOVALUE;
    int _33488 = NOVALUE;
    int _33487 = NOVALUE;
    int _33485 = NOVALUE;
    int _33484 = NOVALUE;
    int _33483 = NOVALUE;
    int _33481 = NOVALUE;
    int _33480 = NOVALUE;
    int _33479 = NOVALUE;
    int _33478 = NOVALUE;
    int _33476 = NOVALUE;
    int _33475 = NOVALUE;
    int _33473 = NOVALUE;
    int _33472 = NOVALUE;
    int _33471 = NOVALUE;
    int _33469 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sub_66480)) {
        _1 = (long)(DBL_PTR(_sub_66480)->dbl);
        if (UNIQUE(DBL_PTR(_sub_66480)) && (DBL_PTR(_sub_66480)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sub_66480);
        _sub_66480 = _1;
    }
    if (!IS_ATOM_INT(_pc_66481)) {
        _1 = (long)(DBL_PTR(_pc_66481)->dbl);
        if (UNIQUE(DBL_PTR(_pc_66481)) && (DBL_PTR(_pc_66481)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pc_66481);
        _pc_66481 = _1;
    }
    if (!IS_ATOM_INT(_file_only_66482)) {
        _1 = (long)(DBL_PTR(_file_only_66482)->dbl);
        if (UNIQUE(DBL_PTR(_file_only_66482)) && (DBL_PTR(_file_only_66482)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_file_only_66482);
        _file_only_66482 = _1;
    }

    /** 	linetab = SymTab[sub][S_LINETAB]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _33469 = (int)*(((s1_ptr)_2)->base + _sub_66480);
    DeRef(_linetab_66483);
    _2 = (int)SEQ_PTR(_33469);
    if (!IS_ATOM_INT(_25S_LINETAB_11948)){
        _linetab_66483 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_LINETAB_11948)->dbl));
    }
    else{
        _linetab_66483 = (int)*(((s1_ptr)_2)->base + _25S_LINETAB_11948);
    }
    Ref(_linetab_66483);
    _33469 = NOVALUE;

    /** 	line = 1*/
    _line_66484 = 1;

    /** 	for i = 1 to length(linetab) do*/
    if (IS_SEQUENCE(_linetab_66483)){
            _33471 = SEQ_PTR(_linetab_66483)->length;
    }
    else {
        _33471 = 1;
    }
    {
        int _i_66491;
        _i_66491 = 1;
L1: 
        if (_i_66491 > _33471){
            goto L2; // [33] 121
        }

        /** 		if linetab[i] >= pc or linetab[i] = -2 then*/
        _2 = (int)SEQ_PTR(_linetab_66483);
        _33472 = (int)*(((s1_ptr)_2)->base + _i_66491);
        if (IS_ATOM_INT(_33472)) {
            _33473 = (_33472 >= _pc_66481);
        }
        else {
            _33473 = binary_op(GREATEREQ, _33472, _pc_66481);
        }
        _33472 = NOVALUE;
        if (IS_ATOM_INT(_33473)) {
            if (_33473 != 0) {
                goto L3; // [50] 67
            }
        }
        else {
            if (DBL_PTR(_33473)->dbl != 0.0) {
                goto L3; // [50] 67
            }
        }
        _2 = (int)SEQ_PTR(_linetab_66483);
        _33475 = (int)*(((s1_ptr)_2)->base + _i_66491);
        if (IS_ATOM_INT(_33475)) {
            _33476 = (_33475 == -2);
        }
        else {
            _33476 = binary_op(EQUALS, _33475, -2);
        }
        _33475 = NOVALUE;
        if (_33476 == 0) {
            DeRef(_33476);
            _33476 = NOVALUE;
            goto L4; // [63] 114
        }
        else {
            if (!IS_ATOM_INT(_33476) && DBL_PTR(_33476)->dbl == 0.0){
                DeRef(_33476);
                _33476 = NOVALUE;
                goto L4; // [63] 114
            }
            DeRef(_33476);
            _33476 = NOVALUE;
        }
        DeRef(_33476);
        _33476 = NOVALUE;
L3: 

        /** 			line = i-1*/
        _line_66484 = _i_66491 - 1;

        /** 			while line > 1 and linetab[line] = -1 do*/
L5: 
        _33478 = (_line_66484 > 1);
        if (_33478 == 0) {
            goto L2; // [82] 121
        }
        _2 = (int)SEQ_PTR(_linetab_66483);
        _33480 = (int)*(((s1_ptr)_2)->base + _line_66484);
        if (IS_ATOM_INT(_33480)) {
            _33481 = (_33480 == -1);
        }
        else {
            _33481 = binary_op(EQUALS, _33480, -1);
        }
        _33480 = NOVALUE;
        if (_33481 <= 0) {
            if (_33481 == 0) {
                DeRef(_33481);
                _33481 = NOVALUE;
                goto L2; // [95] 121
            }
            else {
                if (!IS_ATOM_INT(_33481) && DBL_PTR(_33481)->dbl == 0.0){
                    DeRef(_33481);
                    _33481 = NOVALUE;
                    goto L2; // [95] 121
                }
                DeRef(_33481);
                _33481 = NOVALUE;
            }
        }
        DeRef(_33481);
        _33481 = NOVALUE;

        /** 				line -= 1*/
        _line_66484 = _line_66484 - 1;

        /** 			end while*/
        goto L5; // [106] 78

        /** 			exit*/
        goto L2; // [111] 121
L4: 

        /** 	end for*/
        _i_66491 = _i_66491 + 1;
        goto L1; // [116] 40
L2: 
        ;
    }

    /** 	gline = SymTab[sub][S_FIRSTLINE] + line - 1*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _33483 = (int)*(((s1_ptr)_2)->base + _sub_66480);
    _2 = (int)SEQ_PTR(_33483);
    if (!IS_ATOM_INT(_25S_FIRSTLINE_11953)){
        _33484 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FIRSTLINE_11953)->dbl));
    }
    else{
        _33484 = (int)*(((s1_ptr)_2)->base + _25S_FIRSTLINE_11953);
    }
    _33483 = NOVALUE;
    if (IS_ATOM_INT(_33484)) {
        _33485 = _33484 + _line_66484;
        if ((long)((unsigned long)_33485 + (unsigned long)HIGH_BITS) >= 0) 
        _33485 = NewDouble((double)_33485);
    }
    else {
        _33485 = binary_op(PLUS, _33484, _line_66484);
    }
    _33484 = NOVALUE;
    if (IS_ATOM_INT(_33485)) {
        _gline_66485 = _33485 - 1;
    }
    else {
        _gline_66485 = binary_op(MINUS, _33485, 1);
    }
    DeRef(_33485);
    _33485 = NOVALUE;
    if (!IS_ATOM_INT(_gline_66485)) {
        _1 = (long)(DBL_PTR(_gline_66485)->dbl);
        if (UNIQUE(DBL_PTR(_gline_66485)) && (DBL_PTR(_gline_66485)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_gline_66485);
        _gline_66485 = _1;
    }

    /** 	if file_only then*/
    if (_file_only_66482 == 0)
    {
        goto L6; // [147] 171
    }
    else{
    }

    /** 		return slist[gline][LOCAL_FILE_NO]*/
    _2 = (int)SEQ_PTR(_25slist_12357);
    _33487 = (int)*(((s1_ptr)_2)->base + _gline_66485);
    _2 = (int)SEQ_PTR(_33487);
    _33488 = (int)*(((s1_ptr)_2)->base + 3);
    _33487 = NOVALUE;
    Ref(_33488);
    DeRef(_linetab_66483);
    DeRef(_33478);
    _33478 = NOVALUE;
    DeRef(_33473);
    _33473 = NOVALUE;
    return _33488;
    goto L7; // [168] 266
L6: 

    /** 	elsif gline > length( slist ) or not gline then*/
    if (IS_SEQUENCE(_25slist_12357)){
            _33489 = SEQ_PTR(_25slist_12357)->length;
    }
    else {
        _33489 = 1;
    }
    _33490 = (_gline_66485 > _33489);
    _33489 = NOVALUE;
    if (_33490 != 0) {
        goto L8; // [182] 194
    }
    _33492 = (_gline_66485 == 0);
    if (_33492 == 0)
    {
        DeRef(_33492);
        _33492 = NOVALUE;
        goto L9; // [190] 210
    }
    else{
        DeRef(_33492);
        _33492 = NOVALUE;
    }
L8: 

    /** 		return { "??", -1, "???", gline }*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_33493);
    *((int *)(_2+4)) = _33493;
    *((int *)(_2+8)) = -1;
    RefDS(_33494);
    *((int *)(_2+12)) = _33494;
    *((int *)(_2+16)) = _gline_66485;
    _33495 = MAKE_SEQ(_1);
    DeRef(_linetab_66483);
    DeRef(_33478);
    _33478 = NOVALUE;
    DeRef(_33473);
    _33473 = NOVALUE;
    _33488 = NOVALUE;
    DeRef(_33490);
    _33490 = NOVALUE;
    return _33495;
    goto L7; // [207] 266
L9: 

    /** 		return {known_files[slist[gline][LOCAL_FILE_NO]], slist[gline][LINE], slist[gline][SRC], gline}*/
    _2 = (int)SEQ_PTR(_25slist_12357);
    _33496 = (int)*(((s1_ptr)_2)->base + _gline_66485);
    _2 = (int)SEQ_PTR(_33496);
    _33497 = (int)*(((s1_ptr)_2)->base + 3);
    _33496 = NOVALUE;
    _2 = (int)SEQ_PTR(_26known_files_11139);
    if (!IS_ATOM_INT(_33497)){
        _33498 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33497)->dbl));
    }
    else{
        _33498 = (int)*(((s1_ptr)_2)->base + _33497);
    }
    _2 = (int)SEQ_PTR(_25slist_12357);
    _33499 = (int)*(((s1_ptr)_2)->base + _gline_66485);
    _2 = (int)SEQ_PTR(_33499);
    _33500 = (int)*(((s1_ptr)_2)->base + 2);
    _33499 = NOVALUE;
    _2 = (int)SEQ_PTR(_25slist_12357);
    _33501 = (int)*(((s1_ptr)_2)->base + _gline_66485);
    _2 = (int)SEQ_PTR(_33501);
    _33502 = (int)*(((s1_ptr)_2)->base + 1);
    _33501 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_33498);
    *((int *)(_2+4)) = _33498;
    Ref(_33500);
    *((int *)(_2+8)) = _33500;
    Ref(_33502);
    *((int *)(_2+12)) = _33502;
    *((int *)(_2+16)) = _gline_66485;
    _33503 = MAKE_SEQ(_1);
    _33502 = NOVALUE;
    _33500 = NOVALUE;
    _33498 = NOVALUE;
    DeRef(_linetab_66483);
    DeRef(_33478);
    _33478 = NOVALUE;
    DeRef(_33473);
    _33473 = NOVALUE;
    _33488 = NOVALUE;
    DeRef(_33490);
    _33490 = NOVALUE;
    DeRef(_33495);
    _33495 = NOVALUE;
    _33497 = NOVALUE;
    return _33503;
L7: 
    ;
}


void _70opPROC()
{
    int _n_66544 = NOVALUE;
    int _arg_66545 = NOVALUE;
    int _sub_66546 = NOVALUE;
    int _top_66547 = NOVALUE;
    int _dsm_66548 = NOVALUE;
    int _current_file_66556 = NOVALUE;
    int _33546 = NOVALUE;
    int _33545 = NOVALUE;
    int _33543 = NOVALUE;
    int _33542 = NOVALUE;
    int _33540 = NOVALUE;
    int _33539 = NOVALUE;
    int _33538 = NOVALUE;
    int _33537 = NOVALUE;
    int _33536 = NOVALUE;
    int _33535 = NOVALUE;
    int _33532 = NOVALUE;
    int _33531 = NOVALUE;
    int _33529 = NOVALUE;
    int _33527 = NOVALUE;
    int _33526 = NOVALUE;
    int _33523 = NOVALUE;
    int _33521 = NOVALUE;
    int _33520 = NOVALUE;
    int _33519 = NOVALUE;
    int _33518 = NOVALUE;
    int _33517 = NOVALUE;
    int _33516 = NOVALUE;
    int _33515 = NOVALUE;
    int _33514 = NOVALUE;
    int _33513 = NOVALUE;
    int _33512 = NOVALUE;
    int _33510 = NOVALUE;
    int _33506 = NOVALUE;
    int _33504 = NOVALUE;
    int _0, _1, _2;
    

    /**     sub = Code[pc+1] -- subroutine*/
    _33504 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _sub_66546 = (int)*(((s1_ptr)_2)->base + _33504);
    if (!IS_ATOM_INT(_sub_66546)){
        _sub_66546 = (long)DBL_PTR(_sub_66546)->dbl;
    }

    /**     n = SymTab[sub][S_NUM_ARGS]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _33506 = (int)*(((s1_ptr)_2)->base + _sub_66546);
    _2 = (int)SEQ_PTR(_33506);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _n_66544 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _n_66544 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    if (!IS_ATOM_INT(_n_66544)){
        _n_66544 = (long)DBL_PTR(_n_66544)->dbl;
    }
    _33506 = NOVALUE;

    /** 	integer current_file*/

    /** 	if CurrentSub = TopLevelSub then*/
    if (_25CurrentSub_12270 != _25TopLevelSub_12269)
    goto L1; // [41] 60

    /** 		current_file = find_line( sub, pc )*/
    _current_file_66556 = _70find_line(_sub_66546, _70pc_65963, 1);
    if (!IS_ATOM_INT(_current_file_66556)) {
        _1 = (long)(DBL_PTR(_current_file_66556)->dbl);
        if (UNIQUE(DBL_PTR(_current_file_66556)) && (DBL_PTR(_current_file_66556)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_current_file_66556);
        _current_file_66556 = _1;
    }
    goto L2; // [57] 79
L1: 

    /** 		current_file = SymTab[CurrentSub][S_FILE_NO]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _33510 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_33510);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _current_file_66556 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _current_file_66556 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    if (!IS_ATOM_INT(_current_file_66556)){
        _current_file_66556 = (long)DBL_PTR(_current_file_66556)->dbl;
    }
    _33510 = NOVALUE;
L2: 

    /** 	map:nested_put( called_from, { current_file, CurrentSub, SymTab[sub][S_FILE_NO], sub }, 1 , map:ADD )*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _33512 = (int)*(((s1_ptr)_2)->base + _sub_66546);
    _2 = (int)SEQ_PTR(_33512);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _33513 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _33513 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _33512 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _current_file_66556;
    *((int *)(_2+8)) = _25CurrentSub_12270;
    Ref(_33513);
    *((int *)(_2+12)) = _33513;
    *((int *)(_2+16)) = _sub_66546;
    _33514 = MAKE_SEQ(_1);
    _33513 = NOVALUE;
    Ref(_71called_from_65009);
    _32nested_put(_71called_from_65009, _33514, 1, 2, _32threshold_size_13318);
    _33514 = NOVALUE;

    /** 	map:nested_put( called_by,   { SymTab[sub][S_FILE_NO], sub, current_file, CurrentSub }, 1, map:ADD )*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _33515 = (int)*(((s1_ptr)_2)->base + _sub_66546);
    _2 = (int)SEQ_PTR(_33515);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _33516 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _33516 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _33515 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_33516);
    *((int *)(_2+4)) = _33516;
    *((int *)(_2+8)) = _sub_66546;
    *((int *)(_2+12)) = _current_file_66556;
    *((int *)(_2+16)) = _25CurrentSub_12270;
    _33517 = MAKE_SEQ(_1);
    _33516 = NOVALUE;
    Ref(_71called_by_65011);
    _32nested_put(_71called_by_65011, _33517, 1, 2, _32threshold_size_13318);
    _33517 = NOVALUE;

    /**     dsm = sprintf( "%s: %s",{opnames[Code[pc]],name_or_literal(sub)})*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33518 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33518)){
        _33519 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33518)->dbl));
    }
    else{
        _33519 = (int)*(((s1_ptr)_2)->base + _33518);
    }
    _33520 = _70name_or_literal(_sub_66546);
    RefDS(_33519);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33519;
    ((int *)_2)[2] = _33520;
    _33521 = MAKE_SEQ(_1);
    _33520 = NOVALUE;
    _33519 = NOVALUE;
    DeRef(_dsm_66548);
    _dsm_66548 = EPrintf(-9999999, _33368, _33521);
    DeRefDS(_33521);
    _33521 = NOVALUE;

    /** 	for i = 1 to n do*/
    _33523 = _n_66544;
    {
        int _i_66590;
        _i_66590 = 1;
L3: 
        if (_i_66590 > _33523){
            goto L4; // [186] 243
        }

        /** 		if i < n then*/
        if (_i_66590 >= _n_66544)
        goto L5; // [195] 206

        /** 			dsm &= ", "*/
        Concat((object_ptr)&_dsm_66548, _dsm_66548, _33026);
L5: 

        /** 		arg = Code[pc + i + 1]*/
        _33526 = _70pc_65963 + _i_66590;
        if ((long)((unsigned long)_33526 + (unsigned long)HIGH_BITS) >= 0) 
        _33526 = NewDouble((double)_33526);
        if (IS_ATOM_INT(_33526)) {
            _33527 = _33526 + 1;
        }
        else
        _33527 = binary_op(PLUS, 1, _33526);
        DeRef(_33526);
        _33526 = NOVALUE;
        _2 = (int)SEQ_PTR(_25Code_12355);
        if (!IS_ATOM_INT(_33527)){
            _arg_66545 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33527)->dbl));
        }
        else{
            _arg_66545 = (int)*(((s1_ptr)_2)->base + _33527);
        }
        if (!IS_ATOM_INT(_arg_66545)){
            _arg_66545 = (long)DBL_PTR(_arg_66545)->dbl;
        }

        /** 		dsm &= name_or_literal(arg)*/
        _33529 = _70name_or_literal(_arg_66545);
        if (IS_SEQUENCE(_dsm_66548) && IS_ATOM(_33529)) {
            Ref(_33529);
            Append(&_dsm_66548, _dsm_66548, _33529);
        }
        else if (IS_ATOM(_dsm_66548) && IS_SEQUENCE(_33529)) {
        }
        else {
            Concat((object_ptr)&_dsm_66548, _dsm_66548, _33529);
        }
        DeRef(_33529);
        _33529 = NOVALUE;

        /** 	end for*/
        _i_66590 = _i_66590 + 1;
        goto L3; // [238] 193
L4: 
        ;
    }

    /**     if SymTab[sub][S_TOKEN] != PROC then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _33531 = (int)*(((s1_ptr)_2)->base + _sub_66546);
    _2 = (int)SEQ_PTR(_33531);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _33532 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _33532 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _33531 = NOVALUE;
    if (binary_op_a(EQUALS, _33532, 27)){
        _33532 = NOVALUE;
        goto L6; // [259] 331
    }
    _33532 = NOVALUE;

    /**     	dsm[1..4] = "FUNC"*/
    assign_slice_seq = (s1_ptr *)&_dsm_66548;
    AssignSlice(1, 4, _24848);

    /** 		dsm &= sprintf( " => %s", {name_or_literal(Code[pc + n + 2])})*/
    _33535 = _70pc_65963 + _n_66544;
    if ((long)((unsigned long)_33535 + (unsigned long)HIGH_BITS) >= 0) 
    _33535 = NewDouble((double)_33535);
    if (IS_ATOM_INT(_33535)) {
        _33536 = _33535 + 2;
    }
    else {
        _33536 = NewDouble(DBL_PTR(_33535)->dbl + (double)2);
    }
    DeRef(_33535);
    _33535 = NOVALUE;
    _2 = (int)SEQ_PTR(_25Code_12355);
    if (!IS_ATOM_INT(_33536)){
        _33537 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33536)->dbl));
    }
    else{
        _33537 = (int)*(((s1_ptr)_2)->base + _33536);
    }
    Ref(_33537);
    _33538 = _70name_or_literal(_33537);
    _33537 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _33538;
    _33539 = MAKE_SEQ(_1);
    _33538 = NOVALUE;
    _33540 = EPrintf(-9999999, _33534, _33539);
    DeRefDS(_33539);
    _33539 = NOVALUE;
    Concat((object_ptr)&_dsm_66548, _dsm_66548, _33540);
    DeRefDS(_33540);
    _33540 = NOVALUE;

    /** 		il( dsm, n + 2 )*/
    _33542 = _n_66544 + 2;
    if ((long)((unsigned long)_33542 + (unsigned long)HIGH_BITS) >= 0) 
    _33542 = NewDouble((double)_33542);
    RefDS(_dsm_66548);
    _70il(_dsm_66548, _33542);
    _33542 = NOVALUE;

    /** 		pc += n + 3*/
    _33543 = _n_66544 + 3;
    if ((long)((unsigned long)_33543 + (unsigned long)HIGH_BITS) >= 0) 
    _33543 = NewDouble((double)_33543);
    if (IS_ATOM_INT(_33543)) {
        _70pc_65963 = _70pc_65963 + _33543;
    }
    else {
        _70pc_65963 = NewDouble((double)_70pc_65963 + DBL_PTR(_33543)->dbl);
    }
    DeRef(_33543);
    _33543 = NOVALUE;
    if (!IS_ATOM_INT(_70pc_65963)) {
        _1 = (long)(DBL_PTR(_70pc_65963)->dbl);
        if (UNIQUE(DBL_PTR(_70pc_65963)) && (DBL_PTR(_70pc_65963)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_70pc_65963);
        _70pc_65963 = _1;
    }
    goto L7; // [328] 356
L6: 

    /** 		il( dsm, n + 1 )*/
    _33545 = _n_66544 + 1;
    if (_33545 > MAXINT){
        _33545 = NewDouble((double)_33545);
    }
    RefDS(_dsm_66548);
    _70il(_dsm_66548, _33545);
    _33545 = NOVALUE;

    /** 	    pc += n + 2*/
    _33546 = _n_66544 + 2;
    if ((long)((unsigned long)_33546 + (unsigned long)HIGH_BITS) >= 0) 
    _33546 = NewDouble((double)_33546);
    if (IS_ATOM_INT(_33546)) {
        _70pc_65963 = _70pc_65963 + _33546;
    }
    else {
        _70pc_65963 = NewDouble((double)_70pc_65963 + DBL_PTR(_33546)->dbl);
    }
    DeRef(_33546);
    _33546 = NOVALUE;
    if (!IS_ATOM_INT(_70pc_65963)) {
        _1 = (long)(DBL_PTR(_70pc_65963)->dbl);
        if (UNIQUE(DBL_PTR(_70pc_65963)) && (DBL_PTR(_70pc_65963)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_70pc_65963);
        _70pc_65963 = _1;
    }
L7: 

    /** end procedure*/
    DeRef(_dsm_66548);
    DeRef(_33504);
    _33504 = NOVALUE;
    _33518 = NOVALUE;
    DeRef(_33527);
    _33527 = NOVALUE;
    DeRef(_33536);
    _33536 = NOVALUE;
    return;
    ;
}


void _70opRETURNP()
{
    int _0, _1, _2;
    

    /**     pbinary()*/
    _70pbinary();

    /** end procedure*/
    return;
    ;
}


void _70opRETURNF()
{
    int _33555 = NOVALUE;
    int _33554 = NOVALUE;
    int _33553 = NOVALUE;
    int _33552 = NOVALUE;
    int _33551 = NOVALUE;
    int _33548 = NOVALUE;
    int _0, _1, _2;
    

    /** 	result_val = Code[pc+3]*/
    _33548 = _70pc_65963 + 3;
    DeRef(_70result_val_66625);
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70result_val_66625 = (int)*(((s1_ptr)_2)->base + _33548);
    Ref(_70result_val_66625);

    /** 	il(	sprintf( "RETURNF: %s block[%d]", {name_or_literal(result_val), Code[pc+2]}), 3)*/
    Ref(_70result_val_66625);
    _33551 = _70name_or_literal(_70result_val_66625);
    _33552 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33553 = (int)*(((s1_ptr)_2)->base + _33552);
    Ref(_33553);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33551;
    ((int *)_2)[2] = _33553;
    _33554 = MAKE_SEQ(_1);
    _33553 = NOVALUE;
    _33551 = NOVALUE;
    _33555 = EPrintf(-9999999, _33550, _33554);
    DeRefDS(_33554);
    _33554 = NOVALUE;
    _70il(_33555, 3);
    _33555 = NOVALUE;

    /** 	pc += 4*/
    _70pc_65963 = _70pc_65963 + 4;

    /** end procedure*/
    _33548 = NOVALUE;
    _33552 = NOVALUE;
    return;
    ;
}


void _70opCALL_BACK_RETURN()
{
    int _0, _1, _2;
    

    /** 	il( "CALL_BACK_RETURN", 0 )*/
    RefDS(_33557);
    _70il(_33557, 0);

    /** end procedure*/
    return;
    ;
}


void _70opBADRETURNF()
{
    int _0, _1, _2;
    

    /** 	pnonary()*/
    _70pnonary();

    /** end procedure*/
    return;
    ;
}


void _70opRETURNT()
{
    int _0, _1, _2;
    

    /** 	pnonary()*/
    _70pnonary();

    /** end procedure*/
    return;
    ;
}


void _70opRHS_SUBS()
{
    int _sub_66650 = NOVALUE;
    int _x_66651 = NOVALUE;
    int _33571 = NOVALUE;
    int _33570 = NOVALUE;
    int _33569 = NOVALUE;
    int _33568 = NOVALUE;
    int _33567 = NOVALUE;
    int _33566 = NOVALUE;
    int _33565 = NOVALUE;
    int _33562 = NOVALUE;
    int _33560 = NOVALUE;
    int _33558 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33558 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33558);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     b = Code[pc+2]*/
    _33560 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70b_65965 = (int)*(((s1_ptr)_2)->base + _33560);
    if (!IS_ATOM_INT(_70b_65965)){
        _70b_65965 = (long)DBL_PTR(_70b_65965)->dbl;
    }

    /**     target = Code[pc+3]*/
    _33562 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70target_65968 = (int)*(((s1_ptr)_2)->base + _33562);
    if (!IS_ATOM_INT(_70target_65968)){
        _70target_65968 = (long)DBL_PTR(_70target_65968)->dbl;
    }

    /**     il( sprintf("%s: %s sub %s => %s",{opnames[Code[pc]]} & names({a,b,target})), 3 )*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33565 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33565)){
        _33566 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33565)->dbl));
    }
    else{
        _33566 = (int)*(((s1_ptr)_2)->base + _33565);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_33566);
    *((int *)(_2+4)) = _33566;
    _33567 = MAKE_SEQ(_1);
    _33566 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _70a_65964;
    *((int *)(_2+8)) = _70b_65965;
    *((int *)(_2+12)) = _70target_65968;
    _33568 = MAKE_SEQ(_1);
    _33569 = _70names(_33568);
    _33568 = NOVALUE;
    if (IS_SEQUENCE(_33567) && IS_ATOM(_33569)) {
        Ref(_33569);
        Append(&_33570, _33567, _33569);
    }
    else if (IS_ATOM(_33567) && IS_SEQUENCE(_33569)) {
    }
    else {
        Concat((object_ptr)&_33570, _33567, _33569);
        DeRefDS(_33567);
        _33567 = NOVALUE;
    }
    DeRef(_33567);
    _33567 = NOVALUE;
    DeRef(_33569);
    _33569 = NOVALUE;
    _33571 = EPrintf(-9999999, _33564, _33570);
    DeRefDS(_33570);
    _33570 = NOVALUE;
    _70il(_33571, 3);
    _33571 = NOVALUE;

    /**     pc += 4*/
    _70pc_65963 = _70pc_65963 + 4;

    /** end procedure*/
    _33558 = NOVALUE;
    _33560 = NOVALUE;
    _33562 = NOVALUE;
    _33565 = NOVALUE;
    return;
    ;
}


void _70opIF()
{
    int _33582 = NOVALUE;
    int _33581 = NOVALUE;
    int _33580 = NOVALUE;
    int _33579 = NOVALUE;
    int _33578 = NOVALUE;
    int _33577 = NOVALUE;
    int _33576 = NOVALUE;
    int _33573 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33573 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33573);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     il(sprintf( "%s: %s = 0 goto %04d", {opnames[Code[pc]],name_or_literal(a), Code[pc+2]}),2)*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33576 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33576)){
        _33577 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33576)->dbl));
    }
    else{
        _33577 = (int)*(((s1_ptr)_2)->base + _33576);
    }
    _33578 = _70name_or_literal(_70a_65964);
    _33579 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33580 = (int)*(((s1_ptr)_2)->base + _33579);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_33577);
    *((int *)(_2+4)) = _33577;
    *((int *)(_2+8)) = _33578;
    Ref(_33580);
    *((int *)(_2+12)) = _33580;
    _33581 = MAKE_SEQ(_1);
    _33580 = NOVALUE;
    _33578 = NOVALUE;
    _33577 = NOVALUE;
    _33582 = EPrintf(-9999999, _33575, _33581);
    DeRefDS(_33581);
    _33581 = NOVALUE;
    _70il(_33582, 2);
    _33582 = NOVALUE;

    /**     pc += 3*/
    _70pc_65963 = _70pc_65963 + 3;

    /** end procedure*/
    _33573 = NOVALUE;
    _33576 = NOVALUE;
    _33579 = NOVALUE;
    return;
    ;
}


void _70check()
{
    int _33590 = NOVALUE;
    int _33589 = NOVALUE;
    int _33588 = NOVALUE;
    int _33587 = NOVALUE;
    int _33586 = NOVALUE;
    int _33585 = NOVALUE;
    int _33584 = NOVALUE;
    int _0, _1, _2;
    

    /** 	il( sprintf("%s: %s",{opnames[Code[pc]] ,name_or_literal(Code[pc+1])}), 1 )*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33584 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33584)){
        _33585 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33584)->dbl));
    }
    else{
        _33585 = (int)*(((s1_ptr)_2)->base + _33584);
    }
    _33586 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33587 = (int)*(((s1_ptr)_2)->base + _33586);
    Ref(_33587);
    _33588 = _70name_or_literal(_33587);
    _33587 = NOVALUE;
    RefDS(_33585);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33585;
    ((int *)_2)[2] = _33588;
    _33589 = MAKE_SEQ(_1);
    _33588 = NOVALUE;
    _33585 = NOVALUE;
    _33590 = EPrintf(-9999999, _33368, _33589);
    DeRefDS(_33589);
    _33589 = NOVALUE;
    _70il(_33590, 1);
    _33590 = NOVALUE;

    /** 	pc += 2*/
    _70pc_65963 = _70pc_65963 + 2;

    /** end procedure*/
    _33584 = NOVALUE;
    _33586 = NOVALUE;
    return;
    ;
}


void _70opINTEGER_CHECK()
{
    int _0, _1, _2;
    

    /** 	check()*/
    _70check();

    /** end procedure*/
    return;
    ;
}


void _70opATOM_CHECK()
{
    int _0, _1, _2;
    

    /** 	check()*/
    _70check();

    /** end procedure*/
    return;
    ;
}


void _70opSEQUENCE_CHECK()
{
    int _0, _1, _2;
    

    /** 	check()*/
    _70check();

    /** end procedure*/
    return;
    ;
}


void _70opASSIGN()
{
    int _33599 = NOVALUE;
    int _33598 = NOVALUE;
    int _33597 = NOVALUE;
    int _33594 = NOVALUE;
    int _33592 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33592 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33592);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     target = Code[pc+2]*/
    _33594 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70target_65968 = (int)*(((s1_ptr)_2)->base + _33594);
    if (!IS_ATOM_INT(_70target_65968)){
        _70target_65968 = (long)DBL_PTR(_70target_65968)->dbl;
    }

    /**     il( sprintf("%s => %s", names( {a,target} )), 2 )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _70a_65964;
    ((int *)_2)[2] = _70target_65968;
    _33597 = MAKE_SEQ(_1);
    _33598 = _70names(_33597);
    _33597 = NOVALUE;
    _33599 = EPrintf(-9999999, _33596, _33598);
    DeRef(_33598);
    _33598 = NOVALUE;
    _70il(_33599, 2);
    _33599 = NOVALUE;

    /**     pc += 3*/
    _70pc_65963 = _70pc_65963 + 3;

    /** end procedure*/
    _33592 = NOVALUE;
    _33594 = NOVALUE;
    return;
    ;
}


void _70opELSE()
{
    int _33607 = NOVALUE;
    int _33606 = NOVALUE;
    int _33605 = NOVALUE;
    int _33604 = NOVALUE;
    int _33603 = NOVALUE;
    int _33602 = NOVALUE;
    int _0, _1, _2;
    

    /** 	il( sprintf("%s goto %04d", {opnames[Code[pc]], Code[pc+1]}),1 )*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33602 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33602)){
        _33603 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33602)->dbl));
    }
    else{
        _33603 = (int)*(((s1_ptr)_2)->base + _33602);
    }
    _33604 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33605 = (int)*(((s1_ptr)_2)->base + _33604);
    Ref(_33605);
    RefDS(_33603);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33603;
    ((int *)_2)[2] = _33605;
    _33606 = MAKE_SEQ(_1);
    _33605 = NOVALUE;
    _33603 = NOVALUE;
    _33607 = EPrintf(-9999999, _33601, _33606);
    DeRefDS(_33606);
    _33606 = NOVALUE;
    _70il(_33607, 1);
    _33607 = NOVALUE;

    /** 	pc += 2*/
    _70pc_65963 = _70pc_65963 + 2;

    /** end procedure*/
    _33602 = NOVALUE;
    _33604 = NOVALUE;
    return;
    ;
}


void _70opRIGHT_BRACE_N()
{
    int _x_66736 = NOVALUE;
    int _33630 = NOVALUE;
    int _33629 = NOVALUE;
    int _33627 = NOVALUE;
    int _33626 = NOVALUE;
    int _33625 = NOVALUE;
    int _33623 = NOVALUE;
    int _33622 = NOVALUE;
    int _33620 = NOVALUE;
    int _33619 = NOVALUE;
    int _33618 = NOVALUE;
    int _33617 = NOVALUE;
    int _33615 = NOVALUE;
    int _33614 = NOVALUE;
    int _33613 = NOVALUE;
    int _33609 = NOVALUE;
    int _0, _1, _2;
    

    /**     len = Code[pc+1]*/
    _33609 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70len_65969 = (int)*(((s1_ptr)_2)->base + _33609);
    if (!IS_ATOM_INT(_70len_65969)){
        _70len_65969 = (long)DBL_PTR(_70len_65969)->dbl;
    }

    /**     x = sprintf("RIGHT_BRACE_N: len %d", len)*/
    DeRefi(_x_66736);
    _x_66736 = EPrintf(-9999999, _33611, _70len_65969);

    /**     for i = pc+len+1 to pc+2 by -1 do*/
    _33613 = _70pc_65963 + _70len_65969;
    if ((long)((unsigned long)_33613 + (unsigned long)HIGH_BITS) >= 0) 
    _33613 = NewDouble((double)_33613);
    if (IS_ATOM_INT(_33613)) {
        _33614 = _33613 + 1;
        if (_33614 > MAXINT){
            _33614 = NewDouble((double)_33614);
        }
    }
    else
    _33614 = binary_op(PLUS, 1, _33613);
    DeRef(_33613);
    _33613 = NOVALUE;
    _33615 = _70pc_65963 + 2;
    if ((long)((unsigned long)_33615 + (unsigned long)HIGH_BITS) >= 0) 
    _33615 = NewDouble((double)_33615);
    {
        int _i_66743;
        Ref(_33614);
        _i_66743 = _33614;
L1: 
        if (binary_op_a(LESS, _i_66743, _33615)){
            goto L2; // [45] 83
        }

        /** 		x &= sprintf(", %s",{name_or_literal(Code[i])})*/
        _2 = (int)SEQ_PTR(_25Code_12355);
        if (!IS_ATOM_INT(_i_66743)){
            _33617 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_66743)->dbl));
        }
        else{
            _33617 = (int)*(((s1_ptr)_2)->base + _i_66743);
        }
        Ref(_33617);
        _33618 = _70name_or_literal(_33617);
        _33617 = NOVALUE;
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _33618;
        _33619 = MAKE_SEQ(_1);
        _33618 = NOVALUE;
        _33620 = EPrintf(-9999999, _33616, _33619);
        DeRefDS(_33619);
        _33619 = NOVALUE;
        Concat((object_ptr)&_x_66736, _x_66736, _33620);
        DeRefDS(_33620);
        _33620 = NOVALUE;

        /**     end for*/
        _0 = _i_66743;
        if (IS_ATOM_INT(_i_66743)) {
            _i_66743 = _i_66743 + -1;
            if ((long)((unsigned long)_i_66743 +(unsigned long) HIGH_BITS) >= 0){
                _i_66743 = NewDouble((double)_i_66743);
            }
        }
        else {
            _i_66743 = binary_op_a(PLUS, _i_66743, -1);
        }
        DeRef(_0);
        goto L1; // [78] 52
L2: 
        ;
        DeRef(_i_66743);
    }

    /**     target = Code[pc+len+2]*/
    _33622 = _70pc_65963 + _70len_65969;
    if ((long)((unsigned long)_33622 + (unsigned long)HIGH_BITS) >= 0) 
    _33622 = NewDouble((double)_33622);
    if (IS_ATOM_INT(_33622)) {
        _33623 = _33622 + 2;
    }
    else {
        _33623 = NewDouble(DBL_PTR(_33622)->dbl + (double)2);
    }
    DeRef(_33622);
    _33622 = NOVALUE;
    _2 = (int)SEQ_PTR(_25Code_12355);
    if (!IS_ATOM_INT(_33623)){
        _70target_65968 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33623)->dbl));
    }
    else{
        _70target_65968 = (int)*(((s1_ptr)_2)->base + _33623);
    }
    if (!IS_ATOM_INT(_70target_65968)){
        _70target_65968 = (long)DBL_PTR(_70target_65968)->dbl;
    }

    /**     x &= sprintf(" => %s", {name_or_literal(target)})*/
    _33625 = _70name_or_literal(_70target_65968);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _33625;
    _33626 = MAKE_SEQ(_1);
    _33625 = NOVALUE;
    _33627 = EPrintf(-9999999, _33534, _33626);
    DeRefDS(_33626);
    _33626 = NOVALUE;
    Concat((object_ptr)&_x_66736, _x_66736, _33627);
    DeRefDS(_33627);
    _33627 = NOVALUE;

    /**     il( x, len + 2 )*/
    _33629 = _70len_65969 + 2;
    if ((long)((unsigned long)_33629 + (unsigned long)HIGH_BITS) >= 0) 
    _33629 = NewDouble((double)_33629);
    RefDS(_x_66736);
    _70il(_x_66736, _33629);
    _33629 = NOVALUE;

    /**     pc += 3 + len*/
    _33630 = 3 + _70len_65969;
    if ((long)((unsigned long)_33630 + (unsigned long)HIGH_BITS) >= 0) 
    _33630 = NewDouble((double)_33630);
    if (IS_ATOM_INT(_33630)) {
        _70pc_65963 = _70pc_65963 + _33630;
    }
    else {
        _70pc_65963 = NewDouble((double)_70pc_65963 + DBL_PTR(_33630)->dbl);
    }
    DeRef(_33630);
    _33630 = NOVALUE;
    if (!IS_ATOM_INT(_70pc_65963)) {
        _1 = (long)(DBL_PTR(_70pc_65963)->dbl);
        if (UNIQUE(DBL_PTR(_70pc_65963)) && (DBL_PTR(_70pc_65963)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_70pc_65963);
        _70pc_65963 = _1;
    }

    /** end procedure*/
    DeRefDSi(_x_66736);
    DeRef(_33609);
    _33609 = NOVALUE;
    DeRef(_33615);
    _33615 = NOVALUE;
    DeRef(_33614);
    _33614 = NOVALUE;
    DeRef(_33623);
    _33623 = NOVALUE;
    return;
    ;
}


void _70opRIGHT_BRACE_2()
{
    int _0, _1, _2;
    

    /** 	binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opPLUS1()
{
    int _33639 = NOVALUE;
    int _33638 = NOVALUE;
    int _33637 = NOVALUE;
    int _33634 = NOVALUE;
    int _33632 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33632 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33632);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     target = Code[pc+3]*/
    _33634 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70target_65968 = (int)*(((s1_ptr)_2)->base + _33634);
    if (!IS_ATOM_INT(_70target_65968)){
        _70target_65968 = (long)DBL_PTR(_70target_65968)->dbl;
    }

    /**     il( sprintf("PLUS1: %s + 1 => %s", names( a & target )), 3 )*/
    Concat((object_ptr)&_33637, _70a_65964, _70target_65968);
    _33638 = _70names(_33637);
    _33637 = NOVALUE;
    _33639 = EPrintf(-9999999, _33636, _33638);
    DeRef(_33638);
    _33638 = NOVALUE;
    _70il(_33639, 3);
    _33639 = NOVALUE;

    /**     pc += 4*/
    _70pc_65963 = _70pc_65963 + 4;

    /** end procedure*/
    _33632 = NOVALUE;
    _33634 = NOVALUE;
    return;
    ;
}


void _70opGLOBAL_INIT_CHECK()
{
    int _33647 = NOVALUE;
    int _33646 = NOVALUE;
    int _33645 = NOVALUE;
    int _33644 = NOVALUE;
    int _33643 = NOVALUE;
    int _33641 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33641 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33641);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     il( sprintf("%s: %s", {opnames[Code[pc]],name_or_literal(a)}), 1 )*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33643 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33643)){
        _33644 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33643)->dbl));
    }
    else{
        _33644 = (int)*(((s1_ptr)_2)->base + _33643);
    }
    _33645 = _70name_or_literal(_70a_65964);
    RefDS(_33644);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33644;
    ((int *)_2)[2] = _33645;
    _33646 = MAKE_SEQ(_1);
    _33645 = NOVALUE;
    _33644 = NOVALUE;
    _33647 = EPrintf(-9999999, _33368, _33646);
    DeRefDS(_33646);
    _33646 = NOVALUE;
    _70il(_33647, 1);
    _33647 = NOVALUE;

    /**     pc += 2*/
    _70pc_65963 = _70pc_65963 + 2;

    /** end procedure*/
    _33641 = NOVALUE;
    _33643 = NOVALUE;
    return;
    ;
}


void _70opWHILE()
{
    int _33657 = NOVALUE;
    int _33656 = NOVALUE;
    int _33655 = NOVALUE;
    int _33654 = NOVALUE;
    int _33653 = NOVALUE;
    int _33652 = NOVALUE;
    int _33649 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33649 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33649);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     il( sprintf("WHILE: %s goto %04d else goto %04d", {name_or_literal(a), pc+3, Code[pc+2]}), 2 )*/
    _33652 = _70name_or_literal(_70a_65964);
    _33653 = _70pc_65963 + 3;
    if ((long)((unsigned long)_33653 + (unsigned long)HIGH_BITS) >= 0) 
    _33653 = NewDouble((double)_33653);
    _33654 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33655 = (int)*(((s1_ptr)_2)->base + _33654);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _33652;
    *((int *)(_2+8)) = _33653;
    Ref(_33655);
    *((int *)(_2+12)) = _33655;
    _33656 = MAKE_SEQ(_1);
    _33655 = NOVALUE;
    _33653 = NOVALUE;
    _33652 = NOVALUE;
    _33657 = EPrintf(-9999999, _33651, _33656);
    DeRefDS(_33656);
    _33656 = NOVALUE;
    _70il(_33657, 2);
    _33657 = NOVALUE;

    /**     pc += 3*/
    _70pc_65963 = _70pc_65963 + 3;

    /** end procedure*/
    _33649 = NOVALUE;
    _33654 = NOVALUE;
    return;
    ;
}


void _70opLENGTH()
{
    int _33666 = NOVALUE;
    int _33665 = NOVALUE;
    int _33664 = NOVALUE;
    int _33661 = NOVALUE;
    int _33659 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33659 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33659);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     target = Code[pc+2]*/
    _33661 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70target_65968 = (int)*(((s1_ptr)_2)->base + _33661);
    if (!IS_ATOM_INT(_70target_65968)){
        _70target_65968 = (long)DBL_PTR(_70target_65968)->dbl;
    }

    /**     il( sprintf("LENGTH: %s => %s", names(a&target)), 2 )*/
    Concat((object_ptr)&_33664, _70a_65964, _70target_65968);
    _33665 = _70names(_33664);
    _33664 = NOVALUE;
    _33666 = EPrintf(-9999999, _33663, _33665);
    DeRef(_33665);
    _33665 = NOVALUE;
    _70il(_33666, 2);
    _33666 = NOVALUE;

    /**     pc += 3*/
    _70pc_65963 = _70pc_65963 + 3;

    /** end procedure*/
    _33659 = NOVALUE;
    _33661 = NOVALUE;
    return;
    ;
}


void _70opPLENGTH()
{
    int _33675 = NOVALUE;
    int _33674 = NOVALUE;
    int _33673 = NOVALUE;
    int _33670 = NOVALUE;
    int _33668 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33668 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33668);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     target = Code[pc+2]*/
    _33670 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70target_65968 = (int)*(((s1_ptr)_2)->base + _33670);
    if (!IS_ATOM_INT(_70target_65968)){
        _70target_65968 = (long)DBL_PTR(_70target_65968)->dbl;
    }

    /**     il( sprintf("PLENGTH: %s => %s", names( a & target )), 2 )*/
    Concat((object_ptr)&_33673, _70a_65964, _70target_65968);
    _33674 = _70names(_33673);
    _33673 = NOVALUE;
    _33675 = EPrintf(-9999999, _33672, _33674);
    DeRef(_33674);
    _33674 = NOVALUE;
    _70il(_33675, 2);
    _33675 = NOVALUE;

    /**     pc += 3*/
    _70pc_65963 = _70pc_65963 + 3;

    /** end procedure*/
    _33668 = NOVALUE;
    _33670 = NOVALUE;
    return;
    ;
}


void _70opLHS_SUBS()
{
    int _33688 = NOVALUE;
    int _33687 = NOVALUE;
    int _33686 = NOVALUE;
    int _33685 = NOVALUE;
    int _33684 = NOVALUE;
    int _33681 = NOVALUE;
    int _33679 = NOVALUE;
    int _33677 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1] -- base var sequence, or a temp that contains*/
    _33677 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33677);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     b = Code[pc+2] -- subscript*/
    _33679 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70b_65965 = (int)*(((s1_ptr)_2)->base + _33679);
    if (!IS_ATOM_INT(_70b_65965)){
        _70b_65965 = (long)DBL_PTR(_70b_65965)->dbl;
    }

    /**     target = Code[pc+3] -- temp for storing result*/
    _33681 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70target_65968 = (int)*(((s1_ptr)_2)->base + _33681);
    if (!IS_ATOM_INT(_70target_65968)){
        _70target_65968 = (long)DBL_PTR(_70target_65968)->dbl;
    }

    /**     il( sprintf("LHS_SUBS: %s, %s => %s (UNUSED - %s)", names( Code[pc+1..pc+4] )), 4 )*/
    _33684 = _70pc_65963 + 1;
    if (_33684 > MAXINT){
        _33684 = NewDouble((double)_33684);
    }
    _33685 = _70pc_65963 + 4;
    rhs_slice_target = (object_ptr)&_33686;
    RHS_Slice(_25Code_12355, _33684, _33685);
    _33687 = _70names(_33686);
    _33686 = NOVALUE;
    _33688 = EPrintf(-9999999, _33683, _33687);
    DeRef(_33687);
    _33687 = NOVALUE;
    _70il(_33688, 4);
    _33688 = NOVALUE;

    /**     pc += 5*/
    _70pc_65963 = _70pc_65963 + 5;

    /** end procedure*/
    _33677 = NOVALUE;
    _33679 = NOVALUE;
    _33681 = NOVALUE;
    DeRef(_33684);
    _33684 = NOVALUE;
    _33685 = NOVALUE;
    return;
    ;
}


void _70opLHS_SUBS1()
{
    int _33701 = NOVALUE;
    int _33700 = NOVALUE;
    int _33699 = NOVALUE;
    int _33698 = NOVALUE;
    int _33697 = NOVALUE;
    int _33694 = NOVALUE;
    int _33692 = NOVALUE;
    int _33690 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1] -- base var sequence, or a temp that contains*/
    _33690 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33690);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     b = Code[pc+2] -- subscript*/
    _33692 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70b_65965 = (int)*(((s1_ptr)_2)->base + _33692);
    if (!IS_ATOM_INT(_70b_65965)){
        _70b_65965 = (long)DBL_PTR(_70b_65965)->dbl;
    }

    /**     target = Code[pc+3] -- temp for storing result*/
    _33694 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70target_65968 = (int)*(((s1_ptr)_2)->base + _33694);
    if (!IS_ATOM_INT(_70target_65968)){
        _70target_65968 = (long)DBL_PTR(_70target_65968)->dbl;
    }

    /**     il( sprintf("LHS_SUBS1: %s, %s => %s (UNUSED - %s)", names( Code[pc+1..pc+4] )), 4 )*/
    _33697 = _70pc_65963 + 1;
    if (_33697 > MAXINT){
        _33697 = NewDouble((double)_33697);
    }
    _33698 = _70pc_65963 + 4;
    rhs_slice_target = (object_ptr)&_33699;
    RHS_Slice(_25Code_12355, _33697, _33698);
    _33700 = _70names(_33699);
    _33699 = NOVALUE;
    _33701 = EPrintf(-9999999, _33696, _33700);
    DeRef(_33700);
    _33700 = NOVALUE;
    _70il(_33701, 4);
    _33701 = NOVALUE;

    /**     pc += 5*/
    _70pc_65963 = _70pc_65963 + 5;

    /** end procedure*/
    _33690 = NOVALUE;
    _33692 = NOVALUE;
    _33694 = NOVALUE;
    DeRef(_33697);
    _33697 = NOVALUE;
    _33698 = NOVALUE;
    return;
    ;
}


void _70opLHS_SUBS1_COPY()
{
    int _33716 = NOVALUE;
    int _33715 = NOVALUE;
    int _33714 = NOVALUE;
    int _33713 = NOVALUE;
    int _33712 = NOVALUE;
    int _33709 = NOVALUE;
    int _33707 = NOVALUE;
    int _33705 = NOVALUE;
    int _33703 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1] -- base var sequence*/
    _33703 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33703);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     b = Code[pc+2] -- subscript*/
    _33705 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70b_65965 = (int)*(((s1_ptr)_2)->base + _33705);
    if (!IS_ATOM_INT(_70b_65965)){
        _70b_65965 = (long)DBL_PTR(_70b_65965)->dbl;
    }

    /**     target = Code[pc+3] -- temp for storing result*/
    _33707 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70target_65968 = (int)*(((s1_ptr)_2)->base + _33707);
    if (!IS_ATOM_INT(_70target_65968)){
        _70target_65968 = (long)DBL_PTR(_70target_65968)->dbl;
    }

    /**     c = Code[pc+4] -- temp to hold base sequence while it's manipulated*/
    _33709 = _70pc_65963 + 4;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70c_65966 = (int)*(((s1_ptr)_2)->base + _33709);
    if (!IS_ATOM_INT(_70c_65966)){
        _70c_65966 = (long)DBL_PTR(_70c_65966)->dbl;
    }

    /**     il( sprintf("LHS_SUBS1_COPY: %s, %s => (%s) %s", names( Code[pc+1..pc+4] )), 4 )*/
    _33712 = _70pc_65963 + 1;
    if (_33712 > MAXINT){
        _33712 = NewDouble((double)_33712);
    }
    _33713 = _70pc_65963 + 4;
    rhs_slice_target = (object_ptr)&_33714;
    RHS_Slice(_25Code_12355, _33712, _33713);
    _33715 = _70names(_33714);
    _33714 = NOVALUE;
    _33716 = EPrintf(-9999999, _33711, _33715);
    DeRef(_33715);
    _33715 = NOVALUE;
    _70il(_33716, 4);
    _33716 = NOVALUE;

    /**     pc += 5*/
    _70pc_65963 = _70pc_65963 + 5;

    /** end procedure*/
    _33703 = NOVALUE;
    _33705 = NOVALUE;
    _33707 = NOVALUE;
    _33709 = NOVALUE;
    DeRef(_33712);
    _33712 = NOVALUE;
    _33713 = NOVALUE;
    return;
    ;
}


void _70opASSIGN_SUBS()
{
    int _x_66895 = NOVALUE;
    int _subs_66896 = NOVALUE;
    int _33733 = NOVALUE;
    int _33732 = NOVALUE;
    int _33731 = NOVALUE;
    int _33730 = NOVALUE;
    int _33729 = NOVALUE;
    int _33728 = NOVALUE;
    int _33727 = NOVALUE;
    int _33726 = NOVALUE;
    int _33725 = NOVALUE;
    int _33722 = NOVALUE;
    int _33720 = NOVALUE;
    int _33718 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]  -- the sequence*/
    _33718 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33718);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     b = Code[pc+2]  -- the subscript*/
    _33720 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70b_65965 = (int)*(((s1_ptr)_2)->base + _33720);
    if (!IS_ATOM_INT(_70b_65965)){
        _70b_65965 = (long)DBL_PTR(_70b_65965)->dbl;
    }

    /**     c = Code[pc+3]  -- the RHS value*/
    _33722 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70c_65966 = (int)*(((s1_ptr)_2)->base + _33722);
    if (!IS_ATOM_INT(_70c_65966)){
        _70c_65966 = (long)DBL_PTR(_70c_65966)->dbl;
    }

    /** 	il( sprintf("%s: %s, %s <= %s", {opnames[Code[pc]]} & names( Code[pc+1..pc+3] )), 3 )*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33725 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33725)){
        _33726 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33725)->dbl));
    }
    else{
        _33726 = (int)*(((s1_ptr)_2)->base + _33725);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_33726);
    *((int *)(_2+4)) = _33726;
    _33727 = MAKE_SEQ(_1);
    _33726 = NOVALUE;
    _33728 = _70pc_65963 + 1;
    if (_33728 > MAXINT){
        _33728 = NewDouble((double)_33728);
    }
    _33729 = _70pc_65963 + 3;
    rhs_slice_target = (object_ptr)&_33730;
    RHS_Slice(_25Code_12355, _33728, _33729);
    _33731 = _70names(_33730);
    _33730 = NOVALUE;
    if (IS_SEQUENCE(_33727) && IS_ATOM(_33731)) {
        Ref(_33731);
        Append(&_33732, _33727, _33731);
    }
    else if (IS_ATOM(_33727) && IS_SEQUENCE(_33731)) {
    }
    else {
        Concat((object_ptr)&_33732, _33727, _33731);
        DeRefDS(_33727);
        _33727 = NOVALUE;
    }
    DeRef(_33727);
    _33727 = NOVALUE;
    DeRef(_33731);
    _33731 = NOVALUE;
    _33733 = EPrintf(-9999999, _33724, _33732);
    DeRefDS(_33732);
    _33732 = NOVALUE;
    _70il(_33733, 3);
    _33733 = NOVALUE;

    /**     pc += 4*/
    _70pc_65963 = _70pc_65963 + 4;

    /** end procedure*/
    _33718 = NOVALUE;
    _33720 = NOVALUE;
    _33722 = NOVALUE;
    _33725 = NOVALUE;
    DeRef(_33728);
    _33728 = NOVALUE;
    _33729 = NOVALUE;
    return;
    ;
}


void _70opPASSIGN_SUBS()
{
    int _33746 = NOVALUE;
    int _33745 = NOVALUE;
    int _33744 = NOVALUE;
    int _33743 = NOVALUE;
    int _33742 = NOVALUE;
    int _33739 = NOVALUE;
    int _33737 = NOVALUE;
    int _33735 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33735 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33735);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     b = Code[pc+2]  -- subscript*/
    _33737 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70b_65965 = (int)*(((s1_ptr)_2)->base + _33737);
    if (!IS_ATOM_INT(_70b_65965)){
        _70b_65965 = (long)DBL_PTR(_70b_65965)->dbl;
    }

    /**     c = Code[pc+3]  -- RHS value*/
    _33739 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70c_65966 = (int)*(((s1_ptr)_2)->base + _33739);
    if (!IS_ATOM_INT(_70c_65966)){
        _70c_65966 = (long)DBL_PTR(_70c_65966)->dbl;
    }

    /** 	il( sprintf("PASSIGN_SUBS: %s, %s <= %s", names( Code[pc+1..pc+3] )), 3 )*/
    _33742 = _70pc_65963 + 1;
    if (_33742 > MAXINT){
        _33742 = NewDouble((double)_33742);
    }
    _33743 = _70pc_65963 + 3;
    rhs_slice_target = (object_ptr)&_33744;
    RHS_Slice(_25Code_12355, _33742, _33743);
    _33745 = _70names(_33744);
    _33744 = NOVALUE;
    _33746 = EPrintf(-9999999, _33741, _33745);
    DeRef(_33745);
    _33745 = NOVALUE;
    _70il(_33746, 3);
    _33746 = NOVALUE;

    /**     pc += 4*/
    _70pc_65963 = _70pc_65963 + 4;

    /** end procedure*/
    _33735 = NOVALUE;
    _33737 = NOVALUE;
    _33739 = NOVALUE;
    DeRef(_33742);
    _33742 = NOVALUE;
    _33743 = NOVALUE;
    return;
    ;
}


void _70opASSIGN_OP_SUBS()
{
    int _x_66941 = NOVALUE;
    int _33759 = NOVALUE;
    int _33758 = NOVALUE;
    int _33757 = NOVALUE;
    int _33756 = NOVALUE;
    int _33755 = NOVALUE;
    int _33752 = NOVALUE;
    int _33750 = NOVALUE;
    int _33748 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33748 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33748);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     b = Code[pc+2]*/
    _33750 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70b_65965 = (int)*(((s1_ptr)_2)->base + _33750);
    if (!IS_ATOM_INT(_70b_65965)){
        _70b_65965 = (long)DBL_PTR(_70b_65965)->dbl;
    }

    /**     target = Code[pc+3]*/
    _33752 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70target_65968 = (int)*(((s1_ptr)_2)->base + _33752);
    if (!IS_ATOM_INT(_70target_65968)){
        _70target_65968 = (long)DBL_PTR(_70target_65968)->dbl;
    }

    /** 	il( sprintf("ASSIGN_OP_SUBS: %s, %s => %s", names( Code[pc+1..pc+3] )), 3 )*/
    _33755 = _70pc_65963 + 1;
    if (_33755 > MAXINT){
        _33755 = NewDouble((double)_33755);
    }
    _33756 = _70pc_65963 + 3;
    rhs_slice_target = (object_ptr)&_33757;
    RHS_Slice(_25Code_12355, _33755, _33756);
    _33758 = _70names(_33757);
    _33757 = NOVALUE;
    _33759 = EPrintf(-9999999, _33754, _33758);
    DeRef(_33758);
    _33758 = NOVALUE;
    _70il(_33759, 3);
    _33759 = NOVALUE;

    /**     pc += 4*/
    _70pc_65963 = _70pc_65963 + 4;

    /** end procedure*/
    _33748 = NOVALUE;
    _33750 = NOVALUE;
    _33752 = NOVALUE;
    DeRef(_33755);
    _33755 = NOVALUE;
    _33756 = NOVALUE;
    return;
    ;
}


void _70opPASSIGN_OP_SUBS()
{
    int _33779 = NOVALUE;
    int _33778 = NOVALUE;
    int _33777 = NOVALUE;
    int _33776 = NOVALUE;
    int _33775 = NOVALUE;
    int _33774 = NOVALUE;
    int _33773 = NOVALUE;
    int _33772 = NOVALUE;
    int _33771 = NOVALUE;
    int _33770 = NOVALUE;
    int _33769 = NOVALUE;
    int _33768 = NOVALUE;
    int _33765 = NOVALUE;
    int _33763 = NOVALUE;
    int _33761 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33761 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33761);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     b = Code[pc+2]*/
    _33763 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70b_65965 = (int)*(((s1_ptr)_2)->base + _33763);
    if (!IS_ATOM_INT(_70b_65965)){
        _70b_65965 = (long)DBL_PTR(_70b_65965)->dbl;
    }

    /**     target = Code[pc+3]*/
    _33765 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70target_65968 = (int)*(((s1_ptr)_2)->base + _33765);
    if (!IS_ATOM_INT(_70target_65968)){
        _70target_65968 = (long)DBL_PTR(_70target_65968)->dbl;
    }

    /** 	il( sprintf("PASSIGN_OP_SUBS: %s, %s => %s (patch %04dd => %d)",*/
    _33768 = _70pc_65963 + 1;
    if (_33768 > MAXINT){
        _33768 = NewDouble((double)_33768);
    }
    _33769 = _70pc_65963 + 3;
    rhs_slice_target = (object_ptr)&_33770;
    RHS_Slice(_25Code_12355, _33768, _33769);
    _33771 = _70names(_33770);
    _33770 = NOVALUE;
    _33772 = _70pc_65963 + 9;
    if ((long)((unsigned long)_33772 + (unsigned long)HIGH_BITS) >= 0) 
    _33772 = NewDouble((double)_33772);
    _33773 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33774 = (int)*(((s1_ptr)_2)->base + _33773);
    {
        int concat_list[3];

        concat_list[0] = _33774;
        concat_list[1] = _33772;
        concat_list[2] = _33771;
        Concat_N((object_ptr)&_33775, concat_list, 3);
    }
    _33774 = NOVALUE;
    DeRef(_33772);
    _33772 = NOVALUE;
    DeRef(_33771);
    _33771 = NOVALUE;
    _33776 = EPrintf(-9999999, _33767, _33775);
    DeRefDS(_33775);
    _33775 = NOVALUE;
    _70il(_33776, 3);
    _33776 = NOVALUE;

    /**     Code[pc+9] = Code[pc+1] -- patch upcoming op*/
    _33777 = _70pc_65963 + 9;
    if ((long)((unsigned long)_33777 + (unsigned long)HIGH_BITS) >= 0) 
    _33777 = NewDouble((double)_33777);
    _33778 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33779 = (int)*(((s1_ptr)_2)->base + _33778);
    Ref(_33779);
    _2 = (int)SEQ_PTR(_25Code_12355);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _25Code_12355 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_33777))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_33777)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _33777);
    _1 = *(int *)_2;
    *(int *)_2 = _33779;
    if( _1 != _33779 ){
        DeRef(_1);
    }
    _33779 = NOVALUE;

    /**     pc += 4*/
    _70pc_65963 = _70pc_65963 + 4;

    /** end procedure*/
    _33761 = NOVALUE;
    _33763 = NOVALUE;
    _33765 = NOVALUE;
    DeRef(_33768);
    _33768 = NOVALUE;
    _33769 = NOVALUE;
    DeRef(_33777);
    _33777 = NOVALUE;
    _33778 = NOVALUE;
    _33773 = NOVALUE;
    return;
    ;
}


void _70opASSIGN_OP_SLICE()
{
    int _x_66990 = NOVALUE;
    int _33792 = NOVALUE;
    int _33791 = NOVALUE;
    int _33790 = NOVALUE;
    int _33787 = NOVALUE;
    int _33785 = NOVALUE;
    int _33783 = NOVALUE;
    int _33781 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33781 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33781);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     b = Code[pc+2]*/
    _33783 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70b_65965 = (int)*(((s1_ptr)_2)->base + _33783);
    if (!IS_ATOM_INT(_70b_65965)){
        _70b_65965 = (long)DBL_PTR(_70b_65965)->dbl;
    }

    /**     c = Code[pc+3]*/
    _33785 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70c_65966 = (int)*(((s1_ptr)_2)->base + _33785);
    if (!IS_ATOM_INT(_70c_65966)){
        _70c_65966 = (long)DBL_PTR(_70c_65966)->dbl;
    }

    /**     target = Code[pc+4]*/
    _33787 = _70pc_65963 + 4;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70target_65968 = (int)*(((s1_ptr)_2)->base + _33787);
    if (!IS_ATOM_INT(_70target_65968)){
        _70target_65968 = (long)DBL_PTR(_70target_65968)->dbl;
    }

    /**     il( sprintf("ASSIGN_OP_SLICE: %s, %s, %s => %s", names(a&b&c&target)),4 )*/
    {
        int concat_list[4];

        concat_list[0] = _70target_65968;
        concat_list[1] = _70c_65966;
        concat_list[2] = _70b_65965;
        concat_list[3] = _70a_65964;
        Concat_N((object_ptr)&_33790, concat_list, 4);
    }
    _33791 = _70names(_33790);
    _33790 = NOVALUE;
    _33792 = EPrintf(-9999999, _33789, _33791);
    DeRef(_33791);
    _33791 = NOVALUE;
    _70il(_33792, 4);
    _33792 = NOVALUE;

    /**     pc += 5*/
    _70pc_65963 = _70pc_65963 + 5;

    /** end procedure*/
    _33781 = NOVALUE;
    _33783 = NOVALUE;
    _33785 = NOVALUE;
    _33787 = NOVALUE;
    return;
    ;
}


void _70opPASSIGN_OP_SLICE()
{
    int _x_67010 = NOVALUE;
    int _33812 = NOVALUE;
    int _33811 = NOVALUE;
    int _33810 = NOVALUE;
    int _33809 = NOVALUE;
    int _33808 = NOVALUE;
    int _33807 = NOVALUE;
    int _33806 = NOVALUE;
    int _33805 = NOVALUE;
    int _33804 = NOVALUE;
    int _33803 = NOVALUE;
    int _33800 = NOVALUE;
    int _33798 = NOVALUE;
    int _33796 = NOVALUE;
    int _33794 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33794 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33794);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     b = Code[pc+2]*/
    _33796 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70b_65965 = (int)*(((s1_ptr)_2)->base + _33796);
    if (!IS_ATOM_INT(_70b_65965)){
        _70b_65965 = (long)DBL_PTR(_70b_65965)->dbl;
    }

    /**     c = Code[pc+3]*/
    _33798 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70c_65966 = (int)*(((s1_ptr)_2)->base + _33798);
    if (!IS_ATOM_INT(_70c_65966)){
        _70c_65966 = (long)DBL_PTR(_70c_65966)->dbl;
    }

    /**     target = Code[pc+4]*/
    _33800 = _70pc_65963 + 4;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70target_65968 = (int)*(((s1_ptr)_2)->base + _33800);
    if (!IS_ATOM_INT(_70target_65968)){
        _70target_65968 = (long)DBL_PTR(_70target_65968)->dbl;
    }

    /**     il( sprintf("PASSIGN_OP_SLICE: %s, %s, %s => %s (patch %04d => %d",*/
    {
        int concat_list[4];

        concat_list[0] = _70target_65968;
        concat_list[1] = _70c_65966;
        concat_list[2] = _70b_65965;
        concat_list[3] = _70a_65964;
        Concat_N((object_ptr)&_33803, concat_list, 4);
    }
    _33804 = _70names(_33803);
    _33803 = NOVALUE;
    _33805 = _70pc_65963 + 10;
    if ((long)((unsigned long)_33805 + (unsigned long)HIGH_BITS) >= 0) 
    _33805 = NewDouble((double)_33805);
    _33806 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33807 = (int)*(((s1_ptr)_2)->base + _33806);
    {
        int concat_list[3];

        concat_list[0] = _33807;
        concat_list[1] = _33805;
        concat_list[2] = _33804;
        Concat_N((object_ptr)&_33808, concat_list, 3);
    }
    _33807 = NOVALUE;
    DeRef(_33805);
    _33805 = NOVALUE;
    DeRef(_33804);
    _33804 = NOVALUE;
    _33809 = EPrintf(-9999999, _33802, _33808);
    DeRefDS(_33808);
    _33808 = NOVALUE;
    _70il(_33809, 4);
    _33809 = NOVALUE;

    /**     Code[pc+10] = Code[pc+1]*/
    _33810 = _70pc_65963 + 10;
    if ((long)((unsigned long)_33810 + (unsigned long)HIGH_BITS) >= 0) 
    _33810 = NewDouble((double)_33810);
    _33811 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33812 = (int)*(((s1_ptr)_2)->base + _33811);
    Ref(_33812);
    _2 = (int)SEQ_PTR(_25Code_12355);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _25Code_12355 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_33810))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_33810)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _33810);
    _1 = *(int *)_2;
    *(int *)_2 = _33812;
    if( _1 != _33812 ){
        DeRef(_1);
    }
    _33812 = NOVALUE;

    /**     pc += 5*/
    _70pc_65963 = _70pc_65963 + 5;

    /** end procedure*/
    _33794 = NOVALUE;
    _33796 = NOVALUE;
    _33798 = NOVALUE;
    _33800 = NOVALUE;
    DeRef(_33810);
    _33810 = NOVALUE;
    _33811 = NOVALUE;
    _33806 = NOVALUE;
    return;
    ;
}


void _70opASSIGN_SLICE()
{
    int _x_67040 = NOVALUE;
    int _33825 = NOVALUE;
    int _33824 = NOVALUE;
    int _33823 = NOVALUE;
    int _33820 = NOVALUE;
    int _33818 = NOVALUE;
    int _33816 = NOVALUE;
    int _33814 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]  -- sequence*/
    _33814 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33814);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     b = Code[pc+2]  -- 1st index*/
    _33816 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70b_65965 = (int)*(((s1_ptr)_2)->base + _33816);
    if (!IS_ATOM_INT(_70b_65965)){
        _70b_65965 = (long)DBL_PTR(_70b_65965)->dbl;
    }

    /**     c = Code[pc+3]  -- 2nd index*/
    _33818 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70c_65966 = (int)*(((s1_ptr)_2)->base + _33818);
    if (!IS_ATOM_INT(_70c_65966)){
        _70c_65966 = (long)DBL_PTR(_70c_65966)->dbl;
    }

    /**     d = Code[pc+4]  -- rhs value to assign*/
    _33820 = _70pc_65963 + 4;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70d_65967 = (int)*(((s1_ptr)_2)->base + _33820);
    if (!IS_ATOM_INT(_70d_65967)){
        _70d_65967 = (long)DBL_PTR(_70d_65967)->dbl;
    }

    /**     il(sprintf("ASSIGN_SLICE: %s %s..%s => %s", names({a,b,c,d})),4)*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _70a_65964;
    *((int *)(_2+8)) = _70b_65965;
    *((int *)(_2+12)) = _70c_65966;
    *((int *)(_2+16)) = _70d_65967;
    _33823 = MAKE_SEQ(_1);
    _33824 = _70names(_33823);
    _33823 = NOVALUE;
    _33825 = EPrintf(-9999999, _33822, _33824);
    DeRef(_33824);
    _33824 = NOVALUE;
    _70il(_33825, 4);
    _33825 = NOVALUE;

    /**     pc += 5*/
    _70pc_65963 = _70pc_65963 + 5;

    /** end procedure*/
    _33814 = NOVALUE;
    _33816 = NOVALUE;
    _33818 = NOVALUE;
    _33820 = NOVALUE;
    return;
    ;
}


void _70opPASSIGN_SLICE()
{
    int _33838 = NOVALUE;
    int _33837 = NOVALUE;
    int _33836 = NOVALUE;
    int _33833 = NOVALUE;
    int _33831 = NOVALUE;
    int _33829 = NOVALUE;
    int _33827 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]  -- sequence*/
    _33827 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33827);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     b = Code[pc+2]  -- 1st index*/
    _33829 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70b_65965 = (int)*(((s1_ptr)_2)->base + _33829);
    if (!IS_ATOM_INT(_70b_65965)){
        _70b_65965 = (long)DBL_PTR(_70b_65965)->dbl;
    }

    /**     c = Code[pc+3]  -- 2nd index*/
    _33831 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70c_65966 = (int)*(((s1_ptr)_2)->base + _33831);
    if (!IS_ATOM_INT(_70c_65966)){
        _70c_65966 = (long)DBL_PTR(_70c_65966)->dbl;
    }

    /**     d = Code[pc+4]  -- rhs value to assign*/
    _33833 = _70pc_65963 + 4;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70d_65967 = (int)*(((s1_ptr)_2)->base + _33833);
    if (!IS_ATOM_INT(_70d_65967)){
        _70d_65967 = (long)DBL_PTR(_70d_65967)->dbl;
    }

    /**     il(sprintf("PASSIGN_SLICE: %s %s..%s => %s", names({a,b,c,d})),4)*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _70a_65964;
    *((int *)(_2+8)) = _70b_65965;
    *((int *)(_2+12)) = _70c_65966;
    *((int *)(_2+16)) = _70d_65967;
    _33836 = MAKE_SEQ(_1);
    _33837 = _70names(_33836);
    _33836 = NOVALUE;
    _33838 = EPrintf(-9999999, _33835, _33837);
    DeRef(_33837);
    _33837 = NOVALUE;
    _70il(_33838, 4);
    _33838 = NOVALUE;

    /**     pc += 5*/
    _70pc_65963 = _70pc_65963 + 5;

    /** end procedure*/
    _33827 = NOVALUE;
    _33829 = NOVALUE;
    _33831 = NOVALUE;
    _33833 = NOVALUE;
    return;
    ;
}


void _70opRHS_SLICE()
{
    int _x_67079 = NOVALUE;
    int _33851 = NOVALUE;
    int _33850 = NOVALUE;
    int _33849 = NOVALUE;
    int _33846 = NOVALUE;
    int _33844 = NOVALUE;
    int _33842 = NOVALUE;
    int _33840 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]  -- sequence*/
    _33840 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33840);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     b = Code[pc+2]  -- 1st index*/
    _33842 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70b_65965 = (int)*(((s1_ptr)_2)->base + _33842);
    if (!IS_ATOM_INT(_70b_65965)){
        _70b_65965 = (long)DBL_PTR(_70b_65965)->dbl;
    }

    /**     c = Code[pc+3]  -- 2nd index*/
    _33844 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70c_65966 = (int)*(((s1_ptr)_2)->base + _33844);
    if (!IS_ATOM_INT(_70c_65966)){
        _70c_65966 = (long)DBL_PTR(_70c_65966)->dbl;
    }

    /**     target = Code[pc+4]*/
    _33846 = _70pc_65963 + 4;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70target_65968 = (int)*(((s1_ptr)_2)->base + _33846);
    if (!IS_ATOM_INT(_70target_65968)){
        _70target_65968 = (long)DBL_PTR(_70target_65968)->dbl;
    }

    /**     il(sprintf("RHS_SLICE: %s %s..%s => %s", names({a,b,c,target})),4)*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _70a_65964;
    *((int *)(_2+8)) = _70b_65965;
    *((int *)(_2+12)) = _70c_65966;
    *((int *)(_2+16)) = _70target_65968;
    _33849 = MAKE_SEQ(_1);
    _33850 = _70names(_33849);
    _33849 = NOVALUE;
    _33851 = EPrintf(-9999999, _33848, _33850);
    DeRef(_33850);
    _33850 = NOVALUE;
    _70il(_33851, 4);
    _33851 = NOVALUE;

    /**     pc += 5*/
    _70pc_65963 = _70pc_65963 + 5;

    /** end procedure*/
    _33840 = NOVALUE;
    _33842 = NOVALUE;
    _33844 = NOVALUE;
    _33846 = NOVALUE;
    return;
    ;
}


void _70opTYPE_CHECK_FORWARD()
{
    int _33861 = NOVALUE;
    int _33860 = NOVALUE;
    int _33859 = NOVALUE;
    int _33858 = NOVALUE;
    int _33857 = NOVALUE;
    int _33856 = NOVALUE;
    int _33855 = NOVALUE;
    int _33854 = NOVALUE;
    int _0, _1, _2;
    

    /** 	il( sprintf("TYPE_CHECK_FORWARD: %s OpTypeCheck: %d", names({Code[pc+1]}) & Code[pc+2] ), 2 )*/
    _33854 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33855 = (int)*(((s1_ptr)_2)->base + _33854);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_33855);
    *((int *)(_2+4)) = _33855;
    _33856 = MAKE_SEQ(_1);
    _33855 = NOVALUE;
    _33857 = _70names(_33856);
    _33856 = NOVALUE;
    _33858 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33859 = (int)*(((s1_ptr)_2)->base + _33858);
    if (IS_SEQUENCE(_33857) && IS_ATOM(_33859)) {
        Ref(_33859);
        Append(&_33860, _33857, _33859);
    }
    else if (IS_ATOM(_33857) && IS_SEQUENCE(_33859)) {
        Ref(_33857);
        Prepend(&_33860, _33859, _33857);
    }
    else {
        Concat((object_ptr)&_33860, _33857, _33859);
        DeRef(_33857);
        _33857 = NOVALUE;
    }
    DeRef(_33857);
    _33857 = NOVALUE;
    _33859 = NOVALUE;
    _33861 = EPrintf(-9999999, _33853, _33860);
    DeRefDS(_33860);
    _33860 = NOVALUE;
    _70il(_33861, 2);
    _33861 = NOVALUE;

    /** 	pc += 3*/
    _70pc_65963 = _70pc_65963 + 3;

    /** end procedure*/
    _33854 = NOVALUE;
    _33858 = NOVALUE;
    return;
    ;
}


void _70opTYPE_CHECK()
{
    int _0, _1, _2;
    

    /** 	pnonary()*/
    _70pnonary();

    /** end procedure*/
    return;
    ;
}


void _70is_an()
{
    int _33874 = NOVALUE;
    int _33873 = NOVALUE;
    int _33872 = NOVALUE;
    int _33871 = NOVALUE;
    int _33870 = NOVALUE;
    int _33869 = NOVALUE;
    int _33868 = NOVALUE;
    int _33865 = NOVALUE;
    int _33863 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33863 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33863);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     target = Code[pc+2]*/
    _33865 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70target_65968 = (int)*(((s1_ptr)_2)->base + _33865);
    if (!IS_ATOM_INT(_70target_65968)){
        _70target_65968 = (long)DBL_PTR(_70target_65968)->dbl;
    }

    /**     il( sprintf("%s: %s %s",{ opnames[Code[pc]] } & names(a&target)), 2 )*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33868 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33868)){
        _33869 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33868)->dbl));
    }
    else{
        _33869 = (int)*(((s1_ptr)_2)->base + _33868);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_33869);
    *((int *)(_2+4)) = _33869;
    _33870 = MAKE_SEQ(_1);
    _33869 = NOVALUE;
    Concat((object_ptr)&_33871, _70a_65964, _70target_65968);
    _33872 = _70names(_33871);
    _33871 = NOVALUE;
    if (IS_SEQUENCE(_33870) && IS_ATOM(_33872)) {
        Ref(_33872);
        Append(&_33873, _33870, _33872);
    }
    else if (IS_ATOM(_33870) && IS_SEQUENCE(_33872)) {
    }
    else {
        Concat((object_ptr)&_33873, _33870, _33872);
        DeRefDS(_33870);
        _33870 = NOVALUE;
    }
    DeRef(_33870);
    _33870 = NOVALUE;
    DeRef(_33872);
    _33872 = NOVALUE;
    _33874 = EPrintf(-9999999, _33867, _33873);
    DeRefDS(_33873);
    _33873 = NOVALUE;
    _70il(_33874, 2);
    _33874 = NOVALUE;

    /**     pc += 3*/
    _70pc_65963 = _70pc_65963 + 3;

    /** end procedure*/
    _33863 = NOVALUE;
    _33865 = NOVALUE;
    _33868 = NOVALUE;
    return;
    ;
}


void _70opIS_AN_INTEGER()
{
    int _0, _1, _2;
    

    /**     is_an()*/
    _70is_an();

    /** end procedure*/
    return;
    ;
}


void _70opIS_AN_ATOM()
{
    int _0, _1, _2;
    

    /**     is_an()*/
    _70is_an();

    /** end procedure*/
    return;
    ;
}


void _70opIS_A_SEQUENCE()
{
    int _0, _1, _2;
    

    /**     is_an()*/
    _70is_an();

    /** end procedure*/
    return;
    ;
}


void _70opIS_AN_OBJECT()
{
    int _0, _1, _2;
    

    /**     is_an()*/
    _70is_an();

    /** end procedure*/
    return;
    ;
}


void _70opSQRT()
{
    int _0, _1, _2;
    

    /** 	unary()*/
    _70unary();

    /** end procedure*/
    return;
    ;
}


void _70opSIN()
{
    int _0, _1, _2;
    

    /** 	unary()*/
    _70unary();

    /** end procedure*/
    return;
    ;
}


void _70opCOS()
{
    int _0, _1, _2;
    

    /** 	unary()*/
    _70unary();

    /** end procedure*/
    return;
    ;
}


void _70opTAN()
{
    int _0, _1, _2;
    

    /** 	unary()*/
    _70unary();

    /** end procedure*/
    return;
    ;
}


void _70opARCTAN()
{
    int _0, _1, _2;
    

    /** 	unary()*/
    _70unary();

    /** end procedure*/
    return;
    ;
}


void _70opLOG()
{
    int _0, _1, _2;
    

    /** 	unary()*/
    _70unary();

    /** end procedure*/
    return;
    ;
}


void _70opNOT_BITS()
{
    int _0, _1, _2;
    

    /** 	unary()*/
    _70unary();

    /** end procedure*/
    return;
    ;
}


void _70opFLOOR()
{
    int _0, _1, _2;
    

    /** 	unary()*/
    _70unary();

    /** end procedure*/
    return;
    ;
}


void _70opNOT_IFW()
{
    int _33884 = NOVALUE;
    int _33883 = NOVALUE;
    int _33882 = NOVALUE;
    int _33881 = NOVALUE;
    int _33880 = NOVALUE;
    int _33879 = NOVALUE;
    int _33876 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33876 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33876);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     il( sprintf( "NOT_IFW %s goto %04d else goto %04d",*/
    _33879 = _70name_or_literal(_70a_65964);
    _33880 = _70pc_65963 + 3;
    if ((long)((unsigned long)_33880 + (unsigned long)HIGH_BITS) >= 0) 
    _33880 = NewDouble((double)_33880);
    _33881 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33882 = (int)*(((s1_ptr)_2)->base + _33881);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _33879;
    *((int *)(_2+8)) = _33880;
    Ref(_33882);
    *((int *)(_2+12)) = _33882;
    _33883 = MAKE_SEQ(_1);
    _33882 = NOVALUE;
    _33880 = NOVALUE;
    _33879 = NOVALUE;
    _33884 = EPrintf(-9999999, _33878, _33883);
    DeRefDS(_33883);
    _33883 = NOVALUE;
    _70il(_33884, 2);
    _33884 = NOVALUE;

    /**     pc += 3*/
    _70pc_65963 = _70pc_65963 + 3;

    /** end procedure*/
    _33876 = NOVALUE;
    _33881 = NOVALUE;
    return;
    ;
}


void _70opNOT()
{
    int _0, _1, _2;
    

    /** 	unary()*/
    _70unary();

    /** end procedure*/
    return;
    ;
}


void _70opUMINUS()
{
    int _0, _1, _2;
    

    /** 	unary()*/
    _70unary();

    /** end procedure*/
    return;
    ;
}


void _70opRAND()
{
    int _0, _1, _2;
    

    /** 	unary()*/
    _70unary();

    /** end procedure*/
    return;
    ;
}


void _70opDIV2()
{
    int _33893 = NOVALUE;
    int _33892 = NOVALUE;
    int _33891 = NOVALUE;
    int _33888 = NOVALUE;
    int _33886 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33886 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33886);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     target = Code[pc+3]*/
    _33888 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70target_65968 = (int)*(((s1_ptr)_2)->base + _33888);
    if (!IS_ATOM_INT(_70target_65968)){
        _70target_65968 = (long)DBL_PTR(_70target_65968)->dbl;
    }

    /**     il( sprintf("DIV2: %s => %s",names( a & target )), 3 )*/
    Concat((object_ptr)&_33891, _70a_65964, _70target_65968);
    _33892 = _70names(_33891);
    _33891 = NOVALUE;
    _33893 = EPrintf(-9999999, _33890, _33892);
    DeRef(_33892);
    _33892 = NOVALUE;
    _70il(_33893, 3);
    _33893 = NOVALUE;

    /**     pc += 4*/
    _70pc_65963 = _70pc_65963 + 4;

    /** end procedure*/
    _33886 = NOVALUE;
    _33888 = NOVALUE;
    return;
    ;
}


void _70opFLOOR_DIV2()
{
    int _33902 = NOVALUE;
    int _33901 = NOVALUE;
    int _33900 = NOVALUE;
    int _33897 = NOVALUE;
    int _33895 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33895 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33895);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     target = Code[pc+3]*/
    _33897 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70target_65968 = (int)*(((s1_ptr)_2)->base + _33897);
    if (!IS_ATOM_INT(_70target_65968)){
        _70target_65968 = (long)DBL_PTR(_70target_65968)->dbl;
    }

    /**     il( sprintf("FLOOR_DIV2: %s => %s",names( a & target )), 3 )*/
    Concat((object_ptr)&_33900, _70a_65964, _70target_65968);
    _33901 = _70names(_33900);
    _33900 = NOVALUE;
    _33902 = EPrintf(-9999999, _33899, _33901);
    DeRef(_33901);
    _33901 = NOVALUE;
    _70il(_33902, 3);
    _33902 = NOVALUE;

    /**     pc += 4*/
    _70pc_65963 = _70pc_65963 + 4;

    /** end procedure*/
    _33895 = NOVALUE;
    _33897 = NOVALUE;
    return;
    ;
}


void _70opGREATER_IFW()
{
    int _33915 = NOVALUE;
    int _33914 = NOVALUE;
    int _33913 = NOVALUE;
    int _33912 = NOVALUE;
    int _33911 = NOVALUE;
    int _33910 = NOVALUE;
    int _33909 = NOVALUE;
    int _33906 = NOVALUE;
    int _33904 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33904 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33904);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     b = Code[pc+2]*/
    _33906 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70b_65965 = (int)*(((s1_ptr)_2)->base + _33906);
    if (!IS_ATOM_INT(_70b_65965)){
        _70b_65965 = (long)DBL_PTR(_70b_65965)->dbl;
    }

    /**     il( sprintf( "GREATER_IFW %s > %s goto %04d else goto %04d",*/
    _33909 = _70name_or_literal(_70a_65964);
    _33910 = _70name_or_literal(_70b_65965);
    _33911 = _70pc_65963 + 4;
    if ((long)((unsigned long)_33911 + (unsigned long)HIGH_BITS) >= 0) 
    _33911 = NewDouble((double)_33911);
    _33912 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33913 = (int)*(((s1_ptr)_2)->base + _33912);
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _33909;
    *((int *)(_2+8)) = _33910;
    *((int *)(_2+12)) = _33911;
    Ref(_33913);
    *((int *)(_2+16)) = _33913;
    _33914 = MAKE_SEQ(_1);
    _33913 = NOVALUE;
    _33911 = NOVALUE;
    _33910 = NOVALUE;
    _33909 = NOVALUE;
    _33915 = EPrintf(-9999999, _33908, _33914);
    DeRefDS(_33914);
    _33914 = NOVALUE;
    _70il(_33915, 3);
    _33915 = NOVALUE;

    /** 	pc += 4*/
    _70pc_65963 = _70pc_65963 + 4;

    /** end procedure*/
    _33904 = NOVALUE;
    _33906 = NOVALUE;
    _33912 = NOVALUE;
    return;
    ;
}


void _70opNOTEQ_IFW()
{
    int _33928 = NOVALUE;
    int _33927 = NOVALUE;
    int _33926 = NOVALUE;
    int _33925 = NOVALUE;
    int _33924 = NOVALUE;
    int _33923 = NOVALUE;
    int _33922 = NOVALUE;
    int _33919 = NOVALUE;
    int _33917 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33917 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33917);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     b = Code[pc+2]*/
    _33919 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70b_65965 = (int)*(((s1_ptr)_2)->base + _33919);
    if (!IS_ATOM_INT(_70b_65965)){
        _70b_65965 = (long)DBL_PTR(_70b_65965)->dbl;
    }

    /**     il( sprintf( "NOTEQ_IFW %s != %s goto %04d else goto %04d",*/
    _33922 = _70name_or_literal(_70a_65964);
    _33923 = _70name_or_literal(_70b_65965);
    _33924 = _70pc_65963 + 4;
    if ((long)((unsigned long)_33924 + (unsigned long)HIGH_BITS) >= 0) 
    _33924 = NewDouble((double)_33924);
    _33925 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33926 = (int)*(((s1_ptr)_2)->base + _33925);
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _33922;
    *((int *)(_2+8)) = _33923;
    *((int *)(_2+12)) = _33924;
    Ref(_33926);
    *((int *)(_2+16)) = _33926;
    _33927 = MAKE_SEQ(_1);
    _33926 = NOVALUE;
    _33924 = NOVALUE;
    _33923 = NOVALUE;
    _33922 = NOVALUE;
    _33928 = EPrintf(-9999999, _33921, _33927);
    DeRefDS(_33927);
    _33927 = NOVALUE;
    _70il(_33928, 3);
    _33928 = NOVALUE;

    /** 	pc += 4*/
    _70pc_65963 = _70pc_65963 + 4;

    /** end procedure*/
    _33917 = NOVALUE;
    _33919 = NOVALUE;
    _33925 = NOVALUE;
    return;
    ;
}


void _70opLESSEQ_IFW()
{
    int _33941 = NOVALUE;
    int _33940 = NOVALUE;
    int _33939 = NOVALUE;
    int _33938 = NOVALUE;
    int _33937 = NOVALUE;
    int _33936 = NOVALUE;
    int _33935 = NOVALUE;
    int _33932 = NOVALUE;
    int _33930 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33930 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33930);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     b = Code[pc+2]*/
    _33932 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70b_65965 = (int)*(((s1_ptr)_2)->base + _33932);
    if (!IS_ATOM_INT(_70b_65965)){
        _70b_65965 = (long)DBL_PTR(_70b_65965)->dbl;
    }

    /**     il( sprintf( "LESSEQ_IFW %s <= %s goto %04d else goto %04d",*/
    _33935 = _70name_or_literal(_70a_65964);
    _33936 = _70name_or_literal(_70b_65965);
    _33937 = _70pc_65963 + 4;
    if ((long)((unsigned long)_33937 + (unsigned long)HIGH_BITS) >= 0) 
    _33937 = NewDouble((double)_33937);
    _33938 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33939 = (int)*(((s1_ptr)_2)->base + _33938);
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _33935;
    *((int *)(_2+8)) = _33936;
    *((int *)(_2+12)) = _33937;
    Ref(_33939);
    *((int *)(_2+16)) = _33939;
    _33940 = MAKE_SEQ(_1);
    _33939 = NOVALUE;
    _33937 = NOVALUE;
    _33936 = NOVALUE;
    _33935 = NOVALUE;
    _33941 = EPrintf(-9999999, _33934, _33940);
    DeRefDS(_33940);
    _33940 = NOVALUE;
    _70il(_33941, 3);
    _33941 = NOVALUE;

    /** 	pc += 4*/
    _70pc_65963 = _70pc_65963 + 4;

    /** end procedure*/
    _33930 = NOVALUE;
    _33932 = NOVALUE;
    _33938 = NOVALUE;
    return;
    ;
}


void _70opGREATEREQ_IFW()
{
    int _33954 = NOVALUE;
    int _33953 = NOVALUE;
    int _33952 = NOVALUE;
    int _33951 = NOVALUE;
    int _33950 = NOVALUE;
    int _33949 = NOVALUE;
    int _33948 = NOVALUE;
    int _33945 = NOVALUE;
    int _33943 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33943 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33943);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     b = Code[pc+2]*/
    _33945 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70b_65965 = (int)*(((s1_ptr)_2)->base + _33945);
    if (!IS_ATOM_INT(_70b_65965)){
        _70b_65965 = (long)DBL_PTR(_70b_65965)->dbl;
    }

    /**     il( sprintf( "GREATEREQ_IFW %s > %s goto %04d else goto %04d",*/
    _33948 = _70name_or_literal(_70a_65964);
    _33949 = _70name_or_literal(_70b_65965);
    _33950 = _70pc_65963 + 4;
    if ((long)((unsigned long)_33950 + (unsigned long)HIGH_BITS) >= 0) 
    _33950 = NewDouble((double)_33950);
    _33951 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33952 = (int)*(((s1_ptr)_2)->base + _33951);
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _33948;
    *((int *)(_2+8)) = _33949;
    *((int *)(_2+12)) = _33950;
    Ref(_33952);
    *((int *)(_2+16)) = _33952;
    _33953 = MAKE_SEQ(_1);
    _33952 = NOVALUE;
    _33950 = NOVALUE;
    _33949 = NOVALUE;
    _33948 = NOVALUE;
    _33954 = EPrintf(-9999999, _33947, _33953);
    DeRefDS(_33953);
    _33953 = NOVALUE;
    _70il(_33954, 3);
    _33954 = NOVALUE;

    /** 	pc += 4*/
    _70pc_65963 = _70pc_65963 + 4;

    /** end procedure*/
    _33943 = NOVALUE;
    _33945 = NOVALUE;
    _33951 = NOVALUE;
    return;
    ;
}


void _70opEQUALS_IFW()
{
    int _i_67282 = NOVALUE;
    int _33970 = NOVALUE;
    int _33969 = NOVALUE;
    int _33968 = NOVALUE;
    int _33967 = NOVALUE;
    int _33966 = NOVALUE;
    int _33965 = NOVALUE;
    int _33964 = NOVALUE;
    int _33960 = NOVALUE;
    int _33958 = NOVALUE;
    int _33956 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33956 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33956);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     b = Code[pc+2]*/
    _33958 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70b_65965 = (int)*(((s1_ptr)_2)->base + _33958);
    if (!IS_ATOM_INT(_70b_65965)){
        _70b_65965 = (long)DBL_PTR(_70b_65965)->dbl;
    }

    /** 	sequence i*/

    /** 	if Code[pc] = EQUALS_IFW then*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33960 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    if (binary_op_a(NOTEQ, _33960, 104)){
        _33960 = NOVALUE;
        goto L1; // [47] 61
    }
    _33960 = NOVALUE;

    /** 		i = ""*/
    RefDS(_22682);
    DeRefi(_i_67282);
    _i_67282 = _22682;
    goto L2; // [58] 69
L1: 

    /** 		i = "_I"*/
    RefDS(_33962);
    DeRefi(_i_67282);
    _i_67282 = _33962;
L2: 

    /**     il( sprintf( "EQUALS_IFW%s %s = %s goto %04d else goto %04d",*/
    _33964 = _70name_or_literal(_70a_65964);
    _33965 = _70name_or_literal(_70b_65965);
    _33966 = _70pc_65963 + 4;
    if ((long)((unsigned long)_33966 + (unsigned long)HIGH_BITS) >= 0) 
    _33966 = NewDouble((double)_33966);
    _33967 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33968 = (int)*(((s1_ptr)_2)->base + _33967);
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_i_67282);
    *((int *)(_2+4)) = _i_67282;
    *((int *)(_2+8)) = _33964;
    *((int *)(_2+12)) = _33965;
    *((int *)(_2+16)) = _33966;
    Ref(_33968);
    *((int *)(_2+20)) = _33968;
    _33969 = MAKE_SEQ(_1);
    _33968 = NOVALUE;
    _33966 = NOVALUE;
    _33965 = NOVALUE;
    _33964 = NOVALUE;
    _33970 = EPrintf(-9999999, _33963, _33969);
    DeRefDS(_33969);
    _33969 = NOVALUE;
    _70il(_33970, 3);
    _33970 = NOVALUE;

    /**     pc += 4*/
    _70pc_65963 = _70pc_65963 + 4;

    /** end procedure*/
    DeRefDSi(_i_67282);
    DeRef(_33956);
    _33956 = NOVALUE;
    DeRef(_33958);
    _33958 = NOVALUE;
    _33967 = NOVALUE;
    return;
    ;
}


void _70opLESS_IFW()
{
    int _33983 = NOVALUE;
    int _33982 = NOVALUE;
    int _33981 = NOVALUE;
    int _33980 = NOVALUE;
    int _33979 = NOVALUE;
    int _33978 = NOVALUE;
    int _33977 = NOVALUE;
    int _33974 = NOVALUE;
    int _33972 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33972 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33972);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     b = Code[pc+2]*/
    _33974 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70b_65965 = (int)*(((s1_ptr)_2)->base + _33974);
    if (!IS_ATOM_INT(_70b_65965)){
        _70b_65965 = (long)DBL_PTR(_70b_65965)->dbl;
    }

    /**     il( sprintf( "IFW %s < %s goto %04d else goto %04d",*/
    _33977 = _70name_or_literal(_70a_65964);
    _33978 = _70name_or_literal(_70b_65965);
    _33979 = _70pc_65963 + 4;
    if ((long)((unsigned long)_33979 + (unsigned long)HIGH_BITS) >= 0) 
    _33979 = NewDouble((double)_33979);
    _33980 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33981 = (int)*(((s1_ptr)_2)->base + _33980);
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _33977;
    *((int *)(_2+8)) = _33978;
    *((int *)(_2+12)) = _33979;
    Ref(_33981);
    *((int *)(_2+16)) = _33981;
    _33982 = MAKE_SEQ(_1);
    _33981 = NOVALUE;
    _33979 = NOVALUE;
    _33978 = NOVALUE;
    _33977 = NOVALUE;
    _33983 = EPrintf(-9999999, _33976, _33982);
    DeRefDS(_33982);
    _33982 = NOVALUE;
    _70il(_33983, 3);
    _33983 = NOVALUE;

    /**     pc += 4*/
    _70pc_65963 = _70pc_65963 + 4;

    /** end procedure*/
    _33972 = NOVALUE;
    _33974 = NOVALUE;
    _33980 = NOVALUE;
    return;
    ;
}


void _70opMULTIPLY()
{
    int _0, _1, _2;
    

    /**     binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opPLUS()
{
    int _0, _1, _2;
    

    /**     binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opMINUS()
{
    int _0, _1, _2;
    

    /**     binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opOR()
{
    int _0, _1, _2;
    

    /**     binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opXOR()
{
    int _0, _1, _2;
    

    /**     binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opAND()
{
    int _0, _1, _2;
    

    /**     binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opDIVIDE()
{
    int _0, _1, _2;
    

    /**     binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opREMAINDER()
{
    int _0, _1, _2;
    

    /**     binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opFLOOR_DIV()
{
    int _0, _1, _2;
    

    /**     binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opAND_BITS()
{
    int _0, _1, _2;
    

    /**     binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opOR_BITS()
{
    int _0, _1, _2;
    

    /**     binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opXOR_BITS()
{
    int _0, _1, _2;
    

    /**     binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opPOWER()
{
    int _0, _1, _2;
    

    /**     binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opLESS()
{
    int _0, _1, _2;
    

    /**     binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opGREATER()
{
    int _0, _1, _2;
    

    /**     binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opEQUALS()
{
    int _0, _1, _2;
    

    /**     binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opNOTEQ()
{
    int _0, _1, _2;
    

    /**     binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opLESSEQ()
{
    int _0, _1, _2;
    

    /**     binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opGREATEREQ()
{
    int _0, _1, _2;
    

    /**     binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70short_circuit()
{
    int _33998 = NOVALUE;
    int _33997 = NOVALUE;
    int _33996 = NOVALUE;
    int _33995 = NOVALUE;
    int _33994 = NOVALUE;
    int _33993 = NOVALUE;
    int _33992 = NOVALUE;
    int _33991 = NOVALUE;
    int _33990 = NOVALUE;
    int _33987 = NOVALUE;
    int _33985 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]*/
    _33985 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _33985);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     b = Code[pc+2]*/
    _33987 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70b_65965 = (int)*(((s1_ptr)_2)->base + _33987);
    if (!IS_ATOM_INT(_70b_65965)){
        _70b_65965 = (long)DBL_PTR(_70b_65965)->dbl;
    }

    /**     il( sprintf("%s: %s, %s, %04d", {opnames[Code[pc]]} & names(a&b) & Code[pc+3]), 3 )*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33990 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_33990)){
        _33991 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_33990)->dbl));
    }
    else{
        _33991 = (int)*(((s1_ptr)_2)->base + _33990);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_33991);
    *((int *)(_2+4)) = _33991;
    _33992 = MAKE_SEQ(_1);
    _33991 = NOVALUE;
    Concat((object_ptr)&_33993, _70a_65964, _70b_65965);
    _33994 = _70names(_33993);
    _33993 = NOVALUE;
    _33995 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _33996 = (int)*(((s1_ptr)_2)->base + _33995);
    {
        int concat_list[3];

        concat_list[0] = _33996;
        concat_list[1] = _33994;
        concat_list[2] = _33992;
        Concat_N((object_ptr)&_33997, concat_list, 3);
    }
    _33996 = NOVALUE;
    DeRef(_33994);
    _33994 = NOVALUE;
    DeRefDS(_33992);
    _33992 = NOVALUE;
    _33998 = EPrintf(-9999999, _33989, _33997);
    DeRefDS(_33997);
    _33997 = NOVALUE;
    _70il(_33998, 3);
    _33998 = NOVALUE;

    /**     pc += 4*/
    _70pc_65963 = _70pc_65963 + 4;

    /** end procedure*/
    _33985 = NOVALUE;
    _33987 = NOVALUE;
    _33990 = NOVALUE;
    _33995 = NOVALUE;
    return;
    ;
}


void _70opSC1_AND()
{
    int _0, _1, _2;
    

    /** 	short_circuit()*/
    _70short_circuit();

    /** end procedure*/
    return;
    ;
}


void _70opSC1_AND_IF()
{
    int _0, _1, _2;
    

    /** 	short_circuit()*/
    _70short_circuit();

    /** end procedure*/
    return;
    ;
}


void _70opSC1_OR()
{
    int _0, _1, _2;
    

    /** 	short_circuit()*/
    _70short_circuit();

    /** end procedure*/
    return;
    ;
}


void _70opSC1_OR_IF()
{
    int _0, _1, _2;
    

    /** 	short_circuit()*/
    _70short_circuit();

    /** end procedure*/
    return;
    ;
}


void _70opSC2_OR()
{
    int _0, _1, _2;
    

    /** 	pbinary()*/
    _70pbinary();

    /** end procedure*/
    return;
    ;
}


void _70opFOR()
{
    int _increment_67390 = NOVALUE;
    int _limit_67391 = NOVALUE;
    int _initial_67392 = NOVALUE;
    int _loopvar_67393 = NOVALUE;
    int _jump_67394 = NOVALUE;
    int _34024 = NOVALUE;
    int _34023 = NOVALUE;
    int _34022 = NOVALUE;
    int _34021 = NOVALUE;
    int _34020 = NOVALUE;
    int _34019 = NOVALUE;
    int _34018 = NOVALUE;
    int _34017 = NOVALUE;
    int _34016 = NOVALUE;
    int _34015 = NOVALUE;
    int _34014 = NOVALUE;
    int _34013 = NOVALUE;
    int _34012 = NOVALUE;
    int _34011 = NOVALUE;
    int _34008 = NOVALUE;
    int _34006 = NOVALUE;
    int _34004 = NOVALUE;
    int _34002 = NOVALUE;
    int _34000 = NOVALUE;
    int _0, _1, _2;
    

    /**     increment = Code[pc+1]*/
    _34000 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _increment_67390 = (int)*(((s1_ptr)_2)->base + _34000);
    if (!IS_ATOM_INT(_increment_67390)){
        _increment_67390 = (long)DBL_PTR(_increment_67390)->dbl;
    }

    /**     limit = Code[pc+2]*/
    _34002 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _limit_67391 = (int)*(((s1_ptr)_2)->base + _34002);
    if (!IS_ATOM_INT(_limit_67391)){
        _limit_67391 = (long)DBL_PTR(_limit_67391)->dbl;
    }

    /**     initial = Code[pc+3]*/
    _34004 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _initial_67392 = (int)*(((s1_ptr)_2)->base + _34004);
    if (!IS_ATOM_INT(_initial_67392)){
        _initial_67392 = (long)DBL_PTR(_initial_67392)->dbl;
    }

    /**     loopvar = Code[pc+5]*/
    _34006 = _70pc_65963 + 5;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _loopvar_67393 = (int)*(((s1_ptr)_2)->base + _34006);
    if (!IS_ATOM_INT(_loopvar_67393)){
        _loopvar_67393 = (long)DBL_PTR(_loopvar_67393)->dbl;
    }

    /**     jump = Code[pc+6]*/
    _34008 = _70pc_65963 + 6;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _jump_67394 = (int)*(((s1_ptr)_2)->base + _34008);
    if (!IS_ATOM_INT(_jump_67394)){
        _jump_67394 = (long)DBL_PTR(_jump_67394)->dbl;
    }

    /**     il( sprintf("%s: inc %s, lim %s, initial %s, lv %s, jmp %04d",*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34011 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_34011)){
        _34012 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_34011)->dbl));
    }
    else{
        _34012 = (int)*(((s1_ptr)_2)->base + _34011);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_34012);
    *((int *)(_2+4)) = _34012;
    _34013 = MAKE_SEQ(_1);
    _34012 = NOVALUE;
    _34014 = _70pc_65963 + 1;
    if (_34014 > MAXINT){
        _34014 = NewDouble((double)_34014);
    }
    _34015 = _70pc_65963 + 3;
    rhs_slice_target = (object_ptr)&_34016;
    RHS_Slice(_25Code_12355, _34014, _34015);
    _34017 = _70pc_65963 + 5;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34018 = (int)*(((s1_ptr)_2)->base + _34017);
    if (IS_SEQUENCE(_34016) && IS_ATOM(_34018)) {
        Ref(_34018);
        Append(&_34019, _34016, _34018);
    }
    else if (IS_ATOM(_34016) && IS_SEQUENCE(_34018)) {
    }
    else {
        Concat((object_ptr)&_34019, _34016, _34018);
        DeRefDS(_34016);
        _34016 = NOVALUE;
    }
    DeRef(_34016);
    _34016 = NOVALUE;
    _34018 = NOVALUE;
    _34020 = _70names(_34019);
    _34019 = NOVALUE;
    _34021 = _70pc_65963 + 6;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34022 = (int)*(((s1_ptr)_2)->base + _34021);
    {
        int concat_list[3];

        concat_list[0] = _34022;
        concat_list[1] = _34020;
        concat_list[2] = _34013;
        Concat_N((object_ptr)&_34023, concat_list, 3);
    }
    _34022 = NOVALUE;
    DeRef(_34020);
    _34020 = NOVALUE;
    DeRefDS(_34013);
    _34013 = NOVALUE;
    _34024 = EPrintf(-9999999, _34010, _34023);
    DeRefDS(_34023);
    _34023 = NOVALUE;
    _70il(_34024, 6);
    _34024 = NOVALUE;

    /**     pc += 7*/
    _70pc_65963 = _70pc_65963 + 7;

    /** end procedure*/
    _34000 = NOVALUE;
    _34002 = NOVALUE;
    _34004 = NOVALUE;
    _34006 = NOVALUE;
    _34008 = NOVALUE;
    _34011 = NOVALUE;
    DeRef(_34014);
    _34014 = NOVALUE;
    _34015 = NOVALUE;
    _34021 = NOVALUE;
    _34017 = NOVALUE;
    return;
    ;
}


void _70opENDFOR_GENERAL()
{
    int _34037 = NOVALUE;
    int _34036 = NOVALUE;
    int _34035 = NOVALUE;
    int _34034 = NOVALUE;
    int _34033 = NOVALUE;
    int _34032 = NOVALUE;
    int _34031 = NOVALUE;
    int _34030 = NOVALUE;
    int _34029 = NOVALUE;
    int _34028 = NOVALUE;
    int _34027 = NOVALUE;
    int _0, _1, _2;
    

    /**     il( sprintf("%s: top %04d lim %s, inc %s, lv %s",*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34027 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_34027)){
        _34028 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_34027)->dbl));
    }
    else{
        _34028 = (int)*(((s1_ptr)_2)->base + _34027);
    }
    _34029 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34030 = (int)*(((s1_ptr)_2)->base + _34029);
    Ref(_34030);
    RefDS(_34028);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _34028;
    ((int *)_2)[2] = _34030;
    _34031 = MAKE_SEQ(_1);
    _34030 = NOVALUE;
    _34028 = NOVALUE;
    _34032 = _70pc_65963 + 2;
    if ((long)((unsigned long)_34032 + (unsigned long)HIGH_BITS) >= 0) 
    _34032 = NewDouble((double)_34032);
    _34033 = _70pc_65963 + 4;
    rhs_slice_target = (object_ptr)&_34034;
    RHS_Slice(_25Code_12355, _34032, _34033);
    _34035 = _70names(_34034);
    _34034 = NOVALUE;
    if (IS_SEQUENCE(_34031) && IS_ATOM(_34035)) {
        Ref(_34035);
        Append(&_34036, _34031, _34035);
    }
    else if (IS_ATOM(_34031) && IS_SEQUENCE(_34035)) {
    }
    else {
        Concat((object_ptr)&_34036, _34031, _34035);
        DeRefDS(_34031);
        _34031 = NOVALUE;
    }
    DeRef(_34031);
    _34031 = NOVALUE;
    DeRef(_34035);
    _34035 = NOVALUE;
    _34037 = EPrintf(-9999999, _34026, _34036);
    DeRefDS(_34036);
    _34036 = NOVALUE;
    _70il(_34037, 4);
    _34037 = NOVALUE;

    /**     pc += 5*/
    _70pc_65963 = _70pc_65963 + 5;

    /** end procedure*/
    _34027 = NOVALUE;
    _34029 = NOVALUE;
    DeRef(_34032);
    _34032 = NOVALUE;
    _34033 = NOVALUE;
    return;
    ;
}


void _70opENDFOR_INT_UP1()
{
    int _34047 = NOVALUE;
    int _34046 = NOVALUE;
    int _34045 = NOVALUE;
    int _34044 = NOVALUE;
    int _34043 = NOVALUE;
    int _34042 = NOVALUE;
    int _34041 = NOVALUE;
    int _34040 = NOVALUE;
    int _0, _1, _2;
    

    /**     il( sprintf("ENDFOR_INT_UP1: top %04d, lim: %s, lv %s, inc %s",*/
    _34040 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34041 = (int)*(((s1_ptr)_2)->base + _34040);
    _34042 = _70pc_65963 + 2;
    if ((long)((unsigned long)_34042 + (unsigned long)HIGH_BITS) >= 0) 
    _34042 = NewDouble((double)_34042);
    _34043 = _70pc_65963 + 4;
    rhs_slice_target = (object_ptr)&_34044;
    RHS_Slice(_25Code_12355, _34042, _34043);
    _34045 = _70names(_34044);
    _34044 = NOVALUE;
    if (IS_SEQUENCE(_34041) && IS_ATOM(_34045)) {
        Ref(_34045);
        Append(&_34046, _34041, _34045);
    }
    else if (IS_ATOM(_34041) && IS_SEQUENCE(_34045)) {
        Ref(_34041);
        Prepend(&_34046, _34045, _34041);
    }
    else {
        Concat((object_ptr)&_34046, _34041, _34045);
        _34041 = NOVALUE;
    }
    _34041 = NOVALUE;
    DeRef(_34045);
    _34045 = NOVALUE;
    _34047 = EPrintf(-9999999, _34039, _34046);
    DeRefDS(_34046);
    _34046 = NOVALUE;
    _70il(_34047, 4);
    _34047 = NOVALUE;

    /**     pc += 5*/
    _70pc_65963 = _70pc_65963 + 5;

    /** end procedure*/
    _34040 = NOVALUE;
    DeRef(_34042);
    _34042 = NOVALUE;
    _34043 = NOVALUE;
    return;
    ;
}


void _70opCALL_PROC()
{
    int _proc_67466 = NOVALUE;
    int _34078 = NOVALUE;
    int _34077 = NOVALUE;
    int _34076 = NOVALUE;
    int _34075 = NOVALUE;
    int _34074 = NOVALUE;
    int _34073 = NOVALUE;
    int _34072 = NOVALUE;
    int _34071 = NOVALUE;
    int _34070 = NOVALUE;
    int _34069 = NOVALUE;
    int _34068 = NOVALUE;
    int _34067 = NOVALUE;
    int _34066 = NOVALUE;
    int _34065 = NOVALUE;
    int _34064 = NOVALUE;
    int _34063 = NOVALUE;
    int _34062 = NOVALUE;
    int _34061 = NOVALUE;
    int _34060 = NOVALUE;
    int _34058 = NOVALUE;
    int _34057 = NOVALUE;
    int _34056 = NOVALUE;
    int _34055 = NOVALUE;
    int _34054 = NOVALUE;
    int _34053 = NOVALUE;
    int _34051 = NOVALUE;
    int _34049 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+1]  -- routine id*/
    _34049 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _34049);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     b = Code[pc+2]  -- argument list*/
    _34051 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70b_65965 = (int)*(((s1_ptr)_2)->base + _34051);
    if (!IS_ATOM_INT(_70b_65965)){
        _70b_65965 = (long)DBL_PTR(_70b_65965)->dbl;
    }

    /**     if Code[pc] = CALL_FUNC and pc + 3 <= length(Code) then*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34053 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    if (IS_ATOM_INT(_34053)) {
        _34054 = (_34053 == 137);
    }
    else {
        _34054 = binary_op(EQUALS, _34053, 137);
    }
    _34053 = NOVALUE;
    if (IS_ATOM_INT(_34054)) {
        if (_34054 == 0) {
            goto L1; // [49] 133
        }
    }
    else {
        if (DBL_PTR(_34054)->dbl == 0.0) {
            goto L1; // [49] 133
        }
    }
    _34056 = _70pc_65963 + 3;
    if ((long)((unsigned long)_34056 + (unsigned long)HIGH_BITS) >= 0) 
    _34056 = NewDouble((double)_34056);
    if (IS_SEQUENCE(_25Code_12355)){
            _34057 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _34057 = 1;
    }
    if (IS_ATOM_INT(_34056)) {
        _34058 = (_34056 <= _34057);
    }
    else {
        _34058 = (DBL_PTR(_34056)->dbl <= (double)_34057);
    }
    DeRef(_34056);
    _34056 = NOVALUE;
    _34057 = NOVALUE;
    if (_34058 == 0)
    {
        DeRef(_34058);
        _34058 = NOVALUE;
        goto L1; // [69] 133
    }
    else{
        DeRef(_34058);
        _34058 = NOVALUE;
    }

    /**     	il( sprintf("%s: %s %s => %s", {opnames[Code[pc]]} & names( a&b & Code[pc+3]) ), 3 )*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34060 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_34060)){
        _34061 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_34060)->dbl));
    }
    else{
        _34061 = (int)*(((s1_ptr)_2)->base + _34060);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_34061);
    *((int *)(_2+4)) = _34061;
    _34062 = MAKE_SEQ(_1);
    _34061 = NOVALUE;
    _34063 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34064 = (int)*(((s1_ptr)_2)->base + _34063);
    {
        int concat_list[3];

        concat_list[0] = _34064;
        concat_list[1] = _70b_65965;
        concat_list[2] = _70a_65964;
        Concat_N((object_ptr)&_34065, concat_list, 3);
    }
    _34064 = NOVALUE;
    _34066 = _70names(_34065);
    _34065 = NOVALUE;
    if (IS_SEQUENCE(_34062) && IS_ATOM(_34066)) {
        Ref(_34066);
        Append(&_34067, _34062, _34066);
    }
    else if (IS_ATOM(_34062) && IS_SEQUENCE(_34066)) {
    }
    else {
        Concat((object_ptr)&_34067, _34062, _34066);
        DeRefDS(_34062);
        _34062 = NOVALUE;
    }
    DeRef(_34062);
    _34062 = NOVALUE;
    DeRef(_34066);
    _34066 = NOVALUE;
    _34068 = EPrintf(-9999999, _34059, _34067);
    DeRefDS(_34067);
    _34067 = NOVALUE;
    _70il(_34068, 3);
    _34068 = NOVALUE;
    goto L2; // [130] 178
L1: 

    /**     	il( sprintf("%s: %s %s", {opnames[Code[pc]]} & names( a&b) ), 2 )*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34069 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_34069)){
        _34070 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_34069)->dbl));
    }
    else{
        _34070 = (int)*(((s1_ptr)_2)->base + _34069);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_34070);
    *((int *)(_2+4)) = _34070;
    _34071 = MAKE_SEQ(_1);
    _34070 = NOVALUE;
    Concat((object_ptr)&_34072, _70a_65964, _70b_65965);
    _34073 = _70names(_34072);
    _34072 = NOVALUE;
    if (IS_SEQUENCE(_34071) && IS_ATOM(_34073)) {
        Ref(_34073);
        Append(&_34074, _34071, _34073);
    }
    else if (IS_ATOM(_34071) && IS_SEQUENCE(_34073)) {
    }
    else {
        Concat((object_ptr)&_34074, _34071, _34073);
        DeRefDS(_34071);
        _34071 = NOVALUE;
    }
    DeRef(_34071);
    _34071 = NOVALUE;
    DeRef(_34073);
    _34073 = NOVALUE;
    _34075 = EPrintf(-9999999, _33867, _34074);
    DeRefDS(_34074);
    _34074 = NOVALUE;
    _70il(_34075, 2);
    _34075 = NOVALUE;
L2: 

    /**     pc += 3 + (Code[pc] = CALL_FUNC)*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34076 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    if (IS_ATOM_INT(_34076)) {
        _34077 = (_34076 == 137);
    }
    else {
        _34077 = binary_op(EQUALS, _34076, 137);
    }
    _34076 = NOVALUE;
    if (IS_ATOM_INT(_34077)) {
        _34078 = 3 + _34077;
        if ((long)((unsigned long)_34078 + (unsigned long)HIGH_BITS) >= 0) 
        _34078 = NewDouble((double)_34078);
    }
    else {
        _34078 = binary_op(PLUS, 3, _34077);
    }
    DeRef(_34077);
    _34077 = NOVALUE;
    if (IS_ATOM_INT(_34078)) {
        _70pc_65963 = _70pc_65963 + _34078;
    }
    else {
        _70pc_65963 = binary_op(PLUS, _70pc_65963, _34078);
    }
    DeRef(_34078);
    _34078 = NOVALUE;
    if (!IS_ATOM_INT(_70pc_65963)) {
        _1 = (long)(DBL_PTR(_70pc_65963)->dbl);
        if (UNIQUE(DBL_PTR(_70pc_65963)) && (DBL_PTR(_70pc_65963)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_70pc_65963);
        _70pc_65963 = _1;
    }

    /** end procedure*/
    DeRef(_34049);
    _34049 = NOVALUE;
    DeRef(_34051);
    _34051 = NOVALUE;
    _34060 = NOVALUE;
    DeRef(_34054);
    _34054 = NOVALUE;
    _34069 = NOVALUE;
    DeRef(_34063);
    _34063 = NOVALUE;
    return;
    ;
}


void _70opROUTINE_ID()
{
    int _34099 = NOVALUE;
    int _34098 = NOVALUE;
    int _34097 = NOVALUE;
    int _34096 = NOVALUE;
    int _34095 = NOVALUE;
    int _34092 = NOVALUE;
    int _34090 = NOVALUE;
    int _34089 = NOVALUE;
    int _34088 = NOVALUE;
    int _34087 = NOVALUE;
    int _34086 = NOVALUE;
    int _34085 = NOVALUE;
    int _34084 = NOVALUE;
    int _34083 = NOVALUE;
    int _34080 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L1; // [5] 83
    }
    else{
    }

    /** 		target = Code[pc+4]*/
    _34080 = _70pc_65963 + 4;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70target_65968 = (int)*(((s1_ptr)_2)->base + _34080);
    if (!IS_ATOM_INT(_70target_65968)){
        _70target_65968 = (long)DBL_PTR(_70target_65968)->dbl;
    }

    /** 		il( sprintf("ROUTINE_ID: (max=%d) %s => %s", Code[pc+1] & names( Code[pc+2] & target )), 4 )*/
    _34083 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34084 = (int)*(((s1_ptr)_2)->base + _34083);
    _34085 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34086 = (int)*(((s1_ptr)_2)->base + _34085);
    if (IS_SEQUENCE(_34086) && IS_ATOM(_70target_65968)) {
        Append(&_34087, _34086, _70target_65968);
    }
    else if (IS_ATOM(_34086) && IS_SEQUENCE(_70target_65968)) {
    }
    else {
        Concat((object_ptr)&_34087, _34086, _70target_65968);
        _34086 = NOVALUE;
    }
    _34086 = NOVALUE;
    _34088 = _70names(_34087);
    _34087 = NOVALUE;
    if (IS_SEQUENCE(_34084) && IS_ATOM(_34088)) {
        Ref(_34088);
        Append(&_34089, _34084, _34088);
    }
    else if (IS_ATOM(_34084) && IS_SEQUENCE(_34088)) {
        Ref(_34084);
        Prepend(&_34089, _34088, _34084);
    }
    else {
        Concat((object_ptr)&_34089, _34084, _34088);
        _34084 = NOVALUE;
    }
    _34084 = NOVALUE;
    DeRef(_34088);
    _34088 = NOVALUE;
    _34090 = EPrintf(-9999999, _34082, _34089);
    DeRefDS(_34089);
    _34089 = NOVALUE;
    _70il(_34090, 4);
    _34090 = NOVALUE;

    /**     	pc += 5*/
    _70pc_65963 = _70pc_65963 + 5;
    goto L2; // [80] 140
L1: 

    /** 		target = Code[pc+5]*/
    _34092 = _70pc_65963 + 5;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70target_65968 = (int)*(((s1_ptr)_2)->base + _34092);
    if (!IS_ATOM_INT(_70target_65968)){
        _70target_65968 = (long)DBL_PTR(_70target_65968)->dbl;
    }

    /** 		il( sprintf("ROUTINE_ID: %s => %s", names( Code[pc+3] & target )), 5 )*/
    _34095 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34096 = (int)*(((s1_ptr)_2)->base + _34095);
    if (IS_SEQUENCE(_34096) && IS_ATOM(_70target_65968)) {
        Append(&_34097, _34096, _70target_65968);
    }
    else if (IS_ATOM(_34096) && IS_SEQUENCE(_70target_65968)) {
    }
    else {
        Concat((object_ptr)&_34097, _34096, _70target_65968);
        _34096 = NOVALUE;
    }
    _34096 = NOVALUE;
    _34098 = _70names(_34097);
    _34097 = NOVALUE;
    _34099 = EPrintf(-9999999, _34094, _34098);
    DeRef(_34098);
    _34098 = NOVALUE;
    _70il(_34099, 5);
    _34099 = NOVALUE;

    /** 		pc += 6*/
    _70pc_65963 = _70pc_65963 + 6;
L2: 

    /** end procedure*/
    DeRef(_34080);
    _34080 = NOVALUE;
    DeRef(_34083);
    _34083 = NOVALUE;
    DeRef(_34092);
    _34092 = NOVALUE;
    DeRef(_34085);
    _34085 = NOVALUE;
    DeRef(_34095);
    _34095 = NOVALUE;
    return;
    ;
}


void _70opAPPEND()
{
    int _0, _1, _2;
    

    /** 	binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opPREPEND()
{
    int _0, _1, _2;
    

    /** 	binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opCONCAT()
{
    int _0, _1, _2;
    

    /** 	binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opCONCAT_N()
{
    int _n_67551 = NOVALUE;
    int _x_67552 = NOVALUE;
    int _34122 = NOVALUE;
    int _34121 = NOVALUE;
    int _34120 = NOVALUE;
    int _34119 = NOVALUE;
    int _34118 = NOVALUE;
    int _34116 = NOVALUE;
    int _34115 = NOVALUE;
    int _34113 = NOVALUE;
    int _34112 = NOVALUE;
    int _34111 = NOVALUE;
    int _34110 = NOVALUE;
    int _34109 = NOVALUE;
    int _34108 = NOVALUE;
    int _34107 = NOVALUE;
    int _34105 = NOVALUE;
    int _34104 = NOVALUE;
    int _34101 = NOVALUE;
    int _0, _1, _2;
    

    /**     n = Code[pc+1] -- number of items*/
    _34101 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _n_67551 = (int)*(((s1_ptr)_2)->base + _34101);
    if (!IS_ATOM_INT(_n_67551)){
        _n_67551 = (long)DBL_PTR(_n_67551)->dbl;
    }

    /**     x = sprintf("CONCAT_N: %d", Code[pc+1] )*/
    _34104 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34105 = (int)*(((s1_ptr)_2)->base + _34104);
    DeRefi(_x_67552);
    _x_67552 = EPrintf(-9999999, _34103, _34105);
    _34105 = NOVALUE;

    /**     for i = pc+2 to pc+n+1 do*/
    _34107 = _70pc_65963 + 2;
    if ((long)((unsigned long)_34107 + (unsigned long)HIGH_BITS) >= 0) 
    _34107 = NewDouble((double)_34107);
    _34108 = _70pc_65963 + _n_67551;
    if ((long)((unsigned long)_34108 + (unsigned long)HIGH_BITS) >= 0) 
    _34108 = NewDouble((double)_34108);
    if (IS_ATOM_INT(_34108)) {
        _34109 = _34108 + 1;
        if (_34109 > MAXINT){
            _34109 = NewDouble((double)_34109);
        }
    }
    else
    _34109 = binary_op(PLUS, 1, _34108);
    DeRef(_34108);
    _34108 = NOVALUE;
    {
        int _i_67562;
        Ref(_34107);
        _i_67562 = _34107;
L1: 
        if (binary_op_a(GREATER, _i_67562, _34109)){
            goto L2; // [53] 91
        }

        /** 		x &= sprintf(", %s", {name_or_literal(Code[i])})*/
        _2 = (int)SEQ_PTR(_25Code_12355);
        if (!IS_ATOM_INT(_i_67562)){
            _34110 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_67562)->dbl));
        }
        else{
            _34110 = (int)*(((s1_ptr)_2)->base + _i_67562);
        }
        Ref(_34110);
        _34111 = _70name_or_literal(_34110);
        _34110 = NOVALUE;
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _34111;
        _34112 = MAKE_SEQ(_1);
        _34111 = NOVALUE;
        _34113 = EPrintf(-9999999, _33616, _34112);
        DeRefDS(_34112);
        _34112 = NOVALUE;
        if (IS_SEQUENCE(_x_67552) && IS_ATOM(_34113)) {
        }
        else if (IS_ATOM(_x_67552) && IS_SEQUENCE(_34113)) {
            Ref(_x_67552);
            Prepend(&_x_67552, _34113, _x_67552);
        }
        else {
            Concat((object_ptr)&_x_67552, _x_67552, _34113);
        }
        DeRefDS(_34113);
        _34113 = NOVALUE;

        /**     end for*/
        _0 = _i_67562;
        if (IS_ATOM_INT(_i_67562)) {
            _i_67562 = _i_67562 + 1;
            if ((long)((unsigned long)_i_67562 +(unsigned long) HIGH_BITS) >= 0){
                _i_67562 = NewDouble((double)_i_67562);
            }
        }
        else {
            _i_67562 = binary_op_a(PLUS, _i_67562, 1);
        }
        DeRef(_0);
        goto L1; // [86] 60
L2: 
        ;
        DeRef(_i_67562);
    }

    /**     target = Code[pc+n+2]*/
    _34115 = _70pc_65963 + _n_67551;
    if ((long)((unsigned long)_34115 + (unsigned long)HIGH_BITS) >= 0) 
    _34115 = NewDouble((double)_34115);
    if (IS_ATOM_INT(_34115)) {
        _34116 = _34115 + 2;
    }
    else {
        _34116 = NewDouble(DBL_PTR(_34115)->dbl + (double)2);
    }
    DeRef(_34115);
    _34115 = NOVALUE;
    _2 = (int)SEQ_PTR(_25Code_12355);
    if (!IS_ATOM_INT(_34116)){
        _70target_65968 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_34116)->dbl));
    }
    else{
        _70target_65968 = (int)*(((s1_ptr)_2)->base + _34116);
    }
    if (!IS_ATOM_INT(_70target_65968)){
        _70target_65968 = (long)DBL_PTR(_70target_65968)->dbl;
    }

    /**     il( sprintf( "%s => %s", {x, name_or_literal( target )}), n + 2 )*/
    _34118 = _70name_or_literal(_70target_65968);
    Ref(_x_67552);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _x_67552;
    ((int *)_2)[2] = _34118;
    _34119 = MAKE_SEQ(_1);
    _34118 = NOVALUE;
    _34120 = EPrintf(-9999999, _33596, _34119);
    DeRefDS(_34119);
    _34119 = NOVALUE;
    _34121 = _n_67551 + 2;
    if ((long)((unsigned long)_34121 + (unsigned long)HIGH_BITS) >= 0) 
    _34121 = NewDouble((double)_34121);
    _70il(_34120, _34121);
    _34120 = NOVALUE;
    _34121 = NOVALUE;

    /**     pc += n+3*/
    _34122 = _n_67551 + 3;
    if ((long)((unsigned long)_34122 + (unsigned long)HIGH_BITS) >= 0) 
    _34122 = NewDouble((double)_34122);
    if (IS_ATOM_INT(_34122)) {
        _70pc_65963 = _70pc_65963 + _34122;
    }
    else {
        _70pc_65963 = NewDouble((double)_70pc_65963 + DBL_PTR(_34122)->dbl);
    }
    DeRef(_34122);
    _34122 = NOVALUE;
    if (!IS_ATOM_INT(_70pc_65963)) {
        _1 = (long)(DBL_PTR(_70pc_65963)->dbl);
        if (UNIQUE(DBL_PTR(_70pc_65963)) && (DBL_PTR(_70pc_65963)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_70pc_65963);
        _70pc_65963 = _1;
    }

    /** end procedure*/
    DeRefi(_x_67552);
    DeRef(_34101);
    _34101 = NOVALUE;
    DeRef(_34104);
    _34104 = NOVALUE;
    DeRef(_34107);
    _34107 = NOVALUE;
    DeRef(_34109);
    _34109 = NOVALUE;
    DeRef(_34116);
    _34116 = NOVALUE;
    return;
    ;
}


void _70opREPEAT()
{
    int _0, _1, _2;
    

    /** 	binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opDATE()
{
    int _0, _1, _2;
    

    /** 	nonary()*/
    _70nonary();

    /** end procedure*/
    return;
    ;
}


void _70opTIME()
{
    int _0, _1, _2;
    

    /** 	nonary()*/
    _70nonary();

    /** end procedure*/
    return;
    ;
}


void _70opSPACE_USED()
{
    int _0, _1, _2;
    

    /** 	nonary()*/
    _70nonary();

    /** end procedure*/
    return;
    ;
}


void _70opNOP1()
{
    int _0, _1, _2;
    

    /** 	pnonary()*/
    _70pnonary();

    /** end procedure*/
    return;
    ;
}


void _70opNOP2()
{
    int _34130 = NOVALUE;
    int _34129 = NOVALUE;
    int _34128 = NOVALUE;
    int _34127 = NOVALUE;
    int _34126 = NOVALUE;
    int _34125 = NOVALUE;
    int _0, _1, _2;
    

    /** 	il( sprintf("%s %d", {opnames[Code[pc]], Code[pc+1]}), 1 )*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34125 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_34125)){
        _34126 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_34125)->dbl));
    }
    else{
        _34126 = (int)*(((s1_ptr)_2)->base + _34125);
    }
    _34127 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34128 = (int)*(((s1_ptr)_2)->base + _34127);
    Ref(_34128);
    RefDS(_34126);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _34126;
    ((int *)_2)[2] = _34128;
    _34129 = MAKE_SEQ(_1);
    _34128 = NOVALUE;
    _34126 = NOVALUE;
    _34130 = EPrintf(-9999999, _34124, _34129);
    DeRefDS(_34129);
    _34129 = NOVALUE;
    _70il(_34130, 1);
    _34130 = NOVALUE;

    /** 	pc += 2*/
    _70pc_65963 = _70pc_65963 + 2;

    /** end procedure*/
    _34125 = NOVALUE;
    _34127 = NOVALUE;
    return;
    ;
}


void _70opPOSITION()
{
    int _0, _1, _2;
    

    /** 	pbinary()*/
    _70pbinary();

    /** end procedure*/
    return;
    ;
}


void _70opEQUAL()
{
    int _0, _1, _2;
    

    /** 	binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opHASH()
{
    int _0, _1, _2;
    

    /** 	binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opCOMPARE()
{
    int _0, _1, _2;
    

    /** 	binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opFIND()
{
    int _0, _1, _2;
    

    /** 	binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opFIND_FROM()
{
    int _0, _1, _2;
    

    /** 	trinary()*/
    _70trinary();

    /** end procedure*/
    return;
    ;
}


void _70opMATCH_FROM()
{
    int _0, _1, _2;
    

    /** 	trinary()*/
    _70trinary();

    /** end procedure*/
    return;
    ;
}


void _70opMATCH()
{
    int _0, _1, _2;
    

    /** 	binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opPEEK()
{
    int _0, _1, _2;
    

    /** 	unary()*/
    _70unary();

    /** end procedure*/
    return;
    ;
}


void _70opPOKE()
{
    int _0, _1, _2;
    

    /** 	pbinary()*/
    _70pbinary();

    /** end procedure*/
    return;
    ;
}


void _70opMEM_COPY()
{
    int _0, _1, _2;
    

    /** 	ptrinary()*/
    _70ptrinary();

    /** end procedure*/
    return;
    ;
}


void _70opMEM_SET()
{
    int _0, _1, _2;
    

    /** 	ptrinary()*/
    _70ptrinary();

    /** end procedure*/
    return;
    ;
}


void _70opCALL()
{
    int _0, _1, _2;
    

    /** 	punary()*/
    _70punary();

    /** end procedure*/
    return;
    ;
}


void _70opSYSTEM()
{
    int _0, _1, _2;
    

    /** 	pbinary()*/
    _70pbinary();

    /** end procedure*/
    return;
    ;
}


void _70opSYSTEM_EXEC()
{
    int _0, _1, _2;
    

    /** 	binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opOPEN()
{
    int _0, _1, _2;
    

    /** 	trinary()*/
    _70trinary();

    /** end procedure*/
    return;
    ;
}


void _70opCLOSE()
{
    int _0, _1, _2;
    

    /** 	punary()*/
    _70punary();

    /** end procedure*/
    return;
    ;
}


void _70opABORT()
{
    int _0, _1, _2;
    

    /** 	punary()*/
    _70punary();

    /** end procedure*/
    return;
    ;
}


void _70opGETC()
{
    int _0, _1, _2;
    

    /** 	unary()*/
    _70unary();

    /** end procedure*/
    return;
    ;
}


void _70opGETS()
{
    int _0, _1, _2;
    

    /** 	unary()*/
    _70unary();

    /** end procedure*/
    return;
    ;
}


void _70opGET_KEY()
{
    int _0, _1, _2;
    

    /** 	nonary()*/
    _70nonary();

    /** end procedure*/
    return;
    ;
}


void _70opCLEAR_SCREEN()
{
    int _0, _1, _2;
    

    /** 	pnonary()*/
    _70pnonary();

    /** end procedure*/
    return;
    ;
}


void _70opPUTS()
{
    int _0, _1, _2;
    

    /** 	pbinary()*/
    _70pbinary();

    /** end procedure*/
    return;
    ;
}


void _70opQPRINT()
{
    int _34137 = NOVALUE;
    int _34136 = NOVALUE;
    int _34135 = NOVALUE;
    int _34132 = NOVALUE;
    int _0, _1, _2;
    

    /**     a = Code[pc+2]*/
    _34132 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _70a_65964 = (int)*(((s1_ptr)_2)->base + _34132);
    if (!IS_ATOM_INT(_70a_65964)){
        _70a_65964 = (long)DBL_PTR(_70a_65964)->dbl;
    }

    /**     il( sprintf( "QPRINT: %s",{name_or_literal( a )} ), 2 )*/
    _34135 = _70name_or_literal(_70a_65964);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _34135;
    _34136 = MAKE_SEQ(_1);
    _34135 = NOVALUE;
    _34137 = EPrintf(-9999999, _34134, _34136);
    DeRefDS(_34136);
    _34136 = NOVALUE;
    _70il(_34137, 2);
    _34137 = NOVALUE;

    /**     pc += 3*/
    _70pc_65963 = _70pc_65963 + 3;

    /** end procedure*/
    _34132 = NOVALUE;
    return;
    ;
}


void _70opPRINT()
{
    int _0, _1, _2;
    

    /** 	pbinary()*/
    _70pbinary();

    /** end procedure*/
    return;
    ;
}


void _70opPRINTF()
{
    int _0, _1, _2;
    

    /**     ptrinary()*/
    _70ptrinary();

    /** end procedure*/
    return;
    ;
}


void _70opSPRINTF()
{
    int _0, _1, _2;
    

    /** 	binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opCOMMAND_LINE()
{
    int _cmd_67669 = NOVALUE;
    int _0, _1, _2;
    

    /**     nonary()*/
    _70nonary();

    /** end procedure*/
    return;
    ;
}


void _70opGETENV()
{
    int _0, _1, _2;
    

    /** 	unary()*/
    _70unary();

    /** end procedure*/
    return;
    ;
}


void _70opC_PROC()
{
    int _34147 = NOVALUE;
    int _34146 = NOVALUE;
    int _34145 = NOVALUE;
    int _34144 = NOVALUE;
    int _34143 = NOVALUE;
    int _34142 = NOVALUE;
    int _34141 = NOVALUE;
    int _34140 = NOVALUE;
    int _34139 = NOVALUE;
    int _0, _1, _2;
    

    /** 	il( sprintf( "%s: %s, %s", {opnames[Code[pc]]} & names(Code[pc+1..pc+2])), 3)*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34139 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_34139)){
        _34140 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_34139)->dbl));
    }
    else{
        _34140 = (int)*(((s1_ptr)_2)->base + _34139);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_34140);
    *((int *)(_2+4)) = _34140;
    _34141 = MAKE_SEQ(_1);
    _34140 = NOVALUE;
    _34142 = _70pc_65963 + 1;
    if (_34142 > MAXINT){
        _34142 = NewDouble((double)_34142);
    }
    _34143 = _70pc_65963 + 2;
    rhs_slice_target = (object_ptr)&_34144;
    RHS_Slice(_25Code_12355, _34142, _34143);
    _34145 = _70names(_34144);
    _34144 = NOVALUE;
    if (IS_SEQUENCE(_34141) && IS_ATOM(_34145)) {
        Ref(_34145);
        Append(&_34146, _34141, _34145);
    }
    else if (IS_ATOM(_34141) && IS_SEQUENCE(_34145)) {
    }
    else {
        Concat((object_ptr)&_34146, _34141, _34145);
        DeRefDS(_34141);
        _34141 = NOVALUE;
    }
    DeRef(_34141);
    _34141 = NOVALUE;
    DeRef(_34145);
    _34145 = NOVALUE;
    _34147 = EPrintf(-9999999, _33335, _34146);
    DeRefDS(_34146);
    _34146 = NOVALUE;
    _70il(_34147, 3);
    _34147 = NOVALUE;

    /** 	pc += 4*/
    _70pc_65963 = _70pc_65963 + 4;

    /** end procedure*/
    _34139 = NOVALUE;
    DeRef(_34142);
    _34142 = NOVALUE;
    _34143 = NOVALUE;
    return;
    ;
}


void _70opC_FUNC()
{
    int _34158 = NOVALUE;
    int _34157 = NOVALUE;
    int _34156 = NOVALUE;
    int _34155 = NOVALUE;
    int _34154 = NOVALUE;
    int _34153 = NOVALUE;
    int _34152 = NOVALUE;
    int _34151 = NOVALUE;
    int _34150 = NOVALUE;
    int _0, _1, _2;
    

    /** 	il( sprintf( "%s: %s, %s (sub %s) => %s", {opnames[Code[pc]]} & names(Code[pc+1..pc+4])), 4)*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34150 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_34150)){
        _34151 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_34150)->dbl));
    }
    else{
        _34151 = (int)*(((s1_ptr)_2)->base + _34150);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_34151);
    *((int *)(_2+4)) = _34151;
    _34152 = MAKE_SEQ(_1);
    _34151 = NOVALUE;
    _34153 = _70pc_65963 + 1;
    if (_34153 > MAXINT){
        _34153 = NewDouble((double)_34153);
    }
    _34154 = _70pc_65963 + 4;
    rhs_slice_target = (object_ptr)&_34155;
    RHS_Slice(_25Code_12355, _34153, _34154);
    _34156 = _70names(_34155);
    _34155 = NOVALUE;
    if (IS_SEQUENCE(_34152) && IS_ATOM(_34156)) {
        Ref(_34156);
        Append(&_34157, _34152, _34156);
    }
    else if (IS_ATOM(_34152) && IS_SEQUENCE(_34156)) {
    }
    else {
        Concat((object_ptr)&_34157, _34152, _34156);
        DeRefDS(_34152);
        _34152 = NOVALUE;
    }
    DeRef(_34152);
    _34152 = NOVALUE;
    DeRef(_34156);
    _34156 = NOVALUE;
    _34158 = EPrintf(-9999999, _34149, _34157);
    DeRefDS(_34157);
    _34157 = NOVALUE;
    _70il(_34158, 4);
    _34158 = NOVALUE;

    /** 	pc += 5*/
    _70pc_65963 = _70pc_65963 + 5;

    /** end procedure*/
    _34150 = NOVALUE;
    DeRef(_34153);
    _34153 = NOVALUE;
    _34154 = NOVALUE;
    return;
    ;
}


void _70opTRACE()
{
    int _0, _1, _2;
    

    /** 	punary()*/
    _70punary();

    /** end procedure*/
    return;
    ;
}


void _70opPROFILE()
{
    int _0, _1, _2;
    

    /**     punary()*/
    _70punary();

    /** end procedure*/
    return;
    ;
}


void _70opUPDATE_GLOBALS()
{
    int _0, _1, _2;
    

    /** 	pnonary()*/
    _70pnonary();

    /** end procedure*/
    return;
    ;
}


void _70opMACHINE_FUNC()
{
    int _0, _1, _2;
    

    /** 	binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opMACHINE_PROC()
{
    int _0, _1, _2;
    

    /**     pbinary()*/
    _70pbinary();

    /** end procedure*/
    return;
    ;
}


void _70opSWITCH()
{
    int _34188 = NOVALUE;
    int _34187 = NOVALUE;
    int _34186 = NOVALUE;
    int _34185 = NOVALUE;
    int _34184 = NOVALUE;
    int _34183 = NOVALUE;
    int _34182 = NOVALUE;
    int _34181 = NOVALUE;
    int _34180 = NOVALUE;
    int _34179 = NOVALUE;
    int _34178 = NOVALUE;
    int _34176 = NOVALUE;
    int _34175 = NOVALUE;
    int _34174 = NOVALUE;
    int _34173 = NOVALUE;
    int _34172 = NOVALUE;
    int _34171 = NOVALUE;
    int _34170 = NOVALUE;
    int _34169 = NOVALUE;
    int _34168 = NOVALUE;
    int _34167 = NOVALUE;
    int _34166 = NOVALUE;
    int _34165 = NOVALUE;
    int _34164 = NOVALUE;
    int _34163 = NOVALUE;
    int _34160 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if Code[pc] = SWITCH_SPI then*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34160 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    if (binary_op_a(NOTEQ, _34160, 192)){
        _34160 = NOVALUE;
        goto L1; // [13] 108
    }
    _34160 = NOVALUE;

    /** 		il( sprintf( "%s: value %s case offset %d jump %s else goto %d",*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34163 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_34163)){
        _34164 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_34163)->dbl));
    }
    else{
        _34164 = (int)*(((s1_ptr)_2)->base + _34163);
    }
    _34165 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34166 = (int)*(((s1_ptr)_2)->base + _34165);
    Ref(_34166);
    _34167 = _70name_or_literal(_34166);
    _34166 = NOVALUE;
    _34168 = _70pc_65963 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34169 = (int)*(((s1_ptr)_2)->base + _34168);
    _34170 = _70pc_65963 + 3;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34171 = (int)*(((s1_ptr)_2)->base + _34170);
    Ref(_34171);
    _34172 = _70name_or_literal(_34171);
    _34171 = NOVALUE;
    _34173 = _70pc_65963 + 4;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34174 = (int)*(((s1_ptr)_2)->base + _34173);
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_34164);
    *((int *)(_2+4)) = _34164;
    *((int *)(_2+8)) = _34167;
    Ref(_34169);
    *((int *)(_2+12)) = _34169;
    *((int *)(_2+16)) = _34172;
    Ref(_34174);
    *((int *)(_2+20)) = _34174;
    _34175 = MAKE_SEQ(_1);
    _34174 = NOVALUE;
    _34172 = NOVALUE;
    _34169 = NOVALUE;
    _34167 = NOVALUE;
    _34164 = NOVALUE;
    _34176 = EPrintf(-9999999, _34162, _34175);
    DeRefDS(_34175);
    _34175 = NOVALUE;
    _70il(_34176, 5);
    _34176 = NOVALUE;
    goto L2; // [105] 178
L1: 

    /** 		il( sprintf( "%s: value %s cases %s jump %s else goto %d",*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34178 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_34178)){
        _34179 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_34178)->dbl));
    }
    else{
        _34179 = (int)*(((s1_ptr)_2)->base + _34178);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_34179);
    *((int *)(_2+4)) = _34179;
    _34180 = MAKE_SEQ(_1);
    _34179 = NOVALUE;
    _34181 = _70pc_65963 + 1;
    if (_34181 > MAXINT){
        _34181 = NewDouble((double)_34181);
    }
    _34182 = _70pc_65963 + 3;
    rhs_slice_target = (object_ptr)&_34183;
    RHS_Slice(_25Code_12355, _34181, _34182);
    _34184 = _70names(_34183);
    _34183 = NOVALUE;
    _34185 = _70pc_65963 + 4;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34186 = (int)*(((s1_ptr)_2)->base + _34185);
    {
        int concat_list[3];

        concat_list[0] = _34186;
        concat_list[1] = _34184;
        concat_list[2] = _34180;
        Concat_N((object_ptr)&_34187, concat_list, 3);
    }
    _34186 = NOVALUE;
    DeRef(_34184);
    _34184 = NOVALUE;
    DeRefDS(_34180);
    _34180 = NOVALUE;
    _34188 = EPrintf(-9999999, _34177, _34187);
    DeRefDS(_34187);
    _34187 = NOVALUE;
    _70il(_34188, 5);
    _34188 = NOVALUE;
L2: 

    /** 	pc += 5*/
    _70pc_65963 = _70pc_65963 + 5;

    /** end procedure*/
    _34163 = NOVALUE;
    _34178 = NOVALUE;
    DeRef(_34165);
    _34165 = NOVALUE;
    DeRef(_34168);
    _34168 = NOVALUE;
    DeRef(_34181);
    _34181 = NOVALUE;
    DeRef(_34170);
    _34170 = NOVALUE;
    DeRef(_34173);
    _34173 = NOVALUE;
    DeRef(_34182);
    _34182 = NOVALUE;
    DeRef(_34185);
    _34185 = NOVALUE;
    return;
    ;
}


void _70opCASE()
{
    int _34191 = NOVALUE;
    int _34190 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if Code[pc+1] then*/
    _34190 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34191 = (int)*(((s1_ptr)_2)->base + _34190);
    if (_34191 == 0) {
        _34191 = NOVALUE;
        goto L1; // [15] 25
    }
    else {
        if (!IS_ATOM_INT(_34191) && DBL_PTR(_34191)->dbl == 0.0){
            _34191 = NOVALUE;
            goto L1; // [15] 25
        }
        _34191 = NOVALUE;
    }
    _34191 = NOVALUE;

    /** 		punary()*/
    _70punary();
    goto L2; // [22] 38
L1: 

    /** 		pnonary()*/
    _70pnonary();

    /** 		pc += 1*/
    _70pc_65963 = _70pc_65963 + 1;
L2: 

    /** end procedure*/
    DeRef(_34190);
    _34190 = NOVALUE;
    return;
    ;
}


void _70opENTRY()
{
    int _0, _1, _2;
    

    /** end procedure*/
    return;
    ;
}


void _70opOPTION_SWITCHES()
{
    int _0, _1, _2;
    

    /** 	nonary()*/
    _70nonary();

    /** end procedure*/
    return;
    ;
}


void _70opNOPSWITCH()
{
    int _0, _1, _2;
    

    /** 	pnonary()*/
    _70pnonary();

    /** end procedure*/
    return;
    ;
}


void _70opGLABEL()
{
    int _34199 = NOVALUE;
    int _34198 = NOVALUE;
    int _34197 = NOVALUE;
    int _34196 = NOVALUE;
    int _34195 = NOVALUE;
    int _34194 = NOVALUE;
    int _0, _1, _2;
    

    /** 	il( sprintf("%s: %04d", {opnames[Code[pc]], Code[pc+1]}),1 )*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34194 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    _2 = (int)SEQ_PTR(_59opnames_22728);
    if (!IS_ATOM_INT(_34194)){
        _34195 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_34194)->dbl));
    }
    else{
        _34195 = (int)*(((s1_ptr)_2)->base + _34194);
    }
    _34196 = _70pc_65963 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _34197 = (int)*(((s1_ptr)_2)->base + _34196);
    Ref(_34197);
    RefDS(_34195);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _34195;
    ((int *)_2)[2] = _34197;
    _34198 = MAKE_SEQ(_1);
    _34197 = NOVALUE;
    _34195 = NOVALUE;
    _34199 = EPrintf(-9999999, _34193, _34198);
    DeRefDS(_34198);
    _34198 = NOVALUE;
    _70il(_34199, 1);
    _34199 = NOVALUE;

    /** 	pc += 2*/
    _70pc_65963 = _70pc_65963 + 2;

    /** end procedure*/
    _34194 = NOVALUE;
    _34196 = NOVALUE;
    return;
    ;
}


void _70opSPLICE()
{
    int _0, _1, _2;
    

    /** 	trinary()*/
    _70trinary();

    /** end procedure*/
    return;
    ;
}


void _70opINSERT()
{
    int _0, _1, _2;
    

    /** 	trinary()*/
    _70trinary();

    /** end procedure*/
    return;
    ;
}


void _70opHEAD()
{
    int _0, _1, _2;
    

    /** 	binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opTAIL()
{
    int _0, _1, _2;
    

    /** 	binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opREMOVE()
{
    int _0, _1, _2;
    

    /** 	trinary()*/
    _70trinary();

    /** end procedure*/
    return;
    ;
}


void _70opREPLACE()
{
    int _0, _1, _2;
    

    /** 	quadary()*/
    _70quadary();

    /** end procedure*/
    return;
    ;
}


void _70opDELETE_ROUTINE()
{
    int _0, _1, _2;
    

    /** 	binary()*/
    _70binary();

    /** end procedure*/
    return;
    ;
}


void _70opDELETE_OBJECT()
{
    int _0, _1, _2;
    

    /** 	punary()*/
    _70punary();

    /** end procedure*/
    return;
    ;
}


void _70opEXIT_BLOCK()
{
    int _0, _1, _2;
    

    /** 	punary()*/
    _70punary();

    /** end procedure*/
    return;
    ;
}


int _70strip_path(int _file_67806)
{
    int _34206 = NOVALUE;
    int _34205 = NOVALUE;
    int _34204 = NOVALUE;
    int _34203 = NOVALUE;
    int _34202 = NOVALUE;
    int _34201 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = length( file ) to 1 by -1 do*/
    if (IS_SEQUENCE(_file_67806)){
            _34201 = SEQ_PTR(_file_67806)->length;
    }
    else {
        _34201 = 1;
    }
    {
        int _i_67808;
        _i_67808 = _34201;
L1: 
        if (_i_67808 < 1){
            goto L2; // [8] 55
        }

        /** 		if find( file[i], "/\\" ) then*/
        _2 = (int)SEQ_PTR(_file_67806);
        _34202 = (int)*(((s1_ptr)_2)->base + _i_67808);
        _34203 = find_from(_34202, _24461, 1);
        _34202 = NOVALUE;
        if (_34203 == 0)
        {
            _34203 = NOVALUE;
            goto L3; // [26] 48
        }
        else{
            _34203 = NOVALUE;
        }

        /** 			return file[i+1..$]*/
        _34204 = _i_67808 + 1;
        if (IS_SEQUENCE(_file_67806)){
                _34205 = SEQ_PTR(_file_67806)->length;
        }
        else {
            _34205 = 1;
        }
        rhs_slice_target = (object_ptr)&_34206;
        RHS_Slice(_file_67806, _34204, _34205);
        DeRefDS(_file_67806);
        _34204 = NOVALUE;
        return _34206;
L3: 

        /** 	end for*/
        _i_67808 = _i_67808 + -1;
        goto L1; // [50] 15
L2: 
        ;
    }

    /** 	return file*/
    DeRef(_34204);
    _34204 = NOVALUE;
    DeRef(_34206);
    _34206 = NOVALUE;
    return _file_67806;
    ;
}


void _70make_pngs(int _file_67818)
{
    int _34209 = NOVALUE;
    int _34208 = NOVALUE;
    int _0, _1, _2;
    

    /** 		system( sprintf( "dot -Tpng %s.dot > %s.dot.png", repeat( file, 2 ) ), 2 )*/
    _34208 = Repeat(_file_67818, 2);
    _34209 = EPrintf(-9999999, _34207, _34208);
    DeRefDS(_34208);
    _34208 = NOVALUE;
    system_call(_34209, 2);
    DeRefDS(_34209);
    _34209 = NOVALUE;

    /** end procedure*/
    DeRefDS(_file_67818);
    return;
    ;
}


void _70write_call_info(int _name_67824)
{
    int _files_67825 = NOVALUE;
    int _fn_67828 = NOVALUE;
    int _pp_67832 = NOVALUE;
    int _routines_67848 = NOVALUE;
    int _dn_67856 = NOVALUE;
    int _make_pngs_2__tmp_at184_67869 = NOVALUE;
    int _make_pngs_1__tmp_at184_67868 = NOVALUE;
    int _file_inlined_make_pngs_at_181_67867 = NOVALUE;
    int _make_pngs_2__tmp_at248_67879 = NOVALUE;
    int _make_pngs_1__tmp_at248_67878 = NOVALUE;
    int _file_inlined_make_pngs_at_245_67877 = NOVALUE;
    int _make_pngs_2__tmp_at304_67890 = NOVALUE;
    int _make_pngs_1__tmp_at304_67889 = NOVALUE;
    int _file_inlined_make_pngs_at_301_67888 = NOVALUE;
    int _dn_67895 = NOVALUE;
    int _make_pngs_2__tmp_at399_67911 = NOVALUE;
    int _make_pngs_1__tmp_at399_67910 = NOVALUE;
    int _file_inlined_make_pngs_at_396_67909 = NOVALUE;
    int _34252 = NOVALUE;
    int _34250 = NOVALUE;
    int _34249 = NOVALUE;
    int _34246 = NOVALUE;
    int _34245 = NOVALUE;
    int _34244 = NOVALUE;
    int _34242 = NOVALUE;
    int _34241 = NOVALUE;
    int _34239 = NOVALUE;
    int _34237 = NOVALUE;
    int _34235 = NOVALUE;
    int _34234 = NOVALUE;
    int _34232 = NOVALUE;
    int _34230 = NOVALUE;
    int _34229 = NOVALUE;
    int _34228 = NOVALUE;
    int _34226 = NOVALUE;
    int _34225 = NOVALUE;
    int _34224 = NOVALUE;
    int _34222 = NOVALUE;
    int _34212 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence files = map:keys( called_from )*/
    Ref(_71called_from_65009);
    _0 = _files_67825;
    _files_67825 = _32keys(_71called_from_65009, 0);
    DeRef(_0);

    /** 	integer fn = open( name & "calls", "w" )*/
    Concat((object_ptr)&_34212, _name_67824, _34211);
    _fn_67828 = EOpen(_34212, _22788, 0);
    DeRefDS(_34212);
    _34212 = NOVALUE;

    /** 	sequence pp = PRETTY_DEFAULT*/
    RefDS(_23PRETTY_DEFAULT_8699);
    DeRef(_pp_67832);
    _pp_67832 = _23PRETTY_DEFAULT_8699;

    /** 	pp[DISPLAY_ASCII] = 2*/
    _2 = (int)SEQ_PTR(_pp_67832);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _pp_67832 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);

    /** 	pp[LINE_BREAKS]   = -1*/
    _2 = (int)SEQ_PTR(_pp_67832);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _pp_67832 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 10);
    _1 = *(int *)_2;
    *(int *)_2 = -1;
    DeRef(_1);

    /** 	puts( fn, "\"called_from\"\n" )*/
    EPuts(_fn_67828, _34214); // DJP 

    /** 	pretty_print( fn, called_from, pp )*/
    Ref(_71called_from_65009);
    RefDS(_pp_67832);
    _23pretty_print(_fn_67828, _71called_from_65009, _pp_67832);

    /** 	puts( fn, "\n\n\"called_by\"\n" )*/
    EPuts(_fn_67828, _34215); // DJP 

    /** 	pretty_print( fn, called_by, pp )*/
    Ref(_71called_by_65011);
    RefDS(_pp_67832);
    _23pretty_print(_fn_67828, _71called_by_65011, _pp_67832);

    /** 	puts( fn, "\n\n\"known_files\"\n")*/
    EPuts(_fn_67828, _34216); // DJP 

    /** 	pretty_print( fn, known_files, pp )*/
    RefDS(_26known_files_11139);
    RefDS(_pp_67832);
    _23pretty_print(_fn_67828, _26known_files_11139, _pp_67832);

    /** 	puts( fn, "\n\n\"file_include \"\n" )*/
    EPuts(_fn_67828, _34217); // DJP 

    /** 	pretty_print( fn, file_include, pp )*/
    RefDS(_26file_include_11143);
    RefDS(_pp_67832);
    _23pretty_print(_fn_67828, _26file_include_11143, _pp_67832);

    /** 	puts( fn, "\n" )*/
    EPuts(_fn_67828, _22834); // DJP 

    /** 	close( fn )*/
    EClose(_fn_67828);

    /** 	sequence routines = {"If_statement","main","SetBBType"}*/
    _0 = _routines_67848;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_34218);
    *((int *)(_2+4)) = _34218;
    RefDS(_34219);
    *((int *)(_2+8)) = _34219;
    RefDS(_34220);
    *((int *)(_2+12)) = _34220;
    _routines_67848 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 	for r = 1 to length( routines ) do*/
    _34222 = 3;
    {
        int _r_67854;
        _r_67854 = 1;
L1: 
        if (_r_67854 > 3){
            goto L2; // [128] 213
        }

        /** 		integer dn = open( sprintf( "%s.dot", {routines[r]}), "w" )*/
        _2 = (int)SEQ_PTR(_routines_67848);
        _34224 = (int)*(((s1_ptr)_2)->base + _r_67854);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_34224);
        *((int *)(_2+4)) = _34224;
        _34225 = MAKE_SEQ(_1);
        _34224 = NOVALUE;
        _34226 = EPrintf(-9999999, _34223, _34225);
        DeRefDS(_34225);
        _34225 = NOVALUE;
        _dn_67856 = EOpen(_34226, _22788, 0);
        DeRefDS(_34226);
        _34226 = NOVALUE;

        /** 		puts( dn, diagram_routine( routines[r] ) )*/
        _2 = (int)SEQ_PTR(_routines_67848);
        _34228 = (int)*(((s1_ptr)_2)->base + _r_67854);
        RefDS(_34228);
        _34229 = _71diagram_routine(_34228, 3, 3);
        _34228 = NOVALUE;
        EPuts(_dn_67856, _34229); // DJP 
        DeRef(_34229);
        _34229 = NOVALUE;

        /** 		close( dn )*/
        EClose(_dn_67856);

        /** 		make_pngs( routines[r] )*/
        _2 = (int)SEQ_PTR(_routines_67848);
        _34230 = (int)*(((s1_ptr)_2)->base + _r_67854);
        RefDS(_34230);
        DeRef(_file_inlined_make_pngs_at_181_67867);
        _file_inlined_make_pngs_at_181_67867 = _34230;
        _34230 = NOVALUE;

        /** 		system( sprintf( "dot -Tpng %s.dot > %s.dot.png", repeat( file, 2 ) ), 2 )*/
        DeRef(_make_pngs_1__tmp_at184_67868);
        _make_pngs_1__tmp_at184_67868 = Repeat(_file_inlined_make_pngs_at_181_67867, 2);
        DeRefi(_make_pngs_2__tmp_at184_67869);
        _make_pngs_2__tmp_at184_67869 = EPrintf(-9999999, _34207, _make_pngs_1__tmp_at184_67868);
        system_call(_make_pngs_2__tmp_at184_67869, 2);

        /** end procedure*/
        goto L3; // [199] 202
L3: 
        DeRef(_file_inlined_make_pngs_at_181_67867);
        _file_inlined_make_pngs_at_181_67867 = NOVALUE;
        DeRef(_make_pngs_1__tmp_at184_67868);
        _make_pngs_1__tmp_at184_67868 = NOVALUE;
        DeRefi(_make_pngs_2__tmp_at184_67869);
        _make_pngs_2__tmp_at184_67869 = NOVALUE;

        /** 	end for*/
        _r_67854 = _r_67854 + 1;
        goto L1; // [208] 135
L2: 
        ;
    }

    /** 	fn = open( name & "include.dot", "w" )*/
    Concat((object_ptr)&_34232, _name_67824, _34231);
    _fn_67828 = EOpen(_34232, _22788, 0);
    DeRefDS(_34232);
    _34232 = NOVALUE;

    /** 	puts( fn, diagram_includes() )*/
    _34234 = _71diagram_includes(0, 0);
    EPuts(_fn_67828, _34234); // DJP 
    DeRef(_34234);
    _34234 = NOVALUE;

    /** 	close( fn )*/
    EClose(_fn_67828);

    /** 	make_pngs( name & "include" )*/
    Concat((object_ptr)&_34235, _name_67824, _26599);
    DeRef(_file_inlined_make_pngs_at_245_67877);
    _file_inlined_make_pngs_at_245_67877 = _34235;
    _34235 = NOVALUE;

    /** 		system( sprintf( "dot -Tpng %s.dot > %s.dot.png", repeat( file, 2 ) ), 2 )*/
    DeRef(_make_pngs_1__tmp_at248_67878);
    _make_pngs_1__tmp_at248_67878 = Repeat(_file_inlined_make_pngs_at_245_67877, 2);
    DeRefi(_make_pngs_2__tmp_at248_67879);
    _make_pngs_2__tmp_at248_67879 = EPrintf(-9999999, _34207, _make_pngs_1__tmp_at248_67878);
    system_call(_make_pngs_2__tmp_at248_67879, 2);

    /** end procedure*/
    goto L4; // [264] 267
L4: 
    DeRef(_file_inlined_make_pngs_at_245_67877);
    _file_inlined_make_pngs_at_245_67877 = NOVALUE;
    DeRef(_make_pngs_1__tmp_at248_67878);
    _make_pngs_1__tmp_at248_67878 = NOVALUE;
    DeRefi(_make_pngs_2__tmp_at248_67879);
    _make_pngs_2__tmp_at248_67879 = NOVALUE;

    /** 	fn = open( name & "include_all.dot", "w" )*/
    Concat((object_ptr)&_34237, _name_67824, _34236);
    _fn_67828 = EOpen(_34237, _22788, 0);
    DeRefDS(_34237);
    _34237 = NOVALUE;

    /** 	puts( fn, diagram_includes( 1 ) )*/
    _34239 = _71diagram_includes(1, 0);
    EPuts(_fn_67828, _34239); // DJP 
    DeRef(_34239);
    _34239 = NOVALUE;

    /** 	close( fn )*/
    EClose(_fn_67828);

    /** 	make_pngs( name & "include_all" )*/
    Concat((object_ptr)&_34241, _name_67824, _34240);
    DeRef(_file_inlined_make_pngs_at_301_67888);
    _file_inlined_make_pngs_at_301_67888 = _34241;
    _34241 = NOVALUE;

    /** 		system( sprintf( "dot -Tpng %s.dot > %s.dot.png", repeat( file, 2 ) ), 2 )*/
    DeRef(_make_pngs_1__tmp_at304_67889);
    _make_pngs_1__tmp_at304_67889 = Repeat(_file_inlined_make_pngs_at_301_67888, 2);
    DeRefi(_make_pngs_2__tmp_at304_67890);
    _make_pngs_2__tmp_at304_67890 = EPrintf(-9999999, _34207, _make_pngs_1__tmp_at304_67889);
    system_call(_make_pngs_2__tmp_at304_67890, 2);

    /** end procedure*/
    goto L5; // [320] 323
L5: 
    DeRef(_file_inlined_make_pngs_at_301_67888);
    _file_inlined_make_pngs_at_301_67888 = NOVALUE;
    DeRef(_make_pngs_1__tmp_at304_67889);
    _make_pngs_1__tmp_at304_67889 = NOVALUE;
    DeRefi(_make_pngs_2__tmp_at304_67890);
    _make_pngs_2__tmp_at304_67890 = NOVALUE;

    /** 	files = short_names*/
    RefDS(_71short_names_65016);
    DeRef(_files_67825);
    _files_67825 = _71short_names_65016;

    /** 	for f = 1 to length( files ) do*/
    if (IS_SEQUENCE(_files_67825)){
            _34242 = SEQ_PTR(_files_67825)->length;
    }
    else {
        _34242 = 1;
    }
    {
        int _f_67893;
        _f_67893 = 1;
L6: 
        if (_f_67893 > _34242){
            goto L7; // [339] 429
        }

        /** 		integer dn = open( sprintf("%s.dep.dot", {files[f]}), "w" )*/
        _2 = (int)SEQ_PTR(_files_67825);
        _34244 = (int)*(((s1_ptr)_2)->base + _f_67893);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_34244);
        *((int *)(_2+4)) = _34244;
        _34245 = MAKE_SEQ(_1);
        _34244 = NOVALUE;
        _34246 = EPrintf(-9999999, _34243, _34245);
        DeRefDS(_34245);
        _34245 = NOVALUE;
        _dn_67895 = EOpen(_34246, _22788, 0);
        DeRefDS(_34246);
        _34246 = NOVALUE;

        /** 		if dn != -1 then*/
        if (_dn_67895 == -1)
        goto L8; // [367] 420

        /** 			puts( dn, diagram_file_deps( f ) )*/
        _34249 = _71diagram_file_deps(_f_67893);
        EPuts(_dn_67895, _34249); // DJP 
        DeRef(_34249);
        _34249 = NOVALUE;

        /** 			close( dn )*/
        EClose(_dn_67895);

        /** 			make_pngs( files[f] & ".dep" )*/
        _2 = (int)SEQ_PTR(_files_67825);
        _34250 = (int)*(((s1_ptr)_2)->base + _f_67893);
        if (IS_SEQUENCE(_34250) && IS_ATOM(_34251)) {
        }
        else if (IS_ATOM(_34250) && IS_SEQUENCE(_34251)) {
            Ref(_34250);
            Prepend(&_34252, _34251, _34250);
        }
        else {
            Concat((object_ptr)&_34252, _34250, _34251);
            _34250 = NOVALUE;
        }
        _34250 = NOVALUE;
        DeRef(_file_inlined_make_pngs_at_396_67909);
        _file_inlined_make_pngs_at_396_67909 = _34252;
        _34252 = NOVALUE;

        /** 		system( sprintf( "dot -Tpng %s.dot > %s.dot.png", repeat( file, 2 ) ), 2 )*/
        DeRef(_make_pngs_1__tmp_at399_67910);
        _make_pngs_1__tmp_at399_67910 = Repeat(_file_inlined_make_pngs_at_396_67909, 2);
        DeRefi(_make_pngs_2__tmp_at399_67911);
        _make_pngs_2__tmp_at399_67911 = EPrintf(-9999999, _34207, _make_pngs_1__tmp_at399_67910);
        system_call(_make_pngs_2__tmp_at399_67911, 2);

        /** end procedure*/
        goto L9; // [414] 417
L9: 
        DeRef(_file_inlined_make_pngs_at_396_67909);
        _file_inlined_make_pngs_at_396_67909 = NOVALUE;
        DeRef(_make_pngs_1__tmp_at399_67910);
        _make_pngs_1__tmp_at399_67910 = NOVALUE;
        DeRefi(_make_pngs_2__tmp_at399_67911);
        _make_pngs_2__tmp_at399_67911 = NOVALUE;
L8: 

        /** 	end for*/
        _f_67893 = _f_67893 + 1;
        goto L6; // [424] 346
L7: 
        ;
    }

    /** end procedure*/
    DeRefDS(_name_67824);
    DeRef(_files_67825);
    DeRef(_pp_67832);
    DeRef(_routines_67848);
    return;
    ;
}


void _70line_print(int _fn_67914, int _p_67915)
{
    int _line_break_67916 = NOVALUE;
    int _count_67917 = NOVALUE;
    int _34268 = NOVALUE;
    int _34266 = NOVALUE;
    int _34265 = NOVALUE;
    int _34264 = NOVALUE;
    int _34261 = NOVALUE;
    int _34260 = NOVALUE;
    int _34259 = NOVALUE;
    int _34258 = NOVALUE;
    int _34257 = NOVALUE;
    int _34255 = NOVALUE;
    int _34253 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_fn_67914)) {
        _1 = (long)(DBL_PTR(_fn_67914)->dbl);
        if (UNIQUE(DBL_PTR(_fn_67914)) && (DBL_PTR(_fn_67914)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fn_67914);
        _fn_67914 = _1;
    }

    /** 	if atom(p) then*/
    _34253 = IS_ATOM(_p_67915);
    if (_34253 == 0)
    {
        _34253 = NOVALUE;
        goto L1; // [8] 22
    }
    else{
        _34253 = NOVALUE;
    }

    /** 		print(fn, p)*/
    StdPrint(_fn_67914, _p_67915, 0);

    /** 		return*/
    DeRef(_p_67915);
    return;
L1: 

    /** 	p = pretty_sprint( p, ps_options )*/
    Ref(_p_67915);
    RefDS(_70ps_options_65975);
    _0 = _p_67915;
    _p_67915 = _23pretty_sprint(_p_67915, _70ps_options_65975);
    DeRef(_0);

    /** 	count = 0*/
    _count_67917 = 0;

    /** 	while length(p) > 95 do*/
L2: 
    if (IS_SEQUENCE(_p_67915)){
            _34255 = SEQ_PTR(_p_67915)->length;
    }
    else {
        _34255 = 1;
    }
    if (_34255 <= 95)
    goto L3; // [44] 155

    /** 		line_break = 95*/
    _line_break_67916 = 95;

    /** 		while line_break < length(p) and p[line_break] != ',' do*/
L4: 
    if (IS_SEQUENCE(_p_67915)){
            _34257 = SEQ_PTR(_p_67915)->length;
    }
    else {
        _34257 = 1;
    }
    _34258 = (_line_break_67916 < _34257);
    _34257 = NOVALUE;
    if (_34258 == 0) {
        goto L5; // [65] 92
    }
    _2 = (int)SEQ_PTR(_p_67915);
    _34260 = (int)*(((s1_ptr)_2)->base + _line_break_67916);
    if (IS_ATOM_INT(_34260)) {
        _34261 = (_34260 != 44);
    }
    else {
        _34261 = binary_op(NOTEQ, _34260, 44);
    }
    _34260 = NOVALUE;
    if (_34261 <= 0) {
        if (_34261 == 0) {
            DeRef(_34261);
            _34261 = NOVALUE;
            goto L5; // [78] 92
        }
        else {
            if (!IS_ATOM_INT(_34261) && DBL_PTR(_34261)->dbl == 0.0){
                DeRef(_34261);
                _34261 = NOVALUE;
                goto L5; // [78] 92
            }
            DeRef(_34261);
            _34261 = NOVALUE;
        }
    }
    DeRef(_34261);
    _34261 = NOVALUE;

    /** 			line_break += 1*/
    _line_break_67916 = _line_break_67916 + 1;

    /** 		end while*/
    goto L4; // [89] 58
L5: 

    /** 		if count then*/
    if (_count_67917 == 0)
    {
        goto L6; // [94] 105
    }
    else{
    }

    /** 			puts(fn, '\t')*/
    EPuts(_fn_67914, 9); // DJP 
    goto L7; // [102] 112
L6: 

    /** 			count += 1*/
    _count_67917 = _count_67917 + 1;
L7: 

    /** 		puts(fn, p[1..line_break])*/
    rhs_slice_target = (object_ptr)&_34264;
    RHS_Slice(_p_67915, 1, _line_break_67916);
    EPuts(_fn_67914, _34264); // DJP 
    DeRefDS(_34264);
    _34264 = NOVALUE;

    /** 		p = p[line_break+1..$]*/
    _34265 = _line_break_67916 + 1;
    if (_34265 > MAXINT){
        _34265 = NewDouble((double)_34265);
    }
    if (IS_SEQUENCE(_p_67915)){
            _34266 = SEQ_PTR(_p_67915)->length;
    }
    else {
        _34266 = 1;
    }
    rhs_slice_target = (object_ptr)&_p_67915;
    RHS_Slice(_p_67915, _34265, _34266);

    /** 		if length(p) then*/
    if (IS_SEQUENCE(_p_67915)){
            _34268 = SEQ_PTR(_p_67915)->length;
    }
    else {
        _34268 = 1;
    }
    if (_34268 == 0)
    {
        _34268 = NOVALUE;
        goto L2; // [141] 41
    }
    else{
        _34268 = NOVALUE;
    }

    /** 			puts(fn,"\n")*/
    EPuts(_fn_67914, _22834); // DJP 

    /** 	end while*/
    goto L2; // [152] 41
L3: 

    /** 	if count then*/
    if (_count_67917 == 0)
    {
        goto L8; // [157] 166
    }
    else{
    }

    /** 		puts(fn, '\t')*/
    EPuts(_fn_67914, 9); // DJP 
L8: 

    /** 	puts(fn, p )*/
    EPuts(_fn_67914, _p_67915); // DJP 

    /** end procedure*/
    DeRef(_p_67915);
    DeRef(_34258);
    _34258 = NOVALUE;
    DeRef(_34265);
    _34265 = NOVALUE;
    return;
    ;
}


int _70format_symbol(int _symbol_68002)
{
    int _vtype_68049 = NOVALUE;
    int _34348 = NOVALUE;
    int _34347 = NOVALUE;
    int _34346 = NOVALUE;
    int _34345 = NOVALUE;
    int _34344 = NOVALUE;
    int _34343 = NOVALUE;
    int _34342 = NOVALUE;
    int _34341 = NOVALUE;
    int _34340 = NOVALUE;
    int _34339 = NOVALUE;
    int _34338 = NOVALUE;
    int _34337 = NOVALUE;
    int _34336 = NOVALUE;
    int _34335 = NOVALUE;
    int _34334 = NOVALUE;
    int _34333 = NOVALUE;
    int _34331 = NOVALUE;
    int _34330 = NOVALUE;
    int _34328 = NOVALUE;
    int _34324 = NOVALUE;
    int _34323 = NOVALUE;
    int _34322 = NOVALUE;
    int _34321 = NOVALUE;
    int _34320 = NOVALUE;
    int _34319 = NOVALUE;
    int _34316 = NOVALUE;
    int _34315 = NOVALUE;
    int _34312 = NOVALUE;
    int _34311 = NOVALUE;
    int _34310 = NOVALUE;
    int _34308 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if symbol[S_MODE] = M_TEMP then*/
    _2 = (int)SEQ_PTR(_symbol_68002);
    _34308 = (int)*(((s1_ptr)_2)->base + 3);
    if (binary_op_a(NOTEQ, _34308, 3)){
        _34308 = NOVALUE;
        goto L1; // [13] 40
    }
    _34308 = NOVALUE;

    /** 		symbol[S_USAGE] = TEMP_USAGES[symbol[S_USAGE]]*/
    _2 = (int)SEQ_PTR(_symbol_68002);
    _34310 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_70TEMP_USAGES_67979);
    if (!IS_ATOM_INT(_34310)){
        _34311 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_34310)->dbl));
    }
    else{
        _34311 = (int)*(((s1_ptr)_2)->base + _34310);
    }
    RefDS(_34311);
    _2 = (int)SEQ_PTR(_symbol_68002);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _symbol_68002 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _34311;
    if( _1 != _34311 ){
        DeRef(_1);
    }
    _34311 = NOVALUE;
    goto L2; // [37] 329
L1: 

    /** 		switch symbol[S_USAGE] do*/
    _2 = (int)SEQ_PTR(_symbol_68002);
    _34312 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_34312) ){
        goto L3; // [48] 125
    }
    if(!IS_ATOM_INT(_34312)){
        if( (DBL_PTR(_34312)->dbl != (double) ((int) DBL_PTR(_34312)->dbl) ) ){
            goto L3; // [48] 125
        }
        _0 = (int) DBL_PTR(_34312)->dbl;
    }
    else {
        _0 = _34312;
    };
    _34312 = NOVALUE;
    switch ( _0 ){ 

        /** 		case U_UNUSED then*/
        case 0:

        /** 			symbol[S_USAGE] = "U_UNUSED"*/
        RefDS(_34289);
        _2 = (int)SEQ_PTR(_symbol_68002);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _symbol_68002 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = _34289;
        DeRef(_1);
        goto L4; // [65] 136

        /** 		case U_DELETED then*/
        case 99:

        /** 			symbol[S_USAGE] = "U_DELETED"*/
        RefDS(_34292);
        _2 = (int)SEQ_PTR(_symbol_68002);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _symbol_68002 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = _34292;
        DeRef(_1);
        goto L4; // [79] 136

        /** 		case U_READ, U_WRITTEN then*/
        case 1:
        case 2:

        /** 			symbol[S_USAGE] = USAGES[symbol[S_USAGE]]*/
        _2 = (int)SEQ_PTR(_symbol_68002);
        _34315 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_70USAGES_67972);
        if (!IS_ATOM_INT(_34315)){
            _34316 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_34315)->dbl));
        }
        else{
            _34316 = (int)*(((s1_ptr)_2)->base + _34315);
        }
        RefDS(_34316);
        _2 = (int)SEQ_PTR(_symbol_68002);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _symbol_68002 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = _34316;
        if( _1 != _34316 ){
            DeRef(_1);
        }
        _34316 = NOVALUE;
        goto L4; // [107] 136

        /** 		case 3 then*/
        case 3:

        /** 			symbol[S_USAGE] = "U_READ + U_WRITTEN"*/
        RefDS(_34317);
        _2 = (int)SEQ_PTR(_symbol_68002);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _symbol_68002 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = _34317;
        DeRef(_1);
        goto L4; // [121] 136

        /** 		case else*/
        default:
L3: 

        /** 			symbol[S_USAGE] = "Usage Unknown"*/
        RefDS(_34318);
        _2 = (int)SEQ_PTR(_symbol_68002);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _symbol_68002 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = _34318;
        DeRef(_1);
    ;}L4: 

    /** 		if length( symbol ) >= S_TOKEN and symbol[S_TOKEN] = VARIABLE then*/
    if (IS_SEQUENCE(_symbol_68002)){
            _34319 = SEQ_PTR(_symbol_68002)->length;
    }
    else {
        _34319 = 1;
    }
    if (IS_ATOM_INT(_25S_TOKEN_11918)) {
        _34320 = (_34319 >= _25S_TOKEN_11918);
    }
    else {
        _34320 = binary_op(GREATEREQ, _34319, _25S_TOKEN_11918);
    }
    _34319 = NOVALUE;
    if (IS_ATOM_INT(_34320)) {
        if (_34320 == 0) {
            goto L5; // [147] 328
        }
    }
    else {
        if (DBL_PTR(_34320)->dbl == 0.0) {
            goto L5; // [147] 328
        }
    }
    _2 = (int)SEQ_PTR(_symbol_68002);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _34322 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _34322 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    if (IS_ATOM_INT(_34322)) {
        _34323 = (_34322 == -100);
    }
    else {
        _34323 = binary_op(EQUALS, _34322, -100);
    }
    _34322 = NOVALUE;
    if (_34323 == 0) {
        DeRef(_34323);
        _34323 = NOVALUE;
        goto L5; // [164] 328
    }
    else {
        if (!IS_ATOM_INT(_34323) && DBL_PTR(_34323)->dbl == 0.0){
            DeRef(_34323);
            _34323 = NOVALUE;
            goto L5; // [164] 328
        }
        DeRef(_34323);
        _34323 = NOVALUE;
    }
    DeRef(_34323);
    _34323 = NOVALUE;

    /** 			if length( symbol ) >= S_VTYPE then*/
    if (IS_SEQUENCE(_symbol_68002)){
            _34324 = SEQ_PTR(_symbol_68002)->length;
    }
    else {
        _34324 = 1;
    }
    if (_34324 < 15)
    goto L6; // [174] 221

    /** 				integer vtype = symbol[S_VTYPE]*/
    _2 = (int)SEQ_PTR(_symbol_68002);
    _vtype_68049 = (int)*(((s1_ptr)_2)->base + 15);
    if (!IS_ATOM_INT(_vtype_68049))
    _vtype_68049 = (long)DBL_PTR(_vtype_68049)->dbl;

    /** 				if vtype > 0 then*/
    if (_vtype_68049 <= 0)
    goto L7; // [188] 207

    /** 					symbol[S_VTYPE] = sym_name( vtype )*/
    _34328 = _52sym_name(_vtype_68049);
    _2 = (int)SEQ_PTR(_symbol_68002);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _symbol_68002 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 15);
    _1 = *(int *)_2;
    *(int *)_2 = _34328;
    if( _1 != _34328 ){
        DeRef(_1);
    }
    _34328 = NOVALUE;
    goto L8; // [204] 220
L7: 

    /** 					symbol[S_VTYPE] = sprintf( "Unknown Type: %d", vtype )*/
    _34330 = EPrintf(-9999999, _34329, _vtype_68049);
    _2 = (int)SEQ_PTR(_symbol_68002);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _symbol_68002 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 15);
    _1 = *(int *)_2;
    *(int *)_2 = _34330;
    if( _1 != _34330 ){
        DeRef(_1);
    }
    _34330 = NOVALUE;
L8: 
L6: 

    /** 			if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L9; // [227] 327
    }
    else{
    }

    /** 				symbol[S_GTYPE] = map:get( gtypes, symbol[S_GTYPE], sprintf( "Unknown GType: %d", symbol[S_GTYPE] ) )*/
    _2 = (int)SEQ_PTR(_symbol_68002);
    _34331 = (int)*(((s1_ptr)_2)->base + 36);
    _2 = (int)SEQ_PTR(_symbol_68002);
    _34333 = (int)*(((s1_ptr)_2)->base + 36);
    _34334 = EPrintf(-9999999, _34332, _34333);
    _34333 = NOVALUE;
    Ref(_70gtypes_67986);
    Ref(_34331);
    _34335 = _32get(_70gtypes_67986, _34331, _34334);
    _34331 = NOVALUE;
    _34334 = NOVALUE;
    _2 = (int)SEQ_PTR(_symbol_68002);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _symbol_68002 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = _34335;
    if( _1 != _34335 ){
        DeRef(_1);
    }
    _34335 = NOVALUE;

    /** 				symbol[S_ARG_TYPE] = map:get( gtypes, symbol[S_ARG_TYPE], sprintf( "Unknown GType: %d", symbol[S_ARG_TYPE] ) )*/
    _2 = (int)SEQ_PTR(_symbol_68002);
    _34336 = (int)*(((s1_ptr)_2)->base + 43);
    _2 = (int)SEQ_PTR(_symbol_68002);
    _34337 = (int)*(((s1_ptr)_2)->base + 43);
    _34338 = EPrintf(-9999999, _34332, _34337);
    _34337 = NOVALUE;
    Ref(_70gtypes_67986);
    Ref(_34336);
    _34339 = _32get(_70gtypes_67986, _34336, _34338);
    _34336 = NOVALUE;
    _34338 = NOVALUE;
    _2 = (int)SEQ_PTR(_symbol_68002);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _symbol_68002 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 43);
    _1 = *(int *)_2;
    *(int *)_2 = _34339;
    if( _1 != _34339 ){
        DeRef(_1);
    }
    _34339 = NOVALUE;

    /** 				symbol[S_ARG_TYPE_NEW] = map:get( gtypes, symbol[S_ARG_TYPE_NEW], sprintf( "Unknown GType: %d", symbol[S_ARG_TYPE_NEW] ) )*/
    _2 = (int)SEQ_PTR(_symbol_68002);
    _34340 = (int)*(((s1_ptr)_2)->base + 44);
    _2 = (int)SEQ_PTR(_symbol_68002);
    _34341 = (int)*(((s1_ptr)_2)->base + 44);
    _34342 = EPrintf(-9999999, _34332, _34341);
    _34341 = NOVALUE;
    Ref(_70gtypes_67986);
    Ref(_34340);
    _34343 = _32get(_70gtypes_67986, _34340, _34342);
    _34340 = NOVALUE;
    _34342 = NOVALUE;
    _2 = (int)SEQ_PTR(_symbol_68002);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _symbol_68002 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 44);
    _1 = *(int *)_2;
    *(int *)_2 = _34343;
    if( _1 != _34343 ){
        DeRef(_1);
    }
    _34343 = NOVALUE;
L9: 
L5: 
L2: 

    /** 	symbol[S_MODE] = MODES[symbol[S_MODE]]*/
    _2 = (int)SEQ_PTR(_symbol_68002);
    _34344 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_70MODES_67942);
    if (!IS_ATOM_INT(_34344)){
        _34345 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_34344)->dbl));
    }
    else{
        _34345 = (int)*(((s1_ptr)_2)->base + _34344);
    }
    RefDS(_34345);
    _2 = (int)SEQ_PTR(_symbol_68002);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _symbol_68002 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _34345;
    if( _1 != _34345 ){
        DeRef(_1);
    }
    _34345 = NOVALUE;

    /** 	if symbol[S_SCOPE] then*/
    _2 = (int)SEQ_PTR(_symbol_68002);
    _34346 = (int)*(((s1_ptr)_2)->base + 4);
    if (_34346 == 0) {
        _34346 = NOVALUE;
        goto LA; // [357] 381
    }
    else {
        if (!IS_ATOM_INT(_34346) && DBL_PTR(_34346)->dbl == 0.0){
            _34346 = NOVALUE;
            goto LA; // [357] 381
        }
        _34346 = NOVALUE;
    }
    _34346 = NOVALUE;

    /** 		symbol[S_SCOPE] = SCOPES[symbol[S_SCOPE]]*/
    _2 = (int)SEQ_PTR(_symbol_68002);
    _34347 = (int)*(((s1_ptr)_2)->base + 4);
    _2 = (int)SEQ_PTR(_70SCOPES_67948);
    if (!IS_ATOM_INT(_34347)){
        _34348 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_34347)->dbl));
    }
    else{
        _34348 = (int)*(((s1_ptr)_2)->base + _34347);
    }
    RefDS(_34348);
    _2 = (int)SEQ_PTR(_symbol_68002);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _symbol_68002 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _34348;
    if( _1 != _34348 ){
        DeRef(_1);
    }
    _34348 = NOVALUE;
LA: 

    /** 	return symbol*/
    _34310 = NOVALUE;
    _34315 = NOVALUE;
    DeRef(_34320);
    _34320 = NOVALUE;
    _34344 = NOVALUE;
    _34347 = NOVALUE;
    return _symbol_68002;
    ;
}


void _70write_next_links(int _s_68100, int _fn_68101)
{
    int _34362 = NOVALUE;
    int _34360 = NOVALUE;
    int _34359 = NOVALUE;
    int _34358 = NOVALUE;
    int _34355 = NOVALUE;
    int _34354 = NOVALUE;
    int _34353 = NOVALUE;
    int _34350 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_s_68100)) {
        _1 = (long)(DBL_PTR(_s_68100)->dbl);
        if (UNIQUE(DBL_PTR(_s_68100)) && (DBL_PTR(_s_68100)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_68100);
        _s_68100 = _1;
    }
    if (!IS_ATOM_INT(_fn_68101)) {
        _1 = (long)(DBL_PTR(_fn_68101)->dbl);
        if (UNIQUE(DBL_PTR(_fn_68101)) && (DBL_PTR(_fn_68101)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fn_68101);
        _fn_68101 = _1;
    }

    /** 	puts( fn, "\nSYMBOL CHAIN:\n")*/
    EPuts(_fn_68101, _34349); // DJP 

    /** 	while s do*/
L1: 
    if (_s_68100 == 0)
    {
        goto L2; // [15] 121
    }
    else{
    }

    /** 		if sym_mode( s ) = M_TEMP then*/
    _34350 = _52sym_mode(_s_68100);
    if (binary_op_a(NOTEQ, _34350, 3)){
        DeRef(_34350);
        _34350 = NOVALUE;
        goto L3; // [26] 43
    }
    DeRef(_34350);
    _34350 = NOVALUE;

    /** 			printf(fn, "\t%6d TEMP\n", { s })*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _s_68100;
    _34353 = MAKE_SEQ(_1);
    EPrintf(_fn_68101, _34352, _34353);
    DeRefDS(_34353);
    _34353 = NOVALUE;
    goto L4; // [40] 92
L3: 

    /** 		elsif length( SymTab[s] ) >= S_NAME then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _34354 = (int)*(((s1_ptr)_2)->base + _s_68100);
    if (IS_SEQUENCE(_34354)){
            _34355 = SEQ_PTR(_34354)->length;
    }
    else {
        _34355 = 1;
    }
    _34354 = NOVALUE;
    if (binary_op_a(LESS, _34355, _25S_NAME_11913)){
        _34355 = NOVALUE;
        goto L5; // [56] 85
    }
    _34355 = NOVALUE;

    /** 			printf(fn, "\t%6d %s\n", { s, SymTab[s][S_NAME] })*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _34358 = (int)*(((s1_ptr)_2)->base + _s_68100);
    _2 = (int)SEQ_PTR(_34358);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _34359 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _34359 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _34358 = NOVALUE;
    Ref(_34359);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _s_68100;
    ((int *)_2)[2] = _34359;
    _34360 = MAKE_SEQ(_1);
    _34359 = NOVALUE;
    EPrintf(_fn_68101, _34357, _34360);
    DeRefDS(_34360);
    _34360 = NOVALUE;
    goto L4; // [82] 92
L5: 

    /** 			printf(fn, "\t%6d ?\n", s )*/
    EPrintf(_fn_68101, _34361, _s_68100);
L4: 

    /** 		in_chain[s] = 1*/
    _2 = (int)SEQ_PTR(_70in_chain_68096);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _70in_chain_68096 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _s_68100);
    *(int *)_2 = 1;

    /** 		s = SymTab[s][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _34362 = (int)*(((s1_ptr)_2)->base + _s_68100);
    _2 = (int)SEQ_PTR(_34362);
    _s_68100 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_68100)){
        _s_68100 = (long)DBL_PTR(_s_68100)->dbl;
    }
    _34362 = NOVALUE;

    /** 	end while*/
    goto L1; // [118] 15
L2: 

    /** end procedure*/
    _34354 = NOVALUE;
    return;
    ;
}


void _70save_il(int _name_68131)
{
    int _st_68132 = NOVALUE;
    int _max_width_68133 = NOVALUE;
    int _line_format_68134 = NOVALUE;
    int _pretty_options_68135 = NOVALUE;
    int _used_buckets_68137 = NOVALUE;
    int _symcnt_68138 = NOVALUE;
    int _bucket_usage_68139 = NOVALUE;
    int _bucket_68244 = NOVALUE;
    int _end_size_68248 = NOVALUE;
    int _bucket_reps_68252 = NOVALUE;
    int _size_68260 = NOVALUE;
    int _s_68261 = NOVALUE;
    int _h_68285 = NOVALUE;
    int _bx_68290 = NOVALUE;
    int _hit_counts_68397 = NOVALUE;
    int _hits_68402 = NOVALUE;
    int _34548 = NOVALUE;
    int _34546 = NOVALUE;
    int _34545 = NOVALUE;
    int _34544 = NOVALUE;
    int _34542 = NOVALUE;
    int _34541 = NOVALUE;
    int _34540 = NOVALUE;
    int _34539 = NOVALUE;
    int _34538 = NOVALUE;
    int _34537 = NOVALUE;
    int _34535 = NOVALUE;
    int _34534 = NOVALUE;
    int _34533 = NOVALUE;
    int _34531 = NOVALUE;
    int _34529 = NOVALUE;
    int _34528 = NOVALUE;
    int _34527 = NOVALUE;
    int _34526 = NOVALUE;
    int _34524 = NOVALUE;
    int _34523 = NOVALUE;
    int _34520 = NOVALUE;
    int _34519 = NOVALUE;
    int _34518 = NOVALUE;
    int _34517 = NOVALUE;
    int _34516 = NOVALUE;
    int _34515 = NOVALUE;
    int _34514 = NOVALUE;
    int _34513 = NOVALUE;
    int _34511 = NOVALUE;
    int _34510 = NOVALUE;
    int _34509 = NOVALUE;
    int _34508 = NOVALUE;
    int _34505 = NOVALUE;
    int _34504 = NOVALUE;
    int _34503 = NOVALUE;
    int _34500 = NOVALUE;
    int _34499 = NOVALUE;
    int _34498 = NOVALUE;
    int _34497 = NOVALUE;
    int _34496 = NOVALUE;
    int _34495 = NOVALUE;
    int _34494 = NOVALUE;
    int _34492 = NOVALUE;
    int _34491 = NOVALUE;
    int _34490 = NOVALUE;
    int _34489 = NOVALUE;
    int _34487 = NOVALUE;
    int _34486 = NOVALUE;
    int _34483 = NOVALUE;
    int _34482 = NOVALUE;
    int _34481 = NOVALUE;
    int _34478 = NOVALUE;
    int _34477 = NOVALUE;
    int _34475 = NOVALUE;
    int _34474 = NOVALUE;
    int _34473 = NOVALUE;
    int _34472 = NOVALUE;
    int _34471 = NOVALUE;
    int _34470 = NOVALUE;
    int _34469 = NOVALUE;
    int _34468 = NOVALUE;
    int _34467 = NOVALUE;
    int _34466 = NOVALUE;
    int _34463 = NOVALUE;
    int _34462 = NOVALUE;
    int _34461 = NOVALUE;
    int _34460 = NOVALUE;
    int _34459 = NOVALUE;
    int _34458 = NOVALUE;
    int _34456 = NOVALUE;
    int _34455 = NOVALUE;
    int _34454 = NOVALUE;
    int _34453 = NOVALUE;
    int _34452 = NOVALUE;
    int _34451 = NOVALUE;
    int _34450 = NOVALUE;
    int _34447 = NOVALUE;
    int _34446 = NOVALUE;
    int _34445 = NOVALUE;
    int _34443 = NOVALUE;
    int _34442 = NOVALUE;
    int _34441 = NOVALUE;
    int _34440 = NOVALUE;
    int _34439 = NOVALUE;
    int _34438 = NOVALUE;
    int _34437 = NOVALUE;
    int _34436 = NOVALUE;
    int _34434 = NOVALUE;
    int _34431 = NOVALUE;
    int _34429 = NOVALUE;
    int _34427 = NOVALUE;
    int _34425 = NOVALUE;
    int _34423 = NOVALUE;
    int _34422 = NOVALUE;
    int _34420 = NOVALUE;
    int _34419 = NOVALUE;
    int _34418 = NOVALUE;
    int _34417 = NOVALUE;
    int _34416 = NOVALUE;
    int _34415 = NOVALUE;
    int _34414 = NOVALUE;
    int _34413 = NOVALUE;
    int _34412 = NOVALUE;
    int _34410 = NOVALUE;
    int _34409 = NOVALUE;
    int _34408 = NOVALUE;
    int _34407 = NOVALUE;
    int _34406 = NOVALUE;
    int _34404 = NOVALUE;
    int _34403 = NOVALUE;
    int _34402 = NOVALUE;
    int _34401 = NOVALUE;
    int _34400 = NOVALUE;
    int _34399 = NOVALUE;
    int _34396 = NOVALUE;
    int _34394 = NOVALUE;
    int _34393 = NOVALUE;
    int _34392 = NOVALUE;
    int _34390 = NOVALUE;
    int _34389 = NOVALUE;
    int _34388 = NOVALUE;
    int _34387 = NOVALUE;
    int _34386 = NOVALUE;
    int _34384 = NOVALUE;
    int _34383 = NOVALUE;
    int _34381 = NOVALUE;
    int _34380 = NOVALUE;
    int _34379 = NOVALUE;
    int _34378 = NOVALUE;
    int _34377 = NOVALUE;
    int _34376 = NOVALUE;
    int _34374 = NOVALUE;
    int _34373 = NOVALUE;
    int _34372 = NOVALUE;
    int _34371 = NOVALUE;
    int _34370 = NOVALUE;
    int _34369 = NOVALUE;
    int _34366 = NOVALUE;
    int _34365 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	sequence line_format, pretty_options = PRETTY_DEFAULT*/
    RefDS(_23PRETTY_DEFAULT_8699);
    DeRef(_pretty_options_68135);
    _pretty_options_68135 = _23PRETTY_DEFAULT_8699;

    /** 	st = open( sprintf("%ssym", { name }), "wb" )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_name_68131);
    *((int *)(_2+4)) = _name_68131;
    _34365 = MAKE_SEQ(_1);
    _34366 = EPrintf(-9999999, _34364, _34365);
    DeRefDS(_34365);
    _34365 = NOVALUE;
    _st_68132 = EOpen(_34366, _34367, 0);
    DeRefDS(_34366);
    _34366 = NOVALUE;

    /** 	pretty_options[DISPLAY_ASCII] = 2*/
    _2 = (int)SEQ_PTR(_pretty_options_68135);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _pretty_options_68135 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);

    /** 	pretty_options[LINE_BREAKS]   = 0*/
    _2 = (int)SEQ_PTR(_pretty_options_68135);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _pretty_options_68135 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 10);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	for j = 1 to length( SymTab ) do*/
    if (IS_SEQUENCE(_26SymTab_11138)){
            _34369 = SEQ_PTR(_26SymTab_11138)->length;
    }
    else {
        _34369 = 1;
    }
    {
        int _j_68148;
        _j_68148 = 1;
L1: 
        if (_j_68148 > _34369){
            goto L2; // [50] 93
        }

        /** 		puts( st,  pretty_sprint( j & format_symbol( SymTab[j] ), pretty_options ) )*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _34370 = (int)*(((s1_ptr)_2)->base + _j_68148);
        Ref(_34370);
        _34371 = _70format_symbol(_34370);
        _34370 = NOVALUE;
        if (IS_SEQUENCE(_j_68148) && IS_ATOM(_34371)) {
        }
        else if (IS_ATOM(_j_68148) && IS_SEQUENCE(_34371)) {
            Prepend(&_34372, _34371, _j_68148);
        }
        else {
            Concat((object_ptr)&_34372, _j_68148, _34371);
        }
        DeRef(_34371);
        _34371 = NOVALUE;
        RefDS(_pretty_options_68135);
        _34373 = _23pretty_sprint(_34372, _pretty_options_68135);
        _34372 = NOVALUE;
        EPuts(_st_68132, _34373); // DJP 
        DeRef(_34373);
        _34373 = NOVALUE;

        /** 		puts( st, "\n" )*/
        EPuts(_st_68132, _22834); // DJP 

        /** 	end for*/
        _j_68148 = _j_68148 + 1;
        goto L1; // [88] 57
L2: 
        ;
    }

    /** 	in_chain = repeat( 0, length( SymTab ) )*/
    if (IS_SEQUENCE(_26SymTab_11138)){
            _34374 = SEQ_PTR(_26SymTab_11138)->length;
    }
    else {
        _34374 = 1;
    }
    DeRefi(_70in_chain_68096);
    _70in_chain_68096 = Repeat(0, _34374);
    _34374 = NOVALUE;

    /** 	for j = 1 to length( SymTab ) do*/
    if (IS_SEQUENCE(_26SymTab_11138)){
            _34376 = SEQ_PTR(_26SymTab_11138)->length;
    }
    else {
        _34376 = 1;
    }
    {
        int _j_68161;
        _j_68161 = 1;
L3: 
        if (_j_68161 > _34376){
            goto L4; // [111] 161
        }

        /** 		if not in_chain[j] and sym_mode( j ) != M_TEMP then*/
        _2 = (int)SEQ_PTR(_70in_chain_68096);
        _34377 = (int)*(((s1_ptr)_2)->base + _j_68161);
        _34378 = (_34377 == 0);
        _34377 = NOVALUE;
        if (_34378 == 0) {
            goto L5; // [129] 154
        }
        _34380 = _52sym_mode(_j_68161);
        if (IS_ATOM_INT(_34380)) {
            _34381 = (_34380 != 3);
        }
        else {
            _34381 = binary_op(NOTEQ, _34380, 3);
        }
        DeRef(_34380);
        _34380 = NOVALUE;
        if (_34381 == 0) {
            DeRef(_34381);
            _34381 = NOVALUE;
            goto L5; // [144] 154
        }
        else {
            if (!IS_ATOM_INT(_34381) && DBL_PTR(_34381)->dbl == 0.0){
                DeRef(_34381);
                _34381 = NOVALUE;
                goto L5; // [144] 154
            }
            DeRef(_34381);
            _34381 = NOVALUE;
        }
        DeRef(_34381);
        _34381 = NOVALUE;

        /** 			write_next_links( j, st )*/
        _70write_next_links(_j_68161, _st_68132);
L5: 

        /** 	end for*/
        _j_68161 = _j_68161 + 1;
        goto L3; // [156] 118
L4: 
        ;
    }

    /** 	in_chain = {}*/
    RefDS(_22682);
    DeRefi(_70in_chain_68096);
    _70in_chain_68096 = _22682;

    /** 	close( st )*/
    EClose(_st_68132);

    /** 	st = open( sprintf("%sline", {name}), "wb" )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_name_68131);
    *((int *)(_2+4)) = _name_68131;
    _34383 = MAKE_SEQ(_1);
    _34384 = EPrintf(-9999999, _34382, _34383);
    DeRefDS(_34383);
    _34383 = NOVALUE;
    _st_68132 = EOpen(_34384, _34367, 0);
    DeRefDS(_34384);
    _34384 = NOVALUE;

    /** 	if length(slist) and atom(slist[$]) then*/
    if (IS_SEQUENCE(_25slist_12357)){
            _34386 = SEQ_PTR(_25slist_12357)->length;
    }
    else {
        _34386 = 1;
    }
    if (_34386 == 0) {
        goto L6; // [194] 225
    }
    if (IS_SEQUENCE(_25slist_12357)){
            _34388 = SEQ_PTR(_25slist_12357)->length;
    }
    else {
        _34388 = 1;
    }
    _2 = (int)SEQ_PTR(_25slist_12357);
    _34389 = (int)*(((s1_ptr)_2)->base + _34388);
    _34390 = IS_ATOM(_34389);
    _34389 = NOVALUE;
    if (_34390 == 0)
    {
        _34390 = NOVALUE;
        goto L6; // [211] 225
    }
    else{
        _34390 = NOVALUE;
    }

    /** 		slist = s_expand( slist )*/
    RefDS(_25slist_12357);
    _0 = _60s_expand(_25slist_12357);
    DeRefDS(_25slist_12357);
    _25slist_12357 = _0;
L6: 

    /** 	max_width = 0*/
    _max_width_68133 = 0;

    /** 	for i = 1 to length(known_files) do*/
    if (IS_SEQUENCE(_26known_files_11139)){
            _34392 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _34392 = 1;
    }
    {
        int _i_68189;
        _i_68189 = 1;
L7: 
        if (_i_68189 > _34392){
            goto L8; // [237] 278
        }

        /** 		if length(known_files[i]) > max_width then*/
        _2 = (int)SEQ_PTR(_26known_files_11139);
        _34393 = (int)*(((s1_ptr)_2)->base + _i_68189);
        if (IS_SEQUENCE(_34393)){
                _34394 = SEQ_PTR(_34393)->length;
        }
        else {
            _34394 = 1;
        }
        _34393 = NOVALUE;
        if (_34394 <= _max_width_68133)
        goto L9; // [255] 271

        /** 			max_width = length(known_files[i])*/
        _2 = (int)SEQ_PTR(_26known_files_11139);
        _34396 = (int)*(((s1_ptr)_2)->base + _i_68189);
        if (IS_SEQUENCE(_34396)){
                _max_width_68133 = SEQ_PTR(_34396)->length;
        }
        else {
            _max_width_68133 = 1;
        }
        _34396 = NOVALUE;
L9: 

        /** 	end for*/
        _i_68189 = _i_68189 + 1;
        goto L7; // [273] 244
L8: 
        ;
    }

    /** 	line_format = sprintf("%%%ds %%%dd : %%s\n", {max_width, floor(log( length(slist) ) / log(10) ) + 1})*/
    if (IS_SEQUENCE(_25slist_12357)){
            _34399 = SEQ_PTR(_25slist_12357)->length;
    }
    else {
        _34399 = 1;
    }
    _34400 = e_log(_34399);
    _34399 = NOVALUE;
    _34401 = e_log(10);
    _2 = binary_op(DIVIDE, _34400, _34401);
    _34402 = unary_op(FLOOR, _2);
    DeRef(_2);
    DeRefDS(_34400);
    _34400 = NOVALUE;
    DeRefDS(_34401);
    _34401 = NOVALUE;
    if (IS_ATOM_INT(_34402)) {
        _34403 = _34402 + 1;
        if (_34403 > MAXINT){
            _34403 = NewDouble((double)_34403);
        }
    }
    else
    _34403 = binary_op(PLUS, 1, _34402);
    DeRef(_34402);
    _34402 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _max_width_68133;
    ((int *)_2)[2] = _34403;
    _34404 = MAKE_SEQ(_1);
    _34403 = NOVALUE;
    DeRefi(_line_format_68134);
    _line_format_68134 = EPrintf(-9999999, _34398, _34404);
    DeRefDS(_34404);
    _34404 = NOVALUE;

    /** 	for j = 1 to length(slist) do*/
    if (IS_SEQUENCE(_25slist_12357)){
            _34406 = SEQ_PTR(_25slist_12357)->length;
    }
    else {
        _34406 = 1;
    }
    {
        int _j_68210;
        _j_68210 = 1;
LA: 
        if (_j_68210 > _34406){
            goto LB; // [314] 422
        }

        /** 		if atom(slist[j][SRC]) then*/
        _2 = (int)SEQ_PTR(_25slist_12357);
        _34407 = (int)*(((s1_ptr)_2)->base + _j_68210);
        _2 = (int)SEQ_PTR(_34407);
        _34408 = (int)*(((s1_ptr)_2)->base + 1);
        _34407 = NOVALUE;
        _34409 = IS_ATOM(_34408);
        _34408 = NOVALUE;
        if (_34409 == 0)
        {
            _34409 = NOVALUE;
            goto LC; // [338] 373
        }
        else{
            _34409 = NOVALUE;
        }

        /** 			slist[j][SRC] = fetch_line(slist[j][SRC])*/
        _2 = (int)SEQ_PTR(_25slist_12357);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _25slist_12357 = MAKE_SEQ(_2);
        }
        _3 = (int)(_j_68210 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_25slist_12357);
        _34412 = (int)*(((s1_ptr)_2)->base + _j_68210);
        _2 = (int)SEQ_PTR(_34412);
        _34413 = (int)*(((s1_ptr)_2)->base + 1);
        _34412 = NOVALUE;
        Ref(_34413);
        _34414 = _60fetch_line(_34413);
        _34413 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 1);
        _1 = *(int *)_2;
        *(int *)_2 = _34414;
        if( _1 != _34414 ){
            DeRef(_1);
        }
        _34414 = NOVALUE;
        _34410 = NOVALUE;
LC: 

        /** 		printf( st, line_format, {known_files[slist[j][LOCAL_FILE_NO]], j, slist[j][SRC] })*/
        _2 = (int)SEQ_PTR(_25slist_12357);
        _34415 = (int)*(((s1_ptr)_2)->base + _j_68210);
        _2 = (int)SEQ_PTR(_34415);
        _34416 = (int)*(((s1_ptr)_2)->base + 3);
        _34415 = NOVALUE;
        _2 = (int)SEQ_PTR(_26known_files_11139);
        if (!IS_ATOM_INT(_34416)){
            _34417 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_34416)->dbl));
        }
        else{
            _34417 = (int)*(((s1_ptr)_2)->base + _34416);
        }
        _2 = (int)SEQ_PTR(_25slist_12357);
        _34418 = (int)*(((s1_ptr)_2)->base + _j_68210);
        _2 = (int)SEQ_PTR(_34418);
        _34419 = (int)*(((s1_ptr)_2)->base + 1);
        _34418 = NOVALUE;
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_34417);
        *((int *)(_2+4)) = _34417;
        *((int *)(_2+8)) = _j_68210;
        Ref(_34419);
        *((int *)(_2+12)) = _34419;
        _34420 = MAKE_SEQ(_1);
        _34419 = NOVALUE;
        _34417 = NOVALUE;
        EPrintf(_st_68132, _line_format_68134, _34420);
        DeRefDS(_34420);
        _34420 = NOVALUE;

        /** 	end for*/
        _j_68210 = _j_68210 + 1;
        goto LA; // [417] 321
LB: 
        ;
    }

    /** 	close(st)*/
    EClose(_st_68132);

    /** 	st = open( sprintf("%shash", { name }), "wb" )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_name_68131);
    *((int *)(_2+4)) = _name_68131;
    _34422 = MAKE_SEQ(_1);
    _34423 = EPrintf(-9999999, _34421, _34422);
    DeRefDS(_34422);
    _34422 = NOVALUE;
    _st_68132 = EOpen(_34423, _34367, 0);
    DeRefDS(_34423);
    _34423 = NOVALUE;

    /** 	sequence bucket = repeat( "", length( buckets ) )*/
    _34425 = 2004;
    DeRef(_bucket_68244);
    _bucket_68244 = Repeat(_22682, 2004);
    _34425 = NOVALUE;

    /** 	sequence end_size = repeat( "", length( buckets ) )*/
    _34427 = 2004;
    DeRef(_end_size_68248);
    _end_size_68248 = Repeat(_22682, 2004);
    _34427 = NOVALUE;

    /** 	sequence bucket_reps = repeat( "", length( buckets ) ) */
    _34429 = 2004;
    DeRef(_bucket_reps_68252);
    _bucket_reps_68252 = Repeat(_22682, 2004);
    _34429 = NOVALUE;

    /** 	for i = 1 to length( buckets ) do*/
    _34431 = 2004;
    {
        int _i_68257;
        _i_68257 = 1;
LD: 
        if (_i_68257 > 2004){
            goto LE; // [481] 553
        }

        /** 		integer size = 0*/
        _size_68260 = 0;

        /** 		integer s = buckets[i]*/
        _2 = (int)SEQ_PTR(_52buckets_47095);
        _s_68261 = (int)*(((s1_ptr)_2)->base + _i_68257);
        if (!IS_ATOM_INT(_s_68261)){
            _s_68261 = (long)DBL_PTR(_s_68261)->dbl;
        }

        /** 		while s do*/
LF: 
        if (_s_68261 == 0)
        {
            goto L10; // [508] 538
        }
        else{
        }

        /** 			size += 1*/
        _size_68260 = _size_68260 + 1;

        /** 			s = SymTab[s][S_SAMEHASH]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _34434 = (int)*(((s1_ptr)_2)->base + _s_68261);
        _2 = (int)SEQ_PTR(_34434);
        _s_68261 = (int)*(((s1_ptr)_2)->base + 9);
        if (!IS_ATOM_INT(_s_68261)){
            _s_68261 = (long)DBL_PTR(_s_68261)->dbl;
        }
        _34434 = NOVALUE;

        /** 		end while*/
        goto LF; // [535] 508
L10: 

        /** 		end_size[i] = size*/
        _2 = (int)SEQ_PTR(_end_size_68248);
        _2 = (int)(((s1_ptr)_2)->base + _i_68257);
        _1 = *(int *)_2;
        *(int *)_2 = _size_68260;
        DeRef(_1);

        /** 	end for*/
        _i_68257 = _i_68257 + 1;
        goto LD; // [548] 488
LE: 
        ;
    }

    /** 	used_buckets = 0*/
    _used_buckets_68137 = 0;

    /** 	symcnt = 0*/
    _symcnt_68138 = 0;

    /** 	bucket_usage = {}*/
    RefDS(_22682);
    DeRef(_bucket_usage_68139);
    _bucket_usage_68139 = _22682;

    /** 	for i = 1 to length( SymTab ) do*/
    if (IS_SEQUENCE(_26SymTab_11138)){
            _34436 = SEQ_PTR(_26SymTab_11138)->length;
    }
    else {
        _34436 = 1;
    }
    {
        int _i_68271;
        _i_68271 = 1;
L11: 
        if (_i_68271 > _34436){
            goto L12; // [577] 738
        }

        /** 		if length( SymTab[i] ) >= S_HASHVAL and SymTab[i][S_HASHVAL] then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _34437 = (int)*(((s1_ptr)_2)->base + _i_68271);
        if (IS_SEQUENCE(_34437)){
                _34438 = SEQ_PTR(_34437)->length;
        }
        else {
            _34438 = 1;
        }
        _34437 = NOVALUE;
        _34439 = (_34438 >= 11);
        _34438 = NOVALUE;
        if (_34439 == 0) {
            goto L13; // [601] 729
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _34441 = (int)*(((s1_ptr)_2)->base + _i_68271);
        _2 = (int)SEQ_PTR(_34441);
        _34442 = (int)*(((s1_ptr)_2)->base + 11);
        _34441 = NOVALUE;
        if (_34442 == 0) {
            _34442 = NOVALUE;
            goto L13; // [618] 729
        }
        else {
            if (!IS_ATOM_INT(_34442) && DBL_PTR(_34442)->dbl == 0.0){
                _34442 = NOVALUE;
                goto L13; // [618] 729
            }
            _34442 = NOVALUE;
        }
        _34442 = NOVALUE;

        /** 			integer h = SymTab[i][S_HASHVAL]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _34443 = (int)*(((s1_ptr)_2)->base + _i_68271);
        _2 = (int)SEQ_PTR(_34443);
        _h_68285 = (int)*(((s1_ptr)_2)->base + 11);
        if (!IS_ATOM_INT(_h_68285)){
            _h_68285 = (long)DBL_PTR(_h_68285)->dbl;
        }
        _34443 = NOVALUE;

        /** 			integer bx = find( SymTab[i][S_NAME], bucket[h] )*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _34445 = (int)*(((s1_ptr)_2)->base + _i_68271);
        _2 = (int)SEQ_PTR(_34445);
        if (!IS_ATOM_INT(_25S_NAME_11913)){
            _34446 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
        }
        else{
            _34446 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
        }
        _34445 = NOVALUE;
        _2 = (int)SEQ_PTR(_bucket_68244);
        _34447 = (int)*(((s1_ptr)_2)->base + _h_68285);
        _bx_68290 = find_from(_34446, _34447, 1);
        _34446 = NOVALUE;
        _34447 = NOVALUE;

        /** 			if not bx then*/
        if (_bx_68290 != 0)
        goto L14; // [662] 708

        /** 				bucket[h] = append( bucket[h], SymTab[i][S_NAME] )*/
        _2 = (int)SEQ_PTR(_bucket_68244);
        _34450 = (int)*(((s1_ptr)_2)->base + _h_68285);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _34451 = (int)*(((s1_ptr)_2)->base + _i_68271);
        _2 = (int)SEQ_PTR(_34451);
        if (!IS_ATOM_INT(_25S_NAME_11913)){
            _34452 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
        }
        else{
            _34452 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
        }
        _34451 = NOVALUE;
        Ref(_34452);
        Append(&_34453, _34450, _34452);
        _34450 = NOVALUE;
        _34452 = NOVALUE;
        _2 = (int)SEQ_PTR(_bucket_68244);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _bucket_68244 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _h_68285);
        _1 = *(int *)_2;
        *(int *)_2 = _34453;
        if( _1 != _34453 ){
            DeRef(_1);
        }
        _34453 = NOVALUE;

        /** 				bucket_reps[h] &= 1*/
        _2 = (int)SEQ_PTR(_bucket_reps_68252);
        _34454 = (int)*(((s1_ptr)_2)->base + _h_68285);
        if (IS_SEQUENCE(_34454) && IS_ATOM(1)) {
            Append(&_34455, _34454, 1);
        }
        else if (IS_ATOM(_34454) && IS_SEQUENCE(1)) {
        }
        else {
            Concat((object_ptr)&_34455, _34454, 1);
            _34454 = NOVALUE;
        }
        _34454 = NOVALUE;
        _2 = (int)SEQ_PTR(_bucket_reps_68252);
        _2 = (int)(((s1_ptr)_2)->base + _h_68285);
        _1 = *(int *)_2;
        *(int *)_2 = _34455;
        if( _1 != _34455 ){
            DeRef(_1);
        }
        _34455 = NOVALUE;
        goto L15; // [705] 728
L14: 

        /** 				bucket_reps[h][bx] += 1*/
        _2 = (int)SEQ_PTR(_bucket_reps_68252);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _bucket_reps_68252 = MAKE_SEQ(_2);
        }
        _3 = (int)(_h_68285 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _34458 = (int)*(((s1_ptr)_2)->base + _bx_68290);
        _34456 = NOVALUE;
        if (IS_ATOM_INT(_34458)) {
            _34459 = _34458 + 1;
            if (_34459 > MAXINT){
                _34459 = NewDouble((double)_34459);
            }
        }
        else
        _34459 = binary_op(PLUS, 1, _34458);
        _34458 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _bx_68290);
        _1 = *(int *)_2;
        *(int *)_2 = _34459;
        if( _1 != _34459 ){
            DeRef(_1);
        }
        _34459 = NOVALUE;
        _34456 = NOVALUE;
L15: 
L13: 

        /** 	end for*/
        _i_68271 = _i_68271 + 1;
        goto L11; // [733] 584
L12: 
        ;
    }

    /** 	for i = 1 to length( bucket ) do*/
    if (IS_SEQUENCE(_bucket_68244)){
            _34460 = SEQ_PTR(_bucket_68244)->length;
    }
    else {
        _34460 = 1;
    }
    {
        int _i_68313;
        _i_68313 = 1;
L16: 
        if (_i_68313 > _34460){
            goto L17; // [743] 839
        }

        /** 		for j = 1 to length( bucket[i] ) do*/
        _2 = (int)SEQ_PTR(_bucket_68244);
        _34461 = (int)*(((s1_ptr)_2)->base + _i_68313);
        if (IS_SEQUENCE(_34461)){
                _34462 = SEQ_PTR(_34461)->length;
        }
        else {
            _34462 = 1;
        }
        _34461 = NOVALUE;
        {
            int _j_68316;
            _j_68316 = 1;
L18: 
            if (_j_68316 > _34462){
                goto L19; // [759] 808
            }

            /** 			bucket[i][j] = sprintf( "[%d:%s]", {bucket_reps[i][j], bucket[i][j]})*/
            _2 = (int)SEQ_PTR(_bucket_68244);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _bucket_68244 = MAKE_SEQ(_2);
            }
            _3 = (int)(_i_68313 + ((s1_ptr)_2)->base);
            _2 = (int)SEQ_PTR(_bucket_reps_68252);
            _34466 = (int)*(((s1_ptr)_2)->base + _i_68313);
            _2 = (int)SEQ_PTR(_34466);
            _34467 = (int)*(((s1_ptr)_2)->base + _j_68316);
            _34466 = NOVALUE;
            _2 = (int)SEQ_PTR(_bucket_68244);
            _34468 = (int)*(((s1_ptr)_2)->base + _i_68313);
            _2 = (int)SEQ_PTR(_34468);
            _34469 = (int)*(((s1_ptr)_2)->base + _j_68316);
            _34468 = NOVALUE;
            Ref(_34469);
            Ref(_34467);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _34467;
            ((int *)_2)[2] = _34469;
            _34470 = MAKE_SEQ(_1);
            _34469 = NOVALUE;
            _34467 = NOVALUE;
            _34471 = EPrintf(-9999999, _34465, _34470);
            DeRefDS(_34470);
            _34470 = NOVALUE;
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_68316);
            _1 = *(int *)_2;
            *(int *)_2 = _34471;
            if( _1 != _34471 ){
                DeRef(_1);
            }
            _34471 = NOVALUE;
            _34463 = NOVALUE;

            /** 		end for*/
            _j_68316 = _j_68316 + 1;
            goto L18; // [803] 766
L19: 
            ;
        }

        /** 		bucket[i] = sum(bucket_reps[i]) & i & bucket[i]*/
        _2 = (int)SEQ_PTR(_bucket_reps_68252);
        _34472 = (int)*(((s1_ptr)_2)->base + _i_68313);
        Ref(_34472);
        _34473 = _18sum(_34472);
        _34472 = NOVALUE;
        _2 = (int)SEQ_PTR(_bucket_68244);
        _34474 = (int)*(((s1_ptr)_2)->base + _i_68313);
        {
            int concat_list[3];

            concat_list[0] = _34474;
            concat_list[1] = _i_68313;
            concat_list[2] = _34473;
            Concat_N((object_ptr)&_34475, concat_list, 3);
        }
        _34474 = NOVALUE;
        DeRef(_34473);
        _34473 = NOVALUE;
        _2 = (int)SEQ_PTR(_bucket_68244);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _bucket_68244 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_68313);
        _1 = *(int *)_2;
        *(int *)_2 = _34475;
        if( _1 != _34475 ){
            DeRef(_1);
        }
        _34475 = NOVALUE;

        /** 	end for*/
        _i_68313 = _i_68313 + 1;
        goto L16; // [834] 750
L17: 
        ;
    }

    /** 	bucket = sort(bucket, DESCENDING)*/
    RefDS(_bucket_68244);
    _0 = _bucket_68244;
    _bucket_68244 = _22sort(_bucket_68244, -1);
    DeRefDS(_0);

    /** 	bucket_usage = repeat(0, bucket[1][1])*/
    _2 = (int)SEQ_PTR(_bucket_68244);
    _34477 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_34477);
    _34478 = (int)*(((s1_ptr)_2)->base + 1);
    _34477 = NOVALUE;
    DeRef(_bucket_usage_68139);
    _bucket_usage_68139 = Repeat(0, _34478);
    _34478 = NOVALUE;

    /** 	puts(st, "Bucket size / hashval / Ending Size / hits : contents\n" )*/
    EPuts(_st_68132, _34480); // DJP 

    /** 	for i = 1 to length( bucket ) do*/
    if (IS_SEQUENCE(_bucket_68244)){
            _34481 = SEQ_PTR(_bucket_68244)->length;
    }
    else {
        _34481 = 1;
    }
    {
        int _i_68341;
        _i_68341 = 1;
L1A: 
        if (_i_68341 > _34481){
            goto L1B; // [874] 1036
        }

        /** 		if bucket[i][1] > 0 then*/
        _2 = (int)SEQ_PTR(_bucket_68244);
        _34482 = (int)*(((s1_ptr)_2)->base + _i_68341);
        _2 = (int)SEQ_PTR(_34482);
        _34483 = (int)*(((s1_ptr)_2)->base + 1);
        _34482 = NOVALUE;
        if (binary_op_a(LESSEQ, _34483, 0)){
            _34483 = NOVALUE;
            goto L1C; // [891] 1029
        }
        _34483 = NOVALUE;

        /** 			used_buckets += 1*/
        _used_buckets_68137 = _used_buckets_68137 + 1;

        /** 			symcnt += bucket[i][1]*/
        _2 = (int)SEQ_PTR(_bucket_68244);
        _34486 = (int)*(((s1_ptr)_2)->base + _i_68341);
        _2 = (int)SEQ_PTR(_34486);
        _34487 = (int)*(((s1_ptr)_2)->base + 1);
        _34486 = NOVALUE;
        if (IS_ATOM_INT(_34487)) {
            _symcnt_68138 = _symcnt_68138 + _34487;
        }
        else {
            _symcnt_68138 = binary_op(PLUS, _symcnt_68138, _34487);
        }
        _34487 = NOVALUE;
        if (!IS_ATOM_INT(_symcnt_68138)) {
            _1 = (long)(DBL_PTR(_symcnt_68138)->dbl);
            if (UNIQUE(DBL_PTR(_symcnt_68138)) && (DBL_PTR(_symcnt_68138)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_symcnt_68138);
            _symcnt_68138 = _1;
        }

        /** 			bucket_usage[bucket[i][1]] += 1*/
        _2 = (int)SEQ_PTR(_bucket_68244);
        _34489 = (int)*(((s1_ptr)_2)->base + _i_68341);
        _2 = (int)SEQ_PTR(_34489);
        _34490 = (int)*(((s1_ptr)_2)->base + 1);
        _34489 = NOVALUE;
        _2 = (int)SEQ_PTR(_bucket_usage_68139);
        if (!IS_ATOM_INT(_34490)){
            _34491 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_34490)->dbl));
        }
        else{
            _34491 = (int)*(((s1_ptr)_2)->base + _34490);
        }
        if (IS_ATOM_INT(_34491)) {
            _34492 = _34491 + 1;
            if (_34492 > MAXINT){
                _34492 = NewDouble((double)_34492);
            }
        }
        else
        _34492 = binary_op(PLUS, 1, _34491);
        _34491 = NOVALUE;
        _2 = (int)SEQ_PTR(_bucket_usage_68139);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _bucket_usage_68139 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_34490))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_34490)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _34490);
        _1 = *(int *)_2;
        *(int *)_2 = _34492;
        if( _1 != _34492 ){
            DeRef(_1);
        }
        _34492 = NOVALUE;

        /** 			printf( st, "%5d %5d %5d %5d: ", bucket[i][1..2] & end_size[i] & bucket_hits[i] )*/
        _2 = (int)SEQ_PTR(_bucket_68244);
        _34494 = (int)*(((s1_ptr)_2)->base + _i_68341);
        rhs_slice_target = (object_ptr)&_34495;
        RHS_Slice(_34494, 1, 2);
        _34494 = NOVALUE;
        _2 = (int)SEQ_PTR(_end_size_68248);
        _34496 = (int)*(((s1_ptr)_2)->base + _i_68341);
        _2 = (int)SEQ_PTR(_52bucket_hits_47106);
        _34497 = (int)*(((s1_ptr)_2)->base + _i_68341);
        {
            int concat_list[3];

            concat_list[0] = _34497;
            concat_list[1] = _34496;
            concat_list[2] = _34495;
            Concat_N((object_ptr)&_34498, concat_list, 3);
        }
        _34497 = NOVALUE;
        _34496 = NOVALUE;
        DeRefDS(_34495);
        _34495 = NOVALUE;
        EPrintf(_st_68132, _34493, _34498);
        DeRefDS(_34498);
        _34498 = NOVALUE;

        /** 			for j = 3 to length( bucket[i] ) do*/
        _2 = (int)SEQ_PTR(_bucket_68244);
        _34499 = (int)*(((s1_ptr)_2)->base + _i_68341);
        if (IS_SEQUENCE(_34499)){
                _34500 = SEQ_PTR(_34499)->length;
        }
        else {
            _34500 = 1;
        }
        _34499 = NOVALUE;
        {
            int _j_68363;
            _j_68363 = 3;
L1D: 
            if (_j_68363 > _34500){
                goto L1E; // [979] 1023
            }

            /** 				if j > 3 then*/
            if (_j_68363 <= 3)
            goto L1F; // [988] 998

            /** 					puts( st, ", " )*/
            EPuts(_st_68132, _33026); // DJP 
L1F: 

            /** 				printf( st, "%s", {bucket[i][j]} )*/
            _2 = (int)SEQ_PTR(_bucket_68244);
            _34503 = (int)*(((s1_ptr)_2)->base + _i_68341);
            _2 = (int)SEQ_PTR(_34503);
            _34504 = (int)*(((s1_ptr)_2)->base + _j_68363);
            _34503 = NOVALUE;
            _1 = NewS1(1);
            _2 = (int)((s1_ptr)_1)->base;
            Ref(_34504);
            *((int *)(_2+4)) = _34504;
            _34505 = MAKE_SEQ(_1);
            _34504 = NOVALUE;
            EPrintf(_st_68132, _34502, _34505);
            DeRefDS(_34505);
            _34505 = NOVALUE;

            /** 			end for*/
            _j_68363 = _j_68363 + 1;
            goto L1D; // [1018] 986
L1E: 
            ;
        }

        /** 			puts( st, '\n' )*/
        EPuts(_st_68132, 10); // DJP 
L1C: 

        /** 	end for*/
        _i_68341 = _i_68341 + 1;
        goto L1A; // [1031] 881
L1B: 
        ;
    }

    /** 	puts( st, '\n' )*/
    EPuts(_st_68132, 10); // DJP 

    /** 	printf( st, "Symbols         : %d\n", symcnt )*/
    EPrintf(_st_68132, _34506, _symcnt_68138);

    /** 	printf( st, "Used buckets    : %d (%3.1f%%)\n", {used_buckets, 100 * used_buckets / length(bucket)})*/
    if (_used_buckets_68137 <= INT15 && _used_buckets_68137 >= -INT15)
    _34508 = 100 * _used_buckets_68137;
    else
    _34508 = NewDouble(100 * (double)_used_buckets_68137);
    if (IS_SEQUENCE(_bucket_68244)){
            _34509 = SEQ_PTR(_bucket_68244)->length;
    }
    else {
        _34509 = 1;
    }
    if (IS_ATOM_INT(_34508)) {
        _34510 = (_34508 % _34509) ? NewDouble((double)_34508 / _34509) : (_34508 / _34509);
    }
    else {
        _34510 = NewDouble(DBL_PTR(_34508)->dbl / (double)_34509);
    }
    DeRef(_34508);
    _34508 = NOVALUE;
    _34509 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _used_buckets_68137;
    ((int *)_2)[2] = _34510;
    _34511 = MAKE_SEQ(_1);
    _34510 = NOVALUE;
    EPrintf(_st_68132, _34507, _34511);
    DeRefDS(_34511);
    _34511 = NOVALUE;

    /** 	printf( st, "Empty buckets   : %d (%3.1f%%)\n", {length(bucket) - used_buckets, 100 * (length(bucket) - used_buckets) / length(bucket)})*/
    if (IS_SEQUENCE(_bucket_68244)){
            _34513 = SEQ_PTR(_bucket_68244)->length;
    }
    else {
        _34513 = 1;
    }
    _34514 = _34513 - _used_buckets_68137;
    if ((long)((unsigned long)_34514 +(unsigned long) HIGH_BITS) >= 0){
        _34514 = NewDouble((double)_34514);
    }
    _34513 = NOVALUE;
    if (IS_SEQUENCE(_bucket_68244)){
            _34515 = SEQ_PTR(_bucket_68244)->length;
    }
    else {
        _34515 = 1;
    }
    _34516 = _34515 - _used_buckets_68137;
    if ((long)((unsigned long)_34516 +(unsigned long) HIGH_BITS) >= 0){
        _34516 = NewDouble((double)_34516);
    }
    _34515 = NOVALUE;
    if (IS_ATOM_INT(_34516)) {
        if (_34516 <= INT15 && _34516 >= -INT15)
        _34517 = 100 * _34516;
        else
        _34517 = NewDouble(100 * (double)_34516);
    }
    else {
        _34517 = NewDouble((double)100 * DBL_PTR(_34516)->dbl);
    }
    DeRef(_34516);
    _34516 = NOVALUE;
    if (IS_SEQUENCE(_bucket_68244)){
            _34518 = SEQ_PTR(_bucket_68244)->length;
    }
    else {
        _34518 = 1;
    }
    if (IS_ATOM_INT(_34517)) {
        _34519 = (_34517 % _34518) ? NewDouble((double)_34517 / _34518) : (_34517 / _34518);
    }
    else {
        _34519 = NewDouble(DBL_PTR(_34517)->dbl / (double)_34518);
    }
    DeRef(_34517);
    _34517 = NOVALUE;
    _34518 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _34514;
    ((int *)_2)[2] = _34519;
    _34520 = MAKE_SEQ(_1);
    _34519 = NOVALUE;
    _34514 = NOVALUE;
    EPrintf(_st_68132, _34512, _34520);
    DeRefDS(_34520);
    _34520 = NOVALUE;

    /** 	if used_buckets > 0 then*/
    if (_used_buckets_68137 <= 0)
    goto L20; // [1105] 1321

    /** 		printf( st, "Symbols / bucket: %4.2f\n", symcnt / used_buckets)*/
    _34523 = (_symcnt_68138 % _used_buckets_68137) ? NewDouble((double)_symcnt_68138 / _used_buckets_68137) : (_symcnt_68138 / _used_buckets_68137);
    EPrintf(_st_68132, _34522, _34523);
    DeRef(_34523);
    _34523 = NOVALUE;

    /** 		for i = 1 to length(bucket_usage) do*/
    if (IS_SEQUENCE(_bucket_usage_68139)){
            _34524 = SEQ_PTR(_bucket_usage_68139)->length;
    }
    else {
        _34524 = 1;
    }
    {
        int _i_68392;
        _i_68392 = 1;
L21: 
        if (_i_68392 > _34524){
            goto L22; // [1124] 1152
        }

        /** 			printf( st, "Len %2d : %d\n", {i, bucket_usage[i]})*/
        _2 = (int)SEQ_PTR(_bucket_usage_68139);
        _34526 = (int)*(((s1_ptr)_2)->base + _i_68392);
        Ref(_34526);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _i_68392;
        ((int *)_2)[2] = _34526;
        _34527 = MAKE_SEQ(_1);
        _34526 = NOVALUE;
        EPrintf(_st_68132, _34525, _34527);
        DeRefDS(_34527);
        _34527 = NOVALUE;

        /** 		end for*/
        _i_68392 = _i_68392 + 1;
        goto L21; // [1147] 1131
L22: 
        ;
    }

    /** 		sequence hit_counts = {}*/
    RefDS(_22682);
    DeRef(_hit_counts_68397);
    _hit_counts_68397 = _22682;

    /** 		for i = 1 to length( bucket_hits ) do*/
    if (IS_SEQUENCE(_52bucket_hits_47106)){
            _34528 = SEQ_PTR(_52bucket_hits_47106)->length;
    }
    else {
        _34528 = 1;
    }
    {
        int _i_68399;
        _i_68399 = 1;
L23: 
        if (_i_68399 > _34528){
            goto L24; // [1166] 1235
        }

        /** 			integer hits = bucket_hits[i] + 1 -- could be 0*/
        _2 = (int)SEQ_PTR(_52bucket_hits_47106);
        _34529 = (int)*(((s1_ptr)_2)->base + _i_68399);
        if (IS_ATOM_INT(_34529)) {
            _hits_68402 = _34529 + 1;
        }
        else
        { // coercing _hits_68402 to an integer 1
            _hits_68402 = 1+(long)(DBL_PTR(_34529)->dbl);
            if( !IS_ATOM_INT(_hits_68402) ){
                _hits_68402 = (object)DBL_PTR(_hits_68402)->dbl;
            }
        }
        _34529 = NOVALUE;

        /** 			if length( hit_counts ) < hits then*/
        if (IS_SEQUENCE(_hit_counts_68397)){
                _34531 = SEQ_PTR(_hit_counts_68397)->length;
        }
        else {
            _34531 = 1;
        }
        if (_34531 >= _hits_68402)
        goto L25; // [1190] 1212

        /** 				hit_counts &= repeat( 0, hits - length( hit_counts ) )*/
        if (IS_SEQUENCE(_hit_counts_68397)){
                _34533 = SEQ_PTR(_hit_counts_68397)->length;
        }
        else {
            _34533 = 1;
        }
        _34534 = _hits_68402 - _34533;
        _34533 = NOVALUE;
        _34535 = Repeat(0, _34534);
        _34534 = NOVALUE;
        Concat((object_ptr)&_hit_counts_68397, _hit_counts_68397, _34535);
        DeRefDS(_34535);
        _34535 = NOVALUE;
L25: 

        /** 			hit_counts[hits] += 1*/
        _2 = (int)SEQ_PTR(_hit_counts_68397);
        _34537 = (int)*(((s1_ptr)_2)->base + _hits_68402);
        if (IS_ATOM_INT(_34537)) {
            _34538 = _34537 + 1;
            if (_34538 > MAXINT){
                _34538 = NewDouble((double)_34538);
            }
        }
        else
        _34538 = binary_op(PLUS, 1, _34537);
        _34537 = NOVALUE;
        _2 = (int)SEQ_PTR(_hit_counts_68397);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _hit_counts_68397 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _hits_68402);
        _1 = *(int *)_2;
        *(int *)_2 = _34538;
        if( _1 != _34538 ){
            DeRef(_1);
        }
        _34538 = NOVALUE;

        /** 		end for*/
        _i_68399 = _i_68399 + 1;
        goto L23; // [1230] 1173
L24: 
        ;
    }

    /** 		for i = 1 to length( hit_counts ) do*/
    if (IS_SEQUENCE(_hit_counts_68397)){
            _34539 = SEQ_PTR(_hit_counts_68397)->length;
    }
    else {
        _34539 = 1;
    }
    {
        int _i_68416;
        _i_68416 = 1;
L26: 
        if (_i_68416 > _34539){
            goto L27; // [1240] 1272
        }

        /** 			hit_counts[i] = { i-1, hit_counts[i] }*/
        _34540 = _i_68416 - 1;
        _2 = (int)SEQ_PTR(_hit_counts_68397);
        _34541 = (int)*(((s1_ptr)_2)->base + _i_68416);
        Ref(_34541);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _34540;
        ((int *)_2)[2] = _34541;
        _34542 = MAKE_SEQ(_1);
        _34541 = NOVALUE;
        _34540 = NOVALUE;
        _2 = (int)SEQ_PTR(_hit_counts_68397);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _hit_counts_68397 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_68416);
        _1 = *(int *)_2;
        *(int *)_2 = _34542;
        if( _1 != _34542 ){
            DeRef(_1);
        }
        _34542 = NOVALUE;

        /** 		end for*/
        _i_68416 = _i_68416 + 1;
        goto L26; // [1267] 1247
L27: 
        ;
    }

    /** 		puts( st, "\nBucket search frequency counts (hits : # buckets):\n" )*/
    EPuts(_st_68132, _34543); // DJP 

    /** 		for i = length( hit_counts ) to 1 by -1 do*/
    if (IS_SEQUENCE(_hit_counts_68397)){
            _34544 = SEQ_PTR(_hit_counts_68397)->length;
    }
    else {
        _34544 = 1;
    }
    {
        int _i_68423;
        _i_68423 = _34544;
L28: 
        if (_i_68423 < 1){
            goto L29; // [1282] 1320
        }

        /** 			if hit_counts[i][2] then*/
        _2 = (int)SEQ_PTR(_hit_counts_68397);
        _34545 = (int)*(((s1_ptr)_2)->base + _i_68423);
        _2 = (int)SEQ_PTR(_34545);
        _34546 = (int)*(((s1_ptr)_2)->base + 2);
        _34545 = NOVALUE;
        if (_34546 == 0) {
            _34546 = NOVALUE;
            goto L2A; // [1299] 1313
        }
        else {
            if (!IS_ATOM_INT(_34546) && DBL_PTR(_34546)->dbl == 0.0){
                _34546 = NOVALUE;
                goto L2A; // [1299] 1313
            }
            _34546 = NOVALUE;
        }
        _34546 = NOVALUE;

        /** 				printf( st, "%6d: %d\n", hit_counts[i]  )*/
        _2 = (int)SEQ_PTR(_hit_counts_68397);
        _34548 = (int)*(((s1_ptr)_2)->base + _i_68423);
        EPrintf(_st_68132, _34547, _34548);
        _34548 = NOVALUE;
L2A: 

        /** 		end for*/
        _i_68423 = _i_68423 + -1;
        goto L28; // [1315] 1289
L29: 
        ;
    }
L20: 
    DeRef(_hit_counts_68397);
    _hit_counts_68397 = NOVALUE;

    /** 	close( st )*/
    EClose(_st_68132);

    /** end procedure*/
    DeRefDS(_name_68131);
    DeRefi(_line_format_68134);
    DeRef(_pretty_options_68135);
    DeRef(_bucket_usage_68139);
    DeRef(_bucket_68244);
    DeRef(_end_size_68248);
    DeRef(_bucket_reps_68252);
    DeRef(_34378);
    _34378 = NOVALUE;
    _34393 = NOVALUE;
    _34396 = NOVALUE;
    _34416 = NOVALUE;
    _34437 = NOVALUE;
    DeRef(_34439);
    _34439 = NOVALUE;
    _34461 = NOVALUE;
    _34490 = NOVALUE;
    _34499 = NOVALUE;
    return;
    ;
}


void _70InitBackEnd(int _ignore_68432)
{
    int _name_68433 = NOVALUE;
    int _missing_68434 = NOVALUE;
    int _34665 = NOVALUE;
    int _34662 = NOVALUE;
    int _34661 = NOVALUE;
    int _34660 = NOVALUE;
    int _34659 = NOVALUE;
    int _34656 = NOVALUE;
    int _34655 = NOVALUE;
    int _34654 = NOVALUE;
    int _34651 = NOVALUE;
    int _34648 = NOVALUE;
    int _34647 = NOVALUE;
    int _34642 = NOVALUE;
    int _34641 = NOVALUE;
    int _34639 = NOVALUE;
    int _34637 = NOVALUE;
    int _34634 = NOVALUE;
    int _34631 = NOVALUE;
    int _34628 = NOVALUE;
    int _34625 = NOVALUE;
    int _34622 = NOVALUE;
    int _34619 = NOVALUE;
    int _34616 = NOVALUE;
    int _34615 = NOVALUE;
    int _34607 = NOVALUE;
    int _34604 = NOVALUE;
    int _34603 = NOVALUE;
    int _34598 = NOVALUE;
    int _34595 = NOVALUE;
    int _34594 = NOVALUE;
    int _34587 = NOVALUE;
    int _34584 = NOVALUE;
    int _34581 = NOVALUE;
    int _34578 = NOVALUE;
    int _34575 = NOVALUE;
    int _34572 = NOVALUE;
    int _34571 = NOVALUE;
    int _34565 = NOVALUE;
    int _34562 = NOVALUE;
    int _34561 = NOVALUE;
    int _34557 = NOVALUE;
    int _34556 = NOVALUE;
    int _34552 = NOVALUE;
    int _34549 = NOVALUE;
    int _0, _1, _2;
    

    /**     sequence missing = {}*/
    RefDS(_22682);
    DeRef(_missing_68434);
    _missing_68434 = _22682;

    /**     operation = repeat(-1, length(opnames))*/
    _34549 = 211;
    DeRefi(_70operation_65971);
    _70operation_65971 = Repeat(-1, 211);
    _34549 = NOVALUE;

    /** 	if not TRANSLATE then*/
    if (_25TRANSLATE_11874 != 0)
    goto L1; // [23] 33

    /** 		intoptions()*/
    _68intoptions();
    goto L2; // [30] 38
L1: 

    /** 		transoptions()*/
    _69transoptions();
L2: 

    /** 	for i = 1 to length(opnames) do*/
    _34552 = 211;
    {
        int _i_68445;
        _i_68445 = 1;
L3: 
        if (_i_68445 > 211){
            goto L4; // [45] 637
        }

        /** 		name = opnames[i]*/
        DeRef(_name_68433);
        _2 = (int)SEQ_PTR(_59opnames_22728);
        _name_68433 = (int)*(((s1_ptr)_2)->base + _i_68445);
        RefDS(_name_68433);

        /** 		if find(name, {"RHS_SUBS_CHECK", "RHS_SUBS_I"}) then*/
        RefDS(_34555);
        RefDS(_34554);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _34554;
        ((int *)_2)[2] = _34555;
        _34556 = MAKE_SEQ(_1);
        _34557 = find_from(_name_68433, _34556, 1);
        DeRefDS(_34556);
        _34556 = NOVALUE;
        if (_34557 == 0)
        {
            _34557 = NOVALUE;
            goto L5; // [73] 86
        }
        else{
            _34557 = NOVALUE;
        }

        /** 			name = "RHS_SUBS"*/
        RefDS(_34558);
        DeRefDS(_name_68433);
        _name_68433 = _34558;
        goto L6; // [83] 594
L5: 

        /** 		elsif find(name, {"ASSIGN_SUBS_CHECK", "ASSIGN_SUBS_I"}) then*/
        RefDS(_34560);
        RefDS(_34559);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _34559;
        ((int *)_2)[2] = _34560;
        _34561 = MAKE_SEQ(_1);
        _34562 = find_from(_name_68433, _34561, 1);
        DeRefDS(_34561);
        _34561 = NOVALUE;
        if (_34562 == 0)
        {
            _34562 = NOVALUE;
            goto L7; // [97] 110
        }
        else{
            _34562 = NOVALUE;
        }

        /** 			name = "ASSIGN_SUBS"*/
        RefDS(_34563);
        DeRefDS(_name_68433);
        _name_68433 = _34563;
        goto L6; // [107] 594
L7: 

        /** 		elsif equal(name, "ASSIGN_I") then*/
        if (_name_68433 == _34564)
        _34565 = 1;
        else if (IS_ATOM_INT(_name_68433) && IS_ATOM_INT(_34564))
        _34565 = 0;
        else
        _34565 = (compare(_name_68433, _34564) == 0);
        if (_34565 == 0)
        {
            _34565 = NOVALUE;
            goto L8; // [116] 129
        }
        else{
            _34565 = NOVALUE;
        }

        /** 			name = "ASSIGN"*/
        RefDS(_34566);
        DeRefDS(_name_68433);
        _name_68433 = _34566;
        goto L6; // [126] 594
L8: 

        /** 		elsif find(name, {"EXIT", "ENDWHILE", "RETRY", "GOTO"}) then*/
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_34567);
        *((int *)(_2+4)) = _34567;
        RefDS(_34568);
        *((int *)(_2+8)) = _34568;
        RefDS(_34569);
        *((int *)(_2+12)) = _34569;
        RefDS(_34570);
        *((int *)(_2+16)) = _34570;
        _34571 = MAKE_SEQ(_1);
        _34572 = find_from(_name_68433, _34571, 1);
        DeRefDS(_34571);
        _34571 = NOVALUE;
        if (_34572 == 0)
        {
            _34572 = NOVALUE;
            goto L9; // [143] 156
        }
        else{
            _34572 = NOVALUE;
        }

        /** 			name = "ELSE"*/
        RefDS(_34573);
        DeRefDS(_name_68433);
        _name_68433 = _34573;
        goto L6; // [153] 594
L9: 

        /** 		elsif equal(name, "PLUS1_I") then*/
        if (_name_68433 == _34574)
        _34575 = 1;
        else if (IS_ATOM_INT(_name_68433) && IS_ATOM_INT(_34574))
        _34575 = 0;
        else
        _34575 = (compare(_name_68433, _34574) == 0);
        if (_34575 == 0)
        {
            _34575 = NOVALUE;
            goto LA; // [162] 175
        }
        else{
            _34575 = NOVALUE;
        }

        /** 			name = "PLUS1"*/
        RefDS(_34576);
        DeRefDS(_name_68433);
        _name_68433 = _34576;
        goto L6; // [172] 594
LA: 

        /** 		elsif equal(name, "PRIVATE_INIT_CHECK") then*/
        if (_name_68433 == _34577)
        _34578 = 1;
        else if (IS_ATOM_INT(_name_68433) && IS_ATOM_INT(_34577))
        _34578 = 0;
        else
        _34578 = (compare(_name_68433, _34577) == 0);
        if (_34578 == 0)
        {
            _34578 = NOVALUE;
            goto LB; // [181] 194
        }
        else{
            _34578 = NOVALUE;
        }

        /** 			name = "GLOBAL_INIT_CHECK"*/
        RefDS(_34579);
        DeRefDS(_name_68433);
        _name_68433 = _34579;
        goto L6; // [191] 594
LB: 

        /** 		elsif equal(name, "PLUS_I") then*/
        if (_name_68433 == _34580)
        _34581 = 1;
        else if (IS_ATOM_INT(_name_68433) && IS_ATOM_INT(_34580))
        _34581 = 0;
        else
        _34581 = (compare(_name_68433, _34580) == 0);
        if (_34581 == 0)
        {
            _34581 = NOVALUE;
            goto LC; // [200] 213
        }
        else{
            _34581 = NOVALUE;
        }

        /** 			name = "PLUS"*/
        RefDS(_34582);
        DeRefDS(_name_68433);
        _name_68433 = _34582;
        goto L6; // [210] 594
LC: 

        /** 		elsif equal(name, "MINUS_I") then*/
        if (_name_68433 == _34583)
        _34584 = 1;
        else if (IS_ATOM_INT(_name_68433) && IS_ATOM_INT(_34583))
        _34584 = 0;
        else
        _34584 = (compare(_name_68433, _34583) == 0);
        if (_34584 == 0)
        {
            _34584 = NOVALUE;
            goto LD; // [219] 232
        }
        else{
            _34584 = NOVALUE;
        }

        /** 			name = "MINUS"*/
        RefDS(_34585);
        DeRefDS(_name_68433);
        _name_68433 = _34585;
        goto L6; // [229] 594
LD: 

        /** 		elsif equal(name, "FOR_I") then*/
        if (_name_68433 == _34586)
        _34587 = 1;
        else if (IS_ATOM_INT(_name_68433) && IS_ATOM_INT(_34586))
        _34587 = 0;
        else
        _34587 = (compare(_name_68433, _34586) == 0);
        if (_34587 == 0)
        {
            _34587 = NOVALUE;
            goto LE; // [238] 251
        }
        else{
            _34587 = NOVALUE;
        }

        /** 			name = "FOR"*/
        RefDS(_34588);
        DeRefDS(_name_68433);
        _name_68433 = _34588;
        goto L6; // [248] 594
LE: 

        /** 		elsif find(name, {"ENDFOR_UP", "ENDFOR_DOWN",*/
        _1 = NewS1(5);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_34589);
        *((int *)(_2+4)) = _34589;
        RefDS(_34590);
        *((int *)(_2+8)) = _34590;
        RefDS(_34591);
        *((int *)(_2+12)) = _34591;
        RefDS(_34592);
        *((int *)(_2+16)) = _34592;
        RefDS(_34593);
        *((int *)(_2+20)) = _34593;
        _34594 = MAKE_SEQ(_1);
        _34595 = find_from(_name_68433, _34594, 1);
        DeRefDS(_34594);
        _34594 = NOVALUE;
        if (_34595 == 0)
        {
            _34595 = NOVALUE;
            goto LF; // [266] 279
        }
        else{
            _34595 = NOVALUE;
        }

        /** 			name = "ENDFOR_GENERAL"*/
        RefDS(_34596);
        DeRefDS(_name_68433);
        _name_68433 = _34596;
        goto L6; // [276] 594
LF: 

        /** 		elsif equal(name, "CALL_FUNC") then*/
        if (_name_68433 == _34597)
        _34598 = 1;
        else if (IS_ATOM_INT(_name_68433) && IS_ATOM_INT(_34597))
        _34598 = 0;
        else
        _34598 = (compare(_name_68433, _34597) == 0);
        if (_34598 == 0)
        {
            _34598 = NOVALUE;
            goto L10; // [285] 298
        }
        else{
            _34598 = NOVALUE;
        }

        /** 			name = "CALL_PROC"*/
        RefDS(_34599);
        DeRefDS(_name_68433);
        _name_68433 = _34599;
        goto L6; // [295] 594
L10: 

        /** 		elsif find(name, {"DISPLAY_VAR", "ERASE_PRIVATE_NAMES",*/
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_34600);
        *((int *)(_2+4)) = _34600;
        RefDS(_34601);
        *((int *)(_2+8)) = _34601;
        RefDS(_34602);
        *((int *)(_2+12)) = _34602;
        _34603 = MAKE_SEQ(_1);
        _34604 = find_from(_name_68433, _34603, 1);
        DeRefDS(_34603);
        _34603 = NOVALUE;
        if (_34604 == 0)
        {
            _34604 = NOVALUE;
            goto L11; // [311] 324
        }
        else{
            _34604 = NOVALUE;
        }

        /** 			name = "PROFILE"*/
        RefDS(_34605);
        DeRefDS(_name_68433);
        _name_68433 = _34605;
        goto L6; // [321] 594
L11: 

        /** 		elsif equal(name, "SC2_AND") then*/
        if (_name_68433 == _34606)
        _34607 = 1;
        else if (IS_ATOM_INT(_name_68433) && IS_ATOM_INT(_34606))
        _34607 = 0;
        else
        _34607 = (compare(_name_68433, _34606) == 0);
        if (_34607 == 0)
        {
            _34607 = NOVALUE;
            goto L12; // [330] 343
        }
        else{
            _34607 = NOVALUE;
        }

        /** 			name = "SC2_OR"*/
        RefDS(_34608);
        DeRefDS(_name_68433);
        _name_68433 = _34608;
        goto L6; // [340] 594
L12: 

        /** 		elsif find(name, {"SC2_NULL", "ASSIGN_SUBS2", "PLATFORM",*/
        _1 = NewS1(6);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_34609);
        *((int *)(_2+4)) = _34609;
        RefDS(_34610);
        *((int *)(_2+8)) = _34610;
        RefDS(_34611);
        *((int *)(_2+12)) = _34611;
        RefDS(_34612);
        *((int *)(_2+16)) = _34612;
        RefDS(_34613);
        *((int *)(_2+20)) = _34613;
        RefDS(_34614);
        *((int *)(_2+24)) = _34614;
        _34615 = MAKE_SEQ(_1);
        _34616 = find_from(_name_68433, _34615, 1);
        DeRefDS(_34615);
        _34615 = NOVALUE;
        if (_34616 == 0)
        {
            _34616 = NOVALUE;
            goto L13; // [359] 372
        }
        else{
            _34616 = NOVALUE;
        }

        /** 			name = "NOP2"*/
        RefDS(_34617);
        DeRefDS(_name_68433);
        _name_68433 = _34617;
        goto L6; // [369] 594
L13: 

        /** 		elsif equal(name, "GREATER_IFW_I") then*/
        if (_name_68433 == _34618)
        _34619 = 1;
        else if (IS_ATOM_INT(_name_68433) && IS_ATOM_INT(_34618))
        _34619 = 0;
        else
        _34619 = (compare(_name_68433, _34618) == 0);
        if (_34619 == 0)
        {
            _34619 = NOVALUE;
            goto L14; // [378] 391
        }
        else{
            _34619 = NOVALUE;
        }

        /** 			name = "GREATER_IFW"*/
        RefDS(_34620);
        DeRefDS(_name_68433);
        _name_68433 = _34620;
        goto L6; // [388] 594
L14: 

        /** 		elsif equal(name, "LESS_IFW_I") then*/
        if (_name_68433 == _34621)
        _34622 = 1;
        else if (IS_ATOM_INT(_name_68433) && IS_ATOM_INT(_34621))
        _34622 = 0;
        else
        _34622 = (compare(_name_68433, _34621) == 0);
        if (_34622 == 0)
        {
            _34622 = NOVALUE;
            goto L15; // [397] 410
        }
        else{
            _34622 = NOVALUE;
        }

        /** 			name = "LESS_IFW"*/
        RefDS(_34623);
        DeRefDS(_name_68433);
        _name_68433 = _34623;
        goto L6; // [407] 594
L15: 

        /** 		elsif equal(name, "EQUALS_IFW_I") then*/
        if (_name_68433 == _34624)
        _34625 = 1;
        else if (IS_ATOM_INT(_name_68433) && IS_ATOM_INT(_34624))
        _34625 = 0;
        else
        _34625 = (compare(_name_68433, _34624) == 0);
        if (_34625 == 0)
        {
            _34625 = NOVALUE;
            goto L16; // [416] 429
        }
        else{
            _34625 = NOVALUE;
        }

        /** 			name = "EQUALS_IFW"*/
        RefDS(_34626);
        DeRefDS(_name_68433);
        _name_68433 = _34626;
        goto L6; // [426] 594
L16: 

        /** 		elsif equal(name, "NOTEQ_IFW_I") then*/
        if (_name_68433 == _34627)
        _34628 = 1;
        else if (IS_ATOM_INT(_name_68433) && IS_ATOM_INT(_34627))
        _34628 = 0;
        else
        _34628 = (compare(_name_68433, _34627) == 0);
        if (_34628 == 0)
        {
            _34628 = NOVALUE;
            goto L17; // [435] 448
        }
        else{
            _34628 = NOVALUE;
        }

        /** 			name = "NOTEQ_IFW"*/
        RefDS(_34629);
        DeRefDS(_name_68433);
        _name_68433 = _34629;
        goto L6; // [445] 594
L17: 

        /** 		elsif equal(name, "GREATEREQ_IFW_I") then*/
        if (_name_68433 == _34630)
        _34631 = 1;
        else if (IS_ATOM_INT(_name_68433) && IS_ATOM_INT(_34630))
        _34631 = 0;
        else
        _34631 = (compare(_name_68433, _34630) == 0);
        if (_34631 == 0)
        {
            _34631 = NOVALUE;
            goto L18; // [454] 467
        }
        else{
            _34631 = NOVALUE;
        }

        /** 			name = "GREATEREQ_IFW"*/
        RefDS(_34632);
        DeRefDS(_name_68433);
        _name_68433 = _34632;
        goto L6; // [464] 594
L18: 

        /** 		elsif equal(name, "LESSEQ_IFW_I") then*/
        if (_name_68433 == _34633)
        _34634 = 1;
        else if (IS_ATOM_INT(_name_68433) && IS_ATOM_INT(_34633))
        _34634 = 0;
        else
        _34634 = (compare(_name_68433, _34633) == 0);
        if (_34634 == 0)
        {
            _34634 = NOVALUE;
            goto L19; // [473] 486
        }
        else{
            _34634 = NOVALUE;
        }

        /** 			name = "LESSEQ_IFW"*/
        RefDS(_34635);
        DeRefDS(_name_68433);
        _name_68433 = _34635;
        goto L6; // [483] 594
L19: 

        /** 		elsif match( "PEEK", name ) then*/
        _34637 = e_match_from(_34636, _name_68433, 1);
        if (_34637 == 0)
        {
            _34637 = NOVALUE;
            goto L1A; // [493] 506
        }
        else{
            _34637 = NOVALUE;
        }

        /** 			name = "PEEK"*/
        RefDS(_34636);
        DeRefDS(_name_68433);
        _name_68433 = _34636;
        goto L6; // [503] 594
L1A: 

        /** 		elsif match( "POKE", name ) then*/
        _34639 = e_match_from(_34638, _name_68433, 1);
        if (_34639 == 0)
        {
            _34639 = NOVALUE;
            goto L1B; // [513] 526
        }
        else{
            _34639 = NOVALUE;
        }

        /** 			name = "POKE"*/
        RefDS(_34638);
        DeRefDS(_name_68433);
        _name_68433 = _34638;
        goto L6; // [523] 594
L1B: 

        /** 		elsif find( name, { "NOPWHILE" } ) then*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_34640);
        *((int *)(_2+4)) = _34640;
        _34641 = MAKE_SEQ(_1);
        _34642 = find_from(_name_68433, _34641, 1);
        DeRefDS(_34641);
        _34641 = NOVALUE;
        if (_34642 == 0)
        {
            _34642 = NOVALUE;
            goto L1C; // [537] 550
        }
        else{
            _34642 = NOVALUE;
        }

        /** 			name = "NOP1"*/
        RefDS(_34643);
        DeRefDS(_name_68433);
        _name_68433 = _34643;
        goto L6; // [547] 594
L1C: 

        /** 		elsif find( name, { "SWITCH_I", "SWITCH_SPI", "SWITCH_RT" }) then*/
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_34644);
        *((int *)(_2+4)) = _34644;
        RefDS(_34645);
        *((int *)(_2+8)) = _34645;
        RefDS(_34646);
        *((int *)(_2+12)) = _34646;
        _34647 = MAKE_SEQ(_1);
        _34648 = find_from(_name_68433, _34647, 1);
        DeRefDS(_34647);
        _34647 = NOVALUE;
        if (_34648 == 0)
        {
            _34648 = NOVALUE;
            goto L1D; // [563] 576
        }
        else{
            _34648 = NOVALUE;
        }

        /** 			name = "SWITCH"*/
        RefDS(_34649);
        DeRefDS(_name_68433);
        _name_68433 = _34649;
        goto L6; // [573] 594
L1D: 

        /** 		elsif equal( name, "PROC_TAIL" ) then*/
        if (_name_68433 == _34650)
        _34651 = 1;
        else if (IS_ATOM_INT(_name_68433) && IS_ATOM_INT(_34650))
        _34651 = 0;
        else
        _34651 = (compare(_name_68433, _34650) == 0);
        if (_34651 == 0)
        {
            _34651 = NOVALUE;
            goto L1E; // [582] 593
        }
        else{
            _34651 = NOVALUE;
        }

        /** 			name = "PROC"*/
        RefDS(_34652);
        DeRefDS(_name_68433);
        _name_68433 = _34652;
L1E: 
L6: 

        /** 		operation[i] = routine_id("op" & name)*/
        Concat((object_ptr)&_34654, _34653, _name_68433);
        _34655 = CRoutineId(1592, 70, _34654);
        _2 = (int)SEQ_PTR(_70operation_65971);
        _2 = (int)(((s1_ptr)_2)->base + _i_68445);
        *(int *)_2 = _34655;
        if( _1 != _34655 ){
        }
        _34655 = NOVALUE;

        /** 		if operation[i] = -1 then*/
        _2 = (int)SEQ_PTR(_70operation_65971);
        _34656 = (int)*(((s1_ptr)_2)->base + _i_68445);
        if (_34656 != -1)
        goto L1F; // [619] 630

        /** 			missing = append( missing, name )*/
        RefDS(_name_68433);
        Append(&_missing_68434, _missing_68434, _name_68433);
L1F: 

        /**     end for*/
        _i_68445 = _i_68445 + 1;
        goto L3; // [632] 52
L4: 
        ;
    }

    /**     if length( missing ) then*/
    if (IS_SEQUENCE(_missing_68434)){
            _34659 = SEQ_PTR(_missing_68434)->length;
    }
    else {
        _34659 = 1;
    }
    if (_34659 == 0)
    {
        _34659 = NOVALUE;
        goto L20; // [642] 696
    }
    else{
        _34659 = NOVALUE;
    }

    /**     	name = ""*/
    RefDS(_22682);
    DeRef(_name_68433);
    _name_68433 = _22682;

    /** 		for i = 1 to length( missing ) do*/
    if (IS_SEQUENCE(_missing_68434)){
            _34660 = SEQ_PTR(_missing_68434)->length;
    }
    else {
        _34660 = 1;
    }
    {
        int _i_68584;
        _i_68584 = 1;
L21: 
        if (_i_68584 > _34660){
            goto L22; // [657] 685
        }

        /** 			name &= missing[i] & ' '*/
        _2 = (int)SEQ_PTR(_missing_68434);
        _34661 = (int)*(((s1_ptr)_2)->base + _i_68584);
        Append(&_34662, _34661, 32);
        _34661 = NOVALUE;
        Concat((object_ptr)&_name_68433, _name_68433, _34662);
        DeRefDS(_34662);
        _34662 = NOVALUE;

        /** 		end for*/
        _i_68584 = _i_68584 + 1;
        goto L21; // [680] 664
L22: 
        ;
    }

    /** 		crash( "No routine id for ops: " & name )*/
    Concat((object_ptr)&_34665, _34664, _name_68433);
    RefDS(_22682);
    _8crash(_34665, _22682);
    _34665 = NOVALUE;
L20: 

    /** end procedure*/
    DeRef(_name_68433);
    DeRef(_missing_68434);
    DeRef(_34654);
    _34654 = NOVALUE;
    _34656 = NOVALUE;
    return;
    ;
}


int _70max(int _a_68597, int _b_68598)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_a_68597)) {
        _1 = (long)(DBL_PTR(_a_68597)->dbl);
        if (UNIQUE(DBL_PTR(_a_68597)) && (DBL_PTR(_a_68597)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_a_68597);
        _a_68597 = _1;
    }
    if (!IS_ATOM_INT(_b_68598)) {
        _1 = (long)(DBL_PTR(_b_68598)->dbl);
        if (UNIQUE(DBL_PTR(_b_68598)) && (DBL_PTR(_b_68598)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_b_68598);
        _b_68598 = _1;
    }

    /** 	if a > b then*/
    if (_a_68597 <= _b_68598)
    goto L1; // [7] 18

    /** 		return a*/
    return _a_68597;
L1: 

    /** 	return b*/
    return _b_68598;
    ;
}


void _70dis(int _sub_68603)
{
    int _op_68604 = NOVALUE;
    int _ix_68605 = NOVALUE;
    int _sym_68606 = NOVALUE;
    int _param_68609 = NOVALUE;
    int _params_68614 = NOVALUE;
    int _stack_space_required_68645 = NOVALUE;
    int _line_table_68686 = NOVALUE;
    int _ln_68695 = NOVALUE;
    int _34725 = NOVALUE;
    int _34724 = NOVALUE;
    int _34723 = NOVALUE;
    int _34721 = NOVALUE;
    int _34719 = NOVALUE;
    int _34717 = NOVALUE;
    int _34716 = NOVALUE;
    int _34715 = NOVALUE;
    int _34714 = NOVALUE;
    int _34712 = NOVALUE;
    int _34710 = NOVALUE;
    int _34708 = NOVALUE;
    int _34706 = NOVALUE;
    int _34705 = NOVALUE;
    int _34704 = NOVALUE;
    int _34703 = NOVALUE;
    int _34702 = NOVALUE;
    int _34701 = NOVALUE;
    int _34700 = NOVALUE;
    int _34699 = NOVALUE;
    int _34698 = NOVALUE;
    int _34697 = NOVALUE;
    int _34696 = NOVALUE;
    int _34693 = NOVALUE;
    int _34692 = NOVALUE;
    int _34691 = NOVALUE;
    int _34690 = NOVALUE;
    int _34689 = NOVALUE;
    int _34685 = NOVALUE;
    int _34684 = NOVALUE;
    int _34683 = NOVALUE;
    int _34682 = NOVALUE;
    int _34681 = NOVALUE;
    int _34680 = NOVALUE;
    int _34677 = NOVALUE;
    int _34675 = NOVALUE;
    int _34674 = NOVALUE;
    int _34673 = NOVALUE;
    int _34671 = NOVALUE;
    int _34670 = NOVALUE;
    int _34668 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sub_68603)) {
        _1 = (long)(DBL_PTR(_sub_68603)->dbl);
        if (UNIQUE(DBL_PTR(_sub_68603)) && (DBL_PTR(_sub_68603)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sub_68603);
        _sub_68603 = _1;
    }

    /** 	CurrentSub = sub*/
    _25CurrentSub_12270 = _sub_68603;

    /** 	symtab_index param = SymTab[sub][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _34668 = (int)*(((s1_ptr)_2)->base + _sub_68603);
    _2 = (int)SEQ_PTR(_34668);
    _param_68609 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_param_68609)){
        _param_68609 = (long)DBL_PTR(_param_68609)->dbl;
    }
    _34668 = NOVALUE;

    /** 	sequence params = {}*/
    RefDS(_22682);
    DeRefi(_params_68614);
    _params_68614 = _22682;

    /** 	for p = 1 to SymTab[sub][S_NUM_ARGS] do*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _34670 = (int)*(((s1_ptr)_2)->base + _sub_68603);
    _2 = (int)SEQ_PTR(_34670);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _34671 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _34671 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    _34670 = NOVALUE;
    {
        int _p_68616;
        _p_68616 = 1;
L1: 
        if (binary_op_a(GREATER, _p_68616, _34671)){
            goto L2; // [47] 95
        }

        /** 		params &= sprintf( " %s", names({param}) )*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _param_68609;
        _34673 = MAKE_SEQ(_1);
        _34674 = _70names(_34673);
        _34673 = NOVALUE;
        _34675 = EPrintf(-9999999, _34672, _34674);
        DeRef(_34674);
        _34674 = NOVALUE;
        Concat((object_ptr)&_params_68614, _params_68614, _34675);
        DeRefDS(_34675);
        _34675 = NOVALUE;

        /** 		param = SymTab[param][S_NEXT]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _34677 = (int)*(((s1_ptr)_2)->base + _param_68609);
        _2 = (int)SEQ_PTR(_34677);
        _param_68609 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_param_68609)){
            _param_68609 = (long)DBL_PTR(_param_68609)->dbl;
        }
        _34677 = NOVALUE;

        /** 	end for*/
        _0 = _p_68616;
        if (IS_ATOM_INT(_p_68616)) {
            _p_68616 = _p_68616 + 1;
            if ((long)((unsigned long)_p_68616 +(unsigned long) HIGH_BITS) >= 0){
                _p_68616 = NewDouble((double)_p_68616);
            }
        }
        else {
            _p_68616 = binary_op_a(PLUS, _p_68616, 1);
        }
        DeRef(_0);
        goto L1; // [90] 54
L2: 
        ;
        DeRef(_p_68616);
    }

    /** 	printf( out, "\nSubProgram [%s-%s:%05d] %s\n",*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _34680 = (int)*(((s1_ptr)_2)->base + _sub_68603);
    _2 = (int)SEQ_PTR(_34680);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _34681 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _34681 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _34680 = NOVALUE;
    _2 = (int)SEQ_PTR(_26known_files_11139);
    if (!IS_ATOM_INT(_34681)){
        _34682 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_34681)->dbl));
    }
    else{
        _34682 = (int)*(((s1_ptr)_2)->base + _34681);
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _34683 = (int)*(((s1_ptr)_2)->base + _sub_68603);
    _2 = (int)SEQ_PTR(_34683);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _34684 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _34684 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _34683 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_34682);
    *((int *)(_2+4)) = _34682;
    Ref(_34684);
    *((int *)(_2+8)) = _34684;
    *((int *)(_2+12)) = _sub_68603;
    RefDS(_params_68614);
    *((int *)(_2+16)) = _params_68614;
    _34685 = MAKE_SEQ(_1);
    _34684 = NOVALUE;
    _34682 = NOVALUE;
    EPrintf(_70out_65962, _34679, _34685);
    DeRefDS(_34685);
    _34685 = NOVALUE;

    /** 	if sub != TopLevelSub then*/
    if (_sub_68603 == _25TopLevelSub_12269)
    goto L3; // [144] 251

    /** 		integer stack_space_required = calc_stack_required( sub )*/
    _stack_space_required_68645 = _52calc_stack_required(_sub_68603);
    if (!IS_ATOM_INT(_stack_space_required_68645)) {
        _1 = (long)(DBL_PTR(_stack_space_required_68645)->dbl);
        if (UNIQUE(DBL_PTR(_stack_space_required_68645)) && (DBL_PTR(_stack_space_required_68645)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_stack_space_required_68645);
        _stack_space_required_68645 = _1;
    }

    /** 		printf( out, "\tSTACK SPACE: %d\n\tRequired:    %d\n", { SymTab[sub][S_STACK_SPACE], stack_space_required } )*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _34689 = (int)*(((s1_ptr)_2)->base + _sub_68603);
    _2 = (int)SEQ_PTR(_34689);
    if (!IS_ATOM_INT(_25S_STACK_SPACE_11973)){
        _34690 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_STACK_SPACE_11973)->dbl));
    }
    else{
        _34690 = (int)*(((s1_ptr)_2)->base + _25S_STACK_SPACE_11973);
    }
    _34689 = NOVALUE;
    Ref(_34690);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _34690;
    ((int *)_2)[2] = _stack_space_required_68645;
    _34691 = MAKE_SEQ(_1);
    _34690 = NOVALUE;
    EPrintf(_70out_65962, _34688, _34691);
    DeRefDS(_34691);
    _34691 = NOVALUE;

    /** 		if stack_space_required > SymTab[sub][S_STACK_SPACE] then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _34692 = (int)*(((s1_ptr)_2)->base + _sub_68603);
    _2 = (int)SEQ_PTR(_34692);
    if (!IS_ATOM_INT(_25S_STACK_SPACE_11973)){
        _34693 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_STACK_SPACE_11973)->dbl));
    }
    else{
        _34693 = (int)*(((s1_ptr)_2)->base + _25S_STACK_SPACE_11973);
    }
    _34692 = NOVALUE;
    if (binary_op_a(LESSEQ, _stack_space_required_68645, _34693)){
        _34693 = NOVALUE;
        goto L4; // [194] 250
    }
    _34693 = NOVALUE;

    /** 			printf(2, "Stack space mismatch! %s:%s Reserved[%d] Required[%d]\n", */
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _34696 = (int)*(((s1_ptr)_2)->base + _sub_68603);
    _2 = (int)SEQ_PTR(_34696);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _34697 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _34697 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _34696 = NOVALUE;
    _2 = (int)SEQ_PTR(_26known_files_11139);
    if (!IS_ATOM_INT(_34697)){
        _34698 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_34697)->dbl));
    }
    else{
        _34698 = (int)*(((s1_ptr)_2)->base + _34697);
    }
    Ref(_34698);
    _34699 = _9filename(_34698);
    _34698 = NOVALUE;
    _34700 = _52sym_name(_sub_68603);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _34701 = (int)*(((s1_ptr)_2)->base + _sub_68603);
    _2 = (int)SEQ_PTR(_34701);
    if (!IS_ATOM_INT(_25S_STACK_SPACE_11973)){
        _34702 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_STACK_SPACE_11973)->dbl));
    }
    else{
        _34702 = (int)*(((s1_ptr)_2)->base + _25S_STACK_SPACE_11973);
    }
    _34701 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _34699;
    *((int *)(_2+8)) = _34700;
    Ref(_34702);
    *((int *)(_2+12)) = _34702;
    *((int *)(_2+16)) = _stack_space_required_68645;
    _34703 = MAKE_SEQ(_1);
    _34702 = NOVALUE;
    _34700 = NOVALUE;
    _34699 = NOVALUE;
    EPrintf(2, _34695, _34703);
    DeRefDS(_34703);
    _34703 = NOVALUE;
L4: 
L3: 

    /** 	map:put( proc_names, SymTab[sub][S_NAME], sub )*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _34704 = (int)*(((s1_ptr)_2)->base + _sub_68603);
    _2 = (int)SEQ_PTR(_34704);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _34705 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _34705 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _34704 = NOVALUE;
    Ref(_71proc_names_65013);
    Ref(_34705);
    _32put(_71proc_names_65013, _34705, _sub_68603, 1, _32threshold_size_13318);
    _34705 = NOVALUE;

    /** 	Code = SymTab[sub][S_CODE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _34706 = (int)*(((s1_ptr)_2)->base + _sub_68603);
    DeRef(_25Code_12355);
    _2 = (int)SEQ_PTR(_34706);
    if (!IS_ATOM_INT(_25S_CODE_11925)){
        _25Code_12355 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    }
    else{
        _25Code_12355 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
    }
    Ref(_25Code_12355);
    _34706 = NOVALUE;

    /** 	pc = 1*/
    _70pc_65963 = 1;

    /** 	sequence line_table = SymTab[sub][S_LINETAB]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _34708 = (int)*(((s1_ptr)_2)->base + _sub_68603);
    DeRef(_line_table_68686);
    _2 = (int)SEQ_PTR(_34708);
    if (!IS_ATOM_INT(_25S_LINETAB_11948)){
        _line_table_68686 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_LINETAB_11948)->dbl));
    }
    else{
        _line_table_68686 = (int)*(((s1_ptr)_2)->base + _25S_LINETAB_11948);
    }
    Ref(_line_table_68686);
    _34708 = NOVALUE;

    /** 	while pc <= length(Code) do*/
L5: 
    if (IS_SEQUENCE(_25Code_12355)){
            _34710 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _34710 = 1;
    }
    if (_70pc_65963 > _34710)
    goto L6; // [329] 414

    /** 		integer ln = find( pc-1, line_table )*/
    _34712 = _70pc_65963 - 1;
    if ((long)((unsigned long)_34712 +(unsigned long) HIGH_BITS) >= 0){
        _34712 = NewDouble((double)_34712);
    }
    _ln_68695 = find_from(_34712, _line_table_68686, 1);
    DeRef(_34712);
    _34712 = NOVALUE;

    /** 		if ln > 0 and ln <= length(line_table) then*/
    _34714 = (_ln_68695 > 0);
    if (_34714 == 0) {
        goto L7; // [352] 384
    }
    if (IS_SEQUENCE(_line_table_68686)){
            _34716 = SEQ_PTR(_line_table_68686)->length;
    }
    else {
        _34716 = 1;
    }
    _34717 = (_ln_68695 <= _34716);
    _34716 = NOVALUE;
    if (_34717 == 0)
    {
        DeRef(_34717);
        _34717 = NOVALUE;
        goto L7; // [364] 384
    }
    else{
        DeRef(_34717);
        _34717 = NOVALUE;
    }

    /** 			printf(out, "\n        [%s:%d] %s (%d)\n", find_line( sub, pc, 0 ) )*/
    _34719 = _70find_line(_sub_68603, _70pc_65963, 0);
    EPrintf(_70out_65962, _34718, _34719);
    DeRef(_34719);
    _34719 = NOVALUE;
L7: 

    /** 		op = Code[pc]*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _op_68604 = (int)*(((s1_ptr)_2)->base + _70pc_65963);
    if (!IS_ATOM_INT(_op_68604)){
        _op_68604 = (long)DBL_PTR(_op_68604)->dbl;
    }

    /** 		call_proc(operation[op], {})*/
    _2 = (int)SEQ_PTR(_70operation_65971);
    _34721 = (int)*(((s1_ptr)_2)->base + _op_68604);
    _0 = (int)_00[_34721].addr;
    (*(int (*)())_0)(
                         );

    /** 	end while*/
    goto L5; // [411] 322
L6: 

    /** 	printf( out, "End SubProgram [%s:%05d]\n", {SymTab[sub][S_NAME], sub})*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _34723 = (int)*(((s1_ptr)_2)->base + _sub_68603);
    _2 = (int)SEQ_PTR(_34723);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _34724 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _34724 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _34723 = NOVALUE;
    Ref(_34724);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _34724;
    ((int *)_2)[2] = _sub_68603;
    _34725 = MAKE_SEQ(_1);
    _34724 = NOVALUE;
    EPrintf(_70out_65962, _34722, _34725);
    DeRefDS(_34725);
    _34725 = NOVALUE;

    /** end procedure*/
    DeRefi(_params_68614);
    DeRef(_line_table_68686);
    _34671 = NOVALUE;
    _34681 = NOVALUE;
    _34697 = NOVALUE;
    DeRef(_34714);
    _34714 = NOVALUE;
    _34721 = NOVALUE;
    return;
    ;
}


int _70dis_crash(int _c_68716)
{
    int _0, _1, _2;
    

    /** 	save_il( "dis." )*/
    RefDS(_34726);
    _70save_il(_34726);

    /** 	return 0*/
    return 0;
    ;
}


int _70extract_options(int _s_68723)
{
    int _0, _1, _2;
    

    /** 	return s*/
    return _s_68723;
    ;
}


int _70set_html(int _o_68727)
{
    int _0, _1, _2;
    

    /** 	generate_html = 1*/
    _70generate_html_68724 = 1;

    /** 	return 0*/
    return 0;
    ;
}


int _70enable_file_list(int _o_68731)
{
    int _0, _1, _2;
    

    /** 	generate_file_list = 1*/
    _70generate_file_list_68728 = 1;

    /** 	return 0*/
    return 0;
    ;
}


void _70BackEnd(int _ignore_68796)
{
    int _34809 = NOVALUE;
    int _34808 = NOVALUE;
    int _34807 = NOVALUE;
    int _34806 = NOVALUE;
    int _34805 = NOVALUE;
    int _34804 = NOVALUE;
    int _34803 = NOVALUE;
    int _34802 = NOVALUE;
    int _34801 = NOVALUE;
    int _34800 = NOVALUE;
    int _34799 = NOVALUE;
    int _34798 = NOVALUE;
    int _34796 = NOVALUE;
    int _34795 = NOVALUE;
    int _34794 = NOVALUE;
    int _34793 = NOVALUE;
    int _34792 = NOVALUE;
    int _34790 = NOVALUE;
    int _34788 = NOVALUE;
    int _34787 = NOVALUE;
    int _34784 = NOVALUE;
    int _34782 = NOVALUE;
    int _34781 = NOVALUE;
    int _34780 = NOVALUE;
    int _0, _1, _2;
    

    /** 	save_il( known_files[1] & '.' )*/
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _34780 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_34780) && IS_ATOM(46)) {
        Append(&_34781, _34780, 46);
    }
    else if (IS_ATOM(_34780) && IS_SEQUENCE(46)) {
    }
    else {
        Concat((object_ptr)&_34781, _34780, 46);
        _34780 = NOVALUE;
    }
    _34780 = NOVALUE;
    _70save_il(_34781);
    _34781 = NOVALUE;

    /** 	out = open( known_files[1] & ".dis", "wb" )*/
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _34782 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_34782) && IS_ATOM(_34783)) {
    }
    else if (IS_ATOM(_34782) && IS_SEQUENCE(_34783)) {
        Ref(_34782);
        Prepend(&_34784, _34783, _34782);
    }
    else {
        Concat((object_ptr)&_34784, _34782, _34783);
        _34782 = NOVALUE;
    }
    _34782 = NOVALUE;
    _70out_65962 = EOpen(_34784, _34367, 0);
    DeRefDS(_34784);
    _34784 = NOVALUE;

    /** 	printf(1,"saved to [%s.dis]\n", {known_files[1]})*/
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _34787 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_34787);
    *((int *)(_2+4)) = _34787;
    _34788 = MAKE_SEQ(_1);
    _34787 = NOVALUE;
    EPrintf(1, _34786, _34788);
    DeRefDS(_34788);
    _34788 = NOVALUE;

    /** 	if generate_file_list then*/
    if (_70generate_file_list_68728 == 0)
    {
        goto L1; // [53] 110
    }
    else{
    }

    /** 		puts( out, "File List:\n" )*/
    EPuts(_70out_65962, _34789); // DJP 

    /** 		for i = 1 to length( known_files ) do*/
    if (IS_SEQUENCE(_26known_files_11139)){
            _34790 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _34790 = 1;
    }
    {
        int _i_68812;
        _i_68812 = 1;
L2: 
        if (_i_68812 > _34790){
            goto L3; // [70] 102
        }

        /** 			printf( out, "%s\n", {known_files[i]})*/
        _2 = (int)SEQ_PTR(_26known_files_11139);
        _34792 = (int)*(((s1_ptr)_2)->base + _i_68812);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_34792);
        *((int *)(_2+4)) = _34792;
        _34793 = MAKE_SEQ(_1);
        _34792 = NOVALUE;
        EPrintf(_70out_65962, _34791, _34793);
        DeRefDS(_34793);
        _34793 = NOVALUE;

        /** 		end for*/
        _i_68812 = _i_68812 + 1;
        goto L2; // [97] 77
L3: 
        ;
    }

    /** 		puts( out, "\n" )*/
    EPuts(_70out_65962, _22834); // DJP 
L1: 

    /** 	if atom(slist[$]) then*/
    if (IS_SEQUENCE(_25slist_12357)){
            _34794 = SEQ_PTR(_25slist_12357)->length;
    }
    else {
        _34794 = 1;
    }
    _2 = (int)SEQ_PTR(_25slist_12357);
    _34795 = (int)*(((s1_ptr)_2)->base + _34794);
    _34796 = IS_ATOM(_34795);
    _34795 = NOVALUE;
    if (_34796 == 0)
    {
        _34796 = NOVALUE;
        goto L4; // [124] 138
    }
    else{
        _34796 = NOVALUE;
    }

    /** 		slist = s_expand(slist)*/
    RefDS(_25slist_12357);
    _0 = _60s_expand(_25slist_12357);
    DeRefDS(_25slist_12357);
    _25slist_12357 = _0;
L4: 

    /** 	for i = TopLevelSub to length(SymTab) do*/
    if (IS_SEQUENCE(_26SymTab_11138)){
            _34798 = SEQ_PTR(_26SymTab_11138)->length;
    }
    else {
        _34798 = 1;
    }
    {
        int _i_68829;
        _i_68829 = _25TopLevelSub_12269;
L5: 
        if (_i_68829 > _34798){
            goto L6; // [147] 236
        }

        /** 		if length(SymTab[i]) = SIZEOF_ROUTINE_ENTRY*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _34799 = (int)*(((s1_ptr)_2)->base + _i_68829);
        if (IS_SEQUENCE(_34799)){
                _34800 = SEQ_PTR(_34799)->length;
        }
        else {
            _34800 = 1;
        }
        _34799 = NOVALUE;
        _34801 = (_34800 == _25SIZEOF_ROUTINE_ENTRY_12038);
        _34800 = NOVALUE;
        if (_34801 == 0) {
            _34802 = 0;
            goto L7; // [171] 194
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _34803 = (int)*(((s1_ptr)_2)->base + _i_68829);
        _2 = (int)SEQ_PTR(_34803);
        if (!IS_ATOM_INT(_25S_CODE_11925)){
            _34804 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
        }
        else{
            _34804 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
        }
        _34803 = NOVALUE;
        _34805 = IS_SEQUENCE(_34804);
        _34804 = NOVALUE;
        _34802 = (_34805 != 0);
L7: 
        if (_34802 == 0) {
            goto L8; // [194] 228
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _34807 = (int)*(((s1_ptr)_2)->base + _i_68829);
        _2 = (int)SEQ_PTR(_34807);
        _34808 = (int)*(((s1_ptr)_2)->base + 4);
        _34807 = NOVALUE;
        if (IS_ATOM_INT(_34808)) {
            _34809 = (_34808 != 3);
        }
        else {
            _34809 = binary_op(NOTEQ, _34808, 3);
        }
        _34808 = NOVALUE;
        if (_34809 == 0) {
            DeRef(_34809);
            _34809 = NOVALUE;
            goto L8; // [217] 228
        }
        else {
            if (!IS_ATOM_INT(_34809) && DBL_PTR(_34809)->dbl == 0.0){
                DeRef(_34809);
                _34809 = NOVALUE;
                goto L8; // [217] 228
            }
            DeRef(_34809);
            _34809 = NOVALUE;
        }
        DeRef(_34809);
        _34809 = NOVALUE;

        /** 			dis( i )*/
        _70dis(_i_68829);
        goto L9; // [225] 229
L8: 
L9: 

        /** 	end for*/
        _i_68829 = _i_68829 + 1;
        goto L5; // [231] 154
L6: 
        ;
    }

    /** 	close( out )*/
    EClose(_70out_65962);

    /** 	if generate_html then*/
    if (_70generate_html_68724 == 0)
    {
        goto LA; // [246] 254
    }
    else{
    }

    /** 		dox:generate()*/
    _72generate();
LA: 

    /** end procedure*/
    _34799 = NOVALUE;
    DeRef(_34801);
    _34801 = NOVALUE;
    return;
    ;
}



// 0xE8B9A20D
