// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _33malloc(int _mem_struct_p_12562, int _cleanup_p_12563)
{
    int _temp__12564 = NOVALUE;
    int _6929 = NOVALUE;
    int _6927 = NOVALUE;
    int _6926 = NOVALUE;
    int _6925 = NOVALUE;
    int _6921 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_cleanup_p_12563)) {
        _1 = (long)(DBL_PTR(_cleanup_p_12563)->dbl);
        if (UNIQUE(DBL_PTR(_cleanup_p_12563)) && (DBL_PTR(_cleanup_p_12563)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_cleanup_p_12563);
        _cleanup_p_12563 = _1;
    }

    /** 	if atom(mem_struct_p) then*/
    _6921 = IS_ATOM(_mem_struct_p_12562);
    if (_6921 == 0)
    {
        _6921 = NOVALUE;
        goto L1; // [8] 18
    }
    else{
        _6921 = NOVALUE;
    }

    /** 		mem_struct_p = repeat(0, mem_struct_p)*/
    _0 = _mem_struct_p_12562;
    _mem_struct_p_12562 = Repeat(0, _mem_struct_p_12562);
    DeRef(_0);
L1: 

    /** 	if ram_free_list = 0 then*/
    if (_33ram_free_list_12558 != 0)
    goto L2; // [22] 72

    /** 		ram_space = append(ram_space, mem_struct_p)*/
    Ref(_mem_struct_p_12562);
    Append(&_33ram_space_12557, _33ram_space_12557, _mem_struct_p_12562);

    /** 		if cleanup_p then*/
    if (_cleanup_p_12563 == 0)
    {
        goto L3; // [36] 59
    }
    else{
    }

    /** 			return delete_routine( length(ram_space), free_rid )*/
    if (IS_SEQUENCE(_33ram_space_12557)){
            _6925 = SEQ_PTR(_33ram_space_12557)->length;
    }
    else {
        _6925 = 1;
    }
    _6926 = NewDouble( (double) _6925 );
    _1 = (int) _00[_33free_rid_12559].cleanup;
    if( _1 == 0 ){
        _1 = (int) TransAlloc( sizeof(struct cleanup) );
        _00[_33free_rid_12559].cleanup = (cleanup_ptr)_1;
    }
    ((cleanup_ptr)_1)->type = CLEAN_UDT_RT;
    ((cleanup_ptr)_1)->func.rid = _33free_rid_12559;
    ((cleanup_ptr)_1)->next = 0;
    if(DBL_PTR(_6926)->cleanup != 0 ){
        _1 = (int) ChainDeleteRoutine( (cleanup_ptr)_1, DBL_PTR(_6926)->cleanup );
    }
    else if( !UNIQUE(DBL_PTR(_6926)) ){
        DeRefDS(_6926);
        _6926 = NewDouble( DBL_PTR(_6926)->dbl );
    }
    DBL_PTR(_6926)->cleanup = (cleanup_ptr)_1;
    _6925 = NOVALUE;
    DeRef(_mem_struct_p_12562);
    return _6926;
    goto L4; // [56] 71
L3: 

    /** 			return length(ram_space)*/
    if (IS_SEQUENCE(_33ram_space_12557)){
            _6927 = SEQ_PTR(_33ram_space_12557)->length;
    }
    else {
        _6927 = 1;
    }
    DeRef(_mem_struct_p_12562);
    DeRef(_6926);
    _6926 = NOVALUE;
    return _6927;
L4: 
L2: 

    /** 	temp_ = ram_free_list*/
    _temp__12564 = _33ram_free_list_12558;

    /** 	ram_free_list = ram_space[temp_]*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    _33ram_free_list_12558 = (int)*(((s1_ptr)_2)->base + _temp__12564);
    if (!IS_ATOM_INT(_33ram_free_list_12558))
    _33ram_free_list_12558 = (long)DBL_PTR(_33ram_free_list_12558)->dbl;

    /** 	ram_space[temp_] = mem_struct_p*/
    Ref(_mem_struct_p_12562);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _33ram_space_12557 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _temp__12564);
    _1 = *(int *)_2;
    *(int *)_2 = _mem_struct_p_12562;
    DeRef(_1);

    /** 	if cleanup_p then*/
    if (_cleanup_p_12563 == 0)
    {
        goto L5; // [97] 115
    }
    else{
    }

    /** 		return delete_routine( temp_, free_rid )*/
    _6929 = NewDouble( (double) _temp__12564 );
    _1 = (int) _00[_33free_rid_12559].cleanup;
    if( _1 == 0 ){
        _1 = (int) TransAlloc( sizeof(struct cleanup) );
        _00[_33free_rid_12559].cleanup = (cleanup_ptr)_1;
    }
    ((cleanup_ptr)_1)->type = CLEAN_UDT_RT;
    ((cleanup_ptr)_1)->func.rid = _33free_rid_12559;
    ((cleanup_ptr)_1)->next = 0;
    if(DBL_PTR(_6929)->cleanup != 0 ){
        _1 = (int) ChainDeleteRoutine( (cleanup_ptr)_1, DBL_PTR(_6929)->cleanup );
    }
    else if( !UNIQUE(DBL_PTR(_6929)) ){
        DeRefDS(_6929);
        _6929 = NewDouble( DBL_PTR(_6929)->dbl );
    }
    DBL_PTR(_6929)->cleanup = (cleanup_ptr)_1;
    DeRef(_mem_struct_p_12562);
    DeRef(_6926);
    _6926 = NOVALUE;
    return _6929;
    goto L6; // [112] 122
L5: 

    /** 		return temp_*/
    DeRef(_mem_struct_p_12562);
    DeRef(_6926);
    _6926 = NOVALUE;
    DeRef(_6929);
    _6929 = NOVALUE;
    return _temp__12564;
L6: 
    ;
}


void _33free(int _mem_p_12582)
{
    int _6931 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if mem_p < 1 then return end if*/
    if (binary_op_a(GREATEREQ, _mem_p_12582, 1)){
        goto L1; // [3] 11
    }
    DeRef(_mem_p_12582);
    return;
L1: 

    /** 	if mem_p > length(ram_space) then return end if*/
    if (IS_SEQUENCE(_33ram_space_12557)){
            _6931 = SEQ_PTR(_33ram_space_12557)->length;
    }
    else {
        _6931 = 1;
    }
    if (binary_op_a(LESSEQ, _mem_p_12582, _6931)){
        _6931 = NOVALUE;
        goto L2; // [18] 26
    }
    _6931 = NOVALUE;
    DeRef(_mem_p_12582);
    return;
L2: 

    /** 	ram_space[mem_p] = ram_free_list*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _33ram_space_12557 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_mem_p_12582))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_mem_p_12582)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _mem_p_12582);
    _1 = *(int *)_2;
    *(int *)_2 = _33ram_free_list_12558;
    DeRef(_1);

    /** 	ram_free_list = floor(mem_p)*/
    if (IS_ATOM_INT(_mem_p_12582))
    _33ram_free_list_12558 = e_floor(_mem_p_12582);
    else
    _33ram_free_list_12558 = unary_op(FLOOR, _mem_p_12582);
    if (!IS_ATOM_INT(_33ram_free_list_12558)) {
        _1 = (long)(DBL_PTR(_33ram_free_list_12558)->dbl);
        if (UNIQUE(DBL_PTR(_33ram_free_list_12558)) && (DBL_PTR(_33ram_free_list_12558)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_33ram_free_list_12558);
        _33ram_free_list_12558 = _1;
    }

    /** end procedure*/
    DeRef(_mem_p_12582);
    return;
    ;
}


int _33valid(int _mem_p_12592, int _mem_struct_p_12593)
{
    int _6945 = NOVALUE;
    int _6944 = NOVALUE;
    int _6942 = NOVALUE;
    int _6941 = NOVALUE;
    int _6940 = NOVALUE;
    int _6938 = NOVALUE;
    int _6935 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not integer(mem_p) then return 0 end if*/
    if (IS_ATOM_INT(_mem_p_12592))
    _6935 = 1;
    else if (IS_ATOM_DBL(_mem_p_12592))
    _6935 = IS_ATOM_INT(DoubleToInt(_mem_p_12592));
    else
    _6935 = 0;
    if (_6935 != 0)
    goto L1; // [6] 14
    _6935 = NOVALUE;
    DeRef(_mem_p_12592);
    DeRef(_mem_struct_p_12593);
    return 0;
L1: 

    /** 	if mem_p < 1 then return 0 end if*/
    if (binary_op_a(GREATEREQ, _mem_p_12592, 1)){
        goto L2; // [16] 25
    }
    DeRef(_mem_p_12592);
    DeRef(_mem_struct_p_12593);
    return 0;
L2: 

    /** 	if mem_p > length(ram_space) then return 0 end if*/
    if (IS_SEQUENCE(_33ram_space_12557)){
            _6938 = SEQ_PTR(_33ram_space_12557)->length;
    }
    else {
        _6938 = 1;
    }
    if (binary_op_a(LESSEQ, _mem_p_12592, _6938)){
        _6938 = NOVALUE;
        goto L3; // [32] 41
    }
    _6938 = NOVALUE;
    DeRef(_mem_p_12592);
    DeRef(_mem_struct_p_12593);
    return 0;
L3: 

    /** 	if sequence(mem_struct_p) then return 1 end if*/
    _6940 = IS_SEQUENCE(_mem_struct_p_12593);
    if (_6940 == 0)
    {
        _6940 = NOVALUE;
        goto L4; // [46] 54
    }
    else{
        _6940 = NOVALUE;
    }
    DeRef(_mem_p_12592);
    DeRef(_mem_struct_p_12593);
    return 1;
L4: 

    /** 	if atom(ram_space[mem_p]) then */
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!IS_ATOM_INT(_mem_p_12592)){
        _6941 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_mem_p_12592)->dbl));
    }
    else{
        _6941 = (int)*(((s1_ptr)_2)->base + _mem_p_12592);
    }
    _6942 = IS_ATOM(_6941);
    _6941 = NOVALUE;
    if (_6942 == 0)
    {
        _6942 = NOVALUE;
        goto L5; // [65] 88
    }
    else{
        _6942 = NOVALUE;
    }

    /** 		if mem_struct_p >= 0 then*/
    if (binary_op_a(LESS, _mem_struct_p_12593, 0)){
        goto L6; // [70] 81
    }

    /** 			return 0*/
    DeRef(_mem_p_12592);
    DeRef(_mem_struct_p_12593);
    return 0;
L6: 

    /** 		return 1*/
    DeRef(_mem_p_12592);
    DeRef(_mem_struct_p_12593);
    return 1;
L5: 

    /** 	if length(ram_space[mem_p]) != mem_struct_p then*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!IS_ATOM_INT(_mem_p_12592)){
        _6944 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_mem_p_12592)->dbl));
    }
    else{
        _6944 = (int)*(((s1_ptr)_2)->base + _mem_p_12592);
    }
    if (IS_SEQUENCE(_6944)){
            _6945 = SEQ_PTR(_6944)->length;
    }
    else {
        _6945 = 1;
    }
    _6944 = NOVALUE;
    if (binary_op_a(EQUALS, _6945, _mem_struct_p_12593)){
        _6945 = NOVALUE;
        goto L7; // [99] 110
    }
    _6945 = NOVALUE;

    /** 		return 0*/
    DeRef(_mem_p_12592);
    DeRef(_mem_struct_p_12593);
    _6944 = NOVALUE;
    return 0;
L7: 

    /** 	return 1*/
    DeRef(_mem_p_12592);
    DeRef(_mem_struct_p_12593);
    _6944 = NOVALUE;
    return 1;
    ;
}



// 0x868F648F
