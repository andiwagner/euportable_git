// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _22sort(int _x_5006, int _order_5007)
{
    int _gap_5008 = NOVALUE;
    int _j_5009 = NOVALUE;
    int _first_5010 = NOVALUE;
    int _last_5011 = NOVALUE;
    int _tempi_5012 = NOVALUE;
    int _tempj_5013 = NOVALUE;
    int _2514 = NOVALUE;
    int _2510 = NOVALUE;
    int _2507 = NOVALUE;
    int _2503 = NOVALUE;
    int _2500 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_order_5007)) {
        _1 = (long)(DBL_PTR(_order_5007)->dbl);
        if (UNIQUE(DBL_PTR(_order_5007)) && (DBL_PTR(_order_5007)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_order_5007);
        _order_5007 = _1;
    }

    /** 	if order >= 0 then*/
    if (_order_5007 < 0)
    goto L1; // [7] 19

    /** 		order = -1*/
    _order_5007 = -1;
    goto L2; // [16] 25
L1: 

    /** 		order = 1*/
    _order_5007 = 1;
L2: 

    /** 	last = length(x)*/
    if (IS_SEQUENCE(_x_5006)){
            _last_5011 = SEQ_PTR(_x_5006)->length;
    }
    else {
        _last_5011 = 1;
    }

    /** 	gap = floor(last / 10) + 1*/
    if (10 > 0 && _last_5011 >= 0) {
        _2500 = _last_5011 / 10;
    }
    else {
        temp_dbl = floor((double)_last_5011 / (double)10);
        _2500 = (long)temp_dbl;
    }
    _gap_5008 = _2500 + 1;
    _2500 = NOVALUE;

    /** 	while 1 do*/
L3: 

    /** 		first = gap + 1*/
    _first_5010 = _gap_5008 + 1;

    /** 		for i = first to last do*/
    _2503 = _last_5011;
    {
        int _i_5023;
        _i_5023 = _first_5010;
L4: 
        if (_i_5023 > _2503){
            goto L5; // [56] 152
        }

        /** 			tempi = x[i]*/
        DeRef(_tempi_5012);
        _2 = (int)SEQ_PTR(_x_5006);
        _tempi_5012 = (int)*(((s1_ptr)_2)->base + _i_5023);
        Ref(_tempi_5012);

        /** 			j = i - gap*/
        _j_5009 = _i_5023 - _gap_5008;

        /** 			while 1 do*/
L6: 

        /** 				tempj = x[j]*/
        DeRef(_tempj_5013);
        _2 = (int)SEQ_PTR(_x_5006);
        _tempj_5013 = (int)*(((s1_ptr)_2)->base + _j_5009);
        Ref(_tempj_5013);

        /** 				if eu:compare(tempi, tempj) != order then*/
        if (IS_ATOM_INT(_tempi_5012) && IS_ATOM_INT(_tempj_5013)){
            _2507 = (_tempi_5012 < _tempj_5013) ? -1 : (_tempi_5012 > _tempj_5013);
        }
        else{
            _2507 = compare(_tempi_5012, _tempj_5013);
        }
        if (_2507 == _order_5007)
        goto L7; // [92] 107

        /** 					j += gap*/
        _j_5009 = _j_5009 + _gap_5008;

        /** 					exit*/
        goto L8; // [104] 139
L7: 

        /** 				x[j+gap] = tempj*/
        _2510 = _j_5009 + _gap_5008;
        Ref(_tempj_5013);
        _2 = (int)SEQ_PTR(_x_5006);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_5006 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _2510);
        _1 = *(int *)_2;
        *(int *)_2 = _tempj_5013;
        DeRef(_1);

        /** 				if j <= gap then*/
        if (_j_5009 > _gap_5008)
        goto L9; // [119] 128

        /** 					exit*/
        goto L8; // [125] 139
L9: 

        /** 				j -= gap*/
        _j_5009 = _j_5009 - _gap_5008;

        /** 			end while*/
        goto L6; // [136] 80
L8: 

        /** 			x[j] = tempi*/
        Ref(_tempi_5012);
        _2 = (int)SEQ_PTR(_x_5006);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_5006 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _j_5009);
        _1 = *(int *)_2;
        *(int *)_2 = _tempi_5012;
        DeRef(_1);

        /** 		end for*/
        _i_5023 = _i_5023 + 1;
        goto L4; // [147] 63
L5: 
        ;
    }

    /** 		if gap = 1 then*/
    if (_gap_5008 != 1)
    goto LA; // [154] 167

    /** 			return x*/
    DeRef(_tempi_5012);
    DeRef(_tempj_5013);
    DeRef(_2510);
    _2510 = NOVALUE;
    return _x_5006;
    goto L3; // [164] 45
LA: 

    /** 			gap = floor(gap / 7) + 1*/
    if (7 > 0 && _gap_5008 >= 0) {
        _2514 = _gap_5008 / 7;
    }
    else {
        temp_dbl = floor((double)_gap_5008 / (double)7);
        _2514 = (long)temp_dbl;
    }
    _gap_5008 = _2514 + 1;
    _2514 = NOVALUE;

    /** 	end while*/
    goto L3; // [180] 45
    ;
}


int _22custom_sort(int _custom_compare_5044, int _x_5045, int _data_5046, int _order_5047)
{
    int _gap_5048 = NOVALUE;
    int _j_5049 = NOVALUE;
    int _first_5050 = NOVALUE;
    int _last_5051 = NOVALUE;
    int _tempi_5052 = NOVALUE;
    int _tempj_5053 = NOVALUE;
    int _result_5054 = NOVALUE;
    int _args_5055 = NOVALUE;
    int _2542 = NOVALUE;
    int _2538 = NOVALUE;
    int _2535 = NOVALUE;
    int _2533 = NOVALUE;
    int _2532 = NOVALUE;
    int _2527 = NOVALUE;
    int _2524 = NOVALUE;
    int _2521 = NOVALUE;
    int _2520 = NOVALUE;
    int _2518 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_custom_compare_5044)) {
        _1 = (long)(DBL_PTR(_custom_compare_5044)->dbl);
        if (UNIQUE(DBL_PTR(_custom_compare_5044)) && (DBL_PTR(_custom_compare_5044)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_custom_compare_5044);
        _custom_compare_5044 = _1;
    }
    if (!IS_ATOM_INT(_order_5047)) {
        _1 = (long)(DBL_PTR(_order_5047)->dbl);
        if (UNIQUE(DBL_PTR(_order_5047)) && (DBL_PTR(_order_5047)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_order_5047);
        _order_5047 = _1;
    }

    /** 	sequence args = {0, 0}*/
    DeRef(_args_5055);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 0;
    _args_5055 = MAKE_SEQ(_1);

    /** 	if order >= 0 then*/
    if (_order_5047 < 0)
    goto L1; // [15] 27

    /** 		order = -1*/
    _order_5047 = -1;
    goto L2; // [24] 33
L1: 

    /** 		order = 1*/
    _order_5047 = 1;
L2: 

    /** 	if atom(data) then*/
    _2518 = IS_ATOM(_data_5046);
    if (_2518 == 0)
    {
        _2518 = NOVALUE;
        goto L3; // [38] 50
    }
    else{
        _2518 = NOVALUE;
    }

    /** 		args &= data*/
    if (IS_SEQUENCE(_args_5055) && IS_ATOM(_data_5046)) {
        Ref(_data_5046);
        Append(&_args_5055, _args_5055, _data_5046);
    }
    else if (IS_ATOM(_args_5055) && IS_SEQUENCE(_data_5046)) {
    }
    else {
        Concat((object_ptr)&_args_5055, _args_5055, _data_5046);
    }
    goto L4; // [47] 70
L3: 

    /** 	elsif length(data) then*/
    if (IS_SEQUENCE(_data_5046)){
            _2520 = SEQ_PTR(_data_5046)->length;
    }
    else {
        _2520 = 1;
    }
    if (_2520 == 0)
    {
        _2520 = NOVALUE;
        goto L5; // [55] 69
    }
    else{
        _2520 = NOVALUE;
    }

    /** 		args = append(args, data[1])*/
    _2 = (int)SEQ_PTR(_data_5046);
    _2521 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_2521);
    Append(&_args_5055, _args_5055, _2521);
    _2521 = NOVALUE;
L5: 
L4: 

    /** 	last = length(x)*/
    if (IS_SEQUENCE(_x_5045)){
            _last_5051 = SEQ_PTR(_x_5045)->length;
    }
    else {
        _last_5051 = 1;
    }

    /** 	gap = floor(last / 10) + 1*/
    if (10 > 0 && _last_5051 >= 0) {
        _2524 = _last_5051 / 10;
    }
    else {
        temp_dbl = floor((double)_last_5051 / (double)10);
        _2524 = (long)temp_dbl;
    }
    _gap_5048 = _2524 + 1;
    _2524 = NOVALUE;

    /** 	while 1 do*/
L6: 

    /** 		first = gap + 1*/
    _first_5050 = _gap_5048 + 1;

    /** 		for i = first to last do*/
    _2527 = _last_5051;
    {
        int _i_5073;
        _i_5073 = _first_5050;
L7: 
        if (_i_5073 > _2527){
            goto L8; // [101] 240
        }

        /** 			tempi = x[i]*/
        DeRef(_tempi_5052);
        _2 = (int)SEQ_PTR(_x_5045);
        _tempi_5052 = (int)*(((s1_ptr)_2)->base + _i_5073);
        Ref(_tempi_5052);

        /** 			args[1] = tempi*/
        Ref(_tempi_5052);
        _2 = (int)SEQ_PTR(_args_5055);
        _2 = (int)(((s1_ptr)_2)->base + 1);
        _1 = *(int *)_2;
        *(int *)_2 = _tempi_5052;
        DeRef(_1);

        /** 			j = i - gap*/
        _j_5049 = _i_5073 - _gap_5048;

        /** 			while 1 do*/
L9: 

        /** 				tempj = x[j]*/
        DeRef(_tempj_5053);
        _2 = (int)SEQ_PTR(_x_5045);
        _tempj_5053 = (int)*(((s1_ptr)_2)->base + _j_5049);
        Ref(_tempj_5053);

        /** 				args[2] = tempj*/
        Ref(_tempj_5053);
        _2 = (int)SEQ_PTR(_args_5055);
        _2 = (int)(((s1_ptr)_2)->base + 2);
        _1 = *(int *)_2;
        *(int *)_2 = _tempj_5053;
        DeRef(_1);

        /** 				result = call_func(custom_compare, args)*/
        _1 = (int)SEQ_PTR(_args_5055);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_custom_compare_5044].addr;
        switch(((s1_ptr)_1)->length) {
            case 0:
                _1 = (*(int (*)())_0)(
                                     );
                break;
            case 1:
                Ref(*(int *)(_2+4));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4)
                                     );
                break;
            case 2:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8)
                                     );
                break;
            case 3:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12)
                                     );
                break;
            case 4:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16)
                                     );
                break;
            case 5:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20)
                                     );
                break;
            case 6:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24)
                                     );
                break;
            case 7:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28)
                                     );
                break;
            case 8:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32)
                                     );
                break;
            case 9:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36)
                                     );
                break;
            case 10:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40)
                                     );
                break;
            case 11:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44)
                                     );
                break;
            case 12:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48)
                                     );
                break;
            case 13:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52)
                                     );
                break;
            case 14:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                Ref(*(int *)(_2+56));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52), 
                                    *(int *)(_2+56)
                                     );
                break;
            case 15:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                Ref(*(int *)(_2+56));
                Ref(*(int *)(_2+60));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52), 
                                    *(int *)(_2+56), 
                                    *(int *)(_2+60)
                                     );
                break;
            case 16:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                Ref(*(int *)(_2+56));
                Ref(*(int *)(_2+60));
                Ref(*(int *)(_2+64));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52), 
                                    *(int *)(_2+56), 
                                    *(int *)(_2+60), 
                                    *(int *)(_2+64)
                                     );
                break;
            case 17:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                Ref(*(int *)(_2+56));
                Ref(*(int *)(_2+60));
                Ref(*(int *)(_2+64));
                Ref(*(int *)(_2+68));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52), 
                                    *(int *)(_2+56), 
                                    *(int *)(_2+60), 
                                    *(int *)(_2+64), 
                                    *(int *)(_2+68)
                                     );
                break;
            case 18:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                Ref(*(int *)(_2+56));
                Ref(*(int *)(_2+60));
                Ref(*(int *)(_2+64));
                Ref(*(int *)(_2+68));
                Ref(*(int *)(_2+72));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52), 
                                    *(int *)(_2+56), 
                                    *(int *)(_2+60), 
                                    *(int *)(_2+64), 
                                    *(int *)(_2+68), 
                                    *(int *)(_2+72)
                                     );
                break;
            case 19:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                Ref(*(int *)(_2+56));
                Ref(*(int *)(_2+60));
                Ref(*(int *)(_2+64));
                Ref(*(int *)(_2+68));
                Ref(*(int *)(_2+72));
                Ref(*(int *)(_2+76));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52), 
                                    *(int *)(_2+56), 
                                    *(int *)(_2+60), 
                                    *(int *)(_2+64), 
                                    *(int *)(_2+68), 
                                    *(int *)(_2+72), 
                                    *(int *)(_2+76)
                                     );
                break;
            case 20:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                Ref(*(int *)(_2+56));
                Ref(*(int *)(_2+60));
                Ref(*(int *)(_2+64));
                Ref(*(int *)(_2+68));
                Ref(*(int *)(_2+72));
                Ref(*(int *)(_2+76));
                Ref(*(int *)(_2+80));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52), 
                                    *(int *)(_2+56), 
                                    *(int *)(_2+60), 
                                    *(int *)(_2+64), 
                                    *(int *)(_2+68), 
                                    *(int *)(_2+72), 
                                    *(int *)(_2+76), 
                                    *(int *)(_2+80)
                                     );
                break;
        }
        DeRef(_result_5054);
        _result_5054 = _1;

        /** 				if sequence(result) then*/
        _2532 = IS_SEQUENCE(_result_5054);
        if (_2532 == 0)
        {
            _2532 = NOVALUE;
            goto LA; // [154] 174
        }
        else{
            _2532 = NOVALUE;
        }

        /** 					args[3] = result[2]*/
        _2 = (int)SEQ_PTR(_result_5054);
        _2533 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_2533);
        _2 = (int)SEQ_PTR(_args_5055);
        _2 = (int)(((s1_ptr)_2)->base + 3);
        _1 = *(int *)_2;
        *(int *)_2 = _2533;
        if( _1 != _2533 ){
            DeRef(_1);
        }
        _2533 = NOVALUE;

        /** 					result = result[1]*/
        _0 = _result_5054;
        _2 = (int)SEQ_PTR(_result_5054);
        _result_5054 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_result_5054);
        DeRef(_0);
LA: 

        /** 				if eu:compare(result, 0) != order then*/
        if (IS_ATOM_INT(_result_5054) && IS_ATOM_INT(0)){
            _2535 = (_result_5054 < 0) ? -1 : (_result_5054 > 0);
        }
        else{
            _2535 = compare(_result_5054, 0);
        }
        if (_2535 == _order_5047)
        goto LB; // [180] 195

        /** 					j += gap*/
        _j_5049 = _j_5049 + _gap_5048;

        /** 					exit*/
        goto LC; // [192] 227
LB: 

        /** 				x[j+gap] = tempj*/
        _2538 = _j_5049 + _gap_5048;
        Ref(_tempj_5053);
        _2 = (int)SEQ_PTR(_x_5045);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_5045 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _2538);
        _1 = *(int *)_2;
        *(int *)_2 = _tempj_5053;
        DeRef(_1);

        /** 				if j <= gap then*/
        if (_j_5049 > _gap_5048)
        goto LD; // [207] 216

        /** 					exit*/
        goto LC; // [213] 227
LD: 

        /** 				j -= gap*/
        _j_5049 = _j_5049 - _gap_5048;

        /** 			end while*/
        goto L9; // [224] 131
LC: 

        /** 			x[j] = tempi*/
        Ref(_tempi_5052);
        _2 = (int)SEQ_PTR(_x_5045);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_5045 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _j_5049);
        _1 = *(int *)_2;
        *(int *)_2 = _tempi_5052;
        DeRef(_1);

        /** 		end for*/
        _i_5073 = _i_5073 + 1;
        goto L7; // [235] 108
L8: 
        ;
    }

    /** 		if gap = 1 then*/
    if (_gap_5048 != 1)
    goto LE; // [242] 255

    /** 			return x*/
    DeRef(_data_5046);
    DeRef(_tempi_5052);
    DeRef(_tempj_5053);
    DeRef(_result_5054);
    DeRef(_args_5055);
    DeRef(_2538);
    _2538 = NOVALUE;
    return _x_5045;
    goto L6; // [252] 90
LE: 

    /** 			gap = floor(gap / 7) + 1*/
    if (7 > 0 && _gap_5048 >= 0) {
        _2542 = _gap_5048 / 7;
    }
    else {
        temp_dbl = floor((double)_gap_5048 / (double)7);
        _2542 = (long)temp_dbl;
    }
    _gap_5048 = _2542 + 1;
    _2542 = NOVALUE;

    /** 	end while*/
    goto L6; // [268] 90
    ;
}


int _22column_compare(int _a_5099, int _b_5100, int _cols_5101)
{
    int _sign_5102 = NOVALUE;
    int _column_5103 = NOVALUE;
    int _2565 = NOVALUE;
    int _2563 = NOVALUE;
    int _2562 = NOVALUE;
    int _2561 = NOVALUE;
    int _2560 = NOVALUE;
    int _2559 = NOVALUE;
    int _2558 = NOVALUE;
    int _2556 = NOVALUE;
    int _2555 = NOVALUE;
    int _2554 = NOVALUE;
    int _2552 = NOVALUE;
    int _2550 = NOVALUE;
    int _2547 = NOVALUE;
    int _2545 = NOVALUE;
    int _2544 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(cols) do*/
    if (IS_SEQUENCE(_cols_5101)){
            _2544 = SEQ_PTR(_cols_5101)->length;
    }
    else {
        _2544 = 1;
    }
    {
        int _i_5105;
        _i_5105 = 1;
L1: 
        if (_i_5105 > _2544){
            goto L2; // [6] 176
        }

        /** 		if cols[i] < 0 then*/
        _2 = (int)SEQ_PTR(_cols_5101);
        _2545 = (int)*(((s1_ptr)_2)->base + _i_5105);
        if (binary_op_a(GREATEREQ, _2545, 0)){
            _2545 = NOVALUE;
            goto L3; // [19] 42
        }
        _2545 = NOVALUE;

        /** 			sign = -1*/
        _sign_5102 = -1;

        /** 			column = -cols[i]*/
        _2 = (int)SEQ_PTR(_cols_5101);
        _2547 = (int)*(((s1_ptr)_2)->base + _i_5105);
        if (IS_ATOM_INT(_2547)) {
            if ((unsigned long)_2547 == 0xC0000000)
            _column_5103 = (int)NewDouble((double)-0xC0000000);
            else
            _column_5103 = - _2547;
        }
        else {
            _column_5103 = unary_op(UMINUS, _2547);
        }
        _2547 = NOVALUE;
        if (!IS_ATOM_INT(_column_5103)) {
            _1 = (long)(DBL_PTR(_column_5103)->dbl);
            if (UNIQUE(DBL_PTR(_column_5103)) && (DBL_PTR(_column_5103)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_column_5103);
            _column_5103 = _1;
        }
        goto L4; // [39] 56
L3: 

        /** 			sign = 1*/
        _sign_5102 = 1;

        /** 			column = cols[i]*/
        _2 = (int)SEQ_PTR(_cols_5101);
        _column_5103 = (int)*(((s1_ptr)_2)->base + _i_5105);
        if (!IS_ATOM_INT(_column_5103)){
            _column_5103 = (long)DBL_PTR(_column_5103)->dbl;
        }
L4: 

        /** 		if column <= length(a) then*/
        if (IS_SEQUENCE(_a_5099)){
                _2550 = SEQ_PTR(_a_5099)->length;
        }
        else {
            _2550 = 1;
        }
        if (_column_5103 > _2550)
        goto L5; // [63] 137

        /** 			if column <= length(b) then*/
        if (IS_SEQUENCE(_b_5100)){
                _2552 = SEQ_PTR(_b_5100)->length;
        }
        else {
            _2552 = 1;
        }
        if (_column_5103 > _2552)
        goto L6; // [72] 121

        /** 				if not equal(a[column], b[column]) then*/
        _2 = (int)SEQ_PTR(_a_5099);
        _2554 = (int)*(((s1_ptr)_2)->base + _column_5103);
        _2 = (int)SEQ_PTR(_b_5100);
        _2555 = (int)*(((s1_ptr)_2)->base + _column_5103);
        if (_2554 == _2555)
        _2556 = 1;
        else if (IS_ATOM_INT(_2554) && IS_ATOM_INT(_2555))
        _2556 = 0;
        else
        _2556 = (compare(_2554, _2555) == 0);
        _2554 = NOVALUE;
        _2555 = NOVALUE;
        if (_2556 != 0)
        goto L7; // [90] 169
        _2556 = NOVALUE;

        /** 					return sign * eu:compare(a[column], b[column])*/
        _2 = (int)SEQ_PTR(_a_5099);
        _2558 = (int)*(((s1_ptr)_2)->base + _column_5103);
        _2 = (int)SEQ_PTR(_b_5100);
        _2559 = (int)*(((s1_ptr)_2)->base + _column_5103);
        if (IS_ATOM_INT(_2558) && IS_ATOM_INT(_2559)){
            _2560 = (_2558 < _2559) ? -1 : (_2558 > _2559);
        }
        else{
            _2560 = compare(_2558, _2559);
        }
        _2558 = NOVALUE;
        _2559 = NOVALUE;
        if (_sign_5102 == (short)_sign_5102)
        _2561 = _sign_5102 * _2560;
        else
        _2561 = NewDouble(_sign_5102 * (double)_2560);
        _2560 = NOVALUE;
        DeRef(_a_5099);
        DeRef(_b_5100);
        DeRef(_cols_5101);
        return _2561;
        goto L7; // [118] 169
L6: 

        /** 				return sign * -1*/
        if (_sign_5102 == (short)_sign_5102)
        _2562 = _sign_5102 * -1;
        else
        _2562 = NewDouble(_sign_5102 * (double)-1);
        DeRef(_a_5099);
        DeRef(_b_5100);
        DeRef(_cols_5101);
        DeRef(_2561);
        _2561 = NOVALUE;
        return _2562;
        goto L7; // [134] 169
L5: 

        /** 			if column <= length(b) then*/
        if (IS_SEQUENCE(_b_5100)){
                _2563 = SEQ_PTR(_b_5100)->length;
        }
        else {
            _2563 = 1;
        }
        if (_column_5103 > _2563)
        goto L8; // [142] 161

        /** 				return sign * 1*/
        _2565 = _sign_5102 * 1;
        DeRef(_a_5099);
        DeRef(_b_5100);
        DeRef(_cols_5101);
        DeRef(_2561);
        _2561 = NOVALUE;
        DeRef(_2562);
        _2562 = NOVALUE;
        return _2565;
        goto L9; // [158] 168
L8: 

        /** 				return 0*/
        DeRef(_a_5099);
        DeRef(_b_5100);
        DeRef(_cols_5101);
        DeRef(_2561);
        _2561 = NOVALUE;
        DeRef(_2562);
        _2562 = NOVALUE;
        DeRef(_2565);
        _2565 = NOVALUE;
        return 0;
L9: 
L7: 

        /** 	end for*/
        _i_5105 = _i_5105 + 1;
        goto L1; // [171] 13
L2: 
        ;
    }

    /** 	return 0*/
    DeRef(_a_5099);
    DeRef(_b_5100);
    DeRef(_cols_5101);
    DeRef(_2561);
    _2561 = NOVALUE;
    DeRef(_2562);
    _2562 = NOVALUE;
    DeRef(_2565);
    _2565 = NOVALUE;
    return 0;
    ;
}


int _22sort_columns(int _x_5139, int _column_list_5140)
{
    int _2569 = NOVALUE;
    int _2568 = NOVALUE;
    int _2567 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return custom_sort(routine_id("column_compare"), x, {column_list})*/
    _2567 = CRoutineId(275, 22, _2566);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_column_list_5140);
    *((int *)(_2+4)) = _column_list_5140;
    _2568 = MAKE_SEQ(_1);
    RefDS(_x_5139);
    _2569 = _22custom_sort(_2567, _x_5139, _2568, 1);
    _2567 = NOVALUE;
    _2568 = NOVALUE;
    DeRefDS(_x_5139);
    DeRefDS(_column_list_5140);
    return _2569;
    ;
}


int _22merge(int _a_5147, int _b_5148, int _compfunc_5149, int _userdata_5150)
{
    int _al_5151 = NOVALUE;
    int _bl_5152 = NOVALUE;
    int _n_5153 = NOVALUE;
    int _r_5154 = NOVALUE;
    int _s_5155 = NOVALUE;
    int _2613 = NOVALUE;
    int _2612 = NOVALUE;
    int _2611 = NOVALUE;
    int _2609 = NOVALUE;
    int _2608 = NOVALUE;
    int _2607 = NOVALUE;
    int _2606 = NOVALUE;
    int _2604 = NOVALUE;
    int _2601 = NOVALUE;
    int _2599 = NOVALUE;
    int _2596 = NOVALUE;
    int _2595 = NOVALUE;
    int _2594 = NOVALUE;
    int _2593 = NOVALUE;
    int _2592 = NOVALUE;
    int _2591 = NOVALUE;
    int _2590 = NOVALUE;
    int _2587 = NOVALUE;
    int _2585 = NOVALUE;
    int _2582 = NOVALUE;
    int _2581 = NOVALUE;
    int _2580 = NOVALUE;
    int _2579 = NOVALUE;
    int _2578 = NOVALUE;
    int _2577 = NOVALUE;
    int _2576 = NOVALUE;
    int _2575 = NOVALUE;
    int _2572 = NOVALUE;
    int _2571 = NOVALUE;
    int _2570 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_compfunc_5149)) {
        _1 = (long)(DBL_PTR(_compfunc_5149)->dbl);
        if (UNIQUE(DBL_PTR(_compfunc_5149)) && (DBL_PTR(_compfunc_5149)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_compfunc_5149);
        _compfunc_5149 = _1;
    }

    /** 	al = 1*/
    _al_5151 = 1;

    /** 	bl = 1*/
    _bl_5152 = 1;

    /** 	n = 1*/
    _n_5153 = 1;

    /** 	s = repeat(0, length(a) + length(b))*/
    if (IS_SEQUENCE(_a_5147)){
            _2570 = SEQ_PTR(_a_5147)->length;
    }
    else {
        _2570 = 1;
    }
    if (IS_SEQUENCE(_b_5148)){
            _2571 = SEQ_PTR(_b_5148)->length;
    }
    else {
        _2571 = 1;
    }
    _2572 = _2570 + _2571;
    _2570 = NOVALUE;
    _2571 = NOVALUE;
    DeRef(_s_5155);
    _s_5155 = Repeat(0, _2572);
    _2572 = NOVALUE;

    /** 	if compfunc >= 0 then*/
    if (_compfunc_5149 < 0)
    goto L1; // [40] 149

    /** 		while al <= length(a) and bl <= length(b) do*/
L2: 
    if (IS_SEQUENCE(_a_5147)){
            _2575 = SEQ_PTR(_a_5147)->length;
    }
    else {
        _2575 = 1;
    }
    _2576 = (_al_5151 <= _2575);
    _2575 = NOVALUE;
    if (_2576 == 0) {
        goto L3; // [56] 244
    }
    if (IS_SEQUENCE(_b_5148)){
            _2578 = SEQ_PTR(_b_5148)->length;
    }
    else {
        _2578 = 1;
    }
    _2579 = (_bl_5152 <= _2578);
    _2578 = NOVALUE;
    if (_2579 == 0)
    {
        DeRef(_2579);
        _2579 = NOVALUE;
        goto L3; // [68] 244
    }
    else{
        DeRef(_2579);
        _2579 = NOVALUE;
    }

    /** 			r = call_func(compfunc,{a[al], b[bl], userdata})*/
    _2 = (int)SEQ_PTR(_a_5147);
    _2580 = (int)*(((s1_ptr)_2)->base + _al_5151);
    _2 = (int)SEQ_PTR(_b_5148);
    _2581 = (int)*(((s1_ptr)_2)->base + _bl_5152);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_2580);
    *((int *)(_2+4)) = _2580;
    Ref(_2581);
    *((int *)(_2+8)) = _2581;
    Ref(_userdata_5150);
    *((int *)(_2+12)) = _userdata_5150;
    _2582 = MAKE_SEQ(_1);
    _2581 = NOVALUE;
    _2580 = NOVALUE;
    _1 = (int)SEQ_PTR(_2582);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_compfunc_5149].addr;
    Ref(*(int *)(_2+4));
    Ref(*(int *)(_2+8));
    Ref(*(int *)(_2+12));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4), 
                        *(int *)(_2+8), 
                        *(int *)(_2+12)
                         );
    _r_5154 = _1;
    DeRefDS(_2582);
    _2582 = NOVALUE;
    if (!IS_ATOM_INT(_r_5154)) {
        _1 = (long)(DBL_PTR(_r_5154)->dbl);
        if (UNIQUE(DBL_PTR(_r_5154)) && (DBL_PTR(_r_5154)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_r_5154);
        _r_5154 = _1;
    }

    /** 			if r <= 0 then*/
    if (_r_5154 > 0)
    goto L4; // [95] 118

    /** 				s[n] = a[al]*/
    _2 = (int)SEQ_PTR(_a_5147);
    _2585 = (int)*(((s1_ptr)_2)->base + _al_5151);
    Ref(_2585);
    _2 = (int)SEQ_PTR(_s_5155);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _s_5155 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _n_5153);
    _1 = *(int *)_2;
    *(int *)_2 = _2585;
    if( _1 != _2585 ){
        DeRef(_1);
    }
    _2585 = NOVALUE;

    /** 				al += 1*/
    _al_5151 = _al_5151 + 1;
    goto L5; // [115] 135
L4: 

    /** 				s[n] = b[bl]*/
    _2 = (int)SEQ_PTR(_b_5148);
    _2587 = (int)*(((s1_ptr)_2)->base + _bl_5152);
    Ref(_2587);
    _2 = (int)SEQ_PTR(_s_5155);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _s_5155 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _n_5153);
    _1 = *(int *)_2;
    *(int *)_2 = _2587;
    if( _1 != _2587 ){
        DeRef(_1);
    }
    _2587 = NOVALUE;

    /** 				bl += 1*/
    _bl_5152 = _bl_5152 + 1;
L5: 

    /** 			n += 1*/
    _n_5153 = _n_5153 + 1;

    /** 		end while*/
    goto L2; // [143] 49
    goto L3; // [146] 244
L1: 

    /** 		while al <= length(a) and bl <= length(b) do*/
L6: 
    if (IS_SEQUENCE(_a_5147)){
            _2590 = SEQ_PTR(_a_5147)->length;
    }
    else {
        _2590 = 1;
    }
    _2591 = (_al_5151 <= _2590);
    _2590 = NOVALUE;
    if (_2591 == 0) {
        goto L7; // [161] 243
    }
    if (IS_SEQUENCE(_b_5148)){
            _2593 = SEQ_PTR(_b_5148)->length;
    }
    else {
        _2593 = 1;
    }
    _2594 = (_bl_5152 <= _2593);
    _2593 = NOVALUE;
    if (_2594 == 0)
    {
        DeRef(_2594);
        _2594 = NOVALUE;
        goto L7; // [173] 243
    }
    else{
        DeRef(_2594);
        _2594 = NOVALUE;
    }

    /** 			r = compare(a[al], b[bl])*/
    _2 = (int)SEQ_PTR(_a_5147);
    _2595 = (int)*(((s1_ptr)_2)->base + _al_5151);
    _2 = (int)SEQ_PTR(_b_5148);
    _2596 = (int)*(((s1_ptr)_2)->base + _bl_5152);
    if (IS_ATOM_INT(_2595) && IS_ATOM_INT(_2596)){
        _r_5154 = (_2595 < _2596) ? -1 : (_2595 > _2596);
    }
    else{
        _r_5154 = compare(_2595, _2596);
    }
    _2595 = NOVALUE;
    _2596 = NOVALUE;

    /** 			if r <= 0 then*/
    if (_r_5154 > 0)
    goto L8; // [192] 215

    /** 				s[n] = a[al]*/
    _2 = (int)SEQ_PTR(_a_5147);
    _2599 = (int)*(((s1_ptr)_2)->base + _al_5151);
    Ref(_2599);
    _2 = (int)SEQ_PTR(_s_5155);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _s_5155 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _n_5153);
    _1 = *(int *)_2;
    *(int *)_2 = _2599;
    if( _1 != _2599 ){
        DeRef(_1);
    }
    _2599 = NOVALUE;

    /** 				al += 1*/
    _al_5151 = _al_5151 + 1;
    goto L9; // [212] 232
L8: 

    /** 				s[n] = b[bl]*/
    _2 = (int)SEQ_PTR(_b_5148);
    _2601 = (int)*(((s1_ptr)_2)->base + _bl_5152);
    Ref(_2601);
    _2 = (int)SEQ_PTR(_s_5155);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _s_5155 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _n_5153);
    _1 = *(int *)_2;
    *(int *)_2 = _2601;
    if( _1 != _2601 ){
        DeRef(_1);
    }
    _2601 = NOVALUE;

    /** 				bl += 1*/
    _bl_5152 = _bl_5152 + 1;
L9: 

    /** 			n += 1*/
    _n_5153 = _n_5153 + 1;

    /** 		end while*/
    goto L6; // [240] 154
L7: 
L3: 

    /** 	if al > length(a) then*/
    if (IS_SEQUENCE(_a_5147)){
            _2604 = SEQ_PTR(_a_5147)->length;
    }
    else {
        _2604 = 1;
    }
    if (_al_5151 <= _2604)
    goto LA; // [249] 274

    /** 		s[n .. $] = b[bl .. $]*/
    if (IS_SEQUENCE(_s_5155)){
            _2606 = SEQ_PTR(_s_5155)->length;
    }
    else {
        _2606 = 1;
    }
    if (IS_SEQUENCE(_b_5148)){
            _2607 = SEQ_PTR(_b_5148)->length;
    }
    else {
        _2607 = 1;
    }
    rhs_slice_target = (object_ptr)&_2608;
    RHS_Slice(_b_5148, _bl_5152, _2607);
    assign_slice_seq = (s1_ptr *)&_s_5155;
    AssignSlice(_n_5153, _2606, _2608);
    _2606 = NOVALUE;
    DeRefDS(_2608);
    _2608 = NOVALUE;
    goto LB; // [271] 303
LA: 

    /** 	elsif bl > length(b) then*/
    if (IS_SEQUENCE(_b_5148)){
            _2609 = SEQ_PTR(_b_5148)->length;
    }
    else {
        _2609 = 1;
    }
    if (_bl_5152 <= _2609)
    goto LC; // [279] 302

    /** 		s[n .. $] = a[al .. $]*/
    if (IS_SEQUENCE(_s_5155)){
            _2611 = SEQ_PTR(_s_5155)->length;
    }
    else {
        _2611 = 1;
    }
    if (IS_SEQUENCE(_a_5147)){
            _2612 = SEQ_PTR(_a_5147)->length;
    }
    else {
        _2612 = 1;
    }
    rhs_slice_target = (object_ptr)&_2613;
    RHS_Slice(_a_5147, _al_5151, _2612);
    assign_slice_seq = (s1_ptr *)&_s_5155;
    AssignSlice(_n_5153, _2611, _2613);
    _2611 = NOVALUE;
    DeRefDS(_2613);
    _2613 = NOVALUE;
LC: 
LB: 

    /** 	return s*/
    DeRefDS(_a_5147);
    DeRefDS(_b_5148);
    DeRef(_userdata_5150);
    DeRef(_2576);
    _2576 = NOVALUE;
    DeRef(_2591);
    _2591 = NOVALUE;
    return _s_5155;
    ;
}


int _22insertion_sort(int _s_5212, int _e_5213, int _compfunc_5214, int _userdata_5215)
{
    int _key_5216 = NOVALUE;
    int _a_5217 = NOVALUE;
    int _2657 = NOVALUE;
    int _2656 = NOVALUE;
    int _2655 = NOVALUE;
    int _2654 = NOVALUE;
    int _2653 = NOVALUE;
    int _2652 = NOVALUE;
    int _2648 = NOVALUE;
    int _2647 = NOVALUE;
    int _2646 = NOVALUE;
    int _2645 = NOVALUE;
    int _2643 = NOVALUE;
    int _2642 = NOVALUE;
    int _2641 = NOVALUE;
    int _2640 = NOVALUE;
    int _2639 = NOVALUE;
    int _2638 = NOVALUE;
    int _2637 = NOVALUE;
    int _2633 = NOVALUE;
    int _2632 = NOVALUE;
    int _2631 = NOVALUE;
    int _2628 = NOVALUE;
    int _2626 = NOVALUE;
    int _2625 = NOVALUE;
    int _2624 = NOVALUE;
    int _2623 = NOVALUE;
    int _2622 = NOVALUE;
    int _2621 = NOVALUE;
    int _2620 = NOVALUE;
    int _2619 = NOVALUE;
    int _2618 = NOVALUE;
    int _2616 = NOVALUE;
    int _2614 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_compfunc_5214)) {
        _1 = (long)(DBL_PTR(_compfunc_5214)->dbl);
        if (UNIQUE(DBL_PTR(_compfunc_5214)) && (DBL_PTR(_compfunc_5214)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_compfunc_5214);
        _compfunc_5214 = _1;
    }

    /** 	if atom(e) then*/
    _2614 = IS_ATOM(_e_5213);
    if (_2614 == 0)
    {
        _2614 = NOVALUE;
        goto L1; // [10] 22
    }
    else{
        _2614 = NOVALUE;
    }

    /** 		s &= e*/
    if (IS_SEQUENCE(_s_5212) && IS_ATOM(_e_5213)) {
        Ref(_e_5213);
        Append(&_s_5212, _s_5212, _e_5213);
    }
    else if (IS_ATOM(_s_5212) && IS_SEQUENCE(_e_5213)) {
    }
    else {
        Concat((object_ptr)&_s_5212, _s_5212, _e_5213);
    }
    goto L2; // [19] 78
L1: 

    /** 	elsif length(e) > 1 then*/
    if (IS_SEQUENCE(_e_5213)){
            _2616 = SEQ_PTR(_e_5213)->length;
    }
    else {
        _2616 = 1;
    }
    if (_2616 <= 1)
    goto L3; // [27] 77

    /** 		return merge(insertion_sort(s,,compfunc, userdata), insertion_sort(e,,compfunc, userdata), compfunc, userdata)*/
    RefDS(_s_5212);
    DeRef(_2618);
    _2618 = _s_5212;
    DeRef(_2619);
    _2619 = _compfunc_5214;
    Ref(_userdata_5215);
    DeRef(_2620);
    _2620 = _userdata_5215;
    RefDS(_5);
    _2621 = _22insertion_sort(_2618, _5, _2619, _2620);
    _2618 = NOVALUE;
    _2619 = NOVALUE;
    _2620 = NOVALUE;
    Ref(_e_5213);
    DeRef(_2622);
    _2622 = _e_5213;
    DeRef(_2623);
    _2623 = _compfunc_5214;
    Ref(_userdata_5215);
    DeRef(_2624);
    _2624 = _userdata_5215;
    RefDS(_5);
    _2625 = _22insertion_sort(_2622, _5, _2623, _2624);
    _2622 = NOVALUE;
    _2623 = NOVALUE;
    _2624 = NOVALUE;
    Ref(_userdata_5215);
    _2626 = _22merge(_2621, _2625, _compfunc_5214, _userdata_5215);
    _2621 = NOVALUE;
    _2625 = NOVALUE;
    DeRefDS(_s_5212);
    DeRef(_e_5213);
    DeRef(_userdata_5215);
    DeRef(_key_5216);
    return _2626;
L3: 
L2: 

    /** 	if compfunc = -1 then*/
    if (_compfunc_5214 != -1)
    goto L4; // [80] 225

    /** 		for j = 2 to length(s) label "outer" do*/
    if (IS_SEQUENCE(_s_5212)){
            _2628 = SEQ_PTR(_s_5212)->length;
    }
    else {
        _2628 = 1;
    }
    {
        int _j_5236;
        _j_5236 = 2;
L5: 
        if (_j_5236 > _2628){
            goto L6; // [89] 222
        }

        /** 			key = s[j]*/
        DeRef(_key_5216);
        _2 = (int)SEQ_PTR(_s_5212);
        _key_5216 = (int)*(((s1_ptr)_2)->base + _j_5236);
        Ref(_key_5216);

        /** 			for i = j - 1 to 1 by -1 do*/
        _2631 = _j_5236 - 1;
        {
            int _i_5241;
            _i_5241 = _2631;
L7: 
            if (_i_5241 < 1){
                goto L8; // [108] 187
            }

            /** 				if compare(s[i], key) <= 0 then*/
            _2 = (int)SEQ_PTR(_s_5212);
            _2632 = (int)*(((s1_ptr)_2)->base + _i_5241);
            if (IS_ATOM_INT(_2632) && IS_ATOM_INT(_key_5216)){
                _2633 = (_2632 < _key_5216) ? -1 : (_2632 > _key_5216);
            }
            else{
                _2633 = compare(_2632, _key_5216);
            }
            _2632 = NOVALUE;
            if (_2633 > 0)
            goto L9; // [125] 173

            /** 					a = i+1*/
            _a_5217 = _i_5241 + 1;

            /** 					if a != j then*/
            if (_a_5217 == _j_5236)
            goto LA; // [137] 217

            /** 						s[a+1 .. j] = s[a .. j-1]*/
            _2637 = _a_5217 + 1;
            _2638 = _j_5236 - 1;
            rhs_slice_target = (object_ptr)&_2639;
            RHS_Slice(_s_5212, _a_5217, _2638);
            assign_slice_seq = (s1_ptr *)&_s_5212;
            AssignSlice(_2637, _j_5236, _2639);
            _2637 = NOVALUE;
            DeRefDS(_2639);
            _2639 = NOVALUE;

            /** 						s[a] = key*/
            Ref(_key_5216);
            _2 = (int)SEQ_PTR(_s_5212);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_5212 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _a_5217);
            _1 = *(int *)_2;
            *(int *)_2 = _key_5216;
            DeRef(_1);

            /** 					continue "outer"*/
            goto LA; // [170] 217
L9: 

            /** 				a = i*/
            _a_5217 = _i_5241;

            /** 			end for*/
            _i_5241 = _i_5241 + -1;
            goto L7; // [182] 115
L8: 
            ;
        }

        /** 			s[a+1 .. j] = s[a .. j-1]*/
        _2640 = _a_5217 + 1;
        if (_2640 > MAXINT){
            _2640 = NewDouble((double)_2640);
        }
        _2641 = _j_5236 - 1;
        rhs_slice_target = (object_ptr)&_2642;
        RHS_Slice(_s_5212, _a_5217, _2641);
        assign_slice_seq = (s1_ptr *)&_s_5212;
        AssignSlice(_2640, _j_5236, _2642);
        DeRef(_2640);
        _2640 = NOVALUE;
        DeRefDS(_2642);
        _2642 = NOVALUE;

        /** 			s[a] = key*/
        Ref(_key_5216);
        _2 = (int)SEQ_PTR(_s_5212);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_5212 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _a_5217);
        _1 = *(int *)_2;
        *(int *)_2 = _key_5216;
        DeRef(_1);

        /** 		end for*/
LA: 
        _j_5236 = _j_5236 + 1;
        goto L5; // [217] 96
L6: 
        ;
    }
    goto LB; // [222] 370
L4: 

    /** 		for j = 2 to length(s) label "outer" do*/
    if (IS_SEQUENCE(_s_5212)){
            _2643 = SEQ_PTR(_s_5212)->length;
    }
    else {
        _2643 = 1;
    }
    {
        int _j_5258;
        _j_5258 = 2;
LC: 
        if (_j_5258 > _2643){
            goto LD; // [230] 369
        }

        /** 			key = s[j]*/
        DeRef(_key_5216);
        _2 = (int)SEQ_PTR(_s_5212);
        _key_5216 = (int)*(((s1_ptr)_2)->base + _j_5258);
        Ref(_key_5216);

        /** 			for i = j - 1 to 1 by -1 do*/
        _2645 = _j_5258 - 1;
        {
            int _i_5262;
            _i_5262 = _2645;
LE: 
            if (_i_5262 < 1){
                goto LF; // [249] 334
            }

            /** 				if call_func(compfunc,{s[i], key, userdata}) <= 0 then*/
            _2 = (int)SEQ_PTR(_s_5212);
            _2646 = (int)*(((s1_ptr)_2)->base + _i_5262);
            _1 = NewS1(3);
            _2 = (int)((s1_ptr)_1)->base;
            Ref(_2646);
            *((int *)(_2+4)) = _2646;
            Ref(_key_5216);
            *((int *)(_2+8)) = _key_5216;
            Ref(_userdata_5215);
            *((int *)(_2+12)) = _userdata_5215;
            _2647 = MAKE_SEQ(_1);
            _2646 = NOVALUE;
            _1 = (int)SEQ_PTR(_2647);
            _2 = (int)((s1_ptr)_1)->base;
            _0 = (int)_00[_compfunc_5214].addr;
            Ref(*(int *)(_2+4));
            Ref(*(int *)(_2+8));
            Ref(*(int *)(_2+12));
            _1 = (*(int (*)())_0)(
                                *(int *)(_2+4), 
                                *(int *)(_2+8), 
                                *(int *)(_2+12)
                                 );
            DeRef(_2648);
            _2648 = _1;
            DeRefDS(_2647);
            _2647 = NOVALUE;
            if (binary_op_a(GREATER, _2648, 0)){
                DeRef(_2648);
                _2648 = NOVALUE;
                goto L10; // [272] 320
            }
            DeRef(_2648);
            _2648 = NOVALUE;

            /** 					a = i+1*/
            _a_5217 = _i_5262 + 1;

            /** 					if a != j then*/
            if (_a_5217 == _j_5258)
            goto L11; // [284] 364

            /** 						s[a+1 .. j] = s[a .. j-1]*/
            _2652 = _a_5217 + 1;
            _2653 = _j_5258 - 1;
            rhs_slice_target = (object_ptr)&_2654;
            RHS_Slice(_s_5212, _a_5217, _2653);
            assign_slice_seq = (s1_ptr *)&_s_5212;
            AssignSlice(_2652, _j_5258, _2654);
            _2652 = NOVALUE;
            DeRefDS(_2654);
            _2654 = NOVALUE;

            /** 						s[a] = key*/
            Ref(_key_5216);
            _2 = (int)SEQ_PTR(_s_5212);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_5212 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _a_5217);
            _1 = *(int *)_2;
            *(int *)_2 = _key_5216;
            DeRef(_1);

            /** 					continue "outer"*/
            goto L11; // [317] 364
L10: 

            /** 				a = i*/
            _a_5217 = _i_5262;

            /** 			end for*/
            _i_5262 = _i_5262 + -1;
            goto LE; // [329] 256
LF: 
            ;
        }

        /** 			s[a+1 .. j] = s[a .. j-1]*/
        _2655 = _a_5217 + 1;
        if (_2655 > MAXINT){
            _2655 = NewDouble((double)_2655);
        }
        _2656 = _j_5258 - 1;
        rhs_slice_target = (object_ptr)&_2657;
        RHS_Slice(_s_5212, _a_5217, _2656);
        assign_slice_seq = (s1_ptr *)&_s_5212;
        AssignSlice(_2655, _j_5258, _2657);
        DeRef(_2655);
        _2655 = NOVALUE;
        DeRefDS(_2657);
        _2657 = NOVALUE;

        /** 			s[a] = key*/
        Ref(_key_5216);
        _2 = (int)SEQ_PTR(_s_5212);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_5212 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _a_5217);
        _1 = *(int *)_2;
        *(int *)_2 = _key_5216;
        DeRef(_1);

        /** 		end for*/
L11: 
        _j_5258 = _j_5258 + 1;
        goto LC; // [364] 237
LD: 
        ;
    }
LB: 

    /** 	return s*/
    DeRef(_e_5213);
    DeRef(_userdata_5215);
    DeRef(_key_5216);
    DeRef(_2631);
    _2631 = NOVALUE;
    DeRef(_2645);
    _2645 = NOVALUE;
    DeRef(_2626);
    _2626 = NOVALUE;
    DeRef(_2638);
    _2638 = NOVALUE;
    DeRef(_2641);
    _2641 = NOVALUE;
    DeRef(_2653);
    _2653 = NOVALUE;
    DeRef(_2656);
    _2656 = NOVALUE;
    return _s_5212;
    ;
}



// 0x897ED079
