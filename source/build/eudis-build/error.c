// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _8crash(int _fmt_1604, int _data_1605)
{
    int _msg_1606 = NOVALUE;
    int _0, _1, _2;
    

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_1606);
    _msg_1606 = EPrintf(-9999999, _fmt_1604, _data_1605);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_1606);

    /** end procedure*/
    DeRefDS(_fmt_1604);
    DeRef(_data_1605);
    DeRefDSi(_msg_1606);
    return;
    ;
}


void _8crash_message(int _msg_1610)
{
    int _0, _1, _2;
    

    /** 	machine_proc(M_CRASH_MESSAGE, msg)*/
    machine(37, _msg_1610);

    /** end procedure*/
    DeRefDS(_msg_1610);
    return;
    ;
}


void _8crash_file(int _file_path_1613)
{
    int _0, _1, _2;
    

    /** 	machine_proc(M_CRASH_FILE, file_path)*/
    machine(57, _file_path_1613);

    /** end procedure*/
    DeRefDS(_file_path_1613);
    return;
    ;
}


void _8warning_file(int _file_path_1616)
{
    int _0, _1, _2;
    

    /** 	machine_proc(M_WARNING_FILE, file_path)*/
    machine(72, _file_path_1616);

    /** end procedure*/
    DeRef(_file_path_1616);
    return;
    ;
}


void _8crash_routine(int _func_1619)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_func_1619)) {
        _1 = (long)(DBL_PTR(_func_1619)->dbl);
        if (UNIQUE(DBL_PTR(_func_1619)) && (DBL_PTR(_func_1619)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_func_1619);
        _func_1619 = _1;
    }

    /** 	machine_proc(M_CRASH_ROUTINE, func)*/
    machine(66, _func_1619);

    /** end procedure*/
    return;
    ;
}



// 0xD0E989A9
