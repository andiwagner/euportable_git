CC     = gcc
CFLAGS =  -DEWINDOWS -fomit-frame-pointer -c -w -fsigned-char -O2 -m32 -Ic:/develop/offEu/euphoria -ffast-math
LINKER = gcc
LFLAGS = c:/develop/offEu/euphoria/source/build/eu.a -m32 
DIS_SOURCES = init-.c dis.c main-.c mode.c wildcard.c text.c types.c convert.c search.c error.c filesys.c datetime.c dll.c machine.c memconst.c memory.c get.c io.c math.c rand.c sequence.c sort.c pretty.c serialize.c global.c common.c fwdref.c parser.c info.c map.c eumem.c primes.c stats.c platform.c emit.c pathopen.c cominit.c cmdline.c console.c graphcst.c 0rror.c msgtext.c locale.c eds.c coverage.c regex.c symtab.c c_out.c buildsys.c utils.c c_decl.c compile.c compress.c scanner.c scinot.c fenv.c keylist.c preproc.c shift.c block.c inline.c intinit.c traninit.c 0is.c dot.c dox.c main.c init-0.c
DIS_OBJECTS = init-.o dis.o main-.o mode.o wildcard.o text.o types.o convert.o search.o error.o filesys.o datetime.o dll.o machine.o memconst.o memory.o get.o io.o math.o rand.o sequence.o sort.o pretty.o serialize.o global.o common.o fwdref.o parser.o info.o map.o eumem.o primes.o stats.o platform.o emit.o pathopen.o cominit.o cmdline.o console.o graphcst.o 0rror.o msgtext.o locale.o eds.o coverage.o regex.o symtab.o c_out.o buildsys.o utils.o c_decl.o compile.o compress.o scanner.o scinot.o fenv.o keylist.o preproc.o shift.o block.o inline.o intinit.o traninit.o 0is.o dot.o dox.o main.o init-0.o
DIS_GENERATED_FILES =  init-.c init-.o main-.h dis.c dis.o main-.c main-.o mode.c mode.o wildcard.c wildcard.o text.c text.o types.c types.o convert.c convert.o search.c search.o error.c error.o filesys.c filesys.o datetime.c datetime.o dll.c dll.o machine.c machine.o memconst.c memconst.o memory.c memory.o get.c get.o io.c io.o math.c math.o rand.c rand.o sequence.c sequence.o sort.c sort.o pretty.c pretty.o serialize.c serialize.o global.c global.o common.c common.o fwdref.c fwdref.o parser.c parser.o info.c info.o map.c map.o eumem.c eumem.o primes.c primes.o stats.c stats.o platform.c platform.o emit.c emit.o pathopen.c pathopen.o cominit.c cominit.o cmdline.c cmdline.o console.c console.o graphcst.c graphcst.o 0rror.c 0rror.o msgtext.c msgtext.o locale.c locale.o eds.c eds.o coverage.c coverage.o regex.c regex.o symtab.c symtab.o c_out.c c_out.o buildsys.c buildsys.o utils.c utils.o c_decl.c c_decl.o compile.c compile.o compress.c compress.o scanner.c scanner.o scinot.c scinot.o fenv.c fenv.o keylist.c keylist.o preproc.c preproc.o shift.c shift.o block.c block.o inline.c inline.o intinit.c intinit.o traninit.c traninit.o 0is.c 0is.o dot.c dot.o dox.c dox.o main.c main.o init-0.c init-0.o

c:/develop/offEu/euphoria/source/build/eudis: $(DIS_OBJECTS) c:/develop/offEu/euphoria/source/build/eu.a 
	$(LINKER) -o c:/develop/offEu/euphoria/source/build/eudis $(DIS_OBJECTS)  $(LFLAGS)

.PHONY: dis-clean dis-clean-all

dis-clean:
	rm -rf $(DIS_OBJECTS) 

dis-clean-all: dis-clean
	rm -rf $(DIS_SOURCES)  c:/develop/offEu/euphoria/source/build/eudis

%.o: %.c
	$(CC) $(CFLAGS) $*.c -o $*.o

