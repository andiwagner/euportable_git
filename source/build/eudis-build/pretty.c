// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _23pretty_out(int _text_8558)
{
    int _4631 = NOVALUE;
    int _4629 = NOVALUE;
    int _4627 = NOVALUE;
    int _4626 = NOVALUE;
    int _0, _1, _2;
    

    /** 	pretty_line &= text*/
    if (IS_SEQUENCE(_23pretty_line_8555) && IS_ATOM(_text_8558)) {
        Ref(_text_8558);
        Append(&_23pretty_line_8555, _23pretty_line_8555, _text_8558);
    }
    else if (IS_ATOM(_23pretty_line_8555) && IS_SEQUENCE(_text_8558)) {
    }
    else {
        Concat((object_ptr)&_23pretty_line_8555, _23pretty_line_8555, _text_8558);
    }

    /** 	if equal(text, '\n') and pretty_printing then*/
    if (_text_8558 == 10)
    _4626 = 1;
    else if (IS_ATOM_INT(_text_8558) && IS_ATOM_INT(10))
    _4626 = 0;
    else
    _4626 = (compare(_text_8558, 10) == 0);
    if (_4626 == 0) {
        goto L1; // [15] 50
    }
    if (_23pretty_printing_8552 == 0)
    {
        goto L1; // [22] 50
    }
    else{
    }

    /** 		puts(pretty_file, pretty_line)*/
    EPuts(_23pretty_file_8543, _23pretty_line_8555); // DJP 

    /** 		pretty_line = ""*/
    RefDS(_5);
    DeRefDS(_23pretty_line_8555);
    _23pretty_line_8555 = _5;

    /** 		pretty_line_count += 1*/
    _23pretty_line_count_8548 = _23pretty_line_count_8548 + 1;
L1: 

    /** 	if atom(text) then*/
    _4629 = IS_ATOM(_text_8558);
    if (_4629 == 0)
    {
        _4629 = NOVALUE;
        goto L2; // [55] 69
    }
    else{
        _4629 = NOVALUE;
    }

    /** 		pretty_chars += 1*/
    _23pretty_chars_8540 = _23pretty_chars_8540 + 1;
    goto L3; // [66] 81
L2: 

    /** 		pretty_chars += length(text)*/
    if (IS_SEQUENCE(_text_8558)){
            _4631 = SEQ_PTR(_text_8558)->length;
    }
    else {
        _4631 = 1;
    }
    _23pretty_chars_8540 = _23pretty_chars_8540 + _4631;
    _4631 = NOVALUE;
L3: 

    /** end procedure*/
    DeRef(_text_8558);
    return;
    ;
}


void _23cut_line(int _n_8572)
{
    int _4634 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not pretty_line_breaks then	*/
    if (_23pretty_line_breaks_8551 != 0)
    goto L1; // [7] 21

    /** 		pretty_chars = 0*/
    _23pretty_chars_8540 = 0;

    /** 		return*/
    return;
L1: 

    /** 	if pretty_chars + n > pretty_end_col then*/
    _4634 = _23pretty_chars_8540 + _n_8572;
    if ((long)((unsigned long)_4634 + (unsigned long)HIGH_BITS) >= 0) 
    _4634 = NewDouble((double)_4634);
    if (binary_op_a(LESSEQ, _4634, _23pretty_end_col_8539)){
        DeRef(_4634);
        _4634 = NOVALUE;
        goto L2; // [31] 46
    }
    DeRef(_4634);
    _4634 = NOVALUE;

    /** 		pretty_out('\n')*/
    _23pretty_out(10);

    /** 		pretty_chars = 0*/
    _23pretty_chars_8540 = 0;
L2: 

    /** end procedure*/
    return;
    ;
}


void _23indent()
{
    int _4642 = NOVALUE;
    int _4641 = NOVALUE;
    int _4640 = NOVALUE;
    int _4639 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if pretty_line_breaks = 0 then	*/
    if (_23pretty_line_breaks_8551 != 0)
    goto L1; // [5] 22

    /** 		pretty_chars = 0*/
    _23pretty_chars_8540 = 0;

    /** 		return*/
    return;
    goto L2; // [19] 85
L1: 

    /** 	elsif pretty_line_breaks = -1 then*/
    if (_23pretty_line_breaks_8551 != -1)
    goto L3; // [26] 38

    /** 		cut_line( 0 )*/
    _23cut_line(0);
    goto L2; // [35] 85
L3: 

    /** 		if pretty_chars > 0 then*/
    if (_23pretty_chars_8540 <= 0)
    goto L4; // [42] 57

    /** 			pretty_out('\n')*/
    _23pretty_out(10);

    /** 			pretty_chars = 0*/
    _23pretty_chars_8540 = 0;
L4: 

    /** 		pretty_out(repeat(' ', (pretty_start_col-1) + */
    _4639 = _23pretty_start_col_8541 - 1;
    if ((long)((unsigned long)_4639 +(unsigned long) HIGH_BITS) >= 0){
        _4639 = NewDouble((double)_4639);
    }
    if (_23pretty_level_8542 == (short)_23pretty_level_8542 && _23pretty_indent_8545 <= INT15 && _23pretty_indent_8545 >= -INT15)
    _4640 = _23pretty_level_8542 * _23pretty_indent_8545;
    else
    _4640 = NewDouble(_23pretty_level_8542 * (double)_23pretty_indent_8545);
    if (IS_ATOM_INT(_4639) && IS_ATOM_INT(_4640)) {
        _4641 = _4639 + _4640;
    }
    else {
        if (IS_ATOM_INT(_4639)) {
            _4641 = NewDouble((double)_4639 + DBL_PTR(_4640)->dbl);
        }
        else {
            if (IS_ATOM_INT(_4640)) {
                _4641 = NewDouble(DBL_PTR(_4639)->dbl + (double)_4640);
            }
            else
            _4641 = NewDouble(DBL_PTR(_4639)->dbl + DBL_PTR(_4640)->dbl);
        }
    }
    DeRef(_4639);
    _4639 = NOVALUE;
    DeRef(_4640);
    _4640 = NOVALUE;
    _4642 = Repeat(32, _4641);
    DeRef(_4641);
    _4641 = NOVALUE;
    _23pretty_out(_4642);
    _4642 = NOVALUE;
L2: 

    /** end procedure*/
    return;
    ;
}


int _23esc_char(int _a_8593)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_a_8593)) {
        _1 = (long)(DBL_PTR(_a_8593)->dbl);
        if (UNIQUE(DBL_PTR(_a_8593)) && (DBL_PTR(_a_8593)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_a_8593);
        _a_8593 = _1;
    }

    /** 	switch a do*/
    _0 = _a_8593;
    switch ( _0 ){ 

        /** 		case'\t' then*/
        case 9:

        /** 			return `\t`*/
        RefDS(_4645);
        return _4645;
        goto L1; // [20] 81

        /** 		case'\n' then*/
        case 10:

        /** 			return `\n`*/
        RefDS(_4646);
        return _4646;
        goto L1; // [32] 81

        /** 		case'\r' then*/
        case 13:

        /** 			return `\r`*/
        RefDS(_4647);
        return _4647;
        goto L1; // [44] 81

        /** 		case'\\' then*/
        case 92:

        /** 			return `\\`*/
        RefDS(_732);
        return _732;
        goto L1; // [56] 81

        /** 		case'"' then*/
        case 34:

        /** 			return `\"`*/
        RefDS(_4648);
        return _4648;
        goto L1; // [68] 81

        /** 		case else*/
        default:

        /** 			return a*/
        return _a_8593;
    ;}L1: 
    ;
}


void _23rPrint(int _a_8608)
{
    int _sbuff_8609 = NOVALUE;
    int _multi_line_8610 = NOVALUE;
    int _all_ascii_8611 = NOVALUE;
    int _4704 = NOVALUE;
    int _4703 = NOVALUE;
    int _4702 = NOVALUE;
    int _4701 = NOVALUE;
    int _4697 = NOVALUE;
    int _4696 = NOVALUE;
    int _4695 = NOVALUE;
    int _4694 = NOVALUE;
    int _4692 = NOVALUE;
    int _4691 = NOVALUE;
    int _4689 = NOVALUE;
    int _4688 = NOVALUE;
    int _4686 = NOVALUE;
    int _4685 = NOVALUE;
    int _4684 = NOVALUE;
    int _4683 = NOVALUE;
    int _4682 = NOVALUE;
    int _4681 = NOVALUE;
    int _4680 = NOVALUE;
    int _4679 = NOVALUE;
    int _4678 = NOVALUE;
    int _4677 = NOVALUE;
    int _4676 = NOVALUE;
    int _4675 = NOVALUE;
    int _4674 = NOVALUE;
    int _4673 = NOVALUE;
    int _4672 = NOVALUE;
    int _4671 = NOVALUE;
    int _4670 = NOVALUE;
    int _4666 = NOVALUE;
    int _4665 = NOVALUE;
    int _4664 = NOVALUE;
    int _4663 = NOVALUE;
    int _4662 = NOVALUE;
    int _4661 = NOVALUE;
    int _4659 = NOVALUE;
    int _4658 = NOVALUE;
    int _4655 = NOVALUE;
    int _4654 = NOVALUE;
    int _4653 = NOVALUE;
    int _4650 = NOVALUE;
    int _4649 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _4649 = IS_ATOM(_a_8608);
    if (_4649 == 0)
    {
        _4649 = NOVALUE;
        goto L1; // [6] 176
    }
    else{
        _4649 = NOVALUE;
    }

    /** 		if integer(a) then*/
    if (IS_ATOM_INT(_a_8608))
    _4650 = 1;
    else if (IS_ATOM_DBL(_a_8608))
    _4650 = IS_ATOM_INT(DoubleToInt(_a_8608));
    else
    _4650 = 0;
    if (_4650 == 0)
    {
        _4650 = NOVALUE;
        goto L2; // [14] 157
    }
    else{
        _4650 = NOVALUE;
    }

    /** 			sbuff = sprintf(pretty_int_format, a)*/
    DeRef(_sbuff_8609);
    _sbuff_8609 = EPrintf(-9999999, _23pretty_int_format_8554, _a_8608);

    /** 			if pretty_ascii then */
    if (_23pretty_ascii_8544 == 0)
    {
        goto L3; // [29] 166
    }
    else{
    }

    /** 				if pretty_ascii >= 3 then */
    if (_23pretty_ascii_8544 < 3)
    goto L4; // [36] 103

    /** 					if (a >= pretty_ascii_min and a <= pretty_ascii_max) then*/
    if (IS_ATOM_INT(_a_8608)) {
        _4653 = (_a_8608 >= _23pretty_ascii_min_8546);
    }
    else {
        _4653 = binary_op(GREATEREQ, _a_8608, _23pretty_ascii_min_8546);
    }
    if (IS_ATOM_INT(_4653)) {
        if (_4653 == 0) {
            _4654 = 0;
            goto L5; // [48] 62
        }
    }
    else {
        if (DBL_PTR(_4653)->dbl == 0.0) {
            _4654 = 0;
            goto L5; // [48] 62
        }
    }
    if (IS_ATOM_INT(_a_8608)) {
        _4655 = (_a_8608 <= _23pretty_ascii_max_8547);
    }
    else {
        _4655 = binary_op(LESSEQ, _a_8608, _23pretty_ascii_max_8547);
    }
    DeRef(_4654);
    if (IS_ATOM_INT(_4655))
    _4654 = (_4655 != 0);
    else
    _4654 = DBL_PTR(_4655)->dbl != 0.0;
L5: 
    if (_4654 == 0)
    {
        _4654 = NOVALUE;
        goto L6; // [62] 76
    }
    else{
        _4654 = NOVALUE;
    }

    /** 						sbuff = '\'' & a & '\''  -- display char only*/
    {
        int concat_list[3];

        concat_list[0] = 39;
        concat_list[1] = _a_8608;
        concat_list[2] = 39;
        Concat_N((object_ptr)&_sbuff_8609, concat_list, 3);
    }
    goto L3; // [73] 166
L6: 

    /** 					elsif find(a, "\t\n\r\\") then*/
    _4658 = find_from(_a_8608, _4657, 1);
    if (_4658 == 0)
    {
        _4658 = NOVALUE;
        goto L3; // [83] 166
    }
    else{
        _4658 = NOVALUE;
    }

    /** 						sbuff = '\'' & esc_char(a) & '\''  -- display char only*/
    Ref(_a_8608);
    _4659 = _23esc_char(_a_8608);
    {
        int concat_list[3];

        concat_list[0] = 39;
        concat_list[1] = _4659;
        concat_list[2] = 39;
        Concat_N((object_ptr)&_sbuff_8609, concat_list, 3);
    }
    DeRef(_4659);
    _4659 = NOVALUE;
    goto L3; // [100] 166
L4: 

    /** 					if (a >= pretty_ascii_min and a <= pretty_ascii_max) and pretty_ascii < 2 then*/
    if (IS_ATOM_INT(_a_8608)) {
        _4661 = (_a_8608 >= _23pretty_ascii_min_8546);
    }
    else {
        _4661 = binary_op(GREATEREQ, _a_8608, _23pretty_ascii_min_8546);
    }
    if (IS_ATOM_INT(_4661)) {
        if (_4661 == 0) {
            DeRef(_4662);
            _4662 = 0;
            goto L7; // [111] 125
        }
    }
    else {
        if (DBL_PTR(_4661)->dbl == 0.0) {
            DeRef(_4662);
            _4662 = 0;
            goto L7; // [111] 125
        }
    }
    if (IS_ATOM_INT(_a_8608)) {
        _4663 = (_a_8608 <= _23pretty_ascii_max_8547);
    }
    else {
        _4663 = binary_op(LESSEQ, _a_8608, _23pretty_ascii_max_8547);
    }
    DeRef(_4662);
    if (IS_ATOM_INT(_4663))
    _4662 = (_4663 != 0);
    else
    _4662 = DBL_PTR(_4663)->dbl != 0.0;
L7: 
    if (_4662 == 0) {
        goto L3; // [125] 166
    }
    _4665 = (_23pretty_ascii_8544 < 2);
    if (_4665 == 0)
    {
        DeRef(_4665);
        _4665 = NOVALUE;
        goto L3; // [136] 166
    }
    else{
        DeRef(_4665);
        _4665 = NOVALUE;
    }

    /** 						sbuff &= '\'' & a & '\'' -- add to numeric display*/
    {
        int concat_list[3];

        concat_list[0] = 39;
        concat_list[1] = _a_8608;
        concat_list[2] = 39;
        Concat_N((object_ptr)&_4666, concat_list, 3);
    }
    Concat((object_ptr)&_sbuff_8609, _sbuff_8609, _4666);
    DeRefDS(_4666);
    _4666 = NOVALUE;
    goto L3; // [154] 166
L2: 

    /** 			sbuff = sprintf(pretty_fp_format, a)*/
    DeRef(_sbuff_8609);
    _sbuff_8609 = EPrintf(-9999999, _23pretty_fp_format_8553, _a_8608);
L3: 

    /** 		pretty_out(sbuff)*/
    RefDS(_sbuff_8609);
    _23pretty_out(_sbuff_8609);
    goto L8; // [173] 535
L1: 

    /** 		cut_line(1)*/
    _23cut_line(1);

    /** 		multi_line = 0*/
    _multi_line_8610 = 0;

    /** 		all_ascii = pretty_ascii > 1*/
    _all_ascii_8611 = (_23pretty_ascii_8544 > 1);

    /** 		for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_8608)){
            _4670 = SEQ_PTR(_a_8608)->length;
    }
    else {
        _4670 = 1;
    }
    {
        int _i_8644;
        _i_8644 = 1;
L9: 
        if (_i_8644 > _4670){
            goto LA; // [199] 345
        }

        /** 			if sequence(a[i]) and length(a[i]) > 0 then*/
        _2 = (int)SEQ_PTR(_a_8608);
        _4671 = (int)*(((s1_ptr)_2)->base + _i_8644);
        _4672 = IS_SEQUENCE(_4671);
        _4671 = NOVALUE;
        if (_4672 == 0) {
            goto LB; // [215] 249
        }
        _2 = (int)SEQ_PTR(_a_8608);
        _4674 = (int)*(((s1_ptr)_2)->base + _i_8644);
        if (IS_SEQUENCE(_4674)){
                _4675 = SEQ_PTR(_4674)->length;
        }
        else {
            _4675 = 1;
        }
        _4674 = NOVALUE;
        _4676 = (_4675 > 0);
        _4675 = NOVALUE;
        if (_4676 == 0)
        {
            DeRef(_4676);
            _4676 = NOVALUE;
            goto LB; // [231] 249
        }
        else{
            DeRef(_4676);
            _4676 = NOVALUE;
        }

        /** 				multi_line = 1*/
        _multi_line_8610 = 1;

        /** 				all_ascii = 0*/
        _all_ascii_8611 = 0;

        /** 				exit*/
        goto LA; // [246] 345
LB: 

        /** 			if not integer(a[i]) or*/
        _2 = (int)SEQ_PTR(_a_8608);
        _4677 = (int)*(((s1_ptr)_2)->base + _i_8644);
        if (IS_ATOM_INT(_4677))
        _4678 = 1;
        else if (IS_ATOM_DBL(_4677))
        _4678 = IS_ATOM_INT(DoubleToInt(_4677));
        else
        _4678 = 0;
        _4677 = NOVALUE;
        _4679 = (_4678 == 0);
        _4678 = NOVALUE;
        if (_4679 != 0) {
            _4680 = 1;
            goto LC; // [261] 313
        }
        _2 = (int)SEQ_PTR(_a_8608);
        _4681 = (int)*(((s1_ptr)_2)->base + _i_8644);
        if (IS_ATOM_INT(_4681)) {
            _4682 = (_4681 < _23pretty_ascii_min_8546);
        }
        else {
            _4682 = binary_op(LESS, _4681, _23pretty_ascii_min_8546);
        }
        _4681 = NOVALUE;
        if (IS_ATOM_INT(_4682)) {
            if (_4682 == 0) {
                DeRef(_4683);
                _4683 = 0;
                goto LD; // [275] 309
            }
        }
        else {
            if (DBL_PTR(_4682)->dbl == 0.0) {
                DeRef(_4683);
                _4683 = 0;
                goto LD; // [275] 309
            }
        }
        _4684 = (_23pretty_ascii_8544 < 2);
        if (_4684 != 0) {
            _4685 = 1;
            goto LE; // [285] 305
        }
        _2 = (int)SEQ_PTR(_a_8608);
        _4686 = (int)*(((s1_ptr)_2)->base + _i_8644);
        _4688 = find_from(_4686, _4687, 1);
        _4686 = NOVALUE;
        _4689 = (_4688 == 0);
        _4688 = NOVALUE;
        _4685 = (_4689 != 0);
LE: 
        DeRef(_4683);
        _4683 = (_4685 != 0);
LD: 
        _4680 = (_4683 != 0);
LC: 
        if (_4680 != 0) {
            goto LF; // [313] 332
        }
        _2 = (int)SEQ_PTR(_a_8608);
        _4691 = (int)*(((s1_ptr)_2)->base + _i_8644);
        if (IS_ATOM_INT(_4691)) {
            _4692 = (_4691 > _23pretty_ascii_max_8547);
        }
        else {
            _4692 = binary_op(GREATER, _4691, _23pretty_ascii_max_8547);
        }
        _4691 = NOVALUE;
        if (_4692 == 0) {
            DeRef(_4692);
            _4692 = NOVALUE;
            goto L10; // [328] 338
        }
        else {
            if (!IS_ATOM_INT(_4692) && DBL_PTR(_4692)->dbl == 0.0){
                DeRef(_4692);
                _4692 = NOVALUE;
                goto L10; // [328] 338
            }
            DeRef(_4692);
            _4692 = NOVALUE;
        }
        DeRef(_4692);
        _4692 = NOVALUE;
LF: 

        /** 				all_ascii = 0*/
        _all_ascii_8611 = 0;
L10: 

        /** 		end for*/
        _i_8644 = _i_8644 + 1;
        goto L9; // [340] 206
LA: 
        ;
    }

    /** 		if all_ascii then*/
    if (_all_ascii_8611 == 0)
    {
        goto L11; // [347] 358
    }
    else{
    }

    /** 			pretty_out('\"')*/
    _23pretty_out(34);
    goto L12; // [355] 364
L11: 

    /** 			pretty_out('{')*/
    _23pretty_out(123);
L12: 

    /** 		pretty_level += 1*/
    _23pretty_level_8542 = _23pretty_level_8542 + 1;

    /** 		for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_8608)){
            _4694 = SEQ_PTR(_a_8608)->length;
    }
    else {
        _4694 = 1;
    }
    {
        int _i_8674;
        _i_8674 = 1;
L13: 
        if (_i_8674 > _4694){
            goto L14; // [377] 497
        }

        /** 			if multi_line then*/
        if (_multi_line_8610 == 0)
        {
            goto L15; // [386] 394
        }
        else{
        }

        /** 				indent()*/
        _23indent();
L15: 

        /** 			if all_ascii then*/
        if (_all_ascii_8611 == 0)
        {
            goto L16; // [396] 415
        }
        else{
        }

        /** 				pretty_out(esc_char(a[i]))*/
        _2 = (int)SEQ_PTR(_a_8608);
        _4695 = (int)*(((s1_ptr)_2)->base + _i_8674);
        Ref(_4695);
        _4696 = _23esc_char(_4695);
        _4695 = NOVALUE;
        _23pretty_out(_4696);
        _4696 = NOVALUE;
        goto L17; // [412] 425
L16: 

        /** 				rPrint(a[i])*/
        _2 = (int)SEQ_PTR(_a_8608);
        _4697 = (int)*(((s1_ptr)_2)->base + _i_8674);
        Ref(_4697);
        _23rPrint(_4697);
        _4697 = NOVALUE;
L17: 

        /** 			if pretty_line_count >= pretty_line_max then*/
        if (_23pretty_line_count_8548 < _23pretty_line_max_8549)
        goto L18; // [431] 459

        /** 				if not pretty_dots then*/
        if (_23pretty_dots_8550 != 0)
        goto L19; // [439] 448

        /** 					pretty_out(" ...")*/
        RefDS(_4700);
        _23pretty_out(_4700);
L19: 

        /** 				pretty_dots = 1*/
        _23pretty_dots_8550 = 1;

        /** 				return*/
        DeRef(_a_8608);
        DeRef(_sbuff_8609);
        DeRef(_4653);
        _4653 = NOVALUE;
        DeRef(_4655);
        _4655 = NOVALUE;
        DeRef(_4661);
        _4661 = NOVALUE;
        DeRef(_4663);
        _4663 = NOVALUE;
        _4674 = NOVALUE;
        DeRef(_4679);
        _4679 = NOVALUE;
        DeRef(_4684);
        _4684 = NOVALUE;
        DeRef(_4682);
        _4682 = NOVALUE;
        DeRef(_4689);
        _4689 = NOVALUE;
        return;
L18: 

        /** 			if i != length(a) and not all_ascii then*/
        if (IS_SEQUENCE(_a_8608)){
                _4701 = SEQ_PTR(_a_8608)->length;
        }
        else {
            _4701 = 1;
        }
        _4702 = (_i_8674 != _4701);
        _4701 = NOVALUE;
        if (_4702 == 0) {
            goto L1A; // [468] 490
        }
        _4704 = (_all_ascii_8611 == 0);
        if (_4704 == 0)
        {
            DeRef(_4704);
            _4704 = NOVALUE;
            goto L1A; // [476] 490
        }
        else{
            DeRef(_4704);
            _4704 = NOVALUE;
        }

        /** 				pretty_out(',')*/
        _23pretty_out(44);

        /** 				cut_line(6)*/
        _23cut_line(6);
L1A: 

        /** 		end for*/
        _i_8674 = _i_8674 + 1;
        goto L13; // [492] 384
L14: 
        ;
    }

    /** 		pretty_level -= 1*/
    _23pretty_level_8542 = _23pretty_level_8542 - 1;

    /** 		if multi_line then*/
    if (_multi_line_8610 == 0)
    {
        goto L1B; // [507] 515
    }
    else{
    }

    /** 			indent()*/
    _23indent();
L1B: 

    /** 		if all_ascii then*/
    if (_all_ascii_8611 == 0)
    {
        goto L1C; // [517] 528
    }
    else{
    }

    /** 			pretty_out('\"')*/
    _23pretty_out(34);
    goto L1D; // [525] 534
L1C: 

    /** 			pretty_out('}')*/
    _23pretty_out(125);
L1D: 
L8: 

    /** end procedure*/
    DeRef(_a_8608);
    DeRef(_sbuff_8609);
    DeRef(_4653);
    _4653 = NOVALUE;
    DeRef(_4655);
    _4655 = NOVALUE;
    DeRef(_4661);
    _4661 = NOVALUE;
    DeRef(_4663);
    _4663 = NOVALUE;
    _4674 = NOVALUE;
    DeRef(_4679);
    _4679 = NOVALUE;
    DeRef(_4684);
    _4684 = NOVALUE;
    DeRef(_4682);
    _4682 = NOVALUE;
    DeRef(_4689);
    _4689 = NOVALUE;
    DeRef(_4702);
    _4702 = NOVALUE;
    return;
    ;
}


void _23pretty(int _x_8722, int _options_8723)
{
    int _4725 = NOVALUE;
    int _4724 = NOVALUE;
    int _4723 = NOVALUE;
    int _4722 = NOVALUE;
    int _4720 = NOVALUE;
    int _4719 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(options) < length( PRETTY_DEFAULT ) then*/
    if (IS_SEQUENCE(_options_8723)){
            _4719 = SEQ_PTR(_options_8723)->length;
    }
    else {
        _4719 = 1;
    }
    _4720 = 10;
    if (_4719 >= 10)
    goto L1; // [13] 41

    /** 		options &= PRETTY_DEFAULT[length(options)+1..$]*/
    if (IS_SEQUENCE(_options_8723)){
            _4722 = SEQ_PTR(_options_8723)->length;
    }
    else {
        _4722 = 1;
    }
    _4723 = _4722 + 1;
    _4722 = NOVALUE;
    _4724 = 10;
    rhs_slice_target = (object_ptr)&_4725;
    RHS_Slice(_23PRETTY_DEFAULT_8699, _4723, 10);
    Concat((object_ptr)&_options_8723, _options_8723, _4725);
    DeRefDS(_4725);
    _4725 = NOVALUE;
L1: 

    /** 	pretty_ascii = options[DISPLAY_ASCII] */
    _2 = (int)SEQ_PTR(_options_8723);
    _23pretty_ascii_8544 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_23pretty_ascii_8544))
    _23pretty_ascii_8544 = (long)DBL_PTR(_23pretty_ascii_8544)->dbl;

    /** 	pretty_indent = options[INDENT]*/
    _2 = (int)SEQ_PTR(_options_8723);
    _23pretty_indent_8545 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_23pretty_indent_8545))
    _23pretty_indent_8545 = (long)DBL_PTR(_23pretty_indent_8545)->dbl;

    /** 	pretty_start_col = options[START_COLUMN]*/
    _2 = (int)SEQ_PTR(_options_8723);
    _23pretty_start_col_8541 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_23pretty_start_col_8541))
    _23pretty_start_col_8541 = (long)DBL_PTR(_23pretty_start_col_8541)->dbl;

    /** 	pretty_end_col = options[WRAP]*/
    _2 = (int)SEQ_PTR(_options_8723);
    _23pretty_end_col_8539 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_23pretty_end_col_8539))
    _23pretty_end_col_8539 = (long)DBL_PTR(_23pretty_end_col_8539)->dbl;

    /** 	pretty_int_format = options[INT_FORMAT]*/
    DeRef(_23pretty_int_format_8554);
    _2 = (int)SEQ_PTR(_options_8723);
    _23pretty_int_format_8554 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_23pretty_int_format_8554);

    /** 	pretty_fp_format = options[FP_FORMAT]*/
    DeRef(_23pretty_fp_format_8553);
    _2 = (int)SEQ_PTR(_options_8723);
    _23pretty_fp_format_8553 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_23pretty_fp_format_8553);

    /** 	pretty_ascii_min = options[MIN_ASCII]*/
    _2 = (int)SEQ_PTR(_options_8723);
    _23pretty_ascii_min_8546 = (int)*(((s1_ptr)_2)->base + 7);
    if (!IS_ATOM_INT(_23pretty_ascii_min_8546))
    _23pretty_ascii_min_8546 = (long)DBL_PTR(_23pretty_ascii_min_8546)->dbl;

    /** 	pretty_ascii_max = options[MAX_ASCII]*/
    _2 = (int)SEQ_PTR(_options_8723);
    _23pretty_ascii_max_8547 = (int)*(((s1_ptr)_2)->base + 8);
    if (!IS_ATOM_INT(_23pretty_ascii_max_8547))
    _23pretty_ascii_max_8547 = (long)DBL_PTR(_23pretty_ascii_max_8547)->dbl;

    /** 	pretty_line_max = options[MAX_LINES]*/
    _2 = (int)SEQ_PTR(_options_8723);
    _23pretty_line_max_8549 = (int)*(((s1_ptr)_2)->base + 9);
    if (!IS_ATOM_INT(_23pretty_line_max_8549))
    _23pretty_line_max_8549 = (long)DBL_PTR(_23pretty_line_max_8549)->dbl;

    /** 	pretty_line_breaks = options[LINE_BREAKS]*/
    _2 = (int)SEQ_PTR(_options_8723);
    _23pretty_line_breaks_8551 = (int)*(((s1_ptr)_2)->base + 10);
    if (!IS_ATOM_INT(_23pretty_line_breaks_8551))
    _23pretty_line_breaks_8551 = (long)DBL_PTR(_23pretty_line_breaks_8551)->dbl;

    /** 	pretty_chars = pretty_start_col*/
    _23pretty_chars_8540 = _23pretty_start_col_8541;

    /** 	pretty_level = 0 */
    _23pretty_level_8542 = 0;

    /** 	pretty_line = ""*/
    RefDS(_5);
    DeRef(_23pretty_line_8555);
    _23pretty_line_8555 = _5;

    /** 	pretty_line_count = 0*/
    _23pretty_line_count_8548 = 0;

    /** 	pretty_dots = 0*/
    _23pretty_dots_8550 = 0;

    /** 	rPrint(x)*/
    Ref(_x_8722);
    _23rPrint(_x_8722);

    /** end procedure*/
    DeRef(_x_8722);
    DeRefDS(_options_8723);
    DeRef(_4723);
    _4723 = NOVALUE;
    return;
    ;
}


void _23pretty_print(int _fn_8745, int _x_8746, int _options_8747)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_fn_8745)) {
        _1 = (long)(DBL_PTR(_fn_8745)->dbl);
        if (UNIQUE(DBL_PTR(_fn_8745)) && (DBL_PTR(_fn_8745)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fn_8745);
        _fn_8745 = _1;
    }

    /** 	pretty_printing = 1*/
    _23pretty_printing_8552 = 1;

    /** 	pretty_file = fn*/
    _23pretty_file_8543 = _fn_8745;

    /** 	pretty( x, options )*/
    Ref(_x_8746);
    RefDS(_options_8747);
    _23pretty(_x_8746, _options_8747);

    /** 	puts(pretty_file, pretty_line)*/
    EPuts(_23pretty_file_8543, _23pretty_line_8555); // DJP 

    /** end procedure*/
    DeRef(_x_8746);
    DeRefDS(_options_8747);
    return;
    ;
}


int _23pretty_sprint(int _x_8750, int _options_8751)
{
    int _0, _1, _2;
    

    /** 	pretty_printing = 0*/
    _23pretty_printing_8552 = 0;

    /** 	pretty( x, options )*/
    Ref(_x_8750);
    RefDS(_options_8751);
    _23pretty(_x_8750, _options_8751);

    /** 	return pretty_line*/
    RefDS(_23pretty_line_8555);
    DeRef(_x_8750);
    DeRefDS(_options_8751);
    return _23pretty_line_8555;
    ;
}



// 0xBC5157FF
