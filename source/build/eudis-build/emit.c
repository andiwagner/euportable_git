// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _37Push(int _x_51523)
{
    int _27152 = NOVALUE;
    int _27150 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_51523)) {
        _1 = (long)(DBL_PTR(_x_51523)->dbl);
        if (UNIQUE(DBL_PTR(_x_51523)) && (DBL_PTR(_x_51523)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_51523);
        _x_51523 = _1;
    }

    /** 	cgi += 1*/
    _37cgi_51284 = _37cgi_51284 + 1;

    /** 	if cgi > length(cg_stack) then*/
    if (IS_SEQUENCE(_37cg_stack_51283)){
            _27150 = SEQ_PTR(_37cg_stack_51283)->length;
    }
    else {
        _27150 = 1;
    }
    if (_37cgi_51284 <= _27150)
    goto L1; // [20] 37

    /** 		cg_stack &= repeat(0, 400)*/
    _27152 = Repeat(0, 400);
    Concat((object_ptr)&_37cg_stack_51283, _37cg_stack_51283, _27152);
    DeRefDS(_27152);
    _27152 = NOVALUE;
L1: 

    /** 	cg_stack[cgi] = x*/
    _2 = (int)SEQ_PTR(_37cg_stack_51283);
    _2 = (int)(((s1_ptr)_2)->base + _37cgi_51284);
    _1 = *(int *)_2;
    *(int *)_2 = _x_51523;
    DeRef(_1);

    /** end procedure*/
    return;
    ;
}


int _37Top()
{
    int _27154 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return cg_stack[cgi]*/
    _2 = (int)SEQ_PTR(_37cg_stack_51283);
    _27154 = (int)*(((s1_ptr)_2)->base + _37cgi_51284);
    Ref(_27154);
    return _27154;
    ;
}


int _37Pop()
{
    int _t_51536 = NOVALUE;
    int _s_51542 = NOVALUE;
    int _27166 = NOVALUE;
    int _27164 = NOVALUE;
    int _27162 = NOVALUE;
    int _27159 = NOVALUE;
    int _27158 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	t = cg_stack[cgi]*/
    _2 = (int)SEQ_PTR(_37cg_stack_51283);
    _t_51536 = (int)*(((s1_ptr)_2)->base + _37cgi_51284);
    if (!IS_ATOM_INT(_t_51536)){
        _t_51536 = (long)DBL_PTR(_t_51536)->dbl;
    }

    /** 	cgi -= 1*/
    _37cgi_51284 = _37cgi_51284 - 1;

    /** 	if t > 0 then*/
    if (_t_51536 <= 0)
    goto L1; // [23] 116

    /** 		symtab_index s = t -- for type checking*/
    _s_51542 = _t_51536;

    /** 		if SymTab[t][S_MODE] = M_TEMP then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _27158 = (int)*(((s1_ptr)_2)->base + _t_51536);
    _2 = (int)SEQ_PTR(_27158);
    _27159 = (int)*(((s1_ptr)_2)->base + 3);
    _27158 = NOVALUE;
    if (binary_op_a(NOTEQ, _27159, 3)){
        _27159 = NOVALUE;
        goto L2; // [50] 115
    }
    _27159 = NOVALUE;

    /** 			if use_private_list = 0 then  -- no problem with reusing the temp*/
    if (_25use_private_list_12396 != 0)
    goto L3; // [58] 82

    /** 				SymTab[t][S_SCOPE] = FREE -- mark it as being free*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_t_51536 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _27162 = NOVALUE;
    goto L4; // [79] 114
L3: 

    /** 			elsif find(t, private_sym) = 0 then*/
    _27164 = find_from(_t_51536, _25private_sym_12395, 1);
    if (_27164 != 0)
    goto L5; // [91] 113

    /** 				SymTab[t][S_SCOPE] = FREE -- mark it as being free*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_t_51536 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _27166 = NOVALUE;
L5: 
L4: 
L2: 
L1: 

    /** 	return t*/
    return _t_51536;
    ;
}


void _37TempKeep(int _x_51570)
{
    int _27173 = NOVALUE;
    int _27172 = NOVALUE;
    int _27171 = NOVALUE;
    int _27170 = NOVALUE;
    int _27169 = NOVALUE;
    int _27168 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_x_51570)) {
        _1 = (long)(DBL_PTR(_x_51570)->dbl);
        if (UNIQUE(DBL_PTR(_x_51570)) && (DBL_PTR(_x_51570)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_51570);
        _x_51570 = _1;
    }

    /** 	if x > 0 and SymTab[x][S_MODE] = M_TEMP then*/
    _27168 = (_x_51570 > 0);
    if (_27168 == 0) {
        goto L1; // [9] 53
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _27170 = (int)*(((s1_ptr)_2)->base + _x_51570);
    _2 = (int)SEQ_PTR(_27170);
    _27171 = (int)*(((s1_ptr)_2)->base + 3);
    _27170 = NOVALUE;
    if (IS_ATOM_INT(_27171)) {
        _27172 = (_27171 == 3);
    }
    else {
        _27172 = binary_op(EQUALS, _27171, 3);
    }
    _27171 = NOVALUE;
    if (_27172 == 0) {
        DeRef(_27172);
        _27172 = NOVALUE;
        goto L1; // [32] 53
    }
    else {
        if (!IS_ATOM_INT(_27172) && DBL_PTR(_27172)->dbl == 0.0){
            DeRef(_27172);
            _27172 = NOVALUE;
            goto L1; // [32] 53
        }
        DeRef(_27172);
        _27172 = NOVALUE;
    }
    DeRef(_27172);
    _27172 = NOVALUE;

    /** 		SymTab[x][S_SCOPE] = IN_USE*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_x_51570 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _27173 = NOVALUE;
L1: 

    /** end procedure*/
    DeRef(_27168);
    _27168 = NOVALUE;
    return;
    ;
}


void _37TempFree(int _x_51588)
{
    int _27179 = NOVALUE;
    int _27177 = NOVALUE;
    int _27176 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_x_51588)) {
        _1 = (long)(DBL_PTR(_x_51588)->dbl);
        if (UNIQUE(DBL_PTR(_x_51588)) && (DBL_PTR(_x_51588)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_51588);
        _x_51588 = _1;
    }

    /** 	if x > 0 then*/
    if (_x_51588 <= 0)
    goto L1; // [5] 53

    /** 		if SymTab[x][S_MODE] = M_TEMP then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _27176 = (int)*(((s1_ptr)_2)->base + _x_51588);
    _2 = (int)SEQ_PTR(_27176);
    _27177 = (int)*(((s1_ptr)_2)->base + 3);
    _27176 = NOVALUE;
    if (binary_op_a(NOTEQ, _27177, 3)){
        _27177 = NOVALUE;
        goto L2; // [25] 52
    }
    _27177 = NOVALUE;

    /** 			SymTab[x][S_SCOPE] = FREE*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_x_51588 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _27179 = NOVALUE;

    /** 			clear_temp( x )*/
    _37clear_temp(_x_51588);
L2: 
L1: 

    /** end procedure*/
    return;
    ;
}


void _37TempInteger(int _x_51607)
{
    int _27186 = NOVALUE;
    int _27185 = NOVALUE;
    int _27184 = NOVALUE;
    int _27183 = NOVALUE;
    int _27182 = NOVALUE;
    int _27181 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_x_51607)) {
        _1 = (long)(DBL_PTR(_x_51607)->dbl);
        if (UNIQUE(DBL_PTR(_x_51607)) && (DBL_PTR(_x_51607)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_51607);
        _x_51607 = _1;
    }

    /** 	if x > 0 and SymTab[x][S_MODE] = M_TEMP then*/
    _27181 = (_x_51607 > 0);
    if (_27181 == 0) {
        goto L1; // [9] 53
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _27183 = (int)*(((s1_ptr)_2)->base + _x_51607);
    _2 = (int)SEQ_PTR(_27183);
    _27184 = (int)*(((s1_ptr)_2)->base + 3);
    _27183 = NOVALUE;
    if (IS_ATOM_INT(_27184)) {
        _27185 = (_27184 == 3);
    }
    else {
        _27185 = binary_op(EQUALS, _27184, 3);
    }
    _27184 = NOVALUE;
    if (_27185 == 0) {
        DeRef(_27185);
        _27185 = NOVALUE;
        goto L1; // [32] 53
    }
    else {
        if (!IS_ATOM_INT(_27185) && DBL_PTR(_27185)->dbl == 0.0){
            DeRef(_27185);
            _27185 = NOVALUE;
            goto L1; // [32] 53
        }
        DeRef(_27185);
        _27185 = NOVALUE;
    }
    DeRef(_27185);
    _27185 = NOVALUE;

    /** 		SymTab[x][S_USAGE] = T_INTEGER*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_x_51607 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _27186 = NOVALUE;
L1: 

    /** end procedure*/
    DeRef(_27181);
    _27181 = NOVALUE;
    return;
    ;
}


int _37LexName(int _t_51624, int _defname_51625)
{
    int _name_51627 = NOVALUE;
    int _27195 = NOVALUE;
    int _27193 = NOVALUE;
    int _27191 = NOVALUE;
    int _27190 = NOVALUE;
    int _27189 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_t_51624)) {
        _1 = (long)(DBL_PTR(_t_51624)->dbl);
        if (UNIQUE(DBL_PTR(_t_51624)) && (DBL_PTR(_t_51624)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_t_51624);
        _t_51624 = _1;
    }

    /** 	for i = 1 to length(token_name) do*/
    _27189 = 80;
    {
        int _i_51629;
        _i_51629 = 1;
L1: 
        if (_i_51629 > 80){
            goto L2; // [12] 82
        }

        /** 		if t = token_name[i][LEX_NUMBER] then*/
        _2 = (int)SEQ_PTR(_37token_name_51291);
        _27190 = (int)*(((s1_ptr)_2)->base + _i_51629);
        _2 = (int)SEQ_PTR(_27190);
        _27191 = (int)*(((s1_ptr)_2)->base + 1);
        _27190 = NOVALUE;
        if (binary_op_a(NOTEQ, _t_51624, _27191)){
            _27191 = NOVALUE;
            goto L3; // [31] 75
        }
        _27191 = NOVALUE;

        /** 			name = token_name[i][LEX_NAME]*/
        _2 = (int)SEQ_PTR(_37token_name_51291);
        _27193 = (int)*(((s1_ptr)_2)->base + _i_51629);
        DeRef(_name_51627);
        _2 = (int)SEQ_PTR(_27193);
        _name_51627 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_name_51627);
        _27193 = NOVALUE;

        /** 			if not find(' ', name) then*/
        _27195 = find_from(32, _name_51627, 1);
        if (_27195 != 0)
        goto L4; // [56] 68
        _27195 = NOVALUE;

        /** 				name = "'" & name & "'"*/
        {
            int concat_list[3];

            concat_list[0] = _27197;
            concat_list[1] = _name_51627;
            concat_list[2] = _27197;
            Concat_N((object_ptr)&_name_51627, concat_list, 3);
        }
L4: 

        /** 			return name*/
        DeRefDS(_defname_51625);
        return _name_51627;
L3: 

        /** 	end for*/
        _i_51629 = _i_51629 + 1;
        goto L1; // [77] 19
L2: 
        ;
    }

    /** 	return defname -- try to avoid this case*/
    DeRef(_name_51627);
    return _defname_51625;
    ;
}


void _37InitEmit()
{
    int _0, _1, _2;
    

    /** 	cg_stack = repeat(0, 400)*/
    DeRef(_37cg_stack_51283);
    _37cg_stack_51283 = Repeat(0, 400);

    /** 	cgi = 0*/
    _37cgi_51284 = 0;

    /** end procedure*/
    return;
    ;
}


int _37IsInteger(int _sym_51648)
{
    int _mode_51649 = NOVALUE;
    int _t_51651 = NOVALUE;
    int _pt_51652 = NOVALUE;
    int _27220 = NOVALUE;
    int _27219 = NOVALUE;
    int _27217 = NOVALUE;
    int _27216 = NOVALUE;
    int _27215 = NOVALUE;
    int _27213 = NOVALUE;
    int _27212 = NOVALUE;
    int _27211 = NOVALUE;
    int _27210 = NOVALUE;
    int _27208 = NOVALUE;
    int _27204 = NOVALUE;
    int _27201 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_51648)) {
        _1 = (long)(DBL_PTR(_sym_51648)->dbl);
        if (UNIQUE(DBL_PTR(_sym_51648)) && (DBL_PTR(_sym_51648)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_51648);
        _sym_51648 = _1;
    }

    /** 	if sym < 1 then*/
    if (_sym_51648 >= 1)
    goto L1; // [5] 16

    /** 		return 0*/
    return 0;
L1: 

    /** 	mode = SymTab[sym][S_MODE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _27201 = (int)*(((s1_ptr)_2)->base + _sym_51648);
    _2 = (int)SEQ_PTR(_27201);
    _mode_51649 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_mode_51649)){
        _mode_51649 = (long)DBL_PTR(_mode_51649)->dbl;
    }
    _27201 = NOVALUE;

    /** 	if mode = M_NORMAL then*/
    if (_mode_51649 != 1)
    goto L2; // [36] 136

    /** 		t = SymTab[sym][S_VTYPE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _27204 = (int)*(((s1_ptr)_2)->base + _sym_51648);
    _2 = (int)SEQ_PTR(_27204);
    _t_51651 = (int)*(((s1_ptr)_2)->base + 15);
    if (!IS_ATOM_INT(_t_51651)){
        _t_51651 = (long)DBL_PTR(_t_51651)->dbl;
    }
    _27204 = NOVALUE;

    /** 		if t = integer_type then*/
    if (_t_51651 != _52integer_type_47105)
    goto L3; // [60] 73

    /** 			return TRUE*/
    return _5TRUE_244;
L3: 

    /** 		if t > 0 then*/
    if (_t_51651 <= 0)
    goto L4; // [75] 215

    /** 			pt = SymTab[t][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _27208 = (int)*(((s1_ptr)_2)->base + _t_51651);
    _2 = (int)SEQ_PTR(_27208);
    _pt_51652 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_pt_51652)){
        _pt_51652 = (long)DBL_PTR(_pt_51652)->dbl;
    }
    _27208 = NOVALUE;

    /** 			if pt and SymTab[pt][S_VTYPE] = integer_type then*/
    if (_pt_51652 == 0) {
        goto L4; // [97] 215
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _27211 = (int)*(((s1_ptr)_2)->base + _pt_51652);
    _2 = (int)SEQ_PTR(_27211);
    _27212 = (int)*(((s1_ptr)_2)->base + 15);
    _27211 = NOVALUE;
    if (IS_ATOM_INT(_27212)) {
        _27213 = (_27212 == _52integer_type_47105);
    }
    else {
        _27213 = binary_op(EQUALS, _27212, _52integer_type_47105);
    }
    _27212 = NOVALUE;
    if (_27213 == 0) {
        DeRef(_27213);
        _27213 = NOVALUE;
        goto L4; // [120] 215
    }
    else {
        if (!IS_ATOM_INT(_27213) && DBL_PTR(_27213)->dbl == 0.0){
            DeRef(_27213);
            _27213 = NOVALUE;
            goto L4; // [120] 215
        }
        DeRef(_27213);
        _27213 = NOVALUE;
    }
    DeRef(_27213);
    _27213 = NOVALUE;

    /** 				return TRUE   -- usertype(integer x)*/
    return _5TRUE_244;
    goto L4; // [133] 215
L2: 

    /** 	elsif mode = M_CONSTANT then*/
    if (_mode_51649 != 2)
    goto L5; // [140] 176

    /** 		if integer(SymTab[sym][S_OBJ]) then  -- bug fixed: can't allow PLUS1_I op*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _27215 = (int)*(((s1_ptr)_2)->base + _sym_51648);
    _2 = (int)SEQ_PTR(_27215);
    _27216 = (int)*(((s1_ptr)_2)->base + 1);
    _27215 = NOVALUE;
    if (IS_ATOM_INT(_27216))
    _27217 = 1;
    else if (IS_ATOM_DBL(_27216))
    _27217 = IS_ATOM_INT(DoubleToInt(_27216));
    else
    _27217 = 0;
    _27216 = NOVALUE;
    if (_27217 == 0)
    {
        _27217 = NOVALUE;
        goto L4; // [161] 215
    }
    else{
        _27217 = NOVALUE;
    }

    /** 			return TRUE*/
    return _5TRUE_244;
    goto L4; // [173] 215
L5: 

    /** 	elsif mode = M_TEMP then*/
    if (_mode_51649 != 3)
    goto L6; // [180] 214

    /** 		if SymTab[sym][S_USAGE] = T_INTEGER then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _27219 = (int)*(((s1_ptr)_2)->base + _sym_51648);
    _2 = (int)SEQ_PTR(_27219);
    _27220 = (int)*(((s1_ptr)_2)->base + 5);
    _27219 = NOVALUE;
    if (binary_op_a(NOTEQ, _27220, 1)){
        _27220 = NOVALUE;
        goto L7; // [200] 213
    }
    _27220 = NOVALUE;

    /** 			return TRUE*/
    return _5TRUE_244;
L7: 
L6: 
L4: 

    /** 	return FALSE*/
    return _5FALSE_242;
    ;
}


void _37emit(int _val_51709)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_val_51709)) {
        _1 = (long)(DBL_PTR(_val_51709)->dbl);
        if (UNIQUE(DBL_PTR(_val_51709)) && (DBL_PTR(_val_51709)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_val_51709);
        _val_51709 = _1;
    }

    /** 	Code = append(Code, val)*/
    Append(&_25Code_12355, _25Code_12355, _val_51709);

    /** end procedure*/
    return;
    ;
}


void _37emit_opnd(int _opnd_51716)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_opnd_51716)) {
        _1 = (long)(DBL_PTR(_opnd_51716)->dbl);
        if (UNIQUE(DBL_PTR(_opnd_51716)) && (DBL_PTR(_opnd_51716)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_opnd_51716);
        _opnd_51716 = _1;
    }

    /** 		Push(opnd)*/
    _37Push(_opnd_51716);

    /** 		previous_op = -1  -- N.B.*/
    _25previous_op_12379 = -1;

    /** end procedure*/
    return;
    ;
}


void _37emit_addr(int _x_51720)
{
    int _0, _1, _2;
    

    /** 		Code = append(Code, x)*/
    Ref(_x_51720);
    Append(&_25Code_12355, _25Code_12355, _x_51720);

    /** end procedure*/
    DeRef(_x_51720);
    return;
    ;
}


void _37emit_opcode(int _op_51726)
{
    int _0, _1, _2;
    

    /** 	Code = append(Code, op)*/
    Append(&_25Code_12355, _25Code_12355, _op_51726);

    /** end procedure*/
    return;
    ;
}


void _37emit_temp(int _tempsym_51760, int _referenced_51761)
{
    int _27251 = NOVALUE;
    int _27250 = NOVALUE;
    int _27249 = NOVALUE;
    int _27248 = NOVALUE;
    int _27247 = NOVALUE;
    int _27246 = NOVALUE;
    int _27245 = NOVALUE;
    int _27244 = NOVALUE;
    int _27243 = NOVALUE;
    int _27242 = NOVALUE;
    int _27241 = NOVALUE;
    int _27240 = NOVALUE;
    int _27239 = NOVALUE;
    int _27238 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_referenced_51761)) {
        _1 = (long)(DBL_PTR(_referenced_51761)->dbl);
        if (UNIQUE(DBL_PTR(_referenced_51761)) && (DBL_PTR(_referenced_51761)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_referenced_51761);
        _referenced_51761 = _1;
    }

    /** 	if not TRANSLATE  then -- translator has its own way of handling temps*/
    if (_25TRANSLATE_11874 != 0)
    goto L1; // [7] 129

    /** 		if sequence(tempsym) then*/
    _27238 = IS_SEQUENCE(_tempsym_51760);
    if (_27238 == 0)
    {
        _27238 = NOVALUE;
        goto L2; // [15] 53
    }
    else{
        _27238 = NOVALUE;
    }

    /** 			for i = 1 to length(tempsym) do*/
    if (IS_SEQUENCE(_tempsym_51760)){
            _27239 = SEQ_PTR(_tempsym_51760)->length;
    }
    else {
        _27239 = 1;
    }
    {
        int _i_51768;
        _i_51768 = 1;
L3: 
        if (_i_51768 > _27239){
            goto L4; // [23] 50
        }

        /** 				emit_temp( tempsym[i], referenced )*/
        _2 = (int)SEQ_PTR(_tempsym_51760);
        _27240 = (int)*(((s1_ptr)_2)->base + _i_51768);
        DeRef(_27241);
        _27241 = _referenced_51761;
        Ref(_27240);
        _37emit_temp(_27240, _27241);
        _27240 = NOVALUE;
        _27241 = NOVALUE;

        /** 			end for*/
        _i_51768 = _i_51768 + 1;
        goto L3; // [45] 30
L4: 
        ;
    }
    goto L5; // [50] 128
L2: 

    /** 		elsif tempsym > 0*/
    if (IS_ATOM_INT(_tempsym_51760)) {
        _27242 = (_tempsym_51760 > 0);
    }
    else {
        _27242 = binary_op(GREATER, _tempsym_51760, 0);
    }
    if (IS_ATOM_INT(_27242)) {
        if (_27242 == 0) {
            DeRef(_27243);
            _27243 = 0;
            goto L6; // [59] 77
        }
    }
    else {
        if (DBL_PTR(_27242)->dbl == 0.0) {
            DeRef(_27243);
            _27243 = 0;
            goto L6; // [59] 77
        }
    }
    Ref(_tempsym_51760);
    _27244 = _52sym_mode(_tempsym_51760);
    if (IS_ATOM_INT(_27244)) {
        _27245 = (_27244 == 3);
    }
    else {
        _27245 = binary_op(EQUALS, _27244, 3);
    }
    DeRef(_27244);
    _27244 = NOVALUE;
    DeRef(_27243);
    if (IS_ATOM_INT(_27245))
    _27243 = (_27245 != 0);
    else
    _27243 = DBL_PTR(_27245)->dbl != 0.0;
L6: 
    if (_27243 == 0) {
        _27246 = 0;
        goto L7; // [77] 92
    }
    Ref(_tempsym_51760);
    _27247 = _37IsInteger(_tempsym_51760);
    if (IS_ATOM_INT(_27247)) {
        _27248 = (_27247 == 0);
    }
    else {
        _27248 = unary_op(NOT, _27247);
    }
    DeRef(_27247);
    _27247 = NOVALUE;
    if (IS_ATOM_INT(_27248))
    _27246 = (_27248 != 0);
    else
    _27246 = DBL_PTR(_27248)->dbl != 0.0;
L7: 
    if (_27246 == 0) {
        goto L8; // [92] 127
    }
    _27250 = find_from(_tempsym_51760, _37emitted_temps_51756, 1);
    _27251 = (_27250 == 0);
    _27250 = NOVALUE;
    if (_27251 == 0)
    {
        DeRef(_27251);
        _27251 = NOVALUE;
        goto L8; // [107] 127
    }
    else{
        DeRef(_27251);
        _27251 = NOVALUE;
    }

    /** 			emitted_temps &= tempsym*/
    if (IS_SEQUENCE(_37emitted_temps_51756) && IS_ATOM(_tempsym_51760)) {
        Ref(_tempsym_51760);
        Append(&_37emitted_temps_51756, _37emitted_temps_51756, _tempsym_51760);
    }
    else if (IS_ATOM(_37emitted_temps_51756) && IS_SEQUENCE(_tempsym_51760)) {
    }
    else {
        Concat((object_ptr)&_37emitted_temps_51756, _37emitted_temps_51756, _tempsym_51760);
    }

    /** 			emitted_temp_referenced &= referenced*/
    Append(&_37emitted_temp_referenced_51757, _37emitted_temp_referenced_51757, _referenced_51761);
L8: 
L5: 
L1: 

    /** end procedure*/
    DeRef(_tempsym_51760);
    DeRef(_27242);
    _27242 = NOVALUE;
    DeRef(_27248);
    _27248 = NOVALUE;
    DeRef(_27245);
    _27245 = NOVALUE;
    return;
    ;
}


void _37flush_temps(int _except_for_51790)
{
    int _refs_51793 = NOVALUE;
    int _novalues_51794 = NOVALUE;
    int _sym_51799 = NOVALUE;
    int _27266 = NOVALUE;
    int _27265 = NOVALUE;
    int _27264 = NOVALUE;
    int _27263 = NOVALUE;
    int _27261 = NOVALUE;
    int _27257 = NOVALUE;
    int _27256 = NOVALUE;
    int _27254 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L1; // [7] 16
    }
    else{
    }

    /** 		return*/
    DeRefDS(_except_for_51790);
    DeRef(_refs_51793);
    DeRefi(_novalues_51794);
    return;
L1: 

    /** 	sequence*/

    /** 		refs = {},*/
    RefDS(_22682);
    DeRef(_refs_51793);
    _refs_51793 = _22682;

    /** 		novalues = {}*/
    RefDS(_22682);
    DeRefi(_novalues_51794);
    _novalues_51794 = _22682;

    /** 	derefs = {}*/
    RefDS(_22682);
    DeRefi(_37derefs_51787);
    _37derefs_51787 = _22682;

    /** 	for i = 1 to length( emitted_temps ) do*/
    if (IS_SEQUENCE(_37emitted_temps_51756)){
            _27254 = SEQ_PTR(_37emitted_temps_51756)->length;
    }
    else {
        _27254 = 1;
    }
    {
        int _i_51796;
        _i_51796 = 1;
L2: 
        if (_i_51796 > _27254){
            goto L3; // [46] 119
        }

        /** 		symtab_index sym = emitted_temps[i]*/
        _2 = (int)SEQ_PTR(_37emitted_temps_51756);
        _sym_51799 = (int)*(((s1_ptr)_2)->base + _i_51796);
        if (!IS_ATOM_INT(_sym_51799)){
            _sym_51799 = (long)DBL_PTR(_sym_51799)->dbl;
        }

        /** 		if find( sym, except_for ) then*/
        _27256 = find_from(_sym_51799, _except_for_51790, 1);
        if (_27256 == 0)
        {
            _27256 = NOVALUE;
            goto L4; // [70] 80
        }
        else{
            _27256 = NOVALUE;
        }

        /** 			continue*/
        goto L5; // [77] 114
L4: 

        /** 		if emitted_temp_referenced[i] = NEW_REFERENCE then*/
        _2 = (int)SEQ_PTR(_37emitted_temp_referenced_51757);
        _27257 = (int)*(((s1_ptr)_2)->base + _i_51796);
        if (binary_op_a(NOTEQ, _27257, 1)){
            _27257 = NOVALUE;
            goto L6; // [88] 103
        }
        _27257 = NOVALUE;

        /** 			derefs &= sym*/
        Append(&_37derefs_51787, _37derefs_51787, _sym_51799);
        goto L7; // [100] 110
L6: 

        /** 			novalues &= sym*/
        Append(&_novalues_51794, _novalues_51794, _sym_51799);
L7: 

        /** 	end for*/
L5: 
        _i_51796 = _i_51796 + 1;
        goto L2; // [114] 53
L3: 
        ;
    }

    /** 	if not length( except_for ) then*/
    if (IS_SEQUENCE(_except_for_51790)){
            _27261 = SEQ_PTR(_except_for_51790)->length;
    }
    else {
        _27261 = 1;
    }
    if (_27261 != 0)
    goto L8; // [124] 132
    _27261 = NOVALUE;

    /** 		clear_last()*/
    _37clear_last();
L8: 

    /** 	for i = 1 to length( derefs ) do*/
    if (IS_SEQUENCE(_37derefs_51787)){
            _27263 = SEQ_PTR(_37derefs_51787)->length;
    }
    else {
        _27263 = 1;
    }
    {
        int _i_51814;
        _i_51814 = 1;
L9: 
        if (_i_51814 > _27263){
            goto LA; // [139] 171
        }

        /** 		emit( DEREF_TEMP )*/
        _37emit(208);

        /** 		emit( derefs[i] )*/
        _2 = (int)SEQ_PTR(_37derefs_51787);
        _27264 = (int)*(((s1_ptr)_2)->base + _i_51814);
        _37emit(_27264);
        _27264 = NOVALUE;

        /** 	end for*/
        _i_51814 = _i_51814 + 1;
        goto L9; // [166] 146
LA: 
        ;
    }

    /** 	for i = 1 to length( novalues ) do*/
    if (IS_SEQUENCE(_novalues_51794)){
            _27265 = SEQ_PTR(_novalues_51794)->length;
    }
    else {
        _27265 = 1;
    }
    {
        int _i_51819;
        _i_51819 = 1;
LB: 
        if (_i_51819 > _27265){
            goto LC; // [176] 206
        }

        /** 		emit( NOVALUE_TEMP )*/
        _37emit(209);

        /** 		emit( novalues[i] )*/
        _2 = (int)SEQ_PTR(_novalues_51794);
        _27266 = (int)*(((s1_ptr)_2)->base + _i_51819);
        _37emit(_27266);
        _27266 = NOVALUE;

        /** 	end for*/
        _i_51819 = _i_51819 + 1;
        goto LB; // [201] 183
LC: 
        ;
    }

    /** 	emitted_temps = {}*/
    RefDS(_22682);
    DeRef(_37emitted_temps_51756);
    _37emitted_temps_51756 = _22682;

    /** 	emitted_temp_referenced = {}*/
    RefDS(_22682);
    DeRef(_37emitted_temp_referenced_51757);
    _37emitted_temp_referenced_51757 = _22682;

    /** end procedure*/
    DeRefDS(_except_for_51790);
    DeRef(_refs_51793);
    DeRefi(_novalues_51794);
    return;
    ;
}


void _37flush_temp(int _temp_51826)
{
    int _except_for_51827 = NOVALUE;
    int _ix_51828 = NOVALUE;
    int _27268 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_temp_51826)) {
        _1 = (long)(DBL_PTR(_temp_51826)->dbl);
        if (UNIQUE(DBL_PTR(_temp_51826)) && (DBL_PTR(_temp_51826)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_temp_51826);
        _temp_51826 = _1;
    }

    /** 	sequence except_for = emitted_temps*/
    RefDS(_37emitted_temps_51756);
    DeRef(_except_for_51827);
    _except_for_51827 = _37emitted_temps_51756;

    /** 	integer ix = find( temp, emitted_temps )*/
    _ix_51828 = find_from(_temp_51826, _37emitted_temps_51756, 1);

    /** 	if ix then*/
    if (_ix_51828 == 0)
    {
        goto L1; // [23] 37
    }
    else{
    }

    /** 		flush_temps( remove( except_for, ix ) )*/
    {
        s1_ptr assign_space = SEQ_PTR(_except_for_51827);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_ix_51828)) ? _ix_51828 : (long)(DBL_PTR(_ix_51828)->dbl);
        int stop = (IS_ATOM_INT(_ix_51828)) ? _ix_51828 : (long)(DBL_PTR(_ix_51828)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
            RefDS(_except_for_51827);
            DeRef(_27268);
            _27268 = _except_for_51827;
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_except_for_51827), start, &_27268 );
            }
            else Tail(SEQ_PTR(_except_for_51827), stop+1, &_27268);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_except_for_51827), start, &_27268);
        }
        else {
            assign_slice_seq = &assign_space;
            _1 = Remove_elements(start, stop, 0);
            DeRef(_27268);
            _27268 = _1;
        }
    }
    _37flush_temps(_27268);
    _27268 = NOVALUE;
L1: 

    /** end procedure*/
    DeRef(_except_for_51827);
    return;
    ;
}


void _37check_for_temps()
{
    int _27275 = NOVALUE;
    int _27274 = NOVALUE;
    int _27273 = NOVALUE;
    int _27272 = NOVALUE;
    int _27270 = NOVALUE;
    int _27269 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if TRANSLATE or last_op < 1 or last_pc < 1 then*/
    if (_25TRANSLATE_11874 != 0) {
        _27269 = 1;
        goto L1; // [5] 19
    }
    _27270 = (_37last_op_52162 < 1);
    _27269 = (_27270 != 0);
L1: 
    if (_27269 != 0) {
        goto L2; // [19] 34
    }
    _27272 = (_37last_pc_52163 < 1);
    if (_27272 == 0)
    {
        DeRef(_27272);
        _27272 = NOVALUE;
        goto L3; // [30] 40
    }
    else{
        DeRef(_27272);
        _27272 = NOVALUE;
    }
L2: 

    /** 		return*/
    DeRef(_27270);
    _27270 = NOVALUE;
    return;
L3: 

    /** 	emit_temp( get_target_sym( current_op( last_pc ) ), op_temp_ref[last_op] )*/
    RefDS(_25Code_12355);
    _27273 = _65current_op(_37last_pc_52163, _25Code_12355);
    _27274 = _65get_target_sym(_27273);
    _27273 = NOVALUE;
    _2 = (int)SEQ_PTR(_37op_temp_ref_51978);
    _27275 = (int)*(((s1_ptr)_2)->base + _37last_op_52162);
    _37emit_temp(_27274, _27275);
    _27274 = NOVALUE;
    _27275 = NOVALUE;

    /** end procedure*/
    DeRef(_27270);
    _27270 = NOVALUE;
    return;
    ;
}


void _37clear_temp(int _tempsym_51853)
{
    int _ix_51854 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_tempsym_51853)) {
        _1 = (long)(DBL_PTR(_tempsym_51853)->dbl);
        if (UNIQUE(DBL_PTR(_tempsym_51853)) && (DBL_PTR(_tempsym_51853)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_tempsym_51853);
        _tempsym_51853 = _1;
    }

    /** 	integer ix = find( tempsym, emitted_temps )*/
    _ix_51854 = find_from(_tempsym_51853, _37emitted_temps_51756, 1);

    /** 	if ix then*/
    if (_ix_51854 == 0)
    {
        goto L1; // [14] 36
    }
    else{
    }

    /** 		emitted_temps = remove( emitted_temps, ix )*/
    {
        s1_ptr assign_space = SEQ_PTR(_37emitted_temps_51756);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_ix_51854)) ? _ix_51854 : (long)(DBL_PTR(_ix_51854)->dbl);
        int stop = (IS_ATOM_INT(_ix_51854)) ? _ix_51854 : (long)(DBL_PTR(_ix_51854)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_37emitted_temps_51756), start, &_37emitted_temps_51756 );
            }
            else Tail(SEQ_PTR(_37emitted_temps_51756), stop+1, &_37emitted_temps_51756);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_37emitted_temps_51756), start, &_37emitted_temps_51756);
        }
        else {
            assign_slice_seq = &assign_space;
            _37emitted_temps_51756 = Remove_elements(start, stop, (SEQ_PTR(_37emitted_temps_51756)->ref == 1));
        }
    }

    /** 		emitted_temp_referenced = remove( emitted_temp_referenced, ix )*/
    {
        s1_ptr assign_space = SEQ_PTR(_37emitted_temp_referenced_51757);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_ix_51854)) ? _ix_51854 : (long)(DBL_PTR(_ix_51854)->dbl);
        int stop = (IS_ATOM_INT(_ix_51854)) ? _ix_51854 : (long)(DBL_PTR(_ix_51854)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_37emitted_temp_referenced_51757), start, &_37emitted_temp_referenced_51757 );
            }
            else Tail(SEQ_PTR(_37emitted_temp_referenced_51757), stop+1, &_37emitted_temp_referenced_51757);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_37emitted_temp_referenced_51757), start, &_37emitted_temp_referenced_51757);
        }
        else {
            assign_slice_seq = &assign_space;
            _37emitted_temp_referenced_51757 = Remove_elements(start, stop, (SEQ_PTR(_37emitted_temp_referenced_51757)->ref == 1));
        }
    }
L1: 

    /** end procedure*/
    return;
    ;
}


int _37pop_temps()
{
    int _new_emitted_51861 = NOVALUE;
    int _new_referenced_51862 = NOVALUE;
    int _27279 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence new_emitted  = emitted_temps*/
    RefDS(_37emitted_temps_51756);
    DeRef(_new_emitted_51861);
    _new_emitted_51861 = _37emitted_temps_51756;

    /** 	sequence new_referenced = emitted_temp_referenced*/
    RefDS(_37emitted_temp_referenced_51757);
    DeRef(_new_referenced_51862);
    _new_referenced_51862 = _37emitted_temp_referenced_51757;

    /** 	emitted_temps  = {}*/
    RefDS(_22682);
    DeRefDS(_37emitted_temps_51756);
    _37emitted_temps_51756 = _22682;

    /** 	emitted_temp_referenced = {}*/
    RefDS(_22682);
    DeRefDS(_37emitted_temp_referenced_51757);
    _37emitted_temp_referenced_51757 = _22682;

    /** 	return { new_emitted, new_referenced }*/
    RefDS(_new_referenced_51862);
    RefDS(_new_emitted_51861);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _new_emitted_51861;
    ((int *)_2)[2] = _new_referenced_51862;
    _27279 = MAKE_SEQ(_1);
    DeRefDS(_new_emitted_51861);
    DeRefDS(_new_referenced_51862);
    return _27279;
    ;
}


int _37get_temps(int _add_to_51866)
{
    int _27284 = NOVALUE;
    int _27283 = NOVALUE;
    int _27282 = NOVALUE;
    int _27281 = NOVALUE;
    int _0, _1, _2;
    

    /** 	add_to[1] &= emitted_temps*/
    _2 = (int)SEQ_PTR(_add_to_51866);
    _27281 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_27281) && IS_ATOM(_37emitted_temps_51756)) {
    }
    else if (IS_ATOM(_27281) && IS_SEQUENCE(_37emitted_temps_51756)) {
        Ref(_27281);
        Prepend(&_27282, _37emitted_temps_51756, _27281);
    }
    else {
        Concat((object_ptr)&_27282, _27281, _37emitted_temps_51756);
        _27281 = NOVALUE;
    }
    _27281 = NOVALUE;
    _2 = (int)SEQ_PTR(_add_to_51866);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _add_to_51866 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _27282;
    if( _1 != _27282 ){
        DeRef(_1);
    }
    _27282 = NOVALUE;

    /** 	add_to[2] &= emitted_temp_referenced*/
    _2 = (int)SEQ_PTR(_add_to_51866);
    _27283 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_27283) && IS_ATOM(_37emitted_temp_referenced_51757)) {
    }
    else if (IS_ATOM(_27283) && IS_SEQUENCE(_37emitted_temp_referenced_51757)) {
        Ref(_27283);
        Prepend(&_27284, _37emitted_temp_referenced_51757, _27283);
    }
    else {
        Concat((object_ptr)&_27284, _27283, _37emitted_temp_referenced_51757);
        _27283 = NOVALUE;
    }
    _27283 = NOVALUE;
    _2 = (int)SEQ_PTR(_add_to_51866);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _add_to_51866 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _27284;
    if( _1 != _27284 ){
        DeRef(_1);
    }
    _27284 = NOVALUE;

    /** 	return add_to*/
    return _add_to_51866;
    ;
}


void _37push_temps(int _temps_51874)
{
    int _27287 = NOVALUE;
    int _27285 = NOVALUE;
    int _0, _1, _2;
    

    /** 	emitted_temps &= temps[1]*/
    _2 = (int)SEQ_PTR(_temps_51874);
    _27285 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_37emitted_temps_51756) && IS_ATOM(_27285)) {
        Ref(_27285);
        Append(&_37emitted_temps_51756, _37emitted_temps_51756, _27285);
    }
    else if (IS_ATOM(_37emitted_temps_51756) && IS_SEQUENCE(_27285)) {
    }
    else {
        Concat((object_ptr)&_37emitted_temps_51756, _37emitted_temps_51756, _27285);
    }
    _27285 = NOVALUE;

    /** 	emitted_temp_referenced &= temps[2]*/
    _2 = (int)SEQ_PTR(_temps_51874);
    _27287 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_37emitted_temp_referenced_51757) && IS_ATOM(_27287)) {
        Ref(_27287);
        Append(&_37emitted_temp_referenced_51757, _37emitted_temp_referenced_51757, _27287);
    }
    else if (IS_ATOM(_37emitted_temp_referenced_51757) && IS_SEQUENCE(_27287)) {
    }
    else {
        Concat((object_ptr)&_37emitted_temp_referenced_51757, _37emitted_temp_referenced_51757, _27287);
    }
    _27287 = NOVALUE;

    /** 	flush_temps()*/
    RefDS(_22682);
    _37flush_temps(_22682);

    /** end procedure*/
    DeRefDS(_temps_51874);
    return;
    ;
}


void _37backpatch(int _index_51881, int _val_51882)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_index_51881)) {
        _1 = (long)(DBL_PTR(_index_51881)->dbl);
        if (UNIQUE(DBL_PTR(_index_51881)) && (DBL_PTR(_index_51881)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_index_51881);
        _index_51881 = _1;
    }
    if (!IS_ATOM_INT(_val_51882)) {
        _1 = (long)(DBL_PTR(_val_51882)->dbl);
        if (UNIQUE(DBL_PTR(_val_51882)) && (DBL_PTR(_val_51882)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_val_51882);
        _val_51882 = _1;
    }

    /** 		Code[index] = val*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _25Code_12355 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index_51881);
    _1 = *(int *)_2;
    *(int *)_2 = _val_51882;
    DeRef(_1);

    /** end procedure*/
    return;
    ;
}


void _37cont11ii(int _op_52063, int _ii_52065)
{
    int _t_52066 = NOVALUE;
    int _source_52067 = NOVALUE;
    int _c_52068 = NOVALUE;
    int _27296 = NOVALUE;
    int _27295 = NOVALUE;
    int _27293 = NOVALUE;
    int _0, _1, _2;
    

    /** 	emit_opcode(op)*/
    _37emit_opcode(_op_52063);

    /** 	source = Pop()*/
    _source_52067 = _37Pop();
    if (!IS_ATOM_INT(_source_52067)) {
        _1 = (long)(DBL_PTR(_source_52067)->dbl);
        if (UNIQUE(DBL_PTR(_source_52067)) && (DBL_PTR(_source_52067)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_source_52067);
        _source_52067 = _1;
    }

    /** 	emit_addr(source)*/
    _37emit_addr(_source_52067);

    /** 	assignable = TRUE*/
    _37assignable_51286 = _5TRUE_244;

    /** 	t = op_result[op]*/
    _2 = (int)SEQ_PTR(_37op_result_51884);
    _t_52066 = (int)*(((s1_ptr)_2)->base + _op_52063);

    /** 	if t = T_INTEGER or (ii and IsInteger(source)) then*/
    _27293 = (_t_52066 == 1);
    if (_27293 != 0) {
        goto L1; // [43] 64
    }
    if (_ii_52065 == 0) {
        _27295 = 0;
        goto L2; // [47] 59
    }
    _27296 = _37IsInteger(_source_52067);
    if (IS_ATOM_INT(_27296))
    _27295 = (_27296 != 0);
    else
    _27295 = DBL_PTR(_27296)->dbl != 0.0;
L2: 
    if (_27295 == 0)
    {
        _27295 = NOVALUE;
        goto L3; // [60] 80
    }
    else{
        _27295 = NOVALUE;
    }
L1: 

    /** 		c = NewTempSym()*/
    _c_52068 = _52NewTempSym(0);
    if (!IS_ATOM_INT(_c_52068)) {
        _1 = (long)(DBL_PTR(_c_52068)->dbl);
        if (UNIQUE(DBL_PTR(_c_52068)) && (DBL_PTR(_c_52068)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_52068);
        _c_52068 = _1;
    }

    /** 		TempInteger(c)*/
    _37TempInteger(_c_52068);
    goto L4; // [77] 95
L3: 

    /** 		c = NewTempSym() -- allocate *after* checking opnd type*/
    _c_52068 = _52NewTempSym(0);
    if (!IS_ATOM_INT(_c_52068)) {
        _1 = (long)(DBL_PTR(_c_52068)->dbl);
        if (UNIQUE(DBL_PTR(_c_52068)) && (DBL_PTR(_c_52068)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_52068);
        _c_52068 = _1;
    }

    /** 		emit_temp( c, NEW_REFERENCE )*/
    _37emit_temp(_c_52068, 1);
L4: 

    /** 	Push(c)*/
    _37Push(_c_52068);

    /** 	emit_addr(c)*/
    _37emit_addr(_c_52068);

    /** end procedure*/
    DeRef(_27293);
    _27293 = NOVALUE;
    DeRef(_27296);
    _27296 = NOVALUE;
    return;
    ;
}


void _37cont21d(int _op_52085, int _a_52086, int _b_52087, int _ii_52089)
{
    int _c_52090 = NOVALUE;
    int _t_52091 = NOVALUE;
    int _27306 = NOVALUE;
    int _27305 = NOVALUE;
    int _27304 = NOVALUE;
    int _27303 = NOVALUE;
    int _27301 = NOVALUE;
    int _0, _1, _2;
    

    /** 	assignable = TRUE*/
    _37assignable_51286 = _5TRUE_244;

    /** 	t = op_result[op]*/
    _2 = (int)SEQ_PTR(_37op_result_51884);
    _t_52091 = (int)*(((s1_ptr)_2)->base + _op_52085);

    /** 	if op = C_FUNC then*/
    if (_op_52085 != 133)
    goto L1; // [26] 38

    /** 		emit_addr(CurrentSub)*/
    _37emit_addr(_25CurrentSub_12270);
L1: 

    /** 	if t = T_INTEGER or (ii and IsInteger(a) and IsInteger(b)) then*/
    _27301 = (_t_52091 == 1);
    if (_27301 != 0) {
        goto L2; // [46] 79
    }
    if (_ii_52089 == 0) {
        _27303 = 0;
        goto L3; // [50] 62
    }
    _27304 = _37IsInteger(_a_52086);
    if (IS_ATOM_INT(_27304))
    _27303 = (_27304 != 0);
    else
    _27303 = DBL_PTR(_27304)->dbl != 0.0;
L3: 
    if (_27303 == 0) {
        DeRef(_27305);
        _27305 = 0;
        goto L4; // [62] 74
    }
    _27306 = _37IsInteger(_b_52087);
    if (IS_ATOM_INT(_27306))
    _27305 = (_27306 != 0);
    else
    _27305 = DBL_PTR(_27306)->dbl != 0.0;
L4: 
    if (_27305 == 0)
    {
        _27305 = NOVALUE;
        goto L5; // [75] 95
    }
    else{
        _27305 = NOVALUE;
    }
L2: 

    /** 		c = NewTempSym()*/
    _c_52090 = _52NewTempSym(0);
    if (!IS_ATOM_INT(_c_52090)) {
        _1 = (long)(DBL_PTR(_c_52090)->dbl);
        if (UNIQUE(DBL_PTR(_c_52090)) && (DBL_PTR(_c_52090)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_52090);
        _c_52090 = _1;
    }

    /** 		TempInteger(c)*/
    _37TempInteger(_c_52090);
    goto L6; // [92] 110
L5: 

    /** 		c = NewTempSym() -- allocate *after* checking opnd types*/
    _c_52090 = _52NewTempSym(0);
    if (!IS_ATOM_INT(_c_52090)) {
        _1 = (long)(DBL_PTR(_c_52090)->dbl);
        if (UNIQUE(DBL_PTR(_c_52090)) && (DBL_PTR(_c_52090)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_52090);
        _c_52090 = _1;
    }

    /** 		emit_temp( c, NEW_REFERENCE )*/
    _37emit_temp(_c_52090, 1);
L6: 

    /** 	Push(c)*/
    _37Push(_c_52090);

    /** 	emit_addr(c)*/
    _37emit_addr(_c_52090);

    /** end procedure*/
    DeRef(_27301);
    _27301 = NOVALUE;
    DeRef(_27304);
    _27304 = NOVALUE;
    DeRef(_27306);
    _27306 = NOVALUE;
    return;
    ;
}


void _37cont21ii(int _op_52113, int _ii_52115)
{
    int _a_52116 = NOVALUE;
    int _b_52117 = NOVALUE;
    int _0, _1, _2;
    

    /** 	b = Pop()*/
    _b_52117 = _37Pop();
    if (!IS_ATOM_INT(_b_52117)) {
        _1 = (long)(DBL_PTR(_b_52117)->dbl);
        if (UNIQUE(DBL_PTR(_b_52117)) && (DBL_PTR(_b_52117)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_b_52117);
        _b_52117 = _1;
    }

    /** 	emit_opcode(op)*/
    _37emit_opcode(_op_52113);

    /** 	a = Pop()*/
    _a_52116 = _37Pop();
    if (!IS_ATOM_INT(_a_52116)) {
        _1 = (long)(DBL_PTR(_a_52116)->dbl);
        if (UNIQUE(DBL_PTR(_a_52116)) && (DBL_PTR(_a_52116)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_a_52116);
        _a_52116 = _1;
    }

    /** 	emit_addr(a)*/
    _37emit_addr(_a_52116);

    /** 	emit_addr(b)*/
    _37emit_addr(_b_52117);

    /** 	cont21d(op, a, b, ii)*/
    _37cont21d(_op_52113, _a_52116, _b_52117, _ii_52115);

    /** end procedure*/
    return;
    ;
}


int _37good_string(int _elements_52122)
{
    int _obj_52123 = NOVALUE;
    int _ep_52125 = NOVALUE;
    int _e_52127 = NOVALUE;
    int _element_vals_52128 = NOVALUE;
    int _27329 = NOVALUE;
    int _27328 = NOVALUE;
    int _27327 = NOVALUE;
    int _27326 = NOVALUE;
    int _27325 = NOVALUE;
    int _27324 = NOVALUE;
    int _27323 = NOVALUE;
    int _27322 = NOVALUE;
    int _27321 = NOVALUE;
    int _27320 = NOVALUE;
    int _27319 = NOVALUE;
    int _27317 = NOVALUE;
    int _27314 = NOVALUE;
    int _27313 = NOVALUE;
    int _27312 = NOVALUE;
    int _27311 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence element_vals*/

    /** 	if TRANSLATE and length(elements) > 10000 then*/
    if (_25TRANSLATE_11874 == 0) {
        goto L1; // [9] 31
    }
    if (IS_SEQUENCE(_elements_52122)){
            _27312 = SEQ_PTR(_elements_52122)->length;
    }
    else {
        _27312 = 1;
    }
    _27313 = (_27312 > 10000);
    _27312 = NOVALUE;
    if (_27313 == 0)
    {
        DeRef(_27313);
        _27313 = NOVALUE;
        goto L1; // [21] 31
    }
    else{
        DeRef(_27313);
        _27313 = NOVALUE;
    }

    /** 		return -1 -- A huge string might upset the C compiler.*/
    DeRefDS(_elements_52122);
    DeRef(_obj_52123);
    DeRef(_element_vals_52128);
    return -1;
L1: 

    /** 	element_vals = {}*/
    RefDS(_22682);
    DeRef(_element_vals_52128);
    _element_vals_52128 = _22682;

    /** 	for i = 1 to length(elements) do*/
    if (IS_SEQUENCE(_elements_52122)){
            _27314 = SEQ_PTR(_elements_52122)->length;
    }
    else {
        _27314 = 1;
    }
    {
        int _i_52135;
        _i_52135 = 1;
L2: 
        if (_i_52135 > _27314){
            goto L3; // [43] 183
        }

        /** 		ep = elements[i]*/
        _2 = (int)SEQ_PTR(_elements_52122);
        _ep_52125 = (int)*(((s1_ptr)_2)->base + _i_52135);
        if (!IS_ATOM_INT(_ep_52125)){
            _ep_52125 = (long)DBL_PTR(_ep_52125)->dbl;
        }

        /** 		if ep < 1 then*/
        if (_ep_52125 >= 1)
        goto L4; // [60] 71

        /** 			return -1*/
        DeRefDS(_elements_52122);
        DeRef(_obj_52123);
        DeRef(_element_vals_52128);
        return -1;
L4: 

        /** 		e = ep*/
        _e_52127 = _ep_52125;

        /** 		obj = SymTab[e][S_OBJ]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27317 = (int)*(((s1_ptr)_2)->base + _e_52127);
        DeRef(_obj_52123);
        _2 = (int)SEQ_PTR(_27317);
        _obj_52123 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_obj_52123);
        _27317 = NOVALUE;

        /** 		if SymTab[e][S_MODE] = M_CONSTANT and*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27319 = (int)*(((s1_ptr)_2)->base + _e_52127);
        _2 = (int)SEQ_PTR(_27319);
        _27320 = (int)*(((s1_ptr)_2)->base + 3);
        _27319 = NOVALUE;
        if (IS_ATOM_INT(_27320)) {
            _27321 = (_27320 == 2);
        }
        else {
            _27321 = binary_op(EQUALS, _27320, 2);
        }
        _27320 = NOVALUE;
        if (IS_ATOM_INT(_27321)) {
            if (_27321 == 0) {
                DeRef(_27322);
                _27322 = 0;
                goto L5; // [112] 123
            }
        }
        else {
            if (DBL_PTR(_27321)->dbl == 0.0) {
                DeRef(_27322);
                _27322 = 0;
                goto L5; // [112] 123
            }
        }
        if (IS_ATOM_INT(_obj_52123))
        _27323 = 1;
        else if (IS_ATOM_DBL(_obj_52123))
        _27323 = IS_ATOM_INT(DoubleToInt(_obj_52123));
        else
        _27323 = 0;
        DeRef(_27322);
        _27322 = (_27323 != 0);
L5: 
        if (_27322 == 0) {
            goto L6; // [123] 169
        }
        _27325 = (_25TRANSLATE_11874 == 0);
        if (_27325 != 0) {
            DeRef(_27326);
            _27326 = 1;
            goto L7; // [132] 156
        }
        if (IS_ATOM_INT(_obj_52123)) {
            _27327 = (_obj_52123 >= 1);
        }
        else {
            _27327 = binary_op(GREATEREQ, _obj_52123, 1);
        }
        if (IS_ATOM_INT(_27327)) {
            if (_27327 == 0) {
                DeRef(_27328);
                _27328 = 0;
                goto L8; // [140] 152
            }
        }
        else {
            if (DBL_PTR(_27327)->dbl == 0.0) {
                DeRef(_27328);
                _27328 = 0;
                goto L8; // [140] 152
            }
        }
        if (IS_ATOM_INT(_obj_52123)) {
            _27329 = (_obj_52123 <= 255);
        }
        else {
            _27329 = binary_op(LESSEQ, _obj_52123, 255);
        }
        DeRef(_27328);
        if (IS_ATOM_INT(_27329))
        _27328 = (_27329 != 0);
        else
        _27328 = DBL_PTR(_27329)->dbl != 0.0;
L8: 
        DeRef(_27326);
        _27326 = (_27328 != 0);
L7: 
        if (_27326 == 0)
        {
            _27326 = NOVALUE;
            goto L6; // [157] 169
        }
        else{
            _27326 = NOVALUE;
        }

        /** 			element_vals = prepend(element_vals, obj)*/
        Ref(_obj_52123);
        Prepend(&_element_vals_52128, _element_vals_52128, _obj_52123);
        goto L9; // [166] 176
L6: 

        /** 			return -1*/
        DeRefDS(_elements_52122);
        DeRef(_obj_52123);
        DeRef(_element_vals_52128);
        DeRef(_27325);
        _27325 = NOVALUE;
        DeRef(_27321);
        _27321 = NOVALUE;
        DeRef(_27327);
        _27327 = NOVALUE;
        DeRef(_27329);
        _27329 = NOVALUE;
        return -1;
L9: 

        /** 	end for*/
        _i_52135 = _i_52135 + 1;
        goto L2; // [178] 50
L3: 
        ;
    }

    /** 	return element_vals*/
    DeRefDS(_elements_52122);
    DeRef(_obj_52123);
    DeRef(_27325);
    _27325 = NOVALUE;
    DeRef(_27321);
    _27321 = NOVALUE;
    DeRef(_27327);
    _27327 = NOVALUE;
    DeRef(_27329);
    _27329 = NOVALUE;
    return _element_vals_52128;
    ;
}


int _37Last_op()
{
    int _0, _1, _2;
    

    /** 	return last_op*/
    return _37last_op_52162;
    ;
}


int _37Last_pc()
{
    int _0, _1, _2;
    

    /** 	return last_pc*/
    return _37last_pc_52163;
    ;
}


void _37move_last_pc(int _amount_52170)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_amount_52170)) {
        _1 = (long)(DBL_PTR(_amount_52170)->dbl);
        if (UNIQUE(DBL_PTR(_amount_52170)) && (DBL_PTR(_amount_52170)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_amount_52170);
        _amount_52170 = _1;
    }

    /** 	if last_pc > 0 then*/
    if (_37last_pc_52163 <= 0)
    goto L1; // [7] 20

    /** 		last_pc += amount*/
    _37last_pc_52163 = _37last_pc_52163 + _amount_52170;
L1: 

    /** end procedure*/
    return;
    ;
}


void _37clear_last()
{
    int _0, _1, _2;
    

    /** 	last_op = 0*/
    _37last_op_52162 = 0;

    /** 	last_pc = 0*/
    _37last_pc_52163 = 0;

    /** end procedure*/
    return;
    ;
}


void _37clear_op()
{
    int _0, _1, _2;
    

    /** 	previous_op = -1*/
    _25previous_op_12379 = -1;

    /** 	assignable = FALSE*/
    _37assignable_51286 = _5FALSE_242;

    /** end procedure*/
    return;
    ;
}


void _37inlined_function()
{
    int _0, _1, _2;
    

    /** 	previous_op = PROC*/
    _25previous_op_12379 = 27;

    /** 	assignable = TRUE*/
    _37assignable_51286 = _5TRUE_244;

    /** 	inlined = TRUE*/
    _37inlined_52181 = _5TRUE_244;

    /** end procedure*/
    return;
    ;
}


void _37add_inline_target(int _pc_52192)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_pc_52192)) {
        _1 = (long)(DBL_PTR(_pc_52192)->dbl);
        if (UNIQUE(DBL_PTR(_pc_52192)) && (DBL_PTR(_pc_52192)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pc_52192);
        _pc_52192 = _1;
    }

    /** 	inlined_targets &= pc*/
    Append(&_37inlined_targets_52189, _37inlined_targets_52189, _pc_52192);

    /** end procedure*/
    return;
    ;
}


void _37clear_inline_targets()
{
    int _0, _1, _2;
    

    /** 	inlined_targets = {}*/
    RefDS(_22682);
    DeRefi(_37inlined_targets_52189);
    _37inlined_targets_52189 = _22682;

    /** end procedure*/
    return;
    ;
}


void _37emit_inline(int _code_52198)
{
    int _0, _1, _2;
    

    /** 	last_pc = 0*/
    _37last_pc_52163 = 0;

    /** 	last_op = 0*/
    _37last_op_52162 = 0;

    /** 	Code &= code*/
    Concat((object_ptr)&_25Code_12355, _25Code_12355, _code_52198);

    /** end procedure*/
    DeRefDS(_code_52198);
    return;
    ;
}


void _37emit_op(int _op_52203)
{
    int _a_52205 = NOVALUE;
    int _b_52206 = NOVALUE;
    int _c_52207 = NOVALUE;
    int _d_52208 = NOVALUE;
    int _source_52209 = NOVALUE;
    int _target_52210 = NOVALUE;
    int _subsym_52211 = NOVALUE;
    int _lhs_var_52213 = NOVALUE;
    int _ib_52214 = NOVALUE;
    int _ic_52215 = NOVALUE;
    int _n_52216 = NOVALUE;
    int _obj_52217 = NOVALUE;
    int _elements_52218 = NOVALUE;
    int _element_vals_52219 = NOVALUE;
    int _last_pc_backup_52220 = NOVALUE;
    int _last_op_backup_52221 = NOVALUE;
    int _temp_52230 = NOVALUE;
    int _real_op_52518 = NOVALUE;
    int _ref_52525 = NOVALUE;
    int _paths_52555 = NOVALUE;
    int _if_code_52635 = NOVALUE;
    int _if_code_52674 = NOVALUE;
    int _Top_inlined_Top_at_5200_53241 = NOVALUE;
    int _element_53312 = NOVALUE;
    int _Top_inlined_Top_at_6755_53459 = NOVALUE;
    int _32378 = NOVALUE;
    int _32377 = NOVALUE;
    int _27910 = NOVALUE;
    int _27906 = NOVALUE;
    int _27903 = NOVALUE;
    int _27901 = NOVALUE;
    int _27895 = NOVALUE;
    int _27894 = NOVALUE;
    int _27893 = NOVALUE;
    int _27891 = NOVALUE;
    int _27889 = NOVALUE;
    int _27888 = NOVALUE;
    int _27887 = NOVALUE;
    int _27886 = NOVALUE;
    int _27885 = NOVALUE;
    int _27884 = NOVALUE;
    int _27883 = NOVALUE;
    int _27882 = NOVALUE;
    int _27881 = NOVALUE;
    int _27880 = NOVALUE;
    int _27879 = NOVALUE;
    int _27877 = NOVALUE;
    int _27876 = NOVALUE;
    int _27875 = NOVALUE;
    int _27873 = NOVALUE;
    int _27872 = NOVALUE;
    int _27871 = NOVALUE;
    int _27870 = NOVALUE;
    int _27869 = NOVALUE;
    int _27868 = NOVALUE;
    int _27867 = NOVALUE;
    int _27866 = NOVALUE;
    int _27865 = NOVALUE;
    int _27862 = NOVALUE;
    int _27859 = NOVALUE;
    int _27858 = NOVALUE;
    int _27857 = NOVALUE;
    int _27856 = NOVALUE;
    int _27854 = NOVALUE;
    int _27852 = NOVALUE;
    int _27840 = NOVALUE;
    int _27832 = NOVALUE;
    int _27829 = NOVALUE;
    int _27828 = NOVALUE;
    int _27827 = NOVALUE;
    int _27826 = NOVALUE;
    int _27823 = NOVALUE;
    int _27822 = NOVALUE;
    int _27821 = NOVALUE;
    int _27820 = NOVALUE;
    int _27819 = NOVALUE;
    int _27818 = NOVALUE;
    int _27817 = NOVALUE;
    int _27816 = NOVALUE;
    int _27815 = NOVALUE;
    int _27814 = NOVALUE;
    int _27813 = NOVALUE;
    int _27811 = NOVALUE;
    int _27807 = NOVALUE;
    int _27806 = NOVALUE;
    int _27805 = NOVALUE;
    int _27804 = NOVALUE;
    int _27803 = NOVALUE;
    int _27802 = NOVALUE;
    int _27801 = NOVALUE;
    int _27800 = NOVALUE;
    int _27799 = NOVALUE;
    int _27798 = NOVALUE;
    int _27797 = NOVALUE;
    int _27795 = NOVALUE;
    int _27790 = NOVALUE;
    int _27789 = NOVALUE;
    int _27787 = NOVALUE;
    int _27786 = NOVALUE;
    int _27785 = NOVALUE;
    int _27783 = NOVALUE;
    int _27780 = NOVALUE;
    int _27776 = NOVALUE;
    int _27773 = NOVALUE;
    int _27772 = NOVALUE;
    int _27771 = NOVALUE;
    int _27770 = NOVALUE;
    int _27769 = NOVALUE;
    int _27768 = NOVALUE;
    int _27766 = NOVALUE;
    int _27765 = NOVALUE;
    int _27763 = NOVALUE;
    int _27762 = NOVALUE;
    int _27761 = NOVALUE;
    int _27760 = NOVALUE;
    int _27759 = NOVALUE;
    int _27758 = NOVALUE;
    int _27757 = NOVALUE;
    int _27756 = NOVALUE;
    int _27755 = NOVALUE;
    int _27754 = NOVALUE;
    int _27752 = NOVALUE;
    int _27751 = NOVALUE;
    int _27750 = NOVALUE;
    int _27749 = NOVALUE;
    int _27748 = NOVALUE;
    int _27747 = NOVALUE;
    int _27746 = NOVALUE;
    int _27745 = NOVALUE;
    int _27744 = NOVALUE;
    int _27743 = NOVALUE;
    int _27742 = NOVALUE;
    int _27741 = NOVALUE;
    int _27740 = NOVALUE;
    int _27739 = NOVALUE;
    int _27738 = NOVALUE;
    int _27736 = NOVALUE;
    int _27733 = NOVALUE;
    int _27732 = NOVALUE;
    int _27731 = NOVALUE;
    int _27730 = NOVALUE;
    int _27729 = NOVALUE;
    int _27728 = NOVALUE;
    int _27727 = NOVALUE;
    int _27726 = NOVALUE;
    int _27725 = NOVALUE;
    int _27724 = NOVALUE;
    int _27723 = NOVALUE;
    int _27722 = NOVALUE;
    int _27721 = NOVALUE;
    int _27720 = NOVALUE;
    int _27719 = NOVALUE;
    int _27717 = NOVALUE;
    int _27713 = NOVALUE;
    int _27710 = NOVALUE;
    int _27708 = NOVALUE;
    int _27707 = NOVALUE;
    int _27705 = NOVALUE;
    int _27704 = NOVALUE;
    int _27703 = NOVALUE;
    int _27702 = NOVALUE;
    int _27701 = NOVALUE;
    int _27700 = NOVALUE;
    int _27699 = NOVALUE;
    int _27698 = NOVALUE;
    int _27697 = NOVALUE;
    int _27696 = NOVALUE;
    int _27695 = NOVALUE;
    int _27694 = NOVALUE;
    int _27693 = NOVALUE;
    int _27692 = NOVALUE;
    int _27691 = NOVALUE;
    int _27690 = NOVALUE;
    int _27689 = NOVALUE;
    int _27688 = NOVALUE;
    int _27687 = NOVALUE;
    int _27685 = NOVALUE;
    int _27683 = NOVALUE;
    int _27682 = NOVALUE;
    int _27680 = NOVALUE;
    int _27670 = NOVALUE;
    int _27669 = NOVALUE;
    int _27668 = NOVALUE;
    int _27667 = NOVALUE;
    int _27666 = NOVALUE;
    int _27665 = NOVALUE;
    int _27664 = NOVALUE;
    int _27663 = NOVALUE;
    int _27662 = NOVALUE;
    int _27661 = NOVALUE;
    int _27660 = NOVALUE;
    int _27659 = NOVALUE;
    int _27658 = NOVALUE;
    int _27657 = NOVALUE;
    int _27656 = NOVALUE;
    int _27655 = NOVALUE;
    int _27654 = NOVALUE;
    int _27653 = NOVALUE;
    int _27652 = NOVALUE;
    int _27651 = NOVALUE;
    int _27650 = NOVALUE;
    int _27649 = NOVALUE;
    int _27648 = NOVALUE;
    int _27647 = NOVALUE;
    int _27641 = NOVALUE;
    int _27640 = NOVALUE;
    int _27637 = NOVALUE;
    int _27634 = NOVALUE;
    int _27633 = NOVALUE;
    int _27632 = NOVALUE;
    int _27631 = NOVALUE;
    int _27630 = NOVALUE;
    int _27629 = NOVALUE;
    int _27628 = NOVALUE;
    int _27627 = NOVALUE;
    int _27626 = NOVALUE;
    int _27625 = NOVALUE;
    int _27623 = NOVALUE;
    int _27622 = NOVALUE;
    int _27621 = NOVALUE;
    int _27619 = NOVALUE;
    int _27617 = NOVALUE;
    int _27616 = NOVALUE;
    int _27615 = NOVALUE;
    int _27614 = NOVALUE;
    int _27613 = NOVALUE;
    int _27612 = NOVALUE;
    int _27611 = NOVALUE;
    int _27610 = NOVALUE;
    int _27609 = NOVALUE;
    int _27608 = NOVALUE;
    int _27606 = NOVALUE;
    int _27605 = NOVALUE;
    int _27603 = NOVALUE;
    int _27602 = NOVALUE;
    int _27601 = NOVALUE;
    int _27600 = NOVALUE;
    int _27599 = NOVALUE;
    int _27597 = NOVALUE;
    int _27596 = NOVALUE;
    int _27595 = NOVALUE;
    int _27594 = NOVALUE;
    int _27593 = NOVALUE;
    int _27592 = NOVALUE;
    int _27591 = NOVALUE;
    int _27590 = NOVALUE;
    int _27588 = NOVALUE;
    int _27587 = NOVALUE;
    int _27586 = NOVALUE;
    int _27585 = NOVALUE;
    int _27584 = NOVALUE;
    int _27582 = NOVALUE;
    int _27581 = NOVALUE;
    int _27579 = NOVALUE;
    int _27578 = NOVALUE;
    int _27577 = NOVALUE;
    int _27576 = NOVALUE;
    int _27575 = NOVALUE;
    int _27573 = NOVALUE;
    int _27571 = NOVALUE;
    int _27569 = NOVALUE;
    int _27568 = NOVALUE;
    int _27566 = NOVALUE;
    int _27565 = NOVALUE;
    int _27564 = NOVALUE;
    int _27563 = NOVALUE;
    int _27562 = NOVALUE;
    int _27561 = NOVALUE;
    int _27560 = NOVALUE;
    int _27559 = NOVALUE;
    int _27558 = NOVALUE;
    int _27557 = NOVALUE;
    int _27556 = NOVALUE;
    int _27555 = NOVALUE;
    int _27554 = NOVALUE;
    int _27553 = NOVALUE;
    int _27552 = NOVALUE;
    int _27551 = NOVALUE;
    int _27550 = NOVALUE;
    int _27547 = NOVALUE;
    int _27546 = NOVALUE;
    int _27544 = NOVALUE;
    int _27543 = NOVALUE;
    int _27542 = NOVALUE;
    int _27541 = NOVALUE;
    int _27540 = NOVALUE;
    int _27538 = NOVALUE;
    int _27536 = NOVALUE;
    int _27535 = NOVALUE;
    int _27534 = NOVALUE;
    int _27533 = NOVALUE;
    int _27532 = NOVALUE;
    int _27531 = NOVALUE;
    int _27530 = NOVALUE;
    int _27529 = NOVALUE;
    int _27528 = NOVALUE;
    int _27525 = NOVALUE;
    int _27524 = NOVALUE;
    int _27522 = NOVALUE;
    int _27521 = NOVALUE;
    int _27520 = NOVALUE;
    int _27519 = NOVALUE;
    int _27518 = NOVALUE;
    int _27515 = NOVALUE;
    int _27514 = NOVALUE;
    int _27513 = NOVALUE;
    int _27512 = NOVALUE;
    int _27511 = NOVALUE;
    int _27510 = NOVALUE;
    int _27509 = NOVALUE;
    int _27504 = NOVALUE;
    int _27503 = NOVALUE;
    int _27502 = NOVALUE;
    int _27500 = NOVALUE;
    int _27499 = NOVALUE;
    int _27497 = NOVALUE;
    int _27496 = NOVALUE;
    int _27491 = NOVALUE;
    int _27490 = NOVALUE;
    int _27489 = NOVALUE;
    int _27488 = NOVALUE;
    int _27487 = NOVALUE;
    int _27481 = NOVALUE;
    int _27480 = NOVALUE;
    int _27478 = NOVALUE;
    int _27477 = NOVALUE;
    int _27476 = NOVALUE;
    int _27475 = NOVALUE;
    int _27474 = NOVALUE;
    int _27473 = NOVALUE;
    int _27471 = NOVALUE;
    int _27470 = NOVALUE;
    int _27469 = NOVALUE;
    int _27468 = NOVALUE;
    int _27467 = NOVALUE;
    int _27466 = NOVALUE;
    int _27465 = NOVALUE;
    int _27464 = NOVALUE;
    int _27463 = NOVALUE;
    int _27462 = NOVALUE;
    int _27461 = NOVALUE;
    int _27460 = NOVALUE;
    int _27459 = NOVALUE;
    int _27458 = NOVALUE;
    int _27456 = NOVALUE;
    int _27455 = NOVALUE;
    int _27454 = NOVALUE;
    int _27453 = NOVALUE;
    int _27450 = NOVALUE;
    int _27449 = NOVALUE;
    int _27448 = NOVALUE;
    int _27447 = NOVALUE;
    int _27445 = NOVALUE;
    int _27444 = NOVALUE;
    int _27443 = NOVALUE;
    int _27442 = NOVALUE;
    int _27440 = NOVALUE;
    int _27439 = NOVALUE;
    int _27438 = NOVALUE;
    int _27437 = NOVALUE;
    int _27436 = NOVALUE;
    int _27435 = NOVALUE;
    int _27434 = NOVALUE;
    int _27433 = NOVALUE;
    int _27432 = NOVALUE;
    int _27431 = NOVALUE;
    int _27430 = NOVALUE;
    int _27429 = NOVALUE;
    int _27428 = NOVALUE;
    int _27427 = NOVALUE;
    int _27425 = NOVALUE;
    int _27424 = NOVALUE;
    int _27423 = NOVALUE;
    int _27422 = NOVALUE;
    int _27421 = NOVALUE;
    int _27419 = NOVALUE;
    int _27418 = NOVALUE;
    int _27417 = NOVALUE;
    int _27416 = NOVALUE;
    int _27415 = NOVALUE;
    int _27411 = NOVALUE;
    int _27410 = NOVALUE;
    int _27409 = NOVALUE;
    int _27408 = NOVALUE;
    int _27407 = NOVALUE;
    int _27405 = NOVALUE;
    int _27404 = NOVALUE;
    int _27403 = NOVALUE;
    int _27402 = NOVALUE;
    int _27401 = NOVALUE;
    int _27400 = NOVALUE;
    int _27399 = NOVALUE;
    int _27398 = NOVALUE;
    int _27397 = NOVALUE;
    int _27396 = NOVALUE;
    int _27395 = NOVALUE;
    int _27394 = NOVALUE;
    int _27393 = NOVALUE;
    int _27392 = NOVALUE;
    int _27391 = NOVALUE;
    int _27390 = NOVALUE;
    int _27389 = NOVALUE;
    int _27388 = NOVALUE;
    int _27386 = NOVALUE;
    int _27385 = NOVALUE;
    int _27384 = NOVALUE;
    int _27383 = NOVALUE;
    int _27382 = NOVALUE;
    int _27381 = NOVALUE;
    int _27380 = NOVALUE;
    int _27379 = NOVALUE;
    int _27378 = NOVALUE;
    int _27376 = NOVALUE;
    int _27375 = NOVALUE;
    int _27374 = NOVALUE;
    int _27372 = NOVALUE;
    int _27371 = NOVALUE;
    int _27369 = NOVALUE;
    int _27367 = NOVALUE;
    int _27366 = NOVALUE;
    int _27365 = NOVALUE;
    int _27364 = NOVALUE;
    int _27363 = NOVALUE;
    int _27362 = NOVALUE;
    int _27358 = NOVALUE;
    int _27357 = NOVALUE;
    int _27356 = NOVALUE;
    int _27354 = NOVALUE;
    int _27353 = NOVALUE;
    int _27352 = NOVALUE;
    int _27351 = NOVALUE;
    int _27350 = NOVALUE;
    int _27349 = NOVALUE;
    int _27348 = NOVALUE;
    int _27347 = NOVALUE;
    int _27346 = NOVALUE;
    int _27345 = NOVALUE;
    int _27344 = NOVALUE;
    int _27343 = NOVALUE;
    int _27342 = NOVALUE;
    int _27341 = NOVALUE;
    int _27340 = NOVALUE;
    int _27335 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_op_52203)) {
        _1 = (long)(DBL_PTR(_op_52203)->dbl);
        if (UNIQUE(DBL_PTR(_op_52203)) && (DBL_PTR(_op_52203)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_op_52203);
        _op_52203 = _1;
    }

    /** 	integer ib, ic, n*/

    /** 	object obj*/

    /** 	sequence elements*/

    /** 	object element_vals*/

    /** 	check_for_temps()*/
    _37check_for_temps();

    /** 	integer last_pc_backup = last_pc*/
    _last_pc_backup_52220 = _37last_pc_52163;

    /** 	integer last_op_backup = last_op*/
    _last_op_backup_52221 = _37last_op_52162;

    /** 	last_op = op*/
    _37last_op_52162 = _op_52203;

    /** 	last_pc = length(Code) + 1*/
    if (IS_SEQUENCE(_25Code_12355)){
            _27335 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _27335 = 1;
    }
    _37last_pc_52163 = _27335 + 1;
    _27335 = NOVALUE;

    /** 	switch op label "EMIT" do*/
    _0 = _op_52203;
    switch ( _0 ){ 

        /** 	case ASSIGN then*/
        case 18:

        /** 		sequence temp = {}*/
        RefDS(_22682);
        DeRef(_temp_52230);
        _temp_52230 = _22682;

        /** 		if not TRANSLATE and*/
        _27340 = (_25TRANSLATE_11874 == 0);
        if (_27340 == 0) {
            goto L1; // [70] 202
        }
        _27342 = (_25previous_op_12379 == 92);
        if (_27342 != 0) {
            DeRef(_27343);
            _27343 = 1;
            goto L2; // [82] 98
        }
        _27344 = (_25previous_op_12379 == 25);
        _27343 = (_27344 != 0);
L2: 
        if (_27343 == 0)
        {
            _27343 = NOVALUE;
            goto L1; // [99] 202
        }
        else{
            _27343 = NOVALUE;
        }

        /** 			while Code[$-1] = DEREF_TEMP and find( Code[$], derefs ) do*/
L3: 
        if (IS_SEQUENCE(_25Code_12355)){
                _27345 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27345 = 1;
        }
        _27346 = _27345 - 1;
        _27345 = NOVALUE;
        _2 = (int)SEQ_PTR(_25Code_12355);
        _27347 = (int)*(((s1_ptr)_2)->base + _27346);
        if (IS_ATOM_INT(_27347)) {
            _27348 = (_27347 == 208);
        }
        else {
            _27348 = binary_op(EQUALS, _27347, 208);
        }
        _27347 = NOVALUE;
        if (IS_ATOM_INT(_27348)) {
            if (_27348 == 0) {
                goto L4; // [126] 201
            }
        }
        else {
            if (DBL_PTR(_27348)->dbl == 0.0) {
                goto L4; // [126] 201
            }
        }
        if (IS_SEQUENCE(_25Code_12355)){
                _27350 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27350 = 1;
        }
        _2 = (int)SEQ_PTR(_25Code_12355);
        _27351 = (int)*(((s1_ptr)_2)->base + _27350);
        _27352 = find_from(_27351, _37derefs_51787, 1);
        _27351 = NOVALUE;
        if (_27352 == 0)
        {
            _27352 = NOVALUE;
            goto L4; // [147] 201
        }
        else{
            _27352 = NOVALUE;
        }

        /** 				temp &= Code[$]*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27353 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27353 = 1;
        }
        _2 = (int)SEQ_PTR(_25Code_12355);
        _27354 = (int)*(((s1_ptr)_2)->base + _27353);
        if (IS_SEQUENCE(_temp_52230) && IS_ATOM(_27354)) {
            Ref(_27354);
            Append(&_temp_52230, _temp_52230, _27354);
        }
        else if (IS_ATOM(_temp_52230) && IS_SEQUENCE(_27354)) {
        }
        else {
            Concat((object_ptr)&_temp_52230, _temp_52230, _27354);
        }
        _27354 = NOVALUE;

        /** 				Code = remove( Code, length(Code)-1, length(Code) )*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27356 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27356 = 1;
        }
        _27357 = _27356 - 1;
        _27356 = NOVALUE;
        if (IS_SEQUENCE(_25Code_12355)){
                _27358 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27358 = 1;
        }
        {
            s1_ptr assign_space = SEQ_PTR(_25Code_12355);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_27357)) ? _27357 : (long)(DBL_PTR(_27357)->dbl);
            int stop = (IS_ATOM_INT(_27358)) ? _27358 : (long)(DBL_PTR(_27358)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_25Code_12355), start, &_25Code_12355 );
                }
                else Tail(SEQ_PTR(_25Code_12355), stop+1, &_25Code_12355);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_25Code_12355), start, &_25Code_12355);
            }
            else {
                assign_slice_seq = &assign_space;
                _25Code_12355 = Remove_elements(start, stop, (SEQ_PTR(_25Code_12355)->ref == 1));
            }
        }
        _27357 = NOVALUE;
        _27358 = NOVALUE;

        /** 				emit_temp( temp, NEW_REFERENCE )*/
        RefDS(_temp_52230);
        _37emit_temp(_temp_52230, 1);

        /** 			end while*/
        goto L3; // [198] 107
L4: 
L1: 

        /** 		source = Pop()*/
        _source_52209 = _37Pop();
        if (!IS_ATOM_INT(_source_52209)) {
            _1 = (long)(DBL_PTR(_source_52209)->dbl);
            if (UNIQUE(DBL_PTR(_source_52209)) && (DBL_PTR(_source_52209)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_source_52209);
            _source_52209 = _1;
        }

        /** 		target = Pop()*/
        _target_52210 = _37Pop();
        if (!IS_ATOM_INT(_target_52210)) {
            _1 = (long)(DBL_PTR(_target_52210)->dbl);
            if (UNIQUE(DBL_PTR(_target_52210)) && (DBL_PTR(_target_52210)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_target_52210);
            _target_52210 = _1;
        }

        /** 		if assignable then*/
        if (_37assignable_51286 == 0)
        {
            goto L5; // [220] 606
        }
        else{
        }

        /** 			if inlined then*/
        if (_37inlined_52181 == 0)
        {
            goto L6; // [227] 331
        }
        else{
        }

        /** 				inlined = 0*/
        _37inlined_52181 = 0;

        /** 				if length( inlined_targets ) then*/
        if (IS_SEQUENCE(_37inlined_targets_52189)){
                _27362 = SEQ_PTR(_37inlined_targets_52189)->length;
        }
        else {
            _27362 = 1;
        }
        if (_27362 == 0)
        {
            _27362 = NOVALUE;
            goto L7; // [242] 300
        }
        else{
            _27362 = NOVALUE;
        }

        /** 					for i = 1 to length( inlined_targets ) do*/
        if (IS_SEQUENCE(_37inlined_targets_52189)){
                _27363 = SEQ_PTR(_37inlined_targets_52189)->length;
        }
        else {
            _27363 = 1;
        }
        {
            int _i_52273;
            _i_52273 = 1;
L8: 
            if (_i_52273 > _27363){
                goto L9; // [252] 280
            }

            /** 						Code[inlined_targets[i]] = target*/
            _2 = (int)SEQ_PTR(_37inlined_targets_52189);
            _27364 = (int)*(((s1_ptr)_2)->base + _i_52273);
            _2 = (int)SEQ_PTR(_25Code_12355);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _25Code_12355 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _27364);
            _1 = *(int *)_2;
            *(int *)_2 = _target_52210;
            DeRef(_1);

            /** 					end for*/
            _i_52273 = _i_52273 + 1;
            goto L8; // [275] 259
L9: 
            ;
        }

        /** 					clear_inline_targets()*/

        /** 	inlined_targets = {}*/
        RefDS(_22682);
        DeRefi(_37inlined_targets_52189);
        _37inlined_targets_52189 = _22682;

        /** end procedure*/
        goto LA; // [291] 294
LA: 

        /** 					op = -1*/
        _op_52203 = -1;
L7: 

        /** 				assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;

        /** 				clear_last()*/

        /** 	last_op = 0*/
        _37last_op_52162 = 0;

        /** 	last_pc = 0*/
        _37last_pc_52163 = 0;

        /** end procedure*/
        goto LB; // [321] 324
LB: 

        /** 				break "EMIT"*/
        DeRef(_temp_52230);
        _temp_52230 = NOVALUE;
        goto LC; // [328] 7417
L6: 

        /** 			clear_temp( Code[$] )*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27365 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27365 = 1;
        }
        _2 = (int)SEQ_PTR(_25Code_12355);
        _27366 = (int)*(((s1_ptr)_2)->base + _27365);
        Ref(_27366);
        _37clear_temp(_27366);
        _27366 = NOVALUE;

        /** 			Code = remove( Code, length( Code ) ) -- drop previous target*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27367 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27367 = 1;
        }
        {
            s1_ptr assign_space = SEQ_PTR(_25Code_12355);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_27367)) ? _27367 : (long)(DBL_PTR(_27367)->dbl);
            int stop = (IS_ATOM_INT(_27367)) ? _27367 : (long)(DBL_PTR(_27367)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_25Code_12355), start, &_25Code_12355 );
                }
                else Tail(SEQ_PTR(_25Code_12355), stop+1, &_25Code_12355);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_25Code_12355), start, &_25Code_12355);
            }
            else {
                assign_slice_seq = &assign_space;
                _25Code_12355 = Remove_elements(start, stop, (SEQ_PTR(_25Code_12355)->ref == 1));
            }
        }
        _27367 = NOVALUE;
        _27367 = NOVALUE;

        /** 			op = previous_op -- keep same previous op*/
        _op_52203 = _25previous_op_12379;

        /** 			if IsInteger(target) then*/
        _27369 = _37IsInteger(_target_52210);
        if (_27369 == 0) {
            DeRef(_27369);
            _27369 = NOVALUE;
            goto LD; // [377] 593
        }
        else {
            if (!IS_ATOM_INT(_27369) && DBL_PTR(_27369)->dbl == 0.0){
                DeRef(_27369);
                _27369 = NOVALUE;
                goto LD; // [377] 593
            }
            DeRef(_27369);
            _27369 = NOVALUE;
        }
        DeRef(_27369);
        _27369 = NOVALUE;

        /** 				if previous_op = RHS_SUBS then*/
        if (_25previous_op_12379 != 25)
        goto LE; // [386] 417

        /** 					op = RHS_SUBS_I*/
        _op_52203 = 114;

        /** 					backpatch(length(Code) - 2, op)*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27371 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27371 = 1;
        }
        _27372 = _27371 - 2;
        _27371 = NOVALUE;
        _37backpatch(_27372, 114);
        _27372 = NOVALUE;
        goto LF; // [414] 592
LE: 

        /** 				elsif previous_op = PLUS1 then*/
        if (_25previous_op_12379 != 93)
        goto L10; // [423] 454

        /** 					op = PLUS1_I*/
        _op_52203 = 117;

        /** 					backpatch(length(Code) - 2, op)*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27374 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27374 = 1;
        }
        _27375 = _27374 - 2;
        _27374 = NOVALUE;
        _37backpatch(_27375, 117);
        _27375 = NOVALUE;
        goto LF; // [451] 592
L10: 

        /** 				elsif previous_op = PLUS or previous_op = MINUS then*/
        _27376 = (_25previous_op_12379 == 11);
        if (_27376 != 0) {
            goto L11; // [464] 481
        }
        _27378 = (_25previous_op_12379 == 10);
        if (_27378 == 0)
        {
            DeRef(_27378);
            _27378 = NOVALUE;
            goto L12; // [477] 572
        }
        else{
            DeRef(_27378);
            _27378 = NOVALUE;
        }
L11: 

        /** 					if IsInteger(Code[$]) and*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27379 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27379 = 1;
        }
        _2 = (int)SEQ_PTR(_25Code_12355);
        _27380 = (int)*(((s1_ptr)_2)->base + _27379);
        Ref(_27380);
        _27381 = _37IsInteger(_27380);
        _27380 = NOVALUE;
        if (IS_ATOM_INT(_27381)) {
            if (_27381 == 0) {
                goto LF; // [496] 592
            }
        }
        else {
            if (DBL_PTR(_27381)->dbl == 0.0) {
                goto LF; // [496] 592
            }
        }
        if (IS_SEQUENCE(_25Code_12355)){
                _27383 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27383 = 1;
        }
        _27384 = _27383 - 1;
        _27383 = NOVALUE;
        _2 = (int)SEQ_PTR(_25Code_12355);
        _27385 = (int)*(((s1_ptr)_2)->base + _27384);
        Ref(_27385);
        _27386 = _37IsInteger(_27385);
        _27385 = NOVALUE;
        if (_27386 == 0) {
            DeRef(_27386);
            _27386 = NOVALUE;
            goto LF; // [518] 592
        }
        else {
            if (!IS_ATOM_INT(_27386) && DBL_PTR(_27386)->dbl == 0.0){
                DeRef(_27386);
                _27386 = NOVALUE;
                goto LF; // [518] 592
            }
            DeRef(_27386);
            _27386 = NOVALUE;
        }
        DeRef(_27386);
        _27386 = NOVALUE;

        /** 						if previous_op = PLUS then*/
        if (_25previous_op_12379 != 11)
        goto L13; // [527] 543

        /** 							op = PLUS_I*/
        _op_52203 = 115;
        goto L14; // [540] 553
L13: 

        /** 							op = MINUS_I*/
        _op_52203 = 116;
L14: 

        /** 						backpatch(length(Code) - 2, op)*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27388 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27388 = 1;
        }
        _27389 = _27388 - 2;
        _27388 = NOVALUE;
        _37backpatch(_27389, _op_52203);
        _27389 = NOVALUE;
        goto LF; // [569] 592
L12: 

        /** 					if IsInteger(source) then*/
        _27390 = _37IsInteger(_source_52209);
        if (_27390 == 0) {
            DeRef(_27390);
            _27390 = NOVALUE;
            goto L15; // [578] 591
        }
        else {
            if (!IS_ATOM_INT(_27390) && DBL_PTR(_27390)->dbl == 0.0){
                DeRef(_27390);
                _27390 = NOVALUE;
                goto L15; // [578] 591
            }
            DeRef(_27390);
            _27390 = NOVALUE;
        }
        DeRef(_27390);
        _27390 = NOVALUE;

        /** 						op = ASSIGN_I -- fake to avoid subsequent check*/
        _op_52203 = 113;
L15: 
LF: 
LD: 

        /** 			last_op = last_op_backup*/
        _37last_op_52162 = _last_op_backup_52221;

        /** 			last_pc = last_pc_backup*/
        _37last_pc_52163 = _last_pc_backup_52220;
        goto L16; // [603] 748
L5: 

        /** 			if IsInteger(source) and IsInteger(target) then*/
        _27391 = _37IsInteger(_source_52209);
        if (IS_ATOM_INT(_27391)) {
            if (_27391 == 0) {
                goto L17; // [612] 634
            }
        }
        else {
            if (DBL_PTR(_27391)->dbl == 0.0) {
                goto L17; // [612] 634
            }
        }
        _27393 = _37IsInteger(_target_52210);
        if (_27393 == 0) {
            DeRef(_27393);
            _27393 = NOVALUE;
            goto L17; // [621] 634
        }
        else {
            if (!IS_ATOM_INT(_27393) && DBL_PTR(_27393)->dbl == 0.0){
                DeRef(_27393);
                _27393 = NOVALUE;
                goto L17; // [621] 634
            }
            DeRef(_27393);
            _27393 = NOVALUE;
        }
        DeRef(_27393);
        _27393 = NOVALUE;

        /** 				op = ASSIGN_I*/
        _op_52203 = 113;
L17: 

        /** 			if source > 0 and target > 0 and*/
        _27394 = (_source_52209 > 0);
        if (_27394 == 0) {
            _27395 = 0;
            goto L18; // [640] 652
        }
        _27396 = (_target_52210 > 0);
        _27395 = (_27396 != 0);
L18: 
        if (_27395 == 0) {
            _27397 = 0;
            goto L19; // [652] 678
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27398 = (int)*(((s1_ptr)_2)->base + _source_52209);
        _2 = (int)SEQ_PTR(_27398);
        _27399 = (int)*(((s1_ptr)_2)->base + 3);
        _27398 = NOVALUE;
        if (IS_ATOM_INT(_27399)) {
            _27400 = (_27399 == 2);
        }
        else {
            _27400 = binary_op(EQUALS, _27399, 2);
        }
        _27399 = NOVALUE;
        if (IS_ATOM_INT(_27400))
        _27397 = (_27400 != 0);
        else
        _27397 = DBL_PTR(_27400)->dbl != 0.0;
L19: 
        if (_27397 == 0) {
            goto L1A; // [678] 732
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27402 = (int)*(((s1_ptr)_2)->base + _target_52210);
        _2 = (int)SEQ_PTR(_27402);
        _27403 = (int)*(((s1_ptr)_2)->base + 3);
        _27402 = NOVALUE;
        if (IS_ATOM_INT(_27403)) {
            _27404 = (_27403 == 2);
        }
        else {
            _27404 = binary_op(EQUALS, _27403, 2);
        }
        _27403 = NOVALUE;
        if (_27404 == 0) {
            DeRef(_27404);
            _27404 = NOVALUE;
            goto L1A; // [701] 732
        }
        else {
            if (!IS_ATOM_INT(_27404) && DBL_PTR(_27404)->dbl == 0.0){
                DeRef(_27404);
                _27404 = NOVALUE;
                goto L1A; // [701] 732
            }
            DeRef(_27404);
            _27404 = NOVALUE;
        }
        DeRef(_27404);
        _27404 = NOVALUE;

        /** 				SymTab[target][S_OBJ] = SymTab[source][S_OBJ]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_target_52210 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27407 = (int)*(((s1_ptr)_2)->base + _source_52209);
        _2 = (int)SEQ_PTR(_27407);
        _27408 = (int)*(((s1_ptr)_2)->base + 1);
        _27407 = NOVALUE;
        Ref(_27408);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 1);
        _1 = *(int *)_2;
        *(int *)_2 = _27408;
        if( _1 != _27408 ){
            DeRef(_1);
        }
        _27408 = NOVALUE;
        _27405 = NOVALUE;
L1A: 

        /** 			emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 			emit_addr(source)*/
        _37emit_addr(_source_52209);

        /** 			last_op = op*/
        _37last_op_52162 = _op_52203;
L16: 

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;

        /** 		emit_addr(target)*/
        _37emit_addr(_target_52210);

        /** 		if length(temp) then*/
        if (IS_SEQUENCE(_temp_52230)){
                _27409 = SEQ_PTR(_temp_52230)->length;
        }
        else {
            _27409 = 1;
        }
        if (_27409 == 0)
        {
            _27409 = NOVALUE;
            goto L1B; // [765] 797
        }
        else{
            _27409 = NOVALUE;
        }

        /** 			for i = 1 to length( temp ) do*/
        if (IS_SEQUENCE(_temp_52230)){
                _27410 = SEQ_PTR(_temp_52230)->length;
        }
        else {
            _27410 = 1;
        }
        {
            int _i_52376;
            _i_52376 = 1;
L1C: 
            if (_i_52376 > _27410){
                goto L1D; // [773] 796
            }

            /** 				flush_temp( temp[i] )*/
            _2 = (int)SEQ_PTR(_temp_52230);
            _27411 = (int)*(((s1_ptr)_2)->base + _i_52376);
            Ref(_27411);
            _37flush_temp(_27411);
            _27411 = NOVALUE;

            /** 			end for*/
            _i_52376 = _i_52376 + 1;
            goto L1C; // [791] 780
L1D: 
            ;
        }
L1B: 
        DeRef(_temp_52230);
        _temp_52230 = NOVALUE;
        goto LC; // [799] 7417

        /** 	case RHS_SUBS then*/
        case 25:

        /** 		b = Pop() -- subscript*/
        _b_52206 = _37Pop();
        if (!IS_ATOM_INT(_b_52206)) {
            _1 = (long)(DBL_PTR(_b_52206)->dbl);
            if (UNIQUE(DBL_PTR(_b_52206)) && (DBL_PTR(_b_52206)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52206);
            _b_52206 = _1;
        }

        /** 		c = Pop() -- sequence*/
        _c_52207 = _37Pop();
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		target = NewTempSym() -- target*/
        _target_52210 = _52NewTempSym(0);
        if (!IS_ATOM_INT(_target_52210)) {
            _1 = (long)(DBL_PTR(_target_52210)->dbl);
            if (UNIQUE(DBL_PTR(_target_52210)) && (DBL_PTR(_target_52210)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_target_52210);
            _target_52210 = _1;
        }

        /** 		if c < 0 or length(SymTab[c]) < S_VTYPE or SymTab[c][S_VTYPE] < 0 then -- forward reference*/
        _27415 = (_c_52207 < 0);
        if (_27415 != 0) {
            _27416 = 1;
            goto L1E; // [833] 856
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27417 = (int)*(((s1_ptr)_2)->base + _c_52207);
        if (IS_SEQUENCE(_27417)){
                _27418 = SEQ_PTR(_27417)->length;
        }
        else {
            _27418 = 1;
        }
        _27417 = NOVALUE;
        _27419 = (_27418 < 15);
        _27418 = NOVALUE;
        _27416 = (_27419 != 0);
L1E: 
        if (_27416 != 0) {
            goto L1F; // [856] 881
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27421 = (int)*(((s1_ptr)_2)->base + _c_52207);
        _2 = (int)SEQ_PTR(_27421);
        _27422 = (int)*(((s1_ptr)_2)->base + 15);
        _27421 = NOVALUE;
        if (IS_ATOM_INT(_27422)) {
            _27423 = (_27422 < 0);
        }
        else {
            _27423 = binary_op(LESS, _27422, 0);
        }
        _27422 = NOVALUE;
        if (_27423 == 0) {
            DeRef(_27423);
            _27423 = NOVALUE;
            goto L20; // [877] 893
        }
        else {
            if (!IS_ATOM_INT(_27423) && DBL_PTR(_27423)->dbl == 0.0){
                DeRef(_27423);
                _27423 = NOVALUE;
                goto L20; // [877] 893
            }
            DeRef(_27423);
            _27423 = NOVALUE;
        }
        DeRef(_27423);
        _27423 = NOVALUE;
L1F: 

        /** 			op = RHS_SUBS_CHECK*/
        _op_52203 = 92;
        goto L21; // [890] 1054
L20: 

        /** 		elsif SymTab[c][S_MODE] = M_NORMAL then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27424 = (int)*(((s1_ptr)_2)->base + _c_52207);
        _2 = (int)SEQ_PTR(_27424);
        _27425 = (int)*(((s1_ptr)_2)->base + 3);
        _27424 = NOVALUE;
        if (binary_op_a(NOTEQ, _27425, 1)){
            _27425 = NOVALUE;
            goto L22; // [909] 996
        }
        _27425 = NOVALUE;

        /** 			if SymTab[c][S_VTYPE] != sequence_type and*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27427 = (int)*(((s1_ptr)_2)->base + _c_52207);
        _2 = (int)SEQ_PTR(_27427);
        _27428 = (int)*(((s1_ptr)_2)->base + 15);
        _27427 = NOVALUE;
        if (IS_ATOM_INT(_27428)) {
            _27429 = (_27428 != _52sequence_type_47103);
        }
        else {
            _27429 = binary_op(NOTEQ, _27428, _52sequence_type_47103);
        }
        _27428 = NOVALUE;
        if (IS_ATOM_INT(_27429)) {
            if (_27429 == 0) {
                goto L21; // [933] 1054
            }
        }
        else {
            if (DBL_PTR(_27429)->dbl == 0.0) {
                goto L21; // [933] 1054
            }
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27431 = (int)*(((s1_ptr)_2)->base + _c_52207);
        _2 = (int)SEQ_PTR(_27431);
        _27432 = (int)*(((s1_ptr)_2)->base + 15);
        _27431 = NOVALUE;
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!IS_ATOM_INT(_27432)){
            _27433 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_27432)->dbl));
        }
        else{
            _27433 = (int)*(((s1_ptr)_2)->base + _27432);
        }
        _2 = (int)SEQ_PTR(_27433);
        _27434 = (int)*(((s1_ptr)_2)->base + 2);
        _27433 = NOVALUE;
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!IS_ATOM_INT(_27434)){
            _27435 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_27434)->dbl));
        }
        else{
            _27435 = (int)*(((s1_ptr)_2)->base + _27434);
        }
        _2 = (int)SEQ_PTR(_27435);
        _27436 = (int)*(((s1_ptr)_2)->base + 15);
        _27435 = NOVALUE;
        if (IS_ATOM_INT(_27436)) {
            _27437 = (_27436 != _52sequence_type_47103);
        }
        else {
            _27437 = binary_op(NOTEQ, _27436, _52sequence_type_47103);
        }
        _27436 = NOVALUE;
        if (_27437 == 0) {
            DeRef(_27437);
            _27437 = NOVALUE;
            goto L21; // [980] 1054
        }
        else {
            if (!IS_ATOM_INT(_27437) && DBL_PTR(_27437)->dbl == 0.0){
                DeRef(_27437);
                _27437 = NOVALUE;
                goto L21; // [980] 1054
            }
            DeRef(_27437);
            _27437 = NOVALUE;
        }
        DeRef(_27437);
        _27437 = NOVALUE;

        /** 				op = RHS_SUBS_CHECK*/
        _op_52203 = 92;
        goto L21; // [993] 1054
L22: 

        /** 		elsif SymTab[c][S_MODE] != M_CONSTANT or*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27438 = (int)*(((s1_ptr)_2)->base + _c_52207);
        _2 = (int)SEQ_PTR(_27438);
        _27439 = (int)*(((s1_ptr)_2)->base + 3);
        _27438 = NOVALUE;
        if (IS_ATOM_INT(_27439)) {
            _27440 = (_27439 != 2);
        }
        else {
            _27440 = binary_op(NOTEQ, _27439, 2);
        }
        _27439 = NOVALUE;
        if (IS_ATOM_INT(_27440)) {
            if (_27440 != 0) {
                goto L23; // [1016] 1043
            }
        }
        else {
            if (DBL_PTR(_27440)->dbl != 0.0) {
                goto L23; // [1016] 1043
            }
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27442 = (int)*(((s1_ptr)_2)->base + _c_52207);
        _2 = (int)SEQ_PTR(_27442);
        _27443 = (int)*(((s1_ptr)_2)->base + 1);
        _27442 = NOVALUE;
        _27444 = IS_SEQUENCE(_27443);
        _27443 = NOVALUE;
        _27445 = (_27444 == 0);
        _27444 = NOVALUE;
        if (_27445 == 0)
        {
            DeRef(_27445);
            _27445 = NOVALUE;
            goto L24; // [1039] 1053
        }
        else{
            DeRef(_27445);
            _27445 = NOVALUE;
        }
L23: 

        /** 			op = RHS_SUBS_CHECK*/
        _op_52203 = 92;
L24: 
L21: 

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		emit_addr(c)*/
        _37emit_addr(_c_52207);

        /** 		emit_addr(b)*/
        _37emit_addr(_b_52206);

        /** 		assignable = TRUE*/
        _37assignable_51286 = _5TRUE_244;

        /** 		Push(target)*/
        _37Push(_target_52210);

        /** 		emit_addr(target)*/
        _37emit_addr(_target_52210);

        /** 		emit_temp(target, NEW_REFERENCE)*/
        _37emit_temp(_target_52210, 1);

        /** 		current_sequence = append(current_sequence, target)*/
        Append(&_37current_sequence_51276, _37current_sequence_51276, _target_52210);

        /** 		flush_temp( Code[$-2] )*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27447 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27447 = 1;
        }
        _27448 = _27447 - 2;
        _27447 = NOVALUE;
        _2 = (int)SEQ_PTR(_25Code_12355);
        _27449 = (int)*(((s1_ptr)_2)->base + _27448);
        Ref(_27449);
        _37flush_temp(_27449);
        _27449 = NOVALUE;
        goto LC; // [1118] 7417

        /** 	case PROC then -- procedure, function and type calls*/
        case 27:

        /** 		assignable = FALSE -- assume for now*/
        _37assignable_51286 = _5FALSE_242;

        /** 		subsym = op_info1*/
        _subsym_52211 = _37op_info1_51268;

        /** 		n = SymTab[subsym][S_NUM_ARGS]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27450 = (int)*(((s1_ptr)_2)->base + _subsym_52211);
        _2 = (int)SEQ_PTR(_27450);
        if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
            _n_52216 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
        }
        else{
            _n_52216 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
        }
        if (!IS_ATOM_INT(_n_52216)){
            _n_52216 = (long)DBL_PTR(_n_52216)->dbl;
        }
        _27450 = NOVALUE;

        /** 		if subsym = CurrentSub then*/
        if (_subsym_52211 != _25CurrentSub_12270)
        goto L25; // [1160] 1345

        /** 			for i = cgi-n+1 to cgi do*/
        _27453 = _37cgi_51284 - _n_52216;
        if ((long)((unsigned long)_27453 +(unsigned long) HIGH_BITS) >= 0){
            _27453 = NewDouble((double)_27453);
        }
        if (IS_ATOM_INT(_27453)) {
            _27454 = _27453 + 1;
            if (_27454 > MAXINT){
                _27454 = NewDouble((double)_27454);
            }
        }
        else
        _27454 = binary_op(PLUS, 1, _27453);
        DeRef(_27453);
        _27453 = NOVALUE;
        _27455 = _37cgi_51284;
        {
            int _i_52462;
            Ref(_27454);
            _i_52462 = _27454;
L26: 
            if (binary_op_a(GREATER, _i_52462, _27455)){
                goto L27; // [1181] 1344
            }

            /** 				if cg_stack[i] > 0 then -- if it's a forward reference, it's not a private*/
            _2 = (int)SEQ_PTR(_37cg_stack_51283);
            if (!IS_ATOM_INT(_i_52462)){
                _27456 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52462)->dbl));
            }
            else{
                _27456 = (int)*(((s1_ptr)_2)->base + _i_52462);
            }
            if (binary_op_a(LESSEQ, _27456, 0)){
                _27456 = NOVALUE;
                goto L28; // [1196] 1337
            }
            _27456 = NOVALUE;

            /** 					if SymTab[cg_stack[i]][S_SCOPE] = SC_PRIVATE and*/
            _2 = (int)SEQ_PTR(_37cg_stack_51283);
            if (!IS_ATOM_INT(_i_52462)){
                _27458 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52462)->dbl));
            }
            else{
                _27458 = (int)*(((s1_ptr)_2)->base + _i_52462);
            }
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            if (!IS_ATOM_INT(_27458)){
                _27459 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_27458)->dbl));
            }
            else{
                _27459 = (int)*(((s1_ptr)_2)->base + _27458);
            }
            _2 = (int)SEQ_PTR(_27459);
            _27460 = (int)*(((s1_ptr)_2)->base + 4);
            _27459 = NOVALUE;
            if (IS_ATOM_INT(_27460)) {
                _27461 = (_27460 == 3);
            }
            else {
                _27461 = binary_op(EQUALS, _27460, 3);
            }
            _27460 = NOVALUE;
            if (IS_ATOM_INT(_27461)) {
                if (_27461 == 0) {
                    goto L29; // [1226] 1304
                }
            }
            else {
                if (DBL_PTR(_27461)->dbl == 0.0) {
                    goto L29; // [1226] 1304
                }
            }
            _2 = (int)SEQ_PTR(_37cg_stack_51283);
            if (!IS_ATOM_INT(_i_52462)){
                _27463 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52462)->dbl));
            }
            else{
                _27463 = (int)*(((s1_ptr)_2)->base + _i_52462);
            }
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            if (!IS_ATOM_INT(_27463)){
                _27464 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_27463)->dbl));
            }
            else{
                _27464 = (int)*(((s1_ptr)_2)->base + _27463);
            }
            _2 = (int)SEQ_PTR(_27464);
            _27465 = (int)*(((s1_ptr)_2)->base + 16);
            _27464 = NOVALUE;
            if (IS_ATOM_INT(_27465) && IS_ATOM_INT(_i_52462)) {
                _27466 = (_27465 < _i_52462);
            }
            else {
                _27466 = binary_op(LESS, _27465, _i_52462);
            }
            _27465 = NOVALUE;
            if (_27466 == 0) {
                DeRef(_27466);
                _27466 = NOVALUE;
                goto L29; // [1253] 1304
            }
            else {
                if (!IS_ATOM_INT(_27466) && DBL_PTR(_27466)->dbl == 0.0){
                    DeRef(_27466);
                    _27466 = NOVALUE;
                    goto L29; // [1253] 1304
                }
                DeRef(_27466);
                _27466 = NOVALUE;
            }
            DeRef(_27466);
            _27466 = NOVALUE;

            /** 						emit_opcode(ASSIGN)*/
            _37emit_opcode(18);

            /** 						emit_addr(cg_stack[i])*/
            _2 = (int)SEQ_PTR(_37cg_stack_51283);
            if (!IS_ATOM_INT(_i_52462)){
                _27467 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52462)->dbl));
            }
            else{
                _27467 = (int)*(((s1_ptr)_2)->base + _i_52462);
            }
            Ref(_27467);
            _37emit_addr(_27467);
            _27467 = NOVALUE;

            /** 						cg_stack[i] = NewTempSym()*/
            _27468 = _52NewTempSym(0);
            _2 = (int)SEQ_PTR(_37cg_stack_51283);
            if (!IS_ATOM_INT(_i_52462))
            _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52462)->dbl));
            else
            _2 = (int)(((s1_ptr)_2)->base + _i_52462);
            _1 = *(int *)_2;
            *(int *)_2 = _27468;
            if( _1 != _27468 ){
                DeRef(_1);
            }
            _27468 = NOVALUE;

            /** 						emit_addr(cg_stack[i])*/
            _2 = (int)SEQ_PTR(_37cg_stack_51283);
            if (!IS_ATOM_INT(_i_52462)){
                _27469 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52462)->dbl));
            }
            else{
                _27469 = (int)*(((s1_ptr)_2)->base + _i_52462);
            }
            Ref(_27469);
            _37emit_addr(_27469);
            _27469 = NOVALUE;

            /** 						check_for_temps()*/
            _37check_for_temps();
            goto L2A; // [1301] 1336
L29: 

            /** 					elsif sym_mode( cg_stack[i] ) = M_TEMP then*/
            _2 = (int)SEQ_PTR(_37cg_stack_51283);
            if (!IS_ATOM_INT(_i_52462)){
                _27470 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52462)->dbl));
            }
            else{
                _27470 = (int)*(((s1_ptr)_2)->base + _i_52462);
            }
            Ref(_27470);
            _27471 = _52sym_mode(_27470);
            _27470 = NOVALUE;
            if (binary_op_a(NOTEQ, _27471, 3)){
                DeRef(_27471);
                _27471 = NOVALUE;
                goto L2B; // [1318] 1335
            }
            DeRef(_27471);
            _27471 = NOVALUE;

            /** 						emit_temp( cg_stack[i], NEW_REFERENCE )*/
            _2 = (int)SEQ_PTR(_37cg_stack_51283);
            if (!IS_ATOM_INT(_i_52462)){
                _27473 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52462)->dbl));
            }
            else{
                _27473 = (int)*(((s1_ptr)_2)->base + _i_52462);
            }
            Ref(_27473);
            _37emit_temp(_27473, 1);
            _27473 = NOVALUE;
L2B: 
L2A: 
L28: 

            /** 			end for*/
            _0 = _i_52462;
            if (IS_ATOM_INT(_i_52462)) {
                _i_52462 = _i_52462 + 1;
                if ((long)((unsigned long)_i_52462 +(unsigned long) HIGH_BITS) >= 0){
                    _i_52462 = NewDouble((double)_i_52462);
                }
            }
            else {
                _i_52462 = binary_op_a(PLUS, _i_52462, 1);
            }
            DeRef(_0);
            goto L26; // [1339] 1188
L27: 
            ;
            DeRef(_i_52462);
        }
L25: 

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		emit_addr(subsym)*/
        _37emit_addr(_subsym_52211);

        /** 		for i = cgi-n+1 to cgi do*/
        _27474 = _37cgi_51284 - _n_52216;
        if ((long)((unsigned long)_27474 +(unsigned long) HIGH_BITS) >= 0){
            _27474 = NewDouble((double)_27474);
        }
        if (IS_ATOM_INT(_27474)) {
            _27475 = _27474 + 1;
            if (_27475 > MAXINT){
                _27475 = NewDouble((double)_27475);
            }
        }
        else
        _27475 = binary_op(PLUS, 1, _27474);
        DeRef(_27474);
        _27474 = NOVALUE;
        _27476 = _37cgi_51284;
        {
            int _i_52497;
            Ref(_27475);
            _i_52497 = _27475;
L2C: 
            if (binary_op_a(GREATER, _i_52497, _27476)){
                goto L2D; // [1372] 1409
            }

            /** 			emit_addr(cg_stack[i])*/
            _2 = (int)SEQ_PTR(_37cg_stack_51283);
            if (!IS_ATOM_INT(_i_52497)){
                _27477 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52497)->dbl));
            }
            else{
                _27477 = (int)*(((s1_ptr)_2)->base + _i_52497);
            }
            Ref(_27477);
            _37emit_addr(_27477);
            _27477 = NOVALUE;

            /** 			emit_temp( cg_stack[i], NEW_REFERENCE )*/
            _2 = (int)SEQ_PTR(_37cg_stack_51283);
            if (!IS_ATOM_INT(_i_52497)){
                _27478 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52497)->dbl));
            }
            else{
                _27478 = (int)*(((s1_ptr)_2)->base + _i_52497);
            }
            Ref(_27478);
            _37emit_temp(_27478, 1);
            _27478 = NOVALUE;

            /** 		end for*/
            _0 = _i_52497;
            if (IS_ATOM_INT(_i_52497)) {
                _i_52497 = _i_52497 + 1;
                if ((long)((unsigned long)_i_52497 +(unsigned long) HIGH_BITS) >= 0){
                    _i_52497 = NewDouble((double)_i_52497);
                }
            }
            else {
                _i_52497 = binary_op_a(PLUS, _i_52497, 1);
            }
            DeRef(_0);
            goto L2C; // [1404] 1379
L2D: 
            ;
            DeRef(_i_52497);
        }

        /** 		cgi -= n*/
        _37cgi_51284 = _37cgi_51284 - _n_52216;

        /** 		if SymTab[subsym][S_TOKEN] != PROC then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27480 = (int)*(((s1_ptr)_2)->base + _subsym_52211);
        _2 = (int)SEQ_PTR(_27480);
        if (!IS_ATOM_INT(_25S_TOKEN_11918)){
            _27481 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
        }
        else{
            _27481 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
        }
        _27480 = NOVALUE;
        if (binary_op_a(EQUALS, _27481, 27)){
            _27481 = NOVALUE;
            goto LC; // [1433] 7417
        }
        _27481 = NOVALUE;

        /** 			assignable = TRUE*/
        _37assignable_51286 = _5TRUE_244;

        /** 			c = NewTempSym() -- put final result in temp*/
        _c_52207 = _52NewTempSym(0);
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 			emit_temp( c, NEW_REFERENCE )*/
        _37emit_temp(_c_52207, 1);

        /** 			Push(c)*/
        _37Push(_c_52207);

        /** 			emit_addr(c)*/
        _37emit_addr(_c_52207);
        goto LC; // [1469] 7417

        /** 	case PROC_FORWARD, FUNC_FORWARD then*/
        case 195:
        case 196:

        /** 		assignable = FALSE -- assume for now*/
        _37assignable_51286 = _5FALSE_242;

        /** 		integer real_op*/

        /** 		if op = PROC_FORWARD then*/
        if (_op_52203 != 195)
        goto L2E; // [1490] 1506

        /** 			real_op = PROC*/
        _real_op_52518 = 27;
        goto L2F; // [1503] 1516
L2E: 

        /** 			real_op = FUNC*/
        _real_op_52518 = 501;
L2F: 

        /** 		integer ref*/

        /** 		ref = new_forward_reference( real_op, op_info1, real_op )*/
        _ref_52525 = _29new_forward_reference(_real_op_52518, _37op_info1_51268, _real_op_52518);
        if (!IS_ATOM_INT(_ref_52525)) {
            _1 = (long)(DBL_PTR(_ref_52525)->dbl);
            if (UNIQUE(DBL_PTR(_ref_52525)) && (DBL_PTR(_ref_52525)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_ref_52525);
            _ref_52525 = _1;
        }

        /** 		n = Pop() -- number of known args*/
        _n_52216 = _37Pop();
        if (!IS_ATOM_INT(_n_52216)) {
            _1 = (long)(DBL_PTR(_n_52216)->dbl);
            if (UNIQUE(DBL_PTR(_n_52216)) && (DBL_PTR(_n_52216)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_n_52216);
            _n_52216 = _1;
        }

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		emit_addr(ref)*/
        _37emit_addr(_ref_52525);

        /** 		emit_addr( n ) -- this changes to be the "next" instruction*/
        _37emit_addr(_n_52216);

        /** 		for i = cgi-n+1 to cgi do*/
        _27487 = _37cgi_51284 - _n_52216;
        if ((long)((unsigned long)_27487 +(unsigned long) HIGH_BITS) >= 0){
            _27487 = NewDouble((double)_27487);
        }
        if (IS_ATOM_INT(_27487)) {
            _27488 = _27487 + 1;
            if (_27488 > MAXINT){
                _27488 = NewDouble((double)_27488);
            }
        }
        else
        _27488 = binary_op(PLUS, 1, _27487);
        DeRef(_27487);
        _27487 = NOVALUE;
        _27489 = _37cgi_51284;
        {
            int _i_52530;
            Ref(_27488);
            _i_52530 = _27488;
L30: 
            if (binary_op_a(GREATER, _i_52530, _27489)){
                goto L31; // [1571] 1608
            }

            /** 			emit_addr(cg_stack[i])*/
            _2 = (int)SEQ_PTR(_37cg_stack_51283);
            if (!IS_ATOM_INT(_i_52530)){
                _27490 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52530)->dbl));
            }
            else{
                _27490 = (int)*(((s1_ptr)_2)->base + _i_52530);
            }
            Ref(_27490);
            _37emit_addr(_27490);
            _27490 = NOVALUE;

            /** 			emit_temp( cg_stack[i], NEW_REFERENCE )*/
            _2 = (int)SEQ_PTR(_37cg_stack_51283);
            if (!IS_ATOM_INT(_i_52530)){
                _27491 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52530)->dbl));
            }
            else{
                _27491 = (int)*(((s1_ptr)_2)->base + _i_52530);
            }
            Ref(_27491);
            _37emit_temp(_27491, 1);
            _27491 = NOVALUE;

            /** 		end for*/
            _0 = _i_52530;
            if (IS_ATOM_INT(_i_52530)) {
                _i_52530 = _i_52530 + 1;
                if ((long)((unsigned long)_i_52530 +(unsigned long) HIGH_BITS) >= 0){
                    _i_52530 = NewDouble((double)_i_52530);
                }
            }
            else {
                _i_52530 = binary_op_a(PLUS, _i_52530, 1);
            }
            DeRef(_0);
            goto L30; // [1603] 1578
L31: 
            ;
            DeRef(_i_52530);
        }

        /** 		cgi -= n*/
        _37cgi_51284 = _37cgi_51284 - _n_52216;

        /** 		if op != PROC_FORWARD then*/
        if (_op_52203 == 195)
        goto L32; // [1620] 1656

        /** 			assignable = TRUE*/
        _37assignable_51286 = _5TRUE_244;

        /** 			c = NewTempSym() -- put final result in temp*/
        _c_52207 = _52NewTempSym(0);
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 			Push(c)*/
        _37Push(_c_52207);

        /** 			emit_addr(c)*/
        _37emit_addr(_c_52207);

        /** 			emit_temp( c, NEW_REFERENCE )*/
        _37emit_temp(_c_52207, 1);
L32: 
        goto LC; // [1658] 7417

        /** 	case WARNING then*/
        case 506:

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;

        /** 	    a = Pop()*/
        _a_52205 = _37Pop();
        if (!IS_ATOM_INT(_a_52205)) {
            _1 = (long)(DBL_PTR(_a_52205)->dbl);
            if (UNIQUE(DBL_PTR(_a_52205)) && (DBL_PTR(_a_52205)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52205);
            _a_52205 = _1;
        }

        /** 		Warning(SymTab[a][S_OBJ], custom_warning_flag,"")*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27496 = (int)*(((s1_ptr)_2)->base + _a_52205);
        _2 = (int)SEQ_PTR(_27496);
        _27497 = (int)*(((s1_ptr)_2)->base + 1);
        _27496 = NOVALUE;
        Ref(_27497);
        RefDS(_22682);
        _43Warning(_27497, 64, _22682);
        _27497 = NOVALUE;
        goto LC; // [1699] 7417

        /** 	case INCLUDE_PATHS then*/
        case 507:

        /** 		sequence paths*/

        /** 		assignable = TRUE*/
        _37assignable_51286 = _5TRUE_244;

        /** 	    a = Pop()*/
        _a_52205 = _37Pop();
        if (!IS_ATOM_INT(_a_52205)) {
            _1 = (long)(DBL_PTR(_a_52205)->dbl);
            if (UNIQUE(DBL_PTR(_a_52205)) && (DBL_PTR(_a_52205)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52205);
            _a_52205 = _1;
        }

        /** 	    emit_opcode(RIGHT_BRACE_N)*/
        _37emit_opcode(31);

        /** 	    paths = Include_paths(SymTab[a][S_OBJ])*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27499 = (int)*(((s1_ptr)_2)->base + _a_52205);
        _2 = (int)SEQ_PTR(_27499);
        _27500 = (int)*(((s1_ptr)_2)->base + 1);
        _27499 = NOVALUE;
        Ref(_27500);
        _0 = _paths_52555;
        _paths_52555 = _38Include_paths(_27500);
        DeRef(_0);
        _27500 = NOVALUE;

        /** 	    emit(length(paths))*/
        if (IS_SEQUENCE(_paths_52555)){
                _27502 = SEQ_PTR(_paths_52555)->length;
        }
        else {
            _27502 = 1;
        }
        _37emit(_27502);
        _27502 = NOVALUE;

        /** 	    for i=length(paths) to 1 by -1 do*/
        if (IS_SEQUENCE(_paths_52555)){
                _27503 = SEQ_PTR(_paths_52555)->length;
        }
        else {
            _27503 = 1;
        }
        {
            int _i_52567;
            _i_52567 = _27503;
L33: 
            if (_i_52567 < 1){
                goto L34; // [1761] 1792
            }

            /** 	        c = NewStringSym(paths[i])*/
            _2 = (int)SEQ_PTR(_paths_52555);
            _27504 = (int)*(((s1_ptr)_2)->base + _i_52567);
            Ref(_27504);
            _c_52207 = _52NewStringSym(_27504);
            _27504 = NOVALUE;
            if (!IS_ATOM_INT(_c_52207)) {
                _1 = (long)(DBL_PTR(_c_52207)->dbl);
                if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
                RTFatal("Cannot assign value with a destructor to an integer");                DeRefDS(_c_52207);
                _c_52207 = _1;
            }

            /** 	        emit_addr(c)*/
            _37emit_addr(_c_52207);

            /** 	    end for*/
            _i_52567 = _i_52567 + -1;
            goto L33; // [1787] 1768
L34: 
            ;
        }

        /** 	    b = NewTempSym()*/
        _b_52206 = _52NewTempSym(0);
        if (!IS_ATOM_INT(_b_52206)) {
            _1 = (long)(DBL_PTR(_b_52206)->dbl);
            if (UNIQUE(DBL_PTR(_b_52206)) && (DBL_PTR(_b_52206)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52206);
            _b_52206 = _1;
        }

        /** 		emit_temp(b, NEW_REFERENCE)*/
        _37emit_temp(_b_52206, 1);

        /** 	    Push(b)*/
        _37Push(_b_52206);

        /** 	    emit_addr(b)*/
        _37emit_addr(_b_52206);

        /** 		last_op = RIGHT_BRACE_N*/
        _37last_op_52162 = 31;

        /** 		op = last_op*/
        _op_52203 = 31;
        DeRef(_paths_52555);
        _paths_52555 = NOVALUE;
        goto LC; // [1834] 7417

        /** 	case NOP1, NOP2, NOPWHILE, PRIVATE_INIT_CHECK, GLOBAL_INIT_CHECK,*/
        case 159:
        case 110:
        case 158:
        case 30:
        case 109:
        case 58:
        case 59:
        case 61:
        case 184:
        case 22:
        case 23:
        case 188:
        case 189:
        case 88:
        case 43:
        case 90:
        case 89:
        case 87:
        case 135:
        case 156:
        case 169:
        case 175:
        case 174:
        case 187:
        case 210:
        case 211:

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;

        /** 		if op = UPDATE_GLOBALS then*/
        if (_op_52203 != 89)
        goto LC; // [1906] 7417

        /** 			last_op = last_op_backup*/
        _37last_op_52162 = _last_op_backup_52221;

        /** 			last_pc = last_pc_backup*/
        _37last_pc_52163 = _last_pc_backup_52220;
        goto LC; // [1921] 7417

        /** 	case IF, WHILE then*/
        case 20:
        case 47:

        /** 		a = Pop()*/
        _a_52205 = _37Pop();
        if (!IS_ATOM_INT(_a_52205)) {
            _1 = (long)(DBL_PTR(_a_52205)->dbl);
            if (UNIQUE(DBL_PTR(_a_52205)) && (DBL_PTR(_a_52205)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52205);
            _a_52205 = _1;
        }

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;

        /** 		if previous_op >= LESS and previous_op <= NOT then*/
        _27509 = (_25previous_op_12379 >= 1);
        if (_27509 == 0) {
            goto L35; // [1953] 2245
        }
        _27511 = (_25previous_op_12379 <= 7);
        if (_27511 == 0)
        {
            DeRef(_27511);
            _27511 = NOVALUE;
            goto L35; // [1966] 2245
        }
        else{
            DeRef(_27511);
            _27511 = NOVALUE;
        }

        /** 			clear_temp( Code[$] )*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27512 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27512 = 1;
        }
        _2 = (int)SEQ_PTR(_25Code_12355);
        _27513 = (int)*(((s1_ptr)_2)->base + _27512);
        Ref(_27513);
        _37clear_temp(_27513);
        _27513 = NOVALUE;

        /** 			Code = Code[1..$-1]*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27514 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27514 = 1;
        }
        _27515 = _27514 - 1;
        _27514 = NOVALUE;
        rhs_slice_target = (object_ptr)&_25Code_12355;
        RHS_Slice(_25Code_12355, 1, _27515);

        /** 			if previous_op = NOT then*/
        if (_25previous_op_12379 != 7)
        goto L36; // [2007] 2087

        /** 				op = NOT_IFW*/
        _op_52203 = 108;

        /** 				backpatch(length(Code) - 1, op)*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27518 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27518 = 1;
        }
        _27519 = _27518 - 1;
        _27518 = NOVALUE;
        _37backpatch(_27519, 108);
        _27519 = NOVALUE;

        /** 				sequence if_code = Code[$-1..$]*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27520 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27520 = 1;
        }
        _27521 = _27520 - 1;
        _27520 = NOVALUE;
        if (IS_SEQUENCE(_25Code_12355)){
                _27522 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27522 = 1;
        }
        rhs_slice_target = (object_ptr)&_if_code_52635;
        RHS_Slice(_25Code_12355, _27521, _27522);

        /** 				Code = Code[1..$-2]*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27524 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27524 = 1;
        }
        _27525 = _27524 - 2;
        _27524 = NOVALUE;
        rhs_slice_target = (object_ptr)&_25Code_12355;
        RHS_Slice(_25Code_12355, 1, _27525);

        /** 				Code &= if_code*/
        Concat((object_ptr)&_25Code_12355, _25Code_12355, _if_code_52635);
        DeRefDS(_if_code_52635);
        _if_code_52635 = NOVALUE;
        goto L37; // [2084] 2232
L36: 

        /** 				if IsInteger(Code[$-1]) and IsInteger(Code[$]) then*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27528 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27528 = 1;
        }
        _27529 = _27528 - 1;
        _27528 = NOVALUE;
        _2 = (int)SEQ_PTR(_25Code_12355);
        _27530 = (int)*(((s1_ptr)_2)->base + _27529);
        Ref(_27530);
        _27531 = _37IsInteger(_27530);
        _27530 = NOVALUE;
        if (IS_ATOM_INT(_27531)) {
            if (_27531 == 0) {
                goto L38; // [2106] 2148
            }
        }
        else {
            if (DBL_PTR(_27531)->dbl == 0.0) {
                goto L38; // [2106] 2148
            }
        }
        if (IS_SEQUENCE(_25Code_12355)){
                _27533 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27533 = 1;
        }
        _2 = (int)SEQ_PTR(_25Code_12355);
        _27534 = (int)*(((s1_ptr)_2)->base + _27533);
        Ref(_27534);
        _27535 = _37IsInteger(_27534);
        _27534 = NOVALUE;
        if (_27535 == 0) {
            DeRef(_27535);
            _27535 = NOVALUE;
            goto L38; // [2124] 2148
        }
        else {
            if (!IS_ATOM_INT(_27535) && DBL_PTR(_27535)->dbl == 0.0){
                DeRef(_27535);
                _27535 = NOVALUE;
                goto L38; // [2124] 2148
            }
            DeRef(_27535);
            _27535 = NOVALUE;
        }
        DeRef(_27535);
        _27535 = NOVALUE;

        /** 					op = previous_op + LESS_IFW_I - LESS*/
        _27536 = _25previous_op_12379 + 119;
        if ((long)((unsigned long)_27536 + (unsigned long)HIGH_BITS) >= 0) 
        _27536 = NewDouble((double)_27536);
        if (IS_ATOM_INT(_27536)) {
            _op_52203 = _27536 - 1;
        }
        else {
            _op_52203 = NewDouble(DBL_PTR(_27536)->dbl - (double)1);
        }
        DeRef(_27536);
        _27536 = NOVALUE;
        if (!IS_ATOM_INT(_op_52203)) {
            _1 = (long)(DBL_PTR(_op_52203)->dbl);
            if (UNIQUE(DBL_PTR(_op_52203)) && (DBL_PTR(_op_52203)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_op_52203);
            _op_52203 = _1;
        }
        goto L39; // [2145] 2167
L38: 

        /** 					op = previous_op + LESS_IFW - LESS*/
        _27538 = _25previous_op_12379 + 102;
        if ((long)((unsigned long)_27538 + (unsigned long)HIGH_BITS) >= 0) 
        _27538 = NewDouble((double)_27538);
        if (IS_ATOM_INT(_27538)) {
            _op_52203 = _27538 - 1;
        }
        else {
            _op_52203 = NewDouble(DBL_PTR(_27538)->dbl - (double)1);
        }
        DeRef(_27538);
        _27538 = NOVALUE;
        if (!IS_ATOM_INT(_op_52203)) {
            _1 = (long)(DBL_PTR(_op_52203)->dbl);
            if (UNIQUE(DBL_PTR(_op_52203)) && (DBL_PTR(_op_52203)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_op_52203);
            _op_52203 = _1;
        }
L39: 

        /** 				backpatch(length(Code) - 2, op)*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27540 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27540 = 1;
        }
        _27541 = _27540 - 2;
        _27540 = NOVALUE;
        _37backpatch(_27541, _op_52203);
        _27541 = NOVALUE;

        /** 				sequence if_code = Code[$-2..$]*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27542 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27542 = 1;
        }
        _27543 = _27542 - 2;
        _27542 = NOVALUE;
        if (IS_SEQUENCE(_25Code_12355)){
                _27544 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27544 = 1;
        }
        rhs_slice_target = (object_ptr)&_if_code_52674;
        RHS_Slice(_25Code_12355, _27543, _27544);

        /** 				Code = Code[1..$-3]*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27546 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27546 = 1;
        }
        _27547 = _27546 - 3;
        _27546 = NOVALUE;
        rhs_slice_target = (object_ptr)&_25Code_12355;
        RHS_Slice(_25Code_12355, 1, _27547);

        /** 				Code &= if_code*/
        Concat((object_ptr)&_25Code_12355, _25Code_12355, _if_code_52674);
        DeRefDS(_if_code_52674);
        _if_code_52674 = NOVALUE;
L37: 

        /** 			last_pc = last_pc_backup*/
        _37last_pc_52163 = _last_pc_backup_52220;

        /** 			last_op = op*/
        _37last_op_52162 = _op_52203;
        goto LC; // [2242] 7417
L35: 

        /** 		elsif op = WHILE and*/
        _27550 = (_op_52203 == 47);
        if (_27550 == 0) {
            _27551 = 0;
            goto L3A; // [2253] 2265
        }
        _27552 = (_a_52205 > 0);
        _27551 = (_27552 != 0);
L3A: 
        if (_27551 == 0) {
            _27553 = 0;
            goto L3B; // [2265] 2291
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27554 = (int)*(((s1_ptr)_2)->base + _a_52205);
        _2 = (int)SEQ_PTR(_27554);
        _27555 = (int)*(((s1_ptr)_2)->base + 3);
        _27554 = NOVALUE;
        if (IS_ATOM_INT(_27555)) {
            _27556 = (_27555 == 2);
        }
        else {
            _27556 = binary_op(EQUALS, _27555, 2);
        }
        _27555 = NOVALUE;
        if (IS_ATOM_INT(_27556))
        _27553 = (_27556 != 0);
        else
        _27553 = DBL_PTR(_27556)->dbl != 0.0;
L3B: 
        if (_27553 == 0) {
            _27557 = 0;
            goto L3C; // [2291] 2314
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27558 = (int)*(((s1_ptr)_2)->base + _a_52205);
        _2 = (int)SEQ_PTR(_27558);
        _27559 = (int)*(((s1_ptr)_2)->base + 1);
        _27558 = NOVALUE;
        if (IS_ATOM_INT(_27559))
        _27560 = 1;
        else if (IS_ATOM_DBL(_27559))
        _27560 = IS_ATOM_INT(DoubleToInt(_27559));
        else
        _27560 = 0;
        _27559 = NOVALUE;
        _27557 = (_27560 != 0);
L3C: 
        if (_27557 == 0) {
            goto L3D; // [2314] 2363
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27562 = (int)*(((s1_ptr)_2)->base + _a_52205);
        _2 = (int)SEQ_PTR(_27562);
        _27563 = (int)*(((s1_ptr)_2)->base + 1);
        _27562 = NOVALUE;
        if (_27563 == 0)
        _27564 = 1;
        else if (IS_ATOM_INT(_27563) && IS_ATOM_INT(0))
        _27564 = 0;
        else
        _27564 = (compare(_27563, 0) == 0);
        _27563 = NOVALUE;
        _27565 = (_27564 == 0);
        _27564 = NOVALUE;
        if (_27565 == 0)
        {
            DeRef(_27565);
            _27565 = NOVALUE;
            goto L3D; // [2338] 2363
        }
        else{
            DeRef(_27565);
            _27565 = NOVALUE;
        }

        /** 			optimized_while = TRUE   -- while TRUE ... emit nothing*/
        _37optimized_while_51270 = _5TRUE_244;

        /** 			last_pc = last_pc_backup*/
        _37last_pc_52163 = _last_pc_backup_52220;

        /** 			last_op = last_op_backup*/
        _37last_op_52162 = _last_op_backup_52221;
        goto LC; // [2360] 7417
L3D: 

        /** 			flush_temps( {a} )*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _a_52205;
        _27566 = MAKE_SEQ(_1);
        _37flush_temps(_27566);
        _27566 = NOVALUE;

        /** 			emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 			emit_addr(a)*/
        _37emit_addr(_a_52205);
        goto LC; // [2383] 7417

        /** 	case INTEGER_CHECK then*/
        case 96:

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;

        /** 		if previous_op = ASSIGN then*/
        if (_25previous_op_12379 != 18)
        goto L3E; // [2402] 2461

        /** 			c = Code[$-1]*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27568 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27568 = 1;
        }
        _27569 = _27568 - 1;
        _27568 = NOVALUE;
        _2 = (int)SEQ_PTR(_25Code_12355);
        _c_52207 = (int)*(((s1_ptr)_2)->base + _27569);
        if (!IS_ATOM_INT(_c_52207)){
            _c_52207 = (long)DBL_PTR(_c_52207)->dbl;
        }

        /** 			if not IsInteger(c) then*/
        _27571 = _37IsInteger(_c_52207);
        if (IS_ATOM_INT(_27571)) {
            if (_27571 != 0){
                DeRef(_27571);
                _27571 = NOVALUE;
                goto L3F; // [2429] 2447
            }
        }
        else {
            if (DBL_PTR(_27571)->dbl != 0.0){
                DeRef(_27571);
                _27571 = NOVALUE;
                goto L3F; // [2429] 2447
            }
        }
        DeRef(_27571);
        _27571 = NOVALUE;

        /** 				emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 				emit_addr(op_info1)*/
        _37emit_addr(_37op_info1_51268);
        goto L40; // [2444] 2518
L3F: 

        /** 				last_op = last_op_backup*/
        _37last_op_52162 = _last_op_backup_52221;

        /** 				last_pc = last_pc_backup*/
        _37last_pc_52163 = _last_pc_backup_52220;
        goto L40; // [2458] 2518
L3E: 

        /** 		elsif previous_op = -1 or*/
        _27573 = (_25previous_op_12379 == -1);
        if (_27573 != 0) {
            goto L41; // [2469] 2492
        }
        _2 = (int)SEQ_PTR(_37op_result_51884);
        _27575 = (int)*(((s1_ptr)_2)->base + _25previous_op_12379);
        _27576 = (_27575 != 1);
        _27575 = NOVALUE;
        if (_27576 == 0)
        {
            DeRef(_27576);
            _27576 = NOVALUE;
            goto L42; // [2488] 2507
        }
        else{
            DeRef(_27576);
            _27576 = NOVALUE;
        }
L41: 

        /** 			emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 			emit_addr(op_info1)*/
        _37emit_addr(_37op_info1_51268);
        goto L40; // [2504] 2518
L42: 

        /** 			last_op = last_op_backup*/
        _37last_op_52162 = _last_op_backup_52221;

        /** 			last_pc = last_pc_backup*/
        _37last_pc_52163 = _last_pc_backup_52220;
L40: 

        /** 		clear_temp( Code[$-1] )*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27577 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27577 = 1;
        }
        _27578 = _27577 - 1;
        _27577 = NOVALUE;
        _2 = (int)SEQ_PTR(_25Code_12355);
        _27579 = (int)*(((s1_ptr)_2)->base + _27578);
        Ref(_27579);
        _37clear_temp(_27579);
        _27579 = NOVALUE;
        goto LC; // [2536] 7417

        /** 	case SEQUENCE_CHECK then*/
        case 97:

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;

        /** 		if previous_op = ASSIGN then*/
        if (_25previous_op_12379 != 18)
        goto L43; // [2555] 2682

        /** 			c = Code[$-1]*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27581 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27581 = 1;
        }
        _27582 = _27581 - 1;
        _27581 = NOVALUE;
        _2 = (int)SEQ_PTR(_25Code_12355);
        _c_52207 = (int)*(((s1_ptr)_2)->base + _27582);
        if (!IS_ATOM_INT(_c_52207)){
            _c_52207 = (long)DBL_PTR(_c_52207)->dbl;
        }

        /** 			if c < 1 or*/
        _27584 = (_c_52207 < 1);
        if (_27584 != 0) {
            _27585 = 1;
            goto L44; // [2582] 2608
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27586 = (int)*(((s1_ptr)_2)->base + _c_52207);
        _2 = (int)SEQ_PTR(_27586);
        _27587 = (int)*(((s1_ptr)_2)->base + 3);
        _27586 = NOVALUE;
        if (IS_ATOM_INT(_27587)) {
            _27588 = (_27587 != 2);
        }
        else {
            _27588 = binary_op(NOTEQ, _27587, 2);
        }
        _27587 = NOVALUE;
        if (IS_ATOM_INT(_27588))
        _27585 = (_27588 != 0);
        else
        _27585 = DBL_PTR(_27588)->dbl != 0.0;
L44: 
        if (_27585 != 0) {
            goto L45; // [2608] 2635
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27590 = (int)*(((s1_ptr)_2)->base + _c_52207);
        _2 = (int)SEQ_PTR(_27590);
        _27591 = (int)*(((s1_ptr)_2)->base + 1);
        _27590 = NOVALUE;
        _27592 = IS_SEQUENCE(_27591);
        _27591 = NOVALUE;
        _27593 = (_27592 == 0);
        _27592 = NOVALUE;
        if (_27593 == 0)
        {
            DeRef(_27593);
            _27593 = NOVALUE;
            goto L46; // [2631] 2668
        }
        else{
            DeRef(_27593);
            _27593 = NOVALUE;
        }
L45: 

        /** 				emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 				emit_addr(op_info1)*/
        _37emit_addr(_37op_info1_51268);

        /** 				clear_temp( Code[$-1] )*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27594 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27594 = 1;
        }
        _27595 = _27594 - 1;
        _27594 = NOVALUE;
        _2 = (int)SEQ_PTR(_25Code_12355);
        _27596 = (int)*(((s1_ptr)_2)->base + _27595);
        Ref(_27596);
        _37clear_temp(_27596);
        _27596 = NOVALUE;
        goto LC; // [2665] 7417
L46: 

        /** 				last_op = last_op_backup*/
        _37last_op_52162 = _last_op_backup_52221;

        /** 				last_pc = last_pc_backup*/
        _37last_pc_52163 = _last_pc_backup_52220;
        goto LC; // [2679] 7417
L43: 

        /** 		elsif previous_op = -1 or op_result[previous_op] != T_SEQUENCE then*/
        _27597 = (_25previous_op_12379 == -1);
        if (_27597 != 0) {
            goto L47; // [2690] 2713
        }
        _2 = (int)SEQ_PTR(_37op_result_51884);
        _27599 = (int)*(((s1_ptr)_2)->base + _25previous_op_12379);
        _27600 = (_27599 != 2);
        _27599 = NOVALUE;
        if (_27600 == 0)
        {
            DeRef(_27600);
            _27600 = NOVALUE;
            goto L48; // [2709] 2746
        }
        else{
            DeRef(_27600);
            _27600 = NOVALUE;
        }
L47: 

        /** 			emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 			emit_addr(op_info1)*/
        _37emit_addr(_37op_info1_51268);

        /** 			clear_temp( Code[$-1] )*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27601 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27601 = 1;
        }
        _27602 = _27601 - 1;
        _27601 = NOVALUE;
        _2 = (int)SEQ_PTR(_25Code_12355);
        _27603 = (int)*(((s1_ptr)_2)->base + _27602);
        Ref(_27603);
        _37clear_temp(_27603);
        _27603 = NOVALUE;
        goto LC; // [2743] 7417
L48: 

        /** 			last_op = last_op_backup*/
        _37last_op_52162 = _last_op_backup_52221;

        /** 			last_pc = last_pc_backup*/
        _37last_pc_52163 = _last_pc_backup_52220;
        goto LC; // [2757] 7417

        /** 	case ATOM_CHECK then*/
        case 101:

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;

        /** 		if previous_op = ASSIGN then*/
        if (_25previous_op_12379 != 18)
        goto L49; // [2776] 2975

        /** 			c = Code[$-1]*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27605 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27605 = 1;
        }
        _27606 = _27605 - 1;
        _27605 = NOVALUE;
        _2 = (int)SEQ_PTR(_25Code_12355);
        _c_52207 = (int)*(((s1_ptr)_2)->base + _27606);
        if (!IS_ATOM_INT(_c_52207)){
            _c_52207 = (long)DBL_PTR(_c_52207)->dbl;
        }

        /** 			if c > 1*/
        _27608 = (_c_52207 > 1);
        if (_27608 == 0) {
            goto L4A; // [2803] 2924
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27610 = (int)*(((s1_ptr)_2)->base + _c_52207);
        _2 = (int)SEQ_PTR(_27610);
        _27611 = (int)*(((s1_ptr)_2)->base + 3);
        _27610 = NOVALUE;
        if (IS_ATOM_INT(_27611)) {
            _27612 = (_27611 == 2);
        }
        else {
            _27612 = binary_op(EQUALS, _27611, 2);
        }
        _27611 = NOVALUE;
        if (_27612 == 0) {
            DeRef(_27612);
            _27612 = NOVALUE;
            goto L4A; // [2826] 2924
        }
        else {
            if (!IS_ATOM_INT(_27612) && DBL_PTR(_27612)->dbl == 0.0){
                DeRef(_27612);
                _27612 = NOVALUE;
                goto L4A; // [2826] 2924
            }
            DeRef(_27612);
            _27612 = NOVALUE;
        }
        DeRef(_27612);
        _27612 = NOVALUE;

        /** 				if sequence( SymTab[c][S_OBJ] ) then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27613 = (int)*(((s1_ptr)_2)->base + _c_52207);
        _2 = (int)SEQ_PTR(_27613);
        _27614 = (int)*(((s1_ptr)_2)->base + 1);
        _27613 = NOVALUE;
        _27615 = IS_SEQUENCE(_27614);
        _27614 = NOVALUE;
        if (_27615 == 0)
        {
            _27615 = NOVALUE;
            goto L4B; // [2846] 2875
        }
        else{
            _27615 = NOVALUE;
        }

        /** 					ThisLine = ExprLine*/
        RefDS(_30ExprLine_57228);
        DeRef(_43ThisLine_49532);
        _43ThisLine_49532 = _30ExprLine_57228;

        /** 					bp = expr_bp*/
        _43bp_49536 = _30expr_bp_57229;

        /** 					CompileErr( 346 )*/
        RefDS(_22682);
        _43CompileErr(346, _22682, 0);
        goto L4C; // [2872] 3054
L4B: 

        /** 				elsif SymTab[c][S_OBJ] = NOVALUE then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27616 = (int)*(((s1_ptr)_2)->base + _c_52207);
        _2 = (int)SEQ_PTR(_27616);
        _27617 = (int)*(((s1_ptr)_2)->base + 1);
        _27616 = NOVALUE;
        if (binary_op_a(NOTEQ, _27617, _25NOVALUE_12115)){
            _27617 = NOVALUE;
            goto L4D; // [2891] 2910
        }
        _27617 = NOVALUE;

        /** 					emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 					emit_addr(op_info1)*/
        _37emit_addr(_37op_info1_51268);
        goto L4C; // [2907] 3054
L4D: 

        /** 					last_op = last_op_backup*/
        _37last_op_52162 = _last_op_backup_52221;

        /** 					last_pc = last_pc_backup*/
        _37last_pc_52163 = _last_pc_backup_52220;
        goto L4C; // [2921] 3054
L4A: 

        /** 			elsif c < 1 */
        _27619 = (_c_52207 < 1);
        if (_27619 != 0) {
            goto L4E; // [2930] 2946
        }
        _27621 = _37IsInteger(_c_52207);
        if (IS_ATOM_INT(_27621)) {
            _27622 = (_27621 == 0);
        }
        else {
            _27622 = unary_op(NOT, _27621);
        }
        DeRef(_27621);
        _27621 = NOVALUE;
        if (_27622 == 0) {
            DeRef(_27622);
            _27622 = NOVALUE;
            goto L4F; // [2942] 2961
        }
        else {
            if (!IS_ATOM_INT(_27622) && DBL_PTR(_27622)->dbl == 0.0){
                DeRef(_27622);
                _27622 = NOVALUE;
                goto L4F; // [2942] 2961
            }
            DeRef(_27622);
            _27622 = NOVALUE;
        }
        DeRef(_27622);
        _27622 = NOVALUE;
L4E: 

        /** 				emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 				emit_addr(op_info1)*/
        _37emit_addr(_37op_info1_51268);
        goto L4C; // [2958] 3054
L4F: 

        /** 				last_op = last_op_backup*/
        _37last_op_52162 = _last_op_backup_52221;

        /** 				last_pc = last_pc_backup*/
        _37last_pc_52163 = _last_pc_backup_52220;
        goto L4C; // [2972] 3054
L49: 

        /** 		elsif previous_op = -1 or*/
        _27623 = (_25previous_op_12379 == -1);
        if (_27623 != 0) {
            goto L50; // [2983] 3028
        }
        _2 = (int)SEQ_PTR(_37op_result_51884);
        _27625 = (int)*(((s1_ptr)_2)->base + _25previous_op_12379);
        _27626 = (_27625 != 1);
        _27625 = NOVALUE;
        if (_27626 == 0) {
            DeRef(_27627);
            _27627 = 0;
            goto L51; // [3001] 3023
        }
        _2 = (int)SEQ_PTR(_37op_result_51884);
        _27628 = (int)*(((s1_ptr)_2)->base + _25previous_op_12379);
        _27629 = (_27628 != 3);
        _27628 = NOVALUE;
        _27627 = (_27629 != 0);
L51: 
        if (_27627 == 0)
        {
            _27627 = NOVALUE;
            goto L52; // [3024] 3043
        }
        else{
            _27627 = NOVALUE;
        }
L50: 

        /** 			emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 			emit_addr(op_info1)*/
        _37emit_addr(_37op_info1_51268);
        goto L4C; // [3040] 3054
L52: 

        /** 			last_op = last_op_backup*/
        _37last_op_52162 = _last_op_backup_52221;

        /** 			last_pc = last_pc_backup*/
        _37last_pc_52163 = _last_pc_backup_52220;
L4C: 

        /** 		clear_temp( Code[$-1] )*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27630 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27630 = 1;
        }
        _27631 = _27630 - 1;
        _27630 = NOVALUE;
        _2 = (int)SEQ_PTR(_25Code_12355);
        _27632 = (int)*(((s1_ptr)_2)->base + _27631);
        Ref(_27632);
        _37clear_temp(_27632);
        _27632 = NOVALUE;
        goto LC; // [3072] 7417

        /** 	case RIGHT_BRACE_N then*/
        case 31:

        /** 		n = op_info1*/
        _n_52216 = _37op_info1_51268;

        /** 		elements = {}*/
        RefDS(_22682);
        DeRef(_elements_52218);
        _elements_52218 = _22682;

        /** 		for i = 1 to n do*/
        _27633 = _n_52216;
        {
            int _i_52854;
            _i_52854 = 1;
L53: 
            if (_i_52854 > _27633){
                goto L54; // [3097] 3120
            }

            /** 			elements = append(elements, Pop())*/
            _27634 = _37Pop();
            Ref(_27634);
            Append(&_elements_52218, _elements_52218, _27634);
            DeRef(_27634);
            _27634 = NOVALUE;

            /** 		end for*/
            _i_52854 = _i_52854 + 1;
            goto L53; // [3115] 3104
L54: 
            ;
        }

        /** 		element_vals = good_string(elements)*/
        RefDS(_elements_52218);
        _0 = _element_vals_52219;
        _element_vals_52219 = _37good_string(_elements_52218);
        DeRef(_0);

        /** 		if sequence(element_vals) then*/
        _27637 = IS_SEQUENCE(_element_vals_52219);
        if (_27637 == 0)
        {
            _27637 = NOVALUE;
            goto L55; // [3131] 3162
        }
        else{
            _27637 = NOVALUE;
        }

        /** 			c = NewStringSym(element_vals)  -- make a string literal*/
        Ref(_element_vals_52219);
        _c_52207 = _52NewStringSym(_element_vals_52219);
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 			assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;

        /** 			last_op = last_op_backup*/
        _37last_op_52162 = _last_op_backup_52221;

        /** 			last_pc = last_pc_backup*/
        _37last_pc_52163 = _last_pc_backup_52220;
        goto L56; // [3159] 3253
L55: 

        /** 			if n = 2 then*/
        if (_n_52216 != 2)
        goto L57; // [3164] 3187

        /** 				emit_opcode(RIGHT_BRACE_2) -- faster op for two items*/
        _37emit_opcode(85);

        /** 				last_op = RIGHT_BRACE_2*/
        _37last_op_52162 = 85;
        goto L58; // [3184] 3198
L57: 

        /** 				emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 				emit(n)*/
        _37emit(_n_52216);
L58: 

        /** 			for i = 1 to n do*/
        _27640 = _n_52216;
        {
            int _i_52871;
            _i_52871 = 1;
L59: 
            if (_i_52871 > _27640){
                goto L5A; // [3203] 3226
            }

            /** 				emit_addr(elements[i])*/
            _2 = (int)SEQ_PTR(_elements_52218);
            _27641 = (int)*(((s1_ptr)_2)->base + _i_52871);
            Ref(_27641);
            _37emit_addr(_27641);
            _27641 = NOVALUE;

            /** 			end for*/
            _i_52871 = _i_52871 + 1;
            goto L59; // [3221] 3210
L5A: 
            ;
        }

        /** 			c = NewTempSym()*/
        _c_52207 = _52NewTempSym(0);
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 			emit_addr(c)*/
        _37emit_addr(_c_52207);

        /** 			emit_temp( c, NEW_REFERENCE )*/
        _37emit_temp(_c_52207, 1);

        /** 			assignable = TRUE*/
        _37assignable_51286 = _5TRUE_244;
L56: 

        /** 		Push(c)*/
        _37Push(_c_52207);
        goto LC; // [3258] 7417

        /** 	case ASSIGN_SUBS2,      -- can't change the op*/
        case 148:
        case 16:
        case 162:

        /** 		b = Pop() -- rhs value*/
        _b_52206 = _37Pop();
        if (!IS_ATOM_INT(_b_52206)) {
            _1 = (long)(DBL_PTR(_b_52206)->dbl);
            if (UNIQUE(DBL_PTR(_b_52206)) && (DBL_PTR(_b_52206)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52206);
            _b_52206 = _1;
        }

        /** 		a = Pop() -- subscript*/
        _a_52205 = _37Pop();
        if (!IS_ATOM_INT(_a_52205)) {
            _1 = (long)(DBL_PTR(_a_52205)->dbl);
            if (UNIQUE(DBL_PTR(_a_52205)) && (DBL_PTR(_a_52205)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52205);
            _a_52205 = _1;
        }

        /** 		c = Pop() -- sequence*/
        _c_52207 = _37Pop();
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		if op = ASSIGN_SUBS then*/
        if (_op_52203 != 16)
        goto L5B; // [3293] 3485

        /** 			if (previous_op != LHS_SUBS) and*/
        _27647 = (_25previous_op_12379 != 95);
        if (_27647 == 0) {
            _27648 = 0;
            goto L5C; // [3307] 3319
        }
        _27649 = (_c_52207 > 0);
        _27648 = (_27649 != 0);
L5C: 
        if (_27648 == 0) {
            goto L5D; // [3319] 3457
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27651 = (int)*(((s1_ptr)_2)->base + _c_52207);
        _2 = (int)SEQ_PTR(_27651);
        _27652 = (int)*(((s1_ptr)_2)->base + 3);
        _27651 = NOVALUE;
        if (IS_ATOM_INT(_27652)) {
            _27653 = (_27652 != 1);
        }
        else {
            _27653 = binary_op(NOTEQ, _27652, 1);
        }
        _27652 = NOVALUE;
        if (IS_ATOM_INT(_27653)) {
            if (_27653 != 0) {
                DeRef(_27654);
                _27654 = 1;
                goto L5E; // [3341] 3441
            }
        }
        else {
            if (DBL_PTR(_27653)->dbl != 0.0) {
                DeRef(_27654);
                _27654 = 1;
                goto L5E; // [3341] 3441
            }
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27655 = (int)*(((s1_ptr)_2)->base + _c_52207);
        _2 = (int)SEQ_PTR(_27655);
        _27656 = (int)*(((s1_ptr)_2)->base + 15);
        _27655 = NOVALUE;
        if (IS_ATOM_INT(_27656)) {
            _27657 = (_27656 != _52sequence_type_47103);
        }
        else {
            _27657 = binary_op(NOTEQ, _27656, _52sequence_type_47103);
        }
        _27656 = NOVALUE;
        if (IS_ATOM_INT(_27657)) {
            if (_27657 == 0) {
                DeRef(_27658);
                _27658 = 0;
                goto L5F; // [3363] 3437
            }
        }
        else {
            if (DBL_PTR(_27657)->dbl == 0.0) {
                DeRef(_27658);
                _27658 = 0;
                goto L5F; // [3363] 3437
            }
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27659 = (int)*(((s1_ptr)_2)->base + _c_52207);
        _2 = (int)SEQ_PTR(_27659);
        _27660 = (int)*(((s1_ptr)_2)->base + 15);
        _27659 = NOVALUE;
        if (IS_ATOM_INT(_27660)) {
            _27661 = (_27660 > 0);
        }
        else {
            _27661 = binary_op(GREATER, _27660, 0);
        }
        _27660 = NOVALUE;
        if (IS_ATOM_INT(_27661)) {
            if (_27661 == 0) {
                DeRef(_27662);
                _27662 = 0;
                goto L60; // [3383] 3433
            }
        }
        else {
            if (DBL_PTR(_27661)->dbl == 0.0) {
                DeRef(_27662);
                _27662 = 0;
                goto L60; // [3383] 3433
            }
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27663 = (int)*(((s1_ptr)_2)->base + _c_52207);
        _2 = (int)SEQ_PTR(_27663);
        _27664 = (int)*(((s1_ptr)_2)->base + 15);
        _27663 = NOVALUE;
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!IS_ATOM_INT(_27664)){
            _27665 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_27664)->dbl));
        }
        else{
            _27665 = (int)*(((s1_ptr)_2)->base + _27664);
        }
        _2 = (int)SEQ_PTR(_27665);
        _27666 = (int)*(((s1_ptr)_2)->base + 2);
        _27665 = NOVALUE;
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!IS_ATOM_INT(_27666)){
            _27667 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_27666)->dbl));
        }
        else{
            _27667 = (int)*(((s1_ptr)_2)->base + _27666);
        }
        _2 = (int)SEQ_PTR(_27667);
        _27668 = (int)*(((s1_ptr)_2)->base + 15);
        _27667 = NOVALUE;
        if (IS_ATOM_INT(_27668)) {
            _27669 = (_27668 != _52sequence_type_47103);
        }
        else {
            _27669 = binary_op(NOTEQ, _27668, _52sequence_type_47103);
        }
        _27668 = NOVALUE;
        DeRef(_27662);
        if (IS_ATOM_INT(_27669))
        _27662 = (_27669 != 0);
        else
        _27662 = DBL_PTR(_27669)->dbl != 0.0;
L60: 
        DeRef(_27658);
        _27658 = (_27662 != 0);
L5F: 
        DeRef(_27654);
        _27654 = (_27658 != 0);
L5E: 
        if (_27654 == 0)
        {
            _27654 = NOVALUE;
            goto L5D; // [3442] 3457
        }
        else{
            _27654 = NOVALUE;
        }

        /** 				op = ASSIGN_SUBS_CHECK*/
        _op_52203 = 84;
        goto L61; // [3454] 3477
L5D: 

        /** 				if IsInteger(b) then*/
        _27670 = _37IsInteger(_b_52206);
        if (_27670 == 0) {
            DeRef(_27670);
            _27670 = NOVALUE;
            goto L62; // [3463] 3476
        }
        else {
            if (!IS_ATOM_INT(_27670) && DBL_PTR(_27670)->dbl == 0.0){
                DeRef(_27670);
                _27670 = NOVALUE;
                goto L62; // [3463] 3476
            }
            DeRef(_27670);
            _27670 = NOVALUE;
        }
        DeRef(_27670);
        _27670 = NOVALUE;

        /** 					op = ASSIGN_SUBS_I*/
        _op_52203 = 118;
L62: 
L61: 

        /** 			emit_opcode(op)*/
        _37emit_opcode(_op_52203);
        goto L63; // [3482] 3511
L5B: 

        /** 		elsif op = PASSIGN_SUBS then*/
        if (_op_52203 != 162)
        goto L64; // [3489] 3503

        /** 			emit_opcode(PASSIGN_SUBS) -- always*/
        _37emit_opcode(162);
        goto L63; // [3500] 3511
L64: 

        /** 			emit_opcode(ASSIGN_SUBS) -- always*/
        _37emit_opcode(16);
L63: 

        /** 		emit_addr(c) -- sequence*/
        _37emit_addr(_c_52207);

        /** 		emit_addr(a) -- subscript*/
        _37emit_addr(_a_52205);

        /** 		emit_addr(b) -- rhs value*/
        _37emit_addr(_b_52206);

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [3533] 7417

        /** 	case LHS_SUBS, LHS_SUBS1, LHS_SUBS1_COPY then*/
        case 95:
        case 161:
        case 166:

        /** 		a = Pop() -- subs*/
        _a_52205 = _37Pop();
        if (!IS_ATOM_INT(_a_52205)) {
            _1 = (long)(DBL_PTR(_a_52205)->dbl);
            if (UNIQUE(DBL_PTR(_a_52205)) && (DBL_PTR(_a_52205)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52205);
            _a_52205 = _1;
        }

        /** 		lhs_var = Pop() -- sequence*/
        _lhs_var_52213 = _37Pop();
        if (!IS_ATOM_INT(_lhs_var_52213)) {
            _1 = (long)(DBL_PTR(_lhs_var_52213)->dbl);
            if (UNIQUE(DBL_PTR(_lhs_var_52213)) && (DBL_PTR(_lhs_var_52213)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_lhs_var_52213);
            _lhs_var_52213 = _1;
        }

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		emit_addr(lhs_var)*/
        _37emit_addr(_lhs_var_52213);

        /** 		emit_addr(a)*/
        _37emit_addr(_a_52205);

        /** 		if op = LHS_SUBS then*/
        if (_op_52203 != 95)
        goto L65; // [3576] 3607

        /** 			TempKeep(lhs_var) -- should be lhs_target_temp*/
        _37TempKeep(_lhs_var_52213);

        /** 			emit_addr(lhs_target_temp)*/
        _37emit_addr(_37lhs_target_temp_51282);

        /** 			Push(lhs_target_temp)*/
        _37Push(_37lhs_target_temp_51282);

        /** 			emit_addr(0) -- place holder*/
        _37emit_addr(0);
        goto L66; // [3604] 3661
L65: 

        /** 			lhs_target_temp = NewTempSym() -- use same temp for all subscripts*/
        _0 = _52NewTempSym(0);
        _37lhs_target_temp_51282 = _0;
        if (!IS_ATOM_INT(_37lhs_target_temp_51282)) {
            _1 = (long)(DBL_PTR(_37lhs_target_temp_51282)->dbl);
            if (UNIQUE(DBL_PTR(_37lhs_target_temp_51282)) && (DBL_PTR(_37lhs_target_temp_51282)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_37lhs_target_temp_51282);
            _37lhs_target_temp_51282 = _1;
        }

        /** 			emit_addr(lhs_target_temp) -- target temp holds pointer to sequence*/
        _37emit_addr(_37lhs_target_temp_51282);

        /** 			emit_temp(lhs_target_temp, NEW_REFERENCE )*/
        _37emit_temp(_37lhs_target_temp_51282, 1);

        /** 			Push(lhs_target_temp)*/
        _37Push(_37lhs_target_temp_51282);

        /** 			lhs_subs1_copy_temp = NewTempSym() -- place to copy (may be ignored)*/
        _0 = _52NewTempSym(0);
        _37lhs_subs1_copy_temp_51281 = _0;
        if (!IS_ATOM_INT(_37lhs_subs1_copy_temp_51281)) {
            _1 = (long)(DBL_PTR(_37lhs_subs1_copy_temp_51281)->dbl);
            if (UNIQUE(DBL_PTR(_37lhs_subs1_copy_temp_51281)) && (DBL_PTR(_37lhs_subs1_copy_temp_51281)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_37lhs_subs1_copy_temp_51281);
            _37lhs_subs1_copy_temp_51281 = _1;
        }

        /** 			emit_addr(lhs_subs1_copy_temp)*/
        _37emit_addr(_37lhs_subs1_copy_temp_51281);

        /** 			emit_temp( lhs_subs1_copy_temp, NEW_REFERENCE )*/
        _37emit_temp(_37lhs_subs1_copy_temp_51281, 1);
L66: 

        /** 		current_sequence = append(current_sequence, lhs_target_temp)*/
        Append(&_37current_sequence_51276, _37current_sequence_51276, _37lhs_target_temp_51282);

        /** 		assignable = FALSE  -- need to update current_sequence like in RHS_SUBS*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [3678] 7417

        /** 	case RAND, PEEK, PEEK4S, PEEK4U, NOT_BITS, NOT,*/
        case 62:
        case 127:
        case 139:
        case 140:
        case 51:
        case 7:
        case 173:
        case 180:
        case 179:
        case 181:
        case 182:

        /** 		cont11ii(op, TRUE)*/
        _37cont11ii(_op_52203, _5TRUE_244);
        goto LC; // [3712] 7417

        /** 	case UMINUS then*/
        case 12:

        /** 		a = Pop()*/
        _a_52205 = _37Pop();
        if (!IS_ATOM_INT(_a_52205)) {
            _1 = (long)(DBL_PTR(_a_52205)->dbl);
            if (UNIQUE(DBL_PTR(_a_52205)) && (DBL_PTR(_a_52205)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52205);
            _a_52205 = _1;
        }

        /** 		if a > 0 then*/
        if (_a_52205 <= 0)
        goto L67; // [3727] 3973

        /** 			obj = SymTab[a][S_OBJ]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27680 = (int)*(((s1_ptr)_2)->base + _a_52205);
        DeRef(_obj_52217);
        _2 = (int)SEQ_PTR(_27680);
        _obj_52217 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_obj_52217);
        _27680 = NOVALUE;

        /** 			if SymTab[a][S_MODE] = M_CONSTANT then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27682 = (int)*(((s1_ptr)_2)->base + _a_52205);
        _2 = (int)SEQ_PTR(_27682);
        _27683 = (int)*(((s1_ptr)_2)->base + 3);
        _27682 = NOVALUE;
        if (binary_op_a(NOTEQ, _27683, 2)){
            _27683 = NOVALUE;
            goto L68; // [3761] 3885
        }
        _27683 = NOVALUE;

        /** 				if integer(obj) then*/
        if (IS_ATOM_INT(_obj_52217))
        _27685 = 1;
        else if (IS_ATOM_DBL(_obj_52217))
        _27685 = IS_ATOM_INT(DoubleToInt(_obj_52217));
        else
        _27685 = 0;
        if (_27685 == 0)
        {
            _27685 = NOVALUE;
            goto L69; // [3770] 3824
        }
        else{
            _27685 = NOVALUE;
        }

        /** 					if obj = MININT then*/
        if (binary_op_a(NOTEQ, _obj_52217, -1073741824)){
            goto L6A; // [3777] 3798
        }

        /** 						Push(NewDoubleSym(-MININT))*/
        if ((unsigned long)-1073741824 == 0xC0000000)
        _27687 = (int)NewDouble((double)-0xC0000000);
        else
        _27687 = - -1073741824;
        _27688 = _52NewDoubleSym(_27687);
        _27687 = NOVALUE;
        _37Push(_27688);
        _27688 = NOVALUE;
        goto L6B; // [3795] 3811
L6A: 

        /** 						Push(NewIntSym(-obj))*/
        if (IS_ATOM_INT(_obj_52217)) {
            if ((unsigned long)_obj_52217 == 0xC0000000)
            _27689 = (int)NewDouble((double)-0xC0000000);
            else
            _27689 = - _obj_52217;
        }
        else {
            _27689 = unary_op(UMINUS, _obj_52217);
        }
        _27690 = _52NewIntSym(_27689);
        _27689 = NOVALUE;
        _37Push(_27690);
        _27690 = NOVALUE;
L6B: 

        /** 					last_pc = last_pc_backup*/
        _37last_pc_52163 = _last_pc_backup_52220;

        /** 					last_op = last_op_backup*/
        _37last_op_52162 = _last_op_backup_52221;
        goto LC; // [3821] 7417
L69: 

        /** 				elsif atom(obj) and obj != NOVALUE then*/
        _27691 = IS_ATOM(_obj_52217);
        if (_27691 == 0) {
            goto L6C; // [3829] 3868
        }
        if (IS_ATOM_INT(_obj_52217) && IS_ATOM_INT(_25NOVALUE_12115)) {
            _27693 = (_obj_52217 != _25NOVALUE_12115);
        }
        else {
            _27693 = binary_op(NOTEQ, _obj_52217, _25NOVALUE_12115);
        }
        if (_27693 == 0) {
            DeRef(_27693);
            _27693 = NOVALUE;
            goto L6C; // [3840] 3868
        }
        else {
            if (!IS_ATOM_INT(_27693) && DBL_PTR(_27693)->dbl == 0.0){
                DeRef(_27693);
                _27693 = NOVALUE;
                goto L6C; // [3840] 3868
            }
            DeRef(_27693);
            _27693 = NOVALUE;
        }
        DeRef(_27693);
        _27693 = NOVALUE;

        /** 					Push(NewDoubleSym(-obj))*/
        if (IS_ATOM_INT(_obj_52217)) {
            if ((unsigned long)_obj_52217 == 0xC0000000)
            _27694 = (int)NewDouble((double)-0xC0000000);
            else
            _27694 = - _obj_52217;
        }
        else {
            _27694 = unary_op(UMINUS, _obj_52217);
        }
        _27695 = _52NewDoubleSym(_27694);
        _27694 = NOVALUE;
        _37Push(_27695);
        _27695 = NOVALUE;

        /** 					last_pc = last_pc_backup*/
        _37last_pc_52163 = _last_pc_backup_52220;

        /** 					last_op = last_op_backup*/
        _37last_op_52162 = _last_op_backup_52221;
        goto LC; // [3865] 7417
L6C: 

        /** 					Push(a)*/
        _37Push(_a_52205);

        /** 					cont11ii(op, FALSE)*/
        _37cont11ii(_op_52203, _5FALSE_242);
        goto LC; // [3882] 7417
L68: 

        /** 			elsif TRANSLATE and SymTab[a][S_MODE] = M_TEMP and*/
        if (_25TRANSLATE_11874 == 0) {
            _27696 = 0;
            goto L6D; // [3889] 3915
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27697 = (int)*(((s1_ptr)_2)->base + _a_52205);
        _2 = (int)SEQ_PTR(_27697);
        _27698 = (int)*(((s1_ptr)_2)->base + 3);
        _27697 = NOVALUE;
        if (IS_ATOM_INT(_27698)) {
            _27699 = (_27698 == 3);
        }
        else {
            _27699 = binary_op(EQUALS, _27698, 3);
        }
        _27698 = NOVALUE;
        if (IS_ATOM_INT(_27699))
        _27696 = (_27699 != 0);
        else
        _27696 = DBL_PTR(_27699)->dbl != 0.0;
L6D: 
        if (_27696 == 0) {
            goto L6E; // [3915] 3956
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27701 = (int)*(((s1_ptr)_2)->base + _a_52205);
        _2 = (int)SEQ_PTR(_27701);
        _27702 = (int)*(((s1_ptr)_2)->base + 36);
        _27701 = NOVALUE;
        if (IS_ATOM_INT(_27702)) {
            _27703 = (_27702 == 2);
        }
        else {
            _27703 = binary_op(EQUALS, _27702, 2);
        }
        _27702 = NOVALUE;
        if (_27703 == 0) {
            DeRef(_27703);
            _27703 = NOVALUE;
            goto L6E; // [3938] 3956
        }
        else {
            if (!IS_ATOM_INT(_27703) && DBL_PTR(_27703)->dbl == 0.0){
                DeRef(_27703);
                _27703 = NOVALUE;
                goto L6E; // [3938] 3956
            }
            DeRef(_27703);
            _27703 = NOVALUE;
        }
        DeRef(_27703);
        _27703 = NOVALUE;

        /** 				Push(NewDoubleSym(-obj))*/
        if (IS_ATOM_INT(_obj_52217)) {
            if ((unsigned long)_obj_52217 == 0xC0000000)
            _27704 = (int)NewDouble((double)-0xC0000000);
            else
            _27704 = - _obj_52217;
        }
        else {
            _27704 = unary_op(UMINUS, _obj_52217);
        }
        _27705 = _52NewDoubleSym(_27704);
        _27704 = NOVALUE;
        _37Push(_27705);
        _27705 = NOVALUE;
        goto LC; // [3953] 7417
L6E: 

        /** 				Push(a)*/
        _37Push(_a_52205);

        /** 				cont11ii(op, FALSE)*/
        _37cont11ii(_op_52203, _5FALSE_242);
        goto LC; // [3970] 7417
L67: 

        /** 			Push(a)*/
        _37Push(_a_52205);

        /** 			cont11ii(op, FALSE)*/
        _37cont11ii(_op_52203, _5FALSE_242);
        goto LC; // [3987] 7417

        /** 	case LENGTH, GETC, SQRT, SIN, COS, TAN, ARCTAN, LOG, GETS, GETENV then*/
        case 42:
        case 33:
        case 41:
        case 80:
        case 81:
        case 82:
        case 73:
        case 74:
        case 17:
        case 91:

        /** 		cont11ii(op, FALSE)*/
        _37cont11ii(_op_52203, _5FALSE_242);
        goto LC; // [4019] 7417

        /** 	case IS_AN_INTEGER, IS_AN_ATOM, IS_A_SEQUENCE, IS_AN_OBJECT then*/
        case 94:
        case 67:
        case 68:
        case 40:

        /** 		cont11ii(op, FALSE)*/
        _37cont11ii(_op_52203, _5FALSE_242);
        goto LC; // [4039] 7417

        /** 	case ROUTINE_ID then*/
        case 134:

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		source = Pop()*/
        _source_52209 = _37Pop();
        if (!IS_ATOM_INT(_source_52209)) {
            _1 = (long)(DBL_PTR(_source_52209)->dbl);
            if (UNIQUE(DBL_PTR(_source_52209)) && (DBL_PTR(_source_52209)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_source_52209);
            _source_52209 = _1;
        }

        /** 		if TRANSLATE then*/
        if (_25TRANSLATE_11874 == 0)
        {
            goto L6F; // [4061] 4105
        }
        else{
        }

        /** 			emit_addr(num_routines-1)*/
        _27707 = _25num_routines_12271 - 1;
        if ((long)((unsigned long)_27707 +(unsigned long) HIGH_BITS) >= 0){
            _27707 = NewDouble((double)_27707);
        }
        _37emit_addr(_27707);
        _27707 = NOVALUE;

        /** 			last_routine_id = num_routines*/
        _37last_routine_id_51273 = _25num_routines_12271;

        /** 			last_max_params = max_params*/
        _37last_max_params_51275 = _37max_params_51274;

        /** 			MarkTargets(source, S_RI_TARGET)*/
        _32378 = _52MarkTargets(_source_52209, 53);
        DeRef(_32378);
        _32378 = NOVALUE;
        goto L70; // [4102] 4142
L6F: 

        /** 			emit_addr(CurrentSub)*/
        _37emit_addr(_25CurrentSub_12270);

        /** 			emit_addr(length(SymTab))*/
        if (IS_SEQUENCE(_26SymTab_11138)){
                _27708 = SEQ_PTR(_26SymTab_11138)->length;
        }
        else {
            _27708 = 1;
        }
        _37emit_addr(_27708);
        _27708 = NOVALUE;

        /** 			if BIND then*/
        if (_25BIND_11877 == 0)
        {
            goto L71; // [4126] 4141
        }
        else{
        }

        /** 				MarkTargets(source, S_NREFS)*/
        _32377 = _52MarkTargets(_source_52209, 12);
        DeRef(_32377);
        _32377 = NOVALUE;
L71: 
L70: 

        /** 		emit_addr(source)*/
        _37emit_addr(_source_52209);

        /** 		emit_addr(current_file_no)  -- necessary at top level*/
        _37emit_addr(_25current_file_no_12262);

        /** 		assignable = TRUE*/
        _37assignable_51286 = _5TRUE_244;

        /** 		c = NewTempSym()*/
        _c_52207 = _52NewTempSym(0);
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		TempInteger(c) -- result will always be an integer*/
        _37TempInteger(_c_52207);

        /** 		Push(c)*/
        _37Push(_c_52207);

        /** 		emit_addr(c)*/
        _37emit_addr(_c_52207);
        goto LC; // [4184] 7417

        /** 	case SC1_OR, SC1_AND then*/
        case 143:
        case 141:

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		emit_addr(Pop())*/
        _27710 = _37Pop();
        _37emit_addr(_27710);
        _27710 = NOVALUE;

        /** 		c = NewTempSym()*/
        _c_52207 = _52NewTempSym(0);
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		Push(c)*/
        _37Push(_c_52207);

        /** 		emit_addr(c)*/
        _37emit_addr(_c_52207);

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [4230] 7417

        /** 	case SYSTEM, PUTS, PRINT, QPRINT, POSITION, MACHINE_PROC,*/
        case 99:
        case 44:
        case 19:
        case 36:
        case 60:
        case 112:
        case 132:
        case 128:
        case 138:
        case 168:
        case 178:

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		b = Pop()*/
        _b_52206 = _37Pop();
        if (!IS_ATOM_INT(_b_52206)) {
            _1 = (long)(DBL_PTR(_b_52206)->dbl);
            if (UNIQUE(DBL_PTR(_b_52206)) && (DBL_PTR(_b_52206)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52206);
            _b_52206 = _1;
        }

        /** 		emit_addr(Pop())*/
        _27713 = _37Pop();
        _37emit_addr(_27713);
        _27713 = NOVALUE;

        /** 		emit_addr(b)*/
        _37emit_addr(_b_52206);

        /** 		if op = C_PROC then*/
        if (_op_52203 != 132)
        goto L72; // [4285] 4297

        /** 			emit_addr(CurrentSub)*/
        _37emit_addr(_25CurrentSub_12270);
L72: 

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [4304] 7417

        /** 	case EQUALS, LESS, GREATER, NOTEQ, LESSEQ, GREATEREQ,*/
        case 3:
        case 1:
        case 6:
        case 4:
        case 5:
        case 2:
        case 8:
        case 9:
        case 152:
        case 71:
        case 56:
        case 24:
        case 26:

        /** 		cont21ii(op, TRUE)  -- both integer args => integer result*/
        _37cont21ii(_op_52203, _5TRUE_244);
        goto LC; // [4342] 7417

        /** 	case PLUS then -- elsif op = PLUS then*/
        case 11:

        /** 		b = Pop()*/
        _b_52206 = _37Pop();
        if (!IS_ATOM_INT(_b_52206)) {
            _1 = (long)(DBL_PTR(_b_52206)->dbl);
            if (UNIQUE(DBL_PTR(_b_52206)) && (DBL_PTR(_b_52206)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52206);
            _b_52206 = _1;
        }

        /** 		a = Pop()*/
        _a_52205 = _37Pop();
        if (!IS_ATOM_INT(_a_52205)) {
            _1 = (long)(DBL_PTR(_a_52205)->dbl);
            if (UNIQUE(DBL_PTR(_a_52205)) && (DBL_PTR(_a_52205)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52205);
            _a_52205 = _1;
        }

        /** 		if b < 1 or a < 1 then*/
        _27717 = (_b_52206 < 1);
        if (_27717 != 0) {
            goto L73; // [4368] 4381
        }
        _27719 = (_a_52205 < 1);
        if (_27719 == 0)
        {
            DeRef(_27719);
            _27719 = NOVALUE;
            goto L74; // [4377] 4402
        }
        else{
            DeRef(_27719);
            _27719 = NOVALUE;
        }
L73: 

        /** 			Push(a)*/
        _37Push(_a_52205);

        /** 			Push(b)*/
        _37Push(_b_52206);

        /** 			cont21ii(op, FALSE)*/
        _37cont21ii(_op_52203, _5FALSE_242);
        goto LC; // [4399] 7417
L74: 

        /** 		elsif SymTab[b][S_MODE] = M_CONSTANT and equal(SymTab[b][S_OBJ], 1) then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27720 = (int)*(((s1_ptr)_2)->base + _b_52206);
        _2 = (int)SEQ_PTR(_27720);
        _27721 = (int)*(((s1_ptr)_2)->base + 3);
        _27720 = NOVALUE;
        if (IS_ATOM_INT(_27721)) {
            _27722 = (_27721 == 2);
        }
        else {
            _27722 = binary_op(EQUALS, _27721, 2);
        }
        _27721 = NOVALUE;
        if (IS_ATOM_INT(_27722)) {
            if (_27722 == 0) {
                goto L75; // [4422] 4483
            }
        }
        else {
            if (DBL_PTR(_27722)->dbl == 0.0) {
                goto L75; // [4422] 4483
            }
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27724 = (int)*(((s1_ptr)_2)->base + _b_52206);
        _2 = (int)SEQ_PTR(_27724);
        _27725 = (int)*(((s1_ptr)_2)->base + 1);
        _27724 = NOVALUE;
        if (_27725 == 1)
        _27726 = 1;
        else if (IS_ATOM_INT(_27725) && IS_ATOM_INT(1))
        _27726 = 0;
        else
        _27726 = (compare(_27725, 1) == 0);
        _27725 = NOVALUE;
        if (_27726 == 0)
        {
            _27726 = NOVALUE;
            goto L75; // [4443] 4483
        }
        else{
            _27726 = NOVALUE;
        }

        /** 			op = PLUS1*/
        _op_52203 = 93;

        /** 			emit_opcode(op)*/
        _37emit_opcode(93);

        /** 			emit_addr(a)*/
        _37emit_addr(_a_52205);

        /** 			emit_addr(0)*/
        _37emit_addr(0);

        /** 			cont21d(op, a, b, FALSE)*/
        _37cont21d(93, _a_52205, _b_52206, _5FALSE_242);
        goto LC; // [4480] 7417
L75: 

        /** 		elsif SymTab[a][S_MODE] = M_CONSTANT and equal(SymTab[a][S_OBJ], 1) then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27727 = (int)*(((s1_ptr)_2)->base + _a_52205);
        _2 = (int)SEQ_PTR(_27727);
        _27728 = (int)*(((s1_ptr)_2)->base + 3);
        _27727 = NOVALUE;
        if (IS_ATOM_INT(_27728)) {
            _27729 = (_27728 == 2);
        }
        else {
            _27729 = binary_op(EQUALS, _27728, 2);
        }
        _27728 = NOVALUE;
        if (IS_ATOM_INT(_27729)) {
            if (_27729 == 0) {
                goto L76; // [4503] 4564
            }
        }
        else {
            if (DBL_PTR(_27729)->dbl == 0.0) {
                goto L76; // [4503] 4564
            }
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27731 = (int)*(((s1_ptr)_2)->base + _a_52205);
        _2 = (int)SEQ_PTR(_27731);
        _27732 = (int)*(((s1_ptr)_2)->base + 1);
        _27731 = NOVALUE;
        if (_27732 == 1)
        _27733 = 1;
        else if (IS_ATOM_INT(_27732) && IS_ATOM_INT(1))
        _27733 = 0;
        else
        _27733 = (compare(_27732, 1) == 0);
        _27732 = NOVALUE;
        if (_27733 == 0)
        {
            _27733 = NOVALUE;
            goto L76; // [4524] 4564
        }
        else{
            _27733 = NOVALUE;
        }

        /** 			op = PLUS1*/
        _op_52203 = 93;

        /** 			emit_opcode(op)*/
        _37emit_opcode(93);

        /** 			emit_addr(b)*/
        _37emit_addr(_b_52206);

        /** 			emit_addr(0)*/
        _37emit_addr(0);

        /** 			cont21d(op, a, b, FALSE)*/
        _37cont21d(93, _a_52205, _b_52206, _5FALSE_242);
        goto LC; // [4561] 7417
L76: 

        /** 			Push(a)*/
        _37Push(_a_52205);

        /** 			Push(b)*/
        _37Push(_b_52206);

        /** 			cont21ii(op, FALSE)*/
        _37cont21ii(_op_52203, _5FALSE_242);
        goto LC; // [4583] 7417

        /** 	case rw:MULTIPLY then*/
        case 13:

        /** 		b = Pop()*/
        _b_52206 = _37Pop();
        if (!IS_ATOM_INT(_b_52206)) {
            _1 = (long)(DBL_PTR(_b_52206)->dbl);
            if (UNIQUE(DBL_PTR(_b_52206)) && (DBL_PTR(_b_52206)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52206);
            _b_52206 = _1;
        }

        /** 		a = Pop()*/
        _a_52205 = _37Pop();
        if (!IS_ATOM_INT(_a_52205)) {
            _1 = (long)(DBL_PTR(_a_52205)->dbl);
            if (UNIQUE(DBL_PTR(_a_52205)) && (DBL_PTR(_a_52205)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52205);
            _a_52205 = _1;
        }

        /** 		if a < 1 or b < 1 then*/
        _27736 = (_a_52205 < 1);
        if (_27736 != 0) {
            goto L77; // [4609] 4622
        }
        _27738 = (_b_52206 < 1);
        if (_27738 == 0)
        {
            DeRef(_27738);
            _27738 = NOVALUE;
            goto L78; // [4618] 4643
        }
        else{
            DeRef(_27738);
            _27738 = NOVALUE;
        }
L77: 

        /** 			Push(a)*/
        _37Push(_a_52205);

        /** 			Push(b)*/
        _37Push(_b_52206);

        /** 			cont21ii(op, FALSE)*/
        _37cont21ii(_op_52203, _5FALSE_242);
        goto LC; // [4640] 7417
L78: 

        /** 		elsif SymTab[b][S_MODE] = M_CONSTANT and equal(SymTab[b][S_OBJ], 2) then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27739 = (int)*(((s1_ptr)_2)->base + _b_52206);
        _2 = (int)SEQ_PTR(_27739);
        _27740 = (int)*(((s1_ptr)_2)->base + 3);
        _27739 = NOVALUE;
        if (IS_ATOM_INT(_27740)) {
            _27741 = (_27740 == 2);
        }
        else {
            _27741 = binary_op(EQUALS, _27740, 2);
        }
        _27740 = NOVALUE;
        if (IS_ATOM_INT(_27741)) {
            if (_27741 == 0) {
                goto L79; // [4663] 4724
            }
        }
        else {
            if (DBL_PTR(_27741)->dbl == 0.0) {
                goto L79; // [4663] 4724
            }
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27743 = (int)*(((s1_ptr)_2)->base + _b_52206);
        _2 = (int)SEQ_PTR(_27743);
        _27744 = (int)*(((s1_ptr)_2)->base + 1);
        _27743 = NOVALUE;
        if (_27744 == 2)
        _27745 = 1;
        else if (IS_ATOM_INT(_27744) && IS_ATOM_INT(2))
        _27745 = 0;
        else
        _27745 = (compare(_27744, 2) == 0);
        _27744 = NOVALUE;
        if (_27745 == 0)
        {
            _27745 = NOVALUE;
            goto L79; // [4684] 4724
        }
        else{
            _27745 = NOVALUE;
        }

        /** 			op = PLUS*/
        _op_52203 = 11;

        /** 			emit_opcode(op)*/
        _37emit_opcode(11);

        /** 			emit_addr(a)*/
        _37emit_addr(_a_52205);

        /** 			emit_addr(a)*/
        _37emit_addr(_a_52205);

        /** 			cont21d(op, a, b, FALSE)*/
        _37cont21d(11, _a_52205, _b_52206, _5FALSE_242);
        goto LC; // [4721] 7417
L79: 

        /** 		elsif SymTab[a][S_MODE] = M_CONSTANT and equal(SymTab[a][S_OBJ], 2) then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27746 = (int)*(((s1_ptr)_2)->base + _a_52205);
        _2 = (int)SEQ_PTR(_27746);
        _27747 = (int)*(((s1_ptr)_2)->base + 3);
        _27746 = NOVALUE;
        if (IS_ATOM_INT(_27747)) {
            _27748 = (_27747 == 2);
        }
        else {
            _27748 = binary_op(EQUALS, _27747, 2);
        }
        _27747 = NOVALUE;
        if (IS_ATOM_INT(_27748)) {
            if (_27748 == 0) {
                goto L7A; // [4744] 4805
            }
        }
        else {
            if (DBL_PTR(_27748)->dbl == 0.0) {
                goto L7A; // [4744] 4805
            }
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27750 = (int)*(((s1_ptr)_2)->base + _a_52205);
        _2 = (int)SEQ_PTR(_27750);
        _27751 = (int)*(((s1_ptr)_2)->base + 1);
        _27750 = NOVALUE;
        if (_27751 == 2)
        _27752 = 1;
        else if (IS_ATOM_INT(_27751) && IS_ATOM_INT(2))
        _27752 = 0;
        else
        _27752 = (compare(_27751, 2) == 0);
        _27751 = NOVALUE;
        if (_27752 == 0)
        {
            _27752 = NOVALUE;
            goto L7A; // [4765] 4805
        }
        else{
            _27752 = NOVALUE;
        }

        /** 			op = PLUS*/
        _op_52203 = 11;

        /** 			emit_opcode(op)*/
        _37emit_opcode(11);

        /** 			emit_addr(b)*/
        _37emit_addr(_b_52206);

        /** 			emit_addr(b)*/
        _37emit_addr(_b_52206);

        /** 			cont21d(op, a, b, FALSE)*/
        _37cont21d(11, _a_52205, _b_52206, _5FALSE_242);
        goto LC; // [4802] 7417
L7A: 

        /** 			Push(a)*/
        _37Push(_a_52205);

        /** 			Push(b)*/
        _37Push(_b_52206);

        /** 			cont21ii(op, FALSE)*/
        _37cont21ii(_op_52203, _5FALSE_242);
        goto LC; // [4824] 7417

        /** 	case rw:DIVIDE then*/
        case 14:

        /** 		b = Pop()*/
        _b_52206 = _37Pop();
        if (!IS_ATOM_INT(_b_52206)) {
            _1 = (long)(DBL_PTR(_b_52206)->dbl);
            if (UNIQUE(DBL_PTR(_b_52206)) && (DBL_PTR(_b_52206)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52206);
            _b_52206 = _1;
        }

        /** 		if b > 0 and SymTab[b][S_MODE] = M_CONSTANT and equal(SymTab[b][S_OBJ], 2) then*/
        _27754 = (_b_52206 > 0);
        if (_27754 == 0) {
            _27755 = 0;
            goto L7B; // [4843] 4869
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27756 = (int)*(((s1_ptr)_2)->base + _b_52206);
        _2 = (int)SEQ_PTR(_27756);
        _27757 = (int)*(((s1_ptr)_2)->base + 3);
        _27756 = NOVALUE;
        if (IS_ATOM_INT(_27757)) {
            _27758 = (_27757 == 2);
        }
        else {
            _27758 = binary_op(EQUALS, _27757, 2);
        }
        _27757 = NOVALUE;
        if (IS_ATOM_INT(_27758))
        _27755 = (_27758 != 0);
        else
        _27755 = DBL_PTR(_27758)->dbl != 0.0;
L7B: 
        if (_27755 == 0) {
            goto L7C; // [4869] 4940
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27760 = (int)*(((s1_ptr)_2)->base + _b_52206);
        _2 = (int)SEQ_PTR(_27760);
        _27761 = (int)*(((s1_ptr)_2)->base + 1);
        _27760 = NOVALUE;
        if (_27761 == 2)
        _27762 = 1;
        else if (IS_ATOM_INT(_27761) && IS_ATOM_INT(2))
        _27762 = 0;
        else
        _27762 = (compare(_27761, 2) == 0);
        _27761 = NOVALUE;
        if (_27762 == 0)
        {
            _27762 = NOVALUE;
            goto L7C; // [4890] 4940
        }
        else{
            _27762 = NOVALUE;
        }

        /** 			op = DIV2*/
        _op_52203 = 98;

        /** 			emit_opcode(op)*/
        _37emit_opcode(98);

        /** 			emit_addr(Pop()) -- n.b. "a" hasn't been set*/
        _27763 = _37Pop();
        _37emit_addr(_27763);
        _27763 = NOVALUE;

        /** 			a = 0*/
        _a_52205 = 0;

        /** 			emit_addr(0)*/
        _37emit_addr(0);

        /** 			cont21d(op, a, b, FALSE)  -- could have fractional result*/
        _37cont21d(98, 0, _b_52206, _5FALSE_242);
        goto LC; // [4937] 7417
L7C: 

        /** 			Push(b)*/
        _37Push(_b_52206);

        /** 			cont21ii(op, FALSE)*/
        _37cont21ii(_op_52203, _5FALSE_242);
        goto LC; // [4954] 7417

        /** 	case FLOOR then*/
        case 83:

        /** 		if previous_op = rw:DIVIDE then*/
        if (_25previous_op_12379 != 14)
        goto L7D; // [4964] 5012

        /** 			op = FLOOR_DIV*/
        _op_52203 = 63;

        /** 			backpatch(length(Code) - 3, op)*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27765 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27765 = 1;
        }
        _27766 = _27765 - 3;
        _27765 = NOVALUE;
        _37backpatch(_27766, 63);
        _27766 = NOVALUE;

        /** 			assignable = TRUE*/
        _37assignable_51286 = _5TRUE_244;

        /** 			last_op = op*/
        _37last_op_52162 = 63;

        /** 			last_pc = last_pc_backup*/
        _37last_pc_52163 = _last_pc_backup_52220;
        goto LC; // [5009] 7417
L7D: 

        /** 		elsif previous_op = DIV2 then*/
        if (_25previous_op_12379 != 98)
        goto L7E; // [5018] 5105

        /** 			op = FLOOR_DIV2*/
        _op_52203 = 66;

        /** 			backpatch(length(Code) - 3, op)*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27768 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27768 = 1;
        }
        _27769 = _27768 - 3;
        _27768 = NOVALUE;
        _37backpatch(_27769, 66);
        _27769 = NOVALUE;

        /** 			assignable = TRUE*/
        _37assignable_51286 = _5TRUE_244;

        /** 			if IsInteger(Code[$-2]) then*/
        if (IS_SEQUENCE(_25Code_12355)){
                _27770 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _27770 = 1;
        }
        _27771 = _27770 - 2;
        _27770 = NOVALUE;
        _2 = (int)SEQ_PTR(_25Code_12355);
        _27772 = (int)*(((s1_ptr)_2)->base + _27771);
        Ref(_27772);
        _27773 = _37IsInteger(_27772);
        _27772 = NOVALUE;
        if (_27773 == 0) {
            DeRef(_27773);
            _27773 = NOVALUE;
            goto L7F; // [5072] 5092
        }
        else {
            if (!IS_ATOM_INT(_27773) && DBL_PTR(_27773)->dbl == 0.0){
                DeRef(_27773);
                _27773 = NOVALUE;
                goto L7F; // [5072] 5092
            }
            DeRef(_27773);
            _27773 = NOVALUE;
        }
        DeRef(_27773);
        _27773 = NOVALUE;

        /** 				TempInteger(Top()) --mark temp as integer type*/

        /** 	return cg_stack[cgi]*/
        DeRef(_Top_inlined_Top_at_5200_53241);
        _2 = (int)SEQ_PTR(_37cg_stack_51283);
        _Top_inlined_Top_at_5200_53241 = (int)*(((s1_ptr)_2)->base + _37cgi_51284);
        Ref(_Top_inlined_Top_at_5200_53241);
        Ref(_Top_inlined_Top_at_5200_53241);
        _37TempInteger(_Top_inlined_Top_at_5200_53241);
L7F: 

        /** 			last_op = op*/
        _37last_op_52162 = _op_52203;

        /** 			last_pc = last_pc_backup*/
        _37last_pc_52163 = _last_pc_backup_52220;
        goto LC; // [5102] 7417
L7E: 

        /** 			cont11ii(op, TRUE)*/
        _37cont11ii(_op_52203, _5TRUE_244);
        goto LC; // [5114] 7417

        /** 	case MINUS, rw:APPEND, PREPEND, COMPARE, EQUAL,*/
        case 10:
        case 35:
        case 57:
        case 76:
        case 153:
        case 154:
        case 15:
        case 32:
        case 111:
        case 133:
        case 53:
        case 167:
        case 194:
        case 198:
        case 199:
        case 204:

        /** 		cont21ii(op, FALSE)*/
        _37cont21ii(_op_52203, _5FALSE_242);
        goto LC; // [5158] 7417

        /** 	case SC2_NULL then  -- correct the stack - we aren't emitting anything*/
        case 145:

        /** 		c = Pop()*/
        _c_52207 = _37Pop();
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		TempKeep(c)*/
        _37TempKeep(_c_52207);

        /** 		b = Pop()  -- remove SC1's temp*/
        _b_52206 = _37Pop();
        if (!IS_ATOM_INT(_b_52206)) {
            _1 = (long)(DBL_PTR(_b_52206)->dbl);
            if (UNIQUE(DBL_PTR(_b_52206)) && (DBL_PTR(_b_52206)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52206);
            _b_52206 = _1;
        }

        /** 		Push(c)*/
        _37Push(_c_52207);

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;

        /** 		last_op = last_op_backup*/
        _37last_op_52162 = _last_op_backup_52221;

        /** 		last_pc = last_pc_backup*/
        _37last_pc_52163 = _last_pc_backup_52220;
        goto LC; // [5205] 7417

        /** 	case SC2_AND, SC2_OR then*/
        case 142:
        case 144:

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		emit_addr(Pop())*/
        _27776 = _37Pop();
        _37emit_addr(_27776);
        _27776 = NOVALUE;

        /** 		c = Pop()*/
        _c_52207 = _37Pop();
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		TempKeep(c)*/
        _37TempKeep(_c_52207);

        /** 		emit_addr(c) -- target*/
        _37emit_addr(_c_52207);

        /** 		TempInteger(c)*/
        _37TempInteger(_c_52207);

        /** 		Push(c)*/
        _37Push(_c_52207);

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [5260] 7417

        /** 	case MEM_COPY, MEM_SET, PRINTF then*/
        case 130:
        case 131:
        case 38:

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		c = Pop()*/
        _c_52207 = _37Pop();
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		b = Pop()*/
        _b_52206 = _37Pop();
        if (!IS_ATOM_INT(_b_52206)) {
            _1 = (long)(DBL_PTR(_b_52206)->dbl);
            if (UNIQUE(DBL_PTR(_b_52206)) && (DBL_PTR(_b_52206)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52206);
            _b_52206 = _1;
        }

        /** 		emit_addr(Pop())*/
        _27780 = _37Pop();
        _37emit_addr(_27780);
        _27780 = NOVALUE;

        /** 		emit_addr(b)*/
        _37emit_addr(_b_52206);

        /** 		emit_addr(c)*/
        _37emit_addr(_c_52207);

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [5314] 7417

        /** 	case RHS_SLICE, FIND, MATCH, FIND_FROM, MATCH_FROM, SPLICE, INSERT, REMOVE, OPEN then*/
        case 46:
        case 77:
        case 78:
        case 176:
        case 177:
        case 190:
        case 191:
        case 200:
        case 37:

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		c = Pop()*/
        _c_52207 = _37Pop();
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		b = Pop()*/
        _b_52206 = _37Pop();
        if (!IS_ATOM_INT(_b_52206)) {
            _1 = (long)(DBL_PTR(_b_52206)->dbl);
            if (UNIQUE(DBL_PTR(_b_52206)) && (DBL_PTR(_b_52206)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52206);
            _b_52206 = _1;
        }

        /** 		emit_addr(Pop())*/
        _27783 = _37Pop();
        _37emit_addr(_27783);
        _27783 = NOVALUE;

        /** 		emit_addr(b)*/
        _37emit_addr(_b_52206);

        /** 		emit_addr(c)*/
        _37emit_addr(_c_52207);

        /** 		c = NewTempSym()*/
        _c_52207 = _52NewTempSym(0);
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		if op = FIND or op = FIND_FROM or op = OPEN then*/
        _27785 = (_op_52203 == 77);
        if (_27785 != 0) {
            _27786 = 1;
            goto L80; // [5389] 5403
        }
        _27787 = (_op_52203 == 176);
        _27786 = (_27787 != 0);
L80: 
        if (_27786 != 0) {
            goto L81; // [5403] 5418
        }
        _27789 = (_op_52203 == 37);
        if (_27789 == 0)
        {
            DeRef(_27789);
            _27789 = NOVALUE;
            goto L82; // [5414] 5426
        }
        else{
            DeRef(_27789);
            _27789 = NOVALUE;
        }
L81: 

        /** 			TempInteger( c )*/
        _37TempInteger(_c_52207);
        goto L83; // [5423] 5433
L82: 

        /** 			emit_temp( c, NEW_REFERENCE )*/
        _37emit_temp(_c_52207, 1);
L83: 

        /** 		assignable = TRUE*/
        _37assignable_51286 = _5TRUE_244;

        /** 		Push(c)*/
        _37Push(_c_52207);

        /** 		emit_addr(c)*/
        _37emit_addr(_c_52207);
        goto LC; // [5450] 7417

        /** 	case CONCAT_N then     -- concatenate 3 or more items*/
        case 157:

        /** 		n = op_info1  -- number of items to concatenate*/
        _n_52216 = _37op_info1_51268;

        /** 		emit_opcode(CONCAT_N)*/
        _37emit_opcode(157);

        /** 		emit(n)*/
        _37emit(_n_52216);

        /** 		for i = 1 to n do*/
        _27790 = _n_52216;
        {
            int _i_53309;
            _i_53309 = 1;
L84: 
            if (_i_53309 > _27790){
                goto L85; // [5480] 5508
            }

            /** 			symtab_index element = Pop()*/
            _element_53312 = _37Pop();
            if (!IS_ATOM_INT(_element_53312)) {
                _1 = (long)(DBL_PTR(_element_53312)->dbl);
                if (UNIQUE(DBL_PTR(_element_53312)) && (DBL_PTR(_element_53312)->cleanup != 0))
                RTFatal("Cannot assign value with a destructor to an integer");                DeRefDS(_element_53312);
                _element_53312 = _1;
            }

            /** 			emit_addr( element )  -- reverse order*/
            _37emit_addr(_element_53312);

            /** 		end for*/
            _i_53309 = _i_53309 + 1;
            goto L84; // [5503] 5487
L85: 
            ;
        }

        /** 		c = NewTempSym()*/
        _c_52207 = _52NewTempSym(0);
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		emit_addr(c)*/
        _37emit_addr(_c_52207);

        /** 		emit_temp( c, NEW_REFERENCE )*/
        _37emit_temp(_c_52207, 1);

        /** 		assignable = TRUE*/
        _37assignable_51286 = _5TRUE_244;

        /** 		Push(c)*/
        _37Push(_c_52207);
        goto LC; // [5539] 7417

        /** 	case FOR then*/
        case 21:

        /** 		c = Pop() -- increment*/
        _c_52207 = _37Pop();
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		TempKeep(c)*/
        _37TempKeep(_c_52207);

        /** 		ic = IsInteger(c)*/
        _ic_52215 = _37IsInteger(_c_52207);
        if (!IS_ATOM_INT(_ic_52215)) {
            _1 = (long)(DBL_PTR(_ic_52215)->dbl);
            if (UNIQUE(DBL_PTR(_ic_52215)) && (DBL_PTR(_ic_52215)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_ic_52215);
            _ic_52215 = _1;
        }

        /** 		if c < 1 or*/
        _27795 = (_c_52207 < 1);
        if (_27795 != 0) {
            goto L86; // [5571] 5650
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27797 = (int)*(((s1_ptr)_2)->base + _c_52207);
        _2 = (int)SEQ_PTR(_27797);
        _27798 = (int)*(((s1_ptr)_2)->base + 3);
        _27797 = NOVALUE;
        if (IS_ATOM_INT(_27798)) {
            _27799 = (_27798 == 1);
        }
        else {
            _27799 = binary_op(EQUALS, _27798, 1);
        }
        _27798 = NOVALUE;
        if (IS_ATOM_INT(_27799)) {
            if (_27799 == 0) {
                DeRef(_27800);
                _27800 = 0;
                goto L87; // [5593] 5619
            }
        }
        else {
            if (DBL_PTR(_27799)->dbl == 0.0) {
                DeRef(_27800);
                _27800 = 0;
                goto L87; // [5593] 5619
            }
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27801 = (int)*(((s1_ptr)_2)->base + _c_52207);
        _2 = (int)SEQ_PTR(_27801);
        _27802 = (int)*(((s1_ptr)_2)->base + 4);
        _27801 = NOVALUE;
        if (IS_ATOM_INT(_27802)) {
            _27803 = (_27802 != 2);
        }
        else {
            _27803 = binary_op(NOTEQ, _27802, 2);
        }
        _27802 = NOVALUE;
        DeRef(_27800);
        if (IS_ATOM_INT(_27803))
        _27800 = (_27803 != 0);
        else
        _27800 = DBL_PTR(_27803)->dbl != 0.0;
L87: 
        if (_27800 == 0) {
            DeRef(_27804);
            _27804 = 0;
            goto L88; // [5619] 5645
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27805 = (int)*(((s1_ptr)_2)->base + _c_52207);
        _2 = (int)SEQ_PTR(_27805);
        _27806 = (int)*(((s1_ptr)_2)->base + 4);
        _27805 = NOVALUE;
        if (IS_ATOM_INT(_27806)) {
            _27807 = (_27806 != 4);
        }
        else {
            _27807 = binary_op(NOTEQ, _27806, 4);
        }
        _27806 = NOVALUE;
        if (IS_ATOM_INT(_27807))
        _27804 = (_27807 != 0);
        else
        _27804 = DBL_PTR(_27807)->dbl != 0.0;
L88: 
        if (_27804 == 0)
        {
            _27804 = NOVALUE;
            goto L89; // [5646] 5687
        }
        else{
            _27804 = NOVALUE;
        }
L86: 

        /** 			emit_opcode(ASSIGN)*/
        _37emit_opcode(18);

        /** 			emit_addr(c)*/
        _37emit_addr(_c_52207);

        /** 			c = NewTempSym()*/
        _c_52207 = _52NewTempSym(0);
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 			if ic then*/
        if (_ic_52215 == 0)
        {
            goto L8A; // [5672] 5681
        }
        else{
        }

        /** 				TempInteger( c )*/
        _37TempInteger(_c_52207);
L8A: 

        /** 			emit_addr(c)*/
        _37emit_addr(_c_52207);
L89: 

        /** 		b = Pop() -- limit*/
        _b_52206 = _37Pop();
        if (!IS_ATOM_INT(_b_52206)) {
            _1 = (long)(DBL_PTR(_b_52206)->dbl);
            if (UNIQUE(DBL_PTR(_b_52206)) && (DBL_PTR(_b_52206)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52206);
            _b_52206 = _1;
        }

        /** 		TempKeep(b)*/
        _37TempKeep(_b_52206);

        /** 		ib = IsInteger(b)*/
        _ib_52214 = _37IsInteger(_b_52206);
        if (!IS_ATOM_INT(_ib_52214)) {
            _1 = (long)(DBL_PTR(_ib_52214)->dbl);
            if (UNIQUE(DBL_PTR(_ib_52214)) && (DBL_PTR(_ib_52214)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_ib_52214);
            _ib_52214 = _1;
        }

        /** 		if b < 1 or*/
        _27811 = (_b_52206 < 1);
        if (_27811 != 0) {
            goto L8B; // [5713] 5792
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27813 = (int)*(((s1_ptr)_2)->base + _b_52206);
        _2 = (int)SEQ_PTR(_27813);
        _27814 = (int)*(((s1_ptr)_2)->base + 3);
        _27813 = NOVALUE;
        if (IS_ATOM_INT(_27814)) {
            _27815 = (_27814 == 1);
        }
        else {
            _27815 = binary_op(EQUALS, _27814, 1);
        }
        _27814 = NOVALUE;
        if (IS_ATOM_INT(_27815)) {
            if (_27815 == 0) {
                DeRef(_27816);
                _27816 = 0;
                goto L8C; // [5735] 5761
            }
        }
        else {
            if (DBL_PTR(_27815)->dbl == 0.0) {
                DeRef(_27816);
                _27816 = 0;
                goto L8C; // [5735] 5761
            }
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27817 = (int)*(((s1_ptr)_2)->base + _b_52206);
        _2 = (int)SEQ_PTR(_27817);
        _27818 = (int)*(((s1_ptr)_2)->base + 4);
        _27817 = NOVALUE;
        if (IS_ATOM_INT(_27818)) {
            _27819 = (_27818 != 2);
        }
        else {
            _27819 = binary_op(NOTEQ, _27818, 2);
        }
        _27818 = NOVALUE;
        DeRef(_27816);
        if (IS_ATOM_INT(_27819))
        _27816 = (_27819 != 0);
        else
        _27816 = DBL_PTR(_27819)->dbl != 0.0;
L8C: 
        if (_27816 == 0) {
            DeRef(_27820);
            _27820 = 0;
            goto L8D; // [5761] 5787
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27821 = (int)*(((s1_ptr)_2)->base + _b_52206);
        _2 = (int)SEQ_PTR(_27821);
        _27822 = (int)*(((s1_ptr)_2)->base + 4);
        _27821 = NOVALUE;
        if (IS_ATOM_INT(_27822)) {
            _27823 = (_27822 != 4);
        }
        else {
            _27823 = binary_op(NOTEQ, _27822, 4);
        }
        _27822 = NOVALUE;
        if (IS_ATOM_INT(_27823))
        _27820 = (_27823 != 0);
        else
        _27820 = DBL_PTR(_27823)->dbl != 0.0;
L8D: 
        if (_27820 == 0)
        {
            _27820 = NOVALUE;
            goto L8E; // [5788] 5829
        }
        else{
            _27820 = NOVALUE;
        }
L8B: 

        /** 			emit_opcode(ASSIGN)*/
        _37emit_opcode(18);

        /** 			emit_addr(b)*/
        _37emit_addr(_b_52206);

        /** 			b = NewTempSym()*/
        _b_52206 = _52NewTempSym(0);
        if (!IS_ATOM_INT(_b_52206)) {
            _1 = (long)(DBL_PTR(_b_52206)->dbl);
            if (UNIQUE(DBL_PTR(_b_52206)) && (DBL_PTR(_b_52206)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52206);
            _b_52206 = _1;
        }

        /** 			if ib then*/
        if (_ib_52214 == 0)
        {
            goto L8F; // [5814] 5823
        }
        else{
        }

        /** 				TempInteger( b )*/
        _37TempInteger(_b_52206);
L8F: 

        /** 			emit_addr(b)*/
        _37emit_addr(_b_52206);
L8E: 

        /** 		a = Pop() -- initial value*/
        _a_52205 = _37Pop();
        if (!IS_ATOM_INT(_a_52205)) {
            _1 = (long)(DBL_PTR(_a_52205)->dbl);
            if (UNIQUE(DBL_PTR(_a_52205)) && (DBL_PTR(_a_52205)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52205);
            _a_52205 = _1;
        }

        /** 		if IsInteger(a) and ib and ic then*/
        _27826 = _37IsInteger(_a_52205);
        if (IS_ATOM_INT(_27826)) {
            if (_27826 == 0) {
                DeRef(_27827);
                _27827 = 0;
                goto L90; // [5842] 5850
            }
        }
        else {
            if (DBL_PTR(_27826)->dbl == 0.0) {
                DeRef(_27827);
                _27827 = 0;
                goto L90; // [5842] 5850
            }
        }
        DeRef(_27827);
        _27827 = (_ib_52214 != 0);
L90: 
        if (_27827 == 0) {
            goto L91; // [5850] 5889
        }
        if (_ic_52215 == 0)
        {
            goto L91; // [5855] 5889
        }
        else{
        }

        /** 			SymTab[op_info1][S_VTYPE] = integer_type*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_37op_info1_51268 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 15);
        _1 = *(int *)_2;
        *(int *)_2 = _52integer_type_47105;
        DeRef(_1);
        _27829 = NOVALUE;

        /** 			op = FOR_I*/
        _op_52203 = 125;
        goto L92; // [5886] 5899
L91: 

        /** 			op = FOR*/
        _op_52203 = 21;
L92: 

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		emit_addr(c)*/
        _37emit_addr(_c_52207);

        /** 		emit_addr(b)*/
        _37emit_addr(_b_52206);

        /** 		emit_addr(a)*/
        _37emit_addr(_a_52205);

        /** 		emit_addr(CurrentSub) -- in case recursion check is needed*/
        _37emit_addr(_25CurrentSub_12270);

        /** 		Push(b)*/
        _37Push(_b_52206);

        /** 		Push(c)*/
        _37Push(_c_52207);

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [5943] 7417

        /** 	case ENDFOR_GENERAL, ENDFOR_INT_UP1 then  -- all ENDFORs*/
        case 39:
        case 54:

        /** 		emit_opcode(op) -- will be patched at runtime*/
        _37emit_opcode(_op_52203);

        /** 		a = Pop()*/
        _a_52205 = _37Pop();
        if (!IS_ATOM_INT(_a_52205)) {
            _1 = (long)(DBL_PTR(_a_52205)->dbl);
            if (UNIQUE(DBL_PTR(_a_52205)) && (DBL_PTR(_a_52205)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52205);
            _a_52205 = _1;
        }

        /** 		emit_addr(op_info2) -- address of top of loop*/
        _37emit_addr(_37op_info2_51269);

        /** 		emit_addr(Pop())    -- limit*/
        _27832 = _37Pop();
        _37emit_addr(_27832);
        _27832 = NOVALUE;

        /** 		emit_addr(op_info1) -- loop var*/
        _37emit_addr(_37op_info1_51268);

        /** 		emit_addr(a)        -- increment - not always used -*/
        _37emit_addr(_a_52205);

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [5997] 7417

        /** 	case ASSIGN_OP_SUBS, PASSIGN_OP_SUBS then*/
        case 149:
        case 164:

        /** 		b = Pop()      -- rhs value, keep on stack*/
        _b_52206 = _37Pop();
        if (!IS_ATOM_INT(_b_52206)) {
            _1 = (long)(DBL_PTR(_b_52206)->dbl);
            if (UNIQUE(DBL_PTR(_b_52206)) && (DBL_PTR(_b_52206)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52206);
            _b_52206 = _1;
        }

        /** 		TempKeep(b)*/
        _37TempKeep(_b_52206);

        /** 		a = Pop()      -- subscript, keep on stack*/
        _a_52205 = _37Pop();
        if (!IS_ATOM_INT(_a_52205)) {
            _1 = (long)(DBL_PTR(_a_52205)->dbl);
            if (UNIQUE(DBL_PTR(_a_52205)) && (DBL_PTR(_a_52205)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52205);
            _a_52205 = _1;
        }

        /** 		TempKeep(a)*/
        _37TempKeep(_a_52205);

        /** 		c = Pop()      -- lhs sequence, keep on stack*/
        _c_52207 = _37Pop();
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		TempKeep(c)*/
        _37TempKeep(_c_52207);

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		emit_addr(c)*/
        _37emit_addr(_c_52207);

        /** 		emit_addr(a)*/
        _37emit_addr(_a_52205);

        /** 		d = NewTempSym()*/
        _d_52208 = _52NewTempSym(0);
        if (!IS_ATOM_INT(_d_52208)) {
            _1 = (long)(DBL_PTR(_d_52208)->dbl);
            if (UNIQUE(DBL_PTR(_d_52208)) && (DBL_PTR(_d_52208)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_d_52208);
            _d_52208 = _1;
        }

        /** 		emit_addr(d)   -- place to store result*/
        _37emit_addr(_d_52208);

        /** 		emit_temp( d, NEW_REFERENCE )*/
        _37emit_temp(_d_52208, 1);

        /** 		Push(c)*/
        _37Push(_c_52207);

        /** 		Push(a)*/
        _37Push(_a_52205);

        /** 		Push(d)*/
        _37Push(_d_52208);

        /** 		Push(b)*/
        _37Push(_b_52206);

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [6102] 7417

        /** 	case ASSIGN_SLICE, PASSIGN_SLICE then*/
        case 45:
        case 163:

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		b = Pop() -- rhs value*/
        _b_52206 = _37Pop();
        if (!IS_ATOM_INT(_b_52206)) {
            _1 = (long)(DBL_PTR(_b_52206)->dbl);
            if (UNIQUE(DBL_PTR(_b_52206)) && (DBL_PTR(_b_52206)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52206);
            _b_52206 = _1;
        }

        /** 		a = Pop() -- 2nd subs*/
        _a_52205 = _37Pop();
        if (!IS_ATOM_INT(_a_52205)) {
            _1 = (long)(DBL_PTR(_a_52205)->dbl);
            if (UNIQUE(DBL_PTR(_a_52205)) && (DBL_PTR(_a_52205)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52205);
            _a_52205 = _1;
        }

        /** 		c = Pop() -- 1st subs*/
        _c_52207 = _37Pop();
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		emit_addr(Pop()) -- sequence*/
        _27840 = _37Pop();
        _37emit_addr(_27840);
        _27840 = NOVALUE;

        /** 		emit_addr(c)*/
        _37emit_addr(_c_52207);

        /** 		emit_addr(a)*/
        _37emit_addr(_a_52205);

        /** 		emit_addr(b)*/
        _37emit_addr(_b_52206);

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [6166] 7417

        /** 	case REPLACE then*/
        case 201:

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		b = Pop()  -- source*/
        _b_52206 = _37Pop();
        if (!IS_ATOM_INT(_b_52206)) {
            _1 = (long)(DBL_PTR(_b_52206)->dbl);
            if (UNIQUE(DBL_PTR(_b_52206)) && (DBL_PTR(_b_52206)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52206);
            _b_52206 = _1;
        }

        /** 		a = Pop()  -- replacement*/
        _a_52205 = _37Pop();
        if (!IS_ATOM_INT(_a_52205)) {
            _1 = (long)(DBL_PTR(_a_52205)->dbl);
            if (UNIQUE(DBL_PTR(_a_52205)) && (DBL_PTR(_a_52205)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52205);
            _a_52205 = _1;
        }

        /** 		c = Pop()  -- start of replaced slice*/
        _c_52207 = _37Pop();
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		d = Pop()  -- end of replaced slice*/
        _d_52208 = _37Pop();
        if (!IS_ATOM_INT(_d_52208)) {
            _1 = (long)(DBL_PTR(_d_52208)->dbl);
            if (UNIQUE(DBL_PTR(_d_52208)) && (DBL_PTR(_d_52208)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_d_52208);
            _d_52208 = _1;
        }

        /** 		emit_addr(d)*/
        _37emit_addr(_d_52208);

        /** 		emit_addr(c)*/
        _37emit_addr(_c_52207);

        /** 		emit_addr(a)*/
        _37emit_addr(_a_52205);

        /** 		emit_addr(b)*/
        _37emit_addr(_b_52206);

        /** 		c = NewTempSym()*/
        _c_52207 = _52NewTempSym(0);
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		Push(c)*/
        _37Push(_c_52207);

        /** 		emit_addr(c)     -- place to store result*/
        _37emit_addr(_c_52207);

        /** 		emit_temp( c, NEW_REFERENCE )*/
        _37emit_temp(_c_52207, 1);

        /** 		assignable = TRUE*/
        _37assignable_51286 = _5TRUE_244;
        goto LC; // [6256] 7417

        /** 	case ASSIGN_OP_SLICE, PASSIGN_OP_SLICE then*/
        case 150:
        case 165:

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		b = Pop()        -- rhs value not used*/
        _b_52206 = _37Pop();
        if (!IS_ATOM_INT(_b_52206)) {
            _1 = (long)(DBL_PTR(_b_52206)->dbl);
            if (UNIQUE(DBL_PTR(_b_52206)) && (DBL_PTR(_b_52206)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52206);
            _b_52206 = _1;
        }

        /** 		TempKeep(b)*/
        _37TempKeep(_b_52206);

        /** 		a = Pop()        -- 2nd subs*/
        _a_52205 = _37Pop();
        if (!IS_ATOM_INT(_a_52205)) {
            _1 = (long)(DBL_PTR(_a_52205)->dbl);
            if (UNIQUE(DBL_PTR(_a_52205)) && (DBL_PTR(_a_52205)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52205);
            _a_52205 = _1;
        }

        /** 		TempKeep(a)*/
        _37TempKeep(_a_52205);

        /** 		c = Pop()        -- 1st subs*/
        _c_52207 = _37Pop();
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		TempKeep(c)*/
        _37TempKeep(_c_52207);

        /** 		d = Pop()*/
        _d_52208 = _37Pop();
        if (!IS_ATOM_INT(_d_52208)) {
            _1 = (long)(DBL_PTR(_d_52208)->dbl);
            if (UNIQUE(DBL_PTR(_d_52208)) && (DBL_PTR(_d_52208)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_d_52208);
            _d_52208 = _1;
        }

        /** 		TempKeep(d)      -- sequence*/
        _37TempKeep(_d_52208);

        /** 		emit_addr(d)*/
        _37emit_addr(_d_52208);

        /** 		Push(d)*/
        _37Push(_d_52208);

        /** 		emit_addr(c)*/
        _37emit_addr(_c_52207);

        /** 		Push(c)*/
        _37Push(_c_52207);

        /** 		emit_addr(a)*/
        _37emit_addr(_a_52205);

        /** 		Push(a)*/
        _37Push(_a_52205);

        /** 		c = NewTempSym()*/
        _c_52207 = _52NewTempSym(0);
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		Push(c)*/
        _37Push(_c_52207);

        /** 		emit_addr(c)     -- place to store result*/
        _37emit_addr(_c_52207);

        /** 		emit_temp( c, NEW_REFERENCE )*/
        _37emit_temp(_c_52207, 1);

        /** 		Push(b)*/
        _37Push(_b_52206);

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [6383] 7417

        /** 	case CALL_PROC then*/
        case 136:

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		b = Pop()*/
        _b_52206 = _37Pop();
        if (!IS_ATOM_INT(_b_52206)) {
            _1 = (long)(DBL_PTR(_b_52206)->dbl);
            if (UNIQUE(DBL_PTR(_b_52206)) && (DBL_PTR(_b_52206)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52206);
            _b_52206 = _1;
        }

        /** 		emit_addr(Pop())*/
        _27852 = _37Pop();
        _37emit_addr(_27852);
        _27852 = NOVALUE;

        /** 		emit_addr(b)*/
        _37emit_addr(_b_52206);

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [6421] 7417

        /** 	case CALL_FUNC then*/
        case 137:

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		b = Pop()*/
        _b_52206 = _37Pop();
        if (!IS_ATOM_INT(_b_52206)) {
            _1 = (long)(DBL_PTR(_b_52206)->dbl);
            if (UNIQUE(DBL_PTR(_b_52206)) && (DBL_PTR(_b_52206)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52206);
            _b_52206 = _1;
        }

        /** 		emit_addr(Pop())*/
        _27854 = _37Pop();
        _37emit_addr(_27854);
        _27854 = NOVALUE;

        /** 		emit_addr(b)*/
        _37emit_addr(_b_52206);

        /** 		assignable = TRUE*/
        _37assignable_51286 = _5TRUE_244;

        /** 		c = NewTempSym()*/
        _c_52207 = _52NewTempSym(0);
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		Push(c)*/
        _37Push(_c_52207);

        /** 		emit_addr(c)*/
        _37emit_addr(_c_52207);

        /** 		emit_temp( c, NEW_REFERENCE )*/
        _37emit_temp(_c_52207, 1);
        goto LC; // [6483] 7417

        /** 	case EXIT_BLOCK then*/
        case 206:

        /** 		emit_opcode( op )*/
        _37emit_opcode(_op_52203);

        /** 		emit_addr( Pop() )*/
        _27856 = _37Pop();
        _37emit_addr(_27856);
        _27856 = NOVALUE;

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [6509] 7417

        /** 	case RETURNP then*/
        case 29:

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		emit_addr(CurrentSub)*/
        _37emit_addr(_25CurrentSub_12270);

        /** 		emit_addr(top_block())*/
        _27857 = _66top_block(0);
        _37emit_addr(_27857);
        _27857 = NOVALUE;

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [6543] 7417

        /** 	case RETURNF then*/
        case 28:

        /** 		clear_temp( Top() )*/

        /** 	return cg_stack[cgi]*/
        DeRef(_Top_inlined_Top_at_6755_53459);
        _2 = (int)SEQ_PTR(_37cg_stack_51283);
        _Top_inlined_Top_at_6755_53459 = (int)*(((s1_ptr)_2)->base + _37cgi_51284);
        Ref(_Top_inlined_Top_at_6755_53459);
        Ref(_Top_inlined_Top_at_6755_53459);
        _37clear_temp(_Top_inlined_Top_at_6755_53459);

        /** 		flush_temps()*/
        RefDS(_22682);
        _37flush_temps(_22682);

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		emit_addr(CurrentSub)*/
        _37emit_addr(_25CurrentSub_12270);

        /** 		emit_addr(Least_block())*/
        _27858 = _66Least_block();
        _37emit_addr(_27858);
        _27858 = NOVALUE;

        /** 		emit_addr(Pop())*/
        _27859 = _37Pop();
        _37emit_addr(_27859);
        _27859 = NOVALUE;

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [6605] 7417

        /** 	case RETURNT then*/
        case 34:

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [6623] 7417

        /** 	case DATE, TIME, SPACE_USED, GET_KEY, TASK_LIST,*/
        case 69:
        case 70:
        case 75:
        case 79:
        case 172:
        case 100:
        case 183:

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		c = NewTempSym()*/
        _c_52207 = _52NewTempSym(0);
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		assignable = TRUE*/
        _37assignable_51286 = _5TRUE_244;

        /** 		if op = GET_KEY then  -- it's in op_result as integer*/
        if (_op_52203 != 79)
        goto L93; // [6665] 6677

        /** 			TempInteger(c)*/
        _37TempInteger(_c_52207);
        goto L94; // [6674] 6684
L93: 

        /** 			emit_temp( c, NEW_REFERENCE )*/
        _37emit_temp(_c_52207, 1);
L94: 

        /** 		Push(c)*/
        _37Push(_c_52207);

        /** 		emit_addr(c)*/
        _37emit_addr(_c_52207);
        goto LC; // [6694] 7417

        /** 	case CLOSE, ABORT, CALL, DELETE_OBJECT then*/
        case 86:
        case 126:
        case 129:
        case 205:

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		emit_addr(Pop())*/
        _27862 = _37Pop();
        _37emit_addr(_27862);
        _27862 = NOVALUE;

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [6726] 7417

        /** 	case POWER then*/
        case 72:

        /** 		b = Pop()*/
        _b_52206 = _37Pop();
        if (!IS_ATOM_INT(_b_52206)) {
            _1 = (long)(DBL_PTR(_b_52206)->dbl);
            if (UNIQUE(DBL_PTR(_b_52206)) && (DBL_PTR(_b_52206)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52206);
            _b_52206 = _1;
        }

        /** 		a = Pop()*/
        _a_52205 = _37Pop();
        if (!IS_ATOM_INT(_a_52205)) {
            _1 = (long)(DBL_PTR(_a_52205)->dbl);
            if (UNIQUE(DBL_PTR(_a_52205)) && (DBL_PTR(_a_52205)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52205);
            _a_52205 = _1;
        }

        /** 		if b > 0 and SymTab[b][S_MODE] = M_CONSTANT and equal(SymTab[b][S_OBJ], 2) then*/
        _27865 = (_b_52206 > 0);
        if (_27865 == 0) {
            _27866 = 0;
            goto L95; // [6752] 6778
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27867 = (int)*(((s1_ptr)_2)->base + _b_52206);
        _2 = (int)SEQ_PTR(_27867);
        _27868 = (int)*(((s1_ptr)_2)->base + 3);
        _27867 = NOVALUE;
        if (IS_ATOM_INT(_27868)) {
            _27869 = (_27868 == 2);
        }
        else {
            _27869 = binary_op(EQUALS, _27868, 2);
        }
        _27868 = NOVALUE;
        if (IS_ATOM_INT(_27869))
        _27866 = (_27869 != 0);
        else
        _27866 = DBL_PTR(_27869)->dbl != 0.0;
L95: 
        if (_27866 == 0) {
            goto L96; // [6778] 6835
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _27871 = (int)*(((s1_ptr)_2)->base + _b_52206);
        _2 = (int)SEQ_PTR(_27871);
        _27872 = (int)*(((s1_ptr)_2)->base + 1);
        _27871 = NOVALUE;
        if (_27872 == 2)
        _27873 = 1;
        else if (IS_ATOM_INT(_27872) && IS_ATOM_INT(2))
        _27873 = 0;
        else
        _27873 = (compare(_27872, 2) == 0);
        _27872 = NOVALUE;
        if (_27873 == 0)
        {
            _27873 = NOVALUE;
            goto L96; // [6799] 6835
        }
        else{
            _27873 = NOVALUE;
        }

        /** 			op = rw:MULTIPLY*/
        _op_52203 = 13;

        /** 			emit_opcode(op)*/
        _37emit_opcode(13);

        /** 			emit_addr(a)*/
        _37emit_addr(_a_52205);

        /** 			emit_addr(a)*/
        _37emit_addr(_a_52205);

        /** 			cont21d(op, a, b, FALSE)*/
        _37cont21d(13, _a_52205, _b_52206, _5FALSE_242);
        goto LC; // [6832] 7417
L96: 

        /** 			Push(a)*/
        _37Push(_a_52205);

        /** 			Push(b)*/
        _37Push(_b_52206);

        /** 			cont21ii(op, FALSE)*/
        _37cont21ii(_op_52203, _5FALSE_242);
        goto LC; // [6854] 7417

        /** 	case TYPE_CHECK then*/
        case 65:

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		c = Pop()*/
        _c_52207 = _37Pop();
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [6879] 7417

        /** 	case DOLLAR then*/
        case -22:

        /** 		if current_sequence[$] < 0 or SymTab[current_sequence[$]][S_SCOPE] = SC_UNDEFINED then*/
        if (IS_SEQUENCE(_37current_sequence_51276)){
                _27875 = SEQ_PTR(_37current_sequence_51276)->length;
        }
        else {
            _27875 = 1;
        }
        _2 = (int)SEQ_PTR(_37current_sequence_51276);
        _27876 = (int)*(((s1_ptr)_2)->base + _27875);
        if (IS_ATOM_INT(_27876)) {
            _27877 = (_27876 < 0);
        }
        else {
            _27877 = binary_op(LESS, _27876, 0);
        }
        _27876 = NOVALUE;
        if (IS_ATOM_INT(_27877)) {
            if (_27877 != 0) {
                goto L97; // [6900] 6936
            }
        }
        else {
            if (DBL_PTR(_27877)->dbl != 0.0) {
                goto L97; // [6900] 6936
            }
        }
        if (IS_SEQUENCE(_37current_sequence_51276)){
                _27879 = SEQ_PTR(_37current_sequence_51276)->length;
        }
        else {
            _27879 = 1;
        }
        _2 = (int)SEQ_PTR(_37current_sequence_51276);
        _27880 = (int)*(((s1_ptr)_2)->base + _27879);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!IS_ATOM_INT(_27880)){
            _27881 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_27880)->dbl));
        }
        else{
            _27881 = (int)*(((s1_ptr)_2)->base + _27880);
        }
        _2 = (int)SEQ_PTR(_27881);
        _27882 = (int)*(((s1_ptr)_2)->base + 4);
        _27881 = NOVALUE;
        if (IS_ATOM_INT(_27882)) {
            _27883 = (_27882 == 9);
        }
        else {
            _27883 = binary_op(EQUALS, _27882, 9);
        }
        _27882 = NOVALUE;
        if (_27883 == 0) {
            DeRef(_27883);
            _27883 = NOVALUE;
            goto L98; // [6932] 7006
        }
        else {
            if (!IS_ATOM_INT(_27883) && DBL_PTR(_27883)->dbl == 0.0){
                DeRef(_27883);
                _27883 = NOVALUE;
                goto L98; // [6932] 7006
            }
            DeRef(_27883);
            _27883 = NOVALUE;
        }
        DeRef(_27883);
        _27883 = NOVALUE;
L97: 

        /** 			if lhs_ptr and length(current_sequence) = 1 then*/
        if (_37lhs_ptr_51278 == 0) {
            goto L99; // [6940] 6969
        }
        if (IS_SEQUENCE(_37current_sequence_51276)){
                _27885 = SEQ_PTR(_37current_sequence_51276)->length;
        }
        else {
            _27885 = 1;
        }
        _27886 = (_27885 == 1);
        _27885 = NOVALUE;
        if (_27886 == 0)
        {
            DeRef(_27886);
            _27886 = NOVALUE;
            goto L99; // [6954] 6969
        }
        else{
            DeRef(_27886);
            _27886 = NOVALUE;
        }

        /** 				c = PLENGTH*/
        _c_52207 = 160;
        goto L9A; // [6966] 6979
L99: 

        /** 				c = LENGTH*/
        _c_52207 = 42;
L9A: 

        /** 			c = - new_forward_reference( VARIABLE, current_sequence[$], c )*/
        if (IS_SEQUENCE(_37current_sequence_51276)){
                _27887 = SEQ_PTR(_37current_sequence_51276)->length;
        }
        else {
            _27887 = 1;
        }
        _2 = (int)SEQ_PTR(_37current_sequence_51276);
        _27888 = (int)*(((s1_ptr)_2)->base + _27887);
        Ref(_27888);
        _27889 = _29new_forward_reference(-100, _27888, _c_52207);
        _27888 = NOVALUE;
        if (IS_ATOM_INT(_27889)) {
            if ((unsigned long)_27889 == 0xC0000000)
            _c_52207 = (int)NewDouble((double)-0xC0000000);
            else
            _c_52207 = - _27889;
        }
        else {
            _c_52207 = unary_op(UMINUS, _27889);
        }
        DeRef(_27889);
        _27889 = NOVALUE;
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }
        goto L9B; // [7003] 7020
L98: 

        /** 			c = current_sequence[$]*/
        if (IS_SEQUENCE(_37current_sequence_51276)){
                _27891 = SEQ_PTR(_37current_sequence_51276)->length;
        }
        else {
            _27891 = 1;
        }
        _2 = (int)SEQ_PTR(_37current_sequence_51276);
        _c_52207 = (int)*(((s1_ptr)_2)->base + _27891);
        if (!IS_ATOM_INT(_c_52207)){
            _c_52207 = (long)DBL_PTR(_c_52207)->dbl;
        }
L9B: 

        /** 		if lhs_ptr and length(current_sequence) = 1 then*/
        if (_37lhs_ptr_51278 == 0) {
            goto L9C; // [7024] 7051
        }
        if (IS_SEQUENCE(_37current_sequence_51276)){
                _27894 = SEQ_PTR(_37current_sequence_51276)->length;
        }
        else {
            _27894 = 1;
        }
        _27895 = (_27894 == 1);
        _27894 = NOVALUE;
        if (_27895 == 0)
        {
            DeRef(_27895);
            _27895 = NOVALUE;
            goto L9C; // [7038] 7051
        }
        else{
            DeRef(_27895);
            _27895 = NOVALUE;
        }

        /** 			emit_opcode(PLENGTH)*/
        _37emit_opcode(160);
        goto L9D; // [7048] 7059
L9C: 

        /** 			emit_opcode(LENGTH)*/
        _37emit_opcode(42);
L9D: 

        /** 		emit_addr( c )*/
        _37emit_addr(_c_52207);

        /** 		c = NewTempSym()*/
        _c_52207 = _52NewTempSym(0);
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		TempInteger(c)*/
        _37TempInteger(_c_52207);

        /** 		Push(c)*/
        _37Push(_c_52207);

        /** 		emit_addr(c)*/
        _37emit_addr(_c_52207);

        /** 		assignable = FALSE -- it wouldn't be assigned anyway*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [7094] 7417

        /** 	case TASK_SELF then*/
        case 170:

        /** 		c = NewTempSym()*/
        _c_52207 = _52NewTempSym(0);
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		Push(c)*/
        _37Push(_c_52207);

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		emit_addr(c)*/
        _37emit_addr(_c_52207);

        /** 		assignable = TRUE*/
        _37assignable_51286 = _5TRUE_244;
        goto LC; // [7130] 7417

        /** 	case SWITCH then*/
        case 185:

        /** 		emit_opcode( op )*/
        _37emit_opcode(_op_52203);

        /** 		c = Pop()*/
        _c_52207 = _37Pop();
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 		b = Pop()*/
        _b_52206 = _37Pop();
        if (!IS_ATOM_INT(_b_52206)) {
            _1 = (long)(DBL_PTR(_b_52206)->dbl);
            if (UNIQUE(DBL_PTR(_b_52206)) && (DBL_PTR(_b_52206)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52206);
            _b_52206 = _1;
        }

        /** 		a = Pop()*/
        _a_52205 = _37Pop();
        if (!IS_ATOM_INT(_a_52205)) {
            _1 = (long)(DBL_PTR(_a_52205)->dbl);
            if (UNIQUE(DBL_PTR(_a_52205)) && (DBL_PTR(_a_52205)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52205);
            _a_52205 = _1;
        }

        /** 		emit_addr( a ) -- Switch Expr*/
        _37emit_addr(_a_52205);

        /** 		emit_addr( b ) -- Case values*/
        _37emit_addr(_b_52206);

        /** 		emit_addr( c ) -- Jump table*/
        _37emit_addr(_c_52207);

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [7184] 7417

        /** 	case CASE then*/
        case 186:

        /** 		emit_opcode( op )*/
        _37emit_opcode(_op_52203);

        /** 		emit( cg_stack[cgi] )  -- the case index*/
        _2 = (int)SEQ_PTR(_37cg_stack_51283);
        _27901 = (int)*(((s1_ptr)_2)->base + _37cgi_51284);
        Ref(_27901);
        _37emit(_27901);
        _27901 = NOVALUE;

        /** 		cgi -= 1*/
        _37cgi_51284 = _37cgi_51284 - 1;
        goto LC; // [7216] 7417

        /** 	case PLATFORM then*/
        case 155:

        /** 		if BIND and shroud_only then*/
        if (_25BIND_11877 == 0) {
            goto L9E; // [7226] 7274
        }
        if (_25shroud_only_12260 == 0)
        {
            goto L9E; // [7233] 7274
        }
        else{
        }

        /** 			c = NewTempSym()*/
        _c_52207 = _52NewTempSym(0);
        if (!IS_ATOM_INT(_c_52207)) {
            _1 = (long)(DBL_PTR(_c_52207)->dbl);
            if (UNIQUE(DBL_PTR(_c_52207)) && (DBL_PTR(_c_52207)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52207);
            _c_52207 = _1;
        }

        /** 			TempInteger(c)*/
        _37TempInteger(_c_52207);

        /** 			Push(c)*/
        _37Push(_c_52207);

        /** 			emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 			emit_addr(c)*/
        _37emit_addr(_c_52207);

        /** 			assignable = TRUE*/
        _37assignable_51286 = _5TRUE_244;
        goto LC; // [7271] 7417
L9E: 

        /** 			n = host_platform()*/
        _n_52216 = _36host_platform();
        if (!IS_ATOM_INT(_n_52216)) {
            _1 = (long)(DBL_PTR(_n_52216)->dbl);
            if (UNIQUE(DBL_PTR(_n_52216)) && (DBL_PTR(_n_52216)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_n_52216);
            _n_52216 = _1;
        }

        /** 			Push(NewIntSym(n))*/
        _27906 = _52NewIntSym(_n_52216);
        _37Push(_27906);
        _27906 = NOVALUE;

        /** 			assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [7298] 7417

        /** 	case PROFILE, TASK_SUSPEND then*/
        case 151:
        case 171:

        /** 		a = Pop()*/
        _a_52205 = _37Pop();
        if (!IS_ATOM_INT(_a_52205)) {
            _1 = (long)(DBL_PTR(_a_52205)->dbl);
            if (UNIQUE(DBL_PTR(_a_52205)) && (DBL_PTR(_a_52205)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52205);
            _a_52205 = _1;
        }

        /** 		emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 		emit_addr(a)*/
        _37emit_addr(_a_52205);

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [7330] 7417

        /** 	case TRACE then*/
        case 64:

        /** 		a = Pop()*/
        _a_52205 = _37Pop();
        if (!IS_ATOM_INT(_a_52205)) {
            _1 = (long)(DBL_PTR(_a_52205)->dbl);
            if (UNIQUE(DBL_PTR(_a_52205)) && (DBL_PTR(_a_52205)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52205);
            _a_52205 = _1;
        }

        /** 		if OpTrace then*/
        if (_25OpTrace_12332 == 0)
        {
            goto L9F; // [7347] 7393
        }
        else{
        }

        /** 			emit_opcode(op)*/
        _37emit_opcode(_op_52203);

        /** 			emit_addr(a)*/
        _37emit_addr(_a_52205);

        /** 			if TRANSLATE then*/
        if (_25TRANSLATE_11874 == 0)
        {
            goto LA0; // [7364] 7392
        }
        else{
        }

        /** 				if not trace_called then*/
        if (_37trace_called_51271 != 0)
        goto LA1; // [7371] 7382

        /** 					Warning(217,0)*/
        RefDS(_22682);
        _43Warning(217, 0, _22682);
LA1: 

        /** 				trace_called = TRUE*/
        _37trace_called_51271 = _5TRUE_244;
LA0: 
L9F: 

        /** 		assignable = FALSE*/
        _37assignable_51286 = _5FALSE_242;
        goto LC; // [7400] 7417

        /** 	case else*/
        default:

        /** 		InternalErr(259, {op})*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _op_52203;
        _27910 = MAKE_SEQ(_1);
        _43InternalErr(259, _27910);
        _27910 = NOVALUE;
    ;}LC: 

    /** 	previous_op = op*/
    _25previous_op_12379 = _op_52203;

    /** 	inlined = 0*/
    _37inlined_52181 = 0;

    /** end procedure*/
    DeRef(_obj_52217);
    DeRef(_elements_52218);
    DeRef(_element_vals_52219);
    DeRef(_27348);
    _27348 = NOVALUE;
    DeRef(_27584);
    _27584 = NOVALUE;
    DeRef(_27629);
    _27629 = NOVALUE;
    DeRef(_27877);
    _27877 = NOVALUE;
    DeRef(_27440);
    _27440 = NOVALUE;
    DeRef(_27578);
    _27578 = NOVALUE;
    DeRef(_27346);
    _27346 = NOVALUE;
    DeRef(_27396);
    _27396 = NOVALUE;
    DeRef(_27608);
    _27608 = NOVALUE;
    DeRef(_27647);
    _27647 = NOVALUE;
    DeRef(_27661);
    _27661 = NOVALUE;
    DeRef(_27394);
    _27394 = NOVALUE;
    DeRef(_27509);
    _27509 = NOVALUE;
    DeRef(_27429);
    _27429 = NOVALUE;
    _27434 = NOVALUE;
    DeRef(_27529);
    _27529 = NOVALUE;
    DeRef(_27631);
    _27631 = NOVALUE;
    DeRef(_27736);
    _27736 = NOVALUE;
    DeRef(_27787);
    _27787 = NOVALUE;
    _27417 = NOVALUE;
    DeRef(_27531);
    _27531 = NOVALUE;
    DeRef(_27415);
    _27415 = NOVALUE;
    DeRef(_27461);
    _27461 = NOVALUE;
    DeRef(_27573);
    _27573 = NOVALUE;
    DeRef(_27606);
    _27606 = NOVALUE;
    DeRef(_27741);
    _27741 = NOVALUE;
    DeRef(_27582);
    _27582 = NOVALUE;
    DeRef(_27597);
    _27597 = NOVALUE;
    DeRef(_27669);
    _27669 = NOVALUE;
    DeRef(_27525);
    _27525 = NOVALUE;
    DeRef(_27547);
    _27547 = NOVALUE;
    DeRef(_27556);
    _27556 = NOVALUE;
    DeRef(_27419);
    _27419 = NOVALUE;
    _27432 = NOVALUE;
    DeRef(_27521);
    _27521 = NOVALUE;
    DeRef(_27754);
    _27754 = NOVALUE;
    DeRef(_27795);
    _27795 = NOVALUE;
    _27880 = NOVALUE;
    DeRef(_27657);
    _27657 = NOVALUE;
    DeRef(_27342);
    _27342 = NOVALUE;
    DeRef(_27454);
    _27454 = NOVALUE;
    _27463 = NOVALUE;
    DeRef(_27550);
    _27550 = NOVALUE;
    DeRef(_27619);
    _27619 = NOVALUE;
    DeRef(_27400);
    _27400 = NOVALUE;
    DeRef(_27340);
    _27340 = NOVALUE;
    DeRef(_27552);
    _27552 = NOVALUE;
    DeRef(_27602);
    _27602 = NOVALUE;
    DeRef(_27819);
    _27819 = NOVALUE;
    DeRef(_27344);
    _27344 = NOVALUE;
    DeRef(_27376);
    _27376 = NOVALUE;
    DeRef(_27448);
    _27448 = NOVALUE;
    DeRef(_27569);
    _27569 = NOVALUE;
    DeRef(_27869);
    _27869 = NOVALUE;
    DeRef(_27488);
    _27488 = NOVALUE;
    _27664 = NOVALUE;
    DeRef(_27699);
    _27699 = NOVALUE;
    DeRef(_27803);
    _27803 = NOVALUE;
    DeRef(_27815);
    _27815 = NOVALUE;
    _27458 = NOVALUE;
    DeRef(_27475);
    _27475 = NOVALUE;
    DeRef(_27623);
    _27623 = NOVALUE;
    DeRef(_27649);
    _27649 = NOVALUE;
    DeRef(_27826);
    _27826 = NOVALUE;
    DeRef(_27588);
    _27588 = NOVALUE;
    DeRef(_27717);
    _27717 = NOVALUE;
    DeRef(_27785);
    _27785 = NOVALUE;
    DeRef(_27807);
    _27807 = NOVALUE;
    DeRef(_27758);
    _27758 = NOVALUE;
    DeRef(_27515);
    _27515 = NOVALUE;
    DeRef(_27748);
    _27748 = NOVALUE;
    DeRef(_27823);
    _27823 = NOVALUE;
    DeRef(_27381);
    _27381 = NOVALUE;
    DeRef(_27653);
    _27653 = NOVALUE;
    DeRef(_27391);
    _27391 = NOVALUE;
    DeRef(_27543);
    _27543 = NOVALUE;
    DeRef(_27384);
    _27384 = NOVALUE;
    DeRef(_27595);
    _27595 = NOVALUE;
    DeRef(_27799);
    _27799 = NOVALUE;
    DeRef(_27865);
    _27865 = NOVALUE;
    _27666 = NOVALUE;
    DeRef(_27722);
    _27722 = NOVALUE;
    DeRef(_27771);
    _27771 = NOVALUE;
    _27364 = NOVALUE;
    DeRef(_27626);
    _27626 = NOVALUE;
    DeRef(_27729);
    _27729 = NOVALUE;
    DeRef(_27811);
    _27811 = NOVALUE;
    return;
    ;
}


void _37emit_assign_op(int _op_53610)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_op_53610)) {
        _1 = (long)(DBL_PTR(_op_53610)->dbl);
        if (UNIQUE(DBL_PTR(_op_53610)) && (DBL_PTR(_op_53610)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_op_53610);
        _op_53610 = _1;
    }

    /** 	if op = PLUS_EQUALS then*/
    if (_op_53610 != 515)
    goto L1; // [7] 21

    /** 		emit_op(PLUS)*/
    _37emit_op(11);
    goto L2; // [18] 86
L1: 

    /** 	elsif op = MINUS_EQUALS then*/
    if (_op_53610 != 516)
    goto L3; // [25] 39

    /** 		emit_op(MINUS)*/
    _37emit_op(10);
    goto L2; // [36] 86
L3: 

    /** 	elsif op = MULTIPLY_EQUALS then*/
    if (_op_53610 != 517)
    goto L4; // [43] 55

    /** 		emit_op(rw:MULTIPLY)*/
    _37emit_op(13);
    goto L2; // [52] 86
L4: 

    /** 	elsif op = DIVIDE_EQUALS then*/
    if (_op_53610 != 518)
    goto L5; // [59] 71

    /** 		emit_op(rw:DIVIDE)*/
    _37emit_op(14);
    goto L2; // [68] 86
L5: 

    /** 	elsif op = CONCAT_EQUALS then*/
    if (_op_53610 != 519)
    goto L6; // [75] 85

    /** 		emit_op(rw:CONCAT)*/
    _37emit_op(15);
L6: 
L2: 

    /** end procedure*/
    return;
    ;
}


void _37StartSourceLine(int _sl_53630, int _dup_ok_53631, int _emit_coverage_53632)
{
    int _line_span_53635 = NOVALUE;
    int _27932 = NOVALUE;
    int _27930 = NOVALUE;
    int _27929 = NOVALUE;
    int _27928 = NOVALUE;
    int _27927 = NOVALUE;
    int _27926 = NOVALUE;
    int _27924 = NOVALUE;
    int _27921 = NOVALUE;
    int _27919 = NOVALUE;
    int _27918 = NOVALUE;
    int _27917 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sl_53630)) {
        _1 = (long)(DBL_PTR(_sl_53630)->dbl);
        if (UNIQUE(DBL_PTR(_sl_53630)) && (DBL_PTR(_sl_53630)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sl_53630);
        _sl_53630 = _1;
    }
    if (!IS_ATOM_INT(_dup_ok_53631)) {
        _1 = (long)(DBL_PTR(_dup_ok_53631)->dbl);
        if (UNIQUE(DBL_PTR(_dup_ok_53631)) && (DBL_PTR(_dup_ok_53631)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_dup_ok_53631);
        _dup_ok_53631 = _1;
    }
    if (!IS_ATOM_INT(_emit_coverage_53632)) {
        _1 = (long)(DBL_PTR(_emit_coverage_53632)->dbl);
        if (UNIQUE(DBL_PTR(_emit_coverage_53632)) && (DBL_PTR(_emit_coverage_53632)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_emit_coverage_53632);
        _emit_coverage_53632 = _1;
    }

    /** 	if gline_number = LastLineNumber then*/
    if (_25gline_number_12267 != _60LastLineNumber_24674)
    goto L1; // [13] 66

    /** 		if length(LineTable) then*/
    if (IS_SEQUENCE(_25LineTable_12356)){
            _27917 = SEQ_PTR(_25LineTable_12356)->length;
    }
    else {
        _27917 = 1;
    }
    if (_27917 == 0)
    {
        _27917 = NOVALUE;
        goto L2; // [24] 55
    }
    else{
        _27917 = NOVALUE;
    }

    /** 			if dup_ok then*/
    if (_dup_ok_53631 == 0)
    {
        goto L3; // [29] 47
    }
    else{
    }

    /** 				emit_op( STARTLINE )*/
    _37emit_op(58);

    /** 				emit_addr( gline_number )*/
    _37emit_addr(_25gline_number_12267);
L3: 

    /** 			return -- ignore duplicates*/
    return;
    goto L4; // [52] 65
L2: 

    /** 			sl = FALSE -- top-level new statement to execute on same line*/
    _sl_53630 = _5FALSE_242;
L4: 
L1: 

    /** 	LastLineNumber = gline_number*/
    _60LastLineNumber_24674 = _25gline_number_12267;

    /** 	line_span = gline_number - SymTab[CurrentSub][S_FIRSTLINE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _27918 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_27918);
    if (!IS_ATOM_INT(_25S_FIRSTLINE_11953)){
        _27919 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FIRSTLINE_11953)->dbl));
    }
    else{
        _27919 = (int)*(((s1_ptr)_2)->base + _25S_FIRSTLINE_11953);
    }
    _27918 = NOVALUE;
    if (IS_ATOM_INT(_27919)) {
        _line_span_53635 = _25gline_number_12267 - _27919;
    }
    else {
        _line_span_53635 = binary_op(MINUS, _25gline_number_12267, _27919);
    }
    _27919 = NOVALUE;
    if (!IS_ATOM_INT(_line_span_53635)) {
        _1 = (long)(DBL_PTR(_line_span_53635)->dbl);
        if (UNIQUE(DBL_PTR(_line_span_53635)) && (DBL_PTR(_line_span_53635)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_line_span_53635);
        _line_span_53635 = _1;
    }

    /** 	while length(LineTable) < line_span do*/
L5: 
    if (IS_SEQUENCE(_25LineTable_12356)){
            _27921 = SEQ_PTR(_25LineTable_12356)->length;
    }
    else {
        _27921 = 1;
    }
    if (_27921 >= _line_span_53635)
    goto L6; // [109] 128

    /** 		LineTable = append(LineTable, -1) -- filler*/
    Append(&_25LineTable_12356, _25LineTable_12356, -1);

    /** 	end while*/
    goto L5; // [125] 104
L6: 

    /** 	LineTable = append(LineTable, length(Code))*/
    if (IS_SEQUENCE(_25Code_12355)){
            _27924 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _27924 = 1;
    }
    Append(&_25LineTable_12356, _25LineTable_12356, _27924);
    _27924 = NOVALUE;

    /** 	if sl and (TRANSLATE or (OpTrace or OpProfileStatement)) then*/
    if (_sl_53630 == 0) {
        goto L7; // [145] 190
    }
    if (_25TRANSLATE_11874 != 0) {
        DeRef(_27927);
        _27927 = 1;
        goto L8; // [151] 171
    }
    if (_25OpTrace_12332 != 0) {
        _27928 = 1;
        goto L9; // [157] 167
    }
    _27928 = (_25OpProfileStatement_12334 != 0);
L9: 
    DeRef(_27927);
    _27927 = (_27928 != 0);
L8: 
    if (_27927 == 0)
    {
        _27927 = NOVALUE;
        goto L7; // [172] 190
    }
    else{
        _27927 = NOVALUE;
    }

    /** 		emit_op(STARTLINE)*/
    _37emit_op(58);

    /** 		emit_addr(gline_number)*/
    _37emit_addr(_25gline_number_12267);
L7: 

    /** 	if (sl and emit_coverage = COVERAGE_INCLUDE) or emit_coverage = COVERAGE_OVERRIDE then*/
    if (_sl_53630 == 0) {
        _27929 = 0;
        goto LA; // [192] 206
    }
    _27930 = (_emit_coverage_53632 == 2);
    _27929 = (_27930 != 0);
LA: 
    if (_27929 != 0) {
        goto LB; // [206] 221
    }
    _27932 = (_emit_coverage_53632 == 3);
    if (_27932 == 0)
    {
        DeRef(_27932);
        _27932 = NOVALUE;
        goto LC; // [217] 229
    }
    else{
        DeRef(_27932);
        _27932 = NOVALUE;
    }
LB: 

    /** 		include_line( gline_number )*/
    _49include_line(_25gline_number_12267);
LC: 

    /** end procedure*/
    DeRef(_27930);
    _27930 = NOVALUE;
    return;
    ;
}


int _37has_forward_params(int _sym_53689)
{
    int _27938 = NOVALUE;
    int _27937 = NOVALUE;
    int _27936 = NOVALUE;
    int _27935 = NOVALUE;
    int _27934 = NOVALUE;
    int _27933 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_53689)) {
        _1 = (long)(DBL_PTR(_sym_53689)->dbl);
        if (UNIQUE(DBL_PTR(_sym_53689)) && (DBL_PTR(_sym_53689)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_53689);
        _sym_53689 = _1;
    }

    /** 	for i = cgi - (SymTab[sym][S_NUM_ARGS]-1) to cgi do*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _27933 = (int)*(((s1_ptr)_2)->base + _sym_53689);
    _2 = (int)SEQ_PTR(_27933);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _27934 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _27934 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    _27933 = NOVALUE;
    if (IS_ATOM_INT(_27934)) {
        _27935 = _27934 - 1;
        if ((long)((unsigned long)_27935 +(unsigned long) HIGH_BITS) >= 0){
            _27935 = NewDouble((double)_27935);
        }
    }
    else {
        _27935 = binary_op(MINUS, _27934, 1);
    }
    _27934 = NOVALUE;
    if (IS_ATOM_INT(_27935)) {
        _27936 = _37cgi_51284 - _27935;
        if ((long)((unsigned long)_27936 +(unsigned long) HIGH_BITS) >= 0){
            _27936 = NewDouble((double)_27936);
        }
    }
    else {
        _27936 = binary_op(MINUS, _37cgi_51284, _27935);
    }
    DeRef(_27935);
    _27935 = NOVALUE;
    _27937 = _37cgi_51284;
    {
        int _i_53691;
        Ref(_27936);
        _i_53691 = _27936;
L1: 
        if (binary_op_a(GREATER, _i_53691, _27937)){
            goto L2; // [32] 65
        }

        /** 		if cg_stack[i] < 0 then*/
        _2 = (int)SEQ_PTR(_37cg_stack_51283);
        if (!IS_ATOM_INT(_i_53691)){
            _27938 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_53691)->dbl));
        }
        else{
            _27938 = (int)*(((s1_ptr)_2)->base + _i_53691);
        }
        if (binary_op_a(GREATEREQ, _27938, 0)){
            _27938 = NOVALUE;
            goto L3; // [47] 58
        }
        _27938 = NOVALUE;

        /** 			return 1*/
        DeRef(_i_53691);
        DeRef(_27936);
        _27936 = NOVALUE;
        return 1;
L3: 

        /** 	end for*/
        _0 = _i_53691;
        if (IS_ATOM_INT(_i_53691)) {
            _i_53691 = _i_53691 + 1;
            if ((long)((unsigned long)_i_53691 +(unsigned long) HIGH_BITS) >= 0){
                _i_53691 = NewDouble((double)_i_53691);
            }
        }
        else {
            _i_53691 = binary_op_a(PLUS, _i_53691, 1);
        }
        DeRef(_0);
        goto L1; // [60] 39
L2: 
        ;
        DeRef(_i_53691);
    }

    /** 	return 0*/
    DeRef(_27936);
    _27936 = NOVALUE;
    return 0;
    ;
}



// 0xE16138FB
