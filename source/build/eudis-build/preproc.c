// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _64is_file_newer(int _f1_24466, int _f2_24467)
{
    int _d1_24468 = NOVALUE;
    int _d2_24471 = NOVALUE;
    int _diff_2__tmp_at33_24480 = NOVALUE;
    int _diff_1__tmp_at33_24479 = NOVALUE;
    int _diff_inlined_diff_at_33_24478 = NOVALUE;
    int _14219 = NOVALUE;
    int _0, _1, _2;
    

    /** 	object d1 = file_timestamp(f1)*/
    RefDS(_f1_24466);
    _0 = _d1_24468;
    _d1_24468 = _9file_timestamp(_f1_24466);
    DeRef(_0);

    /** 	object d2 = file_timestamp(f2)*/
    RefDS(_f2_24467);
    _0 = _d2_24471;
    _d2_24471 = _9file_timestamp(_f2_24467);
    DeRef(_0);

    /** 	if atom(d2) then return 1 end if*/
    _14219 = IS_ATOM(_d2_24471);
    if (_14219 == 0)
    {
        _14219 = NOVALUE;
        goto L1; // [22] 30
    }
    else{
        _14219 = NOVALUE;
    }
    DeRefDS(_f1_24466);
    DeRefDS(_f2_24467);
    DeRef(_d1_24468);
    DeRef(_d2_24471);
    return 1;
L1: 

    /** 	if dt:diff(d1, d2) < 0 then*/

    /** 	return datetimeToSeconds(dt2) - datetimeToSeconds(dt1)*/
    Ref(_d2_24471);
    _0 = _diff_1__tmp_at33_24479;
    _diff_1__tmp_at33_24479 = _10datetimeToSeconds(_d2_24471);
    DeRef(_0);
    Ref(_d1_24468);
    _0 = _diff_2__tmp_at33_24480;
    _diff_2__tmp_at33_24480 = _10datetimeToSeconds(_d1_24468);
    DeRef(_0);
    DeRef(_diff_inlined_diff_at_33_24478);
    if (IS_ATOM_INT(_diff_1__tmp_at33_24479) && IS_ATOM_INT(_diff_2__tmp_at33_24480)) {
        _diff_inlined_diff_at_33_24478 = _diff_1__tmp_at33_24479 - _diff_2__tmp_at33_24480;
        if ((long)((unsigned long)_diff_inlined_diff_at_33_24478 +(unsigned long) HIGH_BITS) >= 0){
            _diff_inlined_diff_at_33_24478 = NewDouble((double)_diff_inlined_diff_at_33_24478);
        }
    }
    else {
        _diff_inlined_diff_at_33_24478 = binary_op(MINUS, _diff_1__tmp_at33_24479, _diff_2__tmp_at33_24480);
    }
    DeRef(_diff_1__tmp_at33_24479);
    _diff_1__tmp_at33_24479 = NOVALUE;
    DeRef(_diff_2__tmp_at33_24480);
    _diff_2__tmp_at33_24480 = NOVALUE;
    if (binary_op_a(GREATEREQ, _diff_inlined_diff_at_33_24478, 0)){
        goto L2; // [49] 60
    }

    /** 		return 1*/
    DeRefDS(_f1_24466);
    DeRefDS(_f2_24467);
    DeRef(_d1_24468);
    DeRef(_d2_24471);
    return 1;
L2: 

    /** 	return 0*/
    DeRefDS(_f1_24466);
    DeRefDS(_f2_24467);
    DeRef(_d1_24468);
    DeRef(_d2_24471);
    return 0;
    ;
}


void _64add_preprocessor(int _file_ext_24484, int _command_24485, int _params_24486)
{
    int _tmp_24489 = NOVALUE;
    int _file_exts_24499 = NOVALUE;
    int _exts_24505 = NOVALUE;
    int _14236 = NOVALUE;
    int _14235 = NOVALUE;
    int _14234 = NOVALUE;
    int _14233 = NOVALUE;
    int _14231 = NOVALUE;
    int _14226 = NOVALUE;
    int _14221 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(command) then*/
    _14221 = 1;
    if (_14221 == 0)
    {
        _14221 = NOVALUE;
        goto L1; // [8] 53
    }
    else{
        _14221 = NOVALUE;
    }

    /** 		sequence tmp = split( file_ext, ":")*/
    RefDS(_file_ext_24484);
    RefDS(_14222);
    _0 = _tmp_24489;
    _tmp_24489 = _21split(_file_ext_24484, _14222, 0, 0);
    DeRef(_0);

    /** 		file_ext = tmp[1]*/
    DeRefDS(_file_ext_24484);
    _2 = (int)SEQ_PTR(_tmp_24489);
    _file_ext_24484 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_file_ext_24484);

    /** 		command = tmp[2]*/
    _2 = (int)SEQ_PTR(_tmp_24489);
    _command_24485 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_command_24485);

    /** 		if length(tmp) >= 3 then*/
    if (IS_SEQUENCE(_tmp_24489)){
            _14226 = SEQ_PTR(_tmp_24489)->length;
    }
    else {
        _14226 = 1;
    }
    if (_14226 < 3)
    goto L2; // [41] 52

    /** 			params = tmp[3]*/
    _2 = (int)SEQ_PTR(_tmp_24489);
    _params_24486 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_params_24486);
L2: 
L1: 
    DeRef(_tmp_24489);
    _tmp_24489 = NOVALUE;

    /** 	sequence file_exts = split( file_ext, "," )*/
    RefDS(_file_ext_24484);
    RefDS(_14229);
    _0 = _file_exts_24499;
    _file_exts_24499 = _21split(_file_ext_24484, _14229, 0, 0);
    DeRef(_0);

    /** 	if atom(params) then*/
    _14231 = IS_ATOM(_params_24486);
    if (_14231 == 0)
    {
        _14231 = NOVALUE;
        goto L3; // [71] 80
    }
    else{
        _14231 = NOVALUE;
    }

    /** 		params = ""*/
    RefDS(_5);
    DeRef(_params_24486);
    _params_24486 = _5;
L3: 

    /** 	sequence exts = split(file_ext, ",")*/
    RefDS(_file_ext_24484);
    RefDS(_14229);
    _0 = _exts_24505;
    _exts_24505 = _21split(_file_ext_24484, _14229, 0, 0);
    DeRef(_0);

    /** 	for i = 1 to length(exts) do*/
    if (IS_SEQUENCE(_exts_24505)){
            _14233 = SEQ_PTR(_exts_24505)->length;
    }
    else {
        _14233 = 1;
    }
    {
        int _i_24509;
        _i_24509 = 1;
L4: 
        if (_i_24509 > _14233){
            goto L5; // [96] 135
        }

        /** 		preprocessors &= { { exts[i], command, params, -1 } }*/
        _2 = (int)SEQ_PTR(_exts_24505);
        _14234 = (int)*(((s1_ptr)_2)->base + _i_24509);
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_14234);
        *((int *)(_2+4)) = _14234;
        Ref(_command_24485);
        *((int *)(_2+8)) = _command_24485;
        Ref(_params_24486);
        *((int *)(_2+12)) = _params_24486;
        *((int *)(_2+16)) = -1;
        _14235 = MAKE_SEQ(_1);
        _14234 = NOVALUE;
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _14235;
        _14236 = MAKE_SEQ(_1);
        _14235 = NOVALUE;
        Concat((object_ptr)&_26preprocessors_11157, _26preprocessors_11157, _14236);
        DeRefDS(_14236);
        _14236 = NOVALUE;

        /** 	end for*/
        _i_24509 = _i_24509 + 1;
        goto L4; // [130] 103
L5: 
        ;
    }

    /** end procedure */
    DeRefDS(_file_ext_24484);
    DeRef(_command_24485);
    DeRef(_params_24486);
    DeRef(_file_exts_24499);
    DeRef(_exts_24505);
    return;
    ;
}


int _64maybe_preprocess(int _fname_24518)
{
    int _pp_24519 = NOVALUE;
    int _pp_id_24520 = NOVALUE;
    int _fext_24524 = NOVALUE;
    int _post_fname_24541 = NOVALUE;
    int _rid_24569 = NOVALUE;
    int _dll_id_24573 = NOVALUE;
    int _public_cmd_args_24609 = NOVALUE;
    int _cmd_args_24612 = NOVALUE;
    int _cmd_24641 = NOVALUE;
    int _pcmd_24646 = NOVALUE;
    int _result_24651 = NOVALUE;
    int _14315 = NOVALUE;
    int _14314 = NOVALUE;
    int _14309 = NOVALUE;
    int _14308 = NOVALUE;
    int _14306 = NOVALUE;
    int _14305 = NOVALUE;
    int _14303 = NOVALUE;
    int _14301 = NOVALUE;
    int _14300 = NOVALUE;
    int _14298 = NOVALUE;
    int _14295 = NOVALUE;
    int _14293 = NOVALUE;
    int _14291 = NOVALUE;
    int _14289 = NOVALUE;
    int _14288 = NOVALUE;
    int _14286 = NOVALUE;
    int _14285 = NOVALUE;
    int _14283 = NOVALUE;
    int _14280 = NOVALUE;
    int _14279 = NOVALUE;
    int _14278 = NOVALUE;
    int _14276 = NOVALUE;
    int _14272 = NOVALUE;
    int _14270 = NOVALUE;
    int _14269 = NOVALUE;
    int _14268 = NOVALUE;
    int _14264 = NOVALUE;
    int _14261 = NOVALUE;
    int _14260 = NOVALUE;
    int _14259 = NOVALUE;
    int _14257 = NOVALUE;
    int _14254 = NOVALUE;
    int _14252 = NOVALUE;
    int _14251 = NOVALUE;
    int _14249 = NOVALUE;
    int _14247 = NOVALUE;
    int _14245 = NOVALUE;
    int _14243 = NOVALUE;
    int _14242 = NOVALUE;
    int _14241 = NOVALUE;
    int _14240 = NOVALUE;
    int _14238 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	sequence pp = {}*/
    RefDS(_5);
    DeRef(_pp_24519);
    _pp_24519 = _5;

    /** 	if length(preprocessors) then*/
    if (IS_SEQUENCE(_26preprocessors_11157)){
            _14238 = SEQ_PTR(_26preprocessors_11157)->length;
    }
    else {
        _14238 = 1;
    }
    if (_14238 == 0)
    {
        _14238 = NOVALUE;
        goto L1; // [17] 89
    }
    else{
        _14238 = NOVALUE;
    }

    /** 		sequence fext = fileext(fname)*/
    RefDS(_fname_24518);
    _0 = _fext_24524;
    _fext_24524 = _9fileext(_fname_24518);
    DeRef(_0);

    /** 		for i = 1 to length(preprocessors) do*/
    if (IS_SEQUENCE(_26preprocessors_11157)){
            _14240 = SEQ_PTR(_26preprocessors_11157)->length;
    }
    else {
        _14240 = 1;
    }
    {
        int _i_24528;
        _i_24528 = 1;
L2: 
        if (_i_24528 > _14240){
            goto L3; // [35] 88
        }

        /** 			if equal(fext, preprocessors[i][1]) then*/
        _2 = (int)SEQ_PTR(_26preprocessors_11157);
        _14241 = (int)*(((s1_ptr)_2)->base + _i_24528);
        _2 = (int)SEQ_PTR(_14241);
        _14242 = (int)*(((s1_ptr)_2)->base + 1);
        _14241 = NOVALUE;
        if (_fext_24524 == _14242)
        _14243 = 1;
        else if (IS_ATOM_INT(_fext_24524) && IS_ATOM_INT(_14242))
        _14243 = 0;
        else
        _14243 = (compare(_fext_24524, _14242) == 0);
        _14242 = NOVALUE;
        if (_14243 == 0)
        {
            _14243 = NOVALUE;
            goto L4; // [58] 81
        }
        else{
            _14243 = NOVALUE;
        }

        /** 				pp_id = i*/
        _pp_id_24520 = _i_24528;

        /** 				pp = preprocessors[pp_id]*/
        DeRef(_pp_24519);
        _2 = (int)SEQ_PTR(_26preprocessors_11157);
        _pp_24519 = (int)*(((s1_ptr)_2)->base + _pp_id_24520);
        RefDS(_pp_24519);

        /** 				exit*/
        goto L3; // [78] 88
L4: 

        /** 		end for*/
        _i_24528 = _i_24528 + 1;
        goto L2; // [83] 42
L3: 
        ;
    }
L1: 
    DeRef(_fext_24524);
    _fext_24524 = NOVALUE;

    /** 	if length(pp) = 0 then */
    if (IS_SEQUENCE(_pp_24519)){
            _14245 = SEQ_PTR(_pp_24519)->length;
    }
    else {
        _14245 = 1;
    }
    if (_14245 != 0)
    goto L5; // [96] 107

    /** 		return fname*/
    DeRefDS(_pp_24519);
    DeRef(_post_fname_24541);
    return _fname_24518;
L5: 

    /** 	sequence post_fname = filebase(fname) & ".pp." & fileext(fname)*/
    RefDS(_fname_24518);
    _14247 = _9filebase(_fname_24518);
    RefDS(_fname_24518);
    _14249 = _9fileext(_fname_24518);
    {
        int concat_list[3];

        concat_list[0] = _14249;
        concat_list[1] = _14248;
        concat_list[2] = _14247;
        Concat_N((object_ptr)&_post_fname_24541, concat_list, 3);
    }
    DeRef(_14249);
    _14249 = NOVALUE;
    DeRef(_14247);
    _14247 = NOVALUE;

    /** 	if length(dirname(fname)) > 0 then*/
    RefDS(_fname_24518);
    _14251 = _9dirname(_fname_24518, 0);
    if (IS_SEQUENCE(_14251)){
            _14252 = SEQ_PTR(_14251)->length;
    }
    else {
        _14252 = 1;
    }
    DeRef(_14251);
    _14251 = NOVALUE;
    if (_14252 <= 0)
    goto L6; // [133] 153

    /** 		post_fname = dirname(fname) & SLASH & post_fname*/
    RefDS(_fname_24518);
    _14254 = _9dirname(_fname_24518, 0);
    {
        int concat_list[3];

        concat_list[0] = _post_fname_24541;
        concat_list[1] = 92;
        concat_list[2] = _14254;
        Concat_N((object_ptr)&_post_fname_24541, concat_list, 3);
    }
    DeRef(_14254);
    _14254 = NOVALUE;
L6: 

    /** 	if not force_preprocessor then*/
    if (_26force_preprocessor_11158 != 0)
    goto L7; // [157] 178

    /** 		if not is_file_newer(fname, post_fname) then*/
    RefDS(_fname_24518);
    RefDS(_post_fname_24541);
    _14257 = _64is_file_newer(_fname_24518, _post_fname_24541);
    if (IS_ATOM_INT(_14257)) {
        if (_14257 != 0){
            DeRef(_14257);
            _14257 = NOVALUE;
            goto L8; // [167] 177
        }
    }
    else {
        if (DBL_PTR(_14257)->dbl != 0.0){
            DeRef(_14257);
            _14257 = NOVALUE;
            goto L8; // [167] 177
        }
    }
    DeRef(_14257);
    _14257 = NOVALUE;

    /** 			return post_fname*/
    DeRefDS(_fname_24518);
    DeRef(_pp_24519);
    _14251 = NOVALUE;
    return _post_fname_24541;
L8: 
L7: 

    /** 	if equal(fileext(pp[PP_COMMAND]), SHARED_LIB_EXT) then*/
    _2 = (int)SEQ_PTR(_pp_24519);
    _14259 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_14259);
    _14260 = _9fileext(_14259);
    _14259 = NOVALUE;
    if (_14260 == _9SHARED_LIB_EXT_6955)
    _14261 = 1;
    else if (IS_ATOM_INT(_14260) && IS_ATOM_INT(_9SHARED_LIB_EXT_6955))
    _14261 = 0;
    else
    _14261 = (compare(_14260, _9SHARED_LIB_EXT_6955) == 0);
    DeRef(_14260);
    _14260 = NOVALUE;
    if (_14261 == 0)
    {
        _14261 = NOVALUE;
        goto L9; // [196] 360
    }
    else{
        _14261 = NOVALUE;
    }

    /** 		integer rid = pp[PP_RID]*/
    _2 = (int)SEQ_PTR(_pp_24519);
    _rid_24569 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_rid_24569))
    _rid_24569 = (long)DBL_PTR(_rid_24569)->dbl;

    /** 		if rid = -1 then*/
    if (_rid_24569 != -1)
    goto LA; // [209] 317

    /** 			integer dll_id = open_dll(pp[PP_COMMAND])*/
    _2 = (int)SEQ_PTR(_pp_24519);
    _14264 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_14264);
    _dll_id_24573 = _11open_dll(_14264);
    _14264 = NOVALUE;
    if (!IS_ATOM_INT(_dll_id_24573)) {
        _1 = (long)(DBL_PTR(_dll_id_24573)->dbl);
        if (UNIQUE(DBL_PTR(_dll_id_24573)) && (DBL_PTR(_dll_id_24573)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_dll_id_24573);
        _dll_id_24573 = _1;
    }

    /** 			if dll_id = -1 then*/
    if (_dll_id_24573 != -1)
    goto LB; // [229] 255

    /** 				CompileErr(sprintf("Preprocessor shared library '%s' could not be loaded\n",*/
    _2 = (int)SEQ_PTR(_pp_24519);
    _14268 = (int)*(((s1_ptr)_2)->base + 2);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_14268);
    *((int *)(_2+4)) = _14268;
    _14269 = MAKE_SEQ(_1);
    _14268 = NOVALUE;
    _14270 = EPrintf(-9999999, _14267, _14269);
    DeRefDS(_14269);
    _14269 = NOVALUE;
    RefDS(_22682);
    _43CompileErr(_14270, _22682, 1);
    _14270 = NOVALUE;
LB: 

    /** 			rid = define_c_func(dll_id, "preprocess", { E_SEQUENCE, E_SEQUENCE, E_SEQUENCE }, */
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 134217732;
    *((int *)(_2+8)) = 134217732;
    *((int *)(_2+12)) = 134217732;
    _14272 = MAKE_SEQ(_1);
    RefDS(_14271);
    _rid_24569 = _11define_c_func(_dll_id_24573, _14271, _14272, 100663300);
    _14272 = NOVALUE;
    if (!IS_ATOM_INT(_rid_24569)) {
        _1 = (long)(DBL_PTR(_rid_24569)->dbl);
        if (UNIQUE(DBL_PTR(_rid_24569)) && (DBL_PTR(_rid_24569)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_rid_24569);
        _rid_24569 = _1;
    }

    /** 			if rid = -1 then*/
    if (_rid_24569 != -1)
    goto LC; // [282] 299

    /** 				CompileErr("Preprocessor entry point cound not be found\n",,1)*/
    RefDS(_14275);
    RefDS(_22682);
    _43CompileErr(_14275, _22682, 1);

    /** 				Cleanup(1)*/
    _43Cleanup(1);
LC: 

    /** 			preprocessors[pp_id][PP_RID] = rid*/
    _2 = (int)SEQ_PTR(_26preprocessors_11157);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26preprocessors_11157 = MAKE_SEQ(_2);
    }
    _3 = (int)(_pp_id_24520 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _rid_24569;
    DeRef(_1);
    _14276 = NOVALUE;
LA: 

    /** 		if c_func(rid, { fname, post_fname, pp[PP_PARAMS] }) != 0 then*/
    _2 = (int)SEQ_PTR(_pp_24519);
    _14278 = (int)*(((s1_ptr)_2)->base + 3);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_fname_24518);
    *((int *)(_2+4)) = _fname_24518;
    RefDS(_post_fname_24541);
    *((int *)(_2+8)) = _post_fname_24541;
    Ref(_14278);
    *((int *)(_2+12)) = _14278;
    _14279 = MAKE_SEQ(_1);
    _14278 = NOVALUE;
    _14280 = call_c(1, _rid_24569, _14279);
    DeRefDS(_14279);
    _14279 = NOVALUE;
    if (binary_op_a(EQUALS, _14280, 0)){
        DeRef(_14280);
        _14280 = NOVALUE;
        goto LD; // [338] 355
    }
    DeRef(_14280);
    _14280 = NOVALUE;

    /** 			CompileErr("Preprocessor call failed\n",,1)*/
    RefDS(_14282);
    RefDS(_22682);
    _43CompileErr(_14282, _22682, 1);

    /** 			Cleanup(1)*/
    _43Cleanup(1);
LD: 
    goto LE; // [357] 542
L9: 

    /** 		sequence public_cmd_args = {pp[PP_COMMAND]}*/
    _2 = (int)SEQ_PTR(_pp_24519);
    _14283 = (int)*(((s1_ptr)_2)->base + 2);
    _0 = _public_cmd_args_24609;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_14283);
    *((int *)(_2+4)) = _14283;
    _public_cmd_args_24609 = MAKE_SEQ(_1);
    DeRef(_0);
    _14283 = NOVALUE;

    /** 		sequence cmd_args = {canonical_path(pp[PP_COMMAND],,TO_SHORT)}*/
    _2 = (int)SEQ_PTR(_pp_24519);
    _14285 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_14285);
    _14286 = _9canonical_path(_14285, 0, 4);
    _14285 = NOVALUE;
    _0 = _cmd_args_24612;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _14286;
    _cmd_args_24612 = MAKE_SEQ(_1);
    DeRef(_0);
    _14286 = NOVALUE;

    /** 		if equal(fileext(pp[PP_COMMAND]), "ex") then*/
    _2 = (int)SEQ_PTR(_pp_24519);
    _14288 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_14288);
    _14289 = _9fileext(_14288);
    _14288 = NOVALUE;
    if (_14289 == _14290)
    _14291 = 1;
    else if (IS_ATOM_INT(_14289) && IS_ATOM_INT(_14290))
    _14291 = 0;
    else
    _14291 = (compare(_14289, _14290) == 0);
    DeRef(_14289);
    _14289 = NOVALUE;
    if (_14291 == 0)
    {
        _14291 = NOVALUE;
        goto LF; // [408] 432
    }
    else{
        _14291 = NOVALUE;
    }

    /** 			public_cmd_args = { "eui" } & public_cmd_args*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_14292);
    *((int *)(_2+4)) = _14292;
    _14293 = MAKE_SEQ(_1);
    Concat((object_ptr)&_public_cmd_args_24609, _14293, _public_cmd_args_24609);
    DeRefDS(_14293);
    _14293 = NOVALUE;
    DeRef(_14293);
    _14293 = NOVALUE;

    /** 			cmd_args = { "eui" } & cmd_args*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_14292);
    *((int *)(_2+4)) = _14292;
    _14295 = MAKE_SEQ(_1);
    Concat((object_ptr)&_cmd_args_24612, _14295, _cmd_args_24612);
    DeRefDS(_14295);
    _14295 = NOVALUE;
    DeRef(_14295);
    _14295 = NOVALUE;
LF: 

    /** 		cmd_args &= { "-i", canonical_path(fname,,TO_SHORT), "-o", canonical_path(post_fname,,TO_SHORT) }*/
    RefDS(_fname_24518);
    _14298 = _9canonical_path(_fname_24518, 0, 4);
    RefDS(_post_fname_24541);
    _14300 = _9canonical_path(_post_fname_24541, 0, 4);
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_14297);
    *((int *)(_2+4)) = _14297;
    *((int *)(_2+8)) = _14298;
    RefDS(_14299);
    *((int *)(_2+12)) = _14299;
    *((int *)(_2+16)) = _14300;
    _14301 = MAKE_SEQ(_1);
    _14300 = NOVALUE;
    _14298 = NOVALUE;
    Concat((object_ptr)&_cmd_args_24612, _cmd_args_24612, _14301);
    DeRefDS(_14301);
    _14301 = NOVALUE;

    /** 		public_cmd_args &= { "-i", fname, "-o", post_fname }*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_14297);
    *((int *)(_2+4)) = _14297;
    RefDS(_fname_24518);
    *((int *)(_2+8)) = _fname_24518;
    RefDS(_14299);
    *((int *)(_2+12)) = _14299;
    RefDS(_post_fname_24541);
    *((int *)(_2+16)) = _post_fname_24541;
    _14303 = MAKE_SEQ(_1);
    Concat((object_ptr)&_public_cmd_args_24609, _public_cmd_args_24609, _14303);
    DeRefDS(_14303);
    _14303 = NOVALUE;

    /** 		sequence cmd = build_commandline( cmd_args ) & pp[PP_PARAMS]*/
    RefDS(_cmd_args_24612);
    _14305 = _40build_commandline(_cmd_args_24612);
    _2 = (int)SEQ_PTR(_pp_24519);
    _14306 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_SEQUENCE(_14305) && IS_ATOM(_14306)) {
        Ref(_14306);
        Append(&_cmd_24641, _14305, _14306);
    }
    else if (IS_ATOM(_14305) && IS_SEQUENCE(_14306)) {
        Ref(_14305);
        Prepend(&_cmd_24641, _14306, _14305);
    }
    else {
        Concat((object_ptr)&_cmd_24641, _14305, _14306);
        DeRef(_14305);
        _14305 = NOVALUE;
    }
    DeRef(_14305);
    _14305 = NOVALUE;
    _14306 = NOVALUE;

    /** 		sequence pcmd = build_commandline(public_cmd_args) & pp[PP_PARAMS]*/
    RefDS(_public_cmd_args_24609);
    _14308 = _40build_commandline(_public_cmd_args_24609);
    _2 = (int)SEQ_PTR(_pp_24519);
    _14309 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_SEQUENCE(_14308) && IS_ATOM(_14309)) {
        Ref(_14309);
        Append(&_pcmd_24646, _14308, _14309);
    }
    else if (IS_ATOM(_14308) && IS_SEQUENCE(_14309)) {
        Ref(_14308);
        Prepend(&_pcmd_24646, _14309, _14308);
    }
    else {
        Concat((object_ptr)&_pcmd_24646, _14308, _14309);
        DeRef(_14308);
        _14308 = NOVALUE;
    }
    DeRef(_14308);
    _14308 = NOVALUE;
    _14309 = NOVALUE;

    /** 		integer result = system_exec(cmd, 2)*/
    _result_24651 = system_exec_call(_cmd_24641, 2);

    /** 		if result != 0 then*/
    if (_result_24651 == 0)
    goto L10; // [514] 539

    /** 			CompileErr(sprintf("Preprocessor command failed (%d): %s\n", { result, pcmd } ),,1)*/
    RefDS(_pcmd_24646);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _result_24651;
    ((int *)_2)[2] = _pcmd_24646;
    _14314 = MAKE_SEQ(_1);
    _14315 = EPrintf(-9999999, _14313, _14314);
    DeRefDS(_14314);
    _14314 = NOVALUE;
    RefDS(_22682);
    _43CompileErr(_14315, _22682, 1);
    _14315 = NOVALUE;

    /** 			Cleanup(1)*/
    _43Cleanup(1);
L10: 
    DeRef(_public_cmd_args_24609);
    _public_cmd_args_24609 = NOVALUE;
    DeRef(_cmd_args_24612);
    _cmd_args_24612 = NOVALUE;
    DeRef(_cmd_24641);
    _cmd_24641 = NOVALUE;
    DeRef(_pcmd_24646);
    _pcmd_24646 = NOVALUE;
LE: 

    /** 	return post_fname*/
    DeRefDS(_fname_24518);
    DeRef(_pp_24519);
    _14251 = NOVALUE;
    return _post_fname_24541;
    ;
}



// 0xBAEE8417
