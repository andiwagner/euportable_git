// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _3qmatch(int _p_10839, int _s_10840)
{
    int _k_10841 = NOVALUE;
    int _6100 = NOVALUE;
    int _6099 = NOVALUE;
    int _6098 = NOVALUE;
    int _6097 = NOVALUE;
    int _6096 = NOVALUE;
    int _6095 = NOVALUE;
    int _6094 = NOVALUE;
    int _6093 = NOVALUE;
    int _6092 = NOVALUE;
    int _6091 = NOVALUE;
    int _6090 = NOVALUE;
    int _6089 = NOVALUE;
    int _6087 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not find('?', p) then*/
    _6087 = find_from(63, _p_10839, 1);
    if (_6087 != 0)
    goto L1; // [12] 27
    _6087 = NOVALUE;

    /** 		return match(p, s) -- fast*/
    _6089 = e_match_from(_p_10839, _s_10840, 1);
    DeRefDS(_p_10839);
    DeRefDS(_s_10840);
    return _6089;
L1: 

    /** 	for i = 1 to length(s) - length(p) + 1 do*/
    if (IS_SEQUENCE(_s_10840)){
            _6090 = SEQ_PTR(_s_10840)->length;
    }
    else {
        _6090 = 1;
    }
    if (IS_SEQUENCE(_p_10839)){
            _6091 = SEQ_PTR(_p_10839)->length;
    }
    else {
        _6091 = 1;
    }
    _6092 = _6090 - _6091;
    _6090 = NOVALUE;
    _6091 = NOVALUE;
    _6093 = _6092 + 1;
    _6092 = NOVALUE;
    {
        int _i_10847;
        _i_10847 = 1;
L2: 
        if (_i_10847 > _6093){
            goto L3; // [43] 142
        }

        /** 		k = i*/
        _k_10841 = _i_10847;

        /** 		for j = 1 to length(p) do*/
        if (IS_SEQUENCE(_p_10839)){
                _6094 = SEQ_PTR(_p_10839)->length;
        }
        else {
            _6094 = 1;
        }
        {
            int _j_10853;
            _j_10853 = 1;
L4: 
            if (_j_10853 > _6094){
                goto L5; // [62] 122
            }

            /** 			if p[j] != s[k] and p[j] != '?' then*/
            _2 = (int)SEQ_PTR(_p_10839);
            _6095 = (int)*(((s1_ptr)_2)->base + _j_10853);
            _2 = (int)SEQ_PTR(_s_10840);
            _6096 = (int)*(((s1_ptr)_2)->base + _k_10841);
            if (IS_ATOM_INT(_6095) && IS_ATOM_INT(_6096)) {
                _6097 = (_6095 != _6096);
            }
            else {
                _6097 = binary_op(NOTEQ, _6095, _6096);
            }
            _6095 = NOVALUE;
            _6096 = NOVALUE;
            if (IS_ATOM_INT(_6097)) {
                if (_6097 == 0) {
                    goto L6; // [83] 109
                }
            }
            else {
                if (DBL_PTR(_6097)->dbl == 0.0) {
                    goto L6; // [83] 109
                }
            }
            _2 = (int)SEQ_PTR(_p_10839);
            _6099 = (int)*(((s1_ptr)_2)->base + _j_10853);
            if (IS_ATOM_INT(_6099)) {
                _6100 = (_6099 != 63);
            }
            else {
                _6100 = binary_op(NOTEQ, _6099, 63);
            }
            _6099 = NOVALUE;
            if (_6100 == 0) {
                DeRef(_6100);
                _6100 = NOVALUE;
                goto L6; // [96] 109
            }
            else {
                if (!IS_ATOM_INT(_6100) && DBL_PTR(_6100)->dbl == 0.0){
                    DeRef(_6100);
                    _6100 = NOVALUE;
                    goto L6; // [96] 109
                }
                DeRef(_6100);
                _6100 = NOVALUE;
            }
            DeRef(_6100);
            _6100 = NOVALUE;

            /** 				k = 0*/
            _k_10841 = 0;

            /** 				exit*/
            goto L5; // [106] 122
L6: 

            /** 			k += 1*/
            _k_10841 = _k_10841 + 1;

            /** 		end for*/
            _j_10853 = _j_10853 + 1;
            goto L4; // [117] 69
L5: 
            ;
        }

        /** 		if k != 0 then*/
        if (_k_10841 == 0)
        goto L7; // [124] 135

        /** 			return i*/
        DeRefDS(_p_10839);
        DeRefDS(_s_10840);
        DeRef(_6093);
        _6093 = NOVALUE;
        DeRef(_6097);
        _6097 = NOVALUE;
        return _i_10847;
L7: 

        /** 	end for*/
        _i_10847 = _i_10847 + 1;
        goto L2; // [137] 50
L3: 
        ;
    }

    /** 	return 0*/
    DeRefDS(_p_10839);
    DeRefDS(_s_10840);
    DeRef(_6093);
    _6093 = NOVALUE;
    DeRef(_6097);
    _6097 = NOVALUE;
    return 0;
    ;
}


int _3is_match(int _pattern_10868, int _string_10869)
{
    int _p_10870 = NOVALUE;
    int _f_10871 = NOVALUE;
    int _t_10872 = NOVALUE;
    int _match_string_10873 = NOVALUE;
    int _6142 = NOVALUE;
    int _6141 = NOVALUE;
    int _6139 = NOVALUE;
    int _6135 = NOVALUE;
    int _6134 = NOVALUE;
    int _6133 = NOVALUE;
    int _6130 = NOVALUE;
    int _6129 = NOVALUE;
    int _6126 = NOVALUE;
    int _6123 = NOVALUE;
    int _6121 = NOVALUE;
    int _6119 = NOVALUE;
    int _6117 = NOVALUE;
    int _6114 = NOVALUE;
    int _6112 = NOVALUE;
    int _6110 = NOVALUE;
    int _6109 = NOVALUE;
    int _6108 = NOVALUE;
    int _6107 = NOVALUE;
    int _6105 = NOVALUE;
    int _0, _1, _2;
    

    /** 	pattern = pattern & END_MARKER*/
    Append(&_pattern_10868, _pattern_10868, -1);

    /** 	string = string & END_MARKER*/
    Append(&_string_10869, _string_10869, -1);

    /** 	p = 1*/
    _p_10870 = 1;

    /** 	f = 1*/
    _f_10871 = 1;

    /** 	while f <= length(string) do*/
L1: 
    if (IS_SEQUENCE(_string_10869)){
            _6105 = SEQ_PTR(_string_10869)->length;
    }
    else {
        _6105 = 1;
    }
    if (_f_10871 > _6105)
    goto L2; // [35] 288

    /** 		if not find(pattern[p], {string[f], '?'}) then*/
    _2 = (int)SEQ_PTR(_pattern_10868);
    _6107 = (int)*(((s1_ptr)_2)->base + _p_10870);
    _2 = (int)SEQ_PTR(_string_10869);
    _6108 = (int)*(((s1_ptr)_2)->base + _f_10871);
    Ref(_6108);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6108;
    ((int *)_2)[2] = 63;
    _6109 = MAKE_SEQ(_1);
    _6108 = NOVALUE;
    _6110 = find_from(_6107, _6109, 1);
    _6107 = NOVALUE;
    DeRefDS(_6109);
    _6109 = NOVALUE;
    if (_6110 != 0)
    goto L3; // [58] 248
    _6110 = NOVALUE;

    /** 			if pattern[p] = '*' then*/
    _2 = (int)SEQ_PTR(_pattern_10868);
    _6112 = (int)*(((s1_ptr)_2)->base + _p_10870);
    if (binary_op_a(NOTEQ, _6112, 42)){
        _6112 = NOVALUE;
        goto L4; // [67] 240
    }
    _6112 = NOVALUE;

    /** 				while pattern[p] = '*' do*/
L5: 
    _2 = (int)SEQ_PTR(_pattern_10868);
    _6114 = (int)*(((s1_ptr)_2)->base + _p_10870);
    if (binary_op_a(NOTEQ, _6114, 42)){
        _6114 = NOVALUE;
        goto L6; // [80] 95
    }
    _6114 = NOVALUE;

    /** 					p += 1*/
    _p_10870 = _p_10870 + 1;

    /** 				end while*/
    goto L5; // [92] 76
L6: 

    /** 				if pattern[p] = END_MARKER then*/
    _2 = (int)SEQ_PTR(_pattern_10868);
    _6117 = (int)*(((s1_ptr)_2)->base + _p_10870);
    if (binary_op_a(NOTEQ, _6117, -1)){
        _6117 = NOVALUE;
        goto L7; // [101] 112
    }
    _6117 = NOVALUE;

    /** 					return 1*/
    DeRefDS(_pattern_10868);
    DeRefDS(_string_10869);
    DeRef(_match_string_10873);
    return 1;
L7: 

    /** 				match_string = ""*/
    RefDS(_5);
    DeRef(_match_string_10873);
    _match_string_10873 = _5;

    /** 				while pattern[p] != '*' do*/
L8: 
    _2 = (int)SEQ_PTR(_pattern_10868);
    _6119 = (int)*(((s1_ptr)_2)->base + _p_10870);
    if (binary_op_a(EQUALS, _6119, 42)){
        _6119 = NOVALUE;
        goto L9; // [128] 168
    }
    _6119 = NOVALUE;

    /** 					match_string = match_string & pattern[p]*/
    _2 = (int)SEQ_PTR(_pattern_10868);
    _6121 = (int)*(((s1_ptr)_2)->base + _p_10870);
    if (IS_SEQUENCE(_match_string_10873) && IS_ATOM(_6121)) {
        Ref(_6121);
        Append(&_match_string_10873, _match_string_10873, _6121);
    }
    else if (IS_ATOM(_match_string_10873) && IS_SEQUENCE(_6121)) {
    }
    else {
        Concat((object_ptr)&_match_string_10873, _match_string_10873, _6121);
    }
    _6121 = NOVALUE;

    /** 					if pattern[p] = END_MARKER then*/
    _2 = (int)SEQ_PTR(_pattern_10868);
    _6123 = (int)*(((s1_ptr)_2)->base + _p_10870);
    if (binary_op_a(NOTEQ, _6123, -1)){
        _6123 = NOVALUE;
        goto LA; // [148] 157
    }
    _6123 = NOVALUE;

    /** 						exit*/
    goto L9; // [154] 168
LA: 

    /** 					p += 1*/
    _p_10870 = _p_10870 + 1;

    /** 				end while*/
    goto L8; // [165] 124
L9: 

    /** 				if pattern[p] = '*' then*/
    _2 = (int)SEQ_PTR(_pattern_10868);
    _6126 = (int)*(((s1_ptr)_2)->base + _p_10870);
    if (binary_op_a(NOTEQ, _6126, 42)){
        _6126 = NOVALUE;
        goto LB; // [174] 185
    }
    _6126 = NOVALUE;

    /** 					p -= 1*/
    _p_10870 = _p_10870 - 1;
LB: 

    /** 				t = qmatch(match_string, string[f..$])*/
    if (IS_SEQUENCE(_string_10869)){
            _6129 = SEQ_PTR(_string_10869)->length;
    }
    else {
        _6129 = 1;
    }
    rhs_slice_target = (object_ptr)&_6130;
    RHS_Slice(_string_10869, _f_10871, _6129);
    RefDS(_match_string_10873);
    _t_10872 = _3qmatch(_match_string_10873, _6130);
    _6130 = NOVALUE;
    if (!IS_ATOM_INT(_t_10872)) {
        _1 = (long)(DBL_PTR(_t_10872)->dbl);
        if (UNIQUE(DBL_PTR(_t_10872)) && (DBL_PTR(_t_10872)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_t_10872);
        _t_10872 = _1;
    }

    /** 				if t = 0 then*/
    if (_t_10872 != 0)
    goto LC; // [204] 217

    /** 					return 0*/
    DeRefDS(_pattern_10868);
    DeRefDS(_string_10869);
    DeRefDS(_match_string_10873);
    return 0;
    goto LD; // [214] 247
LC: 

    /** 					f += t + length(match_string) - 2*/
    if (IS_SEQUENCE(_match_string_10873)){
            _6133 = SEQ_PTR(_match_string_10873)->length;
    }
    else {
        _6133 = 1;
    }
    _6134 = _t_10872 + _6133;
    if ((long)((unsigned long)_6134 + (unsigned long)HIGH_BITS) >= 0) 
    _6134 = NewDouble((double)_6134);
    _6133 = NOVALUE;
    if (IS_ATOM_INT(_6134)) {
        _6135 = _6134 - 2;
        if ((long)((unsigned long)_6135 +(unsigned long) HIGH_BITS) >= 0){
            _6135 = NewDouble((double)_6135);
        }
    }
    else {
        _6135 = NewDouble(DBL_PTR(_6134)->dbl - (double)2);
    }
    DeRef(_6134);
    _6134 = NOVALUE;
    if (IS_ATOM_INT(_6135)) {
        _f_10871 = _f_10871 + _6135;
    }
    else {
        _f_10871 = NewDouble((double)_f_10871 + DBL_PTR(_6135)->dbl);
    }
    DeRef(_6135);
    _6135 = NOVALUE;
    if (!IS_ATOM_INT(_f_10871)) {
        _1 = (long)(DBL_PTR(_f_10871)->dbl);
        if (UNIQUE(DBL_PTR(_f_10871)) && (DBL_PTR(_f_10871)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_f_10871);
        _f_10871 = _1;
    }
    goto LD; // [237] 247
L4: 

    /** 				return 0*/
    DeRefDS(_pattern_10868);
    DeRefDS(_string_10869);
    DeRef(_match_string_10873);
    return 0;
LD: 
L3: 

    /** 		p += 1*/
    _p_10870 = _p_10870 + 1;

    /** 		f += 1*/
    _f_10871 = _f_10871 + 1;

    /** 		if p > length(pattern) then*/
    if (IS_SEQUENCE(_pattern_10868)){
            _6139 = SEQ_PTR(_pattern_10868)->length;
    }
    else {
        _6139 = 1;
    }
    if (_p_10870 <= _6139)
    goto L1; // [265] 32

    /** 			return f > length(string) */
    if (IS_SEQUENCE(_string_10869)){
            _6141 = SEQ_PTR(_string_10869)->length;
    }
    else {
        _6141 = 1;
    }
    _6142 = (_f_10871 > _6141);
    _6141 = NOVALUE;
    DeRefDS(_pattern_10868);
    DeRefDS(_string_10869);
    DeRef(_match_string_10873);
    return _6142;

    /** 	end while*/
    goto L1; // [285] 32
L2: 

    /** 	return 0*/
    DeRefDS(_pattern_10868);
    DeRefDS(_string_10869);
    DeRef(_match_string_10873);
    DeRef(_6142);
    _6142 = NOVALUE;
    return 0;
    ;
}



// 0xE519FEFC
