// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _35massage(int _data_set_12854, int _subseq_opt_12855)
{
    int _7082 = NOVALUE;
    int _7081 = NOVALUE;
    int _0, _1, _2;
    

    /** 	switch subseq_opt do*/
    _0 = _subseq_opt_12855;
    switch ( _0 ){ 

        /** 		case ST_IGNSTR then*/
        case 2:

        /** 			return stdseq:remove_subseq(data_set, stdseq:SEQ_NOALT)*/
        RefDS(_data_set_12854);
        RefDS(_21SEQ_NOALT_6768);
        _7081 = _21remove_subseq(_data_set_12854, _21SEQ_NOALT_6768);
        DeRefDS(_data_set_12854);
        return _7081;
        goto L1; // [27] 57

        /** 		case ST_ZEROSTR then*/
        case 3:

        /** 			return stdseq:remove_subseq(data_set, 0)*/
        RefDS(_data_set_12854);
        _7082 = _21remove_subseq(_data_set_12854, 0);
        DeRefDS(_data_set_12854);
        DeRef(_7081);
        _7081 = NOVALUE;
        return _7082;
        goto L1; // [44] 57

        /** 		case else*/
        default:

        /** 			return data_set*/
        DeRef(_7081);
        _7081 = NOVALUE;
        DeRef(_7082);
        _7082 = NOVALUE;
        return _data_set_12854;
    ;}L1: 
    ;
}


int _35stdev(int _data_set_12865, int _subseq_opt_12866, int _population_type_12867)
{
    int _lSum_12868 = NOVALUE;
    int _lMean_12869 = NOVALUE;
    int _lCnt_12870 = NOVALUE;
    int _7099 = NOVALUE;
    int _7098 = NOVALUE;
    int _7094 = NOVALUE;
    int _7093 = NOVALUE;
    int _7092 = NOVALUE;
    int _7091 = NOVALUE;
    int _7088 = NOVALUE;
    int _7087 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data_set = massage(data_set, subseq_opt)*/
    RefDS(_data_set_12865);
    _0 = _data_set_12865;
    _data_set_12865 = _35massage(_data_set_12865, 1);
    DeRefDSi(_0);

    /** 	lCnt = length(data_set)*/
    if (IS_SEQUENCE(_data_set_12865)){
            _lCnt_12870 = SEQ_PTR(_data_set_12865)->length;
    }
    else {
        _lCnt_12870 = 1;
    }

    /** 	if lCnt = 0 then*/
    if (_lCnt_12870 != 0)
    goto L1; // [21] 32

    /** 		return {}*/
    RefDS(_5);
    DeRefDS(_data_set_12865);
    DeRef(_lSum_12868);
    DeRef(_lMean_12869);
    return _5;
L1: 

    /** 	if lCnt = 1 then*/
    if (_lCnt_12870 != 1)
    goto L2; // [34] 45

    /** 		return 0*/
    DeRefDS(_data_set_12865);
    DeRef(_lSum_12868);
    DeRef(_lMean_12869);
    return 0;
L2: 

    /** 	lSum = 0*/
    DeRef(_lSum_12868);
    _lSum_12868 = 0;

    /** 	for i = 1 to length(data_set) do*/
    if (IS_SEQUENCE(_data_set_12865)){
            _7087 = SEQ_PTR(_data_set_12865)->length;
    }
    else {
        _7087 = 1;
    }
    {
        int _i_12878;
        _i_12878 = 1;
L3: 
        if (_i_12878 > _7087){
            goto L4; // [55] 79
        }

        /** 		lSum += data_set[i]*/
        _2 = (int)SEQ_PTR(_data_set_12865);
        _7088 = (int)*(((s1_ptr)_2)->base + _i_12878);
        _0 = _lSum_12868;
        if (IS_ATOM_INT(_lSum_12868) && IS_ATOM_INT(_7088)) {
            _lSum_12868 = _lSum_12868 + _7088;
            if ((long)((unsigned long)_lSum_12868 + (unsigned long)HIGH_BITS) >= 0) 
            _lSum_12868 = NewDouble((double)_lSum_12868);
        }
        else {
            _lSum_12868 = binary_op(PLUS, _lSum_12868, _7088);
        }
        DeRef(_0);
        _7088 = NOVALUE;

        /** 	end for*/
        _i_12878 = _i_12878 + 1;
        goto L3; // [74] 62
L4: 
        ;
    }

    /** 	lMean = lSum / lCnt*/
    DeRef(_lMean_12869);
    if (IS_ATOM_INT(_lSum_12868)) {
        _lMean_12869 = (_lSum_12868 % _lCnt_12870) ? NewDouble((double)_lSum_12868 / _lCnt_12870) : (_lSum_12868 / _lCnt_12870);
    }
    else {
        _lMean_12869 = NewDouble(DBL_PTR(_lSum_12868)->dbl / (double)_lCnt_12870);
    }

    /** 	lSum = 0*/
    DeRef(_lSum_12868);
    _lSum_12868 = 0;

    /** 	for i = 1 to length(data_set) do*/
    if (IS_SEQUENCE(_data_set_12865)){
            _7091 = SEQ_PTR(_data_set_12865)->length;
    }
    else {
        _7091 = 1;
    }
    {
        int _i_12884;
        _i_12884 = 1;
L5: 
        if (_i_12884 > _7091){
            goto L6; // [95] 127
        }

        /** 		lSum += power(data_set[i] - lMean, 2)*/
        _2 = (int)SEQ_PTR(_data_set_12865);
        _7092 = (int)*(((s1_ptr)_2)->base + _i_12884);
        if (IS_ATOM_INT(_7092) && IS_ATOM_INT(_lMean_12869)) {
            _7093 = _7092 - _lMean_12869;
            if ((long)((unsigned long)_7093 +(unsigned long) HIGH_BITS) >= 0){
                _7093 = NewDouble((double)_7093);
            }
        }
        else {
            _7093 = binary_op(MINUS, _7092, _lMean_12869);
        }
        _7092 = NOVALUE;
        if (IS_ATOM_INT(_7093) && IS_ATOM_INT(_7093)) {
            if (_7093 == (short)_7093 && _7093 <= INT15 && _7093 >= -INT15)
            _7094 = _7093 * _7093;
            else
            _7094 = NewDouble(_7093 * (double)_7093);
        }
        else {
            _7094 = binary_op(MULTIPLY, _7093, _7093);
        }
        DeRef(_7093);
        _7093 = NOVALUE;
        _7093 = NOVALUE;
        _0 = _lSum_12868;
        if (IS_ATOM_INT(_lSum_12868) && IS_ATOM_INT(_7094)) {
            _lSum_12868 = _lSum_12868 + _7094;
            if ((long)((unsigned long)_lSum_12868 + (unsigned long)HIGH_BITS) >= 0) 
            _lSum_12868 = NewDouble((double)_lSum_12868);
        }
        else {
            _lSum_12868 = binary_op(PLUS, _lSum_12868, _7094);
        }
        DeRef(_0);
        DeRef(_7094);
        _7094 = NOVALUE;

        /** 	end for*/
        _i_12884 = _i_12884 + 1;
        goto L5; // [122] 102
L6: 
        ;
    }

    /** 	if population_type = ST_SAMPLE then*/
    if (_population_type_12867 != 2)
    goto L7; // [131] 142

    /** 		lCnt -= 1*/
    _lCnt_12870 = _lCnt_12870 - 1;
L7: 

    /** 	return power(lSum / lCnt, 0.5)*/
    if (IS_ATOM_INT(_lSum_12868)) {
        _7098 = (_lSum_12868 % _lCnt_12870) ? NewDouble((double)_lSum_12868 / _lCnt_12870) : (_lSum_12868 / _lCnt_12870);
    }
    else {
        _7098 = NewDouble(DBL_PTR(_lSum_12868)->dbl / (double)_lCnt_12870);
    }
    if (IS_ATOM_INT(_7098)) {
        temp_d.dbl = (double)_7098;
        _7099 = Dpower(&temp_d, DBL_PTR(_2254));
    }
    else {
        _7099 = Dpower(DBL_PTR(_7098), DBL_PTR(_2254));
    }
    DeRef(_7098);
    _7098 = NOVALUE;
    DeRefDS(_data_set_12865);
    DeRef(_lSum_12868);
    DeRef(_lMean_12869);
    return _7099;
    ;
}


int _35sum(int _data_set_12934, int _subseq_opt_12935)
{
    int _result__12936 = NOVALUE;
    int _7123 = NOVALUE;
    int _7122 = NOVALUE;
    int _7120 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(data_set) then*/
    _7120 = IS_ATOM(_data_set_12934);
    if (_7120 == 0)
    {
        _7120 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _7120 = NOVALUE;
    }

    /** 		return data_set*/
    DeRef(_result__12936);
    return _data_set_12934;
L1: 

    /** 	data_set = massage(data_set, subseq_opt)*/
    Ref(_data_set_12934);
    _0 = _data_set_12934;
    _data_set_12934 = _35massage(_data_set_12934, _subseq_opt_12935);
    DeRef(_0);

    /** 	result_ = 0*/
    DeRef(_result__12936);
    _result__12936 = 0;

    /** 	for i = 1 to length(data_set) do*/
    if (IS_SEQUENCE(_data_set_12934)){
            _7122 = SEQ_PTR(_data_set_12934)->length;
    }
    else {
        _7122 = 1;
    }
    {
        int _i_12941;
        _i_12941 = 1;
L2: 
        if (_i_12941 > _7122){
            goto L3; // [33] 57
        }

        /** 		result_ += data_set[i]*/
        _2 = (int)SEQ_PTR(_data_set_12934);
        _7123 = (int)*(((s1_ptr)_2)->base + _i_12941);
        _0 = _result__12936;
        if (IS_ATOM_INT(_result__12936) && IS_ATOM_INT(_7123)) {
            _result__12936 = _result__12936 + _7123;
            if ((long)((unsigned long)_result__12936 + (unsigned long)HIGH_BITS) >= 0) 
            _result__12936 = NewDouble((double)_result__12936);
        }
        else {
            _result__12936 = binary_op(PLUS, _result__12936, _7123);
        }
        DeRef(_0);
        _7123 = NOVALUE;

        /** 	end for*/
        _i_12941 = _i_12941 + 1;
        goto L2; // [52] 40
L3: 
        ;
    }

    /** 	return result_*/
    DeRef(_data_set_12934);
    return _result__12936;
    ;
}


int _35average(int _data_set_12955, int _subseq_opt_12956)
{
    int _7134 = NOVALUE;
    int _7133 = NOVALUE;
    int _7132 = NOVALUE;
    int _7130 = NOVALUE;
    int _7128 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(data_set) then*/
    _7128 = 0;
    if (_7128 == 0)
    {
        _7128 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _7128 = NOVALUE;
    }

    /** 		return data_set*/
    return _data_set_12955;
L1: 

    /** 	data_set = massage(data_set, subseq_opt)*/
    Ref(_data_set_12955);
    _0 = _data_set_12955;
    _data_set_12955 = _35massage(_data_set_12955, _subseq_opt_12956);
    DeRef(_0);

    /** 	if length(data_set) = 0 then*/
    if (IS_SEQUENCE(_data_set_12955)){
            _7130 = SEQ_PTR(_data_set_12955)->length;
    }
    else {
        _7130 = 1;
    }
    if (_7130 != 0)
    goto L2; // [28] 39

    /** 		return {}*/
    RefDS(_5);
    DeRef(_data_set_12955);
    return _5;
L2: 

    /** 	return sum(data_set) / length(data_set)*/
    Ref(_data_set_12955);
    _7132 = _35sum(_data_set_12955, 1);
    if (IS_SEQUENCE(_data_set_12955)){
            _7133 = SEQ_PTR(_data_set_12955)->length;
    }
    else {
        _7133 = 1;
    }
    if (IS_ATOM_INT(_7132)) {
        _7134 = (_7132 % _7133) ? NewDouble((double)_7132 / _7133) : (_7132 / _7133);
    }
    else {
        _7134 = binary_op(DIVIDE, _7132, _7133);
    }
    DeRef(_7132);
    _7132 = NOVALUE;
    _7133 = NOVALUE;
    DeRef(_data_set_12955);
    return _7134;
    ;
}



// 0x87003102
