// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _30EndLineTable()
{
    int _0, _1, _2;
    

    /** 	LineTable = append(LineTable, -2)*/
    Append(&_25LineTable_12356, _25LineTable_12356, -2);

    /** end procedure*/
    return;
    ;
}


void _30CreateTopLevel()
{
    int _28632 = NOVALUE;
    int _28630 = NOVALUE;
    int _28628 = NOVALUE;
    int _28626 = NOVALUE;
    int _28624 = NOVALUE;
    int _28622 = NOVALUE;
    int _28620 = NOVALUE;
    int _28618 = NOVALUE;
    int _28616 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	SymTab[TopLevelSub][S_NUM_ARGS] = 0*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25TopLevelSub_12269 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _28616 = NOVALUE;

    /** 	SymTab[TopLevelSub][S_TEMPS] = 0*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25TopLevelSub_12269 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_TEMPS_11958))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TEMPS_11958)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_TEMPS_11958);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _28618 = NOVALUE;

    /** 	SymTab[TopLevelSub][S_CODE] = {}*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25TopLevelSub_12269 + ((s1_ptr)_2)->base);
    RefDS(_22682);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_CODE_11925))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_CODE_11925);
    _1 = *(int *)_2;
    *(int *)_2 = _22682;
    DeRef(_1);
    _28620 = NOVALUE;

    /** 	SymTab[TopLevelSub][S_LINETAB] = {}*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25TopLevelSub_12269 + ((s1_ptr)_2)->base);
    RefDS(_22682);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_LINETAB_11948))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_LINETAB_11948)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_LINETAB_11948);
    _1 = *(int *)_2;
    *(int *)_2 = _22682;
    DeRef(_1);
    _28622 = NOVALUE;

    /** 	SymTab[TopLevelSub][S_FIRSTLINE] = 1*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25TopLevelSub_12269 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_FIRSTLINE_11953))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FIRSTLINE_11953)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_FIRSTLINE_11953);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _28624 = NOVALUE;

    /** 	SymTab[TopLevelSub][S_REFLIST] = {}*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25TopLevelSub_12269 + ((s1_ptr)_2)->base);
    RefDS(_22682);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 24);
    _1 = *(int *)_2;
    *(int *)_2 = _22682;
    DeRef(_1);
    _28626 = NOVALUE;

    /** 	SymTab[TopLevelSub][S_NREFS] = 1*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25TopLevelSub_12269 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _28628 = NOVALUE;

    /** 	SymTab[TopLevelSub][S_RESIDENT_TASK] = 1*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25TopLevelSub_12269 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 25);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _28630 = NOVALUE;

    /** 	SymTab[TopLevelSub][S_SAVED_PRIVATES] = {}*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25TopLevelSub_12269 + ((s1_ptr)_2)->base);
    RefDS(_22682);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 26);
    _1 = *(int *)_2;
    *(int *)_2 = _22682;
    DeRef(_1);
    _28632 = NOVALUE;

    /** 	Start_block( PROC, TopLevelSub )*/
    _66Start_block(27, _25TopLevelSub_12269);

    /** end procedure*/
    return;
    ;
}


void _30CheckForUndefinedGotoLabels()
{
    int _28646 = NOVALUE;
    int _28645 = NOVALUE;
    int _28642 = NOVALUE;
    int _28640 = NOVALUE;
    int _28638 = NOVALUE;
    int _28636 = NOVALUE;
    int _28635 = NOVALUE;
    int _28634 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(goto_delay) do*/
    if (IS_SEQUENCE(_25goto_delay_12393)){
            _28634 = SEQ_PTR(_25goto_delay_12393)->length;
    }
    else {
        _28634 = 1;
    }
    {
        int _i_55275;
        _i_55275 = 1;
L1: 
        if (_i_55275 > _28634){
            goto L2; // [8] 104
        }

        /** 		if not equal(goto_delay[i],"") then*/
        _2 = (int)SEQ_PTR(_25goto_delay_12393);
        _28635 = (int)*(((s1_ptr)_2)->base + _i_55275);
        if (_28635 == _22682)
        _28636 = 1;
        else if (IS_ATOM_INT(_28635) && IS_ATOM_INT(_22682))
        _28636 = 0;
        else
        _28636 = (compare(_28635, _22682) == 0);
        _28635 = NOVALUE;
        if (_28636 != 0)
        goto L3; // [27] 97
        _28636 = NOVALUE;

        /** 			line_number = goto_line[i][1] -- tell compiler the correct line number*/
        _2 = (int)SEQ_PTR(_30goto_line_55181);
        _28638 = (int)*(((s1_ptr)_2)->base + _i_55275);
        _2 = (int)SEQ_PTR(_28638);
        _25line_number_12263 = (int)*(((s1_ptr)_2)->base + 1);
        if (!IS_ATOM_INT(_25line_number_12263)){
            _25line_number_12263 = (long)DBL_PTR(_25line_number_12263)->dbl;
        }
        _28638 = NOVALUE;

        /** 			gline_number = goto_line[i][1] -- tell compiler the correct line number*/
        _2 = (int)SEQ_PTR(_30goto_line_55181);
        _28640 = (int)*(((s1_ptr)_2)->base + _i_55275);
        _2 = (int)SEQ_PTR(_28640);
        _25gline_number_12267 = (int)*(((s1_ptr)_2)->base + 1);
        if (!IS_ATOM_INT(_25gline_number_12267)){
            _25gline_number_12267 = (long)DBL_PTR(_25gline_number_12267)->dbl;
        }
        _28640 = NOVALUE;

        /** 			ThisLine = goto_line[i][2] -- tell compiler the correct line number*/
        _2 = (int)SEQ_PTR(_30goto_line_55181);
        _28642 = (int)*(((s1_ptr)_2)->base + _i_55275);
        DeRef(_43ThisLine_49532);
        _2 = (int)SEQ_PTR(_28642);
        _43ThisLine_49532 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_43ThisLine_49532);
        _28642 = NOVALUE;

        /** 			bp = length(ThisLine)*/
        if (IS_SEQUENCE(_43ThisLine_49532)){
                _43bp_49536 = SEQ_PTR(_43ThisLine_49532)->length;
        }
        else {
            _43bp_49536 = 1;
        }

        /** 				CompileErr(156, {goto_delay[i]})*/
        _2 = (int)SEQ_PTR(_25goto_delay_12393);
        _28645 = (int)*(((s1_ptr)_2)->base + _i_55275);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_28645);
        *((int *)(_2+4)) = _28645;
        _28646 = MAKE_SEQ(_1);
        _28645 = NOVALUE;
        _43CompileErr(156, _28646, 0);
        _28646 = NOVALUE;
L3: 

        /** 	end for*/
        _i_55275 = _i_55275 + 1;
        goto L1; // [99] 15
L2: 
        ;
    }

    /** end procedure*/
    return;
    ;
}


void _30PushGoto()
{
    int _28647 = NOVALUE;
    int _0, _1, _2;
    

    /** 	goto_stack = append(goto_stack, {goto_addr, goto_list, goto_labels, goto_delay, goto_line, goto_ref, label_block, goto_init })*/
    _1 = NewS1(8);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_30goto_addr_55183);
    *((int *)(_2+4)) = _30goto_addr_55183;
    RefDS(_25goto_list_12394);
    *((int *)(_2+8)) = _25goto_list_12394;
    RefDS(_30goto_labels_55182);
    *((int *)(_2+12)) = _30goto_labels_55182;
    RefDS(_25goto_delay_12393);
    *((int *)(_2+16)) = _25goto_delay_12393;
    RefDS(_30goto_line_55181);
    *((int *)(_2+20)) = _30goto_line_55181;
    RefDS(_30goto_ref_55185);
    *((int *)(_2+24)) = _30goto_ref_55185;
    RefDS(_30label_block_55186);
    *((int *)(_2+28)) = _30label_block_55186;
    Ref(_30goto_init_55188);
    *((int *)(_2+32)) = _30goto_init_55188;
    _28647 = MAKE_SEQ(_1);
    RefDS(_28647);
    Append(&_30goto_stack_55184, _30goto_stack_55184, _28647);
    DeRefDS(_28647);
    _28647 = NOVALUE;

    /** 	goto_addr = {}*/
    RefDS(_22682);
    DeRefDS(_30goto_addr_55183);
    _30goto_addr_55183 = _22682;

    /** 	goto_list = {}*/
    RefDS(_22682);
    DeRefDS(_25goto_list_12394);
    _25goto_list_12394 = _22682;

    /** 	goto_labels = {}*/
    RefDS(_22682);
    DeRefDS(_30goto_labels_55182);
    _30goto_labels_55182 = _22682;

    /** 	goto_delay = {}*/
    RefDS(_22682);
    DeRefDS(_25goto_delay_12393);
    _25goto_delay_12393 = _22682;

    /** 	goto_line = {}*/
    RefDS(_22682);
    DeRefDS(_30goto_line_55181);
    _30goto_line_55181 = _22682;

    /** 	goto_ref = {}*/
    RefDS(_22682);
    DeRefDS(_30goto_ref_55185);
    _30goto_ref_55185 = _22682;

    /** 	label_block = {}*/
    RefDS(_22682);
    DeRefDS(_30label_block_55186);
    _30label_block_55186 = _22682;

    /** 	goto_init = map:new()*/
    _0 = _32new(690);
    DeRef(_30goto_init_55188);
    _30goto_init_55188 = _0;

    /** end procedure*/
    return;
    ;
}


void _30PopGoto()
{
    int _28674 = NOVALUE;
    int _28672 = NOVALUE;
    int _28671 = NOVALUE;
    int _28669 = NOVALUE;
    int _28668 = NOVALUE;
    int _28666 = NOVALUE;
    int _28665 = NOVALUE;
    int _28663 = NOVALUE;
    int _28662 = NOVALUE;
    int _28660 = NOVALUE;
    int _28659 = NOVALUE;
    int _28657 = NOVALUE;
    int _28656 = NOVALUE;
    int _28654 = NOVALUE;
    int _28653 = NOVALUE;
    int _28651 = NOVALUE;
    int _28650 = NOVALUE;
    int _0, _1, _2;
    

    /** 	CheckForUndefinedGotoLabels()*/
    _30CheckForUndefinedGotoLabels();

    /** 	goto_addr   = goto_stack[$][1]*/
    if (IS_SEQUENCE(_30goto_stack_55184)){
            _28650 = SEQ_PTR(_30goto_stack_55184)->length;
    }
    else {
        _28650 = 1;
    }
    _2 = (int)SEQ_PTR(_30goto_stack_55184);
    _28651 = (int)*(((s1_ptr)_2)->base + _28650);
    DeRef(_30goto_addr_55183);
    _2 = (int)SEQ_PTR(_28651);
    _30goto_addr_55183 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_30goto_addr_55183);
    _28651 = NOVALUE;

    /** 	goto_list   = goto_stack[$][2]*/
    if (IS_SEQUENCE(_30goto_stack_55184)){
            _28653 = SEQ_PTR(_30goto_stack_55184)->length;
    }
    else {
        _28653 = 1;
    }
    _2 = (int)SEQ_PTR(_30goto_stack_55184);
    _28654 = (int)*(((s1_ptr)_2)->base + _28653);
    DeRef(_25goto_list_12394);
    _2 = (int)SEQ_PTR(_28654);
    _25goto_list_12394 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_25goto_list_12394);
    _28654 = NOVALUE;

    /** 	goto_labels = goto_stack[$][3]*/
    if (IS_SEQUENCE(_30goto_stack_55184)){
            _28656 = SEQ_PTR(_30goto_stack_55184)->length;
    }
    else {
        _28656 = 1;
    }
    _2 = (int)SEQ_PTR(_30goto_stack_55184);
    _28657 = (int)*(((s1_ptr)_2)->base + _28656);
    DeRef(_30goto_labels_55182);
    _2 = (int)SEQ_PTR(_28657);
    _30goto_labels_55182 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_30goto_labels_55182);
    _28657 = NOVALUE;

    /** 	goto_delay  = goto_stack[$][4]*/
    if (IS_SEQUENCE(_30goto_stack_55184)){
            _28659 = SEQ_PTR(_30goto_stack_55184)->length;
    }
    else {
        _28659 = 1;
    }
    _2 = (int)SEQ_PTR(_30goto_stack_55184);
    _28660 = (int)*(((s1_ptr)_2)->base + _28659);
    DeRef(_25goto_delay_12393);
    _2 = (int)SEQ_PTR(_28660);
    _25goto_delay_12393 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_25goto_delay_12393);
    _28660 = NOVALUE;

    /** 	goto_line   = goto_stack[$][5]*/
    if (IS_SEQUENCE(_30goto_stack_55184)){
            _28662 = SEQ_PTR(_30goto_stack_55184)->length;
    }
    else {
        _28662 = 1;
    }
    _2 = (int)SEQ_PTR(_30goto_stack_55184);
    _28663 = (int)*(((s1_ptr)_2)->base + _28662);
    DeRef(_30goto_line_55181);
    _2 = (int)SEQ_PTR(_28663);
    _30goto_line_55181 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_30goto_line_55181);
    _28663 = NOVALUE;

    /** 	goto_ref    = goto_stack[$][6]*/
    if (IS_SEQUENCE(_30goto_stack_55184)){
            _28665 = SEQ_PTR(_30goto_stack_55184)->length;
    }
    else {
        _28665 = 1;
    }
    _2 = (int)SEQ_PTR(_30goto_stack_55184);
    _28666 = (int)*(((s1_ptr)_2)->base + _28665);
    DeRef(_30goto_ref_55185);
    _2 = (int)SEQ_PTR(_28666);
    _30goto_ref_55185 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_30goto_ref_55185);
    _28666 = NOVALUE;

    /** 	label_block = goto_stack[$][7]*/
    if (IS_SEQUENCE(_30goto_stack_55184)){
            _28668 = SEQ_PTR(_30goto_stack_55184)->length;
    }
    else {
        _28668 = 1;
    }
    _2 = (int)SEQ_PTR(_30goto_stack_55184);
    _28669 = (int)*(((s1_ptr)_2)->base + _28668);
    DeRef(_30label_block_55186);
    _2 = (int)SEQ_PTR(_28669);
    _30label_block_55186 = (int)*(((s1_ptr)_2)->base + 7);
    Ref(_30label_block_55186);
    _28669 = NOVALUE;

    /** 	goto_init   = goto_stack[$][8]*/
    if (IS_SEQUENCE(_30goto_stack_55184)){
            _28671 = SEQ_PTR(_30goto_stack_55184)->length;
    }
    else {
        _28671 = 1;
    }
    _2 = (int)SEQ_PTR(_30goto_stack_55184);
    _28672 = (int)*(((s1_ptr)_2)->base + _28671);
    DeRef(_30goto_init_55188);
    _2 = (int)SEQ_PTR(_28672);
    _30goto_init_55188 = (int)*(((s1_ptr)_2)->base + 8);
    Ref(_30goto_init_55188);
    _28672 = NOVALUE;

    /** 	goto_stack = remove( goto_stack, length( goto_stack ) )*/
    if (IS_SEQUENCE(_30goto_stack_55184)){
            _28674 = SEQ_PTR(_30goto_stack_55184)->length;
    }
    else {
        _28674 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_30goto_stack_55184);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_28674)) ? _28674 : (long)(DBL_PTR(_28674)->dbl);
        int stop = (IS_ATOM_INT(_28674)) ? _28674 : (long)(DBL_PTR(_28674)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_30goto_stack_55184), start, &_30goto_stack_55184 );
            }
            else Tail(SEQ_PTR(_30goto_stack_55184), stop+1, &_30goto_stack_55184);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_30goto_stack_55184), start, &_30goto_stack_55184);
        }
        else {
            assign_slice_seq = &assign_space;
            _30goto_stack_55184 = Remove_elements(start, stop, (SEQ_PTR(_30goto_stack_55184)->ref == 1));
        }
    }
    _28674 = NOVALUE;
    _28674 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


void _30EnterTopLevel(int _end_line_table_55340)
{
    int _28689 = NOVALUE;
    int _28688 = NOVALUE;
    int _28686 = NOVALUE;
    int _28685 = NOVALUE;
    int _28683 = NOVALUE;
    int _28681 = NOVALUE;
    int _28680 = NOVALUE;
    int _28678 = NOVALUE;
    int _28676 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	if CurrentSub then*/
    if (_25CurrentSub_12270 == 0)
    {
        goto L1; // [7] 59
    }
    else{
    }

    /** 		if end_line_table then*/
    if (_end_line_table_55340 == 0)
    {
        goto L2; // [12] 58
    }
    else{
    }

    /** 			EndLineTable()*/
    _30EndLineTable();

    /** 			SymTab[CurrentSub][S_LINETAB] = LineTable*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25CurrentSub_12270 + ((s1_ptr)_2)->base);
    RefDS(_25LineTable_12356);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_LINETAB_11948))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_LINETAB_11948)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_LINETAB_11948);
    _1 = *(int *)_2;
    *(int *)_2 = _25LineTable_12356;
    DeRef(_1);
    _28676 = NOVALUE;

    /** 			SymTab[CurrentSub][S_CODE] = Code*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25CurrentSub_12270 + ((s1_ptr)_2)->base);
    RefDS(_25Code_12355);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_CODE_11925))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_CODE_11925);
    _1 = *(int *)_2;
    *(int *)_2 = _25Code_12355;
    DeRef(_1);
    _28678 = NOVALUE;
L2: 
L1: 

    /** 	if length(goto_stack) then*/
    if (IS_SEQUENCE(_30goto_stack_55184)){
            _28680 = SEQ_PTR(_30goto_stack_55184)->length;
    }
    else {
        _28680 = 1;
    }
    if (_28680 == 0)
    {
        _28680 = NOVALUE;
        goto L3; // [66] 74
    }
    else{
        _28680 = NOVALUE;
    }

    /** 		PopGoto()*/
    _30PopGoto();
L3: 

    /** 	LineTable = SymTab[TopLevelSub][S_LINETAB]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28681 = (int)*(((s1_ptr)_2)->base + _25TopLevelSub_12269);
    DeRef(_25LineTable_12356);
    _2 = (int)SEQ_PTR(_28681);
    if (!IS_ATOM_INT(_25S_LINETAB_11948)){
        _25LineTable_12356 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_LINETAB_11948)->dbl));
    }
    else{
        _25LineTable_12356 = (int)*(((s1_ptr)_2)->base + _25S_LINETAB_11948);
    }
    Ref(_25LineTable_12356);
    _28681 = NOVALUE;

    /** 	Code = SymTab[TopLevelSub][S_CODE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28683 = (int)*(((s1_ptr)_2)->base + _25TopLevelSub_12269);
    DeRef(_25Code_12355);
    _2 = (int)SEQ_PTR(_28683);
    if (!IS_ATOM_INT(_25S_CODE_11925)){
        _25Code_12355 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    }
    else{
        _25Code_12355 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
    }
    Ref(_25Code_12355);
    _28683 = NOVALUE;

    /** 	previous_op = -1*/
    _25previous_op_12379 = -1;

    /** 	CurrentSub = TopLevelSub*/
    _25CurrentSub_12270 = _25TopLevelSub_12269;

    /** 	clear_last()*/
    _37clear_last();

    /** 	if length( branch_stack ) then*/
    if (IS_SEQUENCE(_30branch_stack_55170)){
            _28685 = SEQ_PTR(_30branch_stack_55170)->length;
    }
    else {
        _28685 = 1;
    }
    if (_28685 == 0)
    {
        _28685 = NOVALUE;
        goto L4; // [135] 169
    }
    else{
        _28685 = NOVALUE;
    }

    /** 		branch_list = branch_stack[$]*/
    if (IS_SEQUENCE(_30branch_stack_55170)){
            _28686 = SEQ_PTR(_30branch_stack_55170)->length;
    }
    else {
        _28686 = 1;
    }
    DeRef(_30branch_list_55169);
    _2 = (int)SEQ_PTR(_30branch_stack_55170);
    _30branch_list_55169 = (int)*(((s1_ptr)_2)->base + _28686);
    Ref(_30branch_list_55169);

    /** 		branch_stack = tail( branch_stack )*/
    if (IS_SEQUENCE(_30branch_stack_55170)){
            _28688 = SEQ_PTR(_30branch_stack_55170)->length;
    }
    else {
        _28688 = 1;
    }
    _28689 = _28688 - 1;
    _28688 = NOVALUE;
    {
        int len = SEQ_PTR(_30branch_stack_55170)->length;
        int size = (IS_ATOM_INT(_28689)) ? _28689 : (long)(DBL_PTR(_28689)->dbl);
        if (size <= 0) {
            DeRef(_30branch_stack_55170);
            _30branch_stack_55170 = MAKE_SEQ(NewS1(0));
        }
        else if (len <= size) {
            RefDS(_30branch_stack_55170);
            DeRef(_30branch_stack_55170);
            _30branch_stack_55170 = _30branch_stack_55170;
        }
        else Tail(SEQ_PTR(_30branch_stack_55170), len-size+1, &_30branch_stack_55170);
    }
    _28689 = NOVALUE;
L4: 

    /** end procedure*/
    return;
    ;
}


void _30LeaveTopLevel()
{
    int _28694 = NOVALUE;
    int _28692 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	branch_stack = append( branch_stack, branch_list )*/
    RefDS(_30branch_list_55169);
    Append(&_30branch_stack_55170, _30branch_stack_55170, _30branch_list_55169);

    /** 	branch_list = {}*/
    RefDS(_22682);
    DeRefDS(_30branch_list_55169);
    _30branch_list_55169 = _22682;

    /** 	PushGoto()*/
    _30PushGoto();

    /** 	LastLineNumber = -1*/
    _60LastLineNumber_24674 = -1;

    /** 	SymTab[TopLevelSub][S_LINETAB] = LineTable*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25TopLevelSub_12269 + ((s1_ptr)_2)->base);
    RefDS(_25LineTable_12356);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_LINETAB_11948))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_LINETAB_11948)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_LINETAB_11948);
    _1 = *(int *)_2;
    *(int *)_2 = _25LineTable_12356;
    DeRef(_1);
    _28692 = NOVALUE;

    /** 	SymTab[TopLevelSub][S_CODE] = Code*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25TopLevelSub_12269 + ((s1_ptr)_2)->base);
    RefDS(_25Code_12355);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_CODE_11925))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_CODE_11925);
    _1 = *(int *)_2;
    *(int *)_2 = _25Code_12355;
    DeRef(_1);
    _28694 = NOVALUE;

    /** 	LineTable = {}*/
    RefDS(_22682);
    DeRefDS(_25LineTable_12356);
    _25LineTable_12356 = _22682;

    /** 	Code = {}*/
    RefDS(_22682);
    DeRefDS(_25Code_12355);
    _25Code_12355 = _22682;

    /** 	previous_op = -1*/
    _25previous_op_12379 = -1;

    /** 	clear_last()*/
    _37clear_last();

    /** end procedure*/
    return;
    ;
}


void _30InitParser()
{
    int _0, _1, _2;
    

    /** 	goto_stack = {}*/
    RefDS(_22682);
    DeRef(_30goto_stack_55184);
    _30goto_stack_55184 = _22682;

    /** 	goto_labels = {}*/
    RefDS(_22682);
    DeRef(_30goto_labels_55182);
    _30goto_labels_55182 = _22682;

    /** 	label_block = {}*/
    RefDS(_22682);
    DeRef(_30label_block_55186);
    _30label_block_55186 = _22682;

    /** 	goto_ref = {}*/
    RefDS(_22682);
    DeRef(_30goto_ref_55185);
    _30goto_ref_55185 = _22682;

    /** 	goto_addr = {}*/
    RefDS(_22682);
    DeRef(_30goto_addr_55183);
    _30goto_addr_55183 = _22682;

    /** 	goto_line = {}*/
    RefDS(_22682);
    DeRef(_30goto_line_55181);
    _30goto_line_55181 = _22682;

    /** 	break_list = {}*/
    RefDS(_22682);
    DeRefi(_30break_list_55189);
    _30break_list_55189 = _22682;

    /** 	break_delay = {}*/
    RefDS(_22682);
    DeRef(_30break_delay_55190);
    _30break_delay_55190 = _22682;

    /** 	exit_list = {}*/
    RefDS(_22682);
    DeRefi(_30exit_list_55191);
    _30exit_list_55191 = _22682;

    /** 	exit_delay = {}*/
    RefDS(_22682);
    DeRef(_30exit_delay_55192);
    _30exit_delay_55192 = _22682;

    /** 	continue_list = {}*/
    RefDS(_22682);
    DeRefi(_30continue_list_55193);
    _30continue_list_55193 = _22682;

    /** 	continue_delay = {}*/
    RefDS(_22682);
    DeRef(_30continue_delay_55194);
    _30continue_delay_55194 = _22682;

    /** 	init_stack = {}*/
    RefDS(_22682);
    DeRefi(_30init_stack_55204);
    _30init_stack_55204 = _22682;

    /** 	CurrentSub = 0*/
    _25CurrentSub_12270 = 0;

    /** 	CreateTopLevel()*/
    _30CreateTopLevel();

    /** 	EnterTopLevel()*/
    _30EnterTopLevel(1);

    /** 	backed_up_tok = {}*/
    RefDS(_22682);
    DeRef(_30backed_up_tok_55178);
    _30backed_up_tok_55178 = _22682;

    /** 	loop_stack = {}*/
    RefDS(_22682);
    DeRefi(_30loop_stack_55205);
    _30loop_stack_55205 = _22682;

    /** 	stmt_nest = 0*/
    _30stmt_nest_55203 = 0;

    /** 	loop_labels = {}*/
    RefDS(_22682);
    DeRef(_30loop_labels_55199);
    _30loop_labels_55199 = _22682;

    /** 	if_labels = {}*/
    RefDS(_22682);
    DeRef(_30if_labels_55200);
    _30if_labels_55200 = _22682;

    /** 	if_stack = {}*/
    RefDS(_22682);
    DeRefi(_30if_stack_55206);
    _30if_stack_55206 = _22682;

    /** 	continue_addr = {}*/
    RefDS(_22682);
    DeRefi(_30continue_addr_55196);
    _30continue_addr_55196 = _22682;

    /** 	retry_addr = {}*/
    RefDS(_22682);
    DeRefi(_30retry_addr_55197);
    _30retry_addr_55197 = _22682;

    /** 	entry_addr = {}*/
    RefDS(_22682);
    DeRefi(_30entry_addr_55195);
    _30entry_addr_55195 = _22682;

    /** 	block_list = {}*/
    RefDS(_22682);
    DeRefi(_30block_list_55201);
    _30block_list_55201 = _22682;

    /** 	block_index = 0*/
    _30block_index_55202 = 0;

    /** 	param_num = -1*/
    _30param_num_55180 = -1;

    /** 	entry_stack = {}*/
    RefDS(_22682);
    DeRef(_30entry_stack_55198);
    _30entry_stack_55198 = _22682;

    /** 	goto_init = map:new()*/
    _0 = _32new(690);
    DeRef(_30goto_init_55188);
    _30goto_init_55188 = _0;

    /** end procedure*/
    return;
    ;
}


void _30NotReached(int _tok_55419, int _keyword_55420)
{
    int _28715 = NOVALUE;
    int _28714 = NOVALUE;
    int _28713 = NOVALUE;
    int _28712 = NOVALUE;
    int _28711 = NOVALUE;
    int _28710 = NOVALUE;
    int _28708 = NOVALUE;
    int _28707 = NOVALUE;
    int _28706 = NOVALUE;
    int _28705 = NOVALUE;
    int _28703 = NOVALUE;
    int _28702 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_tok_55419)) {
        _1 = (long)(DBL_PTR(_tok_55419)->dbl);
        if (UNIQUE(DBL_PTR(_tok_55419)) && (DBL_PTR(_tok_55419)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_tok_55419);
        _tok_55419 = _1;
    }

    /** 	if not find(tok, {END, ELSE, ELSIF, END_OF_FILE, CASE, IFDEF, ELSIFDEF, ELSEDEF}) then*/
    _1 = NewS1(8);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 402;
    *((int *)(_2+8)) = 23;
    *((int *)(_2+12)) = 414;
    *((int *)(_2+16)) = -21;
    *((int *)(_2+20)) = 186;
    *((int *)(_2+24)) = 407;
    *((int *)(_2+28)) = 408;
    *((int *)(_2+32)) = 409;
    _28702 = MAKE_SEQ(_1);
    _28703 = find_from(_tok_55419, _28702, 1);
    DeRefDS(_28702);
    _28702 = NOVALUE;
    if (_28703 != 0)
    goto L1; // [39] 135
    _28703 = NOVALUE;

    /** 		if equal(keyword, "goto") and find(tok, {LOOP, LABEL, WHILE}) then*/
    if (_keyword_55420 == _27057)
    _28705 = 1;
    else if (IS_ATOM_INT(_keyword_55420) && IS_ATOM_INT(_27057))
    _28705 = 0;
    else
    _28705 = (compare(_keyword_55420, _27057) == 0);
    if (_28705 == 0) {
        goto L2; // [48] 79
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 422;
    *((int *)(_2+8)) = 419;
    *((int *)(_2+12)) = 47;
    _28707 = MAKE_SEQ(_1);
    _28708 = find_from(_tok_55419, _28707, 1);
    DeRefDS(_28707);
    _28707 = NOVALUE;
    if (_28708 == 0)
    {
        _28708 = NOVALUE;
        goto L2; // [70] 79
    }
    else{
        _28708 = NOVALUE;
    }

    /** 			return*/
    DeRefDSi(_keyword_55420);
    return;
L2: 

    /** 		if equal(keyword, "abort()") and tok = LABEL then*/
    if (_keyword_55420 == _28709)
    _28710 = 1;
    else if (IS_ATOM_INT(_keyword_55420) && IS_ATOM_INT(_28709))
    _28710 = 0;
    else
    _28710 = (compare(_keyword_55420, _28709) == 0);
    if (_28710 == 0) {
        goto L3; // [85] 105
    }
    _28712 = (_tok_55419 == 419);
    if (_28712 == 0)
    {
        DeRef(_28712);
        _28712 = NOVALUE;
        goto L3; // [96] 105
    }
    else{
        DeRef(_28712);
        _28712 = NOVALUE;
    }

    /** 			return*/
    DeRefDSi(_keyword_55420);
    return;
L3: 

    /** 		Warning(218, not_reached_warning_flag,*/
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _28713 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    Ref(_28713);
    _28714 = _52name_ext(_28713);
    _28713 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _28714;
    *((int *)(_2+8)) = _25line_number_12263;
    RefDS(_keyword_55420);
    *((int *)(_2+12)) = _keyword_55420;
    _28715 = MAKE_SEQ(_1);
    _28714 = NOVALUE;
    _43Warning(218, 512, _28715);
    _28715 = NOVALUE;
L1: 

    /** end procedure*/
    DeRefDSi(_keyword_55420);
    return;
    ;
}


void _30Forward_InitCheck(int _tok_55459, int _ref_55460)
{
    int _sym_55462 = NOVALUE;
    int _28721 = NOVALUE;
    int _28720 = NOVALUE;
    int _28719 = NOVALUE;
    int _28717 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if ref then*/
    if (_ref_55460 == 0)
    {
        goto L1; // [5] 83
    }
    else{
    }

    /** 		integer sym = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_55459);
    _sym_55462 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_sym_55462)){
        _sym_55462 = (long)DBL_PTR(_sym_55462)->dbl;
    }

    /** 		if tok[T_ID] = QUALIFIED_VARIABLE then*/
    _2 = (int)SEQ_PTR(_tok_55459);
    _28717 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _28717, 512)){
        _28717 = NOVALUE;
        goto L2; // [28] 50
    }
    _28717 = NOVALUE;

    /** 			set_qualified_fwd( SymTab[sym][S_FILE_NO] )*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28719 = (int)*(((s1_ptr)_2)->base + _sym_55462);
    _2 = (int)SEQ_PTR(_28719);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _28720 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _28720 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _28719 = NOVALUE;
    Ref(_28720);
    _60set_qualified_fwd(_28720);
    _28720 = NOVALUE;
L2: 

    /** 		ref = new_forward_reference( GLOBAL_INIT_CHECK, tok[T_SYM], GLOBAL_INIT_CHECK )*/
    _2 = (int)SEQ_PTR(_tok_55459);
    _28721 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_28721);
    _ref_55460 = _29new_forward_reference(109, _28721, 109);
    _28721 = NOVALUE;
    if (!IS_ATOM_INT(_ref_55460)) {
        _1 = (long)(DBL_PTR(_ref_55460)->dbl);
        if (UNIQUE(DBL_PTR(_ref_55460)) && (DBL_PTR(_ref_55460)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_55460);
        _ref_55460 = _1;
    }

    /** 		emit_op( GLOBAL_INIT_CHECK )*/
    _37emit_op(109);

    /** 		emit_addr( sym )*/
    _37emit_addr(_sym_55462);
L1: 

    /** end procedure*/
    DeRef(_tok_55459);
    return;
    ;
}


void _30InitCheck(int _sym_55487, int _ref_55488)
{
    int _28798 = NOVALUE;
    int _28797 = NOVALUE;
    int _28796 = NOVALUE;
    int _28795 = NOVALUE;
    int _28794 = NOVALUE;
    int _28793 = NOVALUE;
    int _28792 = NOVALUE;
    int _28791 = NOVALUE;
    int _28789 = NOVALUE;
    int _28787 = NOVALUE;
    int _28786 = NOVALUE;
    int _28784 = NOVALUE;
    int _28783 = NOVALUE;
    int _28782 = NOVALUE;
    int _28781 = NOVALUE;
    int _28780 = NOVALUE;
    int _28779 = NOVALUE;
    int _28778 = NOVALUE;
    int _28777 = NOVALUE;
    int _28776 = NOVALUE;
    int _28775 = NOVALUE;
    int _28774 = NOVALUE;
    int _28773 = NOVALUE;
    int _28772 = NOVALUE;
    int _28771 = NOVALUE;
    int _28769 = NOVALUE;
    int _28768 = NOVALUE;
    int _28767 = NOVALUE;
    int _28766 = NOVALUE;
    int _28765 = NOVALUE;
    int _28764 = NOVALUE;
    int _28763 = NOVALUE;
    int _28762 = NOVALUE;
    int _28761 = NOVALUE;
    int _28759 = NOVALUE;
    int _28758 = NOVALUE;
    int _28757 = NOVALUE;
    int _28756 = NOVALUE;
    int _28755 = NOVALUE;
    int _28754 = NOVALUE;
    int _28753 = NOVALUE;
    int _28752 = NOVALUE;
    int _28751 = NOVALUE;
    int _28750 = NOVALUE;
    int _28749 = NOVALUE;
    int _28748 = NOVALUE;
    int _28747 = NOVALUE;
    int _28746 = NOVALUE;
    int _28745 = NOVALUE;
    int _28744 = NOVALUE;
    int _28743 = NOVALUE;
    int _28742 = NOVALUE;
    int _28741 = NOVALUE;
    int _28740 = NOVALUE;
    int _28739 = NOVALUE;
    int _28738 = NOVALUE;
    int _28736 = NOVALUE;
    int _28735 = NOVALUE;
    int _28734 = NOVALUE;
    int _28733 = NOVALUE;
    int _28732 = NOVALUE;
    int _28731 = NOVALUE;
    int _28730 = NOVALUE;
    int _28729 = NOVALUE;
    int _28728 = NOVALUE;
    int _28727 = NOVALUE;
    int _28726 = NOVALUE;
    int _28725 = NOVALUE;
    int _28723 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	if sym < 0 or (SymTab[sym][S_MODE] = M_NORMAL and*/
    _28723 = (_sym_55487 < 0);
    if (_28723 != 0) {
        goto L1; // [11] 90
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28725 = (int)*(((s1_ptr)_2)->base + _sym_55487);
    _2 = (int)SEQ_PTR(_28725);
    _28726 = (int)*(((s1_ptr)_2)->base + 3);
    _28725 = NOVALUE;
    if (IS_ATOM_INT(_28726)) {
        _28727 = (_28726 == 1);
    }
    else {
        _28727 = binary_op(EQUALS, _28726, 1);
    }
    _28726 = NOVALUE;
    if (IS_ATOM_INT(_28727)) {
        if (_28727 == 0) {
            _28728 = 0;
            goto L2; // [33] 59
        }
    }
    else {
        if (DBL_PTR(_28727)->dbl == 0.0) {
            _28728 = 0;
            goto L2; // [33] 59
        }
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28729 = (int)*(((s1_ptr)_2)->base + _sym_55487);
    _2 = (int)SEQ_PTR(_28729);
    _28730 = (int)*(((s1_ptr)_2)->base + 4);
    _28729 = NOVALUE;
    if (IS_ATOM_INT(_28730)) {
        _28731 = (_28730 != 2);
    }
    else {
        _28731 = binary_op(NOTEQ, _28730, 2);
    }
    _28730 = NOVALUE;
    DeRef(_28728);
    if (IS_ATOM_INT(_28731))
    _28728 = (_28731 != 0);
    else
    _28728 = DBL_PTR(_28731)->dbl != 0.0;
L2: 
    if (_28728 == 0) {
        DeRef(_28732);
        _28732 = 0;
        goto L3; // [59] 85
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28733 = (int)*(((s1_ptr)_2)->base + _sym_55487);
    _2 = (int)SEQ_PTR(_28733);
    _28734 = (int)*(((s1_ptr)_2)->base + 4);
    _28733 = NOVALUE;
    if (IS_ATOM_INT(_28734)) {
        _28735 = (_28734 != 4);
    }
    else {
        _28735 = binary_op(NOTEQ, _28734, 4);
    }
    _28734 = NOVALUE;
    if (IS_ATOM_INT(_28735))
    _28732 = (_28735 != 0);
    else
    _28732 = DBL_PTR(_28735)->dbl != 0.0;
L3: 
    if (_28732 == 0)
    {
        _28732 = NOVALUE;
        goto L4; // [86] 502
    }
    else{
        _28732 = NOVALUE;
    }
L1: 

    /** 		if sym < 0 or ((SymTab[sym][S_SCOPE] != SC_PRIVATE and*/
    _28736 = (_sym_55487 < 0);
    if (_28736 != 0) {
        goto L5; // [96] 213
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28738 = (int)*(((s1_ptr)_2)->base + _sym_55487);
    _2 = (int)SEQ_PTR(_28738);
    _28739 = (int)*(((s1_ptr)_2)->base + 4);
    _28738 = NOVALUE;
    if (IS_ATOM_INT(_28739)) {
        _28740 = (_28739 != 3);
    }
    else {
        _28740 = binary_op(NOTEQ, _28739, 3);
    }
    _28739 = NOVALUE;
    if (IS_ATOM_INT(_28740)) {
        if (_28740 == 0) {
            DeRef(_28741);
            _28741 = 0;
            goto L6; // [118] 144
        }
    }
    else {
        if (DBL_PTR(_28740)->dbl == 0.0) {
            DeRef(_28741);
            _28741 = 0;
            goto L6; // [118] 144
        }
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28742 = (int)*(((s1_ptr)_2)->base + _sym_55487);
    _2 = (int)SEQ_PTR(_28742);
    _28743 = (int)*(((s1_ptr)_2)->base + 1);
    _28742 = NOVALUE;
    if (_28743 == _25NOVALUE_12115)
    _28744 = 1;
    else if (IS_ATOM_INT(_28743) && IS_ATOM_INT(_25NOVALUE_12115))
    _28744 = 0;
    else
    _28744 = (compare(_28743, _25NOVALUE_12115) == 0);
    _28743 = NOVALUE;
    DeRef(_28741);
    _28741 = (_28744 != 0);
L6: 
    if (_28741 != 0) {
        DeRef(_28745);
        _28745 = 1;
        goto L7; // [144] 208
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28746 = (int)*(((s1_ptr)_2)->base + _sym_55487);
    _2 = (int)SEQ_PTR(_28746);
    _28747 = (int)*(((s1_ptr)_2)->base + 4);
    _28746 = NOVALUE;
    if (IS_ATOM_INT(_28747)) {
        _28748 = (_28747 == 3);
    }
    else {
        _28748 = binary_op(EQUALS, _28747, 3);
    }
    _28747 = NOVALUE;
    if (IS_ATOM_INT(_28748)) {
        if (_28748 == 0) {
            DeRef(_28749);
            _28749 = 0;
            goto L8; // [166] 204
        }
    }
    else {
        if (DBL_PTR(_28748)->dbl == 0.0) {
            DeRef(_28749);
            _28749 = 0;
            goto L8; // [166] 204
        }
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28750 = (int)*(((s1_ptr)_2)->base + _sym_55487);
    _2 = (int)SEQ_PTR(_28750);
    _28751 = (int)*(((s1_ptr)_2)->base + 16);
    _28750 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28752 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_28752);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _28753 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _28753 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    _28752 = NOVALUE;
    if (IS_ATOM_INT(_28751) && IS_ATOM_INT(_28753)) {
        _28754 = (_28751 >= _28753);
    }
    else {
        _28754 = binary_op(GREATEREQ, _28751, _28753);
    }
    _28751 = NOVALUE;
    _28753 = NOVALUE;
    DeRef(_28749);
    if (IS_ATOM_INT(_28754))
    _28749 = (_28754 != 0);
    else
    _28749 = DBL_PTR(_28754)->dbl != 0.0;
L8: 
    DeRef(_28745);
    _28745 = (_28749 != 0);
L7: 
    if (_28745 == 0)
    {
        _28745 = NOVALUE;
        goto L9; // [209] 566
    }
    else{
        _28745 = NOVALUE;
    }
L5: 

    /** 			if sym < 0 or (SymTab[sym][S_INITLEVEL] = -1)*/
    _28755 = (_sym_55487 < 0);
    if (_28755 != 0) {
        _28756 = 1;
        goto LA; // [219] 243
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28757 = (int)*(((s1_ptr)_2)->base + _sym_55487);
    _2 = (int)SEQ_PTR(_28757);
    _28758 = (int)*(((s1_ptr)_2)->base + 14);
    _28757 = NOVALUE;
    if (IS_ATOM_INT(_28758)) {
        _28759 = (_28758 == -1);
    }
    else {
        _28759 = binary_op(EQUALS, _28758, -1);
    }
    _28758 = NOVALUE;
    if (IS_ATOM_INT(_28759))
    _28756 = (_28759 != 0);
    else
    _28756 = DBL_PTR(_28759)->dbl != 0.0;
LA: 
    if (_28756 != 0) {
        goto LB; // [243] 270
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28761 = (int)*(((s1_ptr)_2)->base + _sym_55487);
    _2 = (int)SEQ_PTR(_28761);
    _28762 = (int)*(((s1_ptr)_2)->base + 4);
    _28761 = NOVALUE;
    if (IS_ATOM_INT(_28762)) {
        _28763 = (_28762 != 3);
    }
    else {
        _28763 = binary_op(NOTEQ, _28762, 3);
    }
    _28762 = NOVALUE;
    if (_28763 == 0) {
        DeRef(_28763);
        _28763 = NOVALUE;
        goto L9; // [266] 566
    }
    else {
        if (!IS_ATOM_INT(_28763) && DBL_PTR(_28763)->dbl == 0.0){
            DeRef(_28763);
            _28763 = NOVALUE;
            goto L9; // [266] 566
        }
        DeRef(_28763);
        _28763 = NOVALUE;
    }
    DeRef(_28763);
    _28763 = NOVALUE;
LB: 

    /** 				if ref then*/
    if (_ref_55488 == 0)
    {
        goto LC; // [272] 375
    }
    else{
    }

    /** 					if sym > 0 and (SymTab[sym][S_SCOPE] = SC_UNDEFINED) then*/
    _28764 = (_sym_55487 > 0);
    if (_28764 == 0) {
        goto LD; // [281] 317
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28766 = (int)*(((s1_ptr)_2)->base + _sym_55487);
    _2 = (int)SEQ_PTR(_28766);
    _28767 = (int)*(((s1_ptr)_2)->base + 4);
    _28766 = NOVALUE;
    if (IS_ATOM_INT(_28767)) {
        _28768 = (_28767 == 9);
    }
    else {
        _28768 = binary_op(EQUALS, _28767, 9);
    }
    _28767 = NOVALUE;
    if (_28768 == 0) {
        DeRef(_28768);
        _28768 = NOVALUE;
        goto LD; // [304] 317
    }
    else {
        if (!IS_ATOM_INT(_28768) && DBL_PTR(_28768)->dbl == 0.0){
            DeRef(_28768);
            _28768 = NOVALUE;
            goto LD; // [304] 317
        }
        DeRef(_28768);
        _28768 = NOVALUE;
    }
    DeRef(_28768);
    _28768 = NOVALUE;

    /** 						emit_op(PRIVATE_INIT_CHECK)*/
    _37emit_op(30);
    goto LE; // [314] 369
LD: 

    /** 					elsif sym < 0 or find(SymTab[sym][S_SCOPE], SCOPE_TYPES) then*/
    _28769 = (_sym_55487 < 0);
    if (_28769 != 0) {
        goto LF; // [323] 351
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28771 = (int)*(((s1_ptr)_2)->base + _sym_55487);
    _2 = (int)SEQ_PTR(_28771);
    _28772 = (int)*(((s1_ptr)_2)->base + 4);
    _28771 = NOVALUE;
    _28773 = find_from(_28772, _30SCOPE_TYPES_55162, 1);
    _28772 = NOVALUE;
    if (_28773 == 0)
    {
        _28773 = NOVALUE;
        goto L10; // [347] 361
    }
    else{
        _28773 = NOVALUE;
    }
LF: 

    /** 						emit_op(GLOBAL_INIT_CHECK) -- will become NOP2*/
    _37emit_op(109);
    goto LE; // [358] 369
L10: 

    /** 						emit_op(PRIVATE_INIT_CHECK)*/
    _37emit_op(30);
LE: 

    /** 					emit_addr(sym)*/
    _37emit_addr(_sym_55487);
LC: 

    /** 				if sym > 0 */
    _28774 = (_sym_55487 > 0);
    if (_28774 == 0) {
        _28775 = 0;
        goto L11; // [381] 411
    }
    _28776 = (_30short_circuit_55171 <= 0);
    if (_28776 != 0) {
        _28777 = 1;
        goto L12; // [391] 407
    }
    _28778 = (_30short_circuit_B_55173 == _5FALSE_242);
    _28777 = (_28778 != 0);
L12: 
    _28775 = (_28777 != 0);
L11: 
    if (_28775 == 0) {
        goto L9; // [411] 566
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28780 = (int)*(((s1_ptr)_2)->base + _sym_55487);
    _2 = (int)SEQ_PTR(_28780);
    _28781 = (int)*(((s1_ptr)_2)->base + 4);
    _28780 = NOVALUE;
    if (IS_ATOM_INT(_28781)) {
        _28782 = (_28781 != 3);
    }
    else {
        _28782 = binary_op(NOTEQ, _28781, 3);
    }
    _28781 = NOVALUE;
    if (IS_ATOM_INT(_28782)) {
        _28783 = (_28782 == 0);
    }
    else {
        _28783 = unary_op(NOT, _28782);
    }
    DeRef(_28782);
    _28782 = NOVALUE;
    if (_28783 == 0) {
        DeRef(_28783);
        _28783 = NOVALUE;
        goto L9; // [437] 566
    }
    else {
        if (!IS_ATOM_INT(_28783) && DBL_PTR(_28783)->dbl == 0.0){
            DeRef(_28783);
            _28783 = NOVALUE;
            goto L9; // [437] 566
        }
        DeRef(_28783);
        _28783 = NOVALUE;
    }
    DeRef(_28783);
    _28783 = NOVALUE;

    /** 					if CurrentSub != TopLevelSub */
    _28784 = (_25CurrentSub_12270 != _25TopLevelSub_12269);
    if (_28784 != 0) {
        goto L13; // [450] 470
    }
    if (IS_SEQUENCE(_26known_files_11139)){
            _28786 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _28786 = 1;
    }
    _28787 = (_25current_file_no_12262 == _28786);
    _28786 = NOVALUE;
    if (_28787 == 0)
    {
        DeRef(_28787);
        _28787 = NOVALUE;
        goto L9; // [466] 566
    }
    else{
        DeRef(_28787);
        _28787 = NOVALUE;
    }
L13: 

    /** 						init_stack = append(init_stack, sym)*/
    Append(&_30init_stack_55204, _30init_stack_55204, _sym_55487);

    /** 						SymTab[sym][S_INITLEVEL] = stmt_nest*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_55487 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 14);
    _1 = *(int *)_2;
    *(int *)_2 = _30stmt_nest_55203;
    DeRef(_1);
    _28789 = NOVALUE;
    goto L9; // [499] 566
L4: 

    /** 	elsif ref and sym > 0 and sym_mode( sym ) = M_CONSTANT and equal( NOVALUE, sym_obj( sym ) ) then*/
    if (_ref_55488 == 0) {
        _28791 = 0;
        goto L14; // [504] 516
    }
    _28792 = (_sym_55487 > 0);
    _28791 = (_28792 != 0);
L14: 
    if (_28791 == 0) {
        _28793 = 0;
        goto L15; // [516] 534
    }
    _28794 = _52sym_mode(_sym_55487);
    if (IS_ATOM_INT(_28794)) {
        _28795 = (_28794 == 2);
    }
    else {
        _28795 = binary_op(EQUALS, _28794, 2);
    }
    DeRef(_28794);
    _28794 = NOVALUE;
    if (IS_ATOM_INT(_28795))
    _28793 = (_28795 != 0);
    else
    _28793 = DBL_PTR(_28795)->dbl != 0.0;
L15: 
    if (_28793 == 0) {
        goto L16; // [534] 565
    }
    _28797 = _52sym_obj(_sym_55487);
    if (_25NOVALUE_12115 == _28797)
    _28798 = 1;
    else if (IS_ATOM_INT(_25NOVALUE_12115) && IS_ATOM_INT(_28797))
    _28798 = 0;
    else
    _28798 = (compare(_25NOVALUE_12115, _28797) == 0);
    DeRef(_28797);
    _28797 = NOVALUE;
    if (_28798 == 0)
    {
        _28798 = NOVALUE;
        goto L16; // [549] 565
    }
    else{
        _28798 = NOVALUE;
    }

    /** 		emit_op( GLOBAL_INIT_CHECK )*/
    _37emit_op(109);

    /** 		emit_addr(sym)*/
    _37emit_addr(_sym_55487);
L16: 
L9: 

    /** end procedure*/
    DeRef(_28723);
    _28723 = NOVALUE;
    DeRef(_28736);
    _28736 = NOVALUE;
    DeRef(_28727);
    _28727 = NOVALUE;
    DeRef(_28731);
    _28731 = NOVALUE;
    DeRef(_28735);
    _28735 = NOVALUE;
    DeRef(_28755);
    _28755 = NOVALUE;
    DeRef(_28740);
    _28740 = NOVALUE;
    DeRef(_28748);
    _28748 = NOVALUE;
    DeRef(_28764);
    _28764 = NOVALUE;
    DeRef(_28754);
    _28754 = NOVALUE;
    DeRef(_28759);
    _28759 = NOVALUE;
    DeRef(_28769);
    _28769 = NOVALUE;
    DeRef(_28774);
    _28774 = NOVALUE;
    DeRef(_28776);
    _28776 = NOVALUE;
    DeRef(_28778);
    _28778 = NOVALUE;
    DeRef(_28784);
    _28784 = NOVALUE;
    DeRef(_28792);
    _28792 = NOVALUE;
    DeRef(_28795);
    _28795 = NOVALUE;
    return;
    ;
}


void _30InitDelete()
{
    int _28811 = NOVALUE;
    int _28810 = NOVALUE;
    int _28808 = NOVALUE;
    int _28807 = NOVALUE;
    int _28806 = NOVALUE;
    int _28805 = NOVALUE;
    int _28804 = NOVALUE;
    int _28803 = NOVALUE;
    int _28802 = NOVALUE;
    int _28801 = NOVALUE;
    int _28800 = NOVALUE;
    int _28799 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	while length(init_stack) and*/
L1: 
    if (IS_SEQUENCE(_30init_stack_55204)){
            _28799 = SEQ_PTR(_30init_stack_55204)->length;
    }
    else {
        _28799 = 1;
    }
    if (_28799 == 0) {
        goto L2; // [11] 91
    }
    if (IS_SEQUENCE(_30init_stack_55204)){
            _28801 = SEQ_PTR(_30init_stack_55204)->length;
    }
    else {
        _28801 = 1;
    }
    _2 = (int)SEQ_PTR(_30init_stack_55204);
    _28802 = (int)*(((s1_ptr)_2)->base + _28801);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28803 = (int)*(((s1_ptr)_2)->base + _28802);
    _2 = (int)SEQ_PTR(_28803);
    _28804 = (int)*(((s1_ptr)_2)->base + 14);
    _28803 = NOVALUE;
    if (IS_ATOM_INT(_28804)) {
        _28805 = (_28804 > _30stmt_nest_55203);
    }
    else {
        _28805 = binary_op(GREATER, _28804, _30stmt_nest_55203);
    }
    _28804 = NOVALUE;
    if (_28805 <= 0) {
        if (_28805 == 0) {
            DeRef(_28805);
            _28805 = NOVALUE;
            goto L2; // [43] 91
        }
        else {
            if (!IS_ATOM_INT(_28805) && DBL_PTR(_28805)->dbl == 0.0){
                DeRef(_28805);
                _28805 = NOVALUE;
                goto L2; // [43] 91
            }
            DeRef(_28805);
            _28805 = NOVALUE;
        }
    }
    DeRef(_28805);
    _28805 = NOVALUE;

    /** 		SymTab[init_stack[$]][S_INITLEVEL] = -1*/
    if (IS_SEQUENCE(_30init_stack_55204)){
            _28806 = SEQ_PTR(_30init_stack_55204)->length;
    }
    else {
        _28806 = 1;
    }
    _2 = (int)SEQ_PTR(_30init_stack_55204);
    _28807 = (int)*(((s1_ptr)_2)->base + _28806);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_28807 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 14);
    _1 = *(int *)_2;
    *(int *)_2 = -1;
    DeRef(_1);
    _28808 = NOVALUE;

    /** 		init_stack = init_stack[1..$-1]*/
    if (IS_SEQUENCE(_30init_stack_55204)){
            _28810 = SEQ_PTR(_30init_stack_55204)->length;
    }
    else {
        _28810 = 1;
    }
    _28811 = _28810 - 1;
    _28810 = NOVALUE;
    rhs_slice_target = (object_ptr)&_30init_stack_55204;
    RHS_Slice(_30init_stack_55204, 1, _28811);

    /** 	end while*/
    goto L1; // [88] 6
L2: 

    /** end procedure*/
    _28802 = NOVALUE;
    _28807 = NOVALUE;
    DeRef(_28811);
    _28811 = NOVALUE;
    return;
    ;
}


void _30emit_forward_addr()
{
    int _28813 = NOVALUE;
    int _0, _1, _2;
    

    /** 	emit_addr(0)*/
    _37emit_addr(0);

    /** 	branch_list = append(branch_list, length(Code))*/
    if (IS_SEQUENCE(_25Code_12355)){
            _28813 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _28813 = 1;
    }
    Append(&_30branch_list_55169, _30branch_list_55169, _28813);
    _28813 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


void _30StraightenBranches()
{
    int _br_55661 = NOVALUE;
    int _target_55662 = NOVALUE;
    int _28834 = NOVALUE;
    int _28833 = NOVALUE;
    int _28832 = NOVALUE;
    int _28831 = NOVALUE;
    int _28829 = NOVALUE;
    int _28828 = NOVALUE;
    int _28827 = NOVALUE;
    int _28825 = NOVALUE;
    int _28824 = NOVALUE;
    int _28823 = NOVALUE;
    int _28822 = NOVALUE;
    int _28820 = NOVALUE;
    int _28817 = NOVALUE;
    int _28816 = NOVALUE;
    int _28815 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L1; // [5] 14
    }
    else{
    }

    /** 		return -- do it in back-end*/
    return;
L1: 

    /** 	for i = length(branch_list) to 1 by -1 do*/
    if (IS_SEQUENCE(_30branch_list_55169)){
            _28815 = SEQ_PTR(_30branch_list_55169)->length;
    }
    else {
        _28815 = 1;
    }
    {
        int _i_55666;
        _i_55666 = _28815;
L2: 
        if (_i_55666 < 1){
            goto L3; // [21] 170
        }

        /** 		if branch_list[i] > length(Code) then*/
        _2 = (int)SEQ_PTR(_30branch_list_55169);
        _28816 = (int)*(((s1_ptr)_2)->base + _i_55666);
        if (IS_SEQUENCE(_25Code_12355)){
                _28817 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _28817 = 1;
        }
        if (binary_op_a(LESSEQ, _28816, _28817)){
            _28816 = NOVALUE;
            _28817 = NOVALUE;
            goto L4; // [41] 53
        }
        _28816 = NOVALUE;
        _28817 = NOVALUE;

        /** 			CompileErr("wtf")*/
        RefDS(_28819);
        RefDS(_22682);
        _43CompileErr(_28819, _22682, 0);
L4: 

        /** 		target = Code[branch_list[i]]*/
        _2 = (int)SEQ_PTR(_30branch_list_55169);
        _28820 = (int)*(((s1_ptr)_2)->base + _i_55666);
        _2 = (int)SEQ_PTR(_25Code_12355);
        if (!IS_ATOM_INT(_28820)){
            _target_55662 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_28820)->dbl));
        }
        else{
            _target_55662 = (int)*(((s1_ptr)_2)->base + _28820);
        }
        if (!IS_ATOM_INT(_target_55662)){
            _target_55662 = (long)DBL_PTR(_target_55662)->dbl;
        }

        /** 		if target <= length(Code) and target > 0 then*/
        if (IS_SEQUENCE(_25Code_12355)){
                _28822 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _28822 = 1;
        }
        _28823 = (_target_55662 <= _28822);
        _28822 = NOVALUE;
        if (_28823 == 0) {
            goto L5; // [80] 163
        }
        _28825 = (_target_55662 > 0);
        if (_28825 == 0)
        {
            DeRef(_28825);
            _28825 = NOVALUE;
            goto L5; // [89] 163
        }
        else{
            DeRef(_28825);
            _28825 = NOVALUE;
        }

        /** 			br = Code[target]*/
        _2 = (int)SEQ_PTR(_25Code_12355);
        _br_55661 = (int)*(((s1_ptr)_2)->base + _target_55662);
        if (!IS_ATOM_INT(_br_55661)){
            _br_55661 = (long)DBL_PTR(_br_55661)->dbl;
        }

        /** 			if br = ELSE or br = ENDWHILE or br = EXIT then*/
        _28827 = (_br_55661 == 23);
        if (_28827 != 0) {
            _28828 = 1;
            goto L6; // [110] 124
        }
        _28829 = (_br_55661 == 22);
        _28828 = (_28829 != 0);
L6: 
        if (_28828 != 0) {
            goto L7; // [124] 139
        }
        _28831 = (_br_55661 == 61);
        if (_28831 == 0)
        {
            DeRef(_28831);
            _28831 = NOVALUE;
            goto L8; // [135] 162
        }
        else{
            DeRef(_28831);
            _28831 = NOVALUE;
        }
L7: 

        /** 				backpatch(branch_list[i], Code[target+1])*/
        _2 = (int)SEQ_PTR(_30branch_list_55169);
        _28832 = (int)*(((s1_ptr)_2)->base + _i_55666);
        _28833 = _target_55662 + 1;
        _2 = (int)SEQ_PTR(_25Code_12355);
        _28834 = (int)*(((s1_ptr)_2)->base + _28833);
        Ref(_28832);
        Ref(_28834);
        _37backpatch(_28832, _28834);
        _28832 = NOVALUE;
        _28834 = NOVALUE;
L8: 
L5: 

        /** 	end for*/
        _i_55666 = _i_55666 + -1;
        goto L2; // [165] 28
L3: 
        ;
    }

    /** 	branch_list = {}*/
    RefDS(_22682);
    DeRef(_30branch_list_55169);
    _30branch_list_55169 = _22682;

    /** end procedure*/
    _28820 = NOVALUE;
    DeRef(_28823);
    _28823 = NOVALUE;
    DeRef(_28827);
    _28827 = NOVALUE;
    DeRef(_28829);
    _28829 = NOVALUE;
    DeRef(_28833);
    _28833 = NOVALUE;
    return;
    ;
}


void _30PatchEList(int _base_55714)
{
    int _break_top_55715 = NOVALUE;
    int _n_55716 = NOVALUE;
    int _28851 = NOVALUE;
    int _28850 = NOVALUE;
    int _28849 = NOVALUE;
    int _28845 = NOVALUE;
    int _28844 = NOVALUE;
    int _28843 = NOVALUE;
    int _28841 = NOVALUE;
    int _28840 = NOVALUE;
    int _28838 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length(break_list) then*/
    if (IS_SEQUENCE(_30break_list_55189)){
            _28838 = SEQ_PTR(_30break_list_55189)->length;
    }
    else {
        _28838 = 1;
    }
    if (_28838 != 0)
    goto L1; // [10] 19
    _28838 = NOVALUE;

    /** 		return*/
    return;
L1: 

    /** 	break_top = 0*/
    _break_top_55715 = 0;

    /** 	for i=length(break_list) to base+1 by -1 do*/
    if (IS_SEQUENCE(_30break_list_55189)){
            _28840 = SEQ_PTR(_30break_list_55189)->length;
    }
    else {
        _28840 = 1;
    }
    _28841 = _base_55714 + 1;
    if (_28841 > MAXINT){
        _28841 = NewDouble((double)_28841);
    }
    {
        int _i_55721;
        _i_55721 = _28840;
L2: 
        if (binary_op_a(LESS, _i_55721, _28841)){
            goto L3; // [35] 129
        }

        /** 		n=break_delay[i]*/
        _2 = (int)SEQ_PTR(_30break_delay_55190);
        if (!IS_ATOM_INT(_i_55721)){
            _n_55716 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55721)->dbl));
        }
        else{
            _n_55716 = (int)*(((s1_ptr)_2)->base + _i_55721);
        }
        if (!IS_ATOM_INT(_n_55716))
        _n_55716 = (long)DBL_PTR(_n_55716)->dbl;

        /** 		break_delay[i] -= (n>0)*/
        _28843 = (_n_55716 > 0);
        _2 = (int)SEQ_PTR(_30break_delay_55190);
        if (!IS_ATOM_INT(_i_55721)){
            _28844 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55721)->dbl));
        }
        else{
            _28844 = (int)*(((s1_ptr)_2)->base + _i_55721);
        }
        if (IS_ATOM_INT(_28844)) {
            _28845 = _28844 - _28843;
            if ((long)((unsigned long)_28845 +(unsigned long) HIGH_BITS) >= 0){
                _28845 = NewDouble((double)_28845);
            }
        }
        else {
            _28845 = binary_op(MINUS, _28844, _28843);
        }
        _28844 = NOVALUE;
        _28843 = NOVALUE;
        _2 = (int)SEQ_PTR(_30break_delay_55190);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _30break_delay_55190 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_i_55721))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55721)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _i_55721);
        _1 = *(int *)_2;
        *(int *)_2 = _28845;
        if( _1 != _28845 ){
            DeRef(_1);
        }
        _28845 = NOVALUE;

        /** 		if n>1 then*/
        if (_n_55716 <= 1)
        goto L4; // [72] 93

        /** 			if break_top = 0 then*/
        if (_break_top_55715 != 0)
        goto L5; // [78] 122

        /** 				break_top = i*/
        Ref(_i_55721);
        _break_top_55715 = _i_55721;
        if (!IS_ATOM_INT(_break_top_55715)) {
            _1 = (long)(DBL_PTR(_break_top_55715)->dbl);
            if (UNIQUE(DBL_PTR(_break_top_55715)) && (DBL_PTR(_break_top_55715)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_break_top_55715);
            _break_top_55715 = _1;
        }
        goto L5; // [90] 122
L4: 

        /** 		elsif n=1 then*/
        if (_n_55716 != 1)
        goto L6; // [95] 121

        /** 			backpatch(break_list[i],length(Code)+1)*/
        _2 = (int)SEQ_PTR(_30break_list_55189);
        if (!IS_ATOM_INT(_i_55721)){
            _28849 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55721)->dbl));
        }
        else{
            _28849 = (int)*(((s1_ptr)_2)->base + _i_55721);
        }
        if (IS_SEQUENCE(_25Code_12355)){
                _28850 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _28850 = 1;
        }
        _28851 = _28850 + 1;
        _28850 = NOVALUE;
        _37backpatch(_28849, _28851);
        _28849 = NOVALUE;
        _28851 = NOVALUE;
L6: 
L5: 

        /** 	end for*/
        _0 = _i_55721;
        if (IS_ATOM_INT(_i_55721)) {
            _i_55721 = _i_55721 + -1;
            if ((long)((unsigned long)_i_55721 +(unsigned long) HIGH_BITS) >= 0){
                _i_55721 = NewDouble((double)_i_55721);
            }
        }
        else {
            _i_55721 = binary_op_a(PLUS, _i_55721, -1);
        }
        DeRef(_0);
        goto L2; // [124] 42
L3: 
        ;
        DeRef(_i_55721);
    }

    /** 	if break_top=0 then*/
    if (_break_top_55715 != 0)
    goto L7; // [131] 141

    /** 	    break_top=base*/
    _break_top_55715 = _base_55714;
L7: 

    /** 	break_delay = break_delay[1..break_top]*/
    rhs_slice_target = (object_ptr)&_30break_delay_55190;
    RHS_Slice(_30break_delay_55190, 1, _break_top_55715);

    /** 	break_list = break_list[1..break_top]*/
    rhs_slice_target = (object_ptr)&_30break_list_55189;
    RHS_Slice(_30break_list_55189, 1, _break_top_55715);

    /** end procedure*/
    DeRef(_28841);
    _28841 = NOVALUE;
    return;
    ;
}


void _30PatchNList(int _base_55745)
{
    int _next_top_55746 = NOVALUE;
    int _n_55747 = NOVALUE;
    int _28868 = NOVALUE;
    int _28867 = NOVALUE;
    int _28866 = NOVALUE;
    int _28862 = NOVALUE;
    int _28861 = NOVALUE;
    int _28860 = NOVALUE;
    int _28858 = NOVALUE;
    int _28857 = NOVALUE;
    int _28855 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length(continue_list) then*/
    if (IS_SEQUENCE(_30continue_list_55193)){
            _28855 = SEQ_PTR(_30continue_list_55193)->length;
    }
    else {
        _28855 = 1;
    }
    if (_28855 != 0)
    goto L1; // [10] 19
    _28855 = NOVALUE;

    /** 		return*/
    return;
L1: 

    /** 	next_top = 0*/
    _next_top_55746 = 0;

    /** 	for i=length(continue_list) to base+1 by -1 do*/
    if (IS_SEQUENCE(_30continue_list_55193)){
            _28857 = SEQ_PTR(_30continue_list_55193)->length;
    }
    else {
        _28857 = 1;
    }
    _28858 = _base_55745 + 1;
    if (_28858 > MAXINT){
        _28858 = NewDouble((double)_28858);
    }
    {
        int _i_55752;
        _i_55752 = _28857;
L2: 
        if (binary_op_a(LESS, _i_55752, _28858)){
            goto L3; // [35] 129
        }

        /** 		n=continue_delay[i]*/
        _2 = (int)SEQ_PTR(_30continue_delay_55194);
        if (!IS_ATOM_INT(_i_55752)){
            _n_55747 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55752)->dbl));
        }
        else{
            _n_55747 = (int)*(((s1_ptr)_2)->base + _i_55752);
        }
        if (!IS_ATOM_INT(_n_55747))
        _n_55747 = (long)DBL_PTR(_n_55747)->dbl;

        /** 		continue_delay[i] -= (n>0)*/
        _28860 = (_n_55747 > 0);
        _2 = (int)SEQ_PTR(_30continue_delay_55194);
        if (!IS_ATOM_INT(_i_55752)){
            _28861 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55752)->dbl));
        }
        else{
            _28861 = (int)*(((s1_ptr)_2)->base + _i_55752);
        }
        if (IS_ATOM_INT(_28861)) {
            _28862 = _28861 - _28860;
            if ((long)((unsigned long)_28862 +(unsigned long) HIGH_BITS) >= 0){
                _28862 = NewDouble((double)_28862);
            }
        }
        else {
            _28862 = binary_op(MINUS, _28861, _28860);
        }
        _28861 = NOVALUE;
        _28860 = NOVALUE;
        _2 = (int)SEQ_PTR(_30continue_delay_55194);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _30continue_delay_55194 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_i_55752))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55752)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _i_55752);
        _1 = *(int *)_2;
        *(int *)_2 = _28862;
        if( _1 != _28862 ){
            DeRef(_1);
        }
        _28862 = NOVALUE;

        /** 		if n>1 then*/
        if (_n_55747 <= 1)
        goto L4; // [72] 93

        /** 			if next_top = 0 then*/
        if (_next_top_55746 != 0)
        goto L5; // [78] 122

        /** 				next_top = i*/
        Ref(_i_55752);
        _next_top_55746 = _i_55752;
        if (!IS_ATOM_INT(_next_top_55746)) {
            _1 = (long)(DBL_PTR(_next_top_55746)->dbl);
            if (UNIQUE(DBL_PTR(_next_top_55746)) && (DBL_PTR(_next_top_55746)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_next_top_55746);
            _next_top_55746 = _1;
        }
        goto L5; // [90] 122
L4: 

        /** 		elsif n=1 then*/
        if (_n_55747 != 1)
        goto L6; // [95] 121

        /** 			backpatch(continue_list[i],length(Code)+1)*/
        _2 = (int)SEQ_PTR(_30continue_list_55193);
        if (!IS_ATOM_INT(_i_55752)){
            _28866 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55752)->dbl));
        }
        else{
            _28866 = (int)*(((s1_ptr)_2)->base + _i_55752);
        }
        if (IS_SEQUENCE(_25Code_12355)){
                _28867 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _28867 = 1;
        }
        _28868 = _28867 + 1;
        _28867 = NOVALUE;
        _37backpatch(_28866, _28868);
        _28866 = NOVALUE;
        _28868 = NOVALUE;
L6: 
L5: 

        /** 	end for*/
        _0 = _i_55752;
        if (IS_ATOM_INT(_i_55752)) {
            _i_55752 = _i_55752 + -1;
            if ((long)((unsigned long)_i_55752 +(unsigned long) HIGH_BITS) >= 0){
                _i_55752 = NewDouble((double)_i_55752);
            }
        }
        else {
            _i_55752 = binary_op_a(PLUS, _i_55752, -1);
        }
        DeRef(_0);
        goto L2; // [124] 42
L3: 
        ;
        DeRef(_i_55752);
    }

    /** 	if next_top=0 then*/
    if (_next_top_55746 != 0)
    goto L7; // [131] 141

    /** 	    next_top=base*/
    _next_top_55746 = _base_55745;
L7: 

    /** 	continue_delay =continue_delay[1..next_top]*/
    rhs_slice_target = (object_ptr)&_30continue_delay_55194;
    RHS_Slice(_30continue_delay_55194, 1, _next_top_55746);

    /** 	continue_list = continue_list[1..next_top]*/
    rhs_slice_target = (object_ptr)&_30continue_list_55193;
    RHS_Slice(_30continue_list_55193, 1, _next_top_55746);

    /** end procedure*/
    DeRef(_28858);
    _28858 = NOVALUE;
    return;
    ;
}


void _30PatchXList(int _base_55776)
{
    int _exit_top_55777 = NOVALUE;
    int _n_55778 = NOVALUE;
    int _28885 = NOVALUE;
    int _28884 = NOVALUE;
    int _28883 = NOVALUE;
    int _28879 = NOVALUE;
    int _28878 = NOVALUE;
    int _28877 = NOVALUE;
    int _28875 = NOVALUE;
    int _28874 = NOVALUE;
    int _28872 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length(exit_list) then*/
    if (IS_SEQUENCE(_30exit_list_55191)){
            _28872 = SEQ_PTR(_30exit_list_55191)->length;
    }
    else {
        _28872 = 1;
    }
    if (_28872 != 0)
    goto L1; // [10] 19
    _28872 = NOVALUE;

    /** 		return*/
    return;
L1: 

    /** 	exit_top = 0*/
    _exit_top_55777 = 0;

    /** 	for i=length(exit_list) to base+1 by -1 do*/
    if (IS_SEQUENCE(_30exit_list_55191)){
            _28874 = SEQ_PTR(_30exit_list_55191)->length;
    }
    else {
        _28874 = 1;
    }
    _28875 = _base_55776 + 1;
    if (_28875 > MAXINT){
        _28875 = NewDouble((double)_28875);
    }
    {
        int _i_55783;
        _i_55783 = _28874;
L2: 
        if (binary_op_a(LESS, _i_55783, _28875)){
            goto L3; // [35] 129
        }

        /** 		n=exit_delay[i]*/
        _2 = (int)SEQ_PTR(_30exit_delay_55192);
        if (!IS_ATOM_INT(_i_55783)){
            _n_55778 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55783)->dbl));
        }
        else{
            _n_55778 = (int)*(((s1_ptr)_2)->base + _i_55783);
        }
        if (!IS_ATOM_INT(_n_55778))
        _n_55778 = (long)DBL_PTR(_n_55778)->dbl;

        /** 		exit_delay[i] -= (n>0)*/
        _28877 = (_n_55778 > 0);
        _2 = (int)SEQ_PTR(_30exit_delay_55192);
        if (!IS_ATOM_INT(_i_55783)){
            _28878 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55783)->dbl));
        }
        else{
            _28878 = (int)*(((s1_ptr)_2)->base + _i_55783);
        }
        if (IS_ATOM_INT(_28878)) {
            _28879 = _28878 - _28877;
            if ((long)((unsigned long)_28879 +(unsigned long) HIGH_BITS) >= 0){
                _28879 = NewDouble((double)_28879);
            }
        }
        else {
            _28879 = binary_op(MINUS, _28878, _28877);
        }
        _28878 = NOVALUE;
        _28877 = NOVALUE;
        _2 = (int)SEQ_PTR(_30exit_delay_55192);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _30exit_delay_55192 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_i_55783))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55783)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _i_55783);
        _1 = *(int *)_2;
        *(int *)_2 = _28879;
        if( _1 != _28879 ){
            DeRef(_1);
        }
        _28879 = NOVALUE;

        /** 		if n>1 then*/
        if (_n_55778 <= 1)
        goto L4; // [72] 93

        /** 			if exit_top = 0 then*/
        if (_exit_top_55777 != 0)
        goto L5; // [78] 122

        /** 				exit_top = i*/
        Ref(_i_55783);
        _exit_top_55777 = _i_55783;
        if (!IS_ATOM_INT(_exit_top_55777)) {
            _1 = (long)(DBL_PTR(_exit_top_55777)->dbl);
            if (UNIQUE(DBL_PTR(_exit_top_55777)) && (DBL_PTR(_exit_top_55777)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_exit_top_55777);
            _exit_top_55777 = _1;
        }
        goto L5; // [90] 122
L4: 

        /** 		elsif n=1 then*/
        if (_n_55778 != 1)
        goto L6; // [95] 121

        /** 			backpatch(exit_list[i],length(Code)+1)*/
        _2 = (int)SEQ_PTR(_30exit_list_55191);
        if (!IS_ATOM_INT(_i_55783)){
            _28883 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_55783)->dbl));
        }
        else{
            _28883 = (int)*(((s1_ptr)_2)->base + _i_55783);
        }
        if (IS_SEQUENCE(_25Code_12355)){
                _28884 = SEQ_PTR(_25Code_12355)->length;
        }
        else {
            _28884 = 1;
        }
        _28885 = _28884 + 1;
        _28884 = NOVALUE;
        _37backpatch(_28883, _28885);
        _28883 = NOVALUE;
        _28885 = NOVALUE;
L6: 
L5: 

        /** 	end for*/
        _0 = _i_55783;
        if (IS_ATOM_INT(_i_55783)) {
            _i_55783 = _i_55783 + -1;
            if ((long)((unsigned long)_i_55783 +(unsigned long) HIGH_BITS) >= 0){
                _i_55783 = NewDouble((double)_i_55783);
            }
        }
        else {
            _i_55783 = binary_op_a(PLUS, _i_55783, -1);
        }
        DeRef(_0);
        goto L2; // [124] 42
L3: 
        ;
        DeRef(_i_55783);
    }

    /** 	if exit_top=0 then*/
    if (_exit_top_55777 != 0)
    goto L7; // [131] 141

    /** 	    exit_top=base*/
    _exit_top_55777 = _base_55776;
L7: 

    /** 	exit_delay = exit_delay [1..exit_top]*/
    rhs_slice_target = (object_ptr)&_30exit_delay_55192;
    RHS_Slice(_30exit_delay_55192, 1, _exit_top_55777);

    /** 	exit_list = exit_list [1..exit_top]*/
    rhs_slice_target = (object_ptr)&_30exit_list_55191;
    RHS_Slice(_30exit_list_55191, 1, _exit_top_55777);

    /** end procedure*/
    DeRef(_28875);
    _28875 = NOVALUE;
    return;
    ;
}


void _30putback(int _t_55808)
{
    int _28890 = NOVALUE;
    int _0, _1, _2;
    

    /** 	backed_up_tok = append(backed_up_tok, t)*/
    Ref(_t_55808);
    Append(&_30backed_up_tok_55178, _30backed_up_tok_55178, _t_55808);

    /** 	if t[T_SYM] then*/
    _2 = (int)SEQ_PTR(_t_55808);
    _28890 = (int)*(((s1_ptr)_2)->base + 2);
    if (_28890 == 0) {
        _28890 = NOVALUE;
        goto L1; // [17] 79
    }
    else {
        if (!IS_ATOM_INT(_28890) && DBL_PTR(_28890)->dbl == 0.0){
            _28890 = NOVALUE;
            goto L1; // [17] 79
        }
        _28890 = NOVALUE;
    }
    _28890 = NOVALUE;

    /** 		putback_ForwardLine     = ForwardLine*/
    Ref(_43ForwardLine_49533);
    DeRef(_43putback_ForwardLine_49534);
    _43putback_ForwardLine_49534 = _43ForwardLine_49533;

    /** 		putback_forward_bp      = forward_bp*/
    _43putback_forward_bp_49538 = _43forward_bp_49537;

    /** 		putback_fwd_line_number = fwd_line_number*/
    _25putback_fwd_line_number_12265 = _25fwd_line_number_12264;

    /** 		if last_fwd_line_number then*/
    if (_25last_fwd_line_number_12266 == 0)
    {
        goto L2; // [49] 78
    }
    else{
    }

    /** 			ForwardLine     = last_ForwardLine*/
    Ref(_43last_ForwardLine_49535);
    DeRef(_43ForwardLine_49533);
    _43ForwardLine_49533 = _43last_ForwardLine_49535;

    /** 			forward_bp      = last_forward_bp*/
    _43forward_bp_49537 = _43last_forward_bp_49539;

    /** 			fwd_line_number = last_fwd_line_number*/
    _25fwd_line_number_12264 = _25last_fwd_line_number_12266;
L2: 
L1: 

    /** end procedure*/
    DeRef(_t_55808);
    return;
    ;
}


void _30start_recording()
{
    int _0, _1, _2;
    

    /** 	psm_stack &= Parser_mode*/
    Append(&_30psm_stack_55827, _30psm_stack_55827, _25Parser_mode_12388);

    /** 	can_stack = append(can_stack,canned_tokens)*/
    Ref(_30canned_tokens_55215);
    Append(&_30can_stack_55828, _30can_stack_55828, _30canned_tokens_55215);

    /** 	idx_stack &= canned_index*/
    Append(&_30idx_stack_55829, _30idx_stack_55829, _30canned_index_55216);

    /** 	tok_stack = append(tok_stack,backed_up_tok)*/
    RefDS(_30backed_up_tok_55178);
    Append(&_30tok_stack_55830, _30tok_stack_55830, _30backed_up_tok_55178);

    /** 	canned_tokens = {}*/
    RefDS(_22682);
    DeRef(_30canned_tokens_55215);
    _30canned_tokens_55215 = _22682;

    /** 	Parser_mode = PAM_RECORD*/
    _25Parser_mode_12388 = 1;

    /** 	clear_last()*/
    _37clear_last();

    /** end procedure*/
    return;
    ;
}


int _30restore_parser()
{
    int _n_55843 = NOVALUE;
    int _tok_55844 = NOVALUE;
    int _x_55845 = NOVALUE;
    int _28921 = NOVALUE;
    int _28920 = NOVALUE;
    int _28919 = NOVALUE;
    int _28917 = NOVALUE;
    int _28913 = NOVALUE;
    int _28912 = NOVALUE;
    int _28910 = NOVALUE;
    int _28908 = NOVALUE;
    int _28907 = NOVALUE;
    int _28905 = NOVALUE;
    int _28903 = NOVALUE;
    int _28902 = NOVALUE;
    int _28900 = NOVALUE;
    int _28898 = NOVALUE;
    int _28897 = NOVALUE;
    int _28895 = NOVALUE;
    int _0, _1, _2;
    

    /** 	n=Parser_mode*/
    _n_55843 = _25Parser_mode_12388;

    /** 	x = canned_tokens*/
    Ref(_30canned_tokens_55215);
    DeRef(_x_55845);
    _x_55845 = _30canned_tokens_55215;

    /** 	canned_tokens = can_stack[$]*/
    if (IS_SEQUENCE(_30can_stack_55828)){
            _28895 = SEQ_PTR(_30can_stack_55828)->length;
    }
    else {
        _28895 = 1;
    }
    DeRef(_30canned_tokens_55215);
    _2 = (int)SEQ_PTR(_30can_stack_55828);
    _30canned_tokens_55215 = (int)*(((s1_ptr)_2)->base + _28895);
    Ref(_30canned_tokens_55215);

    /** 	can_stack     = can_stack[1..$-1]*/
    if (IS_SEQUENCE(_30can_stack_55828)){
            _28897 = SEQ_PTR(_30can_stack_55828)->length;
    }
    else {
        _28897 = 1;
    }
    _28898 = _28897 - 1;
    _28897 = NOVALUE;
    rhs_slice_target = (object_ptr)&_30can_stack_55828;
    RHS_Slice(_30can_stack_55828, 1, _28898);

    /** 	canned_index  = idx_stack[$]*/
    if (IS_SEQUENCE(_30idx_stack_55829)){
            _28900 = SEQ_PTR(_30idx_stack_55829)->length;
    }
    else {
        _28900 = 1;
    }
    _2 = (int)SEQ_PTR(_30idx_stack_55829);
    _30canned_index_55216 = (int)*(((s1_ptr)_2)->base + _28900);

    /** 	idx_stack     = idx_stack[1..$-1]*/
    if (IS_SEQUENCE(_30idx_stack_55829)){
            _28902 = SEQ_PTR(_30idx_stack_55829)->length;
    }
    else {
        _28902 = 1;
    }
    _28903 = _28902 - 1;
    _28902 = NOVALUE;
    rhs_slice_target = (object_ptr)&_30idx_stack_55829;
    RHS_Slice(_30idx_stack_55829, 1, _28903);

    /** 	Parser_mode   = psm_stack[$]*/
    if (IS_SEQUENCE(_30psm_stack_55827)){
            _28905 = SEQ_PTR(_30psm_stack_55827)->length;
    }
    else {
        _28905 = 1;
    }
    _2 = (int)SEQ_PTR(_30psm_stack_55827);
    _25Parser_mode_12388 = (int)*(((s1_ptr)_2)->base + _28905);

    /** 	psm_stack     = psm_stack[1..$-1]*/
    if (IS_SEQUENCE(_30psm_stack_55827)){
            _28907 = SEQ_PTR(_30psm_stack_55827)->length;
    }
    else {
        _28907 = 1;
    }
    _28908 = _28907 - 1;
    _28907 = NOVALUE;
    rhs_slice_target = (object_ptr)&_30psm_stack_55827;
    RHS_Slice(_30psm_stack_55827, 1, _28908);

    /** 	tok 		  = tok_stack[$]*/
    if (IS_SEQUENCE(_30tok_stack_55830)){
            _28910 = SEQ_PTR(_30tok_stack_55830)->length;
    }
    else {
        _28910 = 1;
    }
    DeRef(_tok_55844);
    _2 = (int)SEQ_PTR(_30tok_stack_55830);
    _tok_55844 = (int)*(((s1_ptr)_2)->base + _28910);
    RefDS(_tok_55844);

    /** 	tok_stack 	  = tok_stack[1..$-1]*/
    if (IS_SEQUENCE(_30tok_stack_55830)){
            _28912 = SEQ_PTR(_30tok_stack_55830)->length;
    }
    else {
        _28912 = 1;
    }
    _28913 = _28912 - 1;
    _28912 = NOVALUE;
    rhs_slice_target = (object_ptr)&_30tok_stack_55830;
    RHS_Slice(_30tok_stack_55830, 1, _28913);

    /** 	clear_last()*/
    _37clear_last();

    /** 	if n=PAM_PLAYBACK then*/
    if (_n_55843 != -1)
    goto L1; // [137] 150

    /** 		return {}*/
    RefDS(_22682);
    DeRefDS(_tok_55844);
    DeRefDS(_x_55845);
    _28898 = NOVALUE;
    _28903 = NOVALUE;
    _28908 = NOVALUE;
    _28913 = NOVALUE;
    return _22682;
    goto L2; // [147] 167
L1: 

    /** 	elsif n = PAM_NORMAL then*/
    if (_n_55843 != 0)
    goto L3; // [154] 166

    /** 		use_private_list = 0*/
    _25use_private_list_12396 = 0;
L3: 
L2: 

    /** 	if length(backed_up_tok) > 0 then*/
    if (IS_SEQUENCE(_30backed_up_tok_55178)){
            _28917 = SEQ_PTR(_30backed_up_tok_55178)->length;
    }
    else {
        _28917 = 1;
    }
    if (_28917 <= 0)
    goto L4; // [174] 199

    /** 		return x[1..$-1]*/
    if (IS_SEQUENCE(_x_55845)){
            _28919 = SEQ_PTR(_x_55845)->length;
    }
    else {
        _28919 = 1;
    }
    _28920 = _28919 - 1;
    _28919 = NOVALUE;
    rhs_slice_target = (object_ptr)&_28921;
    RHS_Slice(_x_55845, 1, _28920);
    DeRef(_tok_55844);
    DeRefDS(_x_55845);
    DeRef(_28898);
    _28898 = NOVALUE;
    DeRef(_28903);
    _28903 = NOVALUE;
    DeRef(_28908);
    _28908 = NOVALUE;
    DeRef(_28913);
    _28913 = NOVALUE;
    _28920 = NOVALUE;
    return _28921;
    goto L5; // [196] 206
L4: 

    /** 		return x*/
    DeRef(_tok_55844);
    DeRef(_28898);
    _28898 = NOVALUE;
    DeRef(_28903);
    _28903 = NOVALUE;
    DeRef(_28908);
    _28908 = NOVALUE;
    DeRef(_28913);
    _28913 = NOVALUE;
    DeRef(_28920);
    _28920 = NOVALUE;
    DeRef(_28921);
    _28921 = NOVALUE;
    return _x_55845;
L5: 
    ;
}


void _30start_playback(int _s_55885)
{
    int _0, _1, _2;
    

    /** 	psm_stack &= Parser_mode*/
    Append(&_30psm_stack_55827, _30psm_stack_55827, _25Parser_mode_12388);

    /** 	can_stack = append(can_stack,canned_tokens)*/
    Ref(_30canned_tokens_55215);
    Append(&_30can_stack_55828, _30can_stack_55828, _30canned_tokens_55215);

    /** 	idx_stack &= canned_index*/
    Append(&_30idx_stack_55829, _30idx_stack_55829, _30canned_index_55216);

    /** 	tok_stack = append(tok_stack,backed_up_tok)*/
    RefDS(_30backed_up_tok_55178);
    Append(&_30tok_stack_55830, _30tok_stack_55830, _30backed_up_tok_55178);

    /** 	canned_index = 1*/
    _30canned_index_55216 = 1;

    /** 	canned_tokens = s*/
    RefDS(_s_55885);
    DeRef(_30canned_tokens_55215);
    _30canned_tokens_55215 = _s_55885;

    /** 	backed_up_tok = {}*/
    RefDS(_22682);
    DeRefDS(_30backed_up_tok_55178);
    _30backed_up_tok_55178 = _22682;

    /** 	Parser_mode = PAM_PLAYBACK*/
    _25Parser_mode_12388 = -1;

    /** end procedure*/
    DeRefDS(_s_55885);
    return;
    ;
}


void _30restore_parseargs_states()
{
    int _s_55907 = NOVALUE;
    int _n_55908 = NOVALUE;
    int _28941 = NOVALUE;
    int _28940 = NOVALUE;
    int _28932 = NOVALUE;
    int _28931 = NOVALUE;
    int _28929 = NOVALUE;
    int _0, _1, _2;
    

    /** 	s = parseargs_states[$]*/
    if (IS_SEQUENCE(_30parseargs_states_55893)){
            _28929 = SEQ_PTR(_30parseargs_states_55893)->length;
    }
    else {
        _28929 = 1;
    }
    DeRef(_s_55907);
    _2 = (int)SEQ_PTR(_30parseargs_states_55893);
    _s_55907 = (int)*(((s1_ptr)_2)->base + _28929);
    RefDS(_s_55907);

    /** 	parseargs_states = parseargs_states[1..$-1]*/
    if (IS_SEQUENCE(_30parseargs_states_55893)){
            _28931 = SEQ_PTR(_30parseargs_states_55893)->length;
    }
    else {
        _28931 = 1;
    }
    _28932 = _28931 - 1;
    _28931 = NOVALUE;
    rhs_slice_target = (object_ptr)&_30parseargs_states_55893;
    RHS_Slice(_30parseargs_states_55893, 1, _28932);

    /** 	n=s[PS_POSITION]*/
    _2 = (int)SEQ_PTR(_s_55907);
    _n_55908 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_n_55908))
    _n_55908 = (long)DBL_PTR(_n_55908)->dbl;

    /** 	private_list = private_list[1..n]*/
    rhs_slice_target = (object_ptr)&_30private_list_55901;
    RHS_Slice(_30private_list_55901, 1, _n_55908);

    /** 	private_sym = private_sym[1..n]*/
    rhs_slice_target = (object_ptr)&_25private_sym_12395;
    RHS_Slice(_25private_sym_12395, 1, _n_55908);

    /** 	lock_scanner = s[PS_SCAN_LOCK]*/
    _2 = (int)SEQ_PTR(_s_55907);
    _30lock_scanner_55902 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_30lock_scanner_55902))
    _30lock_scanner_55902 = (long)DBL_PTR(_30lock_scanner_55902)->dbl;

    /** 	use_private_list = s[PS_USE_LIST]*/
    _2 = (int)SEQ_PTR(_s_55907);
    _25use_private_list_12396 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_25use_private_list_12396)){
        _25use_private_list_12396 = (long)DBL_PTR(_25use_private_list_12396)->dbl;
    }

    /** 	on_arg = s[PS_ON_ARG]*/
    _2 = (int)SEQ_PTR(_s_55907);
    _30on_arg_55903 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_30on_arg_55903))
    _30on_arg_55903 = (long)DBL_PTR(_30on_arg_55903)->dbl;

    /** 	nested_calls = nested_calls[1..$-1]*/
    if (IS_SEQUENCE(_30nested_calls_55904)){
            _28940 = SEQ_PTR(_30nested_calls_55904)->length;
    }
    else {
        _28940 = 1;
    }
    _28941 = _28940 - 1;
    _28940 = NOVALUE;
    rhs_slice_target = (object_ptr)&_30nested_calls_55904;
    RHS_Slice(_30nested_calls_55904, 1, _28941);

    /** end procedure*/
    DeRefDS(_s_55907);
    _28932 = NOVALUE;
    _28941 = NOVALUE;
    return;
    ;
}


int _30read_recorded_token(int _n_55928)
{
    int _t_55930 = NOVALUE;
    int _p_55931 = NOVALUE;
    int _prev_Nne_55932 = NOVALUE;
    int _ts_55961 = NOVALUE;
    int _32376 = NOVALUE;
    int _32375 = NOVALUE;
    int _32374 = NOVALUE;
    int _32373 = NOVALUE;
    int _32372 = NOVALUE;
    int _32371 = NOVALUE;
    int _32370 = NOVALUE;
    int _32369 = NOVALUE;
    int _29013 = NOVALUE;
    int _29012 = NOVALUE;
    int _29011 = NOVALUE;
    int _29010 = NOVALUE;
    int _29006 = NOVALUE;
    int _29004 = NOVALUE;
    int _29003 = NOVALUE;
    int _29002 = NOVALUE;
    int _29001 = NOVALUE;
    int _28999 = NOVALUE;
    int _28998 = NOVALUE;
    int _28997 = NOVALUE;
    int _28996 = NOVALUE;
    int _28994 = NOVALUE;
    int _28991 = NOVALUE;
    int _28989 = NOVALUE;
    int _28987 = NOVALUE;
    int _28986 = NOVALUE;
    int _28985 = NOVALUE;
    int _28984 = NOVALUE;
    int _28982 = NOVALUE;
    int _28980 = NOVALUE;
    int _28976 = NOVALUE;
    int _28974 = NOVALUE;
    int _28972 = NOVALUE;
    int _28971 = NOVALUE;
    int _28970 = NOVALUE;
    int _28969 = NOVALUE;
    int _28968 = NOVALUE;
    int _28967 = NOVALUE;
    int _28966 = NOVALUE;
    int _28965 = NOVALUE;
    int _28964 = NOVALUE;
    int _28963 = NOVALUE;
    int _28962 = NOVALUE;
    int _28961 = NOVALUE;
    int _28959 = NOVALUE;
    int _28958 = NOVALUE;
    int _28956 = NOVALUE;
    int _28955 = NOVALUE;
    int _28954 = NOVALUE;
    int _28953 = NOVALUE;
    int _28952 = NOVALUE;
    int _28951 = NOVALUE;
    int _28950 = NOVALUE;
    int _28949 = NOVALUE;
    int _28946 = NOVALUE;
    int _28944 = NOVALUE;
    int _28943 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_n_55928)) {
        _1 = (long)(DBL_PTR(_n_55928)->dbl);
        if (UNIQUE(DBL_PTR(_n_55928)) && (DBL_PTR(_n_55928)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_n_55928);
        _n_55928 = _1;
    }

    /** 	integer p, prev_Nne*/

    /** 	if atom(Ns_recorded[n]) label "top if" then*/
    _2 = (int)SEQ_PTR(_25Ns_recorded_12390);
    _28943 = (int)*(((s1_ptr)_2)->base + _n_55928);
    _28944 = IS_ATOM(_28943);
    _28943 = NOVALUE;
    if (_28944 == 0)
    {
        _28944 = NOVALUE;
        goto L1; // [16] 403
    }
    else{
        _28944 = NOVALUE;
    }

    /** 		if use_private_list then*/
    if (_25use_private_list_12396 == 0)
    {
        goto L2; // [23] 171
    }
    else{
    }

    /** 			p = find( Recorded[n], private_list)*/
    _2 = (int)SEQ_PTR(_25Recorded_12389);
    _28946 = (int)*(((s1_ptr)_2)->base + _n_55928);
    _p_55931 = find_from(_28946, _30private_list_55901, 1);
    _28946 = NOVALUE;

    /** 			if p > 0 then -- the value of this parameter is known, use it*/
    if (_p_55931 <= 0)
    goto L3; // [43] 170

    /** 				if TRANSLATE*/
    if (_25TRANSLATE_11874 == 0) {
        goto L4; // [51] 150
    }
    _2 = (int)SEQ_PTR(_25private_sym_12395);
    _28950 = (int)*(((s1_ptr)_2)->base + _p_55931);
    if (IS_ATOM_INT(_28950)) {
        _28951 = (_28950 < 0);
    }
    else {
        _28951 = binary_op(LESS, _28950, 0);
    }
    _28950 = NOVALUE;
    if (IS_ATOM_INT(_28951)) {
        if (_28951 != 0) {
            _28952 = 1;
            goto L5; // [65] 97
        }
    }
    else {
        if (DBL_PTR(_28951)->dbl != 0.0) {
            _28952 = 1;
            goto L5; // [65] 97
        }
    }
    _2 = (int)SEQ_PTR(_25private_sym_12395);
    _28953 = (int)*(((s1_ptr)_2)->base + _p_55931);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_28953)){
        _28954 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_28953)->dbl));
    }
    else{
        _28954 = (int)*(((s1_ptr)_2)->base + _28953);
    }
    _2 = (int)SEQ_PTR(_28954);
    _28955 = (int)*(((s1_ptr)_2)->base + 3);
    _28954 = NOVALUE;
    if (IS_ATOM_INT(_28955)) {
        _28956 = (_28955 == 3);
    }
    else {
        _28956 = binary_op(EQUALS, _28955, 3);
    }
    _28955 = NOVALUE;
    DeRef(_28952);
    if (IS_ATOM_INT(_28956))
    _28952 = (_28956 != 0);
    else
    _28952 = DBL_PTR(_28956)->dbl != 0.0;
L5: 
    if (_28952 == 0)
    {
        _28952 = NOVALUE;
        goto L4; // [98] 150
    }
    else{
        _28952 = NOVALUE;
    }

    /** 					symtab_index ts = NewTempSym()*/
    _ts_55961 = _52NewTempSym(0);
    if (!IS_ATOM_INT(_ts_55961)) {
        _1 = (long)(DBL_PTR(_ts_55961)->dbl);
        if (UNIQUE(DBL_PTR(_ts_55961)) && (DBL_PTR(_ts_55961)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ts_55961);
        _ts_55961 = _1;
    }

    /** 					Code &= { ASSIGN, private_sym[p], ts }*/
    _2 = (int)SEQ_PTR(_25private_sym_12395);
    _28958 = (int)*(((s1_ptr)_2)->base + _p_55931);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 18;
    Ref(_28958);
    *((int *)(_2+8)) = _28958;
    *((int *)(_2+12)) = _ts_55961;
    _28959 = MAKE_SEQ(_1);
    _28958 = NOVALUE;
    Concat((object_ptr)&_25Code_12355, _25Code_12355, _28959);
    DeRefDS(_28959);
    _28959 = NOVALUE;

    /** 					return {VARIABLE, ts}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -100;
    ((int *)_2)[2] = _ts_55961;
    _28961 = MAKE_SEQ(_1);
    DeRef(_t_55930);
    _28953 = NOVALUE;
    DeRef(_28951);
    _28951 = NOVALUE;
    DeRef(_28956);
    _28956 = NOVALUE;
    return _28961;
    goto L6; // [147] 169
L4: 

    /** 					return {VARIABLE, private_sym[p]}*/
    _2 = (int)SEQ_PTR(_25private_sym_12395);
    _28962 = (int)*(((s1_ptr)_2)->base + _p_55931);
    Ref(_28962);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -100;
    ((int *)_2)[2] = _28962;
    _28963 = MAKE_SEQ(_1);
    _28962 = NOVALUE;
    DeRef(_t_55930);
    _28953 = NOVALUE;
    DeRef(_28951);
    _28951 = NOVALUE;
    DeRef(_28961);
    _28961 = NOVALUE;
    DeRef(_28956);
    _28956 = NOVALUE;
    return _28963;
L6: 
L3: 
L2: 

    /** 		prev_Nne = No_new_entry*/
    _prev_Nne_55932 = _52No_new_entry_48273;

    /** 		No_new_entry = 1*/
    _52No_new_entry_48273 = 1;

    /** 		if Recorded_sym[n] > 0 and  sym_scope( Recorded_sym[n] ) != SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_25Recorded_sym_12391);
    _28964 = (int)*(((s1_ptr)_2)->base + _n_55928);
    if (IS_ATOM_INT(_28964)) {
        _28965 = (_28964 > 0);
    }
    else {
        _28965 = binary_op(GREATER, _28964, 0);
    }
    _28964 = NOVALUE;
    if (IS_ATOM_INT(_28965)) {
        if (_28965 == 0) {
            goto L7; // [199] 250
        }
    }
    else {
        if (DBL_PTR(_28965)->dbl == 0.0) {
            goto L7; // [199] 250
        }
    }
    _2 = (int)SEQ_PTR(_25Recorded_sym_12391);
    _28967 = (int)*(((s1_ptr)_2)->base + _n_55928);
    Ref(_28967);
    _28968 = _52sym_scope(_28967);
    _28967 = NOVALUE;
    if (IS_ATOM_INT(_28968)) {
        _28969 = (_28968 != 9);
    }
    else {
        _28969 = binary_op(NOTEQ, _28968, 9);
    }
    DeRef(_28968);
    _28968 = NOVALUE;
    if (_28969 == 0) {
        DeRef(_28969);
        _28969 = NOVALUE;
        goto L7; // [220] 250
    }
    else {
        if (!IS_ATOM_INT(_28969) && DBL_PTR(_28969)->dbl == 0.0){
            DeRef(_28969);
            _28969 = NOVALUE;
            goto L7; // [220] 250
        }
        DeRef(_28969);
        _28969 = NOVALUE;
    }
    DeRef(_28969);
    _28969 = NOVALUE;

    /** 			t = { sym_token( Recorded_sym[n] ), Recorded_sym[n] }*/
    _2 = (int)SEQ_PTR(_25Recorded_sym_12391);
    _28970 = (int)*(((s1_ptr)_2)->base + _n_55928);
    Ref(_28970);
    _28971 = _52sym_token(_28970);
    _28970 = NOVALUE;
    _2 = (int)SEQ_PTR(_25Recorded_sym_12391);
    _28972 = (int)*(((s1_ptr)_2)->base + _n_55928);
    Ref(_28972);
    DeRef(_t_55930);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _28971;
    ((int *)_2)[2] = _28972;
    _t_55930 = MAKE_SEQ(_1);
    _28972 = NOVALUE;
    _28971 = NOVALUE;

    /** 			break "top if"*/
    goto L8; // [247] 728
L7: 

    /** 		t = keyfind(Recorded[n],-1)*/
    _2 = (int)SEQ_PTR(_25Recorded_12389);
    _28974 = (int)*(((s1_ptr)_2)->base + _n_55928);
    RefDS(_28974);
    DeRef(_32375);
    _32375 = _28974;
    _32376 = _52hashfn(_32375);
    _32375 = NOVALUE;
    RefDS(_28974);
    _0 = _t_55930;
    _t_55930 = _52keyfind(_28974, -1, _25current_file_no_12262, 0, _32376);
    DeRef(_0);
    _28974 = NOVALUE;
    _32376 = NOVALUE;

    /** 		if t[T_ID] = IGNORED then*/
    _2 = (int)SEQ_PTR(_t_55930);
    _28976 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _28976, 509)){
        _28976 = NOVALUE;
        goto L8; // [286] 728
    }
    _28976 = NOVALUE;

    /** 	        p = Recorded_sym[n]*/
    _2 = (int)SEQ_PTR(_25Recorded_sym_12391);
    _p_55931 = (int)*(((s1_ptr)_2)->base + _n_55928);
    if (!IS_ATOM_INT(_p_55931)){
        _p_55931 = (long)DBL_PTR(_p_55931)->dbl;
    }

    /** 	        if p = 0 then*/
    if (_p_55931 != 0)
    goto L9; // [302] 380

    /** 				No_new_entry = 0*/
    _52No_new_entry_48273 = 0;

    /** 				t = keyfind( Recorded[n], -1 )*/
    _2 = (int)SEQ_PTR(_25Recorded_12389);
    _28980 = (int)*(((s1_ptr)_2)->base + _n_55928);
    RefDS(_28980);
    DeRef(_32373);
    _32373 = _28980;
    _32374 = _52hashfn(_32373);
    _32373 = NOVALUE;
    RefDS(_28980);
    _0 = _t_55930;
    _t_55930 = _52keyfind(_28980, -1, _25current_file_no_12262, 0, _32374);
    DeRef(_0);
    _28980 = NOVALUE;
    _32374 = NOVALUE;

    /** 				No_new_entry = 1*/
    _52No_new_entry_48273 = 1;

    /** 				if t[T_ID] = IGNORED then*/
    _2 = (int)SEQ_PTR(_t_55930);
    _28982 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _28982, 509)){
        _28982 = NOVALUE;
        goto L8; // [355] 728
    }
    _28982 = NOVALUE;

    /** 					CompileErr(157,{Recorded[n]})*/
    _2 = (int)SEQ_PTR(_25Recorded_12389);
    _28984 = (int)*(((s1_ptr)_2)->base + _n_55928);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_28984);
    *((int *)(_2+4)) = _28984;
    _28985 = MAKE_SEQ(_1);
    _28984 = NOVALUE;
    _43CompileErr(157, _28985, 0);
    _28985 = NOVALUE;
    goto L8; // [377] 728
L9: 

    /** 				t = {SymTab[p][S_TOKEN], p}*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28986 = (int)*(((s1_ptr)_2)->base + _p_55931);
    _2 = (int)SEQ_PTR(_28986);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _28987 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _28987 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _28986 = NOVALUE;
    Ref(_28987);
    DeRef(_t_55930);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _28987;
    ((int *)_2)[2] = _p_55931;
    _t_55930 = MAKE_SEQ(_1);
    _28987 = NOVALUE;
    goto L8; // [400] 728
L1: 

    /** 		prev_Nne = No_new_entry*/
    _prev_Nne_55932 = _52No_new_entry_48273;

    /** 		No_new_entry = 1*/
    _52No_new_entry_48273 = 1;

    /** 		t = keyfind(Ns_recorded[n],-1, , 1)*/
    _2 = (int)SEQ_PTR(_25Ns_recorded_12390);
    _28989 = (int)*(((s1_ptr)_2)->base + _n_55928);
    Ref(_28989);
    DeRef(_32371);
    _32371 = _28989;
    _32372 = _52hashfn(_32371);
    _32371 = NOVALUE;
    Ref(_28989);
    _0 = _t_55930;
    _t_55930 = _52keyfind(_28989, -1, _25current_file_no_12262, 1, _32372);
    DeRef(_0);
    _28989 = NOVALUE;
    _32372 = NOVALUE;

    /** 		if t[T_ID] != NAMESPACE then*/
    _2 = (int)SEQ_PTR(_t_55930);
    _28991 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _28991, 523)){
        _28991 = NOVALUE;
        goto LA; // [454] 520
    }
    _28991 = NOVALUE;

    /** 			p = Ns_recorded_sym[n]*/
    _2 = (int)SEQ_PTR(_25Ns_recorded_sym_12392);
    _p_55931 = (int)*(((s1_ptr)_2)->base + _n_55928);
    if (!IS_ATOM_INT(_p_55931)){
        _p_55931 = (long)DBL_PTR(_p_55931)->dbl;
    }

    /** 			if p = 0 or sym_token( p ) != NAMESPACE then*/
    _28994 = (_p_55931 == 0);
    if (_28994 != 0) {
        goto LB; // [474] 493
    }
    _28996 = _52sym_token(_p_55931);
    if (IS_ATOM_INT(_28996)) {
        _28997 = (_28996 != 523);
    }
    else {
        _28997 = binary_op(NOTEQ, _28996, 523);
    }
    DeRef(_28996);
    _28996 = NOVALUE;
    if (_28997 == 0) {
        DeRef(_28997);
        _28997 = NOVALUE;
        goto LC; // [489] 511
    }
    else {
        if (!IS_ATOM_INT(_28997) && DBL_PTR(_28997)->dbl == 0.0){
            DeRef(_28997);
            _28997 = NOVALUE;
            goto LC; // [489] 511
        }
        DeRef(_28997);
        _28997 = NOVALUE;
    }
    DeRef(_28997);
    _28997 = NOVALUE;
LB: 

    /** 				CompileErr(153, {Ns_recorded[n]})*/
    _2 = (int)SEQ_PTR(_25Ns_recorded_12390);
    _28998 = (int)*(((s1_ptr)_2)->base + _n_55928);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_28998);
    *((int *)(_2+4)) = _28998;
    _28999 = MAKE_SEQ(_1);
    _28998 = NOVALUE;
    _43CompileErr(153, _28999, 0);
    _28999 = NOVALUE;
LC: 

    /** 			t = {NAMESPACE, p}*/
    DeRef(_t_55930);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 523;
    ((int *)_2)[2] = _p_55931;
    _t_55930 = MAKE_SEQ(_1);
LA: 

    /** 		t = keyfind(Recorded[n],SymTab[t[T_SYM]][S_OBJ])*/
    _2 = (int)SEQ_PTR(_25Recorded_12389);
    _29001 = (int)*(((s1_ptr)_2)->base + _n_55928);
    _2 = (int)SEQ_PTR(_t_55930);
    _29002 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_29002)){
        _29003 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29002)->dbl));
    }
    else{
        _29003 = (int)*(((s1_ptr)_2)->base + _29002);
    }
    _2 = (int)SEQ_PTR(_29003);
    _29004 = (int)*(((s1_ptr)_2)->base + 1);
    _29003 = NOVALUE;
    RefDS(_29001);
    DeRef(_32369);
    _32369 = _29001;
    _32370 = _52hashfn(_32369);
    _32369 = NOVALUE;
    RefDS(_29001);
    Ref(_29004);
    _0 = _t_55930;
    _t_55930 = _52keyfind(_29001, _29004, _25current_file_no_12262, 0, _32370);
    DeRef(_0);
    _29001 = NOVALUE;
    _29004 = NOVALUE;
    _32370 = NOVALUE;

    /** 		if t[T_ID] = IGNORED then*/
    _2 = (int)SEQ_PTR(_t_55930);
    _29006 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29006, 509)){
        _29006 = NOVALUE;
        goto LD; // [573] 630
    }
    _29006 = NOVALUE;

    /** 	        p = Recorded_sym[n]*/
    _2 = (int)SEQ_PTR(_25Recorded_sym_12391);
    _p_55931 = (int)*(((s1_ptr)_2)->base + _n_55928);
    if (!IS_ATOM_INT(_p_55931)){
        _p_55931 = (long)DBL_PTR(_p_55931)->dbl;
    }

    /** 	        if p = 0 then*/
    if (_p_55931 != 0)
    goto LE; // [589] 611

    /** 	        	CompileErr(157,{Recorded[n]})*/
    _2 = (int)SEQ_PTR(_25Recorded_12389);
    _29010 = (int)*(((s1_ptr)_2)->base + _n_55928);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_29010);
    *((int *)(_2+4)) = _29010;
    _29011 = MAKE_SEQ(_1);
    _29010 = NOVALUE;
    _43CompileErr(157, _29011, 0);
    _29011 = NOVALUE;
LE: 

    /** 		    t = {SymTab[p][S_TOKEN], p}*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29012 = (int)*(((s1_ptr)_2)->base + _p_55931);
    _2 = (int)SEQ_PTR(_29012);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _29013 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _29013 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _29012 = NOVALUE;
    Ref(_29013);
    DeRef(_t_55930);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _29013;
    ((int *)_2)[2] = _p_55931;
    _t_55930 = MAKE_SEQ(_1);
    _29013 = NOVALUE;
LD: 

    /** 		n = t[T_ID]*/
    _2 = (int)SEQ_PTR(_t_55930);
    _n_55928 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_n_55928)){
        _n_55928 = (long)DBL_PTR(_n_55928)->dbl;
    }

    /** 		if n = VARIABLE then*/
    if (_n_55928 != -100)
    goto LF; // [644] 660

    /** 			n = QUALIFIED_VARIABLE*/
    _n_55928 = 512;
    goto L10; // [657] 719
LF: 

    /** 		elsif n = FUNC then*/
    if (_n_55928 != 501)
    goto L11; // [664] 680

    /** 			n = QUALIFIED_FUNC*/
    _n_55928 = 520;
    goto L10; // [677] 719
L11: 

    /** 		elsif n = PROC then*/
    if (_n_55928 != 27)
    goto L12; // [684] 700

    /** 			n = QUALIFIED_PROC*/
    _n_55928 = 521;
    goto L10; // [697] 719
L12: 

    /** 		elsif n = TYPE then*/
    if (_n_55928 != 504)
    goto L13; // [704] 718

    /** 			n = QUALIFIED_TYPE*/
    _n_55928 = 522;
L13: 
L10: 

    /** 		t[T_ID] = n*/
    _2 = (int)SEQ_PTR(_t_55930);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _t_55930 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _n_55928;
    DeRef(_1);
L8: 

    /** 	No_new_entry = prev_Nne*/
    _52No_new_entry_48273 = _prev_Nne_55932;

    /**   	return t*/
    _28953 = NOVALUE;
    DeRef(_28951);
    _28951 = NOVALUE;
    DeRef(_28961);
    _28961 = NOVALUE;
    DeRef(_28956);
    _28956 = NOVALUE;
    DeRef(_28963);
    _28963 = NOVALUE;
    DeRef(_28994);
    _28994 = NOVALUE;
    DeRef(_28965);
    _28965 = NOVALUE;
    _29002 = NOVALUE;
    return _t_55930;
    ;
}


int _30next_token()
{
    int _t_56109 = NOVALUE;
    int _s_56110 = NOVALUE;
    int _29052 = NOVALUE;
    int _29051 = NOVALUE;
    int _29050 = NOVALUE;
    int _29049 = NOVALUE;
    int _29048 = NOVALUE;
    int _29047 = NOVALUE;
    int _29046 = NOVALUE;
    int _29045 = NOVALUE;
    int _29043 = NOVALUE;
    int _29042 = NOVALUE;
    int _29041 = NOVALUE;
    int _29040 = NOVALUE;
    int _29038 = NOVALUE;
    int _29036 = NOVALUE;
    int _29034 = NOVALUE;
    int _29030 = NOVALUE;
    int _29027 = NOVALUE;
    int _29024 = NOVALUE;
    int _29022 = NOVALUE;
    int _29020 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence s*/

    /** 	if length(backed_up_tok) > 0 then*/
    if (IS_SEQUENCE(_30backed_up_tok_55178)){
            _29020 = SEQ_PTR(_30backed_up_tok_55178)->length;
    }
    else {
        _29020 = 1;
    }
    if (_29020 <= 0)
    goto L1; // [10] 82

    /** 		t = backed_up_tok[$]*/
    if (IS_SEQUENCE(_30backed_up_tok_55178)){
            _29022 = SEQ_PTR(_30backed_up_tok_55178)->length;
    }
    else {
        _29022 = 1;
    }
    DeRef(_t_56109);
    _2 = (int)SEQ_PTR(_30backed_up_tok_55178);
    _t_56109 = (int)*(((s1_ptr)_2)->base + _29022);
    Ref(_t_56109);

    /** 		backed_up_tok = remove( backed_up_tok, length( backed_up_tok ) )*/
    if (IS_SEQUENCE(_30backed_up_tok_55178)){
            _29024 = SEQ_PTR(_30backed_up_tok_55178)->length;
    }
    else {
        _29024 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_30backed_up_tok_55178);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_29024)) ? _29024 : (long)(DBL_PTR(_29024)->dbl);
        int stop = (IS_ATOM_INT(_29024)) ? _29024 : (long)(DBL_PTR(_29024)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_30backed_up_tok_55178), start, &_30backed_up_tok_55178 );
            }
            else Tail(SEQ_PTR(_30backed_up_tok_55178), stop+1, &_30backed_up_tok_55178);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_30backed_up_tok_55178), start, &_30backed_up_tok_55178);
        }
        else {
            assign_slice_seq = &assign_space;
            _30backed_up_tok_55178 = Remove_elements(start, stop, (SEQ_PTR(_30backed_up_tok_55178)->ref == 1));
        }
    }
    _29024 = NOVALUE;
    _29024 = NOVALUE;

    /** 		if putback_fwd_line_number then*/
    if (_25putback_fwd_line_number_12265 == 0)
    {
        goto L2; // [43] 349
    }
    else{
    }

    /** 			ForwardLine     = putback_ForwardLine*/
    Ref(_43putback_ForwardLine_49534);
    DeRef(_43ForwardLine_49533);
    _43ForwardLine_49533 = _43putback_ForwardLine_49534;

    /** 			forward_bp      = putback_forward_bp*/
    _43forward_bp_49537 = _43putback_forward_bp_49538;

    /** 			fwd_line_number = putback_fwd_line_number*/
    _25fwd_line_number_12264 = _25putback_fwd_line_number_12265;

    /** 			putback_fwd_line_number = 0*/
    _25putback_fwd_line_number_12265 = 0;
    goto L2; // [79] 349
L1: 

    /** 	elsif Parser_mode = PAM_PLAYBACK then*/
    if (_25Parser_mode_12388 != -1)
    goto L3; // [88] 302

    /** 		if canned_index <= length(canned_tokens) then*/
    if (IS_SEQUENCE(_30canned_tokens_55215)){
            _29027 = SEQ_PTR(_30canned_tokens_55215)->length;
    }
    else {
        _29027 = 1;
    }
    if (_30canned_index_55216 > _29027)
    goto L4; // [101] 150

    /** 			t = canned_tokens[canned_index]*/
    DeRef(_t_56109);
    _2 = (int)SEQ_PTR(_30canned_tokens_55215);
    _t_56109 = (int)*(((s1_ptr)_2)->base + _30canned_index_55216);
    Ref(_t_56109);

    /** 			if canned_index < length(canned_tokens) then*/
    if (IS_SEQUENCE(_30canned_tokens_55215)){
            _29030 = SEQ_PTR(_30canned_tokens_55215)->length;
    }
    else {
        _29030 = 1;
    }
    if (_30canned_index_55216 >= _29030)
    goto L5; // [124] 139

    /** 				canned_index += 1*/
    _30canned_index_55216 = _30canned_index_55216 + 1;
    goto L6; // [136] 157
L5: 

    /** 	            s = restore_parser()*/
    _0 = _s_56110;
    _s_56110 = _30restore_parser();
    DeRef(_0);
    goto L6; // [147] 157
L4: 

    /** 	    	InternalErr(266)*/
    RefDS(_22682);
    _43InternalErr(266, _22682);
L6: 

    /** 		if t[T_ID] = RECORDED then*/
    _2 = (int)SEQ_PTR(_t_56109);
    _29034 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29034, 508)){
        _29034 = NOVALUE;
        goto L7; // [169] 188
    }
    _29034 = NOVALUE;

    /** 			t=read_recorded_token(t[T_SYM])*/
    _2 = (int)SEQ_PTR(_t_56109);
    _29036 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_29036);
    _0 = _t_56109;
    _t_56109 = _30read_recorded_token(_29036);
    DeRef(_0);
    _29036 = NOVALUE;
    goto L2; // [185] 349
L7: 

    /** 		elsif t[T_ID] = DEF_PARAM then*/
    _2 = (int)SEQ_PTR(_t_56109);
    _29038 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29038, 510)){
        _29038 = NOVALUE;
        goto L2; // [198] 349
    }
    _29038 = NOVALUE;

    /**         	for i=length(nested_calls) to 1 by -1 do*/
    if (IS_SEQUENCE(_30nested_calls_55904)){
            _29040 = SEQ_PTR(_30nested_calls_55904)->length;
    }
    else {
        _29040 = 1;
    }
    {
        int _i_56157;
        _i_56157 = _29040;
L8: 
        if (_i_56157 < 1){
            goto L9; // [209] 290
        }

        /**         	    if nested_calls[i] = t[T_SYM][2] then*/
        _2 = (int)SEQ_PTR(_30nested_calls_55904);
        _29041 = (int)*(((s1_ptr)_2)->base + _i_56157);
        _2 = (int)SEQ_PTR(_t_56109);
        _29042 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_29042);
        _29043 = (int)*(((s1_ptr)_2)->base + 2);
        _29042 = NOVALUE;
        if (binary_op_a(NOTEQ, _29041, _29043)){
            _29041 = NOVALUE;
            _29043 = NOVALUE;
            goto LA; // [234] 283
        }
        _29041 = NOVALUE;
        _29043 = NOVALUE;

        /** 					return {VARIABLE, private_sym[parseargs_states[i][PS_POSITION]+t[T_SYM][1]]}*/
        _2 = (int)SEQ_PTR(_30parseargs_states_55893);
        _29045 = (int)*(((s1_ptr)_2)->base + _i_56157);
        _2 = (int)SEQ_PTR(_29045);
        _29046 = (int)*(((s1_ptr)_2)->base + 1);
        _29045 = NOVALUE;
        _2 = (int)SEQ_PTR(_t_56109);
        _29047 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_29047);
        _29048 = (int)*(((s1_ptr)_2)->base + 1);
        _29047 = NOVALUE;
        if (IS_ATOM_INT(_29046) && IS_ATOM_INT(_29048)) {
            _29049 = _29046 + _29048;
        }
        else {
            _29049 = binary_op(PLUS, _29046, _29048);
        }
        _29046 = NOVALUE;
        _29048 = NOVALUE;
        _2 = (int)SEQ_PTR(_25private_sym_12395);
        if (!IS_ATOM_INT(_29049)){
            _29050 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29049)->dbl));
        }
        else{
            _29050 = (int)*(((s1_ptr)_2)->base + _29049);
        }
        Ref(_29050);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = -100;
        ((int *)_2)[2] = _29050;
        _29051 = MAKE_SEQ(_1);
        _29050 = NOVALUE;
        DeRef(_t_56109);
        DeRef(_s_56110);
        DeRef(_29049);
        _29049 = NOVALUE;
        return _29051;
LA: 

        /** 			end for*/
        _i_56157 = _i_56157 + -1;
        goto L8; // [285] 216
L9: 
        ;
    }

    /** 			CompileErr(98)*/
    RefDS(_22682);
    _43CompileErr(98, _22682, 0);
    goto L2; // [299] 349
L3: 

    /** 	elsif lock_scanner then*/
    if (_30lock_scanner_55902 == 0)
    {
        goto LB; // [306] 324
    }
    else{
    }

    /** 		return {PLAYBACK_ENDS,0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 505;
    ((int *)_2)[2] = 0;
    _29052 = MAKE_SEQ(_1);
    DeRef(_t_56109);
    DeRef(_s_56110);
    DeRef(_29051);
    _29051 = NOVALUE;
    DeRef(_29049);
    _29049 = NOVALUE;
    return _29052;
    goto L2; // [321] 349
LB: 

    /** 	    t = Scanner()*/
    _0 = _t_56109;
    _t_56109 = _60Scanner();
    DeRef(_0);

    /** 	    if Parser_mode = PAM_RECORD then*/
    if (_25Parser_mode_12388 != 1)
    goto LC; // [335] 348

    /** 	        canned_tokens = append(canned_tokens,t)*/
    Ref(_t_56109);
    Append(&_30canned_tokens_55215, _30canned_tokens_55215, _t_56109);
LC: 
L2: 

    /** 	putback_fwd_line_number = 0*/
    _25putback_fwd_line_number_12265 = 0;

    /** 	return t*/
    DeRef(_s_56110);
    DeRef(_29051);
    _29051 = NOVALUE;
    DeRef(_29052);
    _29052 = NOVALUE;
    DeRef(_29049);
    _29049 = NOVALUE;
    return _t_56109;
    ;
}


int _30Expr_list()
{
    int _tok_56192 = NOVALUE;
    int _n_56193 = NOVALUE;
    int _29068 = NOVALUE;
    int _29065 = NOVALUE;
    int _29064 = NOVALUE;
    int _29062 = NOVALUE;
    int _29061 = NOVALUE;
    int _29057 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer n*/

    /** 	tok = next_token()*/
    _0 = _tok_56192;
    _tok_56192 = _30next_token();
    DeRef(_0);

    /** 	putback(tok)*/
    Ref(_tok_56192);
    _30putback(_tok_56192);

    /** 	if tok[T_ID] = RIGHT_BRACE then*/
    _2 = (int)SEQ_PTR(_tok_56192);
    _29057 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29057, -25)){
        _29057 = NOVALUE;
        goto L1; // [23] 36
    }
    _29057 = NOVALUE;

    /** 		return 0*/
    DeRef(_tok_56192);
    return 0;
    goto L2; // [33] 142
L1: 

    /** 		n = 0*/
    _n_56193 = 0;

    /** 		short_circuit -= 1*/
    _30short_circuit_55171 = _30short_circuit_55171 - 1;

    /** 		while TRUE do*/
L3: 
    if (_5TRUE_244 == 0)
    {
        goto L4; // [56] 133
    }
    else{
    }

    /** 			gListItem &= 1*/
    Append(&_30gListItem_55207, _30gListItem_55207, 1);

    /** 			Expr()*/
    _30Expr();

    /** 			n += gListItem[$]*/
    if (IS_SEQUENCE(_30gListItem_55207)){
            _29061 = SEQ_PTR(_30gListItem_55207)->length;
    }
    else {
        _29061 = 1;
    }
    _2 = (int)SEQ_PTR(_30gListItem_55207);
    _29062 = (int)*(((s1_ptr)_2)->base + _29061);
    _n_56193 = _n_56193 + _29062;
    _29062 = NOVALUE;

    /** 			gListItem = gListItem[1 .. $-1]*/
    if (IS_SEQUENCE(_30gListItem_55207)){
            _29064 = SEQ_PTR(_30gListItem_55207)->length;
    }
    else {
        _29064 = 1;
    }
    _29065 = _29064 - 1;
    _29064 = NOVALUE;
    rhs_slice_target = (object_ptr)&_30gListItem_55207;
    RHS_Slice(_30gListItem_55207, 1, _29065);

    /** 			tok = next_token()*/
    _0 = _tok_56192;
    _tok_56192 = _30next_token();
    DeRef(_0);

    /** 			if tok[T_ID] != COMMA then*/
    _2 = (int)SEQ_PTR(_tok_56192);
    _29068 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _29068, -30)){
        _29068 = NOVALUE;
        goto L3; // [119] 54
    }
    _29068 = NOVALUE;

    /** 				exit*/
    goto L4; // [125] 133

    /** 		end while*/
    goto L3; // [130] 54
L4: 

    /** 		short_circuit += 1*/
    _30short_circuit_55171 = _30short_circuit_55171 + 1;
L2: 

    /** 	putback(tok)*/
    Ref(_tok_56192);
    _30putback(_tok_56192);

    /** 	return n*/
    DeRef(_tok_56192);
    DeRef(_29065);
    _29065 = NOVALUE;
    return _n_56193;
    ;
}


void _30tok_match(int _tok_56221, int _prevtok_56222)
{
    int _t_56224 = NOVALUE;
    int _expected_56225 = NOVALUE;
    int _actual_56226 = NOVALUE;
    int _prevname_56227 = NOVALUE;
    int _29080 = NOVALUE;
    int _29078 = NOVALUE;
    int _29075 = NOVALUE;
    int _29072 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence expected, actual, prevname*/

    /** 	t = next_token()*/
    _0 = _t_56224;
    _t_56224 = _30next_token();
    DeRef(_0);

    /** 	if t[T_ID] != tok then*/
    _2 = (int)SEQ_PTR(_t_56224);
    _29072 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _29072, _tok_56221)){
        _29072 = NOVALUE;
        goto L1; // [20] 92
    }
    _29072 = NOVALUE;

    /** 		expected = LexName(tok)*/
    RefDS(_27188);
    _0 = _expected_56225;
    _expected_56225 = _37LexName(_tok_56221, _27188);
    DeRef(_0);

    /** 		actual = LexName(t[T_ID])*/
    _2 = (int)SEQ_PTR(_t_56224);
    _29075 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_29075);
    RefDS(_27188);
    _0 = _actual_56226;
    _actual_56226 = _37LexName(_29075, _27188);
    DeRef(_0);
    _29075 = NOVALUE;

    /** 		if prevtok = 0 then*/
    if (_prevtok_56222 != 0)
    goto L2; // [50] 68

    /** 			CompileErr(132, {expected, actual})*/
    RefDS(_actual_56226);
    RefDS(_expected_56225);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _expected_56225;
    ((int *)_2)[2] = _actual_56226;
    _29078 = MAKE_SEQ(_1);
    _43CompileErr(132, _29078, 0);
    _29078 = NOVALUE;
    goto L3; // [65] 91
L2: 

    /** 			prevname = LexName(prevtok)*/
    RefDS(_27188);
    _0 = _prevname_56227;
    _prevname_56227 = _37LexName(_prevtok_56222, _27188);
    DeRef(_0);

    /** 			CompileErr(138, {expected, prevname, actual})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_expected_56225);
    *((int *)(_2+4)) = _expected_56225;
    RefDS(_prevname_56227);
    *((int *)(_2+8)) = _prevname_56227;
    RefDS(_actual_56226);
    *((int *)(_2+12)) = _actual_56226;
    _29080 = MAKE_SEQ(_1);
    _43CompileErr(138, _29080, 0);
    _29080 = NOVALUE;
L3: 
L1: 

    /** end procedure*/
    DeRef(_t_56224);
    DeRef(_expected_56225);
    DeRef(_actual_56226);
    DeRef(_prevname_56227);
    return;
    ;
}


void _30UndefinedVar(int _s_56261)
{
    int _dup_56263 = NOVALUE;
    int _errmsg_56264 = NOVALUE;
    int _rname_56265 = NOVALUE;
    int _fname_56266 = NOVALUE;
    int _29103 = NOVALUE;
    int _29102 = NOVALUE;
    int _29100 = NOVALUE;
    int _29098 = NOVALUE;
    int _29097 = NOVALUE;
    int _29095 = NOVALUE;
    int _29093 = NOVALUE;
    int _29091 = NOVALUE;
    int _29090 = NOVALUE;
    int _29089 = NOVALUE;
    int _29088 = NOVALUE;
    int _29087 = NOVALUE;
    int _29085 = NOVALUE;
    int _29084 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_s_56261)) {
        _1 = (long)(DBL_PTR(_s_56261)->dbl);
        if (UNIQUE(DBL_PTR(_s_56261)) && (DBL_PTR(_s_56261)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_56261);
        _s_56261 = _1;
    }

    /** 	sequence errmsg*/

    /** 	sequence rname*/

    /** 	sequence fname*/

    /** 	if SymTab[s][S_SCOPE] = SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29084 = (int)*(((s1_ptr)_2)->base + _s_56261);
    _2 = (int)SEQ_PTR(_29084);
    _29085 = (int)*(((s1_ptr)_2)->base + 4);
    _29084 = NOVALUE;
    if (binary_op_a(NOTEQ, _29085, 9)){
        _29085 = NOVALUE;
        goto L1; // [25] 55
    }
    _29085 = NOVALUE;

    /** 		CompileErr(19, {SymTab[s][S_NAME]})*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29087 = (int)*(((s1_ptr)_2)->base + _s_56261);
    _2 = (int)SEQ_PTR(_29087);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _29088 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _29088 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _29087 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_29088);
    *((int *)(_2+4)) = _29088;
    _29089 = MAKE_SEQ(_1);
    _29088 = NOVALUE;
    _43CompileErr(19, _29089, 0);
    _29089 = NOVALUE;
    goto L2; // [52] 202
L1: 

    /** 	elsif SymTab[s][S_SCOPE] = SC_MULTIPLY_DEFINED then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29090 = (int)*(((s1_ptr)_2)->base + _s_56261);
    _2 = (int)SEQ_PTR(_29090);
    _29091 = (int)*(((s1_ptr)_2)->base + 4);
    _29090 = NOVALUE;
    if (binary_op_a(NOTEQ, _29091, 10)){
        _29091 = NOVALUE;
        goto L3; // [71] 179
    }
    _29091 = NOVALUE;

    /** 		rname = SymTab[s][S_NAME]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29093 = (int)*(((s1_ptr)_2)->base + _s_56261);
    DeRef(_rname_56265);
    _2 = (int)SEQ_PTR(_29093);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _rname_56265 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _rname_56265 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    Ref(_rname_56265);
    _29093 = NOVALUE;

    /** 		errmsg = ""*/
    RefDS(_22682);
    DeRef(_errmsg_56264);
    _errmsg_56264 = _22682;

    /** 		for i = 1 to length(dup_globals) do*/
    if (IS_SEQUENCE(_52dup_globals_48262)){
            _29095 = SEQ_PTR(_52dup_globals_48262)->length;
    }
    else {
        _29095 = 1;
    }
    {
        int _i_56292;
        _i_56292 = 1;
L4: 
        if (_i_56292 > _29095){
            goto L5; // [105] 163
        }

        /** 			dup = dup_globals[i]*/
        _2 = (int)SEQ_PTR(_52dup_globals_48262);
        _dup_56263 = (int)*(((s1_ptr)_2)->base + _i_56292);
        if (!IS_ATOM_INT(_dup_56263)){
            _dup_56263 = (long)DBL_PTR(_dup_56263)->dbl;
        }

        /** 			fname = known_files[SymTab[dup][S_FILE_NO]]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _29097 = (int)*(((s1_ptr)_2)->base + _dup_56263);
        _2 = (int)SEQ_PTR(_29097);
        if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
            _29098 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
        }
        else{
            _29098 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
        }
        _29097 = NOVALUE;
        DeRef(_fname_56266);
        _2 = (int)SEQ_PTR(_26known_files_11139);
        if (!IS_ATOM_INT(_29098)){
            _fname_56266 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29098)->dbl));
        }
        else{
            _fname_56266 = (int)*(((s1_ptr)_2)->base + _29098);
        }
        Ref(_fname_56266);

        /** 			errmsg &= "    " & fname & "\n"*/
        {
            int concat_list[3];

            concat_list[0] = _22834;
            concat_list[1] = _fname_56266;
            concat_list[2] = _25659;
            Concat_N((object_ptr)&_29100, concat_list, 3);
        }
        Concat((object_ptr)&_errmsg_56264, _errmsg_56264, _29100);
        DeRefDS(_29100);
        _29100 = NOVALUE;

        /** 		end for*/
        _i_56292 = _i_56292 + 1;
        goto L4; // [158] 112
L5: 
        ;
    }

    /** 		CompileErr(23, {rname, rname, errmsg})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDSn(_rname_56265, 2);
    *((int *)(_2+4)) = _rname_56265;
    *((int *)(_2+8)) = _rname_56265;
    RefDS(_errmsg_56264);
    *((int *)(_2+12)) = _errmsg_56264;
    _29102 = MAKE_SEQ(_1);
    _43CompileErr(23, _29102, 0);
    _29102 = NOVALUE;
    goto L2; // [176] 202
L3: 

    /** 	elsif length(symbol_resolution_warning) then*/
    if (IS_SEQUENCE(_25symbol_resolution_warning_12382)){
            _29103 = SEQ_PTR(_25symbol_resolution_warning_12382)->length;
    }
    else {
        _29103 = 1;
    }
    if (_29103 == 0)
    {
        _29103 = NOVALUE;
        goto L6; // [186] 201
    }
    else{
        _29103 = NOVALUE;
    }

    /** 		Warning( symbol_resolution_warning, resolution_warning_flag)*/
    RefDS(_25symbol_resolution_warning_12382);
    RefDS(_22682);
    _43Warning(_25symbol_resolution_warning_12382, 1, _22682);
L6: 
L2: 

    /** end procedure*/
    DeRef(_errmsg_56264);
    DeRef(_rname_56265);
    DeRef(_fname_56266);
    _29098 = NOVALUE;
    return;
    ;
}


void _30WrongNumberArgs(int _subsym_56316, int _only_56317)
{
    int _msgno_56318 = NOVALUE;
    int _29115 = NOVALUE;
    int _29114 = NOVALUE;
    int _29113 = NOVALUE;
    int _29112 = NOVALUE;
    int _29111 = NOVALUE;
    int _29109 = NOVALUE;
    int _29107 = NOVALUE;
    int _29105 = NOVALUE;
    int _29104 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_subsym_56316)) {
        _1 = (long)(DBL_PTR(_subsym_56316)->dbl);
        if (UNIQUE(DBL_PTR(_subsym_56316)) && (DBL_PTR(_subsym_56316)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subsym_56316);
        _subsym_56316 = _1;
    }

    /** 	if SymTab[subsym][S_NUM_ARGS] = 1 then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29104 = (int)*(((s1_ptr)_2)->base + _subsym_56316);
    _2 = (int)SEQ_PTR(_29104);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _29105 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _29105 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    _29104 = NOVALUE;
    if (binary_op_a(NOTEQ, _29105, 1)){
        _29105 = NOVALUE;
        goto L1; // [19] 49
    }
    _29105 = NOVALUE;

    /** 		if length(only) = 0 then*/
    if (IS_SEQUENCE(_only_56317)){
            _29107 = SEQ_PTR(_only_56317)->length;
    }
    else {
        _29107 = 1;
    }
    if (_29107 != 0)
    goto L2; // [28] 40

    /** 			msgno = 20*/
    _msgno_56318 = 20;
    goto L3; // [37] 73
L2: 

    /** 			msgno = 237*/
    _msgno_56318 = 237;
    goto L3; // [46] 73
L1: 

    /** 		if length(only) = 0 then*/
    if (IS_SEQUENCE(_only_56317)){
            _29109 = SEQ_PTR(_only_56317)->length;
    }
    else {
        _29109 = 1;
    }
    if (_29109 != 0)
    goto L4; // [54] 66

    /** 			msgno = 236*/
    _msgno_56318 = 236;
    goto L5; // [63] 72
L4: 

    /** 			msgno = 238*/
    _msgno_56318 = 238;
L5: 
L3: 

    /** 	CompileErr(msgno, {SymTab[subsym][S_NAME], SymTab[subsym][S_NUM_ARGS]})*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29111 = (int)*(((s1_ptr)_2)->base + _subsym_56316);
    _2 = (int)SEQ_PTR(_29111);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _29112 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _29112 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _29111 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29113 = (int)*(((s1_ptr)_2)->base + _subsym_56316);
    _2 = (int)SEQ_PTR(_29113);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _29114 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _29114 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    _29113 = NOVALUE;
    Ref(_29114);
    Ref(_29112);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _29112;
    ((int *)_2)[2] = _29114;
    _29115 = MAKE_SEQ(_1);
    _29114 = NOVALUE;
    _29112 = NOVALUE;
    _43CompileErr(_msgno_56318, _29115, 0);
    _29115 = NOVALUE;

    /** end procedure*/
    DeRefDSi(_only_56317);
    return;
    ;
}


void _30MissingArgs(int _subsym_56347)
{
    int _eentry_56348 = NOVALUE;
    int _29120 = NOVALUE;
    int _29119 = NOVALUE;
    int _29118 = NOVALUE;
    int _29117 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence eentry = SymTab[subsym]*/
    DeRef(_eentry_56348);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _eentry_56348 = (int)*(((s1_ptr)_2)->base + _subsym_56347);
    Ref(_eentry_56348);

    /** 	CompileErr(235, {eentry[S_NAME], eentry[S_DEF_ARGS][2]})*/
    _2 = (int)SEQ_PTR(_eentry_56348);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _29117 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _29117 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _2 = (int)SEQ_PTR(_eentry_56348);
    _29118 = (int)*(((s1_ptr)_2)->base + 28);
    _2 = (int)SEQ_PTR(_29118);
    _29119 = (int)*(((s1_ptr)_2)->base + 2);
    _29118 = NOVALUE;
    Ref(_29119);
    Ref(_29117);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _29117;
    ((int *)_2)[2] = _29119;
    _29120 = MAKE_SEQ(_1);
    _29119 = NOVALUE;
    _29117 = NOVALUE;
    _43CompileErr(235, _29120, 0);
    _29120 = NOVALUE;

    /** end procedure*/
    DeRefDS(_eentry_56348);
    return;
    ;
}


void _30Parse_default_arg(int _subsym_56361, int _arg_56362, int _fwd_private_list_56363, int _fwd_private_sym_56364)
{
    int _param_56366 = NOVALUE;
    int _29140 = NOVALUE;
    int _29139 = NOVALUE;
    int _29138 = NOVALUE;
    int _29137 = NOVALUE;
    int _29136 = NOVALUE;
    int _29135 = NOVALUE;
    int _29134 = NOVALUE;
    int _29133 = NOVALUE;
    int _29132 = NOVALUE;
    int _29131 = NOVALUE;
    int _29130 = NOVALUE;
    int _29129 = NOVALUE;
    int _29128 = NOVALUE;
    int _29126 = NOVALUE;
    int _29125 = NOVALUE;
    int _29122 = NOVALUE;
    int _29121 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_subsym_56361)) {
        _1 = (long)(DBL_PTR(_subsym_56361)->dbl);
        if (UNIQUE(DBL_PTR(_subsym_56361)) && (DBL_PTR(_subsym_56361)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subsym_56361);
        _subsym_56361 = _1;
    }
    if (!IS_ATOM_INT(_arg_56362)) {
        _1 = (long)(DBL_PTR(_arg_56362)->dbl);
        if (UNIQUE(DBL_PTR(_arg_56362)) && (DBL_PTR(_arg_56362)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_arg_56362);
        _arg_56362 = _1;
    }

    /** 	symtab_index param = subsym*/
    _param_56366 = _subsym_56361;

    /** 	on_arg = arg*/
    _30on_arg_55903 = _arg_56362;

    /** 	parseargs_states = append(parseargs_states,*/
    if (IS_SEQUENCE(_30private_list_55901)){
            _29121 = SEQ_PTR(_30private_list_55901)->length;
    }
    else {
        _29121 = 1;
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _29121;
    *((int *)(_2+8)) = _30lock_scanner_55902;
    *((int *)(_2+12)) = _25use_private_list_12396;
    *((int *)(_2+16)) = _30on_arg_55903;
    _29122 = MAKE_SEQ(_1);
    _29121 = NOVALUE;
    RefDS(_29122);
    Append(&_30parseargs_states_55893, _30parseargs_states_55893, _29122);
    DeRefDS(_29122);
    _29122 = NOVALUE;

    /** 	nested_calls &= subsym*/
    Append(&_30nested_calls_55904, _30nested_calls_55904, _subsym_56361);

    /** 	for i = 1 to arg do*/
    _29125 = _arg_56362;
    {
        int _i_56373;
        _i_56373 = 1;
L1: 
        if (_i_56373 > _29125){
            goto L2; // [60] 90
        }

        /** 		param = SymTab[param][S_NEXT]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _29126 = (int)*(((s1_ptr)_2)->base + _param_56366);
        _2 = (int)SEQ_PTR(_29126);
        _param_56366 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_param_56366)){
            _param_56366 = (long)DBL_PTR(_param_56366)->dbl;
        }
        _29126 = NOVALUE;

        /** 	end for*/
        _i_56373 = _i_56373 + 1;
        goto L1; // [85] 67
L2: 
        ;
    }

    /** 	private_list = fwd_private_list*/
    RefDS(_fwd_private_list_56363);
    DeRef(_30private_list_55901);
    _30private_list_55901 = _fwd_private_list_56363;

    /** 	private_sym  = fwd_private_sym*/
    RefDS(_fwd_private_sym_56364);
    DeRef(_25private_sym_12395);
    _25private_sym_12395 = _fwd_private_sym_56364;

    /** 	if atom(SymTab[param][S_CODE]) then  -- but no default set*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29128 = (int)*(((s1_ptr)_2)->base + _param_56366);
    _2 = (int)SEQ_PTR(_29128);
    if (!IS_ATOM_INT(_25S_CODE_11925)){
        _29129 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    }
    else{
        _29129 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
    }
    _29128 = NOVALUE;
    _29130 = IS_ATOM(_29129);
    _29129 = NOVALUE;
    if (_29130 == 0)
    {
        _29130 = NOVALUE;
        goto L3; // [121] 162
    }
    else{
        _29130 = NOVALUE;
    }

    /** 		CompileErr(26, {arg, SymTab[subsym][S_NAME], SymTab[param][S_NAME]})*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29131 = (int)*(((s1_ptr)_2)->base + _subsym_56361);
    _2 = (int)SEQ_PTR(_29131);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _29132 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _29132 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _29131 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29133 = (int)*(((s1_ptr)_2)->base + _param_56366);
    _2 = (int)SEQ_PTR(_29133);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _29134 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _29134 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _29133 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _arg_56362;
    Ref(_29132);
    *((int *)(_2+8)) = _29132;
    Ref(_29134);
    *((int *)(_2+12)) = _29134;
    _29135 = MAKE_SEQ(_1);
    _29134 = NOVALUE;
    _29132 = NOVALUE;
    _43CompileErr(26, _29135, 0);
    _29135 = NOVALUE;
L3: 

    /** 	use_private_list = 1*/
    _25use_private_list_12396 = 1;

    /** 	lock_scanner = 1*/
    _30lock_scanner_55902 = 1;

    /** 	start_playback(SymTab[param][S_CODE] )*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29136 = (int)*(((s1_ptr)_2)->base + _param_56366);
    _2 = (int)SEQ_PTR(_29136);
    if (!IS_ATOM_INT(_25S_CODE_11925)){
        _29137 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    }
    else{
        _29137 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
    }
    _29136 = NOVALUE;
    Ref(_29137);
    _30start_playback(_29137);
    _29137 = NOVALUE;

    /** 	call_proc(forward_expr, {})*/
    _0 = (int)_00[_30forward_expr_56188].addr;
    (*(int (*)())_0)(
                         );

    /** 	add_private_symbol( Top(), SymTab[param][S_NAME] )*/
    _29138 = _37Top();
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29139 = (int)*(((s1_ptr)_2)->base + _param_56366);
    _2 = (int)SEQ_PTR(_29139);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _29140 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _29140 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _29139 = NOVALUE;
    Ref(_29140);
    _29add_private_symbol(_29138, _29140);
    _29138 = NOVALUE;
    _29140 = NOVALUE;

    /** 	lock_scanner = 0*/
    _30lock_scanner_55902 = 0;

    /** 	restore_parseargs_states()*/
    _30restore_parseargs_states();

    /** end procedure*/
    DeRefDS(_fwd_private_list_56363);
    DeRefDS(_fwd_private_sym_56364);
    return;
    ;
}


void _30ParseArgs(int _subsym_56411)
{
    int _n_56412 = NOVALUE;
    int _fda_56413 = NOVALUE;
    int _lnda_56414 = NOVALUE;
    int _tok_56416 = NOVALUE;
    int _s_56418 = NOVALUE;
    int _var_code_56419 = NOVALUE;
    int _name_56420 = NOVALUE;
    int _29234 = NOVALUE;
    int _29232 = NOVALUE;
    int _29228 = NOVALUE;
    int _29227 = NOVALUE;
    int _29226 = NOVALUE;
    int _29223 = NOVALUE;
    int _29220 = NOVALUE;
    int _29218 = NOVALUE;
    int _29216 = NOVALUE;
    int _29214 = NOVALUE;
    int _29212 = NOVALUE;
    int _29211 = NOVALUE;
    int _29210 = NOVALUE;
    int _29209 = NOVALUE;
    int _29208 = NOVALUE;
    int _29207 = NOVALUE;
    int _29206 = NOVALUE;
    int _29200 = NOVALUE;
    int _29198 = NOVALUE;
    int _29195 = NOVALUE;
    int _29192 = NOVALUE;
    int _29187 = NOVALUE;
    int _29185 = NOVALUE;
    int _29184 = NOVALUE;
    int _29183 = NOVALUE;
    int _29181 = NOVALUE;
    int _29178 = NOVALUE;
    int _29175 = NOVALUE;
    int _29173 = NOVALUE;
    int _29171 = NOVALUE;
    int _29169 = NOVALUE;
    int _29167 = NOVALUE;
    int _29166 = NOVALUE;
    int _29165 = NOVALUE;
    int _29164 = NOVALUE;
    int _29163 = NOVALUE;
    int _29162 = NOVALUE;
    int _29161 = NOVALUE;
    int _29159 = NOVALUE;
    int _29157 = NOVALUE;
    int _29153 = NOVALUE;
    int _29152 = NOVALUE;
    int _29150 = NOVALUE;
    int _29149 = NOVALUE;
    int _29147 = NOVALUE;
    int _29146 = NOVALUE;
    int _29145 = NOVALUE;
    int _29144 = NOVALUE;
    int _29143 = NOVALUE;
    int _29141 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_subsym_56411)) {
        _1 = (long)(DBL_PTR(_subsym_56411)->dbl);
        if (UNIQUE(DBL_PTR(_subsym_56411)) && (DBL_PTR(_subsym_56411)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subsym_56411);
        _subsym_56411 = _1;
    }

    /** 	object var_code*/

    /** 	sequence name*/

    /** 	n = SymTab[subsym][S_NUM_ARGS]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29141 = (int)*(((s1_ptr)_2)->base + _subsym_56411);
    _2 = (int)SEQ_PTR(_29141);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _n_56412 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _n_56412 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    if (!IS_ATOM_INT(_n_56412)){
        _n_56412 = (long)DBL_PTR(_n_56412)->dbl;
    }
    _29141 = NOVALUE;

    /** 	if sequence(SymTab[subsym][S_DEF_ARGS]) then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29143 = (int)*(((s1_ptr)_2)->base + _subsym_56411);
    _2 = (int)SEQ_PTR(_29143);
    _29144 = (int)*(((s1_ptr)_2)->base + 28);
    _29143 = NOVALUE;
    _29145 = IS_SEQUENCE(_29144);
    _29144 = NOVALUE;
    if (_29145 == 0)
    {
        _29145 = NOVALUE;
        goto L1; // [40] 86
    }
    else{
        _29145 = NOVALUE;
    }

    /** 		fda = SymTab[subsym][S_DEF_ARGS][1]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29146 = (int)*(((s1_ptr)_2)->base + _subsym_56411);
    _2 = (int)SEQ_PTR(_29146);
    _29147 = (int)*(((s1_ptr)_2)->base + 28);
    _29146 = NOVALUE;
    _2 = (int)SEQ_PTR(_29147);
    _fda_56413 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_fda_56413)){
        _fda_56413 = (long)DBL_PTR(_fda_56413)->dbl;
    }
    _29147 = NOVALUE;

    /** 		lnda = SymTab[subsym][S_DEF_ARGS][2]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29149 = (int)*(((s1_ptr)_2)->base + _subsym_56411);
    _2 = (int)SEQ_PTR(_29149);
    _29150 = (int)*(((s1_ptr)_2)->base + 28);
    _29149 = NOVALUE;
    _2 = (int)SEQ_PTR(_29150);
    _lnda_56414 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_lnda_56414)){
        _lnda_56414 = (long)DBL_PTR(_lnda_56414)->dbl;
    }
    _29150 = NOVALUE;
    goto L2; // [83] 97
L1: 

    /** 		fda = 0*/
    _fda_56413 = 0;

    /** 		lnda = 0*/
    _lnda_56414 = 0;
L2: 

    /** 	s = subsym*/
    _s_56418 = _subsym_56411;

    /** 	parseargs_states = append(parseargs_states,*/
    if (IS_SEQUENCE(_30private_list_55901)){
            _29152 = SEQ_PTR(_30private_list_55901)->length;
    }
    else {
        _29152 = 1;
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _29152;
    *((int *)(_2+8)) = _30lock_scanner_55902;
    *((int *)(_2+12)) = _25use_private_list_12396;
    *((int *)(_2+16)) = _30on_arg_55903;
    _29153 = MAKE_SEQ(_1);
    _29152 = NOVALUE;
    RefDS(_29153);
    Append(&_30parseargs_states_55893, _30parseargs_states_55893, _29153);
    DeRefDS(_29153);
    _29153 = NOVALUE;

    /** 	nested_calls &= subsym*/
    Append(&_30nested_calls_55904, _30nested_calls_55904, _subsym_56411);

    /** 	lock_scanner = 0*/
    _30lock_scanner_55902 = 0;

    /** 	on_arg = 0*/
    _30on_arg_55903 = 0;

    /** 	short_circuit -= 1*/
    _30short_circuit_55171 = _30short_circuit_55171 - 1;

    /** 	for i = 1 to n do*/
    _29157 = _n_56412;
    {
        int _i_56449;
        _i_56449 = 1;
L3: 
        if (_i_56449 > _29157){
            goto L4; // [161] 915
        }

        /** 	  	tok = next_token()*/
        _0 = _tok_56416;
        _tok_56416 = _30next_token();
        DeRef(_0);

        /** 		if tok[T_ID] = COMMA then*/
        _2 = (int)SEQ_PTR(_tok_56416);
        _29159 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(NOTEQ, _29159, -30)){
            _29159 = NOVALUE;
            goto L5; // [183] 392
        }
        _29159 = NOVALUE;

        /** 			if SymTab[subsym][S_OPCODE] then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _29161 = (int)*(((s1_ptr)_2)->base + _subsym_56411);
        _2 = (int)SEQ_PTR(_29161);
        _29162 = (int)*(((s1_ptr)_2)->base + 21);
        _29161 = NOVALUE;
        if (_29162 == 0) {
            _29162 = NOVALUE;
            goto L6; // [201] 261
        }
        else {
            if (!IS_ATOM_INT(_29162) && DBL_PTR(_29162)->dbl == 0.0){
                _29162 = NOVALUE;
                goto L6; // [201] 261
            }
            _29162 = NOVALUE;
        }
        _29162 = NOVALUE;

        /** 				if atom(SymTab[subsym][S_CODE]) then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _29163 = (int)*(((s1_ptr)_2)->base + _subsym_56411);
        _2 = (int)SEQ_PTR(_29163);
        if (!IS_ATOM_INT(_25S_CODE_11925)){
            _29164 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
        }
        else{
            _29164 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
        }
        _29163 = NOVALUE;
        _29165 = IS_ATOM(_29164);
        _29164 = NOVALUE;
        if (_29165 == 0)
        {
            _29165 = NOVALUE;
            goto L7; // [221] 232
        }
        else{
            _29165 = NOVALUE;
        }

        /** 					var_code = 0*/
        DeRef(_var_code_56419);
        _var_code_56419 = 0;
        goto L8; // [229] 251
L7: 

        /** 					var_code = SymTab[subsym][S_CODE][i]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _29166 = (int)*(((s1_ptr)_2)->base + _subsym_56411);
        _2 = (int)SEQ_PTR(_29166);
        if (!IS_ATOM_INT(_25S_CODE_11925)){
            _29167 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
        }
        else{
            _29167 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
        }
        _29166 = NOVALUE;
        DeRef(_var_code_56419);
        _2 = (int)SEQ_PTR(_29167);
        _var_code_56419 = (int)*(((s1_ptr)_2)->base + _i_56449);
        Ref(_var_code_56419);
        _29167 = NOVALUE;
L8: 

        /** 				name = ""*/
        RefDS(_22682);
        DeRef(_name_56420);
        _name_56420 = _22682;
        goto L9; // [258] 308
L6: 

        /** 				s = SymTab[s][S_NEXT]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _29169 = (int)*(((s1_ptr)_2)->base + _s_56418);
        _2 = (int)SEQ_PTR(_29169);
        _s_56418 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_s_56418)){
            _s_56418 = (long)DBL_PTR(_s_56418)->dbl;
        }
        _29169 = NOVALUE;

        /** 				var_code = SymTab[s][S_CODE]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _29171 = (int)*(((s1_ptr)_2)->base + _s_56418);
        DeRef(_var_code_56419);
        _2 = (int)SEQ_PTR(_29171);
        if (!IS_ATOM_INT(_25S_CODE_11925)){
            _var_code_56419 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
        }
        else{
            _var_code_56419 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
        }
        Ref(_var_code_56419);
        _29171 = NOVALUE;

        /** 				name = SymTab[s][S_NAME]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _29173 = (int)*(((s1_ptr)_2)->base + _s_56418);
        DeRef(_name_56420);
        _2 = (int)SEQ_PTR(_29173);
        if (!IS_ATOM_INT(_25S_NAME_11913)){
            _name_56420 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
        }
        else{
            _name_56420 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
        }
        Ref(_name_56420);
        _29173 = NOVALUE;
L9: 

        /** 			if atom(var_code) then  -- but no default set*/
        _29175 = IS_ATOM(_var_code_56419);
        if (_29175 == 0)
        {
            _29175 = NOVALUE;
            goto LA; // [315] 326
        }
        else{
            _29175 = NOVALUE;
        }

        /** 				CompileErr(29,i)*/
        _43CompileErr(29, _i_56449, 0);
LA: 

        /** 			use_private_list = 1*/
        _25use_private_list_12396 = 1;

        /** 			start_playback(var_code)*/
        Ref(_var_code_56419);
        _30start_playback(_var_code_56419);

        /** 			lock_scanner=1*/
        _30lock_scanner_55902 = 1;

        /** 			Expr()*/
        _30Expr();

        /** 			lock_scanner=0*/
        _30lock_scanner_55902 = 0;

        /** 			on_arg += 1*/
        _30on_arg_55903 = _30on_arg_55903 + 1;

        /** 			private_list = append(private_list,name)*/
        RefDS(_name_56420);
        Append(&_30private_list_55901, _30private_list_55901, _name_56420);

        /** 			private_sym &= Top()*/
        _29178 = _37Top();
        if (IS_SEQUENCE(_25private_sym_12395) && IS_ATOM(_29178)) {
            Ref(_29178);
            Append(&_25private_sym_12395, _25private_sym_12395, _29178);
        }
        else if (IS_ATOM(_25private_sym_12395) && IS_SEQUENCE(_29178)) {
        }
        else {
            Concat((object_ptr)&_25private_sym_12395, _25private_sym_12395, _29178);
        }
        DeRef(_29178);
        _29178 = NOVALUE;

        /** 			backed_up_tok = {tok} -- ????*/
        _0 = _30backed_up_tok_55178;
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_tok_56416);
        *((int *)(_2+4)) = _tok_56416;
        _30backed_up_tok_55178 = MAKE_SEQ(_1);
        DeRef(_0);
        goto LB; // [389] 520
L5: 

        /** 		elsif tok[T_ID] != RIGHT_ROUND then*/
        _2 = (int)SEQ_PTR(_tok_56416);
        _29181 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(EQUALS, _29181, -27)){
            _29181 = NOVALUE;
            goto LC; // [402] 519
        }
        _29181 = NOVALUE;

        /** 			if SymTab[subsym][S_OPCODE] then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _29183 = (int)*(((s1_ptr)_2)->base + _subsym_56411);
        _2 = (int)SEQ_PTR(_29183);
        _29184 = (int)*(((s1_ptr)_2)->base + 21);
        _29183 = NOVALUE;
        if (_29184 == 0) {
            _29184 = NOVALUE;
            goto LD; // [420] 433
        }
        else {
            if (!IS_ATOM_INT(_29184) && DBL_PTR(_29184)->dbl == 0.0){
                _29184 = NOVALUE;
                goto LD; // [420] 433
            }
            _29184 = NOVALUE;
        }
        _29184 = NOVALUE;

        /** 				name = ""*/
        RefDS(_22682);
        DeRef(_name_56420);
        _name_56420 = _22682;
        goto LE; // [430] 466
LD: 

        /** 				s = SymTab[s][S_NEXT]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _29185 = (int)*(((s1_ptr)_2)->base + _s_56418);
        _2 = (int)SEQ_PTR(_29185);
        _s_56418 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_s_56418)){
            _s_56418 = (long)DBL_PTR(_s_56418)->dbl;
        }
        _29185 = NOVALUE;

        /** 				name = SymTab[s][S_NAME]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _29187 = (int)*(((s1_ptr)_2)->base + _s_56418);
        DeRef(_name_56420);
        _2 = (int)SEQ_PTR(_29187);
        if (!IS_ATOM_INT(_25S_NAME_11913)){
            _name_56420 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
        }
        else{
            _name_56420 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
        }
        Ref(_name_56420);
        _29187 = NOVALUE;
LE: 

        /** 			use_private_list = Parser_mode != PAM_NORMAL*/
        _25use_private_list_12396 = (_25Parser_mode_12388 != 0);

        /** 			putback(tok)*/
        Ref(_tok_56416);
        _30putback(_tok_56416);

        /** 			Expr()*/
        _30Expr();

        /** 			on_arg += 1*/
        _30on_arg_55903 = _30on_arg_55903 + 1;

        /** 			private_list = append(private_list,name)*/
        RefDS(_name_56420);
        Append(&_30private_list_55901, _30private_list_55901, _name_56420);

        /** 			private_sym &= Top()*/
        _29192 = _37Top();
        if (IS_SEQUENCE(_25private_sym_12395) && IS_ATOM(_29192)) {
            Ref(_29192);
            Append(&_25private_sym_12395, _25private_sym_12395, _29192);
        }
        else if (IS_ATOM(_25private_sym_12395) && IS_SEQUENCE(_29192)) {
        }
        else {
            Concat((object_ptr)&_25private_sym_12395, _25private_sym_12395, _29192);
        }
        DeRef(_29192);
        _29192 = NOVALUE;
LC: 
LB: 

        /** 		if on_arg != n then*/
        if (_30on_arg_55903 == _n_56412)
        goto LF; // [524] 908

        /** 			if tok[T_ID] = RIGHT_ROUND then*/
        _2 = (int)SEQ_PTR(_tok_56416);
        _29195 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(NOTEQ, _29195, -27)){
            _29195 = NOVALUE;
            goto L10; // [538] 548
        }
        _29195 = NOVALUE;

        /** 				putback( tok )*/
        Ref(_tok_56416);
        _30putback(_tok_56416);
L10: 

        /** 			tok = next_token()*/
        _0 = _tok_56416;
        _tok_56416 = _30next_token();
        DeRef(_0);

        /** 			if tok[T_ID] != COMMA then*/
        _2 = (int)SEQ_PTR(_tok_56416);
        _29198 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(EQUALS, _29198, -30)){
            _29198 = NOVALUE;
            goto L11; // [563] 907
        }
        _29198 = NOVALUE;

        /** 		  		if tok[T_ID] = RIGHT_ROUND then*/
        _2 = (int)SEQ_PTR(_tok_56416);
        _29200 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(NOTEQ, _29200, -27)){
            _29200 = NOVALUE;
            goto L12; // [577] 892
        }
        _29200 = NOVALUE;

        /** 					if fda=0 then*/
        if (_fda_56413 != 0)
        goto L13; // [585] 598

        /** 						WrongNumberArgs(subsym, "")*/
        RefDS(_22682);
        _30WrongNumberArgs(_subsym_56411, _22682);
        goto L14; // [595] 613
L13: 

        /** 					elsif i<lnda then*/
        if (_i_56449 >= _lnda_56414)
        goto L15; // [602] 612

        /** 						MissingArgs(subsym)*/
        _30MissingArgs(_subsym_56411);
L15: 
L14: 

        /** 					lock_scanner = 1*/
        _30lock_scanner_55902 = 1;

        /** 					use_private_list = 1*/
        _25use_private_list_12396 = 1;

        /** 					while on_arg < n do*/
L16: 
        if (_30on_arg_55903 >= _n_56412)
        goto L17; // [632] 841

        /** 						on_arg += 1*/
        _30on_arg_55903 = _30on_arg_55903 + 1;

        /** 						if SymTab[subsym][S_OPCODE] then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _29206 = (int)*(((s1_ptr)_2)->base + _subsym_56411);
        _2 = (int)SEQ_PTR(_29206);
        _29207 = (int)*(((s1_ptr)_2)->base + 21);
        _29206 = NOVALUE;
        if (_29207 == 0) {
            _29207 = NOVALUE;
            goto L18; // [658] 720
        }
        else {
            if (!IS_ATOM_INT(_29207) && DBL_PTR(_29207)->dbl == 0.0){
                _29207 = NOVALUE;
                goto L18; // [658] 720
            }
            _29207 = NOVALUE;
        }
        _29207 = NOVALUE;

        /** 							if atom(SymTab[subsym][S_CODE]) then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _29208 = (int)*(((s1_ptr)_2)->base + _subsym_56411);
        _2 = (int)SEQ_PTR(_29208);
        if (!IS_ATOM_INT(_25S_CODE_11925)){
            _29209 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
        }
        else{
            _29209 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
        }
        _29208 = NOVALUE;
        _29210 = IS_ATOM(_29209);
        _29209 = NOVALUE;
        if (_29210 == 0)
        {
            _29210 = NOVALUE;
            goto L19; // [678] 689
        }
        else{
            _29210 = NOVALUE;
        }

        /** 								var_code = 0*/
        DeRef(_var_code_56419);
        _var_code_56419 = 0;
        goto L1A; // [686] 710
L19: 

        /** 								var_code = SymTab[subsym][S_CODE][on_arg]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _29211 = (int)*(((s1_ptr)_2)->base + _subsym_56411);
        _2 = (int)SEQ_PTR(_29211);
        if (!IS_ATOM_INT(_25S_CODE_11925)){
            _29212 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
        }
        else{
            _29212 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
        }
        _29211 = NOVALUE;
        DeRef(_var_code_56419);
        _2 = (int)SEQ_PTR(_29212);
        _var_code_56419 = (int)*(((s1_ptr)_2)->base + _30on_arg_55903);
        Ref(_var_code_56419);
        _29212 = NOVALUE;
L1A: 

        /** 							name = ""*/
        RefDS(_22682);
        DeRef(_name_56420);
        _name_56420 = _22682;
        goto L1B; // [717] 767
L18: 

        /** 							s = SymTab[s][S_NEXT]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _29214 = (int)*(((s1_ptr)_2)->base + _s_56418);
        _2 = (int)SEQ_PTR(_29214);
        _s_56418 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_s_56418)){
            _s_56418 = (long)DBL_PTR(_s_56418)->dbl;
        }
        _29214 = NOVALUE;

        /** 							var_code = SymTab[s][S_CODE]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _29216 = (int)*(((s1_ptr)_2)->base + _s_56418);
        DeRef(_var_code_56419);
        _2 = (int)SEQ_PTR(_29216);
        if (!IS_ATOM_INT(_25S_CODE_11925)){
            _var_code_56419 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
        }
        else{
            _var_code_56419 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
        }
        Ref(_var_code_56419);
        _29216 = NOVALUE;

        /** 							name = SymTab[s][S_NAME]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _29218 = (int)*(((s1_ptr)_2)->base + _s_56418);
        DeRef(_name_56420);
        _2 = (int)SEQ_PTR(_29218);
        if (!IS_ATOM_INT(_25S_NAME_11913)){
            _name_56420 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
        }
        else{
            _name_56420 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
        }
        Ref(_name_56420);
        _29218 = NOVALUE;
L1B: 

        /** 						if sequence(var_code) then*/
        _29220 = IS_SEQUENCE(_var_code_56419);
        if (_29220 == 0)
        {
            _29220 = NOVALUE;
            goto L1C; // [774] 826
        }
        else{
            _29220 = NOVALUE;
        }

        /** 							putback( tok )*/
        Ref(_tok_56416);
        _30putback(_tok_56416);

        /** 							start_playback(var_code)*/
        Ref(_var_code_56419);
        _30start_playback(_var_code_56419);

        /** 							Expr()*/
        _30Expr();

        /** 							if on_arg < n then*/
        if (_30on_arg_55903 >= _n_56412)
        goto L16; // [795] 630

        /** 								private_list = append(private_list,name)*/
        RefDS(_name_56420);
        Append(&_30private_list_55901, _30private_list_55901, _name_56420);

        /** 								private_sym &= Top()*/
        _29223 = _37Top();
        if (IS_SEQUENCE(_25private_sym_12395) && IS_ATOM(_29223)) {
            Ref(_29223);
            Append(&_25private_sym_12395, _25private_sym_12395, _29223);
        }
        else if (IS_ATOM(_25private_sym_12395) && IS_SEQUENCE(_29223)) {
        }
        else {
            Concat((object_ptr)&_25private_sym_12395, _25private_sym_12395, _29223);
        }
        DeRef(_29223);
        _29223 = NOVALUE;
        goto L16; // [823] 630
L1C: 

        /** 							CompileErr(29, on_arg)*/
        _43CompileErr(29, _30on_arg_55903, 0);

        /** 		  		    end while*/
        goto L16; // [838] 630
L17: 

        /** 					short_circuit += 1*/
        _30short_circuit_55171 = _30short_circuit_55171 + 1;

        /** 					if backed_up_tok[$][T_ID] = PLAYBACK_ENDS then*/
        if (IS_SEQUENCE(_30backed_up_tok_55178)){
                _29226 = SEQ_PTR(_30backed_up_tok_55178)->length;
        }
        else {
            _29226 = 1;
        }
        _2 = (int)SEQ_PTR(_30backed_up_tok_55178);
        _29227 = (int)*(((s1_ptr)_2)->base + _29226);
        _2 = (int)SEQ_PTR(_29227);
        _29228 = (int)*(((s1_ptr)_2)->base + 1);
        _29227 = NOVALUE;
        if (binary_op_a(NOTEQ, _29228, 505)){
            _29228 = NOVALUE;
            goto L1D; // [868] 880
        }
        _29228 = NOVALUE;

        /** 						backed_up_tok = {}*/
        RefDS(_22682);
        DeRefDS(_30backed_up_tok_55178);
        _30backed_up_tok_55178 = _22682;
L1D: 

        /** 					restore_parseargs_states()*/
        _30restore_parseargs_states();

        /** 					return*/
        DeRef(_tok_56416);
        DeRef(_var_code_56419);
        DeRef(_name_56420);
        return;
        goto L1E; // [889] 906
L12: 

        /** 					putback(tok)*/
        Ref(_tok_56416);
        _30putback(_tok_56416);

        /** 					tok_match(COMMA)*/
        _30tok_match(-30, 0);
L1E: 
L11: 
LF: 

        /** 	end for*/
        _i_56449 = _i_56449 + 1;
        goto L3; // [910] 168
L4: 
        ;
    }

    /** 	tok = next_token()*/
    _0 = _tok_56416;
    _tok_56416 = _30next_token();
    DeRef(_0);

    /** 	short_circuit += 1*/
    _30short_circuit_55171 = _30short_circuit_55171 + 1;

    /** 	if tok[T_ID] != RIGHT_ROUND then*/
    _2 = (int)SEQ_PTR(_tok_56416);
    _29232 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _29232, -27)){
        _29232 = NOVALUE;
        goto L1F; // [938] 980
    }
    _29232 = NOVALUE;

    /** 		if tok[T_ID] = COMMA then*/
    _2 = (int)SEQ_PTR(_tok_56416);
    _29234 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29234, -30)){
        _29234 = NOVALUE;
        goto L20; // [952] 965
    }
    _29234 = NOVALUE;

    /** 			WrongNumberArgs(subsym, "only ")*/
    RefDS(_29236);
    _30WrongNumberArgs(_subsym_56411, _29236);
    goto L21; // [962] 979
L20: 

    /** 			putback(tok)*/
    Ref(_tok_56416);
    _30putback(_tok_56416);

    /** 			tok_match(RIGHT_ROUND)*/
    _30tok_match(-27, 0);
L21: 
L1F: 

    /** 	restore_parseargs_states()*/
    _30restore_parseargs_states();

    /** end procedure*/
    DeRef(_tok_56416);
    DeRef(_var_code_56419);
    DeRef(_name_56420);
    return;
    ;
}


void _30Forward_var(int _tok_56625, int _init_check_56626, int _op_56627)
{
    int _ref_56631 = NOVALUE;
    int _29240 = NOVALUE;
    int _29238 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_op_56627)) {
        _1 = (long)(DBL_PTR(_op_56627)->dbl);
        if (UNIQUE(DBL_PTR(_op_56627)) && (DBL_PTR(_op_56627)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_op_56627);
        _op_56627 = _1;
    }

    /** 	ref = new_forward_reference( VARIABLE, tok[T_SYM], op )*/
    _2 = (int)SEQ_PTR(_tok_56625);
    _29238 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_29238);
    _ref_56631 = _29new_forward_reference(-100, _29238, _op_56627);
    _29238 = NOVALUE;
    if (!IS_ATOM_INT(_ref_56631)) {
        _1 = (long)(DBL_PTR(_ref_56631)->dbl);
        if (UNIQUE(DBL_PTR(_ref_56631)) && (DBL_PTR(_ref_56631)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_56631);
        _ref_56631 = _1;
    }

    /** 	emit_opnd( - ref )*/
    if ((unsigned long)_ref_56631 == 0xC0000000)
    _29240 = (int)NewDouble((double)-0xC0000000);
    else
    _29240 = - _ref_56631;
    _37emit_opnd(_29240);
    _29240 = NOVALUE;

    /** 	if init_check != -1 then*/
    if (_init_check_56626 == -1)
    goto L1; // [33] 44

    /** 		Forward_InitCheck( tok, init_check )*/
    Ref(_tok_56625);
    _30Forward_InitCheck(_tok_56625, _init_check_56626);
L1: 

    /** end procedure*/
    DeRef(_tok_56625);
    return;
    ;
}


void _30Forward_call(int _tok_56644, int _opcode_56645)
{
    int _args_56648 = NOVALUE;
    int _proc_56650 = NOVALUE;
    int _tok_id_56653 = NOVALUE;
    int _id_56660 = NOVALUE;
    int _fc_pc_56683 = NOVALUE;
    int _29259 = NOVALUE;
    int _29258 = NOVALUE;
    int _29255 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer args = 0*/
    _args_56648 = 0;

    /** 	symtab_index proc = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_56644);
    _proc_56650 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_proc_56650)){
        _proc_56650 = (long)DBL_PTR(_proc_56650)->dbl;
    }

    /** 	integer tok_id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_56644);
    _tok_id_56653 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_tok_id_56653)){
        _tok_id_56653 = (long)DBL_PTR(_tok_id_56653)->dbl;
    }

    /** 	remove_symbol( proc )*/
    _52remove_symbol(_proc_56650);

    /** 	short_circuit -= 1*/
    _30short_circuit_55171 = _30short_circuit_55171 - 1;

    /** 	while 1 do*/
L1: 

    /** 		tok = next_token()*/
    _0 = _tok_56644;
    _tok_56644 = _30next_token();
    DeRef(_0);

    /** 		integer id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_56644);
    _id_56660 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_id_56660)){
        _id_56660 = (long)DBL_PTR(_id_56660)->dbl;
    }

    /** 		switch id do*/
    _0 = _id_56660;
    switch ( _0 ){ 

        /** 			case COMMA then*/
        case -30:

        /** 				emit_opnd( 0 ) -- clean this up later*/
        _37emit_opnd(0);

        /** 				args += 1*/
        _args_56648 = _args_56648 + 1;
        goto L2; // [83] 166

        /** 			case RIGHT_ROUND then*/
        case -27:

        /** 				exit*/
        goto L3; // [93] 173
        goto L2; // [95] 166

        /** 			case else*/
        default:

        /** 				putback( tok )*/
        Ref(_tok_56644);
        _30putback(_tok_56644);

        /** 				call_proc( forward_expr, {} )*/
        _0 = (int)_00[_30forward_expr_56188].addr;
        (*(int (*)())_0)(
                             );

        /** 				args += 1*/
        _args_56648 = _args_56648 + 1;

        /** 				tok = next_token()*/
        _0 = _tok_56644;
        _tok_56644 = _30next_token();
        DeRef(_0);

        /** 				id = tok[T_ID]*/
        _2 = (int)SEQ_PTR(_tok_56644);
        _id_56660 = (int)*(((s1_ptr)_2)->base + 1);
        if (!IS_ATOM_INT(_id_56660)){
            _id_56660 = (long)DBL_PTR(_id_56660)->dbl;
        }

        /** 				if id = RIGHT_ROUND then*/
        if (_id_56660 != -27)
        goto L4; // [138] 149

        /** 					exit*/
        goto L3; // [146] 173
L4: 

        /** 				if id != COMMA then*/
        if (_id_56660 == -30)
        goto L5; // [153] 165

        /** 						CompileErr(69)*/
        RefDS(_22682);
        _43CompileErr(69, _22682, 0);
L5: 
    ;}L2: 

    /** 	end while*/
    goto L1; // [170] 46
L3: 

    /** 	integer fc_pc = length( Code ) + 1*/
    if (IS_SEQUENCE(_25Code_12355)){
            _29255 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29255 = 1;
    }
    _fc_pc_56683 = _29255 + 1;
    _29255 = NOVALUE;

    /** 	emit_opnd( args )*/
    _37emit_opnd(_args_56648);

    /** 	op_info1 = proc*/
    _37op_info1_51268 = _proc_56650;

    /** 	if tok_id = QUALIFIED_VARIABLE then*/
    if (_tok_id_56653 != 512)
    goto L6; // [200] 224

    /** 		set_qualified_fwd( SymTab[proc][S_FILE_NO] )*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29258 = (int)*(((s1_ptr)_2)->base + _proc_56650);
    _2 = (int)SEQ_PTR(_29258);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _29259 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _29259 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _29258 = NOVALUE;
    Ref(_29259);
    _60set_qualified_fwd(_29259);
    _29259 = NOVALUE;
    goto L7; // [221] 230
L6: 

    /** 		set_qualified_fwd( -1 )*/
    _60set_qualified_fwd(-1);
L7: 

    /** 	emit_op( opcode )*/
    _37emit_op(_opcode_56645);

    /** 	if not TRANSLATE then*/
    if (_25TRANSLATE_11874 != 0)
    goto L8; // [239] 258

    /** 		if OpTrace then*/
    if (_25OpTrace_12332 == 0)
    {
        goto L9; // [246] 257
    }
    else{
    }

    /** 			emit_op(UPDATE_GLOBALS)*/
    _37emit_op(89);
L9: 
L8: 

    /** 	short_circuit += 1*/
    _30short_circuit_55171 = _30short_circuit_55171 + 1;

    /** end procedure*/
    DeRef(_tok_56644);
    return;
    ;
}


void _30Object_call(int _tok_56711)
{
    int _tok2_56713 = NOVALUE;
    int _tok3_56714 = NOVALUE;
    int _save_factors_56715 = NOVALUE;
    int _save_lhs_subs_level_56716 = NOVALUE;
    int _sym_56718 = NOVALUE;
    int _29319 = NOVALUE;
    int _29317 = NOVALUE;
    int _29316 = NOVALUE;
    int _29313 = NOVALUE;
    int _29312 = NOVALUE;
    int _29308 = NOVALUE;
    int _29302 = NOVALUE;
    int _29299 = NOVALUE;
    int _29298 = NOVALUE;
    int _29297 = NOVALUE;
    int _29295 = NOVALUE;
    int _29294 = NOVALUE;
    int _29292 = NOVALUE;
    int _29291 = NOVALUE;
    int _29288 = NOVALUE;
    int _29287 = NOVALUE;
    int _29286 = NOVALUE;
    int _29284 = NOVALUE;
    int _29283 = NOVALUE;
    int _29281 = NOVALUE;
    int _29280 = NOVALUE;
    int _29279 = NOVALUE;
    int _29278 = NOVALUE;
    int _29276 = NOVALUE;
    int _29275 = NOVALUE;
    int _29273 = NOVALUE;
    int _29272 = NOVALUE;
    int _29269 = NOVALUE;
    int _29267 = NOVALUE;
    int _29266 = NOVALUE;
    int _29264 = NOVALUE;
    int _29263 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	integer save_factors, save_lhs_subs_level*/

    /** 	tok2 = next_token()*/
    _0 = _tok2_56713;
    _tok2_56713 = _30next_token();
    DeRef(_0);

    /** 	if tok2[T_ID] = VARIABLE or tok2[T_ID] = QUALIFIED_VARIABLE then*/
    _2 = (int)SEQ_PTR(_tok2_56713);
    _29263 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29263)) {
        _29264 = (_29263 == -100);
    }
    else {
        _29264 = binary_op(EQUALS, _29263, -100);
    }
    _29263 = NOVALUE;
    if (IS_ATOM_INT(_29264)) {
        if (_29264 != 0) {
            goto L1; // [22] 43
        }
    }
    else {
        if (DBL_PTR(_29264)->dbl != 0.0) {
            goto L1; // [22] 43
        }
    }
    _2 = (int)SEQ_PTR(_tok2_56713);
    _29266 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29266)) {
        _29267 = (_29266 == 512);
    }
    else {
        _29267 = binary_op(EQUALS, _29266, 512);
    }
    _29266 = NOVALUE;
    if (_29267 == 0) {
        DeRef(_29267);
        _29267 = NOVALUE;
        goto L2; // [39] 586
    }
    else {
        if (!IS_ATOM_INT(_29267) && DBL_PTR(_29267)->dbl == 0.0){
            DeRef(_29267);
            _29267 = NOVALUE;
            goto L2; // [39] 586
        }
        DeRef(_29267);
        _29267 = NOVALUE;
    }
    DeRef(_29267);
    _29267 = NOVALUE;
L1: 

    /** 		tok3 = next_token()*/
    _0 = _tok3_56714;
    _tok3_56714 = _30next_token();
    DeRef(_0);

    /** 		if tok3[T_ID] = RIGHT_ROUND then*/
    _2 = (int)SEQ_PTR(_tok3_56714);
    _29269 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29269, -27)){
        _29269 = NOVALUE;
        goto L3; // [58] 155
    }
    _29269 = NOVALUE;

    /** 			sym = tok2[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok2_56713);
    _sym_56718 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_sym_56718)){
        _sym_56718 = (long)DBL_PTR(_sym_56718)->dbl;
    }

    /** 			if SymTab[sym][S_SCOPE] = SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29272 = (int)*(((s1_ptr)_2)->base + _sym_56718);
    _2 = (int)SEQ_PTR(_29272);
    _29273 = (int)*(((s1_ptr)_2)->base + 4);
    _29272 = NOVALUE;
    if (binary_op_a(NOTEQ, _29273, 9)){
        _29273 = NOVALUE;
        goto L4; // [88] 108
    }
    _29273 = NOVALUE;

    /** 				Forward_var( tok2 )*/
    _2 = (int)SEQ_PTR(_tok2_56713);
    _29275 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_tok2_56713);
    Ref(_29275);
    _30Forward_var(_tok2_56713, -1, _29275);
    _29275 = NOVALUE;
    goto L5; // [105] 147
L4: 

    /** 				SymTab[sym][S_USAGE] = or_bits(SymTab[sym][S_USAGE], U_READ)*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_56718 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29278 = (int)*(((s1_ptr)_2)->base + _sym_56718);
    _2 = (int)SEQ_PTR(_29278);
    _29279 = (int)*(((s1_ptr)_2)->base + 5);
    _29278 = NOVALUE;
    if (IS_ATOM_INT(_29279)) {
        {unsigned long tu;
             tu = (unsigned long)_29279 | (unsigned long)1;
             _29280 = MAKE_UINT(tu);
        }
    }
    else {
        _29280 = binary_op(OR_BITS, _29279, 1);
    }
    _29279 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _29280;
    if( _1 != _29280 ){
        DeRef(_1);
    }
    _29280 = NOVALUE;
    _29276 = NOVALUE;

    /** 				emit_opnd(sym)*/
    _37emit_opnd(_sym_56718);
L5: 

    /** 			putback( tok3 )*/
    Ref(_tok3_56714);
    _30putback(_tok3_56714);
    goto L6; // [152] 575
L3: 

    /** 		elsif tok3[T_ID] = COMMA then*/
    _2 = (int)SEQ_PTR(_tok3_56714);
    _29281 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29281, -30)){
        _29281 = NOVALUE;
        goto L7; // [165] 184
    }
    _29281 = NOVALUE;

    /** 			WrongNumberArgs(tok[T_SYM], "")*/
    _2 = (int)SEQ_PTR(_tok_56711);
    _29283 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_29283);
    RefDS(_22682);
    _30WrongNumberArgs(_29283, _22682);
    _29283 = NOVALUE;
    goto L6; // [181] 575
L7: 

    /** 		elsif tok3[T_ID] = LEFT_ROUND then*/
    _2 = (int)SEQ_PTR(_tok3_56714);
    _29284 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29284, -26)){
        _29284 = NOVALUE;
        goto L8; // [194] 244
    }
    _29284 = NOVALUE;

    /** 			if SymTab[tok2[T_SYM]][S_SCOPE] = SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_tok2_56713);
    _29286 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_29286)){
        _29287 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29286)->dbl));
    }
    else{
        _29287 = (int)*(((s1_ptr)_2)->base + _29286);
    }
    _2 = (int)SEQ_PTR(_29287);
    _29288 = (int)*(((s1_ptr)_2)->base + 4);
    _29287 = NOVALUE;
    if (binary_op_a(NOTEQ, _29288, 9)){
        _29288 = NOVALUE;
        goto L9; // [220] 235
    }
    _29288 = NOVALUE;

    /** 				Forward_call( tok2, FUNC_FORWARD )*/
    Ref(_tok2_56713);
    _30Forward_call(_tok2_56713, 196);
    goto L6; // [232] 575
L9: 

    /** 				Function_call( tok2 )*/
    Ref(_tok2_56713);
    _30Function_call(_tok2_56713);
    goto L6; // [241] 575
L8: 

    /** 			sym = tok2[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok2_56713);
    _sym_56718 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_sym_56718)){
        _sym_56718 = (long)DBL_PTR(_sym_56718)->dbl;
    }

    /** 			if SymTab[sym][S_SCOPE] = SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29291 = (int)*(((s1_ptr)_2)->base + _sym_56718);
    _2 = (int)SEQ_PTR(_29291);
    _29292 = (int)*(((s1_ptr)_2)->base + 4);
    _29291 = NOVALUE;
    if (binary_op_a(NOTEQ, _29292, 9)){
        _29292 = NOVALUE;
        goto LA; // [270] 292
    }
    _29292 = NOVALUE;

    /** 				Forward_var( tok2, TRUE )*/
    _2 = (int)SEQ_PTR(_tok2_56713);
    _29294 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_tok2_56713);
    Ref(_29294);
    _30Forward_var(_tok2_56713, _5TRUE_244, _29294);
    _29294 = NOVALUE;
    goto LB; // [289] 339
LA: 

    /** 				SymTab[sym][S_USAGE] = or_bits(SymTab[sym][S_USAGE], U_READ)*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_56718 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29297 = (int)*(((s1_ptr)_2)->base + _sym_56718);
    _2 = (int)SEQ_PTR(_29297);
    _29298 = (int)*(((s1_ptr)_2)->base + 5);
    _29297 = NOVALUE;
    if (IS_ATOM_INT(_29298)) {
        {unsigned long tu;
             tu = (unsigned long)_29298 | (unsigned long)1;
             _29299 = MAKE_UINT(tu);
        }
    }
    else {
        _29299 = binary_op(OR_BITS, _29298, 1);
    }
    _29298 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _29299;
    if( _1 != _29299 ){
        DeRef(_1);
    }
    _29299 = NOVALUE;
    _29295 = NOVALUE;

    /** 				InitCheck(sym, TRUE)*/
    _30InitCheck(_sym_56718, _5TRUE_244);

    /** 				emit_opnd(sym)*/
    _37emit_opnd(_sym_56718);
LB: 

    /** 			if sym = left_sym then*/
    if (_sym_56718 != _30left_sym_55212)
    goto LC; // [343] 353

    /** 				lhs_subs_level = 0*/
    _30lhs_subs_level_55210 = 0;
LC: 

    /** 			tok2 = tok3*/
    Ref(_tok3_56714);
    DeRef(_tok2_56713);
    _tok2_56713 = _tok3_56714;

    /** 			current_sequence = append(current_sequence, sym)*/
    Append(&_37current_sequence_51276, _37current_sequence_51276, _sym_56718);

    /** 			while tok2[T_ID] = LEFT_SQUARE do*/
LD: 
    _2 = (int)SEQ_PTR(_tok2_56713);
    _29302 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29302, -28)){
        _29302 = NOVALUE;
        goto LE; // [381] 551
    }
    _29302 = NOVALUE;

    /** 				subs_depth += 1*/
    _30subs_depth_55213 = _30subs_depth_55213 + 1;

    /** 				if lhs_subs_level >= 0 then*/
    if (_30lhs_subs_level_55210 < 0)
    goto LF; // [397] 410

    /** 					lhs_subs_level += 1*/
    _30lhs_subs_level_55210 = _30lhs_subs_level_55210 + 1;
LF: 

    /** 				save_factors = factors*/
    _save_factors_56715 = _30factors_55209;

    /** 				save_lhs_subs_level = lhs_subs_level*/
    _save_lhs_subs_level_56716 = _30lhs_subs_level_55210;

    /** 				call_proc(forward_expr, {})*/
    _0 = (int)_00[_30forward_expr_56188].addr;
    (*(int (*)())_0)(
                         );

    /** 				tok2 = next_token()*/
    _0 = _tok2_56713;
    _tok2_56713 = _30next_token();
    DeRef(_0);

    /** 				if tok2[T_ID] = SLICE then*/
    _2 = (int)SEQ_PTR(_tok2_56713);
    _29308 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29308, 513)){
        _29308 = NOVALUE;
        goto L10; // [446] 484
    }
    _29308 = NOVALUE;

    /** 					call_proc(forward_expr, {})*/
    _0 = (int)_00[_30forward_expr_56188].addr;
    (*(int (*)())_0)(
                         );

    /** 					emit_op(RHS_SLICE)*/
    _37emit_op(46);

    /** 					tok_match(RIGHT_SQUARE)*/
    _30tok_match(-29, 0);

    /** 					tok2 = next_token()*/
    _0 = _tok2_56713;
    _tok2_56713 = _30next_token();
    DeRef(_0);

    /** 					exit*/
    goto LE; // [479] 551
    goto L11; // [481] 531
L10: 

    /** 					putback(tok2)*/
    Ref(_tok2_56713);
    _30putback(_tok2_56713);

    /** 					tok_match(RIGHT_SQUARE)*/
    _30tok_match(-29, 0);

    /** 					subs_depth -= 1*/
    _30subs_depth_55213 = _30subs_depth_55213 - 1;

    /** 					current_sequence = current_sequence[1..$-1]*/
    if (IS_SEQUENCE(_37current_sequence_51276)){
            _29312 = SEQ_PTR(_37current_sequence_51276)->length;
    }
    else {
        _29312 = 1;
    }
    _29313 = _29312 - 1;
    _29312 = NOVALUE;
    rhs_slice_target = (object_ptr)&_37current_sequence_51276;
    RHS_Slice(_37current_sequence_51276, 1, _29313);

    /** 					emit_op(RHS_SUBS)*/
    _37emit_op(25);
L11: 

    /** 				factors = save_factors*/
    _30factors_55209 = _save_factors_56715;

    /** 				lhs_subs_level = save_lhs_subs_level*/
    _30lhs_subs_level_55210 = _save_lhs_subs_level_56716;

    /** 				tok2 = next_token()*/
    _0 = _tok2_56713;
    _tok2_56713 = _30next_token();
    DeRef(_0);

    /** 			end while*/
    goto LD; // [548] 373
LE: 

    /** 			current_sequence = current_sequence[1..$-1]*/
    if (IS_SEQUENCE(_37current_sequence_51276)){
            _29316 = SEQ_PTR(_37current_sequence_51276)->length;
    }
    else {
        _29316 = 1;
    }
    _29317 = _29316 - 1;
    _29316 = NOVALUE;
    rhs_slice_target = (object_ptr)&_37current_sequence_51276;
    RHS_Slice(_37current_sequence_51276, 1, _29317);

    /** 			putback(tok2)*/
    Ref(_tok2_56713);
    _30putback(_tok2_56713);
L6: 

    /** 		tok_match( RIGHT_ROUND )*/
    _30tok_match(-27, 0);
    goto L12; // [583] 603
L2: 

    /** 		putback(tok2)*/
    Ref(_tok2_56713);
    _30putback(_tok2_56713);

    /** 		ParseArgs(tok[T_SYM])*/
    _2 = (int)SEQ_PTR(_tok_56711);
    _29319 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_29319);
    _30ParseArgs(_29319);
    _29319 = NOVALUE;
L12: 

    /** end procedure*/
    DeRef(_tok_56711);
    DeRef(_tok2_56713);
    DeRef(_tok3_56714);
    _29286 = NOVALUE;
    DeRef(_29264);
    _29264 = NOVALUE;
    DeRef(_29313);
    _29313 = NOVALUE;
    DeRef(_29317);
    _29317 = NOVALUE;
    return;
    ;
}


void _30Function_call(int _tok_56856)
{
    int _id_56857 = NOVALUE;
    int _scope_56858 = NOVALUE;
    int _opcode_56859 = NOVALUE;
    int _e_56860 = NOVALUE;
    int _29360 = NOVALUE;
    int _29359 = NOVALUE;
    int _29358 = NOVALUE;
    int _29357 = NOVALUE;
    int _29356 = NOVALUE;
    int _29355 = NOVALUE;
    int _29354 = NOVALUE;
    int _29352 = NOVALUE;
    int _29351 = NOVALUE;
    int _29349 = NOVALUE;
    int _29348 = NOVALUE;
    int _29347 = NOVALUE;
    int _29346 = NOVALUE;
    int _29345 = NOVALUE;
    int _29344 = NOVALUE;
    int _29343 = NOVALUE;
    int _29342 = NOVALUE;
    int _29341 = NOVALUE;
    int _29340 = NOVALUE;
    int _29339 = NOVALUE;
    int _29338 = NOVALUE;
    int _29337 = NOVALUE;
    int _29336 = NOVALUE;
    int _29335 = NOVALUE;
    int _29333 = NOVALUE;
    int _29331 = NOVALUE;
    int _29330 = NOVALUE;
    int _29328 = NOVALUE;
    int _29326 = NOVALUE;
    int _29325 = NOVALUE;
    int _29324 = NOVALUE;
    int _29323 = NOVALUE;
    int _29321 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_56856);
    _id_56857 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_id_56857)){
        _id_56857 = (long)DBL_PTR(_id_56857)->dbl;
    }

    /** 	if id = FUNC or id = TYPE then*/
    _29321 = (_id_56857 == 501);
    if (_29321 != 0) {
        goto L1; // [19] 34
    }
    _29323 = (_id_56857 == 504);
    if (_29323 == 0)
    {
        DeRef(_29323);
        _29323 = NOVALUE;
        goto L2; // [30] 46
    }
    else{
        DeRef(_29323);
        _29323 = NOVALUE;
    }
L1: 

    /** 		UndefinedVar( tok[T_SYM] )*/
    _2 = (int)SEQ_PTR(_tok_56856);
    _29324 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_29324);
    _30UndefinedVar(_29324);
    _29324 = NOVALUE;
L2: 

    /** 	e = SymTab[tok[T_SYM]][S_EFFECT]*/
    _2 = (int)SEQ_PTR(_tok_56856);
    _29325 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_29325)){
        _29326 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29325)->dbl));
    }
    else{
        _29326 = (int)*(((s1_ptr)_2)->base + _29325);
    }
    _2 = (int)SEQ_PTR(_29326);
    _e_56860 = (int)*(((s1_ptr)_2)->base + 23);
    if (!IS_ATOM_INT(_e_56860)){
        _e_56860 = (long)DBL_PTR(_e_56860)->dbl;
    }
    _29326 = NOVALUE;

    /** 	if e then*/
    if (_e_56860 == 0)
    {
        goto L3; // [70] 229
    }
    else{
    }

    /** 		if e = E_ALL_EFFECT or tok[T_SYM] > left_sym then*/
    _29328 = (_e_56860 == 1073741823);
    if (_29328 != 0) {
        goto L4; // [81] 102
    }
    _2 = (int)SEQ_PTR(_tok_56856);
    _29330 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_29330)) {
        _29331 = (_29330 > _30left_sym_55212);
    }
    else {
        _29331 = binary_op(GREATER, _29330, _30left_sym_55212);
    }
    _29330 = NOVALUE;
    if (_29331 == 0) {
        DeRef(_29331);
        _29331 = NOVALUE;
        goto L5; // [98] 111
    }
    else {
        if (!IS_ATOM_INT(_29331) && DBL_PTR(_29331)->dbl == 0.0){
            DeRef(_29331);
            _29331 = NOVALUE;
            goto L5; // [98] 111
        }
        DeRef(_29331);
        _29331 = NOVALUE;
    }
    DeRef(_29331);
    _29331 = NOVALUE;
L4: 

    /** 			side_effect_calls = or_bits(side_effect_calls, e)*/
    {unsigned long tu;
         tu = (unsigned long)_30side_effect_calls_55208 | (unsigned long)_e_56860;
         _30side_effect_calls_55208 = MAKE_UINT(tu);
    }
L5: 

    /** 		SymTab[CurrentSub][S_EFFECT] = or_bits(SymTab[CurrentSub][S_EFFECT], e)*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25CurrentSub_12270 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29335 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_29335);
    _29336 = (int)*(((s1_ptr)_2)->base + 23);
    _29335 = NOVALUE;
    if (IS_ATOM_INT(_29336)) {
        {unsigned long tu;
             tu = (unsigned long)_29336 | (unsigned long)_e_56860;
             _29337 = MAKE_UINT(tu);
        }
    }
    else {
        _29337 = binary_op(OR_BITS, _29336, _e_56860);
    }
    _29336 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 23);
    _1 = *(int *)_2;
    *(int *)_2 = _29337;
    if( _1 != _29337 ){
        DeRef(_1);
    }
    _29337 = NOVALUE;
    _29333 = NOVALUE;

    /** 		if short_circuit > 0 and short_circuit_B and*/
    _29338 = (_30short_circuit_55171 > 0);
    if (_29338 == 0) {
        _29339 = 0;
        goto L6; // [154] 164
    }
    _29339 = (_30short_circuit_B_55173 != 0);
L6: 
    if (_29339 == 0) {
        goto L7; // [164] 228
    }
    _29341 = find_from(_id_56857, _28FUNC_TOKS_11869, 1);
    if (_29341 == 0)
    {
        _29341 = NOVALUE;
        goto L7; // [176] 228
    }
    else{
        _29341 = NOVALUE;
    }

    /** 			Warning(219, short_circuit_warning_flag,*/
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _29342 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    Ref(_29342);
    RefDS(_22682);
    _29343 = _9abbreviate_path(_29342, _22682);
    _29342 = NOVALUE;
    _2 = (int)SEQ_PTR(_tok_56856);
    _29344 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_29344)){
        _29345 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29344)->dbl));
    }
    else{
        _29345 = (int)*(((s1_ptr)_2)->base + _29344);
    }
    _2 = (int)SEQ_PTR(_29345);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _29346 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _29346 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _29345 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _29343;
    *((int *)(_2+8)) = _25line_number_12263;
    Ref(_29346);
    *((int *)(_2+12)) = _29346;
    _29347 = MAKE_SEQ(_1);
    _29346 = NOVALUE;
    _29343 = NOVALUE;
    _43Warning(219, 2, _29347);
    _29347 = NOVALUE;
L7: 
L3: 

    /** 	tok_match(LEFT_ROUND)*/
    _30tok_match(-26, 0);

    /** 	scope = SymTab[tok[T_SYM]][S_SCOPE]*/
    _2 = (int)SEQ_PTR(_tok_56856);
    _29348 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_29348)){
        _29349 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29348)->dbl));
    }
    else{
        _29349 = (int)*(((s1_ptr)_2)->base + _29348);
    }
    _2 = (int)SEQ_PTR(_29349);
    _scope_56858 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_scope_56858)){
        _scope_56858 = (long)DBL_PTR(_scope_56858)->dbl;
    }
    _29349 = NOVALUE;

    /** 	opcode = SymTab[tok[T_SYM]][S_OPCODE]*/
    _2 = (int)SEQ_PTR(_tok_56856);
    _29351 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_29351)){
        _29352 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29351)->dbl));
    }
    else{
        _29352 = (int)*(((s1_ptr)_2)->base + _29351);
    }
    _2 = (int)SEQ_PTR(_29352);
    _opcode_56859 = (int)*(((s1_ptr)_2)->base + 21);
    if (!IS_ATOM_INT(_opcode_56859)){
        _opcode_56859 = (long)DBL_PTR(_opcode_56859)->dbl;
    }
    _29352 = NOVALUE;

    /** 	if equal(SymTab[tok[T_SYM]][S_NAME],"object") and scope = SC_PREDEF then*/
    _2 = (int)SEQ_PTR(_tok_56856);
    _29354 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_29354)){
        _29355 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29354)->dbl));
    }
    else{
        _29355 = (int)*(((s1_ptr)_2)->base + _29354);
    }
    _2 = (int)SEQ_PTR(_29355);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _29356 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _29356 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _29355 = NOVALUE;
    if (_29356 == _25343)
    _29357 = 1;
    else if (IS_ATOM_INT(_29356) && IS_ATOM_INT(_25343))
    _29357 = 0;
    else
    _29357 = (compare(_29356, _25343) == 0);
    _29356 = NOVALUE;
    if (_29357 == 0) {
        goto L8; // [305] 327
    }
    _29359 = (_scope_56858 == 7);
    if (_29359 == 0)
    {
        DeRef(_29359);
        _29359 = NOVALUE;
        goto L8; // [316] 327
    }
    else{
        DeRef(_29359);
        _29359 = NOVALUE;
    }

    /** 		Object_call( tok )*/
    Ref(_tok_56856);
    _30Object_call(_tok_56856);
    goto L9; // [324] 339
L8: 

    /** 		ParseArgs(tok[T_SYM])*/
    _2 = (int)SEQ_PTR(_tok_56856);
    _29360 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_29360);
    _30ParseArgs(_29360);
    _29360 = NOVALUE;
L9: 

    /** 	if scope = SC_PREDEF then*/
    if (_scope_56858 != 7)
    goto LA; // [343] 355

    /** 		emit_op(opcode)*/
    _37emit_op(_opcode_56859);
    goto LB; // [352] 393
LA: 

    /** 		op_info1 = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_56856);
    _37op_info1_51268 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_37op_info1_51268)){
        _37op_info1_51268 = (long)DBL_PTR(_37op_info1_51268)->dbl;
    }

    /** 		emit_or_inline()*/
    _67emit_or_inline();

    /** 		if not TRANSLATE then*/
    if (_25TRANSLATE_11874 != 0)
    goto LC; // [373] 392

    /** 			if OpTrace then*/
    if (_25OpTrace_12332 == 0)
    {
        goto LD; // [380] 391
    }
    else{
    }

    /** 				emit_op(UPDATE_GLOBALS)*/
    _37emit_op(89);
LD: 
LC: 
LB: 

    /** end procedure*/
    DeRef(_tok_56856);
    DeRef(_29321);
    _29321 = NOVALUE;
    _29325 = NOVALUE;
    DeRef(_29328);
    _29328 = NOVALUE;
    DeRef(_29338);
    _29338 = NOVALUE;
    _29344 = NOVALUE;
    _29348 = NOVALUE;
    _29351 = NOVALUE;
    _29354 = NOVALUE;
    return;
    ;
}


void _30Factor()
{
    int _tok_56964 = NOVALUE;
    int _id_56965 = NOVALUE;
    int _n_56966 = NOVALUE;
    int _save_factors_56967 = NOVALUE;
    int _save_lhs_subs_level_56968 = NOVALUE;
    int _sym_56970 = NOVALUE;
    int _forward_57001 = NOVALUE;
    int _29422 = NOVALUE;
    int _29421 = NOVALUE;
    int _29420 = NOVALUE;
    int _29418 = NOVALUE;
    int _29417 = NOVALUE;
    int _29416 = NOVALUE;
    int _29415 = NOVALUE;
    int _29414 = NOVALUE;
    int _29412 = NOVALUE;
    int _29408 = NOVALUE;
    int _29407 = NOVALUE;
    int _29404 = NOVALUE;
    int _29403 = NOVALUE;
    int _29399 = NOVALUE;
    int _29393 = NOVALUE;
    int _29388 = NOVALUE;
    int _29387 = NOVALUE;
    int _29386 = NOVALUE;
    int _29384 = NOVALUE;
    int _29383 = NOVALUE;
    int _29381 = NOVALUE;
    int _29379 = NOVALUE;
    int _29378 = NOVALUE;
    int _29377 = NOVALUE;
    int _29375 = NOVALUE;
    int _29368 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	integer id, n*/

    /** 	integer save_factors, save_lhs_subs_level*/

    /** 	factors += 1*/
    _30factors_55209 = _30factors_55209 + 1;

    /** 	tok = next_token()*/
    _0 = _tok_56964;
    _tok_56964 = _30next_token();
    DeRef(_0);

    /** 	id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_56964);
    _id_56965 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_id_56965)){
        _id_56965 = (long)DBL_PTR(_id_56965)->dbl;
    }

    /** 	if id = RECORDED then*/
    if (_id_56965 != 508)
    goto L1; // [32] 59

    /** 		tok = read_recorded_token(tok[T_SYM])*/
    _2 = (int)SEQ_PTR(_tok_56964);
    _29368 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_29368);
    _0 = _tok_56964;
    _tok_56964 = _30read_recorded_token(_29368);
    DeRef(_0);
    _29368 = NOVALUE;

    /** 		id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_56964);
    _id_56965 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_id_56965)){
        _id_56965 = (long)DBL_PTR(_id_56965)->dbl;
    }
L1: 

    /** 	switch id label "factor" do*/
    _0 = _id_56965;
    switch ( _0 ){ 

        /** 		case VARIABLE, QUALIFIED_VARIABLE then*/
        case -100:
        case 512:

        /** 			sym = tok[T_SYM]*/
        _2 = (int)SEQ_PTR(_tok_56964);
        _sym_56970 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_sym_56970)){
            _sym_56970 = (long)DBL_PTR(_sym_56970)->dbl;
        }

        /** 			if sym < 0 or SymTab[sym][S_SCOPE] = SC_UNDEFINED then*/
        _29375 = (_sym_56970 < 0);
        if (_29375 != 0) {
            goto L2; // [88] 115
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _29377 = (int)*(((s1_ptr)_2)->base + _sym_56970);
        _2 = (int)SEQ_PTR(_29377);
        _29378 = (int)*(((s1_ptr)_2)->base + 4);
        _29377 = NOVALUE;
        if (IS_ATOM_INT(_29378)) {
            _29379 = (_29378 == 9);
        }
        else {
            _29379 = binary_op(EQUALS, _29378, 9);
        }
        _29378 = NOVALUE;
        if (_29379 == 0) {
            DeRef(_29379);
            _29379 = NOVALUE;
            goto L3; // [111] 177
        }
        else {
            if (!IS_ATOM_INT(_29379) && DBL_PTR(_29379)->dbl == 0.0){
                DeRef(_29379);
                _29379 = NOVALUE;
                goto L3; // [111] 177
            }
            DeRef(_29379);
            _29379 = NOVALUE;
        }
        DeRef(_29379);
        _29379 = NOVALUE;
L2: 

        /** 				token forward = next_token()*/
        _0 = _forward_57001;
        _forward_57001 = _30next_token();
        DeRef(_0);

        /** 				if forward[T_ID] = LEFT_ROUND then*/
        _2 = (int)SEQ_PTR(_forward_57001);
        _29381 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(NOTEQ, _29381, -26)){
            _29381 = NOVALUE;
            goto L4; // [130] 151
        }
        _29381 = NOVALUE;

        /** 					Forward_call( tok, FUNC_FORWARD )*/
        Ref(_tok_56964);
        _30Forward_call(_tok_56964, 196);

        /** 					break "factor"*/
        DeRef(_forward_57001);
        _forward_57001 = NOVALUE;
        goto L5; // [146] 696
        goto L6; // [148] 172
L4: 

        /** 					putback( forward )*/
        Ref(_forward_57001);
        _30putback(_forward_57001);

        /** 					Forward_var( tok, TRUE )*/
        _2 = (int)SEQ_PTR(_tok_56964);
        _29383 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_tok_56964);
        Ref(_29383);
        _30Forward_var(_tok_56964, _5TRUE_244, _29383);
        _29383 = NOVALUE;
L6: 
        DeRef(_forward_57001);
        _forward_57001 = NOVALUE;
        goto L7; // [174] 229
L3: 

        /** 				UndefinedVar(sym)*/
        _30UndefinedVar(_sym_56970);

        /** 				SymTab[sym][S_USAGE] = or_bits(SymTab[sym][S_USAGE], U_READ)*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_sym_56970 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _29386 = (int)*(((s1_ptr)_2)->base + _sym_56970);
        _2 = (int)SEQ_PTR(_29386);
        _29387 = (int)*(((s1_ptr)_2)->base + 5);
        _29386 = NOVALUE;
        if (IS_ATOM_INT(_29387)) {
            {unsigned long tu;
                 tu = (unsigned long)_29387 | (unsigned long)1;
                 _29388 = MAKE_UINT(tu);
            }
        }
        else {
            _29388 = binary_op(OR_BITS, _29387, 1);
        }
        _29387 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = _29388;
        if( _1 != _29388 ){
            DeRef(_1);
        }
        _29388 = NOVALUE;
        _29384 = NOVALUE;

        /** 				InitCheck(sym, TRUE)*/
        _30InitCheck(_sym_56970, _5TRUE_244);

        /** 				emit_opnd(sym)*/
        _37emit_opnd(_sym_56970);
L7: 

        /** 			if sym = left_sym then*/
        if (_sym_56970 != _30left_sym_55212)
        goto L8; // [233] 243

        /** 				lhs_subs_level = 0 -- start counting subscripts*/
        _30lhs_subs_level_55210 = 0;
L8: 

        /** 			short_circuit -= 1*/
        _30short_circuit_55171 = _30short_circuit_55171 - 1;

        /** 			tok = next_token()*/
        _0 = _tok_56964;
        _tok_56964 = _30next_token();
        DeRef(_0);

        /** 			current_sequence = append(current_sequence, sym)*/
        Append(&_37current_sequence_51276, _37current_sequence_51276, _sym_56970);

        /** 			while tok[T_ID] = LEFT_SQUARE do*/
L9: 
        _2 = (int)SEQ_PTR(_tok_56964);
        _29393 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(NOTEQ, _29393, -28)){
            _29393 = NOVALUE;
            goto LA; // [279] 450
        }
        _29393 = NOVALUE;

        /** 				subs_depth += 1*/
        _30subs_depth_55213 = _30subs_depth_55213 + 1;

        /** 				if lhs_subs_level >= 0 then*/
        if (_30lhs_subs_level_55210 < 0)
        goto LB; // [295] 308

        /** 					lhs_subs_level += 1*/
        _30lhs_subs_level_55210 = _30lhs_subs_level_55210 + 1;
LB: 

        /** 				save_factors = factors*/
        _save_factors_56967 = _30factors_55209;

        /** 				save_lhs_subs_level = lhs_subs_level*/
        _save_lhs_subs_level_56968 = _30lhs_subs_level_55210;

        /** 				call_proc(forward_expr, {})*/
        _0 = (int)_00[_30forward_expr_56188].addr;
        (*(int (*)())_0)(
                             );

        /** 				tok = next_token()*/
        _0 = _tok_56964;
        _tok_56964 = _30next_token();
        DeRef(_0);

        /** 				if tok[T_ID] = SLICE then*/
        _2 = (int)SEQ_PTR(_tok_56964);
        _29399 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(NOTEQ, _29399, 513)){
            _29399 = NOVALUE;
            goto LC; // [344] 382
        }
        _29399 = NOVALUE;

        /** 					call_proc(forward_expr, {})*/
        _0 = (int)_00[_30forward_expr_56188].addr;
        (*(int (*)())_0)(
                             );

        /** 					emit_op(RHS_SLICE)*/
        _37emit_op(46);

        /** 					tok_match(RIGHT_SQUARE)*/
        _30tok_match(-29, 0);

        /** 					tok = next_token()*/
        _0 = _tok_56964;
        _tok_56964 = _30next_token();
        DeRef(_0);

        /** 					exit*/
        goto LA; // [377] 450
        goto LD; // [379] 430
LC: 

        /** 					putback(tok)*/
        Ref(_tok_56964);
        _30putback(_tok_56964);

        /** 					tok_match(RIGHT_SQUARE)*/
        _30tok_match(-29, 0);

        /** 					subs_depth -= 1*/
        _30subs_depth_55213 = _30subs_depth_55213 - 1;

        /** 					current_sequence = head( current_sequence, length( current_sequence ) - 1 )*/
        if (IS_SEQUENCE(_37current_sequence_51276)){
                _29403 = SEQ_PTR(_37current_sequence_51276)->length;
        }
        else {
            _29403 = 1;
        }
        _29404 = _29403 - 1;
        _29403 = NOVALUE;
        {
            int len = SEQ_PTR(_37current_sequence_51276)->length;
            int size = (IS_ATOM_INT(_29404)) ? _29404 : (object)(DBL_PTR(_29404)->dbl);
            if (size <= 0){
                DeRef( _37current_sequence_51276 );
                _37current_sequence_51276 = MAKE_SEQ(NewS1(0));
            }
            else if (len <= size) {
                RefDS(_37current_sequence_51276);
                DeRef(_37current_sequence_51276);
                _37current_sequence_51276 = _37current_sequence_51276;
            }
            else{
                Head(SEQ_PTR(_37current_sequence_51276),size+1,&_37current_sequence_51276);
            }
        }
        _29404 = NOVALUE;

        /** 					emit_op(RHS_SUBS) -- current_sequence will be updated*/
        _37emit_op(25);
LD: 

        /** 				factors = save_factors*/
        _30factors_55209 = _save_factors_56967;

        /** 				lhs_subs_level = save_lhs_subs_level*/
        _30lhs_subs_level_55210 = _save_lhs_subs_level_56968;

        /** 				tok = next_token()*/
        _0 = _tok_56964;
        _tok_56964 = _30next_token();
        DeRef(_0);

        /** 			end while*/
        goto L9; // [447] 271
LA: 

        /** 			current_sequence = head( current_sequence, length( current_sequence ) - 1 )*/
        if (IS_SEQUENCE(_37current_sequence_51276)){
                _29407 = SEQ_PTR(_37current_sequence_51276)->length;
        }
        else {
            _29407 = 1;
        }
        _29408 = _29407 - 1;
        _29407 = NOVALUE;
        {
            int len = SEQ_PTR(_37current_sequence_51276)->length;
            int size = (IS_ATOM_INT(_29408)) ? _29408 : (object)(DBL_PTR(_29408)->dbl);
            if (size <= 0){
                DeRef( _37current_sequence_51276 );
                _37current_sequence_51276 = MAKE_SEQ(NewS1(0));
            }
            else if (len <= size) {
                RefDS(_37current_sequence_51276);
                DeRef(_37current_sequence_51276);
                _37current_sequence_51276 = _37current_sequence_51276;
            }
            else{
                Head(SEQ_PTR(_37current_sequence_51276),size+1,&_37current_sequence_51276);
            }
        }
        _29408 = NOVALUE;

        /** 			putback(tok)*/
        Ref(_tok_56964);
        _30putback(_tok_56964);

        /** 			short_circuit += 1*/
        _30short_circuit_55171 = _30short_circuit_55171 + 1;
        goto L5; // [482] 696

        /** 		case DOLLAR then*/
        case -22:

        /** 			tok = next_token()*/
        _0 = _tok_56964;
        _tok_56964 = _30next_token();
        DeRef(_0);

        /** 			putback(tok)*/
        Ref(_tok_56964);
        _30putback(_tok_56964);

        /** 			if tok[T_ID] = RIGHT_BRACE then*/
        _2 = (int)SEQ_PTR(_tok_56964);
        _29412 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(NOTEQ, _29412, -25)){
            _29412 = NOVALUE;
            goto LE; // [508] 526
        }
        _29412 = NOVALUE;

        /** 				gListItem[$] = 0*/
        if (IS_SEQUENCE(_30gListItem_55207)){
                _29414 = SEQ_PTR(_30gListItem_55207)->length;
        }
        else {
            _29414 = 1;
        }
        _2 = (int)SEQ_PTR(_30gListItem_55207);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _30gListItem_55207 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _29414);
        *(int *)_2 = 0;
        goto L5; // [523] 696
LE: 

        /** 				if subs_depth > 0 and length(current_sequence) then*/
        _29415 = (_30subs_depth_55213 > 0);
        if (_29415 == 0) {
            goto LF; // [534] 557
        }
        if (IS_SEQUENCE(_37current_sequence_51276)){
                _29417 = SEQ_PTR(_37current_sequence_51276)->length;
        }
        else {
            _29417 = 1;
        }
        if (_29417 == 0)
        {
            _29417 = NOVALUE;
            goto LF; // [544] 557
        }
        else{
            _29417 = NOVALUE;
        }

        /** 					emit_op(DOLLAR)*/
        _37emit_op(-22);
        goto L5; // [554] 696
LF: 

        /** 					CompileErr(21)*/
        RefDS(_22682);
        _43CompileErr(21, _22682, 0);
        goto L5; // [566] 696

        /** 		case ATOM then*/
        case 502:

        /** 			emit_opnd(tok[T_SYM])*/
        _2 = (int)SEQ_PTR(_tok_56964);
        _29418 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_29418);
        _37emit_opnd(_29418);
        _29418 = NOVALUE;
        goto L5; // [583] 696

        /** 		case LEFT_BRACE then*/
        case -24:

        /** 			n = Expr_list()*/
        _n_56966 = _30Expr_list();
        if (!IS_ATOM_INT(_n_56966)) {
            _1 = (long)(DBL_PTR(_n_56966)->dbl);
            if (UNIQUE(DBL_PTR(_n_56966)) && (DBL_PTR(_n_56966)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_n_56966);
            _n_56966 = _1;
        }

        /** 			tok_match(RIGHT_BRACE)*/
        _30tok_match(-25, 0);

        /** 			op_info1 = n*/
        _37op_info1_51268 = _n_56966;

        /** 			emit_op(RIGHT_BRACE_N)*/
        _37emit_op(31);
        goto L5; // [618] 696

        /** 		case STRING then*/
        case 503:

        /** 			emit_opnd(tok[T_SYM])*/
        _2 = (int)SEQ_PTR(_tok_56964);
        _29420 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_29420);
        _37emit_opnd(_29420);
        _29420 = NOVALUE;
        goto L5; // [635] 696

        /** 		case LEFT_ROUND then*/
        case -26:

        /** 			call_proc(forward_expr, {})*/
        _0 = (int)_00[_30forward_expr_56188].addr;
        (*(int (*)())_0)(
                             );

        /** 			tok_match(RIGHT_ROUND)*/
        _30tok_match(-27, 0);
        goto L5; // [656] 696

        /** 		case FUNC, TYPE, QUALIFIED_FUNC, QUALIFIED_TYPE then*/
        case 501:
        case 504:
        case 520:
        case 522:

        /** 			Function_call( tok )*/
        Ref(_tok_56964);
        _30Function_call(_tok_56964);
        goto L5; // [673] 696

        /** 		case else*/
        default:

        /** 			CompileErr(135, {LexName(id)})*/
        RefDS(_27188);
        _29421 = _37LexName(_id_56965, _27188);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _29421;
        _29422 = MAKE_SEQ(_1);
        _29421 = NOVALUE;
        _43CompileErr(135, _29422, 0);
        _29422 = NOVALUE;
    ;}L5: 

    /** end procedure*/
    DeRef(_tok_56964);
    DeRef(_29375);
    _29375 = NOVALUE;
    DeRef(_29415);
    _29415 = NOVALUE;
    return;
    ;
}


void _30UFactor()
{
    int _tok_57123 = NOVALUE;
    int _29428 = NOVALUE;
    int _29426 = NOVALUE;
    int _29424 = NOVALUE;
    int _0, _1, _2;
    

    /** 	tok = next_token()*/
    _0 = _tok_57123;
    _tok_57123 = _30next_token();
    DeRef(_0);

    /** 	if tok[T_ID] = MINUS then*/
    _2 = (int)SEQ_PTR(_tok_57123);
    _29424 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29424, 10)){
        _29424 = NOVALUE;
        goto L1; // [16] 34
    }
    _29424 = NOVALUE;

    /** 		Factor()*/
    _30Factor();

    /** 		emit_op(UMINUS)*/
    _37emit_op(12);
    goto L2; // [31] 93
L1: 

    /** 	elsif tok[T_ID] = NOT then*/
    _2 = (int)SEQ_PTR(_tok_57123);
    _29426 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29426, 7)){
        _29426 = NOVALUE;
        goto L3; // [44] 62
    }
    _29426 = NOVALUE;

    /** 		Factor()*/
    _30Factor();

    /** 		emit_op(NOT)*/
    _37emit_op(7);
    goto L2; // [59] 93
L3: 

    /** 	elsif tok[T_ID] = PLUS then*/
    _2 = (int)SEQ_PTR(_tok_57123);
    _29428 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29428, 11)){
        _29428 = NOVALUE;
        goto L4; // [72] 83
    }
    _29428 = NOVALUE;

    /** 		Factor()*/
    _30Factor();
    goto L2; // [80] 93
L4: 

    /** 		putback(tok)*/
    Ref(_tok_57123);
    _30putback(_tok_57123);

    /** 		Factor()*/
    _30Factor();
L2: 

    /** end procedure*/
    DeRef(_tok_57123);
    return;
    ;
}


int _30Term()
{
    int _tok_57148 = NOVALUE;
    int _29436 = NOVALUE;
    int _29435 = NOVALUE;
    int _29434 = NOVALUE;
    int _29432 = NOVALUE;
    int _29431 = NOVALUE;
    int _0, _1, _2;
    

    /** 	UFactor()*/
    _30UFactor();

    /** 	tok = next_token()*/
    _0 = _tok_57148;
    _tok_57148 = _30next_token();
    DeRef(_0);

    /** 	while tok[T_ID] = reserved:MULTIPLY or tok[T_ID] = reserved:DIVIDE do*/
L1: 
    _2 = (int)SEQ_PTR(_tok_57148);
    _29431 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29431)) {
        _29432 = (_29431 == 13);
    }
    else {
        _29432 = binary_op(EQUALS, _29431, 13);
    }
    _29431 = NOVALUE;
    if (IS_ATOM_INT(_29432)) {
        if (_29432 != 0) {
            goto L2; // [25] 44
        }
    }
    else {
        if (DBL_PTR(_29432)->dbl != 0.0) {
            goto L2; // [25] 44
        }
    }
    _2 = (int)SEQ_PTR(_tok_57148);
    _29434 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29434)) {
        _29435 = (_29434 == 14);
    }
    else {
        _29435 = binary_op(EQUALS, _29434, 14);
    }
    _29434 = NOVALUE;
    if (_29435 <= 0) {
        if (_29435 == 0) {
            DeRef(_29435);
            _29435 = NOVALUE;
            goto L3; // [40] 69
        }
        else {
            if (!IS_ATOM_INT(_29435) && DBL_PTR(_29435)->dbl == 0.0){
                DeRef(_29435);
                _29435 = NOVALUE;
                goto L3; // [40] 69
            }
            DeRef(_29435);
            _29435 = NOVALUE;
        }
    }
    DeRef(_29435);
    _29435 = NOVALUE;
L2: 

    /** 		UFactor()*/
    _30UFactor();

    /** 		emit_op(tok[T_ID])*/
    _2 = (int)SEQ_PTR(_tok_57148);
    _29436 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_29436);
    _37emit_op(_29436);
    _29436 = NOVALUE;

    /** 		tok = next_token()*/
    _0 = _tok_57148;
    _tok_57148 = _30next_token();
    DeRef(_0);

    /** 	end while*/
    goto L1; // [66] 15
L3: 

    /** 	return tok*/
    DeRef(_29432);
    _29432 = NOVALUE;
    return _tok_57148;
    ;
}


int _30aexpr()
{
    int _tok_57165 = NOVALUE;
    int _id_57166 = NOVALUE;
    int _29443 = NOVALUE;
    int _29442 = NOVALUE;
    int _29440 = NOVALUE;
    int _29439 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer id*/

    /** 	tok = Term()*/
    _0 = _tok_57165;
    _tok_57165 = _30Term();
    DeRef(_0);

    /** 	while tok[T_ID] = PLUS or tok[T_ID] = MINUS do*/
L1: 
    _2 = (int)SEQ_PTR(_tok_57165);
    _29439 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29439)) {
        _29440 = (_29439 == 11);
    }
    else {
        _29440 = binary_op(EQUALS, _29439, 11);
    }
    _29439 = NOVALUE;
    if (IS_ATOM_INT(_29440)) {
        if (_29440 != 0) {
            goto L2; // [25] 46
        }
    }
    else {
        if (DBL_PTR(_29440)->dbl != 0.0) {
            goto L2; // [25] 46
        }
    }
    _2 = (int)SEQ_PTR(_tok_57165);
    _29442 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29442)) {
        _29443 = (_29442 == 10);
    }
    else {
        _29443 = binary_op(EQUALS, _29442, 10);
    }
    _29442 = NOVALUE;
    if (_29443 <= 0) {
        if (_29443 == 0) {
            DeRef(_29443);
            _29443 = NOVALUE;
            goto L3; // [42] 71
        }
        else {
            if (!IS_ATOM_INT(_29443) && DBL_PTR(_29443)->dbl == 0.0){
                DeRef(_29443);
                _29443 = NOVALUE;
                goto L3; // [42] 71
            }
            DeRef(_29443);
            _29443 = NOVALUE;
        }
    }
    DeRef(_29443);
    _29443 = NOVALUE;
L2: 

    /** 		id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_57165);
    _id_57166 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_id_57166)){
        _id_57166 = (long)DBL_PTR(_id_57166)->dbl;
    }

    /** 		tok = Term()*/
    _0 = _tok_57165;
    _tok_57165 = _30Term();
    DeRef(_0);

    /** 		emit_op(id)*/
    _37emit_op(_id_57166);

    /** 	end while*/
    goto L1; // [68] 13
L3: 

    /** 	return tok*/
    DeRef(_29440);
    _29440 = NOVALUE;
    return _tok_57165;
    ;
}


int _30cexpr()
{
    int _tok_57185 = NOVALUE;
    int _concat_count_57186 = NOVALUE;
    int _29447 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer concat_count*/

    /** 	tok = aexpr()*/
    _0 = _tok_57185;
    _tok_57185 = _30aexpr();
    DeRef(_0);

    /** 	concat_count = 0*/
    _concat_count_57186 = 0;

    /** 	while tok[T_ID] = reserved:CONCAT do*/
L1: 
    _2 = (int)SEQ_PTR(_tok_57185);
    _29447 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29447, 15)){
        _29447 = NOVALUE;
        goto L2; // [24] 44
    }
    _29447 = NOVALUE;

    /** 		tok = aexpr()*/
    _0 = _tok_57185;
    _tok_57185 = _30aexpr();
    DeRef(_0);

    /** 		concat_count += 1*/
    _concat_count_57186 = _concat_count_57186 + 1;

    /** 	end while*/
    goto L1; // [41] 18
L2: 

    /** 	if concat_count = 1 then*/
    if (_concat_count_57186 != 1)
    goto L3; // [46] 58

    /** 		emit_op( reserved:CONCAT )*/
    _37emit_op(15);
    goto L4; // [55] 81
L3: 

    /** 	elsif concat_count > 1 then*/
    if (_concat_count_57186 <= 1)
    goto L5; // [60] 80

    /** 		op_info1 = concat_count+1*/
    _37op_info1_51268 = _concat_count_57186 + 1;

    /** 		emit_op(CONCAT_N)*/
    _37emit_op(157);
L5: 
L4: 

    /** 	return tok*/
    return _tok_57185;
    ;
}


int _30rexpr()
{
    int _tok_57206 = NOVALUE;
    int _id_57207 = NOVALUE;
    int _29459 = NOVALUE;
    int _29458 = NOVALUE;
    int _29457 = NOVALUE;
    int _29456 = NOVALUE;
    int _29455 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer id*/

    /** 	tok = cexpr()*/
    _0 = _tok_57206;
    _tok_57206 = _30cexpr();
    DeRef(_0);

    /** 	while tok[T_ID] <= GREATER and tok[T_ID] >= LESS do*/
L1: 
    _2 = (int)SEQ_PTR(_tok_57206);
    _29455 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29455)) {
        _29456 = (_29455 <= 6);
    }
    else {
        _29456 = binary_op(LESSEQ, _29455, 6);
    }
    _29455 = NOVALUE;
    if (IS_ATOM_INT(_29456)) {
        if (_29456 == 0) {
            goto L2; // [25] 70
        }
    }
    else {
        if (DBL_PTR(_29456)->dbl == 0.0) {
            goto L2; // [25] 70
        }
    }
    _2 = (int)SEQ_PTR(_tok_57206);
    _29458 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29458)) {
        _29459 = (_29458 >= 1);
    }
    else {
        _29459 = binary_op(GREATEREQ, _29458, 1);
    }
    _29458 = NOVALUE;
    if (_29459 <= 0) {
        if (_29459 == 0) {
            DeRef(_29459);
            _29459 = NOVALUE;
            goto L2; // [42] 70
        }
        else {
            if (!IS_ATOM_INT(_29459) && DBL_PTR(_29459)->dbl == 0.0){
                DeRef(_29459);
                _29459 = NOVALUE;
                goto L2; // [42] 70
            }
            DeRef(_29459);
            _29459 = NOVALUE;
        }
    }
    DeRef(_29459);
    _29459 = NOVALUE;

    /** 		id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_57206);
    _id_57207 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_id_57207)){
        _id_57207 = (long)DBL_PTR(_id_57207)->dbl;
    }

    /** 		tok = cexpr()*/
    _0 = _tok_57206;
    _tok_57206 = _30cexpr();
    DeRef(_0);

    /** 		emit_op(id)*/
    _37emit_op(_id_57207);

    /** 	end while*/
    goto L1; // [67] 13
L2: 

    /** 	return tok*/
    DeRef(_29456);
    _29456 = NOVALUE;
    return _tok_57206;
    ;
}


void _30Expr()
{
    int _tok_57233 = NOVALUE;
    int _id_57234 = NOVALUE;
    int _patch_57235 = NOVALUE;
    int _29482 = NOVALUE;
    int _29480 = NOVALUE;
    int _29479 = NOVALUE;
    int _29477 = NOVALUE;
    int _29476 = NOVALUE;
    int _29475 = NOVALUE;
    int _29474 = NOVALUE;
    int _29473 = NOVALUE;
    int _29467 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer id*/

    /** 	integer patch*/

    /** 	ExprLine = ThisLine*/
    Ref(_43ThisLine_49532);
    DeRef(_30ExprLine_57228);
    _30ExprLine_57228 = _43ThisLine_49532;

    /** 	expr_bp = bp*/
    _30expr_bp_57229 = _43bp_49536;

    /** 	id = -1*/
    _id_57234 = -1;

    /** 	patch = 0*/
    _patch_57235 = 0;

    /** 	while TRUE do*/
L1: 
    if (_5TRUE_244 == 0)
    {
        goto L2; // [40] 300
    }
    else{
    }

    /** 		if id != -1 then*/
    if (_id_57234 == -1)
    goto L3; // [45] 116

    /** 			if id != XOR then*/
    if (_id_57234 == 152)
    goto L4; // [53] 115

    /** 				if short_circuit > 0 then*/
    if (_30short_circuit_55171 <= 0)
    goto L5; // [61] 114

    /** 					if id = OR then*/
    if (_id_57234 != 9)
    goto L6; // [69] 83

    /** 						emit_op(SC1_OR)*/
    _37emit_op(143);
    goto L7; // [80] 91
L6: 

    /** 						emit_op(SC1_AND)*/
    _37emit_op(141);
L7: 

    /** 					patch = length(Code)+1*/
    if (IS_SEQUENCE(_25Code_12355)){
            _29467 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29467 = 1;
    }
    _patch_57235 = _29467 + 1;
    _29467 = NOVALUE;

    /** 					emit_forward_addr()*/
    _30emit_forward_addr();

    /** 					short_circuit_B = TRUE*/
    _30short_circuit_B_55173 = _5TRUE_244;
L5: 
L4: 
L3: 

    /** 		tok = rexpr()*/
    _0 = _tok_57233;
    _tok_57233 = _30rexpr();
    DeRef(_0);

    /** 		if id != -1 then*/
    if (_id_57234 == -1)
    goto L8; // [123] 268

    /** 			if id != XOR then*/
    if (_id_57234 == 152)
    goto L9; // [131] 261

    /** 				if short_circuit > 0 then*/
    if (_30short_circuit_55171 <= 0)
    goto LA; // [139] 252

    /** 					if tok[T_ID] != THEN and tok[T_ID] != DO then*/
    _2 = (int)SEQ_PTR(_tok_57233);
    _29473 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29473)) {
        _29474 = (_29473 != 410);
    }
    else {
        _29474 = binary_op(NOTEQ, _29473, 410);
    }
    _29473 = NOVALUE;
    if (IS_ATOM_INT(_29474)) {
        if (_29474 == 0) {
            goto LB; // [157] 206
        }
    }
    else {
        if (DBL_PTR(_29474)->dbl == 0.0) {
            goto LB; // [157] 206
        }
    }
    _2 = (int)SEQ_PTR(_tok_57233);
    _29476 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29476)) {
        _29477 = (_29476 != 411);
    }
    else {
        _29477 = binary_op(NOTEQ, _29476, 411);
    }
    _29476 = NOVALUE;
    if (_29477 == 0) {
        DeRef(_29477);
        _29477 = NOVALUE;
        goto LB; // [174] 206
    }
    else {
        if (!IS_ATOM_INT(_29477) && DBL_PTR(_29477)->dbl == 0.0){
            DeRef(_29477);
            _29477 = NOVALUE;
            goto LB; // [174] 206
        }
        DeRef(_29477);
        _29477 = NOVALUE;
    }
    DeRef(_29477);
    _29477 = NOVALUE;

    /** 						if id = OR then*/
    if (_id_57234 != 9)
    goto LC; // [181] 195

    /** 							emit_op(SC2_OR)*/
    _37emit_op(144);
    goto LD; // [192] 219
LC: 

    /** 							emit_op(SC2_AND)*/
    _37emit_op(142);
    goto LD; // [203] 219
LB: 

    /** 						SC1_type = id -- if/while/elsif must patch*/
    _30SC1_type_55176 = _id_57234;

    /** 						emit_op(SC2_NULL)*/
    _37emit_op(145);
LD: 

    /** 					if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto LE; // [223] 234
    }
    else{
    }

    /** 						emit_op(NOP1)   -- to get label here*/
    _37emit_op(159);
LE: 

    /** 					backpatch(patch, length(Code)+1)*/
    if (IS_SEQUENCE(_25Code_12355)){
            _29479 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29479 = 1;
    }
    _29480 = _29479 + 1;
    _29479 = NOVALUE;
    _37backpatch(_patch_57235, _29480);
    _29480 = NOVALUE;
    goto LF; // [249] 267
LA: 

    /** 					emit_op(id)*/
    _37emit_op(_id_57234);
    goto LF; // [258] 267
L9: 

    /** 				emit_op(id)*/
    _37emit_op(_id_57234);
LF: 
L8: 

    /** 		id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_57233);
    _id_57234 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_id_57234)){
        _id_57234 = (long)DBL_PTR(_id_57234)->dbl;
    }

    /** 		if not find(id, boolOps) then*/
    _29482 = find_from(_id_57234, _30boolOps_57223, 1);
    if (_29482 != 0)
    goto L1; // [287] 38
    _29482 = NOVALUE;

    /** 			exit*/
    goto L2; // [292] 300

    /** 	end while*/
    goto L1; // [297] 38
L2: 

    /** 	putback(tok)*/
    Ref(_tok_57233);
    _30putback(_tok_57233);

    /** 	SC1_patch = patch -- extra line*/
    _30SC1_patch_55175 = _patch_57235;

    /** end procedure*/
    DeRef(_tok_57233);
    DeRef(_29474);
    _29474 = NOVALUE;
    return;
    ;
}


void _30TypeCheck(int _var_57310)
{
    int _which_type_57311 = NOVALUE;
    int _ref_57321 = NOVALUE;
    int _ref_57354 = NOVALUE;
    int _29538 = NOVALUE;
    int _29537 = NOVALUE;
    int _29536 = NOVALUE;
    int _29535 = NOVALUE;
    int _29534 = NOVALUE;
    int _29532 = NOVALUE;
    int _29531 = NOVALUE;
    int _29530 = NOVALUE;
    int _29529 = NOVALUE;
    int _29528 = NOVALUE;
    int _29527 = NOVALUE;
    int _29525 = NOVALUE;
    int _29524 = NOVALUE;
    int _29521 = NOVALUE;
    int _29520 = NOVALUE;
    int _29519 = NOVALUE;
    int _29518 = NOVALUE;
    int _29513 = NOVALUE;
    int _29512 = NOVALUE;
    int _29508 = NOVALUE;
    int _29506 = NOVALUE;
    int _29505 = NOVALUE;
    int _29504 = NOVALUE;
    int _29502 = NOVALUE;
    int _29501 = NOVALUE;
    int _29500 = NOVALUE;
    int _29499 = NOVALUE;
    int _29498 = NOVALUE;
    int _29497 = NOVALUE;
    int _29494 = NOVALUE;
    int _29492 = NOVALUE;
    int _29490 = NOVALUE;
    int _29489 = NOVALUE;
    int _29488 = NOVALUE;
    int _29486 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if var < 0 or SymTab[var][S_SCOPE] = SC_UNDEFINED then*/
    _29486 = (_var_57310 < 0);
    if (_29486 != 0) {
        goto L1; // [9] 36
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29488 = (int)*(((s1_ptr)_2)->base + _var_57310);
    _2 = (int)SEQ_PTR(_29488);
    _29489 = (int)*(((s1_ptr)_2)->base + 4);
    _29488 = NOVALUE;
    if (IS_ATOM_INT(_29489)) {
        _29490 = (_29489 == 9);
    }
    else {
        _29490 = binary_op(EQUALS, _29489, 9);
    }
    _29489 = NOVALUE;
    if (_29490 == 0) {
        DeRef(_29490);
        _29490 = NOVALUE;
        goto L2; // [32] 76
    }
    else {
        if (!IS_ATOM_INT(_29490) && DBL_PTR(_29490)->dbl == 0.0){
            DeRef(_29490);
            _29490 = NOVALUE;
            goto L2; // [32] 76
        }
        DeRef(_29490);
        _29490 = NOVALUE;
    }
    DeRef(_29490);
    _29490 = NOVALUE;
L1: 

    /** 		integer ref = new_forward_reference( TYPE_CHECK, var, TYPE_CHECK_FORWARD )*/
    _ref_57321 = _29new_forward_reference(65, _var_57310, 197);
    if (!IS_ATOM_INT(_ref_57321)) {
        _1 = (long)(DBL_PTR(_ref_57321)->dbl);
        if (UNIQUE(DBL_PTR(_ref_57321)) && (DBL_PTR(_ref_57321)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_57321);
        _ref_57321 = _1;
    }

    /** 		Code &= { TYPE_CHECK_FORWARD, var, OpTypeCheck }*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 197;
    *((int *)(_2+8)) = _var_57310;
    *((int *)(_2+12)) = _25OpTypeCheck_12333;
    _29492 = MAKE_SEQ(_1);
    Concat((object_ptr)&_25Code_12355, _25Code_12355, _29492);
    DeRefDS(_29492);
    _29492 = NOVALUE;

    /** 		return*/
    DeRef(_29486);
    _29486 = NOVALUE;
    return;
L2: 

    /** 	which_type = SymTab[var][S_VTYPE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29494 = (int)*(((s1_ptr)_2)->base + _var_57310);
    _2 = (int)SEQ_PTR(_29494);
    _which_type_57311 = (int)*(((s1_ptr)_2)->base + 15);
    if (!IS_ATOM_INT(_which_type_57311)){
        _which_type_57311 = (long)DBL_PTR(_which_type_57311)->dbl;
    }
    _29494 = NOVALUE;

    /** 	if which_type = 0 then*/
    if (_which_type_57311 != 0)
    goto L3; // [96] 106

    /** 		return	-- Not a typed identifier.*/
    DeRef(_29486);
    _29486 = NOVALUE;
    return;
L3: 

    /** 	if which_type > 0 and length(SymTab[which_type]) < S_TOKEN then*/
    _29497 = (_which_type_57311 > 0);
    if (_29497 == 0) {
        goto L4; // [112] 141
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29499 = (int)*(((s1_ptr)_2)->base + _which_type_57311);
    if (IS_SEQUENCE(_29499)){
            _29500 = SEQ_PTR(_29499)->length;
    }
    else {
        _29500 = 1;
    }
    _29499 = NOVALUE;
    if (IS_ATOM_INT(_25S_TOKEN_11918)) {
        _29501 = (_29500 < _25S_TOKEN_11918);
    }
    else {
        _29501 = binary_op(LESS, _29500, _25S_TOKEN_11918);
    }
    _29500 = NOVALUE;
    if (_29501 == 0) {
        DeRef(_29501);
        _29501 = NOVALUE;
        goto L4; // [132] 141
    }
    else {
        if (!IS_ATOM_INT(_29501) && DBL_PTR(_29501)->dbl == 0.0){
            DeRef(_29501);
            _29501 = NOVALUE;
            goto L4; // [132] 141
        }
        DeRef(_29501);
        _29501 = NOVALUE;
    }
    DeRef(_29501);
    _29501 = NOVALUE;

    /** 		return	-- Not a typed identifier.*/
    DeRef(_29486);
    _29486 = NOVALUE;
    DeRef(_29497);
    _29497 = NOVALUE;
    _29499 = NOVALUE;
    return;
L4: 

    /** 	if which_type < 0 or SymTab[which_type][S_TOKEN] = VARIABLE  then*/
    _29502 = (_which_type_57311 < 0);
    if (_29502 != 0) {
        goto L5; // [147] 174
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29504 = (int)*(((s1_ptr)_2)->base + _which_type_57311);
    _2 = (int)SEQ_PTR(_29504);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _29505 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _29505 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _29504 = NOVALUE;
    if (IS_ATOM_INT(_29505)) {
        _29506 = (_29505 == -100);
    }
    else {
        _29506 = binary_op(EQUALS, _29505, -100);
    }
    _29505 = NOVALUE;
    if (_29506 == 0) {
        DeRef(_29506);
        _29506 = NOVALUE;
        goto L6; // [170] 214
    }
    else {
        if (!IS_ATOM_INT(_29506) && DBL_PTR(_29506)->dbl == 0.0){
            DeRef(_29506);
            _29506 = NOVALUE;
            goto L6; // [170] 214
        }
        DeRef(_29506);
        _29506 = NOVALUE;
    }
    DeRef(_29506);
    _29506 = NOVALUE;
L5: 

    /** 		integer ref = new_forward_reference( TYPE_CHECK, which_type, TYPE )*/
    _ref_57354 = _29new_forward_reference(65, _which_type_57311, 504);
    if (!IS_ATOM_INT(_ref_57354)) {
        _1 = (long)(DBL_PTR(_ref_57354)->dbl);
        if (UNIQUE(DBL_PTR(_ref_57354)) && (DBL_PTR(_ref_57354)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_57354);
        _ref_57354 = _1;
    }

    /** 		Code &= { TYPE_CHECK_FORWARD, var, OpTypeCheck }*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 197;
    *((int *)(_2+8)) = _var_57310;
    *((int *)(_2+12)) = _25OpTypeCheck_12333;
    _29508 = MAKE_SEQ(_1);
    Concat((object_ptr)&_25Code_12355, _25Code_12355, _29508);
    DeRefDS(_29508);
    _29508 = NOVALUE;

    /** 		return*/
    DeRef(_29486);
    _29486 = NOVALUE;
    DeRef(_29497);
    _29497 = NOVALUE;
    _29499 = NOVALUE;
    DeRef(_29502);
    _29502 = NOVALUE;
    return;
L6: 

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L7; // [220] 317
    }
    else{
    }

    /** 		if OpTypeCheck then*/
    if (_25OpTypeCheck_12333 == 0)
    {
        goto L8; // [227] 481
    }
    else{
    }

    /** 			switch which_type do*/
    if( _30_57368_cases == 0 ){
        _30_57368_cases = 1;
        SEQ_PTR( _29510 )->base[1] = _52object_type_47099;
        SEQ_PTR( _29510 )->base[2] = _52sequence_type_47103;
        SEQ_PTR( _29510 )->base[3] = _52atom_type_47101;
        SEQ_PTR( _29510 )->base[4] = _52integer_type_47105;
    }
    _1 = find(_which_type_57311, _29510);
    switch ( _1 ){ 

        /** 				case object_type, sequence_type, atom_type then*/
        case 1:
        case 2:
        case 3:

        /** 				case integer_type then*/
        goto L8; // [247] 481
        case 4:

        /** 					op_info1 = var*/
        _37op_info1_51268 = _var_57310;

        /** 					emit_op(INTEGER_CHECK)*/
        _37emit_op(96);
        goto L8; // [265] 481

        /** 				case else*/
        case 0:

        /** 					if SymTab[which_type][S_EFFECT] then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _29512 = (int)*(((s1_ptr)_2)->base + _which_type_57311);
        _2 = (int)SEQ_PTR(_29512);
        _29513 = (int)*(((s1_ptr)_2)->base + 23);
        _29512 = NOVALUE;
        if (_29513 == 0) {
            _29513 = NOVALUE;
            goto L9; // [285] 312
        }
        else {
            if (!IS_ATOM_INT(_29513) && DBL_PTR(_29513)->dbl == 0.0){
                _29513 = NOVALUE;
                goto L9; // [285] 312
            }
            _29513 = NOVALUE;
        }
        _29513 = NOVALUE;

        /** 						emit_opnd(var)*/
        _37emit_opnd(_var_57310);

        /** 						op_info1 = which_type*/
        _37op_info1_51268 = _which_type_57311;

        /** 						emit_or_inline()*/
        _67emit_or_inline();

        /** 						emit_op(TYPE_CHECK)*/
        _37emit_op(65);
L9: 
    ;}    goto L8; // [314] 481
L7: 

    /** 		if OpTypeCheck then*/
    if (_25OpTypeCheck_12333 == 0)
    {
        goto LA; // [321] 480
    }
    else{
    }

    /** 			if which_type != object_type then*/
    if (_which_type_57311 == _52object_type_47099)
    goto LB; // [328] 479

    /** 				if which_type = integer_type then*/
    if (_which_type_57311 != _52integer_type_47105)
    goto LC; // [336] 357

    /** 						op_info1 = var*/
    _37op_info1_51268 = _var_57310;

    /** 						emit_op(INTEGER_CHECK)*/
    _37emit_op(96);
    goto LD; // [354] 478
LC: 

    /** 				elsif which_type = sequence_type then*/
    if (_which_type_57311 != _52sequence_type_47103)
    goto LE; // [361] 382

    /** 						op_info1 = var*/
    _37op_info1_51268 = _var_57310;

    /** 						emit_op(SEQUENCE_CHECK)*/
    _37emit_op(97);
    goto LD; // [379] 478
LE: 

    /** 				elsif which_type = atom_type then*/
    if (_which_type_57311 != _52atom_type_47101)
    goto LF; // [386] 407

    /** 						op_info1 = var*/
    _37op_info1_51268 = _var_57310;

    /** 						emit_op(ATOM_CHECK)*/
    _37emit_op(101);
    goto LD; // [404] 478
LF: 

    /** 						if SymTab[SymTab[which_type][S_NEXT]][S_VTYPE] =*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29518 = (int)*(((s1_ptr)_2)->base + _which_type_57311);
    _2 = (int)SEQ_PTR(_29518);
    _29519 = (int)*(((s1_ptr)_2)->base + 2);
    _29518 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_29519)){
        _29520 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29519)->dbl));
    }
    else{
        _29520 = (int)*(((s1_ptr)_2)->base + _29519);
    }
    _2 = (int)SEQ_PTR(_29520);
    _29521 = (int)*(((s1_ptr)_2)->base + 15);
    _29520 = NOVALUE;
    if (binary_op_a(NOTEQ, _29521, _52integer_type_47105)){
        _29521 = NOVALUE;
        goto L10; // [435] 454
    }
    _29521 = NOVALUE;

    /** 							op_info1 = var*/
    _37op_info1_51268 = _var_57310;

    /** 							emit_op(INTEGER_CHECK) -- need integer conversion*/
    _37emit_op(96);
L10: 

    /** 						emit_opnd(var)*/
    _37emit_opnd(_var_57310);

    /** 						op_info1 = which_type*/
    _37op_info1_51268 = _which_type_57311;

    /** 						emit_or_inline()*/
    _67emit_or_inline();

    /** 						emit_op(TYPE_CHECK)*/
    _37emit_op(65);
LD: 
LB: 
LA: 
L8: 

    /** 	if TRANSLATE or not OpTypeCheck then*/
    if (_25TRANSLATE_11874 != 0) {
        goto L11; // [485] 499
    }
    _29524 = (_25OpTypeCheck_12333 == 0);
    if (_29524 == 0)
    {
        DeRef(_29524);
        _29524 = NOVALUE;
        goto L12; // [495] 620
    }
    else{
        DeRef(_29524);
        _29524 = NOVALUE;
    }
L11: 

    /** 		op_info1 = var*/
    _37op_info1_51268 = _var_57310;

    /** 		if which_type = sequence_type or*/
    _29525 = (_which_type_57311 == _52sequence_type_47103);
    if (_29525 != 0) {
        goto L13; // [514] 553
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29527 = (int)*(((s1_ptr)_2)->base + _which_type_57311);
    _2 = (int)SEQ_PTR(_29527);
    _29528 = (int)*(((s1_ptr)_2)->base + 2);
    _29527 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_29528)){
        _29529 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29528)->dbl));
    }
    else{
        _29529 = (int)*(((s1_ptr)_2)->base + _29528);
    }
    _2 = (int)SEQ_PTR(_29529);
    _29530 = (int)*(((s1_ptr)_2)->base + 15);
    _29529 = NOVALUE;
    if (IS_ATOM_INT(_29530)) {
        _29531 = (_29530 == _52sequence_type_47103);
    }
    else {
        _29531 = binary_op(EQUALS, _29530, _52sequence_type_47103);
    }
    _29530 = NOVALUE;
    if (_29531 == 0) {
        DeRef(_29531);
        _29531 = NOVALUE;
        goto L14; // [549] 563
    }
    else {
        if (!IS_ATOM_INT(_29531) && DBL_PTR(_29531)->dbl == 0.0){
            DeRef(_29531);
            _29531 = NOVALUE;
            goto L14; // [549] 563
        }
        DeRef(_29531);
        _29531 = NOVALUE;
    }
    DeRef(_29531);
    _29531 = NOVALUE;
L13: 

    /** 			emit_op(SEQUENCE_CHECK)*/
    _37emit_op(97);
    goto L15; // [560] 619
L14: 

    /** 		elsif which_type = integer_type or*/
    _29532 = (_which_type_57311 == _52integer_type_47105);
    if (_29532 != 0) {
        goto L16; // [571] 610
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29534 = (int)*(((s1_ptr)_2)->base + _which_type_57311);
    _2 = (int)SEQ_PTR(_29534);
    _29535 = (int)*(((s1_ptr)_2)->base + 2);
    _29534 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_29535)){
        _29536 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29535)->dbl));
    }
    else{
        _29536 = (int)*(((s1_ptr)_2)->base + _29535);
    }
    _2 = (int)SEQ_PTR(_29536);
    _29537 = (int)*(((s1_ptr)_2)->base + 15);
    _29536 = NOVALUE;
    if (IS_ATOM_INT(_29537)) {
        _29538 = (_29537 == _52integer_type_47105);
    }
    else {
        _29538 = binary_op(EQUALS, _29537, _52integer_type_47105);
    }
    _29537 = NOVALUE;
    if (_29538 == 0) {
        DeRef(_29538);
        _29538 = NOVALUE;
        goto L17; // [606] 618
    }
    else {
        if (!IS_ATOM_INT(_29538) && DBL_PTR(_29538)->dbl == 0.0){
            DeRef(_29538);
            _29538 = NOVALUE;
            goto L17; // [606] 618
        }
        DeRef(_29538);
        _29538 = NOVALUE;
    }
    DeRef(_29538);
    _29538 = NOVALUE;
L16: 

    /** 			emit_op(INTEGER_CHECK)*/
    _37emit_op(96);
L17: 
L15: 
L12: 

    /** end procedure*/
    DeRef(_29486);
    _29486 = NOVALUE;
    DeRef(_29497);
    _29497 = NOVALUE;
    _29499 = NOVALUE;
    DeRef(_29502);
    _29502 = NOVALUE;
    _29519 = NOVALUE;
    DeRef(_29525);
    _29525 = NOVALUE;
    _29528 = NOVALUE;
    DeRef(_29532);
    _29532 = NOVALUE;
    _29535 = NOVALUE;
    return;
    ;
}


void _30Assignment(int _left_var_57475)
{
    int _tok_57477 = NOVALUE;
    int _subs_57478 = NOVALUE;
    int _slice_57479 = NOVALUE;
    int _assign_op_57480 = NOVALUE;
    int _subs1_patch_57481 = NOVALUE;
    int _dangerous_57483 = NOVALUE;
    int _lname_57607 = NOVALUE;
    int _temp_len_57624 = NOVALUE;
    int _29637 = NOVALUE;
    int _29636 = NOVALUE;
    int _29635 = NOVALUE;
    int _29634 = NOVALUE;
    int _29633 = NOVALUE;
    int _29632 = NOVALUE;
    int _29631 = NOVALUE;
    int _29630 = NOVALUE;
    int _29621 = NOVALUE;
    int _29620 = NOVALUE;
    int _29619 = NOVALUE;
    int _29618 = NOVALUE;
    int _29617 = NOVALUE;
    int _29616 = NOVALUE;
    int _29615 = NOVALUE;
    int _29614 = NOVALUE;
    int _29613 = NOVALUE;
    int _29612 = NOVALUE;
    int _29611 = NOVALUE;
    int _29610 = NOVALUE;
    int _29609 = NOVALUE;
    int _29608 = NOVALUE;
    int _29607 = NOVALUE;
    int _29605 = NOVALUE;
    int _29604 = NOVALUE;
    int _29603 = NOVALUE;
    int _29601 = NOVALUE;
    int _29596 = NOVALUE;
    int _29595 = NOVALUE;
    int _29592 = NOVALUE;
    int _29591 = NOVALUE;
    int _29589 = NOVALUE;
    int _29583 = NOVALUE;
    int _29578 = NOVALUE;
    int _29575 = NOVALUE;
    int _29574 = NOVALUE;
    int _29571 = NOVALUE;
    int _29568 = NOVALUE;
    int _29567 = NOVALUE;
    int _29566 = NOVALUE;
    int _29564 = NOVALUE;
    int _29563 = NOVALUE;
    int _29562 = NOVALUE;
    int _29561 = NOVALUE;
    int _29560 = NOVALUE;
    int _29559 = NOVALUE;
    int _29557 = NOVALUE;
    int _29556 = NOVALUE;
    int _29555 = NOVALUE;
    int _29554 = NOVALUE;
    int _29552 = NOVALUE;
    int _29551 = NOVALUE;
    int _29550 = NOVALUE;
    int _29549 = NOVALUE;
    int _29548 = NOVALUE;
    int _29546 = NOVALUE;
    int _29545 = NOVALUE;
    int _29544 = NOVALUE;
    int _29541 = NOVALUE;
    int _29540 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	integer subs, slice, assign_op, subs1_patch*/

    /** 	left_sym = left_var[T_SYM]*/
    _2 = (int)SEQ_PTR(_left_var_57475);
    _30left_sym_55212 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_30left_sym_55212)){
        _30left_sym_55212 = (long)DBL_PTR(_30left_sym_55212)->dbl;
    }

    /** 	if SymTab[left_sym][S_SCOPE] = SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29540 = (int)*(((s1_ptr)_2)->base + _30left_sym_55212);
    _2 = (int)SEQ_PTR(_29540);
    _29541 = (int)*(((s1_ptr)_2)->base + 4);
    _29540 = NOVALUE;
    if (binary_op_a(NOTEQ, _29541, 9)){
        _29541 = NOVALUE;
        goto L1; // [31] 54
    }
    _29541 = NOVALUE;

    /** 		Forward_var( left_var, ,ASSIGN )*/
    Ref(_left_var_57475);
    _30Forward_var(_left_var_57475, -1, 18);

    /** 		left_sym = Pop() -- pops off what forward var emitted, because it gets emitted later*/
    _0 = _37Pop();
    _30left_sym_55212 = _0;
    if (!IS_ATOM_INT(_30left_sym_55212)) {
        _1 = (long)(DBL_PTR(_30left_sym_55212)->dbl);
        if (UNIQUE(DBL_PTR(_30left_sym_55212)) && (DBL_PTR(_30left_sym_55212)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_30left_sym_55212);
        _30left_sym_55212 = _1;
    }
    goto L2; // [51] 267
L1: 

    /** 		UndefinedVar(left_sym)*/
    _30UndefinedVar(_30left_sym_55212);

    /** 		if SymTab[left_sym][S_SCOPE] = SC_LOOP_VAR or*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29544 = (int)*(((s1_ptr)_2)->base + _30left_sym_55212);
    _2 = (int)SEQ_PTR(_29544);
    _29545 = (int)*(((s1_ptr)_2)->base + 4);
    _29544 = NOVALUE;
    if (IS_ATOM_INT(_29545)) {
        _29546 = (_29545 == 2);
    }
    else {
        _29546 = binary_op(EQUALS, _29545, 2);
    }
    _29545 = NOVALUE;
    if (IS_ATOM_INT(_29546)) {
        if (_29546 != 0) {
            goto L3; // [83] 112
        }
    }
    else {
        if (DBL_PTR(_29546)->dbl != 0.0) {
            goto L3; // [83] 112
        }
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29548 = (int)*(((s1_ptr)_2)->base + _30left_sym_55212);
    _2 = (int)SEQ_PTR(_29548);
    _29549 = (int)*(((s1_ptr)_2)->base + 4);
    _29548 = NOVALUE;
    if (IS_ATOM_INT(_29549)) {
        _29550 = (_29549 == 4);
    }
    else {
        _29550 = binary_op(EQUALS, _29549, 4);
    }
    _29549 = NOVALUE;
    if (_29550 == 0) {
        DeRef(_29550);
        _29550 = NOVALUE;
        goto L4; // [108] 122
    }
    else {
        if (!IS_ATOM_INT(_29550) && DBL_PTR(_29550)->dbl == 0.0){
            DeRef(_29550);
            _29550 = NOVALUE;
            goto L4; // [108] 122
        }
        DeRef(_29550);
        _29550 = NOVALUE;
    }
    DeRef(_29550);
    _29550 = NOVALUE;
L3: 

    /** 			CompileErr(109)*/
    RefDS(_22682);
    _43CompileErr(109, _22682, 0);
    goto L5; // [119] 229
L4: 

    /** 		elsif SymTab[left_sym][S_MODE] = M_CONSTANT then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29551 = (int)*(((s1_ptr)_2)->base + _30left_sym_55212);
    _2 = (int)SEQ_PTR(_29551);
    _29552 = (int)*(((s1_ptr)_2)->base + 3);
    _29551 = NOVALUE;
    if (binary_op_a(NOTEQ, _29552, 2)){
        _29552 = NOVALUE;
        goto L6; // [140] 154
    }
    _29552 = NOVALUE;

    /** 			CompileErr(110)*/
    RefDS(_22682);
    _43CompileErr(110, _22682, 0);
    goto L5; // [151] 229
L6: 

    /** 		elsif find(SymTab[left_sym][S_SCOPE], SCOPE_TYPES) then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29554 = (int)*(((s1_ptr)_2)->base + _30left_sym_55212);
    _2 = (int)SEQ_PTR(_29554);
    _29555 = (int)*(((s1_ptr)_2)->base + 4);
    _29554 = NOVALUE;
    _29556 = find_from(_29555, _30SCOPE_TYPES_55162, 1);
    _29555 = NOVALUE;
    if (_29556 == 0)
    {
        _29556 = NOVALUE;
        goto L7; // [177] 228
    }
    else{
        _29556 = NOVALUE;
    }

    /** 			SymTab[CurrentSub][S_EFFECT] = or_bits(SymTab[CurrentSub][S_EFFECT],*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25CurrentSub_12270 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29559 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_29559);
    _29560 = (int)*(((s1_ptr)_2)->base + 23);
    _29559 = NOVALUE;
    _29561 = (_30left_sym_55212 % 29);
    _29562 = power(2, _29561);
    _29561 = NOVALUE;
    if (IS_ATOM_INT(_29560) && IS_ATOM_INT(_29562)) {
        {unsigned long tu;
             tu = (unsigned long)_29560 | (unsigned long)_29562;
             _29563 = MAKE_UINT(tu);
        }
    }
    else {
        _29563 = binary_op(OR_BITS, _29560, _29562);
    }
    _29560 = NOVALUE;
    DeRef(_29562);
    _29562 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 23);
    _1 = *(int *)_2;
    *(int *)_2 = _29563;
    if( _1 != _29563 ){
        DeRef(_1);
    }
    _29563 = NOVALUE;
    _29557 = NOVALUE;
L7: 
L5: 

    /** 		SymTab[left_sym][S_USAGE] = or_bits(SymTab[left_sym][S_USAGE], U_WRITTEN)*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_30left_sym_55212 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29566 = (int)*(((s1_ptr)_2)->base + _30left_sym_55212);
    _2 = (int)SEQ_PTR(_29566);
    _29567 = (int)*(((s1_ptr)_2)->base + 5);
    _29566 = NOVALUE;
    if (IS_ATOM_INT(_29567)) {
        {unsigned long tu;
             tu = (unsigned long)_29567 | (unsigned long)2;
             _29568 = MAKE_UINT(tu);
        }
    }
    else {
        _29568 = binary_op(OR_BITS, _29567, 2);
    }
    _29567 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _29568;
    if( _1 != _29568 ){
        DeRef(_1);
    }
    _29568 = NOVALUE;
    _29564 = NOVALUE;
L2: 

    /** 	tok = next_token()*/
    _0 = _tok_57477;
    _tok_57477 = _30next_token();
    DeRef(_0);

    /** 	subs = 0*/
    _subs_57478 = 0;

    /** 	slice = FALSE*/
    _slice_57479 = _5FALSE_242;

    /** 	dangerous = FALSE*/
    _dangerous_57483 = _5FALSE_242;

    /** 	side_effect_calls = 0*/
    _30side_effect_calls_55208 = 0;

    /** 	emit_opnd(left_sym)*/
    _37emit_opnd(_30left_sym_55212);

    /** 	current_sequence = append(current_sequence, left_sym)*/
    Append(&_37current_sequence_51276, _37current_sequence_51276, _30left_sym_55212);

    /** 	while tok[T_ID] = LEFT_SQUARE do*/
L8: 
    _2 = (int)SEQ_PTR(_tok_57477);
    _29571 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29571, -28)){
        _29571 = NOVALUE;
        goto L9; // [330] 522
    }
    _29571 = NOVALUE;

    /** 		subs_depth += 1*/
    _30subs_depth_55213 = _30subs_depth_55213 + 1;

    /** 		if lhs_ptr then*/
    if (_37lhs_ptr_51278 == 0)
    {
        goto LA; // [346] 404
    }
    else{
    }

    /** 			current_sequence = head( current_sequence, length( current_sequence ) - 1 )*/
    if (IS_SEQUENCE(_37current_sequence_51276)){
            _29574 = SEQ_PTR(_37current_sequence_51276)->length;
    }
    else {
        _29574 = 1;
    }
    _29575 = _29574 - 1;
    _29574 = NOVALUE;
    {
        int len = SEQ_PTR(_37current_sequence_51276)->length;
        int size = (IS_ATOM_INT(_29575)) ? _29575 : (object)(DBL_PTR(_29575)->dbl);
        if (size <= 0){
            DeRef( _37current_sequence_51276 );
            _37current_sequence_51276 = MAKE_SEQ(NewS1(0));
        }
        else if (len <= size) {
            RefDS(_37current_sequence_51276);
            DeRef(_37current_sequence_51276);
            _37current_sequence_51276 = _37current_sequence_51276;
        }
        else{
            Head(SEQ_PTR(_37current_sequence_51276),size+1,&_37current_sequence_51276);
        }
    }
    _29575 = NOVALUE;

    /** 			if subs = 1 then*/
    if (_subs_57478 != 1)
    goto LB; // [370] 395

    /** 				subs1_patch = length(Code)+1*/
    if (IS_SEQUENCE(_25Code_12355)){
            _29578 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29578 = 1;
    }
    _subs1_patch_57481 = _29578 + 1;
    _29578 = NOVALUE;

    /** 				emit_op(LHS_SUBS1) -- creates new current_sequence*/
    _37emit_op(161);
    goto LC; // [392] 403
LB: 

    /** 				emit_op(LHS_SUBS) -- adds to current_sequence*/
    _37emit_op(95);
LC: 
LA: 

    /** 		subs += 1*/
    _subs_57478 = _subs_57478 + 1;

    /** 		if subs = 1 then*/
    if (_subs_57478 != 1)
    goto LD; // [412] 427

    /** 			InitCheck(left_sym, TRUE)*/
    _30InitCheck(_30left_sym_55212, _5TRUE_244);
LD: 

    /** 		Expr()*/
    _30Expr();

    /** 		tok = next_token()*/
    _0 = _tok_57477;
    _tok_57477 = _30next_token();
    DeRef(_0);

    /** 		if tok[T_ID] = SLICE then*/
    _2 = (int)SEQ_PTR(_tok_57477);
    _29583 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29583, 513)){
        _29583 = NOVALUE;
        goto LE; // [446] 483
    }
    _29583 = NOVALUE;

    /** 			Expr()*/
    _30Expr();

    /** 			slice = TRUE*/
    _slice_57479 = _5TRUE_244;

    /** 			tok_match(RIGHT_SQUARE)*/
    _30tok_match(-29, 0);

    /** 			tok = next_token()*/
    _0 = _tok_57477;
    _tok_57477 = _30next_token();
    DeRef(_0);

    /** 			exit  -- no further subs or slices allowed*/
    goto L9; // [478] 522
    goto LF; // [480] 505
LE: 

    /** 			putback(tok)*/
    Ref(_tok_57477);
    _30putback(_tok_57477);

    /** 			tok_match(RIGHT_SQUARE)*/
    _30tok_match(-29, 0);

    /** 			subs_depth -= 1*/
    _30subs_depth_55213 = _30subs_depth_55213 - 1;
LF: 

    /** 		tok = next_token()*/
    _0 = _tok_57477;
    _tok_57477 = _30next_token();
    DeRef(_0);

    /** 		lhs_ptr = TRUE*/
    _37lhs_ptr_51278 = _5TRUE_244;

    /** 	end while*/
    goto L8; // [519] 322
L9: 

    /** 	lhs_ptr = FALSE*/
    _37lhs_ptr_51278 = _5FALSE_242;

    /** 	assign_op = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_57477);
    _assign_op_57480 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_assign_op_57480)){
        _assign_op_57480 = (long)DBL_PTR(_assign_op_57480)->dbl;
    }

    /** 	if not find(assign_op, ASSIGN_OPS) then*/
    _29589 = find_from(_assign_op_57480, _30ASSIGN_OPS_55154, 1);
    if (_29589 != 0)
    goto L10; // [548] 608
    _29589 = NOVALUE;

    /** 		sequence lname = SymTab[left_var[T_SYM]][S_NAME]*/
    _2 = (int)SEQ_PTR(_left_var_57475);
    _29591 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_29591)){
        _29592 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29591)->dbl));
    }
    else{
        _29592 = (int)*(((s1_ptr)_2)->base + _29591);
    }
    DeRef(_lname_57607);
    _2 = (int)SEQ_PTR(_29592);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _lname_57607 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _lname_57607 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    Ref(_lname_57607);
    _29592 = NOVALUE;

    /** 		if assign_op = COLON then*/
    if (_assign_op_57480 != -23)
    goto L11; // [577] 595

    /** 			CompileErr(133, {lname})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_lname_57607);
    *((int *)(_2+4)) = _lname_57607;
    _29595 = MAKE_SEQ(_1);
    _43CompileErr(133, _29595, 0);
    _29595 = NOVALUE;
    goto L12; // [592] 607
L11: 

    /** 			CompileErr(76, {lname})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_lname_57607);
    *((int *)(_2+4)) = _lname_57607;
    _29596 = MAKE_SEQ(_1);
    _43CompileErr(76, _29596, 0);
    _29596 = NOVALUE;
L12: 
L10: 
    DeRef(_lname_57607);
    _lname_57607 = NOVALUE;

    /** 	if subs = 0 then*/
    if (_subs_57478 != 0)
    goto L13; // [612] 740

    /** 		integer temp_len = length(Code)*/
    if (IS_SEQUENCE(_25Code_12355)){
            _temp_len_57624 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _temp_len_57624 = 1;
    }

    /** 		if assign_op = EQUALS then*/
    if (_assign_op_57480 != 3)
    goto L14; // [627] 648

    /** 			Expr() -- RHS expression*/
    _30Expr();

    /** 			InitCheck(left_sym, FALSE)*/
    _30InitCheck(_30left_sym_55212, _5FALSE_242);
    goto L15; // [645] 721
L14: 

    /** 			InitCheck(left_sym, TRUE)*/
    _30InitCheck(_30left_sym_55212, _5TRUE_244);

    /** 			if left_sym > 0 then*/
    if (_30left_sym_55212 <= 0)
    goto L16; // [662] 704

    /** 				SymTab[left_sym][S_USAGE] = or_bits(SymTab[left_sym][S_USAGE], U_READ)*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_30left_sym_55212 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29603 = (int)*(((s1_ptr)_2)->base + _30left_sym_55212);
    _2 = (int)SEQ_PTR(_29603);
    _29604 = (int)*(((s1_ptr)_2)->base + 5);
    _29603 = NOVALUE;
    if (IS_ATOM_INT(_29604)) {
        {unsigned long tu;
             tu = (unsigned long)_29604 | (unsigned long)1;
             _29605 = MAKE_UINT(tu);
        }
    }
    else {
        _29605 = binary_op(OR_BITS, _29604, 1);
    }
    _29604 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _29605;
    if( _1 != _29605 ){
        DeRef(_1);
    }
    _29605 = NOVALUE;
    _29601 = NOVALUE;
L16: 

    /** 			emit_opnd(left_sym)*/
    _37emit_opnd(_30left_sym_55212);

    /** 			Expr() -- RHS expression*/
    _30Expr();

    /** 			emit_assign_op(assign_op)*/
    _37emit_assign_op(_assign_op_57480);
L15: 

    /** 		emit_op(ASSIGN)*/
    _37emit_op(18);

    /** 		TypeCheck(left_sym)*/
    _30TypeCheck(_30left_sym_55212);
    goto L17; // [737] 1164
L13: 

    /** 		factors = 0*/
    _30factors_55209 = 0;

    /** 		lhs_subs_level = -1*/
    _30lhs_subs_level_55210 = -1;

    /** 		Expr() -- RHS expression*/
    _30Expr();

    /** 		if subs > 1 then*/
    if (_subs_57478 <= 1)
    goto L18; // [756] 895

    /** 			if left_sym < 0 or SymTab[left_sym][S_SCOPE] != SC_PRIVATE and*/
    _29607 = (_30left_sym_55212 < 0);
    if (_29607 != 0) {
        _29608 = 1;
        goto L19; // [768] 796
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29609 = (int)*(((s1_ptr)_2)->base + _30left_sym_55212);
    _2 = (int)SEQ_PTR(_29609);
    _29610 = (int)*(((s1_ptr)_2)->base + 4);
    _29609 = NOVALUE;
    if (IS_ATOM_INT(_29610)) {
        _29611 = (_29610 != 3);
    }
    else {
        _29611 = binary_op(NOTEQ, _29610, 3);
    }
    _29610 = NOVALUE;
    if (IS_ATOM_INT(_29611))
    _29608 = (_29611 != 0);
    else
    _29608 = DBL_PTR(_29611)->dbl != 0.0;
L19: 
    if (_29608 == 0) {
        goto L1A; // [796] 830
    }
    _29613 = (_30left_sym_55212 % 29);
    _29614 = power(2, _29613);
    _29613 = NOVALUE;
    if (IS_ATOM_INT(_29614)) {
        {unsigned long tu;
             tu = (unsigned long)_30side_effect_calls_55208 & (unsigned long)_29614;
             _29615 = MAKE_UINT(tu);
        }
    }
    else {
        temp_d.dbl = (double)_30side_effect_calls_55208;
        _29615 = Dand_bits(&temp_d, DBL_PTR(_29614));
    }
    DeRef(_29614);
    _29614 = NOVALUE;
    if (_29615 == 0) {
        DeRef(_29615);
        _29615 = NOVALUE;
        goto L1A; // [819] 830
    }
    else {
        if (!IS_ATOM_INT(_29615) && DBL_PTR(_29615)->dbl == 0.0){
            DeRef(_29615);
            _29615 = NOVALUE;
            goto L1A; // [819] 830
        }
        DeRef(_29615);
        _29615 = NOVALUE;
    }
    DeRef(_29615);
    _29615 = NOVALUE;

    /** 				dangerous = TRUE*/
    _dangerous_57483 = _5TRUE_244;
L1A: 

    /** 			if factors = 1 and*/
    _29616 = (_30factors_55209 == 1);
    if (_29616 == 0) {
        _29617 = 0;
        goto L1B; // [838] 852
    }
    _29618 = (_30lhs_subs_level_55210 >= 0);
    _29617 = (_29618 != 0);
L1B: 
    if (_29617 == 0) {
        goto L1C; // [852] 878
    }
    _29620 = _subs_57478 + _slice_57479;
    if ((long)((unsigned long)_29620 + (unsigned long)HIGH_BITS) >= 0) 
    _29620 = NewDouble((double)_29620);
    if (IS_ATOM_INT(_29620)) {
        _29621 = (_30lhs_subs_level_55210 < _29620);
    }
    else {
        _29621 = ((double)_30lhs_subs_level_55210 < DBL_PTR(_29620)->dbl);
    }
    DeRef(_29620);
    _29620 = NOVALUE;
    if (_29621 == 0)
    {
        DeRef(_29621);
        _29621 = NOVALUE;
        goto L1C; // [867] 878
    }
    else{
        DeRef(_29621);
        _29621 = NOVALUE;
    }

    /** 				dangerous = TRUE*/
    _dangerous_57483 = _5TRUE_244;
L1C: 

    /** 			if dangerous then*/
    if (_dangerous_57483 == 0)
    {
        goto L1D; // [880] 894
    }
    else{
    }

    /** 				backpatch(subs1_patch, LHS_SUBS1_COPY)*/
    _37backpatch(_subs1_patch_57481, 166);
L1D: 
L18: 

    /** 		if slice then*/
    if (_slice_57479 == 0)
    {
        goto L1E; // [897] 965
    }
    else{
    }

    /** 			if assign_op != EQUALS then*/
    if (_assign_op_57480 == 3)
    goto L1F; // [904] 938

    /** 				if subs = 1 then*/
    if (_subs_57478 != 1)
    goto L20; // [910] 924

    /** 					emit_op(ASSIGN_OP_SLICE)*/
    _37emit_op(150);
    goto L21; // [921] 932
L20: 

    /** 					emit_op(PASSIGN_OP_SLICE)*/
    _37emit_op(165);
L21: 

    /** 				emit_assign_op(assign_op)*/
    _37emit_assign_op(_assign_op_57480);
L1F: 

    /** 			if subs = 1 then*/
    if (_subs_57478 != 1)
    goto L22; // [940] 954

    /** 				emit_op(ASSIGN_SLICE)*/
    _37emit_op(45);
    goto L23; // [951] 1055
L22: 

    /** 				emit_op(PASSIGN_SLICE)*/
    _37emit_op(163);
    goto L23; // [962] 1055
L1E: 

    /** 			if assign_op = EQUALS then*/
    if (_assign_op_57480 != 3)
    goto L24; // [969] 1000

    /** 				if subs = 1 then*/
    if (_subs_57478 != 1)
    goto L25; // [975] 989

    /** 					emit_op(ASSIGN_SUBS)*/
    _37emit_op(16);
    goto L26; // [986] 1054
L25: 

    /** 					emit_op(PASSIGN_SUBS)*/
    _37emit_op(162);
    goto L26; // [997] 1054
L24: 

    /** 				if subs = 1 then*/
    if (_subs_57478 != 1)
    goto L27; // [1002] 1016

    /** 					emit_op(ASSIGN_OP_SUBS)*/
    _37emit_op(149);
    goto L28; // [1013] 1024
L27: 

    /** 					emit_op(PASSIGN_OP_SUBS)*/
    _37emit_op(164);
L28: 

    /** 				emit_assign_op(assign_op)*/
    _37emit_assign_op(_assign_op_57480);

    /** 				if subs = 1 then*/
    if (_subs_57478 != 1)
    goto L29; // [1031] 1045

    /** 					emit_op(ASSIGN_SUBS2)*/
    _37emit_op(148);
    goto L2A; // [1042] 1053
L29: 

    /** 					emit_op(PASSIGN_SUBS)*/
    _37emit_op(162);
L2A: 
L26: 
L23: 

    /** 		if subs > 1 then*/
    if (_subs_57478 <= 1)
    goto L2B; // [1057] 1109

    /** 			if dangerous then*/
    if (_dangerous_57483 == 0)
    {
        goto L2C; // [1063] 1100
    }
    else{
    }

    /** 				emit_opnd(left_sym)*/
    _37emit_opnd(_30left_sym_55212);

    /** 				emit_opnd(lhs_subs1_copy_temp) -- will be freed*/
    _37emit_opnd(_37lhs_subs1_copy_temp_51281);

    /** 				emit_temp( lhs_subs1_copy_temp, NEW_REFERENCE )*/
    _37emit_temp(_37lhs_subs1_copy_temp_51281, 1);

    /** 				emit_op(ASSIGN)*/
    _37emit_op(18);
    goto L2D; // [1097] 1108
L2C: 

    /** 				TempFree(lhs_subs1_copy_temp)*/
    _37TempFree(_37lhs_subs1_copy_temp_51281);
L2D: 
L2B: 

    /** 		if OpTypeCheck and (left_sym < 0 or SymTab[left_sym][S_VTYPE] != sequence_type) then*/
    if (_25OpTypeCheck_12333 == 0) {
        goto L2E; // [1113] 1163
    }
    _29631 = (_30left_sym_55212 < 0);
    if (_29631 != 0) {
        DeRef(_29632);
        _29632 = 1;
        goto L2F; // [1123] 1151
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29633 = (int)*(((s1_ptr)_2)->base + _30left_sym_55212);
    _2 = (int)SEQ_PTR(_29633);
    _29634 = (int)*(((s1_ptr)_2)->base + 15);
    _29633 = NOVALUE;
    if (IS_ATOM_INT(_29634)) {
        _29635 = (_29634 != _52sequence_type_47103);
    }
    else {
        _29635 = binary_op(NOTEQ, _29634, _52sequence_type_47103);
    }
    _29634 = NOVALUE;
    if (IS_ATOM_INT(_29635))
    _29632 = (_29635 != 0);
    else
    _29632 = DBL_PTR(_29635)->dbl != 0.0;
L2F: 
    if (_29632 == 0)
    {
        _29632 = NOVALUE;
        goto L2E; // [1152] 1163
    }
    else{
        _29632 = NOVALUE;
    }

    /** 			TypeCheck(left_sym)*/
    _30TypeCheck(_30left_sym_55212);
L2E: 
L17: 

    /** 	current_sequence = head( current_sequence, length( current_sequence ) - 1 )*/
    if (IS_SEQUENCE(_37current_sequence_51276)){
            _29636 = SEQ_PTR(_37current_sequence_51276)->length;
    }
    else {
        _29636 = 1;
    }
    _29637 = _29636 - 1;
    _29636 = NOVALUE;
    {
        int len = SEQ_PTR(_37current_sequence_51276)->length;
        int size = (IS_ATOM_INT(_29637)) ? _29637 : (object)(DBL_PTR(_29637)->dbl);
        if (size <= 0){
            DeRef( _37current_sequence_51276 );
            _37current_sequence_51276 = MAKE_SEQ(NewS1(0));
        }
        else if (len <= size) {
            RefDS(_37current_sequence_51276);
            DeRef(_37current_sequence_51276);
            _37current_sequence_51276 = _37current_sequence_51276;
        }
        else{
            Head(SEQ_PTR(_37current_sequence_51276),size+1,&_37current_sequence_51276);
        }
    }
    _29637 = NOVALUE;

    /** 	if not TRANSLATE then*/
    if (_25TRANSLATE_11874 != 0)
    goto L30; // [1187] 1213

    /** 		if OpTrace then*/
    if (_25OpTrace_12332 == 0)
    {
        goto L31; // [1194] 1212
    }
    else{
    }

    /** 			emit_op(DISPLAY_VAR)*/
    _37emit_op(87);

    /** 			emit_addr(left_sym)*/
    _37emit_addr(_30left_sym_55212);
L31: 
L30: 

    /** end procedure*/
    DeRef(_left_var_57475);
    DeRef(_tok_57477);
    _29591 = NOVALUE;
    DeRef(_29546);
    _29546 = NOVALUE;
    DeRef(_29607);
    _29607 = NOVALUE;
    DeRef(_29616);
    _29616 = NOVALUE;
    DeRef(_29611);
    _29611 = NOVALUE;
    DeRef(_29618);
    _29618 = NOVALUE;
    DeRef(_29631);
    _29631 = NOVALUE;
    DeRef(_29635);
    _29635 = NOVALUE;
    return;
    ;
}


void _30Return_statement()
{
    int _tok_57766 = NOVALUE;
    int _pop_57767 = NOVALUE;
    int _last_op_57773 = NOVALUE;
    int _last_pc_57776 = NOVALUE;
    int _is_tail_57779 = NOVALUE;
    int _29671 = NOVALUE;
    int _29669 = NOVALUE;
    int _29668 = NOVALUE;
    int _29667 = NOVALUE;
    int _29666 = NOVALUE;
    int _29664 = NOVALUE;
    int _29663 = NOVALUE;
    int _29662 = NOVALUE;
    int _29661 = NOVALUE;
    int _29660 = NOVALUE;
    int _29659 = NOVALUE;
    int _29658 = NOVALUE;
    int _29657 = NOVALUE;
    int _29653 = NOVALUE;
    int _29652 = NOVALUE;
    int _29650 = NOVALUE;
    int _29649 = NOVALUE;
    int _29648 = NOVALUE;
    int _29647 = NOVALUE;
    int _29646 = NOVALUE;
    int _29645 = NOVALUE;
    int _29644 = NOVALUE;
    int _29643 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer pop*/

    /** 	if CurrentSub = TopLevelSub then*/
    if (_25CurrentSub_12270 != _25TopLevelSub_12269)
    goto L1; // [9] 21

    /** 		CompileErr(130)*/
    RefDS(_22682);
    _43CompileErr(130, _22682, 0);
L1: 

    /** 	integer*/

    /** 		last_op = Last_op(),*/
    _last_op_57773 = _37Last_op();
    if (!IS_ATOM_INT(_last_op_57773)) {
        _1 = (long)(DBL_PTR(_last_op_57773)->dbl);
        if (UNIQUE(DBL_PTR(_last_op_57773)) && (DBL_PTR(_last_op_57773)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_last_op_57773);
        _last_op_57773 = _1;
    }

    /** 		last_pc = Last_pc(),*/
    _last_pc_57776 = _37Last_pc();
    if (!IS_ATOM_INT(_last_pc_57776)) {
        _1 = (long)(DBL_PTR(_last_pc_57776)->dbl);
        if (UNIQUE(DBL_PTR(_last_pc_57776)) && (DBL_PTR(_last_pc_57776)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_last_pc_57776);
        _last_pc_57776 = _1;
    }

    /** 		is_tail = 0*/
    _is_tail_57779 = 0;

    /** 	if last_op = PROC and length(Code) > last_pc and Code[last_pc+1] = CurrentSub then*/
    _29643 = (_last_op_57773 == 27);
    if (_29643 == 0) {
        _29644 = 0;
        goto L2; // [50] 67
    }
    if (IS_SEQUENCE(_25Code_12355)){
            _29645 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29645 = 1;
    }
    _29646 = (_29645 > _last_pc_57776);
    _29645 = NOVALUE;
    _29644 = (_29646 != 0);
L2: 
    if (_29644 == 0) {
        goto L3; // [67] 97
    }
    _29648 = _last_pc_57776 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _29649 = (int)*(((s1_ptr)_2)->base + _29648);
    if (IS_ATOM_INT(_29649)) {
        _29650 = (_29649 == _25CurrentSub_12270);
    }
    else {
        _29650 = binary_op(EQUALS, _29649, _25CurrentSub_12270);
    }
    _29649 = NOVALUE;
    if (_29650 == 0) {
        DeRef(_29650);
        _29650 = NOVALUE;
        goto L3; // [88] 97
    }
    else {
        if (!IS_ATOM_INT(_29650) && DBL_PTR(_29650)->dbl == 0.0){
            DeRef(_29650);
            _29650 = NOVALUE;
            goto L3; // [88] 97
        }
        DeRef(_29650);
        _29650 = NOVALUE;
    }
    DeRef(_29650);
    _29650 = NOVALUE;

    /** 		is_tail = 1*/
    _is_tail_57779 = 1;
L3: 

    /** 	if not TRANSLATE then*/
    if (_25TRANSLATE_11874 != 0)
    goto L4; // [101] 127

    /** 		if OpTrace then*/
    if (_25OpTrace_12332 == 0)
    {
        goto L5; // [108] 126
    }
    else{
    }

    /** 			emit_op(ERASE_PRIVATE_NAMES)*/
    _37emit_op(88);

    /** 			emit_addr(CurrentSub)*/
    _37emit_addr(_25CurrentSub_12270);
L5: 
L4: 

    /** 	if SymTab[CurrentSub][S_TOKEN] != PROC then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _29652 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_29652);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _29653 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _29653 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _29652 = NOVALUE;
    if (binary_op_a(EQUALS, _29653, 27)){
        _29653 = NOVALUE;
        goto L6; // [145] 271
    }
    _29653 = NOVALUE;

    /** 		Expr()*/
    _30Expr();

    /** 		last_op = Last_op()*/
    _last_op_57773 = _37Last_op();
    if (!IS_ATOM_INT(_last_op_57773)) {
        _1 = (long)(DBL_PTR(_last_op_57773)->dbl);
        if (UNIQUE(DBL_PTR(_last_op_57773)) && (DBL_PTR(_last_op_57773)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_last_op_57773);
        _last_op_57773 = _1;
    }

    /** 		last_pc = Last_pc()*/
    _last_pc_57776 = _37Last_pc();
    if (!IS_ATOM_INT(_last_pc_57776)) {
        _1 = (long)(DBL_PTR(_last_pc_57776)->dbl);
        if (UNIQUE(DBL_PTR(_last_pc_57776)) && (DBL_PTR(_last_pc_57776)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_last_pc_57776);
        _last_pc_57776 = _1;
    }

    /** 		if last_op = PROC and length(Code) > last_pc and Code[last_pc+1] = CurrentSub then*/
    _29657 = (_last_op_57773 == 27);
    if (_29657 == 0) {
        _29658 = 0;
        goto L7; // [175] 192
    }
    if (IS_SEQUENCE(_25Code_12355)){
            _29659 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29659 = 1;
    }
    _29660 = (_29659 > _last_pc_57776);
    _29659 = NOVALUE;
    _29658 = (_29660 != 0);
L7: 
    if (_29658 == 0) {
        goto L8; // [192] 251
    }
    _29662 = _last_pc_57776 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _29663 = (int)*(((s1_ptr)_2)->base + _29662);
    if (IS_ATOM_INT(_29663)) {
        _29664 = (_29663 == _25CurrentSub_12270);
    }
    else {
        _29664 = binary_op(EQUALS, _29663, _25CurrentSub_12270);
    }
    _29663 = NOVALUE;
    if (_29664 == 0) {
        DeRef(_29664);
        _29664 = NOVALUE;
        goto L8; // [213] 251
    }
    else {
        if (!IS_ATOM_INT(_29664) && DBL_PTR(_29664)->dbl == 0.0){
            DeRef(_29664);
            _29664 = NOVALUE;
            goto L8; // [213] 251
        }
        DeRef(_29664);
        _29664 = NOVALUE;
    }
    DeRef(_29664);
    _29664 = NOVALUE;

    /** 			pop = Pop() -- prevent cg_stack (code generation stack) leakage*/
    _pop_57767 = _37Pop();
    if (!IS_ATOM_INT(_pop_57767)) {
        _1 = (long)(DBL_PTR(_pop_57767)->dbl);
        if (UNIQUE(DBL_PTR(_pop_57767)) && (DBL_PTR(_pop_57767)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pop_57767);
        _pop_57767 = _1;
    }

    /** 			Code[Last_pc()] = PROC_TAIL*/
    _29666 = _37Last_pc();
    _2 = (int)SEQ_PTR(_25Code_12355);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _25Code_12355 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_29666))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_29666)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _29666);
    _1 = *(int *)_2;
    *(int *)_2 = 203;
    DeRef(_1);

    /** 			if object(pop_temps()) then end if*/
    _29667 = _37pop_temps();
    if( NOVALUE == _29667 ){
        _29668 = 0;
    }
    else{
        if (IS_ATOM_INT(_29667))
        _29668 = 1;
        else if (IS_ATOM_DBL(_29667)) {
             if (IS_ATOM_INT(DoubleToInt(_29667))) {
                 _29668 = 1;
                 } else {
                     _29668 = 2;
                } } else if (IS_SEQUENCE(_29667))
                _29668 = 3;
                else
                _29668 = 0;
            }
            DeRef(_29667);
            _29667 = NOVALUE;
            if (_29668 == 0)
            {
                _29668 = NOVALUE;
                goto L9; // [244] 298
            }
            else{
                _29668 = NOVALUE;
            }
            goto L9; // [248] 298
L8: 

            /** 			FuncReturn = TRUE*/
            _30FuncReturn_55179 = _5TRUE_244;

            /** 			emit_op(RETURNF)*/
            _37emit_op(28);
            goto L9; // [268] 298
L6: 

            /** 		if is_tail then*/
            if (_is_tail_57779 == 0)
            {
                goto LA; // [273] 290
            }
            else{
            }

            /** 			Code[Last_pc()] = PROC_TAIL*/
            _29669 = _37Last_pc();
            _2 = (int)SEQ_PTR(_25Code_12355);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _25Code_12355 = MAKE_SEQ(_2);
            }
            if (!IS_ATOM_INT(_29669))
            _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_29669)->dbl));
            else
            _2 = (int)(((s1_ptr)_2)->base + _29669);
            _1 = *(int *)_2;
            *(int *)_2 = 203;
            DeRef(_1);
LA: 

            /** 		emit_op(RETURNP)*/
            _37emit_op(29);
L9: 

            /** 	tok = next_token()*/
            _0 = _tok_57766;
            _tok_57766 = _30next_token();
            DeRef(_0);

            /** 	putback(tok)*/
            Ref(_tok_57766);
            _30putback(_tok_57766);

            /** 	NotReached(tok[T_ID], "return")*/
            _2 = (int)SEQ_PTR(_tok_57766);
            _29671 = (int)*(((s1_ptr)_2)->base + 1);
            Ref(_29671);
            RefDS(_27115);
            _30NotReached(_29671, _27115);
            _29671 = NOVALUE;

            /** end procedure*/
            DeRef(_tok_57766);
            DeRef(_29643);
            _29643 = NOVALUE;
            DeRef(_29646);
            _29646 = NOVALUE;
            DeRef(_29648);
            _29648 = NOVALUE;
            DeRef(_29657);
            _29657 = NOVALUE;
            DeRef(_29660);
            _29660 = NOVALUE;
            DeRef(_29662);
            _29662 = NOVALUE;
            DeRef(_29666);
            _29666 = NOVALUE;
            DeRef(_29669);
            _29669 = NOVALUE;
            return;
    ;
}


int _30exit_level(int _tok_57855, int _flag_57856)
{
    int _arg_57857 = NOVALUE;
    int _n_57858 = NOVALUE;
    int _num_labels_57859 = NOVALUE;
    int _negative_57860 = NOVALUE;
    int _labels_57861 = NOVALUE;
    int _29700 = NOVALUE;
    int _29699 = NOVALUE;
    int _29698 = NOVALUE;
    int _29697 = NOVALUE;
    int _29696 = NOVALUE;
    int _29693 = NOVALUE;
    int _29692 = NOVALUE;
    int _29691 = NOVALUE;
    int _29689 = NOVALUE;
    int _29688 = NOVALUE;
    int _29687 = NOVALUE;
    int _29686 = NOVALUE;
    int _29684 = NOVALUE;
    int _29679 = NOVALUE;
    int _29678 = NOVALUE;
    int _29676 = NOVALUE;
    int _29673 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer negative = 0*/
    _negative_57860 = 0;

    /** 	if flag then*/
    if (_flag_57856 == 0)
    {
        goto L1; // [10] 25
    }
    else{
    }

    /** 		labels = if_labels*/
    RefDS(_30if_labels_55200);
    DeRef(_labels_57861);
    _labels_57861 = _30if_labels_55200;
    goto L2; // [22] 35
L1: 

    /** 		labels = loop_labels*/
    RefDS(_30loop_labels_55199);
    DeRef(_labels_57861);
    _labels_57861 = _30loop_labels_55199;
L2: 

    /** 	num_labels = length(labels)*/
    if (IS_SEQUENCE(_labels_57861)){
            _num_labels_57859 = SEQ_PTR(_labels_57861)->length;
    }
    else {
        _num_labels_57859 = 1;
    }

    /** 	if tok[T_ID] = MINUS then*/
    _2 = (int)SEQ_PTR(_tok_57855);
    _29673 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29673, 10)){
        _29673 = NOVALUE;
        goto L3; // [52] 67
    }
    _29673 = NOVALUE;

    /** 		tok = next_token()*/
    _0 = _tok_57855;
    _tok_57855 = _30next_token();
    DeRef(_0);

    /** 		negative = 1*/
    _negative_57860 = 1;
L3: 

    /** 	if tok[T_ID]=ATOM then*/
    _2 = (int)SEQ_PTR(_tok_57855);
    _29676 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29676, 502)){
        _29676 = NOVALUE;
        goto L4; // [77] 178
    }
    _29676 = NOVALUE;

    /** 		arg = SymTab[tok[T_SYM]][S_OBJ]*/
    _2 = (int)SEQ_PTR(_tok_57855);
    _29678 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_29678)){
        _29679 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29678)->dbl));
    }
    else{
        _29679 = (int)*(((s1_ptr)_2)->base + _29678);
    }
    DeRef(_arg_57857);
    _2 = (int)SEQ_PTR(_29679);
    _arg_57857 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_arg_57857);
    _29679 = NOVALUE;

    /** 		n = floor(arg)*/
    if (IS_ATOM_INT(_arg_57857))
    _n_57858 = e_floor(_arg_57857);
    else
    _n_57858 = unary_op(FLOOR, _arg_57857);
    if (!IS_ATOM_INT(_n_57858)) {
        _1 = (long)(DBL_PTR(_n_57858)->dbl);
        if (UNIQUE(DBL_PTR(_n_57858)) && (DBL_PTR(_n_57858)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_n_57858);
        _n_57858 = _1;
    }

    /** 		if negative then*/
    if (_negative_57860 == 0)
    {
        goto L5; // [110] 122
    }
    else{
    }

    /** 			n = num_labels - n*/
    _n_57858 = _num_labels_57859 - _n_57858;
    goto L6; // [119] 135
L5: 

    /** 		elsif n = 0 then*/
    if (_n_57858 != 0)
    goto L7; // [124] 134

    /** 			n = num_labels*/
    _n_57858 = _num_labels_57859;
L7: 
L6: 

    /** 		if n<=0 or n>num_labels then*/
    _29684 = (_n_57858 <= 0);
    if (_29684 != 0) {
        goto L8; // [141] 154
    }
    _29686 = (_n_57858 > _num_labels_57859);
    if (_29686 == 0)
    {
        DeRef(_29686);
        _29686 = NOVALUE;
        goto L9; // [150] 162
    }
    else{
        DeRef(_29686);
        _29686 = NOVALUE;
    }
L8: 

    /** 			CompileErr(87)*/
    RefDS(_22682);
    _43CompileErr(87, _22682, 0);
L9: 

    /** 		return {n, next_token()}*/
    _29687 = _30next_token();
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _n_57858;
    ((int *)_2)[2] = _29687;
    _29688 = MAKE_SEQ(_1);
    _29687 = NOVALUE;
    DeRef(_tok_57855);
    DeRef(_arg_57857);
    DeRef(_labels_57861);
    _29678 = NOVALUE;
    DeRef(_29684);
    _29684 = NOVALUE;
    return _29688;
    goto LA; // [175] 266
L4: 

    /** 	elsif tok[T_ID]=STRING then*/
    _2 = (int)SEQ_PTR(_tok_57855);
    _29689 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29689, 503)){
        _29689 = NOVALUE;
        goto LB; // [188] 255
    }
    _29689 = NOVALUE;

    /** 		n = find(SymTab[tok[T_SYM]][S_OBJ],labels)*/
    _2 = (int)SEQ_PTR(_tok_57855);
    _29691 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_29691)){
        _29692 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29691)->dbl));
    }
    else{
        _29692 = (int)*(((s1_ptr)_2)->base + _29691);
    }
    _2 = (int)SEQ_PTR(_29692);
    _29693 = (int)*(((s1_ptr)_2)->base + 1);
    _29692 = NOVALUE;
    _n_57858 = find_from(_29693, _labels_57861, 1);
    _29693 = NOVALUE;

    /** 		if n = 0 then*/
    if (_n_57858 != 0)
    goto LC; // [219] 231

    /** 			CompileErr(152)*/
    RefDS(_22682);
    _43CompileErr(152, _22682, 0);
LC: 

    /** 		return {num_labels + 1 - n, next_token()}*/
    _29696 = _num_labels_57859 + 1;
    if (_29696 > MAXINT){
        _29696 = NewDouble((double)_29696);
    }
    if (IS_ATOM_INT(_29696)) {
        _29697 = _29696 - _n_57858;
        if ((long)((unsigned long)_29697 +(unsigned long) HIGH_BITS) >= 0){
            _29697 = NewDouble((double)_29697);
        }
    }
    else {
        _29697 = NewDouble(DBL_PTR(_29696)->dbl - (double)_n_57858);
    }
    DeRef(_29696);
    _29696 = NOVALUE;
    _29698 = _30next_token();
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _29697;
    ((int *)_2)[2] = _29698;
    _29699 = MAKE_SEQ(_1);
    _29698 = NOVALUE;
    _29697 = NOVALUE;
    DeRef(_tok_57855);
    DeRef(_arg_57857);
    DeRef(_labels_57861);
    _29678 = NOVALUE;
    DeRef(_29684);
    _29684 = NOVALUE;
    DeRef(_29688);
    _29688 = NOVALUE;
    _29691 = NOVALUE;
    return _29699;
    goto LA; // [252] 266
LB: 

    /** 		return {1, tok} -- no parameters*/
    Ref(_tok_57855);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = _tok_57855;
    _29700 = MAKE_SEQ(_1);
    DeRef(_tok_57855);
    DeRef(_arg_57857);
    DeRef(_labels_57861);
    _29678 = NOVALUE;
    DeRef(_29684);
    _29684 = NOVALUE;
    DeRef(_29688);
    _29688 = NOVALUE;
    _29691 = NOVALUE;
    DeRef(_29699);
    _29699 = NOVALUE;
    return _29700;
LA: 
    ;
}


void _30GLabel_statement()
{
    int _tok_57918 = NOVALUE;
    int _labbel_57919 = NOVALUE;
    int _laddr_57920 = NOVALUE;
    int _n_57921 = NOVALUE;
    int _29719 = NOVALUE;
    int _29717 = NOVALUE;
    int _29716 = NOVALUE;
    int _29715 = NOVALUE;
    int _29714 = NOVALUE;
    int _29712 = NOVALUE;
    int _29709 = NOVALUE;
    int _29707 = NOVALUE;
    int _29705 = NOVALUE;
    int _29704 = NOVALUE;
    int _29702 = NOVALUE;
    int _0, _1, _2;
    

    /** 	object labbel*/

    /** 	object laddr*/

    /** 	integer n*/

    /** 	tok = next_token()*/
    _0 = _tok_57918;
    _tok_57918 = _30next_token();
    DeRef(_0);

    /** 	if tok[T_ID] != STRING then*/
    _2 = (int)SEQ_PTR(_tok_57918);
    _29702 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _29702, 503)){
        _29702 = NOVALUE;
        goto L1; // [22] 34
    }
    _29702 = NOVALUE;

    /** 		CompileErr(35)*/
    RefDS(_22682);
    _43CompileErr(35, _22682, 0);
L1: 

    /** 	labbel = SymTab[tok[T_SYM]][S_OBJ]*/
    _2 = (int)SEQ_PTR(_tok_57918);
    _29704 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_29704)){
        _29705 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29704)->dbl));
    }
    else{
        _29705 = (int)*(((s1_ptr)_2)->base + _29704);
    }
    DeRef(_labbel_57919);
    _2 = (int)SEQ_PTR(_29705);
    _labbel_57919 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_labbel_57919);
    _29705 = NOVALUE;

    /** 	laddr = length(Code) + 1*/
    if (IS_SEQUENCE(_25Code_12355)){
            _29707 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29707 = 1;
    }
    _laddr_57920 = _29707 + 1;
    _29707 = NOVALUE;

    /** 	if find(labbel, goto_labels) then*/
    _29709 = find_from(_labbel_57919, _30goto_labels_55182, 1);
    if (_29709 == 0)
    {
        _29709 = NOVALUE;
        goto L2; // [74] 85
    }
    else{
        _29709 = NOVALUE;
    }

    /** 		CompileErr(59)*/
    RefDS(_22682);
    _43CompileErr(59, _22682, 0);
L2: 

    /** 	goto_labels = append(goto_labels, labbel)*/
    Ref(_labbel_57919);
    Append(&_30goto_labels_55182, _30goto_labels_55182, _labbel_57919);

    /** 	goto_addr = append(goto_addr, laddr)*/
    Append(&_30goto_addr_55183, _30goto_addr_55183, _laddr_57920);

    /** 	label_block = append( label_block, top_block() )*/
    _29712 = _66top_block(0);
    Ref(_29712);
    Append(&_30label_block_55186, _30label_block_55186, _29712);
    DeRef(_29712);
    _29712 = NOVALUE;

    /** 	while n with entry do*/
    goto L3; // [115] 174
L4: 
    if (_n_57921 == 0)
    {
        goto L5; // [120] 188
    }
    else{
    }

    /** 		backpatch(goto_list[n], laddr)*/
    _2 = (int)SEQ_PTR(_25goto_list_12394);
    _29714 = (int)*(((s1_ptr)_2)->base + _n_57921);
    Ref(_29714);
    _37backpatch(_29714, _laddr_57920);
    _29714 = NOVALUE;

    /** 		set_glabel_block( goto_ref[n], top_block() )*/
    _2 = (int)SEQ_PTR(_30goto_ref_55185);
    _29715 = (int)*(((s1_ptr)_2)->base + _n_57921);
    _29716 = _66top_block(0);
    Ref(_29715);
    _29set_glabel_block(_29715, _29716);
    _29715 = NOVALUE;
    _29716 = NOVALUE;

    /** 		goto_delay[n] = "" --clear it*/
    RefDS(_22682);
    _2 = (int)SEQ_PTR(_25goto_delay_12393);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _25goto_delay_12393 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _n_57921);
    _1 = *(int *)_2;
    *(int *)_2 = _22682;
    DeRef(_1);

    /** 		goto_line[n] = {-1,""} --clear it*/
    RefDS(_22682);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = _22682;
    _29717 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_30goto_line_55181);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30goto_line_55181 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _n_57921);
    _1 = *(int *)_2;
    *(int *)_2 = _29717;
    if( _1 != _29717 ){
        DeRef(_1);
    }
    _29717 = NOVALUE;

    /** 	entry*/
L3: 

    /** 		n = find(labbel, goto_delay)*/
    _n_57921 = find_from(_labbel_57919, _25goto_delay_12393, 1);

    /** 	end while*/
    goto L4; // [185] 118
L5: 

    /** 	force_uninitialize( map:get( goto_init, labbel, {} ) )*/
    Ref(_30goto_init_55188);
    Ref(_labbel_57919);
    RefDS(_22682);
    _29719 = _32get(_30goto_init_55188, _labbel_57919, _22682);
    _30force_uninitialize(_29719);
    _29719 = NOVALUE;

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L6; // [205] 221
    }
    else{
    }

    /** 		emit_op(GLABEL)*/
    _37emit_op(189);

    /** 		emit_addr(laddr)*/
    _37emit_addr(_laddr_57920);
L6: 

    /** end procedure*/
    DeRef(_tok_57918);
    DeRef(_labbel_57919);
    _29704 = NOVALUE;
    return;
    ;
}


void _30Goto_statement()
{
    int _tok_57968 = NOVALUE;
    int _n_57969 = NOVALUE;
    int _num_labels_57970 = NOVALUE;
    int _32368 = NOVALUE;
    int _29758 = NOVALUE;
    int _29757 = NOVALUE;
    int _29754 = NOVALUE;
    int _29753 = NOVALUE;
    int _29752 = NOVALUE;
    int _29751 = NOVALUE;
    int _29750 = NOVALUE;
    int _29749 = NOVALUE;
    int _29748 = NOVALUE;
    int _29747 = NOVALUE;
    int _29746 = NOVALUE;
    int _29745 = NOVALUE;
    int _29744 = NOVALUE;
    int _29743 = NOVALUE;
    int _29741 = NOVALUE;
    int _29740 = NOVALUE;
    int _29738 = NOVALUE;
    int _29737 = NOVALUE;
    int _29735 = NOVALUE;
    int _29734 = NOVALUE;
    int _29732 = NOVALUE;
    int _29731 = NOVALUE;
    int _29730 = NOVALUE;
    int _29729 = NOVALUE;
    int _29726 = NOVALUE;
    int _29725 = NOVALUE;
    int _29724 = NOVALUE;
    int _29722 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer n*/

    /** 	integer num_labels*/

    /** 	tok = next_token()*/
    _0 = _tok_57968;
    _tok_57968 = _30next_token();
    DeRef(_0);

    /** 	num_labels = length(goto_labels)*/
    if (IS_SEQUENCE(_30goto_labels_55182)){
            _num_labels_57970 = SEQ_PTR(_30goto_labels_55182)->length;
    }
    else {
        _num_labels_57970 = 1;
    }

    /** 	if tok[T_ID]=STRING then*/
    _2 = (int)SEQ_PTR(_tok_57968);
    _29722 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29722, 503)){
        _29722 = NOVALUE;
        goto L1; // [27] 271
    }
    _29722 = NOVALUE;

    /** 		n = find(SymTab[tok[T_SYM]][S_OBJ],goto_labels)*/
    _2 = (int)SEQ_PTR(_tok_57968);
    _29724 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_29724)){
        _29725 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29724)->dbl));
    }
    else{
        _29725 = (int)*(((s1_ptr)_2)->base + _29724);
    }
    _2 = (int)SEQ_PTR(_29725);
    _29726 = (int)*(((s1_ptr)_2)->base + 1);
    _29725 = NOVALUE;
    _n_57969 = find_from(_29726, _30goto_labels_55182, 1);
    _29726 = NOVALUE;

    /** 		if n = 0 then*/
    if (_n_57969 != 0)
    goto L2; // [60] 245

    /** 			goto_delay &= {SymTab[tok[T_SYM]][S_OBJ]}*/
    _2 = (int)SEQ_PTR(_tok_57968);
    _29729 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_29729)){
        _29730 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29729)->dbl));
    }
    else{
        _29730 = (int)*(((s1_ptr)_2)->base + _29729);
    }
    _2 = (int)SEQ_PTR(_29730);
    _29731 = (int)*(((s1_ptr)_2)->base + 1);
    _29730 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_29731);
    *((int *)(_2+4)) = _29731;
    _29732 = MAKE_SEQ(_1);
    _29731 = NOVALUE;
    Concat((object_ptr)&_25goto_delay_12393, _25goto_delay_12393, _29732);
    DeRefDS(_29732);
    _29732 = NOVALUE;

    /** 			goto_list &= length(Code)+2 --not 1???*/
    if (IS_SEQUENCE(_25Code_12355)){
            _29734 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29734 = 1;
    }
    _29735 = _29734 + 2;
    _29734 = NOVALUE;
    Append(&_25goto_list_12394, _25goto_list_12394, _29735);
    _29735 = NOVALUE;

    /** 			goto_line &= {{line_number,ThisLine}}*/
    Ref(_43ThisLine_49532);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _25line_number_12263;
    ((int *)_2)[2] = _43ThisLine_49532;
    _29737 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _29737;
    _29738 = MAKE_SEQ(_1);
    _29737 = NOVALUE;
    Concat((object_ptr)&_30goto_line_55181, _30goto_line_55181, _29738);
    DeRefDS(_29738);
    _29738 = NOVALUE;

    /** 			goto_ref &= new_forward_reference( GOTO, top_block() )*/
    _29740 = _66top_block(0);
    _32368 = 188;
    _29741 = _29new_forward_reference(188, _29740, 188);
    _29740 = NOVALUE;
    _32368 = NOVALUE;
    if (IS_SEQUENCE(_30goto_ref_55185) && IS_ATOM(_29741)) {
        Ref(_29741);
        Append(&_30goto_ref_55185, _30goto_ref_55185, _29741);
    }
    else if (IS_ATOM(_30goto_ref_55185) && IS_SEQUENCE(_29741)) {
    }
    else {
        Concat((object_ptr)&_30goto_ref_55185, _30goto_ref_55185, _29741);
    }
    DeRef(_29741);
    _29741 = NOVALUE;

    /** 			map:put( goto_init, SymTab[tok[T_SYM]][S_OBJ], get_private_uninitialized() )*/
    _2 = (int)SEQ_PTR(_tok_57968);
    _29743 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_29743)){
        _29744 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29743)->dbl));
    }
    else{
        _29744 = (int)*(((s1_ptr)_2)->base + _29743);
    }
    _2 = (int)SEQ_PTR(_29744);
    _29745 = (int)*(((s1_ptr)_2)->base + 1);
    _29744 = NOVALUE;
    _29746 = _30get_private_uninitialized();
    Ref(_30goto_init_55188);
    Ref(_29745);
    _32put(_30goto_init_55188, _29745, _29746, 1, _32threshold_size_13318);
    _29745 = NOVALUE;
    _29746 = NOVALUE;

    /** 			add_data( goto_ref[$], sym_obj( tok[T_SYM] ) )*/
    if (IS_SEQUENCE(_30goto_ref_55185)){
            _29747 = SEQ_PTR(_30goto_ref_55185)->length;
    }
    else {
        _29747 = 1;
    }
    _2 = (int)SEQ_PTR(_30goto_ref_55185);
    _29748 = (int)*(((s1_ptr)_2)->base + _29747);
    _2 = (int)SEQ_PTR(_tok_57968);
    _29749 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_29749);
    _29750 = _52sym_obj(_29749);
    _29749 = NOVALUE;
    Ref(_29748);
    _29add_data(_29748, _29750);
    _29748 = NOVALUE;
    _29750 = NOVALUE;

    /** 			set_line( goto_ref[$], line_number, ThisLine, bp )*/
    if (IS_SEQUENCE(_30goto_ref_55185)){
            _29751 = SEQ_PTR(_30goto_ref_55185)->length;
    }
    else {
        _29751 = 1;
    }
    _2 = (int)SEQ_PTR(_30goto_ref_55185);
    _29752 = (int)*(((s1_ptr)_2)->base + _29751);
    Ref(_29752);
    Ref(_43ThisLine_49532);
    _29set_line(_29752, _25line_number_12263, _43ThisLine_49532, _43bp_49536);
    _29752 = NOVALUE;
    goto L3; // [242] 263
L2: 

    /** 			Goto_block( top_block(), label_block[n] )*/
    _29753 = _66top_block(0);
    _2 = (int)SEQ_PTR(_30label_block_55186);
    _29754 = (int)*(((s1_ptr)_2)->base + _n_57969);
    Ref(_29754);
    _66Goto_block(_29753, _29754, 0);
    _29753 = NOVALUE;
    _29754 = NOVALUE;
L3: 

    /** 		tok = next_token()*/
    _0 = _tok_57968;
    _tok_57968 = _30next_token();
    DeRef(_0);
    goto L4; // [268] 279
L1: 

    /** 		CompileErr(96)*/
    RefDS(_22682);
    _43CompileErr(96, _22682, 0);
L4: 

    /** 	emit_op(GOTO)*/
    _37emit_op(188);

    /** 	if n = 0 then*/
    if (_n_57969 != 0)
    goto L5; // [290] 302

    /** 		emit_addr(0) -- to be back-patched*/
    _37emit_addr(0);
    goto L6; // [299] 314
L5: 

    /** 		emit_addr(goto_addr[n])*/
    _2 = (int)SEQ_PTR(_30goto_addr_55183);
    _29757 = (int)*(((s1_ptr)_2)->base + _n_57969);
    Ref(_29757);
    _37emit_addr(_29757);
    _29757 = NOVALUE;
L6: 

    /** 	putback(tok)*/
    Ref(_tok_57968);
    _30putback(_tok_57968);

    /** 	NotReached(tok[T_ID], "goto")*/
    _2 = (int)SEQ_PTR(_tok_57968);
    _29758 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_29758);
    RefDS(_27057);
    _30NotReached(_29758, _27057);
    _29758 = NOVALUE;

    /** end procedure*/
    DeRef(_tok_57968);
    _29724 = NOVALUE;
    _29729 = NOVALUE;
    _29743 = NOVALUE;
    return;
    ;
}


void _30Exit_statement()
{
    int _addr_inlined_AppendXList_at_63_58071 = NOVALUE;
    int _tok_58054 = NOVALUE;
    int _by_ref_58055 = NOVALUE;
    int _29766 = NOVALUE;
    int _29765 = NOVALUE;
    int _29764 = NOVALUE;
    int _29763 = NOVALUE;
    int _29761 = NOVALUE;
    int _29759 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence by_ref*/

    /** 	if not length(loop_stack) then*/
    if (IS_SEQUENCE(_30loop_stack_55205)){
            _29759 = SEQ_PTR(_30loop_stack_55205)->length;
    }
    else {
        _29759 = 1;
    }
    if (_29759 != 0)
    goto L1; // [10] 21
    _29759 = NOVALUE;

    /** 		CompileErr(88)*/
    RefDS(_22682);
    _43CompileErr(88, _22682, 0);
L1: 

    /** 	by_ref = exit_level(next_token(),0) -- can't pass tok by reference*/
    _29761 = _30next_token();
    _0 = _by_ref_58055;
    _by_ref_58055 = _30exit_level(_29761, 0);
    DeRef(_0);
    _29761 = NOVALUE;

    /** 	Leave_blocks( by_ref[1], LOOP_BLOCK )*/
    _2 = (int)SEQ_PTR(_by_ref_58055);
    _29763 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_29763);
    _66Leave_blocks(_29763, 1);
    _29763 = NOVALUE;

    /** 	emit_op(EXIT)*/
    _37emit_op(61);

    /** 	AppendXList(length(Code)+1)*/
    if (IS_SEQUENCE(_25Code_12355)){
            _29764 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29764 = 1;
    }
    _29765 = _29764 + 1;
    _29764 = NOVALUE;
    _addr_inlined_AppendXList_at_63_58071 = _29765;
    _29765 = NOVALUE;

    /** 	exit_list = append(exit_list, addr)*/
    Append(&_30exit_list_55191, _30exit_list_55191, _addr_inlined_AppendXList_at_63_58071);

    /** end procedure*/
    goto L2; // [78] 81
L2: 

    /** 	exit_delay &= by_ref[1]*/
    _2 = (int)SEQ_PTR(_by_ref_58055);
    _29766 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_30exit_delay_55192) && IS_ATOM(_29766)) {
        Ref(_29766);
        Append(&_30exit_delay_55192, _30exit_delay_55192, _29766);
    }
    else if (IS_ATOM(_30exit_delay_55192) && IS_SEQUENCE(_29766)) {
    }
    else {
        Concat((object_ptr)&_30exit_delay_55192, _30exit_delay_55192, _29766);
    }
    _29766 = NOVALUE;

    /** 	emit_forward_addr()    -- to be back-patched*/
    _30emit_forward_addr();

    /** 	tok = by_ref[2]*/
    DeRef(_tok_58054);
    _2 = (int)SEQ_PTR(_by_ref_58055);
    _tok_58054 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_tok_58054);

    /** 	putback(tok)*/
    Ref(_tok_58054);
    _30putback(_tok_58054);

    /** end procedure*/
    DeRef(_tok_58054);
    DeRefDS(_by_ref_58055);
    return;
    ;
}


void _30Continue_statement()
{
    int _addr_inlined_AppendNList_at_149_58115 = NOVALUE;
    int _tok_58078 = NOVALUE;
    int _by_ref_58079 = NOVALUE;
    int _loop_level_58080 = NOVALUE;
    int _29792 = NOVALUE;
    int _29789 = NOVALUE;
    int _29788 = NOVALUE;
    int _29787 = NOVALUE;
    int _29786 = NOVALUE;
    int _29785 = NOVALUE;
    int _29784 = NOVALUE;
    int _29782 = NOVALUE;
    int _29781 = NOVALUE;
    int _29780 = NOVALUE;
    int _29779 = NOVALUE;
    int _29778 = NOVALUE;
    int _29777 = NOVALUE;
    int _29776 = NOVALUE;
    int _29775 = NOVALUE;
    int _29773 = NOVALUE;
    int _29771 = NOVALUE;
    int _29769 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence by_ref*/

    /** 	integer loop_level*/

    /** 	if not length(loop_stack) then*/
    if (IS_SEQUENCE(_30loop_stack_55205)){
            _29769 = SEQ_PTR(_30loop_stack_55205)->length;
    }
    else {
        _29769 = 1;
    }
    if (_29769 != 0)
    goto L1; // [12] 23
    _29769 = NOVALUE;

    /** 		CompileErr(49)*/
    RefDS(_22682);
    _43CompileErr(49, _22682, 0);
L1: 

    /** 	by_ref = exit_level(next_token(),0) -- can't pass tok by reference*/
    _29771 = _30next_token();
    _0 = _by_ref_58079;
    _by_ref_58079 = _30exit_level(_29771, 0);
    DeRef(_0);
    _29771 = NOVALUE;

    /** 	Leave_blocks( by_ref[1], LOOP_BLOCK )*/
    _2 = (int)SEQ_PTR(_by_ref_58079);
    _29773 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_29773);
    _66Leave_blocks(_29773, 1);
    _29773 = NOVALUE;

    /** 	emit_op(ELSE)*/
    _37emit_op(23);

    /** 	loop_level = by_ref[1]*/
    _2 = (int)SEQ_PTR(_by_ref_58079);
    _loop_level_58080 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_loop_level_58080))
    _loop_level_58080 = (long)DBL_PTR(_loop_level_58080)->dbl;

    /** 	if continue_addr[$+1-loop_level] then -- address is known for while loops*/
    if (IS_SEQUENCE(_30continue_addr_55196)){
            _29775 = SEQ_PTR(_30continue_addr_55196)->length;
    }
    else {
        _29775 = 1;
    }
    _29776 = _29775 + 1;
    _29775 = NOVALUE;
    _29777 = _29776 - _loop_level_58080;
    _29776 = NOVALUE;
    _2 = (int)SEQ_PTR(_30continue_addr_55196);
    _29778 = (int)*(((s1_ptr)_2)->base + _29777);
    if (_29778 == 0)
    {
        _29778 = NOVALUE;
        goto L2; // [79] 138
    }
    else{
        _29778 = NOVALUE;
    }

    /** 		if continue_addr[$+1-loop_level] < 0 then*/
    if (IS_SEQUENCE(_30continue_addr_55196)){
            _29779 = SEQ_PTR(_30continue_addr_55196)->length;
    }
    else {
        _29779 = 1;
    }
    _29780 = _29779 + 1;
    _29779 = NOVALUE;
    _29781 = _29780 - _loop_level_58080;
    _29780 = NOVALUE;
    _2 = (int)SEQ_PTR(_30continue_addr_55196);
    _29782 = (int)*(((s1_ptr)_2)->base + _29781);
    if (_29782 >= 0)
    goto L3; // [101] 113

    /** 			CompileErr(49)*/
    RefDS(_22682);
    _43CompileErr(49, _22682, 0);
L3: 

    /** 		emit_addr(continue_addr[$+1-loop_level])*/
    if (IS_SEQUENCE(_30continue_addr_55196)){
            _29784 = SEQ_PTR(_30continue_addr_55196)->length;
    }
    else {
        _29784 = 1;
    }
    _29785 = _29784 + 1;
    _29784 = NOVALUE;
    _29786 = _29785 - _loop_level_58080;
    _29785 = NOVALUE;
    _2 = (int)SEQ_PTR(_30continue_addr_55196);
    _29787 = (int)*(((s1_ptr)_2)->base + _29786);
    _37emit_addr(_29787);
    _29787 = NOVALUE;
    goto L4; // [135] 182
L2: 

    /** 		AppendNList(length(Code)+1)*/
    if (IS_SEQUENCE(_25Code_12355)){
            _29788 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29788 = 1;
    }
    _29789 = _29788 + 1;
    _29788 = NOVALUE;
    _addr_inlined_AppendNList_at_149_58115 = _29789;
    _29789 = NOVALUE;

    /** 	continue_list = append(continue_list, addr)*/
    Append(&_30continue_list_55193, _30continue_list_55193, _addr_inlined_AppendNList_at_149_58115);

    /** end procedure*/
    goto L5; // [164] 167
L5: 

    /** 		continue_delay &= loop_level*/
    Append(&_30continue_delay_55194, _30continue_delay_55194, _loop_level_58080);

    /** 		emit_forward_addr()    -- to be back-patched*/
    _30emit_forward_addr();
L4: 

    /** 	tok = by_ref[2]*/
    DeRef(_tok_58078);
    _2 = (int)SEQ_PTR(_by_ref_58079);
    _tok_58078 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_tok_58078);

    /** 	putback(tok)*/
    Ref(_tok_58078);
    _30putback(_tok_58078);

    /** 	NotReached(tok[T_ID], "continue")*/
    _2 = (int)SEQ_PTR(_tok_58078);
    _29792 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_29792);
    RefDS(_27019);
    _30NotReached(_29792, _27019);
    _29792 = NOVALUE;

    /** end procedure*/
    DeRef(_tok_58078);
    DeRefDS(_by_ref_58079);
    _29782 = NOVALUE;
    DeRef(_29777);
    _29777 = NOVALUE;
    DeRef(_29781);
    _29781 = NOVALUE;
    DeRef(_29786);
    _29786 = NOVALUE;
    return;
    ;
}


void _30Retry_statement()
{
    int _by_ref_58122 = NOVALUE;
    int _tok_58124 = NOVALUE;
    int _29816 = NOVALUE;
    int _29814 = NOVALUE;
    int _29813 = NOVALUE;
    int _29812 = NOVALUE;
    int _29811 = NOVALUE;
    int _29810 = NOVALUE;
    int _29808 = NOVALUE;
    int _29807 = NOVALUE;
    int _29806 = NOVALUE;
    int _29805 = NOVALUE;
    int _29804 = NOVALUE;
    int _29802 = NOVALUE;
    int _29801 = NOVALUE;
    int _29800 = NOVALUE;
    int _29799 = NOVALUE;
    int _29798 = NOVALUE;
    int _29797 = NOVALUE;
    int _29795 = NOVALUE;
    int _29793 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length(loop_stack) then*/
    if (IS_SEQUENCE(_30loop_stack_55205)){
            _29793 = SEQ_PTR(_30loop_stack_55205)->length;
    }
    else {
        _29793 = 1;
    }
    if (_29793 != 0)
    goto L1; // [8] 19
    _29793 = NOVALUE;

    /** 		CompileErr(131)*/
    RefDS(_22682);
    _43CompileErr(131, _22682, 0);
L1: 

    /** 	by_ref = exit_level(next_token(),0) -- can't pass tok by reference*/
    _29795 = _30next_token();
    _0 = _by_ref_58122;
    _by_ref_58122 = _30exit_level(_29795, 0);
    DeRef(_0);
    _29795 = NOVALUE;

    /** 	Leave_blocks( by_ref[1], LOOP_BLOCK )*/
    _2 = (int)SEQ_PTR(_by_ref_58122);
    _29797 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_29797);
    _66Leave_blocks(_29797, 1);
    _29797 = NOVALUE;

    /** 	if loop_stack[$+1-by_ref[1]]=FOR then*/
    if (IS_SEQUENCE(_30loop_stack_55205)){
            _29798 = SEQ_PTR(_30loop_stack_55205)->length;
    }
    else {
        _29798 = 1;
    }
    _29799 = _29798 + 1;
    _29798 = NOVALUE;
    _2 = (int)SEQ_PTR(_by_ref_58122);
    _29800 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29800)) {
        _29801 = _29799 - _29800;
    }
    else {
        _29801 = binary_op(MINUS, _29799, _29800);
    }
    _29799 = NOVALUE;
    _29800 = NOVALUE;
    _2 = (int)SEQ_PTR(_30loop_stack_55205);
    if (!IS_ATOM_INT(_29801)){
        _29802 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29801)->dbl));
    }
    else{
        _29802 = (int)*(((s1_ptr)_2)->base + _29801);
    }
    if (_29802 != 21)
    goto L2; // [68] 82

    /** 		emit_op(RETRY) -- for Translator to emit a label at the right place*/
    _37emit_op(184);
    goto L3; // [79] 125
L2: 

    /** 		if retry_addr[$+1-by_ref[1]] < 0 then*/
    if (IS_SEQUENCE(_30retry_addr_55197)){
            _29804 = SEQ_PTR(_30retry_addr_55197)->length;
    }
    else {
        _29804 = 1;
    }
    _29805 = _29804 + 1;
    _29804 = NOVALUE;
    _2 = (int)SEQ_PTR(_by_ref_58122);
    _29806 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29806)) {
        _29807 = _29805 - _29806;
    }
    else {
        _29807 = binary_op(MINUS, _29805, _29806);
    }
    _29805 = NOVALUE;
    _29806 = NOVALUE;
    _2 = (int)SEQ_PTR(_30retry_addr_55197);
    if (!IS_ATOM_INT(_29807)){
        _29808 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29807)->dbl));
    }
    else{
        _29808 = (int)*(((s1_ptr)_2)->base + _29807);
    }
    if (_29808 >= 0)
    goto L4; // [105] 117

    /** 			CompileErr(131)*/
    RefDS(_22682);
    _43CompileErr(131, _22682, 0);
L4: 

    /** 		emit_op(ELSE)*/
    _37emit_op(23);
L3: 

    /** 	emit_addr(retry_addr[$+1-by_ref[1]])*/
    if (IS_SEQUENCE(_30retry_addr_55197)){
            _29810 = SEQ_PTR(_30retry_addr_55197)->length;
    }
    else {
        _29810 = 1;
    }
    _29811 = _29810 + 1;
    _29810 = NOVALUE;
    _2 = (int)SEQ_PTR(_by_ref_58122);
    _29812 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29812)) {
        _29813 = _29811 - _29812;
    }
    else {
        _29813 = binary_op(MINUS, _29811, _29812);
    }
    _29811 = NOVALUE;
    _29812 = NOVALUE;
    _2 = (int)SEQ_PTR(_30retry_addr_55197);
    if (!IS_ATOM_INT(_29813)){
        _29814 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29813)->dbl));
    }
    else{
        _29814 = (int)*(((s1_ptr)_2)->base + _29813);
    }
    _37emit_addr(_29814);
    _29814 = NOVALUE;

    /** 	tok = by_ref[2]*/
    DeRef(_tok_58124);
    _2 = (int)SEQ_PTR(_by_ref_58122);
    _tok_58124 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_tok_58124);

    /** 	putback(tok)*/
    Ref(_tok_58124);
    _30putback(_tok_58124);

    /** 	NotReached(tok[T_ID], "retry")*/
    _2 = (int)SEQ_PTR(_tok_58124);
    _29816 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_29816);
    RefDS(_27113);
    _30NotReached(_29816, _27113);
    _29816 = NOVALUE;

    /** end procedure*/
    DeRefDS(_by_ref_58122);
    DeRef(_tok_58124);
    _29802 = NOVALUE;
    _29808 = NOVALUE;
    DeRef(_29801);
    _29801 = NOVALUE;
    DeRef(_29807);
    _29807 = NOVALUE;
    DeRef(_29813);
    _29813 = NOVALUE;
    return;
    ;
}


int _30in_switch()
{
    int _29821 = NOVALUE;
    int _29820 = NOVALUE;
    int _29819 = NOVALUE;
    int _29818 = NOVALUE;
    int _29817 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length( if_stack ) and if_stack[$] = SWITCH then*/
    if (IS_SEQUENCE(_30if_stack_55206)){
            _29817 = SEQ_PTR(_30if_stack_55206)->length;
    }
    else {
        _29817 = 1;
    }
    if (_29817 == 0) {
        goto L1; // [8] 40
    }
    if (IS_SEQUENCE(_30if_stack_55206)){
            _29819 = SEQ_PTR(_30if_stack_55206)->length;
    }
    else {
        _29819 = 1;
    }
    _2 = (int)SEQ_PTR(_30if_stack_55206);
    _29820 = (int)*(((s1_ptr)_2)->base + _29819);
    _29821 = (_29820 == 185);
    _29820 = NOVALUE;
    if (_29821 == 0)
    {
        DeRef(_29821);
        _29821 = NOVALUE;
        goto L1; // [28] 40
    }
    else{
        DeRef(_29821);
        _29821 = NOVALUE;
    }

    /** 		return 1*/
    return 1;
    goto L2; // [37] 47
L1: 

    /** 		return 0*/
    return 0;
L2: 
    ;
}


void _30Break_statement()
{
    int _addr_inlined_AppendEList_at_63_58194 = NOVALUE;
    int _tok_58177 = NOVALUE;
    int _by_ref_58178 = NOVALUE;
    int _29832 = NOVALUE;
    int _29829 = NOVALUE;
    int _29828 = NOVALUE;
    int _29827 = NOVALUE;
    int _29826 = NOVALUE;
    int _29824 = NOVALUE;
    int _29822 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence by_ref*/

    /** 	if not length(if_labels) then*/
    if (IS_SEQUENCE(_30if_labels_55200)){
            _29822 = SEQ_PTR(_30if_labels_55200)->length;
    }
    else {
        _29822 = 1;
    }
    if (_29822 != 0)
    goto L1; // [10] 21
    _29822 = NOVALUE;

    /** 		CompileErr(40)*/
    RefDS(_22682);
    _43CompileErr(40, _22682, 0);
L1: 

    /** 	by_ref = exit_level(next_token(),1)*/
    _29824 = _30next_token();
    _0 = _by_ref_58178;
    _by_ref_58178 = _30exit_level(_29824, 1);
    DeRef(_0);
    _29824 = NOVALUE;

    /** 	Leave_blocks( by_ref[1], CONDITIONAL_BLOCK )*/
    _2 = (int)SEQ_PTR(_by_ref_58178);
    _29826 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_29826);
    _66Leave_blocks(_29826, 2);
    _29826 = NOVALUE;

    /** 	emit_op(ELSE)*/
    _37emit_op(23);

    /** 	AppendEList(length(Code)+1)*/
    if (IS_SEQUENCE(_25Code_12355)){
            _29827 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29827 = 1;
    }
    _29828 = _29827 + 1;
    _29827 = NOVALUE;
    _addr_inlined_AppendEList_at_63_58194 = _29828;
    _29828 = NOVALUE;

    /** 	break_list = append(break_list, addr)*/
    Append(&_30break_list_55189, _30break_list_55189, _addr_inlined_AppendEList_at_63_58194);

    /** end procedure*/
    goto L2; // [78] 81
L2: 

    /** 	break_delay &= by_ref[1]*/
    _2 = (int)SEQ_PTR(_by_ref_58178);
    _29829 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_30break_delay_55190) && IS_ATOM(_29829)) {
        Ref(_29829);
        Append(&_30break_delay_55190, _30break_delay_55190, _29829);
    }
    else if (IS_ATOM(_30break_delay_55190) && IS_SEQUENCE(_29829)) {
    }
    else {
        Concat((object_ptr)&_30break_delay_55190, _30break_delay_55190, _29829);
    }
    _29829 = NOVALUE;

    /** 	emit_forward_addr()    -- to be back-patched*/
    _30emit_forward_addr();

    /** 	tok = by_ref[2]*/
    DeRef(_tok_58177);
    _2 = (int)SEQ_PTR(_by_ref_58178);
    _tok_58177 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_tok_58177);

    /** 	putback(tok)*/
    Ref(_tok_58177);
    _30putback(_tok_58177);

    /** 	NotReached(tok[T_ID], "break")*/
    _2 = (int)SEQ_PTR(_tok_58177);
    _29832 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_29832);
    RefDS(_27003);
    _30NotReached(_29832, _27003);
    _29832 = NOVALUE;

    /** end procedure*/
    DeRef(_tok_58177);
    DeRefDS(_by_ref_58178);
    return;
    ;
}


int _30finish_block_header(int _opcode_58203)
{
    int _tok_58205 = NOVALUE;
    int _labbel_58206 = NOVALUE;
    int _has_entry_58207 = NOVALUE;
    int _29886 = NOVALUE;
    int _29885 = NOVALUE;
    int _29884 = NOVALUE;
    int _29883 = NOVALUE;
    int _29880 = NOVALUE;
    int _29875 = NOVALUE;
    int _29872 = NOVALUE;
    int _29870 = NOVALUE;
    int _29867 = NOVALUE;
    int _29866 = NOVALUE;
    int _29864 = NOVALUE;
    int _29861 = NOVALUE;
    int _29858 = NOVALUE;
    int _29857 = NOVALUE;
    int _29855 = NOVALUE;
    int _29853 = NOVALUE;
    int _29850 = NOVALUE;
    int _29847 = NOVALUE;
    int _29846 = NOVALUE;
    int _29844 = NOVALUE;
    int _29842 = NOVALUE;
    int _29841 = NOVALUE;
    int _29840 = NOVALUE;
    int _29837 = NOVALUE;
    int _29834 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	object labbel*/

    /** 	integer has_entry*/

    /** 	tok = next_token()*/
    _0 = _tok_58205;
    _tok_58205 = _30next_token();
    DeRef(_0);

    /** 	has_entry=0*/
    _has_entry_58207 = 0;

    /** 	if tok[T_ID] = WITH then*/
    _2 = (int)SEQ_PTR(_tok_58205);
    _29834 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29834, 420)){
        _29834 = NOVALUE;
        goto L1; // [27] 156
    }
    _29834 = NOVALUE;

    /** 		tok = next_token()*/
    _0 = _tok_58205;
    _tok_58205 = _30next_token();
    DeRef(_0);

    /** 		switch tok[T_ID] do*/
    _2 = (int)SEQ_PTR(_tok_58205);
    _29837 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_29837) ){
        goto L2; // [44] 138
    }
    if(!IS_ATOM_INT(_29837)){
        if( (DBL_PTR(_29837)->dbl != (double) ((int) DBL_PTR(_29837)->dbl) ) ){
            goto L2; // [44] 138
        }
        _0 = (int) DBL_PTR(_29837)->dbl;
    }
    else {
        _0 = _29837;
    };
    _29837 = NOVALUE;
    switch ( _0 ){ 

        /** 		    case ENTRY then*/
        case 424:

        /** 				if not (opcode = WHILE or opcode = LOOP) then*/
        _29840 = (_opcode_58203 == 47);
        if (_29840 != 0) {
            DeRef(_29841);
            _29841 = 1;
            goto L3; // [61] 75
        }
        _29842 = (_opcode_58203 == 422);
        _29841 = (_29842 != 0);
L3: 
        if (_29841 != 0)
        goto L4; // [75] 86
        _29841 = NOVALUE;

        /** 					CompileErr(14)*/
        RefDS(_22682);
        _43CompileErr(14, _22682, 0);
L4: 

        /** 			    has_entry = 1*/
        _has_entry_58207 = 1;
        goto L5; // [91] 148

        /** 			case FALLTHRU then*/
        case 431:

        /** 				if not opcode = SWITCH then*/
        _29844 = (_opcode_58203 == 0);
        if (_29844 != 185)
        goto L6; // [104] 116

        /** 					CompileErr(13)*/
        RefDS(_22682);
        _43CompileErr(13, _22682, 0);
L6: 

        /** 				switch_stack[$][SWITCH_FALLTHRU] = 1*/
        if (IS_SEQUENCE(_30switch_stack_55405)){
                _29846 = SEQ_PTR(_30switch_stack_55405)->length;
        }
        else {
            _29846 = 1;
        }
        _2 = (int)SEQ_PTR(_30switch_stack_55405);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _30switch_stack_55405 = MAKE_SEQ(_2);
        }
        _3 = (int)(_29846 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = 1;
        DeRef(_1);
        _29847 = NOVALUE;
        goto L5; // [134] 148

        /** 			case else*/
        default:
L2: 

        /** 			    CompileErr(27)*/
        RefDS(_22682);
        _43CompileErr(27, _22682, 0);
    ;}L5: 

    /**         tok = next_token()*/
    _0 = _tok_58205;
    _tok_58205 = _30next_token();
    DeRef(_0);
    goto L7; // [153] 244
L1: 

    /** 	elsif tok[T_ID] = WITHOUT then*/
    _2 = (int)SEQ_PTR(_tok_58205);
    _29850 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29850, 421)){
        _29850 = NOVALUE;
        goto L8; // [166] 243
    }
    _29850 = NOVALUE;

    /** 		tok = next_token()*/
    _0 = _tok_58205;
    _tok_58205 = _30next_token();
    DeRef(_0);

    /** 		if tok[T_ID] = FALLTHRU then*/
    _2 = (int)SEQ_PTR(_tok_58205);
    _29853 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29853, 431)){
        _29853 = NOVALUE;
        goto L9; // [185] 229
    }
    _29853 = NOVALUE;

    /** 			if not opcode = SWITCH then*/
    _29855 = (_opcode_58203 == 0);
    if (_29855 != 185)
    goto LA; // [196] 208

    /** 				CompileErr(15)*/
    RefDS(_22682);
    _43CompileErr(15, _22682, 0);
LA: 

    /** 			switch_stack[$][SWITCH_FALLTHRU] = 0*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _29857 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _29857 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30switch_stack_55405 = MAKE_SEQ(_2);
    }
    _3 = (int)(_29857 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _29858 = NOVALUE;
    goto LB; // [226] 237
L9: 

    /** 			CompileErr(27)*/
    RefDS(_22682);
    _43CompileErr(27, _22682, 0);
LB: 

    /**         tok = next_token()*/
    _0 = _tok_58205;
    _tok_58205 = _30next_token();
    DeRef(_0);
L8: 
L7: 

    /** 	labbel=0*/
    DeRef(_labbel_58206);
    _labbel_58206 = 0;

    /** 	if tok[T_ID]=LABEL then*/
    _2 = (int)SEQ_PTR(_tok_58205);
    _29861 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29861, 419)){
        _29861 = NOVALUE;
        goto LC; // [259] 321
    }
    _29861 = NOVALUE;

    /** 		tok = next_token()*/
    _0 = _tok_58205;
    _tok_58205 = _30next_token();
    DeRef(_0);

    /** 		if tok[T_ID] != STRING then*/
    _2 = (int)SEQ_PTR(_tok_58205);
    _29864 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _29864, 503)){
        _29864 = NOVALUE;
        goto LD; // [278] 290
    }
    _29864 = NOVALUE;

    /** 			CompileErr(38)*/
    RefDS(_22682);
    _43CompileErr(38, _22682, 0);
LD: 

    /** 		labbel = SymTab[tok[T_SYM]][S_OBJ]*/
    _2 = (int)SEQ_PTR(_tok_58205);
    _29866 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_29866)){
        _29867 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_29866)->dbl));
    }
    else{
        _29867 = (int)*(((s1_ptr)_2)->base + _29866);
    }
    DeRef(_labbel_58206);
    _2 = (int)SEQ_PTR(_29867);
    _labbel_58206 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_labbel_58206);
    _29867 = NOVALUE;

    /** 		block_label( labbel )*/
    Ref(_labbel_58206);
    _66block_label(_labbel_58206);

    /** 		tok = next_token()*/
    _0 = _tok_58205;
    _tok_58205 = _30next_token();
    DeRef(_0);
LC: 

    /** 	if opcode = IF or opcode = SWITCH then*/
    _29870 = (_opcode_58203 == 20);
    if (_29870 != 0) {
        goto LE; // [329] 344
    }
    _29872 = (_opcode_58203 == 185);
    if (_29872 == 0)
    {
        DeRef(_29872);
        _29872 = NOVALUE;
        goto LF; // [340] 355
    }
    else{
        DeRef(_29872);
        _29872 = NOVALUE;
    }
LE: 

    /** 		if_labels = append(if_labels,labbel)*/
    Ref(_labbel_58206);
    Append(&_30if_labels_55200, _30if_labels_55200, _labbel_58206);
    goto L10; // [352] 364
LF: 

    /** 		loop_labels = append(loop_labels,labbel)*/
    Ref(_labbel_58206);
    Append(&_30loop_labels_55199, _30loop_labels_55199, _labbel_58206);
L10: 

    /** 	if block_index=length(block_list) then*/
    if (IS_SEQUENCE(_30block_list_55201)){
            _29875 = SEQ_PTR(_30block_list_55201)->length;
    }
    else {
        _29875 = 1;
    }
    if (_30block_index_55202 != _29875)
    goto L11; // [373] 396

    /** 	    block_list &= opcode*/
    Append(&_30block_list_55201, _30block_list_55201, _opcode_58203);

    /** 	    block_index += 1*/
    _30block_index_55202 = _30block_index_55202 + 1;
    goto L12; // [393] 415
L11: 

    /** 	    block_index += 1*/
    _30block_index_55202 = _30block_index_55202 + 1;

    /** 	    block_list[block_index] = opcode*/
    _2 = (int)SEQ_PTR(_30block_list_55201);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30block_list_55201 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _30block_index_55202);
    *(int *)_2 = _opcode_58203;
L12: 

    /** 	if tok[T_ID]=ENTRY then*/
    _2 = (int)SEQ_PTR(_tok_58205);
    _29880 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29880, 424)){
        _29880 = NOVALUE;
        goto L13; // [425] 453
    }
    _29880 = NOVALUE;

    /** 	    if has_entry then*/
    if (_has_entry_58207 == 0)
    {
        goto L14; // [431] 442
    }
    else{
    }

    /** 	        CompileErr(64)*/
    RefDS(_22682);
    _43CompileErr(64, _22682, 0);
L14: 

    /** 	    has_entry=1*/
    _has_entry_58207 = 1;

    /** 	    tok=next_token()*/
    _0 = _tok_58205;
    _tok_58205 = _30next_token();
    DeRef(_0);
L13: 

    /** 	if has_entry and (opcode = IF or opcode = SWITCH) then*/
    if (_has_entry_58207 == 0) {
        goto L15; // [455] 491
    }
    _29884 = (_opcode_58203 == 20);
    if (_29884 != 0) {
        DeRef(_29885);
        _29885 = 1;
        goto L16; // [465] 479
    }
    _29886 = (_opcode_58203 == 185);
    _29885 = (_29886 != 0);
L16: 
    if (_29885 == 0)
    {
        _29885 = NOVALUE;
        goto L15; // [480] 491
    }
    else{
        _29885 = NOVALUE;
    }

    /** 		CompileErr(80)*/
    RefDS(_22682);
    _43CompileErr(80, _22682, 0);
L15: 

    /** 	if opcode = IF then*/
    if (_opcode_58203 != 20)
    goto L17; // [495] 511

    /** 		opcode = THEN*/
    _opcode_58203 = 410;
    goto L18; // [508] 521
L17: 

    /** 		opcode = DO*/
    _opcode_58203 = 411;
L18: 

    /** 	putback(tok)*/
    Ref(_tok_58205);
    _30putback(_tok_58205);

    /** 	tok_match(opcode)*/
    _30tok_match(_opcode_58203, 0);

    /** 	return has_entry*/
    DeRef(_tok_58205);
    DeRef(_labbel_58206);
    DeRef(_29840);
    _29840 = NOVALUE;
    DeRef(_29842);
    _29842 = NOVALUE;
    DeRef(_29844);
    _29844 = NOVALUE;
    DeRef(_29855);
    _29855 = NOVALUE;
    _29866 = NOVALUE;
    DeRef(_29870);
    _29870 = NOVALUE;
    DeRef(_29884);
    _29884 = NOVALUE;
    DeRef(_29886);
    _29886 = NOVALUE;
    return _has_entry_58207;
    ;
}


void _30If_statement()
{
    int _addr_inlined_AppendEList_at_624_58453 = NOVALUE;
    int _addr_inlined_AppendEList_at_260_58382 = NOVALUE;
    int _tok_58325 = NOVALUE;
    int _prev_false_58326 = NOVALUE;
    int _prev_false2_58327 = NOVALUE;
    int _elist_base_58328 = NOVALUE;
    int _temps_58336 = NOVALUE;
    int _32367 = NOVALUE;
    int _29952 = NOVALUE;
    int _29951 = NOVALUE;
    int _29948 = NOVALUE;
    int _29947 = NOVALUE;
    int _29945 = NOVALUE;
    int _29944 = NOVALUE;
    int _29943 = NOVALUE;
    int _29941 = NOVALUE;
    int _29940 = NOVALUE;
    int _29938 = NOVALUE;
    int _29937 = NOVALUE;
    int _29936 = NOVALUE;
    int _29934 = NOVALUE;
    int _29933 = NOVALUE;
    int _29931 = NOVALUE;
    int _29930 = NOVALUE;
    int _29929 = NOVALUE;
    int _29928 = NOVALUE;
    int _29926 = NOVALUE;
    int _29925 = NOVALUE;
    int _29922 = NOVALUE;
    int _29920 = NOVALUE;
    int _29919 = NOVALUE;
    int _29918 = NOVALUE;
    int _29915 = NOVALUE;
    int _29912 = NOVALUE;
    int _29911 = NOVALUE;
    int _29909 = NOVALUE;
    int _29908 = NOVALUE;
    int _29906 = NOVALUE;
    int _29905 = NOVALUE;
    int _29903 = NOVALUE;
    int _29900 = NOVALUE;
    int _29898 = NOVALUE;
    int _29897 = NOVALUE;
    int _29896 = NOVALUE;
    int _29892 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer prev_false*/

    /** 	integer prev_false2*/

    /** 	integer elist_base*/

    /** 	if_stack &= IF*/
    Append(&_30if_stack_55206, _30if_stack_55206, 20);

    /** 	Start_block( IF )*/
    _66Start_block(20, 0);

    /** 	elist_base = length(break_list)*/
    if (IS_SEQUENCE(_30break_list_55189)){
            _elist_base_58328 = SEQ_PTR(_30break_list_55189)->length;
    }
    else {
        _elist_base_58328 = 1;
    }

    /** 	short_circuit += 1*/
    _30short_circuit_55171 = _30short_circuit_55171 + 1;

    /** 	short_circuit_B = FALSE*/
    _30short_circuit_B_55173 = _5FALSE_242;

    /** 	SC1_type = 0*/
    _30SC1_type_55176 = 0;

    /** 	Expr()*/
    _30Expr();

    /** 	sequence temps = get_temps()*/
    _32367 = Repeat(_22682, 2);
    _0 = _temps_58336;
    _temps_58336 = _37get_temps(_32367);
    DeRef(_0);
    _32367 = NOVALUE;

    /** 	emit_op(IF)*/
    _37emit_op(20);

    /** 	prev_false = length(Code)+1*/
    if (IS_SEQUENCE(_25Code_12355)){
            _29892 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29892 = 1;
    }
    _prev_false_58326 = _29892 + 1;
    _29892 = NOVALUE;

    /** 	emit_forward_addr() -- to be patched*/
    _30emit_forward_addr();

    /** 	prev_false2=finish_block_header(IF)  -- 0*/
    _prev_false2_58327 = _30finish_block_header(20);
    if (!IS_ATOM_INT(_prev_false2_58327)) {
        _1 = (long)(DBL_PTR(_prev_false2_58327)->dbl);
        if (UNIQUE(DBL_PTR(_prev_false2_58327)) && (DBL_PTR(_prev_false2_58327)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_prev_false2_58327);
        _prev_false2_58327 = _1;
    }

    /** 	if SC1_type = OR then*/
    if (_30SC1_type_55176 != 9)
    goto L1; // [106] 159

    /** 		backpatch(SC1_patch-3, SC1_OR_IF)*/
    _29896 = _30SC1_patch_55175 - 3;
    if ((long)((unsigned long)_29896 +(unsigned long) HIGH_BITS) >= 0){
        _29896 = NewDouble((double)_29896);
    }
    _37backpatch(_29896, 147);
    _29896 = NOVALUE;

    /** 		if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L2; // [128] 139
    }
    else{
    }

    /** 			emit_op(NOP1)  -- to get label here*/
    _37emit_op(159);
L2: 

    /** 		backpatch(SC1_patch, length(Code)+1)*/
    if (IS_SEQUENCE(_25Code_12355)){
            _29897 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29897 = 1;
    }
    _29898 = _29897 + 1;
    _29897 = NOVALUE;
    _37backpatch(_30SC1_patch_55175, _29898);
    _29898 = NOVALUE;
    goto L3; // [156] 192
L1: 

    /** 	elsif SC1_type = AND then*/
    if (_30SC1_type_55176 != 8)
    goto L4; // [165] 191

    /** 		backpatch(SC1_patch-3, SC1_AND_IF)*/
    _29900 = _30SC1_patch_55175 - 3;
    if ((long)((unsigned long)_29900 +(unsigned long) HIGH_BITS) >= 0){
        _29900 = NewDouble((double)_29900);
    }
    _37backpatch(_29900, 146);
    _29900 = NOVALUE;

    /** 		prev_false2 = SC1_patch*/
    _prev_false2_58327 = _30SC1_patch_55175;
L4: 
L3: 

    /** 	short_circuit -= 1*/
    _30short_circuit_55171 = _30short_circuit_55171 - 1;

    /** 	Statement_list()*/
    _30Statement_list();

    /** 	tok = next_token()*/
    _0 = _tok_58325;
    _tok_58325 = _30next_token();
    DeRef(_0);

    /** 	while tok[T_ID] = ELSIF do*/
L5: 
    _2 = (int)SEQ_PTR(_tok_58325);
    _29903 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29903, 414)){
        _29903 = NOVALUE;
        goto L6; // [222] 532
    }
    _29903 = NOVALUE;

    /** 		Sibling_block( IF )*/
    _66Sibling_block(20);

    /** 		emit_op(ELSE)*/
    _37emit_op(23);

    /** 		AppendEList(length(Code)+1)*/
    if (IS_SEQUENCE(_25Code_12355)){
            _29905 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29905 = 1;
    }
    _29906 = _29905 + 1;
    _29905 = NOVALUE;
    _addr_inlined_AppendEList_at_260_58382 = _29906;
    _29906 = NOVALUE;

    /** 	break_list = append(break_list, addr)*/
    Append(&_30break_list_55189, _30break_list_55189, _addr_inlined_AppendEList_at_260_58382);

    /** end procedure*/
    goto L7; // [266] 269
L7: 

    /** 		break_delay &= 1*/
    Append(&_30break_delay_55190, _30break_delay_55190, 1);

    /** 		emit_forward_addr()  -- to be patched*/
    _30emit_forward_addr();

    /** 		if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L8; // [287] 298
    }
    else{
    }

    /** 			emit_op(NOP1)*/
    _37emit_op(159);
L8: 

    /** 		backpatch(prev_false, length(Code)+1)*/
    if (IS_SEQUENCE(_25Code_12355)){
            _29908 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29908 = 1;
    }
    _29909 = _29908 + 1;
    _29908 = NOVALUE;
    _37backpatch(_prev_false_58326, _29909);
    _29909 = NOVALUE;

    /** 		if prev_false2 != 0 then*/
    if (_prev_false2_58327 == 0)
    goto L9; // [315] 335

    /** 			backpatch(prev_false2, length(Code)+1)*/
    if (IS_SEQUENCE(_25Code_12355)){
            _29911 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29911 = 1;
    }
    _29912 = _29911 + 1;
    _29911 = NOVALUE;
    _37backpatch(_prev_false2_58327, _29912);
    _29912 = NOVALUE;
L9: 

    /** 		StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 		short_circuit += 1*/
    _30short_circuit_55171 = _30short_circuit_55171 + 1;

    /** 		short_circuit_B = FALSE*/
    _30short_circuit_B_55173 = _5FALSE_242;

    /** 		SC1_type = 0*/
    _30SC1_type_55176 = 0;

    /** 		push_temps( temps )*/
    RefDS(_temps_58336);
    _37push_temps(_temps_58336);

    /** 		Expr()*/
    _30Expr();

    /** 		temps = get_temps( temps )*/
    RefDS(_temps_58336);
    _0 = _temps_58336;
    _temps_58336 = _37get_temps(_temps_58336);
    DeRefDS(_0);

    /** 		emit_op(IF)*/
    _37emit_op(20);

    /** 		prev_false = length(Code)+1*/
    if (IS_SEQUENCE(_25Code_12355)){
            _29915 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29915 = 1;
    }
    _prev_false_58326 = _29915 + 1;
    _29915 = NOVALUE;

    /** 		prev_false2 = 0*/
    _prev_false2_58327 = 0;

    /** 		emit_forward_addr() -- to be patched*/
    _30emit_forward_addr();

    /** 		if SC1_type = OR then*/
    if (_30SC1_type_55176 != 9)
    goto LA; // [416] 469

    /** 			backpatch(SC1_patch-3, SC1_OR_IF)*/
    _29918 = _30SC1_patch_55175 - 3;
    if ((long)((unsigned long)_29918 +(unsigned long) HIGH_BITS) >= 0){
        _29918 = NewDouble((double)_29918);
    }
    _37backpatch(_29918, 147);
    _29918 = NOVALUE;

    /** 			if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto LB; // [438] 449
    }
    else{
    }

    /** 				emit_op(NOP1)*/
    _37emit_op(159);
LB: 

    /** 			backpatch(SC1_patch, length(Code)+1)*/
    if (IS_SEQUENCE(_25Code_12355)){
            _29919 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29919 = 1;
    }
    _29920 = _29919 + 1;
    _29919 = NOVALUE;
    _37backpatch(_30SC1_patch_55175, _29920);
    _29920 = NOVALUE;
    goto LC; // [466] 502
LA: 

    /** 		elsif SC1_type = AND then*/
    if (_30SC1_type_55176 != 8)
    goto LD; // [475] 501

    /** 			backpatch(SC1_patch-3, SC1_AND_IF)*/
    _29922 = _30SC1_patch_55175 - 3;
    if ((long)((unsigned long)_29922 +(unsigned long) HIGH_BITS) >= 0){
        _29922 = NewDouble((double)_29922);
    }
    _37backpatch(_29922, 146);
    _29922 = NOVALUE;

    /** 			prev_false2 = SC1_patch*/
    _prev_false2_58327 = _30SC1_patch_55175;
LD: 
LC: 

    /** 		short_circuit -= 1*/
    _30short_circuit_55171 = _30short_circuit_55171 - 1;

    /** 		tok_match(THEN)*/
    _30tok_match(410, 0);

    /** 		Statement_list()*/
    _30Statement_list();

    /** 		tok = next_token()*/
    _0 = _tok_58325;
    _tok_58325 = _30next_token();
    DeRef(_0);

    /** 	end while*/
    goto L5; // [529] 214
L6: 

    /** 	if tok[T_ID] = ELSE or length(temps[1]) then*/
    _2 = (int)SEQ_PTR(_tok_58325);
    _29925 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_29925)) {
        _29926 = (_29925 == 23);
    }
    else {
        _29926 = binary_op(EQUALS, _29925, 23);
    }
    _29925 = NOVALUE;
    if (IS_ATOM_INT(_29926)) {
        if (_29926 != 0) {
            goto LE; // [546] 562
        }
    }
    else {
        if (DBL_PTR(_29926)->dbl != 0.0) {
            goto LE; // [546] 562
        }
    }
    _2 = (int)SEQ_PTR(_temps_58336);
    _29928 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_29928)){
            _29929 = SEQ_PTR(_29928)->length;
    }
    else {
        _29929 = 1;
    }
    _29928 = NOVALUE;
    if (_29929 == 0)
    {
        _29929 = NOVALUE;
        goto LF; // [558] 717
    }
    else{
        _29929 = NOVALUE;
    }
LE: 

    /** 		Sibling_block( IF )*/
    _66Sibling_block(20);

    /** 		StartSourceLine(FALSE, , COVERAGE_SUPPRESS )*/
    _37StartSourceLine(_5FALSE_242, 0, 1);

    /** 		emit_op(ELSE)*/
    _37emit_op(23);

    /** 		AppendEList(length(Code)+1)*/
    if (IS_SEQUENCE(_25Code_12355)){
            _29930 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29930 = 1;
    }
    _29931 = _29930 + 1;
    _29930 = NOVALUE;
    _addr_inlined_AppendEList_at_624_58453 = _29931;
    _29931 = NOVALUE;

    /** 	break_list = append(break_list, addr)*/
    Append(&_30break_list_55189, _30break_list_55189, _addr_inlined_AppendEList_at_624_58453);

    /** end procedure*/
    goto L10; // [613] 616
L10: 

    /** 		break_delay &= 1*/
    Append(&_30break_delay_55190, _30break_delay_55190, 1);

    /** 		emit_forward_addr() -- to be patched*/
    _30emit_forward_addr();

    /** 		if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L11; // [634] 645
    }
    else{
    }

    /** 			emit_op(NOP1)*/
    _37emit_op(159);
L11: 

    /** 		backpatch(prev_false, length(Code)+1)*/
    if (IS_SEQUENCE(_25Code_12355)){
            _29933 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29933 = 1;
    }
    _29934 = _29933 + 1;
    _29933 = NOVALUE;
    _37backpatch(_prev_false_58326, _29934);
    _29934 = NOVALUE;

    /** 		if prev_false2 != 0 then*/
    if (_prev_false2_58327 == 0)
    goto L12; // [662] 682

    /** 			backpatch(prev_false2, length(Code)+1)*/
    if (IS_SEQUENCE(_25Code_12355)){
            _29936 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29936 = 1;
    }
    _29937 = _29936 + 1;
    _29936 = NOVALUE;
    _37backpatch(_prev_false2_58327, _29937);
    _29937 = NOVALUE;
L12: 

    /** 		push_temps( temps )*/
    RefDS(_temps_58336);
    _37push_temps(_temps_58336);

    /** 		if tok[T_ID] = ELSE then*/
    _2 = (int)SEQ_PTR(_tok_58325);
    _29938 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _29938, 23)){
        _29938 = NOVALUE;
        goto L13; // [697] 708
    }
    _29938 = NOVALUE;

    /** 			Statement_list()*/
    _30Statement_list();
    goto L14; // [705] 775
L13: 

    /** 			putback(tok)*/
    Ref(_tok_58325);
    _30putback(_tok_58325);
    goto L14; // [714] 775
LF: 

    /** 		putback(tok)*/
    Ref(_tok_58325);
    _30putback(_tok_58325);

    /** 		if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L15; // [726] 737
    }
    else{
    }

    /** 			emit_op(NOP1)*/
    _37emit_op(159);
L15: 

    /** 		backpatch(prev_false, length(Code)+1)*/
    if (IS_SEQUENCE(_25Code_12355)){
            _29940 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29940 = 1;
    }
    _29941 = _29940 + 1;
    _29940 = NOVALUE;
    _37backpatch(_prev_false_58326, _29941);
    _29941 = NOVALUE;

    /** 		if prev_false2 != 0 then*/
    if (_prev_false2_58327 == 0)
    goto L16; // [754] 774

    /** 			backpatch(prev_false2, length(Code)+1)*/
    if (IS_SEQUENCE(_25Code_12355)){
            _29943 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _29943 = 1;
    }
    _29944 = _29943 + 1;
    _29943 = NOVALUE;
    _37backpatch(_prev_false2_58327, _29944);
    _29944 = NOVALUE;
L16: 
L14: 

    /** 	tok_match(END)*/
    _30tok_match(402, 0);

    /** 	tok_match(IF, END)*/
    _30tok_match(20, 402);

    /** 	End_block( IF )*/
    _66End_block(20);

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L17; // [804] 827
    }
    else{
    }

    /** 		if length(break_list) > elist_base then*/
    if (IS_SEQUENCE(_30break_list_55189)){
            _29945 = SEQ_PTR(_30break_list_55189)->length;
    }
    else {
        _29945 = 1;
    }
    if (_29945 <= _elist_base_58328)
    goto L18; // [814] 826

    /** 			emit_op(NOP1)  -- to emit label here*/
    _37emit_op(159);
L18: 
L17: 

    /** 	PatchEList(elist_base)*/
    _30PatchEList(_elist_base_58328);

    /** 	if_labels = if_labels[1..$-1]*/
    if (IS_SEQUENCE(_30if_labels_55200)){
            _29947 = SEQ_PTR(_30if_labels_55200)->length;
    }
    else {
        _29947 = 1;
    }
    _29948 = _29947 - 1;
    _29947 = NOVALUE;
    rhs_slice_target = (object_ptr)&_30if_labels_55200;
    RHS_Slice(_30if_labels_55200, 1, _29948);

    /** 	block_index -= 1*/
    _30block_index_55202 = _30block_index_55202 - 1;

    /** 	if_stack = if_stack[1..$-1]*/
    if (IS_SEQUENCE(_30if_stack_55206)){
            _29951 = SEQ_PTR(_30if_stack_55206)->length;
    }
    else {
        _29951 = 1;
    }
    _29952 = _29951 - 1;
    _29951 = NOVALUE;
    rhs_slice_target = (object_ptr)&_30if_stack_55206;
    RHS_Slice(_30if_stack_55206, 1, _29952);

    /** end procedure*/
    DeRef(_tok_58325);
    DeRef(_temps_58336);
    _29928 = NOVALUE;
    DeRef(_29926);
    _29926 = NOVALUE;
    _29948 = NOVALUE;
    _29952 = NOVALUE;
    return;
    ;
}


void _30exit_loop(int _exit_base_58513)
{
    int _29967 = NOVALUE;
    int _29966 = NOVALUE;
    int _29964 = NOVALUE;
    int _29963 = NOVALUE;
    int _29961 = NOVALUE;
    int _29960 = NOVALUE;
    int _29958 = NOVALUE;
    int _29957 = NOVALUE;
    int _29955 = NOVALUE;
    int _29954 = NOVALUE;
    int _0, _1, _2;
    

    /** 	PatchXList(exit_base)*/
    _30PatchXList(_exit_base_58513);

    /** 	loop_labels = loop_labels[1..$-1]*/
    if (IS_SEQUENCE(_30loop_labels_55199)){
            _29954 = SEQ_PTR(_30loop_labels_55199)->length;
    }
    else {
        _29954 = 1;
    }
    _29955 = _29954 - 1;
    _29954 = NOVALUE;
    rhs_slice_target = (object_ptr)&_30loop_labels_55199;
    RHS_Slice(_30loop_labels_55199, 1, _29955);

    /** 	loop_stack = loop_stack[1..$-1]*/
    if (IS_SEQUENCE(_30loop_stack_55205)){
            _29957 = SEQ_PTR(_30loop_stack_55205)->length;
    }
    else {
        _29957 = 1;
    }
    _29958 = _29957 - 1;
    _29957 = NOVALUE;
    rhs_slice_target = (object_ptr)&_30loop_stack_55205;
    RHS_Slice(_30loop_stack_55205, 1, _29958);

    /** 	continue_addr = continue_addr[1..$-1]*/
    if (IS_SEQUENCE(_30continue_addr_55196)){
            _29960 = SEQ_PTR(_30continue_addr_55196)->length;
    }
    else {
        _29960 = 1;
    }
    _29961 = _29960 - 1;
    _29960 = NOVALUE;
    rhs_slice_target = (object_ptr)&_30continue_addr_55196;
    RHS_Slice(_30continue_addr_55196, 1, _29961);

    /** 	retry_addr = retry_addr[1..$-1]*/
    if (IS_SEQUENCE(_30retry_addr_55197)){
            _29963 = SEQ_PTR(_30retry_addr_55197)->length;
    }
    else {
        _29963 = 1;
    }
    _29964 = _29963 - 1;
    _29963 = NOVALUE;
    rhs_slice_target = (object_ptr)&_30retry_addr_55197;
    RHS_Slice(_30retry_addr_55197, 1, _29964);

    /** 	entry_addr = entry_addr[1..$-1]*/
    if (IS_SEQUENCE(_30entry_addr_55195)){
            _29966 = SEQ_PTR(_30entry_addr_55195)->length;
    }
    else {
        _29966 = 1;
    }
    _29967 = _29966 - 1;
    _29966 = NOVALUE;
    rhs_slice_target = (object_ptr)&_30entry_addr_55195;
    RHS_Slice(_30entry_addr_55195, 1, _29967);

    /** 	block_index -= 1*/
    _30block_index_55202 = _30block_index_55202 - 1;

    /** end procedure*/
    _29955 = NOVALUE;
    _29958 = NOVALUE;
    _29961 = NOVALUE;
    _29964 = NOVALUE;
    _29967 = NOVALUE;
    return;
    ;
}


void _30push_switch()
{
    int _29971 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if_stack &= SWITCH*/
    Append(&_30if_stack_55206, _30if_stack_55206, 185);

    /** 	switch_stack = append( switch_stack, { {}, {}, 0, 0, 0, 0 })*/
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    RefDSn(_22682, 2);
    *((int *)(_2+4)) = _22682;
    *((int *)(_2+8)) = _22682;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 0;
    *((int *)(_2+20)) = 0;
    *((int *)(_2+24)) = 0;
    _29971 = MAKE_SEQ(_1);
    RefDS(_29971);
    Append(&_30switch_stack_55405, _30switch_stack_55405, _29971);
    DeRefDS(_29971);
    _29971 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


void _30pop_switch(int _break_base_58538)
{
    int _29986 = NOVALUE;
    int _29985 = NOVALUE;
    int _29983 = NOVALUE;
    int _29982 = NOVALUE;
    int _29980 = NOVALUE;
    int _29979 = NOVALUE;
    int _29977 = NOVALUE;
    int _29976 = NOVALUE;
    int _29975 = NOVALUE;
    int _29974 = NOVALUE;
    int _0, _1, _2;
    

    /** 	PatchEList( break_base )*/
    _30PatchEList(_break_base_58538);

    /** 	block_index -= 1*/
    _30block_index_55202 = _30block_index_55202 - 1;

    /** 	if length(switch_stack[$][SWITCH_CASES]) > 0 then*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _29974 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _29974 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    _29975 = (int)*(((s1_ptr)_2)->base + _29974);
    _2 = (int)SEQ_PTR(_29975);
    _29976 = (int)*(((s1_ptr)_2)->base + 1);
    _29975 = NOVALUE;
    if (IS_SEQUENCE(_29976)){
            _29977 = SEQ_PTR(_29976)->length;
    }
    else {
        _29977 = 1;
    }
    _29976 = NOVALUE;
    if (_29977 <= 0)
    goto L1; // [36] 48

    /** 		End_block( CASE )*/
    _66End_block(186);
L1: 

    /** 	if_labels = if_labels[1..$-1]*/
    if (IS_SEQUENCE(_30if_labels_55200)){
            _29979 = SEQ_PTR(_30if_labels_55200)->length;
    }
    else {
        _29979 = 1;
    }
    _29980 = _29979 - 1;
    _29979 = NOVALUE;
    rhs_slice_target = (object_ptr)&_30if_labels_55200;
    RHS_Slice(_30if_labels_55200, 1, _29980);

    /** 	if_stack  = if_stack[1..$-1]*/
    if (IS_SEQUENCE(_30if_stack_55206)){
            _29982 = SEQ_PTR(_30if_stack_55206)->length;
    }
    else {
        _29982 = 1;
    }
    _29983 = _29982 - 1;
    _29982 = NOVALUE;
    rhs_slice_target = (object_ptr)&_30if_stack_55206;
    RHS_Slice(_30if_stack_55206, 1, _29983);

    /** 	switch_stack  = switch_stack[1..$-1]*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _29985 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _29985 = 1;
    }
    _29986 = _29985 - 1;
    _29985 = NOVALUE;
    rhs_slice_target = (object_ptr)&_30switch_stack_55405;
    RHS_Slice(_30switch_stack_55405, 1, _29986);

    /** end procedure*/
    _29976 = NOVALUE;
    _29980 = NOVALUE;
    _29983 = NOVALUE;
    _29986 = NOVALUE;
    return;
    ;
}


void _30add_case(int _sym_58559, int _sign_58560)
{
    int _30012 = NOVALUE;
    int _30011 = NOVALUE;
    int _30010 = NOVALUE;
    int _30009 = NOVALUE;
    int _30008 = NOVALUE;
    int _30007 = NOVALUE;
    int _30006 = NOVALUE;
    int _30005 = NOVALUE;
    int _30003 = NOVALUE;
    int _30002 = NOVALUE;
    int _30001 = NOVALUE;
    int _30000 = NOVALUE;
    int _29999 = NOVALUE;
    int _29998 = NOVALUE;
    int _29996 = NOVALUE;
    int _29995 = NOVALUE;
    int _29993 = NOVALUE;
    int _29992 = NOVALUE;
    int _29991 = NOVALUE;
    int _29990 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	if sign < 0 then*/
    if (_sign_58560 >= 0)
    goto L1; // [5] 15

    /** 		sym = -sym*/
    _0 = _sym_58559;
    if (IS_ATOM_INT(_sym_58559)) {
        if ((unsigned long)_sym_58559 == 0xC0000000)
        _sym_58559 = (int)NewDouble((double)-0xC0000000);
        else
        _sym_58559 = - _sym_58559;
    }
    else {
        _sym_58559 = unary_op(UMINUS, _sym_58559);
    }
    DeRefi(_0);
L1: 

    /** 	if find(sym, switch_stack[$][SWITCH_CASES] ) = 0 then*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _29990 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _29990 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    _29991 = (int)*(((s1_ptr)_2)->base + _29990);
    _2 = (int)SEQ_PTR(_29991);
    _29992 = (int)*(((s1_ptr)_2)->base + 1);
    _29991 = NOVALUE;
    _29993 = find_from(_sym_58559, _29992, 1);
    _29992 = NOVALUE;
    if (_29993 != 0)
    goto L2; // [37] 154

    /** 		switch_stack[$][SWITCH_CASES]       = append( switch_stack[$][SWITCH_CASES], sym )*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _29995 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _29995 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30switch_stack_55405 = MAKE_SEQ(_2);
    }
    _3 = (int)(_29995 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _29998 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _29998 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    _29999 = (int)*(((s1_ptr)_2)->base + _29998);
    _2 = (int)SEQ_PTR(_29999);
    _30000 = (int)*(((s1_ptr)_2)->base + 1);
    _29999 = NOVALUE;
    Ref(_sym_58559);
    Append(&_30001, _30000, _sym_58559);
    _30000 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _30001;
    if( _1 != _30001 ){
        DeRef(_1);
    }
    _30001 = NOVALUE;
    _29996 = NOVALUE;

    /** 		switch_stack[$][SWITCH_JUMP_TABLE] &= length(Code) + 1*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _30002 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _30002 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30switch_stack_55405 = MAKE_SEQ(_2);
    }
    _3 = (int)(_30002 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_25Code_12355)){
            _30005 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _30005 = 1;
    }
    _30006 = _30005 + 1;
    _30005 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    _30007 = (int)*(((s1_ptr)_2)->base + 2);
    _30003 = NOVALUE;
    if (IS_SEQUENCE(_30007) && IS_ATOM(_30006)) {
        Append(&_30008, _30007, _30006);
    }
    else if (IS_ATOM(_30007) && IS_SEQUENCE(_30006)) {
    }
    else {
        Concat((object_ptr)&_30008, _30007, _30006);
        _30007 = NOVALUE;
    }
    _30007 = NOVALUE;
    _30006 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _30008;
    if( _1 != _30008 ){
        DeRef(_1);
    }
    _30008 = NOVALUE;
    _30003 = NOVALUE;

    /** 		if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L3; // [117] 162
    }
    else{
    }

    /** 			emit_addr( CASE )*/
    _37emit_addr(186);

    /** 			emit_addr( length( switch_stack[$][SWITCH_CASES] ) )*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _30009 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _30009 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    _30010 = (int)*(((s1_ptr)_2)->base + _30009);
    _2 = (int)SEQ_PTR(_30010);
    _30011 = (int)*(((s1_ptr)_2)->base + 1);
    _30010 = NOVALUE;
    if (IS_SEQUENCE(_30011)){
            _30012 = SEQ_PTR(_30011)->length;
    }
    else {
        _30012 = 1;
    }
    _30011 = NOVALUE;
    _37emit_addr(_30012);
    _30012 = NOVALUE;
    goto L3; // [151] 162
L2: 

    /** 		CompileErr( 63 )*/
    RefDS(_22682);
    _43CompileErr(63, _22682, 0);
L3: 

    /** end procedure*/
    DeRef(_sym_58559);
    _30011 = NOVALUE;
    return;
    ;
}


void _30case_else()
{
    int _30020 = NOVALUE;
    int _30019 = NOVALUE;
    int _30017 = NOVALUE;
    int _30016 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	switch_stack[$][SWITCH_ELSE] = length(Code) + 1*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _30016 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _30016 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30switch_stack_55405 = MAKE_SEQ(_2);
    }
    _3 = (int)(_30016 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_25Code_12355)){
            _30019 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _30019 = 1;
    }
    _30020 = _30019 + 1;
    _30019 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _30020;
    if( _1 != _30020 ){
        DeRef(_1);
    }
    _30020 = NOVALUE;
    _30017 = NOVALUE;

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L1; // [32] 48
    }
    else{
    }

    /** 		emit_addr( CASE )*/
    _37emit_addr(186);

    /** 		emit_addr( 0 )*/
    _37emit_addr(0);
L1: 

    /** end procedure*/
    return;
    ;
}


void _30Case_statement()
{
    int _else_case_2__tmp_at149_58656 = NOVALUE;
    int _else_case_1__tmp_at149_58655 = NOVALUE;
    int _else_case_inlined_else_case_at_149_58654 = NOVALUE;
    int _tok_58618 = NOVALUE;
    int _condition_58620 = NOVALUE;
    int _start_line_58649 = NOVALUE;
    int _sign_58660 = NOVALUE;
    int _fwd_58673 = NOVALUE;
    int _symi_58683 = NOVALUE;
    int _fwdref_58752 = NOVALUE;
    int _32366 = NOVALUE;
    int _30103 = NOVALUE;
    int _30102 = NOVALUE;
    int _30101 = NOVALUE;
    int _30100 = NOVALUE;
    int _30099 = NOVALUE;
    int _30098 = NOVALUE;
    int _30096 = NOVALUE;
    int _30095 = NOVALUE;
    int _30094 = NOVALUE;
    int _30092 = NOVALUE;
    int _30091 = NOVALUE;
    int _30090 = NOVALUE;
    int _30088 = NOVALUE;
    int _30085 = NOVALUE;
    int _30082 = NOVALUE;
    int _30081 = NOVALUE;
    int _30080 = NOVALUE;
    int _30079 = NOVALUE;
    int _30076 = NOVALUE;
    int _30075 = NOVALUE;
    int _30074 = NOVALUE;
    int _30073 = NOVALUE;
    int _30070 = NOVALUE;
    int _30069 = NOVALUE;
    int _30068 = NOVALUE;
    int _30067 = NOVALUE;
    int _30065 = NOVALUE;
    int _30064 = NOVALUE;
    int _30063 = NOVALUE;
    int _30061 = NOVALUE;
    int _30060 = NOVALUE;
    int _30059 = NOVALUE;
    int _30058 = NOVALUE;
    int _30057 = NOVALUE;
    int _30055 = NOVALUE;
    int _30054 = NOVALUE;
    int _30052 = NOVALUE;
    int _30051 = NOVALUE;
    int _30050 = NOVALUE;
    int _30049 = NOVALUE;
    int _30045 = NOVALUE;
    int _30044 = NOVALUE;
    int _30043 = NOVALUE;
    int _30040 = NOVALUE;
    int _30037 = NOVALUE;
    int _30034 = NOVALUE;
    int _30033 = NOVALUE;
    int _30032 = NOVALUE;
    int _30031 = NOVALUE;
    int _30030 = NOVALUE;
    int _30029 = NOVALUE;
    int _30028 = NOVALUE;
    int _30026 = NOVALUE;
    int _30025 = NOVALUE;
    int _30024 = NOVALUE;
    int _30023 = NOVALUE;
    int _30021 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	if not in_switch() then*/
    _30021 = _30in_switch();
    if (IS_ATOM_INT(_30021)) {
        if (_30021 != 0){
            DeRef(_30021);
            _30021 = NOVALUE;
            goto L1; // [6] 17
        }
    }
    else {
        if (DBL_PTR(_30021)->dbl != 0.0){
            DeRef(_30021);
            _30021 = NOVALUE;
            goto L1; // [6] 17
        }
    }
    DeRef(_30021);
    _30021 = NOVALUE;

    /** 		CompileErr( 34 )*/
    RefDS(_22682);
    _43CompileErr(34, _22682, 0);
L1: 

    /** 	if length(switch_stack[$][SWITCH_CASES]) > 0 then*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _30023 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _30023 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    _30024 = (int)*(((s1_ptr)_2)->base + _30023);
    _2 = (int)SEQ_PTR(_30024);
    _30025 = (int)*(((s1_ptr)_2)->base + 1);
    _30024 = NOVALUE;
    if (IS_SEQUENCE(_30025)){
            _30026 = SEQ_PTR(_30025)->length;
    }
    else {
        _30026 = 1;
    }
    _30025 = NOVALUE;
    if (_30026 <= 0)
    goto L2; // [37] 105

    /** 		Sibling_block( CASE )*/
    _66Sibling_block(186);

    /** 		if not switch_stack[$][SWITCH_FALLTHRU] and*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _30028 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _30028 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    _30029 = (int)*(((s1_ptr)_2)->base + _30028);
    _2 = (int)SEQ_PTR(_30029);
    _30030 = (int)*(((s1_ptr)_2)->base + 5);
    _30029 = NOVALUE;
    if (IS_ATOM_INT(_30030)) {
        _30031 = (_30030 == 0);
    }
    else {
        _30031 = unary_op(NOT, _30030);
    }
    _30030 = NOVALUE;
    if (IS_ATOM_INT(_30031)) {
        if (_30031 == 0) {
            goto L3; // [68] 114
        }
    }
    else {
        if (DBL_PTR(_30031)->dbl == 0.0) {
            goto L3; // [68] 114
        }
    }
    _30033 = (_30fallthru_case_58614 == 0);
    if (_30033 == 0)
    {
        DeRef(_30033);
        _30033 = NOVALUE;
        goto L3; // [78] 114
    }
    else{
        DeRef(_30033);
        _30033 = NOVALUE;
    }

    /** 			putback( {CASE, 0} )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 186;
    ((int *)_2)[2] = 0;
    _30034 = MAKE_SEQ(_1);
    _30putback(_30034);
    _30034 = NOVALUE;

    /** 			Break_statement()*/
    _30Break_statement();

    /** 			tok = next_token()*/
    _0 = _tok_58618;
    _tok_58618 = _30next_token();
    DeRef(_0);
    goto L3; // [102] 114
L2: 

    /** 		Start_block( CASE )*/
    _66Start_block(186, 0);
L3: 

    /** 	StartSourceLine(TRUE, , COVERAGE_SUPPRESS)*/
    _37StartSourceLine(_5TRUE_244, 0, 1);

    /** 	fallthru_case = 0*/
    _30fallthru_case_58614 = 0;

    /** 	integer start_line = line_number*/
    _start_line_58649 = _25line_number_12263;

    /** 	while 1 do*/
L4: 

    /** 		if else_case() then*/

    /** 	return switch_stack[$][SWITCH_ELSE]*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _else_case_1__tmp_at149_58655 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _else_case_1__tmp_at149_58655 = 1;
    }
    DeRef(_else_case_2__tmp_at149_58656);
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    _else_case_2__tmp_at149_58656 = (int)*(((s1_ptr)_2)->base + _else_case_1__tmp_at149_58655);
    RefDS(_else_case_2__tmp_at149_58656);
    DeRef(_else_case_inlined_else_case_at_149_58654);
    _2 = (int)SEQ_PTR(_else_case_2__tmp_at149_58656);
    _else_case_inlined_else_case_at_149_58654 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_else_case_inlined_else_case_at_149_58654);
    DeRef(_else_case_2__tmp_at149_58656);
    _else_case_2__tmp_at149_58656 = NOVALUE;
    if (_else_case_inlined_else_case_at_149_58654 == 0) {
        goto L5; // [166] 177
    }
    else {
        if (!IS_ATOM_INT(_else_case_inlined_else_case_at_149_58654) && DBL_PTR(_else_case_inlined_else_case_at_149_58654)->dbl == 0.0){
            goto L5; // [166] 177
        }
    }

    /** 			CompileErr( 33 )*/
    RefDS(_22682);
    _43CompileErr(33, _22682, 0);
L5: 

    /** 		maybe_namespace()*/
    _60maybe_namespace();

    /** 		tok = next_token()*/
    _0 = _tok_58618;
    _tok_58618 = _30next_token();
    DeRef(_0);

    /** 		integer sign = 1*/
    _sign_58660 = 1;

    /** 		if tok[T_ID] = MINUS then*/
    _2 = (int)SEQ_PTR(_tok_58618);
    _30037 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30037, 10)){
        _30037 = NOVALUE;
        goto L6; // [201] 218
    }
    _30037 = NOVALUE;

    /** 			sign = -1*/
    _sign_58660 = -1;

    /** 			tok = next_token()*/
    _0 = _tok_58618;
    _tok_58618 = _30next_token();
    DeRef(_0);
    goto L7; // [215] 239
L6: 

    /** 		elsif tok[T_ID] = PLUS then*/
    _2 = (int)SEQ_PTR(_tok_58618);
    _30040 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30040, 11)){
        _30040 = NOVALUE;
        goto L8; // [228] 238
    }
    _30040 = NOVALUE;

    /** 			tok = next_token()*/
    _0 = _tok_58618;
    _tok_58618 = _30next_token();
    DeRef(_0);
L8: 
L7: 

    /** 		integer fwd*/

    /** 		if not find( tok[T_ID], {ATOM, STRING, ELSE} ) then*/
    _2 = (int)SEQ_PTR(_tok_58618);
    _30043 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _28ATOM_11704;
    *((int *)(_2+8)) = 503;
    *((int *)(_2+12)) = 23;
    _30044 = MAKE_SEQ(_1);
    _30045 = find_from(_30043, _30044, 1);
    _30043 = NOVALUE;
    DeRefDS(_30044);
    _30044 = NOVALUE;
    if (_30045 != 0)
    goto L9; // [266] 441
    _30045 = NOVALUE;

    /** 			integer symi = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_58618);
    _symi_58683 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_symi_58683)){
        _symi_58683 = (long)DBL_PTR(_symi_58683)->dbl;
    }

    /** 			fwd = -1*/
    _fwd_58673 = -1;

    /** 			if symi > 0 then*/
    if (_symi_58683 <= 0)
    goto LA; // [286] 436

    /** 				if find(tok[T_ID] , VAR_TOKS) then*/
    _2 = (int)SEQ_PTR(_tok_58618);
    _30049 = (int)*(((s1_ptr)_2)->base + 1);
    _30050 = find_from(_30049, _28VAR_TOKS_11867, 1);
    _30049 = NOVALUE;
    if (_30050 == 0)
    {
        _30050 = NOVALUE;
        goto LB; // [305] 435
    }
    else{
        _30050 = NOVALUE;
    }

    /** 					if SymTab[symi][S_SCOPE] = SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30051 = (int)*(((s1_ptr)_2)->base + _symi_58683);
    _2 = (int)SEQ_PTR(_30051);
    _30052 = (int)*(((s1_ptr)_2)->base + 4);
    _30051 = NOVALUE;
    if (binary_op_a(NOTEQ, _30052, 9)){
        _30052 = NOVALUE;
        goto LC; // [324] 336
    }
    _30052 = NOVALUE;

    /** 						fwd = symi*/
    _fwd_58673 = _symi_58683;
    goto LD; // [333] 434
LC: 

    /** 					elsif SymTab[symi][S_MODE] = M_CONSTANT then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30054 = (int)*(((s1_ptr)_2)->base + _symi_58683);
    _2 = (int)SEQ_PTR(_30054);
    _30055 = (int)*(((s1_ptr)_2)->base + 3);
    _30054 = NOVALUE;
    if (binary_op_a(NOTEQ, _30055, 2)){
        _30055 = NOVALUE;
        goto LE; // [352] 433
    }
    _30055 = NOVALUE;

    /** 						fwd = 0*/
    _fwd_58673 = 0;

    /** 						if SymTab[symi][S_CODE] then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30057 = (int)*(((s1_ptr)_2)->base + _symi_58683);
    _2 = (int)SEQ_PTR(_30057);
    if (!IS_ATOM_INT(_25S_CODE_11925)){
        _30058 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    }
    else{
        _30058 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
    }
    _30057 = NOVALUE;
    if (_30058 == 0) {
        _30058 = NOVALUE;
        goto LF; // [375] 399
    }
    else {
        if (!IS_ATOM_INT(_30058) && DBL_PTR(_30058)->dbl == 0.0){
            _30058 = NOVALUE;
            goto LF; // [375] 399
        }
        _30058 = NOVALUE;
    }
    _30058 = NOVALUE;

    /** 							tok[T_SYM] = SymTab[symi][S_CODE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30059 = (int)*(((s1_ptr)_2)->base + _symi_58683);
    _2 = (int)SEQ_PTR(_30059);
    if (!IS_ATOM_INT(_25S_CODE_11925)){
        _30060 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    }
    else{
        _30060 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
    }
    _30059 = NOVALUE;
    Ref(_30060);
    _2 = (int)SEQ_PTR(_tok_58618);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _tok_58618 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _30060;
    if( _1 != _30060 ){
        DeRef(_1);
    }
    _30060 = NOVALUE;
LF: 

    /** 						SymTab[symi][S_USAGE] = or_bits( SymTab[symi][S_USAGE], U_READ )*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_symi_58683 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30063 = (int)*(((s1_ptr)_2)->base + _symi_58683);
    _2 = (int)SEQ_PTR(_30063);
    _30064 = (int)*(((s1_ptr)_2)->base + 5);
    _30063 = NOVALUE;
    if (IS_ATOM_INT(_30064)) {
        {unsigned long tu;
             tu = (unsigned long)_30064 | (unsigned long)1;
             _30065 = MAKE_UINT(tu);
        }
    }
    else {
        _30065 = binary_op(OR_BITS, _30064, 1);
    }
    _30064 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _30065;
    if( _1 != _30065 ){
        DeRef(_1);
    }
    _30065 = NOVALUE;
    _30061 = NOVALUE;
LE: 
LD: 
LB: 
LA: 
    goto L10; // [438] 447
L9: 

    /** 			fwd = 0*/
    _fwd_58673 = 0;
L10: 

    /** 		if fwd < 0 then*/
    if (_fwd_58673 >= 0)
    goto L11; // [451] 477

    /** 			CompileErr( 91, {find_category(tok[T_ID])})*/
    _2 = (int)SEQ_PTR(_tok_58618);
    _30067 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_30067);
    _30068 = _63find_category(_30067);
    _30067 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _30068;
    _30069 = MAKE_SEQ(_1);
    _30068 = NOVALUE;
    _43CompileErr(91, _30069, 0);
    _30069 = NOVALUE;
L11: 

    /** 		if tok[T_ID] = ELSE then*/
    _2 = (int)SEQ_PTR(_tok_58618);
    _30070 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30070, 23)){
        _30070 = NOVALUE;
        goto L12; // [487] 550
    }
    _30070 = NOVALUE;

    /** 			if sign = -1 then*/
    if (_sign_58660 != -1)
    goto L13; // [493] 505

    /** 				CompileErr( 71 )*/
    RefDS(_22682);
    _43CompileErr(71, _22682, 0);
L13: 

    /** 			if length(switch_stack[$][SWITCH_CASES]) = 0 then*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _30073 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _30073 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    _30074 = (int)*(((s1_ptr)_2)->base + _30073);
    _2 = (int)SEQ_PTR(_30074);
    _30075 = (int)*(((s1_ptr)_2)->base + 1);
    _30074 = NOVALUE;
    if (IS_SEQUENCE(_30075)){
            _30076 = SEQ_PTR(_30075)->length;
    }
    else {
        _30076 = 1;
    }
    _30075 = NOVALUE;
    if (_30076 != 0)
    goto L14; // [525] 537

    /** 				CompileErr( 44 )*/
    RefDS(_22682);
    _43CompileErr(44, _22682, 0);
L14: 

    /** 			case_else()*/
    _30case_else();

    /** 			exit*/
    goto L15; // [545] 789
    goto L16; // [547] 623
L12: 

    /** 		elsif fwd then*/
    if (_fwd_58673 == 0)
    {
        goto L17; // [552] 606
    }
    else{
    }

    /** 			integer fwdref = new_forward_reference( CASE, fwd )*/
    DeRef(_32366);
    _32366 = 186;
    _fwdref_58752 = _29new_forward_reference(186, _fwd_58673, 186);
    _32366 = NOVALUE;
    if (!IS_ATOM_INT(_fwdref_58752)) {
        _1 = (long)(DBL_PTR(_fwdref_58752)->dbl);
        if (UNIQUE(DBL_PTR(_fwdref_58752)) && (DBL_PTR(_fwdref_58752)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fwdref_58752);
        _fwdref_58752 = _1;
    }

    /** 			add_case( {fwdref}, sign )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _fwdref_58752;
    _30079 = MAKE_SEQ(_1);
    _30add_case(_30079, _sign_58660);
    _30079 = NOVALUE;

    /** 			fwd:set_data( fwdref, switch_stack[$][SWITCH_PC] )*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _30080 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _30080 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    _30081 = (int)*(((s1_ptr)_2)->base + _30080);
    _2 = (int)SEQ_PTR(_30081);
    _30082 = (int)*(((s1_ptr)_2)->base + 4);
    _30081 = NOVALUE;
    Ref(_30082);
    _29set_data(_fwdref_58752, _30082);
    _30082 = NOVALUE;
    goto L16; // [603] 623
L17: 

    /** 			condition = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_58618);
    _condition_58620 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_condition_58620)){
        _condition_58620 = (long)DBL_PTR(_condition_58620)->dbl;
    }

    /** 			add_case( condition, sign )*/
    _30add_case(_condition_58620, _sign_58660);
L16: 

    /** 		tok = next_token()*/
    _0 = _tok_58618;
    _tok_58618 = _30next_token();
    DeRef(_0);

    /** 		if tok[T_ID] = THEN then*/
    _2 = (int)SEQ_PTR(_tok_58618);
    _30085 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30085, 410)){
        _30085 = NOVALUE;
        goto L18; // [638] 744
    }
    _30085 = NOVALUE;

    /** 			tok = next_token()*/
    _0 = _tok_58618;
    _tok_58618 = _30next_token();
    DeRef(_0);

    /** 			if tok[T_ID] = CASE then*/
    _2 = (int)SEQ_PTR(_tok_58618);
    _30088 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30088, 186)){
        _30088 = NOVALUE;
        goto L19; // [657] 729
    }
    _30088 = NOVALUE;

    /** 				if switch_stack[$][SWITCH_FALLTHRU] then*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _30090 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _30090 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    _30091 = (int)*(((s1_ptr)_2)->base + _30090);
    _2 = (int)SEQ_PTR(_30091);
    _30092 = (int)*(((s1_ptr)_2)->base + 5);
    _30091 = NOVALUE;
    if (_30092 == 0) {
        _30092 = NOVALUE;
        goto L1A; // [678] 693
    }
    else {
        if (!IS_ATOM_INT(_30092) && DBL_PTR(_30092)->dbl == 0.0){
            _30092 = NOVALUE;
            goto L1A; // [678] 693
        }
        _30092 = NOVALUE;
    }
    _30092 = NOVALUE;

    /** 					start_line = line_number*/
    _start_line_58649 = _25line_number_12263;
    goto L1B; // [690] 782
L1A: 

    /** 					putback( tok )*/
    Ref(_tok_58618);
    _30putback(_tok_58618);

    /** 					Warning(220, empty_case_warning_flag,*/
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _30094 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    Ref(_30094);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _30094;
    ((int *)_2)[2] = _start_line_58649;
    _30095 = MAKE_SEQ(_1);
    _30094 = NOVALUE;
    _43Warning(220, 2048, _30095);
    _30095 = NOVALUE;

    /** 					exit*/
    goto L15; // [723] 789
    goto L1B; // [726] 782
L19: 

    /** 				putback( tok )*/
    Ref(_tok_58618);
    _30putback(_tok_58618);

    /** 				exit*/
    goto L15; // [738] 789
    goto L1B; // [741] 782
L18: 

    /** 		elsif tok[T_ID] != COMMA then*/
    _2 = (int)SEQ_PTR(_tok_58618);
    _30096 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _30096, -30)){
        _30096 = NOVALUE;
        goto L1C; // [754] 781
    }
    _30096 = NOVALUE;

    /** 			CompileErr(66,{LexName(tok[T_ID])})*/
    _2 = (int)SEQ_PTR(_tok_58618);
    _30098 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_30098);
    RefDS(_27188);
    _30099 = _37LexName(_30098, _27188);
    _30098 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _30099;
    _30100 = MAKE_SEQ(_1);
    _30099 = NOVALUE;
    _43CompileErr(66, _30100, 0);
    _30100 = NOVALUE;
L1C: 
L1B: 

    /** 	end while*/
    goto L4; // [786] 144
L15: 

    /** 	StartSourceLine( TRUE )*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 	emit_temp( switch_stack[$][SWITCH_VALUE], NEW_REFERENCE )*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _30101 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _30101 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    _30102 = (int)*(((s1_ptr)_2)->base + _30101);
    _2 = (int)SEQ_PTR(_30102);
    _30103 = (int)*(((s1_ptr)_2)->base + 6);
    _30102 = NOVALUE;
    Ref(_30103);
    _37emit_temp(_30103, 1);
    _30103 = NOVALUE;

    /** 	flush_temps()*/
    RefDS(_22682);
    _37flush_temps(_22682);

    /** end procedure*/
    DeRef(_tok_58618);
    _30025 = NOVALUE;
    DeRef(_30031);
    _30031 = NOVALUE;
    _30075 = NOVALUE;
    return;
    ;
}


void _30Fallthru_statement()
{
    int _30104 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not in_switch() then*/
    _30104 = _30in_switch();
    if (IS_ATOM_INT(_30104)) {
        if (_30104 != 0){
            DeRef(_30104);
            _30104 = NOVALUE;
            goto L1; // [6] 17
        }
    }
    else {
        if (DBL_PTR(_30104)->dbl != 0.0){
            DeRef(_30104);
            _30104 = NOVALUE;
            goto L1; // [6] 17
        }
    }
    DeRef(_30104);
    _30104 = NOVALUE;

    /** 		CompileErr( 22 )*/
    RefDS(_22682);
    _43CompileErr(22, _22682, 0);
L1: 

    /** 	tok_match( CASE )*/
    _30tok_match(186, 0);

    /** 	fallthru_case = 1*/
    _30fallthru_case_58614 = 1;

    /** 	Case_statement()*/
    _30Case_statement();

    /** end procedure*/
    return;
    ;
}


void _30update_translator_info(int _sym_58819, int _all_ints_58820, int _has_integer_58821, int _has_atom_58822, int _has_sequence_58823)
{
    int _30129 = NOVALUE;
    int _30127 = NOVALUE;
    int _30125 = NOVALUE;
    int _30123 = NOVALUE;
    int _30121 = NOVALUE;
    int _30120 = NOVALUE;
    int _30118 = NOVALUE;
    int _30116 = NOVALUE;
    int _30115 = NOVALUE;
    int _30114 = NOVALUE;
    int _30113 = NOVALUE;
    int _30112 = NOVALUE;
    int _30110 = NOVALUE;
    int _30108 = NOVALUE;
    int _30106 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	SymTab[sym][S_MODE] = M_TEMP    -- override CONSTANT for compile*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_58819 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 3;
    DeRef(_1);
    _30106 = NOVALUE;

    /** 	SymTab[sym][S_GTYPE] = TYPE_SEQUENCE*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_58819 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = 8;
    DeRef(_1);
    _30108 = NOVALUE;

    /** 	SymTab[sym][S_SEQ_LEN] = length( SymTab[sym][S_OBJ] )*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_58819 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30112 = (int)*(((s1_ptr)_2)->base + _sym_58819);
    _2 = (int)SEQ_PTR(_30112);
    _30113 = (int)*(((s1_ptr)_2)->base + 1);
    _30112 = NOVALUE;
    if (IS_SEQUENCE(_30113)){
            _30114 = SEQ_PTR(_30113)->length;
    }
    else {
        _30114 = 1;
    }
    _30113 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _30114;
    if( _1 != _30114 ){
        DeRef(_1);
    }
    _30114 = NOVALUE;
    _30110 = NOVALUE;

    /** 	if SymTab[sym][S_SEQ_LEN] > 0 then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30115 = (int)*(((s1_ptr)_2)->base + _sym_58819);
    _2 = (int)SEQ_PTR(_30115);
    _30116 = (int)*(((s1_ptr)_2)->base + 32);
    _30115 = NOVALUE;
    if (binary_op_a(LESSEQ, _30116, 0)){
        _30116 = NOVALUE;
        goto L1; // [89] 198
    }
    _30116 = NOVALUE;

    /** 		if all_ints then*/
    if (_all_ints_58820 == 0)
    {
        goto L2; // [95] 118
    }
    else{
    }

    /** 			SymTab[sym][S_SEQ_ELEM] = TYPE_INTEGER*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_58819 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _30118 = NOVALUE;
    goto L3; // [115] 216
L2: 

    /** 		elsif has_atom + has_sequence + has_integer > 1 then*/
    _30120 = _has_atom_58822 + _has_sequence_58823;
    if ((long)((unsigned long)_30120 + (unsigned long)HIGH_BITS) >= 0) 
    _30120 = NewDouble((double)_30120);
    if (IS_ATOM_INT(_30120)) {
        _30121 = _30120 + _has_integer_58821;
        if ((long)((unsigned long)_30121 + (unsigned long)HIGH_BITS) >= 0) 
        _30121 = NewDouble((double)_30121);
    }
    else {
        _30121 = NewDouble(DBL_PTR(_30120)->dbl + (double)_has_integer_58821);
    }
    DeRef(_30120);
    _30120 = NOVALUE;
    if (binary_op_a(LESSEQ, _30121, 1)){
        DeRef(_30121);
        _30121 = NOVALUE;
        goto L4; // [128] 152
    }
    DeRef(_30121);
    _30121 = NOVALUE;

    /** 			SymTab[sym][S_SEQ_ELEM] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_58819 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);
    _30123 = NOVALUE;
    goto L3; // [149] 216
L4: 

    /** 		elsif has_atom then*/
    if (_has_atom_58822 == 0)
    {
        goto L5; // [154] 177
    }
    else{
    }

    /** 			SymTab[sym][S_SEQ_ELEM] = TYPE_ATOM*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_58819 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 4;
    DeRef(_1);
    _30125 = NOVALUE;
    goto L3; // [174] 216
L5: 

    /** 			SymTab[sym][S_SEQ_ELEM] = TYPE_SEQUENCE*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_58819 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 8;
    DeRef(_1);
    _30127 = NOVALUE;
    goto L3; // [195] 216
L1: 

    /** 		SymTab[sym][S_SEQ_ELEM] = TYPE_NULL*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_58819 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _30129 = NOVALUE;
L3: 

    /** end procedure*/
    _30113 = NOVALUE;
    return;
    ;
}


void _30optimize_switch(int _switch_pc_58884, int _else_bp_58885, int _cases_58886, int _jump_table_58887)
{
    int _values_58888 = NOVALUE;
    int _min_58892 = NOVALUE;
    int _max_58894 = NOVALUE;
    int _all_ints_58896 = NOVALUE;
    int _has_integer_58897 = NOVALUE;
    int _has_atom_58898 = NOVALUE;
    int _has_sequence_58899 = NOVALUE;
    int _has_unassigned_58900 = NOVALUE;
    int _has_fwdref_58901 = NOVALUE;
    int _sym_58908 = NOVALUE;
    int _sign_58910 = NOVALUE;
    int _else_target_58974 = NOVALUE;
    int _opcode_58977 = NOVALUE;
    int _delta_58983 = NOVALUE;
    int _jump_58993 = NOVALUE;
    int _switch_table_58997 = NOVALUE;
    int _offset_59000 = NOVALUE;
    int _30207 = NOVALUE;
    int _30206 = NOVALUE;
    int _30205 = NOVALUE;
    int _30204 = NOVALUE;
    int _30202 = NOVALUE;
    int _30200 = NOVALUE;
    int _30197 = NOVALUE;
    int _30196 = NOVALUE;
    int _30195 = NOVALUE;
    int _30194 = NOVALUE;
    int _30193 = NOVALUE;
    int _30192 = NOVALUE;
    int _30191 = NOVALUE;
    int _30188 = NOVALUE;
    int _30186 = NOVALUE;
    int _30185 = NOVALUE;
    int _30184 = NOVALUE;
    int _30183 = NOVALUE;
    int _30182 = NOVALUE;
    int _30181 = NOVALUE;
    int _30180 = NOVALUE;
    int _30176 = NOVALUE;
    int _30175 = NOVALUE;
    int _30173 = NOVALUE;
    int _30172 = NOVALUE;
    int _30171 = NOVALUE;
    int _30170 = NOVALUE;
    int _30169 = NOVALUE;
    int _30168 = NOVALUE;
    int _30167 = NOVALUE;
    int _30166 = NOVALUE;
    int _30165 = NOVALUE;
    int _30164 = NOVALUE;
    int _30162 = NOVALUE;
    int _30161 = NOVALUE;
    int _30157 = NOVALUE;
    int _30154 = NOVALUE;
    int _30153 = NOVALUE;
    int _30152 = NOVALUE;
    int _30150 = NOVALUE;
    int _30149 = NOVALUE;
    int _30148 = NOVALUE;
    int _30147 = NOVALUE;
    int _30146 = NOVALUE;
    int _30144 = NOVALUE;
    int _30143 = NOVALUE;
    int _30142 = NOVALUE;
    int _30138 = NOVALUE;
    int _30137 = NOVALUE;
    int _30136 = NOVALUE;
    int _30132 = NOVALUE;
    int _30131 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	sequence values = switch_stack[$][SWITCH_CASES]*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _30131 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _30131 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    _30132 = (int)*(((s1_ptr)_2)->base + _30131);
    DeRef(_values_58888);
    _2 = (int)SEQ_PTR(_30132);
    _values_58888 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_values_58888);
    _30132 = NOVALUE;

    /** 	atom min =  1e+300*/
    RefDS(_30134);
    DeRef(_min_58892);
    _min_58892 = _30134;

    /** 	atom max = -1e+300*/
    RefDS(_30135);
    DeRef(_max_58894);
    _max_58894 = _30135;

    /** 	integer all_ints = 1*/
    _all_ints_58896 = 1;

    /** 	integer has_integer    = 0*/
    _has_integer_58897 = 0;

    /** 	integer has_atom       = 0*/
    _has_atom_58898 = 0;

    /** 	integer has_sequence   = 0*/
    _has_sequence_58899 = 0;

    /** 	integer has_unassigned = 0*/
    _has_unassigned_58900 = 0;

    /** 	integer has_fwdref     = 0*/
    _has_fwdref_58901 = 0;

    /** 	for i = 1 to length( values ) do*/
    if (IS_SEQUENCE(_values_58888)){
            _30136 = SEQ_PTR(_values_58888)->length;
    }
    else {
        _30136 = 1;
    }
    {
        int _i_58903;
        _i_58903 = 1;
L1: 
        if (_i_58903 > _30136){
            goto L2; // [73] 294
        }

        /** 		if sequence( values[i] ) then*/
        _2 = (int)SEQ_PTR(_values_58888);
        _30137 = (int)*(((s1_ptr)_2)->base + _i_58903);
        _30138 = IS_SEQUENCE(_30137);
        _30137 = NOVALUE;
        if (_30138 == 0)
        {
            _30138 = NOVALUE;
            goto L3; // [89] 102
        }
        else{
            _30138 = NOVALUE;
        }

        /** 			has_fwdref = 1*/
        _has_fwdref_58901 = 1;

        /** 			exit*/
        goto L2; // [99] 294
L3: 

        /** 		integer sym = values[i]*/
        _2 = (int)SEQ_PTR(_values_58888);
        _sym_58908 = (int)*(((s1_ptr)_2)->base + _i_58903);
        if (!IS_ATOM_INT(_sym_58908))
        _sym_58908 = (long)DBL_PTR(_sym_58908)->dbl;

        /** 		integer sign*/

        /** 		if sym < 0 then*/
        if (_sym_58908 >= 0)
        goto L4; // [112] 131

        /** 			sign = -1*/
        _sign_58910 = -1;

        /** 			sym = -sym*/
        _sym_58908 = - _sym_58908;
        goto L5; // [128] 137
L4: 

        /** 			sign = 1*/
        _sign_58910 = 1;
L5: 

        /** 		if not equal(SymTab[sym][S_OBJ], NOVALUE) then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _30142 = (int)*(((s1_ptr)_2)->base + _sym_58908);
        _2 = (int)SEQ_PTR(_30142);
        _30143 = (int)*(((s1_ptr)_2)->base + 1);
        _30142 = NOVALUE;
        if (_30143 == _25NOVALUE_12115)
        _30144 = 1;
        else if (IS_ATOM_INT(_30143) && IS_ATOM_INT(_25NOVALUE_12115))
        _30144 = 0;
        else
        _30144 = (compare(_30143, _25NOVALUE_12115) == 0);
        _30143 = NOVALUE;
        if (_30144 != 0)
        goto L6; // [157] 273
        _30144 = NOVALUE;

        /** 			values[i] = sign * SymTab[sym][S_OBJ]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _30146 = (int)*(((s1_ptr)_2)->base + _sym_58908);
        _2 = (int)SEQ_PTR(_30146);
        _30147 = (int)*(((s1_ptr)_2)->base + 1);
        _30146 = NOVALUE;
        if (IS_ATOM_INT(_30147)) {
            if (_sign_58910 == (short)_sign_58910 && _30147 <= INT15 && _30147 >= -INT15)
            _30148 = _sign_58910 * _30147;
            else
            _30148 = NewDouble(_sign_58910 * (double)_30147);
        }
        else {
            _30148 = binary_op(MULTIPLY, _sign_58910, _30147);
        }
        _30147 = NOVALUE;
        _2 = (int)SEQ_PTR(_values_58888);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values_58888 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_58903);
        _1 = *(int *)_2;
        *(int *)_2 = _30148;
        if( _1 != _30148 ){
            DeRef(_1);
        }
        _30148 = NOVALUE;

        /** 			if not integer( values[i] ) then*/
        _2 = (int)SEQ_PTR(_values_58888);
        _30149 = (int)*(((s1_ptr)_2)->base + _i_58903);
        if (IS_ATOM_INT(_30149))
        _30150 = 1;
        else if (IS_ATOM_DBL(_30149))
        _30150 = IS_ATOM_INT(DoubleToInt(_30149));
        else
        _30150 = 0;
        _30149 = NOVALUE;
        if (_30150 != 0)
        goto L7; // [193] 230
        _30150 = NOVALUE;

        /** 				all_ints = 0*/
        _all_ints_58896 = 0;

        /** 				if atom( values[i] ) then*/
        _2 = (int)SEQ_PTR(_values_58888);
        _30152 = (int)*(((s1_ptr)_2)->base + _i_58903);
        _30153 = IS_ATOM(_30152);
        _30152 = NOVALUE;
        if (_30153 == 0)
        {
            _30153 = NOVALUE;
            goto L8; // [210] 221
        }
        else{
            _30153 = NOVALUE;
        }

        /** 					has_atom = 1*/
        _has_atom_58898 = 1;
        goto L9; // [218] 285
L8: 

        /** 					has_sequence = 1*/
        _has_sequence_58899 = 1;
        goto L9; // [227] 285
L7: 

        /** 				has_integer = 1*/
        _has_integer_58897 = 1;

        /** 				if values[i] < min then*/
        _2 = (int)SEQ_PTR(_values_58888);
        _30154 = (int)*(((s1_ptr)_2)->base + _i_58903);
        if (binary_op_a(GREATEREQ, _30154, _min_58892)){
            _30154 = NOVALUE;
            goto LA; // [241] 252
        }
        _30154 = NOVALUE;

        /** 					min = values[i]*/
        DeRef(_min_58892);
        _2 = (int)SEQ_PTR(_values_58888);
        _min_58892 = (int)*(((s1_ptr)_2)->base + _i_58903);
        Ref(_min_58892);
LA: 

        /** 				if values[i] > max then*/
        _2 = (int)SEQ_PTR(_values_58888);
        _30157 = (int)*(((s1_ptr)_2)->base + _i_58903);
        if (binary_op_a(LESSEQ, _30157, _max_58894)){
            _30157 = NOVALUE;
            goto L9; // [258] 285
        }
        _30157 = NOVALUE;

        /** 					max = values[i]*/
        DeRef(_max_58894);
        _2 = (int)SEQ_PTR(_values_58888);
        _max_58894 = (int)*(((s1_ptr)_2)->base + _i_58903);
        Ref(_max_58894);
        goto L9; // [270] 285
L6: 

        /** 			has_unassigned = 1*/
        _has_unassigned_58900 = 1;

        /** 			exit*/
        goto L2; // [282] 294
L9: 

        /** 	end for*/
        _i_58903 = _i_58903 + 1;
        goto L1; // [289] 80
L2: 
        ;
    }

    /** 	if has_unassigned or has_fwdref then*/
    if (_has_unassigned_58900 != 0) {
        goto LB; // [296] 305
    }
    if (_has_fwdref_58901 == 0)
    {
        goto LC; // [301] 325
    }
    else{
    }
LB: 

    /** 		values = switch_stack[$][SWITCH_CASES]*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _30161 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _30161 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    _30162 = (int)*(((s1_ptr)_2)->base + _30161);
    DeRef(_values_58888);
    _2 = (int)SEQ_PTR(_30162);
    _values_58888 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_values_58888);
    _30162 = NOVALUE;
LC: 

    /** 	if switch_stack[$][SWITCH_ELSE] then*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _30164 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _30164 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    _30165 = (int)*(((s1_ptr)_2)->base + _30164);
    _2 = (int)SEQ_PTR(_30165);
    _30166 = (int)*(((s1_ptr)_2)->base + 3);
    _30165 = NOVALUE;
    if (_30166 == 0) {
        _30166 = NOVALUE;
        goto LD; // [342] 371
    }
    else {
        if (!IS_ATOM_INT(_30166) && DBL_PTR(_30166)->dbl == 0.0){
            _30166 = NOVALUE;
            goto LD; // [342] 371
        }
        _30166 = NOVALUE;
    }
    _30166 = NOVALUE;

    /** 			Code[else_bp] = switch_stack[$][SWITCH_ELSE]*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _30167 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _30167 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    _30168 = (int)*(((s1_ptr)_2)->base + _30167);
    _2 = (int)SEQ_PTR(_30168);
    _30169 = (int)*(((s1_ptr)_2)->base + 3);
    _30168 = NOVALUE;
    Ref(_30169);
    _2 = (int)SEQ_PTR(_25Code_12355);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _25Code_12355 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _else_bp_58885);
    _1 = *(int *)_2;
    *(int *)_2 = _30169;
    if( _1 != _30169 ){
        DeRef(_1);
    }
    _30169 = NOVALUE;
    goto LE; // [368] 395
LD: 

    /** 		Code[else_bp] = length(Code) + 1 + TRANSLATE*/
    if (IS_SEQUENCE(_25Code_12355)){
            _30170 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _30170 = 1;
    }
    _30171 = _30170 + 1;
    _30170 = NOVALUE;
    _30172 = _30171 + _25TRANSLATE_11874;
    _30171 = NOVALUE;
    _2 = (int)SEQ_PTR(_25Code_12355);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _25Code_12355 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _else_bp_58885);
    _1 = *(int *)_2;
    *(int *)_2 = _30172;
    if( _1 != _30172 ){
        DeRef(_1);
    }
    _30172 = NOVALUE;
LE: 

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto LF; // [399] 426
    }
    else{
    }

    /** 		SymTab[cases][S_OBJ] &= 0*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_cases_58886 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _30175 = (int)*(((s1_ptr)_2)->base + 1);
    _30173 = NOVALUE;
    if (IS_SEQUENCE(_30175) && IS_ATOM(0)) {
        Append(&_30176, _30175, 0);
    }
    else if (IS_ATOM(_30175) && IS_SEQUENCE(0)) {
    }
    else {
        Concat((object_ptr)&_30176, _30175, 0);
        _30175 = NOVALUE;
    }
    _30175 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _30176;
    if( _1 != _30176 ){
        DeRef(_1);
    }
    _30176 = NOVALUE;
    _30173 = NOVALUE;
LF: 

    /** 	integer else_target = Code[else_bp]*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _else_target_58974 = (int)*(((s1_ptr)_2)->base + _else_bp_58885);
    if (!IS_ATOM_INT(_else_target_58974)){
        _else_target_58974 = (long)DBL_PTR(_else_target_58974)->dbl;
    }

    /** 	integer opcode = SWITCH*/
    _opcode_58977 = 185;

    /** 	if has_unassigned or has_fwdref then*/
    if (_has_unassigned_58900 != 0) {
        goto L10; // [447] 456
    }
    if (_has_fwdref_58901 == 0)
    {
        goto L11; // [452] 468
    }
    else{
    }
L10: 

    /** 		opcode = SWITCH_RT*/
    _opcode_58977 = 202;
    goto L12; // [465] 642
L11: 

    /** 	elsif all_ints then*/
    if (_all_ints_58896 == 0)
    {
        goto L13; // [470] 639
    }
    else{
    }

    /** 		atom delta = max - min*/
    DeRef(_delta_58983);
    if (IS_ATOM_INT(_max_58894) && IS_ATOM_INT(_min_58892)) {
        _delta_58983 = _max_58894 - _min_58892;
        if ((long)((unsigned long)_delta_58983 +(unsigned long) HIGH_BITS) >= 0){
            _delta_58983 = NewDouble((double)_delta_58983);
        }
    }
    else {
        if (IS_ATOM_INT(_max_58894)) {
            _delta_58983 = NewDouble((double)_max_58894 - DBL_PTR(_min_58892)->dbl);
        }
        else {
            if (IS_ATOM_INT(_min_58892)) {
                _delta_58983 = NewDouble(DBL_PTR(_max_58894)->dbl - (double)_min_58892);
            }
            else
            _delta_58983 = NewDouble(DBL_PTR(_max_58894)->dbl - DBL_PTR(_min_58892)->dbl);
        }
    }

    /** 		if not TRANSLATE and  delta < 1024 and delta >= 0 then*/
    _30180 = (_25TRANSLATE_11874 == 0);
    if (_30180 == 0) {
        _30181 = 0;
        goto L14; // [486] 498
    }
    if (IS_ATOM_INT(_delta_58983)) {
        _30182 = (_delta_58983 < 1024);
    }
    else {
        _30182 = (DBL_PTR(_delta_58983)->dbl < (double)1024);
    }
    _30181 = (_30182 != 0);
L14: 
    if (_30181 == 0) {
        goto L15; // [498] 628
    }
    if (IS_ATOM_INT(_delta_58983)) {
        _30184 = (_delta_58983 >= 0);
    }
    else {
        _30184 = (DBL_PTR(_delta_58983)->dbl >= (double)0);
    }
    if (_30184 == 0)
    {
        DeRef(_30184);
        _30184 = NOVALUE;
        goto L15; // [507] 628
    }
    else{
        DeRef(_30184);
        _30184 = NOVALUE;
    }

    /** 			opcode = SWITCH_SPI*/
    _opcode_58977 = 192;

    /** 			sequence jump = switch_stack[$][SWITCH_JUMP_TABLE]*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _30185 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _30185 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    _30186 = (int)*(((s1_ptr)_2)->base + _30185);
    DeRef(_jump_58993);
    _2 = (int)SEQ_PTR(_30186);
    _jump_58993 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_jump_58993);
    _30186 = NOVALUE;

    /** 			sequence switch_table = repeat( else_target, delta + 1 )*/
    if (IS_ATOM_INT(_delta_58983)) {
        _30188 = _delta_58983 + 1;
    }
    else
    _30188 = binary_op(PLUS, 1, _delta_58983);
    DeRef(_switch_table_58997);
    _switch_table_58997 = Repeat(_else_target_58974, _30188);
    DeRef(_30188);
    _30188 = NOVALUE;

    /** 			integer offset = min - 1*/
    if (IS_ATOM_INT(_min_58892)) {
        _offset_59000 = _min_58892 - 1;
    }
    else {
        _offset_59000 = NewDouble(DBL_PTR(_min_58892)->dbl - (double)1);
    }
    if (!IS_ATOM_INT(_offset_59000)) {
        _1 = (long)(DBL_PTR(_offset_59000)->dbl);
        if (UNIQUE(DBL_PTR(_offset_59000)) && (DBL_PTR(_offset_59000)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_offset_59000);
        _offset_59000 = _1;
    }

    /** 			for i = 1 to length( values ) do*/
    if (IS_SEQUENCE(_values_58888)){
            _30191 = SEQ_PTR(_values_58888)->length;
    }
    else {
        _30191 = 1;
    }
    {
        int _i_59003;
        _i_59003 = 1;
L16: 
        if (_i_59003 > _30191){
            goto L17; // [561] 593
        }

        /** 				switch_table[values[i] - offset] = jump[i]*/
        _2 = (int)SEQ_PTR(_values_58888);
        _30192 = (int)*(((s1_ptr)_2)->base + _i_59003);
        if (IS_ATOM_INT(_30192)) {
            _30193 = _30192 - _offset_59000;
            if ((long)((unsigned long)_30193 +(unsigned long) HIGH_BITS) >= 0){
                _30193 = NewDouble((double)_30193);
            }
        }
        else {
            _30193 = binary_op(MINUS, _30192, _offset_59000);
        }
        _30192 = NOVALUE;
        _2 = (int)SEQ_PTR(_jump_58993);
        _30194 = (int)*(((s1_ptr)_2)->base + _i_59003);
        Ref(_30194);
        _2 = (int)SEQ_PTR(_switch_table_58997);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _switch_table_58997 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_30193))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_30193)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _30193);
        _1 = *(int *)_2;
        *(int *)_2 = _30194;
        if( _1 != _30194 ){
            DeRef(_1);
        }
        _30194 = NOVALUE;

        /** 			end for*/
        _i_59003 = _i_59003 + 1;
        goto L16; // [588] 568
L17: 
        ;
    }

    /** 			Code[switch_pc + 2] = offset*/
    _30195 = _switch_pc_58884 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _25Code_12355 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _30195);
    _1 = *(int *)_2;
    *(int *)_2 = _offset_59000;
    DeRef(_1);

    /** 			switch_stack[$][SWITCH_JUMP_TABLE] = switch_table*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _30196 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _30196 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30switch_stack_55405 = MAKE_SEQ(_2);
    }
    _3 = (int)(_30196 + ((s1_ptr)_2)->base);
    RefDS(_switch_table_58997);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _switch_table_58997;
    DeRef(_1);
    _30197 = NOVALUE;
    DeRef(_jump_58993);
    _jump_58993 = NOVALUE;
    DeRefDS(_switch_table_58997);
    _switch_table_58997 = NOVALUE;
    goto L18; // [625] 638
L15: 

    /** 			opcode = SWITCH_I*/
    _opcode_58977 = 193;
L18: 
L13: 
    DeRef(_delta_58983);
    _delta_58983 = NOVALUE;
L12: 

    /** 	Code[switch_pc] = opcode*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _25Code_12355 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _switch_pc_58884);
    _1 = *(int *)_2;
    *(int *)_2 = _opcode_58977;
    DeRef(_1);

    /** 	if opcode != SWITCH_SPI then*/
    if (_opcode_58977 == 192)
    goto L19; // [654] 691

    /** 		SymTab[cases][S_OBJ] = values*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_cases_58886 + ((s1_ptr)_2)->base);
    RefDS(_values_58888);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _values_58888;
    DeRef(_1);
    _30200 = NOVALUE;

    /** 		if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L1A; // [677] 690
    }
    else{
    }

    /** 			update_translator_info( cases, all_ints, has_integer, has_atom, has_sequence )*/
    _30update_translator_info(_cases_58886, _all_ints_58896, _has_integer_58897, _has_atom_58898, _has_sequence_58899);
L1A: 
L19: 

    /** 	SymTab[jump_table][S_OBJ] = switch_stack[$][SWITCH_JUMP_TABLE] - switch_pc*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_jump_table_58887 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _30204 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _30204 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    _30205 = (int)*(((s1_ptr)_2)->base + _30204);
    _2 = (int)SEQ_PTR(_30205);
    _30206 = (int)*(((s1_ptr)_2)->base + 2);
    _30205 = NOVALUE;
    if (IS_ATOM_INT(_30206)) {
        _30207 = _30206 - _switch_pc_58884;
        if ((long)((unsigned long)_30207 +(unsigned long) HIGH_BITS) >= 0){
            _30207 = NewDouble((double)_30207);
        }
    }
    else {
        _30207 = binary_op(MINUS, _30206, _switch_pc_58884);
    }
    _30206 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _30207;
    if( _1 != _30207 ){
        DeRef(_1);
    }
    _30207 = NOVALUE;
    _30202 = NOVALUE;

    /** end procedure*/
    DeRef(_values_58888);
    DeRef(_min_58892);
    DeRef(_max_58894);
    DeRef(_30180);
    _30180 = NOVALUE;
    DeRef(_30182);
    _30182 = NOVALUE;
    DeRef(_30195);
    _30195 = NOVALUE;
    DeRef(_30193);
    _30193 = NOVALUE;
    return;
    ;
}


void _30Switch_statement()
{
    int _else_case_2__tmp_at256_59097 = NOVALUE;
    int _else_case_1__tmp_at256_59096 = NOVALUE;
    int _else_case_inlined_else_case_at_256_59095 = NOVALUE;
    int _break_base_59035 = NOVALUE;
    int _cases_59037 = NOVALUE;
    int _jump_table_59038 = NOVALUE;
    int _else_bp_59039 = NOVALUE;
    int _switch_pc_59040 = NOVALUE;
    int _t_59077 = NOVALUE;
    int _30238 = NOVALUE;
    int _30237 = NOVALUE;
    int _30236 = NOVALUE;
    int _30235 = NOVALUE;
    int _30234 = NOVALUE;
    int _30230 = NOVALUE;
    int _30226 = NOVALUE;
    int _30225 = NOVALUE;
    int _30223 = NOVALUE;
    int _30222 = NOVALUE;
    int _30220 = NOVALUE;
    int _30219 = NOVALUE;
    int _30217 = NOVALUE;
    int _30216 = NOVALUE;
    int _30215 = NOVALUE;
    int _30214 = NOVALUE;
    int _30213 = NOVALUE;
    int _30212 = NOVALUE;
    int _30210 = NOVALUE;
    int _30209 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	integer else_bp*/

    /** 	integer switch_pc*/

    /** 	push_switch()*/
    _30push_switch();

    /** 	break_base = length(break_list)*/
    if (IS_SEQUENCE(_30break_list_55189)){
            _break_base_59035 = SEQ_PTR(_30break_list_55189)->length;
    }
    else {
        _break_base_59035 = 1;
    }

    /** 	Expr()*/
    _30Expr();

    /** 	switch_stack[$][SWITCH_VALUE] = Top()*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _30209 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _30209 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30switch_stack_55405 = MAKE_SEQ(_2);
    }
    _3 = (int)(_30209 + ((s1_ptr)_2)->base);
    _30212 = _37Top();
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _30212;
    if( _1 != _30212 ){
        DeRef(_1);
    }
    _30212 = NOVALUE;
    _30210 = NOVALUE;

    /** 	clear_temp( switch_stack[$][SWITCH_VALUE] )*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _30213 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _30213 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    _30214 = (int)*(((s1_ptr)_2)->base + _30213);
    _2 = (int)SEQ_PTR(_30214);
    _30215 = (int)*(((s1_ptr)_2)->base + 6);
    _30214 = NOVALUE;
    Ref(_30215);
    _37clear_temp(_30215);
    _30215 = NOVALUE;

    /** 	cases = NewStringSym( {-1, length(SymTab) } )*/
    if (IS_SEQUENCE(_26SymTab_11138)){
            _30216 = SEQ_PTR(_26SymTab_11138)->length;
    }
    else {
        _30216 = 1;
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = _30216;
    _30217 = MAKE_SEQ(_1);
    _30216 = NOVALUE;
    _cases_59037 = _52NewStringSym(_30217);
    _30217 = NOVALUE;
    if (!IS_ATOM_INT(_cases_59037)) {
        _1 = (long)(DBL_PTR(_cases_59037)->dbl);
        if (UNIQUE(DBL_PTR(_cases_59037)) && (DBL_PTR(_cases_59037)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_cases_59037);
        _cases_59037 = _1;
    }

    /** 	emit_opnd( cases )*/
    _37emit_opnd(_cases_59037);

    /** 	jump_table = NewStringSym( {-2, length(SymTab) } )*/
    if (IS_SEQUENCE(_26SymTab_11138)){
            _30219 = SEQ_PTR(_26SymTab_11138)->length;
    }
    else {
        _30219 = 1;
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -2;
    ((int *)_2)[2] = _30219;
    _30220 = MAKE_SEQ(_1);
    _30219 = NOVALUE;
    _jump_table_59038 = _52NewStringSym(_30220);
    _30220 = NOVALUE;
    if (!IS_ATOM_INT(_jump_table_59038)) {
        _1 = (long)(DBL_PTR(_jump_table_59038)->dbl);
        if (UNIQUE(DBL_PTR(_jump_table_59038)) && (DBL_PTR(_jump_table_59038)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_jump_table_59038);
        _jump_table_59038 = _1;
    }

    /** 	emit_opnd( jump_table )*/
    _37emit_opnd(_jump_table_59038);

    /** 	if finish_block_header(SWITCH) then end if*/
    _30222 = _30finish_block_header(185);
    if (_30222 == 0) {
        DeRef(_30222);
        _30222 = NOVALUE;
        goto L1; // [113] 117
    }
    else {
        if (!IS_ATOM_INT(_30222) && DBL_PTR(_30222)->dbl == 0.0){
            DeRef(_30222);
            _30222 = NOVALUE;
            goto L1; // [113] 117
        }
        DeRef(_30222);
        _30222 = NOVALUE;
    }
    DeRef(_30222);
    _30222 = NOVALUE;
L1: 

    /** 	switch_pc = length(Code) + 1*/
    if (IS_SEQUENCE(_25Code_12355)){
            _30223 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _30223 = 1;
    }
    _switch_pc_59040 = _30223 + 1;
    _30223 = NOVALUE;

    /** 	switch_stack[$][SWITCH_PC] = switch_pc*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _30225 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _30225 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30switch_stack_55405 = MAKE_SEQ(_2);
    }
    _3 = (int)(_30225 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _switch_pc_59040;
    DeRef(_1);
    _30226 = NOVALUE;

    /** 	emit_op(SWITCH)*/
    _37emit_op(185);

    /** 	emit_forward_addr()  -- the else*/
    _30emit_forward_addr();

    /** 	else_bp = length( Code )*/
    if (IS_SEQUENCE(_25Code_12355)){
            _else_bp_59039 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _else_bp_59039 = 1;
    }

    /** 	t = next_token()*/
    _0 = _t_59077;
    _t_59077 = _30next_token();
    DeRef(_0);

    /** 	if t[T_ID] = CASE then*/
    _2 = (int)SEQ_PTR(_t_59077);
    _30230 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30230, 186)){
        _30230 = NOVALUE;
        goto L2; // [179] 194
    }
    _30230 = NOVALUE;

    /** 		Case_statement()*/
    _30Case_statement();

    /** 		Statement_list()*/
    _30Statement_list();
    goto L3; // [191] 200
L2: 

    /** 		putback(t)*/
    Ref(_t_59077);
    _30putback(_t_59077);
L3: 

    /** 	optimize_switch( switch_pc, else_bp, cases, jump_table )*/
    _30optimize_switch(_switch_pc_59040, _else_bp_59039, _cases_59037, _jump_table_59038);

    /** 	tok_match(END)*/
    _30tok_match(402, 0);

    /** 	tok_match(SWITCH, END)*/
    _30tok_match(185, 402);

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L4; // [230] 241
    }
    else{
    }

    /** 		emit_op(NOPSWITCH)*/
    _37emit_op(187);
L4: 

    /** 	if not else_case() then*/

    /** 	return switch_stack[$][SWITCH_ELSE]*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _else_case_1__tmp_at256_59096 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _else_case_1__tmp_at256_59096 = 1;
    }
    DeRef(_else_case_2__tmp_at256_59097);
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    _else_case_2__tmp_at256_59097 = (int)*(((s1_ptr)_2)->base + _else_case_1__tmp_at256_59096);
    RefDS(_else_case_2__tmp_at256_59097);
    DeRef(_else_case_inlined_else_case_at_256_59095);
    _2 = (int)SEQ_PTR(_else_case_2__tmp_at256_59097);
    _else_case_inlined_else_case_at_256_59095 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_else_case_inlined_else_case_at_256_59095);
    DeRef(_else_case_2__tmp_at256_59097);
    _else_case_2__tmp_at256_59097 = NOVALUE;
    if (IS_ATOM_INT(_else_case_inlined_else_case_at_256_59095)) {
        if (_else_case_inlined_else_case_at_256_59095 != 0){
            goto L5; // [263] 337
        }
    }
    else {
        if (DBL_PTR(_else_case_inlined_else_case_at_256_59095)->dbl != 0.0){
            goto L5; // [263] 337
        }
    }

    /** 		if not TRANSLATE then*/
    if (_25TRANSLATE_11874 != 0)
    goto L6; // [270] 313

    /** 			StartSourceLine( TRUE, , COVERAGE_SUPPRESS )*/
    _37StartSourceLine(_5TRUE_244, 0, 1);

    /** 			emit_temp( switch_stack[$][SWITCH_VALUE], NEW_REFERENCE )*/
    if (IS_SEQUENCE(_30switch_stack_55405)){
            _30234 = SEQ_PTR(_30switch_stack_55405)->length;
    }
    else {
        _30234 = 1;
    }
    _2 = (int)SEQ_PTR(_30switch_stack_55405);
    _30235 = (int)*(((s1_ptr)_2)->base + _30234);
    _2 = (int)SEQ_PTR(_30235);
    _30236 = (int)*(((s1_ptr)_2)->base + 6);
    _30235 = NOVALUE;
    Ref(_30236);
    _37emit_temp(_30236, 1);
    _30236 = NOVALUE;

    /** 			flush_temps()*/
    RefDS(_22682);
    _37flush_temps(_22682);
L6: 

    /** 		Warning(221, no_case_else_warning_flag,*/
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _30237 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    Ref(_30237);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _30237;
    ((int *)_2)[2] = _25line_number_12263;
    _30238 = MAKE_SEQ(_1);
    _30237 = NOVALUE;
    _43Warning(221, 4096, _30238);
    _30238 = NOVALUE;
L5: 

    /** 	pop_switch( break_base )*/
    _30pop_switch(_break_base_59035);

    /** end procedure*/
    DeRef(_t_59077);
    return;
    ;
}


int _30get_private_uninitialized()
{
    int _uninitialized_59120 = NOVALUE;
    int _s_59126 = NOVALUE;
    int _pu_59132 = NOVALUE;
    int _30255 = NOVALUE;
    int _30253 = NOVALUE;
    int _30252 = NOVALUE;
    int _30251 = NOVALUE;
    int _30250 = NOVALUE;
    int _30249 = NOVALUE;
    int _30248 = NOVALUE;
    int _30247 = NOVALUE;
    int _30246 = NOVALUE;
    int _30245 = NOVALUE;
    int _30244 = NOVALUE;
    int _30243 = NOVALUE;
    int _30240 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence uninitialized = {}*/
    RefDS(_22682);
    DeRefi(_uninitialized_59120);
    _uninitialized_59120 = _22682;

    /** 	if CurrentSub != TopLevelSub then*/
    if (_25CurrentSub_12270 == _25TopLevelSub_12269)
    goto L1; // [14] 149

    /** 		symtab_pointer s = SymTab[CurrentSub][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30240 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_30240);
    _s_59126 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_59126)){
        _s_59126 = (long)DBL_PTR(_s_59126)->dbl;
    }
    _30240 = NOVALUE;

    /** 		sequence pu = { SC_PRIVATE, SC_UNDEFINED }*/
    DeRefi(_pu_59132);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 3;
    ((int *)_2)[2] = 9;
    _pu_59132 = MAKE_SEQ(_1);

    /** 		while s and find( SymTab[s][S_SCOPE], pu ) do*/
L2: 
    if (_s_59126 == 0) {
        goto L3; // [51] 148
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30244 = (int)*(((s1_ptr)_2)->base + _s_59126);
    _2 = (int)SEQ_PTR(_30244);
    _30245 = (int)*(((s1_ptr)_2)->base + 4);
    _30244 = NOVALUE;
    _30246 = find_from(_30245, _pu_59132, 1);
    _30245 = NOVALUE;
    if (_30246 == 0)
    {
        _30246 = NOVALUE;
        goto L3; // [73] 148
    }
    else{
        _30246 = NOVALUE;
    }

    /** 			if SymTab[s][S_SCOPE] = SC_PRIVATE and SymTab[s][S_INITLEVEL] = -1 then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30247 = (int)*(((s1_ptr)_2)->base + _s_59126);
    _2 = (int)SEQ_PTR(_30247);
    _30248 = (int)*(((s1_ptr)_2)->base + 4);
    _30247 = NOVALUE;
    if (IS_ATOM_INT(_30248)) {
        _30249 = (_30248 == 3);
    }
    else {
        _30249 = binary_op(EQUALS, _30248, 3);
    }
    _30248 = NOVALUE;
    if (IS_ATOM_INT(_30249)) {
        if (_30249 == 0) {
            goto L4; // [96] 127
        }
    }
    else {
        if (DBL_PTR(_30249)->dbl == 0.0) {
            goto L4; // [96] 127
        }
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30251 = (int)*(((s1_ptr)_2)->base + _s_59126);
    _2 = (int)SEQ_PTR(_30251);
    _30252 = (int)*(((s1_ptr)_2)->base + 14);
    _30251 = NOVALUE;
    if (IS_ATOM_INT(_30252)) {
        _30253 = (_30252 == -1);
    }
    else {
        _30253 = binary_op(EQUALS, _30252, -1);
    }
    _30252 = NOVALUE;
    if (_30253 == 0) {
        DeRef(_30253);
        _30253 = NOVALUE;
        goto L4; // [117] 127
    }
    else {
        if (!IS_ATOM_INT(_30253) && DBL_PTR(_30253)->dbl == 0.0){
            DeRef(_30253);
            _30253 = NOVALUE;
            goto L4; // [117] 127
        }
        DeRef(_30253);
        _30253 = NOVALUE;
    }
    DeRef(_30253);
    _30253 = NOVALUE;

    /** 				uninitialized &= s*/
    Append(&_uninitialized_59120, _uninitialized_59120, _s_59126);
L4: 

    /** 			s = SymTab[s][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30255 = (int)*(((s1_ptr)_2)->base + _s_59126);
    _2 = (int)SEQ_PTR(_30255);
    _s_59126 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_59126)){
        _s_59126 = (long)DBL_PTR(_s_59126)->dbl;
    }
    _30255 = NOVALUE;

    /** 		end while*/
    goto L2; // [145] 51
L3: 
L1: 
    DeRefi(_pu_59132);
    _pu_59132 = NOVALUE;

    /** 	return uninitialized*/
    DeRef(_30249);
    _30249 = NOVALUE;
    return _uninitialized_59120;
    ;
}


void _30While_statement()
{
    int _bp1_59163 = NOVALUE;
    int _bp2_59164 = NOVALUE;
    int _exit_base_59165 = NOVALUE;
    int _next_base_59166 = NOVALUE;
    int _uninitialized_59167 = NOVALUE;
    int _temps_59237 = NOVALUE;
    int _30291 = NOVALUE;
    int _30290 = NOVALUE;
    int _30289 = NOVALUE;
    int _30285 = NOVALUE;
    int _30284 = NOVALUE;
    int _30282 = NOVALUE;
    int _30280 = NOVALUE;
    int _30279 = NOVALUE;
    int _30278 = NOVALUE;
    int _30274 = NOVALUE;
    int _30272 = NOVALUE;
    int _30270 = NOVALUE;
    int _30264 = NOVALUE;
    int _30262 = NOVALUE;
    int _30261 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence uninitialized = get_private_uninitialized()*/
    _0 = _uninitialized_59167;
    _uninitialized_59167 = _30get_private_uninitialized();
    DeRef(_0);

    /** 	entry_stack = append( entry_stack, uninitialized )*/
    RefDS(_uninitialized_59167);
    Append(&_30entry_stack_55198, _30entry_stack_55198, _uninitialized_59167);

    /** 	Start_block( WHILE )*/
    _66Start_block(47, 0);

    /** 	exit_base = length(exit_list)*/
    if (IS_SEQUENCE(_30exit_list_55191)){
            _exit_base_59165 = SEQ_PTR(_30exit_list_55191)->length;
    }
    else {
        _exit_base_59165 = 1;
    }

    /** 	next_base = length(continue_list)*/
    if (IS_SEQUENCE(_30continue_list_55193)){
            _next_base_59166 = SEQ_PTR(_30continue_list_55193)->length;
    }
    else {
        _next_base_59166 = 1;
    }

    /** 	entry_addr &= length(Code)+1*/
    if (IS_SEQUENCE(_25Code_12355)){
            _30261 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _30261 = 1;
    }
    _30262 = _30261 + 1;
    _30261 = NOVALUE;
    Append(&_30entry_addr_55195, _30entry_addr_55195, _30262);
    _30262 = NOVALUE;

    /** 	emit_op(NOP2) -- Entry_statement may patch this later*/
    _37emit_op(110);

    /** 	emit_addr(0)*/
    _37emit_addr(0);

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L1; // [71] 82
    }
    else{
    }

    /** 		emit_op(NOPWHILE)*/
    _37emit_op(158);
L1: 

    /** 	bp1 = length(Code)+1*/
    if (IS_SEQUENCE(_25Code_12355)){
            _30264 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _30264 = 1;
    }
    _bp1_59163 = _30264 + 1;
    _30264 = NOVALUE;

    /** 	continue_addr &= bp1*/
    Append(&_30continue_addr_55196, _30continue_addr_55196, _bp1_59163);

    /** 	short_circuit += 1*/
    _30short_circuit_55171 = _30short_circuit_55171 + 1;

    /** 	short_circuit_B = FALSE*/
    _30short_circuit_B_55173 = _5FALSE_242;

    /** 	SC1_type = 0*/
    _30SC1_type_55176 = 0;

    /** 	Expr()*/
    _30Expr();

    /** 	optimized_while = FALSE*/
    _37optimized_while_51270 = _5FALSE_242;

    /** 	emit_op(WHILE)*/
    _37emit_op(47);

    /** 	short_circuit -= 1*/
    _30short_circuit_55171 = _30short_circuit_55171 - 1;

    /** 	if not optimized_while then*/
    if (_37optimized_while_51270 != 0)
    goto L2; // [153] 174

    /** 		bp2 = length(Code)+1*/
    if (IS_SEQUENCE(_25Code_12355)){
            _30270 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _30270 = 1;
    }
    _bp2_59164 = _30270 + 1;
    _30270 = NOVALUE;

    /** 		emit_forward_addr() -- will be patched*/
    _30emit_forward_addr();
    goto L3; // [171] 180
L2: 

    /** 		bp2 = 0*/
    _bp2_59164 = 0;
L3: 

    /** 	if finish_block_header(WHILE)=0 then*/
    _30272 = _30finish_block_header(47);
    if (binary_op_a(NOTEQ, _30272, 0)){
        DeRef(_30272);
        _30272 = NOVALUE;
        goto L4; // [188] 204
    }
    DeRef(_30272);
    _30272 = NOVALUE;

    /** 		entry_addr[$]=-1*/
    if (IS_SEQUENCE(_30entry_addr_55195)){
            _30274 = SEQ_PTR(_30entry_addr_55195)->length;
    }
    else {
        _30274 = 1;
    }
    _2 = (int)SEQ_PTR(_30entry_addr_55195);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30entry_addr_55195 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _30274);
    *(int *)_2 = -1;
L4: 

    /** 	loop_stack &= WHILE*/
    Append(&_30loop_stack_55205, _30loop_stack_55205, 47);

    /** 	exit_base = length(exit_list)*/
    if (IS_SEQUENCE(_30exit_list_55191)){
            _exit_base_59165 = SEQ_PTR(_30exit_list_55191)->length;
    }
    else {
        _exit_base_59165 = 1;
    }

    /** 	if SC1_type = OR then*/
    if (_30SC1_type_55176 != 9)
    goto L5; // [227] 280

    /** 		backpatch(SC1_patch-3, SC1_OR_IF)*/
    _30278 = _30SC1_patch_55175 - 3;
    if ((long)((unsigned long)_30278 +(unsigned long) HIGH_BITS) >= 0){
        _30278 = NewDouble((double)_30278);
    }
    _37backpatch(_30278, 147);
    _30278 = NOVALUE;

    /** 		if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L6; // [249] 260
    }
    else{
    }

    /** 			emit_op(NOP1)*/
    _37emit_op(159);
L6: 

    /** 		backpatch(SC1_patch, length(Code)+1)*/
    if (IS_SEQUENCE(_25Code_12355)){
            _30279 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _30279 = 1;
    }
    _30280 = _30279 + 1;
    _30279 = NOVALUE;
    _37backpatch(_30SC1_patch_55175, _30280);
    _30280 = NOVALUE;
    goto L7; // [277] 331
L5: 

    /** 	elsif SC1_type = AND then*/
    if (_30SC1_type_55176 != 8)
    goto L8; // [286] 330

    /** 		backpatch(SC1_patch-3, SC1_AND_IF)*/
    _30282 = _30SC1_patch_55175 - 3;
    if ((long)((unsigned long)_30282 +(unsigned long) HIGH_BITS) >= 0){
        _30282 = NewDouble((double)_30282);
    }
    _37backpatch(_30282, 146);
    _30282 = NOVALUE;

    /** 		AppendXList(SC1_patch)*/

    /** 	exit_list = append(exit_list, addr)*/
    Append(&_30exit_list_55191, _30exit_list_55191, _30SC1_patch_55175);

    /** end procedure*/
    goto L9; // [318] 321
L9: 

    /** 		exit_delay &= 1*/
    Append(&_30exit_delay_55192, _30exit_delay_55192, 1);
L8: 
L7: 

    /** 	retry_addr &= length(Code)+1*/
    if (IS_SEQUENCE(_25Code_12355)){
            _30284 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _30284 = 1;
    }
    _30285 = _30284 + 1;
    _30284 = NOVALUE;
    Append(&_30retry_addr_55197, _30retry_addr_55197, _30285);
    _30285 = NOVALUE;

    /** 	sequence temps = pop_temps()*/
    _0 = _temps_59237;
    _temps_59237 = _37pop_temps();
    DeRef(_0);

    /** 	push_temps( temps )*/
    RefDS(_temps_59237);
    _37push_temps(_temps_59237);

    /** 	Statement_list()*/
    _30Statement_list();

    /** 	PatchNList(next_base)*/
    _30PatchNList(_next_base_59166);

    /** 	tok_match(END)*/
    _30tok_match(402, 0);

    /** 	tok_match(WHILE, END)*/
    _30tok_match(47, 402);

    /** 	End_block( WHILE )*/
    _66End_block(47);

    /** 	StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 	emit_op(ENDWHILE)*/
    _37emit_op(22);

    /** 	emit_addr(bp1)*/
    _37emit_addr(_bp1_59163);

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto LA; // [421] 432
    }
    else{
    }

    /** 		emit_op(NOP1)*/
    _37emit_op(159);
LA: 

    /** 	if bp2 != 0 then*/
    if (_bp2_59164 == 0)
    goto LB; // [436] 456

    /** 		backpatch(bp2, length(Code)+1)*/
    if (IS_SEQUENCE(_25Code_12355)){
            _30289 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _30289 = 1;
    }
    _30290 = _30289 + 1;
    _30289 = NOVALUE;
    _37backpatch(_bp2_59164, _30290);
    _30290 = NOVALUE;
LB: 

    /** 	exit_loop(exit_base)*/
    _30exit_loop(_exit_base_59165);

    /** 	entry_stack = remove( entry_stack, length( entry_stack ) )*/
    if (IS_SEQUENCE(_30entry_stack_55198)){
            _30291 = SEQ_PTR(_30entry_stack_55198)->length;
    }
    else {
        _30291 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_30entry_stack_55198);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_30291)) ? _30291 : (long)(DBL_PTR(_30291)->dbl);
        int stop = (IS_ATOM_INT(_30291)) ? _30291 : (long)(DBL_PTR(_30291)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_30entry_stack_55198), start, &_30entry_stack_55198 );
            }
            else Tail(SEQ_PTR(_30entry_stack_55198), stop+1, &_30entry_stack_55198);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_30entry_stack_55198), start, &_30entry_stack_55198);
        }
        else {
            assign_slice_seq = &assign_space;
            _30entry_stack_55198 = Remove_elements(start, stop, (SEQ_PTR(_30entry_stack_55198)->ref == 1));
        }
    }
    _30291 = NOVALUE;
    _30291 = NOVALUE;

    /** 	push_temps( temps )*/
    RefDS(_temps_59237);
    _37push_temps(_temps_59237);

    /** end procedure*/
    DeRef(_uninitialized_59167);
    DeRefDS(_temps_59237);
    return;
    ;
}


void _30Loop_statement()
{
    int _bp1_59267 = NOVALUE;
    int _exit_base_59268 = NOVALUE;
    int _next_base_59269 = NOVALUE;
    int _t_59271 = NOVALUE;
    int _uninitialized_59274 = NOVALUE;
    int _30315 = NOVALUE;
    int _30313 = NOVALUE;
    int _30312 = NOVALUE;
    int _30311 = NOVALUE;
    int _30305 = NOVALUE;
    int _30304 = NOVALUE;
    int _30302 = NOVALUE;
    int _30299 = NOVALUE;
    int _30298 = NOVALUE;
    int _30297 = NOVALUE;
    int _0, _1, _2;
    

    /** 	Start_block( LOOP )*/
    _66Start_block(422, 0);

    /** 	sequence uninitialized = get_private_uninitialized()*/
    _0 = _uninitialized_59274;
    _uninitialized_59274 = _30get_private_uninitialized();
    DeRef(_0);

    /** 	entry_stack = append( entry_stack, uninitialized )*/
    RefDS(_uninitialized_59274);
    Append(&_30entry_stack_55198, _30entry_stack_55198, _uninitialized_59274);

    /** 	exit_base = length(exit_list)*/
    if (IS_SEQUENCE(_30exit_list_55191)){
            _exit_base_59268 = SEQ_PTR(_30exit_list_55191)->length;
    }
    else {
        _exit_base_59268 = 1;
    }

    /** 	next_base = length(continue_list)*/
    if (IS_SEQUENCE(_30continue_list_55193)){
            _next_base_59269 = SEQ_PTR(_30continue_list_55193)->length;
    }
    else {
        _next_base_59269 = 1;
    }

    /** 	emit_op(NOP2) -- Entry_statement() may patch this*/
    _37emit_op(110);

    /** 	emit_addr(0)*/
    _37emit_addr(0);

    /** 	if finish_block_header(LOOP) then*/
    _30297 = _30finish_block_header(422);
    if (_30297 == 0) {
        DeRef(_30297);
        _30297 = NOVALUE;
        goto L1; // [58] 81
    }
    else {
        if (!IS_ATOM_INT(_30297) && DBL_PTR(_30297)->dbl == 0.0){
            DeRef(_30297);
            _30297 = NOVALUE;
            goto L1; // [58] 81
        }
        DeRef(_30297);
        _30297 = NOVALUE;
    }
    DeRef(_30297);
    _30297 = NOVALUE;

    /** 	    entry_addr &= length(Code)-1*/
    if (IS_SEQUENCE(_25Code_12355)){
            _30298 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _30298 = 1;
    }
    _30299 = _30298 - 1;
    _30298 = NOVALUE;
    Append(&_30entry_addr_55195, _30entry_addr_55195, _30299);
    _30299 = NOVALUE;
    goto L2; // [78] 90
L1: 

    /** 		entry_addr &= -1*/
    Append(&_30entry_addr_55195, _30entry_addr_55195, -1);
L2: 

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L3; // [94] 105
    }
    else{
    }

    /** 		emit_op(NOP1)*/
    _37emit_op(159);
L3: 

    /** 	bp1 = length(Code)+1*/
    if (IS_SEQUENCE(_25Code_12355)){
            _30302 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _30302 = 1;
    }
    _bp1_59267 = _30302 + 1;
    _30302 = NOVALUE;

    /** 	retry_addr &= length(Code)+1*/
    if (IS_SEQUENCE(_25Code_12355)){
            _30304 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _30304 = 1;
    }
    _30305 = _30304 + 1;
    _30304 = NOVALUE;
    Append(&_30retry_addr_55197, _30retry_addr_55197, _30305);
    _30305 = NOVALUE;

    /** 	continue_addr &= 0*/
    Append(&_30continue_addr_55196, _30continue_addr_55196, 0);

    /** 	loop_stack &= LOOP*/
    Append(&_30loop_stack_55205, _30loop_stack_55205, 422);

    /** 	Statement_list()*/
    _30Statement_list();

    /** 	End_block( LOOP )*/
    _66End_block(422);

    /** 	tok_match(UNTIL)*/
    _30tok_match(423, 0);

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L4; // [174] 185
    }
    else{
    }

    /** 		emit_op(NOP1)*/
    _37emit_op(159);
L4: 

    /** 	PatchNList(next_base)*/
    _30PatchNList(_next_base_59269);

    /** 	StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 	short_circuit += 1*/
    _30short_circuit_55171 = _30short_circuit_55171 + 1;

    /** 	short_circuit_B = FALSE*/
    _30short_circuit_B_55173 = _5FALSE_242;

    /** 	SC1_type = 0*/
    _30SC1_type_55176 = 0;

    /** 	Expr()*/
    _30Expr();

    /** 	if SC1_type = OR then*/
    if (_30SC1_type_55176 != 9)
    goto L5; // [231] 284

    /** 		backpatch(SC1_patch-3, SC1_OR_IF)*/
    _30311 = _30SC1_patch_55175 - 3;
    if ((long)((unsigned long)_30311 +(unsigned long) HIGH_BITS) >= 0){
        _30311 = NewDouble((double)_30311);
    }
    _37backpatch(_30311, 147);
    _30311 = NOVALUE;

    /** 		if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L6; // [253] 264
    }
    else{
    }

    /** 		    emit_op(NOP1)  -- to get label here*/
    _37emit_op(159);
L6: 

    /** 		backpatch(SC1_patch, length(Code)+1)*/
    if (IS_SEQUENCE(_25Code_12355)){
            _30312 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _30312 = 1;
    }
    _30313 = _30312 + 1;
    _30312 = NOVALUE;
    _37backpatch(_30SC1_patch_55175, _30313);
    _30313 = NOVALUE;
    goto L7; // [281] 310
L5: 

    /** 	elsif SC1_type = AND then*/
    if (_30SC1_type_55176 != 8)
    goto L8; // [290] 309

    /** 		backpatch(SC1_patch-3, SC1_AND_IF)*/
    _30315 = _30SC1_patch_55175 - 3;
    if ((long)((unsigned long)_30315 +(unsigned long) HIGH_BITS) >= 0){
        _30315 = NewDouble((double)_30315);
    }
    _37backpatch(_30315, 146);
    _30315 = NOVALUE;
L8: 
L7: 

    /** 	short_circuit -= 1*/
    _30short_circuit_55171 = _30short_circuit_55171 - 1;

    /** 	emit_op(IF)*/
    _37emit_op(20);

    /** 	emit_addr(bp1)*/
    _37emit_addr(_bp1_59267);

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L9; // [334] 345
    }
    else{
    }

    /** 		emit_op(NOP1)*/
    _37emit_op(159);
L9: 

    /** 	exit_loop(exit_base)*/
    _30exit_loop(_exit_base_59268);

    /** 	tok_match(END)*/
    _30tok_match(402, 0);

    /** 	tok_match(LOOP, END)*/
    _30tok_match(422, 402);

    /** end procedure*/
    DeRef(_uninitialized_59274);
    return;
    ;
}


void _30Ifdef_statement()
{
    int _option_59353 = NOVALUE;
    int _matched_59354 = NOVALUE;
    int _has_matched_59355 = NOVALUE;
    int _in_matched_59356 = NOVALUE;
    int _dead_ifdef_59357 = NOVALUE;
    int _in_elsedef_59358 = NOVALUE;
    int _tok_59360 = NOVALUE;
    int _keyw_59361 = NOVALUE;
    int _parser_id_59365 = NOVALUE;
    int _negate_59381 = NOVALUE;
    int _conjunction_59382 = NOVALUE;
    int _at_start_59383 = NOVALUE;
    int _prev_conj_59384 = NOVALUE;
    int _this_matched_59457 = NOVALUE;
    int _gotword_59473 = NOVALUE;
    int _gotthen_59474 = NOVALUE;
    int _if_lvl_59475 = NOVALUE;
    int _30432 = NOVALUE;
    int _30431 = NOVALUE;
    int _30427 = NOVALUE;
    int _30425 = NOVALUE;
    int _30422 = NOVALUE;
    int _30420 = NOVALUE;
    int _30419 = NOVALUE;
    int _30415 = NOVALUE;
    int _30412 = NOVALUE;
    int _30409 = NOVALUE;
    int _30405 = NOVALUE;
    int _30403 = NOVALUE;
    int _30402 = NOVALUE;
    int _30401 = NOVALUE;
    int _30400 = NOVALUE;
    int _30399 = NOVALUE;
    int _30398 = NOVALUE;
    int _30397 = NOVALUE;
    int _30393 = NOVALUE;
    int _30390 = NOVALUE;
    int _30389 = NOVALUE;
    int _30388 = NOVALUE;
    int _30384 = NOVALUE;
    int _30383 = NOVALUE;
    int _30382 = NOVALUE;
    int _30379 = NOVALUE;
    int _30376 = NOVALUE;
    int _30375 = NOVALUE;
    int _30374 = NOVALUE;
    int _30372 = NOVALUE;
    int _30361 = NOVALUE;
    int _30359 = NOVALUE;
    int _30358 = NOVALUE;
    int _30357 = NOVALUE;
    int _30356 = NOVALUE;
    int _30355 = NOVALUE;
    int _30354 = NOVALUE;
    int _30353 = NOVALUE;
    int _30350 = NOVALUE;
    int _30349 = NOVALUE;
    int _30347 = NOVALUE;
    int _30345 = NOVALUE;
    int _30344 = NOVALUE;
    int _30342 = NOVALUE;
    int _30340 = NOVALUE;
    int _30339 = NOVALUE;
    int _30337 = NOVALUE;
    int _30336 = NOVALUE;
    int _30335 = NOVALUE;
    int _30332 = NOVALUE;
    int _30330 = NOVALUE;
    int _30327 = NOVALUE;
    int _30326 = NOVALUE;
    int _30325 = NOVALUE;
    int _30323 = NOVALUE;
    int _30321 = NOVALUE;
    int _30320 = NOVALUE;
    int _30319 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer matched = 0, has_matched = 0,  in_matched = 0, dead_ifdef = 0, in_elsedef = 0*/
    _matched_59354 = 0;
    _has_matched_59355 = 0;
    _in_matched_59356 = 0;
    _dead_ifdef_59357 = 0;
    _in_elsedef_59358 = 0;

    /** 	sequence keyw ="ifdef"*/
    RefDS(_27065);
    DeRefi(_keyw_59361);
    _keyw_59361 = _27065;

    /** 	live_ifdef += 1*/
    _30live_ifdef_59349 = _30live_ifdef_59349 + 1;

    /** 	ifdef_lineno &= line_number*/
    Append(&_30ifdef_lineno_59350, _30ifdef_lineno_59350, _25line_number_12263);

    /** 	integer parser_id*/

    /** 	if CurrentSub != TopLevelSub or length(if_labels) or length(loop_labels) then*/
    _30319 = (_25CurrentSub_12270 != _25TopLevelSub_12269);
    if (_30319 != 0) {
        _30320 = 1;
        goto L1; // [55] 68
    }
    if (IS_SEQUENCE(_30if_labels_55200)){
            _30321 = SEQ_PTR(_30if_labels_55200)->length;
    }
    else {
        _30321 = 1;
    }
    _30320 = (_30321 != 0);
L1: 
    if (_30320 != 0) {
        goto L2; // [68] 82
    }
    if (IS_SEQUENCE(_30loop_labels_55199)){
            _30323 = SEQ_PTR(_30loop_labels_55199)->length;
    }
    else {
        _30323 = 1;
    }
    if (_30323 == 0)
    {
        _30323 = NOVALUE;
        goto L3; // [78] 92
    }
    else{
        _30323 = NOVALUE;
    }
L2: 

    /** 		parser_id = forward_Statement_list*/
    _parser_id_59365 = _30forward_Statement_list_58200;
    goto L4; // [89] 100
L3: 

    /** 		parser_id = top_level_parser*/
    _parser_id_59365 = _30top_level_parser_59348;
L4: 

    /** 	while 1 label "top" do*/
L5: 

    /** 		if matched = 0 and in_elsedef = 0 then*/
    _30325 = (_matched_59354 == 0);
    if (_30325 == 0) {
        goto L6; // [111] 632
    }
    _30327 = (_in_elsedef_59358 == 0);
    if (_30327 == 0)
    {
        DeRef(_30327);
        _30327 = NOVALUE;
        goto L6; // [120] 632
    }
    else{
        DeRef(_30327);
        _30327 = NOVALUE;
    }

    /** 			integer negate = 0, conjunction = 0*/
    _negate_59381 = 0;
    _conjunction_59382 = 0;

    /** 			integer at_start = 1*/
    _at_start_59383 = 1;

    /** 			sequence prev_conj = ""*/
    RefDS(_22682);
    DeRef(_prev_conj_59384);
    _prev_conj_59384 = _22682;

    /** 			while 1 label "deflist" do*/
L7: 

    /** 				option = StringToken()*/
    RefDS(_5);
    _0 = _option_59353;
    _option_59353 = _60StringToken(_5);
    DeRef(_0);

    /** 				if equal(option, "then") then*/
    if (_option_59353 == _27132)
    _30330 = 1;
    else if (IS_ATOM_INT(_option_59353) && IS_ATOM_INT(_27132))
    _30330 = 0;
    else
    _30330 = (compare(_option_59353, _27132) == 0);
    if (_30330 == 0)
    {
        _30330 = NOVALUE;
        goto L8; // [162] 234
    }
    else{
        _30330 = NOVALUE;
    }

    /** 					if at_start = 1 then*/
    if (_at_start_59383 != 1)
    goto L9; // [167] 185

    /** 						CompileErr(6, {keyw})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_keyw_59361);
    *((int *)(_2+4)) = _keyw_59361;
    _30332 = MAKE_SEQ(_1);
    _43CompileErr(6, _30332, 0);
    _30332 = NOVALUE;
    goto LA; // [182] 518
L9: 

    /** 					elsif conjunction = 0 then*/
    if (_conjunction_59382 != 0)
    goto LB; // [187] 219

    /** 						if negate = 0 then*/
    if (_negate_59381 != 0)
    goto LC; // [193] 204

    /** 							exit "deflist"*/
    goto LD; // [199] 606
    goto LA; // [201] 518
LC: 

    /** 							CompileErr(11, {keyw})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_keyw_59361);
    *((int *)(_2+4)) = _keyw_59361;
    _30335 = MAKE_SEQ(_1);
    _43CompileErr(11, _30335, 0);
    _30335 = NOVALUE;
    goto LA; // [216] 518
LB: 

    /** 						CompileErr(8, {keyw, prev_conj})*/
    RefDS(_prev_conj_59384);
    RefDS(_keyw_59361);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _keyw_59361;
    ((int *)_2)[2] = _prev_conj_59384;
    _30336 = MAKE_SEQ(_1);
    _43CompileErr(8, _30336, 0);
    _30336 = NOVALUE;
    goto LA; // [231] 518
L8: 

    /** 				elsif equal(option, "not") then*/
    if (_option_59353 == _27093)
    _30337 = 1;
    else if (IS_ATOM_INT(_option_59353) && IS_ATOM_INT(_27093))
    _30337 = 0;
    else
    _30337 = (compare(_option_59353, _27093) == 0);
    if (_30337 == 0)
    {
        _30337 = NOVALUE;
        goto LE; // [240] 276
    }
    else{
        _30337 = NOVALUE;
    }

    /** 					if negate = 0 then*/
    if (_negate_59381 != 0)
    goto LF; // [245] 261

    /** 						negate = 1*/
    _negate_59381 = 1;

    /** 						continue "deflist"*/
    goto L7; // [256] 148
    goto LA; // [258] 518
LF: 

    /** 						CompileErr(7, {keyw})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_keyw_59361);
    *((int *)(_2+4)) = _keyw_59361;
    _30339 = MAKE_SEQ(_1);
    _43CompileErr(7, _30339, 0);
    _30339 = NOVALUE;
    goto LA; // [273] 518
LE: 

    /** 				elsif equal(option, "and") then*/
    if (_option_59353 == _26997)
    _30340 = 1;
    else if (IS_ATOM_INT(_option_59353) && IS_ATOM_INT(_26997))
    _30340 = 0;
    else
    _30340 = (compare(_option_59353, _26997) == 0);
    if (_30340 == 0)
    {
        _30340 = NOVALUE;
        goto L10; // [282] 345
    }
    else{
        _30340 = NOVALUE;
    }

    /** 					if at_start = 1 then*/
    if (_at_start_59383 != 1)
    goto L11; // [287] 305

    /** 						CompileErr(2, {keyw})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_keyw_59361);
    *((int *)(_2+4)) = _keyw_59361;
    _30342 = MAKE_SEQ(_1);
    _43CompileErr(2, _30342, 0);
    _30342 = NOVALUE;
    goto LA; // [302] 518
L11: 

    /** 					elsif conjunction = 0 then*/
    if (_conjunction_59382 != 0)
    goto L12; // [307] 330

    /** 						conjunction = 1*/
    _conjunction_59382 = 1;

    /** 						prev_conj = option*/
    RefDS(_option_59353);
    DeRef(_prev_conj_59384);
    _prev_conj_59384 = _option_59353;

    /** 						continue "deflist"*/
    goto L7; // [325] 148
    goto LA; // [327] 518
L12: 

    /** 						CompileErr(10,{keyw,prev_conj})*/
    RefDS(_prev_conj_59384);
    RefDS(_keyw_59361);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _keyw_59361;
    ((int *)_2)[2] = _prev_conj_59384;
    _30344 = MAKE_SEQ(_1);
    _43CompileErr(10, _30344, 0);
    _30344 = NOVALUE;
    goto LA; // [342] 518
L10: 

    /** 				elsif equal(option, "or") then*/
    if (_option_59353 == _27097)
    _30345 = 1;
    else if (IS_ATOM_INT(_option_59353) && IS_ATOM_INT(_27097))
    _30345 = 0;
    else
    _30345 = (compare(_option_59353, _27097) == 0);
    if (_30345 == 0)
    {
        _30345 = NOVALUE;
        goto L13; // [351] 414
    }
    else{
        _30345 = NOVALUE;
    }

    /** 					if at_start = 1 then*/
    if (_at_start_59383 != 1)
    goto L14; // [356] 374

    /** 						CompileErr(6, {keyw})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_keyw_59361);
    *((int *)(_2+4)) = _keyw_59361;
    _30347 = MAKE_SEQ(_1);
    _43CompileErr(6, _30347, 0);
    _30347 = NOVALUE;
    goto LA; // [371] 518
L14: 

    /** 					elsif conjunction = 0 then*/
    if (_conjunction_59382 != 0)
    goto L15; // [376] 399

    /** 						conjunction = 2*/
    _conjunction_59382 = 2;

    /** 						prev_conj = option*/
    RefDS(_option_59353);
    DeRef(_prev_conj_59384);
    _prev_conj_59384 = _option_59353;

    /** 						continue "deflist"*/
    goto L7; // [394] 148
    goto LA; // [396] 518
L15: 

    /** 						CompileErr(9, {keyw, prev_conj})*/
    RefDS(_prev_conj_59384);
    RefDS(_keyw_59361);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _keyw_59361;
    ((int *)_2)[2] = _prev_conj_59384;
    _30349 = MAKE_SEQ(_1);
    _43CompileErr(9, _30349, 0);
    _30349 = NOVALUE;
    goto LA; // [411] 518
L13: 

    /** 				elsif length(option) = 0 then*/
    if (IS_SEQUENCE(_option_59353)){
            _30350 = SEQ_PTR(_option_59353)->length;
    }
    else {
        _30350 = 1;
    }
    if (_30350 != 0)
    goto L16; // [419] 454

    /** 					if at_start = 1 then*/
    if (_at_start_59383 != 1)
    goto L17; // [425] 443

    /** 						CompileErr(122, {keyw})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_keyw_59361);
    *((int *)(_2+4)) = _keyw_59361;
    _30353 = MAKE_SEQ(_1);
    _43CompileErr(122, _30353, 0);
    _30353 = NOVALUE;
    goto LA; // [440] 518
L17: 

    /** 						CompileErr(82)*/
    RefDS(_22682);
    _43CompileErr(82, _22682, 0);
    goto LA; // [451] 518
L16: 

    /** 				elsif not at_start and length(prev_conj) = 0 then*/
    _30354 = (_at_start_59383 == 0);
    if (_30354 == 0) {
        goto L18; // [459] 488
    }
    if (IS_SEQUENCE(_prev_conj_59384)){
            _30356 = SEQ_PTR(_prev_conj_59384)->length;
    }
    else {
        _30356 = 1;
    }
    _30357 = (_30356 == 0);
    _30356 = NOVALUE;
    if (_30357 == 0)
    {
        DeRef(_30357);
        _30357 = NOVALUE;
        goto L18; // [471] 488
    }
    else{
        DeRef(_30357);
        _30357 = NOVALUE;
    }

    /** 					CompileErr(4, {keyw})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_keyw_59361);
    *((int *)(_2+4)) = _keyw_59361;
    _30358 = MAKE_SEQ(_1);
    _43CompileErr(4, _30358, 0);
    _30358 = NOVALUE;
    goto LA; // [485] 518
L18: 

    /** 				elsif t_identifier(option) = 0 then*/
    RefDS(_option_59353);
    _30359 = _5t_identifier(_option_59353);
    if (binary_op_a(NOTEQ, _30359, 0)){
        DeRef(_30359);
        _30359 = NOVALUE;
        goto L19; // [494] 512
    }
    DeRef(_30359);
    _30359 = NOVALUE;

    /** 					CompileErr(3, {keyw})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_keyw_59361);
    *((int *)(_2+4)) = _keyw_59361;
    _30361 = MAKE_SEQ(_1);
    _43CompileErr(3, _30361, 0);
    _30361 = NOVALUE;
    goto LA; // [509] 518
L19: 

    /** 					at_start = 0*/
    _at_start_59383 = 0;
LA: 

    /** 				integer this_matched = find(option, OpDefines)*/
    _this_matched_59457 = find_from(_option_59353, _25OpDefines_12336, 1);

    /** 				if negate then*/
    if (_negate_59381 == 0)
    {
        goto L1A; // [529] 543
    }
    else{
    }

    /** 					this_matched = not this_matched*/
    _this_matched_59457 = (_this_matched_59457 == 0);

    /** 					negate = 0*/
    _negate_59381 = 0;
L1A: 

    /** 				if conjunction = 0 then*/
    if (_conjunction_59382 != 0)
    goto L1B; // [545] 557

    /** 					matched = this_matched*/
    _matched_59354 = _this_matched_59457;
    goto L1C; // [554] 599
L1B: 

    /** 					if conjunction = 1 then*/
    if (_conjunction_59382 != 1)
    goto L1D; // [559] 572

    /** 						matched = matched and this_matched*/
    _matched_59354 = (_matched_59354 != 0 && _this_matched_59457 != 0);
    goto L1E; // [569] 586
L1D: 

    /** 					elsif conjunction = 2 then*/
    if (_conjunction_59382 != 2)
    goto L1F; // [574] 585

    /** 						matched = matched or this_matched*/
    _matched_59354 = (_matched_59354 != 0 || _this_matched_59457 != 0);
L1F: 
L1E: 

    /** 					conjunction = 0*/
    _conjunction_59382 = 0;

    /** 					prev_conj = ""*/
    RefDS(_22682);
    DeRef(_prev_conj_59384);
    _prev_conj_59384 = _22682;
L1C: 

    /** 			end while*/
    goto L7; // [603] 148
LD: 

    /** 			in_matched = matched*/
    _in_matched_59356 = _matched_59354;

    /** 			if matched then*/
    if (_matched_59354 == 0)
    {
        goto L20; // [613] 631
    }
    else{
    }

    /** 				No_new_entry = 0*/
    _52No_new_entry_48273 = 0;

    /** 				call_proc(parser_id, {})*/
    _0 = (int)_00[_parser_id_59365].addr;
    (*(int (*)())_0)(
                         );
L20: 
L6: 
    DeRef(_prev_conj_59384);
    _prev_conj_59384 = NOVALUE;

    /** 		integer gotword = 0*/
    _gotword_59473 = 0;

    /** 		integer gotthen = 0*/
    _gotthen_59474 = 0;

    /** 		integer if_lvl  = 0*/
    _if_lvl_59475 = 0;

    /** 		No_new_entry = not matched*/
    _52No_new_entry_48273 = (_matched_59354 == 0);

    /** 		has_matched = has_matched or matched*/
    _has_matched_59355 = (_has_matched_59355 != 0 || _matched_59354 != 0);

    /** 		keyw = "elsifdef"*/
    RefDS(_27033);
    DeRefi(_keyw_59361);
    _keyw_59361 = _27033;

    /** 		while 1 do*/
L21: 

    /** 			tok = next_token()*/
    _0 = _tok_59360;
    _tok_59360 = _30next_token();
    DeRef(_0);

    /** 			if tok[T_ID] = END_OF_FILE then*/
    _2 = (int)SEQ_PTR(_tok_59360);
    _30372 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30372, -21)){
        _30372 = NOVALUE;
        goto L22; // [689] 712
    }
    _30372 = NOVALUE;

    /** 				CompileErr(65, ifdef_lineno[$])*/
    if (IS_SEQUENCE(_30ifdef_lineno_59350)){
            _30374 = SEQ_PTR(_30ifdef_lineno_59350)->length;
    }
    else {
        _30374 = 1;
    }
    _2 = (int)SEQ_PTR(_30ifdef_lineno_59350);
    _30375 = (int)*(((s1_ptr)_2)->base + _30374);
    _43CompileErr(65, _30375, 0);
    _30375 = NOVALUE;
    goto L21; // [709] 674
L22: 

    /** 			elsif tok[T_ID] = END then*/
    _2 = (int)SEQ_PTR(_tok_59360);
    _30376 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30376, 402)){
        _30376 = NOVALUE;
        goto L23; // [722] 844
    }
    _30376 = NOVALUE;

    /** 				tok = next_token()*/
    _0 = _tok_59360;
    _tok_59360 = _30next_token();
    DeRef(_0);

    /** 				if tok[T_ID] = IFDEF then*/
    _2 = (int)SEQ_PTR(_tok_59360);
    _30379 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30379, 407)){
        _30379 = NOVALUE;
        goto L24; // [741] 769
    }
    _30379 = NOVALUE;

    /** 					if dead_ifdef then*/
    if (_dead_ifdef_59357 == 0)
    {
        goto L25; // [747] 759
    }
    else{
    }

    /** 						dead_ifdef -= 1*/
    _dead_ifdef_59357 = _dead_ifdef_59357 - 1;
    goto L21; // [756] 674
L25: 

    /** 						exit "top"*/
    goto L26; // [763] 1312
    goto L21; // [766] 674
L24: 

    /** 				elsif in_matched then*/
    if (_in_matched_59356 == 0)
    {
        goto L27; // [771] 793
    }
    else{
    }

    /** 					CompileErr(75, ifdef_lineno[$])*/
    if (IS_SEQUENCE(_30ifdef_lineno_59350)){
            _30382 = SEQ_PTR(_30ifdef_lineno_59350)->length;
    }
    else {
        _30382 = 1;
    }
    _2 = (int)SEQ_PTR(_30ifdef_lineno_59350);
    _30383 = (int)*(((s1_ptr)_2)->base + _30382);
    _43CompileErr(75, _30383, 0);
    _30383 = NOVALUE;
    goto L21; // [790] 674
L27: 

    /** 					if tok[T_ID] = IF then*/
    _2 = (int)SEQ_PTR(_tok_59360);
    _30384 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30384, 20)){
        _30384 = NOVALUE;
        goto L21; // [803] 674
    }
    _30384 = NOVALUE;

    /** 						if if_lvl > 0 then*/
    if (_if_lvl_59475 <= 0)
    goto L28; // [809] 822

    /** 							if_lvl -= 1*/
    _if_lvl_59475 = _if_lvl_59475 - 1;
    goto L21; // [819] 674
L28: 

    /** 							CompileErr(111, ifdef_lineno[$])*/
    if (IS_SEQUENCE(_30ifdef_lineno_59350)){
            _30388 = SEQ_PTR(_30ifdef_lineno_59350)->length;
    }
    else {
        _30388 = 1;
    }
    _2 = (int)SEQ_PTR(_30ifdef_lineno_59350);
    _30389 = (int)*(((s1_ptr)_2)->base + _30388);
    _43CompileErr(111, _30389, 0);
    _30389 = NOVALUE;
    goto L21; // [841] 674
L23: 

    /** 			elsif tok[T_ID] = IF then*/
    _2 = (int)SEQ_PTR(_tok_59360);
    _30390 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30390, 20)){
        _30390 = NOVALUE;
        goto L29; // [854] 867
    }
    _30390 = NOVALUE;

    /** 				if_lvl += 1*/
    _if_lvl_59475 = _if_lvl_59475 + 1;
    goto L21; // [864] 674
L29: 

    /** 			elsif tok[T_ID] = ELSE then*/
    _2 = (int)SEQ_PTR(_tok_59360);
    _30393 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30393, 23)){
        _30393 = NOVALUE;
        goto L2A; // [877] 913
    }
    _30393 = NOVALUE;

    /** 				if not in_matched then*/
    if (_in_matched_59356 != 0)
    goto L21; // [883] 674

    /** 					if if_lvl = 0 then*/
    if (_if_lvl_59475 != 0)
    goto L21; // [888] 674

    /** 						CompileErr(108, ifdef_lineno[$])*/
    if (IS_SEQUENCE(_30ifdef_lineno_59350)){
            _30397 = SEQ_PTR(_30ifdef_lineno_59350)->length;
    }
    else {
        _30397 = 1;
    }
    _2 = (int)SEQ_PTR(_30ifdef_lineno_59350);
    _30398 = (int)*(((s1_ptr)_2)->base + _30397);
    _43CompileErr(108, _30398, 0);
    _30398 = NOVALUE;
    goto L21; // [910] 674
L2A: 

    /** 			elsif tok[T_ID] = ELSIFDEF and not dead_ifdef then*/
    _2 = (int)SEQ_PTR(_tok_59360);
    _30399 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_30399)) {
        _30400 = (_30399 == 408);
    }
    else {
        _30400 = binary_op(EQUALS, _30399, 408);
    }
    _30399 = NOVALUE;
    if (IS_ATOM_INT(_30400)) {
        if (_30400 == 0) {
            goto L2B; // [927] 1065
        }
    }
    else {
        if (DBL_PTR(_30400)->dbl == 0.0) {
            goto L2B; // [927] 1065
        }
    }
    _30402 = (_dead_ifdef_59357 == 0);
    if (_30402 == 0)
    {
        DeRef(_30402);
        _30402 = NOVALUE;
        goto L2B; // [935] 1065
    }
    else{
        DeRef(_30402);
        _30402 = NOVALUE;
    }

    /** 				if has_matched then*/
    if (_has_matched_59355 == 0)
    {
        goto L2C; // [940] 1305
    }
    else{
    }

    /** 					in_matched = 0*/
    _in_matched_59356 = 0;

    /** 					No_new_entry = 1*/
    _52No_new_entry_48273 = 1;

    /** 					gotword = 0*/
    _gotword_59473 = 0;

    /** 					gotthen = 0*/
    _gotthen_59474 = 0;

    /** 					while length(option) > 0 with entry do*/
    goto L2D; // [967] 1009
L2E: 
    if (IS_SEQUENCE(_option_59353)){
            _30403 = SEQ_PTR(_option_59353)->length;
    }
    else {
        _30403 = 1;
    }
    if (_30403 <= 0)
    goto L2F; // [975] 1022

    /** 						if equal(option, "then") then*/
    if (_option_59353 == _27132)
    _30405 = 1;
    else if (IS_ATOM_INT(_option_59353) && IS_ATOM_INT(_27132))
    _30405 = 0;
    else
    _30405 = (compare(_option_59353, _27132) == 0);
    if (_30405 == 0)
    {
        _30405 = NOVALUE;
        goto L30; // [985] 1000
    }
    else{
        _30405 = NOVALUE;
    }

    /** 							gotthen = 1*/
    _gotthen_59474 = 1;

    /** 							exit*/
    goto L2F; // [995] 1022
    goto L31; // [997] 1006
L30: 

    /** 							gotword = 1*/
    _gotword_59473 = 1;
L31: 

    /** 					entry*/
L2D: 

    /** 						option = StringToken()*/
    RefDS(_5);
    _0 = _option_59353;
    _option_59353 = _60StringToken(_5);
    DeRef(_0);

    /** 					end while*/
    goto L2E; // [1019] 970
L2F: 

    /** 					if gotword = 0 then*/
    if (_gotword_59473 != 0)
    goto L32; // [1024] 1036

    /** 						CompileErr(78)*/
    RefDS(_22682);
    _43CompileErr(78, _22682, 0);
L32: 

    /** 					if gotthen = 0 then*/
    if (_gotthen_59474 != 0)
    goto L33; // [1038] 1050

    /** 						CompileErr(77)*/
    RefDS(_22682);
    _43CompileErr(77, _22682, 0);
L33: 

    /** 					read_line()*/
    _60read_line();
    goto L21; // [1054] 674

    /** 					exit*/
    goto L2C; // [1059] 1305
    goto L21; // [1062] 674
L2B: 

    /** 			elsif tok[T_ID] = ELSEDEF then*/
    _2 = (int)SEQ_PTR(_tok_59360);
    _30409 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30409, 409)){
        _30409 = NOVALUE;
        goto L34; // [1075] 1235
    }
    _30409 = NOVALUE;

    /** 				gotword = line_number*/
    _gotword_59473 = _25line_number_12263;

    /** 				option = StringToken()*/
    RefDS(_5);
    _0 = _option_59353;
    _option_59353 = _60StringToken(_5);
    DeRef(_0);

    /** 				if length(option) > 0 then*/
    if (IS_SEQUENCE(_option_59353)){
            _30412 = SEQ_PTR(_option_59353)->length;
    }
    else {
        _30412 = 1;
    }
    if (_30412 <= 0)
    goto L35; // [1101] 1135

    /** 					if line_number = gotword then*/
    if (_25line_number_12263 != _gotword_59473)
    goto L36; // [1109] 1121

    /** 						CompileErr(116)*/
    RefDS(_22682);
    _43CompileErr(116, _22682, 0);
L36: 

    /** 					bp -= length(option)*/
    if (IS_SEQUENCE(_option_59353)){
            _30415 = SEQ_PTR(_option_59353)->length;
    }
    else {
        _30415 = 1;
    }
    _43bp_49536 = _43bp_49536 - _30415;
    _30415 = NOVALUE;
L35: 

    /** 				if not dead_ifdef then*/
    if (_dead_ifdef_59357 != 0)
    goto L21; // [1137] 674

    /** 					if has_matched then*/
    if (_has_matched_59355 == 0)
    {
        goto L37; // [1142] 1164
    }
    else{
    }

    /** 						in_matched = 0*/
    _in_matched_59356 = 0;

    /** 						No_new_entry = 1*/
    _52No_new_entry_48273 = 1;

    /** 						read_line()*/
    _60read_line();
    goto L21; // [1161] 674
L37: 

    /** 						No_new_entry = 0*/
    _52No_new_entry_48273 = 0;

    /** 						in_elsedef = 1*/
    _in_elsedef_59358 = 1;

    /** 						call_proc(parser_id, {})*/
    _0 = (int)_00[_parser_id_59365].addr;
    (*(int (*)())_0)(
                         );

    /** 						tok_match(END)*/
    _30tok_match(402, 0);

    /** 						tok_match(IFDEF, END)*/
    _30tok_match(407, 402);

    /** 						live_ifdef -= 1*/
    _30live_ifdef_59349 = _30live_ifdef_59349 - 1;

    /** 						ifdef_lineno = ifdef_lineno[1..$-1]*/
    if (IS_SEQUENCE(_30ifdef_lineno_59350)){
            _30419 = SEQ_PTR(_30ifdef_lineno_59350)->length;
    }
    else {
        _30419 = 1;
    }
    _30420 = _30419 - 1;
    _30419 = NOVALUE;
    rhs_slice_target = (object_ptr)&_30ifdef_lineno_59350;
    RHS_Slice(_30ifdef_lineno_59350, 1, _30420);

    /** 						return*/
    DeRef(_option_59353);
    DeRef(_tok_59360);
    DeRefi(_keyw_59361);
    DeRef(_30319);
    _30319 = NOVALUE;
    DeRef(_30325);
    _30325 = NOVALUE;
    DeRef(_30354);
    _30354 = NOVALUE;
    _30420 = NOVALUE;
    DeRef(_30400);
    _30400 = NOVALUE;
    return;
    goto L21; // [1232] 674
L34: 

    /** 			elsif tok[T_ID] = IFDEF then*/
    _2 = (int)SEQ_PTR(_tok_59360);
    _30422 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30422, 407)){
        _30422 = NOVALUE;
        goto L38; // [1245] 1258
    }
    _30422 = NOVALUE;

    /** 				dead_ifdef += 1*/
    _dead_ifdef_59357 = _dead_ifdef_59357 + 1;
    goto L21; // [1255] 674
L38: 

    /** 			elsif tok[T_ID] = INCLUDE then*/
    _2 = (int)SEQ_PTR(_tok_59360);
    _30425 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30425, 418)){
        _30425 = NOVALUE;
        goto L39; // [1268] 1279
    }
    _30425 = NOVALUE;

    /** 				read_line()*/
    _60read_line();
    goto L21; // [1276] 674
L39: 

    /** 			elsif tok[T_ID] = CASE then*/
    _2 = (int)SEQ_PTR(_tok_59360);
    _30427 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30427, 186)){
        _30427 = NOVALUE;
        goto L21; // [1289] 674
    }
    _30427 = NOVALUE;

    /** 				tok = next_token()*/
    _0 = _tok_59360;
    _tok_59360 = _30next_token();
    DeRef(_0);

    /** 		end while*/
    goto L21; // [1302] 674
L2C: 

    /** 	end while*/
    goto L5; // [1309] 105
L26: 

    /** 	live_ifdef -= 1*/
    _30live_ifdef_59349 = _30live_ifdef_59349 - 1;

    /** 	ifdef_lineno = ifdef_lineno[1..$-1]*/
    if (IS_SEQUENCE(_30ifdef_lineno_59350)){
            _30431 = SEQ_PTR(_30ifdef_lineno_59350)->length;
    }
    else {
        _30431 = 1;
    }
    _30432 = _30431 - 1;
    _30431 = NOVALUE;
    rhs_slice_target = (object_ptr)&_30ifdef_lineno_59350;
    RHS_Slice(_30ifdef_lineno_59350, 1, _30432);

    /** 	No_new_entry = 0*/
    _52No_new_entry_48273 = 0;

    /** end procedure*/
    DeRef(_option_59353);
    DeRef(_tok_59360);
    DeRefi(_keyw_59361);
    DeRef(_30319);
    _30319 = NOVALUE;
    DeRef(_30325);
    _30325 = NOVALUE;
    DeRef(_30354);
    _30354 = NOVALUE;
    DeRef(_30420);
    _30420 = NOVALUE;
    DeRef(_30400);
    _30400 = NOVALUE;
    _30432 = NOVALUE;
    return;
    ;
}


int _30SetPrivateScope(int _s_59621, int _type_sym_59623, int _n_59624)
{
    int _hashval_59625 = NOVALUE;
    int _scope_59626 = NOVALUE;
    int _t_59628 = NOVALUE;
    int _30453 = NOVALUE;
    int _30452 = NOVALUE;
    int _30451 = NOVALUE;
    int _30450 = NOVALUE;
    int _30449 = NOVALUE;
    int _30448 = NOVALUE;
    int _30445 = NOVALUE;
    int _30442 = NOVALUE;
    int _30440 = NOVALUE;
    int _30438 = NOVALUE;
    int _30434 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_s_59621)) {
        _1 = (long)(DBL_PTR(_s_59621)->dbl);
        if (UNIQUE(DBL_PTR(_s_59621)) && (DBL_PTR(_s_59621)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_59621);
        _s_59621 = _1;
    }

    /** 	scope = SymTab[s][S_SCOPE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30434 = (int)*(((s1_ptr)_2)->base + _s_59621);
    _2 = (int)SEQ_PTR(_30434);
    _scope_59626 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_scope_59626)){
        _scope_59626 = (long)DBL_PTR(_scope_59626)->dbl;
    }
    _30434 = NOVALUE;

    /** 	switch scope do*/
    _0 = _scope_59626;
    switch ( _0 ){ 

        /** 		case SC_PRIVATE then*/
        case 3:

        /** 			DefinedYet(s)*/
        _52DefinedYet(_s_59621);

        /** 			Block_var( s )*/
        _66Block_var(_s_59621);

        /** 			return s*/
        return _s_59621;
        goto L1; // [50] 260

        /** 		case SC_LOOP_VAR then*/
        case 2:

        /** 			DefinedYet(s)*/
        _52DefinedYet(_s_59621);

        /** 			return s*/
        return _s_59621;
        goto L1; // [67] 260

        /** 		case SC_UNDEFINED, SC_MULTIPLY_DEFINED then*/
        case 9:
        case 10:

        /** 			SymTab[s][S_SCOPE] = SC_PRIVATE*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_s_59621 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 4);
        _1 = *(int *)_2;
        *(int *)_2 = 3;
        DeRef(_1);
        _30438 = NOVALUE;

        /** 			SymTab[s][S_VARNUM] = n*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_s_59621 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 16);
        _1 = *(int *)_2;
        *(int *)_2 = _n_59624;
        DeRef(_1);
        _30440 = NOVALUE;

        /** 			SymTab[s][S_VTYPE] = type_sym*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_s_59621 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 15);
        _1 = *(int *)_2;
        *(int *)_2 = _type_sym_59623;
        DeRef(_1);
        _30442 = NOVALUE;

        /** 			if type_sym < 0 then*/
        if (_type_sym_59623 >= 0)
        goto L2; // [124] 135

        /** 				register_forward_type( s, type_sym )*/
        _29register_forward_type(_s_59621, _type_sym_59623);
L2: 

        /** 			Block_var( s )*/
        _66Block_var(_s_59621);

        /** 			return s*/
        return _s_59621;
        goto L1; // [146] 260

        /** 		case SC_LOCAL, SC_GLOBAL, SC_PREDEF, SC_PUBLIC, SC_EXPORT then*/
        case 5:
        case 6:
        case 7:
        case 13:
        case 11:

        /** 			hashval = SymTab[s][S_HASHVAL]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _30445 = (int)*(((s1_ptr)_2)->base + _s_59621);
        _2 = (int)SEQ_PTR(_30445);
        _hashval_59625 = (int)*(((s1_ptr)_2)->base + 11);
        if (!IS_ATOM_INT(_hashval_59625)){
            _hashval_59625 = (long)DBL_PTR(_hashval_59625)->dbl;
        }
        _30445 = NOVALUE;

        /** 			t = buckets[hashval]*/
        _2 = (int)SEQ_PTR(_52buckets_47095);
        _t_59628 = (int)*(((s1_ptr)_2)->base + _hashval_59625);
        if (!IS_ATOM_INT(_t_59628)){
            _t_59628 = (long)DBL_PTR(_t_59628)->dbl;
        }

        /** 			buckets[hashval] = NewEntry(SymTab[s][S_NAME], n, SC_PRIVATE,*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _30448 = (int)*(((s1_ptr)_2)->base + _s_59621);
        _2 = (int)SEQ_PTR(_30448);
        if (!IS_ATOM_INT(_25S_NAME_11913)){
            _30449 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
        }
        else{
            _30449 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
        }
        _30448 = NOVALUE;
        Ref(_30449);
        _30450 = _52NewEntry(_30449, _n_59624, 3, -100, _hashval_59625, _t_59628, _type_sym_59623);
        _30449 = NOVALUE;
        _2 = (int)SEQ_PTR(_52buckets_47095);
        _2 = (int)(((s1_ptr)_2)->base + _hashval_59625);
        _1 = *(int *)_2;
        *(int *)_2 = _30450;
        if( _1 != _30450 ){
            DeRef(_1);
        }
        _30450 = NOVALUE;

        /** 			Block_var( buckets[hashval] )*/
        _2 = (int)SEQ_PTR(_52buckets_47095);
        _30451 = (int)*(((s1_ptr)_2)->base + _hashval_59625);
        Ref(_30451);
        _66Block_var(_30451);
        _30451 = NOVALUE;

        /** 			return buckets[hashval]*/
        _2 = (int)SEQ_PTR(_52buckets_47095);
        _30452 = (int)*(((s1_ptr)_2)->base + _hashval_59625);
        Ref(_30452);
        return _30452;
        goto L1; // [243] 260

        /** 		case else*/
        default:

        /** 			InternalErr(267, {scope})*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _scope_59626;
        _30453 = MAKE_SEQ(_1);
        _43InternalErr(267, _30453);
        _30453 = NOVALUE;
    ;}L1: 

    /** 	return 0*/
    _30452 = NOVALUE;
    return 0;
    ;
}


void _30For_statement()
{
    int _bp1_59693 = NOVALUE;
    int _bp2_59694 = NOVALUE;
    int _exit_base_59695 = NOVALUE;
    int _next_base_59696 = NOVALUE;
    int _end_op_59697 = NOVALUE;
    int _tok_59699 = NOVALUE;
    int _loop_var_59700 = NOVALUE;
    int _loop_var_sym_59702 = NOVALUE;
    int _save_syms_59703 = NOVALUE;
    int _30500 = NOVALUE;
    int _30498 = NOVALUE;
    int _30497 = NOVALUE;
    int _30491 = NOVALUE;
    int _30489 = NOVALUE;
    int _30487 = NOVALUE;
    int _30486 = NOVALUE;
    int _30485 = NOVALUE;
    int _30484 = NOVALUE;
    int _30482 = NOVALUE;
    int _30480 = NOVALUE;
    int _30479 = NOVALUE;
    int _30478 = NOVALUE;
    int _30477 = NOVALUE;
    int _30475 = NOVALUE;
    int _30473 = NOVALUE;
    int _30469 = NOVALUE;
    int _30467 = NOVALUE;
    int _30464 = NOVALUE;
    int _30462 = NOVALUE;
    int _30456 = NOVALUE;
    int _30455 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	sequence save_syms*/

    /** 	Start_block( FOR )*/
    _66Start_block(21, 0);

    /** 	loop_var = next_token()*/
    _0 = _loop_var_59700;
    _loop_var_59700 = _30next_token();
    DeRef(_0);

    /** 	if not find(loop_var[T_ID], ADDR_TOKS) then*/
    _2 = (int)SEQ_PTR(_loop_var_59700);
    _30455 = (int)*(((s1_ptr)_2)->base + 1);
    _30456 = find_from(_30455, _28ADDR_TOKS_11861, 1);
    _30455 = NOVALUE;
    if (_30456 != 0)
    goto L1; // [31] 42
    _30456 = NOVALUE;

    /** 		CompileErr(28)*/
    RefDS(_22682);
    _43CompileErr(28, _22682, 0);
L1: 

    /** 	if BIND then*/
    if (_25BIND_11877 == 0)
    {
        goto L2; // [46] 55
    }
    else{
    }

    /** 		add_ref(loop_var)*/
    Ref(_loop_var_59700);
    _52add_ref(_loop_var_59700);
L2: 

    /** 	tok_match(EQUALS)*/
    _30tok_match(3, 0);

    /** 	exit_base = length(exit_list)*/
    if (IS_SEQUENCE(_30exit_list_55191)){
            _exit_base_59695 = SEQ_PTR(_30exit_list_55191)->length;
    }
    else {
        _exit_base_59695 = 1;
    }

    /** 	next_base = length(continue_list)*/
    if (IS_SEQUENCE(_30continue_list_55193)){
            _next_base_59696 = SEQ_PTR(_30continue_list_55193)->length;
    }
    else {
        _next_base_59696 = 1;
    }

    /** 	Expr()*/
    _30Expr();

    /** 	tok_match(TO)*/
    _30tok_match(403, 0);

    /** 	exit_base = length(exit_list)*/
    if (IS_SEQUENCE(_30exit_list_55191)){
            _exit_base_59695 = SEQ_PTR(_30exit_list_55191)->length;
    }
    else {
        _exit_base_59695 = 1;
    }

    /** 	Expr()*/
    _30Expr();

    /** 	tok = next_token()*/
    _0 = _tok_59699;
    _tok_59699 = _30next_token();
    DeRef(_0);

    /** 	if tok[T_ID] = BY then*/
    _2 = (int)SEQ_PTR(_tok_59699);
    _30462 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30462, 404)){
        _30462 = NOVALUE;
        goto L3; // [115] 135
    }
    _30462 = NOVALUE;

    /** 		Expr()*/
    _30Expr();

    /** 		end_op = ENDFOR_GENERAL -- will be set at runtime by FOR op*/
    _end_op_59697 = 39;
    goto L4; // [132] 159
L3: 

    /** 		emit_opnd(NewIntSym(1))*/
    _30464 = _52NewIntSym(1);
    _37emit_opnd(_30464);
    _30464 = NOVALUE;

    /** 		putback(tok)*/
    Ref(_tok_59699);
    _30putback(_tok_59699);

    /** 		end_op = ENDFOR_INT_UP1*/
    _end_op_59697 = 54;
L4: 

    /** 	loop_var_sym = loop_var[T_SYM]*/
    _2 = (int)SEQ_PTR(_loop_var_59700);
    _loop_var_sym_59702 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_loop_var_sym_59702)){
        _loop_var_sym_59702 = (long)DBL_PTR(_loop_var_sym_59702)->dbl;
    }

    /** 	if CurrentSub = TopLevelSub then*/
    if (_25CurrentSub_12270 != _25TopLevelSub_12269)
    goto L5; // [175] 221

    /** 		DefinedYet(loop_var_sym)*/
    _52DefinedYet(_loop_var_sym_59702);

    /** 		SymTab[loop_var_sym][S_SCOPE] = SC_GLOOP_VAR*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_loop_var_sym_59702 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 4;
    DeRef(_1);
    _30467 = NOVALUE;

    /** 		SymTab[loop_var_sym][S_VTYPE] = object_type*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_loop_var_sym_59702 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 15);
    _1 = *(int *)_2;
    *(int *)_2 = _52object_type_47099;
    DeRef(_1);
    _30469 = NOVALUE;
    goto L6; // [218] 265
L5: 

    /** 		loop_var_sym = SetPrivateScope(loop_var_sym, object_type, param_num)*/
    _loop_var_sym_59702 = _30SetPrivateScope(_loop_var_sym_59702, _52object_type_47099, _30param_num_55180);
    if (!IS_ATOM_INT(_loop_var_sym_59702)) {
        _1 = (long)(DBL_PTR(_loop_var_sym_59702)->dbl);
        if (UNIQUE(DBL_PTR(_loop_var_sym_59702)) && (DBL_PTR(_loop_var_sym_59702)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_loop_var_sym_59702);
        _loop_var_sym_59702 = _1;
    }

    /** 		param_num += 1*/
    _30param_num_55180 = _30param_num_55180 + 1;

    /** 		SymTab[loop_var_sym][S_SCOPE] = SC_LOOP_VAR*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_loop_var_sym_59702 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _30473 = NOVALUE;

    /** 		Pop_block_var()*/
    _66Pop_block_var();
L6: 

    /** 	SymTab[loop_var_sym][S_USAGE] = or_bits(SymTab[loop_var_sym][S_USAGE], U_USED)*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_loop_var_sym_59702 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30477 = (int)*(((s1_ptr)_2)->base + _loop_var_sym_59702);
    _2 = (int)SEQ_PTR(_30477);
    _30478 = (int)*(((s1_ptr)_2)->base + 5);
    _30477 = NOVALUE;
    if (IS_ATOM_INT(_30478)) {
        {unsigned long tu;
             tu = (unsigned long)_30478 | (unsigned long)3;
             _30479 = MAKE_UINT(tu);
        }
    }
    else {
        _30479 = binary_op(OR_BITS, _30478, 3);
    }
    _30478 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _30479;
    if( _1 != _30479 ){
        DeRef(_1);
    }
    _30479 = NOVALUE;
    _30475 = NOVALUE;

    /** 	op_info1 = loop_var_sym*/
    _37op_info1_51268 = _loop_var_sym_59702;

    /** 	emit_op(FOR)*/
    _37emit_op(21);

    /** 	emit_addr(loop_var_sym)*/
    _37emit_addr(_loop_var_sym_59702);

    /** 	if finish_block_header(FOR) then*/
    _30480 = _30finish_block_header(21);
    if (_30480 == 0) {
        DeRef(_30480);
        _30480 = NOVALUE;
        goto L7; // [325] 336
    }
    else {
        if (!IS_ATOM_INT(_30480) && DBL_PTR(_30480)->dbl == 0.0){
            DeRef(_30480);
            _30480 = NOVALUE;
            goto L7; // [325] 336
        }
        DeRef(_30480);
        _30480 = NOVALUE;
    }
    DeRef(_30480);
    _30480 = NOVALUE;

    /** 		CompileErr(83)*/
    RefDS(_22682);
    _43CompileErr(83, _22682, 0);
L7: 

    /** 	entry_addr &= 0*/
    Append(&_30entry_addr_55195, _30entry_addr_55195, 0);

    /** 	bp1 = length(Code)+1*/
    if (IS_SEQUENCE(_25Code_12355)){
            _30482 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _30482 = 1;
    }
    _bp1_59693 = _30482 + 1;
    _30482 = NOVALUE;

    /** 	emit_addr(0) -- will be patched - don't straighten*/
    _37emit_addr(0);

    /** 	save_syms = Code[$-5..$-3] -- could be temps, but can't get rid of them yet*/
    if (IS_SEQUENCE(_25Code_12355)){
            _30484 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _30484 = 1;
    }
    _30485 = _30484 - 5;
    _30484 = NOVALUE;
    if (IS_SEQUENCE(_25Code_12355)){
            _30486 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _30486 = 1;
    }
    _30487 = _30486 - 3;
    _30486 = NOVALUE;
    rhs_slice_target = (object_ptr)&_save_syms_59703;
    RHS_Slice(_25Code_12355, _30485, _30487);

    /** 	for i = 1 to 3 do*/
    {
        int _i_59791;
        _i_59791 = 1;
L8: 
        if (_i_59791 > 3){
            goto L9; // [385] 408
        }

        /** 		clear_temp( save_syms[i] )*/
        _2 = (int)SEQ_PTR(_save_syms_59703);
        _30489 = (int)*(((s1_ptr)_2)->base + _i_59791);
        Ref(_30489);
        _37clear_temp(_30489);
        _30489 = NOVALUE;

        /** 	end for*/
        _i_59791 = _i_59791 + 1;
        goto L8; // [403] 392
L9: 
        ;
    }

    /** 	flush_temps()*/
    RefDS(_22682);
    _37flush_temps(_22682);

    /** 	bp2 = length(Code)*/
    if (IS_SEQUENCE(_25Code_12355)){
            _bp2_59694 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _bp2_59694 = 1;
    }

    /** 	retry_addr &= bp2 + 1*/
    _30491 = _bp2_59694 + 1;
    Append(&_30retry_addr_55197, _30retry_addr_55197, _30491);
    _30491 = NOVALUE;

    /** 	continue_addr &= 0*/
    Append(&_30continue_addr_55196, _30continue_addr_55196, 0);

    /** 	loop_stack &= FOR*/
    Append(&_30loop_stack_55205, _30loop_stack_55205, 21);

    /** 	if not TRANSLATE then*/
    if (_25TRANSLATE_11874 != 0)
    goto LA; // [454] 478

    /** 		if OpTrace then*/
    if (_25OpTrace_12332 == 0)
    {
        goto LB; // [461] 477
    }
    else{
    }

    /** 			emit_op(DISPLAY_VAR)*/
    _37emit_op(87);

    /** 			emit_addr(loop_var_sym)*/
    _37emit_addr(_loop_var_sym_59702);
LB: 
LA: 

    /** 	Statement_list()*/
    _30Statement_list();

    /** 	tok_match(END)*/
    _30tok_match(402, 0);

    /** 	tok_match(FOR, END)*/
    _30tok_match(21, 402);

    /** 	End_block( FOR )*/
    _66End_block(21);

    /** 	StartSourceLine(TRUE, TRANSLATE)*/
    _37StartSourceLine(_5TRUE_244, _25TRANSLATE_11874, 2);

    /** 	op_info1 = loop_var_sym*/
    _37op_info1_51268 = _loop_var_sym_59702;

    /** 	op_info2 = bp2 + 1*/
    _37op_info2_51269 = _bp2_59694 + 1;

    /** 	PatchNList(next_base)*/
    _30PatchNList(_next_base_59696);

    /** 	emit_op(end_op)*/
    _37emit_op(_end_op_59697);

    /** 	backpatch(bp1, length(Code)+1)*/
    if (IS_SEQUENCE(_25Code_12355)){
            _30497 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _30497 = 1;
    }
    _30498 = _30497 + 1;
    _30497 = NOVALUE;
    _37backpatch(_bp1_59693, _30498);
    _30498 = NOVALUE;

    /** 	if not TRANSLATE then*/
    if (_25TRANSLATE_11874 != 0)
    goto LC; // [566] 590

    /** 		if OpTrace then*/
    if (_25OpTrace_12332 == 0)
    {
        goto LD; // [573] 589
    }
    else{
    }

    /** 			emit_op(ERASE_SYMBOL)*/
    _37emit_op(90);

    /** 			emit_addr(loop_var_sym)*/
    _37emit_addr(_loop_var_sym_59702);
LD: 
LC: 

    /** 	Hide(loop_var_sym)*/
    _52Hide(_loop_var_sym_59702);

    /** 	exit_loop(exit_base)*/
    _30exit_loop(_exit_base_59695);

    /** 	for i = 1 to 3 do*/
    {
        int _i_59837;
        _i_59837 = 1;
LE: 
        if (_i_59837 > 3){
            goto LF; // [602] 628
        }

        /** 		emit_temp( save_syms[i], NEW_REFERENCE )*/
        _2 = (int)SEQ_PTR(_save_syms_59703);
        _30500 = (int)*(((s1_ptr)_2)->base + _i_59837);
        Ref(_30500);
        _37emit_temp(_30500, 1);
        _30500 = NOVALUE;

        /** 	end for*/
        _i_59837 = _i_59837 + 1;
        goto LE; // [623] 609
LF: 
        ;
    }

    /** 	flush_temps()*/
    RefDS(_22682);
    _37flush_temps(_22682);

    /** end procedure*/
    DeRef(_tok_59699);
    DeRef(_loop_var_59700);
    DeRef(_save_syms_59703);
    DeRef(_30485);
    _30485 = NOVALUE;
    DeRef(_30487);
    _30487 = NOVALUE;
    return;
    ;
}


int _30CompileType(int _type_ptr_59845)
{
    int _t_59846 = NOVALUE;
    int _30511 = NOVALUE;
    int _30510 = NOVALUE;
    int _30509 = NOVALUE;
    int _30503 = NOVALUE;
    int _30502 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_type_ptr_59845)) {
        _1 = (long)(DBL_PTR(_type_ptr_59845)->dbl);
        if (UNIQUE(DBL_PTR(_type_ptr_59845)) && (DBL_PTR(_type_ptr_59845)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_type_ptr_59845);
        _type_ptr_59845 = _1;
    }

    /** 	if type_ptr < 0 then*/
    if (_type_ptr_59845 >= 0)
    goto L1; // [5] 16

    /** 		return type_ptr*/
    return _type_ptr_59845;
L1: 

    /** 	if SymTab[type_ptr][S_TOKEN] = OBJECT then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30502 = (int)*(((s1_ptr)_2)->base + _type_ptr_59845);
    _2 = (int)SEQ_PTR(_30502);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _30503 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _30503 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _30502 = NOVALUE;
    if (binary_op_a(NOTEQ, _30503, 415)){
        _30503 = NOVALUE;
        goto L2; // [32] 47
    }
    _30503 = NOVALUE;

    /** 		return TYPE_OBJECT*/
    return 16;
    goto L3; // [44] 218
L2: 

    /** 	elsif type_ptr = integer_type then*/
    if (_type_ptr_59845 != _52integer_type_47105)
    goto L4; // [51] 66

    /** 		return TYPE_INTEGER*/
    return 1;
    goto L3; // [63] 218
L4: 

    /** 	elsif type_ptr = atom_type then*/
    if (_type_ptr_59845 != _52atom_type_47101)
    goto L5; // [70] 85

    /** 		return TYPE_ATOM*/
    return 4;
    goto L3; // [82] 218
L5: 

    /** 	elsif type_ptr = sequence_type then*/
    if (_type_ptr_59845 != _52sequence_type_47103)
    goto L6; // [89] 104

    /** 		return TYPE_SEQUENCE*/
    return 8;
    goto L3; // [101] 218
L6: 

    /** 	elsif type_ptr = object_type then*/
    if (_type_ptr_59845 != _52object_type_47099)
    goto L7; // [108] 123

    /** 		return TYPE_OBJECT*/
    return 16;
    goto L3; // [120] 218
L7: 

    /** 		t = SymTab[SymTab[type_ptr][S_NEXT]][S_VTYPE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30509 = (int)*(((s1_ptr)_2)->base + _type_ptr_59845);
    _2 = (int)SEQ_PTR(_30509);
    _30510 = (int)*(((s1_ptr)_2)->base + 2);
    _30509 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_30510)){
        _30511 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_30510)->dbl));
    }
    else{
        _30511 = (int)*(((s1_ptr)_2)->base + _30510);
    }
    _2 = (int)SEQ_PTR(_30511);
    _t_59846 = (int)*(((s1_ptr)_2)->base + 15);
    if (!IS_ATOM_INT(_t_59846)){
        _t_59846 = (long)DBL_PTR(_t_59846)->dbl;
    }
    _30511 = NOVALUE;

    /** 		if t = integer_type then*/
    if (_t_59846 != _52integer_type_47105)
    goto L8; // [155] 170

    /** 			return TYPE_INTEGER*/
    _30510 = NOVALUE;
    return 1;
    goto L9; // [167] 217
L8: 

    /** 		elsif t = atom_type then*/
    if (_t_59846 != _52atom_type_47101)
    goto LA; // [174] 189

    /** 			return TYPE_ATOM*/
    _30510 = NOVALUE;
    return 4;
    goto L9; // [186] 217
LA: 

    /** 		elsif t = sequence_type then*/
    if (_t_59846 != _52sequence_type_47103)
    goto LB; // [193] 208

    /** 			return TYPE_SEQUENCE*/
    _30510 = NOVALUE;
    return 8;
    goto L9; // [205] 217
LB: 

    /** 			return TYPE_OBJECT*/
    _30510 = NOVALUE;
    return 16;
L9: 
L3: 
    ;
}


int _30get_assigned_sym()
{
    int _30524 = NOVALUE;
    int _30523 = NOVALUE;
    int _30522 = NOVALUE;
    int _30520 = NOVALUE;
    int _30519 = NOVALUE;
    int _30518 = NOVALUE;
    int _30517 = NOVALUE;
    int _30516 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not find( Code[$-2], {ASSIGN, ASSIGN_I}) then*/
    if (IS_SEQUENCE(_25Code_12355)){
            _30516 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _30516 = 1;
    }
    _30517 = _30516 - 2;
    _30516 = NOVALUE;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _30518 = (int)*(((s1_ptr)_2)->base + _30517);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 18;
    ((int *)_2)[2] = 113;
    _30519 = MAKE_SEQ(_1);
    _30520 = find_from(_30518, _30519, 1);
    _30518 = NOVALUE;
    DeRefDS(_30519);
    _30519 = NOVALUE;
    if (_30520 != 0)
    goto L1; // [29] 39
    _30520 = NOVALUE;

    /** 		return 0*/
    _30517 = NOVALUE;
    return 0;
L1: 

    /** 	return Code[$-1]*/
    if (IS_SEQUENCE(_25Code_12355)){
            _30522 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _30522 = 1;
    }
    _30523 = _30522 - 1;
    _30522 = NOVALUE;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _30524 = (int)*(((s1_ptr)_2)->base + _30523);
    Ref(_30524);
    DeRef(_30517);
    _30517 = NOVALUE;
    _30523 = NOVALUE;
    return _30524;
    ;
}


void _30Assign_Constant(int _sym_59915)
{
    int _valsym_59917 = NOVALUE;
    int _val_59920 = NOVALUE;
    int _30551 = NOVALUE;
    int _30550 = NOVALUE;
    int _30548 = NOVALUE;
    int _30547 = NOVALUE;
    int _30546 = NOVALUE;
    int _30544 = NOVALUE;
    int _30543 = NOVALUE;
    int _30542 = NOVALUE;
    int _30540 = NOVALUE;
    int _30539 = NOVALUE;
    int _30538 = NOVALUE;
    int _30536 = NOVALUE;
    int _30535 = NOVALUE;
    int _30534 = NOVALUE;
    int _30532 = NOVALUE;
    int _30530 = NOVALUE;
    int _30528 = NOVALUE;
    int _30526 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	symtab_index valsym = Pop() -- pop the sym for the constant, too*/
    _valsym_59917 = _37Pop();
    if (!IS_ATOM_INT(_valsym_59917)) {
        _1 = (long)(DBL_PTR(_valsym_59917)->dbl);
        if (UNIQUE(DBL_PTR(_valsym_59917)) && (DBL_PTR(_valsym_59917)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_valsym_59917);
        _valsym_59917 = _1;
    }

    /** 	object val = SymTab[valsym][S_OBJ]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30526 = (int)*(((s1_ptr)_2)->base + _valsym_59917);
    DeRef(_val_59920);
    _2 = (int)SEQ_PTR(_30526);
    _val_59920 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_val_59920);
    _30526 = NOVALUE;

    /** 	SymTab[sym][S_OBJ] = val*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59915 + ((s1_ptr)_2)->base);
    Ref(_val_59920);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _val_59920;
    DeRef(_1);
    _30528 = NOVALUE;

    /** 	SymTab[sym][S_INITLEVEL] = 0*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59915 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 14);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _30530 = NOVALUE;

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L1; // [58] 197
    }
    else{
    }

    /** 		SymTab[sym][S_GTYPE] = SymTab[valsym][S_GTYPE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59915 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30534 = (int)*(((s1_ptr)_2)->base + _valsym_59917);
    _2 = (int)SEQ_PTR(_30534);
    _30535 = (int)*(((s1_ptr)_2)->base + 36);
    _30534 = NOVALUE;
    Ref(_30535);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = _30535;
    if( _1 != _30535 ){
        DeRef(_1);
    }
    _30535 = NOVALUE;
    _30532 = NOVALUE;

    /** 		SymTab[sym][S_SEQ_ELEM] = SymTab[valsym][S_SEQ_ELEM]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59915 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30538 = (int)*(((s1_ptr)_2)->base + _valsym_59917);
    _2 = (int)SEQ_PTR(_30538);
    _30539 = (int)*(((s1_ptr)_2)->base + 33);
    _30538 = NOVALUE;
    Ref(_30539);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = _30539;
    if( _1 != _30539 ){
        DeRef(_1);
    }
    _30539 = NOVALUE;
    _30536 = NOVALUE;

    /** 		SymTab[sym][S_OBJ_MIN] = SymTab[valsym][S_OBJ_MIN]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59915 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30542 = (int)*(((s1_ptr)_2)->base + _valsym_59917);
    _2 = (int)SEQ_PTR(_30542);
    _30543 = (int)*(((s1_ptr)_2)->base + 30);
    _30542 = NOVALUE;
    Ref(_30543);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 30);
    _1 = *(int *)_2;
    *(int *)_2 = _30543;
    if( _1 != _30543 ){
        DeRef(_1);
    }
    _30543 = NOVALUE;
    _30540 = NOVALUE;

    /** 		SymTab[sym][S_OBJ_MAX] = SymTab[valsym][S_OBJ_MAX]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59915 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30546 = (int)*(((s1_ptr)_2)->base + _valsym_59917);
    _2 = (int)SEQ_PTR(_30546);
    _30547 = (int)*(((s1_ptr)_2)->base + 31);
    _30546 = NOVALUE;
    Ref(_30547);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 31);
    _1 = *(int *)_2;
    *(int *)_2 = _30547;
    if( _1 != _30547 ){
        DeRef(_1);
    }
    _30547 = NOVALUE;
    _30544 = NOVALUE;

    /** 		SymTab[sym][S_SEQ_LEN] = SymTab[valsym][S_SEQ_LEN]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59915 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30550 = (int)*(((s1_ptr)_2)->base + _valsym_59917);
    _2 = (int)SEQ_PTR(_30550);
    _30551 = (int)*(((s1_ptr)_2)->base + 32);
    _30550 = NOVALUE;
    Ref(_30551);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _30551;
    if( _1 != _30551 ){
        DeRef(_1);
    }
    _30551 = NOVALUE;
    _30548 = NOVALUE;
L1: 

    /** end procedure*/
    DeRef(_val_59920);
    return;
    ;
}


int _30Global_declaration(int _type_ptr_59977, int _scope_59978)
{
    int _new_symbols_59979 = NOVALUE;
    int _tok_59981 = NOVALUE;
    int _tsym_59982 = NOVALUE;
    int _prevtok_59983 = NOVALUE;
    int _sym_59985 = NOVALUE;
    int _valsym_59986 = NOVALUE;
    int _prevsym_59987 = NOVALUE;
    int _deltasym_59988 = NOVALUE;
    int _h_59989 = NOVALUE;
    int _count_59990 = NOVALUE;
    int _val_59991 = NOVALUE;
    int _usedval_59992 = NOVALUE;
    int _deltafunc_59993 = NOVALUE;
    int _delta_59994 = NOVALUE;
    int _is_fwd_ref_59995 = NOVALUE;
    int _ptok_60012 = NOVALUE;
    int _negate_60028 = NOVALUE;
    int _one_60368 = NOVALUE;
    int _32365 = NOVALUE;
    int _32364 = NOVALUE;
    int _32363 = NOVALUE;
    int _30761 = NOVALUE;
    int _30759 = NOVALUE;
    int _30757 = NOVALUE;
    int _30755 = NOVALUE;
    int _30753 = NOVALUE;
    int _30750 = NOVALUE;
    int _30748 = NOVALUE;
    int _30747 = NOVALUE;
    int _30746 = NOVALUE;
    int _30745 = NOVALUE;
    int _30744 = NOVALUE;
    int _30743 = NOVALUE;
    int _30741 = NOVALUE;
    int _30739 = NOVALUE;
    int _30737 = NOVALUE;
    int _30735 = NOVALUE;
    int _30734 = NOVALUE;
    int _30733 = NOVALUE;
    int _30731 = NOVALUE;
    int _30730 = NOVALUE;
    int _30729 = NOVALUE;
    int _30728 = NOVALUE;
    int _30727 = NOVALUE;
    int _30726 = NOVALUE;
    int _30723 = NOVALUE;
    int _30722 = NOVALUE;
    int _30720 = NOVALUE;
    int _30719 = NOVALUE;
    int _30715 = NOVALUE;
    int _30712 = NOVALUE;
    int _30710 = NOVALUE;
    int _30706 = NOVALUE;
    int _30705 = NOVALUE;
    int _30704 = NOVALUE;
    int _30701 = NOVALUE;
    int _30697 = NOVALUE;
    int _30696 = NOVALUE;
    int _30695 = NOVALUE;
    int _30694 = NOVALUE;
    int _30693 = NOVALUE;
    int _30692 = NOVALUE;
    int _30689 = NOVALUE;
    int _30686 = NOVALUE;
    int _30684 = NOVALUE;
    int _30681 = NOVALUE;
    int _30680 = NOVALUE;
    int _30679 = NOVALUE;
    int _30677 = NOVALUE;
    int _30675 = NOVALUE;
    int _30674 = NOVALUE;
    int _30673 = NOVALUE;
    int _30672 = NOVALUE;
    int _30670 = NOVALUE;
    int _30669 = NOVALUE;
    int _30668 = NOVALUE;
    int _30667 = NOVALUE;
    int _30663 = NOVALUE;
    int _30662 = NOVALUE;
    int _30661 = NOVALUE;
    int _30660 = NOVALUE;
    int _30659 = NOVALUE;
    int _30658 = NOVALUE;
    int _30655 = NOVALUE;
    int _30653 = NOVALUE;
    int _30652 = NOVALUE;
    int _30651 = NOVALUE;
    int _30650 = NOVALUE;
    int _30649 = NOVALUE;
    int _30646 = NOVALUE;
    int _30644 = NOVALUE;
    int _30642 = NOVALUE;
    int _30641 = NOVALUE;
    int _30640 = NOVALUE;
    int _30639 = NOVALUE;
    int _30638 = NOVALUE;
    int _30637 = NOVALUE;
    int _30636 = NOVALUE;
    int _30634 = NOVALUE;
    int _30631 = NOVALUE;
    int _30629 = NOVALUE;
    int _30628 = NOVALUE;
    int _30627 = NOVALUE;
    int _30626 = NOVALUE;
    int _30625 = NOVALUE;
    int _30624 = NOVALUE;
    int _30623 = NOVALUE;
    int _30622 = NOVALUE;
    int _30619 = NOVALUE;
    int _30618 = NOVALUE;
    int _30617 = NOVALUE;
    int _30615 = NOVALUE;
    int _30614 = NOVALUE;
    int _30613 = NOVALUE;
    int _30612 = NOVALUE;
    int _30611 = NOVALUE;
    int _30609 = NOVALUE;
    int _30608 = NOVALUE;
    int _30607 = NOVALUE;
    int _30605 = NOVALUE;
    int _30604 = NOVALUE;
    int _30602 = NOVALUE;
    int _30599 = NOVALUE;
    int _30597 = NOVALUE;
    int _30595 = NOVALUE;
    int _30592 = NOVALUE;
    int _30587 = NOVALUE;
    int _30580 = NOVALUE;
    int _30579 = NOVALUE;
    int _30577 = NOVALUE;
    int _30574 = NOVALUE;
    int _30567 = NOVALUE;
    int _30564 = NOVALUE;
    int _30563 = NOVALUE;
    int _30561 = NOVALUE;
    int _30557 = NOVALUE;
    int _30556 = NOVALUE;
    int _30555 = NOVALUE;
    int _30554 = NOVALUE;
    int _30553 = NOVALUE;
    int _30552 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_type_ptr_59977)) {
        _1 = (long)(DBL_PTR(_type_ptr_59977)->dbl);
        if (UNIQUE(DBL_PTR(_type_ptr_59977)) && (DBL_PTR(_type_ptr_59977)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_type_ptr_59977);
        _type_ptr_59977 = _1;
    }

    /** 	object tsym*/

    /** 	object prevtok = 0*/
    DeRef(_prevtok_59983);
    _prevtok_59983 = 0;

    /** 	symtab_index sym, valsym, prevsym = 0, deltasym = 0*/
    _prevsym_59987 = 0;
    _deltasym_59988 = 0;

    /** 	integer h, count = 0*/
    _count_59990 = 0;

    /** 	atom val = 1, usedval*/
    _val_59991 = 1;

    /** 	integer deltafunc = '+'*/
    _deltafunc_59993 = 43;

    /** 	atom delta = 1*/
    DeRef(_delta_59994);
    _delta_59994 = 1;

    /** 	new_symbols = {}*/
    RefDS(_22682);
    DeRefi(_new_symbols_59979);
    _new_symbols_59979 = _22682;

    /** 	integer is_fwd_ref = 0*/
    _is_fwd_ref_59995 = 0;

    /** 	if type_ptr > 0 and SymTab[type_ptr][S_SCOPE] = SC_UNDEFINED then*/
    _30552 = (_type_ptr_59977 > 0);
    if (_30552 == 0) {
        goto L1; // [62] 117
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30554 = (int)*(((s1_ptr)_2)->base + _type_ptr_59977);
    _2 = (int)SEQ_PTR(_30554);
    _30555 = (int)*(((s1_ptr)_2)->base + 4);
    _30554 = NOVALUE;
    if (IS_ATOM_INT(_30555)) {
        _30556 = (_30555 == 9);
    }
    else {
        _30556 = binary_op(EQUALS, _30555, 9);
    }
    _30555 = NOVALUE;
    if (_30556 == 0) {
        DeRef(_30556);
        _30556 = NOVALUE;
        goto L1; // [85] 117
    }
    else {
        if (!IS_ATOM_INT(_30556) && DBL_PTR(_30556)->dbl == 0.0){
            DeRef(_30556);
            _30556 = NOVALUE;
            goto L1; // [85] 117
        }
        DeRef(_30556);
        _30556 = NOVALUE;
    }
    DeRef(_30556);
    _30556 = NOVALUE;

    /** 		is_fwd_ref = 1*/
    _is_fwd_ref_59995 = 1;

    /** 		Hide(type_ptr)*/
    _52Hide(_type_ptr_59977);

    /** 		type_ptr = -new_forward_reference( TYPE, type_ptr )*/
    DeRef(_32365);
    _32365 = 504;
    _30557 = _29new_forward_reference(504, _type_ptr_59977, 504);
    _32365 = NOVALUE;
    if (IS_ATOM_INT(_30557)) {
        if ((unsigned long)_30557 == 0xC0000000)
        _type_ptr_59977 = (int)NewDouble((double)-0xC0000000);
        else
        _type_ptr_59977 = - _30557;
    }
    else {
        _type_ptr_59977 = unary_op(UMINUS, _30557);
    }
    DeRef(_30557);
    _30557 = NOVALUE;
    if (!IS_ATOM_INT(_type_ptr_59977)) {
        _1 = (long)(DBL_PTR(_type_ptr_59977)->dbl);
        if (UNIQUE(DBL_PTR(_type_ptr_59977)) && (DBL_PTR(_type_ptr_59977)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_type_ptr_59977);
        _type_ptr_59977 = _1;
    }
L1: 

    /** 	if type_ptr = -1 then*/
    if (_type_ptr_59977 != -1)
    goto L2; // [119] 495

    /** 		sequence ptok = next_token()*/
    _0 = _ptok_60012;
    _ptok_60012 = _30next_token();
    DeRef(_0);

    /** 		if ptok[T_ID] = TYPE_DECL then*/
    _2 = (int)SEQ_PTR(_ptok_60012);
    _30561 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30561, 416)){
        _30561 = NOVALUE;
        goto L3; // [140] 183
    }
    _30561 = NOVALUE;

    /** 			putback(keyfind("enum",-1))*/
    RefDS(_27041);
    DeRef(_32363);
    _32363 = _27041;
    _32364 = _52hashfn(_32363);
    _32363 = NOVALUE;
    RefDS(_27041);
    _30563 = _52keyfind(_27041, -1, _25current_file_no_12262, 0, _32364);
    _32364 = NOVALUE;
    _30putback(_30563);
    _30563 = NOVALUE;

    /** 			SubProg(TYPE_DECL, scope)*/
    _30SubProg(416, _scope_59978);

    /** 			return {}*/
    RefDS(_22682);
    DeRefDS(_ptok_60012);
    DeRefi(_new_symbols_59979);
    DeRef(_tok_59981);
    DeRef(_prevtok_59983);
    DeRef(_delta_59994);
    DeRef(_30552);
    _30552 = NOVALUE;
    return _22682;
    goto L4; // [180] 494
L3: 

    /** 		elsif ptok[T_ID] = BY then*/
    _2 = (int)SEQ_PTR(_ptok_60012);
    _30564 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30564, 404)){
        _30564 = NOVALUE;
        goto L5; // [193] 456
    }
    _30564 = NOVALUE;

    /** 			integer negate = 0*/
    _negate_60028 = 0;

    /** 			ptok = next_token()*/
    _0 = _ptok_60012;
    _ptok_60012 = _30next_token();
    DeRefDS(_0);

    /** 			switch ptok[T_ID] do*/
    _2 = (int)SEQ_PTR(_ptok_60012);
    _30567 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_30567) ){
        goto L6; // [217] 296
    }
    if(!IS_ATOM_INT(_30567)){
        if( (DBL_PTR(_30567)->dbl != (double) ((int) DBL_PTR(_30567)->dbl) ) ){
            goto L6; // [217] 296
        }
        _0 = (int) DBL_PTR(_30567)->dbl;
    }
    else {
        _0 = _30567;
    };
    _30567 = NOVALUE;
    switch ( _0 ){ 

        /** 				case reserved:MULTIPLY then*/
        case 13:

        /** 					deltafunc = '*'*/
        _deltafunc_59993 = 42;

        /** 					ptok = next_token()*/
        _0 = _ptok_60012;
        _ptok_60012 = _30next_token();
        DeRef(_0);
        goto L7; // [238] 304

        /** 				case reserved:DIVIDE then*/
        case 14:

        /** 					deltafunc = '/'*/
        _deltafunc_59993 = 47;

        /** 					ptok = next_token()*/
        _0 = _ptok_60012;
        _ptok_60012 = _30next_token();
        DeRef(_0);
        goto L7; // [256] 304

        /** 				case MINUS then*/
        case 10:

        /** 					deltafunc = '-'*/
        _deltafunc_59993 = 45;

        /** 					ptok = next_token()*/
        _0 = _ptok_60012;
        _ptok_60012 = _30next_token();
        DeRef(_0);
        goto L7; // [274] 304

        /** 				case PLUS then*/
        case 11:

        /** 					deltafunc = '+'*/
        _deltafunc_59993 = 43;

        /** 					ptok = next_token()*/
        _0 = _ptok_60012;
        _ptok_60012 = _30next_token();
        DeRef(_0);
        goto L7; // [292] 304

        /** 				case else*/
        default:
L6: 

        /** 					deltafunc = '+'*/
        _deltafunc_59993 = 43;
    ;}L7: 

    /** 			if ptok[T_ID] = MINUS then*/
    _2 = (int)SEQ_PTR(_ptok_60012);
    _30574 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30574, 10)){
        _30574 = NOVALUE;
        goto L8; // [314] 331
    }
    _30574 = NOVALUE;

    /** 				negate = 1*/
    _negate_60028 = 1;

    /** 				ptok = next_token()*/
    _0 = _ptok_60012;
    _ptok_60012 = _30next_token();
    DeRefDS(_0);
L8: 

    /** 			if ptok[T_ID] != ATOM then*/
    _2 = (int)SEQ_PTR(_ptok_60012);
    _30577 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _30577, 502)){
        _30577 = NOVALUE;
        goto L9; // [341] 353
    }
    _30577 = NOVALUE;

    /** 				CompileErr( 344 )*/
    RefDS(_22682);
    _43CompileErr(344, _22682, 0);
L9: 

    /** 			delta = SymTab[ptok[T_SYM]][S_OBJ]*/
    _2 = (int)SEQ_PTR(_ptok_60012);
    _30579 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_30579)){
        _30580 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_30579)->dbl));
    }
    else{
        _30580 = (int)*(((s1_ptr)_2)->base + _30579);
    }
    DeRef(_delta_59994);
    _2 = (int)SEQ_PTR(_30580);
    _delta_59994 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_delta_59994);
    _30580 = NOVALUE;

    /** 			if negate then*/
    if (_negate_60028 == 0)
    {
        goto LA; // [375] 384
    }
    else{
    }

    /** 				delta = -delta*/
    _0 = _delta_59994;
    if (IS_ATOM_INT(_delta_59994)) {
        if ((unsigned long)_delta_59994 == 0xC0000000)
        _delta_59994 = (int)NewDouble((double)-0xC0000000);
        else
        _delta_59994 = - _delta_59994;
    }
    else {
        _delta_59994 = unary_op(UMINUS, _delta_59994);
    }
    DeRef(_0);
LA: 

    /** 			switch deltafunc do*/
    _0 = _deltafunc_59993;
    switch ( _0 ){ 

        /** 				case '/' then*/
        case 47:

        /** 					delta = 1 / delta*/
        _0 = _delta_59994;
        if (IS_ATOM_INT(_delta_59994)) {
            _delta_59994 = (1 % _delta_59994) ? NewDouble((double)1 / _delta_59994) : (1 / _delta_59994);
        }
        else {
            _delta_59994 = NewDouble((double)1 / DBL_PTR(_delta_59994)->dbl);
        }
        DeRef(_0);

        /** 					deltafunc = '*'*/
        _deltafunc_59993 = 42;
        goto LB; // [406] 423

        /** 				case '-' then*/
        case 45:

        /** 					delta = -delta*/
        _0 = _delta_59994;
        if (IS_ATOM_INT(_delta_59994)) {
            if ((unsigned long)_delta_59994 == 0xC0000000)
            _delta_59994 = (int)NewDouble((double)-0xC0000000);
            else
            _delta_59994 = - _delta_59994;
        }
        else {
            _delta_59994 = unary_op(UMINUS, _delta_59994);
        }
        DeRef(_0);

        /** 					deltafunc = '+'*/
        _deltafunc_59993 = 43;
    ;}LB: 

    /** 			if integer(delta) then*/
    if (IS_ATOM_INT(_delta_59994))
    _30587 = 1;
    else if (IS_ATOM_DBL(_delta_59994))
    _30587 = IS_ATOM_INT(DoubleToInt(_delta_59994));
    else
    _30587 = 0;
    if (_30587 == 0)
    {
        _30587 = NOVALUE;
        goto LC; // [428] 442
    }
    else{
        _30587 = NOVALUE;
    }

    /** 				deltasym = NewIntSym(delta)*/
    Ref(_delta_59994);
    _deltasym_59988 = _52NewIntSym(_delta_59994);
    if (!IS_ATOM_INT(_deltasym_59988)) {
        _1 = (long)(DBL_PTR(_deltasym_59988)->dbl);
        if (UNIQUE(DBL_PTR(_deltasym_59988)) && (DBL_PTR(_deltasym_59988)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_deltasym_59988);
        _deltasym_59988 = _1;
    }
    goto LD; // [439] 451
LC: 

    /** 				deltasym = NewDoubleSym(delta)*/
    Ref(_delta_59994);
    _deltasym_59988 = _52NewDoubleSym(_delta_59994);
    if (!IS_ATOM_INT(_deltasym_59988)) {
        _1 = (long)(DBL_PTR(_deltasym_59988)->dbl);
        if (UNIQUE(DBL_PTR(_deltasym_59988)) && (DBL_PTR(_deltasym_59988)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_deltasym_59988);
        _deltasym_59988 = _1;
    }
LD: 
    goto L4; // [453] 494
L5: 

    /** 			deltasym = NewIntSym(1)*/
    _deltasym_59988 = _52NewIntSym(1);
    if (!IS_ATOM_INT(_deltasym_59988)) {
        _1 = (long)(DBL_PTR(_deltasym_59988)->dbl);
        if (UNIQUE(DBL_PTR(_deltasym_59988)) && (DBL_PTR(_deltasym_59988)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_deltasym_59988);
        _deltasym_59988 = _1;
    }

    /** 			if deltasym > 0 then*/
    if (_deltasym_59988 <= 0)
    goto LE; // [466] 488

    /** 				SymTab[deltasym][S_USAGE] = U_READ*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_deltasym_59988 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _30592 = NOVALUE;
LE: 

    /** 			putback(ptok)*/
    RefDS(_ptok_60012);
    _30putback(_ptok_60012);
L4: 
L2: 
    DeRef(_ptok_60012);
    _ptok_60012 = NOVALUE;

    /** 	valsym = 0*/
    _valsym_59986 = 0;

    /** 	while TRUE do*/
LF: 
    if (_5TRUE_244 == 0)
    {
        goto L10; // [511] 2131
    }
    else{
    }

    /** 		tok = next_token()*/
    _0 = _tok_59981;
    _tok_59981 = _30next_token();
    DeRef(_0);

    /** 		if tok[T_ID] = DOLLAR then*/
    _2 = (int)SEQ_PTR(_tok_59981);
    _30595 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30595, -22)){
        _30595 = NOVALUE;
        goto L11; // [529] 568
    }
    _30595 = NOVALUE;

    /** 			if not equal(prevtok, 0) then*/
    if (_prevtok_59983 == 0)
    _30597 = 1;
    else if (IS_ATOM_INT(_prevtok_59983) && IS_ATOM_INT(0))
    _30597 = 0;
    else
    _30597 = (compare(_prevtok_59983, 0) == 0);
    if (_30597 != 0)
    goto L12; // [539] 567
    _30597 = NOVALUE;

    /** 				if prevtok[T_ID] = COMMA then*/
    _2 = (int)SEQ_PTR(_prevtok_59983);
    _30599 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30599, -30)){
        _30599 = NOVALUE;
        goto L13; // [552] 566
    }
    _30599 = NOVALUE;

    /** 					tok = next_token()*/
    _0 = _tok_59981;
    _tok_59981 = _30next_token();
    DeRef(_0);

    /** 					exit*/
    goto L10; // [563] 2131
L13: 
L12: 
L11: 

    /** 		if tok[T_ID] = END_OF_FILE then*/
    _2 = (int)SEQ_PTR(_tok_59981);
    _30602 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30602, -21)){
        _30602 = NOVALUE;
        goto L14; // [578] 590
    }
    _30602 = NOVALUE;

    /** 			CompileErr( 32 )*/
    RefDS(_22682);
    _43CompileErr(32, _22682, 0);
L14: 

    /** 		if not find(tok[T_ID], ADDR_TOKS) then*/
    _2 = (int)SEQ_PTR(_tok_59981);
    _30604 = (int)*(((s1_ptr)_2)->base + 1);
    _30605 = find_from(_30604, _28ADDR_TOKS_11861, 1);
    _30604 = NOVALUE;
    if (_30605 != 0)
    goto L15; // [605] 630
    _30605 = NOVALUE;

    /** 			CompileErr(25, {find_category(tok[T_ID])} )*/
    _2 = (int)SEQ_PTR(_tok_59981);
    _30607 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_30607);
    _30608 = _63find_category(_30607);
    _30607 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _30608;
    _30609 = MAKE_SEQ(_1);
    _30608 = NOVALUE;
    _43CompileErr(25, _30609, 0);
    _30609 = NOVALUE;
L15: 

    /** 		sym = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_59981);
    _sym_59985 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_sym_59985)){
        _sym_59985 = (long)DBL_PTR(_sym_59985)->dbl;
    }

    /** 		DefinedYet(sym)*/
    _52DefinedYet(_sym_59985);

    /** 		if find(SymTab[sym][S_SCOPE], {SC_GLOBAL, SC_PREDEF, SC_PUBLIC, SC_EXPORT}) then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30611 = (int)*(((s1_ptr)_2)->base + _sym_59985);
    _2 = (int)SEQ_PTR(_30611);
    _30612 = (int)*(((s1_ptr)_2)->base + 4);
    _30611 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 6;
    *((int *)(_2+8)) = 7;
    *((int *)(_2+12)) = 13;
    *((int *)(_2+16)) = 11;
    _30613 = MAKE_SEQ(_1);
    _30614 = find_from(_30612, _30613, 1);
    _30612 = NOVALUE;
    DeRefDS(_30613);
    _30613 = NOVALUE;
    if (_30614 == 0)
    {
        _30614 = NOVALUE;
        goto L16; // [679] 741
    }
    else{
        _30614 = NOVALUE;
    }

    /** 			h = SymTab[sym][S_HASHVAL]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30615 = (int)*(((s1_ptr)_2)->base + _sym_59985);
    _2 = (int)SEQ_PTR(_30615);
    _h_59989 = (int)*(((s1_ptr)_2)->base + 11);
    if (!IS_ATOM_INT(_h_59989)){
        _h_59989 = (long)DBL_PTR(_h_59989)->dbl;
    }
    _30615 = NOVALUE;

    /** 			sym = NewEntry(SymTab[sym][S_NAME], 0, 0, VARIABLE, h, buckets[h], 0)*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30617 = (int)*(((s1_ptr)_2)->base + _sym_59985);
    _2 = (int)SEQ_PTR(_30617);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _30618 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _30618 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _30617 = NOVALUE;
    _2 = (int)SEQ_PTR(_52buckets_47095);
    _30619 = (int)*(((s1_ptr)_2)->base + _h_59989);
    Ref(_30618);
    Ref(_30619);
    _sym_59985 = _52NewEntry(_30618, 0, 0, -100, _h_59989, _30619, 0);
    _30618 = NOVALUE;
    _30619 = NOVALUE;
    if (!IS_ATOM_INT(_sym_59985)) {
        _1 = (long)(DBL_PTR(_sym_59985)->dbl);
        if (UNIQUE(DBL_PTR(_sym_59985)) && (DBL_PTR(_sym_59985)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_59985);
        _sym_59985 = _1;
    }

    /** 			buckets[h] = sym*/
    _2 = (int)SEQ_PTR(_52buckets_47095);
    _2 = (int)(((s1_ptr)_2)->base + _h_59989);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_59985;
    DeRef(_1);
L16: 

    /** 		new_symbols = append(new_symbols, sym)*/
    Append(&_new_symbols_59979, _new_symbols_59979, _sym_59985);

    /** 		Block_var( sym )*/
    _66Block_var(_sym_59985);

    /** 		if SymTab[sym][S_SCOPE] = SC_UNDEFINED and SymTab[sym][S_FILE_NO] != current_file_no then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30622 = (int)*(((s1_ptr)_2)->base + _sym_59985);
    _2 = (int)SEQ_PTR(_30622);
    _30623 = (int)*(((s1_ptr)_2)->base + 4);
    _30622 = NOVALUE;
    if (IS_ATOM_INT(_30623)) {
        _30624 = (_30623 == 9);
    }
    else {
        _30624 = binary_op(EQUALS, _30623, 9);
    }
    _30623 = NOVALUE;
    if (IS_ATOM_INT(_30624)) {
        if (_30624 == 0) {
            goto L17; // [772] 816
        }
    }
    else {
        if (DBL_PTR(_30624)->dbl == 0.0) {
            goto L17; // [772] 816
        }
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30626 = (int)*(((s1_ptr)_2)->base + _sym_59985);
    _2 = (int)SEQ_PTR(_30626);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _30627 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _30627 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _30626 = NOVALUE;
    if (IS_ATOM_INT(_30627)) {
        _30628 = (_30627 != _25current_file_no_12262);
    }
    else {
        _30628 = binary_op(NOTEQ, _30627, _25current_file_no_12262);
    }
    _30627 = NOVALUE;
    if (_30628 == 0) {
        DeRef(_30628);
        _30628 = NOVALUE;
        goto L17; // [795] 816
    }
    else {
        if (!IS_ATOM_INT(_30628) && DBL_PTR(_30628)->dbl == 0.0){
            DeRef(_30628);
            _30628 = NOVALUE;
            goto L17; // [795] 816
        }
        DeRef(_30628);
        _30628 = NOVALUE;
    }
    DeRef(_30628);
    _30628 = NOVALUE;

    /** 			SymTab[sym][S_FILE_NO] = current_file_no*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_FILE_NO_11909))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    _1 = *(int *)_2;
    *(int *)_2 = _25current_file_no_12262;
    DeRef(_1);
    _30629 = NOVALUE;
L17: 

    /** 		SymTab[sym][S_SCOPE] = scope*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _scope_59978;
    DeRef(_1);
    _30631 = NOVALUE;

    /** 		if type_ptr = 0 then*/
    if (_type_ptr_59977 != 0)
    goto L18; // [833] 1168

    /** 			SymTab[sym][S_MODE] = M_CONSTANT*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _30634 = NOVALUE;

    /** 			buckets[SymTab[sym][S_HASHVAL]] = SymTab[sym][S_SAMEHASH]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30636 = (int)*(((s1_ptr)_2)->base + _sym_59985);
    _2 = (int)SEQ_PTR(_30636);
    _30637 = (int)*(((s1_ptr)_2)->base + 11);
    _30636 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30638 = (int)*(((s1_ptr)_2)->base + _sym_59985);
    _2 = (int)SEQ_PTR(_30638);
    _30639 = (int)*(((s1_ptr)_2)->base + 9);
    _30638 = NOVALUE;
    Ref(_30639);
    _2 = (int)SEQ_PTR(_52buckets_47095);
    if (!IS_ATOM_INT(_30637))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_30637)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _30637);
    _1 = *(int *)_2;
    *(int *)_2 = _30639;
    if( _1 != _30639 ){
        DeRef(_1);
    }
    _30639 = NOVALUE;

    /** 			tok_match(EQUALS)*/
    _30tok_match(3, 0);

    /** 			StartSourceLine(FALSE, , COVERAGE_OVERRIDE)*/
    _37StartSourceLine(_5FALSE_242, 0, 3);

    /** 			emit_opnd(sym)*/
    _37emit_opnd(_sym_59985);

    /** 			Expr()  -- no new symbols can be defined in here*/
    _30Expr();

    /** 			buckets[SymTab[sym][S_HASHVAL]] = sym*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30640 = (int)*(((s1_ptr)_2)->base + _sym_59985);
    _2 = (int)SEQ_PTR(_30640);
    _30641 = (int)*(((s1_ptr)_2)->base + 11);
    _30640 = NOVALUE;
    _2 = (int)SEQ_PTR(_52buckets_47095);
    if (!IS_ATOM_INT(_30641))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_30641)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _30641);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_59985;
    DeRef(_1);

    /** 			SymTab[sym][S_USAGE] = U_WRITTEN*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _30642 = NOVALUE;

    /** 			if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L19; // [955] 993
    }
    else{
    }

    /** 				SymTab[sym][S_GTYPE] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);
    _30644 = NOVALUE;

    /** 				SymTab[sym][S_OBJ] = NOVALUE -- distinguish from literals*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);
    _30646 = NOVALUE;
L19: 

    /** 			valsym = Top()*/
    _valsym_59986 = _37Top();
    if (!IS_ATOM_INT(_valsym_59986)) {
        _1 = (long)(DBL_PTR(_valsym_59986)->dbl);
        if (UNIQUE(DBL_PTR(_valsym_59986)) && (DBL_PTR(_valsym_59986)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_valsym_59986);
        _valsym_59986 = _1;
    }

    /** 			if valsym > 0 and compare( SymTab[valsym][S_OBJ], NOVALUE ) then*/
    _30649 = (_valsym_59986 > 0);
    if (_30649 == 0) {
        goto L1A; // [1006] 1047
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30651 = (int)*(((s1_ptr)_2)->base + _valsym_59986);
    _2 = (int)SEQ_PTR(_30651);
    _30652 = (int)*(((s1_ptr)_2)->base + 1);
    _30651 = NOVALUE;
    if (IS_ATOM_INT(_30652) && IS_ATOM_INT(_25NOVALUE_12115)){
        _30653 = (_30652 < _25NOVALUE_12115) ? -1 : (_30652 > _25NOVALUE_12115);
    }
    else{
        _30653 = compare(_30652, _25NOVALUE_12115);
    }
    _30652 = NOVALUE;
    if (_30653 == 0)
    {
        _30653 = NOVALUE;
        goto L1A; // [1029] 1047
    }
    else{
        _30653 = NOVALUE;
    }

    /** 				Assign_Constant( sym )*/
    _30Assign_Constant(_sym_59985);

    /** 				sym = Pop()*/
    _sym_59985 = _37Pop();
    if (!IS_ATOM_INT(_sym_59985)) {
        _1 = (long)(DBL_PTR(_sym_59985)->dbl);
        if (UNIQUE(DBL_PTR(_sym_59985)) && (DBL_PTR(_sym_59985)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_59985);
        _sym_59985 = _1;
    }
    goto L1B; // [1044] 2090
L1A: 

    /** 				emit_op(ASSIGN)*/
    _37emit_op(18);

    /** 				if Last_op() = ASSIGN then*/
    _30655 = _37Last_op();
    if (binary_op_a(NOTEQ, _30655, 18)){
        DeRef(_30655);
        _30655 = NOVALUE;
        goto L1C; // [1061] 1075
    }
    DeRef(_30655);
    _30655 = NOVALUE;

    /** 					valsym = get_assigned_sym()*/
    _valsym_59986 = _30get_assigned_sym();
    if (!IS_ATOM_INT(_valsym_59986)) {
        _1 = (long)(DBL_PTR(_valsym_59986)->dbl);
        if (UNIQUE(DBL_PTR(_valsym_59986)) && (DBL_PTR(_valsym_59986)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_valsym_59986);
        _valsym_59986 = _1;
    }
    goto L1D; // [1072] 1083
L1C: 

    /** 					valsym = -1*/
    _valsym_59986 = -1;
L1D: 

    /** 				if valsym > 0 and compare( SymTab[valsym][S_OBJ], NOVALUE ) then*/
    _30658 = (_valsym_59986 > 0);
    if (_30658 == 0) {
        goto L1E; // [1089] 1131
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30660 = (int)*(((s1_ptr)_2)->base + _valsym_59986);
    _2 = (int)SEQ_PTR(_30660);
    _30661 = (int)*(((s1_ptr)_2)->base + 1);
    _30660 = NOVALUE;
    if (IS_ATOM_INT(_30661) && IS_ATOM_INT(_25NOVALUE_12115)){
        _30662 = (_30661 < _25NOVALUE_12115) ? -1 : (_30661 > _25NOVALUE_12115);
    }
    else{
        _30662 = compare(_30661, _25NOVALUE_12115);
    }
    _30661 = NOVALUE;
    if (_30662 == 0)
    {
        _30662 = NOVALUE;
        goto L1E; // [1112] 1131
    }
    else{
        _30662 = NOVALUE;
    }

    /** 					SymTab[sym][S_CODE] = valsym*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_CODE_11925))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_CODE_11925);
    _1 = *(int *)_2;
    *(int *)_2 = _valsym_59986;
    DeRef(_1);
    _30663 = NOVALUE;
L1E: 

    /** 				if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L1B; // [1135] 2090
    }
    else{
    }

    /** 					count += 1*/
    _count_59990 = _count_59990 + 1;

    /** 					if count = 10 then*/
    if (_count_59990 != 10)
    goto L1B; // [1146] 2090

    /** 						count = 0*/
    _count_59990 = 0;

    /** 						emit_op( RETURNT )*/
    _37emit_op(34);
    goto L1B; // [1165] 2090
L18: 

    /** 		elsif type_ptr = -1 and not is_fwd_ref then*/
    _30667 = (_type_ptr_59977 == -1);
    if (_30667 == 0) {
        goto L1F; // [1174] 1917
    }
    _30669 = (_is_fwd_ref_59995 == 0);
    if (_30669 == 0)
    {
        DeRef(_30669);
        _30669 = NOVALUE;
        goto L1F; // [1182] 1917
    }
    else{
        DeRef(_30669);
        _30669 = NOVALUE;
    }

    /** 			StartSourceLine(FALSE, , COVERAGE_OVERRIDE )*/
    _37StartSourceLine(_5FALSE_242, 0, 3);

    /** 			SymTab[sym][S_MODE] = M_CONSTANT*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _30670 = NOVALUE;

    /** 			buckets[SymTab[sym][S_HASHVAL]] = SymTab[sym][S_SAMEHASH]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30672 = (int)*(((s1_ptr)_2)->base + _sym_59985);
    _2 = (int)SEQ_PTR(_30672);
    _30673 = (int)*(((s1_ptr)_2)->base + 11);
    _30672 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30674 = (int)*(((s1_ptr)_2)->base + _sym_59985);
    _2 = (int)SEQ_PTR(_30674);
    _30675 = (int)*(((s1_ptr)_2)->base + 9);
    _30674 = NOVALUE;
    Ref(_30675);
    _2 = (int)SEQ_PTR(_52buckets_47095);
    if (!IS_ATOM_INT(_30673))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_30673)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _30673);
    _1 = *(int *)_2;
    *(int *)_2 = _30675;
    if( _1 != _30675 ){
        DeRef(_1);
    }
    _30675 = NOVALUE;

    /** 			tok = next_token()*/
    _0 = _tok_59981;
    _tok_59981 = _30next_token();
    DeRef(_0);

    /** 			StartSourceLine(FALSE, , COVERAGE_OVERRIDE)*/
    _37StartSourceLine(_5FALSE_242, 0, 3);

    /** 			emit_opnd(sym)*/
    _37emit_opnd(_sym_59985);

    /** 			if tok[T_ID] = EQUALS then*/
    _2 = (int)SEQ_PTR(_tok_59981);
    _30677 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30677, 3)){
        _30677 = NOVALUE;
        goto L20; // [1276] 1521
    }
    _30677 = NOVALUE;

    /** 				Expr()*/
    _30Expr();

    /** 				buckets[SymTab[sym][S_HASHVAL]] = sym*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30679 = (int)*(((s1_ptr)_2)->base + _sym_59985);
    _2 = (int)SEQ_PTR(_30679);
    _30680 = (int)*(((s1_ptr)_2)->base + 11);
    _30679 = NOVALUE;
    _2 = (int)SEQ_PTR(_52buckets_47095);
    if (!IS_ATOM_INT(_30680))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_30680)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _30680);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_59985;
    DeRef(_1);

    /** 				SymTab[sym][S_USAGE] = U_WRITTEN*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _30681 = NOVALUE;

    /** 				valsym = Top()*/
    _valsym_59986 = _37Top();
    if (!IS_ATOM_INT(_valsym_59986)) {
        _1 = (long)(DBL_PTR(_valsym_59986)->dbl);
        if (UNIQUE(DBL_PTR(_valsym_59986)) && (DBL_PTR(_valsym_59986)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_valsym_59986);
        _valsym_59986 = _1;
    }

    /** 				if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L21; // [1332] 1370
    }
    else{
    }

    /** 					SymTab[sym][S_GTYPE] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);
    _30684 = NOVALUE;

    /** 					SymTab[sym][S_OBJ] = NOVALUE     -- distinguish from literals*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);
    _30686 = NOVALUE;
L21: 

    /** 				valsym = Top()*/
    _valsym_59986 = _37Top();
    if (!IS_ATOM_INT(_valsym_59986)) {
        _1 = (long)(DBL_PTR(_valsym_59986)->dbl);
        if (UNIQUE(DBL_PTR(_valsym_59986)) && (DBL_PTR(_valsym_59986)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_valsym_59986);
        _valsym_59986 = _1;
    }

    /** 				emit_op(ASSIGN)*/
    _37emit_op(18);

    /** 				if Last_op() = ASSIGN then*/
    _30689 = _37Last_op();
    if (binary_op_a(NOTEQ, _30689, 18)){
        DeRef(_30689);
        _30689 = NOVALUE;
        goto L22; // [1391] 1405
    }
    DeRef(_30689);
    _30689 = NOVALUE;

    /** 					valsym = get_assigned_sym()*/
    _valsym_59986 = _30get_assigned_sym();
    if (!IS_ATOM_INT(_valsym_59986)) {
        _1 = (long)(DBL_PTR(_valsym_59986)->dbl);
        if (UNIQUE(DBL_PTR(_valsym_59986)) && (DBL_PTR(_valsym_59986)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_valsym_59986);
        _valsym_59986 = _1;
    }
    goto L23; // [1402] 1413
L22: 

    /** 					valsym = -1*/
    _valsym_59986 = -1;
L23: 

    /** 				if valsym > 0 and compare( SymTab[valsym][S_OBJ], NOVALUE ) then*/
    _30692 = (_valsym_59986 > 0);
    if (_30692 == 0) {
        goto L24; // [1419] 1461
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30694 = (int)*(((s1_ptr)_2)->base + _valsym_59986);
    _2 = (int)SEQ_PTR(_30694);
    _30695 = (int)*(((s1_ptr)_2)->base + 1);
    _30694 = NOVALUE;
    if (IS_ATOM_INT(_30695) && IS_ATOM_INT(_25NOVALUE_12115)){
        _30696 = (_30695 < _25NOVALUE_12115) ? -1 : (_30695 > _25NOVALUE_12115);
    }
    else{
        _30696 = compare(_30695, _25NOVALUE_12115);
    }
    _30695 = NOVALUE;
    if (_30696 == 0)
    {
        _30696 = NOVALUE;
        goto L24; // [1442] 1461
    }
    else{
        _30696 = NOVALUE;
    }

    /** 					SymTab[sym][S_CODE] = valsym*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_CODE_11925))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_CODE_11925);
    _1 = *(int *)_2;
    *(int *)_2 = _valsym_59986;
    DeRef(_1);
    _30697 = NOVALUE;
L24: 

    /** 				if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L25; // [1465] 1816
    }
    else{
    }

    /** 					count += 1*/
    _count_59990 = _count_59990 + 1;

    /** 					if count = 10 then*/
    if (_count_59990 != 10)
    goto L26; // [1476] 1493

    /** 						count = 0*/
    _count_59990 = 0;

    /** 						emit_op( RETURNT )*/
    _37emit_op(34);
L26: 

    /** 					SymTab[sym][S_USAGE] = U_READ*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _30701 = NOVALUE;

    /** 					valsym = get_assigned_sym()*/
    _valsym_59986 = _30get_assigned_sym();
    if (!IS_ATOM_INT(_valsym_59986)) {
        _1 = (long)(DBL_PTR(_valsym_59986)->dbl);
        if (UNIQUE(DBL_PTR(_valsym_59986)) && (DBL_PTR(_valsym_59986)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_valsym_59986);
        _valsym_59986 = _1;
    }
    goto L25; // [1518] 1816
L20: 

    /** 				buckets[SymTab[sym][S_HASHVAL]] = sym*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30704 = (int)*(((s1_ptr)_2)->base + _sym_59985);
    _2 = (int)SEQ_PTR(_30704);
    _30705 = (int)*(((s1_ptr)_2)->base + 11);
    _30704 = NOVALUE;
    _2 = (int)SEQ_PTR(_52buckets_47095);
    if (!IS_ATOM_INT(_30705))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_30705)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _30705);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_59985;
    DeRef(_1);

    /** 				SymTab[sym][S_USAGE] = U_WRITTEN*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _30706 = NOVALUE;

    /** 				if prevsym = 0 then*/
    if (_prevsym_59987 != 0)
    goto L27; // [1560] 1633

    /** 				    symtab_index one = NewIntSym(1)*/
    _one_60368 = _52NewIntSym(1);
    if (!IS_ATOM_INT(_one_60368)) {
        _1 = (long)(DBL_PTR(_one_60368)->dbl);
        if (UNIQUE(DBL_PTR(_one_60368)) && (DBL_PTR(_one_60368)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_one_60368);
        _one_60368 = _1;
    }

    /** 				    SymTab[one][S_USAGE] = U_USED*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_one_60368 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 3;
    DeRef(_1);
    _30710 = NOVALUE;

    /** 					emit_opnd(sym)*/
    _37emit_opnd(_sym_59985);

    /** 					emit_opnd(one)*/
    _37emit_opnd(_one_60368);

    /** 					SymTab[sym][S_CODE] = one*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_CODE_11925))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_CODE_11925);
    _1 = *(int *)_2;
    *(int *)_2 = _one_60368;
    DeRef(_1);
    _30712 = NOVALUE;

    /** 					emit_op(ASSIGN)*/
    _37emit_op(18);

    /** 					valsym = Top()*/
    _valsym_59986 = _37Top();
    if (!IS_ATOM_INT(_valsym_59986)) {
        _1 = (long)(DBL_PTR(_valsym_59986)->dbl);
        if (UNIQUE(DBL_PTR(_valsym_59986)) && (DBL_PTR(_valsym_59986)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_valsym_59986);
        _valsym_59986 = _1;
    }
    goto L28; // [1630] 1755
L27: 

    /** 					emit_opnd(deltasym)*/
    _37emit_opnd(_deltasym_59988);

    /** 					emit_opnd(prevsym)*/
    _37emit_opnd(_prevsym_59987);

    /** 					SymTab[deltasym][S_USAGE] = U_READ					*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_deltasym_59988 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _30715 = NOVALUE;

    /** 					switch deltafunc do*/
    _0 = _deltafunc_59993;
    switch ( _0 ){ 

        /** 					case '+', '-' then*/
        case 43:
        case 45:

        /** 						emit_op(reserved:PLUS)*/
        _37emit_op(11);
        goto L29; // [1678] 1690

        /** 					case else*/
        default:

        /** 						emit_op(reserved:MULTIPLY)*/
        _37emit_op(13);
    ;}L29: 

    /** 					SymTab[Top()][S_USAGE] = U_READ*/
    _30719 = _37Top();
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_30719))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_30719)->dbl));
    else
    _3 = (int)(_30719 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _30720 = NOVALUE;

    /** 					emit_opnd(sym)*/
    _37emit_opnd(_sym_59985);

    /** 					emit_opnd(Top())*/
    _30722 = _37Top();
    _37emit_opnd(_30722);
    _30722 = NOVALUE;

    /** 					emit_op(ASSIGN)*/
    _37emit_op(18);

    /** 					SymTab[sym][S_USAGE] = U_READ*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _30723 = NOVALUE;

    /** 					valsym = get_assigned_sym()*/
    _valsym_59986 = _30get_assigned_sym();
    if (!IS_ATOM_INT(_valsym_59986)) {
        _1 = (long)(DBL_PTR(_valsym_59986)->dbl);
        if (UNIQUE(DBL_PTR(_valsym_59986)) && (DBL_PTR(_valsym_59986)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_valsym_59986);
        _valsym_59986 = _1;
    }
L28: 

    /** 				if valsym > 0 and compare( SymTab[valsym][S_OBJ], NOVALUE ) then*/
    _30726 = (_valsym_59986 > 0);
    if (_30726 == 0) {
        goto L2A; // [1761] 1803
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30728 = (int)*(((s1_ptr)_2)->base + _valsym_59986);
    _2 = (int)SEQ_PTR(_30728);
    _30729 = (int)*(((s1_ptr)_2)->base + 1);
    _30728 = NOVALUE;
    if (IS_ATOM_INT(_30729) && IS_ATOM_INT(_25NOVALUE_12115)){
        _30730 = (_30729 < _25NOVALUE_12115) ? -1 : (_30729 > _25NOVALUE_12115);
    }
    else{
        _30730 = compare(_30729, _25NOVALUE_12115);
    }
    _30729 = NOVALUE;
    if (_30730 == 0)
    {
        _30730 = NOVALUE;
        goto L2A; // [1784] 1803
    }
    else{
        _30730 = NOVALUE;
    }

    /** 					SymTab[sym][S_CODE] = valsym*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_CODE_11925))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_CODE_11925);
    _1 = *(int *)_2;
    *(int *)_2 = _valsym_59986;
    DeRef(_1);
    _30731 = NOVALUE;
L2A: 

    /** 				putback(tok)*/
    Ref(_tok_59981);
    _30putback(_tok_59981);

    /** 				valsym = 0*/
    _valsym_59986 = 0;
L25: 

    /** 			emit_opnd(sym)*/
    _37emit_opnd(_sym_59985);

    /** 			op_info1 = sym*/
    _37op_info1_51268 = _sym_59985;

    /** 			emit_op(ATOM_CHECK)*/
    _37emit_op(101);

    /** 			buckets[SymTab[sym][S_HASHVAL]] = sym*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30733 = (int)*(((s1_ptr)_2)->base + _sym_59985);
    _2 = (int)SEQ_PTR(_30733);
    _30734 = (int)*(((s1_ptr)_2)->base + 11);
    _30733 = NOVALUE;
    _2 = (int)SEQ_PTR(_52buckets_47095);
    if (!IS_ATOM_INT(_30734))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_30734)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _30734);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_59985;
    DeRef(_1);

    /** 			SymTab[sym][S_USAGE] = U_WRITTEN*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _30735 = NOVALUE;

    /** 			if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L1B; // [1876] 2090
    }
    else{
    }

    /** 				SymTab[sym][S_GTYPE] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);
    _30737 = NOVALUE;

    /** 				SymTab[sym][S_OBJ] = NOVALUE     -- distinguish from literals*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);
    _30739 = NOVALUE;
    goto L1B; // [1914] 2090
L1F: 

    /** 			SymTab[sym][S_MODE] = M_NORMAL*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _30741 = NOVALUE;

    /** 			if type_ptr > 0 and SymTab[type_ptr][S_TOKEN] = OBJECT then*/
    _30743 = (_type_ptr_59977 > 0);
    if (_30743 == 0) {
        goto L2B; // [1940] 1986
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30745 = (int)*(((s1_ptr)_2)->base + _type_ptr_59977);
    _2 = (int)SEQ_PTR(_30745);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _30746 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _30746 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _30745 = NOVALUE;
    if (IS_ATOM_INT(_30746)) {
        _30747 = (_30746 == 415);
    }
    else {
        _30747 = binary_op(EQUALS, _30746, 415);
    }
    _30746 = NOVALUE;
    if (_30747 == 0) {
        DeRef(_30747);
        _30747 = NOVALUE;
        goto L2B; // [1963] 1986
    }
    else {
        if (!IS_ATOM_INT(_30747) && DBL_PTR(_30747)->dbl == 0.0){
            DeRef(_30747);
            _30747 = NOVALUE;
            goto L2B; // [1963] 1986
        }
        DeRef(_30747);
        _30747 = NOVALUE;
    }
    DeRef(_30747);
    _30747 = NOVALUE;

    /** 				SymTab[sym][S_VTYPE] = object_type*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 15);
    _1 = *(int *)_2;
    *(int *)_2 = _52object_type_47099;
    DeRef(_1);
    _30748 = NOVALUE;
    goto L2C; // [1983] 2015
L2B: 

    /** 				SymTab[sym][S_VTYPE] = type_ptr*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 15);
    _1 = *(int *)_2;
    *(int *)_2 = _type_ptr_59977;
    DeRef(_1);
    _30750 = NOVALUE;

    /** 				if type_ptr < 0 then*/
    if (_type_ptr_59977 >= 0)
    goto L2D; // [2003] 2014

    /** 					register_forward_type( sym, type_ptr )*/
    _29register_forward_type(_sym_59985, _type_ptr_59977);
L2D: 
L2C: 

    /** 			if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L2E; // [2019] 2042
    }
    else{
    }

    /** 				SymTab[sym][S_GTYPE] = CompileType(type_ptr)*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_59985 + ((s1_ptr)_2)->base);
    _30755 = _30CompileType(_type_ptr_59977);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = _30755;
    if( _1 != _30755 ){
        DeRef(_1);
    }
    _30755 = NOVALUE;
    _30753 = NOVALUE;
L2E: 

    /** 	   		tok = next_token()*/
    _0 = _tok_59981;
    _tok_59981 = _30next_token();
    DeRef(_0);

    /**    			putback(tok)*/
    Ref(_tok_59981);
    _30putback(_tok_59981);

    /** 	   		if tok[T_ID] = EQUALS then -- assign on declare*/
    _2 = (int)SEQ_PTR(_tok_59981);
    _30757 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30757, 3)){
        _30757 = NOVALUE;
        goto L2F; // [2062] 2089
    }
    _30757 = NOVALUE;

    /** 	   			StartSourceLine( FALSE, , COVERAGE_OVERRIDE )*/
    _37StartSourceLine(_5FALSE_242, 0, 3);

    /** 	   			Assignment({VARIABLE,sym})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -100;
    ((int *)_2)[2] = _sym_59985;
    _30759 = MAKE_SEQ(_1);
    _30Assignment(_30759);
    _30759 = NOVALUE;
L2F: 
L1B: 

    /** 		tok = next_token()*/
    _0 = _tok_59981;
    _tok_59981 = _30next_token();
    DeRef(_0);

    /** 		if tok[T_ID] != COMMA then*/
    _2 = (int)SEQ_PTR(_tok_59981);
    _30761 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _30761, -30)){
        _30761 = NOVALUE;
        goto L30; // [2105] 2114
    }
    _30761 = NOVALUE;

    /** 			exit*/
    goto L10; // [2111] 2131
L30: 

    /** 		prevtok = tok*/
    Ref(_tok_59981);
    DeRef(_prevtok_59983);
    _prevtok_59983 = _tok_59981;

    /** 		prevsym = sym*/
    _prevsym_59987 = _sym_59985;

    /** 	end while*/
    goto LF; // [2128] 509
L10: 

    /** 	putback(tok)*/
    Ref(_tok_59981);
    _30putback(_tok_59981);

    /** 	return new_symbols*/
    DeRef(_tok_59981);
    DeRef(_prevtok_59983);
    DeRef(_delta_59994);
    DeRef(_30552);
    _30552 = NOVALUE;
    _30579 = NOVALUE;
    _30637 = NOVALUE;
    DeRef(_30624);
    _30624 = NOVALUE;
    _30641 = NOVALUE;
    DeRef(_30649);
    _30649 = NOVALUE;
    DeRef(_30658);
    _30658 = NOVALUE;
    DeRef(_30667);
    _30667 = NOVALUE;
    _30673 = NOVALUE;
    _30680 = NOVALUE;
    DeRef(_30692);
    _30692 = NOVALUE;
    _30705 = NOVALUE;
    DeRef(_30719);
    _30719 = NOVALUE;
    DeRef(_30726);
    _30726 = NOVALUE;
    _30734 = NOVALUE;
    DeRef(_30743);
    _30743 = NOVALUE;
    return _new_symbols_59979;
    ;
}


void _30Private_declaration(int _type_sym_60513)
{
    int _tok_60515 = NOVALUE;
    int _sym_60517 = NOVALUE;
    int _32362 = NOVALUE;
    int _32361 = NOVALUE;
    int _32360 = NOVALUE;
    int _30787 = NOVALUE;
    int _30785 = NOVALUE;
    int _30783 = NOVALUE;
    int _30781 = NOVALUE;
    int _30779 = NOVALUE;
    int _30777 = NOVALUE;
    int _30775 = NOVALUE;
    int _30772 = NOVALUE;
    int _30770 = NOVALUE;
    int _30769 = NOVALUE;
    int _30766 = NOVALUE;
    int _30764 = NOVALUE;
    int _30763 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_type_sym_60513)) {
        _1 = (long)(DBL_PTR(_type_sym_60513)->dbl);
        if (UNIQUE(DBL_PTR(_type_sym_60513)) && (DBL_PTR(_type_sym_60513)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_type_sym_60513);
        _type_sym_60513 = _1;
    }

    /** 	if SymTab[type_sym][S_SCOPE] = SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30763 = (int)*(((s1_ptr)_2)->base + _type_sym_60513);
    _2 = (int)SEQ_PTR(_30763);
    _30764 = (int)*(((s1_ptr)_2)->base + 4);
    _30763 = NOVALUE;
    if (binary_op_a(NOTEQ, _30764, 9)){
        _30764 = NOVALUE;
        goto L1; // [19] 47
    }
    _30764 = NOVALUE;

    /** 		Hide( type_sym )*/
    _52Hide(_type_sym_60513);

    /** 		type_sym = -new_forward_reference( TYPE, type_sym )*/
    _32362 = 504;
    _30766 = _29new_forward_reference(504, _type_sym_60513, 504);
    _32362 = NOVALUE;
    if (IS_ATOM_INT(_30766)) {
        if ((unsigned long)_30766 == 0xC0000000)
        _type_sym_60513 = (int)NewDouble((double)-0xC0000000);
        else
        _type_sym_60513 = - _30766;
    }
    else {
        _type_sym_60513 = unary_op(UMINUS, _30766);
    }
    DeRef(_30766);
    _30766 = NOVALUE;
    if (!IS_ATOM_INT(_type_sym_60513)) {
        _1 = (long)(DBL_PTR(_type_sym_60513)->dbl);
        if (UNIQUE(DBL_PTR(_type_sym_60513)) && (DBL_PTR(_type_sym_60513)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_type_sym_60513);
        _type_sym_60513 = _1;
    }
L1: 

    /** 	while TRUE do*/
L2: 
    if (_5TRUE_244 == 0)
    {
        goto L3; // [54] 257
    }
    else{
    }

    /** 		tok = next_token()*/
    _0 = _tok_60515;
    _tok_60515 = _30next_token();
    DeRef(_0);

    /** 		if not find(tok[T_ID], ID_TOKS) then*/
    _2 = (int)SEQ_PTR(_tok_60515);
    _30769 = (int)*(((s1_ptr)_2)->base + 1);
    _30770 = find_from(_30769, _28ID_TOKS_11863, 1);
    _30769 = NOVALUE;
    if (_30770 != 0)
    goto L4; // [77] 88
    _30770 = NOVALUE;

    /** 			CompileErr(24)*/
    RefDS(_22682);
    _43CompileErr(24, _22682, 0);
L4: 

    /** 		sym = SetPrivateScope(tok[T_SYM], type_sym, param_num)*/
    _2 = (int)SEQ_PTR(_tok_60515);
    _30772 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_30772);
    _sym_60517 = _30SetPrivateScope(_30772, _type_sym_60513, _30param_num_55180);
    _30772 = NOVALUE;
    if (!IS_ATOM_INT(_sym_60517)) {
        _1 = (long)(DBL_PTR(_sym_60517)->dbl);
        if (UNIQUE(DBL_PTR(_sym_60517)) && (DBL_PTR(_sym_60517)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_60517);
        _sym_60517 = _1;
    }

    /** 		param_num += 1*/
    _30param_num_55180 = _30param_num_55180 + 1;

    /** 		if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L5; // [118] 141
    }
    else{
    }

    /** 			SymTab[sym][S_GTYPE] = CompileType(type_sym)*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_60517 + ((s1_ptr)_2)->base);
    _30777 = _30CompileType(_type_sym_60513);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = _30777;
    if( _1 != _30777 ){
        DeRef(_1);
    }
    _30777 = NOVALUE;
    _30775 = NOVALUE;
L5: 

    /**    		tok = next_token()*/
    _0 = _tok_60515;
    _tok_60515 = _30next_token();
    DeRef(_0);

    /**    		if tok[T_ID] = EQUALS then -- assign on declare*/
    _2 = (int)SEQ_PTR(_tok_60515);
    _30779 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30779, 3)){
        _30779 = NOVALUE;
        goto L6; // [156] 233
    }
    _30779 = NOVALUE;

    /** 		    putback(tok)*/
    Ref(_tok_60515);
    _30putback(_tok_60515);

    /** 		    StartSourceLine( TRUE )*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 		    Assignment({VARIABLE,sym})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -100;
    ((int *)_2)[2] = _sym_60517;
    _30781 = MAKE_SEQ(_1);
    _30Assignment(_30781);
    _30781 = NOVALUE;

    /** 			tok = next_token()*/
    _0 = _tok_60515;
    _tok_60515 = _30next_token();
    DeRef(_0);

    /** 			if tok[T_ID]=IGNORED then*/
    _2 = (int)SEQ_PTR(_tok_60515);
    _30783 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30783, 509)){
        _30783 = NOVALUE;
        goto L7; // [202] 232
    }
    _30783 = NOVALUE;

    /** 				tok = keyfind(tok[T_SYM],-1)*/
    _2 = (int)SEQ_PTR(_tok_60515);
    _30785 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_30785);
    DeRef(_32360);
    _32360 = _30785;
    _32361 = _52hashfn(_32360);
    _32360 = NOVALUE;
    Ref(_30785);
    _0 = _tok_60515;
    _tok_60515 = _52keyfind(_30785, -1, _25current_file_no_12262, 0, _32361);
    DeRef(_0);
    _30785 = NOVALUE;
    _32361 = NOVALUE;
L7: 
L6: 

    /** 		if tok[T_ID] != COMMA then*/
    _2 = (int)SEQ_PTR(_tok_60515);
    _30787 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _30787, -30)){
        _30787 = NOVALUE;
        goto L2; // [243] 52
    }
    _30787 = NOVALUE;

    /** 			exit*/
    goto L3; // [249] 257

    /** 	end while*/
    goto L2; // [254] 52
L3: 

    /** 	putback(tok)*/
    Ref(_tok_60515);
    _30putback(_tok_60515);

    /** end procedure*/
    DeRef(_tok_60515);
    return;
    ;
}


void _30Procedure_call(int _tok_60579)
{
    int _n_60580 = NOVALUE;
    int _scope_60581 = NOVALUE;
    int _opcode_60582 = NOVALUE;
    int _temp_tok_60584 = NOVALUE;
    int _s_60586 = NOVALUE;
    int _sub_60587 = NOVALUE;
    int _30823 = NOVALUE;
    int _30818 = NOVALUE;
    int _30817 = NOVALUE;
    int _30816 = NOVALUE;
    int _30815 = NOVALUE;
    int _30814 = NOVALUE;
    int _30813 = NOVALUE;
    int _30812 = NOVALUE;
    int _30811 = NOVALUE;
    int _30810 = NOVALUE;
    int _30809 = NOVALUE;
    int _30808 = NOVALUE;
    int _30806 = NOVALUE;
    int _30805 = NOVALUE;
    int _30804 = NOVALUE;
    int _30803 = NOVALUE;
    int _30802 = NOVALUE;
    int _30801 = NOVALUE;
    int _30800 = NOVALUE;
    int _30798 = NOVALUE;
    int _30797 = NOVALUE;
    int _30796 = NOVALUE;
    int _30794 = NOVALUE;
    int _30792 = NOVALUE;
    int _30790 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	tok_match(LEFT_ROUND)*/
    _30tok_match(-26, 0);

    /** 	s = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_60579);
    _s_60586 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_60586)){
        _s_60586 = (long)DBL_PTR(_s_60586)->dbl;
    }

    /** 	sub=s*/
    _sub_60587 = _s_60586;

    /** 	n = SymTab[s][S_NUM_ARGS]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30790 = (int)*(((s1_ptr)_2)->base + _s_60586);
    _2 = (int)SEQ_PTR(_30790);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _n_60580 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _n_60580 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    if (!IS_ATOM_INT(_n_60580)){
        _n_60580 = (long)DBL_PTR(_n_60580)->dbl;
    }
    _30790 = NOVALUE;

    /** 	scope = SymTab[s][S_SCOPE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30792 = (int)*(((s1_ptr)_2)->base + _s_60586);
    _2 = (int)SEQ_PTR(_30792);
    _scope_60581 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_scope_60581)){
        _scope_60581 = (long)DBL_PTR(_scope_60581)->dbl;
    }
    _30792 = NOVALUE;

    /** 	opcode = SymTab[s][S_OPCODE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30794 = (int)*(((s1_ptr)_2)->base + _s_60586);
    _2 = (int)SEQ_PTR(_30794);
    _opcode_60582 = (int)*(((s1_ptr)_2)->base + 21);
    if (!IS_ATOM_INT(_opcode_60582)){
        _opcode_60582 = (long)DBL_PTR(_opcode_60582)->dbl;
    }
    _30794 = NOVALUE;

    /** 	if SymTab[s][S_EFFECT] then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30796 = (int)*(((s1_ptr)_2)->base + _s_60586);
    _2 = (int)SEQ_PTR(_30796);
    _30797 = (int)*(((s1_ptr)_2)->base + 23);
    _30796 = NOVALUE;
    if (_30797 == 0) {
        _30797 = NOVALUE;
        goto L1; // [88] 139
    }
    else {
        if (!IS_ATOM_INT(_30797) && DBL_PTR(_30797)->dbl == 0.0){
            _30797 = NOVALUE;
            goto L1; // [88] 139
        }
        _30797 = NOVALUE;
    }
    _30797 = NOVALUE;

    /** 		SymTab[CurrentSub][S_EFFECT] = or_bits(SymTab[CurrentSub][S_EFFECT],*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25CurrentSub_12270 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30800 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_30800);
    _30801 = (int)*(((s1_ptr)_2)->base + 23);
    _30800 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30802 = (int)*(((s1_ptr)_2)->base + _s_60586);
    _2 = (int)SEQ_PTR(_30802);
    _30803 = (int)*(((s1_ptr)_2)->base + 23);
    _30802 = NOVALUE;
    if (IS_ATOM_INT(_30801) && IS_ATOM_INT(_30803)) {
        {unsigned long tu;
             tu = (unsigned long)_30801 | (unsigned long)_30803;
             _30804 = MAKE_UINT(tu);
        }
    }
    else {
        _30804 = binary_op(OR_BITS, _30801, _30803);
    }
    _30801 = NOVALUE;
    _30803 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 23);
    _1 = *(int *)_2;
    *(int *)_2 = _30804;
    if( _1 != _30804 ){
        DeRef(_1);
    }
    _30804 = NOVALUE;
    _30798 = NOVALUE;
L1: 

    /** 	ParseArgs(s)*/
    _30ParseArgs(_s_60586);

    /** 	for i=1 to n+1 do*/
    _30805 = _n_60580 + 1;
    if (_30805 > MAXINT){
        _30805 = NewDouble((double)_30805);
    }
    {
        int _i_60624;
        _i_60624 = 1;
L2: 
        if (binary_op_a(GREATER, _i_60624, _30805)){
            goto L3; // [150] 180
        }

        /** 		s = SymTab[s][S_NEXT]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _30806 = (int)*(((s1_ptr)_2)->base + _s_60586);
        _2 = (int)SEQ_PTR(_30806);
        _s_60586 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_s_60586)){
            _s_60586 = (long)DBL_PTR(_s_60586)->dbl;
        }
        _30806 = NOVALUE;

        /** 	end for*/
        _0 = _i_60624;
        if (IS_ATOM_INT(_i_60624)) {
            _i_60624 = _i_60624 + 1;
            if ((long)((unsigned long)_i_60624 +(unsigned long) HIGH_BITS) >= 0){
                _i_60624 = NewDouble((double)_i_60624);
            }
        }
        else {
            _i_60624 = binary_op_a(PLUS, _i_60624, 1);
        }
        DeRef(_0);
        goto L2; // [175] 157
L3: 
        ;
        DeRef(_i_60624);
    }

    /** 	while s and SymTab[s][S_SCOPE]=SC_PRIVATE do*/
L4: 
    if (_s_60586 == 0) {
        goto L5; // [185] 281
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30809 = (int)*(((s1_ptr)_2)->base + _s_60586);
    _2 = (int)SEQ_PTR(_30809);
    _30810 = (int)*(((s1_ptr)_2)->base + 4);
    _30809 = NOVALUE;
    if (IS_ATOM_INT(_30810)) {
        _30811 = (_30810 == 3);
    }
    else {
        _30811 = binary_op(EQUALS, _30810, 3);
    }
    _30810 = NOVALUE;
    if (_30811 <= 0) {
        if (_30811 == 0) {
            DeRef(_30811);
            _30811 = NOVALUE;
            goto L5; // [208] 281
        }
        else {
            if (!IS_ATOM_INT(_30811) && DBL_PTR(_30811)->dbl == 0.0){
                DeRef(_30811);
                _30811 = NOVALUE;
                goto L5; // [208] 281
            }
            DeRef(_30811);
            _30811 = NOVALUE;
        }
    }
    DeRef(_30811);
    _30811 = NOVALUE;

    /** 		if sequence(SymTab[s][S_CODE]) then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30812 = (int)*(((s1_ptr)_2)->base + _s_60586);
    _2 = (int)SEQ_PTR(_30812);
    if (!IS_ATOM_INT(_25S_CODE_11925)){
        _30813 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    }
    else{
        _30813 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
    }
    _30812 = NOVALUE;
    _30814 = IS_SEQUENCE(_30813);
    _30813 = NOVALUE;
    if (_30814 == 0)
    {
        _30814 = NOVALUE;
        goto L6; // [228] 260
    }
    else{
        _30814 = NOVALUE;
    }

    /** 			start_playback(SymTab[s][S_CODE])*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30815 = (int)*(((s1_ptr)_2)->base + _s_60586);
    _2 = (int)SEQ_PTR(_30815);
    if (!IS_ATOM_INT(_25S_CODE_11925)){
        _30816 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    }
    else{
        _30816 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
    }
    _30815 = NOVALUE;
    Ref(_30816);
    _30start_playback(_30816);
    _30816 = NOVALUE;

    /** 			Assignment({VARIABLE,s})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -100;
    ((int *)_2)[2] = _s_60586;
    _30817 = MAKE_SEQ(_1);
    _30Assignment(_30817);
    _30817 = NOVALUE;
L6: 

    /** 		s = SymTab[s][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30818 = (int)*(((s1_ptr)_2)->base + _s_60586);
    _2 = (int)SEQ_PTR(_30818);
    _s_60586 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_60586)){
        _s_60586 = (long)DBL_PTR(_s_60586)->dbl;
    }
    _30818 = NOVALUE;

    /** 	end while*/
    goto L4; // [278] 185
L5: 

    /** 	s = sub*/
    _s_60586 = _sub_60587;

    /** 	if scope = SC_PREDEF then*/
    if (_scope_60581 != 7)
    goto L7; // [292] 335

    /** 		emit_op(opcode)*/
    _37emit_op(_opcode_60582);

    /** 		if opcode = ABORT then*/
    if (_opcode_60582 != 126)
    goto L8; // [305] 370

    /** 			temp_tok = next_token()*/
    _0 = _temp_tok_60584;
    _temp_tok_60584 = _30next_token();
    DeRef(_0);

    /** 			putback(temp_tok)*/
    Ref(_temp_tok_60584);
    _30putback(_temp_tok_60584);

    /** 			NotReached(temp_tok[T_ID], "abort()")*/
    _2 = (int)SEQ_PTR(_temp_tok_60584);
    _30823 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_30823);
    RefDS(_28709);
    _30NotReached(_30823, _28709);
    _30823 = NOVALUE;
    goto L8; // [332] 370
L7: 

    /** 		op_info1 = s*/
    _37op_info1_51268 = _s_60586;

    /** 		emit_or_inline()*/
    _67emit_or_inline();

    /** 		if not TRANSLATE then*/
    if (_25TRANSLATE_11874 != 0)
    goto L9; // [350] 369

    /** 			if OpTrace then*/
    if (_25OpTrace_12332 == 0)
    {
        goto LA; // [357] 368
    }
    else{
    }

    /** 				emit_op(UPDATE_GLOBALS)*/
    _37emit_op(89);
LA: 
L9: 
L8: 

    /** end procedure*/
    DeRef(_tok_60579);
    DeRef(_temp_tok_60584);
    DeRef(_30805);
    _30805 = NOVALUE;
    return;
    ;
}


void _30Print_statement()
{
    int _30830 = NOVALUE;
    int _30829 = NOVALUE;
    int _30828 = NOVALUE;
    int _30826 = NOVALUE;
    int _30825 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	emit_opnd(NewIntSym(1)) -- stdout*/
    _30825 = _52NewIntSym(1);
    _37emit_opnd(_30825);
    _30825 = NOVALUE;

    /** 	Expr()*/
    _30Expr();

    /** 	emit_op(QPRINT)*/
    _37emit_op(36);

    /** 	SymTab[CurrentSub][S_EFFECT] = or_bits(SymTab[CurrentSub][S_EFFECT],*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25CurrentSub_12270 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30828 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_30828);
    _30829 = (int)*(((s1_ptr)_2)->base + 23);
    _30828 = NOVALUE;
    if (IS_ATOM_INT(_30829)) {
        {unsigned long tu;
             tu = (unsigned long)_30829 | (unsigned long)536870912;
             _30830 = MAKE_UINT(tu);
        }
    }
    else {
        _30830 = binary_op(OR_BITS, _30829, 536870912);
    }
    _30829 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 23);
    _1 = *(int *)_2;
    *(int *)_2 = _30830;
    if( _1 != _30830 ){
        DeRef(_1);
    }
    _30830 = NOVALUE;
    _30826 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


void _30Entry_statement()
{
    int _addr_60695 = NOVALUE;
    int _30854 = NOVALUE;
    int _30853 = NOVALUE;
    int _30852 = NOVALUE;
    int _30851 = NOVALUE;
    int _30850 = NOVALUE;
    int _30849 = NOVALUE;
    int _30848 = NOVALUE;
    int _30847 = NOVALUE;
    int _30843 = NOVALUE;
    int _30841 = NOVALUE;
    int _30840 = NOVALUE;
    int _30839 = NOVALUE;
    int _30838 = NOVALUE;
    int _30836 = NOVALUE;
    int _30835 = NOVALUE;
    int _30834 = NOVALUE;
    int _30832 = NOVALUE;
    int _30831 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length(loop_stack) or block_index=0 then*/
    if (IS_SEQUENCE(_30loop_stack_55205)){
            _30831 = SEQ_PTR(_30loop_stack_55205)->length;
    }
    else {
        _30831 = 1;
    }
    _30832 = (_30831 == 0);
    _30831 = NOVALUE;
    if (_30832 != 0) {
        goto L1; // [11] 26
    }
    _30834 = (_30block_index_55202 == 0);
    if (_30834 == 0)
    {
        DeRef(_30834);
        _30834 = NOVALUE;
        goto L2; // [22] 34
    }
    else{
        DeRef(_30834);
        _30834 = NOVALUE;
    }
L1: 

    /** 		CompileErr(144)*/
    RefDS(_22682);
    _43CompileErr(144, _22682, 0);
L2: 

    /** 	if block_list[block_index]=IF or block_list[block_index]=SWITCH then*/
    _2 = (int)SEQ_PTR(_30block_list_55201);
    _30835 = (int)*(((s1_ptr)_2)->base + _30block_index_55202);
    _30836 = (_30835 == 20);
    _30835 = NOVALUE;
    if (_30836 != 0) {
        goto L3; // [50] 73
    }
    _2 = (int)SEQ_PTR(_30block_list_55201);
    _30838 = (int)*(((s1_ptr)_2)->base + _30block_index_55202);
    _30839 = (_30838 == 185);
    _30838 = NOVALUE;
    if (_30839 == 0)
    {
        DeRef(_30839);
        _30839 = NOVALUE;
        goto L4; // [69] 83
    }
    else{
        DeRef(_30839);
        _30839 = NOVALUE;
    }
L3: 

    /** 		CompileErr(143)*/
    RefDS(_22682);
    _43CompileErr(143, _22682, 0);
    goto L5; // [80] 109
L4: 

    /** 	elsif loop_stack[$] = FOR then  -- not allowed in an innermost for loop*/
    if (IS_SEQUENCE(_30loop_stack_55205)){
            _30840 = SEQ_PTR(_30loop_stack_55205)->length;
    }
    else {
        _30840 = 1;
    }
    _2 = (int)SEQ_PTR(_30loop_stack_55205);
    _30841 = (int)*(((s1_ptr)_2)->base + _30840);
    if (_30841 != 21)
    goto L6; // [96] 108

    /** 		CompileErr(142)*/
    RefDS(_22682);
    _43CompileErr(142, _22682, 0);
L6: 
L5: 

    /** 	addr = entry_addr[$]*/
    if (IS_SEQUENCE(_30entry_addr_55195)){
            _30843 = SEQ_PTR(_30entry_addr_55195)->length;
    }
    else {
        _30843 = 1;
    }
    _2 = (int)SEQ_PTR(_30entry_addr_55195);
    _addr_60695 = (int)*(((s1_ptr)_2)->base + _30843);

    /** 	if addr=0  then*/
    if (_addr_60695 != 0)
    goto L7; // [122] 136

    /** 		CompileErr(141)*/
    RefDS(_22682);
    _43CompileErr(141, _22682, 0);
    goto L8; // [133] 151
L7: 

    /** 	elsif addr<0 then*/
    if (_addr_60695 >= 0)
    goto L9; // [138] 150

    /** 		CompileErr(73)*/
    RefDS(_22682);
    _43CompileErr(73, _22682, 0);
L9: 
L8: 

    /** 	backpatch(addr,ELSE)*/
    _37backpatch(_addr_60695, 23);

    /** 	backpatch(addr+1,length(Code)+1+(TRANSLATE>0))*/
    _30847 = _addr_60695 + 1;
    if (_30847 > MAXINT){
        _30847 = NewDouble((double)_30847);
    }
    if (IS_SEQUENCE(_25Code_12355)){
            _30848 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _30848 = 1;
    }
    _30849 = _30848 + 1;
    _30848 = NOVALUE;
    _30850 = (_25TRANSLATE_11874 > 0);
    _30851 = _30849 + _30850;
    _30849 = NOVALUE;
    _30850 = NOVALUE;
    _37backpatch(_30847, _30851);
    _30847 = NOVALUE;
    _30851 = NOVALUE;

    /** 	entry_addr[$] = 0*/
    if (IS_SEQUENCE(_30entry_addr_55195)){
            _30852 = SEQ_PTR(_30entry_addr_55195)->length;
    }
    else {
        _30852 = 1;
    }
    _2 = (int)SEQ_PTR(_30entry_addr_55195);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30entry_addr_55195 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _30852);
    *(int *)_2 = 0;

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto LA; // [203] 214
    }
    else{
    }

    /** 	    emit_op(NOP1)*/
    _37emit_op(159);
LA: 

    /** 	force_uninitialize( entry_stack[$] )*/
    if (IS_SEQUENCE(_30entry_stack_55198)){
            _30853 = SEQ_PTR(_30entry_stack_55198)->length;
    }
    else {
        _30853 = 1;
    }
    _2 = (int)SEQ_PTR(_30entry_stack_55198);
    _30854 = (int)*(((s1_ptr)_2)->base + _30853);
    Ref(_30854);
    _30force_uninitialize(_30854);
    _30854 = NOVALUE;

    /** end procedure*/
    DeRef(_30832);
    _30832 = NOVALUE;
    _30841 = NOVALUE;
    DeRef(_30836);
    _30836 = NOVALUE;
    return;
    ;
}


void _30force_uninitialize(int _uninitialized_60745)
{
    int _30857 = NOVALUE;
    int _30856 = NOVALUE;
    int _30855 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	for i = 1 to length( uninitialized ) do*/
    if (IS_SEQUENCE(_uninitialized_60745)){
            _30855 = SEQ_PTR(_uninitialized_60745)->length;
    }
    else {
        _30855 = 1;
    }
    {
        int _i_60747;
        _i_60747 = 1;
L1: 
        if (_i_60747 > _30855){
            goto L2; // [8] 41
        }

        /** 		SymTab[uninitialized[i]][S_INITLEVEL] = -1*/
        _2 = (int)SEQ_PTR(_uninitialized_60745);
        _30856 = (int)*(((s1_ptr)_2)->base + _i_60747);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_30856))
        _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_30856)->dbl));
        else
        _3 = (int)(_30856 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 14);
        _1 = *(int *)_2;
        *(int *)_2 = -1;
        DeRef(_1);
        _30857 = NOVALUE;

        /** 	end for*/
        _i_60747 = _i_60747 + 1;
        goto L1; // [36] 15
L2: 
        ;
    }

    /** end procedure*/
    DeRefDS(_uninitialized_60745);
    _30856 = NOVALUE;
    return;
    ;
}


void _30Statement_list()
{
    int _tok_60757 = NOVALUE;
    int _id_60758 = NOVALUE;
    int _forward_60781 = NOVALUE;
    int _test_60930 = NOVALUE;
    int _30929 = NOVALUE;
    int _30928 = NOVALUE;
    int _30925 = NOVALUE;
    int _30923 = NOVALUE;
    int _30922 = NOVALUE;
    int _30919 = NOVALUE;
    int _30917 = NOVALUE;
    int _30916 = NOVALUE;
    int _30915 = NOVALUE;
    int _30912 = NOVALUE;
    int _30910 = NOVALUE;
    int _30908 = NOVALUE;
    int _30906 = NOVALUE;
    int _30888 = NOVALUE;
    int _30887 = NOVALUE;
    int _30885 = NOVALUE;
    int _30883 = NOVALUE;
    int _30882 = NOVALUE;
    int _30880 = NOVALUE;
    int _30878 = NOVALUE;
    int _30877 = NOVALUE;
    int _30876 = NOVALUE;
    int _30875 = NOVALUE;
    int _30870 = NOVALUE;
    int _30867 = NOVALUE;
    int _30866 = NOVALUE;
    int _30865 = NOVALUE;
    int _30864 = NOVALUE;
    int _30862 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer id*/

    /** 	stmt_nest += 1*/
    _30stmt_nest_55203 = _30stmt_nest_55203 + 1;

    /** 	while TRUE do*/
L1: 
    if (_5TRUE_244 == 0)
    {
        goto L2; // [18] 1131
    }
    else{
    }

    /** 		tok = next_token()*/
    _0 = _tok_60757;
    _tok_60757 = _30next_token();
    DeRef(_0);

    /** 		id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_60757);
    _id_60758 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_id_60758)){
        _id_60758 = (long)DBL_PTR(_id_60758)->dbl;
    }

    /** 		if id = VARIABLE or id = QUALIFIED_VARIABLE then*/
    _30862 = (_id_60758 == -100);
    if (_30862 != 0) {
        goto L3; // [44] 59
    }
    _30864 = (_id_60758 == 512);
    if (_30864 == 0)
    {
        DeRef(_30864);
        _30864 = NOVALUE;
        goto L4; // [55] 233
    }
    else{
        DeRef(_30864);
        _30864 = NOVALUE;
    }
L3: 

    /** 			if SymTab[tok[T_SYM]][S_SCOPE] = SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_tok_60757);
    _30865 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_30865)){
        _30866 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_30865)->dbl));
    }
    else{
        _30866 = (int)*(((s1_ptr)_2)->base + _30865);
    }
    _2 = (int)SEQ_PTR(_30866);
    _30867 = (int)*(((s1_ptr)_2)->base + 4);
    _30866 = NOVALUE;
    if (binary_op_a(NOTEQ, _30867, 9)){
        _30867 = NOVALUE;
        goto L5; // [81] 212
    }
    _30867 = NOVALUE;

    /** 				token forward = next_token()*/
    _0 = _forward_60781;
    _forward_60781 = _30next_token();
    DeRef(_0);

    /** 				switch forward[T_ID] do*/
    _2 = (int)SEQ_PTR(_forward_60781);
    _30870 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_30870) ){
        goto L6; // [98] 206
    }
    if(!IS_ATOM_INT(_30870)){
        if( (DBL_PTR(_30870)->dbl != (double) ((int) DBL_PTR(_30870)->dbl) ) ){
            goto L6; // [98] 206
        }
        _0 = (int) DBL_PTR(_30870)->dbl;
    }
    else {
        _0 = _30870;
    };
    _30870 = NOVALUE;
    switch ( _0 ){ 

        /** 					case LEFT_ROUND then*/
        case -26:

        /** 						StartSourceLine( TRUE )*/
        _37StartSourceLine(_5TRUE_244, 0, 2);

        /** 						Forward_call( tok )*/
        Ref(_tok_60757);
        _30Forward_call(_tok_60757, 195);

        /** 						flush_temps()*/
        RefDS(_22682);
        _37flush_temps(_22682);

        /** 						continue*/
        DeRef(_forward_60781);
        _forward_60781 = NOVALUE;
        goto L1; // [135] 16
        goto L6; // [137] 206

        /** 					case VARIABLE then*/
        case -100:

        /** 						putback( forward )*/
        Ref(_forward_60781);
        _30putback(_forward_60781);

        /** 						if param_num != -1 then*/
        if (_30param_num_55180 == -1)
        goto L7; // [152] 178

        /** 							param_num += 1*/
        _30param_num_55180 = _30param_num_55180 + 1;

        /** 							Private_declaration( tok[T_SYM] )*/
        _2 = (int)SEQ_PTR(_tok_60757);
        _30875 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_30875);
        _30Private_declaration(_30875);
        _30875 = NOVALUE;
        goto L8; // [175] 194
L7: 

        /** 							Global_declaration( tok[T_SYM], SC_LOCAL )*/
        _2 = (int)SEQ_PTR(_tok_60757);
        _30876 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_30876);
        _30877 = _30Global_declaration(_30876, 5);
        _30876 = NOVALUE;
L8: 

        /** 						flush_temps()*/
        RefDS(_22682);
        _37flush_temps(_22682);

        /** 						continue*/
        DeRef(_forward_60781);
        _forward_60781 = NOVALUE;
        goto L1; // [203] 16
    ;}L6: 

    /** 				putback( forward )*/
    Ref(_forward_60781);
    _30putback(_forward_60781);
L5: 
    DeRef(_forward_60781);
    _forward_60781 = NOVALUE;

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			Assignment(tok)*/
    Ref(_tok_60757);
    _30Assignment(_tok_60757);
    goto L9; // [230] 1121
L4: 

    /** 		elsif id = PROC or id = QUALIFIED_PROC then*/
    _30878 = (_id_60758 == 27);
    if (_30878 != 0) {
        goto LA; // [241] 256
    }
    _30880 = (_id_60758 == 521);
    if (_30880 == 0)
    {
        DeRef(_30880);
        _30880 = NOVALUE;
        goto LB; // [252] 295
    }
    else{
        DeRef(_30880);
        _30880 = NOVALUE;
    }
LA: 

    /** 			if id = PROC then*/
    if (_id_60758 != 27)
    goto LC; // [260] 276

    /** 				UndefinedVar( tok[T_SYM] )*/
    _2 = (int)SEQ_PTR(_tok_60757);
    _30882 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_30882);
    _30UndefinedVar(_30882);
    _30882 = NOVALUE;
LC: 

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			Procedure_call(tok)*/
    Ref(_tok_60757);
    _30Procedure_call(_tok_60757);
    goto L9; // [292] 1121
LB: 

    /** 		elsif id = FUNC or id = QUALIFIED_FUNC then*/
    _30883 = (_id_60758 == 501);
    if (_30883 != 0) {
        goto LD; // [303] 318
    }
    _30885 = (_id_60758 == 520);
    if (_30885 == 0)
    {
        DeRef(_30885);
        _30885 = NOVALUE;
        goto LE; // [314] 370
    }
    else{
        DeRef(_30885);
        _30885 = NOVALUE;
    }
LD: 

    /** 			if id = FUNC then*/
    if (_id_60758 != 501)
    goto LF; // [322] 338

    /** 				UndefinedVar( tok[T_SYM] )*/
    _2 = (int)SEQ_PTR(_tok_60757);
    _30887 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_30887);
    _30UndefinedVar(_30887);
    _30887 = NOVALUE;
LF: 

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			Procedure_call(tok)*/
    Ref(_tok_60757);
    _30Procedure_call(_tok_60757);

    /** 			clear_op()*/
    _37clear_op();

    /** 			if Pop() then end if*/
    _30888 = _37Pop();
    if (_30888 == 0) {
        DeRef(_30888);
        _30888 = NOVALUE;
        goto L9; // [363] 1121
    }
    else {
        if (!IS_ATOM_INT(_30888) && DBL_PTR(_30888)->dbl == 0.0){
            DeRef(_30888);
            _30888 = NOVALUE;
            goto L9; // [363] 1121
        }
        DeRef(_30888);
        _30888 = NOVALUE;
    }
    DeRef(_30888);
    _30888 = NOVALUE;
    goto L9; // [367] 1121
LE: 

    /** 		elsif id = IF then*/
    if (_id_60758 != 20)
    goto L10; // [374] 396

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			If_statement()*/
    _30If_statement();
    goto L9; // [393] 1121
L10: 

    /** 		elsif id = FOR then*/
    if (_id_60758 != 21)
    goto L11; // [400] 422

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			For_statement()*/
    _30For_statement();
    goto L9; // [419] 1121
L11: 

    /** 		elsif id = RETURN then*/
    if (_id_60758 != 413)
    goto L12; // [426] 448

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			Return_statement()*/
    _30Return_statement();
    goto L9; // [445] 1121
L12: 

    /** 		elsif id = LABEL then*/
    if (_id_60758 != 419)
    goto L13; // [452] 474

    /** 			StartSourceLine(TRUE, , COVERAGE_SUPPRESS )*/
    _37StartSourceLine(_5TRUE_244, 0, 1);

    /** 			GLabel_statement()*/
    _30GLabel_statement();
    goto L9; // [471] 1121
L13: 

    /** 		elsif id = GOTO then*/
    if (_id_60758 != 188)
    goto L14; // [478] 500

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			Goto_statement()*/
    _30Goto_statement();
    goto L9; // [497] 1121
L14: 

    /** 		elsif id = EXIT then*/
    if (_id_60758 != 61)
    goto L15; // [504] 526

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			Exit_statement()*/
    _30Exit_statement();
    goto L9; // [523] 1121
L15: 

    /** 		elsif id = BREAK then*/
    if (_id_60758 != 425)
    goto L16; // [530] 552

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			Break_statement()*/
    _30Break_statement();
    goto L9; // [549] 1121
L16: 

    /** 		elsif id = WHILE then*/
    if (_id_60758 != 47)
    goto L17; // [556] 578

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			While_statement()*/
    _30While_statement();
    goto L9; // [575] 1121
L17: 

    /** 		elsif id = LOOP then*/
    if (_id_60758 != 422)
    goto L18; // [582] 604

    /** 		    StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 	        Loop_statement()*/
    _30Loop_statement();
    goto L9; // [601] 1121
L18: 

    /** 		elsif id = ENTRY then*/
    if (_id_60758 != 424)
    goto L19; // [608] 630

    /** 		    StartSourceLine(TRUE, , COVERAGE_SUPPRESS )*/
    _37StartSourceLine(_5TRUE_244, 0, 1);

    /** 		    Entry_statement()*/
    _30Entry_statement();
    goto L9; // [627] 1121
L19: 

    /** 		elsif id = QUESTION_MARK then*/
    if (_id_60758 != -31)
    goto L1A; // [634] 656

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			Print_statement()*/
    _30Print_statement();
    goto L9; // [653] 1121
L1A: 

    /** 		elsif id = CONTINUE then*/
    if (_id_60758 != 426)
    goto L1B; // [660] 682

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			Continue_statement()*/
    _30Continue_statement();
    goto L9; // [679] 1121
L1B: 

    /** 		elsif id = RETRY then*/
    if (_id_60758 != 184)
    goto L1C; // [686] 708

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			Retry_statement()*/
    _30Retry_statement();
    goto L9; // [705] 1121
L1C: 

    /** 		elsif id = IFDEF then*/
    if (_id_60758 != 407)
    goto L1D; // [712] 734

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			Ifdef_statement()*/
    _30Ifdef_statement();
    goto L9; // [731] 1121
L1D: 

    /** 		elsif id = CASE then*/
    if (_id_60758 != 186)
    goto L1E; // [738] 749

    /** 			Case_statement()*/
    _30Case_statement();
    goto L9; // [746] 1121
L1E: 

    /** 		elsif id = SWITCH then*/
    if (_id_60758 != 185)
    goto L1F; // [753] 775

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			Switch_statement()*/
    _30Switch_statement();
    goto L9; // [772] 1121
L1F: 

    /** 		elsif id = FALLTHRU then*/
    if (_id_60758 != 431)
    goto L20; // [779] 790

    /** 			Fallthru_statement()*/
    _30Fallthru_statement();
    goto L9; // [787] 1121
L20: 

    /** 		elsif id = TYPE or id = QUALIFIED_TYPE then*/
    _30906 = (_id_60758 == 504);
    if (_30906 != 0) {
        goto L21; // [798] 813
    }
    _30908 = (_id_60758 == 522);
    if (_30908 == 0)
    {
        DeRef(_30908);
        _30908 = NOVALUE;
        goto L22; // [809] 942
    }
    else{
        DeRef(_30908);
        _30908 = NOVALUE;
    }
L21: 

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			token test = next_token()*/
    _0 = _test_60930;
    _test_60930 = _30next_token();
    DeRef(_0);

    /** 			putback( test )*/
    Ref(_test_60930);
    _30putback(_test_60930);

    /** 			if test[T_ID] = LEFT_ROUND then*/
    _2 = (int)SEQ_PTR(_test_60930);
    _30910 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30910, -26)){
        _30910 = NOVALUE;
        goto L23; // [844] 890
    }
    _30910 = NOVALUE;

    /** 				StartSourceLine( TRUE )*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 				Procedure_call(tok)*/
    Ref(_tok_60757);
    _30Procedure_call(_tok_60757);

    /** 				clear_op()*/
    _37clear_op();

    /** 				if Pop() then end if*/
    _30912 = _37Pop();
    if (_30912 == 0) {
        DeRef(_30912);
        _30912 = NOVALUE;
        goto L24; // [873] 877
    }
    else {
        if (!IS_ATOM_INT(_30912) && DBL_PTR(_30912)->dbl == 0.0){
            DeRef(_30912);
            _30912 = NOVALUE;
            goto L24; // [873] 877
        }
        DeRef(_30912);
        _30912 = NOVALUE;
    }
    DeRef(_30912);
    _30912 = NOVALUE;
L24: 

    /** 				ExecCommand()*/
    _30ExecCommand();

    /** 				continue*/
    DeRef(_test_60930);
    _test_60930 = NOVALUE;
    goto L1; // [885] 16
    goto L25; // [887] 937
L23: 

    /** 				if param_num != -1 then*/
    if (_30param_num_55180 == -1)
    goto L26; // [894] 920

    /** 					param_num += 1*/
    _30param_num_55180 = _30param_num_55180 + 1;

    /** 					Private_declaration( tok[T_SYM] )*/
    _2 = (int)SEQ_PTR(_tok_60757);
    _30915 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_30915);
    _30Private_declaration(_30915);
    _30915 = NOVALUE;
    goto L27; // [917] 936
L26: 

    /** 					Global_declaration( tok[T_SYM], SC_LOCAL )*/
    _2 = (int)SEQ_PTR(_tok_60757);
    _30916 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_30916);
    _30917 = _30Global_declaration(_30916, 5);
    _30916 = NOVALUE;
L27: 
L25: 
    DeRef(_test_60930);
    _test_60930 = NOVALUE;
    goto L9; // [939] 1121
L22: 

    /** 			if id = ELSE then*/
    if (_id_60758 != 23)
    goto L28; // [946] 1000

    /** 				if length(if_stack) = 0 then*/
    if (IS_SEQUENCE(_30if_stack_55206)){
            _30919 = SEQ_PTR(_30if_stack_55206)->length;
    }
    else {
        _30919 = 1;
    }
    if (_30919 != 0)
    goto L29; // [957] 1057

    /** 					if live_ifdef > 0 then*/
    if (_30live_ifdef_59349 <= 0)
    goto L2A; // [965] 988

    /** 						CompileErr(134, ifdef_lineno[$])*/
    if (IS_SEQUENCE(_30ifdef_lineno_59350)){
            _30922 = SEQ_PTR(_30ifdef_lineno_59350)->length;
    }
    else {
        _30922 = 1;
    }
    _2 = (int)SEQ_PTR(_30ifdef_lineno_59350);
    _30923 = (int)*(((s1_ptr)_2)->base + _30922);
    _43CompileErr(134, _30923, 0);
    _30923 = NOVALUE;
    goto L29; // [985] 1057
L2A: 

    /** 						CompileErr(118)*/
    RefDS(_22682);
    _43CompileErr(118, _22682, 0);
    goto L29; // [997] 1057
L28: 

    /** 			elsif id = ELSIF then*/
    if (_id_60758 != 414)
    goto L2B; // [1004] 1056

    /** 				if length(if_stack) = 0 then*/
    if (IS_SEQUENCE(_30if_stack_55206)){
            _30925 = SEQ_PTR(_30if_stack_55206)->length;
    }
    else {
        _30925 = 1;
    }
    if (_30925 != 0)
    goto L2C; // [1015] 1055

    /** 					if live_ifdef > 0 then*/
    if (_30live_ifdef_59349 <= 0)
    goto L2D; // [1023] 1046

    /** 						CompileErr(139, ifdef_lineno[$])*/
    if (IS_SEQUENCE(_30ifdef_lineno_59350)){
            _30928 = SEQ_PTR(_30ifdef_lineno_59350)->length;
    }
    else {
        _30928 = 1;
    }
    _2 = (int)SEQ_PTR(_30ifdef_lineno_59350);
    _30929 = (int)*(((s1_ptr)_2)->base + _30928);
    _43CompileErr(139, _30929, 0);
    _30929 = NOVALUE;
    goto L2E; // [1043] 1054
L2D: 

    /** 						CompileErr(119)*/
    RefDS(_22682);
    _43CompileErr(119, _22682, 0);
L2E: 
L2C: 
L2B: 
L29: 

    /** 			putback( tok )*/
    Ref(_tok_60757);
    _30putback(_tok_60757);

    /** 			switch id do*/
    _0 = _id_60758;
    switch ( _0 ){ 

        /** 				case END, ELSEDEF, ELSIFDEF, ELSIF, ELSE, UNTIL then*/
        case 402:
        case 409:
        case 408:
        case 414:
        case 23:
        case 423:

        /** 					stmt_nest -= 1*/
        _30stmt_nest_55203 = _30stmt_nest_55203 - 1;

        /** 					InitDelete()*/
        _30InitDelete();

        /** 					flush_temps()*/
        RefDS(_22682);
        _37flush_temps(_22682);

        /** 					return*/
        DeRef(_tok_60757);
        DeRef(_30862);
        _30862 = NOVALUE;
        _30865 = NOVALUE;
        DeRef(_30878);
        _30878 = NOVALUE;
        DeRef(_30877);
        _30877 = NOVALUE;
        DeRef(_30883);
        _30883 = NOVALUE;
        DeRef(_30906);
        _30906 = NOVALUE;
        DeRef(_30917);
        _30917 = NOVALUE;
        return;
        goto L2F; // [1105] 1120

        /** 				case else*/
        default:

        /** 					tok_match( END )*/
        _30tok_match(402, 0);
    ;}L2F: 
L9: 

    /** 		flush_temps()*/
    RefDS(_22682);
    _37flush_temps(_22682);

    /** 	end while*/
    goto L1; // [1128] 16
L2: 

    /** end procedure*/
    DeRef(_tok_60757);
    DeRef(_30862);
    _30862 = NOVALUE;
    _30865 = NOVALUE;
    DeRef(_30878);
    _30878 = NOVALUE;
    DeRef(_30877);
    _30877 = NOVALUE;
    DeRef(_30883);
    _30883 = NOVALUE;
    DeRef(_30906);
    _30906 = NOVALUE;
    DeRef(_30917);
    _30917 = NOVALUE;
    return;
    ;
}


void _30SubProg(int _prog_type_61000, int _scope_61001)
{
    int _h_61002 = NOVALUE;
    int _pt_61003 = NOVALUE;
    int _p_61005 = NOVALUE;
    int _type_sym_61006 = NOVALUE;
    int _sym_61007 = NOVALUE;
    int _tok_61009 = NOVALUE;
    int _prog_name_61010 = NOVALUE;
    int _first_def_arg_61011 = NOVALUE;
    int _again_61012 = NOVALUE;
    int _type_enum_61013 = NOVALUE;
    int _i1_sym_61014 = NOVALUE;
    int _enum_syms_61015 = NOVALUE;
    int _type_enum_gline_61016 = NOVALUE;
    int _real_gline_61017 = NOVALUE;
    int _tsym_61028 = NOVALUE;
    int _seq_symbol_61039 = NOVALUE;
    int _middle_def_args_61237 = NOVALUE;
    int _last_nda_61238 = NOVALUE;
    int _start_def_61239 = NOVALUE;
    int _last_link_61241 = NOVALUE;
    int _temptok_61268 = NOVALUE;
    int _undef_type_61270 = NOVALUE;
    int _tokcat_61319 = NOVALUE;
    int _last_comparison_61556 = NOVALUE;
    int _32359 = NOVALUE;
    int _32358 = NOVALUE;
    int _32357 = NOVALUE;
    int _32356 = NOVALUE;
    int _32355 = NOVALUE;
    int _32354 = NOVALUE;
    int _32353 = NOVALUE;
    int _31203 = NOVALUE;
    int _31201 = NOVALUE;
    int _31200 = NOVALUE;
    int _31198 = NOVALUE;
    int _31197 = NOVALUE;
    int _31196 = NOVALUE;
    int _31195 = NOVALUE;
    int _31194 = NOVALUE;
    int _31192 = NOVALUE;
    int _31191 = NOVALUE;
    int _31188 = NOVALUE;
    int _31187 = NOVALUE;
    int _31186 = NOVALUE;
    int _31185 = NOVALUE;
    int _31183 = NOVALUE;
    int _31175 = NOVALUE;
    int _31173 = NOVALUE;
    int _31172 = NOVALUE;
    int _31171 = NOVALUE;
    int _31169 = NOVALUE;
    int _31168 = NOVALUE;
    int _31167 = NOVALUE;
    int _31166 = NOVALUE;
    int _31164 = NOVALUE;
    int _31162 = NOVALUE;
    int _31161 = NOVALUE;
    int _31160 = NOVALUE;
    int _31159 = NOVALUE;
    int _31156 = NOVALUE;
    int _31155 = NOVALUE;
    int _31154 = NOVALUE;
    int _31152 = NOVALUE;
    int _31149 = NOVALUE;
    int _31148 = NOVALUE;
    int _31147 = NOVALUE;
    int _31145 = NOVALUE;
    int _31144 = NOVALUE;
    int _31141 = NOVALUE;
    int _31139 = NOVALUE;
    int _31137 = NOVALUE;
    int _31136 = NOVALUE;
    int _31135 = NOVALUE;
    int _31134 = NOVALUE;
    int _31132 = NOVALUE;
    int _31131 = NOVALUE;
    int _31130 = NOVALUE;
    int _31129 = NOVALUE;
    int _31128 = NOVALUE;
    int _31127 = NOVALUE;
    int _31124 = NOVALUE;
    int _31122 = NOVALUE;
    int _31120 = NOVALUE;
    int _31118 = NOVALUE;
    int _31116 = NOVALUE;
    int _31113 = NOVALUE;
    int _31111 = NOVALUE;
    int _31110 = NOVALUE;
    int _31107 = NOVALUE;
    int _31103 = NOVALUE;
    int _31102 = NOVALUE;
    int _31100 = NOVALUE;
    int _31098 = NOVALUE;
    int _31096 = NOVALUE;
    int _31094 = NOVALUE;
    int _31092 = NOVALUE;
    int _31090 = NOVALUE;
    int _31089 = NOVALUE;
    int _31088 = NOVALUE;
    int _31087 = NOVALUE;
    int _31086 = NOVALUE;
    int _31085 = NOVALUE;
    int _31084 = NOVALUE;
    int _31083 = NOVALUE;
    int _31082 = NOVALUE;
    int _31081 = NOVALUE;
    int _31080 = NOVALUE;
    int _31079 = NOVALUE;
    int _31076 = NOVALUE;
    int _31075 = NOVALUE;
    int _31074 = NOVALUE;
    int _31073 = NOVALUE;
    int _31072 = NOVALUE;
    int _31071 = NOVALUE;
    int _31070 = NOVALUE;
    int _31069 = NOVALUE;
    int _31068 = NOVALUE;
    int _31067 = NOVALUE;
    int _31066 = NOVALUE;
    int _31065 = NOVALUE;
    int _31064 = NOVALUE;
    int _31063 = NOVALUE;
    int _31062 = NOVALUE;
    int _31060 = NOVALUE;
    int _31058 = NOVALUE;
    int _31057 = NOVALUE;
    int _31052 = NOVALUE;
    int _31051 = NOVALUE;
    int _31049 = NOVALUE;
    int _31048 = NOVALUE;
    int _31047 = NOVALUE;
    int _31046 = NOVALUE;
    int _31045 = NOVALUE;
    int _31044 = NOVALUE;
    int _31043 = NOVALUE;
    int _31042 = NOVALUE;
    int _31041 = NOVALUE;
    int _31040 = NOVALUE;
    int _31038 = NOVALUE;
    int _31037 = NOVALUE;
    int _31035 = NOVALUE;
    int _31034 = NOVALUE;
    int _31033 = NOVALUE;
    int _31032 = NOVALUE;
    int _31031 = NOVALUE;
    int _31030 = NOVALUE;
    int _31029 = NOVALUE;
    int _31027 = NOVALUE;
    int _31024 = NOVALUE;
    int _31022 = NOVALUE;
    int _31020 = NOVALUE;
    int _31018 = NOVALUE;
    int _31016 = NOVALUE;
    int _31014 = NOVALUE;
    int _31012 = NOVALUE;
    int _31010 = NOVALUE;
    int _31008 = NOVALUE;
    int _31007 = NOVALUE;
    int _31006 = NOVALUE;
    int _31005 = NOVALUE;
    int _31004 = NOVALUE;
    int _31003 = NOVALUE;
    int _31002 = NOVALUE;
    int _31000 = NOVALUE;
    int _30999 = NOVALUE;
    int _30997 = NOVALUE;
    int _30995 = NOVALUE;
    int _30993 = NOVALUE;
    int _30992 = NOVALUE;
    int _30989 = NOVALUE;
    int _30988 = NOVALUE;
    int _30987 = NOVALUE;
    int _30986 = NOVALUE;
    int _30985 = NOVALUE;
    int _30982 = NOVALUE;
    int _30981 = NOVALUE;
    int _30980 = NOVALUE;
    int _30979 = NOVALUE;
    int _30978 = NOVALUE;
    int _30976 = NOVALUE;
    int _30975 = NOVALUE;
    int _30974 = NOVALUE;
    int _30972 = NOVALUE;
    int _30971 = NOVALUE;
    int _30970 = NOVALUE;
    int _30969 = NOVALUE;
    int _30965 = NOVALUE;
    int _30964 = NOVALUE;
    int _30963 = NOVALUE;
    int _30961 = NOVALUE;
    int _30960 = NOVALUE;
    int _30959 = NOVALUE;
    int _30958 = NOVALUE;
    int _30957 = NOVALUE;
    int _30956 = NOVALUE;
    int _30953 = NOVALUE;
    int _30952 = NOVALUE;
    int _30951 = NOVALUE;
    int _30949 = NOVALUE;
    int _30948 = NOVALUE;
    int _30947 = NOVALUE;
    int _30945 = NOVALUE;
    int _30944 = NOVALUE;
    int _30942 = NOVALUE;
    int _30941 = NOVALUE;
    int _30940 = NOVALUE;
    int _30936 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_prog_type_61000)) {
        _1 = (long)(DBL_PTR(_prog_type_61000)->dbl);
        if (UNIQUE(DBL_PTR(_prog_type_61000)) && (DBL_PTR(_prog_type_61000)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_prog_type_61000);
        _prog_type_61000 = _1;
    }

    /** 	integer first_def_arg*/

    /** 	integer again*/

    /** 	integer type_enum*/

    /** 	object i1_sym*/

    /** 	sequence enum_syms = {}*/
    RefDS(_22682);
    DeRef(_enum_syms_61015);
    _enum_syms_61015 = _22682;

    /** 	integer type_enum_gline, real_gline*/

    /** 	LeaveTopLevel()*/
    _30LeaveTopLevel();

    /** 	prog_name = next_token()*/
    _0 = _prog_name_61010;
    _prog_name_61010 = _30next_token();
    DeRef(_0);

    /** 	if prog_name[T_ID] = END_OF_FILE then*/
    _2 = (int)SEQ_PTR(_prog_name_61010);
    _30936 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _30936, -21)){
        _30936 = NOVALUE;
        goto L1; // [41] 53
    }
    _30936 = NOVALUE;

    /** 		CompileErr( 32 )*/
    RefDS(_22682);
    _43CompileErr(32, _22682, 0);
L1: 

    /** 	type_enum =  0*/
    _type_enum_61013 = 0;

    /** 	if prog_type = TYPE_DECL then*/
    if (_prog_type_61000 != 416)
    goto L2; // [62] 308

    /** 		object tsym = prog_name[T_SYM]*/
    DeRef(_tsym_61028);
    _2 = (int)SEQ_PTR(_prog_name_61010);
    _tsym_61028 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_tsym_61028);

    /** 		if equal(sym_name(prog_name[T_SYM]),"enum") then*/
    _2 = (int)SEQ_PTR(_prog_name_61010);
    _30940 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_30940);
    _30941 = _52sym_name(_30940);
    _30940 = NOVALUE;
    if (_30941 == _27041)
    _30942 = 1;
    else if (IS_ATOM_INT(_30941) && IS_ATOM_INT(_27041))
    _30942 = 0;
    else
    _30942 = (compare(_30941, _27041) == 0);
    DeRef(_30941);
    _30941 = NOVALUE;
    if (_30942 == 0)
    {
        _30942 = NOVALUE;
        goto L3; // [90] 305
    }
    else{
        _30942 = NOVALUE;
    }

    /** 			EnterTopLevel( FALSE )*/
    _30EnterTopLevel(_5FALSE_242);

    /** 			type_enum_gline = gline_number*/
    _type_enum_gline_61016 = _25gline_number_12267;

    /** 			type_enum = 1*/
    _type_enum_61013 = 1;

    /** 			sequence seq_symbol*/

    /** 			prog_name = next_token()*/
    _0 = _prog_name_61010;
    _prog_name_61010 = _30next_token();
    DeRef(_0);

    /** 			if not find(prog_name[T_ID], ADDR_TOKS) then*/
    _2 = (int)SEQ_PTR(_prog_name_61010);
    _30944 = (int)*(((s1_ptr)_2)->base + 1);
    _30945 = find_from(_30944, _28ADDR_TOKS_11861, 1);
    _30944 = NOVALUE;
    if (_30945 != 0)
    goto L4; // [136] 161
    _30945 = NOVALUE;

    /** 				CompileErr(25, {find_category(prog_name[T_ID])} )*/
    _2 = (int)SEQ_PTR(_prog_name_61010);
    _30947 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_30947);
    _30948 = _63find_category(_30947);
    _30947 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _30948;
    _30949 = MAKE_SEQ(_1);
    _30948 = NOVALUE;
    _43CompileErr(25, _30949, 0);
    _30949 = NOVALUE;
L4: 

    /** 			enum_syms = Global_declaration(-1, scope)*/
    _0 = _enum_syms_61015;
    _enum_syms_61015 = _30Global_declaration(-1, _scope_61001);
    DeRef(_0);

    /** 			seq_symbol = enum_syms*/
    RefDS(_enum_syms_61015);
    DeRef(_seq_symbol_61039);
    _seq_symbol_61039 = _enum_syms_61015;

    /** 			for i = 1 to length( enum_syms ) do*/
    if (IS_SEQUENCE(_enum_syms_61015)){
            _30951 = SEQ_PTR(_enum_syms_61015)->length;
    }
    else {
        _30951 = 1;
    }
    {
        int _i_61055;
        _i_61055 = 1;
L5: 
        if (_i_61055 > _30951){
            goto L6; // [182] 210
        }

        /** 				seq_symbol[i] = sym_obj(enum_syms[i])*/
        _2 = (int)SEQ_PTR(_enum_syms_61015);
        _30952 = (int)*(((s1_ptr)_2)->base + _i_61055);
        Ref(_30952);
        _30953 = _52sym_obj(_30952);
        _30952 = NOVALUE;
        _2 = (int)SEQ_PTR(_seq_symbol_61039);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _seq_symbol_61039 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_61055);
        _1 = *(int *)_2;
        *(int *)_2 = _30953;
        if( _1 != _30953 ){
            DeRef(_1);
        }
        _30953 = NOVALUE;

        /** 			end for*/
        _i_61055 = _i_61055 + 1;
        goto L5; // [205] 189
L6: 
        ;
    }

    /** 			i1_sym = keyfind("i1",-1)*/
    RefDS(_30954);
    DeRef(_32358);
    _32358 = _30954;
    _32359 = _52hashfn(_32358);
    _32358 = NOVALUE;
    RefDS(_30954);
    _0 = _i1_sym_61014;
    _i1_sym_61014 = _52keyfind(_30954, -1, _25current_file_no_12262, 0, _32359);
    DeRef(_0);
    _32359 = NOVALUE;

    /** 			putback(keyfind("return",-1))*/
    RefDS(_27115);
    DeRef(_32356);
    _32356 = _27115;
    _32357 = _52hashfn(_32356);
    _32356 = NOVALUE;
    RefDS(_27115);
    _30956 = _52keyfind(_27115, -1, _25current_file_no_12262, 0, _32357);
    _32357 = NOVALUE;
    _30putback(_30956);
    _30956 = NOVALUE;

    /** 			putback({RIGHT_ROUND,0})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -27;
    ((int *)_2)[2] = 0;
    _30957 = MAKE_SEQ(_1);
    _30putback(_30957);
    _30957 = NOVALUE;

    /** 			putback(i1_sym)*/
    Ref(_i1_sym_61014);
    _30putback(_i1_sym_61014);

    /** 			putback(keyfind("object",-1))*/
    RefDS(_25343);
    DeRef(_32354);
    _32354 = _25343;
    _32355 = _52hashfn(_32354);
    _32354 = NOVALUE;
    RefDS(_25343);
    _30958 = _52keyfind(_25343, -1, _25current_file_no_12262, 0, _32355);
    _32355 = NOVALUE;
    _30putback(_30958);
    _30958 = NOVALUE;

    /** 			putback({LEFT_ROUND,0})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -26;
    ((int *)_2)[2] = 0;
    _30959 = MAKE_SEQ(_1);
    _30putback(_30959);
    _30959 = NOVALUE;

    /** 			LeaveTopLevel()*/
    _30LeaveTopLevel();
L3: 
    DeRef(_seq_symbol_61039);
    _seq_symbol_61039 = NOVALUE;
L2: 
    DeRef(_tsym_61028);
    _tsym_61028 = NOVALUE;

    /** 	if not find(prog_name[T_ID], ADDR_TOKS) then*/
    _2 = (int)SEQ_PTR(_prog_name_61010);
    _30960 = (int)*(((s1_ptr)_2)->base + 1);
    _30961 = find_from(_30960, _28ADDR_TOKS_11861, 1);
    _30960 = NOVALUE;
    if (_30961 != 0)
    goto L7; // [325] 350
    _30961 = NOVALUE;

    /** 		CompileErr(25, {find_category(prog_name[T_ID])} )*/
    _2 = (int)SEQ_PTR(_prog_name_61010);
    _30963 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_30963);
    _30964 = _63find_category(_30963);
    _30963 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _30964;
    _30965 = MAKE_SEQ(_1);
    _30964 = NOVALUE;
    _43CompileErr(25, _30965, 0);
    _30965 = NOVALUE;
L7: 

    /** 	p = prog_name[T_SYM]*/
    _2 = (int)SEQ_PTR(_prog_name_61010);
    _p_61005 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_p_61005)){
        _p_61005 = (long)DBL_PTR(_p_61005)->dbl;
    }

    /** 	DefinedYet(p)*/
    _52DefinedYet(_p_61005);

    /** 	if prog_type = PROCEDURE then*/
    if (_prog_type_61000 != 405)
    goto L8; // [369] 385

    /** 		pt = PROC*/
    _pt_61003 = 27;
    goto L9; // [382] 415
L8: 

    /** 	elsif prog_type = FUNCTION then*/
    if (_prog_type_61000 != 406)
    goto LA; // [389] 405

    /** 		pt = FUNC*/
    _pt_61003 = 501;
    goto L9; // [402] 415
LA: 

    /** 		pt = TYPE*/
    _pt_61003 = 504;
L9: 

    /** 	clear_fwd_refs()*/
    _29clear_fwd_refs();

    /** 	if find(SymTab[p][S_SCOPE], {SC_PREDEF, SC_GLOBAL, SC_PUBLIC, SC_EXPORT, SC_OVERRIDE}) then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30969 = (int)*(((s1_ptr)_2)->base + _p_61005);
    _2 = (int)SEQ_PTR(_30969);
    _30970 = (int)*(((s1_ptr)_2)->base + 4);
    _30969 = NOVALUE;
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 7;
    *((int *)(_2+8)) = 6;
    *((int *)(_2+12)) = 13;
    *((int *)(_2+16)) = 11;
    *((int *)(_2+20)) = 12;
    _30971 = MAKE_SEQ(_1);
    _30972 = find_from(_30970, _30971, 1);
    _30970 = NOVALUE;
    DeRefDS(_30971);
    _30971 = NOVALUE;
    if (_30972 == 0)
    {
        _30972 = NOVALUE;
        goto LB; // [456] 652
    }
    else{
        _30972 = NOVALUE;
    }

    /** 		if scope = SC_OVERRIDE then*/
    if (_scope_61001 != 12)
    goto LC; // [463] 589

    /** 			if SymTab[p][S_SCOPE] = SC_PREDEF or SymTab[p][S_SCOPE] = SC_OVERRIDE then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30974 = (int)*(((s1_ptr)_2)->base + _p_61005);
    _2 = (int)SEQ_PTR(_30974);
    _30975 = (int)*(((s1_ptr)_2)->base + 4);
    _30974 = NOVALUE;
    if (IS_ATOM_INT(_30975)) {
        _30976 = (_30975 == 7);
    }
    else {
        _30976 = binary_op(EQUALS, _30975, 7);
    }
    _30975 = NOVALUE;
    if (IS_ATOM_INT(_30976)) {
        if (_30976 != 0) {
            goto LD; // [487] 514
        }
    }
    else {
        if (DBL_PTR(_30976)->dbl != 0.0) {
            goto LD; // [487] 514
        }
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30978 = (int)*(((s1_ptr)_2)->base + _p_61005);
    _2 = (int)SEQ_PTR(_30978);
    _30979 = (int)*(((s1_ptr)_2)->base + 4);
    _30978 = NOVALUE;
    if (IS_ATOM_INT(_30979)) {
        _30980 = (_30979 == 12);
    }
    else {
        _30980 = binary_op(EQUALS, _30979, 12);
    }
    _30979 = NOVALUE;
    if (_30980 == 0) {
        DeRef(_30980);
        _30980 = NOVALUE;
        goto LE; // [510] 588
    }
    else {
        if (!IS_ATOM_INT(_30980) && DBL_PTR(_30980)->dbl == 0.0){
            DeRef(_30980);
            _30980 = NOVALUE;
            goto LE; // [510] 588
        }
        DeRef(_30980);
        _30980 = NOVALUE;
    }
    DeRef(_30980);
    _30980 = NOVALUE;
LD: 

    /** 					if SymTab[p][S_SCOPE] = SC_OVERRIDE then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30981 = (int)*(((s1_ptr)_2)->base + _p_61005);
    _2 = (int)SEQ_PTR(_30981);
    _30982 = (int)*(((s1_ptr)_2)->base + 4);
    _30981 = NOVALUE;
    if (binary_op_a(NOTEQ, _30982, 12)){
        _30982 = NOVALUE;
        goto LF; // [530] 542
    }
    _30982 = NOVALUE;

    /** 						again = 223*/
    _again_61012 = 223;
    goto L10; // [539] 548
LF: 

    /** 						again = 222*/
    _again_61012 = 222;
L10: 

    /** 					Warning(again, override_warning_flag,*/
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _30985 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30986 = (int)*(((s1_ptr)_2)->base + _p_61005);
    _2 = (int)SEQ_PTR(_30986);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _30987 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _30987 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _30986 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_30985);
    *((int *)(_2+4)) = _30985;
    *((int *)(_2+8)) = _25line_number_12263;
    Ref(_30987);
    *((int *)(_2+12)) = _30987;
    _30988 = MAKE_SEQ(_1);
    _30987 = NOVALUE;
    _30985 = NOVALUE;
    _43Warning(_again_61012, 4, _30988);
    _30988 = NOVALUE;
LE: 
LC: 

    /** 		h = SymTab[p][S_HASHVAL]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30989 = (int)*(((s1_ptr)_2)->base + _p_61005);
    _2 = (int)SEQ_PTR(_30989);
    _h_61002 = (int)*(((s1_ptr)_2)->base + 11);
    if (!IS_ATOM_INT(_h_61002)){
        _h_61002 = (long)DBL_PTR(_h_61002)->dbl;
    }
    _30989 = NOVALUE;

    /** 		sym = buckets[h]*/
    _2 = (int)SEQ_PTR(_52buckets_47095);
    _sym_61007 = (int)*(((s1_ptr)_2)->base + _h_61002);
    if (!IS_ATOM_INT(_sym_61007)){
        _sym_61007 = (long)DBL_PTR(_sym_61007)->dbl;
    }

    /** 		p = NewEntry(SymTab[p][S_NAME], 0, 0, pt, h, sym, 0)*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30992 = (int)*(((s1_ptr)_2)->base + _p_61005);
    _2 = (int)SEQ_PTR(_30992);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _30993 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _30993 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _30992 = NOVALUE;
    Ref(_30993);
    _p_61005 = _52NewEntry(_30993, 0, 0, _pt_61003, _h_61002, _sym_61007, 0);
    _30993 = NOVALUE;
    if (!IS_ATOM_INT(_p_61005)) {
        _1 = (long)(DBL_PTR(_p_61005)->dbl);
        if (UNIQUE(DBL_PTR(_p_61005)) && (DBL_PTR(_p_61005)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_p_61005);
        _p_61005 = _1;
    }

    /** 		buckets[h] = p*/
    _2 = (int)SEQ_PTR(_52buckets_47095);
    _2 = (int)(((s1_ptr)_2)->base + _h_61002);
    _1 = *(int *)_2;
    *(int *)_2 = _p_61005;
    DeRef(_1);
LB: 

    /** 	Start_block( pt, p )*/
    _66Start_block(_pt_61003, _p_61005);

    /** 	CurrentSub = p*/
    _25CurrentSub_12270 = _p_61005;

    /** 	first_def_arg = 0*/
    _first_def_arg_61011 = 0;

    /** 	temps_allocated = 0*/
    _52temps_allocated_47621 = 0;

    /** 	SymTab[p][S_SCOPE] = scope*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_61005 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _scope_61001;
    DeRef(_1);
    _30995 = NOVALUE;

    /** 	SymTab[p][S_TOKEN] = pt*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_61005 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_TOKEN_11918))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    _1 = *(int *)_2;
    *(int *)_2 = _pt_61003;
    DeRef(_1);
    _30997 = NOVALUE;

    /** 	if length(SymTab[p]) < SIZEOF_ROUTINE_ENTRY then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _30999 = (int)*(((s1_ptr)_2)->base + _p_61005);
    if (IS_SEQUENCE(_30999)){
            _31000 = SEQ_PTR(_30999)->length;
    }
    else {
        _31000 = 1;
    }
    _30999 = NOVALUE;
    if (_31000 >= _25SIZEOF_ROUTINE_ENTRY_12038)
    goto L11; // [722] 764

    /** 		SymTab[p] = SymTab[p] & repeat(0, SIZEOF_ROUTINE_ENTRY -*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31002 = (int)*(((s1_ptr)_2)->base + _p_61005);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31003 = (int)*(((s1_ptr)_2)->base + _p_61005);
    if (IS_SEQUENCE(_31003)){
            _31004 = SEQ_PTR(_31003)->length;
    }
    else {
        _31004 = 1;
    }
    _31003 = NOVALUE;
    _31005 = _25SIZEOF_ROUTINE_ENTRY_12038 - _31004;
    _31004 = NOVALUE;
    _31006 = Repeat(0, _31005);
    _31005 = NOVALUE;
    if (IS_SEQUENCE(_31002) && IS_ATOM(_31006)) {
    }
    else if (IS_ATOM(_31002) && IS_SEQUENCE(_31006)) {
        Ref(_31002);
        Prepend(&_31007, _31006, _31002);
    }
    else {
        Concat((object_ptr)&_31007, _31002, _31006);
        _31002 = NOVALUE;
    }
    _31002 = NOVALUE;
    DeRefDS(_31006);
    _31006 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _p_61005);
    _1 = *(int *)_2;
    *(int *)_2 = _31007;
    if( _1 != _31007 ){
        DeRef(_1);
    }
    _31007 = NOVALUE;
L11: 

    /** 	SymTab[p][S_CODE] = 0*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_61005 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_CODE_11925))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_CODE_11925);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _31008 = NOVALUE;

    /** 	SymTab[p][S_LINETAB] = 0*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_61005 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_LINETAB_11948))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_LINETAB_11948)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_LINETAB_11948);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _31010 = NOVALUE;

    /** 	SymTab[p][S_EFFECT] = E_PURE*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_61005 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 23);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _31012 = NOVALUE;

    /** 	SymTab[p][S_REFLIST] = {}*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_61005 + ((s1_ptr)_2)->base);
    RefDS(_22682);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 24);
    _1 = *(int *)_2;
    *(int *)_2 = _22682;
    DeRef(_1);
    _31014 = NOVALUE;

    /** 	SymTab[p][S_FIRSTLINE] = gline_number*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_61005 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_FIRSTLINE_11953))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FIRSTLINE_11953)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_FIRSTLINE_11953);
    _1 = *(int *)_2;
    *(int *)_2 = _25gline_number_12267;
    DeRef(_1);
    _31016 = NOVALUE;

    /** 	SymTab[p][S_TEMPS] = 0*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_61005 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_TEMPS_11958))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TEMPS_11958)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_TEMPS_11958);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _31018 = NOVALUE;

    /** 	SymTab[p][S_RESIDENT_TASK] = 0*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_61005 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 25);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _31020 = NOVALUE;

    /** 	SymTab[p][S_SAVED_PRIVATES] = {}*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_61005 + ((s1_ptr)_2)->base);
    RefDS(_22682);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 26);
    _1 = *(int *)_2;
    *(int *)_2 = _22682;
    DeRef(_1);
    _31022 = NOVALUE;

    /** 	if type_enum then*/
    if (_type_enum_61013 == 0)
    {
        goto L12; // [890] 947
    }
    else{
    }

    /** 		SymTab[p][S_FIRSTLINE] = type_enum_gline*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_61005 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_FIRSTLINE_11953))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FIRSTLINE_11953)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_FIRSTLINE_11953);
    _1 = *(int *)_2;
    *(int *)_2 = _type_enum_gline_61016;
    DeRef(_1);
    _31024 = NOVALUE;

    /** 		real_gline = gline_number*/
    _real_gline_61017 = _25gline_number_12267;

    /** 		gline_number = type_enum_gline*/
    _25gline_number_12267 = _type_enum_gline_61016;

    /** 		StartSourceLine( FALSE, , COVERAGE_OVERRIDE )*/
    _37StartSourceLine(_5FALSE_242, 0, 3);

    /** 		gline_number = real_gline*/
    _25gline_number_12267 = _real_gline_61017;
    goto L13; // [944] 959
L12: 

    /** 		StartSourceLine(FALSE, , COVERAGE_OVERRIDE)*/
    _37StartSourceLine(_5FALSE_242, 0, 3);
L13: 

    /** 	tok_match(LEFT_ROUND)*/
    _30tok_match(-26, 0);

    /** 	tok = next_token()*/
    _0 = _tok_61009;
    _tok_61009 = _30next_token();
    DeRef(_0);

    /** 	param_num = 0*/
    _30param_num_55180 = 0;

    /** 	sequence middle_def_args = {}*/
    RefDS(_22682);
    DeRef(_middle_def_args_61237);
    _middle_def_args_61237 = _22682;

    /** 	integer last_nda = 0, start_def = 0*/
    _last_nda_61238 = 0;
    _start_def_61239 = 0;

    /** 	symtab_index last_link = p*/
    _last_link_61241 = _p_61005;

    /** 	while tok[T_ID] != RIGHT_ROUND do*/
L14: 
    _2 = (int)SEQ_PTR(_tok_61009);
    _31027 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _31027, -27)){
        _31027 = NOVALUE;
        goto L15; // [1012] 1783
    }
    _31027 = NOVALUE;

    /** 		if tok[T_ID] != TYPE and tok[T_ID] != QUALIFIED_TYPE then*/
    _2 = (int)SEQ_PTR(_tok_61009);
    _31029 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31029)) {
        _31030 = (_31029 != 504);
    }
    else {
        _31030 = binary_op(NOTEQ, _31029, 504);
    }
    _31029 = NOVALUE;
    if (IS_ATOM_INT(_31030)) {
        if (_31030 == 0) {
            goto L16; // [1030] 1254
        }
    }
    else {
        if (DBL_PTR(_31030)->dbl == 0.0) {
            goto L16; // [1030] 1254
        }
    }
    _2 = (int)SEQ_PTR(_tok_61009);
    _31032 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31032)) {
        _31033 = (_31032 != 522);
    }
    else {
        _31033 = binary_op(NOTEQ, _31032, 522);
    }
    _31032 = NOVALUE;
    if (_31033 == 0) {
        DeRef(_31033);
        _31033 = NOVALUE;
        goto L16; // [1047] 1254
    }
    else {
        if (!IS_ATOM_INT(_31033) && DBL_PTR(_31033)->dbl == 0.0){
            DeRef(_31033);
            _31033 = NOVALUE;
            goto L16; // [1047] 1254
        }
        DeRef(_31033);
        _31033 = NOVALUE;
    }
    DeRef(_31033);
    _31033 = NOVALUE;

    /** 			if tok[T_ID] = VARIABLE or tok[T_ID] = QUALIFIED_VARIABLE then*/
    _2 = (int)SEQ_PTR(_tok_61009);
    _31034 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31034)) {
        _31035 = (_31034 == -100);
    }
    else {
        _31035 = binary_op(EQUALS, _31034, -100);
    }
    _31034 = NOVALUE;
    if (IS_ATOM_INT(_31035)) {
        if (_31035 != 0) {
            goto L17; // [1064] 1085
        }
    }
    else {
        if (DBL_PTR(_31035)->dbl != 0.0) {
            goto L17; // [1064] 1085
        }
    }
    _2 = (int)SEQ_PTR(_tok_61009);
    _31037 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31037)) {
        _31038 = (_31037 == 512);
    }
    else {
        _31038 = binary_op(EQUALS, _31037, 512);
    }
    _31037 = NOVALUE;
    if (_31038 == 0) {
        DeRef(_31038);
        _31038 = NOVALUE;
        goto L18; // [1081] 1245
    }
    else {
        if (!IS_ATOM_INT(_31038) && DBL_PTR(_31038)->dbl == 0.0){
            DeRef(_31038);
            _31038 = NOVALUE;
            goto L18; // [1081] 1245
        }
        DeRef(_31038);
        _31038 = NOVALUE;
    }
    DeRef(_31038);
    _31038 = NOVALUE;
L17: 

    /** 				token temptok = next_token()*/
    _0 = _temptok_61268;
    _temptok_61268 = _30next_token();
    DeRef(_0);

    /** 				integer undef_type = 0*/
    _undef_type_61270 = 0;

    /** 				if temptok[T_ID] != TYPE and temptok[T_ID] != QUALIFIED_TYPE then*/
    _2 = (int)SEQ_PTR(_temptok_61268);
    _31040 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31040)) {
        _31041 = (_31040 != 504);
    }
    else {
        _31041 = binary_op(NOTEQ, _31040, 504);
    }
    _31040 = NOVALUE;
    if (IS_ATOM_INT(_31041)) {
        if (_31041 == 0) {
            goto L19; // [1109] 1210
        }
    }
    else {
        if (DBL_PTR(_31041)->dbl == 0.0) {
            goto L19; // [1109] 1210
        }
    }
    _2 = (int)SEQ_PTR(_temptok_61268);
    _31043 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31043)) {
        _31044 = (_31043 != 522);
    }
    else {
        _31044 = binary_op(NOTEQ, _31043, 522);
    }
    _31043 = NOVALUE;
    if (_31044 == 0) {
        DeRef(_31044);
        _31044 = NOVALUE;
        goto L19; // [1126] 1210
    }
    else {
        if (!IS_ATOM_INT(_31044) && DBL_PTR(_31044)->dbl == 0.0){
            DeRef(_31044);
            _31044 = NOVALUE;
            goto L19; // [1126] 1210
        }
        DeRef(_31044);
        _31044 = NOVALUE;
    }
    DeRef(_31044);
    _31044 = NOVALUE;

    /** 					if find( temptok[T_ID], FULL_ID_TOKS) then*/
    _2 = (int)SEQ_PTR(_temptok_61268);
    _31045 = (int)*(((s1_ptr)_2)->base + 1);
    _31046 = find_from(_31045, _28FULL_ID_TOKS_11865, 1);
    _31045 = NOVALUE;
    if (_31046 == 0)
    {
        _31046 = NOVALUE;
        goto L1A; // [1144] 1209
    }
    else{
        _31046 = NOVALUE;
    }

    /** 						if SymTab[tok[T_SYM]][S_SCOPE] = SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_tok_61009);
    _31047 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_31047)){
        _31048 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31047)->dbl));
    }
    else{
        _31048 = (int)*(((s1_ptr)_2)->base + _31047);
    }
    _2 = (int)SEQ_PTR(_31048);
    _31049 = (int)*(((s1_ptr)_2)->base + 4);
    _31048 = NOVALUE;
    if (binary_op_a(NOTEQ, _31049, 9)){
        _31049 = NOVALUE;
        goto L1B; // [1169] 1200
    }
    _31049 = NOVALUE;

    /** 							undef_type = - new_forward_reference( TYPE, tok[T_SYM] )*/
    _2 = (int)SEQ_PTR(_tok_61009);
    _31051 = (int)*(((s1_ptr)_2)->base + 2);
    DeRef(_32353);
    _32353 = 504;
    Ref(_31051);
    _31052 = _29new_forward_reference(504, _31051, 504);
    _31051 = NOVALUE;
    _32353 = NOVALUE;
    if (IS_ATOM_INT(_31052)) {
        if ((unsigned long)_31052 == 0xC0000000)
        _undef_type_61270 = (int)NewDouble((double)-0xC0000000);
        else
        _undef_type_61270 = - _31052;
    }
    else {
        _undef_type_61270 = unary_op(UMINUS, _31052);
    }
    DeRef(_31052);
    _31052 = NOVALUE;
    if (!IS_ATOM_INT(_undef_type_61270)) {
        _1 = (long)(DBL_PTR(_undef_type_61270)->dbl);
        if (UNIQUE(DBL_PTR(_undef_type_61270)) && (DBL_PTR(_undef_type_61270)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_undef_type_61270);
        _undef_type_61270 = _1;
    }
    goto L1C; // [1197] 1208
L1B: 

    /** 							CompileErr(37)*/
    RefDS(_22682);
    _43CompileErr(37, _22682, 0);
L1C: 
L1A: 
L19: 

    /** 				putback(temptok) -- Return whatever came after the name back onto the token stream.*/
    Ref(_temptok_61268);
    _30putback(_temptok_61268);

    /** 				if undef_type != 0 then*/
    if (_undef_type_61270 == 0)
    goto L1D; // [1217] 1232

    /** 					tok[T_SYM] = undef_type*/
    _2 = (int)SEQ_PTR(_tok_61009);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _tok_61009 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _undef_type_61270;
    DeRef(_1);
    goto L1E; // [1229] 1240
L1D: 

    /** 					CompileErr(37)*/
    RefDS(_22682);
    _43CompileErr(37, _22682, 0);
L1E: 
    DeRef(_temptok_61268);
    _temptok_61268 = NOVALUE;
    goto L1F; // [1242] 1253
L18: 

    /** 				CompileErr(37)*/
    RefDS(_22682);
    _43CompileErr(37, _22682, 0);
L1F: 
L16: 

    /** 		type_sym = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_61009);
    _type_sym_61006 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_type_sym_61006)){
        _type_sym_61006 = (long)DBL_PTR(_type_sym_61006)->dbl;
    }

    /** 		tok = next_token()*/
    _0 = _tok_61009;
    _tok_61009 = _30next_token();
    DeRef(_0);

    /** 		if not find(tok[T_ID], ID_TOKS) then*/
    _2 = (int)SEQ_PTR(_tok_61009);
    _31057 = (int)*(((s1_ptr)_2)->base + 1);
    _31058 = find_from(_31057, _28ID_TOKS_11863, 1);
    _31057 = NOVALUE;
    if (_31058 != 0)
    goto L20; // [1284] 1398
    _31058 = NOVALUE;

    /** 			sequence tokcat = find_category(tok[T_ID])*/
    _2 = (int)SEQ_PTR(_tok_61009);
    _31060 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_31060);
    _0 = _tokcat_61319;
    _tokcat_61319 = _63find_category(_31060);
    DeRef(_0);
    _31060 = NOVALUE;

    /** 			if tok[T_SYM] != 0 and length(SymTab[tok[T_SYM]]) >= S_NAME then*/
    _2 = (int)SEQ_PTR(_tok_61009);
    _31062 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_31062)) {
        _31063 = (_31062 != 0);
    }
    else {
        _31063 = binary_op(NOTEQ, _31062, 0);
    }
    _31062 = NOVALUE;
    if (IS_ATOM_INT(_31063)) {
        if (_31063 == 0) {
            goto L21; // [1313] 1374
        }
    }
    else {
        if (DBL_PTR(_31063)->dbl == 0.0) {
            goto L21; // [1313] 1374
        }
    }
    _2 = (int)SEQ_PTR(_tok_61009);
    _31065 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_31065)){
        _31066 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31065)->dbl));
    }
    else{
        _31066 = (int)*(((s1_ptr)_2)->base + _31065);
    }
    if (IS_SEQUENCE(_31066)){
            _31067 = SEQ_PTR(_31066)->length;
    }
    else {
        _31067 = 1;
    }
    _31066 = NOVALUE;
    if (IS_ATOM_INT(_25S_NAME_11913)) {
        _31068 = (_31067 >= _25S_NAME_11913);
    }
    else {
        _31068 = binary_op(GREATEREQ, _31067, _25S_NAME_11913);
    }
    _31067 = NOVALUE;
    if (_31068 == 0) {
        DeRef(_31068);
        _31068 = NOVALUE;
        goto L21; // [1339] 1374
    }
    else {
        if (!IS_ATOM_INT(_31068) && DBL_PTR(_31068)->dbl == 0.0){
            DeRef(_31068);
            _31068 = NOVALUE;
            goto L21; // [1339] 1374
        }
        DeRef(_31068);
        _31068 = NOVALUE;
    }
    DeRef(_31068);
    _31068 = NOVALUE;

    /** 				CompileErr(90, {tokcat, SymTab[tok[T_SYM]][S_NAME]})*/
    _2 = (int)SEQ_PTR(_tok_61009);
    _31069 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_31069)){
        _31070 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31069)->dbl));
    }
    else{
        _31070 = (int)*(((s1_ptr)_2)->base + _31069);
    }
    _2 = (int)SEQ_PTR(_31070);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _31071 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _31071 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _31070 = NOVALUE;
    Ref(_31071);
    RefDS(_tokcat_61319);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _tokcat_61319;
    ((int *)_2)[2] = _31071;
    _31072 = MAKE_SEQ(_1);
    _31071 = NOVALUE;
    _43CompileErr(90, _31072, 0);
    _31072 = NOVALUE;
    goto L22; // [1371] 1397
L21: 

    /** 				CompileErr(92, {LexName(tok[T_ID])})*/
    _2 = (int)SEQ_PTR(_tok_61009);
    _31073 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_31073);
    RefDS(_27188);
    _31074 = _37LexName(_31073, _27188);
    _31073 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _31074;
    _31075 = MAKE_SEQ(_1);
    _31074 = NOVALUE;
    _43CompileErr(92, _31075, 0);
    _31075 = NOVALUE;
L22: 
L20: 
    DeRef(_tokcat_61319);
    _tokcat_61319 = NOVALUE;

    /** 		sym = SetPrivateScope(tok[T_SYM], type_sym, param_num)*/
    _2 = (int)SEQ_PTR(_tok_61009);
    _31076 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_31076);
    _sym_61007 = _30SetPrivateScope(_31076, _type_sym_61006, _30param_num_55180);
    _31076 = NOVALUE;
    if (!IS_ATOM_INT(_sym_61007)) {
        _1 = (long)(DBL_PTR(_sym_61007)->dbl);
        if (UNIQUE(DBL_PTR(_sym_61007)) && (DBL_PTR(_sym_61007)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_61007);
        _sym_61007 = _1;
    }

    /** 		param_num += 1*/
    _30param_num_55180 = _30param_num_55180 + 1;

    /** 		if SymTab[last_link][S_NEXT] != sym*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31079 = (int)*(((s1_ptr)_2)->base + _last_link_61241);
    _2 = (int)SEQ_PTR(_31079);
    _31080 = (int)*(((s1_ptr)_2)->base + 2);
    _31079 = NOVALUE;
    if (IS_ATOM_INT(_31080)) {
        _31081 = (_31080 != _sym_61007);
    }
    else {
        _31081 = binary_op(NOTEQ, _31080, _sym_61007);
    }
    _31080 = NOVALUE;
    if (IS_ATOM_INT(_31081)) {
        if (_31081 == 0) {
            goto L23; // [1444] 1525
        }
    }
    else {
        if (DBL_PTR(_31081)->dbl == 0.0) {
            goto L23; // [1444] 1525
        }
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31083 = (int)*(((s1_ptr)_2)->base + _last_link_61241);
    _2 = (int)SEQ_PTR(_31083);
    _31084 = (int)*(((s1_ptr)_2)->base + 2);
    _31083 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_31084)){
        _31085 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31084)->dbl));
    }
    else{
        _31085 = (int)*(((s1_ptr)_2)->base + _31084);
    }
    _2 = (int)SEQ_PTR(_31085);
    _31086 = (int)*(((s1_ptr)_2)->base + 4);
    _31085 = NOVALUE;
    if (IS_ATOM_INT(_31086)) {
        _31087 = (_31086 == 9);
    }
    else {
        _31087 = binary_op(EQUALS, _31086, 9);
    }
    _31086 = NOVALUE;
    if (_31087 == 0) {
        DeRef(_31087);
        _31087 = NOVALUE;
        goto L23; // [1479] 1525
    }
    else {
        if (!IS_ATOM_INT(_31087) && DBL_PTR(_31087)->dbl == 0.0){
            DeRef(_31087);
            _31087 = NOVALUE;
            goto L23; // [1479] 1525
        }
        DeRef(_31087);
        _31087 = NOVALUE;
    }
    DeRef(_31087);
    _31087 = NOVALUE;

    /** 			SymTab[SymTab[last_link][S_NEXT]][S_NEXT] = 0*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31088 = (int)*(((s1_ptr)_2)->base + _last_link_61241);
    _2 = (int)SEQ_PTR(_31088);
    _31089 = (int)*(((s1_ptr)_2)->base + 2);
    _31088 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_31089))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_31089)->dbl));
    else
    _3 = (int)(_31089 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _31090 = NOVALUE;

    /** 			SymTab[last_link][S_NEXT] = sym*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_last_link_61241 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_61007;
    DeRef(_1);
    _31092 = NOVALUE;
L23: 

    /** 		last_link = sym*/
    _last_link_61241 = _sym_61007;

    /** 		if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L24; // [1536] 1559
    }
    else{
    }

    /** 			SymTab[sym][S_GTYPE] = CompileType(type_sym)*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_61007 + ((s1_ptr)_2)->base);
    _31096 = _30CompileType(_type_sym_61006);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = _31096;
    if( _1 != _31096 ){
        DeRef(_1);
    }
    _31096 = NOVALUE;
    _31094 = NOVALUE;
L24: 

    /** 		tok = next_token()*/
    _0 = _tok_61009;
    _tok_61009 = _30next_token();
    DeRef(_0);

    /** 		if tok[T_ID] = EQUALS then -- defaulted parameter*/
    _2 = (int)SEQ_PTR(_tok_61009);
    _31098 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31098, 3)){
        _31098 = NOVALUE;
        goto L25; // [1574] 1654
    }
    _31098 = NOVALUE;

    /** 			start_recording()*/
    _30start_recording();

    /** 			Expr()*/
    _30Expr();

    /** 			SymTab[sym][S_CODE] = restore_parser()*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_61007 + ((s1_ptr)_2)->base);
    _31102 = _30restore_parser();
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_CODE_11925))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_CODE_11925);
    _1 = *(int *)_2;
    *(int *)_2 = _31102;
    if( _1 != _31102 ){
        DeRef(_1);
    }
    _31102 = NOVALUE;
    _31100 = NOVALUE;

    /** 			if Pop() then end if -- don't leak the default argument*/
    _31103 = _37Pop();
    if (_31103 == 0) {
        DeRef(_31103);
        _31103 = NOVALUE;
        goto L26; // [1609] 1613
    }
    else {
        if (!IS_ATOM_INT(_31103) && DBL_PTR(_31103)->dbl == 0.0){
            DeRef(_31103);
            _31103 = NOVALUE;
            goto L26; // [1609] 1613
        }
        DeRef(_31103);
        _31103 = NOVALUE;
    }
    DeRef(_31103);
    _31103 = NOVALUE;
L26: 

    /** 			tok = next_token()*/
    _0 = _tok_61009;
    _tok_61009 = _30next_token();
    DeRef(_0);

    /** 			if first_def_arg = 0 then*/
    if (_first_def_arg_61011 != 0)
    goto L27; // [1620] 1632

    /** 				first_def_arg = param_num*/
    _first_def_arg_61011 = _30param_num_55180;
L27: 

    /** 			previous_op = -1 -- no interferences betwen defparms, or them and subsequent code*/
    _25previous_op_12379 = -1;

    /** 			if start_def = 0 then*/
    if (_start_def_61239 != 0)
    goto L28; // [1639] 1711

    /** 				start_def = param_num*/
    _start_def_61239 = _30param_num_55180;
    goto L28; // [1651] 1711
L25: 

    /** 			last_nda = param_num*/
    _last_nda_61238 = _30param_num_55180;

    /** 			if start_def then*/
    if (_start_def_61239 == 0)
    {
        goto L29; // [1663] 1710
    }
    else{
    }

    /** 				if start_def = param_num-1 then*/
    _31107 = _30param_num_55180 - 1;
    if ((long)((unsigned long)_31107 +(unsigned long) HIGH_BITS) >= 0){
        _31107 = NewDouble((double)_31107);
    }
    if (binary_op_a(NOTEQ, _start_def_61239, _31107)){
        DeRef(_31107);
        _31107 = NOVALUE;
        goto L2A; // [1674] 1687
    }
    DeRef(_31107);
    _31107 = NOVALUE;

    /** 					middle_def_args &= start_def*/
    Append(&_middle_def_args_61237, _middle_def_args_61237, _start_def_61239);
    goto L2B; // [1684] 1704
L2A: 

    /** 					middle_def_args = append(middle_def_args, {start_def, param_num-1})*/
    _31110 = _30param_num_55180 - 1;
    if ((long)((unsigned long)_31110 +(unsigned long) HIGH_BITS) >= 0){
        _31110 = NewDouble((double)_31110);
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _start_def_61239;
    ((int *)_2)[2] = _31110;
    _31111 = MAKE_SEQ(_1);
    _31110 = NOVALUE;
    RefDS(_31111);
    Append(&_middle_def_args_61237, _middle_def_args_61237, _31111);
    DeRefDS(_31111);
    _31111 = NOVALUE;
L2B: 

    /** 				start_def = 0*/
    _start_def_61239 = 0;
L29: 
L28: 

    /** 		if tok[T_ID] = COMMA then*/
    _2 = (int)SEQ_PTR(_tok_61009);
    _31113 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31113, -30)){
        _31113 = NOVALUE;
        goto L2C; // [1721] 1755
    }
    _31113 = NOVALUE;

    /** 			tok = next_token()*/
    _0 = _tok_61009;
    _tok_61009 = _30next_token();
    DeRef(_0);

    /** 			if tok[T_ID] = RIGHT_ROUND then*/
    _2 = (int)SEQ_PTR(_tok_61009);
    _31116 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31116, -27)){
        _31116 = NOVALUE;
        goto L14; // [1740] 1004
    }
    _31116 = NOVALUE;

    /** 				CompileErr(85)*/
    RefDS(_22682);
    _43CompileErr(85, _22682, 0);
    goto L14; // [1752] 1004
L2C: 

    /** 		elsif tok[T_ID] != RIGHT_ROUND then*/
    _2 = (int)SEQ_PTR(_tok_61009);
    _31118 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _31118, -27)){
        _31118 = NOVALUE;
        goto L14; // [1765] 1004
    }
    _31118 = NOVALUE;

    /** 			CompileErr(41)*/
    RefDS(_22682);
    _43CompileErr(41, _22682, 0);

    /** 	end while*/
    goto L14; // [1780] 1004
L15: 

    /** 	Code = {} -- removes any spurious code emitted while recording parameters*/
    RefDS(_22682);
    DeRef(_25Code_12355);
    _25Code_12355 = _22682;

    /** 	SymTab[p][S_NUM_ARGS] = param_num*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_61005 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    _1 = *(int *)_2;
    *(int *)_2 = _30param_num_55180;
    DeRef(_1);
    _31120 = NOVALUE;

    /** 	SymTab[p][S_DEF_ARGS] = {first_def_arg, last_nda, middle_def_args}*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_61005 + ((s1_ptr)_2)->base);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _first_def_arg_61011;
    *((int *)(_2+8)) = _last_nda_61238;
    RefDS(_middle_def_args_61237);
    *((int *)(_2+12)) = _middle_def_args_61237;
    _31124 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 28);
    _1 = *(int *)_2;
    *(int *)_2 = _31124;
    if( _1 != _31124 ){
        DeRef(_1);
    }
    _31124 = NOVALUE;
    _31122 = NOVALUE;

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L2D; // [1832] 1866
    }
    else{
    }

    /** 		if param_num > max_params then*/
    if (_30param_num_55180 <= _37max_params_51274)
    goto L2E; // [1841] 1855

    /** 			max_params = param_num*/
    _37max_params_51274 = _30param_num_55180;
L2E: 

    /** 		num_routines += 1*/
    _25num_routines_12271 = _25num_routines_12271 + 1;
L2D: 

    /** 	if SymTab[p][S_TOKEN] = TYPE and param_num != 1 then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31127 = (int)*(((s1_ptr)_2)->base + _p_61005);
    _2 = (int)SEQ_PTR(_31127);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _31128 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _31128 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _31127 = NOVALUE;
    if (IS_ATOM_INT(_31128)) {
        _31129 = (_31128 == 504);
    }
    else {
        _31129 = binary_op(EQUALS, _31128, 504);
    }
    _31128 = NOVALUE;
    if (IS_ATOM_INT(_31129)) {
        if (_31129 == 0) {
            goto L2F; // [1886] 1908
        }
    }
    else {
        if (DBL_PTR(_31129)->dbl == 0.0) {
            goto L2F; // [1886] 1908
        }
    }
    _31131 = (_30param_num_55180 != 1);
    if (_31131 == 0)
    {
        DeRef(_31131);
        _31131 = NOVALUE;
        goto L2F; // [1897] 1908
    }
    else{
        DeRef(_31131);
        _31131 = NOVALUE;
    }

    /** 		CompileErr(148)*/
    RefDS(_22682);
    _43CompileErr(148, _22682, 0);
L2F: 

    /** 	include_routine()*/
    _49include_routine();

    /** 	sym = SymTab[p][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31132 = (int)*(((s1_ptr)_2)->base + _p_61005);
    _2 = (int)SEQ_PTR(_31132);
    _sym_61007 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_sym_61007)){
        _sym_61007 = (long)DBL_PTR(_sym_61007)->dbl;
    }
    _31132 = NOVALUE;

    /** 	for i = 1 to SymTab[p][S_NUM_ARGS] do*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31134 = (int)*(((s1_ptr)_2)->base + _p_61005);
    _2 = (int)SEQ_PTR(_31134);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _31135 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _31135 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    _31134 = NOVALUE;
    {
        int _i_61473;
        _i_61473 = 1;
L30: 
        if (binary_op_a(GREATER, _i_61473, _31135)){
            goto L31; // [1942] 2021
        }

        /** 		while SymTab[sym][S_SCOPE] != SC_PRIVATE do*/
L32: 
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _31136 = (int)*(((s1_ptr)_2)->base + _sym_61007);
        _2 = (int)SEQ_PTR(_31136);
        _31137 = (int)*(((s1_ptr)_2)->base + 4);
        _31136 = NOVALUE;
        if (binary_op_a(EQUALS, _31137, 3)){
            _31137 = NOVALUE;
            goto L33; // [1968] 1993
        }
        _31137 = NOVALUE;

        /** 			sym = SymTab[sym][S_NEXT]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _31139 = (int)*(((s1_ptr)_2)->base + _sym_61007);
        _2 = (int)SEQ_PTR(_31139);
        _sym_61007 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_sym_61007)){
            _sym_61007 = (long)DBL_PTR(_sym_61007)->dbl;
        }
        _31139 = NOVALUE;

        /** 		end while*/
        goto L32; // [1990] 1954
L33: 

        /** 		TypeCheck(sym)*/
        _30TypeCheck(_sym_61007);

        /** 		sym = SymTab[sym][S_NEXT]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _31141 = (int)*(((s1_ptr)_2)->base + _sym_61007);
        _2 = (int)SEQ_PTR(_31141);
        _sym_61007 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_sym_61007)){
            _sym_61007 = (long)DBL_PTR(_sym_61007)->dbl;
        }
        _31141 = NOVALUE;

        /** 	end for*/
        _0 = _i_61473;
        if (IS_ATOM_INT(_i_61473)) {
            _i_61473 = _i_61473 + 1;
            if ((long)((unsigned long)_i_61473 +(unsigned long) HIGH_BITS) >= 0){
                _i_61473 = NewDouble((double)_i_61473);
            }
        }
        else {
            _i_61473 = binary_op_a(PLUS, _i_61473, 1);
        }
        DeRef(_0);
        goto L30; // [2016] 1949
L31: 
        ;
        DeRef(_i_61473);
    }

    /** 	tok = next_token()*/
    _0 = _tok_61009;
    _tok_61009 = _30next_token();
    DeRef(_0);

    /** 	while tok[T_ID] = TYPE or tok[T_ID] = QUALIFIED_TYPE do*/
L34: 
    _2 = (int)SEQ_PTR(_tok_61009);
    _31144 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31144)) {
        _31145 = (_31144 == 504);
    }
    else {
        _31145 = binary_op(EQUALS, _31144, 504);
    }
    _31144 = NOVALUE;
    if (IS_ATOM_INT(_31145)) {
        if (_31145 != 0) {
            goto L35; // [2043] 2064
        }
    }
    else {
        if (DBL_PTR(_31145)->dbl != 0.0) {
            goto L35; // [2043] 2064
        }
    }
    _2 = (int)SEQ_PTR(_tok_61009);
    _31147 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31147)) {
        _31148 = (_31147 == 522);
    }
    else {
        _31148 = binary_op(EQUALS, _31147, 522);
    }
    _31147 = NOVALUE;
    if (_31148 <= 0) {
        if (_31148 == 0) {
            DeRef(_31148);
            _31148 = NOVALUE;
            goto L36; // [2060] 2085
        }
        else {
            if (!IS_ATOM_INT(_31148) && DBL_PTR(_31148)->dbl == 0.0){
                DeRef(_31148);
                _31148 = NOVALUE;
                goto L36; // [2060] 2085
            }
            DeRef(_31148);
            _31148 = NOVALUE;
        }
    }
    DeRef(_31148);
    _31148 = NOVALUE;
L35: 

    /** 		Private_declaration(tok[T_SYM])*/
    _2 = (int)SEQ_PTR(_tok_61009);
    _31149 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_31149);
    _30Private_declaration(_31149);
    _31149 = NOVALUE;

    /** 		tok = next_token()*/
    _0 = _tok_61009;
    _tok_61009 = _30next_token();
    DeRef(_0);

    /** 	end while*/
    goto L34; // [2082] 2031
L36: 

    /** 	if not TRANSLATE then*/
    if (_25TRANSLATE_11874 != 0)
    goto L37; // [2089] 2192

    /** 		if OpTrace then*/
    if (_25OpTrace_12332 == 0)
    {
        goto L38; // [2096] 2191
    }
    else{
    }

    /** 			emit_op(ERASE_PRIVATE_NAMES)*/
    _37emit_op(88);

    /** 			emit_addr(p)*/
    _37emit_addr(_p_61005);

    /** 			sym = SymTab[p][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31152 = (int)*(((s1_ptr)_2)->base + _p_61005);
    _2 = (int)SEQ_PTR(_31152);
    _sym_61007 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_sym_61007)){
        _sym_61007 = (long)DBL_PTR(_sym_61007)->dbl;
    }
    _31152 = NOVALUE;

    /** 			for i = 1 to SymTab[p][S_NUM_ARGS] do*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31154 = (int)*(((s1_ptr)_2)->base + _p_61005);
    _2 = (int)SEQ_PTR(_31154);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _31155 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _31155 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    _31154 = NOVALUE;
    {
        int _i_61520;
        _i_61520 = 1;
L39: 
        if (binary_op_a(GREATER, _i_61520, _31155)){
            goto L3A; // [2141] 2183
        }

        /** 				emit_op(DISPLAY_VAR)*/
        _37emit_op(87);

        /** 				emit_addr(sym)*/
        _37emit_addr(_sym_61007);

        /** 				sym = SymTab[sym][S_NEXT]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _31156 = (int)*(((s1_ptr)_2)->base + _sym_61007);
        _2 = (int)SEQ_PTR(_31156);
        _sym_61007 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_sym_61007)){
            _sym_61007 = (long)DBL_PTR(_sym_61007)->dbl;
        }
        _31156 = NOVALUE;

        /** 			end for*/
        _0 = _i_61520;
        if (IS_ATOM_INT(_i_61520)) {
            _i_61520 = _i_61520 + 1;
            if ((long)((unsigned long)_i_61520 +(unsigned long) HIGH_BITS) >= 0){
                _i_61520 = NewDouble((double)_i_61520);
            }
        }
        else {
            _i_61520 = binary_op_a(PLUS, _i_61520, 1);
        }
        DeRef(_0);
        goto L39; // [2178] 2148
L3A: 
        ;
        DeRef(_i_61520);
    }

    /** 			emit_op(UPDATE_GLOBALS)*/
    _37emit_op(89);
L38: 
L37: 

    /** 	putback(tok)*/
    Ref(_tok_61009);
    _30putback(_tok_61009);

    /** 	FuncReturn = FALSE*/
    _30FuncReturn_55179 = _5FALSE_242;

    /** 	if type_enum then*/
    if (_type_enum_61013 == 0)
    {
        goto L3B; // [2208] 2448
    }
    else{
    }

    /** 		stmt_nest += 1*/
    _30stmt_nest_55203 = _30stmt_nest_55203 + 1;

    /** 		tok_match(RETURN)*/
    _30tok_match(413, 0);

    /** 		emit_opnd(i1_sym[T_SYM])*/
    _2 = (int)SEQ_PTR(_i1_sym_61014);
    _31159 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_31159);
    _37emit_opnd(_31159);
    _31159 = NOVALUE;

    /** 		emit_opnd(enum_syms[1])*/
    _2 = (int)SEQ_PTR(_enum_syms_61015);
    _31160 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_31160);
    _37emit_opnd(_31160);
    _31160 = NOVALUE;

    /** 		emit_op(EQUAL)*/
    _37emit_op(153);

    /** 		SymTab[Top()][S_USAGE] = U_USED*/
    _31161 = _37Top();
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_31161))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_31161)->dbl));
    else
    _3 = (int)(_31161 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 3;
    DeRef(_1);
    _31162 = NOVALUE;

    /** 		for ei = 2 to length(enum_syms) do*/
    if (IS_SEQUENCE(_enum_syms_61015)){
            _31164 = SEQ_PTR(_enum_syms_61015)->length;
    }
    else {
        _31164 = 1;
    }
    {
        int _ei_61553;
        _ei_61553 = 2;
L3C: 
        if (_ei_61553 > _31164){
            goto L3D; // [2281] 2391
        }

        /** 			symtab_index last_comparison = Top()*/
        _last_comparison_61556 = _37Top();
        if (!IS_ATOM_INT(_last_comparison_61556)) {
            _1 = (long)(DBL_PTR(_last_comparison_61556)->dbl);
            if (UNIQUE(DBL_PTR(_last_comparison_61556)) && (DBL_PTR(_last_comparison_61556)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_last_comparison_61556);
            _last_comparison_61556 = _1;
        }

        /** 			emit_opnd(i1_sym[T_SYM])*/
        _2 = (int)SEQ_PTR(_i1_sym_61014);
        _31166 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_31166);
        _37emit_opnd(_31166);
        _31166 = NOVALUE;

        /** 			emit_opnd(enum_syms[ei])*/
        _2 = (int)SEQ_PTR(_enum_syms_61015);
        _31167 = (int)*(((s1_ptr)_2)->base + _ei_61553);
        Ref(_31167);
        _37emit_opnd(_31167);
        _31167 = NOVALUE;

        /** 			emit_op(EQUAL)*/
        _37emit_op(153);

        /** 			SymTab[Top()][S_USAGE] = U_USED*/
        _31168 = _37Top();
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_31168))
        _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_31168)->dbl));
        else
        _3 = (int)(_31168 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = 3;
        DeRef(_1);
        _31169 = NOVALUE;

        /** 			emit_opnd(Top())*/
        _31171 = _37Top();
        _37emit_opnd(_31171);
        _31171 = NOVALUE;

        /** 			emit_opnd(last_comparison)*/
        _37emit_opnd(_last_comparison_61556);

        /** 			emit_op(OR)*/
        _37emit_op(9);

        /** 			SymTab[Top()][S_USAGE] = U_USED*/
        _31172 = _37Top();
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_31172))
        _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_31172)->dbl));
        else
        _3 = (int)(_31172 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = 3;
        DeRef(_1);
        _31173 = NOVALUE;

        /** 		end for*/
        _ei_61553 = _ei_61553 + 1;
        goto L3C; // [2386] 2288
L3D: 
        ;
    }

    /** 		emit_opnd(Top())*/
    _31175 = _37Top();
    _37emit_opnd(_31175);
    _31175 = NOVALUE;

    /** 		emit_op(RETURNF)*/
    _37emit_op(28);

    /** 		flush_temps()*/
    RefDS(_22682);
    _37flush_temps(_22682);

    /** 		stmt_nest -= 1*/
    _30stmt_nest_55203 = _30stmt_nest_55203 - 1;

    /** 		InitDelete()*/
    _30InitDelete();

    /** 		flush_temps()*/
    RefDS(_22682);
    _37flush_temps(_22682);

    /** 		FuncReturn = TRUE*/
    _30FuncReturn_55179 = _5TRUE_244;

    /** 		tok_match(END)*/
    _30tok_match(402, 0);
    goto L3E; // [2445] 2461
L3B: 

    /** 		Statement_list()*/
    _30Statement_list();

    /** 		tok_match(END)*/
    _30tok_match(402, 0);
L3E: 

    /** 	tok_match(prog_type, END)*/
    _30tok_match(_prog_type_61000, 402);

    /** 	if prog_type != PROCEDURE then*/
    if (_prog_type_61000 == 405)
    goto L3F; // [2473] 2521

    /** 		if not FuncReturn then*/
    if (_30FuncReturn_55179 != 0)
    goto L40; // [2481] 2511

    /** 			if prog_type = FUNCTION then*/
    if (_prog_type_61000 != 406)
    goto L41; // [2488] 2502

    /** 				CompileErr(120)*/
    RefDS(_22682);
    _43CompileErr(120, _22682, 0);
    goto L42; // [2499] 2510
L41: 

    /** 				CompileErr(149)*/
    RefDS(_22682);
    _43CompileErr(149, _22682, 0);
L42: 
L40: 

    /** 		emit_op(BADRETURNF) -- function/type shouldn't reach here*/
    _37emit_op(43);
    goto L43; // [2518] 2583
L3F: 

    /** 		StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 		if not TRANSLATE then*/
    if (_25TRANSLATE_11874 != 0)
    goto L44; // [2536] 2560

    /** 			if OpTrace then*/
    if (_25OpTrace_12332 == 0)
    {
        goto L45; // [2543] 2559
    }
    else{
    }

    /** 				emit_op(ERASE_PRIVATE_NAMES)*/
    _37emit_op(88);

    /** 				emit_addr(p)*/
    _37emit_addr(_p_61005);
L45: 
L44: 

    /** 		emit_op(RETURNP)*/
    _37emit_op(29);

    /** 		if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L46; // [2571] 2582
    }
    else{
    }

    /** 			emit_op(BADRETURNF) -- just to mark end of procedure*/
    _37emit_op(43);
L46: 
L43: 

    /** 	Drop_block( pt )*/
    _66Drop_block(_pt_61003);

    /** 	if Strict_Override > 0 then*/
    if (_25Strict_Override_12329 <= 0)
    goto L47; // [2592] 2607

    /** 		Strict_Override -= 1	-- Reset at the end of each routine.*/
    _25Strict_Override_12329 = _25Strict_Override_12329 - 1;
L47: 

    /** 	SymTab[p][S_STACK_SPACE] += temps_allocated + param_num*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_61005 + ((s1_ptr)_2)->base);
    _31185 = _52temps_allocated_47621 + _30param_num_55180;
    if ((long)((unsigned long)_31185 + (unsigned long)HIGH_BITS) >= 0) 
    _31185 = NewDouble((double)_31185);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!IS_ATOM_INT(_25S_STACK_SPACE_11973)){
        _31186 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_STACK_SPACE_11973)->dbl));
    }
    else{
        _31186 = (int)*(((s1_ptr)_2)->base + _25S_STACK_SPACE_11973);
    }
    _31183 = NOVALUE;
    if (IS_ATOM_INT(_31186) && IS_ATOM_INT(_31185)) {
        _31187 = _31186 + _31185;
        if ((long)((unsigned long)_31187 + (unsigned long)HIGH_BITS) >= 0) 
        _31187 = NewDouble((double)_31187);
    }
    else {
        _31187 = binary_op(PLUS, _31186, _31185);
    }
    _31186 = NOVALUE;
    DeRef(_31185);
    _31185 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_STACK_SPACE_11973))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_STACK_SPACE_11973)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_STACK_SPACE_11973);
    _1 = *(int *)_2;
    *(int *)_2 = _31187;
    if( _1 != _31187 ){
        DeRef(_1);
    }
    _31187 = NOVALUE;
    _31183 = NOVALUE;

    /** 	if temps_allocated + param_num > max_stack_per_call then*/
    _31188 = _52temps_allocated_47621 + _30param_num_55180;
    if ((long)((unsigned long)_31188 + (unsigned long)HIGH_BITS) >= 0) 
    _31188 = NewDouble((double)_31188);
    if (binary_op_a(LESSEQ, _31188, _25max_stack_per_call_12380)){
        DeRef(_31188);
        _31188 = NOVALUE;
        goto L48; // [2650] 2667
    }
    DeRef(_31188);
    _31188 = NOVALUE;

    /** 		max_stack_per_call = temps_allocated + param_num*/
    _25max_stack_per_call_12380 = _52temps_allocated_47621 + _30param_num_55180;
L48: 

    /** 	param_num = -1*/
    _30param_num_55180 = -1;

    /** 	StraightenBranches()*/
    _30StraightenBranches();

    /** 	check_inline( p )*/
    _67check_inline(_p_61005);

    /** 	param_num = -1*/
    _30param_num_55180 = -1;

    /** 	EnterTopLevel()*/
    _30EnterTopLevel(1);

    /** 	if length( enum_syms ) then*/
    if (IS_SEQUENCE(_enum_syms_61015)){
            _31191 = SEQ_PTR(_enum_syms_61015)->length;
    }
    else {
        _31191 = 1;
    }
    if (_31191 == 0)
    {
        _31191 = NOVALUE;
        goto L49; // [2696] 2783
    }
    else{
        _31191 = NOVALUE;
    }

    /** 		SymTab[p][S_NEXT] = SymTab[enum_syms[$]][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_61005 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_enum_syms_61015)){
            _31194 = SEQ_PTR(_enum_syms_61015)->length;
    }
    else {
        _31194 = 1;
    }
    _2 = (int)SEQ_PTR(_enum_syms_61015);
    _31195 = (int)*(((s1_ptr)_2)->base + _31194);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_31195)){
        _31196 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31195)->dbl));
    }
    else{
        _31196 = (int)*(((s1_ptr)_2)->base + _31195);
    }
    _2 = (int)SEQ_PTR(_31196);
    _31197 = (int)*(((s1_ptr)_2)->base + 2);
    _31196 = NOVALUE;
    Ref(_31197);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _31197;
    if( _1 != _31197 ){
        DeRef(_1);
    }
    _31197 = NOVALUE;
    _31192 = NOVALUE;

    /** 		SymTab[last_sym][S_NEXT] = enum_syms[1]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_52last_sym_47110 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_enum_syms_61015);
    _31200 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_31200);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _31200;
    if( _1 != _31200 ){
        DeRef(_1);
    }
    _31200 = NOVALUE;
    _31198 = NOVALUE;

    /** 		last_sym = enum_syms[$]*/
    if (IS_SEQUENCE(_enum_syms_61015)){
            _31201 = SEQ_PTR(_enum_syms_61015)->length;
    }
    else {
        _31201 = 1;
    }
    _2 = (int)SEQ_PTR(_enum_syms_61015);
    _52last_sym_47110 = (int)*(((s1_ptr)_2)->base + _31201);
    if (!IS_ATOM_INT(_52last_sym_47110)){
        _52last_sym_47110 = (long)DBL_PTR(_52last_sym_47110)->dbl;
    }

    /** 		SymTab[last_sym][S_NEXT] = 0*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_52last_sym_47110 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _31203 = NOVALUE;
L49: 

    /** end procedure*/
    DeRef(_tok_61009);
    DeRef(_prog_name_61010);
    DeRef(_i1_sym_61014);
    DeRef(_enum_syms_61015);
    DeRef(_middle_def_args_61237);
    _31047 = NOVALUE;
    DeRef(_31063);
    _31063 = NOVALUE;
    _31003 = NOVALUE;
    _31069 = NOVALUE;
    _30999 = NOVALUE;
    _31089 = NOVALUE;
    DeRef(_31041);
    _31041 = NOVALUE;
    _31195 = NOVALUE;
    _31065 = NOVALUE;
    _31084 = NOVALUE;
    DeRef(_31161);
    _31161 = NOVALUE;
    DeRef(_31035);
    _31035 = NOVALUE;
    _31155 = NOVALUE;
    DeRef(_31168);
    _31168 = NOVALUE;
    DeRef(_31129);
    _31129 = NOVALUE;
    DeRef(_30976);
    _30976 = NOVALUE;
    DeRef(_31030);
    _31030 = NOVALUE;
    DeRef(_31172);
    _31172 = NOVALUE;
    _31066 = NOVALUE;
    DeRef(_31145);
    _31145 = NOVALUE;
    DeRef(_31081);
    _31081 = NOVALUE;
    _31135 = NOVALUE;
    return;
    ;
}


void _30InitGlobals()
{
    int _31222 = NOVALUE;
    int _31220 = NOVALUE;
    int _31219 = NOVALUE;
    int _31218 = NOVALUE;
    int _31217 = NOVALUE;
    int _31216 = NOVALUE;
    int _31215 = NOVALUE;
    int _31213 = NOVALUE;
    int _31212 = NOVALUE;
    int _31211 = NOVALUE;
    int _31210 = NOVALUE;
    int _31208 = NOVALUE;
    int _31207 = NOVALUE;
    int _31206 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ResetTP()*/
    _60ResetTP();

    /** 	OpTypeCheck = TRUE*/
    _25OpTypeCheck_12333 = _5TRUE_244;

    /** 	OpDefines &= {*/
    _31206 = _31version_major();
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _31206;
    _31207 = MAKE_SEQ(_1);
    _31206 = NOVALUE;
    _31208 = EPrintf(-9999999, _31205, _31207);
    DeRefDS(_31207);
    _31207 = NOVALUE;
    _31210 = _31version_major();
    _31211 = _31version_minor();
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _31210;
    ((int *)_2)[2] = _31211;
    _31212 = MAKE_SEQ(_1);
    _31211 = NOVALUE;
    _31210 = NOVALUE;
    _31213 = EPrintf(-9999999, _31209, _31212);
    DeRefDS(_31212);
    _31212 = NOVALUE;
    _31215 = _31version_major();
    _31216 = _31version_minor();
    _31217 = _31version_patch();
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _31215;
    *((int *)(_2+8)) = _31216;
    *((int *)(_2+12)) = _31217;
    _31218 = MAKE_SEQ(_1);
    _31217 = NOVALUE;
    _31216 = NOVALUE;
    _31215 = NOVALUE;
    _31219 = EPrintf(-9999999, _31214, _31218);
    DeRefDS(_31218);
    _31218 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _31208;
    *((int *)(_2+8)) = _31213;
    *((int *)(_2+12)) = _31219;
    _31220 = MAKE_SEQ(_1);
    _31219 = NOVALUE;
    _31213 = NOVALUE;
    _31208 = NOVALUE;
    Concat((object_ptr)&_25OpDefines_12336, _25OpDefines_12336, _31220);
    DeRefDS(_31220);
    _31220 = NOVALUE;

    /** 	OpDefines &= GetPlatformDefines()*/
    _31222 = _36GetPlatformDefines(0);
    if (IS_SEQUENCE(_25OpDefines_12336) && IS_ATOM(_31222)) {
        Ref(_31222);
        Append(&_25OpDefines_12336, _25OpDefines_12336, _31222);
    }
    else if (IS_ATOM(_25OpDefines_12336) && IS_SEQUENCE(_31222)) {
    }
    else {
        Concat((object_ptr)&_25OpDefines_12336, _25OpDefines_12336, _31222);
    }
    DeRef(_31222);
    _31222 = NOVALUE;

    /** 	OpInline = DEFAULT_INLINE*/
    _25OpInline_12340 = 30;

    /** 	OpIndirectInclude = 1*/
    _25OpIndirectInclude_12341 = 1;

    /** end procedure*/
    return;
    ;
}


void _30not_supported_compile(int _feature_61716)
{
    int _31224 = NOVALUE;
    int _0, _1, _2;
    

    /** 	CompileErr(5, {feature, version_name})*/
    RefDS(_25version_name_11888);
    RefDS(_feature_61716);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _feature_61716;
    ((int *)_2)[2] = _25version_name_11888;
    _31224 = MAKE_SEQ(_1);
    _43CompileErr(5, _31224, 0);
    _31224 = NOVALUE;

    /** end procedure*/
    DeRefDSi(_feature_61716);
    return;
    ;
}


void _30SetWith(int _on_off_61722)
{
    int _option_61723 = NOVALUE;
    int _idx_61724 = NOVALUE;
    int _reset_flags_61725 = NOVALUE;
    int _tok_61777 = NOVALUE;
    int _good_sofar_61825 = NOVALUE;
    int _tok_61828 = NOVALUE;
    int _warning_extra_61830 = NOVALUE;
    int _endlist_61921 = NOVALUE;
    int _tok_62064 = NOVALUE;
    int _31369 = NOVALUE;
    int _31368 = NOVALUE;
    int _31367 = NOVALUE;
    int _31366 = NOVALUE;
    int _31365 = NOVALUE;
    int _31362 = NOVALUE;
    int _31361 = NOVALUE;
    int _31360 = NOVALUE;
    int _31358 = NOVALUE;
    int _31356 = NOVALUE;
    int _31353 = NOVALUE;
    int _31351 = NOVALUE;
    int _31350 = NOVALUE;
    int _31349 = NOVALUE;
    int _31348 = NOVALUE;
    int _31347 = NOVALUE;
    int _31343 = NOVALUE;
    int _31341 = NOVALUE;
    int _31339 = NOVALUE;
    int _31335 = NOVALUE;
    int _31330 = NOVALUE;
    int _31329 = NOVALUE;
    int _31324 = NOVALUE;
    int _31323 = NOVALUE;
    int _31322 = NOVALUE;
    int _31321 = NOVALUE;
    int _31320 = NOVALUE;
    int _31319 = NOVALUE;
    int _31318 = NOVALUE;
    int _31317 = NOVALUE;
    int _31316 = NOVALUE;
    int _31315 = NOVALUE;
    int _31313 = NOVALUE;
    int _31312 = NOVALUE;
    int _31310 = NOVALUE;
    int _31309 = NOVALUE;
    int _31308 = NOVALUE;
    int _31306 = NOVALUE;
    int _31305 = NOVALUE;
    int _31303 = NOVALUE;
    int _31300 = NOVALUE;
    int _31298 = NOVALUE;
    int _31295 = NOVALUE;
    int _31294 = NOVALUE;
    int _31293 = NOVALUE;
    int _31292 = NOVALUE;
    int _31285 = NOVALUE;
    int _31284 = NOVALUE;
    int _31282 = NOVALUE;
    int _31279 = NOVALUE;
    int _31278 = NOVALUE;
    int _31276 = NOVALUE;
    int _31275 = NOVALUE;
    int _31274 = NOVALUE;
    int _31273 = NOVALUE;
    int _31272 = NOVALUE;
    int _31271 = NOVALUE;
    int _31268 = NOVALUE;
    int _31267 = NOVALUE;
    int _31266 = NOVALUE;
    int _31265 = NOVALUE;
    int _31264 = NOVALUE;
    int _31263 = NOVALUE;
    int _31260 = NOVALUE;
    int _31259 = NOVALUE;
    int _31258 = NOVALUE;
    int _31256 = NOVALUE;
    int _31253 = NOVALUE;
    int _31251 = NOVALUE;
    int _31250 = NOVALUE;
    int _31248 = NOVALUE;
    int _31247 = NOVALUE;
    int _31246 = NOVALUE;
    int _31245 = NOVALUE;
    int _31244 = NOVALUE;
    int _31243 = NOVALUE;
    int _31241 = NOVALUE;
    int _31238 = NOVALUE;
    int _31237 = NOVALUE;
    int _31236 = NOVALUE;
    int _31235 = NOVALUE;
    int _31233 = NOVALUE;
    int _31232 = NOVALUE;
    int _31231 = NOVALUE;
    int _31230 = NOVALUE;
    int _31228 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer reset_flags = 1*/
    _reset_flags_61725 = 1;

    /** 	option = StringToken("&+=")*/
    RefDS(_31225);
    _0 = _option_61723;
    _option_61723 = _60StringToken(_31225);
    DeRef(_0);

    /** 	if equal(option, "type_check") then*/
    if (_option_61723 == _31227)
    _31228 = 1;
    else if (IS_ATOM_INT(_option_61723) && IS_ATOM_INT(_31227))
    _31228 = 0;
    else
    _31228 = (compare(_option_61723, _31227) == 0);
    if (_31228 == 0)
    {
        _31228 = NOVALUE;
        goto L1; // [22] 35
    }
    else{
        _31228 = NOVALUE;
    }

    /** 		OpTypeCheck = on_off*/
    _25OpTypeCheck_12333 = _on_off_61722;
    goto L2; // [32] 1521
L1: 

    /** 	elsif equal(option, "profile") then*/
    if (_option_61723 == _31229)
    _31230 = 1;
    else if (IS_ATOM_INT(_option_61723) && IS_ATOM_INT(_31229))
    _31230 = 0;
    else
    _31230 = (compare(_option_61723, _31229) == 0);
    if (_31230 == 0)
    {
        _31230 = NOVALUE;
        goto L3; // [41] 121
    }
    else{
        _31230 = NOVALUE;
    }

    /** 		if not TRANSLATE and not BIND then*/
    _31231 = (_25TRANSLATE_11874 == 0);
    if (_31231 == 0) {
        goto L2; // [51] 1521
    }
    _31233 = (_25BIND_11877 == 0);
    if (_31233 == 0)
    {
        DeRef(_31233);
        _31233 = NOVALUE;
        goto L2; // [61] 1521
    }
    else{
        DeRef(_31233);
        _31233 = NOVALUE;
    }

    /** 			OpProfileStatement = on_off*/
    _25OpProfileStatement_12334 = _on_off_61722;

    /** 			if OpProfileStatement then*/
    if (_25OpProfileStatement_12334 == 0)
    {
        goto L2; // [75] 1521
    }
    else{
    }

    /** 				if AnyTimeProfile then*/
    if (_26AnyTimeProfile_11162 == 0)
    {
        goto L4; // [82] 106
    }
    else{
    }

    /** 					Warning(224, mixed_profile_warning_flag)*/
    RefDS(_22682);
    _43Warning(224, 1024, _22682);

    /** 					OpProfileStatement = FALSE*/
    _25OpProfileStatement_12334 = _5FALSE_242;
    goto L2; // [103] 1521
L4: 

    /** 					AnyStatementProfile = TRUE*/
    _26AnyStatementProfile_11163 = _5TRUE_244;
    goto L2; // [118] 1521
L3: 

    /** 	elsif equal(option, "profile_time") then*/
    if (_option_61723 == _31234)
    _31235 = 1;
    else if (IS_ATOM_INT(_option_61723) && IS_ATOM_INT(_31234))
    _31235 = 0;
    else
    _31235 = (compare(_option_61723, _31234) == 0);
    if (_31235 == 0)
    {
        _31235 = NOVALUE;
        goto L5; // [127] 361
    }
    else{
        _31235 = NOVALUE;
    }

    /** 		if not TRANSLATE and not BIND then*/
    _31236 = (_25TRANSLATE_11874 == 0);
    if (_31236 == 0) {
        goto L2; // [137] 1521
    }
    _31238 = (_25BIND_11877 == 0);
    if (_31238 == 0)
    {
        DeRef(_31238);
        _31238 = NOVALUE;
        goto L2; // [147] 1521
    }
    else{
        DeRef(_31238);
        _31238 = NOVALUE;
    }

    /** 			if not IWINDOWS then*/
    if (_36IWINDOWS_14720 != 0)
    goto L6; // [154] 169

    /** 				if on_off then*/
    if (_on_off_61722 == 0)
    {
        goto L7; // [159] 168
    }
    else{
    }

    /** 					not_supported_compile("profile_time")*/
    RefDS(_31234);
    _30not_supported_compile(_31234);
L7: 
L6: 

    /** 			OpProfileTime = on_off*/
    _25OpProfileTime_12335 = _on_off_61722;

    /** 			if OpProfileTime then*/
    if (_25OpProfileTime_12335 == 0)
    {
        goto L8; // [180] 355
    }
    else{
    }

    /** 				if AnyStatementProfile then*/
    if (_26AnyStatementProfile_11163 == 0)
    {
        goto L9; // [187] 209
    }
    else{
    }

    /** 					Warning(224,mixed_profile_warning_flag)*/
    RefDS(_22682);
    _43Warning(224, 1024, _22682);

    /** 					OpProfileTime = FALSE*/
    _25OpProfileTime_12335 = _5FALSE_242;
L9: 

    /** 				token tok = next_token()*/
    _0 = _tok_61777;
    _tok_61777 = _30next_token();
    DeRef(_0);

    /** 				if tok[T_ID] = ATOM then*/
    _2 = (int)SEQ_PTR(_tok_61777);
    _31241 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31241, 502)){
        _31241 = NOVALUE;
        goto LA; // [224] 316
    }
    _31241 = NOVALUE;

    /** 					if integer(SymTab[tok[T_SYM]][S_OBJ]) then*/
    _2 = (int)SEQ_PTR(_tok_61777);
    _31243 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_31243)){
        _31244 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31243)->dbl));
    }
    else{
        _31244 = (int)*(((s1_ptr)_2)->base + _31243);
    }
    _2 = (int)SEQ_PTR(_31244);
    _31245 = (int)*(((s1_ptr)_2)->base + 1);
    _31244 = NOVALUE;
    if (IS_ATOM_INT(_31245))
    _31246 = 1;
    else if (IS_ATOM_DBL(_31245))
    _31246 = IS_ATOM_INT(DoubleToInt(_31245));
    else
    _31246 = 0;
    _31245 = NOVALUE;
    if (_31246 == 0)
    {
        _31246 = NOVALUE;
        goto LB; // [251] 279
    }
    else{
        _31246 = NOVALUE;
    }

    /** 						sample_size = SymTab[tok[T_SYM]][S_OBJ]*/
    _2 = (int)SEQ_PTR(_tok_61777);
    _31247 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_31247)){
        _31248 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31247)->dbl));
    }
    else{
        _31248 = (int)*(((s1_ptr)_2)->base + _31247);
    }
    _2 = (int)SEQ_PTR(_31248);
    _25sample_size_12381 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_25sample_size_12381)){
        _25sample_size_12381 = (long)DBL_PTR(_25sample_size_12381)->dbl;
    }
    _31248 = NOVALUE;
    goto LC; // [276] 287
LB: 

    /** 						sample_size = -1*/
    _25sample_size_12381 = -1;
LC: 

    /** 					if sample_size < 1 and OpProfileTime then*/
    _31250 = (_25sample_size_12381 < 1);
    if (_31250 == 0) {
        goto LD; // [295] 329
    }
    if (_25OpProfileTime_12335 == 0)
    {
        goto LD; // [302] 329
    }
    else{
    }

    /** 						CompileErr(136)*/
    RefDS(_22682);
    _43CompileErr(136, _22682, 0);
    goto LD; // [313] 329
LA: 

    /** 					putback(tok)*/
    Ref(_tok_61777);
    _30putback(_tok_61777);

    /** 					sample_size = DEFAULT_SAMPLE_SIZE*/
    _25sample_size_12381 = 25000;
LD: 

    /** 				if OpProfileTime then*/
    if (_25OpProfileTime_12335 == 0)
    {
        goto LE; // [333] 354
    }
    else{
    }

    /** 					if IWINDOWS then*/
    if (_36IWINDOWS_14720 == 0)
    {
        goto LF; // [340] 353
    }
    else{
    }

    /** 						AnyTimeProfile = TRUE*/
    _26AnyTimeProfile_11162 = _5TRUE_244;
LF: 
LE: 
L8: 
    DeRef(_tok_61777);
    _tok_61777 = NOVALUE;
    goto L2; // [358] 1521
L5: 

    /** 	elsif equal(option, "trace") then*/
    if (_option_61723 == _31252)
    _31253 = 1;
    else if (IS_ATOM_INT(_option_61723) && IS_ATOM_INT(_31252))
    _31253 = 0;
    else
    _31253 = (compare(_option_61723, _31252) == 0);
    if (_31253 == 0)
    {
        _31253 = NOVALUE;
        goto L10; // [367] 388
    }
    else{
        _31253 = NOVALUE;
    }

    /** 		if not BIND then*/
    if (_25BIND_11877 != 0)
    goto L2; // [374] 1521

    /** 			OpTrace = on_off*/
    _25OpTrace_12332 = _on_off_61722;
    goto L2; // [385] 1521
L10: 

    /** 	elsif equal(option, "warning") then*/
    if (_option_61723 == _31255)
    _31256 = 1;
    else if (IS_ATOM_INT(_option_61723) && IS_ATOM_INT(_31255))
    _31256 = 0;
    else
    _31256 = (compare(_option_61723, _31255) == 0);
    if (_31256 == 0)
    {
        _31256 = NOVALUE;
        goto L11; // [394] 1234
    }
    else{
        _31256 = NOVALUE;
    }

    /** 		integer good_sofar = line_number*/
    _good_sofar_61825 = _25line_number_12263;

    /** 		reset_flags = 1*/
    _reset_flags_61725 = 1;

    /** 		token tok = next_token()*/
    _0 = _tok_61828;
    _tok_61828 = _30next_token();
    DeRef(_0);

    /** 		integer warning_extra = 1*/
    _warning_extra_61830 = 1;

    /** 		if find(tok[T_ID], {CONCAT_EQUALS, PLUS_EQUALS}) != 0 then*/
    _2 = (int)SEQ_PTR(_tok_61828);
    _31258 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _28CONCAT_EQUALS_11738;
    ((int *)_2)[2] = 515;
    _31259 = MAKE_SEQ(_1);
    _31260 = find_from(_31258, _31259, 1);
    _31258 = NOVALUE;
    DeRefDS(_31259);
    _31259 = NOVALUE;
    if (_31260 == 0)
    goto L12; // [442] 501

    /** 			tok = next_token()*/
    _0 = _tok_61828;
    _tok_61828 = _30next_token();
    DeRef(_0);

    /** 			if tok[T_ID] != LEFT_BRACE and tok[T_ID] != LEFT_ROUND then*/
    _2 = (int)SEQ_PTR(_tok_61828);
    _31263 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31263)) {
        _31264 = (_31263 != -24);
    }
    else {
        _31264 = binary_op(NOTEQ, _31263, -24);
    }
    _31263 = NOVALUE;
    if (IS_ATOM_INT(_31264)) {
        if (_31264 == 0) {
            goto L13; // [465] 493
        }
    }
    else {
        if (DBL_PTR(_31264)->dbl == 0.0) {
            goto L13; // [465] 493
        }
    }
    _2 = (int)SEQ_PTR(_tok_61828);
    _31266 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31266)) {
        _31267 = (_31266 != -26);
    }
    else {
        _31267 = binary_op(NOTEQ, _31266, -26);
    }
    _31266 = NOVALUE;
    if (_31267 == 0) {
        DeRef(_31267);
        _31267 = NOVALUE;
        goto L13; // [482] 493
    }
    else {
        if (!IS_ATOM_INT(_31267) && DBL_PTR(_31267)->dbl == 0.0){
            DeRef(_31267);
            _31267 = NOVALUE;
            goto L13; // [482] 493
        }
        DeRef(_31267);
        _31267 = NOVALUE;
    }
    DeRef(_31267);
    _31267 = NOVALUE;

    /** 				CompileErr(160)*/
    RefDS(_22682);
    _43CompileErr(160, _22682, 0);
L13: 

    /** 			reset_flags = 0*/
    _reset_flags_61725 = 0;
    goto L14; // [498] 727
L12: 

    /** 		elsif tok[T_ID] = EQUALS then*/
    _2 = (int)SEQ_PTR(_tok_61828);
    _31268 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31268, 3)){
        _31268 = NOVALUE;
        goto L15; // [511] 570
    }
    _31268 = NOVALUE;

    /** 			tok = next_token()*/
    _0 = _tok_61828;
    _tok_61828 = _30next_token();
    DeRef(_0);

    /** 			if tok[T_ID] != LEFT_BRACE and tok[T_ID] != LEFT_ROUND then*/
    _2 = (int)SEQ_PTR(_tok_61828);
    _31271 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31271)) {
        _31272 = (_31271 != -24);
    }
    else {
        _31272 = binary_op(NOTEQ, _31271, -24);
    }
    _31271 = NOVALUE;
    if (IS_ATOM_INT(_31272)) {
        if (_31272 == 0) {
            goto L16; // [534] 562
        }
    }
    else {
        if (DBL_PTR(_31272)->dbl == 0.0) {
            goto L16; // [534] 562
        }
    }
    _2 = (int)SEQ_PTR(_tok_61828);
    _31274 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_31274)) {
        _31275 = (_31274 != -26);
    }
    else {
        _31275 = binary_op(NOTEQ, _31274, -26);
    }
    _31274 = NOVALUE;
    if (_31275 == 0) {
        DeRef(_31275);
        _31275 = NOVALUE;
        goto L16; // [551] 562
    }
    else {
        if (!IS_ATOM_INT(_31275) && DBL_PTR(_31275)->dbl == 0.0){
            DeRef(_31275);
            _31275 = NOVALUE;
            goto L16; // [551] 562
        }
        DeRef(_31275);
        _31275 = NOVALUE;
    }
    DeRef(_31275);
    _31275 = NOVALUE;

    /** 				CompileErr(160)*/
    RefDS(_22682);
    _43CompileErr(160, _22682, 0);
L16: 

    /** 			reset_flags = 1*/
    _reset_flags_61725 = 1;
    goto L14; // [567] 727
L15: 

    /** 		elsif tok[T_ID] = VARIABLE then*/
    _2 = (int)SEQ_PTR(_tok_61828);
    _31276 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31276, -100)){
        _31276 = NOVALUE;
        goto L17; // [580] 726
    }
    _31276 = NOVALUE;

    /** 			option = SymTab[tok[T_SYM]][S_NAME]*/
    _2 = (int)SEQ_PTR(_tok_61828);
    _31278 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_31278)){
        _31279 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31278)->dbl));
    }
    else{
        _31279 = (int)*(((s1_ptr)_2)->base + _31278);
    }
    DeRef(_option_61723);
    _2 = (int)SEQ_PTR(_31279);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _option_61723 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _option_61723 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    Ref(_option_61723);
    _31279 = NOVALUE;

    /** 			if equal(option, "save") then*/
    if (_option_61723 == _31281)
    _31282 = 1;
    else if (IS_ATOM_INT(_option_61723) && IS_ATOM_INT(_31281))
    _31282 = 0;
    else
    _31282 = (compare(_option_61723, _31281) == 0);
    if (_31282 == 0)
    {
        _31282 = NOVALUE;
        goto L18; // [612] 636
    }
    else{
        _31282 = NOVALUE;
    }

    /** 				prev_OpWarning = OpWarning*/
    _25prev_OpWarning_12331 = _25OpWarning_12330;

    /** 				warning_extra = FALSE*/
    _warning_extra_61830 = _5FALSE_242;
    goto L19; // [633] 725
L18: 

    /** 			elsif equal(option, "restore") then*/
    if (_option_61723 == _31283)
    _31284 = 1;
    else if (IS_ATOM_INT(_option_61723) && IS_ATOM_INT(_31283))
    _31284 = 0;
    else
    _31284 = (compare(_option_61723, _31283) == 0);
    if (_31284 == 0)
    {
        _31284 = NOVALUE;
        goto L1A; // [642] 666
    }
    else{
        _31284 = NOVALUE;
    }

    /** 				OpWarning = prev_OpWarning*/
    _25OpWarning_12330 = _25prev_OpWarning_12331;

    /** 				warning_extra = FALSE*/
    _warning_extra_61830 = _5FALSE_242;
    goto L19; // [663] 725
L1A: 

    /** 			elsif equal(option, "strict") then*/
    if (_option_61723 == _26266)
    _31285 = 1;
    else if (IS_ATOM_INT(_option_61723) && IS_ATOM_INT(_26266))
    _31285 = 0;
    else
    _31285 = (compare(_option_61723, _26266) == 0);
    if (_31285 == 0)
    {
        _31285 = NOVALUE;
        goto L1B; // [672] 724
    }
    else{
        _31285 = NOVALUE;
    }

    /** 				if on_off = 0 then*/
    if (_on_off_61722 != 0)
    goto L1C; // [677] 694

    /** 					Strict_Override += 1*/
    _25Strict_Override_12329 = _25Strict_Override_12329 + 1;
    goto L1D; // [691] 714
L1C: 

    /** 				elsif Strict_Override > 0 then*/
    if (_25Strict_Override_12329 <= 0)
    goto L1E; // [698] 713

    /** 					Strict_Override -= 1*/
    _25Strict_Override_12329 = _25Strict_Override_12329 - 1;
L1E: 
L1D: 

    /** 				warning_extra = FALSE*/
    _warning_extra_61830 = _5FALSE_242;
L1B: 
L19: 
L17: 
L14: 

    /** 		if warning_extra = TRUE then*/
    if (_warning_extra_61830 != _5TRUE_244)
    goto L1F; // [731] 1229

    /** 			if reset_flags then*/
    if (_reset_flags_61725 == 0)
    {
        goto L20; // [737] 769
    }
    else{
    }

    /** 				if on_off = 0 then*/
    if (_on_off_61722 != 0)
    goto L21; // [742] 758

    /** 					OpWarning = no_warning_flag*/
    _25OpWarning_12330 = 0;
    goto L22; // [755] 768
L21: 

    /** 					OpWarning = all_warning_flag*/
    _25OpWarning_12330 = 32767;
L22: 
L20: 

    /** 			if find(tok[T_ID], {LEFT_BRACE, LEFT_ROUND}) then*/
    _2 = (int)SEQ_PTR(_tok_61828);
    _31292 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -24;
    ((int *)_2)[2] = -26;
    _31293 = MAKE_SEQ(_1);
    _31294 = find_from(_31292, _31293, 1);
    _31292 = NOVALUE;
    DeRefDS(_31293);
    _31293 = NOVALUE;
    if (_31294 == 0)
    {
        _31294 = NOVALUE;
        goto L23; // [790] 1222
    }
    else{
        _31294 = NOVALUE;
    }

    /** 				integer endlist*/

    /** 				if tok[T_ID] = LEFT_BRACE then*/
    _2 = (int)SEQ_PTR(_tok_61828);
    _31295 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31295, -24)){
        _31295 = NOVALUE;
        goto L24; // [805] 821
    }
    _31295 = NOVALUE;

    /** 					endlist = RIGHT_BRACE*/
    _endlist_61921 = -25;
    goto L25; // [818] 831
L24: 

    /** 					endlist = RIGHT_ROUND*/
    _endlist_61921 = -27;
L25: 

    /** 				tok = next_token()*/
    _0 = _tok_61828;
    _tok_61828 = _30next_token();
    DeRef(_0);

    /** 				while tok[T_ID] != endlist do*/
L26: 
    _2 = (int)SEQ_PTR(_tok_61828);
    _31298 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _31298, _endlist_61921)){
        _31298 = NOVALUE;
        goto L27; // [849] 1217
    }
    _31298 = NOVALUE;

    /** 					if tok[T_ID] = COMMA then*/
    _2 = (int)SEQ_PTR(_tok_61828);
    _31300 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31300, -30)){
        _31300 = NOVALUE;
        goto L28; // [863] 877
    }
    _31300 = NOVALUE;

    /** 						tok = next_token()*/
    _0 = _tok_61828;
    _tok_61828 = _30next_token();
    DeRef(_0);

    /** 						continue*/
    goto L26; // [874] 841
L28: 

    /** 					if tok[T_ID] = STRING then*/
    _2 = (int)SEQ_PTR(_tok_61828);
    _31303 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31303, 503)){
        _31303 = NOVALUE;
        goto L29; // [887] 916
    }
    _31303 = NOVALUE;

    /** 						option = SymTab[tok[T_SYM]][S_OBJ]*/
    _2 = (int)SEQ_PTR(_tok_61828);
    _31305 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_31305)){
        _31306 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31305)->dbl));
    }
    else{
        _31306 = (int)*(((s1_ptr)_2)->base + _31305);
    }
    DeRef(_option_61723);
    _2 = (int)SEQ_PTR(_31306);
    _option_61723 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_option_61723);
    _31306 = NOVALUE;
    goto L2A; // [913] 1064
L29: 

    /** 					elsif length(SymTab[tok[T_SYM]]) >= S_NAME then*/
    _2 = (int)SEQ_PTR(_tok_61828);
    _31308 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_31308)){
        _31309 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31308)->dbl));
    }
    else{
        _31309 = (int)*(((s1_ptr)_2)->base + _31308);
    }
    if (IS_SEQUENCE(_31309)){
            _31310 = SEQ_PTR(_31309)->length;
    }
    else {
        _31310 = 1;
    }
    _31309 = NOVALUE;
    if (binary_op_a(LESS, _31310, _25S_NAME_11913)){
        _31310 = NOVALUE;
        goto L2B; // [935] 964
    }
    _31310 = NOVALUE;

    /** 						option = SymTab[tok[T_SYM]][S_NAME]*/
    _2 = (int)SEQ_PTR(_tok_61828);
    _31312 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_31312)){
        _31313 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31312)->dbl));
    }
    else{
        _31313 = (int)*(((s1_ptr)_2)->base + _31312);
    }
    DeRef(_option_61723);
    _2 = (int)SEQ_PTR(_31313);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _option_61723 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _option_61723 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    Ref(_option_61723);
    _31313 = NOVALUE;
    goto L2A; // [961] 1064
L2B: 

    /** 						option = ""*/
    RefDS(_22682);
    DeRef(_option_61723);
    _option_61723 = _22682;

    /** 						for k = 1 to length(keylist) do*/
    if (IS_SEQUENCE(_63keylist_23599)){
            _31315 = SEQ_PTR(_63keylist_23599)->length;
    }
    else {
        _31315 = 1;
    }
    {
        int _k_61968;
        _k_61968 = 1;
L2C: 
        if (_k_61968 > _31315){
            goto L2D; // [978] 1063
        }

        /** 							if keylist[k][S_SCOPE] = SC_KEYWORD and*/
        _2 = (int)SEQ_PTR(_63keylist_23599);
        _31316 = (int)*(((s1_ptr)_2)->base + _k_61968);
        _2 = (int)SEQ_PTR(_31316);
        _31317 = (int)*(((s1_ptr)_2)->base + 4);
        _31316 = NOVALUE;
        if (IS_ATOM_INT(_31317)) {
            _31318 = (_31317 == 8);
        }
        else {
            _31318 = binary_op(EQUALS, _31317, 8);
        }
        _31317 = NOVALUE;
        if (IS_ATOM_INT(_31318)) {
            if (_31318 == 0) {
                goto L2E; // [1005] 1056
            }
        }
        else {
            if (DBL_PTR(_31318)->dbl == 0.0) {
                goto L2E; // [1005] 1056
            }
        }
        _2 = (int)SEQ_PTR(_63keylist_23599);
        _31320 = (int)*(((s1_ptr)_2)->base + _k_61968);
        _2 = (int)SEQ_PTR(_31320);
        if (!IS_ATOM_INT(_25S_TOKEN_11918)){
            _31321 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
        }
        else{
            _31321 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
        }
        _31320 = NOVALUE;
        _2 = (int)SEQ_PTR(_tok_61828);
        _31322 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_31321) && IS_ATOM_INT(_31322)) {
            _31323 = (_31321 == _31322);
        }
        else {
            _31323 = binary_op(EQUALS, _31321, _31322);
        }
        _31321 = NOVALUE;
        _31322 = NOVALUE;
        if (_31323 == 0) {
            DeRef(_31323);
            _31323 = NOVALUE;
            goto L2E; // [1032] 1056
        }
        else {
            if (!IS_ATOM_INT(_31323) && DBL_PTR(_31323)->dbl == 0.0){
                DeRef(_31323);
                _31323 = NOVALUE;
                goto L2E; // [1032] 1056
            }
            DeRef(_31323);
            _31323 = NOVALUE;
        }
        DeRef(_31323);
        _31323 = NOVALUE;

        /** 									option = keylist[k][S_NAME]*/
        _2 = (int)SEQ_PTR(_63keylist_23599);
        _31324 = (int)*(((s1_ptr)_2)->base + _k_61968);
        DeRef(_option_61723);
        _2 = (int)SEQ_PTR(_31324);
        if (!IS_ATOM_INT(_25S_NAME_11913)){
            _option_61723 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
        }
        else{
            _option_61723 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
        }
        Ref(_option_61723);
        _31324 = NOVALUE;

        /** 									exit*/
        goto L2D; // [1053] 1063
L2E: 

        /** 						end for*/
        _k_61968 = _k_61968 + 1;
        goto L2C; // [1058] 985
L2D: 
        ;
    }
L2A: 

    /** 					idx = find(option, warning_names)*/
    _idx_61724 = find_from(_option_61723, _25warning_names_12307, 1);

    /** 					if idx = 0 then*/
    if (_idx_61724 != 0)
    goto L2F; // [1075] 1128

    /** 	 					if good_sofar != line_number then*/
    if (_good_sofar_61825 == _25line_number_12263)
    goto L30; // [1083] 1095

    /**  							CompileErr(147)*/
    RefDS(_22682);
    _43CompileErr(147, _22682, 0);
L30: 

    /** 						Warning(225, 0,*/
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _31329 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_31329);
    *((int *)(_2+4)) = _31329;
    *((int *)(_2+8)) = _25line_number_12263;
    RefDS(_option_61723);
    *((int *)(_2+12)) = _option_61723;
    _31330 = MAKE_SEQ(_1);
    _31329 = NOVALUE;
    _43Warning(225, 0, _31330);
    _31330 = NOVALUE;

    /** 						tok = next_token()*/
    _0 = _tok_61828;
    _tok_61828 = _30next_token();
    DeRef(_0);

    /** 						continue*/
    goto L26; // [1125] 841
L2F: 

    /** 					idx = warning_flags[idx]*/
    _2 = (int)SEQ_PTR(_25warning_flags_12305);
    _idx_61724 = (int)*(((s1_ptr)_2)->base + _idx_61724);

    /** 					if idx = 0 then*/
    if (_idx_61724 != 0)
    goto L31; // [1140] 1174

    /** 						if on_off then*/
    if (_on_off_61722 == 0)
    {
        goto L32; // [1146] 1161
    }
    else{
    }

    /** 							OpWarning = no_warning_flag*/
    _25OpWarning_12330 = 0;
    goto L33; // [1158] 1207
L32: 

    /** 						    OpWarning = all_warning_flag*/
    _25OpWarning_12330 = 32767;
    goto L33; // [1171] 1207
L31: 

    /** 						if on_off then*/
    if (_on_off_61722 == 0)
    {
        goto L34; // [1176] 1192
    }
    else{
    }

    /** 							OpWarning = or_bits(OpWarning, idx)*/
    {unsigned long tu;
         tu = (unsigned long)_25OpWarning_12330 | (unsigned long)_idx_61724;
         _25OpWarning_12330 = MAKE_UINT(tu);
    }
    if (!IS_ATOM_INT(_25OpWarning_12330)) {
        _1 = (long)(DBL_PTR(_25OpWarning_12330)->dbl);
        if (UNIQUE(DBL_PTR(_25OpWarning_12330)) && (DBL_PTR(_25OpWarning_12330)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_25OpWarning_12330);
        _25OpWarning_12330 = _1;
    }
    goto L35; // [1189] 1206
L34: 

    /** 						    OpWarning = and_bits(OpWarning, not_bits(idx))*/
    _31335 = not_bits(_idx_61724);
    if (IS_ATOM_INT(_31335)) {
        {unsigned long tu;
             tu = (unsigned long)_25OpWarning_12330 & (unsigned long)_31335;
             _25OpWarning_12330 = MAKE_UINT(tu);
        }
    }
    else {
        temp_d.dbl = (double)_25OpWarning_12330;
        _25OpWarning_12330 = Dand_bits(&temp_d, DBL_PTR(_31335));
    }
    DeRef(_31335);
    _31335 = NOVALUE;
    if (!IS_ATOM_INT(_25OpWarning_12330)) {
        _1 = (long)(DBL_PTR(_25OpWarning_12330)->dbl);
        if (UNIQUE(DBL_PTR(_25OpWarning_12330)) && (DBL_PTR(_25OpWarning_12330)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_25OpWarning_12330);
        _25OpWarning_12330 = _1;
    }
L35: 
L33: 

    /** 					tok = next_token()*/
    _0 = _tok_61828;
    _tok_61828 = _30next_token();
    DeRef(_0);

    /** 				end while*/
    goto L26; // [1214] 841
L27: 
    goto L36; // [1219] 1228
L23: 

    /** 				putback(tok)*/
    Ref(_tok_61828);
    _30putback(_tok_61828);
L36: 
L1F: 
    DeRef(_tok_61828);
    _tok_61828 = NOVALUE;
    goto L2; // [1231] 1521
L11: 

    /** 	elsif equal(option, "define") then*/
    if (_option_61723 == _31338)
    _31339 = 1;
    else if (IS_ATOM_INT(_option_61723) && IS_ATOM_INT(_31338))
    _31339 = 0;
    else
    _31339 = (compare(_option_61723, _31338) == 0);
    if (_31339 == 0)
    {
        _31339 = NOVALUE;
        goto L37; // [1240] 1363
    }
    else{
        _31339 = NOVALUE;
    }

    /** 		option = StringToken()*/
    RefDS(_5);
    _0 = _option_61723;
    _option_61723 = _60StringToken(_5);
    DeRefDS(_0);

    /** 		if length(option) = 0 then*/
    if (IS_SEQUENCE(_option_61723)){
            _31341 = SEQ_PTR(_option_61723)->length;
    }
    else {
        _31341 = 1;
    }
    if (_31341 != 0)
    goto L38; // [1256] 1270

    /** 			CompileErr(81)*/
    RefDS(_22682);
    _43CompileErr(81, _22682, 0);
    goto L39; // [1267] 1288
L38: 

    /** 		elsif not t_identifier(option) then*/
    RefDS(_option_61723);
    _31343 = _5t_identifier(_option_61723);
    if (IS_ATOM_INT(_31343)) {
        if (_31343 != 0){
            DeRef(_31343);
            _31343 = NOVALUE;
            goto L3A; // [1276] 1287
        }
    }
    else {
        if (DBL_PTR(_31343)->dbl != 0.0){
            DeRef(_31343);
            _31343 = NOVALUE;
            goto L3A; // [1276] 1287
        }
    }
    DeRef(_31343);
    _31343 = NOVALUE;

    /** 			CompileErr(61)*/
    RefDS(_22682);
    _43CompileErr(61, _22682, 0);
L3A: 
L39: 

    /** 		if on_off = 0 then*/
    if (_on_off_61722 != 0)
    goto L3B; // [1290] 1345

    /** 			idx = find(option, OpDefines)*/
    _idx_61724 = find_from(_option_61723, _25OpDefines_12336, 1);

    /** 			if idx then*/
    if (_idx_61724 == 0)
    {
        goto L2; // [1305] 1521
    }
    else{
    }

    /** 				OpDefines = OpDefines[1..idx-1]&OpDefines[idx+1..$]*/
    _31347 = _idx_61724 - 1;
    rhs_slice_target = (object_ptr)&_31348;
    RHS_Slice(_25OpDefines_12336, 1, _31347);
    _31349 = _idx_61724 + 1;
    if (IS_SEQUENCE(_25OpDefines_12336)){
            _31350 = SEQ_PTR(_25OpDefines_12336)->length;
    }
    else {
        _31350 = 1;
    }
    rhs_slice_target = (object_ptr)&_31351;
    RHS_Slice(_25OpDefines_12336, _31349, _31350);
    Concat((object_ptr)&_25OpDefines_12336, _31348, _31351);
    DeRefDS(_31348);
    _31348 = NOVALUE;
    DeRef(_31348);
    _31348 = NOVALUE;
    DeRefDS(_31351);
    _31351 = NOVALUE;
    goto L2; // [1342] 1521
L3B: 

    /** 			OpDefines &= {option}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_option_61723);
    *((int *)(_2+4)) = _option_61723;
    _31353 = MAKE_SEQ(_1);
    Concat((object_ptr)&_25OpDefines_12336, _25OpDefines_12336, _31353);
    DeRefDS(_31353);
    _31353 = NOVALUE;
    goto L2; // [1360] 1521
L37: 

    /** 	elsif equal(option, "inline") then*/
    if (_option_61723 == _31355)
    _31356 = 1;
    else if (IS_ATOM_INT(_option_61723) && IS_ATOM_INT(_31355))
    _31356 = 0;
    else
    _31356 = (compare(_option_61723, _31355) == 0);
    if (_31356 == 0)
    {
        _31356 = NOVALUE;
        goto L3C; // [1369] 1455
    }
    else{
        _31356 = NOVALUE;
    }

    /** 		if on_off then*/
    if (_on_off_61722 == 0)
    {
        goto L3D; // [1374] 1444
    }
    else{
    }

    /** 			token tok = next_token()*/
    _0 = _tok_62064;
    _tok_62064 = _30next_token();
    DeRef(_0);

    /** 			if tok[T_ID] = ATOM then*/
    _2 = (int)SEQ_PTR(_tok_62064);
    _31358 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31358, 502)){
        _31358 = NOVALUE;
        goto L3E; // [1392] 1424
    }
    _31358 = NOVALUE;

    /** 				OpInline = floor( SymTab[tok[T_SYM]][S_OBJ] )*/
    _2 = (int)SEQ_PTR(_tok_62064);
    _31360 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_31360)){
        _31361 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31360)->dbl));
    }
    else{
        _31361 = (int)*(((s1_ptr)_2)->base + _31360);
    }
    _2 = (int)SEQ_PTR(_31361);
    _31362 = (int)*(((s1_ptr)_2)->base + 1);
    _31361 = NOVALUE;
    if (IS_ATOM_INT(_31362))
    _25OpInline_12340 = e_floor(_31362);
    else
    _25OpInline_12340 = unary_op(FLOOR, _31362);
    _31362 = NOVALUE;
    if (!IS_ATOM_INT(_25OpInline_12340)) {
        _1 = (long)(DBL_PTR(_25OpInline_12340)->dbl);
        if (UNIQUE(DBL_PTR(_25OpInline_12340)) && (DBL_PTR(_25OpInline_12340)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_25OpInline_12340);
        _25OpInline_12340 = _1;
    }
    goto L3F; // [1421] 1439
L3E: 

    /** 				putback(tok)*/
    Ref(_tok_62064);
    _30putback(_tok_62064);

    /** 				OpInline = DEFAULT_INLINE*/
    _25OpInline_12340 = 30;
L3F: 
    DeRef(_tok_62064);
    _tok_62064 = NOVALUE;
    goto L2; // [1441] 1521
L3D: 

    /** 			OpInline = 0*/
    _25OpInline_12340 = 0;
    goto L2; // [1452] 1521
L3C: 

    /** 	elsif equal( option, "indirect_includes" ) then*/
    if (_option_61723 == _31364)
    _31365 = 1;
    else if (IS_ATOM_INT(_option_61723) && IS_ATOM_INT(_31364))
    _31365 = 0;
    else
    _31365 = (compare(_option_61723, _31364) == 0);
    if (_31365 == 0)
    {
        _31365 = NOVALUE;
        goto L40; // [1461] 1474
    }
    else{
        _31365 = NOVALUE;
    }

    /** 		OpIndirectInclude = on_off*/
    _25OpIndirectInclude_12341 = _on_off_61722;
    goto L2; // [1471] 1521
L40: 

    /** 	elsif equal(option, "batch") then*/
    if (_option_61723 == _26263)
    _31366 = 1;
    else if (IS_ATOM_INT(_option_61723) && IS_ATOM_INT(_26263))
    _31366 = 0;
    else
    _31366 = (compare(_option_61723, _26263) == 0);
    if (_31366 == 0)
    {
        _31366 = NOVALUE;
        goto L41; // [1480] 1493
    }
    else{
        _31366 = NOVALUE;
    }

    /** 		batch_job = on_off*/
    _25batch_job_12275 = _on_off_61722;
    goto L2; // [1490] 1521
L41: 

    /** 	elsif integer(to_number(option, -1)) then*/
    RefDS(_option_61723);
    _31367 = _6to_number(_option_61723, -1);
    if (IS_ATOM_INT(_31367))
    _31368 = 1;
    else if (IS_ATOM_DBL(_31367))
    _31368 = IS_ATOM_INT(DoubleToInt(_31367));
    else
    _31368 = 0;
    DeRef(_31367);
    _31367 = NOVALUE;
    if (_31368 == 0)
    {
        _31368 = NOVALUE;
        goto L42; // [1503] 1509
    }
    else{
        _31368 = NOVALUE;
    }
    goto L2; // [1506] 1521
L42: 

    /** 		CompileErr(154, {option})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_option_61723);
    *((int *)(_2+4)) = _option_61723;
    _31369 = MAKE_SEQ(_1);
    _43CompileErr(154, _31369, 0);
    _31369 = NOVALUE;
L2: 

    /** end procedure*/
    DeRef(_option_61723);
    DeRef(_31231);
    _31231 = NOVALUE;
    DeRef(_31236);
    _31236 = NOVALUE;
    _31243 = NOVALUE;
    _31247 = NOVALUE;
    DeRef(_31250);
    _31250 = NOVALUE;
    _31278 = NOVALUE;
    DeRef(_31264);
    _31264 = NOVALUE;
    DeRef(_31272);
    _31272 = NOVALUE;
    _31305 = NOVALUE;
    _31308 = NOVALUE;
    _31309 = NOVALUE;
    _31312 = NOVALUE;
    DeRef(_31347);
    _31347 = NOVALUE;
    DeRef(_31318);
    _31318 = NOVALUE;
    _31360 = NOVALUE;
    DeRef(_31349);
    _31349 = NOVALUE;
    return;
    ;
}


void _30ExecCommand()
{
    int _0, _1, _2;
    

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L1; // [5] 16
    }
    else{
    }

    /** 		emit_op(RETURNT)*/
    _37emit_op(34);
L1: 

    /** 	StraightenBranches()  -- straighten top-level*/
    _30StraightenBranches();

    /** end procedure*/
    return;
    ;
}


int _30undefined_var(int _tok_62107, int _scope_62108)
{
    int _forward_62110 = NOVALUE;
    int _31375 = NOVALUE;
    int _31374 = NOVALUE;
    int _31371 = NOVALUE;
    int _0, _1, _2;
    

    /** 	token forward = next_token()*/
    _0 = _forward_62110;
    _forward_62110 = _30next_token();
    DeRef(_0);

    /** 		switch forward[T_ID] do*/
    _2 = (int)SEQ_PTR(_forward_62110);
    _31371 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_31371) ){
        goto L1; // [16] 84
    }
    if(!IS_ATOM_INT(_31371)){
        if( (DBL_PTR(_31371)->dbl != (double) ((int) DBL_PTR(_31371)->dbl) ) ){
            goto L1; // [16] 84
        }
        _0 = (int) DBL_PTR(_31371)->dbl;
    }
    else {
        _0 = _31371;
    };
    _31371 = NOVALUE;
    switch ( _0 ){ 

        /** 			case LEFT_ROUND then*/
        case -26:

        /** 				StartSourceLine( TRUE )*/
        _37StartSourceLine(_5TRUE_244, 0, 2);

        /** 				Forward_call( tok )*/
        Ref(_tok_62107);
        _30Forward_call(_tok_62107, 195);

        /** 				return 1*/
        DeRef(_tok_62107);
        DeRef(_forward_62110);
        return 1;
        goto L2; // [50] 98

        /** 			case VARIABLE then*/
        case -100:

        /** 				putback( forward )*/
        Ref(_forward_62110);
        _30putback(_forward_62110);

        /** 				Global_declaration( tok[T_SYM], scope )*/
        _2 = (int)SEQ_PTR(_tok_62107);
        _31374 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_31374);
        _31375 = _30Global_declaration(_31374, _scope_62108);
        _31374 = NOVALUE;

        /** 				return 1*/
        DeRef(_tok_62107);
        DeRef(_forward_62110);
        DeRef(_31375);
        _31375 = NOVALUE;
        return 1;
        goto L2; // [80] 98

        /** 			case else*/
        default:
L1: 

        /** 				putback( forward )*/
        Ref(_forward_62110);
        _30putback(_forward_62110);

        /** 				return 0*/
        DeRef(_tok_62107);
        DeRef(_forward_62110);
        DeRef(_31375);
        _31375 = NOVALUE;
        return 0;
    ;}L2: 
    ;
}


void _30real_parser(int _nested_62129)
{
    int _tok_62131 = NOVALUE;
    int _id_62132 = NOVALUE;
    int _scope_62133 = NOVALUE;
    int _test_62274 = NOVALUE;
    int _31508 = NOVALUE;
    int _31506 = NOVALUE;
    int _31505 = NOVALUE;
    int _31504 = NOVALUE;
    int _31503 = NOVALUE;
    int _31502 = NOVALUE;
    int _31501 = NOVALUE;
    int _31500 = NOVALUE;
    int _31495 = NOVALUE;
    int _31494 = NOVALUE;
    int _31491 = NOVALUE;
    int _31489 = NOVALUE;
    int _31488 = NOVALUE;
    int _31485 = NOVALUE;
    int _31472 = NOVALUE;
    int _31465 = NOVALUE;
    int _31464 = NOVALUE;
    int _31462 = NOVALUE;
    int _31460 = NOVALUE;
    int _31459 = NOVALUE;
    int _31457 = NOVALUE;
    int _31455 = NOVALUE;
    int _31450 = NOVALUE;
    int _31448 = NOVALUE;
    int _31446 = NOVALUE;
    int _31445 = NOVALUE;
    int _31444 = NOVALUE;
    int _31442 = NOVALUE;
    int _31440 = NOVALUE;
    int _31438 = NOVALUE;
    int _31436 = NOVALUE;
    int _31435 = NOVALUE;
    int _31434 = NOVALUE;
    int _31433 = NOVALUE;
    int _31432 = NOVALUE;
    int _31431 = NOVALUE;
    int _31430 = NOVALUE;
    int _31429 = NOVALUE;
    int _31428 = NOVALUE;
    int _31427 = NOVALUE;
    int _31426 = NOVALUE;
    int _31425 = NOVALUE;
    int _31424 = NOVALUE;
    int _31423 = NOVALUE;
    int _31421 = NOVALUE;
    int _31420 = NOVALUE;
    int _31419 = NOVALUE;
    int _31418 = NOVALUE;
    int _31416 = NOVALUE;
    int _31414 = NOVALUE;
    int _31413 = NOVALUE;
    int _31412 = NOVALUE;
    int _31410 = NOVALUE;
    int _31403 = NOVALUE;
    int _31401 = NOVALUE;
    int _31400 = NOVALUE;
    int _31399 = NOVALUE;
    int _31398 = NOVALUE;
    int _31397 = NOVALUE;
    int _31396 = NOVALUE;
    int _31395 = NOVALUE;
    int _31393 = NOVALUE;
    int _31392 = NOVALUE;
    int _31391 = NOVALUE;
    int _31390 = NOVALUE;
    int _31389 = NOVALUE;
    int _31388 = NOVALUE;
    int _31387 = NOVALUE;
    int _31386 = NOVALUE;
    int _31385 = NOVALUE;
    int _31384 = NOVALUE;
    int _31382 = NOVALUE;
    int _31378 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_nested_62129)) {
        _1 = (long)(DBL_PTR(_nested_62129)->dbl);
        if (UNIQUE(DBL_PTR(_nested_62129)) && (DBL_PTR(_nested_62129)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_nested_62129);
        _nested_62129 = _1;
    }

    /** 	integer id*/

    /** 	integer scope*/

    /** 	while TRUE do  -- infinite loop until scanner aborts*/
L1: 
    if (_5TRUE_244 == 0)
    {
        goto L2; // [14] 1843
    }
    else{
    }

    /** 		if OpInline = 25000 then*/
    if (_25OpInline_12340 != 25000)
    goto L3; // [21] 35

    /** 			CompileErr("OpInline went nuts: [1]", OpInline )*/
    RefDS(_31377);
    _43CompileErr(_31377, _25OpInline_12340, 0);
L3: 

    /** 		start_index = length(Code)+1*/
    if (IS_SEQUENCE(_25Code_12355)){
            _31378 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _31378 = 1;
    }
    _30start_index_55177 = _31378 + 1;
    _31378 = NOVALUE;

    /** 		tok = next_token()*/
    _0 = _tok_62131;
    _tok_62131 = _30next_token();
    DeRef(_0);

    /** 		id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_62131);
    _id_62132 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_id_62132)){
        _id_62132 = (long)DBL_PTR(_id_62132)->dbl;
    }

    /** 		if id = VARIABLE or id = QUALIFIED_VARIABLE then*/
    _31382 = (_id_62132 == -100);
    if (_31382 != 0) {
        goto L4; // [69] 84
    }
    _31384 = (_id_62132 == 512);
    if (_31384 == 0)
    {
        DeRef(_31384);
        _31384 = NOVALUE;
        goto L5; // [80] 153
    }
    else{
        DeRef(_31384);
        _31384 = NOVALUE;
    }
L4: 

    /** 			if SymTab[tok[T_SYM]][S_SCOPE] = SC_UNDEFINED*/
    _2 = (int)SEQ_PTR(_tok_62131);
    _31385 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_31385)){
        _31386 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31385)->dbl));
    }
    else{
        _31386 = (int)*(((s1_ptr)_2)->base + _31385);
    }
    _2 = (int)SEQ_PTR(_31386);
    _31387 = (int)*(((s1_ptr)_2)->base + 4);
    _31386 = NOVALUE;
    if (IS_ATOM_INT(_31387)) {
        _31388 = (_31387 == 9);
    }
    else {
        _31388 = binary_op(EQUALS, _31387, 9);
    }
    _31387 = NOVALUE;
    if (IS_ATOM_INT(_31388)) {
        if (_31388 == 0) {
            goto L6; // [110] 130
        }
    }
    else {
        if (DBL_PTR(_31388)->dbl == 0.0) {
            goto L6; // [110] 130
        }
    }
    Ref(_tok_62131);
    _31390 = _30undefined_var(_tok_62131, 5);
    if (_31390 == 0) {
        DeRef(_31390);
        _31390 = NOVALUE;
        goto L6; // [122] 130
    }
    else {
        if (!IS_ATOM_INT(_31390) && DBL_PTR(_31390)->dbl == 0.0){
            DeRef(_31390);
            _31390 = NOVALUE;
            goto L6; // [122] 130
        }
        DeRef(_31390);
        _31390 = NOVALUE;
    }
    DeRef(_31390);
    _31390 = NOVALUE;

    /** 				continue*/
    goto L1; // [127] 12
L6: 

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			Assignment(tok)*/
    Ref(_tok_62131);
    _30Assignment(_tok_62131);

    /** 			ExecCommand()*/
    _30ExecCommand();
    goto L7; // [150] 1833
L5: 

    /** 		elsif id = PROCEDURE or id = FUNCTION or id = TYPE_DECL then*/
    _31391 = (_id_62132 == 405);
    if (_31391 != 0) {
        _31392 = 1;
        goto L8; // [161] 175
    }
    _31393 = (_id_62132 == 406);
    _31392 = (_31393 != 0);
L8: 
    if (_31392 != 0) {
        goto L9; // [175] 190
    }
    _31395 = (_id_62132 == 416);
    if (_31395 == 0)
    {
        DeRef(_31395);
        _31395 = NOVALUE;
        goto LA; // [186] 207
    }
    else{
        DeRef(_31395);
        _31395 = NOVALUE;
    }
L9: 

    /** 			SubProg(tok[T_ID], SC_LOCAL)*/
    _2 = (int)SEQ_PTR(_tok_62131);
    _31396 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_31396);
    _30SubProg(_31396, 5);
    _31396 = NOVALUE;
    goto L7; // [204] 1833
LA: 

    /** 		elsif id = GLOBAL or id = EXPORT or id = OVERRIDE or id = PUBLIC then*/
    _31397 = (_id_62132 == 412);
    if (_31397 != 0) {
        _31398 = 1;
        goto LB; // [215] 229
    }
    _31399 = (_id_62132 == 428);
    _31398 = (_31399 != 0);
LB: 
    if (_31398 != 0) {
        _31400 = 1;
        goto LC; // [229] 243
    }
    _31401 = (_id_62132 == 429);
    _31400 = (_31401 != 0);
LC: 
    if (_31400 != 0) {
        goto LD; // [243] 258
    }
    _31403 = (_id_62132 == 430);
    if (_31403 == 0)
    {
        DeRef(_31403);
        _31403 = NOVALUE;
        goto LE; // [254] 628
    }
    else{
        DeRef(_31403);
        _31403 = NOVALUE;
    }
LD: 

    /** 			if id = GLOBAL then*/
    if (_id_62132 != 412)
    goto LF; // [262] 278

    /** 			    scope = SC_GLOBAL*/
    _scope_62133 = 6;
    goto L10; // [275] 337
LF: 

    /** 			elsif id = EXPORT then*/
    if (_id_62132 != 428)
    goto L11; // [282] 298

    /** 				scope = SC_EXPORT*/
    _scope_62133 = 11;
    goto L10; // [295] 337
L11: 

    /** 			elsif id = OVERRIDE then*/
    if (_id_62132 != 429)
    goto L12; // [302] 318

    /** 				scope = SC_OVERRIDE*/
    _scope_62133 = 12;
    goto L10; // [315] 337
L12: 

    /** 			elsif id = PUBLIC then*/
    if (_id_62132 != 430)
    goto L13; // [322] 336

    /** 				scope = SC_PUBLIC*/
    _scope_62133 = 13;
L13: 
L10: 

    /** 			tok = next_token()*/
    _0 = _tok_62131;
    _tok_62131 = _30next_token();
    DeRef(_0);

    /** 			id = tok[T_ID]*/
    _2 = (int)SEQ_PTR(_tok_62131);
    _id_62132 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_id_62132)){
        _id_62132 = (long)DBL_PTR(_id_62132)->dbl;
    }

    /** 			if id = TYPE or id = QUALIFIED_TYPE then*/
    _31410 = (_id_62132 == 504);
    if (_31410 != 0) {
        goto L14; // [360] 375
    }
    _31412 = (_id_62132 == 522);
    if (_31412 == 0)
    {
        DeRef(_31412);
        _31412 = NOVALUE;
        goto L15; // [371] 393
    }
    else{
        DeRef(_31412);
        _31412 = NOVALUE;
    }
L14: 

    /** 				Global_declaration(tok[T_SYM], scope )*/
    _2 = (int)SEQ_PTR(_tok_62131);
    _31413 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_31413);
    _31414 = _30Global_declaration(_31413, _scope_62133);
    _31413 = NOVALUE;
    goto L7; // [390] 1833
L15: 

    /** 			elsif id = CONSTANT then*/
    if (_id_62132 != 417)
    goto L16; // [397] 417

    /** 				Global_declaration(0, scope )*/
    _31416 = _30Global_declaration(0, _scope_62133);

    /** 				ExecCommand()*/
    _30ExecCommand();
    goto L7; // [414] 1833
L16: 

    /** 			elsif id = ENUM then*/
    if (_id_62132 != 427)
    goto L17; // [421] 441

    /** 				Global_declaration(-1, scope )*/
    _31418 = _30Global_declaration(-1, _scope_62133);

    /** 				ExecCommand()*/
    _30ExecCommand();
    goto L7; // [438] 1833
L17: 

    /** 			elsif id = PROCEDURE or id = FUNCTION or id = TYPE_DECL then*/
    _31419 = (_id_62132 == 405);
    if (_31419 != 0) {
        _31420 = 1;
        goto L18; // [449] 463
    }
    _31421 = (_id_62132 == 406);
    _31420 = (_31421 != 0);
L18: 
    if (_31420 != 0) {
        goto L19; // [463] 478
    }
    _31423 = (_id_62132 == 416);
    if (_31423 == 0)
    {
        DeRef(_31423);
        _31423 = NOVALUE;
        goto L1A; // [474] 489
    }
    else{
        DeRef(_31423);
        _31423 = NOVALUE;
    }
L19: 

    /** 				SubProg(id, scope )*/
    _30SubProg(_id_62132, _scope_62133);
    goto L7; // [486] 1833
L1A: 

    /** 			elsif (scope = SC_PUBLIC) and id = INCLUDE then*/
    _31424 = (_scope_62133 == 13);
    if (_31424 == 0) {
        goto L1B; // [499] 525
    }
    _31426 = (_id_62132 == 418);
    if (_31426 == 0)
    {
        DeRef(_31426);
        _31426 = NOVALUE;
        goto L1B; // [510] 525
    }
    else{
        DeRef(_31426);
        _31426 = NOVALUE;
    }

    /** 				IncludeScan( 1 )*/
    _60IncludeScan(1);

    /** 				PushGoto()*/
    _30PushGoto();
    goto L7; // [522] 1833
L1B: 

    /** 			elsif (id = VARIABLE or id = QUALIFIED_VARIABLE)*/
    _31427 = (_id_62132 == -100);
    if (_31427 != 0) {
        _31428 = 1;
        goto L1C; // [533] 547
    }
    _31429 = (_id_62132 == 512);
    _31428 = (_31429 != 0);
L1C: 
    if (_31428 == 0) {
        _31430 = 0;
        goto L1D; // [547] 579
    }
    _2 = (int)SEQ_PTR(_tok_62131);
    _31431 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_31431)){
        _31432 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31431)->dbl));
    }
    else{
        _31432 = (int)*(((s1_ptr)_2)->base + _31431);
    }
    _2 = (int)SEQ_PTR(_31432);
    _31433 = (int)*(((s1_ptr)_2)->base + 4);
    _31432 = NOVALUE;
    if (IS_ATOM_INT(_31433)) {
        _31434 = (_31433 == 9);
    }
    else {
        _31434 = binary_op(EQUALS, _31433, 9);
    }
    _31433 = NOVALUE;
    if (IS_ATOM_INT(_31434))
    _31430 = (_31434 != 0);
    else
    _31430 = DBL_PTR(_31434)->dbl != 0.0;
L1D: 
    if (_31430 == 0) {
        goto L1E; // [579] 599
    }
    Ref(_tok_62131);
    _31436 = _30undefined_var(_tok_62131, _scope_62133);
    if (_31436 == 0) {
        DeRef(_31436);
        _31436 = NOVALUE;
        goto L1E; // [589] 599
    }
    else {
        if (!IS_ATOM_INT(_31436) && DBL_PTR(_31436)->dbl == 0.0){
            DeRef(_31436);
            _31436 = NOVALUE;
            goto L1E; // [589] 599
        }
        DeRef(_31436);
        _31436 = NOVALUE;
    }
    DeRef(_31436);
    _31436 = NOVALUE;

    /** 				continue*/
    goto L1; // [594] 12
    goto L7; // [596] 1833
L1E: 

    /** 			elsif scope = SC_GLOBAL then*/
    if (_scope_62133 != 6)
    goto L1F; // [603] 617

    /** 				CompileErr( 18 )*/
    RefDS(_22682);
    _43CompileErr(18, _22682, 0);
    goto L7; // [614] 1833
L1F: 

    /** 				CompileErr( 16 )*/
    RefDS(_22682);
    _43CompileErr(16, _22682, 0);
    goto L7; // [625] 1833
LE: 

    /** 		elsif id = TYPE or id = QUALIFIED_TYPE then*/
    _31438 = (_id_62132 == 504);
    if (_31438 != 0) {
        goto L20; // [636] 651
    }
    _31440 = (_id_62132 == 522);
    if (_31440 == 0)
    {
        DeRef(_31440);
        _31440 = NOVALUE;
        goto L21; // [647] 738
    }
    else{
        DeRef(_31440);
        _31440 = NOVALUE;
    }
L20: 

    /** 			token test = next_token()*/
    _0 = _test_62274;
    _test_62274 = _30next_token();
    DeRef(_0);

    /** 			putback( test )*/
    Ref(_test_62274);
    _30putback(_test_62274);

    /** 			if test[T_ID] = LEFT_ROUND then*/
    _2 = (int)SEQ_PTR(_test_62274);
    _31442 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _31442, -26)){
        _31442 = NOVALUE;
        goto L22; // [671] 711
    }
    _31442 = NOVALUE;

    /** 					StartSourceLine( TRUE )*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 					Procedure_call(tok)*/
    Ref(_tok_62131);
    _30Procedure_call(_tok_62131);

    /** 					clear_op()*/
    _37clear_op();

    /** 					if Pop() then end if*/
    _31444 = _37Pop();
    if (_31444 == 0) {
        DeRef(_31444);
        _31444 = NOVALUE;
        goto L23; // [700] 704
    }
    else {
        if (!IS_ATOM_INT(_31444) && DBL_PTR(_31444)->dbl == 0.0){
            DeRef(_31444);
            _31444 = NOVALUE;
            goto L23; // [700] 704
        }
        DeRef(_31444);
        _31444 = NOVALUE;
    }
    DeRef(_31444);
    _31444 = NOVALUE;
L23: 

    /** 					ExecCommand()*/
    _30ExecCommand();
    goto L24; // [708] 727
L22: 

    /** 				Global_declaration( tok[T_SYM], SC_LOCAL )*/
    _2 = (int)SEQ_PTR(_tok_62131);
    _31445 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_31445);
    _31446 = _30Global_declaration(_31445, 5);
    _31445 = NOVALUE;
L24: 

    /** 			continue*/
    DeRef(_test_62274);
    _test_62274 = NOVALUE;
    goto L1; // [731] 12
    goto L7; // [735] 1833
L21: 

    /** 		elsif id = CONSTANT then*/
    if (_id_62132 != 417)
    goto L25; // [742] 762

    /** 			Global_declaration(0, SC_LOCAL)*/
    _31448 = _30Global_declaration(0, 5);

    /** 			ExecCommand()*/
    _30ExecCommand();
    goto L7; // [759] 1833
L25: 

    /** 		elsif id = ENUM then*/
    if (_id_62132 != 427)
    goto L26; // [766] 786

    /** 			Global_declaration(-1, SC_LOCAL)*/
    _31450 = _30Global_declaration(-1, 5);

    /** 			ExecCommand()*/
    _30ExecCommand();
    goto L7; // [783] 1833
L26: 

    /** 		elsif id = IF then*/
    if (_id_62132 != 20)
    goto L27; // [790] 816

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			If_statement()*/
    _30If_statement();

    /** 			ExecCommand()*/
    _30ExecCommand();
    goto L7; // [813] 1833
L27: 

    /** 		elsif id = FOR then*/
    if (_id_62132 != 21)
    goto L28; // [820] 846

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			For_statement()*/
    _30For_statement();

    /** 			ExecCommand()*/
    _30ExecCommand();
    goto L7; // [843] 1833
L28: 

    /** 		elsif id = WHILE then*/
    if (_id_62132 != 47)
    goto L29; // [850] 876

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			While_statement()*/
    _30While_statement();

    /** 			ExecCommand()*/
    _30ExecCommand();
    goto L7; // [873] 1833
L29: 

    /** 		elsif id = LOOP then*/
    if (_id_62132 != 422)
    goto L2A; // [880] 906

    /** 		    StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 		    Loop_statement()*/
    _30Loop_statement();

    /** 		    ExecCommand()*/
    _30ExecCommand();
    goto L7; // [903] 1833
L2A: 

    /** 		elsif id = PROC or id = QUALIFIED_PROC then*/
    _31455 = (_id_62132 == 27);
    if (_31455 != 0) {
        goto L2B; // [914] 929
    }
    _31457 = (_id_62132 == 521);
    if (_31457 == 0)
    {
        DeRef(_31457);
        _31457 = NOVALUE;
        goto L2C; // [925] 972
    }
    else{
        DeRef(_31457);
        _31457 = NOVALUE;
    }
L2B: 

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			if id = PROC then*/
    if (_id_62132 != 27)
    goto L2D; // [944] 960

    /** 				UndefinedVar( tok[T_SYM] )*/
    _2 = (int)SEQ_PTR(_tok_62131);
    _31459 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_31459);
    _30UndefinedVar(_31459);
    _31459 = NOVALUE;
L2D: 

    /** 			Procedure_call(tok)*/
    Ref(_tok_62131);
    _30Procedure_call(_tok_62131);

    /** 			ExecCommand()*/
    _30ExecCommand();
    goto L7; // [969] 1833
L2C: 

    /** 		elsif id = FUNC or id = QUALIFIED_FUNC then*/
    _31460 = (_id_62132 == 501);
    if (_31460 != 0) {
        goto L2E; // [980] 995
    }
    _31462 = (_id_62132 == 520);
    if (_31462 == 0)
    {
        DeRef(_31462);
        _31462 = NOVALUE;
        goto L2F; // [991] 1051
    }
    else{
        DeRef(_31462);
        _31462 = NOVALUE;
    }
L2E: 

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			if id = FUNC then*/
    if (_id_62132 != 501)
    goto L30; // [1010] 1026

    /** 				UndefinedVar( tok[T_SYM] )*/
    _2 = (int)SEQ_PTR(_tok_62131);
    _31464 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_31464);
    _30UndefinedVar(_31464);
    _31464 = NOVALUE;
L30: 

    /** 			Procedure_call(tok)*/
    Ref(_tok_62131);
    _30Procedure_call(_tok_62131);

    /** 			clear_op()*/
    _37clear_op();

    /** 			if Pop() then end if*/
    _31465 = _37Pop();
    if (_31465 == 0) {
        DeRef(_31465);
        _31465 = NOVALUE;
        goto L31; // [1040] 1044
    }
    else {
        if (!IS_ATOM_INT(_31465) && DBL_PTR(_31465)->dbl == 0.0){
            DeRef(_31465);
            _31465 = NOVALUE;
            goto L31; // [1040] 1044
        }
        DeRef(_31465);
        _31465 = NOVALUE;
    }
    DeRef(_31465);
    _31465 = NOVALUE;
L31: 

    /** 			ExecCommand()*/
    _30ExecCommand();
    goto L7; // [1048] 1833
L2F: 

    /** 		elsif id = RETURN then*/
    if (_id_62132 != 413)
    goto L32; // [1055] 1066

    /** 			Return_statement() -- will fail - not allowed at top level*/
    _30Return_statement();
    goto L7; // [1063] 1833
L32: 

    /** 		elsif id = EXIT then*/
    if (_id_62132 != 61)
    goto L33; // [1070] 1108

    /** 			if nested then*/
    if (_nested_62129 == 0)
    {
        goto L34; // [1076] 1097
    }
    else{
    }

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			Exit_statement()*/
    _30Exit_statement();
    goto L7; // [1094] 1833
L34: 

    /** 			CompileErr(89)*/
    RefDS(_22682);
    _43CompileErr(89, _22682, 0);
    goto L7; // [1105] 1833
L33: 

    /** 		elsif id = INCLUDE then*/
    if (_id_62132 != 418)
    goto L35; // [1112] 1128

    /** 			IncludeScan( 0 )*/
    _60IncludeScan(0);

    /** 			PushGoto()*/
    _30PushGoto();
    goto L7; // [1125] 1833
L35: 

    /** 		elsif id = WITH then*/
    if (_id_62132 != 420)
    goto L36; // [1132] 1146

    /** 			SetWith(TRUE)*/
    _30SetWith(_5TRUE_244);
    goto L7; // [1143] 1833
L36: 

    /** 		elsif id = WITHOUT then*/
    if (_id_62132 != 421)
    goto L37; // [1150] 1164

    /** 			SetWith(FALSE)*/
    _30SetWith(_5FALSE_242);
    goto L7; // [1161] 1833
L37: 

    /** 		elsif id = END_OF_FILE then*/
    if (_id_62132 != -21)
    goto L38; // [1168] 1285

    /** 			if IncludePop() then*/
    _31472 = _60IncludePop();
    if (_31472 == 0) {
        DeRef(_31472);
        _31472 = NOVALUE;
        goto L39; // [1177] 1273
    }
    else {
        if (!IS_ATOM_INT(_31472) && DBL_PTR(_31472)->dbl == 0.0){
            DeRef(_31472);
            _31472 = NOVALUE;
            goto L39; // [1177] 1273
        }
        DeRef(_31472);
        _31472 = NOVALUE;
    }
    DeRef(_31472);
    _31472 = NOVALUE;

    /** 				backed_up_tok = {}*/
    RefDS(_22682);
    DeRef(_30backed_up_tok_55178);
    _30backed_up_tok_55178 = _22682;

    /** 				PopGoto()*/
    _30PopGoto();

    /** 				read_line()*/
    _60read_line();

    /** 				last_ForwardLine     = ThisLine*/
    Ref(_43ThisLine_49532);
    DeRef(_43last_ForwardLine_49535);
    _43last_ForwardLine_49535 = _43ThisLine_49532;

    /** 				last_fwd_line_number = line_number*/
    _25last_fwd_line_number_12266 = _25line_number_12263;

    /** 				last_forward_bp      = bp*/
    _43last_forward_bp_49539 = _43bp_49536;

    /** 				putback_ForwardLine     = ThisLine*/
    Ref(_43ThisLine_49532);
    DeRef(_43putback_ForwardLine_49534);
    _43putback_ForwardLine_49534 = _43ThisLine_49532;

    /** 				putback_fwd_line_number = line_number*/
    _25putback_fwd_line_number_12265 = _25line_number_12263;

    /** 				putback_forward_bp      = bp*/
    _43putback_forward_bp_49538 = _43bp_49536;

    /** 				ForwardLine     = ThisLine*/
    Ref(_43ThisLine_49532);
    DeRef(_43ForwardLine_49533);
    _43ForwardLine_49533 = _43ThisLine_49532;

    /** 				fwd_line_number = line_number*/
    _25fwd_line_number_12264 = _25line_number_12263;

    /** 				forward_bp      = bp*/
    _43forward_bp_49537 = _43bp_49536;
    goto L7; // [1270] 1833
L39: 

    /** 				CheckForUndefinedGotoLabels()*/
    _30CheckForUndefinedGotoLabels();

    /** 				exit -- all finished*/
    goto L2; // [1279] 1843
    goto L7; // [1282] 1833
L38: 

    /** 		elsif id = QUESTION_MARK then*/
    if (_id_62132 != -31)
    goto L3A; // [1289] 1315

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			Print_statement()*/
    _30Print_statement();

    /** 			ExecCommand()*/
    _30ExecCommand();
    goto L7; // [1312] 1833
L3A: 

    /** 		elsif id = LABEL then*/
    if (_id_62132 != 419)
    goto L3B; // [1319] 1341

    /** 			StartSourceLine(TRUE, , COVERAGE_SUPPRESS)*/
    _37StartSourceLine(_5TRUE_244, 0, 1);

    /** 			GLabel_statement()*/
    _30GLabel_statement();
    goto L7; // [1338] 1833
L3B: 

    /** 		elsif id = GOTO then*/
    if (_id_62132 != 188)
    goto L3C; // [1345] 1367

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			Goto_statement()*/
    _30Goto_statement();
    goto L7; // [1364] 1833
L3C: 

    /** 		elsif id = CONTINUE then*/
    if (_id_62132 != 426)
    goto L3D; // [1371] 1409

    /** 			if nested then*/
    if (_nested_62129 == 0)
    {
        goto L3E; // [1377] 1398
    }
    else{
    }

    /** 				StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 				Continue_statement()*/
    _30Continue_statement();
    goto L7; // [1395] 1833
L3E: 

    /** 				CompileErr(50)*/
    RefDS(_22682);
    _43CompileErr(50, _22682, 0);
    goto L7; // [1406] 1833
L3D: 

    /** 		elsif id = RETRY then*/
    if (_id_62132 != 184)
    goto L3F; // [1413] 1451

    /** 			if nested then*/
    if (_nested_62129 == 0)
    {
        goto L40; // [1419] 1440
    }
    else{
    }

    /** 				StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 				Retry_statement()*/
    _30Retry_statement();
    goto L7; // [1437] 1833
L40: 

    /** 				CompileErr(128)*/
    RefDS(_22682);
    _43CompileErr(128, _22682, 0);
    goto L7; // [1448] 1833
L3F: 

    /** 		elsif id = BREAK then*/
    if (_id_62132 != 425)
    goto L41; // [1455] 1493

    /** 			if nested then*/
    if (_nested_62129 == 0)
    {
        goto L42; // [1461] 1482
    }
    else{
    }

    /** 				StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 				Break_statement()*/
    _30Break_statement();
    goto L7; // [1479] 1833
L42: 

    /** 				CompileErr(39)*/
    RefDS(_22682);
    _43CompileErr(39, _22682, 0);
    goto L7; // [1490] 1833
L41: 

    /** 		elsif id = ENTRY then*/
    if (_id_62132 != 424)
    goto L43; // [1497] 1535

    /** 			if nested then*/
    if (_nested_62129 == 0)
    {
        goto L44; // [1503] 1524
    }
    else{
    }

    /** 			    StartSourceLine(TRUE, , COVERAGE_SUPPRESS)*/
    _37StartSourceLine(_5TRUE_244, 0, 1);

    /** 			    Entry_statement()*/
    _30Entry_statement();
    goto L7; // [1521] 1833
L44: 

    /** 				CompileErr(72)*/
    RefDS(_22682);
    _43CompileErr(72, _22682, 0);
    goto L7; // [1532] 1833
L43: 

    /** 		elsif id = IFDEF then*/
    if (_id_62132 != 407)
    goto L45; // [1539] 1561

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			Ifdef_statement()*/
    _30Ifdef_statement();
    goto L7; // [1558] 1833
L45: 

    /** 		elsif id = CASE then*/
    if (_id_62132 != 186)
    goto L46; // [1565] 1576

    /** 			Case_statement()*/
    _30Case_statement();
    goto L7; // [1573] 1833
L46: 

    /** 		elsif id = SWITCH then*/
    if (_id_62132 != 185)
    goto L47; // [1580] 1602

    /** 			StartSourceLine(TRUE)*/
    _37StartSourceLine(_5TRUE_244, 0, 2);

    /** 			Switch_statement()*/
    _30Switch_statement();
    goto L7; // [1599] 1833
L47: 

    /** 		elsif id = ILLEGAL_CHAR then*/
    if (_id_62132 != -20)
    goto L48; // [1606] 1620

    /** 			CompileErr(102)*/
    RefDS(_22682);
    _43CompileErr(102, _22682, 0);
    goto L7; // [1617] 1833
L48: 

    /** 			if nested then*/
    if (_nested_62129 == 0)
    {
        goto L49; // [1622] 1774
    }
    else{
    }

    /** 				if id = ELSE then*/
    if (_id_62132 != 23)
    goto L4A; // [1629] 1683

    /** 					if length(if_stack) = 0 then*/
    if (IS_SEQUENCE(_30if_stack_55206)){
            _31485 = SEQ_PTR(_30if_stack_55206)->length;
    }
    else {
        _31485 = 1;
    }
    if (_31485 != 0)
    goto L4B; // [1640] 1740

    /** 						if live_ifdef > 0 then*/
    if (_30live_ifdef_59349 <= 0)
    goto L4C; // [1648] 1671

    /** 							CompileErr(134, ifdef_lineno[$])*/
    if (IS_SEQUENCE(_30ifdef_lineno_59350)){
            _31488 = SEQ_PTR(_30ifdef_lineno_59350)->length;
    }
    else {
        _31488 = 1;
    }
    _2 = (int)SEQ_PTR(_30ifdef_lineno_59350);
    _31489 = (int)*(((s1_ptr)_2)->base + _31488);
    _43CompileErr(134, _31489, 0);
    _31489 = NOVALUE;
    goto L4B; // [1668] 1740
L4C: 

    /** 							CompileErr(118)*/
    RefDS(_22682);
    _43CompileErr(118, _22682, 0);
    goto L4B; // [1680] 1740
L4A: 

    /** 				elsif id = ELSIF then*/
    if (_id_62132 != 414)
    goto L4D; // [1687] 1739

    /** 					if length(if_stack) = 0 then*/
    if (IS_SEQUENCE(_30if_stack_55206)){
            _31491 = SEQ_PTR(_30if_stack_55206)->length;
    }
    else {
        _31491 = 1;
    }
    if (_31491 != 0)
    goto L4E; // [1698] 1738

    /** 						if live_ifdef > 0 then*/
    if (_30live_ifdef_59349 <= 0)
    goto L4F; // [1706] 1729

    /** 							CompileErr(139, ifdef_lineno[$])*/
    if (IS_SEQUENCE(_30ifdef_lineno_59350)){
            _31494 = SEQ_PTR(_30ifdef_lineno_59350)->length;
    }
    else {
        _31494 = 1;
    }
    _2 = (int)SEQ_PTR(_30ifdef_lineno_59350);
    _31495 = (int)*(((s1_ptr)_2)->base + _31494);
    _43CompileErr(139, _31495, 0);
    _31495 = NOVALUE;
    goto L50; // [1726] 1737
L4F: 

    /** 							CompileErr(119)*/
    RefDS(_22682);
    _43CompileErr(119, _22682, 0);
L50: 
L4E: 
L4D: 
L4B: 

    /** 				putback(tok)*/
    Ref(_tok_62131);
    _30putback(_tok_62131);

    /** 				if stmt_nest > 0 then*/
    if (_30stmt_nest_55203 <= 0)
    goto L51; // [1749] 1766

    /** 					stmt_nest -= 1*/
    _30stmt_nest_55203 = _30stmt_nest_55203 - 1;

    /** 					InitDelete()*/
    _30InitDelete();
L51: 

    /** 				return*/
    DeRef(_tok_62131);
    DeRef(_31427);
    _31427 = NOVALUE;
    DeRef(_31419);
    _31419 = NOVALUE;
    DeRef(_31424);
    _31424 = NOVALUE;
    DeRef(_31446);
    _31446 = NOVALUE;
    DeRef(_31399);
    _31399 = NOVALUE;
    DeRef(_31448);
    _31448 = NOVALUE;
    DeRef(_31391);
    _31391 = NOVALUE;
    DeRef(_31410);
    _31410 = NOVALUE;
    _31431 = NOVALUE;
    DeRef(_31416);
    _31416 = NOVALUE;
    DeRef(_31388);
    _31388 = NOVALUE;
    DeRef(_31421);
    _31421 = NOVALUE;
    _31385 = NOVALUE;
    DeRef(_31393);
    _31393 = NOVALUE;
    DeRef(_31438);
    _31438 = NOVALUE;
    DeRef(_31460);
    _31460 = NOVALUE;
    DeRef(_31382);
    _31382 = NOVALUE;
    DeRef(_31397);
    _31397 = NOVALUE;
    DeRef(_31414);
    _31414 = NOVALUE;
    DeRef(_31418);
    _31418 = NOVALUE;
    DeRef(_31450);
    _31450 = NOVALUE;
    DeRef(_31434);
    _31434 = NOVALUE;
    DeRef(_31455);
    _31455 = NOVALUE;
    DeRef(_31401);
    _31401 = NOVALUE;
    DeRef(_31429);
    _31429 = NOVALUE;
    return;
    goto L52; // [1771] 1832
L49: 

    /** 				if id = END then*/
    if (_id_62132 != 402)
    goto L53; // [1778] 1809

    /** 					tok = next_token()*/
    _0 = _tok_62131;
    _tok_62131 = _30next_token();
    DeRef(_0);

    /** 					CompileErr(17, {find_token_text(tok[T_ID])})*/
    _2 = (int)SEQ_PTR(_tok_62131);
    _31500 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_31500);
    _31501 = _63find_token_text(_31500);
    _31500 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _31501;
    _31502 = MAKE_SEQ(_1);
    _31501 = NOVALUE;
    _43CompileErr(17, _31502, 0);
    _31502 = NOVALUE;
L53: 

    /** 				CompileErr(117, { match_replace(",", find_token_text(id), "") })*/
    _31503 = _63find_token_text(_id_62132);
    RefDS(_27011);
    RefDS(_22682);
    _31504 = _7match_replace(_27011, _31503, _22682, 0);
    _31503 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _31504;
    _31505 = MAKE_SEQ(_1);
    _31504 = NOVALUE;
    _43CompileErr(117, _31505, 0);
    _31505 = NOVALUE;
L52: 
L7: 

    /** 		flush_temps()*/
    RefDS(_22682);
    _37flush_temps(_22682);

    /** 	end while*/
    goto L1; // [1840] 12
L2: 

    /** 	emit_op(RETURNT)*/
    _37emit_op(34);

    /** 	clear_last()*/
    _37clear_last();

    /** 	StraightenBranches()*/
    _30StraightenBranches();

    /** 	SymTab[TopLevelSub][S_CODE] = Code*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25TopLevelSub_12269 + ((s1_ptr)_2)->base);
    RefDS(_25Code_12355);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_CODE_11925))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_CODE_11925);
    _1 = *(int *)_2;
    *(int *)_2 = _25Code_12355;
    DeRef(_1);
    _31506 = NOVALUE;

    /** 	EndLineTable()*/
    _30EndLineTable();

    /** 	SymTab[TopLevelSub][S_LINETAB] = LineTable*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25TopLevelSub_12269 + ((s1_ptr)_2)->base);
    RefDS(_25LineTable_12356);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_LINETAB_11948))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_LINETAB_11948)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_LINETAB_11948);
    _1 = *(int *)_2;
    *(int *)_2 = _25LineTable_12356;
    DeRef(_1);
    _31508 = NOVALUE;

    /** end procedure*/
    DeRef(_tok_62131);
    DeRef(_31427);
    _31427 = NOVALUE;
    DeRef(_31419);
    _31419 = NOVALUE;
    DeRef(_31424);
    _31424 = NOVALUE;
    DeRef(_31446);
    _31446 = NOVALUE;
    DeRef(_31399);
    _31399 = NOVALUE;
    DeRef(_31448);
    _31448 = NOVALUE;
    DeRef(_31391);
    _31391 = NOVALUE;
    DeRef(_31410);
    _31410 = NOVALUE;
    _31431 = NOVALUE;
    DeRef(_31416);
    _31416 = NOVALUE;
    DeRef(_31388);
    _31388 = NOVALUE;
    DeRef(_31421);
    _31421 = NOVALUE;
    _31385 = NOVALUE;
    DeRef(_31393);
    _31393 = NOVALUE;
    DeRef(_31438);
    _31438 = NOVALUE;
    DeRef(_31460);
    _31460 = NOVALUE;
    DeRef(_31382);
    _31382 = NOVALUE;
    DeRef(_31397);
    _31397 = NOVALUE;
    DeRef(_31414);
    _31414 = NOVALUE;
    DeRef(_31418);
    _31418 = NOVALUE;
    DeRef(_31450);
    _31450 = NOVALUE;
    DeRef(_31434);
    _31434 = NOVALUE;
    DeRef(_31455);
    _31455 = NOVALUE;
    DeRef(_31401);
    _31401 = NOVALUE;
    DeRef(_31429);
    _31429 = NOVALUE;
    return;
    ;
}


void _30parser()
{
    int _0, _1, _2;
    

    /** 	real_parser(0)*/
    _30real_parser(0);

    /** 	mark_final_targets()*/
    _52mark_final_targets();

    /** 	resolve_unincluded_globals( 1 )*/
    _52resolve_unincluded_globals(1);

    /** 	Resolve_forward_references( 1 )*/
    _29Resolve_forward_references(1);

    /** 	inline_deferred_calls()*/
    _67inline_deferred_calls();

    /** 	End_block( PROC )*/
    _66End_block(27);

    /** 	Code = {}*/
    RefDS(_22682);
    DeRef(_25Code_12355);
    _25Code_12355 = _22682;

    /** 	LineTable = {}*/
    RefDS(_22682);
    DeRef(_25LineTable_12356);
    _25LineTable_12356 = _22682;

    /** end procedure*/
    return;
    ;
}


void _30nested_parser()
{
    int _0, _1, _2;
    

    /** 	real_parser(1)*/
    _30real_parser(1);

    /** end procedure*/
    return;
    ;
}



// 0x6EE8F9D4
