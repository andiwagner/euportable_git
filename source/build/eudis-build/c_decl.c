// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _56get_eucompiledir()
{
    int _x_42680 = NOVALUE;
    int _23018 = NOVALUE;
    int _23014 = NOVALUE;
    int _0, _1, _2;
    

    /** 	object x = getenv("EUCOMPILEDIR")*/
    DeRef(_x_42680);
    _x_42680 = EGetEnv(_23012);

    /** 	if is_eudir_from_cmdline() then*/
    _23014 = _26is_eudir_from_cmdline();
    if (_23014 == 0) {
        DeRef(_23014);
        _23014 = NOVALUE;
        goto L1; // [11] 20
    }
    else {
        if (!IS_ATOM_INT(_23014) && DBL_PTR(_23014)->dbl == 0.0){
            DeRef(_23014);
            _23014 = NOVALUE;
            goto L1; // [11] 20
        }
        DeRef(_23014);
        _23014 = NOVALUE;
    }
    DeRef(_23014);
    _23014 = NOVALUE;

    /** 		x = get_eudir()*/
    _0 = _x_42680;
    _x_42680 = _26get_eudir();
    DeRefi(_0);
L1: 

    /** 	ifdef UNIX then*/

    /** 	if equal(x, -1) then*/
    if (_x_42680 == -1)
    _23018 = 1;
    else if (IS_ATOM_INT(_x_42680) && IS_ATOM_INT(-1))
    _23018 = 0;
    else
    _23018 = (compare(_x_42680, -1) == 0);
    if (_23018 == 0)
    {
        _23018 = NOVALUE;
        goto L2; // [28] 37
    }
    else{
        _23018 = NOVALUE;
    }

    /** 		x = get_eudir()*/
    _0 = _x_42680;
    _x_42680 = _26get_eudir();
    DeRef(_0);
L2: 

    /** 	return x*/
    return _x_42680;
    ;
}


void _56NewBB(int _a_call_42696, int _mask_42697, int _sub_42699)
{
    int _s_42701 = NOVALUE;
    int _23051 = NOVALUE;
    int _23050 = NOVALUE;
    int _23048 = NOVALUE;
    int _23047 = NOVALUE;
    int _23045 = NOVALUE;
    int _23044 = NOVALUE;
    int _23043 = NOVALUE;
    int _23042 = NOVALUE;
    int _23041 = NOVALUE;
    int _23040 = NOVALUE;
    int _23039 = NOVALUE;
    int _23038 = NOVALUE;
    int _23037 = NOVALUE;
    int _23036 = NOVALUE;
    int _23035 = NOVALUE;
    int _23034 = NOVALUE;
    int _23033 = NOVALUE;
    int _23032 = NOVALUE;
    int _23031 = NOVALUE;
    int _23030 = NOVALUE;
    int _23029 = NOVALUE;
    int _23028 = NOVALUE;
    int _23027 = NOVALUE;
    int _23026 = NOVALUE;
    int _23025 = NOVALUE;
    int _23024 = NOVALUE;
    int _23023 = NOVALUE;
    int _23021 = NOVALUE;
    int _23020 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_a_call_42696)) {
        _1 = (long)(DBL_PTR(_a_call_42696)->dbl);
        if (UNIQUE(DBL_PTR(_a_call_42696)) && (DBL_PTR(_a_call_42696)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_a_call_42696);
        _a_call_42696 = _1;
    }
    if (!IS_ATOM_INT(_mask_42697)) {
        _1 = (long)(DBL_PTR(_mask_42697)->dbl);
        if (UNIQUE(DBL_PTR(_mask_42697)) && (DBL_PTR(_mask_42697)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mask_42697);
        _mask_42697 = _1;
    }
    if (!IS_ATOM_INT(_sub_42699)) {
        _1 = (long)(DBL_PTR(_sub_42699)->dbl);
        if (UNIQUE(DBL_PTR(_sub_42699)) && (DBL_PTR(_sub_42699)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sub_42699);
        _sub_42699 = _1;
    }

    /** 	if a_call then*/
    if (_a_call_42696 == 0)
    {
        goto L1; // [9] 258
    }
    else{
    }

    /** 		for i = 1 to length(BB_info) do*/
    if (IS_SEQUENCE(_56BB_info_42650)){
            _23020 = SEQ_PTR(_56BB_info_42650)->length;
    }
    else {
        _23020 = 1;
    }
    {
        int _i_42704;
        _i_42704 = 1;
L2: 
        if (_i_42704 > _23020){
            goto L3; // [19] 255
        }

        /** 			s = BB_info[i][BB_VAR]*/
        _2 = (int)SEQ_PTR(_56BB_info_42650);
        _23021 = (int)*(((s1_ptr)_2)->base + _i_42704);
        _2 = (int)SEQ_PTR(_23021);
        _s_42701 = (int)*(((s1_ptr)_2)->base + 1);
        if (!IS_ATOM_INT(_s_42701)){
            _s_42701 = (long)DBL_PTR(_s_42701)->dbl;
        }
        _23021 = NOVALUE;

        /** 			if SymTab[s][S_MODE] = M_NORMAL and*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _23023 = (int)*(((s1_ptr)_2)->base + _s_42701);
        _2 = (int)SEQ_PTR(_23023);
        _23024 = (int)*(((s1_ptr)_2)->base + 3);
        _23023 = NOVALUE;
        if (IS_ATOM_INT(_23024)) {
            _23025 = (_23024 == 1);
        }
        else {
            _23025 = binary_op(EQUALS, _23024, 1);
        }
        _23024 = NOVALUE;
        if (IS_ATOM_INT(_23025)) {
            if (_23025 == 0) {
                goto L4; // [62] 248
            }
        }
        else {
            if (DBL_PTR(_23025)->dbl == 0.0) {
                goto L4; // [62] 248
            }
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _23027 = (int)*(((s1_ptr)_2)->base + _s_42701);
        _2 = (int)SEQ_PTR(_23027);
        _23028 = (int)*(((s1_ptr)_2)->base + 4);
        _23027 = NOVALUE;
        if (IS_ATOM_INT(_23028)) {
            _23029 = (_23028 == 6);
        }
        else {
            _23029 = binary_op(EQUALS, _23028, 6);
        }
        _23028 = NOVALUE;
        if (IS_ATOM_INT(_23029)) {
            if (_23029 != 0) {
                DeRef(_23030);
                _23030 = 1;
                goto L5; // [84] 110
            }
        }
        else {
            if (DBL_PTR(_23029)->dbl != 0.0) {
                DeRef(_23030);
                _23030 = 1;
                goto L5; // [84] 110
            }
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _23031 = (int)*(((s1_ptr)_2)->base + _s_42701);
        _2 = (int)SEQ_PTR(_23031);
        _23032 = (int)*(((s1_ptr)_2)->base + 4);
        _23031 = NOVALUE;
        if (IS_ATOM_INT(_23032)) {
            _23033 = (_23032 == 5);
        }
        else {
            _23033 = binary_op(EQUALS, _23032, 5);
        }
        _23032 = NOVALUE;
        DeRef(_23030);
        if (IS_ATOM_INT(_23033))
        _23030 = (_23033 != 0);
        else
        _23030 = DBL_PTR(_23033)->dbl != 0.0;
L5: 
        if (_23030 != 0) {
            _23034 = 1;
            goto L6; // [110] 136
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _23035 = (int)*(((s1_ptr)_2)->base + _s_42701);
        _2 = (int)SEQ_PTR(_23035);
        _23036 = (int)*(((s1_ptr)_2)->base + 4);
        _23035 = NOVALUE;
        if (IS_ATOM_INT(_23036)) {
            _23037 = (_23036 == 11);
        }
        else {
            _23037 = binary_op(EQUALS, _23036, 11);
        }
        _23036 = NOVALUE;
        if (IS_ATOM_INT(_23037))
        _23034 = (_23037 != 0);
        else
        _23034 = DBL_PTR(_23037)->dbl != 0.0;
L6: 
        if (_23034 != 0) {
            DeRef(_23038);
            _23038 = 1;
            goto L7; // [136] 162
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _23039 = (int)*(((s1_ptr)_2)->base + _s_42701);
        _2 = (int)SEQ_PTR(_23039);
        _23040 = (int)*(((s1_ptr)_2)->base + 4);
        _23039 = NOVALUE;
        if (IS_ATOM_INT(_23040)) {
            _23041 = (_23040 == 13);
        }
        else {
            _23041 = binary_op(EQUALS, _23040, 13);
        }
        _23040 = NOVALUE;
        if (IS_ATOM_INT(_23041))
        _23038 = (_23041 != 0);
        else
        _23038 = DBL_PTR(_23041)->dbl != 0.0;
L7: 
        if (_23038 == 0)
        {
            _23038 = NOVALUE;
            goto L4; // [163] 248
        }
        else{
            _23038 = NOVALUE;
        }

        /** 				  if and_bits(mask, power(2, remainder(s, E_SIZE))) then*/
        _23042 = (_s_42701 % 29);
        _23043 = power(2, _23042);
        _23042 = NOVALUE;
        if (IS_ATOM_INT(_23043)) {
            {unsigned long tu;
                 tu = (unsigned long)_mask_42697 & (unsigned long)_23043;
                 _23044 = MAKE_UINT(tu);
            }
        }
        else {
            temp_d.dbl = (double)_mask_42697;
            _23044 = Dand_bits(&temp_d, DBL_PTR(_23043));
        }
        DeRef(_23043);
        _23043 = NOVALUE;
        if (_23044 == 0) {
            DeRef(_23044);
            _23044 = NOVALUE;
            goto L8; // [182] 247
        }
        else {
            if (!IS_ATOM_INT(_23044) && DBL_PTR(_23044)->dbl == 0.0){
                DeRef(_23044);
                _23044 = NOVALUE;
                goto L8; // [182] 247
            }
            DeRef(_23044);
            _23044 = NOVALUE;
        }
        DeRef(_23044);
        _23044 = NOVALUE;

        /** 					  if mask = E_ALL_EFFECT or s < sub then*/
        _23045 = (_mask_42697 == 1073741823);
        if (_23045 != 0) {
            goto L9; // [193] 206
        }
        _23047 = (_s_42701 < _sub_42699);
        if (_23047 == 0)
        {
            DeRef(_23047);
            _23047 = NOVALUE;
            goto LA; // [202] 246
        }
        else{
            DeRef(_23047);
            _23047 = NOVALUE;
        }
L9: 

        /** 						  BB_info[i][BB_TYPE..BB_OBJ] =*/
        _2 = (int)SEQ_PTR(_56BB_info_42650);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _56BB_info_42650 = MAKE_SEQ(_2);
        }
        _3 = (int)(_i_42704 + ((s1_ptr)_2)->base);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = -1073741824;
        ((int *)_2)[2] = 1073741823;
        _23050 = MAKE_SEQ(_1);
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = 0;
        *((int *)(_2+8)) = 0;
        Ref(_25NOVALUE_12115);
        *((int *)(_2+12)) = _25NOVALUE_12115;
        *((int *)(_2+16)) = _23050;
        _23051 = MAKE_SEQ(_1);
        _23050 = NOVALUE;
        assign_slice_seq = (s1_ptr *)_3;
        AssignSlice(2, 5, _23051);
        DeRefDS(_23051);
        _23051 = NOVALUE;
LA: 
L8: 
L4: 

        /** 		end for*/
        _i_42704 = _i_42704 + 1;
        goto L2; // [250] 26
L3: 
        ;
    }
    goto LB; // [255] 266
L1: 

    /** 		BB_info = {}*/
    RefDS(_22682);
    DeRef(_56BB_info_42650);
    _56BB_info_42650 = _22682;
LB: 

    /** end procedure*/
    DeRef(_23045);
    _23045 = NOVALUE;
    DeRef(_23025);
    _23025 = NOVALUE;
    DeRef(_23029);
    _23029 = NOVALUE;
    DeRef(_23033);
    _23033 = NOVALUE;
    DeRef(_23037);
    _23037 = NOVALUE;
    DeRef(_23041);
    _23041 = NOVALUE;
    DeRef(_23048);
    _23048 = NOVALUE;
    return;
    ;
}


int _56BB_var_obj(int _var_42769)
{
    int _bbi_42770 = NOVALUE;
    int _23062 = NOVALUE;
    int _23060 = NOVALUE;
    int _23058 = NOVALUE;
    int _23057 = NOVALUE;
    int _23055 = NOVALUE;
    int _23053 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_var_42769)) {
        _1 = (long)(DBL_PTR(_var_42769)->dbl);
        if (UNIQUE(DBL_PTR(_var_42769)) && (DBL_PTR(_var_42769)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_var_42769);
        _var_42769 = _1;
    }

    /** 	for i = length(BB_info) to 1 by -1 do*/
    if (IS_SEQUENCE(_56BB_info_42650)){
            _23053 = SEQ_PTR(_56BB_info_42650)->length;
    }
    else {
        _23053 = 1;
    }
    {
        int _i_42772;
        _i_42772 = _23053;
L1: 
        if (_i_42772 < 1){
            goto L2; // [10] 105
        }

        /** 		bbi = BB_info[i]*/
        DeRef(_bbi_42770);
        _2 = (int)SEQ_PTR(_56BB_info_42650);
        _bbi_42770 = (int)*(((s1_ptr)_2)->base + _i_42772);
        Ref(_bbi_42770);

        /** 		if bbi[BB_VAR] != var then*/
        _2 = (int)SEQ_PTR(_bbi_42770);
        _23055 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(EQUALS, _23055, _var_42769)){
            _23055 = NOVALUE;
            goto L3; // [33] 42
        }
        _23055 = NOVALUE;

        /** 			continue*/
        goto L4; // [39] 100
L3: 

        /** 		if SymTab[var][S_MODE] != M_NORMAL then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _23057 = (int)*(((s1_ptr)_2)->base + _var_42769);
        _2 = (int)SEQ_PTR(_23057);
        _23058 = (int)*(((s1_ptr)_2)->base + 3);
        _23057 = NOVALUE;
        if (binary_op_a(EQUALS, _23058, 1)){
            _23058 = NOVALUE;
            goto L5; // [58] 67
        }
        _23058 = NOVALUE;

        /** 			continue*/
        goto L4; // [64] 100
L5: 

        /** 		if bbi[BB_TYPE] != TYPE_INTEGER then*/
        _2 = (int)SEQ_PTR(_bbi_42770);
        _23060 = (int)*(((s1_ptr)_2)->base + 2);
        if (binary_op_a(EQUALS, _23060, 1)){
            _23060 = NOVALUE;
            goto L6; // [77] 86
        }
        _23060 = NOVALUE;

        /** 			exit*/
        goto L2; // [83] 105
L6: 

        /** 		return bbi[BB_OBJ]*/
        _2 = (int)SEQ_PTR(_bbi_42770);
        _23062 = (int)*(((s1_ptr)_2)->base + 5);
        Ref(_23062);
        DeRef(_bbi_42770);
        return _23062;

        /** 	end for*/
L4: 
        _i_42772 = _i_42772 + -1;
        goto L1; // [100] 17
L2: 
        ;
    }

    /** 	return BB_def_values*/
    RefDS(_56BB_def_values_42763);
    DeRef(_bbi_42770);
    _23062 = NOVALUE;
    return _56BB_def_values_42763;
    ;
}


int _56BB_var_type(int _var_42792)
{
    int _23077 = NOVALUE;
    int _23076 = NOVALUE;
    int _23074 = NOVALUE;
    int _23073 = NOVALUE;
    int _23072 = NOVALUE;
    int _23071 = NOVALUE;
    int _23070 = NOVALUE;
    int _23069 = NOVALUE;
    int _23068 = NOVALUE;
    int _23067 = NOVALUE;
    int _23066 = NOVALUE;
    int _23065 = NOVALUE;
    int _23064 = NOVALUE;
    int _23063 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_var_42792)) {
        _1 = (long)(DBL_PTR(_var_42792)->dbl);
        if (UNIQUE(DBL_PTR(_var_42792)) && (DBL_PTR(_var_42792)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_var_42792);
        _var_42792 = _1;
    }

    /** 	for i = length(BB_info) to 1 by -1 do*/
    if (IS_SEQUENCE(_56BB_info_42650)){
            _23063 = SEQ_PTR(_56BB_info_42650)->length;
    }
    else {
        _23063 = 1;
    }
    {
        int _i_42794;
        _i_42794 = _23063;
L1: 
        if (_i_42794 < 1){
            goto L2; // [10] 133
        }

        /** 		if BB_info[i][BB_VAR] = var and*/
        _2 = (int)SEQ_PTR(_56BB_info_42650);
        _23064 = (int)*(((s1_ptr)_2)->base + _i_42794);
        _2 = (int)SEQ_PTR(_23064);
        _23065 = (int)*(((s1_ptr)_2)->base + 1);
        _23064 = NOVALUE;
        if (IS_ATOM_INT(_23065)) {
            _23066 = (_23065 == _var_42792);
        }
        else {
            _23066 = binary_op(EQUALS, _23065, _var_42792);
        }
        _23065 = NOVALUE;
        if (IS_ATOM_INT(_23066)) {
            if (_23066 == 0) {
                goto L3; // [35] 126
            }
        }
        else {
            if (DBL_PTR(_23066)->dbl == 0.0) {
                goto L3; // [35] 126
            }
        }
        _2 = (int)SEQ_PTR(_56BB_info_42650);
        _23068 = (int)*(((s1_ptr)_2)->base + _i_42794);
        _2 = (int)SEQ_PTR(_23068);
        _23069 = (int)*(((s1_ptr)_2)->base + 1);
        _23068 = NOVALUE;
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!IS_ATOM_INT(_23069)){
            _23070 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_23069)->dbl));
        }
        else{
            _23070 = (int)*(((s1_ptr)_2)->base + _23069);
        }
        _2 = (int)SEQ_PTR(_23070);
        _23071 = (int)*(((s1_ptr)_2)->base + 3);
        _23070 = NOVALUE;
        if (IS_ATOM_INT(_23071)) {
            _23072 = (_23071 == 1);
        }
        else {
            _23072 = binary_op(EQUALS, _23071, 1);
        }
        _23071 = NOVALUE;
        if (_23072 == 0) {
            DeRef(_23072);
            _23072 = NOVALUE;
            goto L3; // [70] 126
        }
        else {
            if (!IS_ATOM_INT(_23072) && DBL_PTR(_23072)->dbl == 0.0){
                DeRef(_23072);
                _23072 = NOVALUE;
                goto L3; // [70] 126
            }
            DeRef(_23072);
            _23072 = NOVALUE;
        }
        DeRef(_23072);
        _23072 = NOVALUE;

        /** 			ifdef DEBUG then*/

        /** 			if BB_info[i][BB_TYPE] = TYPE_NULL then  -- var has only been read*/
        _2 = (int)SEQ_PTR(_56BB_info_42650);
        _23073 = (int)*(((s1_ptr)_2)->base + _i_42794);
        _2 = (int)SEQ_PTR(_23073);
        _23074 = (int)*(((s1_ptr)_2)->base + 2);
        _23073 = NOVALUE;
        if (binary_op_a(NOTEQ, _23074, 0)){
            _23074 = NOVALUE;
            goto L4; // [91] 106
        }
        _23074 = NOVALUE;

        /** 				return TYPE_OBJECT*/
        _23069 = NOVALUE;
        DeRef(_23066);
        _23066 = NOVALUE;
        return 16;
        goto L5; // [103] 125
L4: 

        /** 				return BB_info[i][BB_TYPE]*/
        _2 = (int)SEQ_PTR(_56BB_info_42650);
        _23076 = (int)*(((s1_ptr)_2)->base + _i_42794);
        _2 = (int)SEQ_PTR(_23076);
        _23077 = (int)*(((s1_ptr)_2)->base + 2);
        _23076 = NOVALUE;
        Ref(_23077);
        _23069 = NOVALUE;
        DeRef(_23066);
        _23066 = NOVALUE;
        return _23077;
L5: 
L3: 

        /** 	end for*/
        _i_42794 = _i_42794 + -1;
        goto L1; // [128] 17
L2: 
        ;
    }

    /** 	return TYPE_OBJECT*/
    _23069 = NOVALUE;
    DeRef(_23066);
    _23066 = NOVALUE;
    _23077 = NOVALUE;
    return 16;
    ;
}


int _56GType(int _s_42822)
{
    int _t_42823 = NOVALUE;
    int _local_t_42824 = NOVALUE;
    int _23081 = NOVALUE;
    int _23080 = NOVALUE;
    int _23078 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_s_42822)) {
        _1 = (long)(DBL_PTR(_s_42822)->dbl);
        if (UNIQUE(DBL_PTR(_s_42822)) && (DBL_PTR(_s_42822)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_42822);
        _s_42822 = _1;
    }

    /** 	t = SymTab[s][S_GTYPE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23078 = (int)*(((s1_ptr)_2)->base + _s_42822);
    _2 = (int)SEQ_PTR(_23078);
    _t_42823 = (int)*(((s1_ptr)_2)->base + 36);
    if (!IS_ATOM_INT(_t_42823)){
        _t_42823 = (long)DBL_PTR(_t_42823)->dbl;
    }
    _23078 = NOVALUE;

    /** 	ifdef DEBUG then*/

    /** 	if SymTab[s][S_MODE] != M_NORMAL then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23080 = (int)*(((s1_ptr)_2)->base + _s_42822);
    _2 = (int)SEQ_PTR(_23080);
    _23081 = (int)*(((s1_ptr)_2)->base + 3);
    _23080 = NOVALUE;
    if (binary_op_a(EQUALS, _23081, 1)){
        _23081 = NOVALUE;
        goto L1; // [37] 48
    }
    _23081 = NOVALUE;

    /** 		return t*/
    return _t_42823;
L1: 

    /** 	local_t = BB_var_type(s)*/
    _local_t_42824 = _56BB_var_type(_s_42822);
    if (!IS_ATOM_INT(_local_t_42824)) {
        _1 = (long)(DBL_PTR(_local_t_42824)->dbl);
        if (UNIQUE(DBL_PTR(_local_t_42824)) && (DBL_PTR(_local_t_42824)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_local_t_42824);
        _local_t_42824 = _1;
    }

    /** 	if local_t = TYPE_OBJECT then*/
    if (_local_t_42824 != 16)
    goto L2; // [60] 71

    /** 		return t*/
    return _t_42823;
L2: 

    /** 	if t = TYPE_INTEGER then*/
    if (_t_42823 != 1)
    goto L3; // [75] 88

    /** 		return TYPE_INTEGER*/
    return 1;
L3: 

    /** 	return local_t*/
    return _local_t_42824;
    ;
}


int _56GDelete()
{
    int _0, _1, _2;
    

    /** 	return g_has_delete*/
    return _56g_has_delete_42844;
    ;
}


int _56HasDelete(int _s_42851)
{
    int _23096 = NOVALUE;
    int _23095 = NOVALUE;
    int _23093 = NOVALUE;
    int _23092 = NOVALUE;
    int _23091 = NOVALUE;
    int _23090 = NOVALUE;
    int _23088 = NOVALUE;
    int _23087 = NOVALUE;
    int _23086 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_s_42851)) {
        _1 = (long)(DBL_PTR(_s_42851)->dbl);
        if (UNIQUE(DBL_PTR(_s_42851)) && (DBL_PTR(_s_42851)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_42851);
        _s_42851 = _1;
    }

    /** 	for i = length(BB_info) to 1 by -1 do*/
    if (IS_SEQUENCE(_56BB_info_42650)){
            _23086 = SEQ_PTR(_56BB_info_42650)->length;
    }
    else {
        _23086 = 1;
    }
    {
        int _i_42853;
        _i_42853 = _23086;
L1: 
        if (_i_42853 < 1){
            goto L2; // [10] 61
        }

        /** 		if BB_info[i][BB_VAR] = s then*/
        _2 = (int)SEQ_PTR(_56BB_info_42650);
        _23087 = (int)*(((s1_ptr)_2)->base + _i_42853);
        _2 = (int)SEQ_PTR(_23087);
        _23088 = (int)*(((s1_ptr)_2)->base + 1);
        _23087 = NOVALUE;
        if (binary_op_a(NOTEQ, _23088, _s_42851)){
            _23088 = NOVALUE;
            goto L3; // [31] 54
        }
        _23088 = NOVALUE;

        /** 			return BB_info[i][BB_DELETE]*/
        _2 = (int)SEQ_PTR(_56BB_info_42650);
        _23090 = (int)*(((s1_ptr)_2)->base + _i_42853);
        _2 = (int)SEQ_PTR(_23090);
        _23091 = (int)*(((s1_ptr)_2)->base + 6);
        _23090 = NOVALUE;
        Ref(_23091);
        return _23091;
L3: 

        /** 	end for*/
        _i_42853 = _i_42853 + -1;
        goto L1; // [56] 17
L2: 
        ;
    }

    /** 	if length(SymTab[s]) < S_HAS_DELETE then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23092 = (int)*(((s1_ptr)_2)->base + _s_42851);
    if (IS_SEQUENCE(_23092)){
            _23093 = SEQ_PTR(_23092)->length;
    }
    else {
        _23093 = 1;
    }
    _23092 = NOVALUE;
    if (_23093 >= 54)
    goto L4; // [74] 85

    /** 		return 0*/
    _23091 = NOVALUE;
    _23092 = NOVALUE;
    return 0;
L4: 

    /** 	return SymTab[s][S_HAS_DELETE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23095 = (int)*(((s1_ptr)_2)->base + _s_42851);
    _2 = (int)SEQ_PTR(_23095);
    _23096 = (int)*(((s1_ptr)_2)->base + 54);
    _23095 = NOVALUE;
    Ref(_23096);
    _23091 = NOVALUE;
    _23092 = NOVALUE;
    return _23096;
    ;
}


int _56ObjValue(int _s_42874)
{
    int _local_t_42875 = NOVALUE;
    int _st_42876 = NOVALUE;
    int _tmin_42877 = NOVALUE;
    int _tmax_42878 = NOVALUE;
    int _23109 = NOVALUE;
    int _23107 = NOVALUE;
    int _23106 = NOVALUE;
    int _23104 = NOVALUE;
    int _23101 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_s_42874)) {
        _1 = (long)(DBL_PTR(_s_42874)->dbl);
        if (UNIQUE(DBL_PTR(_s_42874)) && (DBL_PTR(_s_42874)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_42874);
        _s_42874 = _1;
    }

    /** 	st = SymTab[s]*/
    DeRef(_st_42876);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _st_42876 = (int)*(((s1_ptr)_2)->base + _s_42874);
    Ref(_st_42876);

    /** 	tmin = st[S_OBJ_MIN]*/
    DeRef(_tmin_42877);
    _2 = (int)SEQ_PTR(_st_42876);
    _tmin_42877 = (int)*(((s1_ptr)_2)->base + 30);
    Ref(_tmin_42877);

    /** 	tmax = st[S_OBJ_MAX]*/
    DeRef(_tmax_42878);
    _2 = (int)SEQ_PTR(_st_42876);
    _tmax_42878 = (int)*(((s1_ptr)_2)->base + 31);
    Ref(_tmax_42878);

    /** 	if tmin != tmax then*/
    if (binary_op_a(EQUALS, _tmin_42877, _tmax_42878)){
        goto L1; // [29] 41
    }

    /** 		tmin = NOVALUE*/
    Ref(_25NOVALUE_12115);
    DeRef(_tmin_42877);
    _tmin_42877 = _25NOVALUE_12115;
L1: 

    /** 	if st[S_MODE] != M_NORMAL then*/
    _2 = (int)SEQ_PTR(_st_42876);
    _23101 = (int)*(((s1_ptr)_2)->base + 3);
    if (binary_op_a(EQUALS, _23101, 1)){
        _23101 = NOVALUE;
        goto L2; // [51] 62
    }
    _23101 = NOVALUE;

    /** 		return tmin*/
    DeRef(_local_t_42875);
    DeRef(_st_42876);
    DeRef(_tmax_42878);
    return _tmin_42877;
L2: 

    /** 	local_t = BB_var_obj(s)*/
    _0 = _local_t_42875;
    _local_t_42875 = _56BB_var_obj(_s_42874);
    DeRef(_0);

    /** 	if local_t[MIN] = NOVALUE then*/
    _2 = (int)SEQ_PTR(_local_t_42875);
    _23104 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _23104, _25NOVALUE_12115)){
        _23104 = NOVALUE;
        goto L3; // [80] 91
    }
    _23104 = NOVALUE;

    /** 		return tmin*/
    DeRefDS(_local_t_42875);
    DeRef(_st_42876);
    DeRef(_tmax_42878);
    return _tmin_42877;
L3: 

    /** 	if local_t[MIN] != local_t[MAX] then*/
    _2 = (int)SEQ_PTR(_local_t_42875);
    _23106 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_local_t_42875);
    _23107 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(EQUALS, _23106, _23107)){
        _23106 = NOVALUE;
        _23107 = NOVALUE;
        goto L4; // [105] 116
    }
    _23106 = NOVALUE;
    _23107 = NOVALUE;

    /** 		return tmin*/
    DeRefDS(_local_t_42875);
    DeRef(_st_42876);
    DeRef(_tmax_42878);
    return _tmin_42877;
L4: 

    /** 	return local_t[MIN]*/
    _2 = (int)SEQ_PTR(_local_t_42875);
    _23109 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_23109);
    DeRefDS(_local_t_42875);
    DeRef(_st_42876);
    DeRef(_tmin_42877);
    DeRef(_tmax_42878);
    return _23109;
    ;
}


int _56TypeIs(int _x_42909, int _typei_42910)
{
    int _23111 = NOVALUE;
    int _23110 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_42909)) {
        _1 = (long)(DBL_PTR(_x_42909)->dbl);
        if (UNIQUE(DBL_PTR(_x_42909)) && (DBL_PTR(_x_42909)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_42909);
        _x_42909 = _1;
    }
    if (!IS_ATOM_INT(_typei_42910)) {
        _1 = (long)(DBL_PTR(_typei_42910)->dbl);
        if (UNIQUE(DBL_PTR(_typei_42910)) && (DBL_PTR(_typei_42910)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_typei_42910);
        _typei_42910 = _1;
    }

    /** 	return GType(x) = typei*/
    _23110 = _56GType(_x_42909);
    if (IS_ATOM_INT(_23110)) {
        _23111 = (_23110 == _typei_42910);
    }
    else {
        _23111 = binary_op(EQUALS, _23110, _typei_42910);
    }
    DeRef(_23110);
    _23110 = NOVALUE;
    return _23111;
    ;
}


int _56TypeIsIn(int _x_42915, int _types_42916)
{
    int _23113 = NOVALUE;
    int _23112 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_42915)) {
        _1 = (long)(DBL_PTR(_x_42915)->dbl);
        if (UNIQUE(DBL_PTR(_x_42915)) && (DBL_PTR(_x_42915)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_42915);
        _x_42915 = _1;
    }

    /** 	return find(GType(x), types)*/
    _23112 = _56GType(_x_42915);
    _23113 = find_from(_23112, _types_42916, 1);
    DeRef(_23112);
    _23112 = NOVALUE;
    DeRefDS(_types_42916);
    return _23113;
    ;
}


int _56TypeIsNot(int _x_42921, int _typei_42922)
{
    int _23115 = NOVALUE;
    int _23114 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_42921)) {
        _1 = (long)(DBL_PTR(_x_42921)->dbl);
        if (UNIQUE(DBL_PTR(_x_42921)) && (DBL_PTR(_x_42921)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_42921);
        _x_42921 = _1;
    }
    if (!IS_ATOM_INT(_typei_42922)) {
        _1 = (long)(DBL_PTR(_typei_42922)->dbl);
        if (UNIQUE(DBL_PTR(_typei_42922)) && (DBL_PTR(_typei_42922)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_typei_42922);
        _typei_42922 = _1;
    }

    /** 	return GType(x) != typei*/
    _23114 = _56GType(_x_42921);
    if (IS_ATOM_INT(_23114)) {
        _23115 = (_23114 != _typei_42922);
    }
    else {
        _23115 = binary_op(NOTEQ, _23114, _typei_42922);
    }
    DeRef(_23114);
    _23114 = NOVALUE;
    return _23115;
    ;
}


int _56TypeIsNotIn(int _x_42927, int _types_42928)
{
    int _23118 = NOVALUE;
    int _23117 = NOVALUE;
    int _23116 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_42927)) {
        _1 = (long)(DBL_PTR(_x_42927)->dbl);
        if (UNIQUE(DBL_PTR(_x_42927)) && (DBL_PTR(_x_42927)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_42927);
        _x_42927 = _1;
    }

    /** 	return not find(GType(x), types)*/
    _23116 = _56GType(_x_42927);
    _23117 = find_from(_23116, _types_42928, 1);
    DeRef(_23116);
    _23116 = NOVALUE;
    _23118 = (_23117 == 0);
    _23117 = NOVALUE;
    DeRefDS(_types_42928);
    return _23118;
    ;
}


int _56or_type(int _t1_42934, int _t2_42935)
{
    int _23138 = NOVALUE;
    int _23137 = NOVALUE;
    int _23136 = NOVALUE;
    int _23135 = NOVALUE;
    int _23130 = NOVALUE;
    int _23128 = NOVALUE;
    int _23123 = NOVALUE;
    int _23121 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_t1_42934)) {
        _1 = (long)(DBL_PTR(_t1_42934)->dbl);
        if (UNIQUE(DBL_PTR(_t1_42934)) && (DBL_PTR(_t1_42934)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_t1_42934);
        _t1_42934 = _1;
    }
    if (!IS_ATOM_INT(_t2_42935)) {
        _1 = (long)(DBL_PTR(_t2_42935)->dbl);
        if (UNIQUE(DBL_PTR(_t2_42935)) && (DBL_PTR(_t2_42935)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_t2_42935);
        _t2_42935 = _1;
    }

    /** 	if t1 = TYPE_NULL then*/
    if (_t1_42934 != 0)
    goto L1; // [9] 22

    /** 		return t2*/
    return _t2_42935;
    goto L2; // [19] 307
L1: 

    /** 	elsif t2 = TYPE_NULL then*/
    if (_t2_42935 != 0)
    goto L3; // [26] 39

    /** 		return t1*/
    return _t1_42934;
    goto L2; // [36] 307
L3: 

    /** 	elsif t1 = TYPE_OBJECT or t2 = TYPE_OBJECT then*/
    _23121 = (_t1_42934 == 16);
    if (_23121 != 0) {
        goto L4; // [47] 62
    }
    _23123 = (_t2_42935 == 16);
    if (_23123 == 0)
    {
        DeRef(_23123);
        _23123 = NOVALUE;
        goto L5; // [58] 73
    }
    else{
        DeRef(_23123);
        _23123 = NOVALUE;
    }
L4: 

    /** 		return TYPE_OBJECT*/
    DeRef(_23121);
    _23121 = NOVALUE;
    return 16;
    goto L2; // [70] 307
L5: 

    /** 	elsif t1 = TYPE_SEQUENCE then*/
    if (_t1_42934 != 8)
    goto L6; // [77] 112

    /** 		if t2 = TYPE_SEQUENCE then*/
    if (_t2_42935 != 8)
    goto L7; // [85] 100

    /** 			return TYPE_SEQUENCE*/
    DeRef(_23121);
    _23121 = NOVALUE;
    return 8;
    goto L2; // [97] 307
L7: 

    /** 			return TYPE_OBJECT*/
    DeRef(_23121);
    _23121 = NOVALUE;
    return 16;
    goto L2; // [109] 307
L6: 

    /** 	elsif t2 = TYPE_SEQUENCE then*/
    if (_t2_42935 != 8)
    goto L8; // [116] 151

    /** 		if t1 = TYPE_SEQUENCE then*/
    if (_t1_42934 != 8)
    goto L9; // [124] 139

    /** 			return TYPE_SEQUENCE*/
    DeRef(_23121);
    _23121 = NOVALUE;
    return 8;
    goto L2; // [136] 307
L9: 

    /** 			return TYPE_OBJECT*/
    DeRef(_23121);
    _23121 = NOVALUE;
    return 16;
    goto L2; // [148] 307
L8: 

    /** 	elsif t1 = TYPE_ATOM or t2 = TYPE_ATOM then*/
    _23128 = (_t1_42934 == 4);
    if (_23128 != 0) {
        goto LA; // [159] 174
    }
    _23130 = (_t2_42935 == 4);
    if (_23130 == 0)
    {
        DeRef(_23130);
        _23130 = NOVALUE;
        goto LB; // [170] 185
    }
    else{
        DeRef(_23130);
        _23130 = NOVALUE;
    }
LA: 

    /** 		return TYPE_ATOM*/
    DeRef(_23121);
    _23121 = NOVALUE;
    DeRef(_23128);
    _23128 = NOVALUE;
    return 4;
    goto L2; // [182] 307
LB: 

    /** 	elsif t1 = TYPE_DOUBLE then*/
    if (_t1_42934 != 2)
    goto LC; // [189] 224

    /** 		if t2 = TYPE_INTEGER then*/
    if (_t2_42935 != 1)
    goto LD; // [197] 212

    /** 			return TYPE_ATOM*/
    DeRef(_23121);
    _23121 = NOVALUE;
    DeRef(_23128);
    _23128 = NOVALUE;
    return 4;
    goto L2; // [209] 307
LD: 

    /** 			return TYPE_DOUBLE*/
    DeRef(_23121);
    _23121 = NOVALUE;
    DeRef(_23128);
    _23128 = NOVALUE;
    return 2;
    goto L2; // [221] 307
LC: 

    /** 	elsif t2 = TYPE_DOUBLE then*/
    if (_t2_42935 != 2)
    goto LE; // [228] 263

    /** 		if t1 = TYPE_INTEGER then*/
    if (_t1_42934 != 1)
    goto LF; // [236] 251

    /** 			return TYPE_ATOM*/
    DeRef(_23121);
    _23121 = NOVALUE;
    DeRef(_23128);
    _23128 = NOVALUE;
    return 4;
    goto L2; // [248] 307
LF: 

    /** 			return TYPE_DOUBLE*/
    DeRef(_23121);
    _23121 = NOVALUE;
    DeRef(_23128);
    _23128 = NOVALUE;
    return 2;
    goto L2; // [260] 307
LE: 

    /** 	elsif t1 = TYPE_INTEGER and t2 = TYPE_INTEGER then*/
    _23135 = (_t1_42934 == 1);
    if (_23135 == 0) {
        goto L10; // [271] 296
    }
    _23137 = (_t2_42935 == 1);
    if (_23137 == 0)
    {
        DeRef(_23137);
        _23137 = NOVALUE;
        goto L10; // [282] 296
    }
    else{
        DeRef(_23137);
        _23137 = NOVALUE;
    }

    /** 		return TYPE_INTEGER*/
    DeRef(_23121);
    _23121 = NOVALUE;
    DeRef(_23128);
    _23128 = NOVALUE;
    DeRef(_23135);
    _23135 = NOVALUE;
    return 1;
    goto L2; // [293] 307
L10: 

    /** 		InternalErr(258, {t1, t2})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _t1_42934;
    ((int *)_2)[2] = _t2_42935;
    _23138 = MAKE_SEQ(_1);
    _43InternalErr(258, _23138);
    _23138 = NOVALUE;
L2: 
    ;
}


void _56RemoveFromBB(int _s_43005)
{
    int _int_43006 = NOVALUE;
    int _23140 = NOVALUE;
    int _23139 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_s_43005)) {
        _1 = (long)(DBL_PTR(_s_43005)->dbl);
        if (UNIQUE(DBL_PTR(_s_43005)) && (DBL_PTR(_s_43005)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_43005);
        _s_43005 = _1;
    }

    /** 	for i = 1 to length(BB_info) do*/
    if (IS_SEQUENCE(_56BB_info_42650)){
            _23139 = SEQ_PTR(_56BB_info_42650)->length;
    }
    else {
        _23139 = 1;
    }
    {
        int _i_43008;
        _i_43008 = 1;
L1: 
        if (_i_43008 > _23139){
            goto L2; // [10] 61
        }

        /** 		int = BB_info[i][BB_VAR]*/
        _2 = (int)SEQ_PTR(_56BB_info_42650);
        _23140 = (int)*(((s1_ptr)_2)->base + _i_43008);
        _2 = (int)SEQ_PTR(_23140);
        _int_43006 = (int)*(((s1_ptr)_2)->base + 1);
        if (!IS_ATOM_INT(_int_43006)){
            _int_43006 = (long)DBL_PTR(_int_43006)->dbl;
        }
        _23140 = NOVALUE;

        /** 		if int = s then*/
        if (_int_43006 != _s_43005)
        goto L3; // [35] 54

        /** 			BB_info = remove( BB_info, int )*/
        {
            s1_ptr assign_space = SEQ_PTR(_56BB_info_42650);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_int_43006)) ? _int_43006 : (long)(DBL_PTR(_int_43006)->dbl);
            int stop = (IS_ATOM_INT(_int_43006)) ? _int_43006 : (long)(DBL_PTR(_int_43006)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_56BB_info_42650), start, &_56BB_info_42650 );
                }
                else Tail(SEQ_PTR(_56BB_info_42650), stop+1, &_56BB_info_42650);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_56BB_info_42650), start, &_56BB_info_42650);
            }
            else {
                assign_slice_seq = &assign_space;
                _56BB_info_42650 = Remove_elements(start, stop, (SEQ_PTR(_56BB_info_42650)->ref == 1));
            }
        }

        /** 			return*/
        return;
L3: 

        /** 	end for*/
        _i_43008 = _i_43008 + 1;
        goto L1; // [56] 17
L2: 
        ;
    }

    /** end procedure*/
    return;
    ;
}


void _56SetBBType(int _s_43026, int _t_43027, int _val_43028, int _etype_43029, int _has_delete_43030)
{
    int _found_43031 = NOVALUE;
    int _i_43032 = NOVALUE;
    int _tn_43033 = NOVALUE;
    int _int_43034 = NOVALUE;
    int _sym_43035 = NOVALUE;
    int _mode_43040 = NOVALUE;
    int _gtype_43055 = NOVALUE;
    int _new_type_43092 = NOVALUE;
    int _bbsym_43115 = NOVALUE;
    int _bbi_43251 = NOVALUE;
    int _23259 = NOVALUE;
    int _23258 = NOVALUE;
    int _23257 = NOVALUE;
    int _23255 = NOVALUE;
    int _23254 = NOVALUE;
    int _23253 = NOVALUE;
    int _23251 = NOVALUE;
    int _23250 = NOVALUE;
    int _23248 = NOVALUE;
    int _23246 = NOVALUE;
    int _23245 = NOVALUE;
    int _23243 = NOVALUE;
    int _23241 = NOVALUE;
    int _23240 = NOVALUE;
    int _23239 = NOVALUE;
    int _23238 = NOVALUE;
    int _23237 = NOVALUE;
    int _23236 = NOVALUE;
    int _23235 = NOVALUE;
    int _23234 = NOVALUE;
    int _23233 = NOVALUE;
    int _23230 = NOVALUE;
    int _23226 = NOVALUE;
    int _23221 = NOVALUE;
    int _23219 = NOVALUE;
    int _23218 = NOVALUE;
    int _23217 = NOVALUE;
    int _23215 = NOVALUE;
    int _23213 = NOVALUE;
    int _23212 = NOVALUE;
    int _23211 = NOVALUE;
    int _23209 = NOVALUE;
    int _23208 = NOVALUE;
    int _23206 = NOVALUE;
    int _23205 = NOVALUE;
    int _23204 = NOVALUE;
    int _23202 = NOVALUE;
    int _23201 = NOVALUE;
    int _23198 = NOVALUE;
    int _23197 = NOVALUE;
    int _23196 = NOVALUE;
    int _23194 = NOVALUE;
    int _23192 = NOVALUE;
    int _23191 = NOVALUE;
    int _23189 = NOVALUE;
    int _23188 = NOVALUE;
    int _23187 = NOVALUE;
    int _23185 = NOVALUE;
    int _23184 = NOVALUE;
    int _23173 = NOVALUE;
    int _23171 = NOVALUE;
    int _23168 = NOVALUE;
    int _23167 = NOVALUE;
    int _23164 = NOVALUE;
    int _23163 = NOVALUE;
    int _23162 = NOVALUE;
    int _23160 = NOVALUE;
    int _23159 = NOVALUE;
    int _23158 = NOVALUE;
    int _23156 = NOVALUE;
    int _23155 = NOVALUE;
    int _23153 = NOVALUE;
    int _23150 = NOVALUE;
    int _23148 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_s_43026)) {
        _1 = (long)(DBL_PTR(_s_43026)->dbl);
        if (UNIQUE(DBL_PTR(_s_43026)) && (DBL_PTR(_s_43026)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_43026);
        _s_43026 = _1;
    }
    if (!IS_ATOM_INT(_t_43027)) {
        _1 = (long)(DBL_PTR(_t_43027)->dbl);
        if (UNIQUE(DBL_PTR(_t_43027)) && (DBL_PTR(_t_43027)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_t_43027);
        _t_43027 = _1;
    }
    if (!IS_ATOM_INT(_etype_43029)) {
        _1 = (long)(DBL_PTR(_etype_43029)->dbl);
        if (UNIQUE(DBL_PTR(_etype_43029)) && (DBL_PTR(_etype_43029)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_etype_43029);
        _etype_43029 = _1;
    }
    if (!IS_ATOM_INT(_has_delete_43030)) {
        _1 = (long)(DBL_PTR(_has_delete_43030)->dbl);
        if (UNIQUE(DBL_PTR(_has_delete_43030)) && (DBL_PTR(_has_delete_43030)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_has_delete_43030);
        _has_delete_43030 = _1;
    }

    /** 	if has_delete then*/
    if (_has_delete_43030 == 0)
    {
        goto L1; // [13] 27
    }
    else{
    }

    /** 		p_has_delete = 1*/
    _56p_has_delete_42845 = 1;

    /** 		g_has_delete = 1*/
    _56g_has_delete_42844 = 1;
L1: 

    /** 	sym = SymTab[s]*/
    DeRef(_sym_43035);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _sym_43035 = (int)*(((s1_ptr)_2)->base + _s_43026);
    Ref(_sym_43035);

    /** 	SymTab[s] = 0*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _s_43026);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	integer mode = sym[S_MODE]*/
    _2 = (int)SEQ_PTR(_sym_43035);
    _mode_43040 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_mode_43040))
    _mode_43040 = (long)DBL_PTR(_mode_43040)->dbl;

    /** 	if mode = M_NORMAL or mode = M_TEMP  then*/
    _23148 = (_mode_43040 == 1);
    if (_23148 != 0) {
        goto L2; // [61] 76
    }
    _23150 = (_mode_43040 == 3);
    if (_23150 == 0)
    {
        DeRef(_23150);
        _23150 = NOVALUE;
        goto L3; // [72] 1197
    }
    else{
        DeRef(_23150);
        _23150 = NOVALUE;
    }
L2: 

    /** 		found = FALSE*/
    _found_43031 = _5FALSE_242;

    /** 		if mode = M_TEMP then*/
    if (_mode_43040 != 3)
    goto L4; // [89] 467

    /** 			sym[S_GTYPE] = t*/
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = _t_43027;
    DeRef(_1);

    /** 			sym[S_SEQ_ELEM] = etype*/
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = _etype_43029;
    DeRef(_1);

    /** 			integer gtype = sym[S_GTYPE]*/
    _2 = (int)SEQ_PTR(_sym_43035);
    _gtype_43055 = (int)*(((s1_ptr)_2)->base + 36);
    if (!IS_ATOM_INT(_gtype_43055))
    _gtype_43055 = (long)DBL_PTR(_gtype_43055)->dbl;

    /** 			if gtype = TYPE_OBJECT*/
    _23153 = (_gtype_43055 == 16);
    if (_23153 != 0) {
        goto L5; // [125] 140
    }
    _23155 = (_gtype_43055 == 8);
    if (_23155 == 0)
    {
        DeRef(_23155);
        _23155 = NOVALUE;
        goto L6; // [136] 213
    }
    else{
        DeRef(_23155);
        _23155 = NOVALUE;
    }
L5: 

    /** 				if val[MIN] < 0 then*/
    _2 = (int)SEQ_PTR(_val_43028);
    _23156 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(GREATEREQ, _23156, 0)){
        _23156 = NOVALUE;
        goto L7; // [148] 165
    }
    _23156 = NOVALUE;

    /** 					sym[S_SEQ_LEN] = NOVALUE*/
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);
    goto L8; // [162] 180
L7: 

    /** 					sym[S_SEQ_LEN] = val[MIN]*/
    _2 = (int)SEQ_PTR(_val_43028);
    _23158 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_23158);
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _23158;
    if( _1 != _23158 ){
        DeRef(_1);
    }
    _23158 = NOVALUE;
L8: 

    /** 				sym[S_OBJ] = NOVALUE*/
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);

    /** 				sym[S_OBJ_MIN] = NOVALUE*/
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 30);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);

    /** 				sym[S_OBJ_MAX] = NOVALUE*/
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 31);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);
    goto L9; // [210] 252
L6: 

    /** 				sym[S_OBJ_MIN] = val[MIN]*/
    _2 = (int)SEQ_PTR(_val_43028);
    _23159 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_23159);
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 30);
    _1 = *(int *)_2;
    *(int *)_2 = _23159;
    if( _1 != _23159 ){
        DeRef(_1);
    }
    _23159 = NOVALUE;

    /** 				sym[S_OBJ_MAX] = val[MAX]*/
    _2 = (int)SEQ_PTR(_val_43028);
    _23160 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_23160);
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 31);
    _1 = *(int *)_2;
    *(int *)_2 = _23160;
    if( _1 != _23160 ){
        DeRef(_1);
    }
    _23160 = NOVALUE;

    /** 				sym[S_SEQ_LEN] = NOVALUE*/
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);
L9: 

    /** 			if not Initializing then*/
    if (_25Initializing_12346 != 0)
    goto LA; // [256] 326

    /** 				integer new_type = or_type(temp_name_type[sym[S_TEMP_NAME]][T_GTYPE_NEW], t)*/
    _2 = (int)SEQ_PTR(_sym_43035);
    _23162 = (int)*(((s1_ptr)_2)->base + 34);
    _2 = (int)SEQ_PTR(_25temp_name_type_12348);
    if (!IS_ATOM_INT(_23162)){
        _23163 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_23162)->dbl));
    }
    else{
        _23163 = (int)*(((s1_ptr)_2)->base + _23162);
    }
    _2 = (int)SEQ_PTR(_23163);
    _23164 = (int)*(((s1_ptr)_2)->base + 2);
    _23163 = NOVALUE;
    Ref(_23164);
    _new_type_43092 = _56or_type(_23164, _t_43027);
    _23164 = NOVALUE;
    if (!IS_ATOM_INT(_new_type_43092)) {
        _1 = (long)(DBL_PTR(_new_type_43092)->dbl);
        if (UNIQUE(DBL_PTR(_new_type_43092)) && (DBL_PTR(_new_type_43092)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_new_type_43092);
        _new_type_43092 = _1;
    }

    /** 				if new_type = TYPE_NULL then*/
    if (_new_type_43092 != 0)
    goto LB; // [290] 304

    /** 					new_type = TYPE_OBJECT*/
    _new_type_43092 = 16;
LB: 

    /** 				temp_name_type[sym[S_TEMP_NAME]][T_GTYPE_NEW] = new_type*/
    _2 = (int)SEQ_PTR(_sym_43035);
    _23167 = (int)*(((s1_ptr)_2)->base + 34);
    _2 = (int)SEQ_PTR(_25temp_name_type_12348);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _25temp_name_type_12348 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_23167))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_23167)->dbl));
    else
    _3 = (int)(_23167 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _new_type_43092;
    DeRef(_1);
    _23168 = NOVALUE;
LA: 

    /** 			tn = sym[S_TEMP_NAME]*/
    _2 = (int)SEQ_PTR(_sym_43035);
    _tn_43033 = (int)*(((s1_ptr)_2)->base + 34);
    if (!IS_ATOM_INT(_tn_43033))
    _tn_43033 = (long)DBL_PTR(_tn_43033)->dbl;

    /** 			i = 1*/
    _i_43032 = 1;

    /** 			while i <= length(BB_info) do*/
LC: 
    if (IS_SEQUENCE(_56BB_info_42650)){
            _23171 = SEQ_PTR(_56BB_info_42650)->length;
    }
    else {
        _23171 = 1;
    }
    if (_i_43032 > _23171)
    goto LD; // [351] 462

    /** 				sequence bbsym*/

    /** 				int = BB_info[i][BB_VAR]*/
    _2 = (int)SEQ_PTR(_56BB_info_42650);
    _23173 = (int)*(((s1_ptr)_2)->base + _i_43032);
    _2 = (int)SEQ_PTR(_23173);
    _int_43034 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_int_43034)){
        _int_43034 = (long)DBL_PTR(_int_43034)->dbl;
    }
    _23173 = NOVALUE;

    /** 				if int = s then*/
    if (_int_43034 != _s_43026)
    goto LE; // [375] 389

    /** 					bbsym = sym*/
    RefDS(_sym_43035);
    DeRef(_bbsym_43115);
    _bbsym_43115 = _sym_43035;
    goto LF; // [386] 400
LE: 

    /** 					bbsym = SymTab[int]*/
    DeRef(_bbsym_43115);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _bbsym_43115 = (int)*(((s1_ptr)_2)->base + _int_43034);
    Ref(_bbsym_43115);
LF: 

    /** 				int = bbsym[S_MODE]*/
    _2 = (int)SEQ_PTR(_bbsym_43115);
    _int_43034 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_int_43034))
    _int_43034 = (long)DBL_PTR(_int_43034)->dbl;

    /** 				if int = M_TEMP then*/
    if (_int_43034 != 3)
    goto L10; // [414] 449

    /** 					int = bbsym[S_TEMP_NAME]*/
    _2 = (int)SEQ_PTR(_bbsym_43115);
    _int_43034 = (int)*(((s1_ptr)_2)->base + 34);
    if (!IS_ATOM_INT(_int_43034))
    _int_43034 = (long)DBL_PTR(_int_43034)->dbl;

    /** 					if int = tn then*/
    if (_int_43034 != _tn_43033)
    goto L11; // [428] 448

    /** 						found = TRUE*/
    _found_43031 = _5TRUE_244;

    /** 						exit*/
    DeRefDS(_bbsym_43115);
    _bbsym_43115 = NOVALUE;
    goto LD; // [445] 462
L11: 
L10: 

    /** 				i += 1*/
    _i_43032 = _i_43032 + 1;
    DeRef(_bbsym_43115);
    _bbsym_43115 = NOVALUE;

    /** 			end while*/
    goto LC; // [459] 346
LD: 
    goto L12; // [464] 893
L4: 

    /** 			if t != TYPE_NULL then*/
    if (_t_43027 == 0)
    goto L13; // [471] 826

    /** 				if not Initializing then*/
    if (_25Initializing_12346 != 0)
    goto L14; // [479] 502

    /** 					sym[S_GTYPE_NEW] = or_type(sym[S_GTYPE_NEW], t)*/
    _2 = (int)SEQ_PTR(_sym_43035);
    _23184 = (int)*(((s1_ptr)_2)->base + 38);
    Ref(_23184);
    _23185 = _56or_type(_23184, _t_43027);
    _23184 = NOVALUE;
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 38);
    _1 = *(int *)_2;
    *(int *)_2 = _23185;
    if( _1 != _23185 ){
        DeRef(_1);
    }
    _23185 = NOVALUE;
L14: 

    /** 				if t = TYPE_SEQUENCE then*/
    if (_t_43027 != 8)
    goto L15; // [506] 635

    /** 					sym[S_SEQ_ELEM_NEW] =*/
    _2 = (int)SEQ_PTR(_sym_43035);
    _23187 = (int)*(((s1_ptr)_2)->base + 40);
    Ref(_23187);
    _23188 = _56or_type(_23187, _etype_43029);
    _23187 = NOVALUE;
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 40);
    _1 = *(int *)_2;
    *(int *)_2 = _23188;
    if( _1 != _23188 ){
        DeRef(_1);
    }
    _23188 = NOVALUE;

    /** 					if val[MIN] != -1 then*/
    _2 = (int)SEQ_PTR(_val_43028);
    _23189 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _23189, -1)){
        _23189 = NOVALUE;
        goto L16; // [537] 825
    }
    _23189 = NOVALUE;

    /** 						if sym[S_SEQ_LEN_NEW] = -NOVALUE then*/
    _2 = (int)SEQ_PTR(_sym_43035);
    _23191 = (int)*(((s1_ptr)_2)->base + 39);
    if (IS_ATOM_INT(_25NOVALUE_12115)) {
        if ((unsigned long)_25NOVALUE_12115 == 0xC0000000)
        _23192 = (int)NewDouble((double)-0xC0000000);
        else
        _23192 = - _25NOVALUE_12115;
    }
    else {
        _23192 = unary_op(UMINUS, _25NOVALUE_12115);
    }
    if (binary_op_a(NOTEQ, _23191, _23192)){
        _23191 = NOVALUE;
        DeRef(_23192);
        _23192 = NOVALUE;
        goto L17; // [554] 601
    }
    _23191 = NOVALUE;
    DeRef(_23192);
    _23192 = NOVALUE;

    /** 							if val[MIN] < 0 then*/
    _2 = (int)SEQ_PTR(_val_43028);
    _23194 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(GREATEREQ, _23194, 0)){
        _23194 = NOVALUE;
        goto L18; // [566] 583
    }
    _23194 = NOVALUE;

    /** 								sym[S_SEQ_LEN_NEW] = NOVALUE*/
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 39);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);
    goto L16; // [580] 825
L18: 

    /** 								sym[S_SEQ_LEN_NEW] = val[MIN]*/
    _2 = (int)SEQ_PTR(_val_43028);
    _23196 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_23196);
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 39);
    _1 = *(int *)_2;
    *(int *)_2 = _23196;
    if( _1 != _23196 ){
        DeRef(_1);
    }
    _23196 = NOVALUE;
    goto L16; // [598] 825
L17: 

    /** 						elsif val[MIN] != sym[S_SEQ_LEN_NEW] then*/
    _2 = (int)SEQ_PTR(_val_43028);
    _23197 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_sym_43035);
    _23198 = (int)*(((s1_ptr)_2)->base + 39);
    if (binary_op_a(EQUALS, _23197, _23198)){
        _23197 = NOVALUE;
        _23198 = NOVALUE;
        goto L16; // [615] 825
    }
    _23197 = NOVALUE;
    _23198 = NOVALUE;

    /** 							sym[S_SEQ_LEN_NEW] = NOVALUE*/
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 39);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);
    goto L16; // [632] 825
L15: 

    /** 				elsif t = TYPE_INTEGER then*/
    if (_t_43027 != 1)
    goto L19; // [639] 776

    /** 					if sym[S_OBJ_MIN_NEW] = -NOVALUE then*/
    _2 = (int)SEQ_PTR(_sym_43035);
    _23201 = (int)*(((s1_ptr)_2)->base + 41);
    if (IS_ATOM_INT(_25NOVALUE_12115)) {
        if ((unsigned long)_25NOVALUE_12115 == 0xC0000000)
        _23202 = (int)NewDouble((double)-0xC0000000);
        else
        _23202 = - _25NOVALUE_12115;
    }
    else {
        _23202 = unary_op(UMINUS, _25NOVALUE_12115);
    }
    if (binary_op_a(NOTEQ, _23201, _23202)){
        _23201 = NOVALUE;
        DeRef(_23202);
        _23202 = NOVALUE;
        goto L1A; // [656] 691
    }
    _23201 = NOVALUE;
    DeRef(_23202);
    _23202 = NOVALUE;

    /** 						sym[S_OBJ_MIN_NEW] = val[MIN]*/
    _2 = (int)SEQ_PTR(_val_43028);
    _23204 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_23204);
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 41);
    _1 = *(int *)_2;
    *(int *)_2 = _23204;
    if( _1 != _23204 ){
        DeRef(_1);
    }
    _23204 = NOVALUE;

    /** 						sym[S_OBJ_MAX_NEW] = val[MAX]*/
    _2 = (int)SEQ_PTR(_val_43028);
    _23205 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_23205);
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 42);
    _1 = *(int *)_2;
    *(int *)_2 = _23205;
    if( _1 != _23205 ){
        DeRef(_1);
    }
    _23205 = NOVALUE;
    goto L16; // [688] 825
L1A: 

    /** 					elsif sym[S_OBJ_MIN_NEW] != NOVALUE then*/
    _2 = (int)SEQ_PTR(_sym_43035);
    _23206 = (int)*(((s1_ptr)_2)->base + 41);
    if (binary_op_a(EQUALS, _23206, _25NOVALUE_12115)){
        _23206 = NOVALUE;
        goto L16; // [701] 825
    }
    _23206 = NOVALUE;

    /** 						if val[MIN] < sym[S_OBJ_MIN_NEW] then*/
    _2 = (int)SEQ_PTR(_val_43028);
    _23208 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_sym_43035);
    _23209 = (int)*(((s1_ptr)_2)->base + 41);
    if (binary_op_a(GREATEREQ, _23208, _23209)){
        _23208 = NOVALUE;
        _23209 = NOVALUE;
        goto L1B; // [719] 738
    }
    _23208 = NOVALUE;
    _23209 = NOVALUE;

    /** 							sym[S_OBJ_MIN_NEW] = val[MIN]*/
    _2 = (int)SEQ_PTR(_val_43028);
    _23211 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_23211);
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 41);
    _1 = *(int *)_2;
    *(int *)_2 = _23211;
    if( _1 != _23211 ){
        DeRef(_1);
    }
    _23211 = NOVALUE;
L1B: 

    /** 						if val[MAX] > sym[S_OBJ_MAX_NEW] then*/
    _2 = (int)SEQ_PTR(_val_43028);
    _23212 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_sym_43035);
    _23213 = (int)*(((s1_ptr)_2)->base + 42);
    if (binary_op_a(LESSEQ, _23212, _23213)){
        _23212 = NOVALUE;
        _23213 = NOVALUE;
        goto L16; // [752] 825
    }
    _23212 = NOVALUE;
    _23213 = NOVALUE;

    /** 							sym[S_OBJ_MAX_NEW] = val[MAX]*/
    _2 = (int)SEQ_PTR(_val_43028);
    _23215 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_23215);
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 42);
    _1 = *(int *)_2;
    *(int *)_2 = _23215;
    if( _1 != _23215 ){
        DeRef(_1);
    }
    _23215 = NOVALUE;
    goto L16; // [773] 825
L19: 

    /** 					sym[S_OBJ_MIN_NEW] = NOVALUE*/
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 41);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);

    /** 					if t = TYPE_OBJECT then*/
    if (_t_43027 != 16)
    goto L1C; // [790] 824

    /** 						sym[S_SEQ_ELEM_NEW] =*/
    _2 = (int)SEQ_PTR(_sym_43035);
    _23217 = (int)*(((s1_ptr)_2)->base + 40);
    Ref(_23217);
    _23218 = _56or_type(_23217, _etype_43029);
    _23217 = NOVALUE;
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 40);
    _1 = *(int *)_2;
    *(int *)_2 = _23218;
    if( _1 != _23218 ){
        DeRef(_1);
    }
    _23218 = NOVALUE;

    /** 						sym[S_SEQ_LEN_NEW] = NOVALUE*/
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 39);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);
L1C: 
L16: 
L13: 

    /** 			i = 1*/
    _i_43032 = 1;

    /** 			while i <= length(BB_info) do*/
L1D: 
    if (IS_SEQUENCE(_56BB_info_42650)){
            _23219 = SEQ_PTR(_56BB_info_42650)->length;
    }
    else {
        _23219 = 1;
    }
    if (_i_43032 > _23219)
    goto L1E; // [841] 892

    /** 				int = BB_info[i][BB_VAR]*/
    _2 = (int)SEQ_PTR(_56BB_info_42650);
    _23221 = (int)*(((s1_ptr)_2)->base + _i_43032);
    _2 = (int)SEQ_PTR(_23221);
    _int_43034 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_int_43034)){
        _int_43034 = (long)DBL_PTR(_int_43034)->dbl;
    }
    _23221 = NOVALUE;

    /** 				if int = s then*/
    if (_int_43034 != _s_43026)
    goto L1F; // [863] 881

    /** 					found = TRUE*/
    _found_43031 = _5TRUE_244;

    /** 					exit*/
    goto L1E; // [878] 892
L1F: 

    /** 				i += 1*/
    _i_43032 = _i_43032 + 1;

    /** 			end while*/
    goto L1D; // [889] 836
L1E: 
L12: 

    /** 		if not found then*/
    if (_found_43031 != 0)
    goto L20; // [895] 911

    /** 			BB_info = append(BB_info, repeat(0, 6))*/
    _23226 = Repeat(0, 6);
    RefDS(_23226);
    Append(&_56BB_info_42650, _56BB_info_42650, _23226);
    DeRefDS(_23226);
    _23226 = NOVALUE;
L20: 

    /** 		if t = TYPE_NULL then*/
    if (_t_43027 != 0)
    goto L21; // [915] 955

    /** 			if not found then*/
    if (_found_43031 != 0)
    goto L22; // [921] 1338

    /** 				BB_info[i] = dummy_bb*/
    RefDS(_56dummy_bb_43015);
    _2 = (int)SEQ_PTR(_56BB_info_42650);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _56BB_info_42650 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _i_43032);
    _1 = *(int *)_2;
    *(int *)_2 = _56dummy_bb_43015;
    DeRef(_1);

    /** 				BB_info[i][BB_VAR] = s*/
    _2 = (int)SEQ_PTR(_56BB_info_42650);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _56BB_info_42650 = MAKE_SEQ(_2);
    }
    _3 = (int)(_i_43032 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _s_43026;
    DeRef(_1);
    _23230 = NOVALUE;
    goto L22; // [952] 1338
L21: 

    /** 			sequence bbi = BB_info[i]*/
    DeRef(_bbi_43251);
    _2 = (int)SEQ_PTR(_56BB_info_42650);
    _bbi_43251 = (int)*(((s1_ptr)_2)->base + _i_43032);
    Ref(_bbi_43251);

    /** 			BB_info[i] = 0*/
    _2 = (int)SEQ_PTR(_56BB_info_42650);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _56BB_info_42650 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _i_43032);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 			bbi[BB_VAR] = s*/
    _2 = (int)SEQ_PTR(_bbi_43251);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bbi_43251 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _s_43026;
    DeRef(_1);

    /** 			bbi[BB_TYPE] = t*/
    _2 = (int)SEQ_PTR(_bbi_43251);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bbi_43251 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _t_43027;
    DeRef(_1);

    /** 			bbi[BB_DELETE] = has_delete*/
    _2 = (int)SEQ_PTR(_bbi_43251);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bbi_43251 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _has_delete_43030;
    DeRef(_1);

    /** 			if t = TYPE_SEQUENCE and val[MIN] = -1 then*/
    _23233 = (_t_43027 == 8);
    if (_23233 == 0) {
        goto L23; // [1007] 1099
    }
    _2 = (int)SEQ_PTR(_val_43028);
    _23235 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_23235)) {
        _23236 = (_23235 == -1);
    }
    else {
        _23236 = binary_op(EQUALS, _23235, -1);
    }
    _23235 = NOVALUE;
    if (_23236 == 0) {
        DeRef(_23236);
        _23236 = NOVALUE;
        goto L23; // [1022] 1099
    }
    else {
        if (!IS_ATOM_INT(_23236) && DBL_PTR(_23236)->dbl == 0.0){
            DeRef(_23236);
            _23236 = NOVALUE;
            goto L23; // [1022] 1099
        }
        DeRef(_23236);
        _23236 = NOVALUE;
    }
    DeRef(_23236);
    _23236 = NOVALUE;

    /** 				if found and bbi[BB_ELEM] != TYPE_NULL then*/
    if (_found_43031 == 0) {
        goto L24; // [1027] 1069
    }
    _2 = (int)SEQ_PTR(_bbi_43251);
    _23238 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_23238)) {
        _23239 = (_23238 != 0);
    }
    else {
        _23239 = binary_op(NOTEQ, _23238, 0);
    }
    _23238 = NOVALUE;
    if (_23239 == 0) {
        DeRef(_23239);
        _23239 = NOVALUE;
        goto L24; // [1044] 1069
    }
    else {
        if (!IS_ATOM_INT(_23239) && DBL_PTR(_23239)->dbl == 0.0){
            DeRef(_23239);
            _23239 = NOVALUE;
            goto L24; // [1044] 1069
        }
        DeRef(_23239);
        _23239 = NOVALUE;
    }
    DeRef(_23239);
    _23239 = NOVALUE;

    /** 					bbi[BB_ELEM] = or_type(bbi[BB_ELEM], etype)*/
    _2 = (int)SEQ_PTR(_bbi_43251);
    _23240 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_23240);
    _23241 = _56or_type(_23240, _etype_43029);
    _23240 = NOVALUE;
    _2 = (int)SEQ_PTR(_bbi_43251);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bbi_43251 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _23241;
    if( _1 != _23241 ){
        DeRef(_1);
    }
    _23241 = NOVALUE;
    goto L25; // [1066] 1080
L24: 

    /** 					bbi[BB_ELEM] = TYPE_NULL*/
    _2 = (int)SEQ_PTR(_bbi_43251);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bbi_43251 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
L25: 

    /** 				if not found then*/
    if (_found_43031 != 0)
    goto L26; // [1082] 1183

    /** 					bbi[BB_SEQLEN] = NOVALUE*/
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(_bbi_43251);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bbi_43251 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);
    goto L26; // [1096] 1183
L23: 

    /** 				bbi[BB_ELEM] = etype*/
    _2 = (int)SEQ_PTR(_bbi_43251);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bbi_43251 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _etype_43029;
    DeRef(_1);

    /** 				if t = TYPE_SEQUENCE or t = TYPE_OBJECT then*/
    _23243 = (_t_43027 == 8);
    if (_23243 != 0) {
        goto L27; // [1115] 1130
    }
    _23245 = (_t_43027 == 16);
    if (_23245 == 0)
    {
        DeRef(_23245);
        _23245 = NOVALUE;
        goto L28; // [1126] 1173
    }
    else{
        DeRef(_23245);
        _23245 = NOVALUE;
    }
L27: 

    /** 					if val[MIN] < 0 then*/
    _2 = (int)SEQ_PTR(_val_43028);
    _23246 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(GREATEREQ, _23246, 0)){
        _23246 = NOVALUE;
        goto L29; // [1138] 1155
    }
    _23246 = NOVALUE;

    /** 						bbi[BB_SEQLEN] = NOVALUE*/
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(_bbi_43251);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bbi_43251 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);
    goto L2A; // [1152] 1182
L29: 

    /** 						bbi[BB_SEQLEN] = val[MIN]*/
    _2 = (int)SEQ_PTR(_val_43028);
    _23248 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_23248);
    _2 = (int)SEQ_PTR(_bbi_43251);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bbi_43251 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _23248;
    if( _1 != _23248 ){
        DeRef(_1);
    }
    _23248 = NOVALUE;
    goto L2A; // [1170] 1182
L28: 

    /** 					bbi[BB_OBJ] = val*/
    RefDS(_val_43028);
    _2 = (int)SEQ_PTR(_bbi_43251);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bbi_43251 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _val_43028;
    DeRef(_1);
L2A: 
L26: 

    /** 			BB_info[i] = bbi*/
    RefDS(_bbi_43251);
    _2 = (int)SEQ_PTR(_56BB_info_42650);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _56BB_info_42650 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _i_43032);
    _1 = *(int *)_2;
    *(int *)_2 = _bbi_43251;
    DeRef(_1);
    DeRefDS(_bbi_43251);
    _bbi_43251 = NOVALUE;
    goto L22; // [1194] 1338
L3: 

    /** 	elsif mode = M_CONSTANT then*/
    if (_mode_43040 != 2)
    goto L2B; // [1201] 1337

    /** 		sym[S_GTYPE] = t*/
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = _t_43027;
    DeRef(_1);

    /** 		sym[S_SEQ_ELEM] = etype*/
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = _etype_43029;
    DeRef(_1);

    /** 		if sym[S_GTYPE] = TYPE_SEQUENCE or*/
    _2 = (int)SEQ_PTR(_sym_43035);
    _23250 = (int)*(((s1_ptr)_2)->base + 36);
    if (IS_ATOM_INT(_23250)) {
        _23251 = (_23250 == 8);
    }
    else {
        _23251 = binary_op(EQUALS, _23250, 8);
    }
    _23250 = NOVALUE;
    if (IS_ATOM_INT(_23251)) {
        if (_23251 != 0) {
            goto L2C; // [1235] 1256
        }
    }
    else {
        if (DBL_PTR(_23251)->dbl != 0.0) {
            goto L2C; // [1235] 1256
        }
    }
    _2 = (int)SEQ_PTR(_sym_43035);
    _23253 = (int)*(((s1_ptr)_2)->base + 36);
    if (IS_ATOM_INT(_23253)) {
        _23254 = (_23253 == 16);
    }
    else {
        _23254 = binary_op(EQUALS, _23253, 16);
    }
    _23253 = NOVALUE;
    if (_23254 == 0) {
        DeRef(_23254);
        _23254 = NOVALUE;
        goto L2D; // [1252] 1299
    }
    else {
        if (!IS_ATOM_INT(_23254) && DBL_PTR(_23254)->dbl == 0.0){
            DeRef(_23254);
            _23254 = NOVALUE;
            goto L2D; // [1252] 1299
        }
        DeRef(_23254);
        _23254 = NOVALUE;
    }
    DeRef(_23254);
    _23254 = NOVALUE;
L2C: 

    /** 			if val[MIN] < 0 then*/
    _2 = (int)SEQ_PTR(_val_43028);
    _23255 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(GREATEREQ, _23255, 0)){
        _23255 = NOVALUE;
        goto L2E; // [1264] 1281
    }
    _23255 = NOVALUE;

    /** 				sym[S_SEQ_LEN] = NOVALUE*/
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);
    goto L2F; // [1278] 1328
L2E: 

    /** 				sym[S_SEQ_LEN] = val[MIN]*/
    _2 = (int)SEQ_PTR(_val_43028);
    _23257 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_23257);
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _23257;
    if( _1 != _23257 ){
        DeRef(_1);
    }
    _23257 = NOVALUE;
    goto L2F; // [1296] 1328
L2D: 

    /** 			sym[S_OBJ_MIN] = val[MIN]*/
    _2 = (int)SEQ_PTR(_val_43028);
    _23258 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_23258);
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 30);
    _1 = *(int *)_2;
    *(int *)_2 = _23258;
    if( _1 != _23258 ){
        DeRef(_1);
    }
    _23258 = NOVALUE;

    /** 			sym[S_OBJ_MAX] = val[MAX]*/
    _2 = (int)SEQ_PTR(_val_43028);
    _23259 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_23259);
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 31);
    _1 = *(int *)_2;
    *(int *)_2 = _23259;
    if( _1 != _23259 ){
        DeRef(_1);
    }
    _23259 = NOVALUE;
L2F: 

    /** 		sym[S_HAS_DELETE] = has_delete*/
    _2 = (int)SEQ_PTR(_sym_43035);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43035 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 54);
    _1 = *(int *)_2;
    *(int *)_2 = _has_delete_43030;
    DeRef(_1);
L2B: 
L22: 

    /** 	SymTab[s] = sym*/
    RefDS(_sym_43035);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _s_43026);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_43035;
    DeRef(_1);

    /** end procedure*/
    DeRefDS(_val_43028);
    DeRefDS(_sym_43035);
    DeRef(_23148);
    _23148 = NOVALUE;
    DeRef(_23153);
    _23153 = NOVALUE;
    _23162 = NOVALUE;
    _23167 = NOVALUE;
    DeRef(_23233);
    _23233 = NOVALUE;
    DeRef(_23243);
    _23243 = NOVALUE;
    DeRef(_23251);
    _23251 = NOVALUE;
    return;
    ;
}


void _56CName(int _s_43325)
{
    int _v_43326 = NOVALUE;
    int _mode_43328 = NOVALUE;
    int _23320 = NOVALUE;
    int _23319 = NOVALUE;
    int _23318 = NOVALUE;
    int _23317 = NOVALUE;
    int _23316 = NOVALUE;
    int _23315 = NOVALUE;
    int _23314 = NOVALUE;
    int _23313 = NOVALUE;
    int _23312 = NOVALUE;
    int _23311 = NOVALUE;
    int _23309 = NOVALUE;
    int _23307 = NOVALUE;
    int _23306 = NOVALUE;
    int _23305 = NOVALUE;
    int _23304 = NOVALUE;
    int _23303 = NOVALUE;
    int _23302 = NOVALUE;
    int _23301 = NOVALUE;
    int _23300 = NOVALUE;
    int _23299 = NOVALUE;
    int _23298 = NOVALUE;
    int _23297 = NOVALUE;
    int _23295 = NOVALUE;
    int _23294 = NOVALUE;
    int _23293 = NOVALUE;
    int _23292 = NOVALUE;
    int _23291 = NOVALUE;
    int _23290 = NOVALUE;
    int _23288 = NOVALUE;
    int _23287 = NOVALUE;
    int _23285 = NOVALUE;
    int _23284 = NOVALUE;
    int _23283 = NOVALUE;
    int _23282 = NOVALUE;
    int _23281 = NOVALUE;
    int _23280 = NOVALUE;
    int _23279 = NOVALUE;
    int _23278 = NOVALUE;
    int _23277 = NOVALUE;
    int _23276 = NOVALUE;
    int _23275 = NOVALUE;
    int _23274 = NOVALUE;
    int _23272 = NOVALUE;
    int _23271 = NOVALUE;
    int _23269 = NOVALUE;
    int _23268 = NOVALUE;
    int _23267 = NOVALUE;
    int _23266 = NOVALUE;
    int _23265 = NOVALUE;
    int _23264 = NOVALUE;
    int _23261 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_s_43325)) {
        _1 = (long)(DBL_PTR(_s_43325)->dbl);
        if (UNIQUE(DBL_PTR(_s_43325)) && (DBL_PTR(_s_43325)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_43325);
        _s_43325 = _1;
    }

    /** 	v = ObjValue(s)*/
    _0 = _v_43326;
    _v_43326 = _56ObjValue(_s_43325);
    DeRef(_0);

    /** 	integer mode = SymTab[s][S_MODE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23261 = (int)*(((s1_ptr)_2)->base + _s_43325);
    _2 = (int)SEQ_PTR(_23261);
    _mode_43328 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_mode_43328)){
        _mode_43328 = (long)DBL_PTR(_mode_43328)->dbl;
    }
    _23261 = NOVALUE;

    /**  	if mode = M_NORMAL then*/
    if (_mode_43328 != 1)
    goto L1; // [29] 240

    /** 		if LeftSym = FALSE and GType(s) = TYPE_INTEGER and v != NOVALUE then*/
    _23264 = (_56LeftSym_42651 == _5FALSE_242);
    if (_23264 == 0) {
        _23265 = 0;
        goto L2; // [43] 61
    }
    _23266 = _56GType(_s_43325);
    if (IS_ATOM_INT(_23266)) {
        _23267 = (_23266 == 1);
    }
    else {
        _23267 = binary_op(EQUALS, _23266, 1);
    }
    DeRef(_23266);
    _23266 = NOVALUE;
    if (IS_ATOM_INT(_23267))
    _23265 = (_23267 != 0);
    else
    _23265 = DBL_PTR(_23267)->dbl != 0.0;
L2: 
    if (_23265 == 0) {
        goto L3; // [61] 84
    }
    if (IS_ATOM_INT(_v_43326) && IS_ATOM_INT(_25NOVALUE_12115)) {
        _23269 = (_v_43326 != _25NOVALUE_12115);
    }
    else {
        _23269 = binary_op(NOTEQ, _v_43326, _25NOVALUE_12115);
    }
    if (_23269 == 0) {
        DeRef(_23269);
        _23269 = NOVALUE;
        goto L3; // [72] 84
    }
    else {
        if (!IS_ATOM_INT(_23269) && DBL_PTR(_23269)->dbl == 0.0){
            DeRef(_23269);
            _23269 = NOVALUE;
            goto L3; // [72] 84
        }
        DeRef(_23269);
        _23269 = NOVALUE;
    }
    DeRef(_23269);
    _23269 = NOVALUE;

    /** 			c_printf("%d", v)*/
    RefDS(_23270);
    Ref(_v_43326);
    _53c_printf(_23270, _v_43326);
    goto L4; // [81] 166
L3: 

    /** 			if SymTab[s][S_SCOPE] > SC_PRIVATE then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23271 = (int)*(((s1_ptr)_2)->base + _s_43325);
    _2 = (int)SEQ_PTR(_23271);
    _23272 = (int)*(((s1_ptr)_2)->base + 4);
    _23271 = NOVALUE;
    if (binary_op_a(LESSEQ, _23272, 3)){
        _23272 = NOVALUE;
        goto L5; // [100] 142
    }
    _23272 = NOVALUE;

    /** 				c_printf("_%d", SymTab[s][S_FILE_NO])*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23274 = (int)*(((s1_ptr)_2)->base + _s_43325);
    _2 = (int)SEQ_PTR(_23274);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _23275 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _23275 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _23274 = NOVALUE;
    RefDS(_22775);
    Ref(_23275);
    _53c_printf(_22775, _23275);
    _23275 = NOVALUE;

    /** 				c_puts(SymTab[s][S_NAME])*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23276 = (int)*(((s1_ptr)_2)->base + _s_43325);
    _2 = (int)SEQ_PTR(_23276);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _23277 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _23277 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _23276 = NOVALUE;
    Ref(_23277);
    _53c_puts(_23277);
    _23277 = NOVALUE;
    goto L6; // [139] 165
L5: 

    /** 				c_puts("_")*/
    RefDS(_22754);
    _53c_puts(_22754);

    /** 				c_puts(SymTab[s][S_NAME])*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23278 = (int)*(((s1_ptr)_2)->base + _s_43325);
    _2 = (int)SEQ_PTR(_23278);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _23279 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _23279 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _23278 = NOVALUE;
    Ref(_23279);
    _53c_puts(_23279);
    _23279 = NOVALUE;
L6: 
L4: 

    /** 		if s != CurrentSub and SymTab[s][S_NREFS] < 2 then*/
    _23280 = (_s_43325 != _25CurrentSub_12270);
    if (_23280 == 0) {
        goto L7; // [174] 222
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23282 = (int)*(((s1_ptr)_2)->base + _s_43325);
    _2 = (int)SEQ_PTR(_23282);
    _23283 = (int)*(((s1_ptr)_2)->base + 12);
    _23282 = NOVALUE;
    if (IS_ATOM_INT(_23283)) {
        _23284 = (_23283 < 2);
    }
    else {
        _23284 = binary_op(LESS, _23283, 2);
    }
    _23283 = NOVALUE;
    if (_23284 == 0) {
        DeRef(_23284);
        _23284 = NOVALUE;
        goto L7; // [195] 222
    }
    else {
        if (!IS_ATOM_INT(_23284) && DBL_PTR(_23284)->dbl == 0.0){
            DeRef(_23284);
            _23284 = NOVALUE;
            goto L7; // [195] 222
        }
        DeRef(_23284);
        _23284 = NOVALUE;
    }
    DeRef(_23284);
    _23284 = NOVALUE;

    /** 			SymTab[s][S_NREFS] += 1*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_s_43325 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _23287 = (int)*(((s1_ptr)_2)->base + 12);
    _23285 = NOVALUE;
    if (IS_ATOM_INT(_23287)) {
        _23288 = _23287 + 1;
        if (_23288 > MAXINT){
            _23288 = NewDouble((double)_23288);
        }
    }
    else
    _23288 = binary_op(PLUS, 1, _23287);
    _23287 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = _23288;
    if( _1 != _23288 ){
        DeRef(_1);
    }
    _23288 = NOVALUE;
    _23285 = NOVALUE;
L7: 

    /** 		SetBBType(s, TYPE_NULL, novalue, TYPE_OBJECT, 0) -- record that this var was referenced in this BB*/
    RefDS(_53novalue_46495);
    _56SetBBType(_s_43325, 0, _53novalue_46495, 16, 0);
    goto L8; // [237] 490
L1: 

    /**  	elsif mode = M_CONSTANT then*/
    if (_mode_43328 != 2)
    goto L9; // [244] 419

    /** 		if (integer( sym_obj( s ) ) and SymTab[s][S_GTYPE] != TYPE_DOUBLE ) or (LeftSym = FALSE and TypeIs(s, TYPE_INTEGER) and v != NOVALUE) then*/
    _23290 = _52sym_obj(_s_43325);
    if (IS_ATOM_INT(_23290))
    _23291 = 1;
    else if (IS_ATOM_DBL(_23290))
    _23291 = IS_ATOM_INT(DoubleToInt(_23290));
    else
    _23291 = 0;
    DeRef(_23290);
    _23290 = NOVALUE;
    if (_23291 == 0) {
        _23292 = 0;
        goto LA; // [257] 283
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23293 = (int)*(((s1_ptr)_2)->base + _s_43325);
    _2 = (int)SEQ_PTR(_23293);
    _23294 = (int)*(((s1_ptr)_2)->base + 36);
    _23293 = NOVALUE;
    if (IS_ATOM_INT(_23294)) {
        _23295 = (_23294 != 2);
    }
    else {
        _23295 = binary_op(NOTEQ, _23294, 2);
    }
    _23294 = NOVALUE;
    if (IS_ATOM_INT(_23295))
    _23292 = (_23295 != 0);
    else
    _23292 = DBL_PTR(_23295)->dbl != 0.0;
LA: 
    if (_23292 != 0) {
        goto LB; // [283] 329
    }
    _23297 = (_56LeftSym_42651 == _5FALSE_242);
    if (_23297 == 0) {
        _23298 = 0;
        goto LC; // [295] 310
    }
    _23299 = _56TypeIs(_s_43325, 1);
    if (IS_ATOM_INT(_23299))
    _23298 = (_23299 != 0);
    else
    _23298 = DBL_PTR(_23299)->dbl != 0.0;
LC: 
    if (_23298 == 0) {
        DeRef(_23300);
        _23300 = 0;
        goto LD; // [310] 324
    }
    if (IS_ATOM_INT(_v_43326) && IS_ATOM_INT(_25NOVALUE_12115)) {
        _23301 = (_v_43326 != _25NOVALUE_12115);
    }
    else {
        _23301 = binary_op(NOTEQ, _v_43326, _25NOVALUE_12115);
    }
    if (IS_ATOM_INT(_23301))
    _23300 = (_23301 != 0);
    else
    _23300 = DBL_PTR(_23301)->dbl != 0.0;
LD: 
    if (_23300 == 0)
    {
        _23300 = NOVALUE;
        goto LE; // [325] 338
    }
    else{
        _23300 = NOVALUE;
    }
LB: 

    /** 			c_printf("%d", v)*/
    RefDS(_23270);
    Ref(_v_43326);
    _53c_printf(_23270, _v_43326);
    goto L8; // [335] 490
LE: 

    /** 			c_printf("_%d", SymTab[s][S_FILE_NO])*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23302 = (int)*(((s1_ptr)_2)->base + _s_43325);
    _2 = (int)SEQ_PTR(_23302);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _23303 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _23303 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _23302 = NOVALUE;
    RefDS(_22775);
    Ref(_23303);
    _53c_printf(_22775, _23303);
    _23303 = NOVALUE;

    /** 			c_puts(SymTab[s][S_NAME])*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23304 = (int)*(((s1_ptr)_2)->base + _s_43325);
    _2 = (int)SEQ_PTR(_23304);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _23305 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _23305 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _23304 = NOVALUE;
    Ref(_23305);
    _53c_puts(_23305);
    _23305 = NOVALUE;

    /** 			if SymTab[s][S_NREFS] < 2 then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23306 = (int)*(((s1_ptr)_2)->base + _s_43325);
    _2 = (int)SEQ_PTR(_23306);
    _23307 = (int)*(((s1_ptr)_2)->base + 12);
    _23306 = NOVALUE;
    if (binary_op_a(GREATEREQ, _23307, 2)){
        _23307 = NOVALUE;
        goto L8; // [387] 490
    }
    _23307 = NOVALUE;

    /** 				SymTab[s][S_NREFS] += 1*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_s_43325 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _23311 = (int)*(((s1_ptr)_2)->base + 12);
    _23309 = NOVALUE;
    if (IS_ATOM_INT(_23311)) {
        _23312 = _23311 + 1;
        if (_23312 > MAXINT){
            _23312 = NewDouble((double)_23312);
        }
    }
    else
    _23312 = binary_op(PLUS, 1, _23311);
    _23311 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = _23312;
    if( _1 != _23312 ){
        DeRef(_1);
    }
    _23312 = NOVALUE;
    _23309 = NOVALUE;
    goto L8; // [416] 490
L9: 

    /** 		if LeftSym = FALSE and GType(s) = TYPE_INTEGER and v != NOVALUE then*/
    _23313 = (_56LeftSym_42651 == _5FALSE_242);
    if (_23313 == 0) {
        _23314 = 0;
        goto LF; // [429] 447
    }
    _23315 = _56GType(_s_43325);
    if (IS_ATOM_INT(_23315)) {
        _23316 = (_23315 == 1);
    }
    else {
        _23316 = binary_op(EQUALS, _23315, 1);
    }
    DeRef(_23315);
    _23315 = NOVALUE;
    if (IS_ATOM_INT(_23316))
    _23314 = (_23316 != 0);
    else
    _23314 = DBL_PTR(_23316)->dbl != 0.0;
LF: 
    if (_23314 == 0) {
        goto L10; // [447] 470
    }
    if (IS_ATOM_INT(_v_43326) && IS_ATOM_INT(_25NOVALUE_12115)) {
        _23318 = (_v_43326 != _25NOVALUE_12115);
    }
    else {
        _23318 = binary_op(NOTEQ, _v_43326, _25NOVALUE_12115);
    }
    if (_23318 == 0) {
        DeRef(_23318);
        _23318 = NOVALUE;
        goto L10; // [458] 470
    }
    else {
        if (!IS_ATOM_INT(_23318) && DBL_PTR(_23318)->dbl == 0.0){
            DeRef(_23318);
            _23318 = NOVALUE;
            goto L10; // [458] 470
        }
        DeRef(_23318);
        _23318 = NOVALUE;
    }
    DeRef(_23318);
    _23318 = NOVALUE;

    /** 			c_printf("%d", v)*/
    RefDS(_23270);
    Ref(_v_43326);
    _53c_printf(_23270, _v_43326);
    goto L11; // [467] 489
L10: 

    /** 			c_printf("_%d", SymTab[s][S_TEMP_NAME])*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23319 = (int)*(((s1_ptr)_2)->base + _s_43325);
    _2 = (int)SEQ_PTR(_23319);
    _23320 = (int)*(((s1_ptr)_2)->base + 34);
    _23319 = NOVALUE;
    RefDS(_22775);
    Ref(_23320);
    _53c_printf(_22775, _23320);
    _23320 = NOVALUE;
L11: 
L8: 

    /** 	LeftSym = FALSE*/
    _56LeftSym_42651 = _5FALSE_242;

    /** end procedure*/
    DeRef(_v_43326);
    DeRef(_23264);
    _23264 = NOVALUE;
    DeRef(_23280);
    _23280 = NOVALUE;
    DeRef(_23267);
    _23267 = NOVALUE;
    DeRef(_23297);
    _23297 = NOVALUE;
    DeRef(_23295);
    _23295 = NOVALUE;
    DeRef(_23299);
    _23299 = NOVALUE;
    DeRef(_23301);
    _23301 = NOVALUE;
    DeRef(_23313);
    _23313 = NOVALUE;
    DeRef(_23316);
    _23316 = NOVALUE;
    return;
    ;
}


void _56c_stmt(int _stmt_43459, int _arg_43460, int _lhs_arg_43462)
{
    int _argcount_43463 = NOVALUE;
    int _i_43464 = NOVALUE;
    int _23375 = NOVALUE;
    int _23374 = NOVALUE;
    int _23373 = NOVALUE;
    int _23372 = NOVALUE;
    int _23371 = NOVALUE;
    int _23370 = NOVALUE;
    int _23369 = NOVALUE;
    int _23368 = NOVALUE;
    int _23367 = NOVALUE;
    int _23366 = NOVALUE;
    int _23365 = NOVALUE;
    int _23364 = NOVALUE;
    int _23363 = NOVALUE;
    int _23362 = NOVALUE;
    int _23361 = NOVALUE;
    int _23360 = NOVALUE;
    int _23359 = NOVALUE;
    int _23357 = NOVALUE;
    int _23355 = NOVALUE;
    int _23353 = NOVALUE;
    int _23352 = NOVALUE;
    int _23351 = NOVALUE;
    int _23350 = NOVALUE;
    int _23348 = NOVALUE;
    int _23347 = NOVALUE;
    int _23346 = NOVALUE;
    int _23345 = NOVALUE;
    int _23344 = NOVALUE;
    int _23343 = NOVALUE;
    int _23342 = NOVALUE;
    int _23341 = NOVALUE;
    int _23340 = NOVALUE;
    int _23339 = NOVALUE;
    int _23338 = NOVALUE;
    int _23337 = NOVALUE;
    int _23336 = NOVALUE;
    int _23335 = NOVALUE;
    int _23332 = NOVALUE;
    int _23331 = NOVALUE;
    int _23330 = NOVALUE;
    int _23329 = NOVALUE;
    int _23328 = NOVALUE;
    int _23327 = NOVALUE;
    int _23325 = NOVALUE;
    int _23323 = NOVALUE;
    int _23322 = NOVALUE;
    int _23321 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_lhs_arg_43462)) {
        _1 = (long)(DBL_PTR(_lhs_arg_43462)->dbl);
        if (UNIQUE(DBL_PTR(_lhs_arg_43462)) && (DBL_PTR(_lhs_arg_43462)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_lhs_arg_43462);
        _lhs_arg_43462 = _1;
    }

    /** 	if LAST_PASS = TRUE and Initializing = FALSE then*/
    _23321 = (_56LAST_PASS_42636 == _5TRUE_244);
    if (_23321 == 0) {
        goto L1; // [15] 47
    }
    _23323 = (_25Initializing_12346 == _5FALSE_242);
    if (_23323 == 0)
    {
        DeRef(_23323);
        _23323 = NOVALUE;
        goto L1; // [28] 47
    }
    else{
        DeRef(_23323);
        _23323 = NOVALUE;
    }

    /** 		cfile_size += 1*/
    _25cfile_size_12345 = _25cfile_size_12345 + 1;

    /** 		update_checksum( stmt )*/
    RefDS(_stmt_43459);
    _54update_checksum(_stmt_43459);
L1: 

    /** 	if emit_c_output then*/
    if (_53emit_c_output_46488 == 0)
    {
        goto L2; // [51] 60
    }
    else{
    }

    /** 		adjust_indent_before(stmt)*/
    RefDS(_stmt_43459);
    _53adjust_indent_before(_stmt_43459);
L2: 

    /** 	if atom(arg) then*/
    _23325 = IS_ATOM(_arg_43460);
    if (_23325 == 0)
    {
        _23325 = NOVALUE;
        goto L3; // [65] 75
    }
    else{
        _23325 = NOVALUE;
    }

    /** 		arg = {arg}*/
    _0 = _arg_43460;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_arg_43460);
    *((int *)(_2+4)) = _arg_43460;
    _arg_43460 = MAKE_SEQ(_1);
    DeRef(_0);
L3: 

    /** 	argcount = 1*/
    _argcount_43463 = 1;

    /** 	i = 1*/
    _i_43464 = 1;

    /** 	while i <= length(stmt) and length(stmt) > 0 do*/
L4: 
    if (IS_SEQUENCE(_stmt_43459)){
            _23327 = SEQ_PTR(_stmt_43459)->length;
    }
    else {
        _23327 = 1;
    }
    _23328 = (_i_43464 <= _23327);
    _23327 = NOVALUE;
    if (_23328 == 0) {
        goto L5; // [97] 435
    }
    if (IS_SEQUENCE(_stmt_43459)){
            _23330 = SEQ_PTR(_stmt_43459)->length;
    }
    else {
        _23330 = 1;
    }
    _23331 = (_23330 > 0);
    _23330 = NOVALUE;
    if (_23331 == 0)
    {
        DeRef(_23331);
        _23331 = NOVALUE;
        goto L5; // [109] 435
    }
    else{
        DeRef(_23331);
        _23331 = NOVALUE;
    }

    /** 		if stmt[i] = '@' then*/
    _2 = (int)SEQ_PTR(_stmt_43459);
    _23332 = (int)*(((s1_ptr)_2)->base + _i_43464);
    if (binary_op_a(NOTEQ, _23332, 64)){
        _23332 = NOVALUE;
        goto L6; // [118] 288
    }
    _23332 = NOVALUE;

    /** 			if i = 1 then*/
    if (_i_43464 != 1)
    goto L7; // [124] 138

    /** 				LeftSym = TRUE*/
    _56LeftSym_42651 = _5TRUE_244;
L7: 

    /** 			if i < length(stmt) and stmt[i+1] > '0' and stmt[i+1] <= '9' then*/
    if (IS_SEQUENCE(_stmt_43459)){
            _23335 = SEQ_PTR(_stmt_43459)->length;
    }
    else {
        _23335 = 1;
    }
    _23336 = (_i_43464 < _23335);
    _23335 = NOVALUE;
    if (_23336 == 0) {
        _23337 = 0;
        goto L8; // [147] 167
    }
    _23338 = _i_43464 + 1;
    _2 = (int)SEQ_PTR(_stmt_43459);
    _23339 = (int)*(((s1_ptr)_2)->base + _23338);
    if (IS_ATOM_INT(_23339)) {
        _23340 = (_23339 > 48);
    }
    else {
        _23340 = binary_op(GREATER, _23339, 48);
    }
    _23339 = NOVALUE;
    if (IS_ATOM_INT(_23340))
    _23337 = (_23340 != 0);
    else
    _23337 = DBL_PTR(_23340)->dbl != 0.0;
L8: 
    if (_23337 == 0) {
        goto L9; // [167] 249
    }
    _23342 = _i_43464 + 1;
    _2 = (int)SEQ_PTR(_stmt_43459);
    _23343 = (int)*(((s1_ptr)_2)->base + _23342);
    if (IS_ATOM_INT(_23343)) {
        _23344 = (_23343 <= 57);
    }
    else {
        _23344 = binary_op(LESSEQ, _23343, 57);
    }
    _23343 = NOVALUE;
    if (_23344 == 0) {
        DeRef(_23344);
        _23344 = NOVALUE;
        goto L9; // [184] 249
    }
    else {
        if (!IS_ATOM_INT(_23344) && DBL_PTR(_23344)->dbl == 0.0){
            DeRef(_23344);
            _23344 = NOVALUE;
            goto L9; // [184] 249
        }
        DeRef(_23344);
        _23344 = NOVALUE;
    }
    DeRef(_23344);
    _23344 = NOVALUE;

    /** 				if arg[stmt[i+1]-'0'] = lhs_arg then*/
    _23345 = _i_43464 + 1;
    _2 = (int)SEQ_PTR(_stmt_43459);
    _23346 = (int)*(((s1_ptr)_2)->base + _23345);
    if (IS_ATOM_INT(_23346)) {
        _23347 = _23346 - 48;
    }
    else {
        _23347 = binary_op(MINUS, _23346, 48);
    }
    _23346 = NOVALUE;
    _2 = (int)SEQ_PTR(_arg_43460);
    if (!IS_ATOM_INT(_23347)){
        _23348 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_23347)->dbl));
    }
    else{
        _23348 = (int)*(((s1_ptr)_2)->base + _23347);
    }
    if (binary_op_a(NOTEQ, _23348, _lhs_arg_43462)){
        _23348 = NOVALUE;
        goto LA; // [205] 219
    }
    _23348 = NOVALUE;

    /** 					LeftSym = TRUE*/
    _56LeftSym_42651 = _5TRUE_244;
LA: 

    /** 				CName(arg[stmt[i+1]-'0'])*/
    _23350 = _i_43464 + 1;
    _2 = (int)SEQ_PTR(_stmt_43459);
    _23351 = (int)*(((s1_ptr)_2)->base + _23350);
    if (IS_ATOM_INT(_23351)) {
        _23352 = _23351 - 48;
    }
    else {
        _23352 = binary_op(MINUS, _23351, 48);
    }
    _23351 = NOVALUE;
    _2 = (int)SEQ_PTR(_arg_43460);
    if (!IS_ATOM_INT(_23352)){
        _23353 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_23352)->dbl));
    }
    else{
        _23353 = (int)*(((s1_ptr)_2)->base + _23352);
    }
    Ref(_23353);
    _56CName(_23353);
    _23353 = NOVALUE;

    /** 				i += 1*/
    _i_43464 = _i_43464 + 1;
    goto LB; // [246] 279
L9: 

    /** 				if arg[argcount] = lhs_arg then*/
    _2 = (int)SEQ_PTR(_arg_43460);
    _23355 = (int)*(((s1_ptr)_2)->base + _argcount_43463);
    if (binary_op_a(NOTEQ, _23355, _lhs_arg_43462)){
        _23355 = NOVALUE;
        goto LC; // [255] 269
    }
    _23355 = NOVALUE;

    /** 					LeftSym = TRUE*/
    _56LeftSym_42651 = _5TRUE_244;
LC: 

    /** 				CName(arg[argcount])*/
    _2 = (int)SEQ_PTR(_arg_43460);
    _23357 = (int)*(((s1_ptr)_2)->base + _argcount_43463);
    Ref(_23357);
    _56CName(_23357);
    _23357 = NOVALUE;
LB: 

    /** 			argcount += 1*/
    _argcount_43463 = _argcount_43463 + 1;
    goto LD; // [285] 353
L6: 

    /** 			c_putc(stmt[i])*/
    _2 = (int)SEQ_PTR(_stmt_43459);
    _23359 = (int)*(((s1_ptr)_2)->base + _i_43464);
    Ref(_23359);
    _53c_putc(_23359);
    _23359 = NOVALUE;

    /** 			if stmt[i] = '&' and i < length(stmt) and stmt[i+1] = '@' then*/
    _2 = (int)SEQ_PTR(_stmt_43459);
    _23360 = (int)*(((s1_ptr)_2)->base + _i_43464);
    if (IS_ATOM_INT(_23360)) {
        _23361 = (_23360 == 38);
    }
    else {
        _23361 = binary_op(EQUALS, _23360, 38);
    }
    _23360 = NOVALUE;
    if (IS_ATOM_INT(_23361)) {
        if (_23361 == 0) {
            DeRef(_23362);
            _23362 = 0;
            goto LE; // [307] 322
        }
    }
    else {
        if (DBL_PTR(_23361)->dbl == 0.0) {
            DeRef(_23362);
            _23362 = 0;
            goto LE; // [307] 322
        }
    }
    if (IS_SEQUENCE(_stmt_43459)){
            _23363 = SEQ_PTR(_stmt_43459)->length;
    }
    else {
        _23363 = 1;
    }
    _23364 = (_i_43464 < _23363);
    _23363 = NOVALUE;
    DeRef(_23362);
    _23362 = (_23364 != 0);
LE: 
    if (_23362 == 0) {
        goto LF; // [322] 352
    }
    _23366 = _i_43464 + 1;
    _2 = (int)SEQ_PTR(_stmt_43459);
    _23367 = (int)*(((s1_ptr)_2)->base + _23366);
    if (IS_ATOM_INT(_23367)) {
        _23368 = (_23367 == 64);
    }
    else {
        _23368 = binary_op(EQUALS, _23367, 64);
    }
    _23367 = NOVALUE;
    if (_23368 == 0) {
        DeRef(_23368);
        _23368 = NOVALUE;
        goto LF; // [339] 352
    }
    else {
        if (!IS_ATOM_INT(_23368) && DBL_PTR(_23368)->dbl == 0.0){
            DeRef(_23368);
            _23368 = NOVALUE;
            goto LF; // [339] 352
        }
        DeRef(_23368);
        _23368 = NOVALUE;
    }
    DeRef(_23368);
    _23368 = NOVALUE;

    /** 				LeftSym = TRUE -- never say: x = x &y or andy - always leave space*/
    _56LeftSym_42651 = _5TRUE_244;
LF: 
LD: 

    /** 		if stmt[i] = '\n' and i < length(stmt) then*/
    _2 = (int)SEQ_PTR(_stmt_43459);
    _23369 = (int)*(((s1_ptr)_2)->base + _i_43464);
    if (IS_ATOM_INT(_23369)) {
        _23370 = (_23369 == 10);
    }
    else {
        _23370 = binary_op(EQUALS, _23369, 10);
    }
    _23369 = NOVALUE;
    if (IS_ATOM_INT(_23370)) {
        if (_23370 == 0) {
            goto L10; // [363] 424
        }
    }
    else {
        if (DBL_PTR(_23370)->dbl == 0.0) {
            goto L10; // [363] 424
        }
    }
    if (IS_SEQUENCE(_stmt_43459)){
            _23372 = SEQ_PTR(_stmt_43459)->length;
    }
    else {
        _23372 = 1;
    }
    _23373 = (_i_43464 < _23372);
    _23372 = NOVALUE;
    if (_23373 == 0)
    {
        DeRef(_23373);
        _23373 = NOVALUE;
        goto L10; // [375] 424
    }
    else{
        DeRef(_23373);
        _23373 = NOVALUE;
    }

    /** 			if emit_c_output then*/
    if (_53emit_c_output_46488 == 0)
    {
        goto L11; // [382] 391
    }
    else{
    }

    /** 				adjust_indent_after(stmt)*/
    RefDS(_stmt_43459);
    _53adjust_indent_after(_stmt_43459);
L11: 

    /** 			stmt = stmt[i+1..$]*/
    _23374 = _i_43464 + 1;
    if (_23374 > MAXINT){
        _23374 = NewDouble((double)_23374);
    }
    if (IS_SEQUENCE(_stmt_43459)){
            _23375 = SEQ_PTR(_stmt_43459)->length;
    }
    else {
        _23375 = 1;
    }
    rhs_slice_target = (object_ptr)&_stmt_43459;
    RHS_Slice(_stmt_43459, _23374, _23375);

    /** 			i = 0*/
    _i_43464 = 0;

    /** 			if emit_c_output then*/
    if (_53emit_c_output_46488 == 0)
    {
        goto L12; // [414] 423
    }
    else{
    }

    /** 				adjust_indent_before(stmt)*/
    RefDS(_stmt_43459);
    _53adjust_indent_before(_stmt_43459);
L12: 
L10: 

    /** 		i += 1*/
    _i_43464 = _i_43464 + 1;

    /** 	end while*/
    goto L4; // [432] 90
L5: 

    /** 	if emit_c_output then*/
    if (_53emit_c_output_46488 == 0)
    {
        goto L13; // [439] 448
    }
    else{
    }

    /** 		adjust_indent_after(stmt)*/
    RefDS(_stmt_43459);
    _53adjust_indent_after(_stmt_43459);
L13: 

    /** end procedure*/
    DeRefDS(_stmt_43459);
    DeRef(_arg_43460);
    DeRef(_23321);
    _23321 = NOVALUE;
    DeRef(_23328);
    _23328 = NOVALUE;
    DeRef(_23336);
    _23336 = NOVALUE;
    DeRef(_23338);
    _23338 = NOVALUE;
    DeRef(_23342);
    _23342 = NOVALUE;
    DeRef(_23340);
    _23340 = NOVALUE;
    DeRef(_23345);
    _23345 = NOVALUE;
    DeRef(_23350);
    _23350 = NOVALUE;
    DeRef(_23347);
    _23347 = NOVALUE;
    DeRef(_23364);
    _23364 = NOVALUE;
    DeRef(_23352);
    _23352 = NOVALUE;
    DeRef(_23361);
    _23361 = NOVALUE;
    DeRef(_23366);
    _23366 = NOVALUE;
    DeRef(_23374);
    _23374 = NOVALUE;
    DeRef(_23370);
    _23370 = NOVALUE;
    return;
    ;
}


void _56c_stmt0(int _stmt_43558)
{
    int _0, _1, _2;
    

    /** 	if emit_c_output then*/
    if (_53emit_c_output_46488 == 0)
    {
        goto L1; // [7] 18
    }
    else{
    }

    /** 		c_stmt(stmt, {})*/
    RefDS(_stmt_43558);
    RefDS(_22682);
    _56c_stmt(_stmt_43558, _22682, 0);
L1: 

    /** end procedure*/
    DeRefDS(_stmt_43558);
    return;
    ;
}


void _56DeclareFileVars()
{
    int _s_43564 = NOVALUE;
    int _eentry_43566 = NOVALUE;
    int _23417 = NOVALUE;
    int _23416 = NOVALUE;
    int _23415 = NOVALUE;
    int _23412 = NOVALUE;
    int _23410 = NOVALUE;
    int _23409 = NOVALUE;
    int _23408 = NOVALUE;
    int _23407 = NOVALUE;
    int _23403 = NOVALUE;
    int _23402 = NOVALUE;
    int _23401 = NOVALUE;
    int _23400 = NOVALUE;
    int _23399 = NOVALUE;
    int _23398 = NOVALUE;
    int _23397 = NOVALUE;
    int _23396 = NOVALUE;
    int _23395 = NOVALUE;
    int _23394 = NOVALUE;
    int _23393 = NOVALUE;
    int _23392 = NOVALUE;
    int _23391 = NOVALUE;
    int _23390 = NOVALUE;
    int _23389 = NOVALUE;
    int _23388 = NOVALUE;
    int _23387 = NOVALUE;
    int _23386 = NOVALUE;
    int _23385 = NOVALUE;
    int _23384 = NOVALUE;
    int _23383 = NOVALUE;
    int _23382 = NOVALUE;
    int _23379 = NOVALUE;
    int _0, _1, _2;
    

    /** 	c_puts("// Declaring file vars\n")*/
    RefDS(_23378);
    _53c_puts(_23378);

    /** 	s = SymTab[TopLevelSub][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23379 = (int)*(((s1_ptr)_2)->base + _25TopLevelSub_12269);
    _2 = (int)SEQ_PTR(_23379);
    _s_43564 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_43564)){
        _s_43564 = (long)DBL_PTR(_s_43564)->dbl;
    }
    _23379 = NOVALUE;

    /** 	while s do*/
L1: 
    if (_s_43564 == 0)
    {
        goto L2; // [29] 321
    }
    else{
    }

    /** 		eentry = SymTab[s]*/
    DeRef(_eentry_43566);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _eentry_43566 = (int)*(((s1_ptr)_2)->base + _s_43564);
    Ref(_eentry_43566);

    /** 		if eentry[S_SCOPE] >= SC_LOCAL*/
    _2 = (int)SEQ_PTR(_eentry_43566);
    _23382 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_23382)) {
        _23383 = (_23382 >= 5);
    }
    else {
        _23383 = binary_op(GREATEREQ, _23382, 5);
    }
    _23382 = NOVALUE;
    if (IS_ATOM_INT(_23383)) {
        if (_23383 == 0) {
            DeRef(_23384);
            _23384 = 0;
            goto L3; // [56] 116
        }
    }
    else {
        if (DBL_PTR(_23383)->dbl == 0.0) {
            DeRef(_23384);
            _23384 = 0;
            goto L3; // [56] 116
        }
    }
    _2 = (int)SEQ_PTR(_eentry_43566);
    _23385 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_23385)) {
        _23386 = (_23385 <= 6);
    }
    else {
        _23386 = binary_op(LESSEQ, _23385, 6);
    }
    _23385 = NOVALUE;
    if (IS_ATOM_INT(_23386)) {
        if (_23386 != 0) {
            DeRef(_23387);
            _23387 = 1;
            goto L4; // [72] 92
        }
    }
    else {
        if (DBL_PTR(_23386)->dbl != 0.0) {
            DeRef(_23387);
            _23387 = 1;
            goto L4; // [72] 92
        }
    }
    _2 = (int)SEQ_PTR(_eentry_43566);
    _23388 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_23388)) {
        _23389 = (_23388 == 11);
    }
    else {
        _23389 = binary_op(EQUALS, _23388, 11);
    }
    _23388 = NOVALUE;
    DeRef(_23387);
    if (IS_ATOM_INT(_23389))
    _23387 = (_23389 != 0);
    else
    _23387 = DBL_PTR(_23389)->dbl != 0.0;
L4: 
    if (_23387 != 0) {
        _23390 = 1;
        goto L5; // [92] 112
    }
    _2 = (int)SEQ_PTR(_eentry_43566);
    _23391 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_23391)) {
        _23392 = (_23391 == 13);
    }
    else {
        _23392 = binary_op(EQUALS, _23391, 13);
    }
    _23391 = NOVALUE;
    if (IS_ATOM_INT(_23392))
    _23390 = (_23392 != 0);
    else
    _23390 = DBL_PTR(_23392)->dbl != 0.0;
L5: 
    DeRef(_23384);
    _23384 = (_23390 != 0);
L3: 
    if (_23384 == 0) {
        _23393 = 0;
        goto L6; // [116] 136
    }
    _2 = (int)SEQ_PTR(_eentry_43566);
    _23394 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_ATOM_INT(_23394)) {
        _23395 = (_23394 != 0);
    }
    else {
        _23395 = binary_op(NOTEQ, _23394, 0);
    }
    _23394 = NOVALUE;
    if (IS_ATOM_INT(_23395))
    _23393 = (_23395 != 0);
    else
    _23393 = DBL_PTR(_23395)->dbl != 0.0;
L6: 
    if (_23393 == 0) {
        _23396 = 0;
        goto L7; // [136] 156
    }
    _2 = (int)SEQ_PTR(_eentry_43566);
    _23397 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_ATOM_INT(_23397)) {
        _23398 = (_23397 != 99);
    }
    else {
        _23398 = binary_op(NOTEQ, _23397, 99);
    }
    _23397 = NOVALUE;
    if (IS_ATOM_INT(_23398))
    _23396 = (_23398 != 0);
    else
    _23396 = DBL_PTR(_23398)->dbl != 0.0;
L7: 
    if (_23396 == 0) {
        goto L8; // [156] 300
    }
    _2 = (int)SEQ_PTR(_eentry_43566);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _23400 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _23400 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _23401 = find_from(_23400, _28RTN_TOKS_11857, 1);
    _23400 = NOVALUE;
    _23402 = (_23401 == 0);
    _23401 = NOVALUE;
    if (_23402 == 0)
    {
        DeRef(_23402);
        _23402 = NOVALUE;
        goto L8; // [177] 300
    }
    else{
        DeRef(_23402);
        _23402 = NOVALUE;
    }

    /** 			if eentry[S_TOKEN] = PROC then*/
    _2 = (int)SEQ_PTR(_eentry_43566);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _23403 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _23403 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    if (binary_op_a(NOTEQ, _23403, 27)){
        _23403 = NOVALUE;
        goto L9; // [190] 202
    }
    _23403 = NOVALUE;

    /** 				c_puts( "void ")*/
    RefDS(_23405);
    _53c_puts(_23405);
    goto LA; // [199] 208
L9: 

    /** 				c_puts("int ")*/
    RefDS(_23406);
    _53c_puts(_23406);
LA: 

    /** 			c_printf("_%d", eentry[S_FILE_NO])*/
    _2 = (int)SEQ_PTR(_eentry_43566);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _23407 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _23407 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    RefDS(_22775);
    Ref(_23407);
    _53c_printf(_22775, _23407);
    _23407 = NOVALUE;

    /** 			c_puts(eentry[S_NAME])*/
    _2 = (int)SEQ_PTR(_eentry_43566);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _23408 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _23408 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    Ref(_23408);
    _53c_puts(_23408);
    _23408 = NOVALUE;

    /** 			if integer( eentry[S_OBJ] ) then*/
    _2 = (int)SEQ_PTR(_eentry_43566);
    _23409 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_23409))
    _23410 = 1;
    else if (IS_ATOM_DBL(_23409))
    _23410 = IS_ATOM_INT(DoubleToInt(_23409));
    else
    _23410 = 0;
    _23409 = NOVALUE;
    if (_23410 == 0)
    {
        _23410 = NOVALUE;
        goto LB; // [242] 260
    }
    else{
        _23410 = NOVALUE;
    }

    /** 					c_printf(" = %d;\n", eentry[S_OBJ] )*/
    _2 = (int)SEQ_PTR(_eentry_43566);
    _23412 = (int)*(((s1_ptr)_2)->base + 1);
    RefDS(_23411);
    Ref(_23412);
    _53c_printf(_23411, _23412);
    _23412 = NOVALUE;
    goto LC; // [257] 266
LB: 

    /** 				c_puts(" = NOVALUE;\n")*/
    RefDS(_23413);
    _53c_puts(_23413);
LC: 

    /** 			c_hputs("extern int ")*/
    RefDS(_23414);
    _53c_hputs(_23414);

    /** 			c_hprintf("_%d", eentry[S_FILE_NO])*/
    _2 = (int)SEQ_PTR(_eentry_43566);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _23415 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _23415 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    RefDS(_22775);
    Ref(_23415);
    _53c_hprintf(_22775, _23415);
    _23415 = NOVALUE;

    /** 			c_hputs(eentry[S_NAME])*/
    _2 = (int)SEQ_PTR(_eentry_43566);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _23416 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _23416 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    Ref(_23416);
    _53c_hputs(_23416);
    _23416 = NOVALUE;

    /** 			c_hputs(";\n")*/
    RefDS(_22912);
    _53c_hputs(_22912);
L8: 

    /** 		s = SymTab[s][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23417 = (int)*(((s1_ptr)_2)->base + _s_43564);
    _2 = (int)SEQ_PTR(_23417);
    _s_43564 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_43564)){
        _s_43564 = (long)DBL_PTR(_s_43564)->dbl;
    }
    _23417 = NOVALUE;

    /** 	end while*/
    goto L1; // [318] 29
L2: 

    /** 	c_puts("\n")*/
    RefDS(_22834);
    _53c_puts(_22834);

    /** 	c_hputs("\n")*/
    RefDS(_22834);
    _53c_hputs(_22834);

    /** end procedure*/
    DeRef(_eentry_43566);
    DeRef(_23383);
    _23383 = NOVALUE;
    DeRef(_23386);
    _23386 = NOVALUE;
    DeRef(_23389);
    _23389 = NOVALUE;
    DeRef(_23392);
    _23392 = NOVALUE;
    DeRef(_23395);
    _23395 = NOVALUE;
    DeRef(_23398);
    _23398 = NOVALUE;
    return;
    ;
}


int _56PromoteTypeInfo()
{
    int _updsym_43658 = NOVALUE;
    int _s_43660 = NOVALUE;
    int _sym_43661 = NOVALUE;
    int _symo_43662 = NOVALUE;
    int _upd_43891 = NOVALUE;
    int _23517 = NOVALUE;
    int _23515 = NOVALUE;
    int _23514 = NOVALUE;
    int _23513 = NOVALUE;
    int _23512 = NOVALUE;
    int _23510 = NOVALUE;
    int _23508 = NOVALUE;
    int _23507 = NOVALUE;
    int _23506 = NOVALUE;
    int _23505 = NOVALUE;
    int _23504 = NOVALUE;
    int _23500 = NOVALUE;
    int _23497 = NOVALUE;
    int _23496 = NOVALUE;
    int _23495 = NOVALUE;
    int _23494 = NOVALUE;
    int _23493 = NOVALUE;
    int _23492 = NOVALUE;
    int _23491 = NOVALUE;
    int _23490 = NOVALUE;
    int _23489 = NOVALUE;
    int _23488 = NOVALUE;
    int _23487 = NOVALUE;
    int _23485 = NOVALUE;
    int _23484 = NOVALUE;
    int _23483 = NOVALUE;
    int _23482 = NOVALUE;
    int _23481 = NOVALUE;
    int _23480 = NOVALUE;
    int _23479 = NOVALUE;
    int _23478 = NOVALUE;
    int _23477 = NOVALUE;
    int _23476 = NOVALUE;
    int _23474 = NOVALUE;
    int _23473 = NOVALUE;
    int _23472 = NOVALUE;
    int _23470 = NOVALUE;
    int _23469 = NOVALUE;
    int _23468 = NOVALUE;
    int _23466 = NOVALUE;
    int _23465 = NOVALUE;
    int _23464 = NOVALUE;
    int _23463 = NOVALUE;
    int _23462 = NOVALUE;
    int _23461 = NOVALUE;
    int _23460 = NOVALUE;
    int _23458 = NOVALUE;
    int _23457 = NOVALUE;
    int _23456 = NOVALUE;
    int _23455 = NOVALUE;
    int _23453 = NOVALUE;
    int _23452 = NOVALUE;
    int _23450 = NOVALUE;
    int _23449 = NOVALUE;
    int _23448 = NOVALUE;
    int _23447 = NOVALUE;
    int _23446 = NOVALUE;
    int _23445 = NOVALUE;
    int _23444 = NOVALUE;
    int _23442 = NOVALUE;
    int _23441 = NOVALUE;
    int _23440 = NOVALUE;
    int _23439 = NOVALUE;
    int _23438 = NOVALUE;
    int _23437 = NOVALUE;
    int _23436 = NOVALUE;
    int _23435 = NOVALUE;
    int _23434 = NOVALUE;
    int _23433 = NOVALUE;
    int _23432 = NOVALUE;
    int _23431 = NOVALUE;
    int _23430 = NOVALUE;
    int _23429 = NOVALUE;
    int _23427 = NOVALUE;
    int _23426 = NOVALUE;
    int _23425 = NOVALUE;
    int _23423 = NOVALUE;
    int _23422 = NOVALUE;
    int _23419 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	sequence sym, symo*/

    /** 	updsym = 0*/
    _updsym_43658 = 0;

    /** 	g_has_delete = p_has_delete*/
    _56g_has_delete_42844 = _56p_has_delete_42845;

    /** 	s = SymTab[TopLevelSub][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23419 = (int)*(((s1_ptr)_2)->base + _25TopLevelSub_12269);
    _2 = (int)SEQ_PTR(_23419);
    _s_43660 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_43660)){
        _s_43660 = (long)DBL_PTR(_s_43660)->dbl;
    }
    _23419 = NOVALUE;

    /** 	while s do*/
L1: 
    if (_s_43660 == 0)
    {
        goto L2; // [38] 921
    }
    else{
    }

    /** 		sym = SymTab[s]*/
    DeRef(_sym_43661);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _sym_43661 = (int)*(((s1_ptr)_2)->base + _s_43660);
    Ref(_sym_43661);

    /** 		symo = sym*/
    RefDS(_sym_43661);
    DeRef(_symo_43662);
    _symo_43662 = _sym_43661;

    /** 		if sym[S_TOKEN] = FUNC or sym[S_TOKEN] = TYPE then*/
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _23422 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _23422 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    if (IS_ATOM_INT(_23422)) {
        _23423 = (_23422 == 501);
    }
    else {
        _23423 = binary_op(EQUALS, _23422, 501);
    }
    _23422 = NOVALUE;
    if (IS_ATOM_INT(_23423)) {
        if (_23423 != 0) {
            goto L3; // [72] 93
        }
    }
    else {
        if (DBL_PTR(_23423)->dbl != 0.0) {
            goto L3; // [72] 93
        }
    }
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _23425 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _23425 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    if (IS_ATOM_INT(_23425)) {
        _23426 = (_23425 == 504);
    }
    else {
        _23426 = binary_op(EQUALS, _23425, 504);
    }
    _23425 = NOVALUE;
    if (_23426 == 0) {
        DeRef(_23426);
        _23426 = NOVALUE;
        goto L4; // [89] 138
    }
    else {
        if (!IS_ATOM_INT(_23426) && DBL_PTR(_23426)->dbl == 0.0){
            DeRef(_23426);
            _23426 = NOVALUE;
            goto L4; // [89] 138
        }
        DeRef(_23426);
        _23426 = NOVALUE;
    }
    DeRef(_23426);
    _23426 = NOVALUE;
L3: 

    /** 			if sym[S_GTYPE_NEW] = TYPE_NULL then*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23427 = (int)*(((s1_ptr)_2)->base + 38);
    if (binary_op_a(NOTEQ, _23427, 0)){
        _23427 = NOVALUE;
        goto L5; // [103] 120
    }
    _23427 = NOVALUE;

    /** 				sym[S_GTYPE] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);
    goto L6; // [117] 549
L5: 

    /** 				sym[S_GTYPE] = sym[S_GTYPE_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23429 = (int)*(((s1_ptr)_2)->base + 38);
    Ref(_23429);
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = _23429;
    if( _1 != _23429 ){
        DeRef(_1);
    }
    _23429 = NOVALUE;
    goto L6; // [135] 549
L4: 

    /** 			if sym[S_GTYPE] != TYPE_INTEGER and*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23430 = (int)*(((s1_ptr)_2)->base + 36);
    if (IS_ATOM_INT(_23430)) {
        _23431 = (_23430 != 1);
    }
    else {
        _23431 = binary_op(NOTEQ, _23430, 1);
    }
    _23430 = NOVALUE;
    if (IS_ATOM_INT(_23431)) {
        if (_23431 == 0) {
            DeRef(_23432);
            _23432 = 0;
            goto L7; // [152] 172
        }
    }
    else {
        if (DBL_PTR(_23431)->dbl == 0.0) {
            DeRef(_23432);
            _23432 = 0;
            goto L7; // [152] 172
        }
    }
    _2 = (int)SEQ_PTR(_sym_43661);
    _23433 = (int)*(((s1_ptr)_2)->base + 38);
    if (IS_ATOM_INT(_23433)) {
        _23434 = (_23433 != 16);
    }
    else {
        _23434 = binary_op(NOTEQ, _23433, 16);
    }
    _23433 = NOVALUE;
    DeRef(_23432);
    if (IS_ATOM_INT(_23434))
    _23432 = (_23434 != 0);
    else
    _23432 = DBL_PTR(_23434)->dbl != 0.0;
L7: 
    if (_23432 == 0) {
        goto L8; // [172] 283
    }
    _2 = (int)SEQ_PTR(_sym_43661);
    _23436 = (int)*(((s1_ptr)_2)->base + 38);
    if (IS_ATOM_INT(_23436)) {
        _23437 = (_23436 != 0);
    }
    else {
        _23437 = binary_op(NOTEQ, _23436, 0);
    }
    _23436 = NOVALUE;
    if (_23437 == 0) {
        DeRef(_23437);
        _23437 = NOVALUE;
        goto L8; // [189] 283
    }
    else {
        if (!IS_ATOM_INT(_23437) && DBL_PTR(_23437)->dbl == 0.0){
            DeRef(_23437);
            _23437 = NOVALUE;
            goto L8; // [189] 283
        }
        DeRef(_23437);
        _23437 = NOVALUE;
    }
    DeRef(_23437);
    _23437 = NOVALUE;

    /** 				if sym[S_GTYPE_NEW] = TYPE_INTEGER or*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23438 = (int)*(((s1_ptr)_2)->base + 38);
    if (IS_ATOM_INT(_23438)) {
        _23439 = (_23438 == 1);
    }
    else {
        _23439 = binary_op(EQUALS, _23438, 1);
    }
    _23438 = NOVALUE;
    if (IS_ATOM_INT(_23439)) {
        if (_23439 != 0) {
            DeRef(_23440);
            _23440 = 1;
            goto L9; // [206] 226
        }
    }
    else {
        if (DBL_PTR(_23439)->dbl != 0.0) {
            DeRef(_23440);
            _23440 = 1;
            goto L9; // [206] 226
        }
    }
    _2 = (int)SEQ_PTR(_sym_43661);
    _23441 = (int)*(((s1_ptr)_2)->base + 36);
    if (IS_ATOM_INT(_23441)) {
        _23442 = (_23441 == 16);
    }
    else {
        _23442 = binary_op(EQUALS, _23441, 16);
    }
    _23441 = NOVALUE;
    DeRef(_23440);
    if (IS_ATOM_INT(_23442))
    _23440 = (_23442 != 0);
    else
    _23440 = DBL_PTR(_23442)->dbl != 0.0;
L9: 
    if (_23440 != 0) {
        goto LA; // [226] 267
    }
    _2 = (int)SEQ_PTR(_sym_43661);
    _23444 = (int)*(((s1_ptr)_2)->base + 36);
    if (IS_ATOM_INT(_23444)) {
        _23445 = (_23444 == 4);
    }
    else {
        _23445 = binary_op(EQUALS, _23444, 4);
    }
    _23444 = NOVALUE;
    if (IS_ATOM_INT(_23445)) {
        if (_23445 == 0) {
            DeRef(_23446);
            _23446 = 0;
            goto LB; // [242] 262
        }
    }
    else {
        if (DBL_PTR(_23445)->dbl == 0.0) {
            DeRef(_23446);
            _23446 = 0;
            goto LB; // [242] 262
        }
    }
    _2 = (int)SEQ_PTR(_sym_43661);
    _23447 = (int)*(((s1_ptr)_2)->base + 38);
    if (IS_ATOM_INT(_23447)) {
        _23448 = (_23447 == 2);
    }
    else {
        _23448 = binary_op(EQUALS, _23447, 2);
    }
    _23447 = NOVALUE;
    DeRef(_23446);
    if (IS_ATOM_INT(_23448))
    _23446 = (_23448 != 0);
    else
    _23446 = DBL_PTR(_23448)->dbl != 0.0;
LB: 
    if (_23446 == 0)
    {
        _23446 = NOVALUE;
        goto LC; // [263] 282
    }
    else{
        _23446 = NOVALUE;
    }
LA: 

    /** 						sym[S_GTYPE] = sym[S_GTYPE_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23449 = (int)*(((s1_ptr)_2)->base + 38);
    Ref(_23449);
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = _23449;
    if( _1 != _23449 ){
        DeRef(_1);
    }
    _23449 = NOVALUE;
LC: 
L8: 

    /** 			if sym[S_ARG_TYPE_NEW] = TYPE_NULL then*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23450 = (int)*(((s1_ptr)_2)->base + 44);
    if (binary_op_a(NOTEQ, _23450, 0)){
        _23450 = NOVALUE;
        goto LD; // [293] 310
    }
    _23450 = NOVALUE;

    /** 				sym[S_ARG_TYPE] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 43);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);
    goto LE; // [307] 325
LD: 

    /** 				sym[S_ARG_TYPE] = sym[S_ARG_TYPE_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23452 = (int)*(((s1_ptr)_2)->base + 44);
    Ref(_23452);
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 43);
    _1 = *(int *)_2;
    *(int *)_2 = _23452;
    if( _1 != _23452 ){
        DeRef(_1);
    }
    _23452 = NOVALUE;
LE: 

    /** 			sym[S_ARG_TYPE_NEW] = TYPE_NULL*/
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 44);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 			if sym[S_ARG_SEQ_ELEM_NEW] = TYPE_NULL then*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23453 = (int)*(((s1_ptr)_2)->base + 46);
    if (binary_op_a(NOTEQ, _23453, 0)){
        _23453 = NOVALUE;
        goto LF; // [345] 362
    }
    _23453 = NOVALUE;

    /** 				sym[S_ARG_SEQ_ELEM] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 45);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);
    goto L10; // [359] 377
LF: 

    /** 				sym[S_ARG_SEQ_ELEM] = sym[S_ARG_SEQ_ELEM_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23455 = (int)*(((s1_ptr)_2)->base + 46);
    Ref(_23455);
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 45);
    _1 = *(int *)_2;
    *(int *)_2 = _23455;
    if( _1 != _23455 ){
        DeRef(_1);
    }
    _23455 = NOVALUE;
L10: 

    /** 			sym[S_ARG_SEQ_ELEM_NEW] = TYPE_NULL*/
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 46);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 			if sym[S_ARG_MIN_NEW] = -NOVALUE or*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23456 = (int)*(((s1_ptr)_2)->base + 49);
    if (IS_ATOM_INT(_25NOVALUE_12115)) {
        if ((unsigned long)_25NOVALUE_12115 == 0xC0000000)
        _23457 = (int)NewDouble((double)-0xC0000000);
        else
        _23457 = - _25NOVALUE_12115;
    }
    else {
        _23457 = unary_op(UMINUS, _25NOVALUE_12115);
    }
    if (IS_ATOM_INT(_23456) && IS_ATOM_INT(_23457)) {
        _23458 = (_23456 == _23457);
    }
    else {
        _23458 = binary_op(EQUALS, _23456, _23457);
    }
    _23456 = NOVALUE;
    DeRef(_23457);
    _23457 = NOVALUE;
    if (IS_ATOM_INT(_23458)) {
        if (_23458 != 0) {
            goto L11; // [404] 425
        }
    }
    else {
        if (DBL_PTR(_23458)->dbl != 0.0) {
            goto L11; // [404] 425
        }
    }
    _2 = (int)SEQ_PTR(_sym_43661);
    _23460 = (int)*(((s1_ptr)_2)->base + 49);
    if (IS_ATOM_INT(_23460) && IS_ATOM_INT(_25NOVALUE_12115)) {
        _23461 = (_23460 == _25NOVALUE_12115);
    }
    else {
        _23461 = binary_op(EQUALS, _23460, _25NOVALUE_12115);
    }
    _23460 = NOVALUE;
    if (_23461 == 0) {
        DeRef(_23461);
        _23461 = NOVALUE;
        goto L12; // [421] 448
    }
    else {
        if (!IS_ATOM_INT(_23461) && DBL_PTR(_23461)->dbl == 0.0){
            DeRef(_23461);
            _23461 = NOVALUE;
            goto L12; // [421] 448
        }
        DeRef(_23461);
        _23461 = NOVALUE;
    }
    DeRef(_23461);
    _23461 = NOVALUE;
L11: 

    /** 				sym[S_ARG_MIN] = MININT*/
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 47);
    _1 = *(int *)_2;
    *(int *)_2 = -1073741824;
    DeRef(_1);

    /** 				sym[S_ARG_MAX] = MAXINT*/
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 48);
    _1 = *(int *)_2;
    *(int *)_2 = 1073741823;
    DeRef(_1);
    goto L13; // [445] 477
L12: 

    /** 				sym[S_ARG_MIN] = sym[S_ARG_MIN_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23462 = (int)*(((s1_ptr)_2)->base + 49);
    Ref(_23462);
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 47);
    _1 = *(int *)_2;
    *(int *)_2 = _23462;
    if( _1 != _23462 ){
        DeRef(_1);
    }
    _23462 = NOVALUE;

    /** 				sym[S_ARG_MAX] = sym[S_ARG_MAX_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23463 = (int)*(((s1_ptr)_2)->base + 50);
    Ref(_23463);
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 48);
    _1 = *(int *)_2;
    *(int *)_2 = _23463;
    if( _1 != _23463 ){
        DeRef(_1);
    }
    _23463 = NOVALUE;
L13: 

    /** 			sym[S_ARG_MIN_NEW] = -NOVALUE*/
    if (IS_ATOM_INT(_25NOVALUE_12115)) {
        if ((unsigned long)_25NOVALUE_12115 == 0xC0000000)
        _23464 = (int)NewDouble((double)-0xC0000000);
        else
        _23464 = - _25NOVALUE_12115;
    }
    else {
        _23464 = unary_op(UMINUS, _25NOVALUE_12115);
    }
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 49);
    _1 = *(int *)_2;
    *(int *)_2 = _23464;
    if( _1 != _23464 ){
        DeRef(_1);
    }
    _23464 = NOVALUE;

    /** 			if sym[S_ARG_SEQ_LEN_NEW] = -NOVALUE then*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23465 = (int)*(((s1_ptr)_2)->base + 52);
    if (IS_ATOM_INT(_25NOVALUE_12115)) {
        if ((unsigned long)_25NOVALUE_12115 == 0xC0000000)
        _23466 = (int)NewDouble((double)-0xC0000000);
        else
        _23466 = - _25NOVALUE_12115;
    }
    else {
        _23466 = unary_op(UMINUS, _25NOVALUE_12115);
    }
    if (binary_op_a(NOTEQ, _23465, _23466)){
        _23465 = NOVALUE;
        DeRef(_23466);
        _23466 = NOVALUE;
        goto L14; // [503] 520
    }
    _23465 = NOVALUE;
    DeRef(_23466);
    _23466 = NOVALUE;

    /** 				sym[S_ARG_SEQ_LEN] = NOVALUE*/
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 51);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);
    goto L15; // [517] 535
L14: 

    /** 				sym[S_ARG_SEQ_LEN] = sym[S_ARG_SEQ_LEN_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23468 = (int)*(((s1_ptr)_2)->base + 52);
    Ref(_23468);
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 51);
    _1 = *(int *)_2;
    *(int *)_2 = _23468;
    if( _1 != _23468 ){
        DeRef(_1);
    }
    _23468 = NOVALUE;
L15: 

    /** 			sym[S_ARG_SEQ_LEN_NEW] = -NOVALUE*/
    if (IS_ATOM_INT(_25NOVALUE_12115)) {
        if ((unsigned long)_25NOVALUE_12115 == 0xC0000000)
        _23469 = (int)NewDouble((double)-0xC0000000);
        else
        _23469 = - _25NOVALUE_12115;
    }
    else {
        _23469 = unary_op(UMINUS, _25NOVALUE_12115);
    }
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 52);
    _1 = *(int *)_2;
    *(int *)_2 = _23469;
    if( _1 != _23469 ){
        DeRef(_1);
    }
    _23469 = NOVALUE;
L6: 

    /** 		sym[S_GTYPE_NEW] = TYPE_NULL*/
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 38);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		if sym[S_SEQ_ELEM_NEW] = TYPE_NULL then*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23470 = (int)*(((s1_ptr)_2)->base + 40);
    if (binary_op_a(NOTEQ, _23470, 0)){
        _23470 = NOVALUE;
        goto L16; // [569] 586
    }
    _23470 = NOVALUE;

    /** 		   sym[S_SEQ_ELEM] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);
    goto L17; // [583] 601
L16: 

    /** 			sym[S_SEQ_ELEM] = sym[S_SEQ_ELEM_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23472 = (int)*(((s1_ptr)_2)->base + 40);
    Ref(_23472);
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = _23472;
    if( _1 != _23472 ){
        DeRef(_1);
    }
    _23472 = NOVALUE;
L17: 

    /** 		sym[S_SEQ_ELEM_NEW] = TYPE_NULL*/
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 40);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		if sym[S_SEQ_LEN_NEW] = -NOVALUE then*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23473 = (int)*(((s1_ptr)_2)->base + 39);
    if (IS_ATOM_INT(_25NOVALUE_12115)) {
        if ((unsigned long)_25NOVALUE_12115 == 0xC0000000)
        _23474 = (int)NewDouble((double)-0xC0000000);
        else
        _23474 = - _25NOVALUE_12115;
    }
    else {
        _23474 = unary_op(UMINUS, _25NOVALUE_12115);
    }
    if (binary_op_a(NOTEQ, _23473, _23474)){
        _23473 = NOVALUE;
        DeRef(_23474);
        _23474 = NOVALUE;
        goto L18; // [624] 641
    }
    _23473 = NOVALUE;
    DeRef(_23474);
    _23474 = NOVALUE;

    /** 			sym[S_SEQ_LEN] = NOVALUE*/
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);
    goto L19; // [638] 656
L18: 

    /** 			sym[S_SEQ_LEN] = sym[S_SEQ_LEN_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23476 = (int)*(((s1_ptr)_2)->base + 39);
    Ref(_23476);
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _23476;
    if( _1 != _23476 ){
        DeRef(_1);
    }
    _23476 = NOVALUE;
L19: 

    /** 		sym[S_SEQ_LEN_NEW] = -NOVALUE*/
    if (IS_ATOM_INT(_25NOVALUE_12115)) {
        if ((unsigned long)_25NOVALUE_12115 == 0xC0000000)
        _23477 = (int)NewDouble((double)-0xC0000000);
        else
        _23477 = - _25NOVALUE_12115;
    }
    else {
        _23477 = unary_op(UMINUS, _25NOVALUE_12115);
    }
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 39);
    _1 = *(int *)_2;
    *(int *)_2 = _23477;
    if( _1 != _23477 ){
        DeRef(_1);
    }
    _23477 = NOVALUE;

    /** 		if sym[S_TOKEN] != NAMESPACE*/
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _23478 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _23478 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    if (IS_ATOM_INT(_23478)) {
        _23479 = (_23478 != 523);
    }
    else {
        _23479 = binary_op(NOTEQ, _23478, 523);
    }
    _23478 = NOVALUE;
    if (IS_ATOM_INT(_23479)) {
        if (_23479 == 0) {
            goto L1A; // [683] 794
        }
    }
    else {
        if (DBL_PTR(_23479)->dbl == 0.0) {
            goto L1A; // [683] 794
        }
    }
    _2 = (int)SEQ_PTR(_sym_43661);
    _23481 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_23481)) {
        _23482 = (_23481 != 2);
    }
    else {
        _23482 = binary_op(NOTEQ, _23481, 2);
    }
    _23481 = NOVALUE;
    if (_23482 == 0) {
        DeRef(_23482);
        _23482 = NOVALUE;
        goto L1A; // [700] 794
    }
    else {
        if (!IS_ATOM_INT(_23482) && DBL_PTR(_23482)->dbl == 0.0){
            DeRef(_23482);
            _23482 = NOVALUE;
            goto L1A; // [700] 794
        }
        DeRef(_23482);
        _23482 = NOVALUE;
    }
    DeRef(_23482);
    _23482 = NOVALUE;

    /** 			if sym[S_OBJ_MIN_NEW] = -NOVALUE or*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23483 = (int)*(((s1_ptr)_2)->base + 41);
    if (IS_ATOM_INT(_25NOVALUE_12115)) {
        if ((unsigned long)_25NOVALUE_12115 == 0xC0000000)
        _23484 = (int)NewDouble((double)-0xC0000000);
        else
        _23484 = - _25NOVALUE_12115;
    }
    else {
        _23484 = unary_op(UMINUS, _25NOVALUE_12115);
    }
    if (IS_ATOM_INT(_23483) && IS_ATOM_INT(_23484)) {
        _23485 = (_23483 == _23484);
    }
    else {
        _23485 = binary_op(EQUALS, _23483, _23484);
    }
    _23483 = NOVALUE;
    DeRef(_23484);
    _23484 = NOVALUE;
    if (IS_ATOM_INT(_23485)) {
        if (_23485 != 0) {
            goto L1B; // [720] 741
        }
    }
    else {
        if (DBL_PTR(_23485)->dbl != 0.0) {
            goto L1B; // [720] 741
        }
    }
    _2 = (int)SEQ_PTR(_sym_43661);
    _23487 = (int)*(((s1_ptr)_2)->base + 41);
    if (IS_ATOM_INT(_23487) && IS_ATOM_INT(_25NOVALUE_12115)) {
        _23488 = (_23487 == _25NOVALUE_12115);
    }
    else {
        _23488 = binary_op(EQUALS, _23487, _25NOVALUE_12115);
    }
    _23487 = NOVALUE;
    if (_23488 == 0) {
        DeRef(_23488);
        _23488 = NOVALUE;
        goto L1C; // [737] 764
    }
    else {
        if (!IS_ATOM_INT(_23488) && DBL_PTR(_23488)->dbl == 0.0){
            DeRef(_23488);
            _23488 = NOVALUE;
            goto L1C; // [737] 764
        }
        DeRef(_23488);
        _23488 = NOVALUE;
    }
    DeRef(_23488);
    _23488 = NOVALUE;
L1B: 

    /** 				sym[S_OBJ_MIN] = MININT*/
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 30);
    _1 = *(int *)_2;
    *(int *)_2 = -1073741824;
    DeRef(_1);

    /** 				sym[S_OBJ_MAX] = MAXINT*/
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 31);
    _1 = *(int *)_2;
    *(int *)_2 = 1073741823;
    DeRef(_1);
    goto L1D; // [761] 793
L1C: 

    /** 				sym[S_OBJ_MIN] = sym[S_OBJ_MIN_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23489 = (int)*(((s1_ptr)_2)->base + 41);
    Ref(_23489);
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 30);
    _1 = *(int *)_2;
    *(int *)_2 = _23489;
    if( _1 != _23489 ){
        DeRef(_1);
    }
    _23489 = NOVALUE;

    /** 				sym[S_OBJ_MAX] = sym[S_OBJ_MAX_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23490 = (int)*(((s1_ptr)_2)->base + 42);
    Ref(_23490);
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 31);
    _1 = *(int *)_2;
    *(int *)_2 = _23490;
    if( _1 != _23490 ){
        DeRef(_1);
    }
    _23490 = NOVALUE;
L1D: 
L1A: 

    /** 		sym[S_OBJ_MIN_NEW] = -NOVALUE*/
    if (IS_ATOM_INT(_25NOVALUE_12115)) {
        if ((unsigned long)_25NOVALUE_12115 == 0xC0000000)
        _23491 = (int)NewDouble((double)-0xC0000000);
        else
        _23491 = - _25NOVALUE_12115;
    }
    else {
        _23491 = unary_op(UMINUS, _25NOVALUE_12115);
    }
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 41);
    _1 = *(int *)_2;
    *(int *)_2 = _23491;
    if( _1 != _23491 ){
        DeRef(_1);
    }
    _23491 = NOVALUE;

    /** 		if sym[S_NREFS] = 1 and*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23492 = (int)*(((s1_ptr)_2)->base + 12);
    if (IS_ATOM_INT(_23492)) {
        _23493 = (_23492 == 1);
    }
    else {
        _23493 = binary_op(EQUALS, _23492, 1);
    }
    _23492 = NOVALUE;
    if (IS_ATOM_INT(_23493)) {
        if (_23493 == 0) {
            goto L1E; // [819] 874
        }
    }
    else {
        if (DBL_PTR(_23493)->dbl == 0.0) {
            goto L1E; // [819] 874
        }
    }
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _23495 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _23495 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _23496 = find_from(_23495, _28RTN_TOKS_11857, 1);
    _23495 = NOVALUE;
    if (_23496 == 0)
    {
        _23496 = NOVALUE;
        goto L1E; // [837] 874
    }
    else{
        _23496 = NOVALUE;
    }

    /** 			if sym[S_USAGE] != U_DELETED then*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _23497 = (int)*(((s1_ptr)_2)->base + 5);
    if (binary_op_a(EQUALS, _23497, 99)){
        _23497 = NOVALUE;
        goto L1F; // [850] 873
    }
    _23497 = NOVALUE;

    /** 				sym[S_USAGE] = U_DELETED*/
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 99;
    DeRef(_1);

    /** 				deleted_routines += 1*/
    _56deleted_routines_43655 = _56deleted_routines_43655 + 1;
L1F: 
L1E: 

    /** 		sym[S_NREFS] = 0*/
    _2 = (int)SEQ_PTR(_sym_43661);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43661 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		if not equal(symo, sym) then*/
    if (_symo_43662 == _sym_43661)
    _23500 = 1;
    else if (IS_ATOM_INT(_symo_43662) && IS_ATOM_INT(_sym_43661))
    _23500 = 0;
    else
    _23500 = (compare(_symo_43662, _sym_43661) == 0);
    if (_23500 != 0)
    goto L20; // [888] 906
    _23500 = NOVALUE;

    /** 			SymTab[s] = sym*/
    RefDS(_sym_43661);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _s_43660);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_43661;
    DeRef(_1);

    /** 			updsym += 1*/
    _updsym_43658 = _updsym_43658 + 1;
L20: 

    /** 		s = sym[S_NEXT]*/
    _2 = (int)SEQ_PTR(_sym_43661);
    _s_43660 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_43660)){
        _s_43660 = (long)DBL_PTR(_s_43660)->dbl;
    }

    /** 	end while*/
    goto L1; // [918] 38
L2: 

    /** 	for i = 1 to length(temp_name_type) do*/
    if (IS_SEQUENCE(_25temp_name_type_12348)){
            _23504 = SEQ_PTR(_25temp_name_type_12348)->length;
    }
    else {
        _23504 = 1;
    }
    {
        int _i_43888;
        _i_43888 = 1;
L21: 
        if (_i_43888 > _23504){
            goto L22; // [928] 1061
        }

        /** 		integer upd = 0*/
        _upd_43891 = 0;

        /** 		if temp_name_type[i][T_GTYPE] != temp_name_type[i][T_GTYPE_NEW] then*/
        _2 = (int)SEQ_PTR(_25temp_name_type_12348);
        _23505 = (int)*(((s1_ptr)_2)->base + _i_43888);
        _2 = (int)SEQ_PTR(_23505);
        _23506 = (int)*(((s1_ptr)_2)->base + 1);
        _23505 = NOVALUE;
        _2 = (int)SEQ_PTR(_25temp_name_type_12348);
        _23507 = (int)*(((s1_ptr)_2)->base + _i_43888);
        _2 = (int)SEQ_PTR(_23507);
        _23508 = (int)*(((s1_ptr)_2)->base + 2);
        _23507 = NOVALUE;
        if (binary_op_a(EQUALS, _23506, _23508)){
            _23506 = NOVALUE;
            _23508 = NOVALUE;
            goto L23; // [966] 1003
        }
        _23506 = NOVALUE;
        _23508 = NOVALUE;

        /** 			temp_name_type[i][T_GTYPE] = temp_name_type[i][T_GTYPE_NEW]*/
        _2 = (int)SEQ_PTR(_25temp_name_type_12348);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _25temp_name_type_12348 = MAKE_SEQ(_2);
        }
        _3 = (int)(_i_43888 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_25temp_name_type_12348);
        _23512 = (int)*(((s1_ptr)_2)->base + _i_43888);
        _2 = (int)SEQ_PTR(_23512);
        _23513 = (int)*(((s1_ptr)_2)->base + 2);
        _23512 = NOVALUE;
        Ref(_23513);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 1);
        _1 = *(int *)_2;
        *(int *)_2 = _23513;
        if( _1 != _23513 ){
            DeRef(_1);
        }
        _23513 = NOVALUE;
        _23510 = NOVALUE;

        /** 			upd = 1*/
        _upd_43891 = 1;
L23: 

        /** 		if temp_name_type[i][T_GTYPE_NEW] != TYPE_NULL then*/
        _2 = (int)SEQ_PTR(_25temp_name_type_12348);
        _23514 = (int)*(((s1_ptr)_2)->base + _i_43888);
        _2 = (int)SEQ_PTR(_23514);
        _23515 = (int)*(((s1_ptr)_2)->base + 2);
        _23514 = NOVALUE;
        if (binary_op_a(EQUALS, _23515, 0)){
            _23515 = NOVALUE;
            goto L24; // [1019] 1046
        }
        _23515 = NOVALUE;

        /** 			temp_name_type[i][T_GTYPE_NEW] = TYPE_NULL*/
        _2 = (int)SEQ_PTR(_25temp_name_type_12348);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _25temp_name_type_12348 = MAKE_SEQ(_2);
        }
        _3 = (int)(_i_43888 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 2);
        _1 = *(int *)_2;
        *(int *)_2 = 0;
        DeRef(_1);
        _23517 = NOVALUE;

        /** 			upd = 1*/
        _upd_43891 = 1;
L24: 

        /** 		updsym += upd*/
        _updsym_43658 = _updsym_43658 + _upd_43891;

        /** 	end for*/
        _i_43888 = _i_43888 + 1;
        goto L21; // [1056] 935
L22: 
        ;
    }

    /** 	return updsym*/
    DeRef(_sym_43661);
    DeRef(_symo_43662);
    DeRef(_23423);
    _23423 = NOVALUE;
    DeRef(_23431);
    _23431 = NOVALUE;
    DeRef(_23434);
    _23434 = NOVALUE;
    DeRef(_23439);
    _23439 = NOVALUE;
    DeRef(_23442);
    _23442 = NOVALUE;
    DeRef(_23445);
    _23445 = NOVALUE;
    DeRef(_23448);
    _23448 = NOVALUE;
    DeRef(_23479);
    _23479 = NOVALUE;
    DeRef(_23458);
    _23458 = NOVALUE;
    DeRef(_23493);
    _23493 = NOVALUE;
    DeRef(_23485);
    _23485 = NOVALUE;
    return _updsym_43658;
    ;
}


void _56declare_prototype(int _s_43926)
{
    int _ret_type_43927 = NOVALUE;
    int _scope_43938 = NOVALUE;
    int _23537 = NOVALUE;
    int _23536 = NOVALUE;
    int _23534 = NOVALUE;
    int _23533 = NOVALUE;
    int _23532 = NOVALUE;
    int _23531 = NOVALUE;
    int _23529 = NOVALUE;
    int _23528 = NOVALUE;
    int _23527 = NOVALUE;
    int _23526 = NOVALUE;
    int _23525 = NOVALUE;
    int _23523 = NOVALUE;
    int _23522 = NOVALUE;
    int _23520 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sym_token( s ) = PROC then*/
    _23520 = _52sym_token(_s_43926);
    if (binary_op_a(NOTEQ, _23520, 27)){
        DeRef(_23520);
        _23520 = NOVALUE;
        goto L1; // [11] 25
    }
    DeRef(_23520);
    _23520 = NOVALUE;

    /** 		ret_type = "void "*/
    RefDS(_23405);
    DeRefi(_ret_type_43927);
    _ret_type_43927 = _23405;
    goto L2; // [22] 33
L1: 

    /** 		ret_type ="int "*/
    RefDS(_23406);
    DeRefi(_ret_type_43927);
    _ret_type_43927 = _23406;
L2: 

    /** 	c_hputs(ret_type)*/
    RefDS(_ret_type_43927);
    _53c_hputs(_ret_type_43927);

    /** 	if dll_option and TWINDOWS  then*/
    if (_56dll_option_42654 == 0) {
        goto L3; // [44] 116
    }
    if (_36TWINDOWS_14721 == 0)
    {
        goto L3; // [51] 116
    }
    else{
    }

    /** 		integer scope = SymTab[s][S_SCOPE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23523 = (int)*(((s1_ptr)_2)->base + _s_43926);
    _2 = (int)SEQ_PTR(_23523);
    _scope_43938 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_scope_43938)){
        _scope_43938 = (long)DBL_PTR(_scope_43938)->dbl;
    }
    _23523 = NOVALUE;

    /** 		if (scope = SC_PUBLIC*/
    _23525 = (_scope_43938 == 13);
    if (_23525 != 0) {
        _23526 = 1;
        goto L4; // [78] 92
    }
    _23527 = (_scope_43938 == 11);
    _23526 = (_23527 != 0);
L4: 
    if (_23526 != 0) {
        DeRef(_23528);
        _23528 = 1;
        goto L5; // [92] 106
    }
    _23529 = (_scope_43938 == 6);
    _23528 = (_23529 != 0);
L5: 
    if (_23528 == 0)
    {
        _23528 = NOVALUE;
        goto L6; // [106] 115
    }
    else{
        _23528 = NOVALUE;
    }

    /** 			c_hputs("__stdcall ")*/
    RefDS(_23530);
    _53c_hputs(_23530);
L6: 
L3: 

    /** 	c_hprintf("_%d", SymTab[s][S_FILE_NO])*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23531 = (int)*(((s1_ptr)_2)->base + _s_43926);
    _2 = (int)SEQ_PTR(_23531);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _23532 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _23532 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _23531 = NOVALUE;
    RefDS(_22775);
    Ref(_23532);
    _53c_hprintf(_22775, _23532);
    _23532 = NOVALUE;

    /** 	c_hputs(SymTab[s][S_NAME])*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23533 = (int)*(((s1_ptr)_2)->base + _s_43926);
    _2 = (int)SEQ_PTR(_23533);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _23534 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _23534 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _23533 = NOVALUE;
    Ref(_23534);
    _53c_hputs(_23534);
    _23534 = NOVALUE;

    /** 	c_hputs("(")*/
    RefDS(_23535);
    _53c_hputs(_23535);

    /** 	for i = 1 to SymTab[s][S_NUM_ARGS] do*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23536 = (int)*(((s1_ptr)_2)->base + _s_43926);
    _2 = (int)SEQ_PTR(_23536);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _23537 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _23537 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    _23536 = NOVALUE;
    {
        int _i_43967;
        _i_43967 = 1;
L7: 
        if (binary_op_a(GREATER, _i_43967, _23537)){
            goto L8; // [172] 206
        }

        /** 		if i = 1 then*/
        if (binary_op_a(NOTEQ, _i_43967, 1)){
            goto L9; // [181] 193
        }

        /** 			c_hputs("int")*/
        RefDS(_23539);
        _53c_hputs(_23539);
        goto LA; // [190] 199
L9: 

        /** 			c_hputs(", int")*/
        RefDS(_23540);
        _53c_hputs(_23540);
LA: 

        /** 	end for*/
        _0 = _i_43967;
        if (IS_ATOM_INT(_i_43967)) {
            _i_43967 = _i_43967 + 1;
            if ((long)((unsigned long)_i_43967 +(unsigned long) HIGH_BITS) >= 0){
                _i_43967 = NewDouble((double)_i_43967);
            }
        }
        else {
            _i_43967 = binary_op_a(PLUS, _i_43967, 1);
        }
        DeRef(_0);
        goto L7; // [201] 179
L8: 
        ;
        DeRef(_i_43967);
    }

    /** 	c_hputs(");\n")*/
    RefDS(_22944);
    _53c_hputs(_22944);

    /** end procedure*/
    DeRefi(_ret_type_43927);
    DeRef(_23525);
    _23525 = NOVALUE;
    DeRef(_23527);
    _23527 = NOVALUE;
    DeRef(_23529);
    _23529 = NOVALUE;
    _23537 = NOVALUE;
    return;
    ;
}


void _56add_to_routine_list(int _s_43983, int _seq_num_43984, int _first_43985)
{
    int _p_44060 = NOVALUE;
    int _23586 = NOVALUE;
    int _23584 = NOVALUE;
    int _23582 = NOVALUE;
    int _23580 = NOVALUE;
    int _23578 = NOVALUE;
    int _23577 = NOVALUE;
    int _23576 = NOVALUE;
    int _23574 = NOVALUE;
    int _23572 = NOVALUE;
    int _23570 = NOVALUE;
    int _23569 = NOVALUE;
    int _23567 = NOVALUE;
    int _23566 = NOVALUE;
    int _23562 = NOVALUE;
    int _23561 = NOVALUE;
    int _23560 = NOVALUE;
    int _23559 = NOVALUE;
    int _23558 = NOVALUE;
    int _23557 = NOVALUE;
    int _23556 = NOVALUE;
    int _23555 = NOVALUE;
    int _23554 = NOVALUE;
    int _23553 = NOVALUE;
    int _23551 = NOVALUE;
    int _23550 = NOVALUE;
    int _23549 = NOVALUE;
    int _23548 = NOVALUE;
    int _23545 = NOVALUE;
    int _23544 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	if not first then*/
    if (_first_43985 != 0)
    goto L1; // [9] 18

    /** 		c_puts(",\n")*/
    RefDS(_23542);
    _53c_puts(_23542);
L1: 

    /** 	c_puts("  {\"")*/
    RefDS(_23543);
    _53c_puts(_23543);

    /** 	c_puts(SymTab[s][S_NAME])*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23544 = (int)*(((s1_ptr)_2)->base + _s_43983);
    _2 = (int)SEQ_PTR(_23544);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _23545 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _23545 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _23544 = NOVALUE;
    Ref(_23545);
    _53c_puts(_23545);
    _23545 = NOVALUE;

    /** 	c_puts("\", ")*/
    RefDS(_23546);
    _53c_puts(_23546);

    /** 	c_puts("(int (*)())")*/
    RefDS(_23547);
    _53c_puts(_23547);

    /** 	c_printf("_%d", SymTab[s][S_FILE_NO])*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23548 = (int)*(((s1_ptr)_2)->base + _s_43983);
    _2 = (int)SEQ_PTR(_23548);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _23549 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _23549 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _23548 = NOVALUE;
    RefDS(_22775);
    Ref(_23549);
    _53c_printf(_22775, _23549);
    _23549 = NOVALUE;

    /** 	c_puts(SymTab[s][S_NAME])*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23550 = (int)*(((s1_ptr)_2)->base + _s_43983);
    _2 = (int)SEQ_PTR(_23550);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _23551 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _23551 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _23550 = NOVALUE;
    Ref(_23551);
    _53c_puts(_23551);
    _23551 = NOVALUE;

    /** 	c_printf(", %d", seq_num)*/
    RefDS(_23552);
    _53c_printf(_23552, _seq_num_43984);

    /** 	c_printf(", %d", SymTab[s][S_FILE_NO])*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23553 = (int)*(((s1_ptr)_2)->base + _s_43983);
    _2 = (int)SEQ_PTR(_23553);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _23554 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _23554 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _23553 = NOVALUE;
    RefDS(_23552);
    Ref(_23554);
    _53c_printf(_23552, _23554);
    _23554 = NOVALUE;

    /** 	c_printf(", %d", SymTab[s][S_NUM_ARGS])*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23555 = (int)*(((s1_ptr)_2)->base + _s_43983);
    _2 = (int)SEQ_PTR(_23555);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _23556 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _23556 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    _23555 = NOVALUE;
    RefDS(_23552);
    Ref(_23556);
    _53c_printf(_23552, _23556);
    _23556 = NOVALUE;

    /** 	if TWINDOWS and dll_option and find( SymTab[s][S_SCOPE], { SC_GLOBAL, SC_EXPORT, SC_PUBLIC} ) then*/
    if (_36TWINDOWS_14721 == 0) {
        _23557 = 0;
        goto L2; // [131] 141
    }
    _23557 = (_56dll_option_42654 != 0);
L2: 
    if (_23557 == 0) {
        goto L3; // [141] 186
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23559 = (int)*(((s1_ptr)_2)->base + _s_43983);
    _2 = (int)SEQ_PTR(_23559);
    _23560 = (int)*(((s1_ptr)_2)->base + 4);
    _23559 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 6;
    *((int *)(_2+8)) = 11;
    *((int *)(_2+12)) = 13;
    _23561 = MAKE_SEQ(_1);
    _23562 = find_from(_23560, _23561, 1);
    _23560 = NOVALUE;
    DeRefDS(_23561);
    _23561 = NOVALUE;
    if (_23562 == 0)
    {
        _23562 = NOVALUE;
        goto L3; // [175] 186
    }
    else{
        _23562 = NOVALUE;
    }

    /** 		c_puts(", 1")  -- must call with __stdcall convention*/
    RefDS(_23563);
    _53c_puts(_23563);
    goto L4; // [183] 192
L3: 

    /** 		c_puts(", 0")  -- default: call with normal or __cdecl convention*/
    RefDS(_23564);
    _53c_puts(_23564);
L4: 

    /** 	c_printf(", %d, 0", SymTab[s][S_SCOPE] )*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23566 = (int)*(((s1_ptr)_2)->base + _s_43983);
    _2 = (int)SEQ_PTR(_23566);
    _23567 = (int)*(((s1_ptr)_2)->base + 4);
    _23566 = NOVALUE;
    RefDS(_23565);
    Ref(_23567);
    _53c_printf(_23565, _23567);
    _23567 = NOVALUE;

    /** 	c_puts("}")*/
    RefDS(_23568);
    _53c_puts(_23568);

    /** 	if SymTab[s][S_NREFS] < 2 then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23569 = (int)*(((s1_ptr)_2)->base + _s_43983);
    _2 = (int)SEQ_PTR(_23569);
    _23570 = (int)*(((s1_ptr)_2)->base + 12);
    _23569 = NOVALUE;
    if (binary_op_a(GREATEREQ, _23570, 2)){
        _23570 = NOVALUE;
        goto L5; // [229] 249
    }
    _23570 = NOVALUE;

    /** 		SymTab[s][S_NREFS] = 2 --s->nrefs++*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_s_43983 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _23572 = NOVALUE;
L5: 

    /** 	symtab_index p = SymTab[s][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23574 = (int)*(((s1_ptr)_2)->base + _s_43983);
    _2 = (int)SEQ_PTR(_23574);
    _p_44060 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_p_44060)){
        _p_44060 = (long)DBL_PTR(_p_44060)->dbl;
    }
    _23574 = NOVALUE;

    /** 	for i = 1 to SymTab[s][S_NUM_ARGS] do*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23576 = (int)*(((s1_ptr)_2)->base + _s_43983);
    _2 = (int)SEQ_PTR(_23576);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _23577 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _23577 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    _23576 = NOVALUE;
    {
        int _i_44066;
        _i_44066 = 1;
L6: 
        if (binary_op_a(GREATER, _i_44066, _23577)){
            goto L7; // [279] 377
        }

        /** 		SymTab[p][S_ARG_SEQ_ELEM_NEW] = TYPE_OBJECT*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_p_44060 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 46);
        _1 = *(int *)_2;
        *(int *)_2 = 16;
        DeRef(_1);
        _23578 = NOVALUE;

        /** 		SymTab[p][S_ARG_TYPE_NEW] = TYPE_OBJECT*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_p_44060 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 44);
        _1 = *(int *)_2;
        *(int *)_2 = 16;
        DeRef(_1);
        _23580 = NOVALUE;

        /** 		SymTab[p][S_ARG_MIN_NEW] = NOVALUE*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_p_44060 + ((s1_ptr)_2)->base);
        Ref(_25NOVALUE_12115);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 49);
        _1 = *(int *)_2;
        *(int *)_2 = _25NOVALUE_12115;
        DeRef(_1);
        _23582 = NOVALUE;

        /** 		SymTab[p][S_ARG_SEQ_LEN_NEW] = NOVALUE*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_p_44060 + ((s1_ptr)_2)->base);
        Ref(_25NOVALUE_12115);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 52);
        _1 = *(int *)_2;
        *(int *)_2 = _25NOVALUE_12115;
        DeRef(_1);
        _23584 = NOVALUE;

        /** 		p = SymTab[p][S_NEXT]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _23586 = (int)*(((s1_ptr)_2)->base + _p_44060);
        _2 = (int)SEQ_PTR(_23586);
        _p_44060 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_p_44060)){
            _p_44060 = (long)DBL_PTR(_p_44060)->dbl;
        }
        _23586 = NOVALUE;

        /** 	end for*/
        _0 = _i_44066;
        if (IS_ATOM_INT(_i_44066)) {
            _i_44066 = _i_44066 + 1;
            if ((long)((unsigned long)_i_44066 +(unsigned long) HIGH_BITS) >= 0){
                _i_44066 = NewDouble((double)_i_44066);
            }
        }
        else {
            _i_44066 = binary_op_a(PLUS, _i_44066, 1);
        }
        DeRef(_0);
        goto L6; // [372] 286
L7: 
        ;
        DeRef(_i_44066);
    }

    /** end procedure*/
    _23577 = NOVALUE;
    return;
    ;
}


void _56DeclareRoutineList()
{
    int _s_44098 = NOVALUE;
    int _first_44099 = NOVALUE;
    int _seq_num_44100 = NOVALUE;
    int _these_routines_44108 = NOVALUE;
    int _these_routines_44130 = NOVALUE;
    int _23602 = NOVALUE;
    int _23601 = NOVALUE;
    int _23599 = NOVALUE;
    int _23597 = NOVALUE;
    int _23594 = NOVALUE;
    int _23593 = NOVALUE;
    int _23591 = NOVALUE;
    int _23589 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer first, seq_num*/

    /** 	c_hputs("extern struct routine_list _00[];\n")*/
    RefDS(_23588);
    _53c_hputs(_23588);

    /** 	check_file_routines()*/
    _56check_file_routines();

    /** 	for f = 1 to length( file_routines ) do*/
    if (IS_SEQUENCE(_56file_routines_44684)){
            _23589 = SEQ_PTR(_56file_routines_44684)->length;
    }
    else {
        _23589 = 1;
    }
    {
        int _f_44105;
        _f_44105 = 1;
L1: 
        if (_f_44105 > _23589){
            goto L2; // [19] 98
        }

        /** 		sequence these_routines = file_routines[f]*/
        DeRef(_these_routines_44108);
        _2 = (int)SEQ_PTR(_56file_routines_44684);
        _these_routines_44108 = (int)*(((s1_ptr)_2)->base + _f_44105);
        Ref(_these_routines_44108);

        /** 		for r = 1 to length( these_routines ) do*/
        if (IS_SEQUENCE(_these_routines_44108)){
                _23591 = SEQ_PTR(_these_routines_44108)->length;
        }
        else {
            _23591 = 1;
        }
        {
            int _r_44112;
            _r_44112 = 1;
L3: 
            if (_r_44112 > _23591){
                goto L4; // [41] 89
            }

            /** 			s = these_routines[r]*/
            _2 = (int)SEQ_PTR(_these_routines_44108);
            _s_44098 = (int)*(((s1_ptr)_2)->base + _r_44112);
            if (!IS_ATOM_INT(_s_44098)){
                _s_44098 = (long)DBL_PTR(_s_44098)->dbl;
            }

            /** 			if SymTab[s][S_USAGE] != U_DELETED then*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _23593 = (int)*(((s1_ptr)_2)->base + _s_44098);
            _2 = (int)SEQ_PTR(_23593);
            _23594 = (int)*(((s1_ptr)_2)->base + 5);
            _23593 = NOVALUE;
            if (binary_op_a(EQUALS, _23594, 99)){
                _23594 = NOVALUE;
                goto L5; // [72] 82
            }
            _23594 = NOVALUE;

            /** 				declare_prototype( s )*/
            _56declare_prototype(_s_44098);
L5: 

            /** 		end for*/
            _r_44112 = _r_44112 + 1;
            goto L3; // [84] 48
L4: 
            ;
        }
        DeRef(_these_routines_44108);
        _these_routines_44108 = NOVALUE;

        /** 	end for*/
        _f_44105 = _f_44105 + 1;
        goto L1; // [93] 26
L2: 
        ;
    }

    /** 	c_puts("\n")*/
    RefDS(_22834);
    _53c_puts(_22834);

    /** 	seq_num = 0*/
    _seq_num_44100 = 0;

    /** 	first = TRUE*/
    _first_44099 = _5TRUE_244;

    /** 	c_puts("struct routine_list _00[] = {\n")*/
    RefDS(_23596);
    _53c_puts(_23596);

    /** 	for f = 1 to length( file_routines ) do*/
    if (IS_SEQUENCE(_56file_routines_44684)){
            _23597 = SEQ_PTR(_56file_routines_44684)->length;
    }
    else {
        _23597 = 1;
    }
    {
        int _f_44127;
        _f_44127 = 1;
L6: 
        if (_f_44127 > _23597){
            goto L7; // [129] 222
        }

        /** 		sequence these_routines = file_routines[f]*/
        DeRef(_these_routines_44130);
        _2 = (int)SEQ_PTR(_56file_routines_44684);
        _these_routines_44130 = (int)*(((s1_ptr)_2)->base + _f_44127);
        Ref(_these_routines_44130);

        /** 		for r = 1 to length( these_routines ) do*/
        if (IS_SEQUENCE(_these_routines_44130)){
                _23599 = SEQ_PTR(_these_routines_44130)->length;
        }
        else {
            _23599 = 1;
        }
        {
            int _r_44134;
            _r_44134 = 1;
L8: 
            if (_r_44134 > _23599){
                goto L9; // [151] 213
            }

            /** 			s = these_routines[r]*/
            _2 = (int)SEQ_PTR(_these_routines_44130);
            _s_44098 = (int)*(((s1_ptr)_2)->base + _r_44134);
            if (!IS_ATOM_INT(_s_44098)){
                _s_44098 = (long)DBL_PTR(_s_44098)->dbl;
            }

            /** 			if SymTab[s][S_RI_TARGET] then*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _23601 = (int)*(((s1_ptr)_2)->base + _s_44098);
            _2 = (int)SEQ_PTR(_23601);
            _23602 = (int)*(((s1_ptr)_2)->base + 53);
            _23601 = NOVALUE;
            if (_23602 == 0) {
                _23602 = NOVALUE;
                goto LA; // [180] 200
            }
            else {
                if (!IS_ATOM_INT(_23602) && DBL_PTR(_23602)->dbl == 0.0){
                    _23602 = NOVALUE;
                    goto LA; // [180] 200
                }
                _23602 = NOVALUE;
            }
            _23602 = NOVALUE;

            /** 				add_to_routine_list( s, seq_num, first )*/
            _56add_to_routine_list(_s_44098, _seq_num_44100, _first_44099);

            /** 				first = FALSE*/
            _first_44099 = _5FALSE_242;
LA: 

            /** 			seq_num += 1*/
            _seq_num_44100 = _seq_num_44100 + 1;

            /** 		end for*/
            _r_44134 = _r_44134 + 1;
            goto L8; // [208] 158
L9: 
            ;
        }
        DeRef(_these_routines_44130);
        _these_routines_44130 = NOVALUE;

        /** 	end for*/
        _f_44127 = _f_44127 + 1;
        goto L6; // [217] 136
L7: 
        ;
    }

    /** 	if not first then*/
    if (_first_44099 != 0)
    goto LB; // [224] 233

    /** 		c_puts(",\n")*/
    RefDS(_23542);
    _53c_puts(_23542);
LB: 

    /** 	c_puts("  {\"\", 0, 999999999, 0, 0, 0, 0}\n};\n\n")  -- end marker*/
    RefDS(_23605);
    _53c_puts(_23605);

    /** 	c_hputs("extern unsigned char ** _02;\n")*/
    RefDS(_23606);
    _53c_hputs(_23606);

    /** 	c_puts("unsigned char ** _02;\n")*/
    RefDS(_23607);
    _53c_puts(_23607);

    /** 	c_hputs("extern object _0switches;\n")*/
    RefDS(_23608);
    _53c_hputs(_23608);

    /** 	c_puts("object _0switches;\n")*/
    RefDS(_23609);
    _53c_puts(_23609);

    /** end procedure*/
    return;
    ;
}


void _56DeclareNameSpaceList()
{
    int _s_44160 = NOVALUE;
    int _first_44161 = NOVALUE;
    int _seq_num_44162 = NOVALUE;
    int _23629 = NOVALUE;
    int _23627 = NOVALUE;
    int _23626 = NOVALUE;
    int _23625 = NOVALUE;
    int _23624 = NOVALUE;
    int _23622 = NOVALUE;
    int _23621 = NOVALUE;
    int _23618 = NOVALUE;
    int _23617 = NOVALUE;
    int _23616 = NOVALUE;
    int _23615 = NOVALUE;
    int _23614 = NOVALUE;
    int _23612 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer first, seq_num*/

    /** 	c_hputs("extern struct ns_list _01[];\n")*/
    RefDS(_23610);
    _53c_hputs(_23610);

    /** 	c_puts("struct ns_list _01[] = {\n")*/
    RefDS(_23611);
    _53c_puts(_23611);

    /** 	seq_num = 0*/
    _seq_num_44162 = 0;

    /** 	first = TRUE*/
    _first_44161 = _5TRUE_244;

    /** 	s = SymTab[TopLevelSub][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23612 = (int)*(((s1_ptr)_2)->base + _25TopLevelSub_12269);
    _2 = (int)SEQ_PTR(_23612);
    _s_44160 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_44160)){
        _s_44160 = (long)DBL_PTR(_s_44160)->dbl;
    }
    _23612 = NOVALUE;

    /** 	while s do*/
L1: 
    if (_s_44160 == 0)
    {
        goto L2; // [50] 215
    }
    else{
    }

    /** 		if find(SymTab[s][S_TOKEN], NAMED_TOKS) then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23614 = (int)*(((s1_ptr)_2)->base + _s_44160);
    _2 = (int)SEQ_PTR(_23614);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _23615 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _23615 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _23614 = NOVALUE;
    _23616 = find_from(_23615, _28NAMED_TOKS_11859, 1);
    _23615 = NOVALUE;
    if (_23616 == 0)
    {
        _23616 = NOVALUE;
        goto L3; // [74] 194
    }
    else{
        _23616 = NOVALUE;
    }

    /** 			if SymTab[s][S_TOKEN] = NAMESPACE then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23617 = (int)*(((s1_ptr)_2)->base + _s_44160);
    _2 = (int)SEQ_PTR(_23617);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _23618 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _23618 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _23617 = NOVALUE;
    if (binary_op_a(NOTEQ, _23618, 523)){
        _23618 = NOVALUE;
        goto L4; // [93] 187
    }
    _23618 = NOVALUE;

    /** 				if not first then*/
    if (_first_44161 != 0)
    goto L5; // [99] 108

    /** 					c_puts(",\n")*/
    RefDS(_23542);
    _53c_puts(_23542);
L5: 

    /** 				first = FALSE*/
    _first_44161 = _5FALSE_242;

    /** 				c_puts("  {\"")*/
    RefDS(_23543);
    _53c_puts(_23543);

    /** 				c_puts(SymTab[s][S_NAME])*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23621 = (int)*(((s1_ptr)_2)->base + _s_44160);
    _2 = (int)SEQ_PTR(_23621);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _23622 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _23622 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _23621 = NOVALUE;
    Ref(_23622);
    _53c_puts(_23622);
    _23622 = NOVALUE;

    /** 				c_printf("\", %d", SymTab[s][S_OBJ])*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23624 = (int)*(((s1_ptr)_2)->base + _s_44160);
    _2 = (int)SEQ_PTR(_23624);
    _23625 = (int)*(((s1_ptr)_2)->base + 1);
    _23624 = NOVALUE;
    RefDS(_23623);
    Ref(_23625);
    _53c_printf(_23623, _23625);
    _23625 = NOVALUE;

    /** 				c_printf(", %d", seq_num)*/
    RefDS(_23552);
    _53c_printf(_23552, _seq_num_44162);

    /** 				c_printf(", %d", SymTab[s][S_FILE_NO])*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23626 = (int)*(((s1_ptr)_2)->base + _s_44160);
    _2 = (int)SEQ_PTR(_23626);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _23627 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _23627 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _23626 = NOVALUE;
    RefDS(_23552);
    Ref(_23627);
    _53c_printf(_23552, _23627);
    _23627 = NOVALUE;

    /** 				c_puts("}")*/
    RefDS(_23568);
    _53c_puts(_23568);
L4: 

    /** 			seq_num += 1*/
    _seq_num_44162 = _seq_num_44162 + 1;
L3: 

    /** 		s = SymTab[s][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23629 = (int)*(((s1_ptr)_2)->base + _s_44160);
    _2 = (int)SEQ_PTR(_23629);
    _s_44160 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_44160)){
        _s_44160 = (long)DBL_PTR(_s_44160)->dbl;
    }
    _23629 = NOVALUE;

    /** 	end while*/
    goto L1; // [212] 50
L2: 

    /** 	if not first then*/
    if (_first_44161 != 0)
    goto L6; // [217] 226

    /** 		c_puts(",\n")*/
    RefDS(_23542);
    _53c_puts(_23542);
L6: 

    /** 	c_puts("  {\"\", 0, 999999999, 0}\n};\n\n")  -- end marker*/
    RefDS(_23632);
    _53c_puts(_23632);

    /** end procedure*/
    return;
    ;
}


int _56is_exported(int _s_44224)
{
    int _eentry_44225 = NOVALUE;
    int _scope_44228 = NOVALUE;
    int _23647 = NOVALUE;
    int _23646 = NOVALUE;
    int _23645 = NOVALUE;
    int _23644 = NOVALUE;
    int _23643 = NOVALUE;
    int _23642 = NOVALUE;
    int _23641 = NOVALUE;
    int _23640 = NOVALUE;
    int _23639 = NOVALUE;
    int _23638 = NOVALUE;
    int _23637 = NOVALUE;
    int _23635 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_s_44224)) {
        _1 = (long)(DBL_PTR(_s_44224)->dbl);
        if (UNIQUE(DBL_PTR(_s_44224)) && (DBL_PTR(_s_44224)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_44224);
        _s_44224 = _1;
    }

    /** 	sequence eentry = SymTab[s]*/
    DeRef(_eentry_44225);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _eentry_44225 = (int)*(((s1_ptr)_2)->base + _s_44224);
    Ref(_eentry_44225);

    /** 	integer scope = eentry[S_SCOPE]*/
    _2 = (int)SEQ_PTR(_eentry_44225);
    _scope_44228 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_scope_44228))
    _scope_44228 = (long)DBL_PTR(_scope_44228)->dbl;

    /** 	if eentry[S_MODE] = M_NORMAL then*/
    _2 = (int)SEQ_PTR(_eentry_44225);
    _23635 = (int)*(((s1_ptr)_2)->base + 3);
    if (binary_op_a(NOTEQ, _23635, 1)){
        _23635 = NOVALUE;
        goto L1; // [31] 125
    }
    _23635 = NOVALUE;

    /** 		if eentry[S_FILE_NO] = 1 and find(scope, { SC_EXPORT, SC_PUBLIC, SC_GLOBAL }) then*/
    _2 = (int)SEQ_PTR(_eentry_44225);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _23637 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _23637 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    if (IS_ATOM_INT(_23637)) {
        _23638 = (_23637 == 1);
    }
    else {
        _23638 = binary_op(EQUALS, _23637, 1);
    }
    _23637 = NOVALUE;
    if (IS_ATOM_INT(_23638)) {
        if (_23638 == 0) {
            goto L2; // [47] 79
        }
    }
    else {
        if (DBL_PTR(_23638)->dbl == 0.0) {
            goto L2; // [47] 79
        }
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 11;
    *((int *)(_2+8)) = 13;
    *((int *)(_2+12)) = 6;
    _23640 = MAKE_SEQ(_1);
    _23641 = find_from(_scope_44228, _23640, 1);
    DeRefDS(_23640);
    _23640 = NOVALUE;
    if (_23641 == 0)
    {
        _23641 = NOVALUE;
        goto L2; // [69] 79
    }
    else{
        _23641 = NOVALUE;
    }

    /** 			return 1*/
    DeRef(_eentry_44225);
    DeRef(_23638);
    _23638 = NOVALUE;
    return 1;
L2: 

    /** 		if scope = SC_PUBLIC and*/
    _23642 = (_scope_44228 == 13);
    if (_23642 == 0) {
        goto L3; // [87] 124
    }
    _2 = (int)SEQ_PTR(_26include_matrix_11145);
    _23644 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_eentry_44225);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _23645 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _23645 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _2 = (int)SEQ_PTR(_23644);
    if (!IS_ATOM_INT(_23645)){
        _23646 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_23645)->dbl));
    }
    else{
        _23646 = (int)*(((s1_ptr)_2)->base + _23645);
    }
    _23644 = NOVALUE;
    if (IS_ATOM_INT(_23646)) {
        {unsigned long tu;
             tu = (unsigned long)_23646 & (unsigned long)4;
             _23647 = MAKE_UINT(tu);
        }
    }
    else {
        _23647 = binary_op(AND_BITS, _23646, 4);
    }
    _23646 = NOVALUE;
    if (_23647 == 0) {
        DeRef(_23647);
        _23647 = NOVALUE;
        goto L3; // [114] 124
    }
    else {
        if (!IS_ATOM_INT(_23647) && DBL_PTR(_23647)->dbl == 0.0){
            DeRef(_23647);
            _23647 = NOVALUE;
            goto L3; // [114] 124
        }
        DeRef(_23647);
        _23647 = NOVALUE;
    }
    DeRef(_23647);
    _23647 = NOVALUE;

    /** 			return 1*/
    DeRef(_eentry_44225);
    DeRef(_23642);
    _23642 = NOVALUE;
    DeRef(_23638);
    _23638 = NOVALUE;
    _23645 = NOVALUE;
    return 1;
L3: 
L1: 

    /** 	return 0*/
    DeRef(_eentry_44225);
    DeRef(_23642);
    _23642 = NOVALUE;
    DeRef(_23638);
    _23638 = NOVALUE;
    _23645 = NOVALUE;
    return 0;
    ;
}


void _56version()
{
    int _23681 = NOVALUE;
    int _23680 = NOVALUE;
    int _0, _1, _2;
    

    /** 	c_puts("// Euphoria To C version " & version_string() & "\n")*/
    _23680 = _31version_string(0);
    {
        int concat_list[3];

        concat_list[0] = _22834;
        concat_list[1] = _23680;
        concat_list[2] = _23679;
        Concat_N((object_ptr)&_23681, concat_list, 3);
    }
    DeRef(_23680);
    _23680 = NOVALUE;
    _53c_puts(_23681);
    _23681 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


void _56new_c_file(int _name_44332)
{
    int _23684 = NOVALUE;
    int _0, _1, _2;
    

    /** 	cfile_size = 0*/
    _25cfile_size_12345 = 0;

    /** 	if LAST_PASS = FALSE then*/
    if (_56LAST_PASS_42636 != _5FALSE_242)
    goto L1; // [16] 26

    /** 		return*/
    DeRefDS(_name_44332);
    return;
L1: 

    /** 	write_checksum( c_code )*/
    _54write_checksum(_53c_code_46491);

    /** 	close(c_code)*/
    EClose(_53c_code_46491);

    /** 	c_code = open(output_dir & name & ".c", "w")*/
    {
        int concat_list[3];

        concat_list[0] = _23683;
        concat_list[1] = _name_44332;
        concat_list[2] = _56output_dir_42676;
        Concat_N((object_ptr)&_23684, concat_list, 3);
    }
    _53c_code_46491 = EOpen(_23684, _22788, 0);
    DeRefDS(_23684);
    _23684 = NOVALUE;

    /** 	if c_code = -1 then*/
    if (_53c_code_46491 != -1)
    goto L2; // [60] 72

    /** 		CompileErr(57)*/
    RefDS(_22682);
    _43CompileErr(57, _22682, 0);
L2: 

    /** 	cfile_count += 1*/
    _25cfile_count_12344 = _25cfile_count_12344 + 1;

    /** 	version()*/
    _56version();

    /** 	c_puts("#include \"include/euphoria.h\"\n")*/
    RefDS(_22792);
    _53c_puts(_22792);

    /** 	c_puts("#include \"main-.h\"\n\n")*/
    RefDS(_22793);
    _53c_puts(_22793);

    /** 	if not TUNIX then*/
    if (_36TUNIX_14725 != 0)
    goto L3; // [100] 112

    /** 		name = lower(name)  -- for faster compare later*/
    RefDS(_name_44332);
    _0 = _name_44332;
    _name_44332 = _4lower(_name_44332);
    DeRefDS(_0);
L3: 

    /** end procedure*/
    DeRefDS(_name_44332);
    return;
    ;
}


int _56unique_c_name(int _name_44361)
{
    int _i_44362 = NOVALUE;
    int _compare_name_44363 = NOVALUE;
    int _next_fc_44364 = NOVALUE;
    int _23700 = NOVALUE;
    int _23698 = NOVALUE;
    int _23697 = NOVALUE;
    int _23696 = NOVALUE;
    int _23694 = NOVALUE;
    int _0, _1, _2;
    

    /** 	compare_name = name & ".c"*/
    Concat((object_ptr)&_compare_name_44363, _name_44361, _23683);

    /** 	if not TUNIX then*/
    if (_36TUNIX_14725 != 0)
    goto L1; // [13] 25

    /** 		compare_name = lower(compare_name)*/
    RefDS(_compare_name_44363);
    _0 = _compare_name_44363;
    _compare_name_44363 = _4lower(_compare_name_44363);
    DeRefDS(_0);
L1: 

    /** 	next_fc = 1*/
    _next_fc_44364 = 1;

    /** 	i = 1*/
    _i_44362 = 1;

    /** 	while i <= length(generated_files) do*/
L2: 
    if (IS_SEQUENCE(_56generated_files_42658)){
            _23694 = SEQ_PTR(_56generated_files_42658)->length;
    }
    else {
        _23694 = 1;
    }
    if (_i_44362 > _23694)
    goto L3; // [45] 139

    /** 		if equal(generated_files[i], compare_name) then*/
    _2 = (int)SEQ_PTR(_56generated_files_42658);
    _23696 = (int)*(((s1_ptr)_2)->base + _i_44362);
    if (_23696 == _compare_name_44363)
    _23697 = 1;
    else if (IS_ATOM_INT(_23696) && IS_ATOM_INT(_compare_name_44363))
    _23697 = 0;
    else
    _23697 = (compare(_23696, _compare_name_44363) == 0);
    _23696 = NOVALUE;
    if (_23697 == 0)
    {
        _23697 = NOVALUE;
        goto L4; // [61] 127
    }
    else{
        _23697 = NOVALUE;
    }

    /** 			if next_fc > length(file_chars) then*/
    if (IS_SEQUENCE(_56file_chars_44357)){
            _23698 = SEQ_PTR(_56file_chars_44357)->length;
    }
    else {
        _23698 = 1;
    }
    if (_next_fc_44364 <= _23698)
    goto L5; // [69] 81

    /** 				CompileErr(140)*/
    RefDS(_22682);
    _43CompileErr(140, _22682, 0);
L5: 

    /** 			name[1] = file_chars[next_fc]*/
    _2 = (int)SEQ_PTR(_56file_chars_44357);
    _23700 = (int)*(((s1_ptr)_2)->base + _next_fc_44364);
    Ref(_23700);
    _2 = (int)SEQ_PTR(_name_44361);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _name_44361 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _23700;
    if( _1 != _23700 ){
        DeRef(_1);
    }
    _23700 = NOVALUE;

    /** 			compare_name = name & ".c"*/
    Concat((object_ptr)&_compare_name_44363, _name_44361, _23683);

    /** 			if not TUNIX then*/
    if (_36TUNIX_14725 != 0)
    goto L6; // [101] 113

    /** 				compare_name = lower(compare_name)*/
    RefDS(_compare_name_44363);
    _0 = _compare_name_44363;
    _compare_name_44363 = _4lower(_compare_name_44363);
    DeRefDS(_0);
L6: 

    /** 			next_fc += 1*/
    _next_fc_44364 = _next_fc_44364 + 1;

    /** 			i = 1 -- start over and compare again*/
    _i_44362 = 1;
    goto L2; // [124] 40
L4: 

    /** 			i += 1*/
    _i_44362 = _i_44362 + 1;

    /** 	end while*/
    goto L2; // [136] 40
L3: 

    /** 	return name*/
    DeRef(_compare_name_44363);
    return _name_44361;
    ;
}


int _56is_file_newer(int _f1_44393, int _f2_44394)
{
    int _d1_44395 = NOVALUE;
    int _d2_44398 = NOVALUE;
    int _diff_2__tmp_at42_44409 = NOVALUE;
    int _diff_1__tmp_at42_44408 = NOVALUE;
    int _diff_inlined_diff_at_42_44407 = NOVALUE;
    int _23710 = NOVALUE;
    int _23708 = NOVALUE;
    int _0, _1, _2;
    

    /** 	object d1 = file_timestamp(f1)*/
    RefDS(_f1_44393);
    _0 = _d1_44395;
    _d1_44395 = _9file_timestamp(_f1_44393);
    DeRef(_0);

    /** 	object d2 = file_timestamp(f2)*/
    RefDS(_f2_44394);
    _0 = _d2_44398;
    _d2_44398 = _9file_timestamp(_f2_44394);
    DeRef(_0);

    /** 	if atom(d1) or atom(d2) then return 1 end if*/
    _23708 = IS_ATOM(_d1_44395);
    if (_23708 != 0) {
        goto L1; // [22] 34
    }
    _23710 = IS_ATOM(_d2_44398);
    if (_23710 == 0)
    {
        _23710 = NOVALUE;
        goto L2; // [30] 39
    }
    else{
        _23710 = NOVALUE;
    }
L1: 
    DeRefDS(_f1_44393);
    DeRefDS(_f2_44394);
    DeRef(_d1_44395);
    DeRef(_d2_44398);
    return 1;
L2: 

    /** 	if datetime:diff(d1, d2) < 0 then*/

    /** 	return datetimeToSeconds(dt2) - datetimeToSeconds(dt1)*/
    Ref(_d2_44398);
    _0 = _diff_1__tmp_at42_44408;
    _diff_1__tmp_at42_44408 = _10datetimeToSeconds(_d2_44398);
    DeRef(_0);
    Ref(_d1_44395);
    _0 = _diff_2__tmp_at42_44409;
    _diff_2__tmp_at42_44409 = _10datetimeToSeconds(_d1_44395);
    DeRef(_0);
    DeRef(_diff_inlined_diff_at_42_44407);
    if (IS_ATOM_INT(_diff_1__tmp_at42_44408) && IS_ATOM_INT(_diff_2__tmp_at42_44409)) {
        _diff_inlined_diff_at_42_44407 = _diff_1__tmp_at42_44408 - _diff_2__tmp_at42_44409;
        if ((long)((unsigned long)_diff_inlined_diff_at_42_44407 +(unsigned long) HIGH_BITS) >= 0){
            _diff_inlined_diff_at_42_44407 = NewDouble((double)_diff_inlined_diff_at_42_44407);
        }
    }
    else {
        _diff_inlined_diff_at_42_44407 = binary_op(MINUS, _diff_1__tmp_at42_44408, _diff_2__tmp_at42_44409);
    }
    DeRef(_diff_1__tmp_at42_44408);
    _diff_1__tmp_at42_44408 = NOVALUE;
    DeRef(_diff_2__tmp_at42_44409);
    _diff_2__tmp_at42_44409 = NOVALUE;
    if (binary_op_a(GREATEREQ, _diff_inlined_diff_at_42_44407, 0)){
        goto L3; // [58] 69
    }

    /** 		return 1*/
    DeRefDS(_f1_44393);
    DeRefDS(_f2_44394);
    DeRef(_d1_44395);
    DeRef(_d2_44398);
    return 1;
L3: 

    /** 	return 0*/
    DeRefDS(_f1_44393);
    DeRefDS(_f2_44394);
    DeRef(_d1_44395);
    DeRef(_d2_44398);
    return 0;
    ;
}


void _56add_file(int _filename_44413, int _eu_filename_44414)
{
    int _obj_fname_44434 = NOVALUE;
    int _src_fname_44435 = NOVALUE;
    int _23734 = NOVALUE;
    int _23733 = NOVALUE;
    int _23720 = NOVALUE;
    int _23719 = NOVALUE;
    int _23716 = NOVALUE;
    int _23715 = NOVALUE;
    int _23714 = NOVALUE;
    int _23713 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if equal("c", fileext(filename)) then*/
    RefDS(_filename_44413);
    _23713 = _9fileext(_filename_44413);
    if (_23712 == _23713)
    _23714 = 1;
    else if (IS_ATOM_INT(_23712) && IS_ATOM_INT(_23713))
    _23714 = 0;
    else
    _23714 = (compare(_23712, _23713) == 0);
    DeRef(_23713);
    _23713 = NOVALUE;
    if (_23714 == 0)
    {
        _23714 = NOVALUE;
        goto L1; // [15] 35
    }
    else{
        _23714 = NOVALUE;
    }

    /** 		filename = filename[1..$-2]*/
    if (IS_SEQUENCE(_filename_44413)){
            _23715 = SEQ_PTR(_filename_44413)->length;
    }
    else {
        _23715 = 1;
    }
    _23716 = _23715 - 2;
    _23715 = NOVALUE;
    rhs_slice_target = (object_ptr)&_filename_44413;
    RHS_Slice(_filename_44413, 1, _23716);
    goto L2; // [32] 82
L1: 

    /** 	elsif equal("h", fileext(filename)) then*/
    RefDS(_filename_44413);
    _23719 = _9fileext(_filename_44413);
    if (_23718 == _23719)
    _23720 = 1;
    else if (IS_ATOM_INT(_23718) && IS_ATOM_INT(_23719))
    _23720 = 0;
    else
    _23720 = (compare(_23718, _23719) == 0);
    DeRef(_23719);
    _23719 = NOVALUE;
    if (_23720 == 0)
    {
        _23720 = NOVALUE;
        goto L3; // [45] 81
    }
    else{
        _23720 = NOVALUE;
    }

    /** 		generated_files = append(generated_files, filename)*/
    RefDS(_filename_44413);
    Append(&_56generated_files_42658, _56generated_files_42658, _filename_44413);

    /** 		if build_system_type = BUILD_DIRECT then*/
    if (_54build_system_type_45311 != 3)
    goto L4; // [62] 75

    /** 			outdated_files  = append(outdated_files, 0)*/
    Append(&_56outdated_files_42659, _56outdated_files_42659, 0);
L4: 

    /** 		return*/
    DeRefDS(_filename_44413);
    DeRefDS(_eu_filename_44414);
    DeRef(_obj_fname_44434);
    DeRef(_src_fname_44435);
    DeRef(_23716);
    _23716 = NOVALUE;
    return;
L3: 
L2: 

    /** 	sequence obj_fname = filename, src_fname = filename & ".c"*/
    RefDS(_filename_44413);
    DeRef(_obj_fname_44434);
    _obj_fname_44434 = _filename_44413;
    Concat((object_ptr)&_src_fname_44435, _filename_44413, _23683);

    /** 	if compiler_type = COMPILER_WATCOM then*/
    if (_54compiler_type_45317 != 2)
    goto L5; // [99] 112

    /** 		obj_fname &= ".obj"*/
    Concat((object_ptr)&_obj_fname_44434, _obj_fname_44434, _23726);
    goto L6; // [109] 119
L5: 

    /** 		obj_fname &= ".o"*/
    Concat((object_ptr)&_obj_fname_44434, _obj_fname_44434, _23728);
L6: 

    /** 	generated_files = append(generated_files, src_fname)*/
    RefDS(_src_fname_44435);
    Append(&_56generated_files_42658, _56generated_files_42658, _src_fname_44435);

    /** 	generated_files = append(generated_files, obj_fname)*/
    RefDS(_obj_fname_44434);
    Append(&_56generated_files_42658, _56generated_files_42658, _obj_fname_44434);

    /** 	if build_system_type = BUILD_DIRECT then*/
    if (_54build_system_type_45311 != 3)
    goto L7; // [141] 173

    /** 		outdated_files  = append(outdated_files, is_file_newer(eu_filename, output_dir & src_fname))*/
    Concat((object_ptr)&_23733, _56output_dir_42676, _src_fname_44435);
    RefDS(_eu_filename_44414);
    _23734 = _56is_file_newer(_eu_filename_44414, _23733);
    _23733 = NOVALUE;
    Ref(_23734);
    Append(&_56outdated_files_42659, _56outdated_files_42659, _23734);
    DeRef(_23734);
    _23734 = NOVALUE;

    /** 		outdated_files  = append(outdated_files, 0)*/
    Append(&_56outdated_files_42659, _56outdated_files_42659, 0);
L7: 

    /** end procedure*/
    DeRefDS(_filename_44413);
    DeRefDS(_eu_filename_44414);
    DeRef(_obj_fname_44434);
    DeRef(_src_fname_44435);
    DeRef(_23716);
    _23716 = NOVALUE;
    return;
    ;
}


int _56any_code(int _file_no_44458)
{
    int _these_routines_44460 = NOVALUE;
    int _s_44467 = NOVALUE;
    int _23750 = NOVALUE;
    int _23749 = NOVALUE;
    int _23748 = NOVALUE;
    int _23747 = NOVALUE;
    int _23746 = NOVALUE;
    int _23745 = NOVALUE;
    int _23744 = NOVALUE;
    int _23743 = NOVALUE;
    int _23742 = NOVALUE;
    int _23741 = NOVALUE;
    int _23740 = NOVALUE;
    int _23738 = NOVALUE;
    int _0, _1, _2;
    

    /** 	check_file_routines()*/
    _56check_file_routines();

    /** 	sequence these_routines = file_routines[file_no]*/
    DeRef(_these_routines_44460);
    _2 = (int)SEQ_PTR(_56file_routines_44684);
    _these_routines_44460 = (int)*(((s1_ptr)_2)->base + _file_no_44458);
    Ref(_these_routines_44460);

    /** 	for i = 1 to length( these_routines ) do*/
    if (IS_SEQUENCE(_these_routines_44460)){
            _23738 = SEQ_PTR(_these_routines_44460)->length;
    }
    else {
        _23738 = 1;
    }
    {
        int _i_44464;
        _i_44464 = 1;
L1: 
        if (_i_44464 > _23738){
            goto L2; // [22] 126
        }

        /** 		symtab_index s = these_routines[i]*/
        _2 = (int)SEQ_PTR(_these_routines_44460);
        _s_44467 = (int)*(((s1_ptr)_2)->base + _i_44464);
        if (!IS_ATOM_INT(_s_44467)){
            _s_44467 = (long)DBL_PTR(_s_44467)->dbl;
        }

        /** 		if SymTab[s][S_FILE_NO] = file_no and*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _23740 = (int)*(((s1_ptr)_2)->base + _s_44467);
        _2 = (int)SEQ_PTR(_23740);
        if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
            _23741 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
        }
        else{
            _23741 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
        }
        _23740 = NOVALUE;
        if (IS_ATOM_INT(_23741)) {
            _23742 = (_23741 == _file_no_44458);
        }
        else {
            _23742 = binary_op(EQUALS, _23741, _file_no_44458);
        }
        _23741 = NOVALUE;
        if (IS_ATOM_INT(_23742)) {
            if (_23742 == 0) {
                DeRef(_23743);
                _23743 = 0;
                goto L3; // [55] 81
            }
        }
        else {
            if (DBL_PTR(_23742)->dbl == 0.0) {
                DeRef(_23743);
                _23743 = 0;
                goto L3; // [55] 81
            }
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _23744 = (int)*(((s1_ptr)_2)->base + _s_44467);
        _2 = (int)SEQ_PTR(_23744);
        _23745 = (int)*(((s1_ptr)_2)->base + 5);
        _23744 = NOVALUE;
        if (IS_ATOM_INT(_23745)) {
            _23746 = (_23745 != 99);
        }
        else {
            _23746 = binary_op(NOTEQ, _23745, 99);
        }
        _23745 = NOVALUE;
        DeRef(_23743);
        if (IS_ATOM_INT(_23746))
        _23743 = (_23746 != 0);
        else
        _23743 = DBL_PTR(_23746)->dbl != 0.0;
L3: 
        if (_23743 == 0) {
            goto L4; // [81] 117
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _23748 = (int)*(((s1_ptr)_2)->base + _s_44467);
        _2 = (int)SEQ_PTR(_23748);
        if (!IS_ATOM_INT(_25S_TOKEN_11918)){
            _23749 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
        }
        else{
            _23749 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
        }
        _23748 = NOVALUE;
        _23750 = find_from(_23749, _28RTN_TOKS_11857, 1);
        _23749 = NOVALUE;
        if (_23750 == 0)
        {
            _23750 = NOVALUE;
            goto L4; // [105] 117
        }
        else{
            _23750 = NOVALUE;
        }

        /** 			return TRUE -- found a non-deleted routine in this file*/
        DeRef(_these_routines_44460);
        DeRef(_23742);
        _23742 = NOVALUE;
        DeRef(_23746);
        _23746 = NOVALUE;
        return _5TRUE_244;
L4: 

        /** 	end for*/
        _i_44464 = _i_44464 + 1;
        goto L1; // [121] 29
L2: 
        ;
    }

    /** 	return FALSE*/
    DeRef(_these_routines_44460);
    DeRef(_23742);
    _23742 = NOVALUE;
    DeRef(_23746);
    _23746 = NOVALUE;
    return _5FALSE_242;
    ;
}


int _56legaldos_filename_char(int _i_44494)
{
    int _23765 = NOVALUE;
    int _23764 = NOVALUE;
    int _23761 = NOVALUE;
    int _23760 = NOVALUE;
    int _23759 = NOVALUE;
    int _23758 = NOVALUE;
    int _23757 = NOVALUE;
    int _23756 = NOVALUE;
    int _23755 = NOVALUE;
    int _23754 = NOVALUE;
    int _23753 = NOVALUE;
    int _23752 = NOVALUE;
    int _23751 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_i_44494)) {
        _1 = (long)(DBL_PTR(_i_44494)->dbl);
        if (UNIQUE(DBL_PTR(_i_44494)) && (DBL_PTR(_i_44494)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_44494);
        _i_44494 = _1;
    }

    /** 	if ('A' <= i and i <= 'Z') or*/
    _23751 = (65 <= _i_44494);
    if (_23751 == 0) {
        _23752 = 0;
        goto L1; // [9] 21
    }
    _23753 = (_i_44494 <= 90);
    _23752 = (_23753 != 0);
L1: 
    if (_23752 != 0) {
        _23754 = 1;
        goto L2; // [21] 45
    }
    _23755 = (48 <= _i_44494);
    if (_23755 == 0) {
        _23756 = 0;
        goto L3; // [29] 41
    }
    _23757 = (_i_44494 <= 57);
    _23756 = (_23757 != 0);
L3: 
    _23754 = (_23756 != 0);
L2: 
    if (_23754 != 0) {
        _23758 = 1;
        goto L4; // [45] 69
    }
    _23759 = (128 <= _i_44494);
    if (_23759 == 0) {
        _23760 = 0;
        goto L5; // [53] 65
    }
    _23761 = (_i_44494 <= 255);
    _23760 = (_23761 != 0);
L5: 
    _23758 = (_23760 != 0);
L4: 
    if (_23758 != 0) {
        goto L6; // [69] 87
    }
    _23764 = find_from(_i_44494, _23763, 1);
    _23765 = (_23764 != 0);
    _23764 = NOVALUE;
    if (_23765 == 0)
    {
        DeRef(_23765);
        _23765 = NOVALUE;
        goto L7; // [83] 96
    }
    else{
        DeRef(_23765);
        _23765 = NOVALUE;
    }
L6: 

    /** 		return 1*/
    DeRef(_23751);
    _23751 = NOVALUE;
    DeRef(_23753);
    _23753 = NOVALUE;
    DeRef(_23755);
    _23755 = NOVALUE;
    DeRef(_23757);
    _23757 = NOVALUE;
    DeRef(_23759);
    _23759 = NOVALUE;
    DeRef(_23761);
    _23761 = NOVALUE;
    return 1;
    goto L8; // [93] 103
L7: 

    /** 		return 0*/
    DeRef(_23751);
    _23751 = NOVALUE;
    DeRef(_23753);
    _23753 = NOVALUE;
    DeRef(_23755);
    _23755 = NOVALUE;
    DeRef(_23757);
    _23757 = NOVALUE;
    DeRef(_23759);
    _23759 = NOVALUE;
    DeRef(_23761);
    _23761 = NOVALUE;
    return 0;
L8: 
    ;
}


int _56legaldos_filename(int _s_44514)
{
    int _dloc_44515 = NOVALUE;
    int _23792 = NOVALUE;
    int _23791 = NOVALUE;
    int _23789 = NOVALUE;
    int _23788 = NOVALUE;
    int _23787 = NOVALUE;
    int _23786 = NOVALUE;
    int _23784 = NOVALUE;
    int _23783 = NOVALUE;
    int _23782 = NOVALUE;
    int _23781 = NOVALUE;
    int _23780 = NOVALUE;
    int _23779 = NOVALUE;
    int _23777 = NOVALUE;
    int _23776 = NOVALUE;
    int _23775 = NOVALUE;
    int _23774 = NOVALUE;
    int _23773 = NOVALUE;
    int _23772 = NOVALUE;
    int _23771 = NOVALUE;
    int _23769 = NOVALUE;
    int _23768 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if find( s, { "..", "." } ) then*/
    RefDS(_23767);
    RefDS(_23766);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _23766;
    ((int *)_2)[2] = _23767;
    _23768 = MAKE_SEQ(_1);
    _23769 = find_from(_s_44514, _23768, 1);
    DeRefDS(_23768);
    _23768 = NOVALUE;
    if (_23769 == 0)
    {
        _23769 = NOVALUE;
        goto L1; // [14] 24
    }
    else{
        _23769 = NOVALUE;
    }

    /** 		return 1*/
    DeRefDS(_s_44514);
    return 1;
L1: 

    /** 	dloc = find('.',s)*/
    _dloc_44515 = find_from(46, _s_44514, 1);

    /** 	if dloc > 8 or ( dloc > 0 and dloc + 3 < length(s) ) or ( dloc = 0 and length(s) > 8 ) then*/
    _23771 = (_dloc_44515 > 8);
    if (_23771 != 0) {
        _23772 = 1;
        goto L2; // [37] 68
    }
    _23773 = (_dloc_44515 > 0);
    if (_23773 == 0) {
        _23774 = 0;
        goto L3; // [45] 64
    }
    _23775 = _dloc_44515 + 3;
    if (IS_SEQUENCE(_s_44514)){
            _23776 = SEQ_PTR(_s_44514)->length;
    }
    else {
        _23776 = 1;
    }
    _23777 = (_23775 < _23776);
    _23775 = NOVALUE;
    _23776 = NOVALUE;
    _23774 = (_23777 != 0);
L3: 
    _23772 = (_23774 != 0);
L2: 
    if (_23772 != 0) {
        goto L4; // [68] 96
    }
    _23779 = (_dloc_44515 == 0);
    if (_23779 == 0) {
        DeRef(_23780);
        _23780 = 0;
        goto L5; // [76] 91
    }
    if (IS_SEQUENCE(_s_44514)){
            _23781 = SEQ_PTR(_s_44514)->length;
    }
    else {
        _23781 = 1;
    }
    _23782 = (_23781 > 8);
    _23781 = NOVALUE;
    _23780 = (_23782 != 0);
L5: 
    if (_23780 == 0)
    {
        _23780 = NOVALUE;
        goto L6; // [92] 103
    }
    else{
        _23780 = NOVALUE;
    }
L4: 

    /** 		return 0*/
    DeRefDS(_s_44514);
    DeRef(_23771);
    _23771 = NOVALUE;
    DeRef(_23773);
    _23773 = NOVALUE;
    DeRef(_23779);
    _23779 = NOVALUE;
    DeRef(_23777);
    _23777 = NOVALUE;
    DeRef(_23782);
    _23782 = NOVALUE;
    return 0;
L6: 

    /** 	for i = 1 to length(s) do*/
    if (IS_SEQUENCE(_s_44514)){
            _23783 = SEQ_PTR(_s_44514)->length;
    }
    else {
        _23783 = 1;
    }
    {
        int _i_44536;
        _i_44536 = 1;
L7: 
        if (_i_44536 > _23783){
            goto L8; // [108] 200
        }

        /** 		if s[i] = '.' then*/
        _2 = (int)SEQ_PTR(_s_44514);
        _23784 = (int)*(((s1_ptr)_2)->base + _i_44536);
        if (binary_op_a(NOTEQ, _23784, 46)){
            _23784 = NOVALUE;
            goto L9; // [121] 173
        }
        _23784 = NOVALUE;

        /** 			for j = i+1 to length(s) do*/
        _23786 = _i_44536 + 1;
        if (IS_SEQUENCE(_s_44514)){
                _23787 = SEQ_PTR(_s_44514)->length;
        }
        else {
            _23787 = 1;
        }
        {
            int _j_44542;
            _j_44542 = _23786;
LA: 
            if (_j_44542 > _23787){
                goto LB; // [134] 168
            }

            /** 				if not legaldos_filename_char(s[j]) then*/
            _2 = (int)SEQ_PTR(_s_44514);
            _23788 = (int)*(((s1_ptr)_2)->base + _j_44542);
            Ref(_23788);
            _23789 = _56legaldos_filename_char(_23788);
            _23788 = NOVALUE;
            if (IS_ATOM_INT(_23789)) {
                if (_23789 != 0){
                    DeRef(_23789);
                    _23789 = NOVALUE;
                    goto LC; // [151] 161
                }
            }
            else {
                if (DBL_PTR(_23789)->dbl != 0.0){
                    DeRef(_23789);
                    _23789 = NOVALUE;
                    goto LC; // [151] 161
                }
            }
            DeRef(_23789);
            _23789 = NOVALUE;

            /** 					return 0*/
            DeRefDS(_s_44514);
            DeRef(_23771);
            _23771 = NOVALUE;
            DeRef(_23773);
            _23773 = NOVALUE;
            DeRef(_23779);
            _23779 = NOVALUE;
            DeRef(_23777);
            _23777 = NOVALUE;
            DeRef(_23782);
            _23782 = NOVALUE;
            DeRef(_23786);
            _23786 = NOVALUE;
            return 0;
LC: 

            /** 			end for*/
            _j_44542 = _j_44542 + 1;
            goto LA; // [163] 141
LB: 
            ;
        }

        /** 			exit*/
        goto L8; // [170] 200
L9: 

        /** 		if not legaldos_filename_char(s[i]) then*/
        _2 = (int)SEQ_PTR(_s_44514);
        _23791 = (int)*(((s1_ptr)_2)->base + _i_44536);
        Ref(_23791);
        _23792 = _56legaldos_filename_char(_23791);
        _23791 = NOVALUE;
        if (IS_ATOM_INT(_23792)) {
            if (_23792 != 0){
                DeRef(_23792);
                _23792 = NOVALUE;
                goto LD; // [183] 193
            }
        }
        else {
            if (DBL_PTR(_23792)->dbl != 0.0){
                DeRef(_23792);
                _23792 = NOVALUE;
                goto LD; // [183] 193
            }
        }
        DeRef(_23792);
        _23792 = NOVALUE;

        /** 			return 0*/
        DeRefDS(_s_44514);
        DeRef(_23771);
        _23771 = NOVALUE;
        DeRef(_23773);
        _23773 = NOVALUE;
        DeRef(_23779);
        _23779 = NOVALUE;
        DeRef(_23777);
        _23777 = NOVALUE;
        DeRef(_23782);
        _23782 = NOVALUE;
        DeRef(_23786);
        _23786 = NOVALUE;
        return 0;
LD: 

        /** 	end for*/
        _i_44536 = _i_44536 + 1;
        goto L7; // [195] 115
L8: 
        ;
    }

    /** 	return 1*/
    DeRefDS(_s_44514);
    DeRef(_23771);
    _23771 = NOVALUE;
    DeRef(_23773);
    _23773 = NOVALUE;
    DeRef(_23779);
    _23779 = NOVALUE;
    DeRef(_23777);
    _23777 = NOVALUE;
    DeRef(_23782);
    _23782 = NOVALUE;
    DeRef(_23786);
    _23786 = NOVALUE;
    return 1;
    ;
}


int _56shrink_to_83(int _s_44555)
{
    int _dl_44556 = NOVALUE;
    int _sl_44557 = NOVALUE;
    int _osl_44558 = NOVALUE;
    int _se_44559 = NOVALUE;
    int _23873 = NOVALUE;
    int _23872 = NOVALUE;
    int _23871 = NOVALUE;
    int _23870 = NOVALUE;
    int _23869 = NOVALUE;
    int _23868 = NOVALUE;
    int _23867 = NOVALUE;
    int _23866 = NOVALUE;
    int _23865 = NOVALUE;
    int _23864 = NOVALUE;
    int _23862 = NOVALUE;
    int _23861 = NOVALUE;
    int _23860 = NOVALUE;
    int _23859 = NOVALUE;
    int _23857 = NOVALUE;
    int _23856 = NOVALUE;
    int _23855 = NOVALUE;
    int _23854 = NOVALUE;
    int _23851 = NOVALUE;
    int _23850 = NOVALUE;
    int _23849 = NOVALUE;
    int _23846 = NOVALUE;
    int _23845 = NOVALUE;
    int _23844 = NOVALUE;
    int _23842 = NOVALUE;
    int _23841 = NOVALUE;
    int _23840 = NOVALUE;
    int _23839 = NOVALUE;
    int _23837 = NOVALUE;
    int _23836 = NOVALUE;
    int _23834 = NOVALUE;
    int _23833 = NOVALUE;
    int _23831 = NOVALUE;
    int _23830 = NOVALUE;
    int _23829 = NOVALUE;
    int _23828 = NOVALUE;
    int _23827 = NOVALUE;
    int _23826 = NOVALUE;
    int _23825 = NOVALUE;
    int _23824 = NOVALUE;
    int _23823 = NOVALUE;
    int _23822 = NOVALUE;
    int _23820 = NOVALUE;
    int _23819 = NOVALUE;
    int _23818 = NOVALUE;
    int _23815 = NOVALUE;
    int _23814 = NOVALUE;
    int _23813 = NOVALUE;
    int _23812 = NOVALUE;
    int _23809 = NOVALUE;
    int _23808 = NOVALUE;
    int _23807 = NOVALUE;
    int _23805 = NOVALUE;
    int _23804 = NOVALUE;
    int _23803 = NOVALUE;
    int _23802 = NOVALUE;
    int _23800 = NOVALUE;
    int _23798 = NOVALUE;
    int _23797 = NOVALUE;
    int _23796 = NOVALUE;
    int _23795 = NOVALUE;
    int _0, _1, _2;
    

    /** 	osl = find( ':', s )*/
    _osl_44558 = find_from(58, _s_44555, 1);

    /** 	sl = osl + find( '\\', s[osl+1..$] ) -- find_from osl*/
    _23795 = _osl_44558 + 1;
    if (IS_SEQUENCE(_s_44555)){
            _23796 = SEQ_PTR(_s_44555)->length;
    }
    else {
        _23796 = 1;
    }
    rhs_slice_target = (object_ptr)&_23797;
    RHS_Slice(_s_44555, _23795, _23796);
    _23798 = find_from(92, _23797, 1);
    DeRefDS(_23797);
    _23797 = NOVALUE;
    _sl_44557 = _osl_44558 + _23798;
    _23798 = NOVALUE;

    /** 	if sl=osl+1 then*/
    _23800 = _osl_44558 + 1;
    if (_sl_44557 != _23800)
    goto L1; // [39] 49

    /** 		osl = sl*/
    _osl_44558 = _sl_44557;
L1: 

    /** 	sl = osl + find( '\\', s[osl+1..$] )*/
    _23802 = _osl_44558 + 1;
    if (_23802 > MAXINT){
        _23802 = NewDouble((double)_23802);
    }
    if (IS_SEQUENCE(_s_44555)){
            _23803 = SEQ_PTR(_s_44555)->length;
    }
    else {
        _23803 = 1;
    }
    rhs_slice_target = (object_ptr)&_23804;
    RHS_Slice(_s_44555, _23802, _23803);
    _23805 = find_from(92, _23804, 1);
    DeRefDS(_23804);
    _23804 = NOVALUE;
    _sl_44557 = _osl_44558 + _23805;
    _23805 = NOVALUE;

    /** 	dl = osl + find( '.', s[osl+1..sl] )*/
    _23807 = _osl_44558 + 1;
    rhs_slice_target = (object_ptr)&_23808;
    RHS_Slice(_s_44555, _23807, _sl_44557);
    _23809 = find_from(46, _23808, 1);
    DeRefDS(_23808);
    _23808 = NOVALUE;
    _dl_44556 = _osl_44558 + _23809;
    _23809 = NOVALUE;

    /** 	if dl > osl then*/
    if (_dl_44556 <= _osl_44558)
    goto L2; // [94] 124

    /** 		se = s[dl..min({dl+3,sl-1})]*/
    _23812 = _dl_44556 + 3;
    if ((long)((unsigned long)_23812 + (unsigned long)HIGH_BITS) >= 0) 
    _23812 = NewDouble((double)_23812);
    _23813 = _sl_44557 - 1;
    if ((long)((unsigned long)_23813 +(unsigned long) HIGH_BITS) >= 0){
        _23813 = NewDouble((double)_23813);
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _23812;
    ((int *)_2)[2] = _23813;
    _23814 = MAKE_SEQ(_1);
    _23813 = NOVALUE;
    _23812 = NOVALUE;
    _23815 = _18min(_23814);
    _23814 = NOVALUE;
    rhs_slice_target = (object_ptr)&_se_44559;
    RHS_Slice(_s_44555, _dl_44556, _23815);
    goto L3; // [121] 132
L2: 

    /** 		se = ""*/
    RefDS(_22682);
    DeRef(_se_44559);
    _se_44559 = _22682;
L3: 

    /** 	while sl != osl do*/
L4: 
    if (_sl_44557 == _osl_44558)
    goto L5; // [137] 333

    /** 		if find( ' ', s[osl+1..sl] ) or not legaldos_filename(upper(s[osl+1..sl-1])) then*/
    _23818 = _osl_44558 + 1;
    rhs_slice_target = (object_ptr)&_23819;
    RHS_Slice(_s_44555, _23818, _sl_44557);
    _23820 = find_from(32, _23819, 1);
    DeRefDS(_23819);
    _23819 = NOVALUE;
    if (_23820 != 0) {
        goto L6; // [157] 190
    }
    _23822 = _osl_44558 + 1;
    if (_23822 > MAXINT){
        _23822 = NewDouble((double)_23822);
    }
    _23823 = _sl_44557 - 1;
    rhs_slice_target = (object_ptr)&_23824;
    RHS_Slice(_s_44555, _23822, _23823);
    _23825 = _4upper(_23824);
    _23824 = NOVALUE;
    _23826 = _56legaldos_filename(_23825);
    _23825 = NOVALUE;
    if (IS_ATOM_INT(_23826)) {
        _23827 = (_23826 == 0);
    }
    else {
        _23827 = unary_op(NOT, _23826);
    }
    DeRef(_23826);
    _23826 = NOVALUE;
    if (_23827 == 0) {
        DeRef(_23827);
        _23827 = NOVALUE;
        goto L7; // [186] 244
    }
    else {
        if (!IS_ATOM_INT(_23827) && DBL_PTR(_23827)->dbl == 0.0){
            DeRef(_23827);
            _23827 = NOVALUE;
            goto L7; // [186] 244
        }
        DeRef(_23827);
        _23827 = NOVALUE;
    }
    DeRef(_23827);
    _23827 = NOVALUE;
L6: 

    /** 			s = s[1..osl] & s[osl+1..osl+6] & "~1" & se & s[sl..$]*/
    rhs_slice_target = (object_ptr)&_23828;
    RHS_Slice(_s_44555, 1, _osl_44558);
    _23829 = _osl_44558 + 1;
    if (_23829 > MAXINT){
        _23829 = NewDouble((double)_23829);
    }
    _23830 = _osl_44558 + 6;
    rhs_slice_target = (object_ptr)&_23831;
    RHS_Slice(_s_44555, _23829, _23830);
    if (IS_SEQUENCE(_s_44555)){
            _23833 = SEQ_PTR(_s_44555)->length;
    }
    else {
        _23833 = 1;
    }
    rhs_slice_target = (object_ptr)&_23834;
    RHS_Slice(_s_44555, _sl_44557, _23833);
    {
        int concat_list[5];

        concat_list[0] = _23834;
        concat_list[1] = _se_44559;
        concat_list[2] = _23832;
        concat_list[3] = _23831;
        concat_list[4] = _23828;
        Concat_N((object_ptr)&_s_44555, concat_list, 5);
    }
    DeRefDS(_23834);
    _23834 = NOVALUE;
    DeRefDS(_23831);
    _23831 = NOVALUE;
    DeRefDS(_23828);
    _23828 = NOVALUE;

    /** 			sl = osl+8+length(se)*/
    _23836 = _osl_44558 + 8;
    if ((long)((unsigned long)_23836 + (unsigned long)HIGH_BITS) >= 0) 
    _23836 = NewDouble((double)_23836);
    if (IS_SEQUENCE(_se_44559)){
            _23837 = SEQ_PTR(_se_44559)->length;
    }
    else {
        _23837 = 1;
    }
    if (IS_ATOM_INT(_23836)) {
        _sl_44557 = _23836 + _23837;
    }
    else {
        _sl_44557 = NewDouble(DBL_PTR(_23836)->dbl + (double)_23837);
    }
    DeRef(_23836);
    _23836 = NOVALUE;
    _23837 = NOVALUE;
    if (!IS_ATOM_INT(_sl_44557)) {
        _1 = (long)(DBL_PTR(_sl_44557)->dbl);
        if (UNIQUE(DBL_PTR(_sl_44557)) && (DBL_PTR(_sl_44557)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sl_44557);
        _sl_44557 = _1;
    }
L7: 

    /** 		osl = sl*/
    _osl_44558 = _sl_44557;

    /** 		sl += find( '\\', s[sl+1..$] )*/
    _23839 = _sl_44557 + 1;
    if (_23839 > MAXINT){
        _23839 = NewDouble((double)_23839);
    }
    if (IS_SEQUENCE(_s_44555)){
            _23840 = SEQ_PTR(_s_44555)->length;
    }
    else {
        _23840 = 1;
    }
    rhs_slice_target = (object_ptr)&_23841;
    RHS_Slice(_s_44555, _23839, _23840);
    _23842 = find_from(92, _23841, 1);
    DeRefDS(_23841);
    _23841 = NOVALUE;
    _sl_44557 = _sl_44557 + _23842;
    _23842 = NOVALUE;

    /** 		dl = osl + find( '.', s[osl+1..sl] )*/
    _23844 = _osl_44558 + 1;
    rhs_slice_target = (object_ptr)&_23845;
    RHS_Slice(_s_44555, _23844, _sl_44557);
    _23846 = find_from(46, _23845, 1);
    DeRefDS(_23845);
    _23845 = NOVALUE;
    _dl_44556 = _osl_44558 + _23846;
    _23846 = NOVALUE;

    /** 		if dl > osl then*/
    if (_dl_44556 <= _osl_44558)
    goto L8; // [294] 320

    /** 			se = s[dl..min({dl+3,sl})]*/
    _23849 = _dl_44556 + 3;
    if ((long)((unsigned long)_23849 + (unsigned long)HIGH_BITS) >= 0) 
    _23849 = NewDouble((double)_23849);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _23849;
    ((int *)_2)[2] = _sl_44557;
    _23850 = MAKE_SEQ(_1);
    _23849 = NOVALUE;
    _23851 = _18min(_23850);
    _23850 = NOVALUE;
    rhs_slice_target = (object_ptr)&_se_44559;
    RHS_Slice(_s_44555, _dl_44556, _23851);
    goto L4; // [317] 137
L8: 

    /** 			se = ""*/
    RefDS(_22682);
    DeRef(_se_44559);
    _se_44559 = _22682;

    /** 	end while*/
    goto L4; // [330] 137
L5: 

    /** 	if dl > osl then*/
    if (_dl_44556 <= _osl_44558)
    goto L9; // [335] 362

    /** 		se = s[dl..min({dl+3,length(s)})]*/
    _23854 = _dl_44556 + 3;
    if ((long)((unsigned long)_23854 + (unsigned long)HIGH_BITS) >= 0) 
    _23854 = NewDouble((double)_23854);
    if (IS_SEQUENCE(_s_44555)){
            _23855 = SEQ_PTR(_s_44555)->length;
    }
    else {
        _23855 = 1;
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _23854;
    ((int *)_2)[2] = _23855;
    _23856 = MAKE_SEQ(_1);
    _23855 = NOVALUE;
    _23854 = NOVALUE;
    _23857 = _18min(_23856);
    _23856 = NOVALUE;
    rhs_slice_target = (object_ptr)&_se_44559;
    RHS_Slice(_s_44555, _dl_44556, _23857);
L9: 

    /** 	if find( ' ', s[osl+1..$] ) or not legaldos_filename(upper(s[osl+1..$])) then*/
    _23859 = _osl_44558 + 1;
    if (_23859 > MAXINT){
        _23859 = NewDouble((double)_23859);
    }
    if (IS_SEQUENCE(_s_44555)){
            _23860 = SEQ_PTR(_s_44555)->length;
    }
    else {
        _23860 = 1;
    }
    rhs_slice_target = (object_ptr)&_23861;
    RHS_Slice(_s_44555, _23859, _23860);
    _23862 = find_from(32, _23861, 1);
    DeRefDS(_23861);
    _23861 = NOVALUE;
    if (_23862 != 0) {
        goto LA; // [381] 413
    }
    _23864 = _osl_44558 + 1;
    if (_23864 > MAXINT){
        _23864 = NewDouble((double)_23864);
    }
    if (IS_SEQUENCE(_s_44555)){
            _23865 = SEQ_PTR(_s_44555)->length;
    }
    else {
        _23865 = 1;
    }
    rhs_slice_target = (object_ptr)&_23866;
    RHS_Slice(_s_44555, _23864, _23865);
    _23867 = _4upper(_23866);
    _23866 = NOVALUE;
    _23868 = _56legaldos_filename(_23867);
    _23867 = NOVALUE;
    if (IS_ATOM_INT(_23868)) {
        _23869 = (_23868 == 0);
    }
    else {
        _23869 = unary_op(NOT, _23868);
    }
    DeRef(_23868);
    _23868 = NOVALUE;
    if (_23869 == 0) {
        DeRef(_23869);
        _23869 = NOVALUE;
        goto LB; // [409] 443
    }
    else {
        if (!IS_ATOM_INT(_23869) && DBL_PTR(_23869)->dbl == 0.0){
            DeRef(_23869);
            _23869 = NOVALUE;
            goto LB; // [409] 443
        }
        DeRef(_23869);
        _23869 = NOVALUE;
    }
    DeRef(_23869);
    _23869 = NOVALUE;
LA: 

    /** 		s = s[1..osl] & s[osl+1..osl+6] & "~1" & se*/
    rhs_slice_target = (object_ptr)&_23870;
    RHS_Slice(_s_44555, 1, _osl_44558);
    _23871 = _osl_44558 + 1;
    if (_23871 > MAXINT){
        _23871 = NewDouble((double)_23871);
    }
    _23872 = _osl_44558 + 6;
    rhs_slice_target = (object_ptr)&_23873;
    RHS_Slice(_s_44555, _23871, _23872);
    {
        int concat_list[4];

        concat_list[0] = _se_44559;
        concat_list[1] = _23832;
        concat_list[2] = _23873;
        concat_list[3] = _23870;
        Concat_N((object_ptr)&_s_44555, concat_list, 4);
    }
    DeRefDS(_23873);
    _23873 = NOVALUE;
    DeRefDS(_23870);
    _23870 = NOVALUE;
LB: 

    /** 	return s*/
    DeRef(_se_44559);
    DeRef(_23795);
    _23795 = NOVALUE;
    DeRef(_23800);
    _23800 = NOVALUE;
    DeRef(_23802);
    _23802 = NOVALUE;
    DeRef(_23807);
    _23807 = NOVALUE;
    DeRef(_23818);
    _23818 = NOVALUE;
    DeRef(_23815);
    _23815 = NOVALUE;
    DeRef(_23822);
    _23822 = NOVALUE;
    DeRef(_23823);
    _23823 = NOVALUE;
    DeRef(_23839);
    _23839 = NOVALUE;
    DeRef(_23829);
    _23829 = NOVALUE;
    DeRef(_23830);
    _23830 = NOVALUE;
    DeRef(_23844);
    _23844 = NOVALUE;
    DeRef(_23859);
    _23859 = NOVALUE;
    DeRef(_23851);
    _23851 = NOVALUE;
    DeRef(_23857);
    _23857 = NOVALUE;
    DeRef(_23864);
    _23864 = NOVALUE;
    DeRef(_23871);
    _23871 = NOVALUE;
    DeRef(_23872);
    _23872 = NOVALUE;
    return _s_44555;
    ;
}


int _56truncate_to_83(int _lfn_44657)
{
    int _dl_44658 = NOVALUE;
    int _23894 = NOVALUE;
    int _23893 = NOVALUE;
    int _23892 = NOVALUE;
    int _23891 = NOVALUE;
    int _23890 = NOVALUE;
    int _23889 = NOVALUE;
    int _23888 = NOVALUE;
    int _23887 = NOVALUE;
    int _23886 = NOVALUE;
    int _23885 = NOVALUE;
    int _23884 = NOVALUE;
    int _23883 = NOVALUE;
    int _23882 = NOVALUE;
    int _23881 = NOVALUE;
    int _23880 = NOVALUE;
    int _23879 = NOVALUE;
    int _23878 = NOVALUE;
    int _23877 = NOVALUE;
    int _23876 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer dl = find( '.', lfn )*/
    _dl_44658 = find_from(46, _lfn_44657, 1);

    /** 	if dl = 0 and length(lfn) > 8 then*/
    _23876 = (_dl_44658 == 0);
    if (_23876 == 0) {
        goto L1; // [16] 45
    }
    if (IS_SEQUENCE(_lfn_44657)){
            _23878 = SEQ_PTR(_lfn_44657)->length;
    }
    else {
        _23878 = 1;
    }
    _23879 = (_23878 > 8);
    _23878 = NOVALUE;
    if (_23879 == 0)
    {
        DeRef(_23879);
        _23879 = NOVALUE;
        goto L1; // [28] 45
    }
    else{
        DeRef(_23879);
        _23879 = NOVALUE;
    }

    /** 		return lfn[1..8]*/
    rhs_slice_target = (object_ptr)&_23880;
    RHS_Slice(_lfn_44657, 1, 8);
    DeRefDS(_lfn_44657);
    DeRef(_23876);
    _23876 = NOVALUE;
    return _23880;
    goto L2; // [42] 138
L1: 

    /** 	elsif dl = 0 and length(lfn) <= 8 then*/
    _23881 = (_dl_44658 == 0);
    if (_23881 == 0) {
        goto L3; // [51] 75
    }
    if (IS_SEQUENCE(_lfn_44657)){
            _23883 = SEQ_PTR(_lfn_44657)->length;
    }
    else {
        _23883 = 1;
    }
    _23884 = (_23883 <= 8);
    _23883 = NOVALUE;
    if (_23884 == 0)
    {
        DeRef(_23884);
        _23884 = NOVALUE;
        goto L3; // [63] 75
    }
    else{
        DeRef(_23884);
        _23884 = NOVALUE;
    }

    /** 		return lfn*/
    DeRef(_23876);
    _23876 = NOVALUE;
    DeRef(_23880);
    _23880 = NOVALUE;
    DeRef(_23881);
    _23881 = NOVALUE;
    return _lfn_44657;
    goto L2; // [72] 138
L3: 

    /** 	elsif dl > 9 and dl + 3 <= length(lfn) then*/
    _23885 = (_dl_44658 > 9);
    if (_23885 == 0) {
        goto L4; // [81] 126
    }
    _23887 = _dl_44658 + 3;
    if ((long)((unsigned long)_23887 + (unsigned long)HIGH_BITS) >= 0) 
    _23887 = NewDouble((double)_23887);
    if (IS_SEQUENCE(_lfn_44657)){
            _23888 = SEQ_PTR(_lfn_44657)->length;
    }
    else {
        _23888 = 1;
    }
    if (IS_ATOM_INT(_23887)) {
        _23889 = (_23887 <= _23888);
    }
    else {
        _23889 = (DBL_PTR(_23887)->dbl <= (double)_23888);
    }
    DeRef(_23887);
    _23887 = NOVALUE;
    _23888 = NOVALUE;
    if (_23889 == 0)
    {
        DeRef(_23889);
        _23889 = NOVALUE;
        goto L4; // [97] 126
    }
    else{
        DeRef(_23889);
        _23889 = NOVALUE;
    }

    /** 		return lfn[1..8] & lfn[dl..$]*/
    rhs_slice_target = (object_ptr)&_23890;
    RHS_Slice(_lfn_44657, 1, 8);
    if (IS_SEQUENCE(_lfn_44657)){
            _23891 = SEQ_PTR(_lfn_44657)->length;
    }
    else {
        _23891 = 1;
    }
    rhs_slice_target = (object_ptr)&_23892;
    RHS_Slice(_lfn_44657, _dl_44658, _23891);
    Concat((object_ptr)&_23893, _23890, _23892);
    DeRefDS(_23890);
    _23890 = NOVALUE;
    DeRef(_23890);
    _23890 = NOVALUE;
    DeRefDS(_23892);
    _23892 = NOVALUE;
    DeRefDS(_lfn_44657);
    DeRef(_23876);
    _23876 = NOVALUE;
    DeRef(_23880);
    _23880 = NOVALUE;
    DeRef(_23881);
    _23881 = NOVALUE;
    DeRef(_23885);
    _23885 = NOVALUE;
    return _23893;
    goto L2; // [123] 138
L4: 

    /** 		CompileErr( 48, {lfn})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_lfn_44657);
    *((int *)(_2+4)) = _lfn_44657;
    _23894 = MAKE_SEQ(_1);
    _43CompileErr(48, _23894, 0);
    _23894 = NOVALUE;
L2: 
    ;
}


void _56check_file_routines()
{
    int _s_44693 = NOVALUE;
    int _23912 = NOVALUE;
    int _23911 = NOVALUE;
    int _23910 = NOVALUE;
    int _23909 = NOVALUE;
    int _23908 = NOVALUE;
    int _23907 = NOVALUE;
    int _23906 = NOVALUE;
    int _23905 = NOVALUE;
    int _23904 = NOVALUE;
    int _23903 = NOVALUE;
    int _23902 = NOVALUE;
    int _23901 = NOVALUE;
    int _23899 = NOVALUE;
    int _23897 = NOVALUE;
    int _23895 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length( file_routines ) then*/
    if (IS_SEQUENCE(_56file_routines_44684)){
            _23895 = SEQ_PTR(_56file_routines_44684)->length;
    }
    else {
        _23895 = 1;
    }
    if (_23895 != 0)
    goto L1; // [8] 146
    _23895 = NOVALUE;

    /** 		file_routines = repeat( {}, length( known_files ) )*/
    if (IS_SEQUENCE(_26known_files_11139)){
            _23897 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _23897 = 1;
    }
    DeRefDS(_56file_routines_44684);
    _56file_routines_44684 = Repeat(_22682, _23897);
    _23897 = NOVALUE;

    /** 		integer s = SymTab[TopLevelSub][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23899 = (int)*(((s1_ptr)_2)->base + _25TopLevelSub_12269);
    _2 = (int)SEQ_PTR(_23899);
    _s_44693 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_44693)){
        _s_44693 = (long)DBL_PTR(_s_44693)->dbl;
    }
    _23899 = NOVALUE;

    /** 		while s do*/
L2: 
    if (_s_44693 == 0)
    {
        goto L3; // [45] 145
    }
    else{
    }

    /** 			if SymTab[s][S_USAGE] != U_DELETED and*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23901 = (int)*(((s1_ptr)_2)->base + _s_44693);
    _2 = (int)SEQ_PTR(_23901);
    _23902 = (int)*(((s1_ptr)_2)->base + 5);
    _23901 = NOVALUE;
    if (IS_ATOM_INT(_23902)) {
        _23903 = (_23902 != 99);
    }
    else {
        _23903 = binary_op(NOTEQ, _23902, 99);
    }
    _23902 = NOVALUE;
    if (IS_ATOM_INT(_23903)) {
        if (_23903 == 0) {
            goto L4; // [68] 124
        }
    }
    else {
        if (DBL_PTR(_23903)->dbl == 0.0) {
            goto L4; // [68] 124
        }
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23905 = (int)*(((s1_ptr)_2)->base + _s_44693);
    _2 = (int)SEQ_PTR(_23905);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _23906 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _23906 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _23905 = NOVALUE;
    _23907 = find_from(_23906, _28RTN_TOKS_11857, 1);
    _23906 = NOVALUE;
    if (_23907 == 0)
    {
        _23907 = NOVALUE;
        goto L4; // [92] 124
    }
    else{
        _23907 = NOVALUE;
    }

    /** 				file_routines[SymTab[s][S_FILE_NO]] &= s*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23908 = (int)*(((s1_ptr)_2)->base + _s_44693);
    _2 = (int)SEQ_PTR(_23908);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _23909 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _23909 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _23908 = NOVALUE;
    _2 = (int)SEQ_PTR(_56file_routines_44684);
    if (!IS_ATOM_INT(_23909)){
        _23910 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_23909)->dbl));
    }
    else{
        _23910 = (int)*(((s1_ptr)_2)->base + _23909);
    }
    if (IS_SEQUENCE(_23910) && IS_ATOM(_s_44693)) {
        Append(&_23911, _23910, _s_44693);
    }
    else if (IS_ATOM(_23910) && IS_SEQUENCE(_s_44693)) {
    }
    else {
        Concat((object_ptr)&_23911, _23910, _s_44693);
        _23910 = NOVALUE;
    }
    _23910 = NOVALUE;
    _2 = (int)SEQ_PTR(_56file_routines_44684);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _56file_routines_44684 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_23909))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_23909)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _23909);
    _1 = *(int *)_2;
    *(int *)_2 = _23911;
    if( _1 != _23911 ){
        DeRef(_1);
    }
    _23911 = NOVALUE;
L4: 

    /** 			s = SymTab[s][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _23912 = (int)*(((s1_ptr)_2)->base + _s_44693);
    _2 = (int)SEQ_PTR(_23912);
    _s_44693 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_44693)){
        _s_44693 = (long)DBL_PTR(_s_44693)->dbl;
    }
    _23912 = NOVALUE;

    /** 		end while*/
    goto L2; // [142] 45
L3: 
L1: 

    /** end procedure*/
    _23909 = NOVALUE;
    DeRef(_23903);
    _23903 = NOVALUE;
    return;
    ;
}


void _56GenerateUserRoutines()
{
    int _s_44727 = NOVALUE;
    int _sp_44728 = NOVALUE;
    int _next_c_char_44729 = NOVALUE;
    int _q_44730 = NOVALUE;
    int _temps_44731 = NOVALUE;
    int _buff_44732 = NOVALUE;
    int _base_name_44733 = NOVALUE;
    int _long_c_file_44734 = NOVALUE;
    int _c_file_44735 = NOVALUE;
    int _these_routines_44802 = NOVALUE;
    int _ret_type_44860 = NOVALUE;
    int _scope_44931 = NOVALUE;
    int _names_44965 = NOVALUE;
    int _name_44975 = NOVALUE;
    int _24125 = NOVALUE;
    int _24123 = NOVALUE;
    int _24122 = NOVALUE;
    int _24119 = NOVALUE;
    int _24117 = NOVALUE;
    int _24116 = NOVALUE;
    int _24115 = NOVALUE;
    int _24114 = NOVALUE;
    int _24113 = NOVALUE;
    int _24112 = NOVALUE;
    int _24110 = NOVALUE;
    int _24106 = NOVALUE;
    int _24104 = NOVALUE;
    int _24103 = NOVALUE;
    int _24102 = NOVALUE;
    int _24101 = NOVALUE;
    int _24100 = NOVALUE;
    int _24099 = NOVALUE;
    int _24097 = NOVALUE;
    int _24096 = NOVALUE;
    int _24094 = NOVALUE;
    int _24093 = NOVALUE;
    int _24092 = NOVALUE;
    int _24091 = NOVALUE;
    int _24090 = NOVALUE;
    int _24088 = NOVALUE;
    int _24086 = NOVALUE;
    int _24085 = NOVALUE;
    int _24084 = NOVALUE;
    int _24083 = NOVALUE;
    int _24082 = NOVALUE;
    int _24081 = NOVALUE;
    int _24080 = NOVALUE;
    int _24078 = NOVALUE;
    int _24077 = NOVALUE;
    int _24076 = NOVALUE;
    int _24075 = NOVALUE;
    int _24074 = NOVALUE;
    int _24073 = NOVALUE;
    int _24072 = NOVALUE;
    int _24071 = NOVALUE;
    int _24069 = NOVALUE;
    int _24068 = NOVALUE;
    int _24066 = NOVALUE;
    int _24065 = NOVALUE;
    int _24064 = NOVALUE;
    int _24063 = NOVALUE;
    int _24062 = NOVALUE;
    int _24061 = NOVALUE;
    int _24060 = NOVALUE;
    int _24059 = NOVALUE;
    int _24057 = NOVALUE;
    int _24056 = NOVALUE;
    int _24054 = NOVALUE;
    int _24053 = NOVALUE;
    int _24052 = NOVALUE;
    int _24050 = NOVALUE;
    int _24047 = NOVALUE;
    int _24046 = NOVALUE;
    int _24044 = NOVALUE;
    int _24042 = NOVALUE;
    int _24036 = NOVALUE;
    int _24035 = NOVALUE;
    int _24034 = NOVALUE;
    int _24033 = NOVALUE;
    int _24032 = NOVALUE;
    int _24031 = NOVALUE;
    int _24030 = NOVALUE;
    int _24029 = NOVALUE;
    int _24027 = NOVALUE;
    int _24026 = NOVALUE;
    int _24024 = NOVALUE;
    int _24023 = NOVALUE;
    int _24020 = NOVALUE;
    int _24018 = NOVALUE;
    int _24017 = NOVALUE;
    int _24016 = NOVALUE;
    int _24012 = NOVALUE;
    int _24009 = NOVALUE;
    int _24006 = NOVALUE;
    int _24005 = NOVALUE;
    int _24004 = NOVALUE;
    int _24003 = NOVALUE;
    int _24001 = NOVALUE;
    int _24000 = NOVALUE;
    int _23998 = NOVALUE;
    int _23997 = NOVALUE;
    int _23996 = NOVALUE;
    int _23994 = NOVALUE;
    int _23991 = NOVALUE;
    int _23990 = NOVALUE;
    int _23989 = NOVALUE;
    int _23988 = NOVALUE;
    int _23987 = NOVALUE;
    int _23986 = NOVALUE;
    int _23984 = NOVALUE;
    int _23983 = NOVALUE;
    int _23981 = NOVALUE;
    int _23978 = NOVALUE;
    int _23977 = NOVALUE;
    int _23973 = NOVALUE;
    int _23972 = NOVALUE;
    int _23970 = NOVALUE;
    int _23966 = NOVALUE;
    int _23965 = NOVALUE;
    int _23964 = NOVALUE;
    int _23963 = NOVALUE;
    int _23962 = NOVALUE;
    int _23961 = NOVALUE;
    int _23960 = NOVALUE;
    int _23959 = NOVALUE;
    int _23958 = NOVALUE;
    int _23957 = NOVALUE;
    int _23956 = NOVALUE;
    int _23955 = NOVALUE;
    int _23954 = NOVALUE;
    int _23953 = NOVALUE;
    int _23951 = NOVALUE;
    int _23950 = NOVALUE;
    int _23948 = NOVALUE;
    int _23945 = NOVALUE;
    int _23942 = NOVALUE;
    int _23939 = NOVALUE;
    int _23936 = NOVALUE;
    int _23935 = NOVALUE;
    int _23934 = NOVALUE;
    int _23931 = NOVALUE;
    int _23928 = NOVALUE;
    int _23926 = NOVALUE;
    int _23922 = NOVALUE;
    int _23921 = NOVALUE;
    int _23919 = NOVALUE;
    int _23918 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	integer next_c_char, q, temps*/

    /** 	sequence buff, base_name, long_c_file, c_file*/

    /** 	if not silent then*/
    if (_25silent_12398 != 0)
    goto L1; // [9] 62

    /** 		if Pass = 1 then*/
    if (_56Pass_42638 != 1)
    goto L2; // [16] 29

    /** 			ShowMsg(1, 239,,0)*/
    RefDS(_22682);
    _44ShowMsg(1, 239, _22682, 0);
L2: 

    /** 		if LAST_PASS = TRUE then*/
    if (_56LAST_PASS_42636 != _5TRUE_244)
    goto L3; // [35] 50

    /** 			ShowMsg(1, 240)*/
    RefDS(_22682);
    _44ShowMsg(1, 240, _22682, 1);
    goto L4; // [47] 61
L3: 

    /** 			ShowMsg(1, 241, Pass, 0)*/
    _44ShowMsg(1, 241, _56Pass_42638, 0);
L4: 
L1: 

    /** 	check_file_routines()*/
    _56check_file_routines();

    /** 	c_puts("// GenerateUserRoutines\n")*/
    RefDS(_23917);
    _53c_puts(_23917);

    /** 	for file_no = 1 to length(known_files) do*/
    if (IS_SEQUENCE(_26known_files_11139)){
            _23918 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _23918 = 1;
    }
    {
        int _file_no_44751;
        _file_no_44751 = 1;
L5: 
        if (_file_no_44751 > _23918){
            goto L6; // [78] 2060
        }

        /** 		if file_no = 1 or any_code(file_no) then*/
        _23919 = (_file_no_44751 == 1);
        if (_23919 != 0) {
            goto L7; // [91] 104
        }
        _23921 = _56any_code(_file_no_44751);
        if (_23921 == 0) {
            DeRef(_23921);
            _23921 = NOVALUE;
            goto L8; // [100] 2051
        }
        else {
            if (!IS_ATOM_INT(_23921) && DBL_PTR(_23921)->dbl == 0.0){
                DeRef(_23921);
                _23921 = NOVALUE;
                goto L8; // [100] 2051
            }
            DeRef(_23921);
            _23921 = NOVALUE;
        }
        DeRef(_23921);
        _23921 = NOVALUE;
L7: 

        /** 			next_c_char = 1*/
        _next_c_char_44729 = 1;

        /** 			base_name = name_ext(known_files[file_no])*/
        _2 = (int)SEQ_PTR(_26known_files_11139);
        _23922 = (int)*(((s1_ptr)_2)->base + _file_no_44751);
        Ref(_23922);
        _0 = _base_name_44733;
        _base_name_44733 = _52name_ext(_23922);
        DeRef(_0);
        _23922 = NOVALUE;

        /** 			c_file = base_name*/
        RefDS(_base_name_44733);
        DeRef(_c_file_44735);
        _c_file_44735 = _base_name_44733;

        /** 			q = length(c_file)*/
        if (IS_SEQUENCE(_c_file_44735)){
                _q_44730 = SEQ_PTR(_c_file_44735)->length;
        }
        else {
            _q_44730 = 1;
        }

        /** 			while q >= 1 do*/
L9: 
        if (_q_44730 < 1)
        goto LA; // [140] 181

        /** 				if c_file[q] = '.' then*/
        _2 = (int)SEQ_PTR(_c_file_44735);
        _23926 = (int)*(((s1_ptr)_2)->base + _q_44730);
        if (binary_op_a(NOTEQ, _23926, 46)){
            _23926 = NOVALUE;
            goto LB; // [150] 170
        }
        _23926 = NOVALUE;

        /** 					c_file = c_file[1..q-1]*/
        _23928 = _q_44730 - 1;
        rhs_slice_target = (object_ptr)&_c_file_44735;
        RHS_Slice(_c_file_44735, 1, _23928);

        /** 					exit*/
        goto LA; // [167] 181
LB: 

        /** 				q -= 1*/
        _q_44730 = _q_44730 - 1;

        /** 			end while*/
        goto L9; // [178] 140
LA: 

        /** 			if find(lower(c_file), {"main-", "init-"})  then*/
        RefDS(_c_file_44735);
        _23931 = _4lower(_c_file_44735);
        RefDS(_23933);
        RefDS(_23932);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _23932;
        ((int *)_2)[2] = _23933;
        _23934 = MAKE_SEQ(_1);
        _23935 = find_from(_23931, _23934, 1);
        DeRef(_23931);
        _23931 = NOVALUE;
        DeRefDS(_23934);
        _23934 = NOVALUE;
        if (_23935 == 0)
        {
            _23935 = NOVALUE;
            goto LC; // [196] 211
        }
        else{
            _23935 = NOVALUE;
        }

        /** 				CompileErr(12, {base_name})*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_base_name_44733);
        *((int *)(_2+4)) = _base_name_44733;
        _23936 = MAKE_SEQ(_1);
        _43CompileErr(12, _23936, 0);
        _23936 = NOVALUE;
LC: 

        /** 			long_c_file = c_file*/
        RefDS(_c_file_44735);
        DeRef(_long_c_file_44734);
        _long_c_file_44734 = _c_file_44735;

        /** 			if LAST_PASS = TRUE then*/
        if (_56LAST_PASS_42636 != _5TRUE_244)
        goto LD; // [224] 249

        /** 				c_file = unique_c_name(c_file)*/
        RefDS(_c_file_44735);
        _0 = _c_file_44735;
        _c_file_44735 = _56unique_c_name(_c_file_44735);
        DeRefDS(_0);

        /** 				add_file(c_file, known_files[file_no])*/
        _2 = (int)SEQ_PTR(_26known_files_11139);
        _23939 = (int)*(((s1_ptr)_2)->base + _file_no_44751);
        RefDS(_c_file_44735);
        Ref(_23939);
        _56add_file(_c_file_44735, _23939);
        _23939 = NOVALUE;
LD: 

        /** 			if file_no = 1 then*/
        if (_file_no_44751 != 1)
        goto LE; // [251] 314

        /** 				if LAST_PASS = TRUE then*/
        if (_56LAST_PASS_42636 != _5TRUE_244)
        goto LF; // [261] 306

        /** 					add_file("main-")*/
        RefDS(_23932);
        RefDS(_22682);
        _56add_file(_23932, _22682);

        /** 					for i = 0 to main_name_num-1 do*/
        _23942 = _53main_name_num_46493 - 1;
        if ((long)((unsigned long)_23942 +(unsigned long) HIGH_BITS) >= 0){
            _23942 = NewDouble((double)_23942);
        }
        {
            int _i_44792;
            _i_44792 = 0;
L10: 
            if (binary_op_a(GREATER, _i_44792, _23942)){
                goto L11; // [279] 305
            }

            /** 						buff = sprintf("main-%d", i)*/
            DeRefi(_buff_44732);
            _buff_44732 = EPrintf(-9999999, _23943, _i_44792);

            /** 						add_file(buff)*/
            RefDS(_buff_44732);
            RefDS(_22682);
            _56add_file(_buff_44732, _22682);

            /** 					end for*/
            _0 = _i_44792;
            if (IS_ATOM_INT(_i_44792)) {
                _i_44792 = _i_44792 + 1;
                if ((long)((unsigned long)_i_44792 +(unsigned long) HIGH_BITS) >= 0){
                    _i_44792 = NewDouble((double)_i_44792);
                }
            }
            else {
                _i_44792 = binary_op_a(PLUS, _i_44792, 1);
            }
            DeRef(_0);
            goto L10; // [300] 286
L11: 
            ;
            DeRef(_i_44792);
        }
LF: 

        /** 				file0 = long_c_file*/
        RefDS(_long_c_file_44734);
        DeRef(_56file0_44491);
        _56file0_44491 = _long_c_file_44734;
LE: 

        /** 			new_c_file(c_file)*/
        RefDS(_c_file_44735);
        _56new_c_file(_c_file_44735);

        /** 			s = SymTab[TopLevelSub][S_NEXT]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _23945 = (int)*(((s1_ptr)_2)->base + _25TopLevelSub_12269);
        _2 = (int)SEQ_PTR(_23945);
        _s_44727 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_s_44727)){
            _s_44727 = (long)DBL_PTR(_s_44727)->dbl;
        }
        _23945 = NOVALUE;

        /** 			sequence these_routines = file_routines[file_no]*/
        DeRef(_these_routines_44802);
        _2 = (int)SEQ_PTR(_56file_routines_44684);
        _these_routines_44802 = (int)*(((s1_ptr)_2)->base + _file_no_44751);
        Ref(_these_routines_44802);

        /** 			for routine_no = 1 to length( these_routines ) do*/
        if (IS_SEQUENCE(_these_routines_44802)){
                _23948 = SEQ_PTR(_these_routines_44802)->length;
        }
        else {
            _23948 = 1;
        }
        {
            int _routine_no_44805;
            _routine_no_44805 = 1;
L12: 
            if (_routine_no_44805 > _23948){
                goto L13; // [352] 2050
            }

            /** 				s = these_routines[routine_no]*/
            _2 = (int)SEQ_PTR(_these_routines_44802);
            _s_44727 = (int)*(((s1_ptr)_2)->base + _routine_no_44805);
            if (!IS_ATOM_INT(_s_44727)){
                _s_44727 = (long)DBL_PTR(_s_44727)->dbl;
            }

            /** 				if SymTab[s][S_USAGE] != U_DELETED then*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _23950 = (int)*(((s1_ptr)_2)->base + _s_44727);
            _2 = (int)SEQ_PTR(_23950);
            _23951 = (int)*(((s1_ptr)_2)->base + 5);
            _23950 = NOVALUE;
            if (binary_op_a(EQUALS, _23951, 99)){
                _23951 = NOVALUE;
                goto L14; // [383] 2041
            }
            _23951 = NOVALUE;

            /** 					if LAST_PASS = TRUE and*/
            _23953 = (_56LAST_PASS_42636 == _5TRUE_244);
            if (_23953 == 0) {
                goto L15; // [397] 593
            }
            _23955 = (_25cfile_size_12345 > _54max_cfile_size_45332);
            if (_23955 != 0) {
                DeRef(_23956);
                _23956 = 1;
                goto L16; // [409] 472
            }
            _23957 = (_s_44727 != _25TopLevelSub_12269);
            if (_23957 == 0) {
                _23958 = 0;
                goto L17; // [419] 439
            }
            _23959 = (_54max_cfile_size_45332 % 4) ? NewDouble((double)_54max_cfile_size_45332 / 4) : (_54max_cfile_size_45332 / 4);
            if (IS_ATOM_INT(_23959)) {
                _23960 = (_25cfile_size_12345 > _23959);
            }
            else {
                _23960 = ((double)_25cfile_size_12345 > DBL_PTR(_23959)->dbl);
            }
            DeRef(_23959);
            _23959 = NOVALUE;
            _23958 = (_23960 != 0);
L17: 
            if (_23958 == 0) {
                _23961 = 0;
                goto L18; // [439] 468
            }
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _23962 = (int)*(((s1_ptr)_2)->base + _s_44727);
            _2 = (int)SEQ_PTR(_23962);
            if (!IS_ATOM_INT(_25S_CODE_11925)){
                _23963 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
            }
            else{
                _23963 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
            }
            _23962 = NOVALUE;
            if (IS_SEQUENCE(_23963)){
                    _23964 = SEQ_PTR(_23963)->length;
            }
            else {
                _23964 = 1;
            }
            _23963 = NOVALUE;
            _23965 = (_23964 > _54max_cfile_size_45332);
            _23964 = NOVALUE;
            _23961 = (_23965 != 0);
L18: 
            DeRef(_23956);
            _23956 = (_23961 != 0);
L16: 
            if (_23956 == 0)
            {
                _23956 = NOVALUE;
                goto L15; // [473] 593
            }
            else{
                _23956 = NOVALUE;
            }

            /** 						if length(c_file) = 7 then*/
            if (IS_SEQUENCE(_c_file_44735)){
                    _23966 = SEQ_PTR(_c_file_44735)->length;
            }
            else {
                _23966 = 1;
            }
            if (_23966 != 7)
            goto L19; // [481] 492

            /** 							c_file &= " "*/
            Concat((object_ptr)&_c_file_44735, _c_file_44735, _23968);
L19: 

            /** 						if length(c_file) >= 8 then*/
            if (IS_SEQUENCE(_c_file_44735)){
                    _23970 = SEQ_PTR(_c_file_44735)->length;
            }
            else {
                _23970 = 1;
            }
            if (_23970 < 8)
            goto L1A; // [497] 520

            /** 							c_file[7] = '_'*/
            _2 = (int)SEQ_PTR(_c_file_44735);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _c_file_44735 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 7);
            _1 = *(int *)_2;
            *(int *)_2 = 95;
            DeRef(_1);

            /** 							c_file[8] = file_chars[next_c_char]*/
            _2 = (int)SEQ_PTR(_56file_chars_44357);
            _23972 = (int)*(((s1_ptr)_2)->base + _next_c_char_44729);
            Ref(_23972);
            _2 = (int)SEQ_PTR(_c_file_44735);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _c_file_44735 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 8);
            _1 = *(int *)_2;
            *(int *)_2 = _23972;
            if( _1 != _23972 ){
                DeRef(_1);
            }
            _23972 = NOVALUE;
            goto L1B; // [517] 552
L1A: 

            /** 							if find('_', c_file) = 0 then*/
            _23973 = find_from(95, _c_file_44735, 1);
            if (_23973 != 0)
            goto L1C; // [527] 538

            /** 								c_file &= "_ "*/
            Concat((object_ptr)&_c_file_44735, _c_file_44735, _23975);
L1C: 

            /** 							c_file[$] = file_chars[next_c_char]*/
            if (IS_SEQUENCE(_c_file_44735)){
                    _23977 = SEQ_PTR(_c_file_44735)->length;
            }
            else {
                _23977 = 1;
            }
            _2 = (int)SEQ_PTR(_56file_chars_44357);
            _23978 = (int)*(((s1_ptr)_2)->base + _next_c_char_44729);
            Ref(_23978);
            _2 = (int)SEQ_PTR(_c_file_44735);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _c_file_44735 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _23977);
            _1 = *(int *)_2;
            *(int *)_2 = _23978;
            if( _1 != _23978 ){
                DeRef(_1);
            }
            _23978 = NOVALUE;
L1B: 

            /** 						c_file = unique_c_name(c_file)*/
            RefDS(_c_file_44735);
            _0 = _c_file_44735;
            _c_file_44735 = _56unique_c_name(_c_file_44735);
            DeRefDS(_0);

            /** 						new_c_file(c_file)*/
            RefDS(_c_file_44735);
            _56new_c_file(_c_file_44735);

            /** 						next_c_char += 1*/
            _next_c_char_44729 = _next_c_char_44729 + 1;

            /** 						if next_c_char > length(file_chars) then*/
            if (IS_SEQUENCE(_56file_chars_44357)){
                    _23981 = SEQ_PTR(_56file_chars_44357)->length;
            }
            else {
                _23981 = 1;
            }
            if (_next_c_char_44729 <= _23981)
            goto L1D; // [576] 586

            /** 							next_c_char = 1  -- (unique_c_name will resolve)*/
            _next_c_char_44729 = 1;
L1D: 

            /** 						add_file(c_file)*/
            RefDS(_c_file_44735);
            RefDS(_22682);
            _56add_file(_c_file_44735, _22682);
L15: 

            /** 					sequence ret_type*/

            /** 					if SymTab[s][S_TOKEN] = PROC then*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _23983 = (int)*(((s1_ptr)_2)->base + _s_44727);
            _2 = (int)SEQ_PTR(_23983);
            if (!IS_ATOM_INT(_25S_TOKEN_11918)){
                _23984 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
            }
            else{
                _23984 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
            }
            _23983 = NOVALUE;
            if (binary_op_a(NOTEQ, _23984, 27)){
                _23984 = NOVALUE;
                goto L1E; // [611] 625
            }
            _23984 = NOVALUE;

            /** 						ret_type = "void "*/
            RefDS(_23405);
            DeRefi(_ret_type_44860);
            _ret_type_44860 = _23405;
            goto L1F; // [622] 633
L1E: 

            /** 						ret_type = "int "*/
            RefDS(_23406);
            DeRefi(_ret_type_44860);
            _ret_type_44860 = _23406;
L1F: 

            /** 					if find( SymTab[s][S_SCOPE], {SC_GLOBAL, SC_EXPORT, SC_PUBLIC} ) and dll_option then*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _23986 = (int)*(((s1_ptr)_2)->base + _s_44727);
            _2 = (int)SEQ_PTR(_23986);
            _23987 = (int)*(((s1_ptr)_2)->base + 4);
            _23986 = NOVALUE;
            _1 = NewS1(3);
            _2 = (int)((s1_ptr)_1)->base;
            *((int *)(_2+4)) = 6;
            *((int *)(_2+8)) = 11;
            *((int *)(_2+12)) = 13;
            _23988 = MAKE_SEQ(_1);
            _23989 = find_from(_23987, _23988, 1);
            _23987 = NOVALUE;
            DeRefDS(_23988);
            _23988 = NOVALUE;
            if (_23989 == 0) {
                goto L20; // [664] 740
            }
            if (_56dll_option_42654 == 0)
            {
                goto L20; // [671] 740
            }
            else{
            }

            /** 						SymTab[s][S_RI_TARGET] = TRUE*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _26SymTab_11138 = MAKE_SEQ(_2);
            }
            _3 = (int)(_s_44727 + ((s1_ptr)_2)->base);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 53);
            _1 = *(int *)_2;
            *(int *)_2 = _5TRUE_244;
            DeRef(_1);
            _23991 = NOVALUE;

            /** 						LeftSym = TRUE*/
            _56LeftSym_42651 = _5TRUE_244;

            /** 						if TWINDOWS then*/
            if (_36TWINDOWS_14721 == 0)
            {
                goto L21; // [704] 723
            }
            else{
            }

            /** 							c_stmt(ret_type & " __stdcall @(", s)*/
            Concat((object_ptr)&_23994, _ret_type_44860, _23993);
            _56c_stmt(_23994, _s_44727, 0);
            _23994 = NOVALUE;
            goto L22; // [720] 763
L21: 

            /** 							c_stmt(ret_type & "@(", s)*/
            Concat((object_ptr)&_23996, _ret_type_44860, _23995);
            _56c_stmt(_23996, _s_44727, 0);
            _23996 = NOVALUE;
            goto L22; // [737] 763
L20: 

            /** 						LeftSym = TRUE*/
            _56LeftSym_42651 = _5TRUE_244;

            /** 						c_stmt( ret_type & "@(", s)*/
            Concat((object_ptr)&_23997, _ret_type_44860, _23995);
            _56c_stmt(_23997, _s_44727, 0);
            _23997 = NOVALUE;
L22: 

            /** 					sp = SymTab[s][S_NEXT]*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _23998 = (int)*(((s1_ptr)_2)->base + _s_44727);
            _2 = (int)SEQ_PTR(_23998);
            _sp_44728 = (int)*(((s1_ptr)_2)->base + 2);
            if (!IS_ATOM_INT(_sp_44728)){
                _sp_44728 = (long)DBL_PTR(_sp_44728)->dbl;
            }
            _23998 = NOVALUE;

            /** 					for p = 1 to SymTab[s][S_NUM_ARGS] do*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _24000 = (int)*(((s1_ptr)_2)->base + _s_44727);
            _2 = (int)SEQ_PTR(_24000);
            if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
                _24001 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
            }
            else{
                _24001 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
            }
            _24000 = NOVALUE;
            {
                int _p_44901;
                _p_44901 = 1;
L23: 
                if (binary_op_a(GREATER, _p_44901, _24001)){
                    goto L24; // [793] 869
                }

                /** 						c_puts("int _")*/
                RefDS(_24002);
                _53c_puts(_24002);

                /** 						c_puts(SymTab[sp][S_NAME])*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24003 = (int)*(((s1_ptr)_2)->base + _sp_44728);
                _2 = (int)SEQ_PTR(_24003);
                if (!IS_ATOM_INT(_25S_NAME_11913)){
                    _24004 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
                }
                else{
                    _24004 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
                }
                _24003 = NOVALUE;
                Ref(_24004);
                _53c_puts(_24004);
                _24004 = NOVALUE;

                /** 						if p != SymTab[s][S_NUM_ARGS] then*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24005 = (int)*(((s1_ptr)_2)->base + _s_44727);
                _2 = (int)SEQ_PTR(_24005);
                if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
                    _24006 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
                }
                else{
                    _24006 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
                }
                _24005 = NOVALUE;
                if (binary_op_a(EQUALS, _p_44901, _24006)){
                    _24006 = NOVALUE;
                    goto L25; // [836] 846
                }
                _24006 = NOVALUE;

                /** 							c_puts(", ")*/
                RefDS(_24008);
                _53c_puts(_24008);
L25: 

                /** 						sp = SymTab[sp][S_NEXT]*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24009 = (int)*(((s1_ptr)_2)->base + _sp_44728);
                _2 = (int)SEQ_PTR(_24009);
                _sp_44728 = (int)*(((s1_ptr)_2)->base + 2);
                if (!IS_ATOM_INT(_sp_44728)){
                    _sp_44728 = (long)DBL_PTR(_sp_44728)->dbl;
                }
                _24009 = NOVALUE;

                /** 					end for*/
                _0 = _p_44901;
                if (IS_ATOM_INT(_p_44901)) {
                    _p_44901 = _p_44901 + 1;
                    if ((long)((unsigned long)_p_44901 +(unsigned long) HIGH_BITS) >= 0){
                        _p_44901 = NewDouble((double)_p_44901);
                    }
                }
                else {
                    _p_44901 = binary_op_a(PLUS, _p_44901, 1);
                }
                DeRef(_0);
                goto L23; // [864] 800
L24: 
                ;
                DeRef(_p_44901);
            }

            /** 					c_puts(")\n")*/
            RefDS(_24011);
            _53c_puts(_24011);

            /** 					c_stmt0("{\n")*/
            RefDS(_22839);
            _56c_stmt0(_22839);

            /** 					NewBB(0, E_ALL_EFFECT, 0)*/
            _56NewBB(0, 1073741823, 0);

            /** 					Initializing = TRUE*/
            _25Initializing_12346 = _5TRUE_244;

            /** 					while sp do*/
L26: 
            if (_sp_44728 == 0)
            {
                goto L27; // [902] 1041
            }
            else{
            }

            /** 						integer scope = SymTab[sp][S_SCOPE]*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _24012 = (int)*(((s1_ptr)_2)->base + _sp_44728);
            _2 = (int)SEQ_PTR(_24012);
            _scope_44931 = (int)*(((s1_ptr)_2)->base + 4);
            if (!IS_ATOM_INT(_scope_44931)){
                _scope_44931 = (long)DBL_PTR(_scope_44931)->dbl;
            }
            _24012 = NOVALUE;

            /** 						switch scope with fallthru do*/
            _0 = _scope_44931;
            switch ( _0 ){ 

                /** 							case SC_LOOP_VAR, SC_UNDEFINED then*/
                case 2:
                case 9:

                /** 								break*/
                goto L28; // [936] 1018

                /** 							case SC_PRIVATE then*/
                case 3:

                /** 								c_stmt0("int ")*/
                RefDS(_23406);
                _56c_stmt0(_23406);

                /** 								c_puts("_")*/
                RefDS(_22754);
                _53c_puts(_22754);

                /** 								c_puts(SymTab[sp][S_NAME])*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24016 = (int)*(((s1_ptr)_2)->base + _sp_44728);
                _2 = (int)SEQ_PTR(_24016);
                if (!IS_ATOM_INT(_25S_NAME_11913)){
                    _24017 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
                }
                else{
                    _24017 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
                }
                _24016 = NOVALUE;
                Ref(_24017);
                _53c_puts(_24017);
                _24017 = NOVALUE;

                /** 								c_puts(" = NOVALUE;\n")*/
                RefDS(_23413);
                _53c_puts(_23413);

                /** 								target[MIN] = NOVALUE*/
                Ref(_25NOVALUE_12115);
                _2 = (int)SEQ_PTR(_57target_28321);
                if (!UNIQUE(_2)) {
                    _2 = (int)SequenceCopy((s1_ptr)_2);
                    _57target_28321 = MAKE_SEQ(_2);
                }
                _2 = (int)(((s1_ptr)_2)->base + 1);
                _1 = *(int *)_2;
                *(int *)_2 = _25NOVALUE_12115;
                DeRef(_1);

                /** 								target[MAX] = NOVALUE*/
                Ref(_25NOVALUE_12115);
                _2 = (int)SEQ_PTR(_57target_28321);
                if (!UNIQUE(_2)) {
                    _2 = (int)SequenceCopy((s1_ptr)_2);
                    _57target_28321 = MAKE_SEQ(_2);
                }
                _2 = (int)(((s1_ptr)_2)->base + 2);
                _1 = *(int *)_2;
                *(int *)_2 = _25NOVALUE_12115;
                DeRef(_1);

                /** 								RemoveFromBB( sp )*/
                _56RemoveFromBB(_sp_44728);

                /** 								break*/
                goto L28; // [1005] 1018

                /** 							case else*/
                default:

                /** 								exit*/
                goto L27; // [1015] 1041
            ;}L28: 

            /** 						sp = SymTab[sp][S_NEXT]*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _24018 = (int)*(((s1_ptr)_2)->base + _sp_44728);
            _2 = (int)SEQ_PTR(_24018);
            _sp_44728 = (int)*(((s1_ptr)_2)->base + 2);
            if (!IS_ATOM_INT(_sp_44728)){
                _sp_44728 = (long)DBL_PTR(_sp_44728)->dbl;
            }
            _24018 = NOVALUE;

            /** 					end while*/
            goto L26; // [1038] 902
L27: 

            /** 					temps = SymTab[s][S_TEMPS]*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _24020 = (int)*(((s1_ptr)_2)->base + _s_44727);
            _2 = (int)SEQ_PTR(_24020);
            if (!IS_ATOM_INT(_25S_TEMPS_11958)){
                _temps_44731 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TEMPS_11958)->dbl));
            }
            else{
                _temps_44731 = (int)*(((s1_ptr)_2)->base + _25S_TEMPS_11958);
            }
            if (!IS_ATOM_INT(_temps_44731)){
                _temps_44731 = (long)DBL_PTR(_temps_44731)->dbl;
            }
            _24020 = NOVALUE;

            /** 					sequence names = {}*/
            RefDS(_22682);
            DeRef(_names_44965);
            _names_44965 = _22682;

            /** 					while temps != 0 do*/
L29: 
            if (_temps_44731 == 0)
            goto L2A; // [1069] 1261

            /** 						if SymTab[temps][S_SCOPE] != DELETED then*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _24023 = (int)*(((s1_ptr)_2)->base + _temps_44731);
            _2 = (int)SEQ_PTR(_24023);
            _24024 = (int)*(((s1_ptr)_2)->base + 4);
            _24023 = NOVALUE;
            if (binary_op_a(EQUALS, _24024, 2)){
                _24024 = NOVALUE;
                goto L2B; // [1089] 1221
            }
            _24024 = NOVALUE;

            /** 							sequence name = sprintf("_%d", SymTab[temps][S_TEMP_NAME] )*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _24026 = (int)*(((s1_ptr)_2)->base + _temps_44731);
            _2 = (int)SEQ_PTR(_24026);
            _24027 = (int)*(((s1_ptr)_2)->base + 34);
            _24026 = NOVALUE;
            DeRefi(_name_44975);
            _name_44975 = EPrintf(-9999999, _22775, _24027);
            _24027 = NOVALUE;

            /** 							if temp_name_type[SymTab[temps][S_TEMP_NAME]][T_GTYPE]*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _24029 = (int)*(((s1_ptr)_2)->base + _temps_44731);
            _2 = (int)SEQ_PTR(_24029);
            _24030 = (int)*(((s1_ptr)_2)->base + 34);
            _24029 = NOVALUE;
            _2 = (int)SEQ_PTR(_25temp_name_type_12348);
            if (!IS_ATOM_INT(_24030)){
                _24031 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_24030)->dbl));
            }
            else{
                _24031 = (int)*(((s1_ptr)_2)->base + _24030);
            }
            _2 = (int)SEQ_PTR(_24031);
            _24032 = (int)*(((s1_ptr)_2)->base + 1);
            _24031 = NOVALUE;
            if (IS_ATOM_INT(_24032)) {
                _24033 = (_24032 != 0);
            }
            else {
                _24033 = binary_op(NOTEQ, _24032, 0);
            }
            _24032 = NOVALUE;
            if (IS_ATOM_INT(_24033)) {
                if (_24033 == 0) {
                    goto L2C; // [1143] 1217
                }
            }
            else {
                if (DBL_PTR(_24033)->dbl == 0.0) {
                    goto L2C; // [1143] 1217
                }
            }
            _24035 = find_from(_name_44975, _names_44965, 1);
            _24036 = (_24035 == 0);
            _24035 = NOVALUE;
            if (_24036 == 0)
            {
                DeRef(_24036);
                _24036 = NOVALUE;
                goto L2C; // [1156] 1217
            }
            else{
                DeRef(_24036);
                _24036 = NOVALUE;
            }

            /** 								c_stmt0("int ")*/
            RefDS(_23406);
            _56c_stmt0(_23406);

            /** 								c_puts( name )*/
            RefDS(_name_44975);
            _53c_puts(_name_44975);

            /** 								c_puts(" = NOVALUE")*/
            RefDS(_24037);
            _53c_puts(_24037);

            /** 								target = {NOVALUE, NOVALUE}*/
            Ref(_25NOVALUE_12115);
            Ref(_25NOVALUE_12115);
            DeRef(_57target_28321);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _25NOVALUE_12115;
            ((int *)_2)[2] = _25NOVALUE_12115;
            _57target_28321 = MAKE_SEQ(_1);

            /** 								SetBBType(temps, TYPE_INTEGER, target, TYPE_OBJECT, 0)*/
            RefDS(_57target_28321);
            _56SetBBType(_temps_44731, 1, _57target_28321, 16, 0);

            /** 								ifdef DEBUG then*/

            /** 									c_puts(";\n")*/
            RefDS(_22912);
            _53c_puts(_22912);

            /** 								names = prepend( names, name )*/
            RefDS(_name_44975);
            Prepend(&_names_44965, _names_44965, _name_44975);
            goto L2D; // [1214] 1220
L2C: 

            /** 								ifdef DEBUG then*/
L2D: 
L2B: 
            DeRefi(_name_44975);
            _name_44975 = NOVALUE;

            /** 						SymTab[temps][S_GTYPE] = TYPE_OBJECT*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _26SymTab_11138 = MAKE_SEQ(_2);
            }
            _3 = (int)(_temps_44731 + ((s1_ptr)_2)->base);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 36);
            _1 = *(int *)_2;
            *(int *)_2 = 16;
            DeRef(_1);
            _24042 = NOVALUE;

            /** 						temps = SymTab[temps][S_NEXT]*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _24044 = (int)*(((s1_ptr)_2)->base + _temps_44731);
            _2 = (int)SEQ_PTR(_24044);
            _temps_44731 = (int)*(((s1_ptr)_2)->base + 2);
            if (!IS_ATOM_INT(_temps_44731)){
                _temps_44731 = (long)DBL_PTR(_temps_44731)->dbl;
            }
            _24044 = NOVALUE;

            /** 					end while*/
            goto L29; // [1258] 1069
L2A: 

            /** 					Initializing = FALSE*/
            _25Initializing_12346 = _5FALSE_242;

            /** 					if SymTab[s][S_LHS_SUBS2] then*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _24046 = (int)*(((s1_ptr)_2)->base + _s_44727);
            _2 = (int)SEQ_PTR(_24046);
            _24047 = (int)*(((s1_ptr)_2)->base + 37);
            _24046 = NOVALUE;
            if (_24047 == 0) {
                _24047 = NOVALUE;
                goto L2E; // [1284] 1295
            }
            else {
                if (!IS_ATOM_INT(_24047) && DBL_PTR(_24047)->dbl == 0.0){
                    _24047 = NOVALUE;
                    goto L2E; // [1284] 1295
                }
                _24047 = NOVALUE;
            }
            _24047 = NOVALUE;

            /** 						c_stmt0("int _0, _1, _2, _3;\n\n")*/
            RefDS(_24048);
            _56c_stmt0(_24048);
            goto L2F; // [1292] 1301
L2E: 

            /** 						c_stmt0("int _0, _1, _2;\n\n")*/
            RefDS(_24049);
            _56c_stmt0(_24049);
L2F: 

            /** 					sp = SymTab[s][S_NEXT]*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _24050 = (int)*(((s1_ptr)_2)->base + _s_44727);
            _2 = (int)SEQ_PTR(_24050);
            _sp_44728 = (int)*(((s1_ptr)_2)->base + 2);
            if (!IS_ATOM_INT(_sp_44728)){
                _sp_44728 = (long)DBL_PTR(_sp_44728)->dbl;
            }
            _24050 = NOVALUE;

            /** 					for p = 1 to SymTab[s][S_NUM_ARGS] do*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _24052 = (int)*(((s1_ptr)_2)->base + _s_44727);
            _2 = (int)SEQ_PTR(_24052);
            if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
                _24053 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
            }
            else{
                _24053 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
            }
            _24052 = NOVALUE;
            {
                int _p_45034;
                _p_45034 = 1;
L30: 
                if (binary_op_a(GREATER, _p_45034, _24053)){
                    goto L31; // [1331] 1682
                }

                /** 						SymTab[sp][S_ONE_REF] = FALSE*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                if (!UNIQUE(_2)) {
                    _2 = (int)SequenceCopy((s1_ptr)_2);
                    _26SymTab_11138 = MAKE_SEQ(_2);
                }
                _3 = (int)(_sp_44728 + ((s1_ptr)_2)->base);
                _2 = (int)SEQ_PTR(*(int *)_3);
                if (!UNIQUE(_2)) {
                    _2 = (int)SequenceCopy((s1_ptr)_2);
                    *(int *)_3 = MAKE_SEQ(_2);
                }
                _2 = (int)(((s1_ptr)_2)->base + 35);
                _1 = *(int *)_2;
                *(int *)_2 = _5FALSE_242;
                DeRef(_1);
                _24054 = NOVALUE;

                /** 						if SymTab[sp][S_ARG_TYPE] = TYPE_SEQUENCE then*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24056 = (int)*(((s1_ptr)_2)->base + _sp_44728);
                _2 = (int)SEQ_PTR(_24056);
                _24057 = (int)*(((s1_ptr)_2)->base + 43);
                _24056 = NOVALUE;
                if (binary_op_a(NOTEQ, _24057, 8)){
                    _24057 = NOVALUE;
                    goto L32; // [1371] 1435
                }
                _24057 = NOVALUE;

                /** 							target[MIN] = SymTab[sp][S_ARG_SEQ_LEN]*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24059 = (int)*(((s1_ptr)_2)->base + _sp_44728);
                _2 = (int)SEQ_PTR(_24059);
                _24060 = (int)*(((s1_ptr)_2)->base + 51);
                _24059 = NOVALUE;
                Ref(_24060);
                _2 = (int)SEQ_PTR(_57target_28321);
                if (!UNIQUE(_2)) {
                    _2 = (int)SequenceCopy((s1_ptr)_2);
                    _57target_28321 = MAKE_SEQ(_2);
                }
                _2 = (int)(((s1_ptr)_2)->base + 1);
                _1 = *(int *)_2;
                *(int *)_2 = _24060;
                if( _1 != _24060 ){
                    DeRef(_1);
                }
                _24060 = NOVALUE;

                /** 							SetBBType(sp, SymTab[sp][S_ARG_TYPE], target,*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24061 = (int)*(((s1_ptr)_2)->base + _sp_44728);
                _2 = (int)SEQ_PTR(_24061);
                _24062 = (int)*(((s1_ptr)_2)->base + 43);
                _24061 = NOVALUE;
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24063 = (int)*(((s1_ptr)_2)->base + _sp_44728);
                _2 = (int)SEQ_PTR(_24063);
                _24064 = (int)*(((s1_ptr)_2)->base + 45);
                _24063 = NOVALUE;
                Ref(_24062);
                RefDS(_57target_28321);
                Ref(_24064);
                _56SetBBType(_sp_44728, _24062, _57target_28321, _24064, 0);
                _24062 = NOVALUE;
                _24064 = NOVALUE;
                goto L33; // [1432] 1659
L32: 

                /** 						elsif SymTab[sp][S_ARG_TYPE] = TYPE_INTEGER then*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24065 = (int)*(((s1_ptr)_2)->base + _sp_44728);
                _2 = (int)SEQ_PTR(_24065);
                _24066 = (int)*(((s1_ptr)_2)->base + 43);
                _24065 = NOVALUE;
                if (binary_op_a(NOTEQ, _24066, 1)){
                    _24066 = NOVALUE;
                    goto L34; // [1451] 1575
                }
                _24066 = NOVALUE;

                /** 							if SymTab[sp][S_ARG_MIN] = NOVALUE then*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24068 = (int)*(((s1_ptr)_2)->base + _sp_44728);
                _2 = (int)SEQ_PTR(_24068);
                _24069 = (int)*(((s1_ptr)_2)->base + 47);
                _24068 = NOVALUE;
                if (binary_op_a(NOTEQ, _24069, _25NOVALUE_12115)){
                    _24069 = NOVALUE;
                    goto L35; // [1471] 1502
                }
                _24069 = NOVALUE;

                /** 								target[MIN] = MININT*/
                _2 = (int)SEQ_PTR(_57target_28321);
                if (!UNIQUE(_2)) {
                    _2 = (int)SequenceCopy((s1_ptr)_2);
                    _57target_28321 = MAKE_SEQ(_2);
                }
                _2 = (int)(((s1_ptr)_2)->base + 1);
                _1 = *(int *)_2;
                *(int *)_2 = -1073741824;
                DeRef(_1);

                /** 								target[MAX] = MAXINT*/
                _2 = (int)SEQ_PTR(_57target_28321);
                if (!UNIQUE(_2)) {
                    _2 = (int)SequenceCopy((s1_ptr)_2);
                    _57target_28321 = MAKE_SEQ(_2);
                }
                _2 = (int)(((s1_ptr)_2)->base + 2);
                _1 = *(int *)_2;
                *(int *)_2 = 1073741823;
                DeRef(_1);
                goto L36; // [1499] 1547
L35: 

                /** 								target[MIN] = SymTab[sp][S_ARG_MIN]*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24071 = (int)*(((s1_ptr)_2)->base + _sp_44728);
                _2 = (int)SEQ_PTR(_24071);
                _24072 = (int)*(((s1_ptr)_2)->base + 47);
                _24071 = NOVALUE;
                Ref(_24072);
                _2 = (int)SEQ_PTR(_57target_28321);
                if (!UNIQUE(_2)) {
                    _2 = (int)SequenceCopy((s1_ptr)_2);
                    _57target_28321 = MAKE_SEQ(_2);
                }
                _2 = (int)(((s1_ptr)_2)->base + 1);
                _1 = *(int *)_2;
                *(int *)_2 = _24072;
                if( _1 != _24072 ){
                    DeRef(_1);
                }
                _24072 = NOVALUE;

                /** 								target[MAX] = SymTab[sp][S_ARG_MAX]*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24073 = (int)*(((s1_ptr)_2)->base + _sp_44728);
                _2 = (int)SEQ_PTR(_24073);
                _24074 = (int)*(((s1_ptr)_2)->base + 48);
                _24073 = NOVALUE;
                Ref(_24074);
                _2 = (int)SEQ_PTR(_57target_28321);
                if (!UNIQUE(_2)) {
                    _2 = (int)SequenceCopy((s1_ptr)_2);
                    _57target_28321 = MAKE_SEQ(_2);
                }
                _2 = (int)(((s1_ptr)_2)->base + 2);
                _1 = *(int *)_2;
                *(int *)_2 = _24074;
                if( _1 != _24074 ){
                    DeRef(_1);
                }
                _24074 = NOVALUE;
L36: 

                /** 							SetBBType(sp, SymTab[sp][S_ARG_TYPE], target, TYPE_OBJECT, 0)*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24075 = (int)*(((s1_ptr)_2)->base + _sp_44728);
                _2 = (int)SEQ_PTR(_24075);
                _24076 = (int)*(((s1_ptr)_2)->base + 43);
                _24075 = NOVALUE;
                Ref(_24076);
                RefDS(_57target_28321);
                _56SetBBType(_sp_44728, _24076, _57target_28321, 16, 0);
                _24076 = NOVALUE;
                goto L33; // [1572] 1659
L34: 

                /** 						elsif SymTab[sp][S_ARG_TYPE] = TYPE_OBJECT then*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24077 = (int)*(((s1_ptr)_2)->base + _sp_44728);
                _2 = (int)SEQ_PTR(_24077);
                _24078 = (int)*(((s1_ptr)_2)->base + 43);
                _24077 = NOVALUE;
                if (binary_op_a(NOTEQ, _24078, 16)){
                    _24078 = NOVALUE;
                    goto L37; // [1591] 1633
                }
                _24078 = NOVALUE;

                /** 							SetBBType(sp, SymTab[sp][S_ARG_TYPE], novalue,*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24080 = (int)*(((s1_ptr)_2)->base + _sp_44728);
                _2 = (int)SEQ_PTR(_24080);
                _24081 = (int)*(((s1_ptr)_2)->base + 43);
                _24080 = NOVALUE;
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24082 = (int)*(((s1_ptr)_2)->base + _sp_44728);
                _2 = (int)SEQ_PTR(_24082);
                _24083 = (int)*(((s1_ptr)_2)->base + 45);
                _24082 = NOVALUE;
                Ref(_24081);
                RefDS(_53novalue_46495);
                Ref(_24083);
                _56SetBBType(_sp_44728, _24081, _53novalue_46495, _24083, 0);
                _24081 = NOVALUE;
                _24083 = NOVALUE;
                goto L33; // [1630] 1659
L37: 

                /** 							SetBBType(sp, SymTab[sp][S_ARG_TYPE], novalue, TYPE_OBJECT, 0)*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24084 = (int)*(((s1_ptr)_2)->base + _sp_44728);
                _2 = (int)SEQ_PTR(_24084);
                _24085 = (int)*(((s1_ptr)_2)->base + 43);
                _24084 = NOVALUE;
                Ref(_24085);
                RefDS(_53novalue_46495);
                _56SetBBType(_sp_44728, _24085, _53novalue_46495, 16, 0);
                _24085 = NOVALUE;
L33: 

                /** 						sp = SymTab[sp][S_NEXT]*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24086 = (int)*(((s1_ptr)_2)->base + _sp_44728);
                _2 = (int)SEQ_PTR(_24086);
                _sp_44728 = (int)*(((s1_ptr)_2)->base + 2);
                if (!IS_ATOM_INT(_sp_44728)){
                    _sp_44728 = (long)DBL_PTR(_sp_44728)->dbl;
                }
                _24086 = NOVALUE;

                /** 					end for*/
                _0 = _p_45034;
                if (IS_ATOM_INT(_p_45034)) {
                    _p_45034 = _p_45034 + 1;
                    if ((long)((unsigned long)_p_45034 +(unsigned long) HIGH_BITS) >= 0){
                        _p_45034 = NewDouble((double)_p_45034);
                    }
                }
                else {
                    _p_45034 = binary_op_a(PLUS, _p_45034, 1);
                }
                DeRef(_0);
                goto L30; // [1677] 1338
L31: 
                ;
                DeRef(_p_45034);
            }

            /** 					call_proc(Execute_id, {s})*/
            _1 = NewS1(1);
            _2 = (int)((s1_ptr)_1)->base;
            *((int *)(_2+4)) = _s_44727;
            _24088 = MAKE_SEQ(_1);
            _1 = (int)SEQ_PTR(_24088);
            _2 = (int)((s1_ptr)_1)->base;
            _0 = (int)_00[_25Execute_id_12354].addr;
            Ref(*(int *)(_2+4));
            (*(int (*)())_0)(
                                *(int *)(_2+4)
                                 );
            DeRefDS(_24088);
            _24088 = NOVALUE;

            /** 					c_puts("    ;\n}\n")*/
            RefDS(_24089);
            _53c_puts(_24089);

            /** 					if TUNIX and dll_option and is_exported( s ) then*/
            if (_36TUNIX_14725 == 0) {
                _24090 = 0;
                goto L38; // [1702] 1712
            }
            _24090 = (_56dll_option_42654 != 0);
L38: 
            if (_24090 == 0) {
                goto L39; // [1712] 2035
            }
            _24092 = _56is_exported(_s_44727);
            if (_24092 == 0) {
                DeRef(_24092);
                _24092 = NOVALUE;
                goto L39; // [1721] 2035
            }
            else {
                if (!IS_ATOM_INT(_24092) && DBL_PTR(_24092)->dbl == 0.0){
                    DeRef(_24092);
                    _24092 = NOVALUE;
                    goto L39; // [1721] 2035
                }
                DeRef(_24092);
                _24092 = NOVALUE;
            }
            DeRef(_24092);
            _24092 = NOVALUE;

            /** 						LeftSym = TRUE*/
            _56LeftSym_42651 = _5TRUE_244;

            /** 						if TOSX then*/
            if (_36TOSX_14729 == 0)
            {
                goto L3A; // [1737] 1997
            }
            else{
            }

            /** 							c_stmt0( ret_type & SymTab[s][S_NAME] & " (" )*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _24093 = (int)*(((s1_ptr)_2)->base + _s_44727);
            _2 = (int)SEQ_PTR(_24093);
            if (!IS_ATOM_INT(_25S_NAME_11913)){
                _24094 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
            }
            else{
                _24094 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
            }
            _24093 = NOVALUE;
            {
                int concat_list[3];

                concat_list[0] = _24095;
                concat_list[1] = _24094;
                concat_list[2] = _ret_type_44860;
                Concat_N((object_ptr)&_24096, concat_list, 3);
            }
            _24094 = NOVALUE;
            _56c_stmt0(_24096);
            _24096 = NOVALUE;

            /** 							sp = SymTab[s][S_NEXT]*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _24097 = (int)*(((s1_ptr)_2)->base + _s_44727);
            _2 = (int)SEQ_PTR(_24097);
            _sp_44728 = (int)*(((s1_ptr)_2)->base + 2);
            if (!IS_ATOM_INT(_sp_44728)){
                _sp_44728 = (long)DBL_PTR(_sp_44728)->dbl;
            }
            _24097 = NOVALUE;

            /** 							for p = 1 to SymTab[s][S_NUM_ARGS] do*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _24099 = (int)*(((s1_ptr)_2)->base + _s_44727);
            _2 = (int)SEQ_PTR(_24099);
            if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
                _24100 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
            }
            else{
                _24100 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
            }
            _24099 = NOVALUE;
            {
                int _p_45155;
                _p_45155 = 1;
L3B: 
                if (binary_op_a(GREATER, _p_45155, _24100)){
                    goto L3C; // [1795] 1871
                }

                /** 								c_puts("int _")*/
                RefDS(_24002);
                _53c_puts(_24002);

                /** 								c_puts(SymTab[sp][S_NAME])*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24101 = (int)*(((s1_ptr)_2)->base + _sp_44728);
                _2 = (int)SEQ_PTR(_24101);
                if (!IS_ATOM_INT(_25S_NAME_11913)){
                    _24102 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
                }
                else{
                    _24102 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
                }
                _24101 = NOVALUE;
                Ref(_24102);
                _53c_puts(_24102);
                _24102 = NOVALUE;

                /** 								if p != SymTab[s][S_NUM_ARGS] then*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24103 = (int)*(((s1_ptr)_2)->base + _s_44727);
                _2 = (int)SEQ_PTR(_24103);
                if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
                    _24104 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
                }
                else{
                    _24104 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
                }
                _24103 = NOVALUE;
                if (binary_op_a(EQUALS, _p_45155, _24104)){
                    _24104 = NOVALUE;
                    goto L3D; // [1838] 1848
                }
                _24104 = NOVALUE;

                /** 									c_puts(", ")*/
                RefDS(_24008);
                _53c_puts(_24008);
L3D: 

                /** 								sp = SymTab[sp][S_NEXT]*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24106 = (int)*(((s1_ptr)_2)->base + _sp_44728);
                _2 = (int)SEQ_PTR(_24106);
                _sp_44728 = (int)*(((s1_ptr)_2)->base + 2);
                if (!IS_ATOM_INT(_sp_44728)){
                    _sp_44728 = (long)DBL_PTR(_sp_44728)->dbl;
                }
                _24106 = NOVALUE;

                /** 							end for*/
                _0 = _p_45155;
                if (IS_ATOM_INT(_p_45155)) {
                    _p_45155 = _p_45155 + 1;
                    if ((long)((unsigned long)_p_45155 +(unsigned long) HIGH_BITS) >= 0){
                        _p_45155 = NewDouble((double)_p_45155);
                    }
                }
                else {
                    _p_45155 = binary_op_a(PLUS, _p_45155, 1);
                }
                DeRef(_0);
                goto L3B; // [1866] 1802
L3C: 
                ;
                DeRef(_p_45155);
            }

            /** 							c_puts( ") {\n")*/
            RefDS(_24108);
            _53c_puts(_24108);

            /** 							c_stmt("    return @(", s)*/
            RefDS(_24109);
            _56c_stmt(_24109, _s_44727, 0);

            /** 							sp = SymTab[s][S_NEXT]*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _24110 = (int)*(((s1_ptr)_2)->base + _s_44727);
            _2 = (int)SEQ_PTR(_24110);
            _sp_44728 = (int)*(((s1_ptr)_2)->base + 2);
            if (!IS_ATOM_INT(_sp_44728)){
                _sp_44728 = (long)DBL_PTR(_sp_44728)->dbl;
            }
            _24110 = NOVALUE;

            /** 							for p = 1 to SymTab[s][S_NUM_ARGS] do*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _24112 = (int)*(((s1_ptr)_2)->base + _s_44727);
            _2 = (int)SEQ_PTR(_24112);
            if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
                _24113 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
            }
            else{
                _24113 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
            }
            _24112 = NOVALUE;
            {
                int _p_45185;
                _p_45185 = 1;
L3E: 
                if (binary_op_a(GREATER, _p_45185, _24113)){
                    goto L3F; // [1913] 1989
                }

                /** 								c_puts("_")*/
                RefDS(_22754);
                _53c_puts(_22754);

                /** 								c_puts(SymTab[sp][S_NAME])*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24114 = (int)*(((s1_ptr)_2)->base + _sp_44728);
                _2 = (int)SEQ_PTR(_24114);
                if (!IS_ATOM_INT(_25S_NAME_11913)){
                    _24115 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
                }
                else{
                    _24115 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
                }
                _24114 = NOVALUE;
                Ref(_24115);
                _53c_puts(_24115);
                _24115 = NOVALUE;

                /** 								if p != SymTab[s][S_NUM_ARGS] then*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24116 = (int)*(((s1_ptr)_2)->base + _s_44727);
                _2 = (int)SEQ_PTR(_24116);
                if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
                    _24117 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
                }
                else{
                    _24117 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
                }
                _24116 = NOVALUE;
                if (binary_op_a(EQUALS, _p_45185, _24117)){
                    _24117 = NOVALUE;
                    goto L40; // [1956] 1966
                }
                _24117 = NOVALUE;

                /** 									c_puts(", ")*/
                RefDS(_24008);
                _53c_puts(_24008);
L40: 

                /** 								sp = SymTab[sp][S_NEXT]*/
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _24119 = (int)*(((s1_ptr)_2)->base + _sp_44728);
                _2 = (int)SEQ_PTR(_24119);
                _sp_44728 = (int)*(((s1_ptr)_2)->base + 2);
                if (!IS_ATOM_INT(_sp_44728)){
                    _sp_44728 = (long)DBL_PTR(_sp_44728)->dbl;
                }
                _24119 = NOVALUE;

                /** 							end for*/
                _0 = _p_45185;
                if (IS_ATOM_INT(_p_45185)) {
                    _p_45185 = _p_45185 + 1;
                    if ((long)((unsigned long)_p_45185 +(unsigned long) HIGH_BITS) >= 0){
                        _p_45185 = NewDouble((double)_p_45185);
                    }
                }
                else {
                    _p_45185 = binary_op_a(PLUS, _p_45185, 1);
                }
                DeRef(_0);
                goto L3E; // [1984] 1920
L3F: 
                ;
                DeRef(_p_45185);
            }

            /** 							c_puts( ");\n}\n" )	*/
            RefDS(_24121);
            _53c_puts(_24121);
            goto L41; // [1994] 2025
L3A: 

            /** 							c_stmt( ret_type & SymTab[s][S_NAME] & "() __attribute__ ((alias (\"@\")));\n", s )*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _24122 = (int)*(((s1_ptr)_2)->base + _s_44727);
            _2 = (int)SEQ_PTR(_24122);
            if (!IS_ATOM_INT(_25S_NAME_11913)){
                _24123 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
            }
            else{
                _24123 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
            }
            _24122 = NOVALUE;
            {
                int concat_list[3];

                concat_list[0] = _24124;
                concat_list[1] = _24123;
                concat_list[2] = _ret_type_44860;
                Concat_N((object_ptr)&_24125, concat_list, 3);
            }
            _24123 = NOVALUE;
            _56c_stmt(_24125, _s_44727, 0);
            _24125 = NOVALUE;
L41: 

            /** 						LeftSym = FALSE*/
            _56LeftSym_42651 = _5FALSE_242;
L39: 

            /** 					c_puts("\n\n" )*/
            RefDS(_22796);
            _53c_puts(_22796);
L14: 
            DeRefi(_ret_type_44860);
            _ret_type_44860 = NOVALUE;
            DeRef(_names_44965);
            _names_44965 = NOVALUE;

            /** 			end for*/
            _routine_no_44805 = _routine_no_44805 + 1;
            goto L12; // [2045] 359
L13: 
            ;
        }
L8: 
        DeRef(_these_routines_44802);
        _these_routines_44802 = NOVALUE;

        /** 	end for*/
        _file_no_44751 = _file_no_44751 + 1;
        goto L5; // [2055] 85
L6: 
        ;
    }

    /** end procedure*/
    DeRefi(_buff_44732);
    DeRef(_base_name_44733);
    DeRef(_long_c_file_44734);
    DeRef(_c_file_44735);
    DeRef(_23919);
    _23919 = NOVALUE;
    DeRef(_23928);
    _23928 = NOVALUE;
    DeRef(_23942);
    _23942 = NOVALUE;
    DeRef(_23953);
    _23953 = NOVALUE;
    DeRef(_23955);
    _23955 = NOVALUE;
    DeRef(_23957);
    _23957 = NOVALUE;
    _23963 = NOVALUE;
    DeRef(_23960);
    _23960 = NOVALUE;
    DeRef(_23965);
    _23965 = NOVALUE;
    _24001 = NOVALUE;
    _24030 = NOVALUE;
    _24053 = NOVALUE;
    DeRef(_24033);
    _24033 = NOVALUE;
    _24100 = NOVALUE;
    _24113 = NOVALUE;
    return;
    ;
}



// 0x9E19ED0A
