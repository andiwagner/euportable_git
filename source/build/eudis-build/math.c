// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _18abs(int _a_4380)
{
    int _t_4381 = NOVALUE;
    int _2191 = NOVALUE;
    int _2190 = NOVALUE;
    int _2189 = NOVALUE;
    int _2187 = NOVALUE;
    int _2185 = NOVALUE;
    int _2184 = NOVALUE;
    int _2182 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _2182 = IS_ATOM(_a_4380);
    if (_2182 == 0)
    {
        _2182 = NOVALUE;
        goto L1; // [6] 35
    }
    else{
        _2182 = NOVALUE;
    }

    /** 		if a >= 0 then*/
    if (binary_op_a(LESS, _a_4380, 0)){
        goto L2; // [11] 24
    }

    /** 			return a*/
    DeRef(_t_4381);
    return _a_4380;
    goto L3; // [21] 34
L2: 

    /** 			return - a*/
    if (IS_ATOM_INT(_a_4380)) {
        if ((unsigned long)_a_4380 == 0xC0000000)
        _2184 = (int)NewDouble((double)-0xC0000000);
        else
        _2184 = - _a_4380;
    }
    else {
        _2184 = unary_op(UMINUS, _a_4380);
    }
    DeRef(_a_4380);
    DeRef(_t_4381);
    return _2184;
L3: 
L1: 

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_4380)){
            _2185 = SEQ_PTR(_a_4380)->length;
    }
    else {
        _2185 = 1;
    }
    {
        int _i_4389;
        _i_4389 = 1;
L4: 
        if (_i_4389 > _2185){
            goto L5; // [40] 101
        }

        /** 		t = a[i]*/
        DeRef(_t_4381);
        _2 = (int)SEQ_PTR(_a_4380);
        _t_4381 = (int)*(((s1_ptr)_2)->base + _i_4389);
        Ref(_t_4381);

        /** 		if atom(t) then*/
        _2187 = IS_ATOM(_t_4381);
        if (_2187 == 0)
        {
            _2187 = NOVALUE;
            goto L6; // [58] 80
        }
        else{
            _2187 = NOVALUE;
        }

        /** 			if t < 0 then*/
        if (binary_op_a(GREATEREQ, _t_4381, 0)){
            goto L7; // [63] 94
        }

        /** 				a[i] = - t*/
        if (IS_ATOM_INT(_t_4381)) {
            if ((unsigned long)_t_4381 == 0xC0000000)
            _2189 = (int)NewDouble((double)-0xC0000000);
            else
            _2189 = - _t_4381;
        }
        else {
            _2189 = unary_op(UMINUS, _t_4381);
        }
        _2 = (int)SEQ_PTR(_a_4380);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _a_4380 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4389);
        _1 = *(int *)_2;
        *(int *)_2 = _2189;
        if( _1 != _2189 ){
            DeRef(_1);
        }
        _2189 = NOVALUE;
        goto L7; // [77] 94
L6: 

        /** 			a[i] = abs(t)*/
        Ref(_t_4381);
        DeRef(_2190);
        _2190 = _t_4381;
        _2191 = _18abs(_2190);
        _2190 = NOVALUE;
        _2 = (int)SEQ_PTR(_a_4380);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _a_4380 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4389);
        _1 = *(int *)_2;
        *(int *)_2 = _2191;
        if( _1 != _2191 ){
            DeRef(_1);
        }
        _2191 = NOVALUE;
L7: 

        /** 	end for*/
        _i_4389 = _i_4389 + 1;
        goto L4; // [96] 47
L5: 
        ;
    }

    /** 	return a*/
    DeRef(_t_4381);
    DeRef(_2184);
    _2184 = NOVALUE;
    return _a_4380;
    ;
}


int _18sign(int _a_4402)
{
    int _2194 = NOVALUE;
    int _2193 = NOVALUE;
    int _2192 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return (a > 0) - (a < 0)*/
    if (IS_ATOM_INT(_a_4402)) {
        _2192 = (_a_4402 > 0);
    }
    else {
        _2192 = binary_op(GREATER, _a_4402, 0);
    }
    if (IS_ATOM_INT(_a_4402)) {
        _2193 = (_a_4402 < 0);
    }
    else {
        _2193 = binary_op(LESS, _a_4402, 0);
    }
    if (IS_ATOM_INT(_2192) && IS_ATOM_INT(_2193)) {
        _2194 = _2192 - _2193;
        if ((long)((unsigned long)_2194 +(unsigned long) HIGH_BITS) >= 0){
            _2194 = NewDouble((double)_2194);
        }
    }
    else {
        _2194 = binary_op(MINUS, _2192, _2193);
    }
    DeRef(_2192);
    _2192 = NOVALUE;
    DeRef(_2193);
    _2193 = NOVALUE;
    DeRef(_a_4402);
    return _2194;
    ;
}


int _18larger_of(int _objA_4408, int _objB_4409)
{
    int _2195 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if compare(objA, objB) > 0 then*/
    if (IS_ATOM_INT(_objA_4408) && IS_ATOM_INT(_objB_4409)){
        _2195 = (_objA_4408 < _objB_4409) ? -1 : (_objA_4408 > _objB_4409);
    }
    else{
        _2195 = compare(_objA_4408, _objB_4409);
    }
    if (_2195 <= 0)
    goto L1; // [7] 20

    /** 		return objA*/
    DeRef(_objB_4409);
    return _objA_4408;
    goto L2; // [17] 27
L1: 

    /** 		return objB*/
    DeRef(_objA_4408);
    return _objB_4409;
L2: 
    ;
}


int _18smaller_of(int _objA_4416, int _objB_4417)
{
    int _2197 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if compare(objA, objB) < 0 then*/
    if (IS_ATOM_INT(_objA_4416) && IS_ATOM_INT(_objB_4417)){
        _2197 = (_objA_4416 < _objB_4417) ? -1 : (_objA_4416 > _objB_4417);
    }
    else{
        _2197 = compare(_objA_4416, _objB_4417);
    }
    if (_2197 >= 0)
    goto L1; // [7] 20

    /** 		return objA*/
    DeRef(_objB_4417);
    return _objA_4416;
    goto L2; // [17] 27
L1: 

    /** 		return objB*/
    DeRef(_objA_4416);
    return _objB_4417;
L2: 
    ;
}


int _18max(int _a_4424)
{
    int _b_4425 = NOVALUE;
    int _c_4426 = NOVALUE;
    int _2201 = NOVALUE;
    int _2200 = NOVALUE;
    int _2199 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _2199 = IS_ATOM(_a_4424);
    if (_2199 == 0)
    {
        _2199 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _2199 = NOVALUE;
    }

    /** 		return a*/
    DeRef(_b_4425);
    DeRef(_c_4426);
    return _a_4424;
L1: 

    /** 	b = mathcons:MINF*/
    RefDS(_20MINF_4358);
    DeRef(_b_4425);
    _b_4425 = _20MINF_4358;

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_4424)){
            _2200 = SEQ_PTR(_a_4424)->length;
    }
    else {
        _2200 = 1;
    }
    {
        int _i_4430;
        _i_4430 = 1;
L2: 
        if (_i_4430 > _2200){
            goto L3; // [28] 64
        }

        /** 		c = max(a[i])*/
        _2 = (int)SEQ_PTR(_a_4424);
        _2201 = (int)*(((s1_ptr)_2)->base + _i_4430);
        Ref(_2201);
        _0 = _c_4426;
        _c_4426 = _18max(_2201);
        DeRef(_0);
        _2201 = NOVALUE;

        /** 		if c > b then*/
        if (binary_op_a(LESSEQ, _c_4426, _b_4425)){
            goto L4; // [47] 57
        }

        /** 			b = c*/
        Ref(_c_4426);
        DeRef(_b_4425);
        _b_4425 = _c_4426;
L4: 

        /** 	end for*/
        _i_4430 = _i_4430 + 1;
        goto L2; // [59] 35
L3: 
        ;
    }

    /** 	return b*/
    DeRef(_a_4424);
    DeRef(_c_4426);
    return _b_4425;
    ;
}


int _18min(int _a_4438)
{
    int _b_4439 = NOVALUE;
    int _c_4440 = NOVALUE;
    int _2206 = NOVALUE;
    int _2205 = NOVALUE;
    int _2204 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _2204 = IS_ATOM(_a_4438);
    if (_2204 == 0)
    {
        _2204 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _2204 = NOVALUE;
    }

    /** 			return a*/
    DeRef(_b_4439);
    DeRef(_c_4440);
    return _a_4438;
L1: 

    /** 	b = mathcons:PINF*/
    RefDS(_20PINF_4355);
    DeRef(_b_4439);
    _b_4439 = _20PINF_4355;

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_4438)){
            _2205 = SEQ_PTR(_a_4438)->length;
    }
    else {
        _2205 = 1;
    }
    {
        int _i_4444;
        _i_4444 = 1;
L2: 
        if (_i_4444 > _2205){
            goto L3; // [28] 64
        }

        /** 		c = min(a[i])*/
        _2 = (int)SEQ_PTR(_a_4438);
        _2206 = (int)*(((s1_ptr)_2)->base + _i_4444);
        Ref(_2206);
        _0 = _c_4440;
        _c_4440 = _18min(_2206);
        DeRef(_0);
        _2206 = NOVALUE;

        /** 			if c < b then*/
        if (binary_op_a(GREATEREQ, _c_4440, _b_4439)){
            goto L4; // [47] 57
        }

        /** 				b = c*/
        Ref(_c_4440);
        DeRef(_b_4439);
        _b_4439 = _c_4440;
L4: 

        /** 	end for*/
        _i_4444 = _i_4444 + 1;
        goto L2; // [59] 35
L3: 
        ;
    }

    /** 	return b*/
    DeRef(_a_4438);
    DeRef(_c_4440);
    return _b_4439;
    ;
}


int _18ensure_in_range(int _item_4452, int _range_limits_4453)
{
    int _2220 = NOVALUE;
    int _2219 = NOVALUE;
    int _2217 = NOVALUE;
    int _2216 = NOVALUE;
    int _2215 = NOVALUE;
    int _2214 = NOVALUE;
    int _2212 = NOVALUE;
    int _2211 = NOVALUE;
    int _2209 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(range_limits) < 2 then*/
    if (IS_SEQUENCE(_range_limits_4453)){
            _2209 = SEQ_PTR(_range_limits_4453)->length;
    }
    else {
        _2209 = 1;
    }
    if (_2209 >= 2)
    goto L1; // [8] 19

    /** 		return item*/
    DeRefDS(_range_limits_4453);
    return _item_4452;
L1: 

    /** 	if eu:compare(item, range_limits[1]) < 0 then*/
    _2 = (int)SEQ_PTR(_range_limits_4453);
    _2211 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_item_4452) && IS_ATOM_INT(_2211)){
        _2212 = (_item_4452 < _2211) ? -1 : (_item_4452 > _2211);
    }
    else{
        _2212 = compare(_item_4452, _2211);
    }
    _2211 = NOVALUE;
    if (_2212 >= 0)
    goto L2; // [29] 44

    /** 		return range_limits[1]*/
    _2 = (int)SEQ_PTR(_range_limits_4453);
    _2214 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_2214);
    DeRef(_item_4452);
    DeRefDS(_range_limits_4453);
    return _2214;
L2: 

    /** 	if eu:compare(item, range_limits[$]) > 0 then*/
    if (IS_SEQUENCE(_range_limits_4453)){
            _2215 = SEQ_PTR(_range_limits_4453)->length;
    }
    else {
        _2215 = 1;
    }
    _2 = (int)SEQ_PTR(_range_limits_4453);
    _2216 = (int)*(((s1_ptr)_2)->base + _2215);
    if (IS_ATOM_INT(_item_4452) && IS_ATOM_INT(_2216)){
        _2217 = (_item_4452 < _2216) ? -1 : (_item_4452 > _2216);
    }
    else{
        _2217 = compare(_item_4452, _2216);
    }
    _2216 = NOVALUE;
    if (_2217 <= 0)
    goto L3; // [57] 75

    /** 		return range_limits[$]*/
    if (IS_SEQUENCE(_range_limits_4453)){
            _2219 = SEQ_PTR(_range_limits_4453)->length;
    }
    else {
        _2219 = 1;
    }
    _2 = (int)SEQ_PTR(_range_limits_4453);
    _2220 = (int)*(((s1_ptr)_2)->base + _2219);
    Ref(_2220);
    DeRef(_item_4452);
    DeRefDS(_range_limits_4453);
    _2214 = NOVALUE;
    return _2220;
L3: 

    /** 	return item*/
    DeRefDS(_range_limits_4453);
    _2214 = NOVALUE;
    _2220 = NOVALUE;
    return _item_4452;
    ;
}


int _18ensure_in_list(int _item_4471, int _list_4472, int _default_4473)
{
    int _2230 = NOVALUE;
    int _2229 = NOVALUE;
    int _2228 = NOVALUE;
    int _2227 = NOVALUE;
    int _2226 = NOVALUE;
    int _2225 = NOVALUE;
    int _2223 = NOVALUE;
    int _2221 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_default_4473)) {
        _1 = (long)(DBL_PTR(_default_4473)->dbl);
        if (UNIQUE(DBL_PTR(_default_4473)) && (DBL_PTR(_default_4473)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_default_4473);
        _default_4473 = _1;
    }

    /** 	if length(list) = 0 then*/
    if (IS_SEQUENCE(_list_4472)){
            _2221 = SEQ_PTR(_list_4472)->length;
    }
    else {
        _2221 = 1;
    }
    if (_2221 != 0)
    goto L1; // [10] 21

    /** 		return item*/
    DeRefDS(_list_4472);
    return _item_4471;
L1: 

    /** 	if find(item, list) = 0 then*/
    _2223 = find_from(_item_4471, _list_4472, 1);
    if (_2223 != 0)
    goto L2; // [28] 78

    /** 		if default>=1 and default<=length(list) then*/
    _2225 = (_default_4473 >= 1);
    if (_2225 == 0) {
        goto L3; // [38] 66
    }
    if (IS_SEQUENCE(_list_4472)){
            _2227 = SEQ_PTR(_list_4472)->length;
    }
    else {
        _2227 = 1;
    }
    _2228 = (_default_4473 <= _2227);
    _2227 = NOVALUE;
    if (_2228 == 0)
    {
        DeRef(_2228);
        _2228 = NOVALUE;
        goto L3; // [50] 66
    }
    else{
        DeRef(_2228);
        _2228 = NOVALUE;
    }

    /** 		    return list[default]*/
    _2 = (int)SEQ_PTR(_list_4472);
    _2229 = (int)*(((s1_ptr)_2)->base + _default_4473);
    Ref(_2229);
    DeRef(_item_4471);
    DeRefDS(_list_4472);
    DeRef(_2225);
    _2225 = NOVALUE;
    return _2229;
    goto L4; // [63] 77
L3: 

    /** 			return list[1]*/
    _2 = (int)SEQ_PTR(_list_4472);
    _2230 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_2230);
    DeRef(_item_4471);
    DeRefDS(_list_4472);
    DeRef(_2225);
    _2225 = NOVALUE;
    _2229 = NOVALUE;
    return _2230;
L4: 
L2: 

    /** 	return item*/
    DeRefDS(_list_4472);
    DeRef(_2225);
    _2225 = NOVALUE;
    _2229 = NOVALUE;
    _2230 = NOVALUE;
    return _item_4471;
    ;
}


int _18mod(int _x_4490, int _y_4491)
{
    int _sign_2__tmp_at2_4496 = NOVALUE;
    int _sign_1__tmp_at2_4495 = NOVALUE;
    int _sign_inlined_sign_at_2_4494 = NOVALUE;
    int _sign_2__tmp_at19_4500 = NOVALUE;
    int _sign_1__tmp_at19_4499 = NOVALUE;
    int _sign_inlined_sign_at_19_4498 = NOVALUE;
    int _2235 = NOVALUE;
    int _2234 = NOVALUE;
    int _2233 = NOVALUE;
    int _2232 = NOVALUE;
    int _2231 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if equal(sign(x), sign(y)) then*/

    /** 	return (a > 0) - (a < 0)*/
    DeRef(_sign_1__tmp_at2_4495);
    if (IS_ATOM_INT(_x_4490)) {
        _sign_1__tmp_at2_4495 = (_x_4490 > 0);
    }
    else {
        _sign_1__tmp_at2_4495 = binary_op(GREATER, _x_4490, 0);
    }
    DeRef(_sign_2__tmp_at2_4496);
    if (IS_ATOM_INT(_x_4490)) {
        _sign_2__tmp_at2_4496 = (_x_4490 < 0);
    }
    else {
        _sign_2__tmp_at2_4496 = binary_op(LESS, _x_4490, 0);
    }
    DeRef(_sign_inlined_sign_at_2_4494);
    if (IS_ATOM_INT(_sign_1__tmp_at2_4495) && IS_ATOM_INT(_sign_2__tmp_at2_4496)) {
        _sign_inlined_sign_at_2_4494 = _sign_1__tmp_at2_4495 - _sign_2__tmp_at2_4496;
        if ((long)((unsigned long)_sign_inlined_sign_at_2_4494 +(unsigned long) HIGH_BITS) >= 0){
            _sign_inlined_sign_at_2_4494 = NewDouble((double)_sign_inlined_sign_at_2_4494);
        }
    }
    else {
        _sign_inlined_sign_at_2_4494 = binary_op(MINUS, _sign_1__tmp_at2_4495, _sign_2__tmp_at2_4496);
    }
    DeRef(_sign_1__tmp_at2_4495);
    _sign_1__tmp_at2_4495 = NOVALUE;
    DeRef(_sign_2__tmp_at2_4496);
    _sign_2__tmp_at2_4496 = NOVALUE;

    /** 	return (a > 0) - (a < 0)*/
    DeRef(_sign_1__tmp_at19_4499);
    if (IS_ATOM_INT(_y_4491)) {
        _sign_1__tmp_at19_4499 = (_y_4491 > 0);
    }
    else {
        _sign_1__tmp_at19_4499 = binary_op(GREATER, _y_4491, 0);
    }
    DeRef(_sign_2__tmp_at19_4500);
    if (IS_ATOM_INT(_y_4491)) {
        _sign_2__tmp_at19_4500 = (_y_4491 < 0);
    }
    else {
        _sign_2__tmp_at19_4500 = binary_op(LESS, _y_4491, 0);
    }
    DeRef(_sign_inlined_sign_at_19_4498);
    if (IS_ATOM_INT(_sign_1__tmp_at19_4499) && IS_ATOM_INT(_sign_2__tmp_at19_4500)) {
        _sign_inlined_sign_at_19_4498 = _sign_1__tmp_at19_4499 - _sign_2__tmp_at19_4500;
        if ((long)((unsigned long)_sign_inlined_sign_at_19_4498 +(unsigned long) HIGH_BITS) >= 0){
            _sign_inlined_sign_at_19_4498 = NewDouble((double)_sign_inlined_sign_at_19_4498);
        }
    }
    else {
        _sign_inlined_sign_at_19_4498 = binary_op(MINUS, _sign_1__tmp_at19_4499, _sign_2__tmp_at19_4500);
    }
    DeRef(_sign_1__tmp_at19_4499);
    _sign_1__tmp_at19_4499 = NOVALUE;
    DeRef(_sign_2__tmp_at19_4500);
    _sign_2__tmp_at19_4500 = NOVALUE;
    if (_sign_inlined_sign_at_2_4494 == _sign_inlined_sign_at_19_4498)
    _2231 = 1;
    else if (IS_ATOM_INT(_sign_inlined_sign_at_2_4494) && IS_ATOM_INT(_sign_inlined_sign_at_19_4498))
    _2231 = 0;
    else
    _2231 = (compare(_sign_inlined_sign_at_2_4494, _sign_inlined_sign_at_19_4498) == 0);
    if (_2231 == 0)
    {
        _2231 = NOVALUE;
        goto L1; // [41] 55
    }
    else{
        _2231 = NOVALUE;
    }

    /** 		return remainder(x,y)*/
    if (IS_ATOM_INT(_x_4490) && IS_ATOM_INT(_y_4491)) {
        _2232 = (_x_4490 % _y_4491);
    }
    else {
        _2232 = binary_op(REMAINDER, _x_4490, _y_4491);
    }
    DeRef(_x_4490);
    DeRef(_y_4491);
    return _2232;
L1: 

    /** 	return x - y * floor(x / y)*/
    if (IS_ATOM_INT(_x_4490) && IS_ATOM_INT(_y_4491)) {
        if (_y_4491 > 0 && _x_4490 >= 0) {
            _2233 = _x_4490 / _y_4491;
        }
        else {
            temp_dbl = floor((double)_x_4490 / (double)_y_4491);
            if (_x_4490 != MININT)
            _2233 = (long)temp_dbl;
            else
            _2233 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_4490, _y_4491);
        _2233 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    if (IS_ATOM_INT(_y_4491) && IS_ATOM_INT(_2233)) {
        if (_y_4491 == (short)_y_4491 && _2233 <= INT15 && _2233 >= -INT15)
        _2234 = _y_4491 * _2233;
        else
        _2234 = NewDouble(_y_4491 * (double)_2233);
    }
    else {
        _2234 = binary_op(MULTIPLY, _y_4491, _2233);
    }
    DeRef(_2233);
    _2233 = NOVALUE;
    if (IS_ATOM_INT(_x_4490) && IS_ATOM_INT(_2234)) {
        _2235 = _x_4490 - _2234;
        if ((long)((unsigned long)_2235 +(unsigned long) HIGH_BITS) >= 0){
            _2235 = NewDouble((double)_2235);
        }
    }
    else {
        _2235 = binary_op(MINUS, _x_4490, _2234);
    }
    DeRef(_2234);
    _2234 = NOVALUE;
    DeRef(_x_4490);
    DeRef(_y_4491);
    DeRef(_2232);
    _2232 = NOVALUE;
    return _2235;
    ;
}


int _18trunc(int _x_4508)
{
    int _sign_2__tmp_at2_4512 = NOVALUE;
    int _sign_1__tmp_at2_4511 = NOVALUE;
    int _sign_inlined_sign_at_2_4510 = NOVALUE;
    int _2238 = NOVALUE;
    int _2237 = NOVALUE;
    int _2236 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return sign(x) * floor(abs(x))*/

    /** 	return (a > 0) - (a < 0)*/
    DeRef(_sign_1__tmp_at2_4511);
    if (IS_ATOM_INT(_x_4508)) {
        _sign_1__tmp_at2_4511 = (_x_4508 > 0);
    }
    else {
        _sign_1__tmp_at2_4511 = binary_op(GREATER, _x_4508, 0);
    }
    DeRef(_sign_2__tmp_at2_4512);
    if (IS_ATOM_INT(_x_4508)) {
        _sign_2__tmp_at2_4512 = (_x_4508 < 0);
    }
    else {
        _sign_2__tmp_at2_4512 = binary_op(LESS, _x_4508, 0);
    }
    DeRef(_sign_inlined_sign_at_2_4510);
    if (IS_ATOM_INT(_sign_1__tmp_at2_4511) && IS_ATOM_INT(_sign_2__tmp_at2_4512)) {
        _sign_inlined_sign_at_2_4510 = _sign_1__tmp_at2_4511 - _sign_2__tmp_at2_4512;
        if ((long)((unsigned long)_sign_inlined_sign_at_2_4510 +(unsigned long) HIGH_BITS) >= 0){
            _sign_inlined_sign_at_2_4510 = NewDouble((double)_sign_inlined_sign_at_2_4510);
        }
    }
    else {
        _sign_inlined_sign_at_2_4510 = binary_op(MINUS, _sign_1__tmp_at2_4511, _sign_2__tmp_at2_4512);
    }
    DeRef(_sign_1__tmp_at2_4511);
    _sign_1__tmp_at2_4511 = NOVALUE;
    DeRef(_sign_2__tmp_at2_4512);
    _sign_2__tmp_at2_4512 = NOVALUE;
    Ref(_x_4508);
    _2236 = _18abs(_x_4508);
    if (IS_ATOM_INT(_2236))
    _2237 = e_floor(_2236);
    else
    _2237 = unary_op(FLOOR, _2236);
    DeRef(_2236);
    _2236 = NOVALUE;
    if (IS_ATOM_INT(_sign_inlined_sign_at_2_4510) && IS_ATOM_INT(_2237)) {
        if (_sign_inlined_sign_at_2_4510 == (short)_sign_inlined_sign_at_2_4510 && _2237 <= INT15 && _2237 >= -INT15)
        _2238 = _sign_inlined_sign_at_2_4510 * _2237;
        else
        _2238 = NewDouble(_sign_inlined_sign_at_2_4510 * (double)_2237);
    }
    else {
        _2238 = binary_op(MULTIPLY, _sign_inlined_sign_at_2_4510, _2237);
    }
    DeRef(_2237);
    _2237 = NOVALUE;
    DeRef(_x_4508);
    return _2238;
    ;
}


int _18frac(int _x_4518)
{
    int _temp_4519 = NOVALUE;
    int _sign_2__tmp_at8_4524 = NOVALUE;
    int _sign_1__tmp_at8_4523 = NOVALUE;
    int _sign_inlined_sign_at_8_4522 = NOVALUE;
    int _2242 = NOVALUE;
    int _2241 = NOVALUE;
    int _2240 = NOVALUE;
    int _0, _1, _2;
    

    /** 	object temp = abs(x)*/
    Ref(_x_4518);
    _0 = _temp_4519;
    _temp_4519 = _18abs(_x_4518);
    DeRef(_0);

    /** 	return sign(x) * (temp - floor(temp))*/

    /** 	return (a > 0) - (a < 0)*/
    DeRef(_sign_1__tmp_at8_4523);
    if (IS_ATOM_INT(_x_4518)) {
        _sign_1__tmp_at8_4523 = (_x_4518 > 0);
    }
    else {
        _sign_1__tmp_at8_4523 = binary_op(GREATER, _x_4518, 0);
    }
    DeRef(_sign_2__tmp_at8_4524);
    if (IS_ATOM_INT(_x_4518)) {
        _sign_2__tmp_at8_4524 = (_x_4518 < 0);
    }
    else {
        _sign_2__tmp_at8_4524 = binary_op(LESS, _x_4518, 0);
    }
    DeRef(_sign_inlined_sign_at_8_4522);
    if (IS_ATOM_INT(_sign_1__tmp_at8_4523) && IS_ATOM_INT(_sign_2__tmp_at8_4524)) {
        _sign_inlined_sign_at_8_4522 = _sign_1__tmp_at8_4523 - _sign_2__tmp_at8_4524;
        if ((long)((unsigned long)_sign_inlined_sign_at_8_4522 +(unsigned long) HIGH_BITS) >= 0){
            _sign_inlined_sign_at_8_4522 = NewDouble((double)_sign_inlined_sign_at_8_4522);
        }
    }
    else {
        _sign_inlined_sign_at_8_4522 = binary_op(MINUS, _sign_1__tmp_at8_4523, _sign_2__tmp_at8_4524);
    }
    DeRef(_sign_1__tmp_at8_4523);
    _sign_1__tmp_at8_4523 = NOVALUE;
    DeRef(_sign_2__tmp_at8_4524);
    _sign_2__tmp_at8_4524 = NOVALUE;
    if (IS_ATOM_INT(_temp_4519))
    _2240 = e_floor(_temp_4519);
    else
    _2240 = unary_op(FLOOR, _temp_4519);
    if (IS_ATOM_INT(_temp_4519) && IS_ATOM_INT(_2240)) {
        _2241 = _temp_4519 - _2240;
        if ((long)((unsigned long)_2241 +(unsigned long) HIGH_BITS) >= 0){
            _2241 = NewDouble((double)_2241);
        }
    }
    else {
        _2241 = binary_op(MINUS, _temp_4519, _2240);
    }
    DeRef(_2240);
    _2240 = NOVALUE;
    if (IS_ATOM_INT(_sign_inlined_sign_at_8_4522) && IS_ATOM_INT(_2241)) {
        if (_sign_inlined_sign_at_8_4522 == (short)_sign_inlined_sign_at_8_4522 && _2241 <= INT15 && _2241 >= -INT15)
        _2242 = _sign_inlined_sign_at_8_4522 * _2241;
        else
        _2242 = NewDouble(_sign_inlined_sign_at_8_4522 * (double)_2241);
    }
    else {
        _2242 = binary_op(MULTIPLY, _sign_inlined_sign_at_8_4522, _2241);
    }
    DeRef(_2241);
    _2241 = NOVALUE;
    DeRef(_x_4518);
    DeRef(_temp_4519);
    return _2242;
    ;
}


int _18intdiv(int _a_4530, int _b_4531)
{
    int _sign_2__tmp_at2_4535 = NOVALUE;
    int _sign_1__tmp_at2_4534 = NOVALUE;
    int _sign_inlined_sign_at_2_4533 = NOVALUE;
    int _2247 = NOVALUE;
    int _2246 = NOVALUE;
    int _2245 = NOVALUE;
    int _2244 = NOVALUE;
    int _2243 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return sign(a)*ceil(abs(a)/abs(b))*/

    /** 	return (a > 0) - (a < 0)*/
    DeRef(_sign_1__tmp_at2_4534);
    if (IS_ATOM_INT(_a_4530)) {
        _sign_1__tmp_at2_4534 = (_a_4530 > 0);
    }
    else {
        _sign_1__tmp_at2_4534 = binary_op(GREATER, _a_4530, 0);
    }
    DeRef(_sign_2__tmp_at2_4535);
    if (IS_ATOM_INT(_a_4530)) {
        _sign_2__tmp_at2_4535 = (_a_4530 < 0);
    }
    else {
        _sign_2__tmp_at2_4535 = binary_op(LESS, _a_4530, 0);
    }
    DeRef(_sign_inlined_sign_at_2_4533);
    if (IS_ATOM_INT(_sign_1__tmp_at2_4534) && IS_ATOM_INT(_sign_2__tmp_at2_4535)) {
        _sign_inlined_sign_at_2_4533 = _sign_1__tmp_at2_4534 - _sign_2__tmp_at2_4535;
        if ((long)((unsigned long)_sign_inlined_sign_at_2_4533 +(unsigned long) HIGH_BITS) >= 0){
            _sign_inlined_sign_at_2_4533 = NewDouble((double)_sign_inlined_sign_at_2_4533);
        }
    }
    else {
        _sign_inlined_sign_at_2_4533 = binary_op(MINUS, _sign_1__tmp_at2_4534, _sign_2__tmp_at2_4535);
    }
    DeRef(_sign_1__tmp_at2_4534);
    _sign_1__tmp_at2_4534 = NOVALUE;
    DeRef(_sign_2__tmp_at2_4535);
    _sign_2__tmp_at2_4535 = NOVALUE;
    Ref(_a_4530);
    _2243 = _18abs(_a_4530);
    Ref(_b_4531);
    _2244 = _18abs(_b_4531);
    if (IS_ATOM_INT(_2243) && IS_ATOM_INT(_2244)) {
        _2245 = (_2243 % _2244) ? NewDouble((double)_2243 / _2244) : (_2243 / _2244);
    }
    else {
        _2245 = binary_op(DIVIDE, _2243, _2244);
    }
    DeRef(_2243);
    _2243 = NOVALUE;
    DeRef(_2244);
    _2244 = NOVALUE;
    _2246 = _18ceil(_2245);
    _2245 = NOVALUE;
    if (IS_ATOM_INT(_sign_inlined_sign_at_2_4533) && IS_ATOM_INT(_2246)) {
        if (_sign_inlined_sign_at_2_4533 == (short)_sign_inlined_sign_at_2_4533 && _2246 <= INT15 && _2246 >= -INT15)
        _2247 = _sign_inlined_sign_at_2_4533 * _2246;
        else
        _2247 = NewDouble(_sign_inlined_sign_at_2_4533 * (double)_2246);
    }
    else {
        _2247 = binary_op(MULTIPLY, _sign_inlined_sign_at_2_4533, _2246);
    }
    DeRef(_2246);
    _2246 = NOVALUE;
    DeRef(_a_4530);
    DeRef(_b_4531);
    return _2247;
    ;
}


int _18ceil(int _a_4544)
{
    int _2250 = NOVALUE;
    int _2249 = NOVALUE;
    int _2248 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return -floor(-a)*/
    if (IS_ATOM_INT(_a_4544)) {
        if ((unsigned long)_a_4544 == 0xC0000000)
        _2248 = (int)NewDouble((double)-0xC0000000);
        else
        _2248 = - _a_4544;
    }
    else {
        _2248 = unary_op(UMINUS, _a_4544);
    }
    if (IS_ATOM_INT(_2248))
    _2249 = e_floor(_2248);
    else
    _2249 = unary_op(FLOOR, _2248);
    DeRef(_2248);
    _2248 = NOVALUE;
    if (IS_ATOM_INT(_2249)) {
        if ((unsigned long)_2249 == 0xC0000000)
        _2250 = (int)NewDouble((double)-0xC0000000);
        else
        _2250 = - _2249;
    }
    else {
        _2250 = unary_op(UMINUS, _2249);
    }
    DeRef(_2249);
    _2249 = NOVALUE;
    DeRef(_a_4544);
    return _2250;
    ;
}


int _18round(int _a_4550, int _precision_4551)
{
    int _len_4552 = NOVALUE;
    int _s_4553 = NOVALUE;
    int _t_4554 = NOVALUE;
    int _u_4555 = NOVALUE;
    int _msg_inlined_crash_at_257_4606 = NOVALUE;
    int _2303 = NOVALUE;
    int _2302 = NOVALUE;
    int _2301 = NOVALUE;
    int _2300 = NOVALUE;
    int _2299 = NOVALUE;
    int _2298 = NOVALUE;
    int _2297 = NOVALUE;
    int _2296 = NOVALUE;
    int _2295 = NOVALUE;
    int _2294 = NOVALUE;
    int _2293 = NOVALUE;
    int _2291 = NOVALUE;
    int _2289 = NOVALUE;
    int _2285 = NOVALUE;
    int _2283 = NOVALUE;
    int _2282 = NOVALUE;
    int _2281 = NOVALUE;
    int _2280 = NOVALUE;
    int _2279 = NOVALUE;
    int _2278 = NOVALUE;
    int _2277 = NOVALUE;
    int _2276 = NOVALUE;
    int _2274 = NOVALUE;
    int _2271 = NOVALUE;
    int _2270 = NOVALUE;
    int _2269 = NOVALUE;
    int _2268 = NOVALUE;
    int _2267 = NOVALUE;
    int _2266 = NOVALUE;
    int _2265 = NOVALUE;
    int _2264 = NOVALUE;
    int _2263 = NOVALUE;
    int _2261 = NOVALUE;
    int _2258 = NOVALUE;
    int _2257 = NOVALUE;
    int _2256 = NOVALUE;
    int _2255 = NOVALUE;
    int _2253 = NOVALUE;
    int _2252 = NOVALUE;
    int _0, _1, _2;
    

    /** 	precision = abs(precision)*/
    Ref(_precision_4551);
    _0 = _precision_4551;
    _precision_4551 = _18abs(_precision_4551);
    DeRef(_0);

    /** 	if atom(a) then*/
    _2252 = IS_ATOM(_a_4550);
    if (_2252 == 0)
    {
        _2252 = NOVALUE;
        goto L1; // [12] 140
    }
    else{
        _2252 = NOVALUE;
    }

    /** 		if atom(precision) then*/
    _2253 = IS_ATOM(_precision_4551);
    if (_2253 == 0)
    {
        _2253 = NOVALUE;
        goto L2; // [20] 45
    }
    else{
        _2253 = NOVALUE;
    }

    /** 			return floor(0.5 + (a * precision )) / precision*/
    if (IS_ATOM_INT(_a_4550) && IS_ATOM_INT(_precision_4551)) {
        if (_a_4550 == (short)_a_4550 && _precision_4551 <= INT15 && _precision_4551 >= -INT15)
        _2255 = _a_4550 * _precision_4551;
        else
        _2255 = NewDouble(_a_4550 * (double)_precision_4551);
    }
    else {
        _2255 = binary_op(MULTIPLY, _a_4550, _precision_4551);
    }
    _2256 = binary_op(PLUS, _2254, _2255);
    DeRef(_2255);
    _2255 = NOVALUE;
    if (IS_ATOM_INT(_2256))
    _2257 = e_floor(_2256);
    else
    _2257 = unary_op(FLOOR, _2256);
    DeRef(_2256);
    _2256 = NOVALUE;
    if (IS_ATOM_INT(_2257) && IS_ATOM_INT(_precision_4551)) {
        _2258 = (_2257 % _precision_4551) ? NewDouble((double)_2257 / _precision_4551) : (_2257 / _precision_4551);
    }
    else {
        _2258 = binary_op(DIVIDE, _2257, _precision_4551);
    }
    DeRef(_2257);
    _2257 = NOVALUE;
    DeRef(_a_4550);
    DeRef(_precision_4551);
    DeRef(_s_4553);
    DeRef(_t_4554);
    DeRef(_u_4555);
    return _2258;
L2: 

    /** 		len = length(precision)*/
    if (IS_SEQUENCE(_precision_4551)){
            _len_4552 = SEQ_PTR(_precision_4551)->length;
    }
    else {
        _len_4552 = 1;
    }

    /** 		s = repeat(0, len)*/
    DeRef(_s_4553);
    _s_4553 = Repeat(0, _len_4552);

    /** 		for i = 1 to len do*/
    _2261 = _len_4552;
    {
        int _i_4569;
        _i_4569 = 1;
L3: 
        if (_i_4569 > _2261){
            goto L4; // [61] 131
        }

        /** 			t = precision[i]*/
        DeRef(_t_4554);
        _2 = (int)SEQ_PTR(_precision_4551);
        _t_4554 = (int)*(((s1_ptr)_2)->base + _i_4569);
        Ref(_t_4554);

        /** 			if atom (t) then*/
        _2263 = IS_ATOM(_t_4554);
        if (_2263 == 0)
        {
            _2263 = NOVALUE;
            goto L5; // [79] 106
        }
        else{
            _2263 = NOVALUE;
        }

        /** 				s[i] = floor( 0.5 + (a * t)) / t*/
        if (IS_ATOM_INT(_a_4550) && IS_ATOM_INT(_t_4554)) {
            if (_a_4550 == (short)_a_4550 && _t_4554 <= INT15 && _t_4554 >= -INT15)
            _2264 = _a_4550 * _t_4554;
            else
            _2264 = NewDouble(_a_4550 * (double)_t_4554);
        }
        else {
            _2264 = binary_op(MULTIPLY, _a_4550, _t_4554);
        }
        _2265 = binary_op(PLUS, _2254, _2264);
        DeRef(_2264);
        _2264 = NOVALUE;
        if (IS_ATOM_INT(_2265))
        _2266 = e_floor(_2265);
        else
        _2266 = unary_op(FLOOR, _2265);
        DeRef(_2265);
        _2265 = NOVALUE;
        if (IS_ATOM_INT(_2266) && IS_ATOM_INT(_t_4554)) {
            _2267 = (_2266 % _t_4554) ? NewDouble((double)_2266 / _t_4554) : (_2266 / _t_4554);
        }
        else {
            _2267 = binary_op(DIVIDE, _2266, _t_4554);
        }
        DeRef(_2266);
        _2266 = NOVALUE;
        _2 = (int)SEQ_PTR(_s_4553);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_4553 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4569);
        _1 = *(int *)_2;
        *(int *)_2 = _2267;
        if( _1 != _2267 ){
            DeRef(_1);
        }
        _2267 = NOVALUE;
        goto L6; // [103] 124
L5: 

        /** 				s[i] = round(a, t)*/
        Ref(_a_4550);
        DeRef(_2268);
        _2268 = _a_4550;
        Ref(_t_4554);
        DeRef(_2269);
        _2269 = _t_4554;
        _2270 = _18round(_2268, _2269);
        _2268 = NOVALUE;
        _2269 = NOVALUE;
        _2 = (int)SEQ_PTR(_s_4553);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_4553 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4569);
        _1 = *(int *)_2;
        *(int *)_2 = _2270;
        if( _1 != _2270 ){
            DeRef(_1);
        }
        _2270 = NOVALUE;
L6: 

        /** 		end for*/
        _i_4569 = _i_4569 + 1;
        goto L3; // [126] 68
L4: 
        ;
    }

    /** 		return s*/
    DeRef(_a_4550);
    DeRef(_precision_4551);
    DeRef(_t_4554);
    DeRef(_u_4555);
    DeRef(_2258);
    _2258 = NOVALUE;
    return _s_4553;
    goto L7; // [137] 242
L1: 

    /** 	elsif atom(precision) then*/
    _2271 = IS_ATOM(_precision_4551);
    if (_2271 == 0)
    {
        _2271 = NOVALUE;
        goto L8; // [145] 241
    }
    else{
        _2271 = NOVALUE;
    }

    /** 		len = length(a)*/
    if (IS_SEQUENCE(_a_4550)){
            _len_4552 = SEQ_PTR(_a_4550)->length;
    }
    else {
        _len_4552 = 1;
    }

    /** 		s = repeat(0, len)*/
    DeRef(_s_4553);
    _s_4553 = Repeat(0, _len_4552);

    /** 		for i = 1 to len do*/
    _2274 = _len_4552;
    {
        int _i_4587;
        _i_4587 = 1;
L9: 
        if (_i_4587 > _2274){
            goto LA; // [164] 234
        }

        /** 			t = a[i]*/
        DeRef(_t_4554);
        _2 = (int)SEQ_PTR(_a_4550);
        _t_4554 = (int)*(((s1_ptr)_2)->base + _i_4587);
        Ref(_t_4554);

        /** 			if atom(t) then*/
        _2276 = IS_ATOM(_t_4554);
        if (_2276 == 0)
        {
            _2276 = NOVALUE;
            goto LB; // [182] 209
        }
        else{
            _2276 = NOVALUE;
        }

        /** 				s[i] = floor(0.5 + (t * precision)) / precision*/
        if (IS_ATOM_INT(_t_4554) && IS_ATOM_INT(_precision_4551)) {
            if (_t_4554 == (short)_t_4554 && _precision_4551 <= INT15 && _precision_4551 >= -INT15)
            _2277 = _t_4554 * _precision_4551;
            else
            _2277 = NewDouble(_t_4554 * (double)_precision_4551);
        }
        else {
            _2277 = binary_op(MULTIPLY, _t_4554, _precision_4551);
        }
        _2278 = binary_op(PLUS, _2254, _2277);
        DeRef(_2277);
        _2277 = NOVALUE;
        if (IS_ATOM_INT(_2278))
        _2279 = e_floor(_2278);
        else
        _2279 = unary_op(FLOOR, _2278);
        DeRef(_2278);
        _2278 = NOVALUE;
        if (IS_ATOM_INT(_2279) && IS_ATOM_INT(_precision_4551)) {
            _2280 = (_2279 % _precision_4551) ? NewDouble((double)_2279 / _precision_4551) : (_2279 / _precision_4551);
        }
        else {
            _2280 = binary_op(DIVIDE, _2279, _precision_4551);
        }
        DeRef(_2279);
        _2279 = NOVALUE;
        _2 = (int)SEQ_PTR(_s_4553);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_4553 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4587);
        _1 = *(int *)_2;
        *(int *)_2 = _2280;
        if( _1 != _2280 ){
            DeRef(_1);
        }
        _2280 = NOVALUE;
        goto LC; // [206] 227
LB: 

        /** 				s[i] = round(t, precision)*/
        Ref(_t_4554);
        DeRef(_2281);
        _2281 = _t_4554;
        Ref(_precision_4551);
        DeRef(_2282);
        _2282 = _precision_4551;
        _2283 = _18round(_2281, _2282);
        _2281 = NOVALUE;
        _2282 = NOVALUE;
        _2 = (int)SEQ_PTR(_s_4553);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_4553 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4587);
        _1 = *(int *)_2;
        *(int *)_2 = _2283;
        if( _1 != _2283 ){
            DeRef(_1);
        }
        _2283 = NOVALUE;
LC: 

        /** 		end for*/
        _i_4587 = _i_4587 + 1;
        goto L9; // [229] 171
LA: 
        ;
    }

    /** 		return s*/
    DeRef(_a_4550);
    DeRef(_precision_4551);
    DeRef(_t_4554);
    DeRef(_u_4555);
    DeRef(_2258);
    _2258 = NOVALUE;
    return _s_4553;
L8: 
L7: 

    /** 	len = length(a)*/
    if (IS_SEQUENCE(_a_4550)){
            _len_4552 = SEQ_PTR(_a_4550)->length;
    }
    else {
        _len_4552 = 1;
    }

    /** 	if len != length(precision) then*/
    if (IS_SEQUENCE(_precision_4551)){
            _2285 = SEQ_PTR(_precision_4551)->length;
    }
    else {
        _2285 = 1;
    }
    if (_len_4552 == _2285)
    goto LD; // [252] 277

    /** 		error:crash("The lengths of the two supplied sequences do not match.")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_257_4606);
    _msg_inlined_crash_at_257_4606 = EPrintf(-9999999, _2287, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_257_4606);

    /** end procedure*/
    goto LE; // [271] 274
LE: 
    DeRefi(_msg_inlined_crash_at_257_4606);
    _msg_inlined_crash_at_257_4606 = NOVALUE;
LD: 

    /** 	s = repeat(0, len)*/
    DeRef(_s_4553);
    _s_4553 = Repeat(0, _len_4552);

    /** 	for i = 1 to len do*/
    _2289 = _len_4552;
    {
        int _i_4609;
        _i_4609 = 1;
LF: 
        if (_i_4609 > _2289){
            goto L10; // [288] 394
        }

        /** 		t = precision[i]*/
        DeRef(_t_4554);
        _2 = (int)SEQ_PTR(_precision_4551);
        _t_4554 = (int)*(((s1_ptr)_2)->base + _i_4609);
        Ref(_t_4554);

        /** 		if atom(t) then*/
        _2291 = IS_ATOM(_t_4554);
        if (_2291 == 0)
        {
            _2291 = NOVALUE;
            goto L11; // [306] 368
        }
        else{
            _2291 = NOVALUE;
        }

        /** 			u = a[i]*/
        DeRef(_u_4555);
        _2 = (int)SEQ_PTR(_a_4550);
        _u_4555 = (int)*(((s1_ptr)_2)->base + _i_4609);
        Ref(_u_4555);

        /** 			if atom(u) then*/
        _2293 = IS_ATOM(_u_4555);
        if (_2293 == 0)
        {
            _2293 = NOVALUE;
            goto L12; // [320] 347
        }
        else{
            _2293 = NOVALUE;
        }

        /** 				s[i] = floor(0.5 + (u * t)) / t*/
        if (IS_ATOM_INT(_u_4555) && IS_ATOM_INT(_t_4554)) {
            if (_u_4555 == (short)_u_4555 && _t_4554 <= INT15 && _t_4554 >= -INT15)
            _2294 = _u_4555 * _t_4554;
            else
            _2294 = NewDouble(_u_4555 * (double)_t_4554);
        }
        else {
            _2294 = binary_op(MULTIPLY, _u_4555, _t_4554);
        }
        _2295 = binary_op(PLUS, _2254, _2294);
        DeRef(_2294);
        _2294 = NOVALUE;
        if (IS_ATOM_INT(_2295))
        _2296 = e_floor(_2295);
        else
        _2296 = unary_op(FLOOR, _2295);
        DeRef(_2295);
        _2295 = NOVALUE;
        if (IS_ATOM_INT(_2296) && IS_ATOM_INT(_t_4554)) {
            _2297 = (_2296 % _t_4554) ? NewDouble((double)_2296 / _t_4554) : (_2296 / _t_4554);
        }
        else {
            _2297 = binary_op(DIVIDE, _2296, _t_4554);
        }
        DeRef(_2296);
        _2296 = NOVALUE;
        _2 = (int)SEQ_PTR(_s_4553);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_4553 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4609);
        _1 = *(int *)_2;
        *(int *)_2 = _2297;
        if( _1 != _2297 ){
            DeRef(_1);
        }
        _2297 = NOVALUE;
        goto L13; // [344] 387
L12: 

        /** 				s[i] = round(u, t)*/
        Ref(_u_4555);
        DeRef(_2298);
        _2298 = _u_4555;
        Ref(_t_4554);
        DeRef(_2299);
        _2299 = _t_4554;
        _2300 = _18round(_2298, _2299);
        _2298 = NOVALUE;
        _2299 = NOVALUE;
        _2 = (int)SEQ_PTR(_s_4553);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_4553 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4609);
        _1 = *(int *)_2;
        *(int *)_2 = _2300;
        if( _1 != _2300 ){
            DeRef(_1);
        }
        _2300 = NOVALUE;
        goto L13; // [365] 387
L11: 

        /** 			s[i] = round(a[i], t)*/
        _2 = (int)SEQ_PTR(_a_4550);
        _2301 = (int)*(((s1_ptr)_2)->base + _i_4609);
        Ref(_t_4554);
        DeRef(_2302);
        _2302 = _t_4554;
        Ref(_2301);
        _2303 = _18round(_2301, _2302);
        _2301 = NOVALUE;
        _2302 = NOVALUE;
        _2 = (int)SEQ_PTR(_s_4553);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_4553 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4609);
        _1 = *(int *)_2;
        *(int *)_2 = _2303;
        if( _1 != _2303 ){
            DeRef(_1);
        }
        _2303 = NOVALUE;
L13: 

        /** 	end for*/
        _i_4609 = _i_4609 + 1;
        goto LF; // [389] 295
L10: 
        ;
    }

    /** 	return s*/
    DeRef(_a_4550);
    DeRef(_precision_4551);
    DeRef(_t_4554);
    DeRef(_u_4555);
    DeRef(_2258);
    _2258 = NOVALUE;
    return _s_4553;
    ;
}


int _18arccos(int _x_4631)
{
    int _2311 = NOVALUE;
    int _2310 = NOVALUE;
    int _2309 = NOVALUE;
    int _2308 = NOVALUE;
    int _2307 = NOVALUE;
    int _2306 = NOVALUE;
    int _2305 = NOVALUE;
    int _2304 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return mathcons:HALFPI - 2 * arctan(x / (1.0 + sqrt(1.0 - x * x)))*/
    if (IS_ATOM_INT(_x_4631) && IS_ATOM_INT(_x_4631)) {
        if (_x_4631 == (short)_x_4631 && _x_4631 <= INT15 && _x_4631 >= -INT15)
        _2304 = _x_4631 * _x_4631;
        else
        _2304 = NewDouble(_x_4631 * (double)_x_4631);
    }
    else {
        _2304 = binary_op(MULTIPLY, _x_4631, _x_4631);
    }
    _2305 = binary_op(MINUS, _2121, _2304);
    DeRef(_2304);
    _2304 = NOVALUE;
    if (IS_ATOM_INT(_2305))
    _2306 = e_sqrt(_2305);
    else
    _2306 = unary_op(SQRT, _2305);
    DeRef(_2305);
    _2305 = NOVALUE;
    _2307 = binary_op(PLUS, _2121, _2306);
    DeRef(_2306);
    _2306 = NOVALUE;
    if (IS_ATOM_INT(_x_4631) && IS_ATOM_INT(_2307)) {
        _2308 = (_x_4631 % _2307) ? NewDouble((double)_x_4631 / _2307) : (_x_4631 / _2307);
    }
    else {
        _2308 = binary_op(DIVIDE, _x_4631, _2307);
    }
    DeRef(_2307);
    _2307 = NOVALUE;
    if (IS_ATOM_INT(_2308))
    _2309 = e_arctan(_2308);
    else
    _2309 = unary_op(ARCTAN, _2308);
    DeRef(_2308);
    _2308 = NOVALUE;
    if (IS_ATOM_INT(_2309) && IS_ATOM_INT(_2309)) {
        _2310 = _2309 + _2309;
        if ((long)((unsigned long)_2310 + (unsigned long)HIGH_BITS) >= 0) 
        _2310 = NewDouble((double)_2310);
    }
    else {
        _2310 = binary_op(PLUS, _2309, _2309);
    }
    DeRef(_2309);
    _2309 = NOVALUE;
    _2309 = NOVALUE;
    _2311 = binary_op(MINUS, _20HALFPI_4321, _2310);
    DeRef(_2310);
    _2310 = NOVALUE;
    DeRef(_x_4631);
    return _2311;
    ;
}


int _18arcsin(int _x_4642)
{
    int _2318 = NOVALUE;
    int _2317 = NOVALUE;
    int _2316 = NOVALUE;
    int _2315 = NOVALUE;
    int _2314 = NOVALUE;
    int _2313 = NOVALUE;
    int _2312 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return 2 * arctan(x / (1.0 + sqrt(1.0 - x * x)))*/
    if (IS_ATOM_INT(_x_4642) && IS_ATOM_INT(_x_4642)) {
        if (_x_4642 == (short)_x_4642 && _x_4642 <= INT15 && _x_4642 >= -INT15)
        _2312 = _x_4642 * _x_4642;
        else
        _2312 = NewDouble(_x_4642 * (double)_x_4642);
    }
    else {
        _2312 = binary_op(MULTIPLY, _x_4642, _x_4642);
    }
    _2313 = binary_op(MINUS, _2121, _2312);
    DeRef(_2312);
    _2312 = NOVALUE;
    if (IS_ATOM_INT(_2313))
    _2314 = e_sqrt(_2313);
    else
    _2314 = unary_op(SQRT, _2313);
    DeRef(_2313);
    _2313 = NOVALUE;
    _2315 = binary_op(PLUS, _2121, _2314);
    DeRef(_2314);
    _2314 = NOVALUE;
    if (IS_ATOM_INT(_x_4642) && IS_ATOM_INT(_2315)) {
        _2316 = (_x_4642 % _2315) ? NewDouble((double)_x_4642 / _2315) : (_x_4642 / _2315);
    }
    else {
        _2316 = binary_op(DIVIDE, _x_4642, _2315);
    }
    DeRef(_2315);
    _2315 = NOVALUE;
    if (IS_ATOM_INT(_2316))
    _2317 = e_arctan(_2316);
    else
    _2317 = unary_op(ARCTAN, _2316);
    DeRef(_2316);
    _2316 = NOVALUE;
    if (IS_ATOM_INT(_2317) && IS_ATOM_INT(_2317)) {
        _2318 = _2317 + _2317;
        if ((long)((unsigned long)_2318 + (unsigned long)HIGH_BITS) >= 0) 
        _2318 = NewDouble((double)_2318);
    }
    else {
        _2318 = binary_op(PLUS, _2317, _2317);
    }
    DeRef(_2317);
    _2317 = NOVALUE;
    _2317 = NOVALUE;
    DeRef(_x_4642);
    return _2318;
    ;
}


int _18atan2(int _y_4652, int _x_4653)
{
    int _2329 = NOVALUE;
    int _2328 = NOVALUE;
    int _2327 = NOVALUE;
    int _2326 = NOVALUE;
    int _2325 = NOVALUE;
    int _2324 = NOVALUE;
    int _2321 = NOVALUE;
    int _2320 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if x > 0 then*/
    if (binary_op_a(LESSEQ, _x_4653, 0)){
        goto L1; // [3] 23
    }

    /** 		return arctan(y/x)*/
    if (IS_ATOM_INT(_y_4652) && IS_ATOM_INT(_x_4653)) {
        _2320 = (_y_4652 % _x_4653) ? NewDouble((double)_y_4652 / _x_4653) : (_y_4652 / _x_4653);
    }
    else {
        if (IS_ATOM_INT(_y_4652)) {
            _2320 = NewDouble((double)_y_4652 / DBL_PTR(_x_4653)->dbl);
        }
        else {
            if (IS_ATOM_INT(_x_4653)) {
                _2320 = NewDouble(DBL_PTR(_y_4652)->dbl / (double)_x_4653);
            }
            else
            _2320 = NewDouble(DBL_PTR(_y_4652)->dbl / DBL_PTR(_x_4653)->dbl);
        }
    }
    if (IS_ATOM_INT(_2320))
    _2321 = e_arctan(_2320);
    else
    _2321 = unary_op(ARCTAN, _2320);
    DeRef(_2320);
    _2320 = NOVALUE;
    DeRef(_y_4652);
    DeRef(_x_4653);
    return _2321;
    goto L2; // [20] 113
L1: 

    /** 	elsif x < 0 then*/
    if (binary_op_a(GREATEREQ, _x_4653, 0)){
        goto L3; // [25] 76
    }

    /** 		if y < 0 then*/
    if (binary_op_a(GREATEREQ, _y_4652, 0)){
        goto L4; // [31] 55
    }

    /** 			return arctan(y/x) - mathcons:PI*/
    if (IS_ATOM_INT(_y_4652) && IS_ATOM_INT(_x_4653)) {
        _2324 = (_y_4652 % _x_4653) ? NewDouble((double)_y_4652 / _x_4653) : (_y_4652 / _x_4653);
    }
    else {
        if (IS_ATOM_INT(_y_4652)) {
            _2324 = NewDouble((double)_y_4652 / DBL_PTR(_x_4653)->dbl);
        }
        else {
            if (IS_ATOM_INT(_x_4653)) {
                _2324 = NewDouble(DBL_PTR(_y_4652)->dbl / (double)_x_4653);
            }
            else
            _2324 = NewDouble(DBL_PTR(_y_4652)->dbl / DBL_PTR(_x_4653)->dbl);
        }
    }
    if (IS_ATOM_INT(_2324))
    _2325 = e_arctan(_2324);
    else
    _2325 = unary_op(ARCTAN, _2324);
    DeRef(_2324);
    _2324 = NOVALUE;
    _2326 = NewDouble(DBL_PTR(_2325)->dbl - DBL_PTR(_20PI_4317)->dbl);
    DeRefDS(_2325);
    _2325 = NOVALUE;
    DeRef(_y_4652);
    DeRef(_x_4653);
    DeRef(_2321);
    _2321 = NOVALUE;
    return _2326;
    goto L2; // [52] 113
L4: 

    /** 			return arctan(y/x) + mathcons:PI*/
    if (IS_ATOM_INT(_y_4652) && IS_ATOM_INT(_x_4653)) {
        _2327 = (_y_4652 % _x_4653) ? NewDouble((double)_y_4652 / _x_4653) : (_y_4652 / _x_4653);
    }
    else {
        if (IS_ATOM_INT(_y_4652)) {
            _2327 = NewDouble((double)_y_4652 / DBL_PTR(_x_4653)->dbl);
        }
        else {
            if (IS_ATOM_INT(_x_4653)) {
                _2327 = NewDouble(DBL_PTR(_y_4652)->dbl / (double)_x_4653);
            }
            else
            _2327 = NewDouble(DBL_PTR(_y_4652)->dbl / DBL_PTR(_x_4653)->dbl);
        }
    }
    if (IS_ATOM_INT(_2327))
    _2328 = e_arctan(_2327);
    else
    _2328 = unary_op(ARCTAN, _2327);
    DeRef(_2327);
    _2327 = NOVALUE;
    _2329 = NewDouble(DBL_PTR(_2328)->dbl + DBL_PTR(_20PI_4317)->dbl);
    DeRefDS(_2328);
    _2328 = NOVALUE;
    DeRef(_y_4652);
    DeRef(_x_4653);
    DeRef(_2321);
    _2321 = NOVALUE;
    DeRef(_2326);
    _2326 = NOVALUE;
    return _2329;
    goto L2; // [73] 113
L3: 

    /** 	elsif y > 0 then*/
    if (binary_op_a(LESSEQ, _y_4652, 0)){
        goto L5; // [78] 91
    }

    /** 		return mathcons:HALFPI*/
    RefDS(_20HALFPI_4321);
    DeRef(_y_4652);
    DeRef(_x_4653);
    DeRef(_2321);
    _2321 = NOVALUE;
    DeRef(_2326);
    _2326 = NOVALUE;
    DeRef(_2329);
    _2329 = NOVALUE;
    return _20HALFPI_4321;
    goto L2; // [88] 113
L5: 

    /** 	elsif y < 0 then*/
    if (binary_op_a(GREATEREQ, _y_4652, 0)){
        goto L6; // [93] 106
    }

    /** 		return -(mathcons:HALFPI)*/
    RefDS(_2332);
    DeRef(_y_4652);
    DeRef(_x_4653);
    DeRef(_2321);
    _2321 = NOVALUE;
    DeRef(_2326);
    _2326 = NOVALUE;
    DeRef(_2329);
    _2329 = NOVALUE;
    return _2332;
    goto L2; // [103] 113
L6: 

    /** 		return 0*/
    DeRef(_y_4652);
    DeRef(_x_4653);
    DeRef(_2321);
    _2321 = NOVALUE;
    DeRef(_2326);
    _2326 = NOVALUE;
    DeRef(_2329);
    _2329 = NOVALUE;
    return 0;
L2: 
    ;
}


int _18rad2deg(int _x_4677)
{
    int _2333 = NOVALUE;
    int _0, _1, _2;
    

    /**    return x * mathcons:RADIANS_TO_DEGREES*/
    _2333 = binary_op(MULTIPLY, _x_4677, _20RADIANS_TO_DEGREES_4349);
    DeRef(_x_4677);
    return _2333;
    ;
}


int _18deg2rad(int _x_4681)
{
    int _2334 = NOVALUE;
    int _0, _1, _2;
    

    /**    return x * mathcons:DEGREES_TO_RADIANS*/
    _2334 = binary_op(MULTIPLY, _x_4681, _20DEGREES_TO_RADIANS_4347);
    DeRef(_x_4681);
    return _2334;
    ;
}


int _18log10(int _x1_4685)
{
    int _2336 = NOVALUE;
    int _2335 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return log(x1) * mathcons:INVLN10*/
    if (IS_ATOM_INT(_x1_4685))
    _2335 = e_log(_x1_4685);
    else
    _2335 = unary_op(LOG, _x1_4685);
    _2336 = binary_op(MULTIPLY, _2335, _20INVLN10_4339);
    DeRef(_2335);
    _2335 = NOVALUE;
    DeRef(_x1_4685);
    return _2336;
    ;
}


int _18exp(int _x_4690)
{
    int _2337 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return power( mathcons:E, x)*/
    if (IS_ATOM_INT(_x_4690)) {
        temp_d.dbl = (double)_x_4690;
        _2337 = Dpower(DBL_PTR(_20E_4331), &temp_d);
    }
    else
    _2337 = Dpower(DBL_PTR(_20E_4331), DBL_PTR(_x_4690));
    DeRef(_x_4690);
    return _2337;
    ;
}


int _18fib(int _i_4694)
{
    int _2341 = NOVALUE;
    int _2340 = NOVALUE;
    int _2339 = NOVALUE;
    int _2338 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_i_4694)) {
        _1 = (long)(DBL_PTR(_i_4694)->dbl);
        if (UNIQUE(DBL_PTR(_i_4694)) && (DBL_PTR(_i_4694)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_4694);
        _i_4694 = _1;
    }

    /** 	return floor((power( mathcons:PHI, i) / mathcons:SQRT5) + 0.5)*/
    temp_d.dbl = (double)_i_4694;
    _2338 = Dpower(DBL_PTR(_20PHI_4329), &temp_d);
    _2339 = NewDouble(DBL_PTR(_2338)->dbl / DBL_PTR(_20SQRT5_4360)->dbl);
    DeRefDS(_2338);
    _2338 = NOVALUE;
    _2340 = NewDouble(DBL_PTR(_2339)->dbl + DBL_PTR(_2254)->dbl);
    DeRefDS(_2339);
    _2339 = NOVALUE;
    _2341 = unary_op(FLOOR, _2340);
    DeRefDS(_2340);
    _2340 = NOVALUE;
    return _2341;
    ;
}


int _18cosh(int _a_4701)
{
    int _exp_inlined_exp_at_2_4703 = NOVALUE;
    int _exp_inlined_exp_at_15_4707 = NOVALUE;
    int _x_inlined_exp_at_12_4706 = NOVALUE;
    int _2344 = NOVALUE;
    int _2343 = NOVALUE;
    int _2342 = NOVALUE;
    int _0, _1, _2;
    

    /**     return (exp(a)+exp(-a))/2*/

    /** 	return power( mathcons:E, x)*/
    DeRef(_exp_inlined_exp_at_2_4703);
    _exp_inlined_exp_at_2_4703 = binary_op(POWER, _20E_4331, _a_4701);
    if (IS_ATOM_INT(_a_4701)) {
        if ((unsigned long)_a_4701 == 0xC0000000)
        _2342 = (int)NewDouble((double)-0xC0000000);
        else
        _2342 = - _a_4701;
    }
    else {
        _2342 = unary_op(UMINUS, _a_4701);
    }
    DeRef(_x_inlined_exp_at_12_4706);
    _x_inlined_exp_at_12_4706 = _2342;
    _2342 = NOVALUE;

    /** 	return power( mathcons:E, x)*/
    DeRef(_exp_inlined_exp_at_15_4707);
    _exp_inlined_exp_at_15_4707 = binary_op(POWER, _20E_4331, _x_inlined_exp_at_12_4706);
    DeRef(_x_inlined_exp_at_12_4706);
    _x_inlined_exp_at_12_4706 = NOVALUE;
    if (IS_ATOM_INT(_exp_inlined_exp_at_2_4703) && IS_ATOM_INT(_exp_inlined_exp_at_15_4707)) {
        _2343 = _exp_inlined_exp_at_2_4703 + _exp_inlined_exp_at_15_4707;
        if ((long)((unsigned long)_2343 + (unsigned long)HIGH_BITS) >= 0) 
        _2343 = NewDouble((double)_2343);
    }
    else {
        _2343 = binary_op(PLUS, _exp_inlined_exp_at_2_4703, _exp_inlined_exp_at_15_4707);
    }
    if (IS_ATOM_INT(_2343)) {
        if (_2343 & 1) {
            _2344 = NewDouble((_2343 >> 1) + 0.5);
        }
        else
        _2344 = _2343 >> 1;
    }
    else {
        _2344 = binary_op(DIVIDE, _2343, 2);
    }
    DeRef(_2343);
    _2343 = NOVALUE;
    DeRef(_a_4701);
    return _2344;
    ;
}


int _18sinh(int _a_4712)
{
    int _exp_inlined_exp_at_2_4714 = NOVALUE;
    int _exp_inlined_exp_at_15_4718 = NOVALUE;
    int _x_inlined_exp_at_12_4717 = NOVALUE;
    int _2347 = NOVALUE;
    int _2346 = NOVALUE;
    int _2345 = NOVALUE;
    int _0, _1, _2;
    

    /**     return (exp(a)-exp(-a))/2*/

    /** 	return power( mathcons:E, x)*/
    DeRef(_exp_inlined_exp_at_2_4714);
    _exp_inlined_exp_at_2_4714 = binary_op(POWER, _20E_4331, _a_4712);
    if (IS_ATOM_INT(_a_4712)) {
        if ((unsigned long)_a_4712 == 0xC0000000)
        _2345 = (int)NewDouble((double)-0xC0000000);
        else
        _2345 = - _a_4712;
    }
    else {
        _2345 = unary_op(UMINUS, _a_4712);
    }
    DeRef(_x_inlined_exp_at_12_4717);
    _x_inlined_exp_at_12_4717 = _2345;
    _2345 = NOVALUE;

    /** 	return power( mathcons:E, x)*/
    DeRef(_exp_inlined_exp_at_15_4718);
    _exp_inlined_exp_at_15_4718 = binary_op(POWER, _20E_4331, _x_inlined_exp_at_12_4717);
    DeRef(_x_inlined_exp_at_12_4717);
    _x_inlined_exp_at_12_4717 = NOVALUE;
    if (IS_ATOM_INT(_exp_inlined_exp_at_2_4714) && IS_ATOM_INT(_exp_inlined_exp_at_15_4718)) {
        _2346 = _exp_inlined_exp_at_2_4714 - _exp_inlined_exp_at_15_4718;
        if ((long)((unsigned long)_2346 +(unsigned long) HIGH_BITS) >= 0){
            _2346 = NewDouble((double)_2346);
        }
    }
    else {
        _2346 = binary_op(MINUS, _exp_inlined_exp_at_2_4714, _exp_inlined_exp_at_15_4718);
    }
    if (IS_ATOM_INT(_2346)) {
        if (_2346 & 1) {
            _2347 = NewDouble((_2346 >> 1) + 0.5);
        }
        else
        _2347 = _2346 >> 1;
    }
    else {
        _2347 = binary_op(DIVIDE, _2346, 2);
    }
    DeRef(_2346);
    _2346 = NOVALUE;
    DeRef(_a_4712);
    return _2347;
    ;
}


int _18tanh(int _a_4723)
{
    int _2350 = NOVALUE;
    int _2349 = NOVALUE;
    int _2348 = NOVALUE;
    int _0, _1, _2;
    

    /**     return sinh(a)/cosh(a)*/
    Ref(_a_4723);
    _2348 = _18sinh(_a_4723);
    Ref(_a_4723);
    _2349 = _18cosh(_a_4723);
    if (IS_ATOM_INT(_2348) && IS_ATOM_INT(_2349)) {
        _2350 = (_2348 % _2349) ? NewDouble((double)_2348 / _2349) : (_2348 / _2349);
    }
    else {
        _2350 = binary_op(DIVIDE, _2348, _2349);
    }
    DeRef(_2348);
    _2348 = NOVALUE;
    DeRef(_2349);
    _2349 = NOVALUE;
    DeRef(_a_4723);
    return _2350;
    ;
}


int _18arcsinh(int _a_4729)
{
    int _2355 = NOVALUE;
    int _2354 = NOVALUE;
    int _2353 = NOVALUE;
    int _2352 = NOVALUE;
    int _2351 = NOVALUE;
    int _0, _1, _2;
    

    /**     return log(a+sqrt(1+a*a))*/
    if (IS_ATOM_INT(_a_4729) && IS_ATOM_INT(_a_4729)) {
        if (_a_4729 == (short)_a_4729 && _a_4729 <= INT15 && _a_4729 >= -INT15)
        _2351 = _a_4729 * _a_4729;
        else
        _2351 = NewDouble(_a_4729 * (double)_a_4729);
    }
    else {
        _2351 = binary_op(MULTIPLY, _a_4729, _a_4729);
    }
    if (IS_ATOM_INT(_2351)) {
        _2352 = _2351 + 1;
        if (_2352 > MAXINT){
            _2352 = NewDouble((double)_2352);
        }
    }
    else
    _2352 = binary_op(PLUS, 1, _2351);
    DeRef(_2351);
    _2351 = NOVALUE;
    if (IS_ATOM_INT(_2352))
    _2353 = e_sqrt(_2352);
    else
    _2353 = unary_op(SQRT, _2352);
    DeRef(_2352);
    _2352 = NOVALUE;
    if (IS_ATOM_INT(_a_4729) && IS_ATOM_INT(_2353)) {
        _2354 = _a_4729 + _2353;
        if ((long)((unsigned long)_2354 + (unsigned long)HIGH_BITS) >= 0) 
        _2354 = NewDouble((double)_2354);
    }
    else {
        _2354 = binary_op(PLUS, _a_4729, _2353);
    }
    DeRef(_2353);
    _2353 = NOVALUE;
    if (IS_ATOM_INT(_2354))
    _2355 = e_log(_2354);
    else
    _2355 = unary_op(LOG, _2354);
    DeRef(_2354);
    _2354 = NOVALUE;
    DeRef(_a_4729);
    return _2355;
    ;
}


int _18arccosh(int _a_4750)
{
    int _2366 = NOVALUE;
    int _2365 = NOVALUE;
    int _2364 = NOVALUE;
    int _2363 = NOVALUE;
    int _2362 = NOVALUE;
    int _0, _1, _2;
    

    /**     return log(a+sqrt(a*a-1))*/
    if (IS_ATOM_INT(_a_4750) && IS_ATOM_INT(_a_4750)) {
        if (_a_4750 == (short)_a_4750 && _a_4750 <= INT15 && _a_4750 >= -INT15)
        _2362 = _a_4750 * _a_4750;
        else
        _2362 = NewDouble(_a_4750 * (double)_a_4750);
    }
    else {
        _2362 = binary_op(MULTIPLY, _a_4750, _a_4750);
    }
    if (IS_ATOM_INT(_2362)) {
        _2363 = _2362 - 1;
        if ((long)((unsigned long)_2363 +(unsigned long) HIGH_BITS) >= 0){
            _2363 = NewDouble((double)_2363);
        }
    }
    else {
        _2363 = binary_op(MINUS, _2362, 1);
    }
    DeRef(_2362);
    _2362 = NOVALUE;
    if (IS_ATOM_INT(_2363))
    _2364 = e_sqrt(_2363);
    else
    _2364 = unary_op(SQRT, _2363);
    DeRef(_2363);
    _2363 = NOVALUE;
    if (IS_ATOM_INT(_a_4750) && IS_ATOM_INT(_2364)) {
        _2365 = _a_4750 + _2364;
        if ((long)((unsigned long)_2365 + (unsigned long)HIGH_BITS) >= 0) 
        _2365 = NewDouble((double)_2365);
    }
    else {
        _2365 = binary_op(PLUS, _a_4750, _2364);
    }
    DeRef(_2364);
    _2364 = NOVALUE;
    if (IS_ATOM_INT(_2365))
    _2366 = e_log(_2365);
    else
    _2366 = unary_op(LOG, _2365);
    DeRef(_2365);
    _2365 = NOVALUE;
    DeRef(_a_4750);
    return _2366;
    ;
}


int _18arctanh(int _a_4774)
{
    int _2380 = NOVALUE;
    int _2379 = NOVALUE;
    int _2378 = NOVALUE;
    int _2377 = NOVALUE;
    int _2376 = NOVALUE;
    int _0, _1, _2;
    

    /**     return log((1+a)/(1-a))/2*/
    if (IS_ATOM_INT(_a_4774)) {
        _2376 = _a_4774 + 1;
        if (_2376 > MAXINT){
            _2376 = NewDouble((double)_2376);
        }
    }
    else
    _2376 = binary_op(PLUS, 1, _a_4774);
    if (IS_ATOM_INT(_a_4774)) {
        _2377 = 1 - _a_4774;
        if ((long)((unsigned long)_2377 +(unsigned long) HIGH_BITS) >= 0){
            _2377 = NewDouble((double)_2377);
        }
    }
    else {
        _2377 = binary_op(MINUS, 1, _a_4774);
    }
    if (IS_ATOM_INT(_2376) && IS_ATOM_INT(_2377)) {
        _2378 = (_2376 % _2377) ? NewDouble((double)_2376 / _2377) : (_2376 / _2377);
    }
    else {
        _2378 = binary_op(DIVIDE, _2376, _2377);
    }
    DeRef(_2376);
    _2376 = NOVALUE;
    DeRef(_2377);
    _2377 = NOVALUE;
    if (IS_ATOM_INT(_2378))
    _2379 = e_log(_2378);
    else
    _2379 = unary_op(LOG, _2378);
    DeRef(_2378);
    _2378 = NOVALUE;
    if (IS_ATOM_INT(_2379)) {
        if (_2379 & 1) {
            _2380 = NewDouble((_2379 >> 1) + 0.5);
        }
        else
        _2380 = _2379 >> 1;
    }
    else {
        _2380 = binary_op(DIVIDE, _2379, 2);
    }
    DeRef(_2379);
    _2379 = NOVALUE;
    DeRef(_a_4774);
    return _2380;
    ;
}


int _18sum(int _a_4782)
{
    int _b_4783 = NOVALUE;
    int _2388 = NOVALUE;
    int _2387 = NOVALUE;
    int _2385 = NOVALUE;
    int _2384 = NOVALUE;
    int _2383 = NOVALUE;
    int _2382 = NOVALUE;
    int _2381 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _2381 = IS_ATOM(_a_4782);
    if (_2381 == 0)
    {
        _2381 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _2381 = NOVALUE;
    }

    /** 		return a*/
    DeRef(_b_4783);
    return _a_4782;
L1: 

    /** 	b = 0*/
    DeRef(_b_4783);
    _b_4783 = 0;

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_4782)){
            _2382 = SEQ_PTR(_a_4782)->length;
    }
    else {
        _2382 = 1;
    }
    {
        int _i_4787;
        _i_4787 = 1;
L2: 
        if (_i_4787 > _2382){
            goto L3; // [26] 80
        }

        /** 		if atom(a[i]) then*/
        _2 = (int)SEQ_PTR(_a_4782);
        _2383 = (int)*(((s1_ptr)_2)->base + _i_4787);
        _2384 = IS_ATOM(_2383);
        _2383 = NOVALUE;
        if (_2384 == 0)
        {
            _2384 = NOVALUE;
            goto L4; // [42] 58
        }
        else{
            _2384 = NOVALUE;
        }

        /** 			b += a[i]*/
        _2 = (int)SEQ_PTR(_a_4782);
        _2385 = (int)*(((s1_ptr)_2)->base + _i_4787);
        _0 = _b_4783;
        if (IS_ATOM_INT(_b_4783) && IS_ATOM_INT(_2385)) {
            _b_4783 = _b_4783 + _2385;
            if ((long)((unsigned long)_b_4783 + (unsigned long)HIGH_BITS) >= 0) 
            _b_4783 = NewDouble((double)_b_4783);
        }
        else {
            _b_4783 = binary_op(PLUS, _b_4783, _2385);
        }
        DeRef(_0);
        _2385 = NOVALUE;
        goto L5; // [55] 73
L4: 

        /** 			b += sum(a[i])*/
        _2 = (int)SEQ_PTR(_a_4782);
        _2387 = (int)*(((s1_ptr)_2)->base + _i_4787);
        Ref(_2387);
        _2388 = _18sum(_2387);
        _2387 = NOVALUE;
        _0 = _b_4783;
        if (IS_ATOM_INT(_b_4783) && IS_ATOM_INT(_2388)) {
            _b_4783 = _b_4783 + _2388;
            if ((long)((unsigned long)_b_4783 + (unsigned long)HIGH_BITS) >= 0) 
            _b_4783 = NewDouble((double)_b_4783);
        }
        else {
            _b_4783 = binary_op(PLUS, _b_4783, _2388);
        }
        DeRef(_0);
        DeRef(_2388);
        _2388 = NOVALUE;
L5: 

        /** 	end for*/
        _i_4787 = _i_4787 + 1;
        goto L2; // [75] 33
L3: 
        ;
    }

    /** 	return b*/
    DeRef(_a_4782);
    return _b_4783;
    ;
}


int _18product(int _a_4800)
{
    int _b_4801 = NOVALUE;
    int _2397 = NOVALUE;
    int _2396 = NOVALUE;
    int _2394 = NOVALUE;
    int _2393 = NOVALUE;
    int _2392 = NOVALUE;
    int _2391 = NOVALUE;
    int _2390 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _2390 = IS_ATOM(_a_4800);
    if (_2390 == 0)
    {
        _2390 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _2390 = NOVALUE;
    }

    /** 		return a*/
    DeRef(_b_4801);
    return _a_4800;
L1: 

    /** 	b = 1*/
    DeRef(_b_4801);
    _b_4801 = 1;

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_4800)){
            _2391 = SEQ_PTR(_a_4800)->length;
    }
    else {
        _2391 = 1;
    }
    {
        int _i_4805;
        _i_4805 = 1;
L2: 
        if (_i_4805 > _2391){
            goto L3; // [26] 80
        }

        /** 		if atom(a[i]) then*/
        _2 = (int)SEQ_PTR(_a_4800);
        _2392 = (int)*(((s1_ptr)_2)->base + _i_4805);
        _2393 = IS_ATOM(_2392);
        _2392 = NOVALUE;
        if (_2393 == 0)
        {
            _2393 = NOVALUE;
            goto L4; // [42] 58
        }
        else{
            _2393 = NOVALUE;
        }

        /** 			b *= a[i]*/
        _2 = (int)SEQ_PTR(_a_4800);
        _2394 = (int)*(((s1_ptr)_2)->base + _i_4805);
        _0 = _b_4801;
        if (IS_ATOM_INT(_b_4801) && IS_ATOM_INT(_2394)) {
            if (_b_4801 == (short)_b_4801 && _2394 <= INT15 && _2394 >= -INT15)
            _b_4801 = _b_4801 * _2394;
            else
            _b_4801 = NewDouble(_b_4801 * (double)_2394);
        }
        else {
            _b_4801 = binary_op(MULTIPLY, _b_4801, _2394);
        }
        DeRef(_0);
        _2394 = NOVALUE;
        goto L5; // [55] 73
L4: 

        /** 			b *= product(a[i])*/
        _2 = (int)SEQ_PTR(_a_4800);
        _2396 = (int)*(((s1_ptr)_2)->base + _i_4805);
        Ref(_2396);
        _2397 = _18product(_2396);
        _2396 = NOVALUE;
        _0 = _b_4801;
        if (IS_ATOM_INT(_b_4801) && IS_ATOM_INT(_2397)) {
            if (_b_4801 == (short)_b_4801 && _2397 <= INT15 && _2397 >= -INT15)
            _b_4801 = _b_4801 * _2397;
            else
            _b_4801 = NewDouble(_b_4801 * (double)_2397);
        }
        else {
            _b_4801 = binary_op(MULTIPLY, _b_4801, _2397);
        }
        DeRef(_0);
        DeRef(_2397);
        _2397 = NOVALUE;
L5: 

        /** 	end for*/
        _i_4805 = _i_4805 + 1;
        goto L2; // [75] 33
L3: 
        ;
    }

    /** 	return b*/
    DeRef(_a_4800);
    return _b_4801;
    ;
}


int _18or_all(int _a_4818)
{
    int _b_4819 = NOVALUE;
    int _2406 = NOVALUE;
    int _2405 = NOVALUE;
    int _2403 = NOVALUE;
    int _2402 = NOVALUE;
    int _2401 = NOVALUE;
    int _2400 = NOVALUE;
    int _2399 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _2399 = IS_ATOM(_a_4818);
    if (_2399 == 0)
    {
        _2399 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _2399 = NOVALUE;
    }

    /** 		return a*/
    DeRef(_b_4819);
    return _a_4818;
L1: 

    /** 	b = 0*/
    DeRef(_b_4819);
    _b_4819 = 0;

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_4818)){
            _2400 = SEQ_PTR(_a_4818)->length;
    }
    else {
        _2400 = 1;
    }
    {
        int _i_4823;
        _i_4823 = 1;
L2: 
        if (_i_4823 > _2400){
            goto L3; // [26] 80
        }

        /** 		if atom(a[i]) then*/
        _2 = (int)SEQ_PTR(_a_4818);
        _2401 = (int)*(((s1_ptr)_2)->base + _i_4823);
        _2402 = IS_ATOM(_2401);
        _2401 = NOVALUE;
        if (_2402 == 0)
        {
            _2402 = NOVALUE;
            goto L4; // [42] 58
        }
        else{
            _2402 = NOVALUE;
        }

        /** 			b = or_bits(b, a[i])*/
        _2 = (int)SEQ_PTR(_a_4818);
        _2403 = (int)*(((s1_ptr)_2)->base + _i_4823);
        _0 = _b_4819;
        if (IS_ATOM_INT(_b_4819) && IS_ATOM_INT(_2403)) {
            {unsigned long tu;
                 tu = (unsigned long)_b_4819 | (unsigned long)_2403;
                 _b_4819 = MAKE_UINT(tu);
            }
        }
        else {
            _b_4819 = binary_op(OR_BITS, _b_4819, _2403);
        }
        DeRef(_0);
        _2403 = NOVALUE;
        goto L5; // [55] 73
L4: 

        /** 			b = or_bits(b, or_all(a[i]))*/
        _2 = (int)SEQ_PTR(_a_4818);
        _2405 = (int)*(((s1_ptr)_2)->base + _i_4823);
        Ref(_2405);
        _2406 = _18or_all(_2405);
        _2405 = NOVALUE;
        _0 = _b_4819;
        if (IS_ATOM_INT(_b_4819) && IS_ATOM_INT(_2406)) {
            {unsigned long tu;
                 tu = (unsigned long)_b_4819 | (unsigned long)_2406;
                 _b_4819 = MAKE_UINT(tu);
            }
        }
        else {
            _b_4819 = binary_op(OR_BITS, _b_4819, _2406);
        }
        DeRef(_0);
        DeRef(_2406);
        _2406 = NOVALUE;
L5: 

        /** 	end for*/
        _i_4823 = _i_4823 + 1;
        goto L2; // [75] 33
L3: 
        ;
    }

    /** 	return b*/
    DeRef(_a_4818);
    return _b_4819;
    ;
}


int _18shift_bits(int _source_number_4836, int _shift_distance_4837)
{
    int _lSigned_4855 = NOVALUE;
    int _2430 = NOVALUE;
    int _2428 = NOVALUE;
    int _2427 = NOVALUE;
    int _2426 = NOVALUE;
    int _2425 = NOVALUE;
    int _2423 = NOVALUE;
    int _2420 = NOVALUE;
    int _2417 = NOVALUE;
    int _2416 = NOVALUE;
    int _2412 = NOVALUE;
    int _2411 = NOVALUE;
    int _2410 = NOVALUE;
    int _2409 = NOVALUE;
    int _2408 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_shift_distance_4837)) {
        _1 = (long)(DBL_PTR(_shift_distance_4837)->dbl);
        if (UNIQUE(DBL_PTR(_shift_distance_4837)) && (DBL_PTR(_shift_distance_4837)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_shift_distance_4837);
        _shift_distance_4837 = _1;
    }

    /** 	if sequence(source_number) then*/
    _2408 = IS_SEQUENCE(_source_number_4836);
    if (_2408 == 0)
    {
        _2408 = NOVALUE;
        goto L1; // [8] 55
    }
    else{
        _2408 = NOVALUE;
    }

    /** 		for i = 1 to length(source_number) do*/
    if (IS_SEQUENCE(_source_number_4836)){
            _2409 = SEQ_PTR(_source_number_4836)->length;
    }
    else {
        _2409 = 1;
    }
    {
        int _i_4841;
        _i_4841 = 1;
L2: 
        if (_i_4841 > _2409){
            goto L3; // [16] 48
        }

        /** 			source_number[i] = shift_bits(source_number[i], shift_distance)*/
        _2 = (int)SEQ_PTR(_source_number_4836);
        _2410 = (int)*(((s1_ptr)_2)->base + _i_4841);
        DeRef(_2411);
        _2411 = _shift_distance_4837;
        Ref(_2410);
        _2412 = _18shift_bits(_2410, _2411);
        _2410 = NOVALUE;
        _2411 = NOVALUE;
        _2 = (int)SEQ_PTR(_source_number_4836);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _source_number_4836 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4841);
        _1 = *(int *)_2;
        *(int *)_2 = _2412;
        if( _1 != _2412 ){
            DeRef(_1);
        }
        _2412 = NOVALUE;

        /** 		end for*/
        _i_4841 = _i_4841 + 1;
        goto L2; // [43] 23
L3: 
        ;
    }

    /** 		return source_number*/
    return _source_number_4836;
L1: 

    /** 	source_number = and_bits(source_number, 0xFFFFFFFF)*/
    _0 = _source_number_4836;
    _source_number_4836 = binary_op(AND_BITS, _source_number_4836, _2113);
    DeRef(_0);

    /** 	if shift_distance = 0 then*/
    if (_shift_distance_4837 != 0)
    goto L4; // [63] 74

    /** 		return source_number*/
    return _source_number_4836;
L4: 

    /** 	if shift_distance < 0 then*/
    if (_shift_distance_4837 >= 0)
    goto L5; // [76] 96

    /** 		source_number *= power(2, -shift_distance)*/
    if ((unsigned long)_shift_distance_4837 == 0xC0000000)
    _2416 = (int)NewDouble((double)-0xC0000000);
    else
    _2416 = - _shift_distance_4837;
    if (IS_ATOM_INT(_2416)) {
        _2417 = power(2, _2416);
    }
    else {
        temp_d.dbl = (double)2;
        _2417 = Dpower(&temp_d, DBL_PTR(_2416));
    }
    DeRef(_2416);
    _2416 = NOVALUE;
    _0 = _source_number_4836;
    if (IS_ATOM_INT(_source_number_4836) && IS_ATOM_INT(_2417)) {
        if (_source_number_4836 == (short)_source_number_4836 && _2417 <= INT15 && _2417 >= -INT15)
        _source_number_4836 = _source_number_4836 * _2417;
        else
        _source_number_4836 = NewDouble(_source_number_4836 * (double)_2417);
    }
    else {
        _source_number_4836 = binary_op(MULTIPLY, _source_number_4836, _2417);
    }
    DeRef(_0);
    DeRef(_2417);
    _2417 = NOVALUE;
    goto L6; // [93] 164
L5: 

    /** 		integer lSigned = 0*/
    _lSigned_4855 = 0;

    /** 		if and_bits(source_number, 0x80000000) then*/
    _2420 = binary_op(AND_BITS, _source_number_4836, _2419);
    if (_2420 == 0) {
        DeRef(_2420);
        _2420 = NOVALUE;
        goto L7; // [107] 122
    }
    else {
        if (!IS_ATOM_INT(_2420) && DBL_PTR(_2420)->dbl == 0.0){
            DeRef(_2420);
            _2420 = NOVALUE;
            goto L7; // [107] 122
        }
        DeRef(_2420);
        _2420 = NOVALUE;
    }
    DeRef(_2420);
    _2420 = NOVALUE;

    /** 			lSigned = 1*/
    _lSigned_4855 = 1;

    /** 			source_number = and_bits(source_number, 0x7FFFFFFF)*/
    _0 = _source_number_4836;
    _source_number_4836 = binary_op(AND_BITS, _source_number_4836, _2421);
    DeRef(_0);
L7: 

    /** 		source_number /= power(2, shift_distance)*/
    _2423 = power(2, _shift_distance_4837);
    _0 = _source_number_4836;
    if (IS_ATOM_INT(_source_number_4836) && IS_ATOM_INT(_2423)) {
        _source_number_4836 = (_source_number_4836 % _2423) ? NewDouble((double)_source_number_4836 / _2423) : (_source_number_4836 / _2423);
    }
    else {
        _source_number_4836 = binary_op(DIVIDE, _source_number_4836, _2423);
    }
    DeRef(_0);
    DeRef(_2423);
    _2423 = NOVALUE;

    /** 		if lSigned and shift_distance < 32 then*/
    if (_lSigned_4855 == 0) {
        goto L8; // [134] 161
    }
    _2426 = (_shift_distance_4837 < 32);
    if (_2426 == 0)
    {
        DeRef(_2426);
        _2426 = NOVALUE;
        goto L8; // [143] 161
    }
    else{
        DeRef(_2426);
        _2426 = NOVALUE;
    }

    /** 			source_number = or_bits(source_number, power(2, 31-shift_distance))*/
    _2427 = 31 - _shift_distance_4837;
    if ((long)((unsigned long)_2427 +(unsigned long) HIGH_BITS) >= 0){
        _2427 = NewDouble((double)_2427);
    }
    if (IS_ATOM_INT(_2427)) {
        _2428 = power(2, _2427);
    }
    else {
        temp_d.dbl = (double)2;
        _2428 = Dpower(&temp_d, DBL_PTR(_2427));
    }
    DeRef(_2427);
    _2427 = NOVALUE;
    _0 = _source_number_4836;
    if (IS_ATOM_INT(_source_number_4836) && IS_ATOM_INT(_2428)) {
        {unsigned long tu;
             tu = (unsigned long)_source_number_4836 | (unsigned long)_2428;
             _source_number_4836 = MAKE_UINT(tu);
        }
    }
    else {
        _source_number_4836 = binary_op(OR_BITS, _source_number_4836, _2428);
    }
    DeRef(_0);
    DeRef(_2428);
    _2428 = NOVALUE;
L8: 
L6: 

    /** 	return and_bits(source_number, 0xFFFFFFFF)*/
    _2430 = binary_op(AND_BITS, _source_number_4836, _2113);
    DeRef(_source_number_4836);
    return _2430;
    ;
}


int _18rotate_bits(int _source_number_4872, int _shift_distance_4873)
{
    int _lTemp_4874 = NOVALUE;
    int _lSave_4875 = NOVALUE;
    int _lRest_4876 = NOVALUE;
    int _2450 = NOVALUE;
    int _2447 = NOVALUE;
    int _2444 = NOVALUE;
    int _2441 = NOVALUE;
    int _2440 = NOVALUE;
    int _2439 = NOVALUE;
    int _2435 = NOVALUE;
    int _2434 = NOVALUE;
    int _2433 = NOVALUE;
    int _2432 = NOVALUE;
    int _2431 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_shift_distance_4873)) {
        _1 = (long)(DBL_PTR(_shift_distance_4873)->dbl);
        if (UNIQUE(DBL_PTR(_shift_distance_4873)) && (DBL_PTR(_shift_distance_4873)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_shift_distance_4873);
        _shift_distance_4873 = _1;
    }

    /** 	if sequence(source_number) then*/
    _2431 = IS_SEQUENCE(_source_number_4872);
    if (_2431 == 0)
    {
        _2431 = NOVALUE;
        goto L1; // [8] 55
    }
    else{
        _2431 = NOVALUE;
    }

    /** 		for i = 1 to length(source_number) do*/
    if (IS_SEQUENCE(_source_number_4872)){
            _2432 = SEQ_PTR(_source_number_4872)->length;
    }
    else {
        _2432 = 1;
    }
    {
        int _i_4880;
        _i_4880 = 1;
L2: 
        if (_i_4880 > _2432){
            goto L3; // [16] 48
        }

        /** 			source_number[i] = rotate_bits(source_number[i], shift_distance)*/
        _2 = (int)SEQ_PTR(_source_number_4872);
        _2433 = (int)*(((s1_ptr)_2)->base + _i_4880);
        DeRef(_2434);
        _2434 = _shift_distance_4873;
        Ref(_2433);
        _2435 = _18rotate_bits(_2433, _2434);
        _2433 = NOVALUE;
        _2434 = NOVALUE;
        _2 = (int)SEQ_PTR(_source_number_4872);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _source_number_4872 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4880);
        _1 = *(int *)_2;
        *(int *)_2 = _2435;
        if( _1 != _2435 ){
            DeRef(_1);
        }
        _2435 = NOVALUE;

        /** 		end for*/
        _i_4880 = _i_4880 + 1;
        goto L2; // [43] 23
L3: 
        ;
    }

    /** 		return source_number*/
    DeRef(_lTemp_4874);
    DeRef(_lSave_4875);
    return _source_number_4872;
L1: 

    /** 	source_number = and_bits(source_number, 0xFFFFFFFF)*/
    _0 = _source_number_4872;
    _source_number_4872 = binary_op(AND_BITS, _source_number_4872, _2113);
    DeRef(_0);

    /** 	if shift_distance = 0 then*/
    if (_shift_distance_4873 != 0)
    goto L4; // [63] 74

    /** 		return source_number*/
    DeRef(_lTemp_4874);
    DeRef(_lSave_4875);
    return _source_number_4872;
L4: 

    /** 	if shift_distance < 0 then*/
    if (_shift_distance_4873 >= 0)
    goto L5; // [76] 106

    /** 		lSave = not_bits(power(2, 32 + shift_distance) - 1) 	*/
    _2439 = 32 + _shift_distance_4873;
    if ((long)((unsigned long)_2439 + (unsigned long)HIGH_BITS) >= 0) 
    _2439 = NewDouble((double)_2439);
    if (IS_ATOM_INT(_2439)) {
        _2440 = power(2, _2439);
    }
    else {
        temp_d.dbl = (double)2;
        _2440 = Dpower(&temp_d, DBL_PTR(_2439));
    }
    DeRef(_2439);
    _2439 = NOVALUE;
    if (IS_ATOM_INT(_2440)) {
        _2441 = _2440 - 1;
        if ((long)((unsigned long)_2441 +(unsigned long) HIGH_BITS) >= 0){
            _2441 = NewDouble((double)_2441);
        }
    }
    else {
        _2441 = NewDouble(DBL_PTR(_2440)->dbl - (double)1);
    }
    DeRef(_2440);
    _2440 = NOVALUE;
    DeRef(_lSave_4875);
    if (IS_ATOM_INT(_2441))
    _lSave_4875 = not_bits(_2441);
    else
    _lSave_4875 = unary_op(NOT_BITS, _2441);
    DeRef(_2441);
    _2441 = NOVALUE;

    /** 		lRest = 32 + shift_distance*/
    _lRest_4876 = 32 + _shift_distance_4873;
    goto L6; // [103] 123
L5: 

    /** 		lSave = power(2, shift_distance) - 1*/
    _2444 = power(2, _shift_distance_4873);
    DeRef(_lSave_4875);
    if (IS_ATOM_INT(_2444)) {
        _lSave_4875 = _2444 - 1;
        if ((long)((unsigned long)_lSave_4875 +(unsigned long) HIGH_BITS) >= 0){
            _lSave_4875 = NewDouble((double)_lSave_4875);
        }
    }
    else {
        _lSave_4875 = NewDouble(DBL_PTR(_2444)->dbl - (double)1);
    }
    DeRef(_2444);
    _2444 = NOVALUE;

    /** 		lRest = shift_distance - 32*/
    _lRest_4876 = _shift_distance_4873 - 32;
L6: 

    /** 	lTemp = shift_bits(and_bits(source_number, lSave), lRest)*/
    if (IS_ATOM_INT(_source_number_4872) && IS_ATOM_INT(_lSave_4875)) {
        {unsigned long tu;
             tu = (unsigned long)_source_number_4872 & (unsigned long)_lSave_4875;
             _2447 = MAKE_UINT(tu);
        }
    }
    else {
        _2447 = binary_op(AND_BITS, _source_number_4872, _lSave_4875);
    }
    _0 = _lTemp_4874;
    _lTemp_4874 = _18shift_bits(_2447, _lRest_4876);
    DeRef(_0);
    _2447 = NOVALUE;

    /** 	source_number = shift_bits(source_number, shift_distance)*/
    Ref(_source_number_4872);
    _0 = _source_number_4872;
    _source_number_4872 = _18shift_bits(_source_number_4872, _shift_distance_4873);
    DeRef(_0);

    /** 	return or_bits(source_number, lTemp)*/
    if (IS_ATOM_INT(_source_number_4872) && IS_ATOM_INT(_lTemp_4874)) {
        {unsigned long tu;
             tu = (unsigned long)_source_number_4872 | (unsigned long)_lTemp_4874;
             _2450 = MAKE_UINT(tu);
        }
    }
    else {
        _2450 = binary_op(OR_BITS, _source_number_4872, _lTemp_4874);
    }
    DeRef(_source_number_4872);
    DeRef(_lTemp_4874);
    DeRef(_lSave_4875);
    return _2450;
    ;
}


int _18gcd(int _p_4905, int _q_4906)
{
    int _r_4907 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if p < 0 then*/
    if (binary_op_a(GREATEREQ, _p_4905, 0)){
        goto L1; // [3] 13
    }

    /** 		p = -p*/
    _0 = _p_4905;
    if (IS_ATOM_INT(_p_4905)) {
        if ((unsigned long)_p_4905 == 0xC0000000)
        _p_4905 = (int)NewDouble((double)-0xC0000000);
        else
        _p_4905 = - _p_4905;
    }
    else {
        _p_4905 = unary_op(UMINUS, _p_4905);
    }
    DeRef(_0);
L1: 

    /** 	if q < 0 then*/
    if (binary_op_a(GREATEREQ, _q_4906, 0)){
        goto L2; // [15] 25
    }

    /** 		q = -q*/
    _0 = _q_4906;
    if (IS_ATOM_INT(_q_4906)) {
        if ((unsigned long)_q_4906 == 0xC0000000)
        _q_4906 = (int)NewDouble((double)-0xC0000000);
        else
        _q_4906 = - _q_4906;
    }
    else {
        _q_4906 = unary_op(UMINUS, _q_4906);
    }
    DeRef(_0);
L2: 

    /** 	p = floor(p)*/
    _0 = _p_4905;
    if (IS_ATOM_INT(_p_4905))
    _p_4905 = e_floor(_p_4905);
    else
    _p_4905 = unary_op(FLOOR, _p_4905);
    DeRef(_0);

    /** 	q = floor(q)*/
    _0 = _q_4906;
    if (IS_ATOM_INT(_q_4906))
    _q_4906 = e_floor(_q_4906);
    else
    _q_4906 = unary_op(FLOOR, _q_4906);
    DeRef(_0);

    /** 	if p < q then*/
    if (binary_op_a(GREATEREQ, _p_4905, _q_4906)){
        goto L3; // [37] 57
    }

    /** 		r = p*/
    Ref(_p_4905);
    DeRef(_r_4907);
    _r_4907 = _p_4905;

    /** 		p = q*/
    Ref(_q_4906);
    DeRef(_p_4905);
    _p_4905 = _q_4906;

    /** 		q = r*/
    Ref(_r_4907);
    DeRef(_q_4906);
    _q_4906 = _r_4907;
L3: 

    /** 	if q = 0 then*/
    if (binary_op_a(NOTEQ, _q_4906, 0)){
        goto L4; // [59] 94
    }

    /** 		return p*/
    DeRef(_q_4906);
    DeRef(_r_4907);
    return _p_4905;

    /**     while r > 1 with entry do*/
    goto L4; // [72] 94
L5: 
    if (binary_op_a(LESSEQ, _r_4907, 1)){
        goto L6; // [77] 105
    }

    /** 		p = q*/
    Ref(_q_4906);
    DeRef(_p_4905);
    _p_4905 = _q_4906;

    /** 		q = r*/
    Ref(_r_4907);
    DeRef(_q_4906);
    _q_4906 = _r_4907;

    /** 	entry*/
L4: 

    /** 		r = remainder(p, q)*/
    DeRef(_r_4907);
    if (IS_ATOM_INT(_p_4905) && IS_ATOM_INT(_q_4906)) {
        _r_4907 = (_p_4905 % _q_4906);
    }
    else {
        if (IS_ATOM_INT(_p_4905)) {
            temp_d.dbl = (double)_p_4905;
            _r_4907 = Dremainder(&temp_d, DBL_PTR(_q_4906));
        }
        else {
            if (IS_ATOM_INT(_q_4906)) {
                temp_d.dbl = (double)_q_4906;
                _r_4907 = Dremainder(DBL_PTR(_p_4905), &temp_d);
            }
            else
            _r_4907 = Dremainder(DBL_PTR(_p_4905), DBL_PTR(_q_4906));
        }
    }

    /**     end while*/
    goto L5; // [102] 75
L6: 

    /** 	if r = 1 then*/
    if (binary_op_a(NOTEQ, _r_4907, 1)){
        goto L7; // [109] 122
    }

    /** 		return 1*/
    DeRef(_p_4905);
    DeRef(_q_4906);
    DeRef(_r_4907);
    return 1;
    goto L8; // [119] 129
L7: 

    /** 		return q*/
    DeRef(_p_4905);
    DeRef(_r_4907);
    return _q_4906;
L8: 
    ;
}


int _18approx(int _p_4928, int _q_4929, int _epsilon_4930)
{
    int _msg_inlined_crash_at_30_4942 = NOVALUE;
    int _2484 = NOVALUE;
    int _2482 = NOVALUE;
    int _2481 = NOVALUE;
    int _2480 = NOVALUE;
    int _2479 = NOVALUE;
    int _2478 = NOVALUE;
    int _2477 = NOVALUE;
    int _2476 = NOVALUE;
    int _2475 = NOVALUE;
    int _2474 = NOVALUE;
    int _2473 = NOVALUE;
    int _2472 = NOVALUE;
    int _2471 = NOVALUE;
    int _2470 = NOVALUE;
    int _2469 = NOVALUE;
    int _2466 = NOVALUE;
    int _2465 = NOVALUE;
    int _2464 = NOVALUE;
    int _2463 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(p) then*/
    _2463 = IS_SEQUENCE(_p_4928);
    if (_2463 == 0)
    {
        _2463 = NOVALUE;
        goto L1; // [6] 146
    }
    else{
        _2463 = NOVALUE;
    }

    /** 		if sequence(q) then*/
    _2464 = IS_SEQUENCE(_q_4929);
    if (_2464 == 0)
    {
        _2464 = NOVALUE;
        goto L2; // [14] 98
    }
    else{
        _2464 = NOVALUE;
    }

    /** 			if length(p) != length(q) then*/
    if (IS_SEQUENCE(_p_4928)){
            _2465 = SEQ_PTR(_p_4928)->length;
    }
    else {
        _2465 = 1;
    }
    if (IS_SEQUENCE(_q_4929)){
            _2466 = SEQ_PTR(_q_4929)->length;
    }
    else {
        _2466 = 1;
    }
    if (_2465 == _2466)
    goto L3; // [25] 50

    /** 				error:crash("approx(): Sequence arguments must be the same length")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_30_4942);
    _msg_inlined_crash_at_30_4942 = EPrintf(-9999999, _2468, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_30_4942);

    /** end procedure*/
    goto L4; // [44] 47
L4: 
    DeRefi(_msg_inlined_crash_at_30_4942);
    _msg_inlined_crash_at_30_4942 = NOVALUE;
L3: 

    /** 			for i = 1 to length(p) do*/
    if (IS_SEQUENCE(_p_4928)){
            _2469 = SEQ_PTR(_p_4928)->length;
    }
    else {
        _2469 = 1;
    }
    {
        int _i_4944;
        _i_4944 = 1;
L5: 
        if (_i_4944 > _2469){
            goto L6; // [55] 89
        }

        /** 				p[i] = approx(p[i], q[i])*/
        _2 = (int)SEQ_PTR(_p_4928);
        _2470 = (int)*(((s1_ptr)_2)->base + _i_4944);
        _2 = (int)SEQ_PTR(_q_4929);
        _2471 = (int)*(((s1_ptr)_2)->base + _i_4944);
        Ref(_2470);
        Ref(_2471);
        RefDS(_2462);
        _2472 = _18approx(_2470, _2471, _2462);
        _2470 = NOVALUE;
        _2471 = NOVALUE;
        _2 = (int)SEQ_PTR(_p_4928);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _p_4928 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4944);
        _1 = *(int *)_2;
        *(int *)_2 = _2472;
        if( _1 != _2472 ){
            DeRef(_1);
        }
        _2472 = NOVALUE;

        /** 			end for*/
        _i_4944 = _i_4944 + 1;
        goto L5; // [84] 62
L6: 
        ;
    }

    /** 			return p*/
    DeRef(_q_4929);
    DeRef(_epsilon_4930);
    return _p_4928;
    goto L7; // [95] 242
L2: 

    /** 			for i = 1 to length(p) do*/
    if (IS_SEQUENCE(_p_4928)){
            _2473 = SEQ_PTR(_p_4928)->length;
    }
    else {
        _2473 = 1;
    }
    {
        int _i_4951;
        _i_4951 = 1;
L8: 
        if (_i_4951 > _2473){
            goto L9; // [103] 136
        }

        /** 				p[i] = approx(p[i], q)*/
        _2 = (int)SEQ_PTR(_p_4928);
        _2474 = (int)*(((s1_ptr)_2)->base + _i_4951);
        Ref(_q_4929);
        DeRef(_2475);
        _2475 = _q_4929;
        Ref(_2474);
        RefDS(_2462);
        _2476 = _18approx(_2474, _2475, _2462);
        _2474 = NOVALUE;
        _2475 = NOVALUE;
        _2 = (int)SEQ_PTR(_p_4928);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _p_4928 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4951);
        _1 = *(int *)_2;
        *(int *)_2 = _2476;
        if( _1 != _2476 ){
            DeRef(_1);
        }
        _2476 = NOVALUE;

        /** 			end for*/
        _i_4951 = _i_4951 + 1;
        goto L8; // [131] 110
L9: 
        ;
    }

    /** 			return p*/
    DeRef(_q_4929);
    DeRef(_epsilon_4930);
    return _p_4928;
    goto L7; // [143] 242
L1: 

    /** 	elsif sequence(q) then*/
    _2477 = IS_SEQUENCE(_q_4929);
    if (_2477 == 0)
    {
        _2477 = NOVALUE;
        goto LA; // [151] 201
    }
    else{
        _2477 = NOVALUE;
    }

    /** 			for i = 1 to length(q) do*/
    if (IS_SEQUENCE(_q_4929)){
            _2478 = SEQ_PTR(_q_4929)->length;
    }
    else {
        _2478 = 1;
    }
    {
        int _i_4959;
        _i_4959 = 1;
LB: 
        if (_i_4959 > _2478){
            goto LC; // [159] 192
        }

        /** 				q[i] = approx(p, q[i])*/
        _2 = (int)SEQ_PTR(_q_4929);
        _2479 = (int)*(((s1_ptr)_2)->base + _i_4959);
        Ref(_p_4928);
        DeRef(_2480);
        _2480 = _p_4928;
        Ref(_2479);
        RefDS(_2462);
        _2481 = _18approx(_2480, _2479, _2462);
        _2480 = NOVALUE;
        _2479 = NOVALUE;
        _2 = (int)SEQ_PTR(_q_4929);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _q_4929 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4959);
        _1 = *(int *)_2;
        *(int *)_2 = _2481;
        if( _1 != _2481 ){
            DeRef(_1);
        }
        _2481 = NOVALUE;

        /** 			end for*/
        _i_4959 = _i_4959 + 1;
        goto LB; // [187] 166
LC: 
        ;
    }

    /** 			return q*/
    DeRef(_p_4928);
    DeRef(_epsilon_4930);
    return _q_4929;
    goto L7; // [198] 242
LA: 

    /** 		if p > (q + epsilon) then*/
    if (IS_ATOM_INT(_q_4929) && IS_ATOM_INT(_epsilon_4930)) {
        _2482 = _q_4929 + _epsilon_4930;
        if ((long)((unsigned long)_2482 + (unsigned long)HIGH_BITS) >= 0) 
        _2482 = NewDouble((double)_2482);
    }
    else {
        _2482 = binary_op(PLUS, _q_4929, _epsilon_4930);
    }
    if (binary_op_a(LESSEQ, _p_4928, _2482)){
        DeRef(_2482);
        _2482 = NOVALUE;
        goto LD; // [207] 218
    }
    DeRef(_2482);
    _2482 = NOVALUE;

    /** 			return 1*/
    DeRef(_p_4928);
    DeRef(_q_4929);
    DeRef(_epsilon_4930);
    return 1;
LD: 

    /** 		if p < (q - epsilon) then*/
    if (IS_ATOM_INT(_q_4929) && IS_ATOM_INT(_epsilon_4930)) {
        _2484 = _q_4929 - _epsilon_4930;
        if ((long)((unsigned long)_2484 +(unsigned long) HIGH_BITS) >= 0){
            _2484 = NewDouble((double)_2484);
        }
    }
    else {
        _2484 = binary_op(MINUS, _q_4929, _epsilon_4930);
    }
    if (binary_op_a(GREATEREQ, _p_4928, _2484)){
        DeRef(_2484);
        _2484 = NOVALUE;
        goto LE; // [224] 235
    }
    DeRef(_2484);
    _2484 = NOVALUE;

    /** 			return -1*/
    DeRef(_p_4928);
    DeRef(_q_4929);
    DeRef(_epsilon_4930);
    return -1;
LE: 

    /** 		return 0*/
    DeRef(_p_4928);
    DeRef(_q_4929);
    DeRef(_epsilon_4930);
    return 0;
L7: 
    ;
}


int _18powof2(int _p_4973)
{
    int _2488 = NOVALUE;
    int _2487 = NOVALUE;
    int _2486 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return not (and_bits(p, p-1))*/
    if (IS_ATOM_INT(_p_4973)) {
        _2486 = _p_4973 - 1;
        if ((long)((unsigned long)_2486 +(unsigned long) HIGH_BITS) >= 0){
            _2486 = NewDouble((double)_2486);
        }
    }
    else {
        _2486 = binary_op(MINUS, _p_4973, 1);
    }
    if (IS_ATOM_INT(_p_4973) && IS_ATOM_INT(_2486)) {
        {unsigned long tu;
             tu = (unsigned long)_p_4973 & (unsigned long)_2486;
             _2487 = MAKE_UINT(tu);
        }
    }
    else {
        _2487 = binary_op(AND_BITS, _p_4973, _2486);
    }
    DeRef(_2486);
    _2486 = NOVALUE;
    if (IS_ATOM_INT(_2487)) {
        _2488 = (_2487 == 0);
    }
    else {
        _2488 = unary_op(NOT, _2487);
    }
    DeRef(_2487);
    _2487 = NOVALUE;
    DeRef(_p_4973);
    return _2488;
    ;
}


int _18is_even(int _test_integer_4979)
{
    int _2490 = NOVALUE;
    int _2489 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_test_integer_4979)) {
        _1 = (long)(DBL_PTR(_test_integer_4979)->dbl);
        if (UNIQUE(DBL_PTR(_test_integer_4979)) && (DBL_PTR(_test_integer_4979)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_test_integer_4979);
        _test_integer_4979 = _1;
    }

    /** 	return (and_bits(test_integer, 1) = 0)*/
    {unsigned long tu;
         tu = (unsigned long)_test_integer_4979 & (unsigned long)1;
         _2489 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_2489)) {
        _2490 = (_2489 == 0);
    }
    else {
        _2490 = (DBL_PTR(_2489)->dbl == (double)0);
    }
    DeRef(_2489);
    _2489 = NOVALUE;
    return _2490;
    ;
}


int _18is_even_obj(int _test_object_4984)
{
    int _2497 = NOVALUE;
    int _2496 = NOVALUE;
    int _2495 = NOVALUE;
    int _2494 = NOVALUE;
    int _2493 = NOVALUE;
    int _2492 = NOVALUE;
    int _2491 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(test_object) then*/
    _2491 = IS_ATOM(_test_object_4984);
    if (_2491 == 0)
    {
        _2491 = NOVALUE;
        goto L1; // [6] 39
    }
    else{
        _2491 = NOVALUE;
    }

    /** 		if integer(test_object) then*/
    if (IS_ATOM_INT(_test_object_4984))
    _2492 = 1;
    else if (IS_ATOM_DBL(_test_object_4984))
    _2492 = IS_ATOM_INT(DoubleToInt(_test_object_4984));
    else
    _2492 = 0;
    if (_2492 == 0)
    {
        _2492 = NOVALUE;
        goto L2; // [14] 32
    }
    else{
        _2492 = NOVALUE;
    }

    /** 			return (and_bits(test_object, 1) = 0)*/
    if (IS_ATOM_INT(_test_object_4984)) {
        {unsigned long tu;
             tu = (unsigned long)_test_object_4984 & (unsigned long)1;
             _2493 = MAKE_UINT(tu);
        }
    }
    else {
        _2493 = binary_op(AND_BITS, _test_object_4984, 1);
    }
    if (IS_ATOM_INT(_2493)) {
        _2494 = (_2493 == 0);
    }
    else {
        _2494 = binary_op(EQUALS, _2493, 0);
    }
    DeRef(_2493);
    _2493 = NOVALUE;
    DeRef(_test_object_4984);
    return _2494;
L2: 

    /** 		return 0*/
    DeRef(_test_object_4984);
    DeRef(_2494);
    _2494 = NOVALUE;
    return 0;
L1: 

    /** 	for i = 1 to length(test_object) do*/
    if (IS_SEQUENCE(_test_object_4984)){
            _2495 = SEQ_PTR(_test_object_4984)->length;
    }
    else {
        _2495 = 1;
    }
    {
        int _i_4992;
        _i_4992 = 1;
L3: 
        if (_i_4992 > _2495){
            goto L4; // [44] 72
        }

        /** 		test_object[i] = is_even_obj(test_object[i])*/
        _2 = (int)SEQ_PTR(_test_object_4984);
        _2496 = (int)*(((s1_ptr)_2)->base + _i_4992);
        Ref(_2496);
        _2497 = _18is_even_obj(_2496);
        _2496 = NOVALUE;
        _2 = (int)SEQ_PTR(_test_object_4984);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _test_object_4984 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4992);
        _1 = *(int *)_2;
        *(int *)_2 = _2497;
        if( _1 != _2497 ){
            DeRef(_1);
        }
        _2497 = NOVALUE;

        /** 	end for*/
        _i_4992 = _i_4992 + 1;
        goto L3; // [67] 51
L4: 
        ;
    }

    /** 	return test_object*/
    DeRef(_2494);
    _2494 = NOVALUE;
    return _test_object_4984;
    ;
}



// 0x81988495
