// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _61reverse(int _s_23099)
{
    int _lower_23100 = NOVALUE;
    int _n_23101 = NOVALUE;
    int _n2_23102 = NOVALUE;
    int _t_23103 = NOVALUE;
    int _13524 = NOVALUE;
    int _13523 = NOVALUE;
    int _13522 = NOVALUE;
    int _13519 = NOVALUE;
    int _0, _1, _2;
    

    /** 	n = length(s)*/
    if (IS_SEQUENCE(_s_23099)){
            _n_23101 = SEQ_PTR(_s_23099)->length;
    }
    else {
        _n_23101 = 1;
    }

    /** 	n2 = floor(n/2)+1*/
    _13519 = _n_23101 >> 1;
    _n2_23102 = _13519 + 1;
    _13519 = NOVALUE;

    /** 	t = repeat(0, n)*/
    DeRef(_t_23103);
    _t_23103 = Repeat(0, _n_23101);

    /** 	lower = 1*/
    _lower_23100 = 1;

    /** 	for upper = n to n2 by -1 do*/
    _13522 = _n2_23102;
    {
        int _upper_23109;
        _upper_23109 = _n_23101;
L1: 
        if (_upper_23109 < _13522){
            goto L2; // [34] 74
        }

        /** 		t[upper] = s[lower]*/
        _2 = (int)SEQ_PTR(_s_23099);
        _13523 = (int)*(((s1_ptr)_2)->base + _lower_23100);
        Ref(_13523);
        _2 = (int)SEQ_PTR(_t_23103);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _t_23103 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _upper_23109);
        _1 = *(int *)_2;
        *(int *)_2 = _13523;
        if( _1 != _13523 ){
            DeRef(_1);
        }
        _13523 = NOVALUE;

        /** 		t[lower] = s[upper]*/
        _2 = (int)SEQ_PTR(_s_23099);
        _13524 = (int)*(((s1_ptr)_2)->base + _upper_23109);
        Ref(_13524);
        _2 = (int)SEQ_PTR(_t_23103);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _t_23103 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _lower_23100);
        _1 = *(int *)_2;
        *(int *)_2 = _13524;
        if( _1 != _13524 ){
            DeRef(_1);
        }
        _13524 = NOVALUE;

        /** 		lower += 1*/
        _lower_23100 = _lower_23100 + 1;

        /** 	end for*/
        _upper_23109 = _upper_23109 + -1;
        goto L1; // [69] 41
L2: 
        ;
    }

    /** 	return t*/
    DeRefDS(_s_23099);
    return _t_23103;
    ;
}


int _61carry(int _a_23116, int _radix_23117)
{
    int _q_23118 = NOVALUE;
    int _r_23119 = NOVALUE;
    int _b_23120 = NOVALUE;
    int _rmax_23121 = NOVALUE;
    int _i_23122 = NOVALUE;
    int _13538 = NOVALUE;
    int _13537 = NOVALUE;
    int _13536 = NOVALUE;
    int _13533 = NOVALUE;
    int _13527 = NOVALUE;
    int _0, _1, _2;
    

    /** 		rmax = radix - 1*/
    DeRef(_rmax_23121);
    _rmax_23121 = _radix_23117 - 1;
    if ((long)((unsigned long)_rmax_23121 +(unsigned long) HIGH_BITS) >= 0){
        _rmax_23121 = NewDouble((double)_rmax_23121);
    }

    /** 		i = 1*/
    DeRef(_i_23122);
    _i_23122 = 1;

    /** 		while i <= length(a) do*/
L1: 
    if (IS_SEQUENCE(_a_23116)){
            _13527 = SEQ_PTR(_a_23116)->length;
    }
    else {
        _13527 = 1;
    }
    if (binary_op_a(GREATER, _i_23122, _13527)){
        _13527 = NOVALUE;
        goto L2; // [24] 104
    }
    _13527 = NOVALUE;

    /** 				b = a[i]*/
    DeRef(_b_23120);
    _2 = (int)SEQ_PTR(_a_23116);
    if (!IS_ATOM_INT(_i_23122)){
        _b_23120 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_23122)->dbl));
    }
    else{
        _b_23120 = (int)*(((s1_ptr)_2)->base + _i_23122);
    }
    Ref(_b_23120);

    /** 				if b > rmax then*/
    if (binary_op_a(LESSEQ, _b_23120, _rmax_23121)){
        goto L3; // [36] 93
    }

    /** 						q = floor( b / radix )*/
    DeRef(_q_23118);
    if (IS_ATOM_INT(_b_23120)) {
        if (_radix_23117 > 0 && _b_23120 >= 0) {
            _q_23118 = _b_23120 / _radix_23117;
        }
        else {
            temp_dbl = floor((double)_b_23120 / (double)_radix_23117);
            if (_b_23120 != MININT)
            _q_23118 = (long)temp_dbl;
            else
            _q_23118 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _b_23120, _radix_23117);
        _q_23118 = unary_op(FLOOR, _2);
        DeRef(_2);
    }

    /** 						r = remainder( b, radix )*/
    DeRef(_r_23119);
    if (IS_ATOM_INT(_b_23120)) {
        _r_23119 = (_b_23120 % _radix_23117);
    }
    else {
        temp_d.dbl = (double)_radix_23117;
        _r_23119 = Dremainder(DBL_PTR(_b_23120), &temp_d);
    }

    /** 						a[i] = r*/
    Ref(_r_23119);
    _2 = (int)SEQ_PTR(_a_23116);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _a_23116 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_i_23122))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_23122)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _i_23122);
    _1 = *(int *)_2;
    *(int *)_2 = _r_23119;
    DeRef(_1);

    /** 						if i = length(a) then*/
    if (IS_SEQUENCE(_a_23116)){
            _13533 = SEQ_PTR(_a_23116)->length;
    }
    else {
        _13533 = 1;
    }
    if (binary_op_a(NOTEQ, _i_23122, _13533)){
        _13533 = NOVALUE;
        goto L4; // [63] 74
    }
    _13533 = NOVALUE;

    /** 								a &= 0*/
    Append(&_a_23116, _a_23116, 0);
L4: 

    /** 						a[i+1] += q*/
    if (IS_ATOM_INT(_i_23122)) {
        _13536 = _i_23122 + 1;
    }
    else
    _13536 = binary_op(PLUS, 1, _i_23122);
    _2 = (int)SEQ_PTR(_a_23116);
    if (!IS_ATOM_INT(_13536)){
        _13537 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_13536)->dbl));
    }
    else{
        _13537 = (int)*(((s1_ptr)_2)->base + _13536);
    }
    if (IS_ATOM_INT(_13537) && IS_ATOM_INT(_q_23118)) {
        _13538 = _13537 + _q_23118;
        if ((long)((unsigned long)_13538 + (unsigned long)HIGH_BITS) >= 0) 
        _13538 = NewDouble((double)_13538);
    }
    else {
        _13538 = binary_op(PLUS, _13537, _q_23118);
    }
    _13537 = NOVALUE;
    _2 = (int)SEQ_PTR(_a_23116);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _a_23116 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_13536))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_13536)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _13536);
    _1 = *(int *)_2;
    *(int *)_2 = _13538;
    if( _1 != _13538 ){
        DeRef(_1);
    }
    _13538 = NOVALUE;
L3: 

    /** 				i += 1*/
    _0 = _i_23122;
    if (IS_ATOM_INT(_i_23122)) {
        _i_23122 = _i_23122 + 1;
        if (_i_23122 > MAXINT){
            _i_23122 = NewDouble((double)_i_23122);
        }
    }
    else
    _i_23122 = binary_op(PLUS, 1, _i_23122);
    DeRef(_0);

    /** 		end while*/
    goto L1; // [101] 21
L2: 

    /** 		return a*/
    DeRef(_q_23118);
    DeRef(_r_23119);
    DeRef(_b_23120);
    DeRef(_rmax_23121);
    DeRef(_i_23122);
    DeRef(_13536);
    _13536 = NOVALUE;
    return _a_23116;
    ;
}


int _61add(int _a_23142, int _b_23143)
{
    int _13556 = NOVALUE;
    int _13554 = NOVALUE;
    int _13553 = NOVALUE;
    int _13552 = NOVALUE;
    int _13551 = NOVALUE;
    int _13549 = NOVALUE;
    int _13548 = NOVALUE;
    int _13546 = NOVALUE;
    int _13545 = NOVALUE;
    int _13544 = NOVALUE;
    int _13543 = NOVALUE;
    int _13541 = NOVALUE;
    int _13540 = NOVALUE;
    int _0, _1, _2;
    

    /** 		if length(a) < length(b) then*/
    if (IS_SEQUENCE(_a_23142)){
            _13540 = SEQ_PTR(_a_23142)->length;
    }
    else {
        _13540 = 1;
    }
    if (IS_SEQUENCE(_b_23143)){
            _13541 = SEQ_PTR(_b_23143)->length;
    }
    else {
        _13541 = 1;
    }
    if (_13540 >= _13541)
    goto L1; // [13] 40

    /** 				a &= repeat( 0, length(b) - length(a) )*/
    if (IS_SEQUENCE(_b_23143)){
            _13543 = SEQ_PTR(_b_23143)->length;
    }
    else {
        _13543 = 1;
    }
    if (IS_SEQUENCE(_a_23142)){
            _13544 = SEQ_PTR(_a_23142)->length;
    }
    else {
        _13544 = 1;
    }
    _13545 = _13543 - _13544;
    _13543 = NOVALUE;
    _13544 = NOVALUE;
    _13546 = Repeat(0, _13545);
    _13545 = NOVALUE;
    Concat((object_ptr)&_a_23142, _a_23142, _13546);
    DeRefDS(_13546);
    _13546 = NOVALUE;
    goto L2; // [37] 74
L1: 

    /** 		elsif length(b) < length(a) then*/
    if (IS_SEQUENCE(_b_23143)){
            _13548 = SEQ_PTR(_b_23143)->length;
    }
    else {
        _13548 = 1;
    }
    if (IS_SEQUENCE(_a_23142)){
            _13549 = SEQ_PTR(_a_23142)->length;
    }
    else {
        _13549 = 1;
    }
    if (_13548 >= _13549)
    goto L3; // [48] 73

    /** 				b &= repeat( 0, length(a) - length(b) )*/
    if (IS_SEQUENCE(_a_23142)){
            _13551 = SEQ_PTR(_a_23142)->length;
    }
    else {
        _13551 = 1;
    }
    if (IS_SEQUENCE(_b_23143)){
            _13552 = SEQ_PTR(_b_23143)->length;
    }
    else {
        _13552 = 1;
    }
    _13553 = _13551 - _13552;
    _13551 = NOVALUE;
    _13552 = NOVALUE;
    _13554 = Repeat(0, _13553);
    _13553 = NOVALUE;
    Concat((object_ptr)&_b_23143, _b_23143, _13554);
    DeRefDS(_13554);
    _13554 = NOVALUE;
L3: 
L2: 

    /** 		return a + b*/
    _13556 = binary_op(PLUS, _a_23142, _b_23143);
    DeRefDS(_a_23142);
    DeRefDS(_b_23143);
    return _13556;
    ;
}


int _61borrow(int _a_23165, int _radix_23166)
{
    int _13564 = NOVALUE;
    int _13563 = NOVALUE;
    int _13562 = NOVALUE;
    int _13561 = NOVALUE;
    int _13560 = NOVALUE;
    int _13558 = NOVALUE;
    int _13557 = NOVALUE;
    int _0, _1, _2;
    

    /** 		for i = length(a) to 2 by -1 do*/
    if (IS_SEQUENCE(_a_23165)){
            _13557 = SEQ_PTR(_a_23165)->length;
    }
    else {
        _13557 = 1;
    }
    {
        int _i_23168;
        _i_23168 = _13557;
L1: 
        if (_i_23168 < 2){
            goto L2; // [10] 67
        }

        /** 				if a[i] < 0 then*/
        _2 = (int)SEQ_PTR(_a_23165);
        _13558 = (int)*(((s1_ptr)_2)->base + _i_23168);
        if (binary_op_a(GREATEREQ, _13558, 0)){
            _13558 = NOVALUE;
            goto L3; // [23] 60
        }
        _13558 = NOVALUE;

        /** 						a[i] += radix*/
        _2 = (int)SEQ_PTR(_a_23165);
        _13560 = (int)*(((s1_ptr)_2)->base + _i_23168);
        if (IS_ATOM_INT(_13560)) {
            _13561 = _13560 + _radix_23166;
            if ((long)((unsigned long)_13561 + (unsigned long)HIGH_BITS) >= 0) 
            _13561 = NewDouble((double)_13561);
        }
        else {
            _13561 = binary_op(PLUS, _13560, _radix_23166);
        }
        _13560 = NOVALUE;
        _2 = (int)SEQ_PTR(_a_23165);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _a_23165 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_23168);
        _1 = *(int *)_2;
        *(int *)_2 = _13561;
        if( _1 != _13561 ){
            DeRef(_1);
        }
        _13561 = NOVALUE;

        /** 						a[i-1] -= 1*/
        _13562 = _i_23168 - 1;
        _2 = (int)SEQ_PTR(_a_23165);
        _13563 = (int)*(((s1_ptr)_2)->base + _13562);
        if (IS_ATOM_INT(_13563)) {
            _13564 = _13563 - 1;
            if ((long)((unsigned long)_13564 +(unsigned long) HIGH_BITS) >= 0){
                _13564 = NewDouble((double)_13564);
            }
        }
        else {
            _13564 = binary_op(MINUS, _13563, 1);
        }
        _13563 = NOVALUE;
        _2 = (int)SEQ_PTR(_a_23165);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _a_23165 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _13562);
        _1 = *(int *)_2;
        *(int *)_2 = _13564;
        if( _1 != _13564 ){
            DeRef(_1);
        }
        _13564 = NOVALUE;
L3: 

        /** 		end for*/
        _i_23168 = _i_23168 + -1;
        goto L1; // [62] 17
L2: 
        ;
    }

    /** 		return a*/
    DeRef(_13562);
    _13562 = NOVALUE;
    return _a_23165;
    ;
}


int _61bits_to_bytes(int _bits_23180)
{
    int _bytes_23181 = NOVALUE;
    int _r_23182 = NOVALUE;
    int _13573 = NOVALUE;
    int _13572 = NOVALUE;
    int _13571 = NOVALUE;
    int _13570 = NOVALUE;
    int _13568 = NOVALUE;
    int _13567 = NOVALUE;
    int _13565 = NOVALUE;
    int _0, _1, _2;
    

    /** 		r = remainder( length(bits), 8 )*/
    if (IS_SEQUENCE(_bits_23180)){
            _13565 = SEQ_PTR(_bits_23180)->length;
    }
    else {
        _13565 = 1;
    }
    _r_23182 = (_13565 % 8);
    _13565 = NOVALUE;

    /** 		if r  then*/
    if (_r_23182 == 0)
    {
        goto L1; // [14] 32
    }
    else{
    }

    /** 				bits &= repeat( 0, 8 - r )*/
    _13567 = 8 - _r_23182;
    _13568 = Repeat(0, _13567);
    _13567 = NOVALUE;
    Concat((object_ptr)&_bits_23180, _bits_23180, _13568);
    DeRefDS(_13568);
    _13568 = NOVALUE;
L1: 

    /** 		bytes = {}*/
    RefDS(_5);
    DeRef(_bytes_23181);
    _bytes_23181 = _5;

    /** 		for i = 1 to length(bits) by 8 do*/
    if (IS_SEQUENCE(_bits_23180)){
            _13570 = SEQ_PTR(_bits_23180)->length;
    }
    else {
        _13570 = 1;
    }
    {
        int _i_23190;
        _i_23190 = 1;
L2: 
        if (_i_23190 > _13570){
            goto L3; // [44] 77
        }

        /** 				bytes &= bits_to_int( bits[i..i+7] )*/
        _13571 = _i_23190 + 7;
        rhs_slice_target = (object_ptr)&_13572;
        RHS_Slice(_bits_23180, _i_23190, _13571);
        _13573 = _6bits_to_int(_13572);
        _13572 = NOVALUE;
        if (IS_SEQUENCE(_bytes_23181) && IS_ATOM(_13573)) {
            Ref(_13573);
            Append(&_bytes_23181, _bytes_23181, _13573);
        }
        else if (IS_ATOM(_bytes_23181) && IS_SEQUENCE(_13573)) {
        }
        else {
            Concat((object_ptr)&_bytes_23181, _bytes_23181, _13573);
        }
        DeRef(_13573);
        _13573 = NOVALUE;

        /** 		end for*/
        _i_23190 = _i_23190 + 8;
        goto L2; // [72] 51
L3: 
        ;
    }

    /** 		return bytes*/
    DeRefDS(_bits_23180);
    DeRef(_13571);
    _13571 = NOVALUE;
    return _bytes_23181;
    ;
}


int _61bytes_to_bits(int _bytes_23199)
{
    int _bits_23200 = NOVALUE;
    int _13577 = NOVALUE;
    int _13576 = NOVALUE;
    int _13575 = NOVALUE;
    int _0, _1, _2;
    

    /** 		bits = {}*/
    RefDS(_5);
    DeRef(_bits_23200);
    _bits_23200 = _5;

    /** 		for i = 1 to length(bytes) do*/
    if (IS_SEQUENCE(_bytes_23199)){
            _13575 = SEQ_PTR(_bytes_23199)->length;
    }
    else {
        _13575 = 1;
    }
    {
        int _i_23202;
        _i_23202 = 1;
L1: 
        if (_i_23202 > _13575){
            goto L2; // [15] 44
        }

        /** 				bits &= int_to_bits( bytes[i], 8 )*/
        _2 = (int)SEQ_PTR(_bytes_23199);
        _13576 = (int)*(((s1_ptr)_2)->base + _i_23202);
        Ref(_13576);
        _13577 = _6int_to_bits(_13576, 8);
        _13576 = NOVALUE;
        if (IS_SEQUENCE(_bits_23200) && IS_ATOM(_13577)) {
            Ref(_13577);
            Append(&_bits_23200, _bits_23200, _13577);
        }
        else if (IS_ATOM(_bits_23200) && IS_SEQUENCE(_13577)) {
        }
        else {
            Concat((object_ptr)&_bits_23200, _bits_23200, _13577);
        }
        DeRef(_13577);
        _13577 = NOVALUE;

        /** 		end for*/
        _i_23202 = _i_23202 + 1;
        goto L1; // [39] 22
L2: 
        ;
    }

    /** 		return bits*/
    DeRefDS(_bytes_23199);
    return _bits_23200;
    ;
}


int _61convert_radix(int _number_23210, int _from_radix_23211, int _to_radix_23212)
{
    int _target_23213 = NOVALUE;
    int _base_23214 = NOVALUE;
    int _13584 = NOVALUE;
    int _13583 = NOVALUE;
    int _13582 = NOVALUE;
    int _13581 = NOVALUE;
    int _0, _1, _2;
    

    /** 		base = {1}*/
    RefDS(_13579);
    DeRef(_base_23214);
    _base_23214 = _13579;

    /** 		target = {0}*/
    _0 = _target_23213;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    _target_23213 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 		for i = 1 to length(number) do*/
    if (IS_SEQUENCE(_number_23210)){
            _13581 = SEQ_PTR(_number_23210)->length;
    }
    else {
        _13581 = 1;
    }
    {
        int _i_23218;
        _i_23218 = 1;
L1: 
        if (_i_23218 > _13581){
            goto L2; // [25] 78
        }

        /** 				target = carry( add( base * number[i], target ), to_radix )*/
        _2 = (int)SEQ_PTR(_number_23210);
        _13582 = (int)*(((s1_ptr)_2)->base + _i_23218);
        _13583 = binary_op(MULTIPLY, _base_23214, _13582);
        _13582 = NOVALUE;
        RefDS(_target_23213);
        _13584 = _61add(_13583, _target_23213);
        _13583 = NOVALUE;
        _0 = _target_23213;
        _target_23213 = _61carry(_13584, _to_radix_23212);
        DeRefDS(_0);
        _13584 = NOVALUE;

        /** 				base *= from_radix*/
        _0 = _base_23214;
        _base_23214 = binary_op(MULTIPLY, _base_23214, _from_radix_23211);
        DeRefDS(_0);

        /** 				base = carry( base, to_radix )*/
        RefDS(_base_23214);
        _0 = _base_23214;
        _base_23214 = _61carry(_base_23214, _to_radix_23212);
        DeRefDS(_0);

        /** 		end for*/
        _i_23218 = _i_23218 + 1;
        goto L1; // [73] 32
L2: 
        ;
    }

    /** 		return target*/
    DeRefDS(_number_23210);
    DeRef(_base_23214);
    return _target_23213;
    ;
}


int _61half(int _decimal_23228)
{
    int _quotient_23229 = NOVALUE;
    int _q_23230 = NOVALUE;
    int _Q_23231 = NOVALUE;
    int _13605 = NOVALUE;
    int _13604 = NOVALUE;
    int _13603 = NOVALUE;
    int _13602 = NOVALUE;
    int _13601 = NOVALUE;
    int _13600 = NOVALUE;
    int _13597 = NOVALUE;
    int _13595 = NOVALUE;
    int _13594 = NOVALUE;
    int _13591 = NOVALUE;
    int _13590 = NOVALUE;
    int _13588 = NOVALUE;
    int _0, _1, _2;
    

    /** 		quotient = repeat( 0, length(decimal) )*/
    if (IS_SEQUENCE(_decimal_23228)){
            _13588 = SEQ_PTR(_decimal_23228)->length;
    }
    else {
        _13588 = 1;
    }
    DeRef(_quotient_23229);
    _quotient_23229 = Repeat(0, _13588);
    _13588 = NOVALUE;

    /** 		for i = 1 to length( decimal ) do*/
    if (IS_SEQUENCE(_decimal_23228)){
            _13590 = SEQ_PTR(_decimal_23228)->length;
    }
    else {
        _13590 = 1;
    }
    {
        int _i_23235;
        _i_23235 = 1;
L1: 
        if (_i_23235 > _13590){
            goto L2; // [17] 101
        }

        /** 				q = decimal[i] / 2*/
        _2 = (int)SEQ_PTR(_decimal_23228);
        _13591 = (int)*(((s1_ptr)_2)->base + _i_23235);
        DeRef(_q_23230);
        if (IS_ATOM_INT(_13591)) {
            if (_13591 & 1) {
                _q_23230 = NewDouble((_13591 >> 1) + 0.5);
            }
            else
            _q_23230 = _13591 >> 1;
        }
        else {
            _q_23230 = binary_op(DIVIDE, _13591, 2);
        }
        _13591 = NOVALUE;

        /** 				Q = floor( q )*/
        DeRef(_Q_23231);
        if (IS_ATOM_INT(_q_23230))
        _Q_23231 = e_floor(_q_23230);
        else
        _Q_23231 = unary_op(FLOOR, _q_23230);

        /** 				quotient[i] +=  Q*/
        _2 = (int)SEQ_PTR(_quotient_23229);
        _13594 = (int)*(((s1_ptr)_2)->base + _i_23235);
        if (IS_ATOM_INT(_13594) && IS_ATOM_INT(_Q_23231)) {
            _13595 = _13594 + _Q_23231;
            if ((long)((unsigned long)_13595 + (unsigned long)HIGH_BITS) >= 0) 
            _13595 = NewDouble((double)_13595);
        }
        else {
            _13595 = binary_op(PLUS, _13594, _Q_23231);
        }
        _13594 = NOVALUE;
        _2 = (int)SEQ_PTR(_quotient_23229);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _quotient_23229 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_23235);
        _1 = *(int *)_2;
        *(int *)_2 = _13595;
        if( _1 != _13595 ){
            DeRef(_1);
        }
        _13595 = NOVALUE;

        /** 				if q != Q then*/
        if (binary_op_a(EQUALS, _q_23230, _Q_23231)){
            goto L3; // [55] 94
        }

        /** 						if length(quotient) = i then*/
        if (IS_SEQUENCE(_quotient_23229)){
                _13597 = SEQ_PTR(_quotient_23229)->length;
        }
        else {
            _13597 = 1;
        }
        if (_13597 != _i_23235)
        goto L4; // [64] 75

        /** 								quotient &= 0*/
        Append(&_quotient_23229, _quotient_23229, 0);
L4: 

        /** 						quotient[i+1] += 5*/
        _13600 = _i_23235 + 1;
        _2 = (int)SEQ_PTR(_quotient_23229);
        _13601 = (int)*(((s1_ptr)_2)->base + _13600);
        if (IS_ATOM_INT(_13601)) {
            _13602 = _13601 + 5;
            if ((long)((unsigned long)_13602 + (unsigned long)HIGH_BITS) >= 0) 
            _13602 = NewDouble((double)_13602);
        }
        else {
            _13602 = binary_op(PLUS, _13601, 5);
        }
        _13601 = NOVALUE;
        _2 = (int)SEQ_PTR(_quotient_23229);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _quotient_23229 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _13600);
        _1 = *(int *)_2;
        *(int *)_2 = _13602;
        if( _1 != _13602 ){
            DeRef(_1);
        }
        _13602 = NOVALUE;
L3: 

        /** 		end for*/
        _i_23235 = _i_23235 + 1;
        goto L1; // [96] 24
L2: 
        ;
    }

    /** 		return reverse( carry( reverse( quotient ), 10 ) )*/
    RefDS(_quotient_23229);
    _13603 = _61reverse(_quotient_23229);
    _13604 = _61carry(_13603, 10);
    _13603 = NOVALUE;
    _13605 = _61reverse(_13604);
    _13604 = NOVALUE;
    DeRefDS(_decimal_23228);
    DeRefDS(_quotient_23229);
    DeRef(_q_23230);
    DeRef(_Q_23231);
    DeRef(_13600);
    _13600 = NOVALUE;
    return _13605;
    ;
}


int _61decimals_to_bits(int _decimals_23264)
{
    int _sub_23265 = NOVALUE;
    int _bits_23266 = NOVALUE;
    int _bit_23267 = NOVALUE;
    int _assigned_23268 = NOVALUE;
    int _13632 = NOVALUE;
    int _13628 = NOVALUE;
    int _13627 = NOVALUE;
    int _13626 = NOVALUE;
    int _13625 = NOVALUE;
    int _13623 = NOVALUE;
    int _13622 = NOVALUE;
    int _13621 = NOVALUE;
    int _13619 = NOVALUE;
    int _13617 = NOVALUE;
    int _13616 = NOVALUE;
    int _13615 = NOVALUE;
    int _13614 = NOVALUE;
    int _13612 = NOVALUE;
    int _13610 = NOVALUE;
    int _0, _1, _2;
    

    /** 		sub = {5}*/
    RefDS(_13608);
    DeRef(_sub_23265);
    _sub_23265 = _13608;

    /** 		bits = repeat( 0, 53 )*/
    DeRef(_bits_23266);
    _bits_23266 = Repeat(0, 53);

    /** 		bit = 1*/
    _bit_23267 = 1;

    /** 		assigned = 0*/
    _assigned_23268 = 0;

    /** 		if compare(decimals, bits) > 0 then */
    if (IS_ATOM_INT(_decimals_23264) && IS_ATOM_INT(_bits_23266)){
        _13610 = (_decimals_23264 < _bits_23266) ? -1 : (_decimals_23264 > _bits_23266);
    }
    else{
        _13610 = compare(_decimals_23264, _bits_23266);
    }
    if (_13610 <= 0)
    goto L1; // [32] 160

    /** 			while (not assigned) or (bit < find( 1, bits ) + 54)  do*/
L2: 
    _13612 = (_assigned_23268 == 0);
    if (_13612 != 0) {
        goto L3; // [44] 66
    }
    _13614 = find_from(1, _bits_23266, 1);
    _13615 = _13614 + 54;
    _13614 = NOVALUE;
    _13616 = (_bit_23267 < _13615);
    _13615 = NOVALUE;
    if (_13616 == 0)
    {
        DeRef(_13616);
        _13616 = NOVALUE;
        goto L4; // [62] 159
    }
    else{
        DeRef(_13616);
        _13616 = NOVALUE;
    }
L3: 

    /** 				if compare( sub, decimals ) <= 0 then*/
    if (IS_ATOM_INT(_sub_23265) && IS_ATOM_INT(_decimals_23264)){
        _13617 = (_sub_23265 < _decimals_23264) ? -1 : (_sub_23265 > _decimals_23264);
    }
    else{
        _13617 = compare(_sub_23265, _decimals_23264);
    }
    if (_13617 > 0)
    goto L5; // [72] 140

    /** 						assigned = 1*/
    _assigned_23268 = 1;

    /** 						if length( bits ) < bit then*/
    if (IS_SEQUENCE(_bits_23266)){
            _13619 = SEQ_PTR(_bits_23266)->length;
    }
    else {
        _13619 = 1;
    }
    if (_13619 >= _bit_23267)
    goto L6; // [86] 108

    /** 								bits &= repeat( 0, bit - length(bits)) */
    if (IS_SEQUENCE(_bits_23266)){
            _13621 = SEQ_PTR(_bits_23266)->length;
    }
    else {
        _13621 = 1;
    }
    _13622 = _bit_23267 - _13621;
    _13621 = NOVALUE;
    _13623 = Repeat(0, _13622);
    _13622 = NOVALUE;
    Concat((object_ptr)&_bits_23266, _bits_23266, _13623);
    DeRefDS(_13623);
    _13623 = NOVALUE;
L6: 

    /** 						bits[bit] += 1*/
    _2 = (int)SEQ_PTR(_bits_23266);
    _13625 = (int)*(((s1_ptr)_2)->base + _bit_23267);
    if (IS_ATOM_INT(_13625)) {
        _13626 = _13625 + 1;
        if (_13626 > MAXINT){
            _13626 = NewDouble((double)_13626);
        }
    }
    else
    _13626 = binary_op(PLUS, 1, _13625);
    _13625 = NOVALUE;
    _2 = (int)SEQ_PTR(_bits_23266);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bits_23266 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bit_23267);
    _1 = *(int *)_2;
    *(int *)_2 = _13626;
    if( _1 != _13626 ){
        DeRef(_1);
    }
    _13626 = NOVALUE;

    /** 						decimals = borrow( add( decimals, -sub ), 10 )*/
    _13627 = unary_op(UMINUS, _sub_23265);
    RefDS(_decimals_23264);
    _13628 = _61add(_decimals_23264, _13627);
    _13627 = NOVALUE;
    _0 = _decimals_23264;
    _decimals_23264 = _61borrow(_13628, 10);
    DeRefDS(_0);
    _13628 = NOVALUE;
L5: 

    /** 				sub = half( sub )*/
    RefDS(_sub_23265);
    _0 = _sub_23265;
    _sub_23265 = _61half(_sub_23265);
    DeRefDS(_0);

    /** 				bit += 1*/
    _bit_23267 = _bit_23267 + 1;

    /** 			end while*/
    goto L2; // [156] 41
L4: 
L1: 

    /** 		return reverse(bits)*/
    RefDS(_bits_23266);
    _13632 = _61reverse(_bits_23266);
    DeRefDS(_decimals_23264);
    DeRef(_sub_23265);
    DeRefDS(_bits_23266);
    DeRef(_13612);
    _13612 = NOVALUE;
    return _13632;
    ;
}


int _61string_to_int(int _s_23300)
{
    int _int_23301 = NOVALUE;
    int _13636 = NOVALUE;
    int _13635 = NOVALUE;
    int _13633 = NOVALUE;
    int _0, _1, _2;
    

    /** 		int = 0*/
    _int_23301 = 0;

    /** 		for i = 1 to length(s) do*/
    if (IS_SEQUENCE(_s_23300)){
            _13633 = SEQ_PTR(_s_23300)->length;
    }
    else {
        _13633 = 1;
    }
    {
        int _i_23303;
        _i_23303 = 1;
L1: 
        if (_i_23303 > _13633){
            goto L2; // [13] 51
        }

        /** 				int *= 10*/
        _int_23301 = _int_23301 * 10;

        /** 				int += s[i] - '0'*/
        _2 = (int)SEQ_PTR(_s_23300);
        _13635 = (int)*(((s1_ptr)_2)->base + _i_23303);
        if (IS_ATOM_INT(_13635)) {
            _13636 = _13635 - 48;
            if ((long)((unsigned long)_13636 +(unsigned long) HIGH_BITS) >= 0){
                _13636 = NewDouble((double)_13636);
            }
        }
        else {
            _13636 = binary_op(MINUS, _13635, 48);
        }
        _13635 = NOVALUE;
        if (IS_ATOM_INT(_13636)) {
            _int_23301 = _int_23301 + _13636;
        }
        else {
            _int_23301 = binary_op(PLUS, _int_23301, _13636);
        }
        DeRef(_13636);
        _13636 = NOVALUE;
        if (!IS_ATOM_INT(_int_23301)) {
            _1 = (long)(DBL_PTR(_int_23301)->dbl);
            if (UNIQUE(DBL_PTR(_int_23301)) && (DBL_PTR(_int_23301)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_int_23301);
            _int_23301 = _1;
        }

        /** 		end for*/
        _i_23303 = _i_23303 + 1;
        goto L1; // [46] 20
L2: 
        ;
    }

    /** 		return int*/
    DeRefDS(_s_23300);
    return _int_23301;
    ;
}


int _61trim_bits(int _bits_23311)
{
    int _13644 = NOVALUE;
    int _13643 = NOVALUE;
    int _13642 = NOVALUE;
    int _13641 = NOVALUE;
    int _13640 = NOVALUE;
    int _13639 = NOVALUE;
    int _13638 = NOVALUE;
    int _0, _1, _2;
    

    /** 		while length(bits) and not bits[$] do*/
L1: 
    if (IS_SEQUENCE(_bits_23311)){
            _13638 = SEQ_PTR(_bits_23311)->length;
    }
    else {
        _13638 = 1;
    }
    if (_13638 == 0) {
        goto L2; // [11] 48
    }
    if (IS_SEQUENCE(_bits_23311)){
            _13640 = SEQ_PTR(_bits_23311)->length;
    }
    else {
        _13640 = 1;
    }
    _2 = (int)SEQ_PTR(_bits_23311);
    _13641 = (int)*(((s1_ptr)_2)->base + _13640);
    if (IS_ATOM_INT(_13641)) {
        _13642 = (_13641 == 0);
    }
    else {
        _13642 = unary_op(NOT, _13641);
    }
    _13641 = NOVALUE;
    if (_13642 <= 0) {
        if (_13642 == 0) {
            DeRef(_13642);
            _13642 = NOVALUE;
            goto L2; // [26] 48
        }
        else {
            if (!IS_ATOM_INT(_13642) && DBL_PTR(_13642)->dbl == 0.0){
                DeRef(_13642);
                _13642 = NOVALUE;
                goto L2; // [26] 48
            }
            DeRef(_13642);
            _13642 = NOVALUE;
        }
    }
    DeRef(_13642);
    _13642 = NOVALUE;

    /** 				bits = bits[1..$-1]*/
    if (IS_SEQUENCE(_bits_23311)){
            _13643 = SEQ_PTR(_bits_23311)->length;
    }
    else {
        _13643 = 1;
    }
    _13644 = _13643 - 1;
    _13643 = NOVALUE;
    rhs_slice_target = (object_ptr)&_bits_23311;
    RHS_Slice(_bits_23311, 1, _13644);

    /** 		end while*/
    goto L1; // [45] 8
L2: 

    /** 		return bits*/
    DeRef(_13644);
    _13644 = NOVALUE;
    return _bits_23311;
    ;
}


int _61scientific_to_float64(int _s_23333)
{
    int _dp_23334 = NOVALUE;
    int _e_23335 = NOVALUE;
    int _exp_23336 = NOVALUE;
    int _almost_nothing_23337 = NOVALUE;
    int _carried_23338 = NOVALUE;
    int _int_bits_23339 = NOVALUE;
    int _frac_bits_23340 = NOVALUE;
    int _mbits_23341 = NOVALUE;
    int _ebits_23342 = NOVALUE;
    int _sbits_23343 = NOVALUE;
    int _mbits_len_23530 = NOVALUE;
    int _mbits_len_23551 = NOVALUE;
    int _13843 = NOVALUE;
    int _13842 = NOVALUE;
    int _13841 = NOVALUE;
    int _13840 = NOVALUE;
    int _13839 = NOVALUE;
    int _13838 = NOVALUE;
    int _13837 = NOVALUE;
    int _13835 = NOVALUE;
    int _13834 = NOVALUE;
    int _13829 = NOVALUE;
    int _13828 = NOVALUE;
    int _13827 = NOVALUE;
    int _13825 = NOVALUE;
    int _13824 = NOVALUE;
    int _13821 = NOVALUE;
    int _13820 = NOVALUE;
    int _13819 = NOVALUE;
    int _13818 = NOVALUE;
    int _13817 = NOVALUE;
    int _13816 = NOVALUE;
    int _13815 = NOVALUE;
    int _13813 = NOVALUE;
    int _13812 = NOVALUE;
    int _13811 = NOVALUE;
    int _13810 = NOVALUE;
    int _13808 = NOVALUE;
    int _13807 = NOVALUE;
    int _13804 = NOVALUE;
    int _13803 = NOVALUE;
    int _13802 = NOVALUE;
    int _13801 = NOVALUE;
    int _13800 = NOVALUE;
    int _13799 = NOVALUE;
    int _13798 = NOVALUE;
    int _13795 = NOVALUE;
    int _13792 = NOVALUE;
    int _13791 = NOVALUE;
    int _13790 = NOVALUE;
    int _13787 = NOVALUE;
    int _13786 = NOVALUE;
    int _13784 = NOVALUE;
    int _13783 = NOVALUE;
    int _13781 = NOVALUE;
    int _13779 = NOVALUE;
    int _13778 = NOVALUE;
    int _13776 = NOVALUE;
    int _13774 = NOVALUE;
    int _13773 = NOVALUE;
    int _13772 = NOVALUE;
    int _13771 = NOVALUE;
    int _13770 = NOVALUE;
    int _13769 = NOVALUE;
    int _13768 = NOVALUE;
    int _13767 = NOVALUE;
    int _13765 = NOVALUE;
    int _13764 = NOVALUE;
    int _13763 = NOVALUE;
    int _13762 = NOVALUE;
    int _13760 = NOVALUE;
    int _13758 = NOVALUE;
    int _13757 = NOVALUE;
    int _13756 = NOVALUE;
    int _13755 = NOVALUE;
    int _13754 = NOVALUE;
    int _13752 = NOVALUE;
    int _13751 = NOVALUE;
    int _13750 = NOVALUE;
    int _13749 = NOVALUE;
    int _13748 = NOVALUE;
    int _13747 = NOVALUE;
    int _13745 = NOVALUE;
    int _13744 = NOVALUE;
    int _13743 = NOVALUE;
    int _13742 = NOVALUE;
    int _13741 = NOVALUE;
    int _13739 = NOVALUE;
    int _13738 = NOVALUE;
    int _13736 = NOVALUE;
    int _13735 = NOVALUE;
    int _13733 = NOVALUE;
    int _13732 = NOVALUE;
    int _13731 = NOVALUE;
    int _13730 = NOVALUE;
    int _13729 = NOVALUE;
    int _13724 = NOVALUE;
    int _13721 = NOVALUE;
    int _13720 = NOVALUE;
    int _13719 = NOVALUE;
    int _13716 = NOVALUE;
    int _13715 = NOVALUE;
    int _13714 = NOVALUE;
    int _13713 = NOVALUE;
    int _13711 = NOVALUE;
    int _13710 = NOVALUE;
    int _13706 = NOVALUE;
    int _13705 = NOVALUE;
    int _13704 = NOVALUE;
    int _13703 = NOVALUE;
    int _13702 = NOVALUE;
    int _13700 = NOVALUE;
    int _13699 = NOVALUE;
    int _13697 = NOVALUE;
    int _13694 = NOVALUE;
    int _13693 = NOVALUE;
    int _13692 = NOVALUE;
    int _13691 = NOVALUE;
    int _13690 = NOVALUE;
    int _13688 = NOVALUE;
    int _13687 = NOVALUE;
    int _13686 = NOVALUE;
    int _13685 = NOVALUE;
    int _13683 = NOVALUE;
    int _13682 = NOVALUE;
    int _13681 = NOVALUE;
    int _13680 = NOVALUE;
    int _13678 = NOVALUE;
    int _13677 = NOVALUE;
    int _13675 = NOVALUE;
    int _13674 = NOVALUE;
    int _13673 = NOVALUE;
    int _13672 = NOVALUE;
    int _13670 = NOVALUE;
    int _13669 = NOVALUE;
    int _13663 = NOVALUE;
    int _13662 = NOVALUE;
    int _13661 = NOVALUE;
    int _13660 = NOVALUE;
    int _13659 = NOVALUE;
    int _13657 = NOVALUE;
    int _13655 = NOVALUE;
    int _13652 = NOVALUE;
    int _13650 = NOVALUE;
    int _0, _1, _2;
    

    /** 		almost_nothing = 0*/
    _almost_nothing_23337 = 0;

    /** 		carried = 0*/
    _carried_23338 = 0;

    /** 		if s[1] = '-' then*/
    _2 = (int)SEQ_PTR(_s_23333);
    _13650 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _13650, 45)){
        _13650 = NOVALUE;
        goto L1; // [19] 43
    }
    _13650 = NOVALUE;

    /** 				sbits = {1}*/
    RefDS(_13579);
    DeRefi(_sbits_23343);
    _sbits_23343 = _13579;

    /** 				s = s[2..$]*/
    if (IS_SEQUENCE(_s_23333)){
            _13652 = SEQ_PTR(_s_23333)->length;
    }
    else {
        _13652 = 1;
    }
    rhs_slice_target = (object_ptr)&_s_23333;
    RHS_Slice(_s_23333, 2, _13652);
    goto L2; // [40] 71
L1: 

    /** 				sbits = {0}*/
    _0 = _sbits_23343;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    _sbits_23343 = MAKE_SEQ(_1);
    DeRefi(_0);

    /** 				if s[1] = '+' then*/
    _2 = (int)SEQ_PTR(_s_23333);
    _13655 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _13655, 43)){
        _13655 = NOVALUE;
        goto L3; // [55] 70
    }
    _13655 = NOVALUE;

    /** 						s = s[2..$]*/
    if (IS_SEQUENCE(_s_23333)){
            _13657 = SEQ_PTR(_s_23333)->length;
    }
    else {
        _13657 = 1;
    }
    rhs_slice_target = (object_ptr)&_s_23333;
    RHS_Slice(_s_23333, 2, _13657);
L3: 
L2: 

    /** 		while length(s) and s[1] = '0' do*/
L4: 
    if (IS_SEQUENCE(_s_23333)){
            _13659 = SEQ_PTR(_s_23333)->length;
    }
    else {
        _13659 = 1;
    }
    if (_13659 == 0) {
        goto L5; // [79] 110
    }
    _2 = (int)SEQ_PTR(_s_23333);
    _13661 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_13661)) {
        _13662 = (_13661 == 48);
    }
    else {
        _13662 = binary_op(EQUALS, _13661, 48);
    }
    _13661 = NOVALUE;
    if (_13662 <= 0) {
        if (_13662 == 0) {
            DeRef(_13662);
            _13662 = NOVALUE;
            goto L5; // [92] 110
        }
        else {
            if (!IS_ATOM_INT(_13662) && DBL_PTR(_13662)->dbl == 0.0){
                DeRef(_13662);
                _13662 = NOVALUE;
                goto L5; // [92] 110
            }
            DeRef(_13662);
            _13662 = NOVALUE;
        }
    }
    DeRef(_13662);
    _13662 = NOVALUE;

    /** 			s = s[2..$]*/
    if (IS_SEQUENCE(_s_23333)){
            _13663 = SEQ_PTR(_s_23333)->length;
    }
    else {
        _13663 = 1;
    }
    rhs_slice_target = (object_ptr)&_s_23333;
    RHS_Slice(_s_23333, 2, _13663);

    /** 		end while*/
    goto L4; // [107] 76
L5: 

    /** 		dp = find('.', s)		*/
    _dp_23334 = find_from(46, _s_23333, 1);

    /** 		e = find( 'e', s )*/
    _e_23335 = find_from(101, _s_23333, 1);

    /** 		if not e then*/
    if (_e_23335 != 0)
    goto L6; // [126] 137

    /** 				e = find('E', s )*/
    _e_23335 = find_from(69, _s_23333, 1);
L6: 

    /** 		exp = 0*/
    _exp_23336 = 0;

    /** 		if s[e+1] = '-' then*/
    _13669 = _e_23335 + 1;
    _2 = (int)SEQ_PTR(_s_23333);
    _13670 = (int)*(((s1_ptr)_2)->base + _13669);
    if (binary_op_a(NOTEQ, _13670, 45)){
        _13670 = NOVALUE;
        goto L7; // [152] 183
    }
    _13670 = NOVALUE;

    /** 				exp -= string_to_int( s[e+2..$] )*/
    _13672 = _e_23335 + 2;
    if ((long)((unsigned long)_13672 + (unsigned long)HIGH_BITS) >= 0) 
    _13672 = NewDouble((double)_13672);
    if (IS_SEQUENCE(_s_23333)){
            _13673 = SEQ_PTR(_s_23333)->length;
    }
    else {
        _13673 = 1;
    }
    rhs_slice_target = (object_ptr)&_13674;
    RHS_Slice(_s_23333, _13672, _13673);
    _13675 = _61string_to_int(_13674);
    _13674 = NOVALUE;
    if (IS_ATOM_INT(_13675)) {
        _exp_23336 = _exp_23336 - _13675;
    }
    else {
        _exp_23336 = binary_op(MINUS, _exp_23336, _13675);
    }
    DeRef(_13675);
    _13675 = NOVALUE;
    if (!IS_ATOM_INT(_exp_23336)) {
        _1 = (long)(DBL_PTR(_exp_23336)->dbl);
        if (UNIQUE(DBL_PTR(_exp_23336)) && (DBL_PTR(_exp_23336)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_exp_23336);
        _exp_23336 = _1;
    }
    goto L8; // [180] 250
L7: 

    /** 				if s[e+1] = '+' then*/
    _13677 = _e_23335 + 1;
    _2 = (int)SEQ_PTR(_s_23333);
    _13678 = (int)*(((s1_ptr)_2)->base + _13677);
    if (binary_op_a(NOTEQ, _13678, 43)){
        _13678 = NOVALUE;
        goto L9; // [193] 224
    }
    _13678 = NOVALUE;

    /** 						exp += string_to_int( s[e+2..$] )*/
    _13680 = _e_23335 + 2;
    if ((long)((unsigned long)_13680 + (unsigned long)HIGH_BITS) >= 0) 
    _13680 = NewDouble((double)_13680);
    if (IS_SEQUENCE(_s_23333)){
            _13681 = SEQ_PTR(_s_23333)->length;
    }
    else {
        _13681 = 1;
    }
    rhs_slice_target = (object_ptr)&_13682;
    RHS_Slice(_s_23333, _13680, _13681);
    _13683 = _61string_to_int(_13682);
    _13682 = NOVALUE;
    if (IS_ATOM_INT(_13683)) {
        _exp_23336 = _exp_23336 + _13683;
    }
    else {
        _exp_23336 = binary_op(PLUS, _exp_23336, _13683);
    }
    DeRef(_13683);
    _13683 = NOVALUE;
    if (!IS_ATOM_INT(_exp_23336)) {
        _1 = (long)(DBL_PTR(_exp_23336)->dbl);
        if (UNIQUE(DBL_PTR(_exp_23336)) && (DBL_PTR(_exp_23336)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_exp_23336);
        _exp_23336 = _1;
    }
    goto LA; // [221] 249
L9: 

    /** 						exp += string_to_int( s[e+1..$] )*/
    _13685 = _e_23335 + 1;
    if (_13685 > MAXINT){
        _13685 = NewDouble((double)_13685);
    }
    if (IS_SEQUENCE(_s_23333)){
            _13686 = SEQ_PTR(_s_23333)->length;
    }
    else {
        _13686 = 1;
    }
    rhs_slice_target = (object_ptr)&_13687;
    RHS_Slice(_s_23333, _13685, _13686);
    _13688 = _61string_to_int(_13687);
    _13687 = NOVALUE;
    if (IS_ATOM_INT(_13688)) {
        _exp_23336 = _exp_23336 + _13688;
    }
    else {
        _exp_23336 = binary_op(PLUS, _exp_23336, _13688);
    }
    DeRef(_13688);
    _13688 = NOVALUE;
    if (!IS_ATOM_INT(_exp_23336)) {
        _1 = (long)(DBL_PTR(_exp_23336)->dbl);
        if (UNIQUE(DBL_PTR(_exp_23336)) && (DBL_PTR(_exp_23336)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_exp_23336);
        _exp_23336 = _1;
    }
LA: 
L8: 

    /** 		if dp then*/
    if (_dp_23334 == 0)
    {
        goto LB; // [252] 301
    }
    else{
    }

    /** 				s = s[1..dp-1] & s[dp+1..$]*/
    _13690 = _dp_23334 - 1;
    rhs_slice_target = (object_ptr)&_13691;
    RHS_Slice(_s_23333, 1, _13690);
    _13692 = _dp_23334 + 1;
    if (_13692 > MAXINT){
        _13692 = NewDouble((double)_13692);
    }
    if (IS_SEQUENCE(_s_23333)){
            _13693 = SEQ_PTR(_s_23333)->length;
    }
    else {
        _13693 = 1;
    }
    rhs_slice_target = (object_ptr)&_13694;
    RHS_Slice(_s_23333, _13692, _13693);
    Concat((object_ptr)&_s_23333, _13691, _13694);
    DeRefDS(_13691);
    _13691 = NOVALUE;
    DeRef(_13691);
    _13691 = NOVALUE;
    DeRefDS(_13694);
    _13694 = NOVALUE;

    /** 				e -= 1*/
    _e_23335 = _e_23335 - 1;

    /** 				exp -= e - dp*/
    _13697 = _e_23335 - _dp_23334;
    if ((long)((unsigned long)_13697 +(unsigned long) HIGH_BITS) >= 0){
        _13697 = NewDouble((double)_13697);
    }
    if (IS_ATOM_INT(_13697)) {
        _exp_23336 = _exp_23336 - _13697;
    }
    else {
        _exp_23336 = NewDouble((double)_exp_23336 - DBL_PTR(_13697)->dbl);
    }
    DeRef(_13697);
    _13697 = NOVALUE;
    if (!IS_ATOM_INT(_exp_23336)) {
        _1 = (long)(DBL_PTR(_exp_23336)->dbl);
        if (UNIQUE(DBL_PTR(_exp_23336)) && (DBL_PTR(_exp_23336)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_exp_23336);
        _exp_23336 = _1;
    }
LB: 

    /** 		s = s[1..e-1] - '0'*/
    _13699 = _e_23335 - 1;
    rhs_slice_target = (object_ptr)&_13700;
    RHS_Slice(_s_23333, 1, _13699);
    DeRefDS(_s_23333);
    _s_23333 = binary_op(MINUS, _13700, 48);
    DeRefDS(_13700);
    _13700 = NOVALUE;

    /** 		while length(s) and s[1] = 0 do*/
LC: 
    if (IS_SEQUENCE(_s_23333)){
            _13702 = SEQ_PTR(_s_23333)->length;
    }
    else {
        _13702 = 1;
    }
    if (_13702 == 0) {
        goto LD; // [326] 369
    }
    _2 = (int)SEQ_PTR(_s_23333);
    _13704 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_13704)) {
        _13705 = (_13704 == 0);
    }
    else {
        _13705 = binary_op(EQUALS, _13704, 0);
    }
    _13704 = NOVALUE;
    if (_13705 <= 0) {
        if (_13705 == 0) {
            DeRef(_13705);
            _13705 = NOVALUE;
            goto LD; // [339] 369
        }
        else {
            if (!IS_ATOM_INT(_13705) && DBL_PTR(_13705)->dbl == 0.0){
                DeRef(_13705);
                _13705 = NOVALUE;
                goto LD; // [339] 369
            }
            DeRef(_13705);
            _13705 = NOVALUE;
        }
    }
    DeRef(_13705);
    _13705 = NOVALUE;

    /** 			s = s[2..$]*/
    if (IS_SEQUENCE(_s_23333)){
            _13706 = SEQ_PTR(_s_23333)->length;
    }
    else {
        _13706 = 1;
    }
    rhs_slice_target = (object_ptr)&_s_23333;
    RHS_Slice(_s_23333, 2, _13706);

    /** 			e -= 1*/
    _e_23335 = _e_23335 - 1;

    /** 			dp -= 1*/
    _dp_23334 = _dp_23334 - 1;

    /** 		end while*/
    goto LC; // [366] 323
LD: 

    /** 		if not find(0, s = 0) then*/
    _13710 = binary_op(EQUALS, _s_23333, 0);
    _13711 = find_from(0, _13710, 1);
    DeRefDS(_13710);
    _13710 = NOVALUE;
    if (_13711 != 0)
    goto LE; // [380] 394
    _13711 = NOVALUE;

    /** 			return atom_to_float64(0)*/
    _13713 = _6atom_to_float64(0);
    DeRefDS(_s_23333);
    DeRef(_int_bits_23339);
    DeRef(_frac_bits_23340);
    DeRef(_mbits_23341);
    DeRef(_ebits_23342);
    DeRefi(_sbits_23343);
    DeRef(_13669);
    _13669 = NOVALUE;
    DeRef(_13672);
    _13672 = NOVALUE;
    DeRef(_13677);
    _13677 = NOVALUE;
    DeRef(_13680);
    _13680 = NOVALUE;
    DeRef(_13685);
    _13685 = NOVALUE;
    DeRef(_13690);
    _13690 = NOVALUE;
    DeRef(_13699);
    _13699 = NOVALUE;
    DeRef(_13692);
    _13692 = NOVALUE;
    return _13713;
LE: 

    /** 		if exp + length(s) - 1 > 308 then*/
    if (IS_SEQUENCE(_s_23333)){
            _13714 = SEQ_PTR(_s_23333)->length;
    }
    else {
        _13714 = 1;
    }
    _13715 = _exp_23336 + _13714;
    if ((long)((unsigned long)_13715 + (unsigned long)HIGH_BITS) >= 0) 
    _13715 = NewDouble((double)_13715);
    _13714 = NOVALUE;
    if (IS_ATOM_INT(_13715)) {
        _13716 = _13715 - 1;
        if ((long)((unsigned long)_13716 +(unsigned long) HIGH_BITS) >= 0){
            _13716 = NewDouble((double)_13716);
        }
    }
    else {
        _13716 = NewDouble(DBL_PTR(_13715)->dbl - (double)1);
    }
    DeRef(_13715);
    _13715 = NOVALUE;
    if (binary_op_a(LESSEQ, _13716, 308)){
        DeRef(_13716);
        _13716 = NOVALUE;
        goto LF; // [407] 425
    }
    DeRef(_13716);
    _13716 = NOVALUE;

    /** 			exp = 1024*/
    _exp_23336 = 1024;

    /** 			mbits = repeat(0, 52)*/
    DeRef(_mbits_23341);
    _mbits_23341 = Repeat(0, 52);
    goto L10; // [422] 1057
LF: 

    /** 		elsif exp + length(s) - 1 < -324 then -- -324 = floor((-1022-52)*log(2)/log(10))*/
    if (IS_SEQUENCE(_s_23333)){
            _13719 = SEQ_PTR(_s_23333)->length;
    }
    else {
        _13719 = 1;
    }
    _13720 = _exp_23336 + _13719;
    if ((long)((unsigned long)_13720 + (unsigned long)HIGH_BITS) >= 0) 
    _13720 = NewDouble((double)_13720);
    _13719 = NOVALUE;
    if (IS_ATOM_INT(_13720)) {
        _13721 = _13720 - 1;
        if ((long)((unsigned long)_13721 +(unsigned long) HIGH_BITS) >= 0){
            _13721 = NewDouble((double)_13721);
        }
    }
    else {
        _13721 = NewDouble(DBL_PTR(_13720)->dbl - (double)1);
    }
    DeRef(_13720);
    _13720 = NOVALUE;
    if (binary_op_a(GREATEREQ, _13721, -324)){
        DeRef(_13721);
        _13721 = NOVALUE;
        goto L11; // [438] 464
    }
    DeRef(_13721);
    _13721 = NOVALUE;

    /** 			fenv:raise(FE_UNDERFLOW)*/
    _13724 = _62raise(117);

    /** 			exp = -1023*/
    _exp_23336 = -1023;

    /** 			mbits = repeat(0, 52)*/
    DeRef(_mbits_23341);
    _mbits_23341 = Repeat(0, 52);
    goto L10; // [461] 1057
L11: 

    /** 			if exp >= 0 then*/
    if (_exp_23336 < 0)
    goto L12; // [466] 510

    /** 					int_bits = trim_bits( bytes_to_bits( convert_radix( repeat( 0, exp ) & reverse( s ), 10, #100 ) ) )*/
    _13729 = Repeat(0, _exp_23336);
    RefDS(_s_23333);
    _13730 = _61reverse(_s_23333);
    if (IS_SEQUENCE(_13729) && IS_ATOM(_13730)) {
        Ref(_13730);
        Append(&_13731, _13729, _13730);
    }
    else if (IS_ATOM(_13729) && IS_SEQUENCE(_13730)) {
    }
    else {
        Concat((object_ptr)&_13731, _13729, _13730);
        DeRefDS(_13729);
        _13729 = NOVALUE;
    }
    DeRef(_13729);
    _13729 = NOVALUE;
    DeRef(_13730);
    _13730 = NOVALUE;
    _13732 = _61convert_radix(_13731, 10, 256);
    _13731 = NOVALUE;
    _13733 = _61bytes_to_bits(_13732);
    _13732 = NOVALUE;
    _0 = _int_bits_23339;
    _int_bits_23339 = _61trim_bits(_13733);
    DeRef(_0);
    _13733 = NOVALUE;

    /** 					frac_bits = {}*/
    RefDS(_5);
    DeRef(_frac_bits_23340);
    _frac_bits_23340 = _5;
    goto L13; // [507] 637
L12: 

    /** 					almost_nothing = exp + e - dp = -324*/
    _13735 = _exp_23336 + _e_23335;
    if ((long)((unsigned long)_13735 + (unsigned long)HIGH_BITS) >= 0) 
    _13735 = NewDouble((double)_13735);
    if (IS_ATOM_INT(_13735)) {
        _13736 = _13735 - _dp_23334;
        if ((long)((unsigned long)_13736 +(unsigned long) HIGH_BITS) >= 0){
            _13736 = NewDouble((double)_13736);
        }
    }
    else {
        _13736 = NewDouble(DBL_PTR(_13735)->dbl - (double)_dp_23334);
    }
    DeRef(_13735);
    _13735 = NOVALUE;
    if (IS_ATOM_INT(_13736)) {
        _almost_nothing_23337 = (_13736 == -324);
    }
    else {
        _almost_nothing_23337 = (DBL_PTR(_13736)->dbl == (double)-324);
    }
    DeRef(_13736);
    _13736 = NOVALUE;

    /** 					if -exp > length(s) then*/
    if ((unsigned long)_exp_23336 == 0xC0000000)
    _13738 = (int)NewDouble((double)-0xC0000000);
    else
    _13738 = - _exp_23336;
    if (IS_SEQUENCE(_s_23333)){
            _13739 = SEQ_PTR(_s_23333)->length;
    }
    else {
        _13739 = 1;
    }
    if (binary_op_a(LESSEQ, _13738, _13739)){
        DeRef(_13738);
        _13738 = NOVALUE;
        _13739 = NOVALUE;
        goto L14; // [534] 574
    }
    DeRef(_13738);
    _13738 = NOVALUE;
    _13739 = NOVALUE;

    /** 							int_bits = {}*/
    RefDS(_5);
    DeRef(_int_bits_23339);
    _int_bits_23339 = _5;

    /** 							frac_bits = decimals_to_bits( repeat( 0, -exp-length(s) ) & s )*/
    if ((unsigned long)_exp_23336 == 0xC0000000)
    _13741 = (int)NewDouble((double)-0xC0000000);
    else
    _13741 = - _exp_23336;
    if (IS_SEQUENCE(_s_23333)){
            _13742 = SEQ_PTR(_s_23333)->length;
    }
    else {
        _13742 = 1;
    }
    if (IS_ATOM_INT(_13741)) {
        _13743 = _13741 - _13742;
    }
    else {
        _13743 = NewDouble(DBL_PTR(_13741)->dbl - (double)_13742);
    }
    DeRef(_13741);
    _13741 = NOVALUE;
    _13742 = NOVALUE;
    _13744 = Repeat(0, _13743);
    DeRef(_13743);
    _13743 = NOVALUE;
    Concat((object_ptr)&_13745, _13744, _s_23333);
    DeRefDS(_13744);
    _13744 = NOVALUE;
    DeRef(_13744);
    _13744 = NOVALUE;
    _0 = _frac_bits_23340;
    _frac_bits_23340 = _61decimals_to_bits(_13745);
    DeRef(_0);
    _13745 = NOVALUE;
    goto L15; // [571] 636
L14: 

    /** 							int_bits = trim_bits( bytes_to_bits( convert_radix( reverse( s[1..$+exp] ), 10, #100 ) ) )*/
    if (IS_SEQUENCE(_s_23333)){
            _13747 = SEQ_PTR(_s_23333)->length;
    }
    else {
        _13747 = 1;
    }
    _13748 = _13747 + _exp_23336;
    _13747 = NOVALUE;
    rhs_slice_target = (object_ptr)&_13749;
    RHS_Slice(_s_23333, 1, _13748);
    _13750 = _61reverse(_13749);
    _13749 = NOVALUE;
    _13751 = _61convert_radix(_13750, 10, 256);
    _13750 = NOVALUE;
    _13752 = _61bytes_to_bits(_13751);
    _13751 = NOVALUE;
    _0 = _int_bits_23339;
    _int_bits_23339 = _61trim_bits(_13752);
    DeRef(_0);
    _13752 = NOVALUE;

    /** 							frac_bits =  decimals_to_bits( s[$+exp+1..$] )*/
    if (IS_SEQUENCE(_s_23333)){
            _13754 = SEQ_PTR(_s_23333)->length;
    }
    else {
        _13754 = 1;
    }
    _13755 = _13754 + _exp_23336;
    if ((long)((unsigned long)_13755 + (unsigned long)HIGH_BITS) >= 0) 
    _13755 = NewDouble((double)_13755);
    _13754 = NOVALUE;
    if (IS_ATOM_INT(_13755)) {
        _13756 = _13755 + 1;
        if (_13756 > MAXINT){
            _13756 = NewDouble((double)_13756);
        }
    }
    else
    _13756 = binary_op(PLUS, 1, _13755);
    DeRef(_13755);
    _13755 = NOVALUE;
    if (IS_SEQUENCE(_s_23333)){
            _13757 = SEQ_PTR(_s_23333)->length;
    }
    else {
        _13757 = 1;
    }
    rhs_slice_target = (object_ptr)&_13758;
    RHS_Slice(_s_23333, _13756, _13757);
    _0 = _frac_bits_23340;
    _frac_bits_23340 = _61decimals_to_bits(_13758);
    DeRef(_0);
    _13758 = NOVALUE;
L15: 
L13: 

    /** 			if length(int_bits) >= 53 then*/
    if (IS_SEQUENCE(_int_bits_23339)){
            _13760 = SEQ_PTR(_int_bits_23339)->length;
    }
    else {
        _13760 = 1;
    }
    if (_13760 < 53)
    goto L16; // [644] 762

    /** 				mbits = int_bits[$-52..$-1]*/
    if (IS_SEQUENCE(_int_bits_23339)){
            _13762 = SEQ_PTR(_int_bits_23339)->length;
    }
    else {
        _13762 = 1;
    }
    _13763 = _13762 - 52;
    _13762 = NOVALUE;
    if (IS_SEQUENCE(_int_bits_23339)){
            _13764 = SEQ_PTR(_int_bits_23339)->length;
    }
    else {
        _13764 = 1;
    }
    _13765 = _13764 - 1;
    _13764 = NOVALUE;
    rhs_slice_target = (object_ptr)&_mbits_23341;
    RHS_Slice(_int_bits_23339, _13763, _13765);

    /** 				if length(int_bits) > 53 and int_bits[$-53] then*/
    if (IS_SEQUENCE(_int_bits_23339)){
            _13767 = SEQ_PTR(_int_bits_23339)->length;
    }
    else {
        _13767 = 1;
    }
    _13768 = (_13767 > 53);
    _13767 = NOVALUE;
    if (_13768 == 0) {
        goto L17; // [678] 750
    }
    if (IS_SEQUENCE(_int_bits_23339)){
            _13770 = SEQ_PTR(_int_bits_23339)->length;
    }
    else {
        _13770 = 1;
    }
    _13771 = _13770 - 53;
    _13770 = NOVALUE;
    _2 = (int)SEQ_PTR(_int_bits_23339);
    _13772 = (int)*(((s1_ptr)_2)->base + _13771);
    if (_13772 == 0) {
        _13772 = NOVALUE;
        goto L17; // [694] 750
    }
    else {
        if (!IS_ATOM_INT(_13772) && DBL_PTR(_13772)->dbl == 0.0){
            _13772 = NOVALUE;
            goto L17; // [694] 750
        }
        _13772 = NOVALUE;
    }
    _13772 = NOVALUE;

    /** 						mbits[1] += 1*/
    _2 = (int)SEQ_PTR(_mbits_23341);
    _13773 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_13773)) {
        _13774 = _13773 + 1;
        if (_13774 > MAXINT){
            _13774 = NewDouble((double)_13774);
        }
    }
    else
    _13774 = binary_op(PLUS, 1, _13773);
    _13773 = NOVALUE;
    _2 = (int)SEQ_PTR(_mbits_23341);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _mbits_23341 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _13774;
    if( _1 != _13774 ){
        DeRef(_1);
    }
    _13774 = NOVALUE;

    /** 						mbits = carry( mbits, 2 )*/
    RefDS(_mbits_23341);
    _0 = _mbits_23341;
    _mbits_23341 = _61carry(_mbits_23341, 2);
    DeRefDS(_0);

    /** 						if length(mbits) = 53 then*/
    if (IS_SEQUENCE(_mbits_23341)){
            _13776 = SEQ_PTR(_mbits_23341)->length;
    }
    else {
        _13776 = 1;
    }
    if (_13776 != 53)
    goto L18; // [725] 749

    /** 							mbits = mbits[1..$-1]*/
    if (IS_SEQUENCE(_mbits_23341)){
            _13778 = SEQ_PTR(_mbits_23341)->length;
    }
    else {
        _13778 = 1;
    }
    _13779 = _13778 - 1;
    _13778 = NOVALUE;
    rhs_slice_target = (object_ptr)&_mbits_23341;
    RHS_Slice(_mbits_23341, 1, _13779);

    /** 							carried = 1*/
    _carried_23338 = 1;
L18: 
L17: 

    /** 				exp = length(int_bits)-1*/
    if (IS_SEQUENCE(_int_bits_23339)){
            _13781 = SEQ_PTR(_int_bits_23339)->length;
    }
    else {
        _13781 = 1;
    }
    _exp_23336 = _13781 - 1;
    _13781 = NOVALUE;
    goto L19; // [759] 1050
L16: 

    /** 					if length(int_bits) then*/
    if (IS_SEQUENCE(_int_bits_23339)){
            _13783 = SEQ_PTR(_int_bits_23339)->length;
    }
    else {
        _13783 = 1;
    }
    if (_13783 == 0)
    {
        _13783 = NOVALUE;
        goto L1A; // [767] 782
    }
    else{
        _13783 = NOVALUE;
    }

    /** 							exp = length(int_bits)-1*/
    if (IS_SEQUENCE(_int_bits_23339)){
            _13784 = SEQ_PTR(_int_bits_23339)->length;
    }
    else {
        _13784 = 1;
    }
    _exp_23336 = _13784 - 1;
    _13784 = NOVALUE;
    goto L1B; // [779] 837
L1A: 

    /** 							exp = - find( 1, reverse( frac_bits ) )*/
    RefDS(_frac_bits_23340);
    _13786 = _61reverse(_frac_bits_23340);
    _13787 = find_from(1, _13786, 1);
    DeRef(_13786);
    _13786 = NOVALUE;
    _exp_23336 = - _13787;

    /** 							if exp < -1023 then*/
    if (_exp_23336 >= -1023)
    goto L1C; // [802] 812

    /** 									exp = -1023*/
    _exp_23336 = -1023;
L1C: 

    /** 							if exp then*/
    if (_exp_23336 == 0)
    {
        goto L1D; // [814] 836
    }
    else{
    }

    /** 									frac_bits = frac_bits[1..$+exp+1]*/
    if (IS_SEQUENCE(_frac_bits_23340)){
            _13790 = SEQ_PTR(_frac_bits_23340)->length;
    }
    else {
        _13790 = 1;
    }
    _13791 = _13790 + _exp_23336;
    if ((long)((unsigned long)_13791 + (unsigned long)HIGH_BITS) >= 0) 
    _13791 = NewDouble((double)_13791);
    _13790 = NOVALUE;
    if (IS_ATOM_INT(_13791)) {
        _13792 = _13791 + 1;
    }
    else
    _13792 = binary_op(PLUS, 1, _13791);
    DeRef(_13791);
    _13791 = NOVALUE;
    rhs_slice_target = (object_ptr)&_frac_bits_23340;
    RHS_Slice(_frac_bits_23340, 1, _13792);
L1D: 
L1B: 

    /** 					mbits = frac_bits & int_bits*/
    Concat((object_ptr)&_mbits_23341, _frac_bits_23340, _int_bits_23339);

    /** 					mbits = repeat( 0, 53 ) & mbits*/
    _13795 = Repeat(0, 53);
    Concat((object_ptr)&_mbits_23341, _13795, _mbits_23341);
    DeRefDS(_13795);
    _13795 = NOVALUE;
    DeRef(_13795);
    _13795 = NOVALUE;

    /** 					if exp > -1023 then*/
    if (_exp_23336 <= -1023)
    goto L1E; // [857] 958

    /** 							if mbits[$-53] then*/
    if (IS_SEQUENCE(_mbits_23341)){
            _13798 = SEQ_PTR(_mbits_23341)->length;
    }
    else {
        _13798 = 1;
    }
    _13799 = _13798 - 53;
    _13798 = NOVALUE;
    _2 = (int)SEQ_PTR(_mbits_23341);
    _13800 = (int)*(((s1_ptr)_2)->base + _13799);
    if (_13800 == 0) {
        _13800 = NOVALUE;
        goto L1F; // [874] 932
    }
    else {
        if (!IS_ATOM_INT(_13800) && DBL_PTR(_13800)->dbl == 0.0){
            _13800 = NOVALUE;
            goto L1F; // [874] 932
        }
        _13800 = NOVALUE;
    }
    _13800 = NOVALUE;

    /** 									mbits[$-52] += 1*/
    if (IS_SEQUENCE(_mbits_23341)){
            _13801 = SEQ_PTR(_mbits_23341)->length;
    }
    else {
        _13801 = 1;
    }
    _13802 = _13801 - 52;
    _13801 = NOVALUE;
    _2 = (int)SEQ_PTR(_mbits_23341);
    _13803 = (int)*(((s1_ptr)_2)->base + _13802);
    if (IS_ATOM_INT(_13803)) {
        _13804 = _13803 + 1;
        if (_13804 > MAXINT){
            _13804 = NewDouble((double)_13804);
        }
    }
    else
    _13804 = binary_op(PLUS, 1, _13803);
    _13803 = NOVALUE;
    _2 = (int)SEQ_PTR(_mbits_23341);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _mbits_23341 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _13802);
    _1 = *(int *)_2;
    *(int *)_2 = _13804;
    if( _1 != _13804 ){
        DeRef(_1);
    }
    _13804 = NOVALUE;

    /** 									integer mbits_len = length(mbits)*/
    if (IS_SEQUENCE(_mbits_23341)){
            _mbits_len_23530 = SEQ_PTR(_mbits_23341)->length;
    }
    else {
        _mbits_len_23530 = 1;
    }

    /** 									mbits = carry( mbits, 2 )*/
    RefDS(_mbits_23341);
    _0 = _mbits_23341;
    _mbits_23341 = _61carry(_mbits_23341, 2);
    DeRefDS(_0);

    /** 									if length(mbits) = mbits_len + 1 then*/
    if (IS_SEQUENCE(_mbits_23341)){
            _13807 = SEQ_PTR(_mbits_23341)->length;
    }
    else {
        _13807 = 1;
    }
    _13808 = _mbits_len_23530 + 1;
    if (_13807 != _13808)
    goto L20; // [921] 931

    /** 										carried = 1*/
    _carried_23338 = 1;
L20: 
L1F: 

    /** 							mbits = mbits[$-52..$-1]*/
    if (IS_SEQUENCE(_mbits_23341)){
            _13810 = SEQ_PTR(_mbits_23341)->length;
    }
    else {
        _13810 = 1;
    }
    _13811 = _13810 - 52;
    _13810 = NOVALUE;
    if (IS_SEQUENCE(_mbits_23341)){
            _13812 = SEQ_PTR(_mbits_23341)->length;
    }
    else {
        _13812 = 1;
    }
    _13813 = _13812 - 1;
    _13812 = NOVALUE;
    rhs_slice_target = (object_ptr)&_mbits_23341;
    RHS_Slice(_mbits_23341, _13811, _13813);
    goto L21; // [955] 1049
L1E: 

    /** 							if mbits[$-52] then*/
    if (IS_SEQUENCE(_mbits_23341)){
            _13815 = SEQ_PTR(_mbits_23341)->length;
    }
    else {
        _13815 = 1;
    }
    _13816 = _13815 - 52;
    _13815 = NOVALUE;
    _2 = (int)SEQ_PTR(_mbits_23341);
    _13817 = (int)*(((s1_ptr)_2)->base + _13816);
    if (_13817 == 0) {
        _13817 = NOVALUE;
        goto L22; // [971] 1029
    }
    else {
        if (!IS_ATOM_INT(_13817) && DBL_PTR(_13817)->dbl == 0.0){
            _13817 = NOVALUE;
            goto L22; // [971] 1029
        }
        _13817 = NOVALUE;
    }
    _13817 = NOVALUE;

    /** 									mbits[$-52] += 1*/
    if (IS_SEQUENCE(_mbits_23341)){
            _13818 = SEQ_PTR(_mbits_23341)->length;
    }
    else {
        _13818 = 1;
    }
    _13819 = _13818 - 52;
    _13818 = NOVALUE;
    _2 = (int)SEQ_PTR(_mbits_23341);
    _13820 = (int)*(((s1_ptr)_2)->base + _13819);
    if (IS_ATOM_INT(_13820)) {
        _13821 = _13820 + 1;
        if (_13821 > MAXINT){
            _13821 = NewDouble((double)_13821);
        }
    }
    else
    _13821 = binary_op(PLUS, 1, _13820);
    _13820 = NOVALUE;
    _2 = (int)SEQ_PTR(_mbits_23341);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _mbits_23341 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _13819);
    _1 = *(int *)_2;
    *(int *)_2 = _13821;
    if( _1 != _13821 ){
        DeRef(_1);
    }
    _13821 = NOVALUE;

    /** 									integer mbits_len = length(mbits)*/
    if (IS_SEQUENCE(_mbits_23341)){
            _mbits_len_23551 = SEQ_PTR(_mbits_23341)->length;
    }
    else {
        _mbits_len_23551 = 1;
    }

    /** 									mbits = carry( mbits, 2 )*/
    RefDS(_mbits_23341);
    _0 = _mbits_23341;
    _mbits_23341 = _61carry(_mbits_23341, 2);
    DeRefDS(_0);

    /** 									if length(mbits) = mbits_len + 1 then*/
    if (IS_SEQUENCE(_mbits_23341)){
            _13824 = SEQ_PTR(_mbits_23341)->length;
    }
    else {
        _13824 = 1;
    }
    _13825 = _mbits_len_23551 + 1;
    if (_13824 != _13825)
    goto L23; // [1018] 1028

    /** 										carried = 1*/
    _carried_23338 = 1;
L23: 
L22: 

    /** 							mbits = mbits[$-51..$]*/
    if (IS_SEQUENCE(_mbits_23341)){
            _13827 = SEQ_PTR(_mbits_23341)->length;
    }
    else {
        _13827 = 1;
    }
    _13828 = _13827 - 51;
    _13827 = NOVALUE;
    if (IS_SEQUENCE(_mbits_23341)){
            _13829 = SEQ_PTR(_mbits_23341)->length;
    }
    else {
        _13829 = 1;
    }
    rhs_slice_target = (object_ptr)&_mbits_23341;
    RHS_Slice(_mbits_23341, _13828, _13829);
L21: 
L19: 

    /** 			exp += carried*/
    _exp_23336 = _exp_23336 + _carried_23338;
L10: 

    /** 		if exp >= 1024 then*/
    if (_exp_23336 < 1024)
    goto L24; // [1059] 1081

    /** 			exp   = 1024*/
    _exp_23336 = 1024;

    /** 			mbits = repeat(0, 52)*/
    DeRef(_mbits_23341);
    _mbits_23341 = Repeat(0, 52);

    /** 			fenv:raise(fenv:FE_OVERFLOW)*/
    _13834 = _62raise(111);
L24: 

    /** 		ebits = int_to_bits( exp + 1023, 11 )*/
    _13835 = _exp_23336 + 1023;
    if ((long)((unsigned long)_13835 + (unsigned long)HIGH_BITS) >= 0) 
    _13835 = NewDouble((double)_13835);
    _0 = _ebits_23342;
    _ebits_23342 = _6int_to_bits(_13835, 11);
    DeRef(_0);
    _13835 = NOVALUE;

    /** 		if almost_nothing and not find(1, mbits & ebits) then*/
    if (_almost_nothing_23337 == 0) {
        goto L25; // [1096] 1125
    }
    Concat((object_ptr)&_13838, _mbits_23341, _ebits_23342);
    _13839 = find_from(1, _13838, 1);
    DeRefDS(_13838);
    _13838 = NOVALUE;
    _13840 = (_13839 == 0);
    _13839 = NOVALUE;
    if (_13840 == 0)
    {
        DeRef(_13840);
        _13840 = NOVALUE;
        goto L25; // [1115] 1125
    }
    else{
        DeRef(_13840);
        _13840 = NOVALUE;
    }

    /** 			fenv:raise(fenv:FE_UNDERFLOW)*/
    _13841 = _62raise(117);
L25: 

    /** 		return bits_to_bytes( mbits & ebits & sbits )*/
    {
        int concat_list[3];

        concat_list[0] = _sbits_23343;
        concat_list[1] = _ebits_23342;
        concat_list[2] = _mbits_23341;
        Concat_N((object_ptr)&_13842, concat_list, 3);
    }
    _13843 = _61bits_to_bytes(_13842);
    _13842 = NOVALUE;
    DeRefDS(_s_23333);
    DeRef(_int_bits_23339);
    DeRef(_frac_bits_23340);
    DeRefDS(_mbits_23341);
    DeRefDS(_ebits_23342);
    DeRefDSi(_sbits_23343);
    DeRef(_13813);
    _13813 = NOVALUE;
    DeRef(_13765);
    _13765 = NOVALUE;
    DeRef(_13819);
    _13819 = NOVALUE;
    DeRef(_13771);
    _13771 = NOVALUE;
    DeRef(_13748);
    _13748 = NOVALUE;
    DeRef(_13672);
    _13672 = NOVALUE;
    DeRef(_13699);
    _13699 = NOVALUE;
    DeRef(_13816);
    _13816 = NOVALUE;
    DeRef(_13680);
    _13680 = NOVALUE;
    DeRef(_13763);
    _13763 = NOVALUE;
    DeRef(_13792);
    _13792 = NOVALUE;
    DeRef(_13799);
    _13799 = NOVALUE;
    DeRef(_13669);
    _13669 = NOVALUE;
    DeRef(_13834);
    _13834 = NOVALUE;
    DeRef(_13713);
    _13713 = NOVALUE;
    DeRef(_13690);
    _13690 = NOVALUE;
    DeRef(_13692);
    _13692 = NOVALUE;
    DeRef(_13756);
    _13756 = NOVALUE;
    DeRef(_13802);
    _13802 = NOVALUE;
    DeRef(_13768);
    _13768 = NOVALUE;
    DeRef(_13841);
    _13841 = NOVALUE;
    DeRef(_13724);
    _13724 = NOVALUE;
    DeRef(_13825);
    _13825 = NOVALUE;
    DeRef(_13685);
    _13685 = NOVALUE;
    DeRef(_13811);
    _13811 = NOVALUE;
    DeRef(_13677);
    _13677 = NOVALUE;
    DeRef(_13828);
    _13828 = NOVALUE;
    DeRef(_13779);
    _13779 = NOVALUE;
    DeRef(_13808);
    _13808 = NOVALUE;
    return _13843;
    ;
}


int _61scientific_to_atom(int _s_23580)
{
    int _13845 = NOVALUE;
    int _13844 = NOVALUE;
    int _0, _1, _2;
    

    /** 		return float64_to_atom( scientific_to_float64( s ) )*/
    RefDS(_s_23580);
    _13844 = _61scientific_to_float64(_s_23580);
    _13845 = _6float64_to_atom(_13844);
    _13844 = NOVALUE;
    DeRefDS(_s_23580);
    return _13845;
    ;
}



// 0x89E8F08C
