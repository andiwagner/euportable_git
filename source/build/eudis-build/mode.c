// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _2set_mode(int _mode_164, int _extra_check_165)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_extra_check_165)) {
        _1 = (long)(DBL_PTR(_extra_check_165)->dbl);
        if (UNIQUE(DBL_PTR(_extra_check_165)) && (DBL_PTR(_extra_check_165)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_extra_check_165);
        _extra_check_165 = _1;
    }

    /** 	interpret = equal( mode, "interpret" )*/
    if (_mode_164 == _10)
    _2interpret_140 = 1;
    else if (IS_ATOM_INT(_mode_164) && IS_ATOM_INT(_10))
    _2interpret_140 = 0;
    else
    _2interpret_140 = (compare(_mode_164, _10) == 0);

    /** 	translate = equal( mode, "translate" )*/
    if (_mode_164 == _11)
    _2translate_141 = 1;
    else if (IS_ATOM_INT(_mode_164) && IS_ATOM_INT(_11))
    _2translate_141 = 0;
    else
    _2translate_141 = (compare(_mode_164, _11) == 0);

    /** 	bind      = equal( mode, "bind" )*/
    if (_mode_164 == _12)
    _2bind_142 = 1;
    else if (IS_ATOM_INT(_mode_164) && IS_ATOM_INT(_12))
    _2bind_142 = 0;
    else
    _2bind_142 = (compare(_mode_164, _12) == 0);

    /** 	backend   = equal( mode, "backend" )*/
    if (_mode_164 == _13)
    _2backend_149 = 1;
    else if (IS_ATOM_INT(_mode_164) && IS_ATOM_INT(_13))
    _2backend_149 = 0;
    else
    _2backend_149 = (compare(_mode_164, _13) == 0);

    /** 	do_extra_check = extra_check*/
    _2do_extra_check_143 = _extra_check_165;

    /** end procedure*/
    DeRefDS(_mode_164);
    return;
    ;
}


void _2set_backend(int _rid_172)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_rid_172)) {
        _1 = (long)(DBL_PTR(_rid_172)->dbl);
        if (UNIQUE(DBL_PTR(_rid_172)) && (DBL_PTR(_rid_172)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_rid_172);
        _rid_172 = _1;
    }

    /** 	backend_rid = rid*/
    _2backend_rid_146 = _rid_172;

    /** end procedure*/
    return;
    ;
}


void _2set_init_backend(int _rid_175)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_rid_175)) {
        _1 = (long)(DBL_PTR(_rid_175)->dbl);
        if (UNIQUE(DBL_PTR(_rid_175)) && (DBL_PTR(_rid_175)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_rid_175);
        _rid_175 = _1;
    }

    /** 	init_backend_rid = rid*/
    _2init_backend_rid_144 = _rid_175;

    /** end procedure*/
    return;
    ;
}


void _2InitBackEnd(int _x_178)
{
    int _20 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_178)) {
        _1 = (long)(DBL_PTR(_x_178)->dbl);
        if (UNIQUE(DBL_PTR(_x_178)) && (DBL_PTR(_x_178)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_178);
        _x_178 = _1;
    }

    /** 	call_proc( init_backend_rid, {x} )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _x_178;
    _20 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_20);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_2init_backend_rid_144].addr;
    Ref(*(int *)(_2+4));
    (*(int (*)())_0)(
                        *(int *)(_2+4)
                         );
    DeRefDS(_20);
    _20 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


void _2BackEnd(int _x_182)
{
    int _21 = NOVALUE;
    int _0, _1, _2;
    

    /** 	call_proc( backend_rid, {x} )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_x_182);
    *((int *)(_2+4)) = _x_182;
    _21 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_21);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_2backend_rid_146].addr;
    Ref(*(int *)(_2+4));
    (*(int (*)())_0)(
                        *(int *)(_2+4)
                         );
    DeRefDS(_21);
    _21 = NOVALUE;

    /** end procedure*/
    DeRef(_x_182);
    return;
    ;
}


void _2CheckPlatform()
{
    int _0, _1, _2;
    

    /** 	if check_platform_rid != -1 then*/
    if (_2check_platform_rid_150 == -1)
    goto L1; // [5] 17

    /** 		call_proc( check_platform_rid, {} )*/
    _0 = (int)_00[_2check_platform_rid_150].addr;
    (*(int (*)())_0)(
                         );
L1: 

    /** end procedure*/
    return;
    ;
}


void _2set_check_platform(int _rid_190)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_rid_190)) {
        _1 = (long)(DBL_PTR(_rid_190)->dbl);
        if (UNIQUE(DBL_PTR(_rid_190)) && (DBL_PTR(_rid_190)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_rid_190);
        _rid_190 = _1;
    }

    /** 	check_platform_rid = rid*/
    _2check_platform_rid_150 = _rid_190;

    /** end procedure*/
    return;
    ;
}


int _2get_interpret()
{
    int _0, _1, _2;
    

    /** 		return interpret*/
    return _2interpret_140;
    ;
}


int _2get_translate()
{
    int _0, _1, _2;
    

    /** 	return translate*/
    return _2translate_141;
    ;
}


int _2get_bind()
{
    int _0, _1, _2;
    

    /** 	return bind*/
    return _2bind_142;
    ;
}


int _2get_backend()
{
    int _0, _1, _2;
    

    /** 	return backend*/
    return _2backend_149;
    ;
}


int _2get_extra_check()
{
    int _0, _1, _2;
    

    /** 	return do_extra_check*/
    return _2do_extra_check_143;
    ;
}


void _2set_extract_options(int _rid_203)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_rid_203)) {
        _1 = (long)(DBL_PTR(_rid_203)->dbl);
        if (UNIQUE(DBL_PTR(_rid_203)) && (DBL_PTR(_rid_203)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_rid_203);
        _rid_203 = _1;
    }

    /** 	extract_options_rid = rid*/
    _2extract_options_rid_147 = _rid_203;

    /** end procedure*/
    return;
    ;
}


int _2extract_options(int _s_206)
{
    int _24 = NOVALUE;
    int _23 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return call_func( extract_options_rid, {s} )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_s_206);
    *((int *)(_2+4)) = _s_206;
    _23 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_23);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_2extract_options_rid_147].addr;
    Ref(*(int *)(_2+4));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4)
                         );
    _24 = _1;
    DeRefDS(_23);
    _23 = NOVALUE;
    DeRefDS(_s_206);
    return _24;
    ;
}


void _2set_output_il(int _rid_211)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_rid_211)) {
        _1 = (long)(DBL_PTR(_rid_211)->dbl);
        if (UNIQUE(DBL_PTR(_rid_211)) && (DBL_PTR(_rid_211)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_rid_211);
        _rid_211 = _1;
    }

    /** 	output_il_rid = rid*/
    _2output_il_rid_148 = _rid_211;

    /** end procedure*/
    return;
    ;
}


void _2OutputIL()
{
    int _0, _1, _2;
    

    /** 	call_proc( output_il_rid, {} )*/
    _0 = (int)_00[_2output_il_rid_148].addr;
    (*(int (*)())_0)(
                         );

    /** end procedure*/
    return;
    ;
}


void _2set_target_platform(int _target_216)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_target_216)) {
        _1 = (long)(DBL_PTR(_target_216)->dbl);
        if (UNIQUE(DBL_PTR(_target_216)) && (DBL_PTR(_target_216)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_target_216);
        _target_216 = _1;
    }

    /** 	target_plat = target*/
    _2target_plat_151 = _target_216;

    /** end procedure*/
    return;
    ;
}


int _2target_platform()
{
    int _0, _1, _2;
    

    /** 	return target_plat*/
    return _2target_plat_151;
    ;
}



// 0x3402E6FB
