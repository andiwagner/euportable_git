// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _24init_float80()
{
    int _code_8807 = NOVALUE;
    int _4778 = NOVALUE;
    int _4777 = NOVALUE;
    int _4775 = NOVALUE;
    int _0, _1, _2;
    

    /** 	f80 = allocate( 10 )*/
    _0 = _12allocate(10, 0);
    DeRef(_24f80_8796);
    _24f80_8796 = _0;

    /** 	if f64 = 0 then*/
    if (binary_op_a(NOTEQ, _24f64_8797, 0)){
        goto L1; // [12] 24
    }

    /** 		f64 = allocate( 8 )*/
    _0 = _12allocate(8, 0);
    DeRef(_24f64_8797);
    _24f64_8797 = _0;
L1: 

    /** 	atom code*/

    /** 	code = machine:allocate_code(*/
    _1 = NewS1(25);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 85;
    *((int *)(_2+8)) = 137;
    *((int *)(_2+12)) = 229;
    *((int *)(_2+16)) = 131;
    *((int *)(_2+20)) = 236;
    *((int *)(_2+24)) = 8;
    *((int *)(_2+28)) = 139;
    *((int *)(_2+32)) = 69;
    *((int *)(_2+36)) = 12;
    *((int *)(_2+40)) = 139;
    *((int *)(_2+44)) = 85;
    *((int *)(_2+48)) = 8;
    *((int *)(_2+52)) = 219;
    *((int *)(_2+56)) = 42;
    *((int *)(_2+60)) = 221;
    *((int *)(_2+64)) = 93;
    *((int *)(_2+68)) = 248;
    *((int *)(_2+72)) = 221;
    *((int *)(_2+76)) = 69;
    *((int *)(_2+80)) = 248;
    *((int *)(_2+84)) = 221;
    *((int *)(_2+88)) = 24;
    *((int *)(_2+92)) = 201;
    *((int *)(_2+96)) = 195;
    *((int *)(_2+100)) = 0;
    _4775 = MAKE_SEQ(_1);
    _0 = _code_8807;
    _code_8807 = _12allocate_code(_4775, 1);
    DeRef(_0);
    _4775 = NOVALUE;

    /** 	F80_TO_ATOM = dll:define_c_proc( "", {'+', code}, { dll:C_POINTER, dll:C_POINTER } )*/
    Ref(_code_8807);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 43;
    ((int *)_2)[2] = _code_8807;
    _4777 = MAKE_SEQ(_1);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 33554436;
    ((int *)_2)[2] = 33554436;
    _4778 = MAKE_SEQ(_1);
    RefDS(_5);
    _0 = _11define_c_proc(_5, _4777, _4778);
    _24F80_TO_ATOM_8798 = _0;
    _4777 = NOVALUE;
    _4778 = NOVALUE;
    if (!IS_ATOM_INT(_24F80_TO_ATOM_8798)) {
        _1 = (long)(DBL_PTR(_24F80_TO_ATOM_8798)->dbl);
        if (UNIQUE(DBL_PTR(_24F80_TO_ATOM_8798)) && (DBL_PTR(_24F80_TO_ATOM_8798)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_24F80_TO_ATOM_8798);
        _24F80_TO_ATOM_8798 = _1;
    }

    /** end procedure*/
    DeRef(_code_8807);
    return;
    ;
}


int _24float80_to_atom(int _f_8828)
{
    int _4784 = NOVALUE;
    int _4783 = NOVALUE;
    int _4782 = NOVALUE;
    int _4781 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not f80 then*/
    if (IS_ATOM_INT(_24f80_8796)) {
        if (_24f80_8796 != 0){
            goto L1; // [7] 15
        }
    }
    else {
        if (DBL_PTR(_24f80_8796)->dbl != 0.0){
            goto L1; // [7] 15
        }
    }

    /** 		init_float80()*/
    _24init_float80();
L1: 

    /** 	poke( f80, f )*/
    if (IS_ATOM_INT(_24f80_8796)){
        poke_addr = (unsigned char *)_24f80_8796;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_24f80_8796)->dbl);
    }
    _1 = (int)SEQ_PTR(_f_8828);
    _1 = (int)((s1_ptr)_1)->base;
    while (1) {
        _1 += 4;
        _2 = *((int *)_1);
        if (IS_ATOM_INT(_2))
        *poke_addr++ = (unsigned char)_2;
        else if (_2 == NOVALUE)
        break;
        else {
            _0 = (signed char)DBL_PTR(_2)->dbl;
            *poke_addr++ = (signed char)_0;
        }
    }

    /** 	c_proc( F80_TO_ATOM, { f80, f64 } )*/
    Ref(_24f64_8797);
    Ref(_24f80_8796);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24f80_8796;
    ((int *)_2)[2] = _24f64_8797;
    _4781 = MAKE_SEQ(_1);
    call_c(0, _24F80_TO_ATOM_8798, _4781);
    DeRefDS(_4781);
    _4781 = NOVALUE;

    /** 	return float64_to_atom( peek( { f64, 8 } ) )*/
    Ref(_24f64_8797);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24f64_8797;
    ((int *)_2)[2] = 8;
    _4782 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_4782);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _4783 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_4782);
    _4782 = NOVALUE;
    _4784 = _6float64_to_atom(_4783);
    _4783 = NOVALUE;
    DeRefDS(_f_8828);
    return _4784;
    ;
}


void _24init_peek8s()
{
    int _code_8846 = NOVALUE;
    int _4792 = NOVALUE;
    int _4791 = NOVALUE;
    int _4789 = NOVALUE;
    int _0, _1, _2;
    

    /** 	i64 = allocate( 8 )*/
    _0 = _12allocate(8, 0);
    DeRef(_24i64_8836);
    _24i64_8836 = _0;

    /** 	if f64 = 0 then*/
    if (binary_op_a(NOTEQ, _24f64_8797, 0)){
        goto L1; // [12] 24
    }

    /** 		f64 = allocate( 8 )*/
    _0 = _12allocate(8, 0);
    DeRef(_24f64_8797);
    _24f64_8797 = _0;
L1: 

    /** 	atom code = machine:allocate_code(	{*/
    _1 = NewS1(30);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 85;
    *((int *)(_2+8)) = 137;
    *((int *)(_2+12)) = 229;
    *((int *)(_2+16)) = 131;
    *((int *)(_2+20)) = 236;
    *((int *)(_2+24)) = 8;
    *((int *)(_2+28)) = 139;
    *((int *)(_2+32)) = 69;
    *((int *)(_2+36)) = 8;
    *((int *)(_2+40)) = 139;
    *((int *)(_2+44)) = 80;
    *((int *)(_2+48)) = 4;
    *((int *)(_2+52)) = 139;
    *((int *)(_2+56)) = 0;
    *((int *)(_2+60)) = 137;
    *((int *)(_2+64)) = 69;
    *((int *)(_2+68)) = 248;
    *((int *)(_2+72)) = 137;
    *((int *)(_2+76)) = 85;
    *((int *)(_2+80)) = 252;
    *((int *)(_2+84)) = 223;
    *((int *)(_2+88)) = 109;
    *((int *)(_2+92)) = 248;
    *((int *)(_2+96)) = 139;
    *((int *)(_2+100)) = 69;
    *((int *)(_2+104)) = 12;
    *((int *)(_2+108)) = 221;
    *((int *)(_2+112)) = 24;
    *((int *)(_2+116)) = 201;
    *((int *)(_2+120)) = 195;
    _4789 = MAKE_SEQ(_1);
    _0 = _code_8846;
    _code_8846 = _12allocate_code(_4789, 1);
    DeRef(_0);
    _4789 = NOVALUE;

    /** 	PEEK8S = define_c_proc( {}, {'+', code}, { C_POINTER, C_POINTER } )*/
    Ref(_code_8846);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 43;
    ((int *)_2)[2] = _code_8846;
    _4791 = MAKE_SEQ(_1);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 33554436;
    ((int *)_2)[2] = 33554436;
    _4792 = MAKE_SEQ(_1);
    RefDS(_5);
    _0 = _11define_c_proc(_5, _4791, _4792);
    _24PEEK8S_8837 = _0;
    _4791 = NOVALUE;
    _4792 = NOVALUE;
    if (!IS_ATOM_INT(_24PEEK8S_8837)) {
        _1 = (long)(DBL_PTR(_24PEEK8S_8837)->dbl);
        if (UNIQUE(DBL_PTR(_24PEEK8S_8837)) && (DBL_PTR(_24PEEK8S_8837)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_24PEEK8S_8837);
        _24PEEK8S_8837 = _1;
    }

    /** end procedure*/
    DeRef(_code_8846);
    return;
    ;
}


int _24int64_to_atom(int _s_8858)
{
    int _4798 = NOVALUE;
    int _4797 = NOVALUE;
    int _4796 = NOVALUE;
    int _4795 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if i64 = 0 then*/
    if (binary_op_a(NOTEQ, _24i64_8836, 0)){
        goto L1; // [7] 16
    }

    /** 		init_peek8s()*/
    _24init_peek8s();
L1: 

    /** 	poke( i64, s )*/
    if (IS_ATOM_INT(_24i64_8836)){
        poke_addr = (unsigned char *)_24i64_8836;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_24i64_8836)->dbl);
    }
    _1 = (int)SEQ_PTR(_s_8858);
    _1 = (int)((s1_ptr)_1)->base;
    while (1) {
        _1 += 4;
        _2 = *((int *)_1);
        if (IS_ATOM_INT(_2))
        *poke_addr++ = (unsigned char)_2;
        else if (_2 == NOVALUE)
        break;
        else {
            _0 = (signed char)DBL_PTR(_2)->dbl;
            *poke_addr++ = (signed char)_0;
        }
    }

    /** 	c_proc( PEEK8S, { i64, f64 } )*/
    Ref(_24f64_8797);
    Ref(_24i64_8836);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24i64_8836;
    ((int *)_2)[2] = _24f64_8797;
    _4795 = MAKE_SEQ(_1);
    call_c(0, _24PEEK8S_8837, _4795);
    DeRefDS(_4795);
    _4795 = NOVALUE;

    /** 	return float64_to_atom( peek( f64 & 8 ) )*/
    Concat((object_ptr)&_4796, _24f64_8797, 8);
    _1 = (int)SEQ_PTR(_4796);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _4797 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_4796);
    _4796 = NOVALUE;
    _4798 = _6float64_to_atom(_4797);
    _4797 = NOVALUE;
    DeRefDS(_s_8858);
    return _4798;
    ;
}


int _24get4(int _fh_8868)
{
    int _4803 = NOVALUE;
    int _4802 = NOVALUE;
    int _4801 = NOVALUE;
    int _4800 = NOVALUE;
    int _4799 = NOVALUE;
    int _0, _1, _2;
    

    /** 	poke(mem0, getc(fh))*/
    if (_fh_8868 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_8868, EF_READ);
        last_r_file_no = _fh_8868;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _4799 = getKBchar();
        }
        else
        _4799 = getc(last_r_file_ptr);
    }
    else
    _4799 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_24mem0_8788)){
        poke_addr = (unsigned char *)_24mem0_8788;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_24mem0_8788)->dbl);
    }
    *poke_addr = (unsigned char)_4799;
    _4799 = NOVALUE;

    /** 	poke(mem1, getc(fh))*/
    if (_fh_8868 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_8868, EF_READ);
        last_r_file_no = _fh_8868;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _4800 = getKBchar();
        }
        else
        _4800 = getc(last_r_file_ptr);
    }
    else
    _4800 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_24mem1_8789)){
        poke_addr = (unsigned char *)_24mem1_8789;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_24mem1_8789)->dbl);
    }
    *poke_addr = (unsigned char)_4800;
    _4800 = NOVALUE;

    /** 	poke(mem2, getc(fh))*/
    if (_fh_8868 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_8868, EF_READ);
        last_r_file_no = _fh_8868;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _4801 = getKBchar();
        }
        else
        _4801 = getc(last_r_file_ptr);
    }
    else
    _4801 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_24mem2_8790)){
        poke_addr = (unsigned char *)_24mem2_8790;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_24mem2_8790)->dbl);
    }
    *poke_addr = (unsigned char)_4801;
    _4801 = NOVALUE;

    /** 	poke(mem3, getc(fh))*/
    if (_fh_8868 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_8868, EF_READ);
        last_r_file_no = _fh_8868;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _4802 = getKBchar();
        }
        else
        _4802 = getc(last_r_file_ptr);
    }
    else
    _4802 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_24mem3_8791)){
        poke_addr = (unsigned char *)_24mem3_8791;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_24mem3_8791)->dbl);
    }
    *poke_addr = (unsigned char)_4802;
    _4802 = NOVALUE;

    /** 	return peek4u(mem0)*/
    if (IS_ATOM_INT(_24mem0_8788)) {
        _4803 = *(unsigned long *)_24mem0_8788;
        if ((unsigned)_4803 > (unsigned)MAXINT)
        _4803 = NewDouble((double)(unsigned long)_4803);
    }
    else {
        _4803 = *(unsigned long *)(unsigned long)(DBL_PTR(_24mem0_8788)->dbl);
        if ((unsigned)_4803 > (unsigned)MAXINT)
        _4803 = NewDouble((double)(unsigned long)_4803);
    }
    return _4803;
    ;
}


int _24deserialize_file(int _fh_8876, int _c_8877)
{
    int _s_8878 = NOVALUE;
    int _len_8879 = NOVALUE;
    int _float32_to_atom_inlined_float32_to_atom_at_159_8915 = NOVALUE;
    int _ieee32_inlined_float32_to_atom_at_156_8914 = NOVALUE;
    int _float64_to_atom_inlined_float64_to_atom_at_218_8928 = NOVALUE;
    int _ieee64_inlined_float64_to_atom_at_215_8927 = NOVALUE;
    int _msg_inlined_crash_at_424_8973 = NOVALUE;
    int _4882 = NOVALUE;
    int _4881 = NOVALUE;
    int _4880 = NOVALUE;
    int _4879 = NOVALUE;
    int _4876 = NOVALUE;
    int _4873 = NOVALUE;
    int _4872 = NOVALUE;
    int _4871 = NOVALUE;
    int _4870 = NOVALUE;
    int _4869 = NOVALUE;
    int _4868 = NOVALUE;
    int _4867 = NOVALUE;
    int _4866 = NOVALUE;
    int _4865 = NOVALUE;
    int _4864 = NOVALUE;
    int _4863 = NOVALUE;
    int _4862 = NOVALUE;
    int _4860 = NOVALUE;
    int _4859 = NOVALUE;
    int _4858 = NOVALUE;
    int _4857 = NOVALUE;
    int _4856 = NOVALUE;
    int _4855 = NOVALUE;
    int _4854 = NOVALUE;
    int _4853 = NOVALUE;
    int _4852 = NOVALUE;
    int _4851 = NOVALUE;
    int _4849 = NOVALUE;
    int _4848 = NOVALUE;
    int _4847 = NOVALUE;
    int _4846 = NOVALUE;
    int _4845 = NOVALUE;
    int _4843 = NOVALUE;
    int _4839 = NOVALUE;
    int _4838 = NOVALUE;
    int _4837 = NOVALUE;
    int _4836 = NOVALUE;
    int _4835 = NOVALUE;
    int _4834 = NOVALUE;
    int _4833 = NOVALUE;
    int _4832 = NOVALUE;
    int _4831 = NOVALUE;
    int _4830 = NOVALUE;
    int _4829 = NOVALUE;
    int _4828 = NOVALUE;
    int _4827 = NOVALUE;
    int _4826 = NOVALUE;
    int _4825 = NOVALUE;
    int _4824 = NOVALUE;
    int _4823 = NOVALUE;
    int _4822 = NOVALUE;
    int _4821 = NOVALUE;
    int _4820 = NOVALUE;
    int _4818 = NOVALUE;
    int _4817 = NOVALUE;
    int _4816 = NOVALUE;
    int _4815 = NOVALUE;
    int _4814 = NOVALUE;
    int _4813 = NOVALUE;
    int _4812 = NOVALUE;
    int _4811 = NOVALUE;
    int _4810 = NOVALUE;
    int _4807 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if c = 0 then*/
    if (_c_8877 != 0)
    goto L1; // [7] 34

    /** 		c = getc(fh)*/
    if (_fh_8876 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_8876, EF_READ);
        last_r_file_no = _fh_8876;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_8877 = getKBchar();
        }
        else
        _c_8877 = getc(last_r_file_ptr);
    }
    else
    _c_8877 = getc(last_r_file_ptr);

    /** 		if c < I2B then*/
    if (_c_8877 >= 249)
    goto L2; // [18] 33

    /** 			return c + MIN1B*/
    _4807 = _c_8877 + -9;
    DeRef(_s_8878);
    DeRef(_len_8879);
    return _4807;
L2: 
L1: 

    /** 	switch c with fallthru do*/
    _0 = _c_8877;
    switch ( _0 ){ 

        /** 		case I2B then*/
        case 249:

        /** 			return getc(fh) +*/
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4810 = getKBchar();
            }
            else
            _4810 = getc(last_r_file_ptr);
        }
        else
        _4810 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4811 = getKBchar();
            }
            else
            _4811 = getc(last_r_file_ptr);
        }
        else
        _4811 = getc(last_r_file_ptr);
        _4812 = 256 * _4811;
        _4811 = NOVALUE;
        _4813 = _4810 + _4812;
        _4810 = NOVALUE;
        _4812 = NOVALUE;
        _4814 = _4813 + _24MIN2B_8773;
        if ((long)((unsigned long)_4814 + (unsigned long)HIGH_BITS) >= 0) 
        _4814 = NewDouble((double)_4814);
        _4813 = NOVALUE;
        DeRef(_s_8878);
        DeRef(_len_8879);
        DeRef(_4807);
        _4807 = NOVALUE;
        return _4814;

        /** 		case I3B then*/
        case 250:

        /** 			return getc(fh) +*/
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4815 = getKBchar();
            }
            else
            _4815 = getc(last_r_file_ptr);
        }
        else
        _4815 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4816 = getKBchar();
            }
            else
            _4816 = getc(last_r_file_ptr);
        }
        else
        _4816 = getc(last_r_file_ptr);
        _4817 = 256 * _4816;
        _4816 = NOVALUE;
        _4818 = _4815 + _4817;
        _4815 = NOVALUE;
        _4817 = NOVALUE;
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4820 = getKBchar();
            }
            else
            _4820 = getc(last_r_file_ptr);
        }
        else
        _4820 = getc(last_r_file_ptr);
        _4821 = 65536 * _4820;
        _4820 = NOVALUE;
        _4822 = _4818 + _4821;
        _4818 = NOVALUE;
        _4821 = NOVALUE;
        _4823 = _4822 + _24MIN3B_8779;
        if ((long)((unsigned long)_4823 + (unsigned long)HIGH_BITS) >= 0) 
        _4823 = NewDouble((double)_4823);
        _4822 = NOVALUE;
        DeRef(_s_8878);
        DeRef(_len_8879);
        DeRef(_4807);
        _4807 = NOVALUE;
        DeRef(_4814);
        _4814 = NOVALUE;
        return _4823;

        /** 		case I4B then*/
        case 251:

        /** 			return get4(fh) + MIN4B*/
        _4824 = _24get4(_fh_8876);
        if (IS_ATOM_INT(_4824) && IS_ATOM_INT(_24MIN4B_8785)) {
            _4825 = _4824 + _24MIN4B_8785;
            if ((long)((unsigned long)_4825 + (unsigned long)HIGH_BITS) >= 0) 
            _4825 = NewDouble((double)_4825);
        }
        else {
            _4825 = binary_op(PLUS, _4824, _24MIN4B_8785);
        }
        DeRef(_4824);
        _4824 = NOVALUE;
        DeRef(_s_8878);
        DeRef(_len_8879);
        DeRef(_4807);
        _4807 = NOVALUE;
        DeRef(_4814);
        _4814 = NOVALUE;
        DeRef(_4823);
        _4823 = NOVALUE;
        return _4825;

        /** 		case F4B then*/
        case 252:

        /** 			return convert:float32_to_atom({getc(fh), getc(fh),*/
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4826 = getKBchar();
            }
            else
            _4826 = getc(last_r_file_ptr);
        }
        else
        _4826 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4827 = getKBchar();
            }
            else
            _4827 = getc(last_r_file_ptr);
        }
        else
        _4827 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4828 = getKBchar();
            }
            else
            _4828 = getc(last_r_file_ptr);
        }
        else
        _4828 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4829 = getKBchar();
            }
            else
            _4829 = getc(last_r_file_ptr);
        }
        else
        _4829 = getc(last_r_file_ptr);
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _4826;
        *((int *)(_2+8)) = _4827;
        *((int *)(_2+12)) = _4828;
        *((int *)(_2+16)) = _4829;
        _4830 = MAKE_SEQ(_1);
        _4829 = NOVALUE;
        _4828 = NOVALUE;
        _4827 = NOVALUE;
        _4826 = NOVALUE;
        DeRefi(_ieee32_inlined_float32_to_atom_at_156_8914);
        _ieee32_inlined_float32_to_atom_at_156_8914 = _4830;
        _4830 = NOVALUE;

        /** 	return machine_func(M_F32_TO_A, ieee32)*/
        DeRef(_float32_to_atom_inlined_float32_to_atom_at_159_8915);
        _float32_to_atom_inlined_float32_to_atom_at_159_8915 = machine(49, _ieee32_inlined_float32_to_atom_at_156_8914);
        DeRefi(_ieee32_inlined_float32_to_atom_at_156_8914);
        _ieee32_inlined_float32_to_atom_at_156_8914 = NOVALUE;
        DeRef(_s_8878);
        DeRef(_len_8879);
        DeRef(_4807);
        _4807 = NOVALUE;
        DeRef(_4814);
        _4814 = NOVALUE;
        DeRef(_4823);
        _4823 = NOVALUE;
        DeRef(_4825);
        _4825 = NOVALUE;
        return _float32_to_atom_inlined_float32_to_atom_at_159_8915;

        /** 		case F8B then*/
        case 253:

        /** 			return convert:float64_to_atom({getc(fh), getc(fh),*/
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4831 = getKBchar();
            }
            else
            _4831 = getc(last_r_file_ptr);
        }
        else
        _4831 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4832 = getKBchar();
            }
            else
            _4832 = getc(last_r_file_ptr);
        }
        else
        _4832 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4833 = getKBchar();
            }
            else
            _4833 = getc(last_r_file_ptr);
        }
        else
        _4833 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4834 = getKBchar();
            }
            else
            _4834 = getc(last_r_file_ptr);
        }
        else
        _4834 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4835 = getKBchar();
            }
            else
            _4835 = getc(last_r_file_ptr);
        }
        else
        _4835 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4836 = getKBchar();
            }
            else
            _4836 = getc(last_r_file_ptr);
        }
        else
        _4836 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4837 = getKBchar();
            }
            else
            _4837 = getc(last_r_file_ptr);
        }
        else
        _4837 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4838 = getKBchar();
            }
            else
            _4838 = getc(last_r_file_ptr);
        }
        else
        _4838 = getc(last_r_file_ptr);
        _1 = NewS1(8);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _4831;
        *((int *)(_2+8)) = _4832;
        *((int *)(_2+12)) = _4833;
        *((int *)(_2+16)) = _4834;
        *((int *)(_2+20)) = _4835;
        *((int *)(_2+24)) = _4836;
        *((int *)(_2+28)) = _4837;
        *((int *)(_2+32)) = _4838;
        _4839 = MAKE_SEQ(_1);
        _4838 = NOVALUE;
        _4837 = NOVALUE;
        _4836 = NOVALUE;
        _4835 = NOVALUE;
        _4834 = NOVALUE;
        _4833 = NOVALUE;
        _4832 = NOVALUE;
        _4831 = NOVALUE;
        DeRefi(_ieee64_inlined_float64_to_atom_at_215_8927);
        _ieee64_inlined_float64_to_atom_at_215_8927 = _4839;
        _4839 = NOVALUE;

        /** 	return machine_func(M_F64_TO_A, ieee64)*/
        DeRef(_float64_to_atom_inlined_float64_to_atom_at_218_8928);
        _float64_to_atom_inlined_float64_to_atom_at_218_8928 = machine(47, _ieee64_inlined_float64_to_atom_at_215_8927);
        DeRefi(_ieee64_inlined_float64_to_atom_at_215_8927);
        _ieee64_inlined_float64_to_atom_at_215_8927 = NOVALUE;
        DeRef(_s_8878);
        DeRef(_len_8879);
        DeRef(_4807);
        _4807 = NOVALUE;
        DeRef(_4814);
        _4814 = NOVALUE;
        DeRef(_4823);
        _4823 = NOVALUE;
        DeRef(_4825);
        _4825 = NOVALUE;
        return _float64_to_atom_inlined_float64_to_atom_at_218_8928;

        /** 		case else*/
        default:

        /** 			if c = S1B then*/
        if (_c_8877 != 254)
        goto L3; // [240] 252

        /** 				len = getc(fh)*/
        DeRef(_len_8879);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _len_8879 = getKBchar();
            }
            else
            _len_8879 = getc(last_r_file_ptr);
        }
        else
        _len_8879 = getc(last_r_file_ptr);
        goto L4; // [249] 259
L3: 

        /** 				len = get4(fh)*/
        _0 = _len_8879;
        _len_8879 = _24get4(_fh_8876);
        DeRef(_0);
L4: 

        /** 			if len < 0  or not integer(len) then*/
        if (IS_ATOM_INT(_len_8879)) {
            _4843 = (_len_8879 < 0);
        }
        else {
            _4843 = (DBL_PTR(_len_8879)->dbl < (double)0);
        }
        if (_4843 != 0) {
            goto L5; // [267] 282
        }
        if (IS_ATOM_INT(_len_8879))
        _4845 = 1;
        else if (IS_ATOM_DBL(_len_8879))
        _4845 = IS_ATOM_INT(DoubleToInt(_len_8879));
        else
        _4845 = 0;
        _4846 = (_4845 == 0);
        _4845 = NOVALUE;
        if (_4846 == 0)
        {
            DeRef(_4846);
            _4846 = NOVALUE;
            goto L6; // [278] 289
        }
        else{
            DeRef(_4846);
            _4846 = NOVALUE;
        }
L5: 

        /** 				return 0*/
        DeRef(_s_8878);
        DeRef(_len_8879);
        DeRef(_4807);
        _4807 = NOVALUE;
        DeRef(_4814);
        _4814 = NOVALUE;
        DeRef(_4823);
        _4823 = NOVALUE;
        DeRef(_4843);
        _4843 = NOVALUE;
        DeRef(_4825);
        _4825 = NOVALUE;
        return 0;
L6: 

        /** 			if c = S4B and len < 256 then*/
        _4847 = (_c_8877 == 255);
        if (_4847 == 0) {
            goto L7; // [295] 447
        }
        if (IS_ATOM_INT(_len_8879)) {
            _4849 = (_len_8879 < 256);
        }
        else {
            _4849 = (DBL_PTR(_len_8879)->dbl < (double)256);
        }
        if (_4849 == 0)
        {
            DeRef(_4849);
            _4849 = NOVALUE;
            goto L7; // [304] 447
        }
        else{
            DeRef(_4849);
            _4849 = NOVALUE;
        }

        /** 				if len = I8B then*/
        if (binary_op_a(NOTEQ, _len_8879, 0)){
            goto L8; // [309] 361
        }

        /** 					return int64_to_atom( {*/
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4851 = getKBchar();
            }
            else
            _4851 = getc(last_r_file_ptr);
        }
        else
        _4851 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4852 = getKBchar();
            }
            else
            _4852 = getc(last_r_file_ptr);
        }
        else
        _4852 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4853 = getKBchar();
            }
            else
            _4853 = getc(last_r_file_ptr);
        }
        else
        _4853 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4854 = getKBchar();
            }
            else
            _4854 = getc(last_r_file_ptr);
        }
        else
        _4854 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4855 = getKBchar();
            }
            else
            _4855 = getc(last_r_file_ptr);
        }
        else
        _4855 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4856 = getKBchar();
            }
            else
            _4856 = getc(last_r_file_ptr);
        }
        else
        _4856 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4857 = getKBchar();
            }
            else
            _4857 = getc(last_r_file_ptr);
        }
        else
        _4857 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4858 = getKBchar();
            }
            else
            _4858 = getc(last_r_file_ptr);
        }
        else
        _4858 = getc(last_r_file_ptr);
        _1 = NewS1(8);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _4851;
        *((int *)(_2+8)) = _4852;
        *((int *)(_2+12)) = _4853;
        *((int *)(_2+16)) = _4854;
        *((int *)(_2+20)) = _4855;
        *((int *)(_2+24)) = _4856;
        *((int *)(_2+28)) = _4857;
        *((int *)(_2+32)) = _4858;
        _4859 = MAKE_SEQ(_1);
        _4858 = NOVALUE;
        _4857 = NOVALUE;
        _4856 = NOVALUE;
        _4855 = NOVALUE;
        _4854 = NOVALUE;
        _4853 = NOVALUE;
        _4852 = NOVALUE;
        _4851 = NOVALUE;
        _4860 = _24int64_to_atom(_4859);
        _4859 = NOVALUE;
        DeRef(_s_8878);
        DeRef(_len_8879);
        DeRef(_4807);
        _4807 = NOVALUE;
        DeRef(_4814);
        _4814 = NOVALUE;
        DeRef(_4823);
        _4823 = NOVALUE;
        DeRef(_4843);
        _4843 = NOVALUE;
        DeRef(_4825);
        _4825 = NOVALUE;
        DeRef(_4847);
        _4847 = NOVALUE;
        return _4860;
        goto L9; // [358] 521
L8: 

        /** 				elsif len = F10B then*/
        if (binary_op_a(NOTEQ, _len_8879, 1)){
            goto LA; // [363] 423
        }

        /** 					return float80_to_atom(*/
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4862 = getKBchar();
            }
            else
            _4862 = getc(last_r_file_ptr);
        }
        else
        _4862 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4863 = getKBchar();
            }
            else
            _4863 = getc(last_r_file_ptr);
        }
        else
        _4863 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4864 = getKBchar();
            }
            else
            _4864 = getc(last_r_file_ptr);
        }
        else
        _4864 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4865 = getKBchar();
            }
            else
            _4865 = getc(last_r_file_ptr);
        }
        else
        _4865 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4866 = getKBchar();
            }
            else
            _4866 = getc(last_r_file_ptr);
        }
        else
        _4866 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4867 = getKBchar();
            }
            else
            _4867 = getc(last_r_file_ptr);
        }
        else
        _4867 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4868 = getKBchar();
            }
            else
            _4868 = getc(last_r_file_ptr);
        }
        else
        _4868 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4869 = getKBchar();
            }
            else
            _4869 = getc(last_r_file_ptr);
        }
        else
        _4869 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4870 = getKBchar();
            }
            else
            _4870 = getc(last_r_file_ptr);
        }
        else
        _4870 = getc(last_r_file_ptr);
        if (_fh_8876 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8876, EF_READ);
            last_r_file_no = _fh_8876;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4871 = getKBchar();
            }
            else
            _4871 = getc(last_r_file_ptr);
        }
        else
        _4871 = getc(last_r_file_ptr);
        _1 = NewS1(10);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _4862;
        *((int *)(_2+8)) = _4863;
        *((int *)(_2+12)) = _4864;
        *((int *)(_2+16)) = _4865;
        *((int *)(_2+20)) = _4866;
        *((int *)(_2+24)) = _4867;
        *((int *)(_2+28)) = _4868;
        *((int *)(_2+32)) = _4869;
        *((int *)(_2+36)) = _4870;
        *((int *)(_2+40)) = _4871;
        _4872 = MAKE_SEQ(_1);
        _4871 = NOVALUE;
        _4870 = NOVALUE;
        _4869 = NOVALUE;
        _4868 = NOVALUE;
        _4867 = NOVALUE;
        _4866 = NOVALUE;
        _4865 = NOVALUE;
        _4864 = NOVALUE;
        _4863 = NOVALUE;
        _4862 = NOVALUE;
        _4873 = _24float80_to_atom(_4872);
        _4872 = NOVALUE;
        DeRef(_s_8878);
        DeRef(_len_8879);
        DeRef(_4807);
        _4807 = NOVALUE;
        DeRef(_4814);
        _4814 = NOVALUE;
        DeRef(_4823);
        _4823 = NOVALUE;
        DeRef(_4843);
        _4843 = NOVALUE;
        DeRef(_4825);
        _4825 = NOVALUE;
        DeRef(_4847);
        _4847 = NOVALUE;
        DeRef(_4860);
        _4860 = NOVALUE;
        return _4873;
        goto L9; // [420] 521
LA: 

        /** 					error:crash( "Invalid sequence serialization" )*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_424_8973);
        _msg_inlined_crash_at_424_8973 = EPrintf(-9999999, _4874, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_424_8973);

        /** end procedure*/
        goto LB; // [438] 441
LB: 
        DeRefi(_msg_inlined_crash_at_424_8973);
        _msg_inlined_crash_at_424_8973 = NOVALUE;
        goto L9; // [444] 521
L7: 

        /** 				s = repeat(0, len)*/
        DeRef(_s_8878);
        _s_8878 = Repeat(0, _len_8879);

        /** 				for i = 1 to len do*/
        Ref(_len_8879);
        DeRef(_4876);
        _4876 = _len_8879;
        {
            int _i_8977;
            _i_8977 = 1;
LC: 
            if (binary_op_a(GREATER, _i_8977, _4876)){
                goto LD; // [458] 514
            }

            /** 					c = getc(fh)*/
            if (_fh_8876 != last_r_file_no) {
                last_r_file_ptr = which_file(_fh_8876, EF_READ);
                last_r_file_no = _fh_8876;
            }
            if (last_r_file_ptr == xstdin) {
                show_console();
                if (in_from_keyb) {
                    _c_8877 = getKBchar();
                }
                else
                _c_8877 = getc(last_r_file_ptr);
            }
            else
            _c_8877 = getc(last_r_file_ptr);

            /** 					if c < I2B then*/
            if (_c_8877 >= 249)
            goto LE; // [472] 489

            /** 						s[i] = c + MIN1B*/
            _4879 = _c_8877 + -9;
            _2 = (int)SEQ_PTR(_s_8878);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_8878 = MAKE_SEQ(_2);
            }
            if (!IS_ATOM_INT(_i_8977))
            _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_8977)->dbl));
            else
            _2 = (int)(((s1_ptr)_2)->base + _i_8977);
            _1 = *(int *)_2;
            *(int *)_2 = _4879;
            if( _1 != _4879 ){
                DeRef(_1);
            }
            _4879 = NOVALUE;
            goto LF; // [486] 507
LE: 

            /** 						s[i] = deserialize_file(fh, c)*/
            DeRef(_4880);
            _4880 = _fh_8876;
            DeRef(_4881);
            _4881 = _c_8877;
            _4882 = _24deserialize_file(_4880, _4881);
            _4880 = NOVALUE;
            _4881 = NOVALUE;
            _2 = (int)SEQ_PTR(_s_8878);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_8878 = MAKE_SEQ(_2);
            }
            if (!IS_ATOM_INT(_i_8977))
            _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_8977)->dbl));
            else
            _2 = (int)(((s1_ptr)_2)->base + _i_8977);
            _1 = *(int *)_2;
            *(int *)_2 = _4882;
            if( _1 != _4882 ){
                DeRef(_1);
            }
            _4882 = NOVALUE;
LF: 

            /** 				end for*/
            _0 = _i_8977;
            if (IS_ATOM_INT(_i_8977)) {
                _i_8977 = _i_8977 + 1;
                if ((long)((unsigned long)_i_8977 +(unsigned long) HIGH_BITS) >= 0){
                    _i_8977 = NewDouble((double)_i_8977);
                }
            }
            else {
                _i_8977 = binary_op_a(PLUS, _i_8977, 1);
            }
            DeRef(_0);
            goto LC; // [509] 465
LD: 
            ;
            DeRef(_i_8977);
        }

        /** 				return s*/
        DeRef(_len_8879);
        DeRef(_4807);
        _4807 = NOVALUE;
        DeRef(_4814);
        _4814 = NOVALUE;
        DeRef(_4823);
        _4823 = NOVALUE;
        DeRef(_4843);
        _4843 = NOVALUE;
        DeRef(_4825);
        _4825 = NOVALUE;
        DeRef(_4847);
        _4847 = NOVALUE;
        DeRef(_4860);
        _4860 = NOVALUE;
        DeRef(_4873);
        _4873 = NOVALUE;
        return _s_8878;
L9: 
    ;}    ;
}


int _24getp4(int _sdata_8989, int _pos_8990)
{
    int _4891 = NOVALUE;
    int _4890 = NOVALUE;
    int _4889 = NOVALUE;
    int _4888 = NOVALUE;
    int _4887 = NOVALUE;
    int _4886 = NOVALUE;
    int _4885 = NOVALUE;
    int _4884 = NOVALUE;
    int _4883 = NOVALUE;
    int _0, _1, _2;
    

    /** 	poke(mem0, sdata[pos+0])*/
    _4883 = _pos_8990 + 0;
    _2 = (int)SEQ_PTR(_sdata_8989);
    _4884 = (int)*(((s1_ptr)_2)->base + _4883);
    if (IS_ATOM_INT(_24mem0_8788)){
        poke_addr = (unsigned char *)_24mem0_8788;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_24mem0_8788)->dbl);
    }
    if (IS_ATOM_INT(_4884)) {
        *poke_addr = (unsigned char)_4884;
    }
    else if (IS_ATOM(_4884)) {
        _1 = (signed char)DBL_PTR(_4884)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_4884);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }
    _4884 = NOVALUE;

    /** 	poke(mem1, sdata[pos+1])*/
    _4885 = _pos_8990 + 1;
    _2 = (int)SEQ_PTR(_sdata_8989);
    _4886 = (int)*(((s1_ptr)_2)->base + _4885);
    if (IS_ATOM_INT(_24mem1_8789)){
        poke_addr = (unsigned char *)_24mem1_8789;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_24mem1_8789)->dbl);
    }
    if (IS_ATOM_INT(_4886)) {
        *poke_addr = (unsigned char)_4886;
    }
    else if (IS_ATOM(_4886)) {
        _1 = (signed char)DBL_PTR(_4886)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_4886);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }
    _4886 = NOVALUE;

    /** 	poke(mem2, sdata[pos+2])*/
    _4887 = _pos_8990 + 2;
    _2 = (int)SEQ_PTR(_sdata_8989);
    _4888 = (int)*(((s1_ptr)_2)->base + _4887);
    if (IS_ATOM_INT(_24mem2_8790)){
        poke_addr = (unsigned char *)_24mem2_8790;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_24mem2_8790)->dbl);
    }
    if (IS_ATOM_INT(_4888)) {
        *poke_addr = (unsigned char)_4888;
    }
    else if (IS_ATOM(_4888)) {
        _1 = (signed char)DBL_PTR(_4888)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_4888);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }
    _4888 = NOVALUE;

    /** 	poke(mem3, sdata[pos+3])*/
    _4889 = _pos_8990 + 3;
    _2 = (int)SEQ_PTR(_sdata_8989);
    _4890 = (int)*(((s1_ptr)_2)->base + _4889);
    if (IS_ATOM_INT(_24mem3_8791)){
        poke_addr = (unsigned char *)_24mem3_8791;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_24mem3_8791)->dbl);
    }
    if (IS_ATOM_INT(_4890)) {
        *poke_addr = (unsigned char)_4890;
    }
    else if (IS_ATOM(_4890)) {
        _1 = (signed char)DBL_PTR(_4890)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_4890);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }
    _4890 = NOVALUE;

    /** 	return peek4u(mem0)*/
    if (IS_ATOM_INT(_24mem0_8788)) {
        _4891 = *(unsigned long *)_24mem0_8788;
        if ((unsigned)_4891 > (unsigned)MAXINT)
        _4891 = NewDouble((double)(unsigned long)_4891);
    }
    else {
        _4891 = *(unsigned long *)(unsigned long)(DBL_PTR(_24mem0_8788)->dbl);
        if ((unsigned)_4891 > (unsigned)MAXINT)
        _4891 = NewDouble((double)(unsigned long)_4891);
    }
    DeRefDS(_sdata_8989);
    _4883 = NOVALUE;
    _4885 = NOVALUE;
    _4887 = NOVALUE;
    _4889 = NOVALUE;
    return _4891;
    ;
}


int _24deserialize_object(int _sdata_9002, int _pos_9003, int _c_9004)
{
    int _s_9005 = NOVALUE;
    int _len_9006 = NOVALUE;
    int _float32_to_atom_inlined_float32_to_atom_at_230_9055 = NOVALUE;
    int _ieee32_inlined_float32_to_atom_at_227_9054 = NOVALUE;
    int _float64_to_atom_inlined_float64_to_atom_at_333_9077 = NOVALUE;
    int _ieee64_inlined_float64_to_atom_at_330_9076 = NOVALUE;
    int _msg_inlined_crash_at_493_9108 = NOVALUE;
    int _temp_9120 = NOVALUE;
    int _4984 = NOVALUE;
    int _4982 = NOVALUE;
    int _4980 = NOVALUE;
    int _4979 = NOVALUE;
    int _4978 = NOVALUE;
    int _4977 = NOVALUE;
    int _4973 = NOVALUE;
    int _4971 = NOVALUE;
    int _4970 = NOVALUE;
    int _4969 = NOVALUE;
    int _4968 = NOVALUE;
    int _4967 = NOVALUE;
    int _4965 = NOVALUE;
    int _4964 = NOVALUE;
    int _4963 = NOVALUE;
    int _4962 = NOVALUE;
    int _4961 = NOVALUE;
    int _4959 = NOVALUE;
    int _4958 = NOVALUE;
    int _4957 = NOVALUE;
    int _4951 = NOVALUE;
    int _4950 = NOVALUE;
    int _4949 = NOVALUE;
    int _4948 = NOVALUE;
    int _4947 = NOVALUE;
    int _4946 = NOVALUE;
    int _4945 = NOVALUE;
    int _4944 = NOVALUE;
    int _4943 = NOVALUE;
    int _4942 = NOVALUE;
    int _4941 = NOVALUE;
    int _4940 = NOVALUE;
    int _4939 = NOVALUE;
    int _4938 = NOVALUE;
    int _4937 = NOVALUE;
    int _4936 = NOVALUE;
    int _4935 = NOVALUE;
    int _4934 = NOVALUE;
    int _4933 = NOVALUE;
    int _4932 = NOVALUE;
    int _4931 = NOVALUE;
    int _4930 = NOVALUE;
    int _4929 = NOVALUE;
    int _4928 = NOVALUE;
    int _4927 = NOVALUE;
    int _4926 = NOVALUE;
    int _4925 = NOVALUE;
    int _4924 = NOVALUE;
    int _4923 = NOVALUE;
    int _4922 = NOVALUE;
    int _4921 = NOVALUE;
    int _4920 = NOVALUE;
    int _4919 = NOVALUE;
    int _4918 = NOVALUE;
    int _4917 = NOVALUE;
    int _4916 = NOVALUE;
    int _4915 = NOVALUE;
    int _4914 = NOVALUE;
    int _4913 = NOVALUE;
    int _4912 = NOVALUE;
    int _4911 = NOVALUE;
    int _4910 = NOVALUE;
    int _4909 = NOVALUE;
    int _4908 = NOVALUE;
    int _4907 = NOVALUE;
    int _4906 = NOVALUE;
    int _4905 = NOVALUE;
    int _4904 = NOVALUE;
    int _4903 = NOVALUE;
    int _4902 = NOVALUE;
    int _4901 = NOVALUE;
    int _4900 = NOVALUE;
    int _4897 = NOVALUE;
    int _4896 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if c = 0 then*/
    if (_c_9004 != 0)
    goto L1; // [9] 47

    /** 		c = sdata[pos]*/
    _2 = (int)SEQ_PTR(_sdata_9002);
    _c_9004 = (int)*(((s1_ptr)_2)->base + _pos_9003);
    if (!IS_ATOM_INT(_c_9004))
    _c_9004 = (long)DBL_PTR(_c_9004)->dbl;

    /** 		pos += 1*/
    _pos_9003 = _pos_9003 + 1;

    /** 		if c < I2B then*/
    if (_c_9004 >= 249)
    goto L2; // [27] 46

    /** 			return {c + MIN1B, pos}*/
    _4896 = _c_9004 + -9;
    if ((long)((unsigned long)_4896 + (unsigned long)HIGH_BITS) >= 0) 
    _4896 = NewDouble((double)_4896);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _4896;
    ((int *)_2)[2] = _pos_9003;
    _4897 = MAKE_SEQ(_1);
    _4896 = NOVALUE;
    DeRefDS(_sdata_9002);
    DeRef(_s_9005);
    return _4897;
L2: 
L1: 

    /** 	switch c with fallthru do*/
    _0 = _c_9004;
    switch ( _0 ){ 

        /** 		case I2B then*/
        case 249:

        /** 			return {sdata[pos] +*/
        _2 = (int)SEQ_PTR(_sdata_9002);
        _4900 = (int)*(((s1_ptr)_2)->base + _pos_9003);
        _4901 = _pos_9003 + 1;
        _2 = (int)SEQ_PTR(_sdata_9002);
        _4902 = (int)*(((s1_ptr)_2)->base + _4901);
        if (IS_ATOM_INT(_4902)) {
            if (_4902 <= INT15 && _4902 >= -INT15)
            _4903 = 256 * _4902;
            else
            _4903 = NewDouble(256 * (double)_4902);
        }
        else {
            _4903 = binary_op(MULTIPLY, 256, _4902);
        }
        _4902 = NOVALUE;
        if (IS_ATOM_INT(_4900) && IS_ATOM_INT(_4903)) {
            _4904 = _4900 + _4903;
            if ((long)((unsigned long)_4904 + (unsigned long)HIGH_BITS) >= 0) 
            _4904 = NewDouble((double)_4904);
        }
        else {
            _4904 = binary_op(PLUS, _4900, _4903);
        }
        _4900 = NOVALUE;
        DeRef(_4903);
        _4903 = NOVALUE;
        if (IS_ATOM_INT(_4904)) {
            _4905 = _4904 + _24MIN2B_8773;
            if ((long)((unsigned long)_4905 + (unsigned long)HIGH_BITS) >= 0) 
            _4905 = NewDouble((double)_4905);
        }
        else {
            _4905 = binary_op(PLUS, _4904, _24MIN2B_8773);
        }
        DeRef(_4904);
        _4904 = NOVALUE;
        _4906 = _pos_9003 + 2;
        if ((long)((unsigned long)_4906 + (unsigned long)HIGH_BITS) >= 0) 
        _4906 = NewDouble((double)_4906);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _4905;
        ((int *)_2)[2] = _4906;
        _4907 = MAKE_SEQ(_1);
        _4906 = NOVALUE;
        _4905 = NOVALUE;
        DeRefDS(_sdata_9002);
        DeRef(_s_9005);
        DeRef(_4897);
        _4897 = NOVALUE;
        _4901 = NOVALUE;
        return _4907;

        /** 		case I3B then*/
        case 250:

        /** 			return {sdata[pos] +*/
        _2 = (int)SEQ_PTR(_sdata_9002);
        _4908 = (int)*(((s1_ptr)_2)->base + _pos_9003);
        _4909 = _pos_9003 + 1;
        _2 = (int)SEQ_PTR(_sdata_9002);
        _4910 = (int)*(((s1_ptr)_2)->base + _4909);
        if (IS_ATOM_INT(_4910)) {
            if (_4910 <= INT15 && _4910 >= -INT15)
            _4911 = 256 * _4910;
            else
            _4911 = NewDouble(256 * (double)_4910);
        }
        else {
            _4911 = binary_op(MULTIPLY, 256, _4910);
        }
        _4910 = NOVALUE;
        if (IS_ATOM_INT(_4908) && IS_ATOM_INT(_4911)) {
            _4912 = _4908 + _4911;
            if ((long)((unsigned long)_4912 + (unsigned long)HIGH_BITS) >= 0) 
            _4912 = NewDouble((double)_4912);
        }
        else {
            _4912 = binary_op(PLUS, _4908, _4911);
        }
        _4908 = NOVALUE;
        DeRef(_4911);
        _4911 = NOVALUE;
        _4913 = _pos_9003 + 2;
        _2 = (int)SEQ_PTR(_sdata_9002);
        _4914 = (int)*(((s1_ptr)_2)->base + _4913);
        if (IS_ATOM_INT(_4914)) {
            _4915 = NewDouble(65536 * (double)_4914);
        }
        else {
            _4915 = binary_op(MULTIPLY, 65536, _4914);
        }
        _4914 = NOVALUE;
        if (IS_ATOM_INT(_4912) && IS_ATOM_INT(_4915)) {
            _4916 = _4912 + _4915;
            if ((long)((unsigned long)_4916 + (unsigned long)HIGH_BITS) >= 0) 
            _4916 = NewDouble((double)_4916);
        }
        else {
            _4916 = binary_op(PLUS, _4912, _4915);
        }
        DeRef(_4912);
        _4912 = NOVALUE;
        DeRef(_4915);
        _4915 = NOVALUE;
        if (IS_ATOM_INT(_4916)) {
            _4917 = _4916 + _24MIN3B_8779;
            if ((long)((unsigned long)_4917 + (unsigned long)HIGH_BITS) >= 0) 
            _4917 = NewDouble((double)_4917);
        }
        else {
            _4917 = binary_op(PLUS, _4916, _24MIN3B_8779);
        }
        DeRef(_4916);
        _4916 = NOVALUE;
        _4918 = _pos_9003 + 3;
        if ((long)((unsigned long)_4918 + (unsigned long)HIGH_BITS) >= 0) 
        _4918 = NewDouble((double)_4918);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _4917;
        ((int *)_2)[2] = _4918;
        _4919 = MAKE_SEQ(_1);
        _4918 = NOVALUE;
        _4917 = NOVALUE;
        DeRefDS(_sdata_9002);
        DeRef(_s_9005);
        DeRef(_4897);
        _4897 = NOVALUE;
        DeRef(_4907);
        _4907 = NOVALUE;
        DeRef(_4901);
        _4901 = NOVALUE;
        _4913 = NOVALUE;
        _4909 = NOVALUE;
        return _4919;

        /** 		case I4B then*/
        case 251:

        /** 			return {getp4(sdata, pos) + MIN4B, pos + 4}*/
        RefDS(_sdata_9002);
        _4920 = _24getp4(_sdata_9002, _pos_9003);
        if (IS_ATOM_INT(_4920) && IS_ATOM_INT(_24MIN4B_8785)) {
            _4921 = _4920 + _24MIN4B_8785;
            if ((long)((unsigned long)_4921 + (unsigned long)HIGH_BITS) >= 0) 
            _4921 = NewDouble((double)_4921);
        }
        else {
            _4921 = binary_op(PLUS, _4920, _24MIN4B_8785);
        }
        DeRef(_4920);
        _4920 = NOVALUE;
        _4922 = _pos_9003 + 4;
        if ((long)((unsigned long)_4922 + (unsigned long)HIGH_BITS) >= 0) 
        _4922 = NewDouble((double)_4922);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _4921;
        ((int *)_2)[2] = _4922;
        _4923 = MAKE_SEQ(_1);
        _4922 = NOVALUE;
        _4921 = NOVALUE;
        DeRefDS(_sdata_9002);
        DeRef(_s_9005);
        DeRef(_4897);
        _4897 = NOVALUE;
        DeRef(_4907);
        _4907 = NOVALUE;
        DeRef(_4901);
        _4901 = NOVALUE;
        DeRef(_4913);
        _4913 = NOVALUE;
        DeRef(_4909);
        _4909 = NOVALUE;
        DeRef(_4919);
        _4919 = NOVALUE;
        return _4923;

        /** 		case F4B then*/
        case 252:

        /** 			return {convert:float32_to_atom({sdata[pos], sdata[pos+1],*/
        _2 = (int)SEQ_PTR(_sdata_9002);
        _4924 = (int)*(((s1_ptr)_2)->base + _pos_9003);
        _4925 = _pos_9003 + 1;
        _2 = (int)SEQ_PTR(_sdata_9002);
        _4926 = (int)*(((s1_ptr)_2)->base + _4925);
        _4927 = _pos_9003 + 2;
        _2 = (int)SEQ_PTR(_sdata_9002);
        _4928 = (int)*(((s1_ptr)_2)->base + _4927);
        _4929 = _pos_9003 + 3;
        _2 = (int)SEQ_PTR(_sdata_9002);
        _4930 = (int)*(((s1_ptr)_2)->base + _4929);
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_4924);
        *((int *)(_2+4)) = _4924;
        Ref(_4926);
        *((int *)(_2+8)) = _4926;
        Ref(_4928);
        *((int *)(_2+12)) = _4928;
        Ref(_4930);
        *((int *)(_2+16)) = _4930;
        _4931 = MAKE_SEQ(_1);
        _4930 = NOVALUE;
        _4928 = NOVALUE;
        _4926 = NOVALUE;
        _4924 = NOVALUE;
        DeRef(_ieee32_inlined_float32_to_atom_at_227_9054);
        _ieee32_inlined_float32_to_atom_at_227_9054 = _4931;
        _4931 = NOVALUE;

        /** 	return machine_func(M_F32_TO_A, ieee32)*/
        DeRef(_float32_to_atom_inlined_float32_to_atom_at_230_9055);
        _float32_to_atom_inlined_float32_to_atom_at_230_9055 = machine(49, _ieee32_inlined_float32_to_atom_at_227_9054);
        DeRef(_ieee32_inlined_float32_to_atom_at_227_9054);
        _ieee32_inlined_float32_to_atom_at_227_9054 = NOVALUE;
        _4932 = _pos_9003 + 4;
        if ((long)((unsigned long)_4932 + (unsigned long)HIGH_BITS) >= 0) 
        _4932 = NewDouble((double)_4932);
        Ref(_float32_to_atom_inlined_float32_to_atom_at_230_9055);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _float32_to_atom_inlined_float32_to_atom_at_230_9055;
        ((int *)_2)[2] = _4932;
        _4933 = MAKE_SEQ(_1);
        _4932 = NOVALUE;
        DeRefDS(_sdata_9002);
        DeRef(_s_9005);
        DeRef(_4897);
        _4897 = NOVALUE;
        DeRef(_4907);
        _4907 = NOVALUE;
        DeRef(_4901);
        _4901 = NOVALUE;
        DeRef(_4913);
        _4913 = NOVALUE;
        DeRef(_4909);
        _4909 = NOVALUE;
        DeRef(_4919);
        _4919 = NOVALUE;
        DeRef(_4923);
        _4923 = NOVALUE;
        DeRef(_4925);
        _4925 = NOVALUE;
        DeRef(_4927);
        _4927 = NOVALUE;
        DeRef(_4929);
        _4929 = NOVALUE;
        return _4933;

        /** 		case F8B then*/
        case 253:

        /** 			return {convert:float64_to_atom({sdata[pos], sdata[pos+1],*/
        _2 = (int)SEQ_PTR(_sdata_9002);
        _4934 = (int)*(((s1_ptr)_2)->base + _pos_9003);
        _4935 = _pos_9003 + 1;
        _2 = (int)SEQ_PTR(_sdata_9002);
        _4936 = (int)*(((s1_ptr)_2)->base + _4935);
        _4937 = _pos_9003 + 2;
        _2 = (int)SEQ_PTR(_sdata_9002);
        _4938 = (int)*(((s1_ptr)_2)->base + _4937);
        _4939 = _pos_9003 + 3;
        _2 = (int)SEQ_PTR(_sdata_9002);
        _4940 = (int)*(((s1_ptr)_2)->base + _4939);
        _4941 = _pos_9003 + 4;
        _2 = (int)SEQ_PTR(_sdata_9002);
        _4942 = (int)*(((s1_ptr)_2)->base + _4941);
        _4943 = _pos_9003 + 5;
        _2 = (int)SEQ_PTR(_sdata_9002);
        _4944 = (int)*(((s1_ptr)_2)->base + _4943);
        _4945 = _pos_9003 + 6;
        _2 = (int)SEQ_PTR(_sdata_9002);
        _4946 = (int)*(((s1_ptr)_2)->base + _4945);
        _4947 = _pos_9003 + 7;
        _2 = (int)SEQ_PTR(_sdata_9002);
        _4948 = (int)*(((s1_ptr)_2)->base + _4947);
        _1 = NewS1(8);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_4934);
        *((int *)(_2+4)) = _4934;
        Ref(_4936);
        *((int *)(_2+8)) = _4936;
        Ref(_4938);
        *((int *)(_2+12)) = _4938;
        Ref(_4940);
        *((int *)(_2+16)) = _4940;
        Ref(_4942);
        *((int *)(_2+20)) = _4942;
        Ref(_4944);
        *((int *)(_2+24)) = _4944;
        Ref(_4946);
        *((int *)(_2+28)) = _4946;
        Ref(_4948);
        *((int *)(_2+32)) = _4948;
        _4949 = MAKE_SEQ(_1);
        _4948 = NOVALUE;
        _4946 = NOVALUE;
        _4944 = NOVALUE;
        _4942 = NOVALUE;
        _4940 = NOVALUE;
        _4938 = NOVALUE;
        _4936 = NOVALUE;
        _4934 = NOVALUE;
        DeRef(_ieee64_inlined_float64_to_atom_at_330_9076);
        _ieee64_inlined_float64_to_atom_at_330_9076 = _4949;
        _4949 = NOVALUE;

        /** 	return machine_func(M_F64_TO_A, ieee64)*/
        DeRef(_float64_to_atom_inlined_float64_to_atom_at_333_9077);
        _float64_to_atom_inlined_float64_to_atom_at_333_9077 = machine(47, _ieee64_inlined_float64_to_atom_at_330_9076);
        DeRef(_ieee64_inlined_float64_to_atom_at_330_9076);
        _ieee64_inlined_float64_to_atom_at_330_9076 = NOVALUE;
        _4950 = _pos_9003 + 8;
        if ((long)((unsigned long)_4950 + (unsigned long)HIGH_BITS) >= 0) 
        _4950 = NewDouble((double)_4950);
        Ref(_float64_to_atom_inlined_float64_to_atom_at_333_9077);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _float64_to_atom_inlined_float64_to_atom_at_333_9077;
        ((int *)_2)[2] = _4950;
        _4951 = MAKE_SEQ(_1);
        _4950 = NOVALUE;
        DeRefDS(_sdata_9002);
        DeRef(_s_9005);
        DeRef(_4909);
        _4909 = NOVALUE;
        DeRef(_4897);
        _4897 = NOVALUE;
        DeRef(_4927);
        _4927 = NOVALUE;
        DeRef(_4913);
        _4913 = NOVALUE;
        DeRef(_4925);
        _4925 = NOVALUE;
        DeRef(_4929);
        _4929 = NOVALUE;
        DeRef(_4919);
        _4919 = NOVALUE;
        DeRef(_4943);
        _4943 = NOVALUE;
        DeRef(_4939);
        _4939 = NOVALUE;
        DeRef(_4941);
        _4941 = NOVALUE;
        DeRef(_4937);
        _4937 = NOVALUE;
        DeRef(_4947);
        _4947 = NOVALUE;
        DeRef(_4901);
        _4901 = NOVALUE;
        DeRef(_4923);
        _4923 = NOVALUE;
        DeRef(_4933);
        _4933 = NOVALUE;
        DeRef(_4945);
        _4945 = NOVALUE;
        DeRef(_4935);
        _4935 = NOVALUE;
        DeRef(_4907);
        _4907 = NOVALUE;
        return _4951;

        /** 		case else*/
        default:

        /** 			if c = S1B then*/
        if (_c_9004 != 254)
        goto L3; // [363] 382

        /** 				len = sdata[pos]*/
        _2 = (int)SEQ_PTR(_sdata_9002);
        _len_9006 = (int)*(((s1_ptr)_2)->base + _pos_9003);
        if (!IS_ATOM_INT(_len_9006))
        _len_9006 = (long)DBL_PTR(_len_9006)->dbl;

        /** 				pos += 1*/
        _pos_9003 = _pos_9003 + 1;
        goto L4; // [379] 398
L3: 

        /** 				len = getp4(sdata, pos)*/
        RefDS(_sdata_9002);
        _len_9006 = _24getp4(_sdata_9002, _pos_9003);
        if (!IS_ATOM_INT(_len_9006)) {
            _1 = (long)(DBL_PTR(_len_9006)->dbl);
            if (UNIQUE(DBL_PTR(_len_9006)) && (DBL_PTR(_len_9006)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_len_9006);
            _len_9006 = _1;
        }

        /** 				pos += 4*/
        _pos_9003 = _pos_9003 + 4;
L4: 

        /** 			if c = S4B and len < 256 then*/
        _4957 = (_c_9004 == 255);
        if (_4957 == 0) {
            goto L5; // [404] 516
        }
        _4959 = (_len_9006 < 256);
        if (_4959 == 0)
        {
            DeRef(_4959);
            _4959 = NOVALUE;
            goto L5; // [415] 516
        }
        else{
            DeRef(_4959);
            _4959 = NOVALUE;
        }

        /** 				if len = I8B then*/
        if (_len_9006 != 0)
        goto L6; // [422] 456

        /** 					return {int64_to_atom( sdata[pos..pos+7] ), pos + 8 }*/
        _4961 = _pos_9003 + 7;
        rhs_slice_target = (object_ptr)&_4962;
        RHS_Slice(_sdata_9002, _pos_9003, _4961);
        _4963 = _24int64_to_atom(_4962);
        _4962 = NOVALUE;
        _4964 = _pos_9003 + 8;
        if ((long)((unsigned long)_4964 + (unsigned long)HIGH_BITS) >= 0) 
        _4964 = NewDouble((double)_4964);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _4963;
        ((int *)_2)[2] = _4964;
        _4965 = MAKE_SEQ(_1);
        _4964 = NOVALUE;
        _4963 = NOVALUE;
        DeRefDS(_sdata_9002);
        DeRef(_s_9005);
        DeRef(_4909);
        _4909 = NOVALUE;
        DeRef(_4897);
        _4897 = NOVALUE;
        DeRef(_4927);
        _4927 = NOVALUE;
        DeRef(_4913);
        _4913 = NOVALUE;
        DeRef(_4925);
        _4925 = NOVALUE;
        DeRef(_4929);
        _4929 = NOVALUE;
        DeRef(_4919);
        _4919 = NOVALUE;
        _4961 = NOVALUE;
        DeRef(_4943);
        _4943 = NOVALUE;
        DeRef(_4951);
        _4951 = NOVALUE;
        DeRef(_4939);
        _4939 = NOVALUE;
        DeRef(_4941);
        _4941 = NOVALUE;
        DeRef(_4937);
        _4937 = NOVALUE;
        DeRef(_4947);
        _4947 = NOVALUE;
        DeRef(_4901);
        _4901 = NOVALUE;
        DeRef(_4923);
        _4923 = NOVALUE;
        DeRef(_4933);
        _4933 = NOVALUE;
        DeRef(_4945);
        _4945 = NOVALUE;
        DeRef(_4935);
        _4935 = NOVALUE;
        DeRef(_4957);
        _4957 = NOVALUE;
        DeRef(_4907);
        _4907 = NOVALUE;
        return _4965;
        goto L7; // [453] 623
L6: 

        /** 				elsif len = F10B then*/
        if (_len_9006 != 1)
        goto L8; // [458] 492

        /** 					return { float80_to_atom( sdata[pos..pos+9] ), pos + 10 }*/
        _4967 = _pos_9003 + 9;
        rhs_slice_target = (object_ptr)&_4968;
        RHS_Slice(_sdata_9002, _pos_9003, _4967);
        _4969 = _24float80_to_atom(_4968);
        _4968 = NOVALUE;
        _4970 = _pos_9003 + 10;
        if ((long)((unsigned long)_4970 + (unsigned long)HIGH_BITS) >= 0) 
        _4970 = NewDouble((double)_4970);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _4969;
        ((int *)_2)[2] = _4970;
        _4971 = MAKE_SEQ(_1);
        _4970 = NOVALUE;
        _4969 = NOVALUE;
        DeRefDS(_sdata_9002);
        DeRef(_s_9005);
        DeRef(_4909);
        _4909 = NOVALUE;
        DeRef(_4897);
        _4897 = NOVALUE;
        DeRef(_4927);
        _4927 = NOVALUE;
        DeRef(_4913);
        _4913 = NOVALUE;
        DeRef(_4925);
        _4925 = NOVALUE;
        DeRef(_4929);
        _4929 = NOVALUE;
        DeRef(_4919);
        _4919 = NOVALUE;
        DeRef(_4961);
        _4961 = NOVALUE;
        DeRef(_4943);
        _4943 = NOVALUE;
        DeRef(_4951);
        _4951 = NOVALUE;
        DeRef(_4939);
        _4939 = NOVALUE;
        DeRef(_4941);
        _4941 = NOVALUE;
        DeRef(_4937);
        _4937 = NOVALUE;
        DeRef(_4947);
        _4947 = NOVALUE;
        DeRef(_4901);
        _4901 = NOVALUE;
        _4967 = NOVALUE;
        DeRef(_4923);
        _4923 = NOVALUE;
        DeRef(_4933);
        _4933 = NOVALUE;
        DeRef(_4965);
        _4965 = NOVALUE;
        DeRef(_4945);
        _4945 = NOVALUE;
        DeRef(_4935);
        _4935 = NOVALUE;
        DeRef(_4957);
        _4957 = NOVALUE;
        DeRef(_4907);
        _4907 = NOVALUE;
        return _4971;
        goto L7; // [489] 623
L8: 

        /** 					error:crash( "Invalid sequence serialization" )*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_493_9108);
        _msg_inlined_crash_at_493_9108 = EPrintf(-9999999, _4874, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_493_9108);

        /** end procedure*/
        goto L9; // [507] 510
L9: 
        DeRefi(_msg_inlined_crash_at_493_9108);
        _msg_inlined_crash_at_493_9108 = NOVALUE;
        goto L7; // [513] 623
L5: 

        /** 				s = repeat(0, len)*/
        DeRef(_s_9005);
        _s_9005 = Repeat(0, _len_9006);

        /** 				for i = 1 to len do*/
        _4973 = _len_9006;
        {
            int _i_9112;
            _i_9112 = 1;
LA: 
            if (_i_9112 > _4973){
                goto LB; // [529] 612
            }

            /** 					c = sdata[pos]*/
            _2 = (int)SEQ_PTR(_sdata_9002);
            _c_9004 = (int)*(((s1_ptr)_2)->base + _pos_9003);
            if (!IS_ATOM_INT(_c_9004))
            _c_9004 = (long)DBL_PTR(_c_9004)->dbl;

            /** 					pos += 1*/
            _pos_9003 = _pos_9003 + 1;

            /** 					if c < I2B then*/
            if (_c_9004 >= 249)
            goto LC; // [550] 567

            /** 						s[i] = c + MIN1B*/
            _4977 = _c_9004 + -9;
            if ((long)((unsigned long)_4977 + (unsigned long)HIGH_BITS) >= 0) 
            _4977 = NewDouble((double)_4977);
            _2 = (int)SEQ_PTR(_s_9005);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_9005 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_9112);
            _1 = *(int *)_2;
            *(int *)_2 = _4977;
            if( _1 != _4977 ){
                DeRef(_1);
            }
            _4977 = NOVALUE;
            goto LD; // [564] 605
LC: 

            /** 						sequence temp = deserialize_object(sdata, pos, c)*/
            RefDS(_sdata_9002);
            DeRef(_4978);
            _4978 = _sdata_9002;
            DeRef(_4979);
            _4979 = _pos_9003;
            DeRef(_4980);
            _4980 = _c_9004;
            _0 = _temp_9120;
            _temp_9120 = _24deserialize_object(_4978, _4979, _4980);
            DeRef(_0);
            _4978 = NOVALUE;
            _4979 = NOVALUE;
            _4980 = NOVALUE;

            /** 						s[i] = temp[1]*/
            _2 = (int)SEQ_PTR(_temp_9120);
            _4982 = (int)*(((s1_ptr)_2)->base + 1);
            Ref(_4982);
            _2 = (int)SEQ_PTR(_s_9005);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_9005 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_9112);
            _1 = *(int *)_2;
            *(int *)_2 = _4982;
            if( _1 != _4982 ){
                DeRef(_1);
            }
            _4982 = NOVALUE;

            /** 						pos = temp[2]*/
            _2 = (int)SEQ_PTR(_temp_9120);
            _pos_9003 = (int)*(((s1_ptr)_2)->base + 2);
            if (!IS_ATOM_INT(_pos_9003))
            _pos_9003 = (long)DBL_PTR(_pos_9003)->dbl;
            DeRefDS(_temp_9120);
            _temp_9120 = NOVALUE;
LD: 

            /** 				end for*/
            _i_9112 = _i_9112 + 1;
            goto LA; // [607] 536
LB: 
            ;
        }

        /** 				return {s, pos}*/
        RefDS(_s_9005);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _s_9005;
        ((int *)_2)[2] = _pos_9003;
        _4984 = MAKE_SEQ(_1);
        DeRefDS(_sdata_9002);
        DeRefDS(_s_9005);
        DeRef(_4971);
        _4971 = NOVALUE;
        DeRef(_4909);
        _4909 = NOVALUE;
        DeRef(_4897);
        _4897 = NOVALUE;
        DeRef(_4927);
        _4927 = NOVALUE;
        DeRef(_4913);
        _4913 = NOVALUE;
        DeRef(_4925);
        _4925 = NOVALUE;
        DeRef(_4929);
        _4929 = NOVALUE;
        DeRef(_4919);
        _4919 = NOVALUE;
        DeRef(_4961);
        _4961 = NOVALUE;
        DeRef(_4943);
        _4943 = NOVALUE;
        DeRef(_4951);
        _4951 = NOVALUE;
        DeRef(_4939);
        _4939 = NOVALUE;
        DeRef(_4941);
        _4941 = NOVALUE;
        DeRef(_4937);
        _4937 = NOVALUE;
        DeRef(_4947);
        _4947 = NOVALUE;
        DeRef(_4901);
        _4901 = NOVALUE;
        DeRef(_4967);
        _4967 = NOVALUE;
        DeRef(_4923);
        _4923 = NOVALUE;
        DeRef(_4933);
        _4933 = NOVALUE;
        DeRef(_4965);
        _4965 = NOVALUE;
        DeRef(_4945);
        _4945 = NOVALUE;
        DeRef(_4935);
        _4935 = NOVALUE;
        DeRef(_4957);
        _4957 = NOVALUE;
        DeRef(_4907);
        _4907 = NOVALUE;
        return _4984;
L7: 
    ;}    ;
}


int _24deserialize(int _sdata_9130, int _pos_9131)
{
    int _4988 = NOVALUE;
    int _4987 = NOVALUE;
    int _4986 = NOVALUE;
    int _4985 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(sdata) then*/
    _4985 = 1;
    if (_4985 == 0)
    {
        _4985 = NOVALUE;
        goto L1; // [8] 23
    }
    else{
        _4985 = NOVALUE;
    }

    /** 		return deserialize_file(sdata, 0)*/
    _4986 = _24deserialize_file(_sdata_9130, 0);
    return _4986;
L1: 

    /** 	if atom(sdata) then*/
    _4987 = 1;
    if (_4987 == 0)
    {
        _4987 = NOVALUE;
        goto L2; // [28] 38
    }
    else{
        _4987 = NOVALUE;
    }

    /** 		return 0*/
    DeRef(_4986);
    _4986 = NOVALUE;
    return 0;
L2: 

    /** 	return deserialize_object(sdata, pos, 0)*/
    _4988 = _24deserialize_object(_sdata_9130, _pos_9131, 0);
    DeRef(_4986);
    _4986 = NOVALUE;
    return _4988;
    ;
}


int _24serialize(int _x_9140)
{
    int _x4_9141 = NOVALUE;
    int _s_9142 = NOVALUE;
    int _atom_to_float32_inlined_atom_to_float32_at_192_9176 = NOVALUE;
    int _float32_to_atom_inlined_float32_to_atom_at_203_9179 = NOVALUE;
    int _atom_to_float64_inlined_atom_to_float64_at_229_9184 = NOVALUE;
    int _5027 = NOVALUE;
    int _5026 = NOVALUE;
    int _5025 = NOVALUE;
    int _5023 = NOVALUE;
    int _5022 = NOVALUE;
    int _5020 = NOVALUE;
    int _5018 = NOVALUE;
    int _5017 = NOVALUE;
    int _5016 = NOVALUE;
    int _5014 = NOVALUE;
    int _5013 = NOVALUE;
    int _5012 = NOVALUE;
    int _5011 = NOVALUE;
    int _5010 = NOVALUE;
    int _5009 = NOVALUE;
    int _5008 = NOVALUE;
    int _5007 = NOVALUE;
    int _5006 = NOVALUE;
    int _5004 = NOVALUE;
    int _5003 = NOVALUE;
    int _5002 = NOVALUE;
    int _5001 = NOVALUE;
    int _5000 = NOVALUE;
    int _4999 = NOVALUE;
    int _4997 = NOVALUE;
    int _4996 = NOVALUE;
    int _4995 = NOVALUE;
    int _4994 = NOVALUE;
    int _4993 = NOVALUE;
    int _4992 = NOVALUE;
    int _4991 = NOVALUE;
    int _4990 = NOVALUE;
    int _4989 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(x) then*/
    if (IS_ATOM_INT(_x_9140))
    _4989 = 1;
    else if (IS_ATOM_DBL(_x_9140))
    _4989 = IS_ATOM_INT(DoubleToInt(_x_9140));
    else
    _4989 = 0;
    if (_4989 == 0)
    {
        _4989 = NOVALUE;
        goto L1; // [6] 183
    }
    else{
        _4989 = NOVALUE;
    }

    /** 		if x >= MIN1B and x <= MAX1B then*/
    if (IS_ATOM_INT(_x_9140)) {
        _4990 = (_x_9140 >= -9);
    }
    else {
        _4990 = binary_op(GREATEREQ, _x_9140, -9);
    }
    if (IS_ATOM_INT(_4990)) {
        if (_4990 == 0) {
            goto L2; // [15] 44
        }
    }
    else {
        if (DBL_PTR(_4990)->dbl == 0.0) {
            goto L2; // [15] 44
        }
    }
    if (IS_ATOM_INT(_x_9140)) {
        _4992 = (_x_9140 <= 239);
    }
    else {
        _4992 = binary_op(LESSEQ, _x_9140, 239);
    }
    if (_4992 == 0) {
        DeRef(_4992);
        _4992 = NOVALUE;
        goto L2; // [24] 44
    }
    else {
        if (!IS_ATOM_INT(_4992) && DBL_PTR(_4992)->dbl == 0.0){
            DeRef(_4992);
            _4992 = NOVALUE;
            goto L2; // [24] 44
        }
        DeRef(_4992);
        _4992 = NOVALUE;
    }
    DeRef(_4992);
    _4992 = NOVALUE;

    /** 			return {x - MIN1B}*/
    if (IS_ATOM_INT(_x_9140)) {
        _4993 = _x_9140 - -9;
        if ((long)((unsigned long)_4993 +(unsigned long) HIGH_BITS) >= 0){
            _4993 = NewDouble((double)_4993);
        }
    }
    else {
        _4993 = binary_op(MINUS, _x_9140, -9);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _4993;
    _4994 = MAKE_SEQ(_1);
    _4993 = NOVALUE;
    DeRef(_x_9140);
    DeRefi(_x4_9141);
    DeRef(_s_9142);
    DeRef(_4990);
    _4990 = NOVALUE;
    return _4994;
    goto L3; // [41] 328
L2: 

    /** 		elsif x >= MIN2B and x <= MAX2B then*/
    if (IS_ATOM_INT(_x_9140)) {
        _4995 = (_x_9140 >= _24MIN2B_8773);
    }
    else {
        _4995 = binary_op(GREATEREQ, _x_9140, _24MIN2B_8773);
    }
    if (IS_ATOM_INT(_4995)) {
        if (_4995 == 0) {
            goto L4; // [52] 97
        }
    }
    else {
        if (DBL_PTR(_4995)->dbl == 0.0) {
            goto L4; // [52] 97
        }
    }
    if (IS_ATOM_INT(_x_9140)) {
        _4997 = (_x_9140 <= 32767);
    }
    else {
        _4997 = binary_op(LESSEQ, _x_9140, 32767);
    }
    if (_4997 == 0) {
        DeRef(_4997);
        _4997 = NOVALUE;
        goto L4; // [63] 97
    }
    else {
        if (!IS_ATOM_INT(_4997) && DBL_PTR(_4997)->dbl == 0.0){
            DeRef(_4997);
            _4997 = NOVALUE;
            goto L4; // [63] 97
        }
        DeRef(_4997);
        _4997 = NOVALUE;
    }
    DeRef(_4997);
    _4997 = NOVALUE;

    /** 			x -= MIN2B*/
    _0 = _x_9140;
    if (IS_ATOM_INT(_x_9140)) {
        _x_9140 = _x_9140 - _24MIN2B_8773;
        if ((long)((unsigned long)_x_9140 +(unsigned long) HIGH_BITS) >= 0){
            _x_9140 = NewDouble((double)_x_9140);
        }
    }
    else {
        _x_9140 = binary_op(MINUS, _x_9140, _24MIN2B_8773);
    }
    DeRef(_0);

    /** 			return {I2B, and_bits(x, #FF), floor(x / #100)}*/
    if (IS_ATOM_INT(_x_9140)) {
        {unsigned long tu;
             tu = (unsigned long)_x_9140 & (unsigned long)255;
             _4999 = MAKE_UINT(tu);
        }
    }
    else {
        _4999 = binary_op(AND_BITS, _x_9140, 255);
    }
    if (IS_ATOM_INT(_x_9140)) {
        if (256 > 0 && _x_9140 >= 0) {
            _5000 = _x_9140 / 256;
        }
        else {
            temp_dbl = floor((double)_x_9140 / (double)256);
            if (_x_9140 != MININT)
            _5000 = (long)temp_dbl;
            else
            _5000 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_9140, 256);
        _5000 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 249;
    *((int *)(_2+8)) = _4999;
    *((int *)(_2+12)) = _5000;
    _5001 = MAKE_SEQ(_1);
    _5000 = NOVALUE;
    _4999 = NOVALUE;
    DeRef(_x_9140);
    DeRefi(_x4_9141);
    DeRef(_s_9142);
    DeRef(_4990);
    _4990 = NOVALUE;
    DeRef(_4994);
    _4994 = NOVALUE;
    DeRef(_4995);
    _4995 = NOVALUE;
    return _5001;
    goto L3; // [94] 328
L4: 

    /** 		elsif x >= MIN3B and x <= MAX3B then*/
    if (IS_ATOM_INT(_x_9140)) {
        _5002 = (_x_9140 >= _24MIN3B_8779);
    }
    else {
        _5002 = binary_op(GREATEREQ, _x_9140, _24MIN3B_8779);
    }
    if (IS_ATOM_INT(_5002)) {
        if (_5002 == 0) {
            goto L5; // [105] 159
        }
    }
    else {
        if (DBL_PTR(_5002)->dbl == 0.0) {
            goto L5; // [105] 159
        }
    }
    if (IS_ATOM_INT(_x_9140)) {
        _5004 = (_x_9140 <= 8388607);
    }
    else {
        _5004 = binary_op(LESSEQ, _x_9140, 8388607);
    }
    if (_5004 == 0) {
        DeRef(_5004);
        _5004 = NOVALUE;
        goto L5; // [116] 159
    }
    else {
        if (!IS_ATOM_INT(_5004) && DBL_PTR(_5004)->dbl == 0.0){
            DeRef(_5004);
            _5004 = NOVALUE;
            goto L5; // [116] 159
        }
        DeRef(_5004);
        _5004 = NOVALUE;
    }
    DeRef(_5004);
    _5004 = NOVALUE;

    /** 			x -= MIN3B*/
    _0 = _x_9140;
    if (IS_ATOM_INT(_x_9140)) {
        _x_9140 = _x_9140 - _24MIN3B_8779;
        if ((long)((unsigned long)_x_9140 +(unsigned long) HIGH_BITS) >= 0){
            _x_9140 = NewDouble((double)_x_9140);
        }
    }
    else {
        _x_9140 = binary_op(MINUS, _x_9140, _24MIN3B_8779);
    }
    DeRef(_0);

    /** 			return {I3B, and_bits(x, #FF), and_bits(floor(x / #100), #FF), floor(x / #10000)}*/
    if (IS_ATOM_INT(_x_9140)) {
        {unsigned long tu;
             tu = (unsigned long)_x_9140 & (unsigned long)255;
             _5006 = MAKE_UINT(tu);
        }
    }
    else {
        _5006 = binary_op(AND_BITS, _x_9140, 255);
    }
    if (IS_ATOM_INT(_x_9140)) {
        if (256 > 0 && _x_9140 >= 0) {
            _5007 = _x_9140 / 256;
        }
        else {
            temp_dbl = floor((double)_x_9140 / (double)256);
            if (_x_9140 != MININT)
            _5007 = (long)temp_dbl;
            else
            _5007 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_9140, 256);
        _5007 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    if (IS_ATOM_INT(_5007)) {
        {unsigned long tu;
             tu = (unsigned long)_5007 & (unsigned long)255;
             _5008 = MAKE_UINT(tu);
        }
    }
    else {
        _5008 = binary_op(AND_BITS, _5007, 255);
    }
    DeRef(_5007);
    _5007 = NOVALUE;
    if (IS_ATOM_INT(_x_9140)) {
        if (65536 > 0 && _x_9140 >= 0) {
            _5009 = _x_9140 / 65536;
        }
        else {
            temp_dbl = floor((double)_x_9140 / (double)65536);
            if (_x_9140 != MININT)
            _5009 = (long)temp_dbl;
            else
            _5009 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_9140, 65536);
        _5009 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 250;
    *((int *)(_2+8)) = _5006;
    *((int *)(_2+12)) = _5008;
    *((int *)(_2+16)) = _5009;
    _5010 = MAKE_SEQ(_1);
    _5009 = NOVALUE;
    _5008 = NOVALUE;
    _5006 = NOVALUE;
    DeRef(_x_9140);
    DeRefi(_x4_9141);
    DeRef(_s_9142);
    DeRef(_4990);
    _4990 = NOVALUE;
    DeRef(_4994);
    _4994 = NOVALUE;
    DeRef(_4995);
    _4995 = NOVALUE;
    DeRef(_5001);
    _5001 = NOVALUE;
    DeRef(_5002);
    _5002 = NOVALUE;
    return _5010;
    goto L3; // [156] 328
L5: 

    /** 			return I4B & convert:int_to_bytes(x-MIN4B)*/
    if (IS_ATOM_INT(_x_9140) && IS_ATOM_INT(_24MIN4B_8785)) {
        _5011 = _x_9140 - _24MIN4B_8785;
        if ((long)((unsigned long)_5011 +(unsigned long) HIGH_BITS) >= 0){
            _5011 = NewDouble((double)_5011);
        }
    }
    else {
        _5011 = binary_op(MINUS, _x_9140, _24MIN4B_8785);
    }
    _5012 = _6int_to_bytes(_5011);
    _5011 = NOVALUE;
    if (IS_SEQUENCE(251) && IS_ATOM(_5012)) {
    }
    else if (IS_ATOM(251) && IS_SEQUENCE(_5012)) {
        Prepend(&_5013, _5012, 251);
    }
    else {
        Concat((object_ptr)&_5013, 251, _5012);
    }
    DeRef(_5012);
    _5012 = NOVALUE;
    DeRef(_x_9140);
    DeRefi(_x4_9141);
    DeRef(_s_9142);
    DeRef(_4990);
    _4990 = NOVALUE;
    DeRef(_4994);
    _4994 = NOVALUE;
    DeRef(_4995);
    _4995 = NOVALUE;
    DeRef(_5001);
    _5001 = NOVALUE;
    DeRef(_5002);
    _5002 = NOVALUE;
    DeRef(_5010);
    _5010 = NOVALUE;
    return _5013;
    goto L3; // [180] 328
L1: 

    /** 	elsif atom(x) then*/
    _5014 = IS_ATOM(_x_9140);
    if (_5014 == 0)
    {
        _5014 = NOVALUE;
        goto L6; // [188] 249
    }
    else{
        _5014 = NOVALUE;
    }

    /** 		x4 = convert:atom_to_float32(x)*/

    /** 	return machine_func(M_A_TO_F32, a)*/
    DeRefi(_x4_9141);
    _x4_9141 = machine(48, _x_9140);

    /** 		if x = convert:float32_to_atom(x4) then*/

    /** 	return machine_func(M_F32_TO_A, ieee32)*/
    DeRef(_float32_to_atom_inlined_float32_to_atom_at_203_9179);
    _float32_to_atom_inlined_float32_to_atom_at_203_9179 = machine(49, _x4_9141);
    if (binary_op_a(NOTEQ, _x_9140, _float32_to_atom_inlined_float32_to_atom_at_203_9179)){
        goto L7; // [211] 228
    }

    /** 			return F4B & x4*/
    Prepend(&_5016, _x4_9141, 252);
    DeRef(_x_9140);
    DeRefDSi(_x4_9141);
    DeRef(_s_9142);
    DeRef(_4990);
    _4990 = NOVALUE;
    DeRef(_4994);
    _4994 = NOVALUE;
    DeRef(_4995);
    _4995 = NOVALUE;
    DeRef(_5001);
    _5001 = NOVALUE;
    DeRef(_5002);
    _5002 = NOVALUE;
    DeRef(_5010);
    _5010 = NOVALUE;
    DeRef(_5013);
    _5013 = NOVALUE;
    return _5016;
    goto L3; // [225] 328
L7: 

    /** 			return F8B & convert:atom_to_float64(x)*/

    /** 	return machine_func(M_A_TO_F64, a)*/
    DeRefi(_atom_to_float64_inlined_atom_to_float64_at_229_9184);
    _atom_to_float64_inlined_atom_to_float64_at_229_9184 = machine(46, _x_9140);
    Prepend(&_5017, _atom_to_float64_inlined_atom_to_float64_at_229_9184, 253);
    DeRef(_x_9140);
    DeRefi(_x4_9141);
    DeRef(_s_9142);
    DeRef(_4990);
    _4990 = NOVALUE;
    DeRef(_4994);
    _4994 = NOVALUE;
    DeRef(_4995);
    _4995 = NOVALUE;
    DeRef(_5001);
    _5001 = NOVALUE;
    DeRef(_5002);
    _5002 = NOVALUE;
    DeRef(_5010);
    _5010 = NOVALUE;
    DeRef(_5013);
    _5013 = NOVALUE;
    DeRef(_5016);
    _5016 = NOVALUE;
    return _5017;
    goto L3; // [246] 328
L6: 

    /** 		if length(x) <= 255 then*/
    if (IS_SEQUENCE(_x_9140)){
            _5018 = SEQ_PTR(_x_9140)->length;
    }
    else {
        _5018 = 1;
    }
    if (_5018 > 255)
    goto L8; // [254] 270

    /** 			s = {S1B, length(x)}*/
    if (IS_SEQUENCE(_x_9140)){
            _5020 = SEQ_PTR(_x_9140)->length;
    }
    else {
        _5020 = 1;
    }
    DeRef(_s_9142);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 254;
    ((int *)_2)[2] = _5020;
    _s_9142 = MAKE_SEQ(_1);
    _5020 = NOVALUE;
    goto L9; // [267] 284
L8: 

    /** 			s = S4B & convert:int_to_bytes(length(x))*/
    if (IS_SEQUENCE(_x_9140)){
            _5022 = SEQ_PTR(_x_9140)->length;
    }
    else {
        _5022 = 1;
    }
    _5023 = _6int_to_bytes(_5022);
    _5022 = NOVALUE;
    if (IS_SEQUENCE(255) && IS_ATOM(_5023)) {
    }
    else if (IS_ATOM(255) && IS_SEQUENCE(_5023)) {
        Prepend(&_s_9142, _5023, 255);
    }
    else {
        Concat((object_ptr)&_s_9142, 255, _5023);
    }
    DeRef(_5023);
    _5023 = NOVALUE;
L9: 

    /** 		for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_9140)){
            _5025 = SEQ_PTR(_x_9140)->length;
    }
    else {
        _5025 = 1;
    }
    {
        int _i_9197;
        _i_9197 = 1;
LA: 
        if (_i_9197 > _5025){
            goto LB; // [289] 319
        }

        /** 			s &= serialize(x[i])*/
        _2 = (int)SEQ_PTR(_x_9140);
        _5026 = (int)*(((s1_ptr)_2)->base + _i_9197);
        Ref(_5026);
        _5027 = _24serialize(_5026);
        _5026 = NOVALUE;
        if (IS_SEQUENCE(_s_9142) && IS_ATOM(_5027)) {
            Ref(_5027);
            Append(&_s_9142, _s_9142, _5027);
        }
        else if (IS_ATOM(_s_9142) && IS_SEQUENCE(_5027)) {
        }
        else {
            Concat((object_ptr)&_s_9142, _s_9142, _5027);
        }
        DeRef(_5027);
        _5027 = NOVALUE;

        /** 		end for*/
        _i_9197 = _i_9197 + 1;
        goto LA; // [314] 296
LB: 
        ;
    }

    /** 		return s*/
    DeRef(_x_9140);
    DeRefi(_x4_9141);
    DeRef(_4990);
    _4990 = NOVALUE;
    DeRef(_4994);
    _4994 = NOVALUE;
    DeRef(_4995);
    _4995 = NOVALUE;
    DeRef(_5001);
    _5001 = NOVALUE;
    DeRef(_5002);
    _5002 = NOVALUE;
    DeRef(_5010);
    _5010 = NOVALUE;
    DeRef(_5013);
    _5013 = NOVALUE;
    DeRef(_5016);
    _5016 = NOVALUE;
    DeRef(_5017);
    _5017 = NOVALUE;
    return _s_9142;
L3: 
    ;
}



// 0x8EBFADF5
