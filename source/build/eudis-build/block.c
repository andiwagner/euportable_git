// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _66block_type_name(int _opcode_46675)
{
    int _24849 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_opcode_46675)) {
        _1 = (long)(DBL_PTR(_opcode_46675)->dbl);
        if (UNIQUE(DBL_PTR(_opcode_46675)) && (DBL_PTR(_opcode_46675)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_opcode_46675);
        _opcode_46675 = _1;
    }

    /** 	switch opcode do*/
    _0 = _opcode_46675;
    switch ( _0 ){ 

        /** 		case LOOP then*/
        case 422:

        /** 			return "LOOP"*/
        RefDS(_24847);
        return _24847;
        goto L1; // [20] 63

        /** 		case PROC then*/
        case 27:

        /** 			return "PROC"*/
        RefDS(_22440);
        return _22440;
        goto L1; // [32] 63

        /** 		case FUNC then*/
        case 501:

        /** 			return "FUNC"*/
        RefDS(_24848);
        return _24848;
        goto L1; // [44] 63

        /** 		case else*/
        default:

        /** 			return opnames[opcode]*/
        _2 = (int)SEQ_PTR(_59opnames_22728);
        _24849 = (int)*(((s1_ptr)_2)->base + _opcode_46675);
        RefDS(_24849);
        return _24849;
    ;}L1: 
    ;
}


void _66check_block(int _got_46691)
{
    int _expected_46692 = NOVALUE;
    int _24857 = NOVALUE;
    int _24856 = NOVALUE;
    int _24855 = NOVALUE;
    int _24851 = NOVALUE;
    int _24850 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer expected = block_stack[$][BLOCK_OPCODE]*/
    if (IS_SEQUENCE(_66block_stack_46665)){
            _24850 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _24850 = 1;
    }
    _2 = (int)SEQ_PTR(_66block_stack_46665);
    _24851 = (int)*(((s1_ptr)_2)->base + _24850);
    _2 = (int)SEQ_PTR(_24851);
    _expected_46692 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_expected_46692)){
        _expected_46692 = (long)DBL_PTR(_expected_46692)->dbl;
    }
    _24851 = NOVALUE;

    /** 	if got = FUNC then*/
    if (_got_46691 != 501)
    goto L1; // [26] 40

    /** 		got = PROC*/
    _got_46691 = 27;
L1: 

    /** 	if got != expected then*/
    if (_got_46691 == _expected_46692)
    goto L2; // [42] 66

    /** 		CompileErr( 79, {block_type_name( expected ), block_type_name( got)} )*/
    _24855 = _66block_type_name(_expected_46692);
    _24856 = _66block_type_name(_got_46691);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24855;
    ((int *)_2)[2] = _24856;
    _24857 = MAKE_SEQ(_1);
    _24856 = NOVALUE;
    _24855 = NOVALUE;
    _43CompileErr(79, _24857, 0);
    _24857 = NOVALUE;
L2: 

    /** end procedure*/
    return;
    ;
}


void _66Block_var(int _sym_46709)
{
    int _block_46710 = NOVALUE;
    int _24878 = NOVALUE;
    int _24877 = NOVALUE;
    int _24876 = NOVALUE;
    int _24874 = NOVALUE;
    int _24873 = NOVALUE;
    int _24871 = NOVALUE;
    int _24870 = NOVALUE;
    int _24869 = NOVALUE;
    int _24868 = NOVALUE;
    int _24867 = NOVALUE;
    int _24866 = NOVALUE;
    int _24865 = NOVALUE;
    int _24863 = NOVALUE;
    int _24861 = NOVALUE;
    int _24860 = NOVALUE;
    int _24858 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_sym_46709)) {
        _1 = (long)(DBL_PTR(_sym_46709)->dbl);
        if (UNIQUE(DBL_PTR(_sym_46709)) && (DBL_PTR(_sym_46709)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_46709);
        _sym_46709 = _1;
    }

    /** 	sequence block = block_stack[$]*/
    if (IS_SEQUENCE(_66block_stack_46665)){
            _24858 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _24858 = 1;
    }
    DeRef(_block_46710);
    _2 = (int)SEQ_PTR(_66block_stack_46665);
    _block_46710 = (int)*(((s1_ptr)_2)->base + _24858);
    Ref(_block_46710);

    /** 	block_stack[$] = 0*/
    if (IS_SEQUENCE(_66block_stack_46665)){
            _24860 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _24860 = 1;
    }
    _2 = (int)SEQ_PTR(_66block_stack_46665);
    _2 = (int)(((s1_ptr)_2)->base + _24860);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	if length(block_stack) > 1 then*/
    if (IS_SEQUENCE(_66block_stack_46665)){
            _24861 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _24861 = 1;
    }
    if (_24861 <= 1)
    goto L1; // [34] 60

    /** 		SymTab[sym][S_BLOCK] = block[BLOCK_SYM]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_46709 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_block_46710);
    _24865 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_24865);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_BLOCK_11933))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_BLOCK_11933)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_BLOCK_11933);
    _1 = *(int *)_2;
    *(int *)_2 = _24865;
    if( _1 != _24865 ){
        DeRef(_1);
    }
    _24865 = NOVALUE;
    _24863 = NOVALUE;
L1: 

    /** 	if length(block[BLOCK_VARS]) then*/
    _2 = (int)SEQ_PTR(_block_46710);
    _24866 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_24866)){
            _24867 = SEQ_PTR(_24866)->length;
    }
    else {
        _24867 = 1;
    }
    _24866 = NOVALUE;
    if (_24867 == 0)
    {
        _24867 = NOVALUE;
        goto L2; // [71] 105
    }
    else{
        _24867 = NOVALUE;
    }

    /** 		SymTab[block[BLOCK_VARS][$]][S_NEXT_IN_BLOCK] = sym*/
    _2 = (int)SEQ_PTR(_block_46710);
    _24868 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_24868)){
            _24869 = SEQ_PTR(_24868)->length;
    }
    else {
        _24869 = 1;
    }
    _2 = (int)SEQ_PTR(_24868);
    _24870 = (int)*(((s1_ptr)_2)->base + _24869);
    _24868 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_24870))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_24870)->dbl));
    else
    _3 = (int)(_24870 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_NEXT_IN_BLOCK_11905))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NEXT_IN_BLOCK_11905)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_NEXT_IN_BLOCK_11905);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_46709;
    DeRef(_1);
    _24871 = NOVALUE;
    goto L3; // [102] 127
L2: 

    /** 		SymTab[block[BLOCK_SYM]][S_NEXT_IN_BLOCK] = sym*/
    _2 = (int)SEQ_PTR(_block_46710);
    _24873 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_24873))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_24873)->dbl));
    else
    _3 = (int)(_24873 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_NEXT_IN_BLOCK_11905))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NEXT_IN_BLOCK_11905)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_NEXT_IN_BLOCK_11905);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_46709;
    DeRef(_1);
    _24874 = NOVALUE;
L3: 

    /** 	block[BLOCK_VARS] &= sym*/
    _2 = (int)SEQ_PTR(_block_46710);
    _24876 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_24876) && IS_ATOM(_sym_46709)) {
        Append(&_24877, _24876, _sym_46709);
    }
    else if (IS_ATOM(_24876) && IS_SEQUENCE(_sym_46709)) {
    }
    else {
        Concat((object_ptr)&_24877, _24876, _sym_46709);
        _24876 = NOVALUE;
    }
    _24876 = NOVALUE;
    _2 = (int)SEQ_PTR(_block_46710);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _block_46710 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _24877;
    if( _1 != _24877 ){
        DeRef(_1);
    }
    _24877 = NOVALUE;

    /** 	block_stack[$] = block*/
    if (IS_SEQUENCE(_66block_stack_46665)){
            _24878 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _24878 = 1;
    }
    RefDS(_block_46710);
    _2 = (int)SEQ_PTR(_66block_stack_46665);
    _2 = (int)(((s1_ptr)_2)->base + _24878);
    _1 = *(int *)_2;
    *(int *)_2 = _block_46710;
    DeRef(_1);

    /** 	ifdef BDEBUG then*/

    /** end procedure*/
    DeRefDS(_block_46710);
    _24866 = NOVALUE;
    _24870 = NOVALUE;
    _24873 = NOVALUE;
    return;
    ;
}


void _66NewBlock(int _opcode_46744, int _block_label_46745)
{
    int _block_46763 = NOVALUE;
    int _24892 = NOVALUE;
    int _24891 = NOVALUE;
    int _24890 = NOVALUE;
    int _24888 = NOVALUE;
    int _24886 = NOVALUE;
    int _24885 = NOVALUE;
    int _24883 = NOVALUE;
    int _24882 = NOVALUE;
    int _24880 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	SymTab = append( SymTab, repeat( 0, SIZEOF_BLOCK_ENTRY ) )*/
    _24880 = Repeat(0, _25SIZEOF_BLOCK_ENTRY_12044);
    RefDS(_24880);
    Append(&_26SymTab_11138, _26SymTab_11138, _24880);
    DeRefDS(_24880);
    _24880 = NOVALUE;

    /** 	SymTab[$][S_MODE] = M_BLOCK*/
    if (IS_SEQUENCE(_26SymTab_11138)){
            _24882 = SEQ_PTR(_26SymTab_11138)->length;
    }
    else {
        _24882 = 1;
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_24882 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 4;
    DeRef(_1);
    _24883 = NOVALUE;

    /** 	SymTab[$][S_FIRST_LINE] = gline_number*/
    if (IS_SEQUENCE(_26SymTab_11138)){
            _24885 = SEQ_PTR(_26SymTab_11138)->length;
    }
    else {
        _24885 = 1;
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_24885 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_FIRST_LINE_11938))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FIRST_LINE_11938)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_FIRST_LINE_11938);
    _1 = *(int *)_2;
    *(int *)_2 = _25gline_number_12267;
    DeRef(_1);
    _24886 = NOVALUE;

    /** 	sequence block = repeat( 0, BLOCK_SIZE-1 )*/
    _24888 = 6;
    DeRef(_block_46763);
    _block_46763 = Repeat(0, 6);
    _24888 = NOVALUE;

    /** 	block[BLOCK_SYM]    = length(SymTab)*/
    if (IS_SEQUENCE(_26SymTab_11138)){
            _24890 = SEQ_PTR(_26SymTab_11138)->length;
    }
    else {
        _24890 = 1;
    }
    _2 = (int)SEQ_PTR(_block_46763);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _block_46763 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    *(int *)_2 = _24890;
    if( _1 != _24890 ){
    }
    _24890 = NOVALUE;

    /** 	block[BLOCK_OPCODE] = opcode*/
    _2 = (int)SEQ_PTR(_block_46763);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _block_46763 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    *(int *)_2 = _opcode_46744;

    /** 	block[BLOCK_LABEL]  = block_label*/
    Ref(_block_label_46745);
    _2 = (int)SEQ_PTR(_block_46763);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _block_46763 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    *(int *)_2 = _block_label_46745;

    /** 	block[BLOCK_START]  = length(Code) + 1*/
    if (IS_SEQUENCE(_25Code_12355)){
            _24891 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _24891 = 1;
    }
    _24892 = _24891 + 1;
    _24891 = NOVALUE;
    _2 = (int)SEQ_PTR(_block_46763);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _block_46763 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _24892;
    if( _1 != _24892 ){
        DeRef(_1);
    }
    _24892 = NOVALUE;

    /** 	block[BLOCK_VARS]   = {}*/
    RefDS(_22682);
    _2 = (int)SEQ_PTR(_block_46763);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _block_46763 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _22682;
    DeRef(_1);

    /** 	block_stack = append( block_stack, block )*/
    RefDS(_block_46763);
    Append(&_66block_stack_46665, _66block_stack_46665, _block_46763);

    /** 	current_block = length(SymTab)*/
    if (IS_SEQUENCE(_26SymTab_11138)){
            _66current_block_46672 = SEQ_PTR(_26SymTab_11138)->length;
    }
    else {
        _66current_block_46672 = 1;
    }

    /** end procedure*/
    DeRef(_block_label_46745);
    DeRefDS(_block_46763);
    return;
    ;
}


void _66Start_block(int _opcode_46776, int _block_label_46777)
{
    int _last_block_46779 = NOVALUE;
    int _label_name_46807 = NOVALUE;
    int _24914 = NOVALUE;
    int _24913 = NOVALUE;
    int _24912 = NOVALUE;
    int _24909 = NOVALUE;
    int _24908 = NOVALUE;
    int _24906 = NOVALUE;
    int _24905 = NOVALUE;
    int _24904 = NOVALUE;
    int _24903 = NOVALUE;
    int _24902 = NOVALUE;
    int _24899 = NOVALUE;
    int _24897 = NOVALUE;
    int _24896 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_opcode_46776)) {
        _1 = (long)(DBL_PTR(_opcode_46776)->dbl);
        if (UNIQUE(DBL_PTR(_opcode_46776)) && (DBL_PTR(_opcode_46776)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_opcode_46776);
        _opcode_46776 = _1;
    }

    /** 	symtab_index last_block = current_block*/
    _last_block_46779 = _66current_block_46672;

    /** 	if opcode = FUNC then*/
    if (_opcode_46776 != 501)
    goto L1; // [16] 30

    /** 		opcode = PROC*/
    _opcode_46776 = 27;
L1: 

    /** 	NewBlock( opcode, block_label )*/
    Ref(_block_label_46777);
    _66NewBlock(_opcode_46776, _block_label_46777);

    /** 	if find(opcode, RTN_TOKS) then*/
    _24896 = find_from(_opcode_46776, _28RTN_TOKS_11857, 1);
    if (_24896 == 0)
    {
        _24896 = NOVALUE;
        goto L2; // [45] 105
    }
    else{
        _24896 = NOVALUE;
    }

    /** 		SymTab[block_label][S_BLOCK] = current_block*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_block_label_46777))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_block_label_46777)->dbl));
    else
    _3 = (int)(_block_label_46777 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_BLOCK_11933))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_BLOCK_11933)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_BLOCK_11933);
    _1 = *(int *)_2;
    *(int *)_2 = _66current_block_46672;
    DeRef(_1);
    _24897 = NOVALUE;

    /** 		SymTab[current_block][S_NAME] = sprintf("BLOCK: %s", {SymTab[block_label][S_NAME]})*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_66current_block_46672 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_block_label_46777)){
        _24902 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_block_label_46777)->dbl));
    }
    else{
        _24902 = (int)*(((s1_ptr)_2)->base + _block_label_46777);
    }
    _2 = (int)SEQ_PTR(_24902);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _24903 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _24903 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _24902 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24903);
    *((int *)(_2+4)) = _24903;
    _24904 = MAKE_SEQ(_1);
    _24903 = NOVALUE;
    _24905 = EPrintf(-9999999, _24901, _24904);
    DeRefDS(_24904);
    _24904 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_NAME_11913))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_NAME_11913);
    _1 = *(int *)_2;
    *(int *)_2 = _24905;
    if( _1 != _24905 ){
        DeRef(_1);
    }
    _24905 = NOVALUE;
    _24899 = NOVALUE;
    goto L3; // [102] 185
L2: 

    /** 	elsif current_block then*/
    if (_66current_block_46672 == 0)
    {
        goto L4; // [109] 182
    }
    else{
    }

    /** 		SymTab[current_block][S_BLOCK] = last_block*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_66current_block_46672 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_BLOCK_11933))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_BLOCK_11933)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_BLOCK_11933);
    _1 = *(int *)_2;
    *(int *)_2 = _last_block_46779;
    DeRef(_1);
    _24906 = NOVALUE;

    /** 		sequence label_name = ""*/
    RefDS(_22682);
    DeRef(_label_name_46807);
    _label_name_46807 = _22682;

    /** 		if sequence(block_label) then*/
    _24908 = IS_SEQUENCE(_block_label_46777);
    if (_24908 == 0)
    {
        _24908 = NOVALUE;
        goto L5; // [141] 152
    }
    else{
        _24908 = NOVALUE;
    }

    /** 			label_name = block_label*/
    Ref(_block_label_46777);
    DeRefDS(_label_name_46807);
    _label_name_46807 = _block_label_46777;
L5: 

    /** 		SymTab[current_block][S_NAME] = sprintf( "BLOCK: %s-%s", {block_type_name(opcode), label_name})*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_66current_block_46672 + ((s1_ptr)_2)->base);
    _24912 = _66block_type_name(_opcode_46776);
    RefDS(_label_name_46807);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24912;
    ((int *)_2)[2] = _label_name_46807;
    _24913 = MAKE_SEQ(_1);
    _24912 = NOVALUE;
    _24914 = EPrintf(-9999999, _24911, _24913);
    DeRefDS(_24913);
    _24913 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_NAME_11913))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_NAME_11913);
    _1 = *(int *)_2;
    *(int *)_2 = _24914;
    if( _1 != _24914 ){
        DeRef(_1);
    }
    _24914 = NOVALUE;
    _24909 = NOVALUE;
L4: 
    DeRef(_label_name_46807);
    _label_name_46807 = NOVALUE;
L3: 

    /** 	ifdef BDEBUG then*/

    /** end procedure*/
    DeRef(_block_label_46777);
    return;
    ;
}


void _66block_label(int _label_name_46823)
{
    int _24928 = NOVALUE;
    int _24927 = NOVALUE;
    int _24926 = NOVALUE;
    int _24925 = NOVALUE;
    int _24924 = NOVALUE;
    int _24923 = NOVALUE;
    int _24921 = NOVALUE;
    int _24919 = NOVALUE;
    int _24918 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	block_stack[$][BLOCK_LABEL] = label_name*/
    if (IS_SEQUENCE(_66block_stack_46665)){
            _24918 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _24918 = 1;
    }
    _2 = (int)SEQ_PTR(_66block_stack_46665);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _66block_stack_46665 = MAKE_SEQ(_2);
    }
    _3 = (int)(_24918 + ((s1_ptr)_2)->base);
    RefDS(_label_name_46823);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _label_name_46823;
    DeRef(_1);
    _24919 = NOVALUE;

    /** 	SymTab[current_block][S_NAME] = sprintf( "BLOCK: %s-%s", */
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_66current_block_46672 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_66block_stack_46665)){
            _24923 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _24923 = 1;
    }
    _2 = (int)SEQ_PTR(_66block_stack_46665);
    _24924 = (int)*(((s1_ptr)_2)->base + _24923);
    _2 = (int)SEQ_PTR(_24924);
    _24925 = (int)*(((s1_ptr)_2)->base + 2);
    _24924 = NOVALUE;
    Ref(_24925);
    _24926 = _66block_type_name(_24925);
    _24925 = NOVALUE;
    RefDS(_label_name_46823);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24926;
    ((int *)_2)[2] = _label_name_46823;
    _24927 = MAKE_SEQ(_1);
    _24926 = NOVALUE;
    _24928 = EPrintf(-9999999, _24911, _24927);
    DeRefDS(_24927);
    _24927 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_NAME_11913))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_NAME_11913);
    _1 = *(int *)_2;
    *(int *)_2 = _24928;
    if( _1 != _24928 ){
        DeRef(_1);
    }
    _24928 = NOVALUE;
    _24921 = NOVALUE;

    /** end procedure*/
    DeRefDS(_label_name_46823);
    return;
    ;
}


int _66pop_block()
{
    int _block_46842 = NOVALUE;
    int _block_vars_46855 = NOVALUE;
    int _24957 = NOVALUE;
    int _24955 = NOVALUE;
    int _24954 = NOVALUE;
    int _24953 = NOVALUE;
    int _24952 = NOVALUE;
    int _24950 = NOVALUE;
    int _24949 = NOVALUE;
    int _24948 = NOVALUE;
    int _24947 = NOVALUE;
    int _24946 = NOVALUE;
    int _24945 = NOVALUE;
    int _24944 = NOVALUE;
    int _24943 = NOVALUE;
    int _24942 = NOVALUE;
    int _24941 = NOVALUE;
    int _24937 = NOVALUE;
    int _24936 = NOVALUE;
    int _24934 = NOVALUE;
    int _24933 = NOVALUE;
    int _24931 = NOVALUE;
    int _24929 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	if not length(block_stack) then*/
    if (IS_SEQUENCE(_66block_stack_46665)){
            _24929 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _24929 = 1;
    }
    if (_24929 != 0)
    goto L1; // [8] 18
    _24929 = NOVALUE;

    /** 		return 0*/
    DeRef(_block_46842);
    DeRef(_block_vars_46855);
    return 0;
L1: 

    /** 	sequence  block = block_stack[$]*/
    if (IS_SEQUENCE(_66block_stack_46665)){
            _24931 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _24931 = 1;
    }
    DeRef(_block_46842);
    _2 = (int)SEQ_PTR(_66block_stack_46665);
    _block_46842 = (int)*(((s1_ptr)_2)->base + _24931);
    Ref(_block_46842);

    /** 	block_stack = block_stack[1..$-1]*/
    if (IS_SEQUENCE(_66block_stack_46665)){
            _24933 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _24933 = 1;
    }
    _24934 = _24933 - 1;
    _24933 = NOVALUE;
    rhs_slice_target = (object_ptr)&_66block_stack_46665;
    RHS_Slice(_66block_stack_46665, 1, _24934);

    /** 	SymTab[block[BLOCK_SYM]][S_LAST_LINE] = gline_number*/
    _2 = (int)SEQ_PTR(_block_46842);
    _24936 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_24936))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_24936)->dbl));
    else
    _3 = (int)(_24936 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_LAST_LINE_11943))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_LAST_LINE_11943)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_LAST_LINE_11943);
    _1 = *(int *)_2;
    *(int *)_2 = _25gline_number_12267;
    DeRef(_1);
    _24937 = NOVALUE;

    /** 	ifdef BDEBUG then*/

    /** 	sequence block_vars = block[BLOCK_VARS]*/
    DeRef(_block_vars_46855);
    _2 = (int)SEQ_PTR(_block_46842);
    _block_vars_46855 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_block_vars_46855);

    /** 	for sx = 1 to length( block_vars ) do*/
    if (IS_SEQUENCE(_block_vars_46855)){
            _24941 = SEQ_PTR(_block_vars_46855)->length;
    }
    else {
        _24941 = 1;
    }
    {
        int _sx_46858;
        _sx_46858 = 1;
L2: 
        if (_sx_46858 > _24941){
            goto L3; // [87] 176
        }

        /** 		if SymTab[block_vars[sx]][S_MODE] = M_NORMAL */
        _2 = (int)SEQ_PTR(_block_vars_46855);
        _24942 = (int)*(((s1_ptr)_2)->base + _sx_46858);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!IS_ATOM_INT(_24942)){
            _24943 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_24942)->dbl));
        }
        else{
            _24943 = (int)*(((s1_ptr)_2)->base + _24942);
        }
        _2 = (int)SEQ_PTR(_24943);
        _24944 = (int)*(((s1_ptr)_2)->base + 3);
        _24943 = NOVALUE;
        if (IS_ATOM_INT(_24944)) {
            _24945 = (_24944 == 1);
        }
        else {
            _24945 = binary_op(EQUALS, _24944, 1);
        }
        _24944 = NOVALUE;
        if (IS_ATOM_INT(_24945)) {
            if (_24945 == 0) {
                goto L4; // [118] 169
            }
        }
        else {
            if (DBL_PTR(_24945)->dbl == 0.0) {
                goto L4; // [118] 169
            }
        }
        _2 = (int)SEQ_PTR(_block_vars_46855);
        _24947 = (int)*(((s1_ptr)_2)->base + _sx_46858);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!IS_ATOM_INT(_24947)){
            _24948 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_24947)->dbl));
        }
        else{
            _24948 = (int)*(((s1_ptr)_2)->base + _24947);
        }
        _2 = (int)SEQ_PTR(_24948);
        _24949 = (int)*(((s1_ptr)_2)->base + 4);
        _24948 = NOVALUE;
        if (IS_ATOM_INT(_24949)) {
            _24950 = (_24949 <= 5);
        }
        else {
            _24950 = binary_op(LESSEQ, _24949, 5);
        }
        _24949 = NOVALUE;
        if (_24950 == 0) {
            DeRef(_24950);
            _24950 = NOVALUE;
            goto L4; // [145] 169
        }
        else {
            if (!IS_ATOM_INT(_24950) && DBL_PTR(_24950)->dbl == 0.0){
                DeRef(_24950);
                _24950 = NOVALUE;
                goto L4; // [145] 169
            }
            DeRef(_24950);
            _24950 = NOVALUE;
        }
        DeRef(_24950);
        _24950 = NOVALUE;

        /** 			ifdef BDEBUG then*/

        /** 			Hide( block_vars[sx] )*/
        _2 = (int)SEQ_PTR(_block_vars_46855);
        _24952 = (int)*(((s1_ptr)_2)->base + _sx_46858);
        Ref(_24952);
        _52Hide(_24952);
        _24952 = NOVALUE;

        /** 			LintCheck( block_vars[sx] )*/
        _2 = (int)SEQ_PTR(_block_vars_46855);
        _24953 = (int)*(((s1_ptr)_2)->base + _sx_46858);
        Ref(_24953);
        _52LintCheck(_24953);
        _24953 = NOVALUE;
L4: 

        /** 	end for*/
        _sx_46858 = _sx_46858 + 1;
        goto L2; // [171] 94
L3: 
        ;
    }

    /** 	current_block = block_stack[$][BLOCK_SYM]*/
    if (IS_SEQUENCE(_66block_stack_46665)){
            _24954 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _24954 = 1;
    }
    _2 = (int)SEQ_PTR(_66block_stack_46665);
    _24955 = (int)*(((s1_ptr)_2)->base + _24954);
    _2 = (int)SEQ_PTR(_24955);
    _66current_block_46672 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_66current_block_46672)){
        _66current_block_46672 = (long)DBL_PTR(_66current_block_46672)->dbl;
    }
    _24955 = NOVALUE;

    /** 	return block[BLOCK_SYM]*/
    _2 = (int)SEQ_PTR(_block_46842);
    _24957 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_24957);
    DeRefDS(_block_46842);
    DeRef(_block_vars_46855);
    DeRef(_24934);
    _24934 = NOVALUE;
    _24936 = NOVALUE;
    _24942 = NOVALUE;
    _24947 = NOVALUE;
    DeRef(_24945);
    _24945 = NOVALUE;
    return _24957;
    ;
}


int _66top_block(int _offset_46887)
{
    int _24965 = NOVALUE;
    int _24964 = NOVALUE;
    int _24963 = NOVALUE;
    int _24962 = NOVALUE;
    int _24961 = NOVALUE;
    int _24960 = NOVALUE;
    int _24958 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_offset_46887)) {
        _1 = (long)(DBL_PTR(_offset_46887)->dbl);
        if (UNIQUE(DBL_PTR(_offset_46887)) && (DBL_PTR(_offset_46887)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_offset_46887);
        _offset_46887 = _1;
    }

    /** 	if offset >= length(block_stack) then*/
    if (IS_SEQUENCE(_66block_stack_46665)){
            _24958 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _24958 = 1;
    }
    if (_offset_46887 < _24958)
    goto L1; // [10] 33

    /** 		CompileErr(107, {offset,length(block_stack)})*/
    if (IS_SEQUENCE(_66block_stack_46665)){
            _24960 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _24960 = 1;
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _offset_46887;
    ((int *)_2)[2] = _24960;
    _24961 = MAKE_SEQ(_1);
    _24960 = NOVALUE;
    _43CompileErr(107, _24961, 0);
    _24961 = NOVALUE;
    goto L2; // [30] 59
L1: 

    /** 		return block_stack[$-offset][BLOCK_SYM]*/
    if (IS_SEQUENCE(_66block_stack_46665)){
            _24962 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _24962 = 1;
    }
    _24963 = _24962 - _offset_46887;
    _24962 = NOVALUE;
    _2 = (int)SEQ_PTR(_66block_stack_46665);
    _24964 = (int)*(((s1_ptr)_2)->base + _24963);
    _2 = (int)SEQ_PTR(_24964);
    _24965 = (int)*(((s1_ptr)_2)->base + 1);
    _24964 = NOVALUE;
    Ref(_24965);
    _24963 = NOVALUE;
    return _24965;
L2: 
    ;
}


void _66End_block(int _opcode_46901)
{
    int _ix_46912 = NOVALUE;
    int _24973 = NOVALUE;
    int _24970 = NOVALUE;
    int _24969 = NOVALUE;
    int _24968 = NOVALUE;
    int _24967 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_opcode_46901)) {
        _1 = (long)(DBL_PTR(_opcode_46901)->dbl);
        if (UNIQUE(DBL_PTR(_opcode_46901)) && (DBL_PTR(_opcode_46901)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_opcode_46901);
        _opcode_46901 = _1;
    }

    /** 	if opcode = FUNC then*/
    if (_opcode_46901 != 501)
    goto L1; // [7] 21

    /** 		opcode = PROC*/
    _opcode_46901 = 27;
L1: 

    /** 	check_block( opcode )*/
    _66check_block(_opcode_46901);

    /** 	if not length(block_stack[$][BLOCK_VARS]) then*/
    if (IS_SEQUENCE(_66block_stack_46665)){
            _24967 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _24967 = 1;
    }
    _2 = (int)SEQ_PTR(_66block_stack_46665);
    _24968 = (int)*(((s1_ptr)_2)->base + _24967);
    _2 = (int)SEQ_PTR(_24968);
    _24969 = (int)*(((s1_ptr)_2)->base + 6);
    _24968 = NOVALUE;
    if (IS_SEQUENCE(_24969)){
            _24970 = SEQ_PTR(_24969)->length;
    }
    else {
        _24970 = 1;
    }
    _24969 = NOVALUE;
    if (_24970 != 0)
    goto L2; // [46] 66
    _24970 = NOVALUE;

    /** 		integer ix = 1*/
    _ix_46912 = 1;

    /** 		ix = pop_block()*/
    _ix_46912 = _66pop_block();
    if (!IS_ATOM_INT(_ix_46912)) {
        _1 = (long)(DBL_PTR(_ix_46912)->dbl);
        if (UNIQUE(DBL_PTR(_ix_46912)) && (DBL_PTR(_ix_46912)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ix_46912);
        _ix_46912 = _1;
    }
    goto L3; // [63] 82
L2: 

    /** 		Push( pop_block() )*/
    _24973 = _66pop_block();
    _37Push(_24973);
    _24973 = NOVALUE;

    /** 		emit_op( EXIT_BLOCK )*/
    _37emit_op(206);
L3: 

    /** end procedure*/
    _24969 = NOVALUE;
    return;
    ;
}


int _66End_inline_block(int _opcode_46921)
{
    int _24980 = NOVALUE;
    int _24979 = NOVALUE;
    int _24978 = NOVALUE;
    int _24977 = NOVALUE;
    int _24976 = NOVALUE;
    int _24975 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_opcode_46921)) {
        _1 = (long)(DBL_PTR(_opcode_46921)->dbl);
        if (UNIQUE(DBL_PTR(_opcode_46921)) && (DBL_PTR(_opcode_46921)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_opcode_46921);
        _opcode_46921 = _1;
    }

    /** 	if opcode = FUNC then*/
    if (_opcode_46921 != 501)
    goto L1; // [7] 21

    /** 		opcode = PROC*/
    _opcode_46921 = 27;
L1: 

    /** 	if length(block_stack[$][BLOCK_VARS]) then*/
    if (IS_SEQUENCE(_66block_stack_46665)){
            _24975 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _24975 = 1;
    }
    _2 = (int)SEQ_PTR(_66block_stack_46665);
    _24976 = (int)*(((s1_ptr)_2)->base + _24975);
    _2 = (int)SEQ_PTR(_24976);
    _24977 = (int)*(((s1_ptr)_2)->base + 6);
    _24976 = NOVALUE;
    if (IS_SEQUENCE(_24977)){
            _24978 = SEQ_PTR(_24977)->length;
    }
    else {
        _24978 = 1;
    }
    _24977 = NOVALUE;
    if (_24978 == 0)
    {
        _24978 = NOVALUE;
        goto L2; // [41] 62
    }
    else{
        _24978 = NOVALUE;
    }

    /** 		return { EXIT_BLOCK, pop_block() }*/
    _24979 = _66pop_block();
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 206;
    ((int *)_2)[2] = _24979;
    _24980 = MAKE_SEQ(_1);
    _24979 = NOVALUE;
    _24977 = NOVALUE;
    return _24980;
    goto L3; // [59] 74
L2: 

    /** 		Drop_block( opcode )*/
    _66Drop_block(_opcode_46921);

    /** 		return {}*/
    RefDS(_22682);
    _24977 = NOVALUE;
    DeRef(_24980);
    _24980 = NOVALUE;
    return _22682;
L3: 
    ;
}


void _66Sibling_block(int _opcode_46938)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_opcode_46938)) {
        _1 = (long)(DBL_PTR(_opcode_46938)->dbl);
        if (UNIQUE(DBL_PTR(_opcode_46938)) && (DBL_PTR(_opcode_46938)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_opcode_46938);
        _opcode_46938 = _1;
    }

    /** 	End_block( opcode )*/
    _66End_block(_opcode_46938);

    /** 	Start_block( opcode )*/
    _66Start_block(_opcode_46938, 0);

    /** end procedure*/
    return;
    ;
}


void _66Leave_block(int _offset_46941)
{
    int _24986 = NOVALUE;
    int _24985 = NOVALUE;
    int _24984 = NOVALUE;
    int _24983 = NOVALUE;
    int _24982 = NOVALUE;
    int _24981 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_offset_46941)) {
        _1 = (long)(DBL_PTR(_offset_46941)->dbl);
        if (UNIQUE(DBL_PTR(_offset_46941)) && (DBL_PTR(_offset_46941)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_offset_46941);
        _offset_46941 = _1;
    }

    /** 	if length( block_stack[$-offset][BLOCK_VARS]) then*/
    if (IS_SEQUENCE(_66block_stack_46665)){
            _24981 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _24981 = 1;
    }
    _24982 = _24981 - _offset_46941;
    _24981 = NOVALUE;
    _2 = (int)SEQ_PTR(_66block_stack_46665);
    _24983 = (int)*(((s1_ptr)_2)->base + _24982);
    _2 = (int)SEQ_PTR(_24983);
    _24984 = (int)*(((s1_ptr)_2)->base + 6);
    _24983 = NOVALUE;
    if (IS_SEQUENCE(_24984)){
            _24985 = SEQ_PTR(_24984)->length;
    }
    else {
        _24985 = 1;
    }
    _24984 = NOVALUE;
    if (_24985 == 0)
    {
        _24985 = NOVALUE;
        goto L1; // [27] 47
    }
    else{
        _24985 = NOVALUE;
    }

    /** 		Push( top_block( offset ) )*/
    _24986 = _66top_block(_offset_46941);
    _37Push(_24986);
    _24986 = NOVALUE;

    /** 		emit_op( EXIT_BLOCK )*/
    _37emit_op(206);
L1: 

    /** end procedure*/
    DeRef(_24982);
    _24982 = NOVALUE;
    _24984 = NOVALUE;
    return;
    ;
}


void _66Leave_blocks(int _blocks_46961, int _block_type_46962)
{
    int _bx_46963 = NOVALUE;
    int _Block_opcode_3__tmp_at29_46970 = NOVALUE;
    int _Block_opcode_2__tmp_at29_46969 = NOVALUE;
    int _Block_opcode_1__tmp_at29_46968 = NOVALUE;
    int _Block_opcode_inlined_Block_opcode_at_29_46967 = NOVALUE;
    int _24999 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_blocks_46961)) {
        _1 = (long)(DBL_PTR(_blocks_46961)->dbl);
        if (UNIQUE(DBL_PTR(_blocks_46961)) && (DBL_PTR(_blocks_46961)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_blocks_46961);
        _blocks_46961 = _1;
    }
    if (!IS_ATOM_INT(_block_type_46962)) {
        _1 = (long)(DBL_PTR(_block_type_46962)->dbl);
        if (UNIQUE(DBL_PTR(_block_type_46962)) && (DBL_PTR(_block_type_46962)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_block_type_46962);
        _block_type_46962 = _1;
    }

    /** 	integer bx = 0*/
    _bx_46963 = 0;

    /** 	while blocks do*/
L1: 
    if (_blocks_46961 == 0)
    {
        goto L2; // [15] 125
    }
    else{
    }

    /** 		Leave_block( bx )*/
    _66Leave_block(_bx_46963);

    /** 		if block_type then*/
    if (_block_type_46962 == 0)
    {
        goto L3; // [25] 107
    }
    else{
    }

    /** 			switch Block_opcode( bx ) do*/

    /** 	return block_stack[$-bx][BLOCK_OPCODE]*/
    if (IS_SEQUENCE(_66block_stack_46665)){
            _Block_opcode_1__tmp_at29_46968 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _Block_opcode_1__tmp_at29_46968 = 1;
    }
    _Block_opcode_2__tmp_at29_46969 = _Block_opcode_1__tmp_at29_46968 - _bx_46963;
    DeRef(_Block_opcode_3__tmp_at29_46970);
    _2 = (int)SEQ_PTR(_66block_stack_46665);
    _Block_opcode_3__tmp_at29_46970 = (int)*(((s1_ptr)_2)->base + _Block_opcode_2__tmp_at29_46969);
    Ref(_Block_opcode_3__tmp_at29_46970);
    DeRef(_Block_opcode_inlined_Block_opcode_at_29_46967);
    _2 = (int)SEQ_PTR(_Block_opcode_3__tmp_at29_46970);
    _Block_opcode_inlined_Block_opcode_at_29_46967 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_Block_opcode_inlined_Block_opcode_at_29_46967);
    DeRef(_Block_opcode_3__tmp_at29_46970);
    _Block_opcode_3__tmp_at29_46970 = NOVALUE;
    if (IS_SEQUENCE(_Block_opcode_inlined_Block_opcode_at_29_46967) ){
        goto L4; // [54] 86
    }
    if(!IS_ATOM_INT(_Block_opcode_inlined_Block_opcode_at_29_46967)){
        if( (DBL_PTR(_Block_opcode_inlined_Block_opcode_at_29_46967)->dbl != (double) ((int) DBL_PTR(_Block_opcode_inlined_Block_opcode_at_29_46967)->dbl) ) ){
            goto L4; // [54] 86
        }
        _0 = (int) DBL_PTR(_Block_opcode_inlined_Block_opcode_at_29_46967)->dbl;
    }
    else {
        _0 = _Block_opcode_inlined_Block_opcode_at_29_46967;
    };
    switch ( _0 ){ 

        /** 				case FOR, WHILE, LOOP then*/
        case 21:
        case 47:
        case 422:

        /** 					if block_type = LOOP_BLOCK then*/
        if (_block_type_46962 != 1)
        goto L5; // [71] 114

        /** 						blocks -= 1*/
        _blocks_46961 = _blocks_46961 - 1;
        goto L5; // [82] 114

        /** 				case else*/
        default:
L4: 

        /** 					if block_type = CONDITIONAL_BLOCK then*/
        if (_block_type_46962 != 2)
        goto L6; // [92] 103

        /** 						blocks -= 1*/
        _blocks_46961 = _blocks_46961 - 1;
L6: 
    ;}    goto L5; // [104] 114
L3: 

    /** 			blocks -= 1*/
    _blocks_46961 = _blocks_46961 - 1;
L5: 

    /** 		bx += 1*/
    _bx_46963 = _bx_46963 + 1;

    /** 	end while*/
    goto L1; // [122] 15
L2: 

    /** 	for i = 0 to blocks - 1 do*/
    _24999 = _blocks_46961 - 1;
    if ((long)((unsigned long)_24999 +(unsigned long) HIGH_BITS) >= 0){
        _24999 = NewDouble((double)_24999);
    }
    {
        int _i_46988;
        _i_46988 = 0;
L7: 
        if (binary_op_a(GREATER, _i_46988, _24999)){
            goto L8; // [131] 150
        }

        /** 		Leave_block( i )*/
        Ref(_i_46988);
        _66Leave_block(_i_46988);

        /** 	end for*/
        _0 = _i_46988;
        if (IS_ATOM_INT(_i_46988)) {
            _i_46988 = _i_46988 + 1;
            if ((long)((unsigned long)_i_46988 +(unsigned long) HIGH_BITS) >= 0){
                _i_46988 = NewDouble((double)_i_46988);
            }
        }
        else {
            _i_46988 = binary_op_a(PLUS, _i_46988, 1);
        }
        DeRef(_0);
        goto L7; // [145] 138
L8: 
        ;
        DeRef(_i_46988);
    }

    /** end procedure*/
    DeRef(_24999);
    _24999 = NOVALUE;
    return;
    ;
}


void _66Drop_block(int _opcode_46992)
{
    int _x_46994 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_opcode_46992)) {
        _1 = (long)(DBL_PTR(_opcode_46992)->dbl);
        if (UNIQUE(DBL_PTR(_opcode_46992)) && (DBL_PTR(_opcode_46992)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_opcode_46992);
        _opcode_46992 = _1;
    }

    /** 	check_block( opcode )*/
    _66check_block(_opcode_46992);

    /** 	symtab_index x = pop_block()*/
    _x_46994 = _66pop_block();
    if (!IS_ATOM_INT(_x_46994)) {
        _1 = (long)(DBL_PTR(_x_46994)->dbl);
        if (UNIQUE(DBL_PTR(_x_46994)) && (DBL_PTR(_x_46994)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_46994);
        _x_46994 = _1;
    }

    /** end procedure*/
    return;
    ;
}


void _66Pop_block_var()
{
    int _sym_46999 = NOVALUE;
    int _block_sym_47006 = NOVALUE;
    int _25027 = NOVALUE;
    int _25026 = NOVALUE;
    int _25025 = NOVALUE;
    int _25024 = NOVALUE;
    int _25023 = NOVALUE;
    int _25022 = NOVALUE;
    int _25021 = NOVALUE;
    int _25020 = NOVALUE;
    int _25018 = NOVALUE;
    int _25017 = NOVALUE;
    int _25015 = NOVALUE;
    int _25014 = NOVALUE;
    int _25012 = NOVALUE;
    int _25009 = NOVALUE;
    int _25007 = NOVALUE;
    int _25006 = NOVALUE;
    int _25004 = NOVALUE;
    int _25003 = NOVALUE;
    int _25002 = NOVALUE;
    int _25001 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	symtab_index sym = block_stack[$][BLOCK_VARS][$]*/
    if (IS_SEQUENCE(_66block_stack_46665)){
            _25001 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _25001 = 1;
    }
    _2 = (int)SEQ_PTR(_66block_stack_46665);
    _25002 = (int)*(((s1_ptr)_2)->base + _25001);
    _2 = (int)SEQ_PTR(_25002);
    _25003 = (int)*(((s1_ptr)_2)->base + 6);
    _25002 = NOVALUE;
    if (IS_SEQUENCE(_25003)){
            _25004 = SEQ_PTR(_25003)->length;
    }
    else {
        _25004 = 1;
    }
    _2 = (int)SEQ_PTR(_25003);
    _sym_46999 = (int)*(((s1_ptr)_2)->base + _25004);
    if (!IS_ATOM_INT(_sym_46999)){
        _sym_46999 = (long)DBL_PTR(_sym_46999)->dbl;
    }
    _25003 = NOVALUE;

    /** 	symtab_index block_sym = block_stack[$][BLOCK_SYM]*/
    if (IS_SEQUENCE(_66block_stack_46665)){
            _25006 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _25006 = 1;
    }
    _2 = (int)SEQ_PTR(_66block_stack_46665);
    _25007 = (int)*(((s1_ptr)_2)->base + _25006);
    _2 = (int)SEQ_PTR(_25007);
    _block_sym_47006 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_block_sym_47006)){
        _block_sym_47006 = (long)DBL_PTR(_block_sym_47006)->dbl;
    }
    _25007 = NOVALUE;

    /** 	while sym_next_in_block( block_sym ) != sym do*/
L1: 
    _25009 = _52sym_next_in_block(_block_sym_47006);
    if (binary_op_a(EQUALS, _25009, _sym_46999)){
        DeRef(_25009);
        _25009 = NOVALUE;
        goto L2; // [55] 72
    }
    DeRef(_25009);
    _25009 = NOVALUE;

    /** 		block_sym = sym_next_in_block( block_sym )*/
    _block_sym_47006 = _52sym_next_in_block(_block_sym_47006);
    if (!IS_ATOM_INT(_block_sym_47006)) {
        _1 = (long)(DBL_PTR(_block_sym_47006)->dbl);
        if (UNIQUE(DBL_PTR(_block_sym_47006)) && (DBL_PTR(_block_sym_47006)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_block_sym_47006);
        _block_sym_47006 = _1;
    }

    /** 	end while*/
    goto L1; // [69] 51
L2: 

    /** 	SymTab[block_sym][S_NEXT_IN_BLOCK] = sym_next_in_block( sym )*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_block_sym_47006 + ((s1_ptr)_2)->base);
    _25014 = _52sym_next_in_block(_sym_46999);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_NEXT_IN_BLOCK_11905))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NEXT_IN_BLOCK_11905)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_NEXT_IN_BLOCK_11905);
    _1 = *(int *)_2;
    *(int *)_2 = _25014;
    if( _1 != _25014 ){
        DeRef(_1);
    }
    _25014 = NOVALUE;
    _25012 = NOVALUE;

    /** 	SymTab[sym][S_NEXT_IN_BLOCK] = 0*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_46999 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_NEXT_IN_BLOCK_11905))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NEXT_IN_BLOCK_11905)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_NEXT_IN_BLOCK_11905);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _25015 = NOVALUE;

    /** 	block_stack[$][BLOCK_VARS] = eu:remove( block_stack[$][BLOCK_VARS], */
    if (IS_SEQUENCE(_66block_stack_46665)){
            _25017 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _25017 = 1;
    }
    _2 = (int)SEQ_PTR(_66block_stack_46665);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _66block_stack_46665 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25017 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_66block_stack_46665)){
            _25020 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _25020 = 1;
    }
    _2 = (int)SEQ_PTR(_66block_stack_46665);
    _25021 = (int)*(((s1_ptr)_2)->base + _25020);
    _2 = (int)SEQ_PTR(_25021);
    _25022 = (int)*(((s1_ptr)_2)->base + 6);
    _25021 = NOVALUE;
    if (IS_SEQUENCE(_66block_stack_46665)){
            _25023 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _25023 = 1;
    }
    _2 = (int)SEQ_PTR(_66block_stack_46665);
    _25024 = (int)*(((s1_ptr)_2)->base + _25023);
    _2 = (int)SEQ_PTR(_25024);
    _25025 = (int)*(((s1_ptr)_2)->base + 6);
    _25024 = NOVALUE;
    if (IS_SEQUENCE(_25025)){
            _25026 = SEQ_PTR(_25025)->length;
    }
    else {
        _25026 = 1;
    }
    _25025 = NOVALUE;
    {
        s1_ptr assign_space = SEQ_PTR(_25022);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_25026)) ? _25026 : (long)(DBL_PTR(_25026)->dbl);
        int stop = (IS_ATOM_INT(_25026)) ? _25026 : (long)(DBL_PTR(_25026)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
            RefDS(_25022);
            DeRef(_25027);
            _25027 = _25022;
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_25022), start, &_25027 );
            }
            else Tail(SEQ_PTR(_25022), stop+1, &_25027);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_25022), start, &_25027);
        }
        else {
            assign_slice_seq = &assign_space;
            _1 = Remove_elements(start, stop, 0);
            DeRef(_25027);
            _25027 = _1;
        }
    }
    _25022 = NOVALUE;
    _25026 = NOVALUE;
    _25026 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _25027;
    if( _1 != _25027 ){
        DeRef(_1);
    }
    _25027 = NOVALUE;
    _25018 = NOVALUE;

    /** end procedure*/
    _25025 = NOVALUE;
    return;
    ;
}


void _66Goto_block(int _from_block_47040, int _to_block_47042, int _pc_47043)
{
    int _code_47044 = NOVALUE;
    int _next_block_47046 = NOVALUE;
    int _25038 = NOVALUE;
    int _25035 = NOVALUE;
    int _25034 = NOVALUE;
    int _25033 = NOVALUE;
    int _25032 = NOVALUE;
    int _25031 = NOVALUE;
    int _25030 = NOVALUE;
    int _25029 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_block_47040)) {
        _1 = (long)(DBL_PTR(_from_block_47040)->dbl);
        if (UNIQUE(DBL_PTR(_from_block_47040)) && (DBL_PTR(_from_block_47040)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_block_47040);
        _from_block_47040 = _1;
    }
    if (!IS_ATOM_INT(_to_block_47042)) {
        _1 = (long)(DBL_PTR(_to_block_47042)->dbl);
        if (UNIQUE(DBL_PTR(_to_block_47042)) && (DBL_PTR(_to_block_47042)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_to_block_47042);
        _to_block_47042 = _1;
    }
    if (!IS_ATOM_INT(_pc_47043)) {
        _1 = (long)(DBL_PTR(_pc_47043)->dbl);
        if (UNIQUE(DBL_PTR(_pc_47043)) && (DBL_PTR(_pc_47043)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pc_47043);
        _pc_47043 = _1;
    }

    /** 	sequence code = {}*/
    RefDS(_22682);
    DeRefi(_code_47044);
    _code_47044 = _22682;

    /** 	symtab_index next_block = sym_block( from_block )*/
    _next_block_47046 = _52sym_block(_from_block_47040);
    if (!IS_ATOM_INT(_next_block_47046)) {
        _1 = (long)(DBL_PTR(_next_block_47046)->dbl);
        if (UNIQUE(DBL_PTR(_next_block_47046)) && (DBL_PTR(_next_block_47046)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_next_block_47046);
        _next_block_47046 = _1;
    }

    /** 	while next_block */
L1: 
    if (_next_block_47046 == 0) {
        _25029 = 0;
        goto L2; // [27] 39
    }
    _25030 = (_from_block_47040 != _to_block_47042);
    _25029 = (_25030 != 0);
L2: 
    if (_25029 == 0) {
        goto L3; // [39] 93
    }
    _25032 = _52sym_token(_next_block_47046);
    _25033 = find_from(_25032, _28RTN_TOKS_11857, 1);
    DeRef(_25032);
    _25032 = NOVALUE;
    _25034 = (_25033 == 0);
    _25033 = NOVALUE;
    if (_25034 == 0)
    {
        DeRef(_25034);
        _25034 = NOVALUE;
        goto L3; // [58] 93
    }
    else{
        DeRef(_25034);
        _25034 = NOVALUE;
    }

    /** 		code &= { EXIT_BLOCK, from_block }*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 206;
    ((int *)_2)[2] = _from_block_47040;
    _25035 = MAKE_SEQ(_1);
    Concat((object_ptr)&_code_47044, _code_47044, _25035);
    DeRefDS(_25035);
    _25035 = NOVALUE;

    /** 		from_block = next_block*/
    _from_block_47040 = _next_block_47046;

    /** 		next_block = sym_block( next_block )*/
    _next_block_47046 = _52sym_block(_next_block_47046);
    if (!IS_ATOM_INT(_next_block_47046)) {
        _1 = (long)(DBL_PTR(_next_block_47046)->dbl);
        if (UNIQUE(DBL_PTR(_next_block_47046)) && (DBL_PTR(_next_block_47046)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_next_block_47046);
        _next_block_47046 = _1;
    }

    /** 	end while*/
    goto L1; // [90] 27
L3: 

    /** 	if length(code) then*/
    if (IS_SEQUENCE(_code_47044)){
            _25038 = SEQ_PTR(_code_47044)->length;
    }
    else {
        _25038 = 1;
    }
    if (_25038 == 0)
    {
        _25038 = NOVALUE;
        goto L4; // [98] 127
    }
    else{
        _25038 = NOVALUE;
    }

    /** 		if pc then*/
    if (_pc_47043 == 0)
    {
        goto L5; // [103] 115
    }
    else{
    }

    /** 			insert_code( code, pc )*/
    RefDS(_code_47044);
    _65insert_code(_code_47044, _pc_47043);
    goto L6; // [112] 126
L5: 

    /** 			Code &= code*/
    Concat((object_ptr)&_25Code_12355, _25Code_12355, _code_47044);
L6: 
L4: 

    /** end procedure*/
    DeRefi(_code_47044);
    DeRef(_25030);
    _25030 = NOVALUE;
    return;
    ;
}


void _66blocks_info()
{
    int _0, _1, _2;
    

    /** 	? block_stack*/
    StdPrint(1, _66block_stack_46665, 1);

    /** end procedure*/
    return;
    ;
}


int _66Least_block()
{
    int _ix_47074 = NOVALUE;
    int _sub_block_47077 = NOVALUE;
    int _25052 = NOVALUE;
    int _25051 = NOVALUE;
    int _25049 = NOVALUE;
    int _25048 = NOVALUE;
    int _25047 = NOVALUE;
    int _25046 = NOVALUE;
    int _25045 = NOVALUE;
    int _25044 = NOVALUE;
    int _25043 = NOVALUE;
    int _25042 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer ix = length( block_stack )*/
    if (IS_SEQUENCE(_66block_stack_46665)){
            _ix_47074 = SEQ_PTR(_66block_stack_46665)->length;
    }
    else {
        _ix_47074 = 1;
    }

    /** 	symtab_index sub_block = sym_block( CurrentSub )*/
    _sub_block_47077 = _52sym_block(_25CurrentSub_12270);
    if (!IS_ATOM_INT(_sub_block_47077)) {
        _1 = (long)(DBL_PTR(_sub_block_47077)->dbl);
        if (UNIQUE(DBL_PTR(_sub_block_47077)) && (DBL_PTR(_sub_block_47077)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sub_block_47077);
        _sub_block_47077 = _1;
    }

    /** 	while not length( block_stack[ix][BLOCK_VARS] ) */
L1: 
    _2 = (int)SEQ_PTR(_66block_stack_46665);
    _25042 = (int)*(((s1_ptr)_2)->base + _ix_47074);
    _2 = (int)SEQ_PTR(_25042);
    _25043 = (int)*(((s1_ptr)_2)->base + 6);
    _25042 = NOVALUE;
    if (IS_SEQUENCE(_25043)){
            _25044 = SEQ_PTR(_25043)->length;
    }
    else {
        _25044 = 1;
    }
    _25043 = NOVALUE;
    _25045 = (_25044 == 0);
    _25044 = NOVALUE;
    if (_25045 == 0) {
        goto L2; // [41] 76
    }
    _2 = (int)SEQ_PTR(_66block_stack_46665);
    _25047 = (int)*(((s1_ptr)_2)->base + _ix_47074);
    _2 = (int)SEQ_PTR(_25047);
    _25048 = (int)*(((s1_ptr)_2)->base + 1);
    _25047 = NOVALUE;
    if (IS_ATOM_INT(_25048)) {
        _25049 = (_25048 != _sub_block_47077);
    }
    else {
        _25049 = binary_op(NOTEQ, _25048, _sub_block_47077);
    }
    _25048 = NOVALUE;
    if (_25049 <= 0) {
        if (_25049 == 0) {
            DeRef(_25049);
            _25049 = NOVALUE;
            goto L2; // [62] 76
        }
        else {
            if (!IS_ATOM_INT(_25049) && DBL_PTR(_25049)->dbl == 0.0){
                DeRef(_25049);
                _25049 = NOVALUE;
                goto L2; // [62] 76
            }
            DeRef(_25049);
            _25049 = NOVALUE;
        }
    }
    DeRef(_25049);
    _25049 = NOVALUE;

    /** 		ix -= 1	*/
    _ix_47074 = _ix_47074 - 1;

    /** 	end while*/
    goto L1; // [73] 23
L2: 

    /** 	return block_stack[ix][BLOCK_SYM]*/
    _2 = (int)SEQ_PTR(_66block_stack_46665);
    _25051 = (int)*(((s1_ptr)_2)->base + _ix_47074);
    _2 = (int)SEQ_PTR(_25051);
    _25052 = (int)*(((s1_ptr)_2)->base + 1);
    _25051 = NOVALUE;
    Ref(_25052);
    _25043 = NOVALUE;
    DeRef(_25045);
    _25045 = NOVALUE;
    return _25052;
    ;
}



// 0x7A6B7D1B
