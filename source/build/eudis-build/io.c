// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _16get_bytes(int _fn_2298, int _n_2299)
{
    int _s_2300 = NOVALUE;
    int _c_2301 = NOVALUE;
    int _first_2302 = NOVALUE;
    int _last_2303 = NOVALUE;
    int _1014 = NOVALUE;
    int _1011 = NOVALUE;
    int _1009 = NOVALUE;
    int _1008 = NOVALUE;
    int _1007 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_n_2299)) {
        _1 = (long)(DBL_PTR(_n_2299)->dbl);
        if (UNIQUE(DBL_PTR(_n_2299)) && (DBL_PTR(_n_2299)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_n_2299);
        _n_2299 = _1;
    }

    /** 	if n = 0 then*/
    if (_n_2299 != 0)
    goto L1; // [7] 18

    /** 		return {}*/
    RefDS(_5);
    DeRefi(_s_2300);
    return _5;
L1: 

    /** 	c = getc(fn)*/
    if (_fn_2298 != last_r_file_no) {
        last_r_file_ptr = which_file(_fn_2298, EF_READ);
        last_r_file_no = _fn_2298;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_2301 = getKBchar();
        }
        else
        _c_2301 = getc(last_r_file_ptr);
    }
    else
    _c_2301 = getc(last_r_file_ptr);

    /** 	if c = EOF then*/
    if (_c_2301 != -1)
    goto L2; // [25] 36

    /** 		return {}*/
    RefDS(_5);
    DeRefi(_s_2300);
    return _5;
L2: 

    /** 	s = repeat(c, n)*/
    DeRefi(_s_2300);
    _s_2300 = Repeat(_c_2301, _n_2299);

    /** 	last = 1*/
    _last_2303 = 1;

    /** 	while last < n do*/
L3: 
    if (_last_2303 >= _n_2299)
    goto L4; // [52] 159

    /** 		first = last+1*/
    _first_2302 = _last_2303 + 1;

    /** 		last  = last+CHUNK*/
    _last_2303 = _last_2303 + 100;

    /** 		if last > n then*/
    if (_last_2303 <= _n_2299)
    goto L5; // [70] 80

    /** 			last = n*/
    _last_2303 = _n_2299;
L5: 

    /** 		for i = first to last do*/
    _1007 = _last_2303;
    {
        int _i_2317;
        _i_2317 = _first_2302;
L6: 
        if (_i_2317 > _1007){
            goto L7; // [85] 108
        }

        /** 			s[i] = getc(fn)*/
        if (_fn_2298 != last_r_file_no) {
            last_r_file_ptr = which_file(_fn_2298, EF_READ);
            last_r_file_no = _fn_2298;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _1008 = getKBchar();
            }
            else
            _1008 = getc(last_r_file_ptr);
        }
        else
        _1008 = getc(last_r_file_ptr);
        _2 = (int)SEQ_PTR(_s_2300);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_2300 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_2317);
        *(int *)_2 = _1008;
        if( _1 != _1008 ){
        }
        _1008 = NOVALUE;

        /** 		end for*/
        _i_2317 = _i_2317 + 1;
        goto L6; // [103] 92
L7: 
        ;
    }

    /** 		if s[last] = EOF then*/
    _2 = (int)SEQ_PTR(_s_2300);
    _1009 = (int)*(((s1_ptr)_2)->base + _last_2303);
    if (_1009 != -1)
    goto L3; // [114] 52

    /** 			while s[last] = EOF do*/
L8: 
    _2 = (int)SEQ_PTR(_s_2300);
    _1011 = (int)*(((s1_ptr)_2)->base + _last_2303);
    if (_1011 != -1)
    goto L9; // [127] 142

    /** 				last -= 1*/
    _last_2303 = _last_2303 - 1;

    /** 			end while*/
    goto L8; // [139] 123
L9: 

    /** 			return s[1..last]*/
    rhs_slice_target = (object_ptr)&_1014;
    RHS_Slice(_s_2300, 1, _last_2303);
    DeRefDSi(_s_2300);
    _1009 = NOVALUE;
    _1011 = NOVALUE;
    return _1014;

    /** 	end while*/
    goto L3; // [156] 52
L4: 

    /** 	return s*/
    _1009 = NOVALUE;
    _1011 = NOVALUE;
    DeRef(_1014);
    _1014 = NOVALUE;
    return _s_2300;
    ;
}


int _16get_integer32(int _fh_2338)
{
    int _1023 = NOVALUE;
    int _1022 = NOVALUE;
    int _1021 = NOVALUE;
    int _1020 = NOVALUE;
    int _1019 = NOVALUE;
    int _0, _1, _2;
    

    /** 	poke(mem0, getc(fh))*/
    if (_fh_2338 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_2338, EF_READ);
        last_r_file_no = _fh_2338;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _1019 = getKBchar();
        }
        else
        _1019 = getc(last_r_file_ptr);
    }
    else
    _1019 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_16mem0_2328)){
        poke_addr = (unsigned char *)_16mem0_2328;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_16mem0_2328)->dbl);
    }
    *poke_addr = (unsigned char)_1019;
    _1019 = NOVALUE;

    /** 	poke(mem1, getc(fh))*/
    if (_fh_2338 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_2338, EF_READ);
        last_r_file_no = _fh_2338;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _1020 = getKBchar();
        }
        else
        _1020 = getc(last_r_file_ptr);
    }
    else
    _1020 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_16mem1_2329)){
        poke_addr = (unsigned char *)_16mem1_2329;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_16mem1_2329)->dbl);
    }
    *poke_addr = (unsigned char)_1020;
    _1020 = NOVALUE;

    /** 	poke(mem2, getc(fh))*/
    if (_fh_2338 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_2338, EF_READ);
        last_r_file_no = _fh_2338;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _1021 = getKBchar();
        }
        else
        _1021 = getc(last_r_file_ptr);
    }
    else
    _1021 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_16mem2_2330)){
        poke_addr = (unsigned char *)_16mem2_2330;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_16mem2_2330)->dbl);
    }
    *poke_addr = (unsigned char)_1021;
    _1021 = NOVALUE;

    /** 	poke(mem3, getc(fh))*/
    if (_fh_2338 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_2338, EF_READ);
        last_r_file_no = _fh_2338;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _1022 = getKBchar();
        }
        else
        _1022 = getc(last_r_file_ptr);
    }
    else
    _1022 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_16mem3_2331)){
        poke_addr = (unsigned char *)_16mem3_2331;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_16mem3_2331)->dbl);
    }
    *poke_addr = (unsigned char)_1022;
    _1022 = NOVALUE;

    /** 	return peek4u(mem0)*/
    if (IS_ATOM_INT(_16mem0_2328)) {
        _1023 = *(unsigned long *)_16mem0_2328;
        if ((unsigned)_1023 > (unsigned)MAXINT)
        _1023 = NewDouble((double)(unsigned long)_1023);
    }
    else {
        _1023 = *(unsigned long *)(unsigned long)(DBL_PTR(_16mem0_2328)->dbl);
        if ((unsigned)_1023 > (unsigned)MAXINT)
        _1023 = NewDouble((double)(unsigned long)_1023);
    }
    return _1023;
    ;
}


int _16get_integer16(int _fh_2346)
{
    int _1026 = NOVALUE;
    int _1025 = NOVALUE;
    int _1024 = NOVALUE;
    int _0, _1, _2;
    

    /** 	poke(mem0, getc(fh))*/
    if (_fh_2346 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_2346, EF_READ);
        last_r_file_no = _fh_2346;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _1024 = getKBchar();
        }
        else
        _1024 = getc(last_r_file_ptr);
    }
    else
    _1024 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_16mem0_2328)){
        poke_addr = (unsigned char *)_16mem0_2328;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_16mem0_2328)->dbl);
    }
    *poke_addr = (unsigned char)_1024;
    _1024 = NOVALUE;

    /** 	poke(mem1, getc(fh))*/
    if (_fh_2346 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_2346, EF_READ);
        last_r_file_no = _fh_2346;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _1025 = getKBchar();
        }
        else
        _1025 = getc(last_r_file_ptr);
    }
    else
    _1025 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_16mem1_2329)){
        poke_addr = (unsigned char *)_16mem1_2329;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_16mem1_2329)->dbl);
    }
    *poke_addr = (unsigned char)_1025;
    _1025 = NOVALUE;

    /** 	return peek2u(mem0)*/
    if (IS_ATOM_INT(_16mem0_2328)) {
        _1026 = *(unsigned short *)_16mem0_2328;
    }
    else {
        _1026 = *(unsigned short *)(unsigned long)(DBL_PTR(_16mem0_2328)->dbl);
    }
    return _1026;
    ;
}


int _16seek(int _fn_2440, int _pos_2441)
{
    int _1073 = NOVALUE;
    int _1072 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_2441);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _fn_2440;
    ((int *)_2)[2] = _pos_2441;
    _1072 = MAKE_SEQ(_1);
    _1073 = machine(19, _1072);
    DeRefDS(_1072);
    _1072 = NOVALUE;
    DeRef(_pos_2441);
    return _1073;
    ;
}


int _16where(int _fn_2446)
{
    int _1074 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_WHERE, fn)*/
    _1074 = machine(20, _fn_2446);
    return _1074;
    ;
}


int _16read_lines(int _file_2465)
{
    int _fn_2466 = NOVALUE;
    int _ret_2467 = NOVALUE;
    int _y_2468 = NOVALUE;
    int _1098 = NOVALUE;
    int _1097 = NOVALUE;
    int _1096 = NOVALUE;
    int _1095 = NOVALUE;
    int _1089 = NOVALUE;
    int _1088 = NOVALUE;
    int _1086 = NOVALUE;
    int _1085 = NOVALUE;
    int _1084 = NOVALUE;
    int _1079 = NOVALUE;
    int _1078 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(file) then*/
    _1078 = 1;
    if (_1078 == 0)
    {
        _1078 = NOVALUE;
        goto L1; // [6] 37
    }
    else{
        _1078 = NOVALUE;
    }

    /** 		if length(file) = 0 then*/
    if (IS_SEQUENCE(_file_2465)){
            _1079 = SEQ_PTR(_file_2465)->length;
    }
    else {
        _1079 = 1;
    }
    if (_1079 != 0)
    goto L2; // [14] 26

    /** 			fn = 0*/
    DeRef(_fn_2466);
    _fn_2466 = 0;
    goto L3; // [23] 43
L2: 

    /** 			fn = open(file, "r")*/
    DeRef(_fn_2466);
    _fn_2466 = EOpen(_file_2465, _1081, 0);
    goto L3; // [34] 43
L1: 

    /** 		fn = file*/
    Ref(_file_2465);
    DeRef(_fn_2466);
    _fn_2466 = _file_2465;
L3: 

    /** 	if fn < 0 then return -1 end if*/
    if (binary_op_a(GREATEREQ, _fn_2466, 0)){
        goto L4; // [47] 56
    }
    DeRef(_file_2465);
    DeRef(_fn_2466);
    DeRef(_ret_2467);
    DeRefi(_y_2468);
    return -1;
L4: 

    /** 	ret = {}*/
    RefDS(_5);
    DeRef(_ret_2467);
    _ret_2467 = _5;

    /** 	while sequence(y) with entry do*/
    goto L5; // [63] 125
L6: 
    _1084 = IS_SEQUENCE(_y_2468);
    if (_1084 == 0)
    {
        _1084 = NOVALUE;
        goto L7; // [71] 135
    }
    else{
        _1084 = NOVALUE;
    }

    /** 		if y[$] = '\n' then*/
    if (IS_SEQUENCE(_y_2468)){
            _1085 = SEQ_PTR(_y_2468)->length;
    }
    else {
        _1085 = 1;
    }
    _2 = (int)SEQ_PTR(_y_2468);
    _1086 = (int)*(((s1_ptr)_2)->base + _1085);
    if (_1086 != 10)
    goto L8; // [83] 104

    /** 			y = y[1..$-1]*/
    if (IS_SEQUENCE(_y_2468)){
            _1088 = SEQ_PTR(_y_2468)->length;
    }
    else {
        _1088 = 1;
    }
    _1089 = _1088 - 1;
    _1088 = NOVALUE;
    rhs_slice_target = (object_ptr)&_y_2468;
    RHS_Slice(_y_2468, 1, _1089);

    /** 			ifdef UNIX then*/
L8: 

    /** 		ret = append(ret, y)*/
    Ref(_y_2468);
    Append(&_ret_2467, _ret_2467, _y_2468);

    /** 		if fn = 0 then*/
    if (binary_op_a(NOTEQ, _fn_2466, 0)){
        goto L9; // [112] 122
    }

    /** 			puts(2, '\n')*/
    EPuts(2, 10); // DJP 
L9: 

    /** 	entry*/
L5: 

    /** 		y = gets(fn)*/
    DeRefi(_y_2468);
    _y_2468 = EGets(_fn_2466);

    /** 	end while*/
    goto L6; // [132] 66
L7: 

    /** 	if sequence(file) and length(file) != 0 then*/
    _1095 = IS_SEQUENCE(_file_2465);
    if (_1095 == 0) {
        goto LA; // [140] 160
    }
    if (IS_SEQUENCE(_file_2465)){
            _1097 = SEQ_PTR(_file_2465)->length;
    }
    else {
        _1097 = 1;
    }
    _1098 = (_1097 != 0);
    _1097 = NOVALUE;
    if (_1098 == 0)
    {
        DeRef(_1098);
        _1098 = NOVALUE;
        goto LA; // [152] 160
    }
    else{
        DeRef(_1098);
        _1098 = NOVALUE;
    }

    /** 		close(fn)*/
    if (IS_ATOM_INT(_fn_2466))
    EClose(_fn_2466);
    else
    EClose((int)DBL_PTR(_fn_2466)->dbl);
LA: 

    /** 	return ret*/
    DeRef(_file_2465);
    DeRef(_fn_2466);
    DeRefi(_y_2468);
    _1086 = NOVALUE;
    DeRef(_1089);
    _1089 = NOVALUE;
    return _ret_2467;
    ;
}


int _16write_lines(int _file_2543, int _lines_2544)
{
    int _fn_2545 = NOVALUE;
    int _1128 = NOVALUE;
    int _1127 = NOVALUE;
    int _1126 = NOVALUE;
    int _1122 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(file) then*/
    _1122 = 1;
    if (_1122 == 0)
    {
        _1122 = NOVALUE;
        goto L1; // [8] 21
    }
    else{
        _1122 = NOVALUE;
    }

    /**     	fn = open(file, "w")*/
    DeRef(_fn_2545);
    _fn_2545 = EOpen(_file_2543, _1123, 0);
    goto L2; // [18] 27
L1: 

    /** 		fn = file*/
    Ref(_file_2543);
    DeRef(_fn_2545);
    _fn_2545 = _file_2543;
L2: 

    /** 	if fn < 0 then return -1 end if*/
    if (binary_op_a(GREATEREQ, _fn_2545, 0)){
        goto L3; // [31] 40
    }
    DeRef(_file_2543);
    DeRefDS(_lines_2544);
    DeRef(_fn_2545);
    return -1;
L3: 

    /** 	for i = 1 to length(lines) do*/
    if (IS_SEQUENCE(_lines_2544)){
            _1126 = SEQ_PTR(_lines_2544)->length;
    }
    else {
        _1126 = 1;
    }
    {
        int _i_2554;
        _i_2554 = 1;
L4: 
        if (_i_2554 > _1126){
            goto L5; // [45] 73
        }

        /** 		puts(fn, lines[i])*/
        _2 = (int)SEQ_PTR(_lines_2544);
        _1127 = (int)*(((s1_ptr)_2)->base + _i_2554);
        EPuts(_fn_2545, _1127); // DJP 
        _1127 = NOVALUE;

        /** 		puts(fn, '\n')*/
        EPuts(_fn_2545, 10); // DJP 

        /** 	end for*/
        _i_2554 = _i_2554 + 1;
        goto L4; // [68] 52
L5: 
        ;
    }

    /** 	if sequence(file) then*/
    _1128 = IS_SEQUENCE(_file_2543);
    if (_1128 == 0)
    {
        _1128 = NOVALUE;
        goto L6; // [78] 86
    }
    else{
        _1128 = NOVALUE;
    }

    /** 		close(fn)*/
    if (IS_ATOM_INT(_fn_2545))
    EClose(_fn_2545);
    else
    EClose((int)DBL_PTR(_fn_2545)->dbl);
L6: 

    /** 	return 1*/
    DeRef(_file_2543);
    DeRefDS(_lines_2544);
    DeRef(_fn_2545);
    return 1;
    ;
}


int _16write_file(int _file_2637, int _data_2638, int _as_text_2639)
{
    int _fn_2640 = NOVALUE;
    int _1186 = NOVALUE;
    int _1180 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if as_text != BINARY_MODE then*/

    /** 	if sequence(file) then*/
    _1180 = 1;
    if (_1180 == 0)
    {
        _1180 = NOVALUE;
        goto L1; // [159] 191
    }
    else{
        _1180 = NOVALUE;
    }

    /** 		if as_text = TEXT_MODE then*/

    /** 			fn = open(file, "wb")*/
    _fn_2640 = EOpen(_file_2637, _1183, 0);
    goto L2; // [188] 199
L1: 

    /** 		fn = file*/
    Ref(_file_2637);
    _fn_2640 = _file_2637;
    if (!IS_ATOM_INT(_fn_2640)) {
        _1 = (long)(DBL_PTR(_fn_2640)->dbl);
        if (UNIQUE(DBL_PTR(_fn_2640)) && (DBL_PTR(_fn_2640)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fn_2640);
        _fn_2640 = _1;
    }
L2: 

    /** 	if fn < 0 then return -1 end if*/
    if (_fn_2640 >= 0)
    goto L3; // [203] 212
    DeRefi(_file_2637);
    DeRefDS(_data_2638);
    return -1;
L3: 

    /** 	puts(fn, data)*/
    EPuts(_fn_2640, _data_2638); // DJP 

    /** 	if sequence(file) then*/
    _1186 = IS_SEQUENCE(_file_2637);
    if (_1186 == 0)
    {
        _1186 = NOVALUE;
        goto L4; // [222] 230
    }
    else{
        _1186 = NOVALUE;
    }

    /** 		close(fn)*/
    EClose(_fn_2640);
L4: 

    /** 	return 1*/
    DeRefi(_file_2637);
    DeRefDS(_data_2638);
    return 1;
    ;
}


void _16writef(int _fm_2681, int _data_2682, int _fn_2683, int _data_not_string_2684)
{
    int _real_fn_2685 = NOVALUE;
    int _close_fn_2686 = NOVALUE;
    int _out_style_2687 = NOVALUE;
    int _ts_2690 = NOVALUE;
    int _msg_inlined_crash_at_163_2715 = NOVALUE;
    int _data_inlined_crash_at_160_2714 = NOVALUE;
    int _1206 = NOVALUE;
    int _1204 = NOVALUE;
    int _1203 = NOVALUE;
    int _1202 = NOVALUE;
    int _1196 = NOVALUE;
    int _1195 = NOVALUE;
    int _1194 = NOVALUE;
    int _1193 = NOVALUE;
    int _1192 = NOVALUE;
    int _1191 = NOVALUE;
    int _1189 = NOVALUE;
    int _1188 = NOVALUE;
    int _1187 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer real_fn = 0*/
    _real_fn_2685 = 0;

    /** 	integer close_fn = 0*/
    _close_fn_2686 = 0;

    /** 	sequence out_style = "w"*/
    RefDS(_1123);
    DeRefi(_out_style_2687);
    _out_style_2687 = _1123;

    /** 	if integer(fm) then*/
    _1187 = 1;
    if (_1187 == 0)
    {
        _1187 = NOVALUE;
        goto L1; // [23] 49
    }
    else{
        _1187 = NOVALUE;
    }

    /** 		object ts*/

    /** 		ts = fm*/
    _ts_2690 = _fm_2681;

    /** 		fm = data*/
    RefDS(_data_2682);
    _fm_2681 = _data_2682;

    /** 		data = fn*/
    RefDS(_fn_2683);
    DeRefDS(_data_2682);
    _data_2682 = _fn_2683;

    /** 		fn = ts*/
    DeRefDS(_fn_2683);
    _fn_2683 = _ts_2690;
L1: 

    /** 	if sequence(fn) then*/
    _1188 = IS_SEQUENCE(_fn_2683);
    if (_1188 == 0)
    {
        _1188 = NOVALUE;
        goto L2; // [56] 191
    }
    else{
        _1188 = NOVALUE;
    }

    /** 		if length(fn) = 2 then*/
    if (IS_SEQUENCE(_fn_2683)){
            _1189 = SEQ_PTR(_fn_2683)->length;
    }
    else {
        _1189 = 1;
    }
    if (_1189 != 2)
    goto L3; // [64] 142

    /** 			if sequence(fn[1]) then*/
    _2 = (int)SEQ_PTR(_fn_2683);
    _1191 = (int)*(((s1_ptr)_2)->base + 1);
    _1192 = IS_SEQUENCE(_1191);
    _1191 = NOVALUE;
    if (_1192 == 0)
    {
        _1192 = NOVALUE;
        goto L4; // [77] 141
    }
    else{
        _1192 = NOVALUE;
    }

    /** 				if equal(fn[2], 'a') then*/
    _2 = (int)SEQ_PTR(_fn_2683);
    _1193 = (int)*(((s1_ptr)_2)->base + 2);
    if (_1193 == 97)
    _1194 = 1;
    else if (IS_ATOM_INT(_1193) && IS_ATOM_INT(97))
    _1194 = 0;
    else
    _1194 = (compare(_1193, 97) == 0);
    _1193 = NOVALUE;
    if (_1194 == 0)
    {
        _1194 = NOVALUE;
        goto L5; // [90] 103
    }
    else{
        _1194 = NOVALUE;
    }

    /** 					out_style = "a"*/
    RefDS(_1129);
    DeRefi(_out_style_2687);
    _out_style_2687 = _1129;
    goto L6; // [100] 134
L5: 

    /** 				elsif not equal(fn[2], "a") then*/
    _2 = (int)SEQ_PTR(_fn_2683);
    _1195 = (int)*(((s1_ptr)_2)->base + 2);
    if (_1195 == _1129)
    _1196 = 1;
    else if (IS_ATOM_INT(_1195) && IS_ATOM_INT(_1129))
    _1196 = 0;
    else
    _1196 = (compare(_1195, _1129) == 0);
    _1195 = NOVALUE;
    if (_1196 != 0)
    goto L7; // [113] 126
    _1196 = NOVALUE;

    /** 					out_style = "w"*/
    RefDS(_1123);
    DeRefi(_out_style_2687);
    _out_style_2687 = _1123;
    goto L6; // [123] 134
L7: 

    /** 					out_style = "a"*/
    RefDS(_1129);
    DeRefi(_out_style_2687);
    _out_style_2687 = _1129;
L6: 

    /** 				fn = fn[1]*/
    _0 = _fn_2683;
    _2 = (int)SEQ_PTR(_fn_2683);
    _fn_2683 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_fn_2683);
    DeRef(_0);
L4: 
L3: 

    /** 		real_fn = open(fn, out_style)*/
    _real_fn_2685 = EOpen(_fn_2683, _out_style_2687, 0);

    /** 		if real_fn = -1 then*/
    if (_real_fn_2685 != -1)
    goto L8; // [151] 183

    /** 			error:crash("Unable to write to '%s'", {fn})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_fn_2683);
    *((int *)(_2+4)) = _fn_2683;
    _1202 = MAKE_SEQ(_1);
    DeRef(_data_inlined_crash_at_160_2714);
    _data_inlined_crash_at_160_2714 = _1202;
    _1202 = NOVALUE;

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_163_2715);
    _msg_inlined_crash_at_163_2715 = EPrintf(-9999999, _1201, _data_inlined_crash_at_160_2714);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_163_2715);

    /** end procedure*/
    goto L9; // [177] 180
L9: 
    DeRef(_data_inlined_crash_at_160_2714);
    _data_inlined_crash_at_160_2714 = NOVALUE;
    DeRefi(_msg_inlined_crash_at_163_2715);
    _msg_inlined_crash_at_163_2715 = NOVALUE;
L8: 

    /** 		close_fn = 1*/
    _close_fn_2686 = 1;
    goto LA; // [188] 199
L2: 

    /** 		real_fn = fn*/
    Ref(_fn_2683);
    _real_fn_2685 = _fn_2683;
    if (!IS_ATOM_INT(_real_fn_2685)) {
        _1 = (long)(DBL_PTR(_real_fn_2685)->dbl);
        if (UNIQUE(DBL_PTR(_real_fn_2685)) && (DBL_PTR(_real_fn_2685)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_real_fn_2685);
        _real_fn_2685 = _1;
    }
LA: 

    /** 	if equal(data_not_string, 0) then*/
    if (_data_not_string_2684 == 0)
    _1203 = 1;
    else if (IS_ATOM_INT(_data_not_string_2684) && IS_ATOM_INT(0))
    _1203 = 0;
    else
    _1203 = (compare(_data_not_string_2684, 0) == 0);
    if (_1203 == 0)
    {
        _1203 = NOVALUE;
        goto LB; // [205] 225
    }
    else{
        _1203 = NOVALUE;
    }

    /** 		if types:t_display(data) then*/
    Ref(_data_2682);
    _1204 = _5t_display(_data_2682);
    if (_1204 == 0) {
        DeRef(_1204);
        _1204 = NOVALUE;
        goto LC; // [214] 224
    }
    else {
        if (!IS_ATOM_INT(_1204) && DBL_PTR(_1204)->dbl == 0.0){
            DeRef(_1204);
            _1204 = NOVALUE;
            goto LC; // [214] 224
        }
        DeRef(_1204);
        _1204 = NOVALUE;
    }
    DeRef(_1204);
    _1204 = NOVALUE;

    /** 			data = {data}*/
    _0 = _data_2682;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_data_2682);
    *((int *)(_2+4)) = _data_2682;
    _data_2682 = MAKE_SEQ(_1);
    DeRef(_0);
LC: 
LB: 

    /**     puts(real_fn, text:format( fm, data ) )*/
    Ref(_fm_2681);
    Ref(_data_2682);
    _1206 = _4format(_fm_2681, _data_2682);
    EPuts(_real_fn_2685, _1206); // DJP 
    DeRef(_1206);
    _1206 = NOVALUE;

    /**     if close_fn then*/
    if (_close_fn_2686 == 0)
    {
        goto LD; // [237] 245
    }
    else{
    }

    /**     	close(real_fn)*/
    EClose(_real_fn_2685);
LD: 

    /** end procedure*/
    DeRef(_fm_2681);
    DeRef(_data_2682);
    DeRef(_fn_2683);
    DeRefi(_out_style_2687);
    return;
    ;
}



// 0x08C3607D
