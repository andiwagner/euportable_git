// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _15get_ch()
{
    int _1221 = NOVALUE;
    int _1220 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(input_string) then*/
    _1220 = IS_SEQUENCE(_15input_string_2761);
    if (_1220 == 0)
    {
        _1220 = NOVALUE;
        goto L1; // [8] 56
    }
    else{
        _1220 = NOVALUE;
    }

    /** 		if string_next <= length(input_string) then*/
    if (IS_SEQUENCE(_15input_string_2761)){
            _1221 = SEQ_PTR(_15input_string_2761)->length;
    }
    else {
        _1221 = 1;
    }
    if (_15string_next_2762 > _1221)
    goto L2; // [20] 47

    /** 			ch = input_string[string_next]*/
    _2 = (int)SEQ_PTR(_15input_string_2761);
    _15ch_2763 = (int)*(((s1_ptr)_2)->base + _15string_next_2762);
    if (!IS_ATOM_INT(_15ch_2763)){
        _15ch_2763 = (long)DBL_PTR(_15ch_2763)->dbl;
    }

    /** 			string_next += 1*/
    _15string_next_2762 = _15string_next_2762 + 1;
    goto L3; // [44] 81
L2: 

    /** 			ch = GET_EOF*/
    _15ch_2763 = -1;
    goto L3; // [53] 81
L1: 

    /** 		ch = getc(input_file)*/
    if (_15input_file_2760 != last_r_file_no) {
        last_r_file_ptr = which_file(_15input_file_2760, EF_READ);
        last_r_file_no = _15input_file_2760;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _15ch_2763 = getKBchar();
        }
        else
        _15ch_2763 = getc(last_r_file_ptr);
    }
    else
    _15ch_2763 = getc(last_r_file_ptr);

    /** 		if ch = GET_EOF then*/
    if (_15ch_2763 != -1)
    goto L4; // [67] 80

    /** 			string_next += 1*/
    _15string_next_2762 = _15string_next_2762 + 1;
L4: 
L3: 

    /** end procedure*/
    return;
    ;
}


int _15escape_char(int _c_2790)
{
    int _i_2791 = NOVALUE;
    int _1233 = NOVALUE;
    int _0, _1, _2;
    

    /** 	i = find(c, ESCAPE_CHARS)*/
    _i_2791 = find_from(_c_2790, _15ESCAPE_CHARS_2784, 1);

    /** 	if i = 0 then*/
    if (_i_2791 != 0)
    goto L1; // [12] 25

    /** 		return GET_FAIL*/
    return 1;
    goto L2; // [22] 36
L1: 

    /** 		return ESCAPED_CHARS[i]*/
    _2 = (int)SEQ_PTR(_15ESCAPED_CHARS_2786);
    _1233 = (int)*(((s1_ptr)_2)->base + _i_2791);
    Ref(_1233);
    return _1233;
L2: 
    ;
}


int _15get_qchar()
{
    int _c_2799 = NOVALUE;
    int _1244 = NOVALUE;
    int _1243 = NOVALUE;
    int _1241 = NOVALUE;
    int _1238 = NOVALUE;
    int _0, _1, _2;
    

    /** 	get_ch()*/
    _15get_ch();

    /** 	c = ch*/
    _c_2799 = _15ch_2763;

    /** 	if ch = '\\' then*/
    if (_15ch_2763 != 92)
    goto L1; // [16] 54

    /** 		get_ch()*/
    _15get_ch();

    /** 		c = escape_char(ch)*/
    _c_2799 = _15escape_char(_15ch_2763);
    if (!IS_ATOM_INT(_c_2799)) {
        _1 = (long)(DBL_PTR(_c_2799)->dbl);
        if (UNIQUE(DBL_PTR(_c_2799)) && (DBL_PTR(_c_2799)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_2799);
        _c_2799 = _1;
    }

    /** 		if c = GET_FAIL then*/
    if (_c_2799 != 1)
    goto L2; // [36] 74

    /** 			return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1238 = MAKE_SEQ(_1);
    return _1238;
    goto L2; // [51] 74
L1: 

    /** 	elsif ch = '\'' then*/
    if (_15ch_2763 != 39)
    goto L3; // [58] 73

    /** 		return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1241 = MAKE_SEQ(_1);
    DeRef(_1238);
    _1238 = NOVALUE;
    return _1241;
L3: 
L2: 

    /** 	get_ch()*/
    _15get_ch();

    /** 	if ch != '\'' then*/
    if (_15ch_2763 == 39)
    goto L4; // [82] 99

    /** 		return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1243 = MAKE_SEQ(_1);
    DeRef(_1238);
    _1238 = NOVALUE;
    DeRef(_1241);
    _1241 = NOVALUE;
    return _1243;
    goto L5; // [96] 114
L4: 

    /** 		get_ch()*/
    _15get_ch();

    /** 		return {GET_SUCCESS, c}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _c_2799;
    _1244 = MAKE_SEQ(_1);
    DeRef(_1238);
    _1238 = NOVALUE;
    DeRef(_1241);
    _1241 = NOVALUE;
    DeRef(_1243);
    _1243 = NOVALUE;
    return _1244;
L5: 
    ;
}


int _15get_heredoc(int _terminator_2818)
{
    int _text_2819 = NOVALUE;
    int _ends_at_2820 = NOVALUE;
    int _1259 = NOVALUE;
    int _1258 = NOVALUE;
    int _1257 = NOVALUE;
    int _1256 = NOVALUE;
    int _1255 = NOVALUE;
    int _1252 = NOVALUE;
    int _1250 = NOVALUE;
    int _1249 = NOVALUE;
    int _1248 = NOVALUE;
    int _1247 = NOVALUE;
    int _1245 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence text = ""*/
    RefDS(_5);
    DeRefi(_text_2819);
    _text_2819 = _5;

    /** 	integer ends_at = 1 - length( terminator )*/
    if (IS_SEQUENCE(_terminator_2818)){
            _1245 = SEQ_PTR(_terminator_2818)->length;
    }
    else {
        _1245 = 1;
    }
    _ends_at_2820 = 1 - _1245;
    _1245 = NOVALUE;

    /** 	while ends_at < 1 or not match( terminator, text, ends_at ) with entry do*/
    goto L1; // [21] 69
L2: 
    _1247 = (_ends_at_2820 < 1);
    if (_1247 != 0) {
        DeRef(_1248);
        _1248 = 1;
        goto L3; // [28] 44
    }
    _1249 = e_match_from(_terminator_2818, _text_2819, _ends_at_2820);
    _1250 = (_1249 == 0);
    _1249 = NOVALUE;
    _1248 = (_1250 != 0);
L3: 
    if (_1248 == 0)
    {
        _1248 = NOVALUE;
        goto L4; // [44] 92
    }
    else{
        _1248 = NOVALUE;
    }

    /** 		if ch = GET_EOF then*/
    if (_15ch_2763 != -1)
    goto L5; // [51] 66

    /** 			return { GET_FAIL, 0 }*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1252 = MAKE_SEQ(_1);
    DeRefDSi(_terminator_2818);
    DeRefi(_text_2819);
    DeRef(_1247);
    _1247 = NOVALUE;
    DeRef(_1250);
    _1250 = NOVALUE;
    return _1252;
L5: 

    /** 	entry*/
L1: 

    /** 		get_ch()*/
    _15get_ch();

    /** 		text &= ch*/
    Append(&_text_2819, _text_2819, _15ch_2763);

    /** 		ends_at += 1*/
    _ends_at_2820 = _ends_at_2820 + 1;

    /** 	end while*/
    goto L2; // [89] 24
L4: 

    /** 	return { GET_SUCCESS, head( text, length( text ) - length( terminator ) ) }*/
    if (IS_SEQUENCE(_text_2819)){
            _1255 = SEQ_PTR(_text_2819)->length;
    }
    else {
        _1255 = 1;
    }
    if (IS_SEQUENCE(_terminator_2818)){
            _1256 = SEQ_PTR(_terminator_2818)->length;
    }
    else {
        _1256 = 1;
    }
    _1257 = _1255 - _1256;
    _1255 = NOVALUE;
    _1256 = NOVALUE;
    {
        int len = SEQ_PTR(_text_2819)->length;
        int size = (IS_ATOM_INT(_1257)) ? _1257 : (object)(DBL_PTR(_1257)->dbl);
        if (size <= 0){
            DeRef( _1258 );
            _1258 = MAKE_SEQ(NewS1(0));
        }
        else if (len <= size) {
            RefDS(_text_2819);
            DeRef(_1258);
            _1258 = _text_2819;
        }
        else{
            Head(SEQ_PTR(_text_2819),size+1,&_1258);
        }
    }
    _1257 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _1258;
    _1259 = MAKE_SEQ(_1);
    _1258 = NOVALUE;
    DeRefDSi(_terminator_2818);
    DeRefDSi(_text_2819);
    DeRef(_1247);
    _1247 = NOVALUE;
    DeRef(_1250);
    _1250 = NOVALUE;
    DeRef(_1252);
    _1252 = NOVALUE;
    return _1259;
    ;
}


int _15get_string()
{
    int _text_2840 = NOVALUE;
    int _1276 = NOVALUE;
    int _1272 = NOVALUE;
    int _1271 = NOVALUE;
    int _1268 = NOVALUE;
    int _1267 = NOVALUE;
    int _1266 = NOVALUE;
    int _1265 = NOVALUE;
    int _1263 = NOVALUE;
    int _1262 = NOVALUE;
    int _1260 = NOVALUE;
    int _0, _1, _2;
    

    /** 	text = ""*/
    RefDS(_5);
    DeRefi(_text_2840);
    _text_2840 = _5;

    /** 	while TRUE do*/
L1: 

    /** 		get_ch()*/
    _15get_ch();

    /** 		if ch = GET_EOF or ch = '\n' then*/
    _1260 = (_15ch_2763 == -1);
    if (_1260 != 0) {
        goto L2; // [25] 40
    }
    _1262 = (_15ch_2763 == 10);
    if (_1262 == 0)
    {
        DeRef(_1262);
        _1262 = NOVALUE;
        goto L3; // [36] 53
    }
    else{
        DeRef(_1262);
        _1262 = NOVALUE;
    }
L2: 

    /** 			return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1263 = MAKE_SEQ(_1);
    DeRefi(_text_2840);
    DeRef(_1260);
    _1260 = NOVALUE;
    return _1263;
    goto L4; // [50] 164
L3: 

    /** 		elsif ch = '"' then*/
    if (_15ch_2763 != 34)
    goto L5; // [57] 121

    /** 			get_ch()*/
    _15get_ch();

    /** 			if length( text ) = 0 and ch = '"' then*/
    if (IS_SEQUENCE(_text_2840)){
            _1265 = SEQ_PTR(_text_2840)->length;
    }
    else {
        _1265 = 1;
    }
    _1266 = (_1265 == 0);
    _1265 = NOVALUE;
    if (_1266 == 0) {
        goto L6; // [74] 108
    }
    _1268 = (_15ch_2763 == 34);
    if (_1268 == 0)
    {
        DeRef(_1268);
        _1268 = NOVALUE;
        goto L6; // [85] 108
    }
    else{
        DeRef(_1268);
        _1268 = NOVALUE;
    }

    /** 				if ch = '"' then*/
    if (_15ch_2763 != 34)
    goto L7; // [92] 107

    /** 					return get_heredoc( `"""` )*/
    RefDS(_1270);
    _1271 = _15get_heredoc(_1270);
    DeRefi(_text_2840);
    DeRef(_1260);
    _1260 = NOVALUE;
    DeRef(_1263);
    _1263 = NOVALUE;
    DeRef(_1266);
    _1266 = NOVALUE;
    return _1271;
L7: 
L6: 

    /** 			return {GET_SUCCESS, text}*/
    RefDS(_text_2840);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _text_2840;
    _1272 = MAKE_SEQ(_1);
    DeRefDSi(_text_2840);
    DeRef(_1260);
    _1260 = NOVALUE;
    DeRef(_1263);
    _1263 = NOVALUE;
    DeRef(_1266);
    _1266 = NOVALUE;
    DeRef(_1271);
    _1271 = NOVALUE;
    return _1272;
    goto L4; // [118] 164
L5: 

    /** 		elsif ch = '\\' then*/
    if (_15ch_2763 != 92)
    goto L8; // [125] 163

    /** 			get_ch()*/
    _15get_ch();

    /** 			ch = escape_char(ch)*/
    _0 = _15escape_char(_15ch_2763);
    _15ch_2763 = _0;
    if (!IS_ATOM_INT(_15ch_2763)) {
        _1 = (long)(DBL_PTR(_15ch_2763)->dbl);
        if (UNIQUE(DBL_PTR(_15ch_2763)) && (DBL_PTR(_15ch_2763)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_15ch_2763);
        _15ch_2763 = _1;
    }

    /** 			if ch = GET_FAIL then*/
    if (_15ch_2763 != 1)
    goto L9; // [147] 162

    /** 				return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1276 = MAKE_SEQ(_1);
    DeRefi(_text_2840);
    DeRef(_1260);
    _1260 = NOVALUE;
    DeRef(_1263);
    _1263 = NOVALUE;
    DeRef(_1266);
    _1266 = NOVALUE;
    DeRef(_1271);
    _1271 = NOVALUE;
    DeRef(_1272);
    _1272 = NOVALUE;
    return _1276;
L9: 
L8: 
L4: 

    /** 		text = text & ch*/
    Append(&_text_2840, _text_2840, _15ch_2763);

    /** 	end while*/
    goto L1; // [174] 13
    ;
}


int _15read_comment()
{
    int _1297 = NOVALUE;
    int _1296 = NOVALUE;
    int _1294 = NOVALUE;
    int _1292 = NOVALUE;
    int _1290 = NOVALUE;
    int _1289 = NOVALUE;
    int _1288 = NOVALUE;
    int _1286 = NOVALUE;
    int _1285 = NOVALUE;
    int _1284 = NOVALUE;
    int _1283 = NOVALUE;
    int _1282 = NOVALUE;
    int _1281 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(input_string) then*/
    _1281 = IS_ATOM(_15input_string_2761);
    if (_1281 == 0)
    {
        _1281 = NOVALUE;
        goto L1; // [8] 98
    }
    else{
        _1281 = NOVALUE;
    }

    /** 		while ch!='\n' and ch!='\r' and ch!=-1 do*/
L2: 
    _1282 = (_15ch_2763 != 10);
    if (_1282 == 0) {
        _1283 = 0;
        goto L3; // [22] 36
    }
    _1284 = (_15ch_2763 != 13);
    _1283 = (_1284 != 0);
L3: 
    if (_1283 == 0) {
        goto L4; // [36] 59
    }
    _1286 = (_15ch_2763 != -1);
    if (_1286 == 0)
    {
        DeRef(_1286);
        _1286 = NOVALUE;
        goto L4; // [47] 59
    }
    else{
        DeRef(_1286);
        _1286 = NOVALUE;
    }

    /** 			get_ch()*/
    _15get_ch();

    /** 		end while*/
    goto L2; // [56] 16
L4: 

    /** 		get_ch()*/
    _15get_ch();

    /** 		if ch=-1 then*/
    if (_15ch_2763 != -1)
    goto L5; // [67] 84

    /** 			return {GET_EOF, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = 0;
    _1288 = MAKE_SEQ(_1);
    DeRef(_1282);
    _1282 = NOVALUE;
    DeRef(_1284);
    _1284 = NOVALUE;
    return _1288;
    goto L6; // [81] 182
L5: 

    /** 			return {GET_IGNORE, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -2;
    ((int *)_2)[2] = 0;
    _1289 = MAKE_SEQ(_1);
    DeRef(_1282);
    _1282 = NOVALUE;
    DeRef(_1284);
    _1284 = NOVALUE;
    DeRef(_1288);
    _1288 = NOVALUE;
    return _1289;
    goto L6; // [95] 182
L1: 

    /** 		for i=string_next to length(input_string) do*/
    if (IS_SEQUENCE(_15input_string_2761)){
            _1290 = SEQ_PTR(_15input_string_2761)->length;
    }
    else {
        _1290 = 1;
    }
    {
        int _i_2890;
        _i_2890 = _15string_next_2762;
L7: 
        if (_i_2890 > _1290){
            goto L8; // [107] 171
        }

        /** 			ch=input_string[i]*/
        _2 = (int)SEQ_PTR(_15input_string_2761);
        _15ch_2763 = (int)*(((s1_ptr)_2)->base + _i_2890);
        if (!IS_ATOM_INT(_15ch_2763)){
            _15ch_2763 = (long)DBL_PTR(_15ch_2763)->dbl;
        }

        /** 			if ch='\n' or ch='\r' then*/
        _1292 = (_15ch_2763 == 10);
        if (_1292 != 0) {
            goto L9; // [132] 147
        }
        _1294 = (_15ch_2763 == 13);
        if (_1294 == 0)
        {
            DeRef(_1294);
            _1294 = NOVALUE;
            goto LA; // [143] 164
        }
        else{
            DeRef(_1294);
            _1294 = NOVALUE;
        }
L9: 

        /** 				string_next=i+1*/
        _15string_next_2762 = _i_2890 + 1;

        /** 				return {GET_IGNORE, 0}*/
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = -2;
        ((int *)_2)[2] = 0;
        _1296 = MAKE_SEQ(_1);
        DeRef(_1282);
        _1282 = NOVALUE;
        DeRef(_1284);
        _1284 = NOVALUE;
        DeRef(_1288);
        _1288 = NOVALUE;
        DeRef(_1289);
        _1289 = NOVALUE;
        DeRef(_1292);
        _1292 = NOVALUE;
        return _1296;
LA: 

        /** 		end for*/
        _i_2890 = _i_2890 + 1;
        goto L7; // [166] 114
L8: 
        ;
    }

    /** 		return {GET_EOF, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = 0;
    _1297 = MAKE_SEQ(_1);
    DeRef(_1282);
    _1282 = NOVALUE;
    DeRef(_1284);
    _1284 = NOVALUE;
    DeRef(_1288);
    _1288 = NOVALUE;
    DeRef(_1289);
    _1289 = NOVALUE;
    DeRef(_1292);
    _1292 = NOVALUE;
    DeRef(_1296);
    _1296 = NOVALUE;
    return _1297;
L6: 
    ;
}


int _15get_number()
{
    int _sign_2902 = NOVALUE;
    int _e_sign_2903 = NOVALUE;
    int _ndigits_2904 = NOVALUE;
    int _hex_digit_2905 = NOVALUE;
    int _mantissa_2906 = NOVALUE;
    int _dec_2907 = NOVALUE;
    int _e_mag_2908 = NOVALUE;
    int _1359 = NOVALUE;
    int _1357 = NOVALUE;
    int _1355 = NOVALUE;
    int _1351 = NOVALUE;
    int _1347 = NOVALUE;
    int _1345 = NOVALUE;
    int _1344 = NOVALUE;
    int _1343 = NOVALUE;
    int _1342 = NOVALUE;
    int _1341 = NOVALUE;
    int _1339 = NOVALUE;
    int _1338 = NOVALUE;
    int _1337 = NOVALUE;
    int _1334 = NOVALUE;
    int _1332 = NOVALUE;
    int _1330 = NOVALUE;
    int _1326 = NOVALUE;
    int _1325 = NOVALUE;
    int _1323 = NOVALUE;
    int _1322 = NOVALUE;
    int _1321 = NOVALUE;
    int _1318 = NOVALUE;
    int _1317 = NOVALUE;
    int _1315 = NOVALUE;
    int _1314 = NOVALUE;
    int _1313 = NOVALUE;
    int _1312 = NOVALUE;
    int _1311 = NOVALUE;
    int _1310 = NOVALUE;
    int _1307 = NOVALUE;
    int _1303 = NOVALUE;
    int _1300 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sign = +1*/
    _sign_2902 = 1;

    /** 	mantissa = 0*/
    DeRef(_mantissa_2906);
    _mantissa_2906 = 0;

    /** 	ndigits = 0*/
    _ndigits_2904 = 0;

    /** 	if ch = '-' then*/
    if (_15ch_2763 != 45)
    goto L1; // [20] 54

    /** 		sign = -1*/
    _sign_2902 = -1;

    /** 		get_ch()*/
    _15get_ch();

    /** 		if ch='-' then*/
    if (_15ch_2763 != 45)
    goto L2; // [37] 68

    /** 			return read_comment()*/
    _1300 = _15read_comment();
    DeRef(_dec_2907);
    DeRef(_e_mag_2908);
    return _1300;
    goto L2; // [51] 68
L1: 

    /** 	elsif ch = '+' then*/
    if (_15ch_2763 != 43)
    goto L3; // [58] 67

    /** 		get_ch()*/
    _15get_ch();
L3: 
L2: 

    /** 	if ch = '#' then*/
    if (_15ch_2763 != 35)
    goto L4; // [72] 170

    /** 		get_ch()*/
    _15get_ch();

    /** 		while TRUE do*/
L5: 

    /** 			hex_digit = find(ch, HEX_DIGITS)-1*/
    _1303 = find_from(_15ch_2763, _15HEX_DIGITS_2743, 1);
    _hex_digit_2905 = _1303 - 1;
    _1303 = NOVALUE;

    /** 			if hex_digit >= 0 then*/
    if (_hex_digit_2905 < 0)
    goto L6; // [102] 129

    /** 				ndigits += 1*/
    _ndigits_2904 = _ndigits_2904 + 1;

    /** 				mantissa = mantissa * 16 + hex_digit*/
    if (IS_ATOM_INT(_mantissa_2906)) {
        if (_mantissa_2906 == (short)_mantissa_2906)
        _1307 = _mantissa_2906 * 16;
        else
        _1307 = NewDouble(_mantissa_2906 * (double)16);
    }
    else {
        _1307 = NewDouble(DBL_PTR(_mantissa_2906)->dbl * (double)16);
    }
    DeRef(_mantissa_2906);
    if (IS_ATOM_INT(_1307)) {
        _mantissa_2906 = _1307 + _hex_digit_2905;
        if ((long)((unsigned long)_mantissa_2906 + (unsigned long)HIGH_BITS) >= 0) 
        _mantissa_2906 = NewDouble((double)_mantissa_2906);
    }
    else {
        _mantissa_2906 = NewDouble(DBL_PTR(_1307)->dbl + (double)_hex_digit_2905);
    }
    DeRef(_1307);
    _1307 = NOVALUE;

    /** 				get_ch()*/
    _15get_ch();
    goto L5; // [126] 85
L6: 

    /** 				if ndigits > 0 then*/
    if (_ndigits_2904 <= 0)
    goto L7; // [131] 152

    /** 					return {GET_SUCCESS, sign * mantissa}*/
    if (IS_ATOM_INT(_mantissa_2906)) {
        if (_sign_2902 == (short)_sign_2902 && _mantissa_2906 <= INT15 && _mantissa_2906 >= -INT15)
        _1310 = _sign_2902 * _mantissa_2906;
        else
        _1310 = NewDouble(_sign_2902 * (double)_mantissa_2906);
    }
    else {
        _1310 = NewDouble((double)_sign_2902 * DBL_PTR(_mantissa_2906)->dbl);
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _1310;
    _1311 = MAKE_SEQ(_1);
    _1310 = NOVALUE;
    DeRef(_mantissa_2906);
    DeRef(_dec_2907);
    DeRef(_e_mag_2908);
    DeRef(_1300);
    _1300 = NOVALUE;
    return _1311;
    goto L5; // [149] 85
L7: 

    /** 					return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1312 = MAKE_SEQ(_1);
    DeRef(_mantissa_2906);
    DeRef(_dec_2907);
    DeRef(_e_mag_2908);
    DeRef(_1300);
    _1300 = NOVALUE;
    DeRef(_1311);
    _1311 = NOVALUE;
    return _1312;

    /** 		end while*/
    goto L5; // [166] 85
L4: 

    /** 	while ch >= '0' and ch <= '9' do*/
L8: 
    _1313 = (_15ch_2763 >= 48);
    if (_1313 == 0) {
        goto L9; // [181] 226
    }
    _1315 = (_15ch_2763 <= 57);
    if (_1315 == 0)
    {
        DeRef(_1315);
        _1315 = NOVALUE;
        goto L9; // [192] 226
    }
    else{
        DeRef(_1315);
        _1315 = NOVALUE;
    }

    /** 		ndigits += 1*/
    _ndigits_2904 = _ndigits_2904 + 1;

    /** 		mantissa = mantissa * 10 + (ch - '0')*/
    if (IS_ATOM_INT(_mantissa_2906)) {
        if (_mantissa_2906 == (short)_mantissa_2906)
        _1317 = _mantissa_2906 * 10;
        else
        _1317 = NewDouble(_mantissa_2906 * (double)10);
    }
    else {
        _1317 = NewDouble(DBL_PTR(_mantissa_2906)->dbl * (double)10);
    }
    _1318 = _15ch_2763 - 48;
    if ((long)((unsigned long)_1318 +(unsigned long) HIGH_BITS) >= 0){
        _1318 = NewDouble((double)_1318);
    }
    DeRef(_mantissa_2906);
    if (IS_ATOM_INT(_1317) && IS_ATOM_INT(_1318)) {
        _mantissa_2906 = _1317 + _1318;
        if ((long)((unsigned long)_mantissa_2906 + (unsigned long)HIGH_BITS) >= 0) 
        _mantissa_2906 = NewDouble((double)_mantissa_2906);
    }
    else {
        if (IS_ATOM_INT(_1317)) {
            _mantissa_2906 = NewDouble((double)_1317 + DBL_PTR(_1318)->dbl);
        }
        else {
            if (IS_ATOM_INT(_1318)) {
                _mantissa_2906 = NewDouble(DBL_PTR(_1317)->dbl + (double)_1318);
            }
            else
            _mantissa_2906 = NewDouble(DBL_PTR(_1317)->dbl + DBL_PTR(_1318)->dbl);
        }
    }
    DeRef(_1317);
    _1317 = NOVALUE;
    DeRef(_1318);
    _1318 = NOVALUE;

    /** 		get_ch()*/
    _15get_ch();

    /** 	end while*/
    goto L8; // [223] 175
L9: 

    /** 	if ch = '.' then*/
    if (_15ch_2763 != 46)
    goto LA; // [230] 306

    /** 		get_ch()*/
    _15get_ch();

    /** 		dec = 10*/
    DeRef(_dec_2907);
    _dec_2907 = 10;

    /** 		while ch >= '0' and ch <= '9' do*/
LB: 
    _1321 = (_15ch_2763 >= 48);
    if (_1321 == 0) {
        goto LC; // [254] 305
    }
    _1323 = (_15ch_2763 <= 57);
    if (_1323 == 0)
    {
        DeRef(_1323);
        _1323 = NOVALUE;
        goto LC; // [265] 305
    }
    else{
        DeRef(_1323);
        _1323 = NOVALUE;
    }

    /** 			ndigits += 1*/
    _ndigits_2904 = _ndigits_2904 + 1;

    /** 			mantissa += (ch - '0') / dec*/
    _1325 = _15ch_2763 - 48;
    if ((long)((unsigned long)_1325 +(unsigned long) HIGH_BITS) >= 0){
        _1325 = NewDouble((double)_1325);
    }
    if (IS_ATOM_INT(_1325) && IS_ATOM_INT(_dec_2907)) {
        _1326 = (_1325 % _dec_2907) ? NewDouble((double)_1325 / _dec_2907) : (_1325 / _dec_2907);
    }
    else {
        if (IS_ATOM_INT(_1325)) {
            _1326 = NewDouble((double)_1325 / DBL_PTR(_dec_2907)->dbl);
        }
        else {
            if (IS_ATOM_INT(_dec_2907)) {
                _1326 = NewDouble(DBL_PTR(_1325)->dbl / (double)_dec_2907);
            }
            else
            _1326 = NewDouble(DBL_PTR(_1325)->dbl / DBL_PTR(_dec_2907)->dbl);
        }
    }
    DeRef(_1325);
    _1325 = NOVALUE;
    _0 = _mantissa_2906;
    if (IS_ATOM_INT(_mantissa_2906) && IS_ATOM_INT(_1326)) {
        _mantissa_2906 = _mantissa_2906 + _1326;
        if ((long)((unsigned long)_mantissa_2906 + (unsigned long)HIGH_BITS) >= 0) 
        _mantissa_2906 = NewDouble((double)_mantissa_2906);
    }
    else {
        if (IS_ATOM_INT(_mantissa_2906)) {
            _mantissa_2906 = NewDouble((double)_mantissa_2906 + DBL_PTR(_1326)->dbl);
        }
        else {
            if (IS_ATOM_INT(_1326)) {
                _mantissa_2906 = NewDouble(DBL_PTR(_mantissa_2906)->dbl + (double)_1326);
            }
            else
            _mantissa_2906 = NewDouble(DBL_PTR(_mantissa_2906)->dbl + DBL_PTR(_1326)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_1326);
    _1326 = NOVALUE;

    /** 			dec *= 10*/
    _0 = _dec_2907;
    if (IS_ATOM_INT(_dec_2907)) {
        if (_dec_2907 == (short)_dec_2907)
        _dec_2907 = _dec_2907 * 10;
        else
        _dec_2907 = NewDouble(_dec_2907 * (double)10);
    }
    else {
        _dec_2907 = NewDouble(DBL_PTR(_dec_2907)->dbl * (double)10);
    }
    DeRef(_0);

    /** 			get_ch()*/
    _15get_ch();

    /** 		end while*/
    goto LB; // [302] 248
LC: 
LA: 

    /** 	if ndigits = 0 then*/
    if (_ndigits_2904 != 0)
    goto LD; // [308] 323

    /** 		return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1330 = MAKE_SEQ(_1);
    DeRef(_mantissa_2906);
    DeRef(_dec_2907);
    DeRef(_e_mag_2908);
    DeRef(_1300);
    _1300 = NOVALUE;
    DeRef(_1311);
    _1311 = NOVALUE;
    DeRef(_1312);
    _1312 = NOVALUE;
    DeRef(_1313);
    _1313 = NOVALUE;
    DeRef(_1321);
    _1321 = NOVALUE;
    return _1330;
LD: 

    /** 	mantissa = sign * mantissa*/
    _0 = _mantissa_2906;
    if (IS_ATOM_INT(_mantissa_2906)) {
        if (_sign_2902 == (short)_sign_2902 && _mantissa_2906 <= INT15 && _mantissa_2906 >= -INT15)
        _mantissa_2906 = _sign_2902 * _mantissa_2906;
        else
        _mantissa_2906 = NewDouble(_sign_2902 * (double)_mantissa_2906);
    }
    else {
        _mantissa_2906 = NewDouble((double)_sign_2902 * DBL_PTR(_mantissa_2906)->dbl);
    }
    DeRef(_0);

    /** 	if ch = 'e' or ch = 'E' then*/
    _1332 = (_15ch_2763 == 101);
    if (_1332 != 0) {
        goto LE; // [337] 352
    }
    _1334 = (_15ch_2763 == 69);
    if (_1334 == 0)
    {
        DeRef(_1334);
        _1334 = NOVALUE;
        goto LF; // [348] 573
    }
    else{
        DeRef(_1334);
        _1334 = NOVALUE;
    }
LE: 

    /** 		e_sign = +1*/
    _e_sign_2903 = 1;

    /** 		e_mag = 0*/
    DeRef(_e_mag_2908);
    _e_mag_2908 = 0;

    /** 		get_ch()*/
    _15get_ch();

    /** 		if ch = '-' then*/
    if (_15ch_2763 != 45)
    goto L10; // [370] 386

    /** 			e_sign = -1*/
    _e_sign_2903 = -1;

    /** 			get_ch()*/
    _15get_ch();
    goto L11; // [383] 400
L10: 

    /** 		elsif ch = '+' then*/
    if (_15ch_2763 != 43)
    goto L12; // [390] 399

    /** 			get_ch()*/
    _15get_ch();
L12: 
L11: 

    /** 		if ch >= '0' and ch <= '9' then*/
    _1337 = (_15ch_2763 >= 48);
    if (_1337 == 0) {
        goto L13; // [408] 487
    }
    _1339 = (_15ch_2763 <= 57);
    if (_1339 == 0)
    {
        DeRef(_1339);
        _1339 = NOVALUE;
        goto L13; // [419] 487
    }
    else{
        DeRef(_1339);
        _1339 = NOVALUE;
    }

    /** 			e_mag = ch - '0'*/
    DeRef(_e_mag_2908);
    _e_mag_2908 = _15ch_2763 - 48;
    if ((long)((unsigned long)_e_mag_2908 +(unsigned long) HIGH_BITS) >= 0){
        _e_mag_2908 = NewDouble((double)_e_mag_2908);
    }

    /** 			get_ch()*/
    _15get_ch();

    /** 			while ch >= '0' and ch <= '9' do*/
L14: 
    _1341 = (_15ch_2763 >= 48);
    if (_1341 == 0) {
        goto L15; // [445] 498
    }
    _1343 = (_15ch_2763 <= 57);
    if (_1343 == 0)
    {
        DeRef(_1343);
        _1343 = NOVALUE;
        goto L15; // [456] 498
    }
    else{
        DeRef(_1343);
        _1343 = NOVALUE;
    }

    /** 				e_mag = e_mag * 10 + ch - '0'*/
    if (IS_ATOM_INT(_e_mag_2908)) {
        if (_e_mag_2908 == (short)_e_mag_2908)
        _1344 = _e_mag_2908 * 10;
        else
        _1344 = NewDouble(_e_mag_2908 * (double)10);
    }
    else {
        _1344 = NewDouble(DBL_PTR(_e_mag_2908)->dbl * (double)10);
    }
    if (IS_ATOM_INT(_1344)) {
        _1345 = _1344 + _15ch_2763;
        if ((long)((unsigned long)_1345 + (unsigned long)HIGH_BITS) >= 0) 
        _1345 = NewDouble((double)_1345);
    }
    else {
        _1345 = NewDouble(DBL_PTR(_1344)->dbl + (double)_15ch_2763);
    }
    DeRef(_1344);
    _1344 = NOVALUE;
    DeRef(_e_mag_2908);
    if (IS_ATOM_INT(_1345)) {
        _e_mag_2908 = _1345 - 48;
        if ((long)((unsigned long)_e_mag_2908 +(unsigned long) HIGH_BITS) >= 0){
            _e_mag_2908 = NewDouble((double)_e_mag_2908);
        }
    }
    else {
        _e_mag_2908 = NewDouble(DBL_PTR(_1345)->dbl - (double)48);
    }
    DeRef(_1345);
    _1345 = NOVALUE;

    /** 				get_ch()*/
    _15get_ch();

    /** 			end while*/
    goto L14; // [481] 439
    goto L15; // [484] 498
L13: 

    /** 			return {GET_FAIL, 0} -- no exponent*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1347 = MAKE_SEQ(_1);
    DeRef(_mantissa_2906);
    DeRef(_dec_2907);
    DeRef(_e_mag_2908);
    DeRef(_1300);
    _1300 = NOVALUE;
    DeRef(_1311);
    _1311 = NOVALUE;
    DeRef(_1312);
    _1312 = NOVALUE;
    DeRef(_1313);
    _1313 = NOVALUE;
    DeRef(_1321);
    _1321 = NOVALUE;
    DeRef(_1330);
    _1330 = NOVALUE;
    DeRef(_1332);
    _1332 = NOVALUE;
    DeRef(_1337);
    _1337 = NOVALUE;
    DeRef(_1341);
    _1341 = NOVALUE;
    return _1347;
L15: 

    /** 		e_mag *= e_sign*/
    _0 = _e_mag_2908;
    if (IS_ATOM_INT(_e_mag_2908)) {
        if (_e_mag_2908 == (short)_e_mag_2908 && _e_sign_2903 <= INT15 && _e_sign_2903 >= -INT15)
        _e_mag_2908 = _e_mag_2908 * _e_sign_2903;
        else
        _e_mag_2908 = NewDouble(_e_mag_2908 * (double)_e_sign_2903);
    }
    else {
        _e_mag_2908 = NewDouble(DBL_PTR(_e_mag_2908)->dbl * (double)_e_sign_2903);
    }
    DeRef(_0);

    /** 		if e_mag > 308 then*/
    if (binary_op_a(LESSEQ, _e_mag_2908, 308)){
        goto L16; // [506] 561
    }

    /** 			mantissa *= power(10, 308)*/
    _1351 = power(10, 308);
    _0 = _mantissa_2906;
    if (IS_ATOM_INT(_mantissa_2906) && IS_ATOM_INT(_1351)) {
        if (_mantissa_2906 == (short)_mantissa_2906 && _1351 <= INT15 && _1351 >= -INT15)
        _mantissa_2906 = _mantissa_2906 * _1351;
        else
        _mantissa_2906 = NewDouble(_mantissa_2906 * (double)_1351);
    }
    else {
        if (IS_ATOM_INT(_mantissa_2906)) {
            _mantissa_2906 = NewDouble((double)_mantissa_2906 * DBL_PTR(_1351)->dbl);
        }
        else {
            if (IS_ATOM_INT(_1351)) {
                _mantissa_2906 = NewDouble(DBL_PTR(_mantissa_2906)->dbl * (double)_1351);
            }
            else
            _mantissa_2906 = NewDouble(DBL_PTR(_mantissa_2906)->dbl * DBL_PTR(_1351)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_1351);
    _1351 = NOVALUE;

    /** 			if e_mag > 1000 then*/
    if (binary_op_a(LESSEQ, _e_mag_2908, 1000)){
        goto L17; // [522] 532
    }

    /** 				e_mag = 1000*/
    DeRef(_e_mag_2908);
    _e_mag_2908 = 1000;
L17: 

    /** 			for i = 1 to e_mag - 308 do*/
    if (IS_ATOM_INT(_e_mag_2908)) {
        _1355 = _e_mag_2908 - 308;
        if ((long)((unsigned long)_1355 +(unsigned long) HIGH_BITS) >= 0){
            _1355 = NewDouble((double)_1355);
        }
    }
    else {
        _1355 = NewDouble(DBL_PTR(_e_mag_2908)->dbl - (double)308);
    }
    {
        int _i_2988;
        _i_2988 = 1;
L18: 
        if (binary_op_a(GREATER, _i_2988, _1355)){
            goto L19; // [538] 558
        }

        /** 				mantissa *= 10*/
        _0 = _mantissa_2906;
        if (IS_ATOM_INT(_mantissa_2906)) {
            if (_mantissa_2906 == (short)_mantissa_2906)
            _mantissa_2906 = _mantissa_2906 * 10;
            else
            _mantissa_2906 = NewDouble(_mantissa_2906 * (double)10);
        }
        else {
            _mantissa_2906 = NewDouble(DBL_PTR(_mantissa_2906)->dbl * (double)10);
        }
        DeRef(_0);

        /** 			end for*/
        _0 = _i_2988;
        if (IS_ATOM_INT(_i_2988)) {
            _i_2988 = _i_2988 + 1;
            if ((long)((unsigned long)_i_2988 +(unsigned long) HIGH_BITS) >= 0){
                _i_2988 = NewDouble((double)_i_2988);
            }
        }
        else {
            _i_2988 = binary_op_a(PLUS, _i_2988, 1);
        }
        DeRef(_0);
        goto L18; // [553] 545
L19: 
        ;
        DeRef(_i_2988);
    }
    goto L1A; // [558] 572
L16: 

    /** 			mantissa *= power(10, e_mag)*/
    if (IS_ATOM_INT(_e_mag_2908)) {
        _1357 = power(10, _e_mag_2908);
    }
    else {
        temp_d.dbl = (double)10;
        _1357 = Dpower(&temp_d, DBL_PTR(_e_mag_2908));
    }
    _0 = _mantissa_2906;
    if (IS_ATOM_INT(_mantissa_2906) && IS_ATOM_INT(_1357)) {
        if (_mantissa_2906 == (short)_mantissa_2906 && _1357 <= INT15 && _1357 >= -INT15)
        _mantissa_2906 = _mantissa_2906 * _1357;
        else
        _mantissa_2906 = NewDouble(_mantissa_2906 * (double)_1357);
    }
    else {
        if (IS_ATOM_INT(_mantissa_2906)) {
            _mantissa_2906 = NewDouble((double)_mantissa_2906 * DBL_PTR(_1357)->dbl);
        }
        else {
            if (IS_ATOM_INT(_1357)) {
                _mantissa_2906 = NewDouble(DBL_PTR(_mantissa_2906)->dbl * (double)_1357);
            }
            else
            _mantissa_2906 = NewDouble(DBL_PTR(_mantissa_2906)->dbl * DBL_PTR(_1357)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_1357);
    _1357 = NOVALUE;
L1A: 
LF: 

    /** 	return {GET_SUCCESS, mantissa}*/
    Ref(_mantissa_2906);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _mantissa_2906;
    _1359 = MAKE_SEQ(_1);
    DeRef(_mantissa_2906);
    DeRef(_dec_2907);
    DeRef(_e_mag_2908);
    DeRef(_1300);
    _1300 = NOVALUE;
    DeRef(_1311);
    _1311 = NOVALUE;
    DeRef(_1312);
    _1312 = NOVALUE;
    DeRef(_1313);
    _1313 = NOVALUE;
    DeRef(_1321);
    _1321 = NOVALUE;
    DeRef(_1330);
    _1330 = NOVALUE;
    DeRef(_1332);
    _1332 = NOVALUE;
    DeRef(_1337);
    _1337 = NOVALUE;
    DeRef(_1341);
    _1341 = NOVALUE;
    DeRef(_1347);
    _1347 = NOVALUE;
    DeRef(_1355);
    _1355 = NOVALUE;
    return _1359;
    ;
}


int _15Get()
{
    int _skip_blanks_1__tmp_at328_3041 = NOVALUE;
    int _skip_blanks_1__tmp_at177_3022 = NOVALUE;
    int _skip_blanks_1__tmp_at88_3013 = NOVALUE;
    int _s_2997 = NOVALUE;
    int _e_2998 = NOVALUE;
    int _e1_2999 = NOVALUE;
    int _1398 = NOVALUE;
    int _1397 = NOVALUE;
    int _1395 = NOVALUE;
    int _1392 = NOVALUE;
    int _1390 = NOVALUE;
    int _1388 = NOVALUE;
    int _1386 = NOVALUE;
    int _1383 = NOVALUE;
    int _1381 = NOVALUE;
    int _1377 = NOVALUE;
    int _1373 = NOVALUE;
    int _1370 = NOVALUE;
    int _1369 = NOVALUE;
    int _1367 = NOVALUE;
    int _1365 = NOVALUE;
    int _1363 = NOVALUE;
    int _1362 = NOVALUE;
    int _1360 = NOVALUE;
    int _0, _1, _2;
    

    /** 	while find(ch, white_space) do*/
L1: 
    _1360 = find_from(_15ch_2763, _15white_space_2779, 1);
    if (_1360 == 0)
    {
        _1360 = NOVALUE;
        goto L2; // [13] 25
    }
    else{
        _1360 = NOVALUE;
    }

    /** 		get_ch()*/
    _15get_ch();

    /** 	end while*/
    goto L1; // [22] 6
L2: 

    /** 	if ch = -1 then -- string is made of whitespace only*/
    if (_15ch_2763 != -1)
    goto L3; // [29] 44

    /** 		return {GET_EOF, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = 0;
    _1362 = MAKE_SEQ(_1);
    DeRef(_s_2997);
    DeRef(_e_2998);
    return _1362;
L3: 

    /** 	while 1 do*/
L4: 

    /** 		if find(ch, START_NUMERIC) then*/
    _1363 = find_from(_15ch_2763, _15START_NUMERIC_2746, 1);
    if (_1363 == 0)
    {
        _1363 = NOVALUE;
        goto L5; // [60] 157
    }
    else{
        _1363 = NOVALUE;
    }

    /** 			e = get_number()*/
    _0 = _e_2998;
    _e_2998 = _15get_number();
    DeRef(_0);

    /** 			if e[1] != GET_IGNORE then -- either a number or something illegal was read, so exit: the other goto*/
    _2 = (int)SEQ_PTR(_e_2998);
    _1365 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _1365, -2)){
        _1365 = NOVALUE;
        goto L6; // [76] 87
    }
    _1365 = NOVALUE;

    /** 				return e*/
    DeRef(_s_2997);
    DeRef(_1362);
    _1362 = NOVALUE;
    return _e_2998;
L6: 

    /** 			skip_blanks()*/

    /** 	while find(ch, white_space) do*/
L7: 
    _skip_blanks_1__tmp_at88_3013 = find_from(_15ch_2763, _15white_space_2779, 1);
    if (_skip_blanks_1__tmp_at88_3013 == 0)
    {
        goto L8; // [101] 118
    }
    else{
    }

    /** 		get_ch()*/
    _15get_ch();

    /** 	end while*/
    goto L7; // [110] 94

    /** end procedure*/
    goto L8; // [115] 118
L8: 

    /** 			if ch=-1 or ch='}' then -- '}' is expected only in the "{--\n}" case*/
    _1367 = (_15ch_2763 == -1);
    if (_1367 != 0) {
        goto L9; // [128] 143
    }
    _1369 = (_15ch_2763 == 125);
    if (_1369 == 0)
    {
        DeRef(_1369);
        _1369 = NOVALUE;
        goto L4; // [139] 49
    }
    else{
        DeRef(_1369);
        _1369 = NOVALUE;
    }
L9: 

    /** 				return {GET_NOTHING, 0} -- just a comment*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -2;
    ((int *)_2)[2] = 0;
    _1370 = MAKE_SEQ(_1);
    DeRef(_s_2997);
    DeRef(_e_2998);
    DeRef(_1362);
    _1362 = NOVALUE;
    DeRef(_1367);
    _1367 = NOVALUE;
    return _1370;
    goto L4; // [154] 49
L5: 

    /** 		elsif ch = '{' then*/
    if (_15ch_2763 != 123)
    goto LA; // [161] 465

    /** 			s = {}*/
    RefDS(_5);
    DeRef(_s_2997);
    _s_2997 = _5;

    /** 			get_ch()*/
    _15get_ch();

    /** 			skip_blanks()*/

    /** 	while find(ch, white_space) do*/
LB: 
    _skip_blanks_1__tmp_at177_3022 = find_from(_15ch_2763, _15white_space_2779, 1);
    if (_skip_blanks_1__tmp_at177_3022 == 0)
    {
        goto LC; // [190] 207
    }
    else{
    }

    /** 		get_ch()*/
    _15get_ch();

    /** 	end while*/
    goto LB; // [199] 183

    /** end procedure*/
    goto LC; // [204] 207
LC: 

    /** 			if ch = '}' then -- empty sequence*/
    if (_15ch_2763 != 125)
    goto LD; // [213] 232

    /** 				get_ch()*/
    _15get_ch();

    /** 				return {GET_SUCCESS, s} -- empty sequence*/
    RefDS(_s_2997);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _s_2997;
    _1373 = MAKE_SEQ(_1);
    DeRefDS(_s_2997);
    DeRef(_e_2998);
    DeRef(_1362);
    _1362 = NOVALUE;
    DeRef(_1367);
    _1367 = NOVALUE;
    DeRef(_1370);
    _1370 = NOVALUE;
    return _1373;
LD: 

    /** 			while TRUE do -- read: comment(s), element, comment(s), comma and so on till it terminates or errors out*/
LE: 

    /** 				while 1 do -- read zero or more comments and an element*/
LF: 

    /** 					e = Get() -- read next element, using standard function*/
    _0 = _e_2998;
    _e_2998 = _15Get();
    DeRef(_0);

    /** 					e1 = e[1]*/
    _2 = (int)SEQ_PTR(_e_2998);
    _e1_2999 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_e1_2999))
    _e1_2999 = (long)DBL_PTR(_e1_2999)->dbl;

    /** 					if e1 = GET_SUCCESS then*/
    if (_e1_2999 != 0)
    goto L10; // [257] 278

    /** 						s = append(s, e[2])*/
    _2 = (int)SEQ_PTR(_e_2998);
    _1377 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_1377);
    Append(&_s_2997, _s_2997, _1377);
    _1377 = NOVALUE;

    /** 						exit  -- element read and added to result*/
    goto L11; // [273] 322
    goto LF; // [275] 242
L10: 

    /** 					elsif e1 != GET_IGNORE then*/
    if (_e1_2999 == -2)
    goto L12; // [280] 293

    /** 						return e*/
    DeRef(_s_2997);
    DeRef(_1362);
    _1362 = NOVALUE;
    DeRef(_1367);
    _1367 = NOVALUE;
    DeRef(_1370);
    _1370 = NOVALUE;
    DeRef(_1373);
    _1373 = NOVALUE;
    return _e_2998;
    goto LF; // [290] 242
L12: 

    /** 					elsif ch='}' then*/
    if (_15ch_2763 != 125)
    goto LF; // [297] 242

    /** 						get_ch()*/
    _15get_ch();

    /** 						return {GET_SUCCESS, s} -- empty sequence*/
    RefDS(_s_2997);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _s_2997;
    _1381 = MAKE_SEQ(_1);
    DeRefDS(_s_2997);
    DeRef(_e_2998);
    DeRef(_1362);
    _1362 = NOVALUE;
    DeRef(_1367);
    _1367 = NOVALUE;
    DeRef(_1370);
    _1370 = NOVALUE;
    DeRef(_1373);
    _1373 = NOVALUE;
    return _1381;

    /** 				end while*/
    goto LF; // [319] 242
L11: 

    /** 				while 1 do -- now read zero or more post element comments*/
L13: 

    /** 					skip_blanks()*/

    /** 	while find(ch, white_space) do*/
L14: 
    _skip_blanks_1__tmp_at328_3041 = find_from(_15ch_2763, _15white_space_2779, 1);
    if (_skip_blanks_1__tmp_at328_3041 == 0)
    {
        goto L15; // [341] 358
    }
    else{
    }

    /** 		get_ch()*/
    _15get_ch();

    /** 	end while*/
    goto L14; // [350] 334

    /** end procedure*/
    goto L15; // [355] 358
L15: 

    /** 					if ch = '}' then*/
    if (_15ch_2763 != 125)
    goto L16; // [364] 385

    /** 						get_ch()*/
    _15get_ch();

    /** 					return {GET_SUCCESS, s}*/
    RefDS(_s_2997);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _s_2997;
    _1383 = MAKE_SEQ(_1);
    DeRefDS(_s_2997);
    DeRef(_e_2998);
    DeRef(_1362);
    _1362 = NOVALUE;
    DeRef(_1367);
    _1367 = NOVALUE;
    DeRef(_1370);
    _1370 = NOVALUE;
    DeRef(_1373);
    _1373 = NOVALUE;
    DeRef(_1381);
    _1381 = NOVALUE;
    return _1383;
    goto L13; // [382] 327
L16: 

    /** 					elsif ch!='-' then*/
    if (_15ch_2763 == 45)
    goto L17; // [389] 400

    /** 						exit*/
    goto L18; // [395] 434
    goto L13; // [397] 327
L17: 

    /** 						e = get_number() -- reads anything starting with '-'*/
    _0 = _e_2998;
    _e_2998 = _15get_number();
    DeRef(_0);

    /** 						if e[1] != GET_IGNORE then  -- it wasn't a comment, this is illegal*/
    _2 = (int)SEQ_PTR(_e_2998);
    _1386 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _1386, -2)){
        _1386 = NOVALUE;
        goto L13; // [413] 327
    }
    _1386 = NOVALUE;

    /** 							return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1388 = MAKE_SEQ(_1);
    DeRef(_s_2997);
    DeRefDS(_e_2998);
    DeRef(_1362);
    _1362 = NOVALUE;
    DeRef(_1367);
    _1367 = NOVALUE;
    DeRef(_1370);
    _1370 = NOVALUE;
    DeRef(_1373);
    _1373 = NOVALUE;
    DeRef(_1381);
    _1381 = NOVALUE;
    DeRef(_1383);
    _1383 = NOVALUE;
    return _1388;

    /** 			end while*/
    goto L13; // [431] 327
L18: 

    /** 				if ch != ',' then*/
    if (_15ch_2763 == 44)
    goto L19; // [438] 453

    /** 				return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1390 = MAKE_SEQ(_1);
    DeRef(_s_2997);
    DeRef(_e_2998);
    DeRef(_1362);
    _1362 = NOVALUE;
    DeRef(_1367);
    _1367 = NOVALUE;
    DeRef(_1370);
    _1370 = NOVALUE;
    DeRef(_1373);
    _1373 = NOVALUE;
    DeRef(_1381);
    _1381 = NOVALUE;
    DeRef(_1383);
    _1383 = NOVALUE;
    DeRef(_1388);
    _1388 = NOVALUE;
    return _1390;
L19: 

    /** 			get_ch() -- skip comma*/
    _15get_ch();

    /** 			end while*/
    goto LE; // [459] 237
    goto L4; // [462] 49
LA: 

    /** 		elsif ch = '\"' then*/
    if (_15ch_2763 != 34)
    goto L1A; // [469] 485

    /** 			return get_string()*/
    _1392 = _15get_string();
    DeRef(_s_2997);
    DeRef(_e_2998);
    DeRef(_1362);
    _1362 = NOVALUE;
    DeRef(_1367);
    _1367 = NOVALUE;
    DeRef(_1370);
    _1370 = NOVALUE;
    DeRef(_1373);
    _1373 = NOVALUE;
    DeRef(_1381);
    _1381 = NOVALUE;
    DeRef(_1383);
    _1383 = NOVALUE;
    DeRef(_1388);
    _1388 = NOVALUE;
    DeRef(_1390);
    _1390 = NOVALUE;
    return _1392;
    goto L4; // [482] 49
L1A: 

    /** 		elsif ch = '`' then*/
    if (_15ch_2763 != 96)
    goto L1B; // [489] 506

    /** 			return get_heredoc("`")*/
    RefDS(_1394);
    _1395 = _15get_heredoc(_1394);
    DeRef(_s_2997);
    DeRef(_e_2998);
    DeRef(_1362);
    _1362 = NOVALUE;
    DeRef(_1367);
    _1367 = NOVALUE;
    DeRef(_1370);
    _1370 = NOVALUE;
    DeRef(_1373);
    _1373 = NOVALUE;
    DeRef(_1381);
    _1381 = NOVALUE;
    DeRef(_1383);
    _1383 = NOVALUE;
    DeRef(_1388);
    _1388 = NOVALUE;
    DeRef(_1390);
    _1390 = NOVALUE;
    DeRef(_1392);
    _1392 = NOVALUE;
    return _1395;
    goto L4; // [503] 49
L1B: 

    /** 		elsif ch = '\'' then*/
    if (_15ch_2763 != 39)
    goto L1C; // [510] 526

    /** 			return get_qchar()*/
    _1397 = _15get_qchar();
    DeRef(_s_2997);
    DeRef(_e_2998);
    DeRef(_1362);
    _1362 = NOVALUE;
    DeRef(_1367);
    _1367 = NOVALUE;
    DeRef(_1370);
    _1370 = NOVALUE;
    DeRef(_1373);
    _1373 = NOVALUE;
    DeRef(_1381);
    _1381 = NOVALUE;
    DeRef(_1383);
    _1383 = NOVALUE;
    DeRef(_1388);
    _1388 = NOVALUE;
    DeRef(_1390);
    _1390 = NOVALUE;
    DeRef(_1392);
    _1392 = NOVALUE;
    DeRef(_1395);
    _1395 = NOVALUE;
    return _1397;
    goto L4; // [523] 49
L1C: 

    /** 			return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1398 = MAKE_SEQ(_1);
    DeRef(_s_2997);
    DeRef(_e_2998);
    DeRef(_1362);
    _1362 = NOVALUE;
    DeRef(_1367);
    _1367 = NOVALUE;
    DeRef(_1370);
    _1370 = NOVALUE;
    DeRef(_1373);
    _1373 = NOVALUE;
    DeRef(_1381);
    _1381 = NOVALUE;
    DeRef(_1383);
    _1383 = NOVALUE;
    DeRef(_1388);
    _1388 = NOVALUE;
    DeRef(_1390);
    _1390 = NOVALUE;
    DeRef(_1392);
    _1392 = NOVALUE;
    DeRef(_1395);
    _1395 = NOVALUE;
    DeRef(_1397);
    _1397 = NOVALUE;
    return _1398;

    /** 	end while*/
    goto L4; // [539] 49
    ;
}


int _15Get2()
{
    int _skip_blanks_1__tmp_at464_3142 = NOVALUE;
    int _skip_blanks_1__tmp_at233_3109 = NOVALUE;
    int _s_3071 = NOVALUE;
    int _e_3072 = NOVALUE;
    int _e1_3073 = NOVALUE;
    int _offset_3074 = NOVALUE;
    int _1498 = NOVALUE;
    int _1497 = NOVALUE;
    int _1496 = NOVALUE;
    int _1495 = NOVALUE;
    int _1494 = NOVALUE;
    int _1493 = NOVALUE;
    int _1492 = NOVALUE;
    int _1491 = NOVALUE;
    int _1490 = NOVALUE;
    int _1489 = NOVALUE;
    int _1488 = NOVALUE;
    int _1485 = NOVALUE;
    int _1484 = NOVALUE;
    int _1483 = NOVALUE;
    int _1482 = NOVALUE;
    int _1481 = NOVALUE;
    int _1480 = NOVALUE;
    int _1477 = NOVALUE;
    int _1476 = NOVALUE;
    int _1475 = NOVALUE;
    int _1474 = NOVALUE;
    int _1473 = NOVALUE;
    int _1472 = NOVALUE;
    int _1469 = NOVALUE;
    int _1468 = NOVALUE;
    int _1467 = NOVALUE;
    int _1466 = NOVALUE;
    int _1465 = NOVALUE;
    int _1463 = NOVALUE;
    int _1462 = NOVALUE;
    int _1461 = NOVALUE;
    int _1460 = NOVALUE;
    int _1459 = NOVALUE;
    int _1457 = NOVALUE;
    int _1454 = NOVALUE;
    int _1453 = NOVALUE;
    int _1452 = NOVALUE;
    int _1451 = NOVALUE;
    int _1450 = NOVALUE;
    int _1448 = NOVALUE;
    int _1447 = NOVALUE;
    int _1446 = NOVALUE;
    int _1445 = NOVALUE;
    int _1444 = NOVALUE;
    int _1442 = NOVALUE;
    int _1441 = NOVALUE;
    int _1440 = NOVALUE;
    int _1439 = NOVALUE;
    int _1438 = NOVALUE;
    int _1437 = NOVALUE;
    int _1434 = NOVALUE;
    int _1430 = NOVALUE;
    int _1429 = NOVALUE;
    int _1428 = NOVALUE;
    int _1427 = NOVALUE;
    int _1426 = NOVALUE;
    int _1423 = NOVALUE;
    int _1422 = NOVALUE;
    int _1421 = NOVALUE;
    int _1420 = NOVALUE;
    int _1419 = NOVALUE;
    int _1417 = NOVALUE;
    int _1416 = NOVALUE;
    int _1415 = NOVALUE;
    int _1414 = NOVALUE;
    int _1413 = NOVALUE;
    int _1412 = NOVALUE;
    int _1410 = NOVALUE;
    int _1408 = NOVALUE;
    int _1406 = NOVALUE;
    int _1405 = NOVALUE;
    int _1404 = NOVALUE;
    int _1403 = NOVALUE;
    int _1402 = NOVALUE;
    int _1400 = NOVALUE;
    int _0, _1, _2;
    

    /** 	offset = string_next-1*/
    _offset_3074 = _15string_next_2762 - 1;

    /** 	get_ch()*/
    _15get_ch();

    /** 	while find(ch, white_space) do*/
L1: 
    _1400 = find_from(_15ch_2763, _15white_space_2779, 1);
    if (_1400 == 0)
    {
        _1400 = NOVALUE;
        goto L2; // [25] 37
    }
    else{
        _1400 = NOVALUE;
    }

    /** 		get_ch()*/
    _15get_ch();

    /** 	end while*/
    goto L1; // [34] 18
L2: 

    /** 	if ch = -1 then -- string is made of whitespace only*/
    if (_15ch_2763 != -1)
    goto L3; // [41] 75

    /** 		return {GET_EOF, 0, string_next-1-offset ,string_next-1}*/
    _1402 = _15string_next_2762 - 1;
    if ((long)((unsigned long)_1402 +(unsigned long) HIGH_BITS) >= 0){
        _1402 = NewDouble((double)_1402);
    }
    if (IS_ATOM_INT(_1402)) {
        _1403 = _1402 - _offset_3074;
        if ((long)((unsigned long)_1403 +(unsigned long) HIGH_BITS) >= 0){
            _1403 = NewDouble((double)_1403);
        }
    }
    else {
        _1403 = NewDouble(DBL_PTR(_1402)->dbl - (double)_offset_3074);
    }
    DeRef(_1402);
    _1402 = NOVALUE;
    _1404 = _15string_next_2762 - 1;
    if ((long)((unsigned long)_1404 +(unsigned long) HIGH_BITS) >= 0){
        _1404 = NewDouble((double)_1404);
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -1;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _1403;
    *((int *)(_2+16)) = _1404;
    _1405 = MAKE_SEQ(_1);
    _1404 = NOVALUE;
    _1403 = NOVALUE;
    DeRef(_s_3071);
    DeRef(_e_3072);
    return _1405;
L3: 

    /** 	leading_whitespace = string_next-2-offset -- index of the last whitespace: string_next points past the first non whitespace*/
    _1406 = _15string_next_2762 - 2;
    if ((long)((unsigned long)_1406 +(unsigned long) HIGH_BITS) >= 0){
        _1406 = NewDouble((double)_1406);
    }
    if (IS_ATOM_INT(_1406)) {
        _15leading_whitespace_3068 = _1406 - _offset_3074;
    }
    else {
        _15leading_whitespace_3068 = NewDouble(DBL_PTR(_1406)->dbl - (double)_offset_3074);
    }
    DeRef(_1406);
    _1406 = NOVALUE;
    if (!IS_ATOM_INT(_15leading_whitespace_3068)) {
        _1 = (long)(DBL_PTR(_15leading_whitespace_3068)->dbl);
        if (UNIQUE(DBL_PTR(_15leading_whitespace_3068)) && (DBL_PTR(_15leading_whitespace_3068)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_15leading_whitespace_3068);
        _15leading_whitespace_3068 = _1;
    }

    /** 	while 1 do*/
L4: 

    /** 		if find(ch, START_NUMERIC) then*/
    _1408 = find_from(_15ch_2763, _15START_NUMERIC_2746, 1);
    if (_1408 == 0)
    {
        _1408 = NOVALUE;
        goto L5; // [105] 213
    }
    else{
        _1408 = NOVALUE;
    }

    /** 			e = get_number()*/
    _0 = _e_3072;
    _e_3072 = _15get_number();
    DeRef(_0);

    /** 			if e[1] != GET_IGNORE then -- either a number or something illegal was read, so exit: the other goto*/
    _2 = (int)SEQ_PTR(_e_3072);
    _1410 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _1410, -2)){
        _1410 = NOVALUE;
        goto L6; // [121] 162
    }
    _1410 = NOVALUE;

    /** 				return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1412 = _15string_next_2762 - 1;
    if ((long)((unsigned long)_1412 +(unsigned long) HIGH_BITS) >= 0){
        _1412 = NewDouble((double)_1412);
    }
    if (IS_ATOM_INT(_1412)) {
        _1413 = _1412 - _offset_3074;
        if ((long)((unsigned long)_1413 +(unsigned long) HIGH_BITS) >= 0){
            _1413 = NewDouble((double)_1413);
        }
    }
    else {
        _1413 = NewDouble(DBL_PTR(_1412)->dbl - (double)_offset_3074);
    }
    DeRef(_1412);
    _1412 = NOVALUE;
    _1414 = (_15ch_2763 != -1);
    if (IS_ATOM_INT(_1413)) {
        _1415 = _1413 - _1414;
        if ((long)((unsigned long)_1415 +(unsigned long) HIGH_BITS) >= 0){
            _1415 = NewDouble((double)_1415);
        }
    }
    else {
        _1415 = NewDouble(DBL_PTR(_1413)->dbl - (double)_1414);
    }
    DeRef(_1413);
    _1413 = NOVALUE;
    _1414 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _1415;
    ((int *)_2)[2] = _15leading_whitespace_3068;
    _1416 = MAKE_SEQ(_1);
    _1415 = NOVALUE;
    Concat((object_ptr)&_1417, _e_3072, _1416);
    DeRefDS(_1416);
    _1416 = NOVALUE;
    DeRef(_s_3071);
    DeRefDS(_e_3072);
    DeRef(_1405);
    _1405 = NOVALUE;
    return _1417;
L6: 

    /** 			get_ch()*/
    _15get_ch();

    /** 			if ch=-1 then*/
    if (_15ch_2763 != -1)
    goto L4; // [170] 94

    /** 				return {GET_NOTHING, 0, string_next-1-offset-(ch!=-1), leading_whitespace} -- empty sequence*/
    _1419 = _15string_next_2762 - 1;
    if ((long)((unsigned long)_1419 +(unsigned long) HIGH_BITS) >= 0){
        _1419 = NewDouble((double)_1419);
    }
    if (IS_ATOM_INT(_1419)) {
        _1420 = _1419 - _offset_3074;
        if ((long)((unsigned long)_1420 +(unsigned long) HIGH_BITS) >= 0){
            _1420 = NewDouble((double)_1420);
        }
    }
    else {
        _1420 = NewDouble(DBL_PTR(_1419)->dbl - (double)_offset_3074);
    }
    DeRef(_1419);
    _1419 = NOVALUE;
    _1421 = (_15ch_2763 != -1);
    if (IS_ATOM_INT(_1420)) {
        _1422 = _1420 - _1421;
        if ((long)((unsigned long)_1422 +(unsigned long) HIGH_BITS) >= 0){
            _1422 = NewDouble((double)_1422);
        }
    }
    else {
        _1422 = NewDouble(DBL_PTR(_1420)->dbl - (double)_1421);
    }
    DeRef(_1420);
    _1420 = NOVALUE;
    _1421 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -2;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _1422;
    *((int *)(_2+16)) = _15leading_whitespace_3068;
    _1423 = MAKE_SEQ(_1);
    _1422 = NOVALUE;
    DeRef(_s_3071);
    DeRef(_e_3072);
    DeRef(_1405);
    _1405 = NOVALUE;
    DeRef(_1417);
    _1417 = NOVALUE;
    return _1423;
    goto L4; // [210] 94
L5: 

    /** 		elsif ch = '{' then*/
    if (_15ch_2763 != 123)
    goto L7; // [217] 676

    /** 			s = {}*/
    RefDS(_5);
    DeRef(_s_3071);
    _s_3071 = _5;

    /** 			get_ch()*/
    _15get_ch();

    /** 			skip_blanks()*/

    /** 	while find(ch, white_space) do*/
L8: 
    _skip_blanks_1__tmp_at233_3109 = find_from(_15ch_2763, _15white_space_2779, 1);
    if (_skip_blanks_1__tmp_at233_3109 == 0)
    {
        goto L9; // [246] 263
    }
    else{
    }

    /** 		get_ch()*/
    _15get_ch();

    /** 	end while*/
    goto L8; // [255] 239

    /** end procedure*/
    goto L9; // [260] 263
L9: 

    /** 			if ch = '}' then -- empty sequence*/
    if (_15ch_2763 != 125)
    goto LA; // [269] 313

    /** 				get_ch()*/
    _15get_ch();

    /** 				return {GET_SUCCESS, s, string_next-1-offset-(ch!=-1), leading_whitespace} -- empty sequence*/
    _1426 = _15string_next_2762 - 1;
    if ((long)((unsigned long)_1426 +(unsigned long) HIGH_BITS) >= 0){
        _1426 = NewDouble((double)_1426);
    }
    if (IS_ATOM_INT(_1426)) {
        _1427 = _1426 - _offset_3074;
        if ((long)((unsigned long)_1427 +(unsigned long) HIGH_BITS) >= 0){
            _1427 = NewDouble((double)_1427);
        }
    }
    else {
        _1427 = NewDouble(DBL_PTR(_1426)->dbl - (double)_offset_3074);
    }
    DeRef(_1426);
    _1426 = NOVALUE;
    _1428 = (_15ch_2763 != -1);
    if (IS_ATOM_INT(_1427)) {
        _1429 = _1427 - _1428;
        if ((long)((unsigned long)_1429 +(unsigned long) HIGH_BITS) >= 0){
            _1429 = NewDouble((double)_1429);
        }
    }
    else {
        _1429 = NewDouble(DBL_PTR(_1427)->dbl - (double)_1428);
    }
    DeRef(_1427);
    _1427 = NOVALUE;
    _1428 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    RefDS(_s_3071);
    *((int *)(_2+8)) = _s_3071;
    *((int *)(_2+12)) = _1429;
    *((int *)(_2+16)) = _15leading_whitespace_3068;
    _1430 = MAKE_SEQ(_1);
    _1429 = NOVALUE;
    DeRefDS(_s_3071);
    DeRef(_e_3072);
    DeRef(_1405);
    _1405 = NOVALUE;
    DeRef(_1417);
    _1417 = NOVALUE;
    DeRef(_1423);
    _1423 = NOVALUE;
    return _1430;
LA: 

    /** 			while TRUE do -- read: comment(s), element, comment(s), comma and so on till it terminates or errors out*/
LB: 

    /** 				while 1 do -- read zero or more comments and an element*/
LC: 

    /** 					e = Get() -- read next element, using standard function*/
    _0 = _e_3072;
    _e_3072 = _15Get();
    DeRef(_0);

    /** 					e1 = e[1]*/
    _2 = (int)SEQ_PTR(_e_3072);
    _e1_3073 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_e1_3073))
    _e1_3073 = (long)DBL_PTR(_e1_3073)->dbl;

    /** 					if e1 = GET_SUCCESS then*/
    if (_e1_3073 != 0)
    goto LD; // [338] 359

    /** 						s = append(s, e[2])*/
    _2 = (int)SEQ_PTR(_e_3072);
    _1434 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_1434);
    Append(&_s_3071, _s_3071, _1434);
    _1434 = NOVALUE;

    /** 						exit  -- element read and added to result*/
    goto LE; // [354] 458
    goto LC; // [356] 323
LD: 

    /** 					elsif e1 != GET_IGNORE then*/
    if (_e1_3073 == -2)
    goto LF; // [361] 404

    /** 						return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1437 = _15string_next_2762 - 1;
    if ((long)((unsigned long)_1437 +(unsigned long) HIGH_BITS) >= 0){
        _1437 = NewDouble((double)_1437);
    }
    if (IS_ATOM_INT(_1437)) {
        _1438 = _1437 - _offset_3074;
        if ((long)((unsigned long)_1438 +(unsigned long) HIGH_BITS) >= 0){
            _1438 = NewDouble((double)_1438);
        }
    }
    else {
        _1438 = NewDouble(DBL_PTR(_1437)->dbl - (double)_offset_3074);
    }
    DeRef(_1437);
    _1437 = NOVALUE;
    _1439 = (_15ch_2763 != -1);
    if (IS_ATOM_INT(_1438)) {
        _1440 = _1438 - _1439;
        if ((long)((unsigned long)_1440 +(unsigned long) HIGH_BITS) >= 0){
            _1440 = NewDouble((double)_1440);
        }
    }
    else {
        _1440 = NewDouble(DBL_PTR(_1438)->dbl - (double)_1439);
    }
    DeRef(_1438);
    _1438 = NOVALUE;
    _1439 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _1440;
    ((int *)_2)[2] = _15leading_whitespace_3068;
    _1441 = MAKE_SEQ(_1);
    _1440 = NOVALUE;
    Concat((object_ptr)&_1442, _e_3072, _1441);
    DeRefDS(_1441);
    _1441 = NOVALUE;
    DeRef(_s_3071);
    DeRefDS(_e_3072);
    DeRef(_1405);
    _1405 = NOVALUE;
    DeRef(_1417);
    _1417 = NOVALUE;
    DeRef(_1423);
    _1423 = NOVALUE;
    DeRef(_1430);
    _1430 = NOVALUE;
    return _1442;
    goto LC; // [401] 323
LF: 

    /** 					elsif ch='}' then*/
    if (_15ch_2763 != 125)
    goto LC; // [408] 323

    /** 						get_ch()*/
    _15get_ch();

    /** 						return {GET_SUCCESS, s, string_next-1-offset-(ch!=-1),leading_whitespace} -- empty sequence*/
    _1444 = _15string_next_2762 - 1;
    if ((long)((unsigned long)_1444 +(unsigned long) HIGH_BITS) >= 0){
        _1444 = NewDouble((double)_1444);
    }
    if (IS_ATOM_INT(_1444)) {
        _1445 = _1444 - _offset_3074;
        if ((long)((unsigned long)_1445 +(unsigned long) HIGH_BITS) >= 0){
            _1445 = NewDouble((double)_1445);
        }
    }
    else {
        _1445 = NewDouble(DBL_PTR(_1444)->dbl - (double)_offset_3074);
    }
    DeRef(_1444);
    _1444 = NOVALUE;
    _1446 = (_15ch_2763 != -1);
    if (IS_ATOM_INT(_1445)) {
        _1447 = _1445 - _1446;
        if ((long)((unsigned long)_1447 +(unsigned long) HIGH_BITS) >= 0){
            _1447 = NewDouble((double)_1447);
        }
    }
    else {
        _1447 = NewDouble(DBL_PTR(_1445)->dbl - (double)_1446);
    }
    DeRef(_1445);
    _1445 = NOVALUE;
    _1446 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    RefDS(_s_3071);
    *((int *)(_2+8)) = _s_3071;
    *((int *)(_2+12)) = _1447;
    *((int *)(_2+16)) = _15leading_whitespace_3068;
    _1448 = MAKE_SEQ(_1);
    _1447 = NOVALUE;
    DeRefDS(_s_3071);
    DeRef(_e_3072);
    DeRef(_1405);
    _1405 = NOVALUE;
    DeRef(_1417);
    _1417 = NOVALUE;
    DeRef(_1423);
    _1423 = NOVALUE;
    DeRef(_1430);
    _1430 = NOVALUE;
    DeRef(_1442);
    _1442 = NOVALUE;
    return _1448;

    /** 				end while*/
    goto LC; // [455] 323
LE: 

    /** 				while 1 do -- now read zero or more post element comments*/
L10: 

    /** 					skip_blanks()*/

    /** 	while find(ch, white_space) do*/
L11: 
    _skip_blanks_1__tmp_at464_3142 = find_from(_15ch_2763, _15white_space_2779, 1);
    if (_skip_blanks_1__tmp_at464_3142 == 0)
    {
        goto L12; // [477] 494
    }
    else{
    }

    /** 		get_ch()*/
    _15get_ch();

    /** 	end while*/
    goto L11; // [486] 470

    /** end procedure*/
    goto L12; // [491] 494
L12: 

    /** 					if ch = '}' then*/
    if (_15ch_2763 != 125)
    goto L13; // [500] 546

    /** 						get_ch()*/
    _15get_ch();

    /** 					return {GET_SUCCESS, s, string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1450 = _15string_next_2762 - 1;
    if ((long)((unsigned long)_1450 +(unsigned long) HIGH_BITS) >= 0){
        _1450 = NewDouble((double)_1450);
    }
    if (IS_ATOM_INT(_1450)) {
        _1451 = _1450 - _offset_3074;
        if ((long)((unsigned long)_1451 +(unsigned long) HIGH_BITS) >= 0){
            _1451 = NewDouble((double)_1451);
        }
    }
    else {
        _1451 = NewDouble(DBL_PTR(_1450)->dbl - (double)_offset_3074);
    }
    DeRef(_1450);
    _1450 = NOVALUE;
    _1452 = (_15ch_2763 != -1);
    if (IS_ATOM_INT(_1451)) {
        _1453 = _1451 - _1452;
        if ((long)((unsigned long)_1453 +(unsigned long) HIGH_BITS) >= 0){
            _1453 = NewDouble((double)_1453);
        }
    }
    else {
        _1453 = NewDouble(DBL_PTR(_1451)->dbl - (double)_1452);
    }
    DeRef(_1451);
    _1451 = NOVALUE;
    _1452 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    RefDS(_s_3071);
    *((int *)(_2+8)) = _s_3071;
    *((int *)(_2+12)) = _1453;
    *((int *)(_2+16)) = _15leading_whitespace_3068;
    _1454 = MAKE_SEQ(_1);
    _1453 = NOVALUE;
    DeRefDS(_s_3071);
    DeRef(_e_3072);
    DeRef(_1405);
    _1405 = NOVALUE;
    DeRef(_1417);
    _1417 = NOVALUE;
    DeRef(_1423);
    _1423 = NOVALUE;
    DeRef(_1430);
    _1430 = NOVALUE;
    DeRef(_1442);
    _1442 = NOVALUE;
    DeRef(_1448);
    _1448 = NOVALUE;
    return _1454;
    goto L10; // [543] 463
L13: 

    /** 					elsif ch!='-' then*/
    if (_15ch_2763 == 45)
    goto L14; // [550] 561

    /** 						exit*/
    goto L15; // [556] 620
    goto L10; // [558] 463
L14: 

    /** 						e = get_number() -- reads anything starting with '-'*/
    _0 = _e_3072;
    _e_3072 = _15get_number();
    DeRef(_0);

    /** 						if e[1] != GET_IGNORE then  -- it was not a comment, this is illegal*/
    _2 = (int)SEQ_PTR(_e_3072);
    _1457 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _1457, -2)){
        _1457 = NOVALUE;
        goto L10; // [574] 463
    }
    _1457 = NOVALUE;

    /** 							return {GET_FAIL, 0, string_next-1-offset-(ch!=-1),leading_whitespace}*/
    _1459 = _15string_next_2762 - 1;
    if ((long)((unsigned long)_1459 +(unsigned long) HIGH_BITS) >= 0){
        _1459 = NewDouble((double)_1459);
    }
    if (IS_ATOM_INT(_1459)) {
        _1460 = _1459 - _offset_3074;
        if ((long)((unsigned long)_1460 +(unsigned long) HIGH_BITS) >= 0){
            _1460 = NewDouble((double)_1460);
        }
    }
    else {
        _1460 = NewDouble(DBL_PTR(_1459)->dbl - (double)_offset_3074);
    }
    DeRef(_1459);
    _1459 = NOVALUE;
    _1461 = (_15ch_2763 != -1);
    if (IS_ATOM_INT(_1460)) {
        _1462 = _1460 - _1461;
        if ((long)((unsigned long)_1462 +(unsigned long) HIGH_BITS) >= 0){
            _1462 = NewDouble((double)_1462);
        }
    }
    else {
        _1462 = NewDouble(DBL_PTR(_1460)->dbl - (double)_1461);
    }
    DeRef(_1460);
    _1460 = NOVALUE;
    _1461 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 1;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _1462;
    *((int *)(_2+16)) = _15leading_whitespace_3068;
    _1463 = MAKE_SEQ(_1);
    _1462 = NOVALUE;
    DeRef(_s_3071);
    DeRefDS(_e_3072);
    DeRef(_1405);
    _1405 = NOVALUE;
    DeRef(_1417);
    _1417 = NOVALUE;
    DeRef(_1423);
    _1423 = NOVALUE;
    DeRef(_1430);
    _1430 = NOVALUE;
    DeRef(_1442);
    _1442 = NOVALUE;
    DeRef(_1448);
    _1448 = NOVALUE;
    DeRef(_1454);
    _1454 = NOVALUE;
    return _1463;

    /** 			end while*/
    goto L10; // [617] 463
L15: 

    /** 				if ch != ',' then*/
    if (_15ch_2763 == 44)
    goto L16; // [624] 664

    /** 				return {GET_FAIL, 0, string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1465 = _15string_next_2762 - 1;
    if ((long)((unsigned long)_1465 +(unsigned long) HIGH_BITS) >= 0){
        _1465 = NewDouble((double)_1465);
    }
    if (IS_ATOM_INT(_1465)) {
        _1466 = _1465 - _offset_3074;
        if ((long)((unsigned long)_1466 +(unsigned long) HIGH_BITS) >= 0){
            _1466 = NewDouble((double)_1466);
        }
    }
    else {
        _1466 = NewDouble(DBL_PTR(_1465)->dbl - (double)_offset_3074);
    }
    DeRef(_1465);
    _1465 = NOVALUE;
    _1467 = (_15ch_2763 != -1);
    if (IS_ATOM_INT(_1466)) {
        _1468 = _1466 - _1467;
        if ((long)((unsigned long)_1468 +(unsigned long) HIGH_BITS) >= 0){
            _1468 = NewDouble((double)_1468);
        }
    }
    else {
        _1468 = NewDouble(DBL_PTR(_1466)->dbl - (double)_1467);
    }
    DeRef(_1466);
    _1466 = NOVALUE;
    _1467 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 1;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _1468;
    *((int *)(_2+16)) = _15leading_whitespace_3068;
    _1469 = MAKE_SEQ(_1);
    _1468 = NOVALUE;
    DeRef(_s_3071);
    DeRef(_e_3072);
    DeRef(_1405);
    _1405 = NOVALUE;
    DeRef(_1417);
    _1417 = NOVALUE;
    DeRef(_1423);
    _1423 = NOVALUE;
    DeRef(_1430);
    _1430 = NOVALUE;
    DeRef(_1442);
    _1442 = NOVALUE;
    DeRef(_1448);
    _1448 = NOVALUE;
    DeRef(_1454);
    _1454 = NOVALUE;
    DeRef(_1463);
    _1463 = NOVALUE;
    return _1469;
L16: 

    /** 			get_ch() -- skip comma*/
    _15get_ch();

    /** 			end while*/
    goto LB; // [670] 318
    goto L4; // [673] 94
L7: 

    /** 		elsif ch = '\"' then*/
    if (_15ch_2763 != 34)
    goto L17; // [680] 730

    /** 			e = get_string()*/
    _0 = _e_3072;
    _e_3072 = _15get_string();
    DeRef(_0);

    /** 			return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1472 = _15string_next_2762 - 1;
    if ((long)((unsigned long)_1472 +(unsigned long) HIGH_BITS) >= 0){
        _1472 = NewDouble((double)_1472);
    }
    if (IS_ATOM_INT(_1472)) {
        _1473 = _1472 - _offset_3074;
        if ((long)((unsigned long)_1473 +(unsigned long) HIGH_BITS) >= 0){
            _1473 = NewDouble((double)_1473);
        }
    }
    else {
        _1473 = NewDouble(DBL_PTR(_1472)->dbl - (double)_offset_3074);
    }
    DeRef(_1472);
    _1472 = NOVALUE;
    _1474 = (_15ch_2763 != -1);
    if (IS_ATOM_INT(_1473)) {
        _1475 = _1473 - _1474;
        if ((long)((unsigned long)_1475 +(unsigned long) HIGH_BITS) >= 0){
            _1475 = NewDouble((double)_1475);
        }
    }
    else {
        _1475 = NewDouble(DBL_PTR(_1473)->dbl - (double)_1474);
    }
    DeRef(_1473);
    _1473 = NOVALUE;
    _1474 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _1475;
    ((int *)_2)[2] = _15leading_whitespace_3068;
    _1476 = MAKE_SEQ(_1);
    _1475 = NOVALUE;
    Concat((object_ptr)&_1477, _e_3072, _1476);
    DeRefDS(_1476);
    _1476 = NOVALUE;
    DeRef(_s_3071);
    DeRefDS(_e_3072);
    DeRef(_1405);
    _1405 = NOVALUE;
    DeRef(_1417);
    _1417 = NOVALUE;
    DeRef(_1423);
    _1423 = NOVALUE;
    DeRef(_1430);
    _1430 = NOVALUE;
    DeRef(_1442);
    _1442 = NOVALUE;
    DeRef(_1448);
    _1448 = NOVALUE;
    DeRef(_1454);
    _1454 = NOVALUE;
    DeRef(_1463);
    _1463 = NOVALUE;
    DeRef(_1469);
    _1469 = NOVALUE;
    return _1477;
    goto L4; // [727] 94
L17: 

    /** 		elsif ch = '`' then*/
    if (_15ch_2763 != 96)
    goto L18; // [734] 785

    /** 			e = get_heredoc("`")*/
    RefDS(_1394);
    _0 = _e_3072;
    _e_3072 = _15get_heredoc(_1394);
    DeRef(_0);

    /** 			return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1480 = _15string_next_2762 - 1;
    if ((long)((unsigned long)_1480 +(unsigned long) HIGH_BITS) >= 0){
        _1480 = NewDouble((double)_1480);
    }
    if (IS_ATOM_INT(_1480)) {
        _1481 = _1480 - _offset_3074;
        if ((long)((unsigned long)_1481 +(unsigned long) HIGH_BITS) >= 0){
            _1481 = NewDouble((double)_1481);
        }
    }
    else {
        _1481 = NewDouble(DBL_PTR(_1480)->dbl - (double)_offset_3074);
    }
    DeRef(_1480);
    _1480 = NOVALUE;
    _1482 = (_15ch_2763 != -1);
    if (IS_ATOM_INT(_1481)) {
        _1483 = _1481 - _1482;
        if ((long)((unsigned long)_1483 +(unsigned long) HIGH_BITS) >= 0){
            _1483 = NewDouble((double)_1483);
        }
    }
    else {
        _1483 = NewDouble(DBL_PTR(_1481)->dbl - (double)_1482);
    }
    DeRef(_1481);
    _1481 = NOVALUE;
    _1482 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _1483;
    ((int *)_2)[2] = _15leading_whitespace_3068;
    _1484 = MAKE_SEQ(_1);
    _1483 = NOVALUE;
    Concat((object_ptr)&_1485, _e_3072, _1484);
    DeRefDS(_1484);
    _1484 = NOVALUE;
    DeRef(_s_3071);
    DeRefDS(_e_3072);
    DeRef(_1405);
    _1405 = NOVALUE;
    DeRef(_1417);
    _1417 = NOVALUE;
    DeRef(_1423);
    _1423 = NOVALUE;
    DeRef(_1430);
    _1430 = NOVALUE;
    DeRef(_1442);
    _1442 = NOVALUE;
    DeRef(_1448);
    _1448 = NOVALUE;
    DeRef(_1454);
    _1454 = NOVALUE;
    DeRef(_1463);
    _1463 = NOVALUE;
    DeRef(_1469);
    _1469 = NOVALUE;
    DeRef(_1477);
    _1477 = NOVALUE;
    return _1485;
    goto L4; // [782] 94
L18: 

    /** 		elsif ch = '\'' then*/
    if (_15ch_2763 != 39)
    goto L19; // [789] 839

    /** 			e = get_qchar()*/
    _0 = _e_3072;
    _e_3072 = _15get_qchar();
    DeRef(_0);

    /** 			return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1488 = _15string_next_2762 - 1;
    if ((long)((unsigned long)_1488 +(unsigned long) HIGH_BITS) >= 0){
        _1488 = NewDouble((double)_1488);
    }
    if (IS_ATOM_INT(_1488)) {
        _1489 = _1488 - _offset_3074;
        if ((long)((unsigned long)_1489 +(unsigned long) HIGH_BITS) >= 0){
            _1489 = NewDouble((double)_1489);
        }
    }
    else {
        _1489 = NewDouble(DBL_PTR(_1488)->dbl - (double)_offset_3074);
    }
    DeRef(_1488);
    _1488 = NOVALUE;
    _1490 = (_15ch_2763 != -1);
    if (IS_ATOM_INT(_1489)) {
        _1491 = _1489 - _1490;
        if ((long)((unsigned long)_1491 +(unsigned long) HIGH_BITS) >= 0){
            _1491 = NewDouble((double)_1491);
        }
    }
    else {
        _1491 = NewDouble(DBL_PTR(_1489)->dbl - (double)_1490);
    }
    DeRef(_1489);
    _1489 = NOVALUE;
    _1490 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _1491;
    ((int *)_2)[2] = _15leading_whitespace_3068;
    _1492 = MAKE_SEQ(_1);
    _1491 = NOVALUE;
    Concat((object_ptr)&_1493, _e_3072, _1492);
    DeRefDS(_1492);
    _1492 = NOVALUE;
    DeRef(_s_3071);
    DeRefDS(_e_3072);
    DeRef(_1405);
    _1405 = NOVALUE;
    DeRef(_1417);
    _1417 = NOVALUE;
    DeRef(_1423);
    _1423 = NOVALUE;
    DeRef(_1430);
    _1430 = NOVALUE;
    DeRef(_1442);
    _1442 = NOVALUE;
    DeRef(_1448);
    _1448 = NOVALUE;
    DeRef(_1454);
    _1454 = NOVALUE;
    DeRef(_1463);
    _1463 = NOVALUE;
    DeRef(_1469);
    _1469 = NOVALUE;
    DeRef(_1477);
    _1477 = NOVALUE;
    DeRef(_1485);
    _1485 = NOVALUE;
    return _1493;
    goto L4; // [836] 94
L19: 

    /** 			return {GET_FAIL, 0, string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1494 = _15string_next_2762 - 1;
    if ((long)((unsigned long)_1494 +(unsigned long) HIGH_BITS) >= 0){
        _1494 = NewDouble((double)_1494);
    }
    if (IS_ATOM_INT(_1494)) {
        _1495 = _1494 - _offset_3074;
        if ((long)((unsigned long)_1495 +(unsigned long) HIGH_BITS) >= 0){
            _1495 = NewDouble((double)_1495);
        }
    }
    else {
        _1495 = NewDouble(DBL_PTR(_1494)->dbl - (double)_offset_3074);
    }
    DeRef(_1494);
    _1494 = NOVALUE;
    _1496 = (_15ch_2763 != -1);
    if (IS_ATOM_INT(_1495)) {
        _1497 = _1495 - _1496;
        if ((long)((unsigned long)_1497 +(unsigned long) HIGH_BITS) >= 0){
            _1497 = NewDouble((double)_1497);
        }
    }
    else {
        _1497 = NewDouble(DBL_PTR(_1495)->dbl - (double)_1496);
    }
    DeRef(_1495);
    _1495 = NOVALUE;
    _1496 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 1;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _1497;
    *((int *)(_2+16)) = _15leading_whitespace_3068;
    _1498 = MAKE_SEQ(_1);
    _1497 = NOVALUE;
    DeRef(_s_3071);
    DeRef(_e_3072);
    DeRef(_1405);
    _1405 = NOVALUE;
    DeRef(_1417);
    _1417 = NOVALUE;
    DeRef(_1423);
    _1423 = NOVALUE;
    DeRef(_1430);
    _1430 = NOVALUE;
    DeRef(_1442);
    _1442 = NOVALUE;
    DeRef(_1448);
    _1448 = NOVALUE;
    DeRef(_1454);
    _1454 = NOVALUE;
    DeRef(_1463);
    _1463 = NOVALUE;
    DeRef(_1469);
    _1469 = NOVALUE;
    DeRef(_1477);
    _1477 = NOVALUE;
    DeRef(_1485);
    _1485 = NOVALUE;
    DeRef(_1493);
    _1493 = NOVALUE;
    return _1498;

    /** 	end while*/
    goto L4; // [877] 94
    ;
}


int _15get_value(int _target_3210, int _start_point_3211, int _answer_type_3212)
{
    int _msg_inlined_crash_at_35_3223 = NOVALUE;
    int _data_inlined_crash_at_32_3222 = NOVALUE;
    int _where_inlined_where_at_76_3229 = NOVALUE;
    int _seek_1__tmp_at90_3234 = NOVALUE;
    int _seek_inlined_seek_at_90_3233 = NOVALUE;
    int _pos_inlined_seek_at_87_3232 = NOVALUE;
    int _msg_inlined_crash_at_108_3237 = NOVALUE;
    int _1514 = NOVALUE;
    int _1511 = NOVALUE;
    int _1510 = NOVALUE;
    int _1509 = NOVALUE;
    int _1505 = NOVALUE;
    int _1504 = NOVALUE;
    int _1503 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if answer_type != GET_SHORT_ANSWER and answer_type != GET_LONG_ANSWER then*/
    _1503 = (_answer_type_3212 != _15GET_SHORT_ANSWER_3202);
    if (_1503 == 0) {
        goto L1; // [13] 55
    }
    _1505 = (_answer_type_3212 != _15GET_LONG_ANSWER_3205);
    if (_1505 == 0)
    {
        DeRef(_1505);
        _1505 = NOVALUE;
        goto L1; // [24] 55
    }
    else{
        DeRef(_1505);
        _1505 = NOVALUE;
    }

    /** 		error:crash("Invalid type of answer, please only use %s (the default) or %s.", {"GET_SHORT_ANSWER", "GET_LONG_ANSWER"})*/
    RefDS(_1508);
    RefDS(_1507);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _1507;
    ((int *)_2)[2] = _1508;
    _1509 = MAKE_SEQ(_1);
    DeRef(_data_inlined_crash_at_32_3222);
    _data_inlined_crash_at_32_3222 = _1509;
    _1509 = NOVALUE;

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_35_3223);
    _msg_inlined_crash_at_35_3223 = EPrintf(-9999999, _1506, _data_inlined_crash_at_32_3222);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_35_3223);

    /** end procedure*/
    goto L2; // [49] 52
L2: 
    DeRef(_data_inlined_crash_at_32_3222);
    _data_inlined_crash_at_32_3222 = NOVALUE;
    DeRefi(_msg_inlined_crash_at_35_3223);
    _msg_inlined_crash_at_35_3223 = NOVALUE;
L1: 

    /** 	if atom(target) then -- get()*/
    _1510 = IS_ATOM(_target_3210);
    if (_1510 == 0)
    {
        _1510 = NOVALUE;
        goto L3; // [60] 142
    }
    else{
        _1510 = NOVALUE;
    }

    /** 		input_file = target*/
    Ref(_target_3210);
    _15input_file_2760 = _target_3210;
    if (!IS_ATOM_INT(_15input_file_2760)) {
        _1 = (long)(DBL_PTR(_15input_file_2760)->dbl);
        if (UNIQUE(DBL_PTR(_15input_file_2760)) && (DBL_PTR(_15input_file_2760)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_15input_file_2760);
        _15input_file_2760 = _1;
    }

    /** 		if start_point then*/
    if (_start_point_3211 == 0)
    {
        goto L4; // [72] 129
    }
    else{
    }

    /** 			if io:seek(target, io:where(target)+start_point) then*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_76_3229);
    _where_inlined_where_at_76_3229 = machine(20, _target_3210);
    if (IS_ATOM_INT(_where_inlined_where_at_76_3229)) {
        _1511 = _where_inlined_where_at_76_3229 + _start_point_3211;
        if ((long)((unsigned long)_1511 + (unsigned long)HIGH_BITS) >= 0) 
        _1511 = NewDouble((double)_1511);
    }
    else {
        _1511 = NewDouble(DBL_PTR(_where_inlined_where_at_76_3229)->dbl + (double)_start_point_3211);
    }
    DeRef(_pos_inlined_seek_at_87_3232);
    _pos_inlined_seek_at_87_3232 = _1511;
    _1511 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_87_3232);
    Ref(_target_3210);
    DeRef(_seek_1__tmp_at90_3234);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _target_3210;
    ((int *)_2)[2] = _pos_inlined_seek_at_87_3232;
    _seek_1__tmp_at90_3234 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_90_3233 = machine(19, _seek_1__tmp_at90_3234);
    DeRef(_pos_inlined_seek_at_87_3232);
    _pos_inlined_seek_at_87_3232 = NOVALUE;
    DeRef(_seek_1__tmp_at90_3234);
    _seek_1__tmp_at90_3234 = NOVALUE;
    if (_seek_inlined_seek_at_90_3233 == 0)
    {
        goto L5; // [104] 128
    }
    else{
    }

    /** 				error:crash("Initial seek() for get() failed!")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_108_3237);
    _msg_inlined_crash_at_108_3237 = EPrintf(-9999999, _1512, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_108_3237);

    /** end procedure*/
    goto L6; // [122] 125
L6: 
    DeRefi(_msg_inlined_crash_at_108_3237);
    _msg_inlined_crash_at_108_3237 = NOVALUE;
L5: 
L4: 

    /** 		string_next = 1*/
    _15string_next_2762 = 1;

    /** 		input_string = 0*/
    DeRef(_15input_string_2761);
    _15input_string_2761 = 0;
    goto L7; // [139] 153
L3: 

    /** 		input_string = target*/
    Ref(_target_3210);
    DeRef(_15input_string_2761);
    _15input_string_2761 = _target_3210;

    /** 		string_next = start_point*/
    _15string_next_2762 = _start_point_3211;
L7: 

    /** 	if answer_type = GET_SHORT_ANSWER then*/
    if (_answer_type_3212 != _15GET_SHORT_ANSWER_3202)
    goto L8; // [157] 166

    /** 		get_ch()*/
    _15get_ch();
L8: 

    /** 	return call_func(answer_type, {})*/
    _0 = (int)_00[_answer_type_3212].addr;
    _1 = (*(int (*)())_0)(
                         );
    DeRef(_1514);
    _1514 = _1;
    DeRef(_target_3210);
    DeRef(_1503);
    _1503 = NOVALUE;
    return _1514;
    ;
}


int _15value(int _st_3250, int _start_point_3251, int _answer_3252)
{
    int _1516 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return get_value(st, start_point, answer)*/
    RefDS(_st_3250);
    _1516 = _15get_value(_st_3250, 1, _answer_3252);
    DeRefDS(_st_3250);
    return _1516;
    ;
}



// 0x7696446A
