// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _58compress(int _x_22468)
{
    int _x4_22469 = NOVALUE;
    int _s_22470 = NOVALUE;
    int _13104 = NOVALUE;
    int _13103 = NOVALUE;
    int _13102 = NOVALUE;
    int _13100 = NOVALUE;
    int _13099 = NOVALUE;
    int _13097 = NOVALUE;
    int _13095 = NOVALUE;
    int _13094 = NOVALUE;
    int _13093 = NOVALUE;
    int _13092 = NOVALUE;
    int _13090 = NOVALUE;
    int _13088 = NOVALUE;
    int _13087 = NOVALUE;
    int _13086 = NOVALUE;
    int _13085 = NOVALUE;
    int _13084 = NOVALUE;
    int _13083 = NOVALUE;
    int _13082 = NOVALUE;
    int _13081 = NOVALUE;
    int _13080 = NOVALUE;
    int _13078 = NOVALUE;
    int _13077 = NOVALUE;
    int _13076 = NOVALUE;
    int _13075 = NOVALUE;
    int _13074 = NOVALUE;
    int _13073 = NOVALUE;
    int _13071 = NOVALUE;
    int _13070 = NOVALUE;
    int _13069 = NOVALUE;
    int _13068 = NOVALUE;
    int _13067 = NOVALUE;
    int _13066 = NOVALUE;
    int _13065 = NOVALUE;
    int _13064 = NOVALUE;
    int _13063 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(x) then*/
    if (IS_ATOM_INT(_x_22468))
    _13063 = 1;
    else if (IS_ATOM_DBL(_x_22468))
    _13063 = IS_ATOM_INT(DoubleToInt(_x_22468));
    else
    _13063 = 0;
    if (_13063 == 0)
    {
        _13063 = NOVALUE;
        goto L1; // [6] 183
    }
    else{
        _13063 = NOVALUE;
    }

    /** 		if x >= MIN1B and x <= MAX1B then*/
    if (IS_ATOM_INT(_x_22468)) {
        _13064 = (_x_22468 >= -2);
    }
    else {
        _13064 = binary_op(GREATEREQ, _x_22468, -2);
    }
    if (IS_ATOM_INT(_13064)) {
        if (_13064 == 0) {
            goto L2; // [15] 44
        }
    }
    else {
        if (DBL_PTR(_13064)->dbl == 0.0) {
            goto L2; // [15] 44
        }
    }
    if (IS_ATOM_INT(_x_22468)) {
        _13066 = (_x_22468 <= 246);
    }
    else {
        _13066 = binary_op(LESSEQ, _x_22468, 246);
    }
    if (_13066 == 0) {
        DeRef(_13066);
        _13066 = NOVALUE;
        goto L2; // [24] 44
    }
    else {
        if (!IS_ATOM_INT(_13066) && DBL_PTR(_13066)->dbl == 0.0){
            DeRef(_13066);
            _13066 = NOVALUE;
            goto L2; // [24] 44
        }
        DeRef(_13066);
        _13066 = NOVALUE;
    }
    DeRef(_13066);
    _13066 = NOVALUE;

    /** 			return {x - MIN1B}*/
    if (IS_ATOM_INT(_x_22468)) {
        _13067 = _x_22468 - -2;
        if ((long)((unsigned long)_13067 +(unsigned long) HIGH_BITS) >= 0){
            _13067 = NewDouble((double)_13067);
        }
    }
    else {
        _13067 = binary_op(MINUS, _x_22468, -2);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _13067;
    _13068 = MAKE_SEQ(_1);
    _13067 = NOVALUE;
    DeRef(_x_22468);
    DeRef(_x4_22469);
    DeRef(_s_22470);
    DeRef(_13064);
    _13064 = NOVALUE;
    return _13068;
    goto L3; // [41] 319
L2: 

    /** 		elsif x >= MIN2B and x <= MAX2B then*/
    if (IS_ATOM_INT(_x_22468)) {
        _13069 = (_x_22468 >= _58MIN2B_22451);
    }
    else {
        _13069 = binary_op(GREATEREQ, _x_22468, _58MIN2B_22451);
    }
    if (IS_ATOM_INT(_13069)) {
        if (_13069 == 0) {
            goto L4; // [52] 97
        }
    }
    else {
        if (DBL_PTR(_13069)->dbl == 0.0) {
            goto L4; // [52] 97
        }
    }
    if (IS_ATOM_INT(_x_22468)) {
        _13071 = (_x_22468 <= 32767);
    }
    else {
        _13071 = binary_op(LESSEQ, _x_22468, 32767);
    }
    if (_13071 == 0) {
        DeRef(_13071);
        _13071 = NOVALUE;
        goto L4; // [63] 97
    }
    else {
        if (!IS_ATOM_INT(_13071) && DBL_PTR(_13071)->dbl == 0.0){
            DeRef(_13071);
            _13071 = NOVALUE;
            goto L4; // [63] 97
        }
        DeRef(_13071);
        _13071 = NOVALUE;
    }
    DeRef(_13071);
    _13071 = NOVALUE;

    /** 			x -= MIN2B*/
    _0 = _x_22468;
    if (IS_ATOM_INT(_x_22468)) {
        _x_22468 = _x_22468 - _58MIN2B_22451;
        if ((long)((unsigned long)_x_22468 +(unsigned long) HIGH_BITS) >= 0){
            _x_22468 = NewDouble((double)_x_22468);
        }
    }
    else {
        _x_22468 = binary_op(MINUS, _x_22468, _58MIN2B_22451);
    }
    DeRef(_0);

    /** 			return {I2B, and_bits(x, #FF), floor(x / #100)}*/
    if (IS_ATOM_INT(_x_22468)) {
        {unsigned long tu;
             tu = (unsigned long)_x_22468 & (unsigned long)255;
             _13073 = MAKE_UINT(tu);
        }
    }
    else {
        _13073 = binary_op(AND_BITS, _x_22468, 255);
    }
    if (IS_ATOM_INT(_x_22468)) {
        if (256 > 0 && _x_22468 >= 0) {
            _13074 = _x_22468 / 256;
        }
        else {
            temp_dbl = floor((double)_x_22468 / (double)256);
            if (_x_22468 != MININT)
            _13074 = (long)temp_dbl;
            else
            _13074 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_22468, 256);
        _13074 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 249;
    *((int *)(_2+8)) = _13073;
    *((int *)(_2+12)) = _13074;
    _13075 = MAKE_SEQ(_1);
    _13074 = NOVALUE;
    _13073 = NOVALUE;
    DeRef(_x_22468);
    DeRef(_x4_22469);
    DeRef(_s_22470);
    DeRef(_13064);
    _13064 = NOVALUE;
    DeRef(_13068);
    _13068 = NOVALUE;
    DeRef(_13069);
    _13069 = NOVALUE;
    return _13075;
    goto L3; // [94] 319
L4: 

    /** 		elsif x >= MIN3B and x <= MAX3B then*/
    if (IS_ATOM_INT(_x_22468)) {
        _13076 = (_x_22468 >= _58MIN3B_22457);
    }
    else {
        _13076 = binary_op(GREATEREQ, _x_22468, _58MIN3B_22457);
    }
    if (IS_ATOM_INT(_13076)) {
        if (_13076 == 0) {
            goto L5; // [105] 159
        }
    }
    else {
        if (DBL_PTR(_13076)->dbl == 0.0) {
            goto L5; // [105] 159
        }
    }
    if (IS_ATOM_INT(_x_22468)) {
        _13078 = (_x_22468 <= 8388607);
    }
    else {
        _13078 = binary_op(LESSEQ, _x_22468, 8388607);
    }
    if (_13078 == 0) {
        DeRef(_13078);
        _13078 = NOVALUE;
        goto L5; // [116] 159
    }
    else {
        if (!IS_ATOM_INT(_13078) && DBL_PTR(_13078)->dbl == 0.0){
            DeRef(_13078);
            _13078 = NOVALUE;
            goto L5; // [116] 159
        }
        DeRef(_13078);
        _13078 = NOVALUE;
    }
    DeRef(_13078);
    _13078 = NOVALUE;

    /** 			x -= MIN3B*/
    _0 = _x_22468;
    if (IS_ATOM_INT(_x_22468)) {
        _x_22468 = _x_22468 - _58MIN3B_22457;
        if ((long)((unsigned long)_x_22468 +(unsigned long) HIGH_BITS) >= 0){
            _x_22468 = NewDouble((double)_x_22468);
        }
    }
    else {
        _x_22468 = binary_op(MINUS, _x_22468, _58MIN3B_22457);
    }
    DeRef(_0);

    /** 			return {I3B, and_bits(x, #FF), and_bits(floor(x / #100), #FF), floor(x / #10000)}*/
    if (IS_ATOM_INT(_x_22468)) {
        {unsigned long tu;
             tu = (unsigned long)_x_22468 & (unsigned long)255;
             _13080 = MAKE_UINT(tu);
        }
    }
    else {
        _13080 = binary_op(AND_BITS, _x_22468, 255);
    }
    if (IS_ATOM_INT(_x_22468)) {
        if (256 > 0 && _x_22468 >= 0) {
            _13081 = _x_22468 / 256;
        }
        else {
            temp_dbl = floor((double)_x_22468 / (double)256);
            if (_x_22468 != MININT)
            _13081 = (long)temp_dbl;
            else
            _13081 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_22468, 256);
        _13081 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    if (IS_ATOM_INT(_13081)) {
        {unsigned long tu;
             tu = (unsigned long)_13081 & (unsigned long)255;
             _13082 = MAKE_UINT(tu);
        }
    }
    else {
        _13082 = binary_op(AND_BITS, _13081, 255);
    }
    DeRef(_13081);
    _13081 = NOVALUE;
    if (IS_ATOM_INT(_x_22468)) {
        if (65536 > 0 && _x_22468 >= 0) {
            _13083 = _x_22468 / 65536;
        }
        else {
            temp_dbl = floor((double)_x_22468 / (double)65536);
            if (_x_22468 != MININT)
            _13083 = (long)temp_dbl;
            else
            _13083 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_22468, 65536);
        _13083 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 250;
    *((int *)(_2+8)) = _13080;
    *((int *)(_2+12)) = _13082;
    *((int *)(_2+16)) = _13083;
    _13084 = MAKE_SEQ(_1);
    _13083 = NOVALUE;
    _13082 = NOVALUE;
    _13080 = NOVALUE;
    DeRef(_x_22468);
    DeRef(_x4_22469);
    DeRef(_s_22470);
    DeRef(_13064);
    _13064 = NOVALUE;
    DeRef(_13068);
    _13068 = NOVALUE;
    DeRef(_13069);
    _13069 = NOVALUE;
    DeRef(_13075);
    _13075 = NOVALUE;
    DeRef(_13076);
    _13076 = NOVALUE;
    return _13084;
    goto L3; // [156] 319
L5: 

    /** 			return I4B & int_to_bytes(x-MIN4B)*/
    if (IS_ATOM_INT(_x_22468) && IS_ATOM_INT(_58MIN4B_22463)) {
        _13085 = _x_22468 - _58MIN4B_22463;
        if ((long)((unsigned long)_13085 +(unsigned long) HIGH_BITS) >= 0){
            _13085 = NewDouble((double)_13085);
        }
    }
    else {
        _13085 = binary_op(MINUS, _x_22468, _58MIN4B_22463);
    }
    _13086 = _6int_to_bytes(_13085);
    _13085 = NOVALUE;
    if (IS_SEQUENCE(251) && IS_ATOM(_13086)) {
    }
    else if (IS_ATOM(251) && IS_SEQUENCE(_13086)) {
        Prepend(&_13087, _13086, 251);
    }
    else {
        Concat((object_ptr)&_13087, 251, _13086);
    }
    DeRef(_13086);
    _13086 = NOVALUE;
    DeRef(_x_22468);
    DeRef(_x4_22469);
    DeRef(_s_22470);
    DeRef(_13064);
    _13064 = NOVALUE;
    DeRef(_13068);
    _13068 = NOVALUE;
    DeRef(_13069);
    _13069 = NOVALUE;
    DeRef(_13075);
    _13075 = NOVALUE;
    DeRef(_13076);
    _13076 = NOVALUE;
    DeRef(_13084);
    _13084 = NOVALUE;
    return _13087;
    goto L3; // [180] 319
L1: 

    /** 	elsif atom(x) then*/
    _13088 = IS_ATOM(_x_22468);
    if (_13088 == 0)
    {
        _13088 = NOVALUE;
        goto L6; // [188] 240
    }
    else{
        _13088 = NOVALUE;
    }

    /** 		x4 = atom_to_float32(x)*/
    Ref(_x_22468);
    _0 = _x4_22469;
    _x4_22469 = _6atom_to_float32(_x_22468);
    DeRef(_0);

    /** 		if x = float32_to_atom(x4) then*/
    RefDS(_x4_22469);
    _13090 = _6float32_to_atom(_x4_22469);
    if (binary_op_a(NOTEQ, _x_22468, _13090)){
        DeRef(_13090);
        _13090 = NOVALUE;
        goto L7; // [205] 222
    }
    DeRef(_13090);
    _13090 = NOVALUE;

    /** 			return F4B & x4*/
    Prepend(&_13092, _x4_22469, 252);
    DeRef(_x_22468);
    DeRefDS(_x4_22469);
    DeRef(_s_22470);
    DeRef(_13064);
    _13064 = NOVALUE;
    DeRef(_13068);
    _13068 = NOVALUE;
    DeRef(_13069);
    _13069 = NOVALUE;
    DeRef(_13075);
    _13075 = NOVALUE;
    DeRef(_13076);
    _13076 = NOVALUE;
    DeRef(_13084);
    _13084 = NOVALUE;
    DeRef(_13087);
    _13087 = NOVALUE;
    return _13092;
    goto L3; // [219] 319
L7: 

    /** 			return F8B & atom_to_float64(x)*/
    Ref(_x_22468);
    _13093 = _6atom_to_float64(_x_22468);
    if (IS_SEQUENCE(253) && IS_ATOM(_13093)) {
    }
    else if (IS_ATOM(253) && IS_SEQUENCE(_13093)) {
        Prepend(&_13094, _13093, 253);
    }
    else {
        Concat((object_ptr)&_13094, 253, _13093);
    }
    DeRef(_13093);
    _13093 = NOVALUE;
    DeRef(_x_22468);
    DeRef(_x4_22469);
    DeRef(_s_22470);
    DeRef(_13064);
    _13064 = NOVALUE;
    DeRef(_13068);
    _13068 = NOVALUE;
    DeRef(_13069);
    _13069 = NOVALUE;
    DeRef(_13075);
    _13075 = NOVALUE;
    DeRef(_13076);
    _13076 = NOVALUE;
    DeRef(_13084);
    _13084 = NOVALUE;
    DeRef(_13087);
    _13087 = NOVALUE;
    DeRef(_13092);
    _13092 = NOVALUE;
    return _13094;
    goto L3; // [237] 319
L6: 

    /** 		if length(x) <= 255 then*/
    if (IS_SEQUENCE(_x_22468)){
            _13095 = SEQ_PTR(_x_22468)->length;
    }
    else {
        _13095 = 1;
    }
    if (_13095 > 255)
    goto L8; // [245] 261

    /** 			s = {S1B, length(x)}*/
    if (IS_SEQUENCE(_x_22468)){
            _13097 = SEQ_PTR(_x_22468)->length;
    }
    else {
        _13097 = 1;
    }
    DeRef(_s_22470);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 254;
    ((int *)_2)[2] = _13097;
    _s_22470 = MAKE_SEQ(_1);
    _13097 = NOVALUE;
    goto L9; // [258] 275
L8: 

    /** 			s = S4B & int_to_bytes(length(x))*/
    if (IS_SEQUENCE(_x_22468)){
            _13099 = SEQ_PTR(_x_22468)->length;
    }
    else {
        _13099 = 1;
    }
    _13100 = _6int_to_bytes(_13099);
    _13099 = NOVALUE;
    if (IS_SEQUENCE(255) && IS_ATOM(_13100)) {
    }
    else if (IS_ATOM(255) && IS_SEQUENCE(_13100)) {
        Prepend(&_s_22470, _13100, 255);
    }
    else {
        Concat((object_ptr)&_s_22470, 255, _13100);
    }
    DeRef(_13100);
    _13100 = NOVALUE;
L9: 

    /** 		for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_22468)){
            _13102 = SEQ_PTR(_x_22468)->length;
    }
    else {
        _13102 = 1;
    }
    {
        int _i_22527;
        _i_22527 = 1;
LA: 
        if (_i_22527 > _13102){
            goto LB; // [280] 310
        }

        /** 			s &= compress(x[i])*/
        _2 = (int)SEQ_PTR(_x_22468);
        _13103 = (int)*(((s1_ptr)_2)->base + _i_22527);
        Ref(_13103);
        _13104 = _58compress(_13103);
        _13103 = NOVALUE;
        if (IS_SEQUENCE(_s_22470) && IS_ATOM(_13104)) {
            Ref(_13104);
            Append(&_s_22470, _s_22470, _13104);
        }
        else if (IS_ATOM(_s_22470) && IS_SEQUENCE(_13104)) {
        }
        else {
            Concat((object_ptr)&_s_22470, _s_22470, _13104);
        }
        DeRef(_13104);
        _13104 = NOVALUE;

        /** 		end for*/
        _i_22527 = _i_22527 + 1;
        goto LA; // [305] 287
LB: 
        ;
    }

    /** 		return s*/
    DeRef(_x_22468);
    DeRef(_x4_22469);
    DeRef(_13064);
    _13064 = NOVALUE;
    DeRef(_13068);
    _13068 = NOVALUE;
    DeRef(_13069);
    _13069 = NOVALUE;
    DeRef(_13075);
    _13075 = NOVALUE;
    DeRef(_13076);
    _13076 = NOVALUE;
    DeRef(_13084);
    _13084 = NOVALUE;
    DeRef(_13087);
    _13087 = NOVALUE;
    DeRef(_13092);
    _13092 = NOVALUE;
    DeRef(_13094);
    _13094 = NOVALUE;
    return _s_22470;
L3: 
    ;
}


void _58init_compress()
{
    int _0, _1, _2;
    

    /** 	comp_cache = repeat({}, COMP_CACHE_SIZE)*/
    DeRef(_58comp_cache_22538);
    _58comp_cache_22538 = Repeat(_5, 64);

    /** end procedure*/
    return;
    ;
}


void _58fcompress(int _f_22544, int _x_22545)
{
    int _x4_22546 = NOVALUE;
    int _s_22547 = NOVALUE;
    int _p_22548 = NOVALUE;
    int _13156 = NOVALUE;
    int _13155 = NOVALUE;
    int _13154 = NOVALUE;
    int _13152 = NOVALUE;
    int _13151 = NOVALUE;
    int _13149 = NOVALUE;
    int _13147 = NOVALUE;
    int _13146 = NOVALUE;
    int _13145 = NOVALUE;
    int _13144 = NOVALUE;
    int _13142 = NOVALUE;
    int _13140 = NOVALUE;
    int _13139 = NOVALUE;
    int _13138 = NOVALUE;
    int _13137 = NOVALUE;
    int _13136 = NOVALUE;
    int _13135 = NOVALUE;
    int _13134 = NOVALUE;
    int _13133 = NOVALUE;
    int _13132 = NOVALUE;
    int _13130 = NOVALUE;
    int _13129 = NOVALUE;
    int _13128 = NOVALUE;
    int _13127 = NOVALUE;
    int _13126 = NOVALUE;
    int _13125 = NOVALUE;
    int _13123 = NOVALUE;
    int _13122 = NOVALUE;
    int _13121 = NOVALUE;
    int _13120 = NOVALUE;
    int _13119 = NOVALUE;
    int _13118 = NOVALUE;
    int _13116 = NOVALUE;
    int _13115 = NOVALUE;
    int _13114 = NOVALUE;
    int _13113 = NOVALUE;
    int _13112 = NOVALUE;
    int _13111 = NOVALUE;
    int _13110 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_f_22544)) {
        _1 = (long)(DBL_PTR(_f_22544)->dbl);
        if (UNIQUE(DBL_PTR(_f_22544)) && (DBL_PTR(_f_22544)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_f_22544);
        _f_22544 = _1;
    }

    /** 	if integer(x) then*/
    if (IS_ATOM_INT(_x_22545))
    _13110 = 1;
    else if (IS_ATOM_DBL(_x_22545))
    _13110 = IS_ATOM_INT(DoubleToInt(_x_22545));
    else
    _13110 = 0;
    if (_13110 == 0)
    {
        _13110 = NOVALUE;
        goto L1; // [8] 232
    }
    else{
        _13110 = NOVALUE;
    }

    /** 		if x >= MIN1B and x <= max1b then*/
    if (IS_ATOM_INT(_x_22545)) {
        _13111 = (_x_22545 >= -2);
    }
    else {
        _13111 = binary_op(GREATEREQ, _x_22545, -2);
    }
    if (IS_ATOM_INT(_13111)) {
        if (_13111 == 0) {
            goto L2; // [17] 43
        }
    }
    else {
        if (DBL_PTR(_13111)->dbl == 0.0) {
            goto L2; // [17] 43
        }
    }
    if (IS_ATOM_INT(_x_22545)) {
        _13113 = (_x_22545 <= 182);
    }
    else {
        _13113 = binary_op(LESSEQ, _x_22545, 182);
    }
    if (_13113 == 0) {
        DeRef(_13113);
        _13113 = NOVALUE;
        goto L2; // [28] 43
    }
    else {
        if (!IS_ATOM_INT(_13113) && DBL_PTR(_13113)->dbl == 0.0){
            DeRef(_13113);
            _13113 = NOVALUE;
            goto L2; // [28] 43
        }
        DeRef(_13113);
        _13113 = NOVALUE;
    }
    DeRef(_13113);
    _13113 = NOVALUE;

    /** 			puts(f, x - MIN1B) -- normal, quite small integer*/
    if (IS_ATOM_INT(_x_22545)) {
        _13114 = _x_22545 - -2;
        if ((long)((unsigned long)_13114 +(unsigned long) HIGH_BITS) >= 0){
            _13114 = NewDouble((double)_13114);
        }
    }
    else {
        _13114 = binary_op(MINUS, _x_22545, -2);
    }
    EPuts(_f_22544, _13114); // DJP 
    DeRef(_13114);
    _13114 = NOVALUE;
    goto L3; // [40] 362
L2: 

    /** 			p = 1 + and_bits(x, COMP_CACHE_SIZE-1)*/
    _13115 = 63;
    if (IS_ATOM_INT(_x_22545)) {
        {unsigned long tu;
             tu = (unsigned long)_x_22545 & (unsigned long)63;
             _13116 = MAKE_UINT(tu);
        }
    }
    else {
        _13116 = binary_op(AND_BITS, _x_22545, 63);
    }
    _13115 = NOVALUE;
    if (IS_ATOM_INT(_13116)) {
        _p_22548 = _13116 + 1;
    }
    else
    { // coercing _p_22548 to an integer 1
        _p_22548 = 1+(long)(DBL_PTR(_13116)->dbl);
        if( !IS_ATOM_INT(_p_22548) ){
            _p_22548 = (object)DBL_PTR(_p_22548)->dbl;
        }
    }
    DeRef(_13116);
    _13116 = NOVALUE;

    /** 			if equal(comp_cache[p], x) then*/
    _2 = (int)SEQ_PTR(_58comp_cache_22538);
    _13118 = (int)*(((s1_ptr)_2)->base + _p_22548);
    if (_13118 == _x_22545)
    _13119 = 1;
    else if (IS_ATOM_INT(_13118) && IS_ATOM_INT(_x_22545))
    _13119 = 0;
    else
    _13119 = (compare(_13118, _x_22545) == 0);
    _13118 = NOVALUE;
    if (_13119 == 0)
    {
        _13119 = NOVALUE;
        goto L4; // [69] 86
    }
    else{
        _13119 = NOVALUE;
    }

    /** 				puts(f, CACHE0 + p) -- output the cache slot number*/
    _13120 = 184 + _p_22548;
    if ((long)((unsigned long)_13120 + (unsigned long)HIGH_BITS) >= 0) 
    _13120 = NewDouble((double)_13120);
    EPuts(_f_22544, _13120); // DJP 
    DeRef(_13120);
    _13120 = NOVALUE;
    goto L3; // [83] 362
L4: 

    /** 				comp_cache[p] = x -- store it in cache slot p*/
    Ref(_x_22545);
    _2 = (int)SEQ_PTR(_58comp_cache_22538);
    _2 = (int)(((s1_ptr)_2)->base + _p_22548);
    _1 = *(int *)_2;
    *(int *)_2 = _x_22545;
    DeRef(_1);

    /** 				if x >= MIN2B and x <= MAX2B then*/
    if (IS_ATOM_INT(_x_22545)) {
        _13121 = (_x_22545 >= _58MIN2B_22451);
    }
    else {
        _13121 = binary_op(GREATEREQ, _x_22545, _58MIN2B_22451);
    }
    if (IS_ATOM_INT(_13121)) {
        if (_13121 == 0) {
            goto L5; // [102] 146
        }
    }
    else {
        if (DBL_PTR(_13121)->dbl == 0.0) {
            goto L5; // [102] 146
        }
    }
    if (IS_ATOM_INT(_x_22545)) {
        _13123 = (_x_22545 <= 32767);
    }
    else {
        _13123 = binary_op(LESSEQ, _x_22545, 32767);
    }
    if (_13123 == 0) {
        DeRef(_13123);
        _13123 = NOVALUE;
        goto L5; // [113] 146
    }
    else {
        if (!IS_ATOM_INT(_13123) && DBL_PTR(_13123)->dbl == 0.0){
            DeRef(_13123);
            _13123 = NOVALUE;
            goto L5; // [113] 146
        }
        DeRef(_13123);
        _13123 = NOVALUE;
    }
    DeRef(_13123);
    _13123 = NOVALUE;

    /** 					x -= MIN2B*/
    _0 = _x_22545;
    if (IS_ATOM_INT(_x_22545)) {
        _x_22545 = _x_22545 - _58MIN2B_22451;
        if ((long)((unsigned long)_x_22545 +(unsigned long) HIGH_BITS) >= 0){
            _x_22545 = NewDouble((double)_x_22545);
        }
    }
    else {
        _x_22545 = binary_op(MINUS, _x_22545, _58MIN2B_22451);
    }
    DeRef(_0);

    /** 					puts(f, {I2B, and_bits(x, #FF), floor(x / #100)})*/
    if (IS_ATOM_INT(_x_22545)) {
        {unsigned long tu;
             tu = (unsigned long)_x_22545 & (unsigned long)255;
             _13125 = MAKE_UINT(tu);
        }
    }
    else {
        _13125 = binary_op(AND_BITS, _x_22545, 255);
    }
    if (IS_ATOM_INT(_x_22545)) {
        if (256 > 0 && _x_22545 >= 0) {
            _13126 = _x_22545 / 256;
        }
        else {
            temp_dbl = floor((double)_x_22545 / (double)256);
            if (_x_22545 != MININT)
            _13126 = (long)temp_dbl;
            else
            _13126 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_22545, 256);
        _13126 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 249;
    *((int *)(_2+8)) = _13125;
    *((int *)(_2+12)) = _13126;
    _13127 = MAKE_SEQ(_1);
    _13126 = NOVALUE;
    _13125 = NOVALUE;
    EPuts(_f_22544, _13127); // DJP 
    DeRefDS(_13127);
    _13127 = NOVALUE;
    goto L3; // [143] 362
L5: 

    /** 				elsif x >= MIN3B and x <= MAX3B then*/
    if (IS_ATOM_INT(_x_22545)) {
        _13128 = (_x_22545 >= _58MIN3B_22457);
    }
    else {
        _13128 = binary_op(GREATEREQ, _x_22545, _58MIN3B_22457);
    }
    if (IS_ATOM_INT(_13128)) {
        if (_13128 == 0) {
            goto L6; // [154] 207
        }
    }
    else {
        if (DBL_PTR(_13128)->dbl == 0.0) {
            goto L6; // [154] 207
        }
    }
    if (IS_ATOM_INT(_x_22545)) {
        _13130 = (_x_22545 <= 8388607);
    }
    else {
        _13130 = binary_op(LESSEQ, _x_22545, 8388607);
    }
    if (_13130 == 0) {
        DeRef(_13130);
        _13130 = NOVALUE;
        goto L6; // [165] 207
    }
    else {
        if (!IS_ATOM_INT(_13130) && DBL_PTR(_13130)->dbl == 0.0){
            DeRef(_13130);
            _13130 = NOVALUE;
            goto L6; // [165] 207
        }
        DeRef(_13130);
        _13130 = NOVALUE;
    }
    DeRef(_13130);
    _13130 = NOVALUE;

    /** 					x -= MIN3B*/
    _0 = _x_22545;
    if (IS_ATOM_INT(_x_22545)) {
        _x_22545 = _x_22545 - _58MIN3B_22457;
        if ((long)((unsigned long)_x_22545 +(unsigned long) HIGH_BITS) >= 0){
            _x_22545 = NewDouble((double)_x_22545);
        }
    }
    else {
        _x_22545 = binary_op(MINUS, _x_22545, _58MIN3B_22457);
    }
    DeRef(_0);

    /** 					puts(f, {I3B, and_bits(x, #FF), and_bits(floor(x / #100), #FF), floor(x / #10000)})*/
    if (IS_ATOM_INT(_x_22545)) {
        {unsigned long tu;
             tu = (unsigned long)_x_22545 & (unsigned long)255;
             _13132 = MAKE_UINT(tu);
        }
    }
    else {
        _13132 = binary_op(AND_BITS, _x_22545, 255);
    }
    if (IS_ATOM_INT(_x_22545)) {
        if (256 > 0 && _x_22545 >= 0) {
            _13133 = _x_22545 / 256;
        }
        else {
            temp_dbl = floor((double)_x_22545 / (double)256);
            if (_x_22545 != MININT)
            _13133 = (long)temp_dbl;
            else
            _13133 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_22545, 256);
        _13133 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    if (IS_ATOM_INT(_13133)) {
        {unsigned long tu;
             tu = (unsigned long)_13133 & (unsigned long)255;
             _13134 = MAKE_UINT(tu);
        }
    }
    else {
        _13134 = binary_op(AND_BITS, _13133, 255);
    }
    DeRef(_13133);
    _13133 = NOVALUE;
    if (IS_ATOM_INT(_x_22545)) {
        if (65536 > 0 && _x_22545 >= 0) {
            _13135 = _x_22545 / 65536;
        }
        else {
            temp_dbl = floor((double)_x_22545 / (double)65536);
            if (_x_22545 != MININT)
            _13135 = (long)temp_dbl;
            else
            _13135 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_22545, 65536);
        _13135 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 250;
    *((int *)(_2+8)) = _13132;
    *((int *)(_2+12)) = _13134;
    *((int *)(_2+16)) = _13135;
    _13136 = MAKE_SEQ(_1);
    _13135 = NOVALUE;
    _13134 = NOVALUE;
    _13132 = NOVALUE;
    EPuts(_f_22544, _13136); // DJP 
    DeRefDS(_13136);
    _13136 = NOVALUE;
    goto L3; // [204] 362
L6: 

    /** 					puts(f, I4B & int_to_bytes(x-MIN4B))*/
    if (IS_ATOM_INT(_x_22545) && IS_ATOM_INT(_58MIN4B_22463)) {
        _13137 = _x_22545 - _58MIN4B_22463;
        if ((long)((unsigned long)_13137 +(unsigned long) HIGH_BITS) >= 0){
            _13137 = NewDouble((double)_13137);
        }
    }
    else {
        _13137 = binary_op(MINUS, _x_22545, _58MIN4B_22463);
    }
    _13138 = _6int_to_bytes(_13137);
    _13137 = NOVALUE;
    if (IS_SEQUENCE(251) && IS_ATOM(_13138)) {
    }
    else if (IS_ATOM(251) && IS_SEQUENCE(_13138)) {
        Prepend(&_13139, _13138, 251);
    }
    else {
        Concat((object_ptr)&_13139, 251, _13138);
    }
    DeRef(_13138);
    _13138 = NOVALUE;
    EPuts(_f_22544, _13139); // DJP 
    DeRefDS(_13139);
    _13139 = NOVALUE;
    goto L3; // [229] 362
L1: 

    /** 	elsif atom(x) then*/
    _13140 = IS_ATOM(_x_22545);
    if (_13140 == 0)
    {
        _13140 = NOVALUE;
        goto L7; // [237] 287
    }
    else{
        _13140 = NOVALUE;
    }

    /** 		x4 = atom_to_float32(x)*/
    Ref(_x_22545);
    _0 = _x4_22546;
    _x4_22546 = _6atom_to_float32(_x_22545);
    DeRef(_0);

    /** 		if x = float32_to_atom(x4) then*/
    RefDS(_x4_22546);
    _13142 = _6float32_to_atom(_x4_22546);
    if (binary_op_a(NOTEQ, _x_22545, _13142)){
        DeRef(_13142);
        _13142 = NOVALUE;
        goto L8; // [254] 270
    }
    DeRef(_13142);
    _13142 = NOVALUE;

    /** 			puts(f, F4B & x4)*/
    Prepend(&_13144, _x4_22546, 252);
    EPuts(_f_22544, _13144); // DJP 
    DeRefDS(_13144);
    _13144 = NOVALUE;
    goto L3; // [267] 362
L8: 

    /** 			puts(f, F8B & atom_to_float64(x))*/
    Ref(_x_22545);
    _13145 = _6atom_to_float64(_x_22545);
    if (IS_SEQUENCE(253) && IS_ATOM(_13145)) {
    }
    else if (IS_ATOM(253) && IS_SEQUENCE(_13145)) {
        Prepend(&_13146, _13145, 253);
    }
    else {
        Concat((object_ptr)&_13146, 253, _13145);
    }
    DeRef(_13145);
    _13145 = NOVALUE;
    EPuts(_f_22544, _13146); // DJP 
    DeRefDS(_13146);
    _13146 = NOVALUE;
    goto L3; // [284] 362
L7: 

    /** 		if length(x) <= 255 then*/
    if (IS_SEQUENCE(_x_22545)){
            _13147 = SEQ_PTR(_x_22545)->length;
    }
    else {
        _13147 = 1;
    }
    if (_13147 > 255)
    goto L9; // [292] 308

    /** 			s = {S1B, length(x)}*/
    if (IS_SEQUENCE(_x_22545)){
            _13149 = SEQ_PTR(_x_22545)->length;
    }
    else {
        _13149 = 1;
    }
    DeRef(_s_22547);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 254;
    ((int *)_2)[2] = _13149;
    _s_22547 = MAKE_SEQ(_1);
    _13149 = NOVALUE;
    goto LA; // [305] 322
L9: 

    /** 			s = S4B & int_to_bytes(length(x))*/
    if (IS_SEQUENCE(_x_22545)){
            _13151 = SEQ_PTR(_x_22545)->length;
    }
    else {
        _13151 = 1;
    }
    _13152 = _6int_to_bytes(_13151);
    _13151 = NOVALUE;
    if (IS_SEQUENCE(255) && IS_ATOM(_13152)) {
    }
    else if (IS_ATOM(255) && IS_SEQUENCE(_13152)) {
        Prepend(&_s_22547, _13152, 255);
    }
    else {
        Concat((object_ptr)&_s_22547, 255, _13152);
    }
    DeRef(_13152);
    _13152 = NOVALUE;
LA: 

    /** 		puts(f, s)*/
    EPuts(_f_22544, _s_22547); // DJP 

    /** 		for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_22545)){
            _13154 = SEQ_PTR(_x_22545)->length;
    }
    else {
        _13154 = 1;
    }
    {
        int _i_22613;
        _i_22613 = 1;
LB: 
        if (_i_22613 > _13154){
            goto LC; // [334] 361
        }

        /** 			fcompress(f, x[i])*/
        _2 = (int)SEQ_PTR(_x_22545);
        _13155 = (int)*(((s1_ptr)_2)->base + _i_22613);
        DeRef(_13156);
        _13156 = _f_22544;
        Ref(_13155);
        _58fcompress(_13156, _13155);
        _13156 = NOVALUE;
        _13155 = NOVALUE;

        /** 		end for*/
        _i_22613 = _i_22613 + 1;
        goto LB; // [356] 341
LC: 
        ;
    }
L3: 

    /** end procedure*/
    DeRef(_x_22545);
    DeRef(_x4_22546);
    DeRef(_s_22547);
    DeRef(_13111);
    _13111 = NOVALUE;
    DeRef(_13121);
    _13121 = NOVALUE;
    DeRef(_13128);
    _13128 = NOVALUE;
    return;
    ;
}


int _58get4()
{
    int _13165 = NOVALUE;
    int _13164 = NOVALUE;
    int _13163 = NOVALUE;
    int _13162 = NOVALUE;
    int _13161 = NOVALUE;
    int _0, _1, _2;
    

    /** 	poke(mem0, getc(current_db))*/
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _13161 = getKBchar();
        }
        else
        _13161 = getc(last_r_file_ptr);
    }
    else
    _13161 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_58mem0_22618)){
        poke_addr = (unsigned char *)_58mem0_22618;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_58mem0_22618)->dbl);
    }
    *poke_addr = (unsigned char)_13161;
    _13161 = NOVALUE;

    /** 	poke(mem1, getc(current_db))*/
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _13162 = getKBchar();
        }
        else
        _13162 = getc(last_r_file_ptr);
    }
    else
    _13162 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_58mem1_22619)){
        poke_addr = (unsigned char *)_58mem1_22619;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_58mem1_22619)->dbl);
    }
    *poke_addr = (unsigned char)_13162;
    _13162 = NOVALUE;

    /** 	poke(mem2, getc(current_db))*/
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _13163 = getKBchar();
        }
        else
        _13163 = getc(last_r_file_ptr);
    }
    else
    _13163 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_58mem2_22620)){
        poke_addr = (unsigned char *)_58mem2_22620;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_58mem2_22620)->dbl);
    }
    *poke_addr = (unsigned char)_13163;
    _13163 = NOVALUE;

    /** 	poke(mem3, getc(current_db))*/
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _13164 = getKBchar();
        }
        else
        _13164 = getc(last_r_file_ptr);
    }
    else
    _13164 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_58mem3_22621)){
        poke_addr = (unsigned char *)_58mem3_22621;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_58mem3_22621)->dbl);
    }
    *poke_addr = (unsigned char)_13164;
    _13164 = NOVALUE;

    /** 	return peek4u(mem0)*/
    if (IS_ATOM_INT(_58mem0_22618)) {
        _13165 = *(unsigned long *)_58mem0_22618;
        if ((unsigned)_13165 > (unsigned)MAXINT)
        _13165 = NewDouble((double)(unsigned long)_13165);
    }
    else {
        _13165 = *(unsigned long *)(unsigned long)(DBL_PTR(_58mem0_22618)->dbl);
        if ((unsigned)_13165 > (unsigned)MAXINT)
        _13165 = NewDouble((double)(unsigned long)_13165);
    }
    return _13165;
    ;
}


int _58fdecompress(int _c_22636)
{
    int _s_22637 = NOVALUE;
    int _len_22638 = NOVALUE;
    int _ival_22639 = NOVALUE;
    int _13233 = NOVALUE;
    int _13232 = NOVALUE;
    int _13231 = NOVALUE;
    int _13230 = NOVALUE;
    int _13228 = NOVALUE;
    int _13227 = NOVALUE;
    int _13223 = NOVALUE;
    int _13218 = NOVALUE;
    int _13217 = NOVALUE;
    int _13216 = NOVALUE;
    int _13215 = NOVALUE;
    int _13214 = NOVALUE;
    int _13213 = NOVALUE;
    int _13212 = NOVALUE;
    int _13211 = NOVALUE;
    int _13210 = NOVALUE;
    int _13209 = NOVALUE;
    int _13207 = NOVALUE;
    int _13206 = NOVALUE;
    int _13205 = NOVALUE;
    int _13204 = NOVALUE;
    int _13203 = NOVALUE;
    int _13202 = NOVALUE;
    int _13200 = NOVALUE;
    int _13199 = NOVALUE;
    int _13198 = NOVALUE;
    int _13196 = NOVALUE;
    int _13194 = NOVALUE;
    int _13193 = NOVALUE;
    int _13192 = NOVALUE;
    int _13190 = NOVALUE;
    int _13189 = NOVALUE;
    int _13188 = NOVALUE;
    int _13187 = NOVALUE;
    int _13186 = NOVALUE;
    int _13185 = NOVALUE;
    int _13184 = NOVALUE;
    int _13182 = NOVALUE;
    int _13181 = NOVALUE;
    int _13180 = NOVALUE;
    int _13178 = NOVALUE;
    int _13177 = NOVALUE;
    int _13176 = NOVALUE;
    int _13175 = NOVALUE;
    int _13173 = NOVALUE;
    int _13172 = NOVALUE;
    int _13170 = NOVALUE;
    int _13169 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_c_22636)) {
        _1 = (long)(DBL_PTR(_c_22636)->dbl);
        if (UNIQUE(DBL_PTR(_c_22636)) && (DBL_PTR(_c_22636)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_22636);
        _c_22636 = _1;
    }

    /** 	if c = 0 then*/
    if (_c_22636 != 0)
    goto L1; // [5] 70

    /** 		c = getc(current_db)*/
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_22636 = getKBchar();
        }
        else
        _c_22636 = getc(last_r_file_ptr);
    }
    else
    _c_22636 = getc(last_r_file_ptr);

    /** 		if c <= CACHE0 then*/
    if (_c_22636 > 184)
    goto L2; // [20] 37

    /** 			return c + MIN1B  -- a normal, quite small integer*/
    _13169 = _c_22636 + -2;
    DeRef(_s_22637);
    return _13169;
    goto L3; // [34] 69
L2: 

    /** 		elsif c <= CACHE0 + COMP_CACHE_SIZE then*/
    _13170 = 248;
    if (_c_22636 > 248)
    goto L4; // [45] 68

    /** 			return comp_cache[c-CACHE0]*/
    _13172 = _c_22636 - 184;
    _2 = (int)SEQ_PTR(_58comp_cache_22538);
    _13173 = (int)*(((s1_ptr)_2)->base + _13172);
    Ref(_13173);
    DeRef(_s_22637);
    DeRef(_13169);
    _13169 = NOVALUE;
    _13170 = NOVALUE;
    _13172 = NOVALUE;
    return _13173;
L4: 
L3: 
L1: 

    /** 	if c = I2B then*/
    if (_c_22636 != 249)
    goto L5; // [72] 133

    /** 		ival = getc(current_db) +*/
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _13175 = getKBchar();
        }
        else
        _13175 = getc(last_r_file_ptr);
    }
    else
    _13175 = getc(last_r_file_ptr);
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _13176 = getKBchar();
        }
        else
        _13176 = getc(last_r_file_ptr);
    }
    else
    _13176 = getc(last_r_file_ptr);
    _13177 = 256 * _13176;
    _13176 = NOVALUE;
    _13178 = _13175 + _13177;
    _13175 = NOVALUE;
    _13177 = NOVALUE;
    _ival_22639 = _13178 + _58MIN2B_22451;
    _13178 = NOVALUE;

    /** 		comp_cache[1 + and_bits(ival, COMP_CACHE_SIZE-1)] = ival*/
    _13180 = 63;
    {unsigned long tu;
         tu = (unsigned long)_ival_22639 & (unsigned long)63;
         _13181 = MAKE_UINT(tu);
    }
    _13180 = NOVALUE;
    if (IS_ATOM_INT(_13181)) {
        _13182 = _13181 + 1;
    }
    else
    _13182 = binary_op(PLUS, 1, _13181);
    DeRef(_13181);
    _13181 = NOVALUE;
    _2 = (int)SEQ_PTR(_58comp_cache_22538);
    if (!IS_ATOM_INT(_13182))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_13182)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _13182);
    _1 = *(int *)_2;
    *(int *)_2 = _ival_22639;
    DeRef(_1);

    /** 		return ival*/
    DeRef(_s_22637);
    DeRef(_13169);
    _13169 = NOVALUE;
    DeRef(_13170);
    _13170 = NOVALUE;
    DeRef(_13172);
    _13172 = NOVALUE;
    _13173 = NOVALUE;
    DeRef(_13182);
    _13182 = NOVALUE;
    return _ival_22639;
    goto L6; // [130] 514
L5: 

    /** 	elsif c = I3B then*/
    if (_c_22636 != 250)
    goto L7; // [135] 209

    /** 		ival = getc(current_db) +*/
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _13184 = getKBchar();
        }
        else
        _13184 = getc(last_r_file_ptr);
    }
    else
    _13184 = getc(last_r_file_ptr);
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _13185 = getKBchar();
        }
        else
        _13185 = getc(last_r_file_ptr);
    }
    else
    _13185 = getc(last_r_file_ptr);
    _13186 = 256 * _13185;
    _13185 = NOVALUE;
    _13187 = _13184 + _13186;
    _13184 = NOVALUE;
    _13186 = NOVALUE;
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _13188 = getKBchar();
        }
        else
        _13188 = getc(last_r_file_ptr);
    }
    else
    _13188 = getc(last_r_file_ptr);
    _13189 = 65536 * _13188;
    _13188 = NOVALUE;
    _13190 = _13187 + _13189;
    _13187 = NOVALUE;
    _13189 = NOVALUE;
    _ival_22639 = _13190 + _58MIN3B_22457;
    _13190 = NOVALUE;

    /** 		comp_cache[1 + and_bits(ival, COMP_CACHE_SIZE-1)] = ival*/
    _13192 = 63;
    {unsigned long tu;
         tu = (unsigned long)_ival_22639 & (unsigned long)63;
         _13193 = MAKE_UINT(tu);
    }
    _13192 = NOVALUE;
    if (IS_ATOM_INT(_13193)) {
        _13194 = _13193 + 1;
    }
    else
    _13194 = binary_op(PLUS, 1, _13193);
    DeRef(_13193);
    _13193 = NOVALUE;
    _2 = (int)SEQ_PTR(_58comp_cache_22538);
    if (!IS_ATOM_INT(_13194))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_13194)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _13194);
    _1 = *(int *)_2;
    *(int *)_2 = _ival_22639;
    DeRef(_1);

    /** 		return ival*/
    DeRef(_s_22637);
    DeRef(_13169);
    _13169 = NOVALUE;
    DeRef(_13170);
    _13170 = NOVALUE;
    DeRef(_13172);
    _13172 = NOVALUE;
    _13173 = NOVALUE;
    DeRef(_13182);
    _13182 = NOVALUE;
    DeRef(_13194);
    _13194 = NOVALUE;
    return _ival_22639;
    goto L6; // [206] 514
L7: 

    /** 	elsif c = I4B  then*/
    if (_c_22636 != 251)
    goto L8; // [211] 257

    /** 		ival = get4() + MIN4B*/
    _13196 = _58get4();
    if (IS_ATOM_INT(_13196) && IS_ATOM_INT(_58MIN4B_22463)) {
        _ival_22639 = _13196 + _58MIN4B_22463;
    }
    else {
        _ival_22639 = binary_op(PLUS, _13196, _58MIN4B_22463);
    }
    DeRef(_13196);
    _13196 = NOVALUE;
    if (!IS_ATOM_INT(_ival_22639)) {
        _1 = (long)(DBL_PTR(_ival_22639)->dbl);
        if (UNIQUE(DBL_PTR(_ival_22639)) && (DBL_PTR(_ival_22639)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ival_22639);
        _ival_22639 = _1;
    }

    /** 		comp_cache[1 + and_bits(ival, COMP_CACHE_SIZE-1)] = ival*/
    _13198 = 63;
    {unsigned long tu;
         tu = (unsigned long)_ival_22639 & (unsigned long)63;
         _13199 = MAKE_UINT(tu);
    }
    _13198 = NOVALUE;
    if (IS_ATOM_INT(_13199)) {
        _13200 = _13199 + 1;
    }
    else
    _13200 = binary_op(PLUS, 1, _13199);
    DeRef(_13199);
    _13199 = NOVALUE;
    _2 = (int)SEQ_PTR(_58comp_cache_22538);
    if (!IS_ATOM_INT(_13200))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_13200)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _13200);
    _1 = *(int *)_2;
    *(int *)_2 = _ival_22639;
    DeRef(_1);

    /** 		return ival*/
    DeRef(_s_22637);
    DeRef(_13169);
    _13169 = NOVALUE;
    DeRef(_13170);
    _13170 = NOVALUE;
    DeRef(_13172);
    _13172 = NOVALUE;
    _13173 = NOVALUE;
    DeRef(_13182);
    _13182 = NOVALUE;
    DeRef(_13194);
    _13194 = NOVALUE;
    DeRef(_13200);
    _13200 = NOVALUE;
    return _ival_22639;
    goto L6; // [254] 514
L8: 

    /** 	elsif c = F4B then*/
    if (_c_22636 != 252)
    goto L9; // [259] 303

    /** 		return float32_to_atom({getc(current_db), getc(current_db),*/
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _13202 = getKBchar();
        }
        else
        _13202 = getc(last_r_file_ptr);
    }
    else
    _13202 = getc(last_r_file_ptr);
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _13203 = getKBchar();
        }
        else
        _13203 = getc(last_r_file_ptr);
    }
    else
    _13203 = getc(last_r_file_ptr);
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _13204 = getKBchar();
        }
        else
        _13204 = getc(last_r_file_ptr);
    }
    else
    _13204 = getc(last_r_file_ptr);
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _13205 = getKBchar();
        }
        else
        _13205 = getc(last_r_file_ptr);
    }
    else
    _13205 = getc(last_r_file_ptr);
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _13202;
    *((int *)(_2+8)) = _13203;
    *((int *)(_2+12)) = _13204;
    *((int *)(_2+16)) = _13205;
    _13206 = MAKE_SEQ(_1);
    _13205 = NOVALUE;
    _13204 = NOVALUE;
    _13203 = NOVALUE;
    _13202 = NOVALUE;
    _13207 = _6float32_to_atom(_13206);
    _13206 = NOVALUE;
    DeRef(_s_22637);
    DeRef(_13169);
    _13169 = NOVALUE;
    DeRef(_13170);
    _13170 = NOVALUE;
    DeRef(_13172);
    _13172 = NOVALUE;
    _13173 = NOVALUE;
    DeRef(_13182);
    _13182 = NOVALUE;
    DeRef(_13194);
    _13194 = NOVALUE;
    DeRef(_13200);
    _13200 = NOVALUE;
    return _13207;
    goto L6; // [300] 514
L9: 

    /** 	elsif c = F8B then*/
    if (_c_22636 != 253)
    goto LA; // [305] 373

    /** 		return float64_to_atom({getc(current_db), getc(current_db),*/
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _13209 = getKBchar();
        }
        else
        _13209 = getc(last_r_file_ptr);
    }
    else
    _13209 = getc(last_r_file_ptr);
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _13210 = getKBchar();
        }
        else
        _13210 = getc(last_r_file_ptr);
    }
    else
    _13210 = getc(last_r_file_ptr);
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _13211 = getKBchar();
        }
        else
        _13211 = getc(last_r_file_ptr);
    }
    else
    _13211 = getc(last_r_file_ptr);
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _13212 = getKBchar();
        }
        else
        _13212 = getc(last_r_file_ptr);
    }
    else
    _13212 = getc(last_r_file_ptr);
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _13213 = getKBchar();
        }
        else
        _13213 = getc(last_r_file_ptr);
    }
    else
    _13213 = getc(last_r_file_ptr);
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _13214 = getKBchar();
        }
        else
        _13214 = getc(last_r_file_ptr);
    }
    else
    _13214 = getc(last_r_file_ptr);
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _13215 = getKBchar();
        }
        else
        _13215 = getc(last_r_file_ptr);
    }
    else
    _13215 = getc(last_r_file_ptr);
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _13216 = getKBchar();
        }
        else
        _13216 = getc(last_r_file_ptr);
    }
    else
    _13216 = getc(last_r_file_ptr);
    _1 = NewS1(8);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _13209;
    *((int *)(_2+8)) = _13210;
    *((int *)(_2+12)) = _13211;
    *((int *)(_2+16)) = _13212;
    *((int *)(_2+20)) = _13213;
    *((int *)(_2+24)) = _13214;
    *((int *)(_2+28)) = _13215;
    *((int *)(_2+32)) = _13216;
    _13217 = MAKE_SEQ(_1);
    _13216 = NOVALUE;
    _13215 = NOVALUE;
    _13214 = NOVALUE;
    _13213 = NOVALUE;
    _13212 = NOVALUE;
    _13211 = NOVALUE;
    _13210 = NOVALUE;
    _13209 = NOVALUE;
    _13218 = _6float64_to_atom(_13217);
    _13217 = NOVALUE;
    DeRef(_s_22637);
    DeRef(_13169);
    _13169 = NOVALUE;
    DeRef(_13170);
    _13170 = NOVALUE;
    DeRef(_13172);
    _13172 = NOVALUE;
    _13173 = NOVALUE;
    DeRef(_13182);
    _13182 = NOVALUE;
    DeRef(_13194);
    _13194 = NOVALUE;
    DeRef(_13200);
    _13200 = NOVALUE;
    DeRef(_13207);
    _13207 = NOVALUE;
    return _13218;
    goto L6; // [370] 514
LA: 

    /** 		if c = S1B then*/
    if (_c_22636 != 254)
    goto LB; // [375] 389

    /** 			len = getc(current_db)*/
    if (_58current_db_22626 != last_r_file_no) {
        last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
        last_r_file_no = _58current_db_22626;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _len_22638 = getKBchar();
        }
        else
        _len_22638 = getc(last_r_file_ptr);
    }
    else
    _len_22638 = getc(last_r_file_ptr);
    goto LC; // [386] 397
LB: 

    /** 			len = get4()*/
    _len_22638 = _58get4();
    if (!IS_ATOM_INT(_len_22638)) {
        _1 = (long)(DBL_PTR(_len_22638)->dbl);
        if (UNIQUE(DBL_PTR(_len_22638)) && (DBL_PTR(_len_22638)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_len_22638);
        _len_22638 = _1;
    }
LC: 

    /** 		s = repeat(0, len)*/
    DeRef(_s_22637);
    _s_22637 = Repeat(0, _len_22638);

    /** 		for i = 1 to len do*/
    _13223 = _len_22638;
    {
        int _i_22711;
        _i_22711 = 1;
LD: 
        if (_i_22711 > _13223){
            goto LE; // [410] 507
        }

        /** 			c = getc(current_db)*/
        if (_58current_db_22626 != last_r_file_no) {
            last_r_file_ptr = which_file(_58current_db_22626, EF_READ);
            last_r_file_no = _58current_db_22626;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _c_22636 = getKBchar();
            }
            else
            _c_22636 = getc(last_r_file_ptr);
        }
        else
        _c_22636 = getc(last_r_file_ptr);

        /** 			if c < I2B then*/
        if (_c_22636 >= 249)
        goto LF; // [426] 486

        /** 				if c <= CACHE0 then*/
        if (_c_22636 > 184)
        goto L10; // [434] 451

        /** 					s[i] = c + MIN1B*/
        _13227 = _c_22636 + -2;
        _2 = (int)SEQ_PTR(_s_22637);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_22637 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_22711);
        _1 = *(int *)_2;
        *(int *)_2 = _13227;
        if( _1 != _13227 ){
            DeRef(_1);
        }
        _13227 = NOVALUE;
        goto L11; // [448] 500
L10: 

        /** 				elsif c <= CACHE0 + COMP_CACHE_SIZE then*/
        _13228 = 248;
        if (_c_22636 > 248)
        goto L11; // [459] 500

        /** 					s[i] = comp_cache[c - CACHE0]*/
        _13230 = _c_22636 - 184;
        _2 = (int)SEQ_PTR(_58comp_cache_22538);
        _13231 = (int)*(((s1_ptr)_2)->base + _13230);
        Ref(_13231);
        _2 = (int)SEQ_PTR(_s_22637);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_22637 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_22711);
        _1 = *(int *)_2;
        *(int *)_2 = _13231;
        if( _1 != _13231 ){
            DeRef(_1);
        }
        _13231 = NOVALUE;
        goto L11; // [483] 500
LF: 

        /** 				s[i] = fdecompress(c)*/
        DeRef(_13232);
        _13232 = _c_22636;
        _13233 = _58fdecompress(_13232);
        _13232 = NOVALUE;
        _2 = (int)SEQ_PTR(_s_22637);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_22637 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_22711);
        _1 = *(int *)_2;
        *(int *)_2 = _13233;
        if( _1 != _13233 ){
            DeRef(_1);
        }
        _13233 = NOVALUE;
L11: 

        /** 		end for*/
        _i_22711 = _i_22711 + 1;
        goto LD; // [502] 417
LE: 
        ;
    }

    /** 		return s*/
    DeRef(_13169);
    _13169 = NOVALUE;
    DeRef(_13170);
    _13170 = NOVALUE;
    DeRef(_13172);
    _13172 = NOVALUE;
    _13173 = NOVALUE;
    DeRef(_13182);
    _13182 = NOVALUE;
    DeRef(_13194);
    _13194 = NOVALUE;
    DeRef(_13200);
    _13200 = NOVALUE;
    DeRef(_13228);
    _13228 = NOVALUE;
    DeRef(_13207);
    _13207 = NOVALUE;
    DeRef(_13218);
    _13218 = NOVALUE;
    DeRef(_13230);
    _13230 = NOVALUE;
    return _s_22637;
L6: 
    ;
}



// 0x4CF828B4
