// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _25print_sym(int _s_12007)
{
    int _s_obj_12010 = NOVALUE;
    int _6681 = NOVALUE;
    int _6680 = NOVALUE;
    int _6676 = NOVALUE;
    int _6674 = NOVALUE;
    int _6673 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_s_12007)) {
        _1 = (long)(DBL_PTR(_s_12007)->dbl);
        if (UNIQUE(DBL_PTR(_s_12007)) && (DBL_PTR(_s_12007)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_12007);
        _s_12007 = _1;
    }

    /** 	printf(1,"[%d]:\n", {s} )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _s_12007;
    _6673 = MAKE_SEQ(_1);
    EPrintf(1, _6672, _6673);
    DeRefDS(_6673);
    _6673 = NOVALUE;

    /** 	object s_obj = SymTab[s][S_OBJ]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _6674 = (int)*(((s1_ptr)_2)->base + _s_12007);
    DeRef(_s_obj_12010);
    _2 = (int)SEQ_PTR(_6674);
    _s_obj_12010 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_s_obj_12010);
    _6674 = NOVALUE;

    /** 	if equal(s_obj,NOVALUE) then */
    if (_s_obj_12010 == _25NOVALUE_12115)
    _6676 = 1;
    else if (IS_ATOM_INT(_s_obj_12010) && IS_ATOM_INT(_25NOVALUE_12115))
    _6676 = 0;
    else
    _6676 = (compare(_s_obj_12010, _25NOVALUE_12115) == 0);
    if (_6676 == 0)
    {
        _6676 = NOVALUE;
        goto L1; // [35] 46
    }
    else{
        _6676 = NOVALUE;
    }

    /** 		puts(1,"S_OBJ=>NOVALUE\n")*/
    EPuts(1, _6677); // DJP 
    goto L2; // [43] 57
L1: 

    /** 		puts(1,"S_OBJ=>")*/
    EPuts(1, _6678); // DJP 

    /** 		? s_obj		*/
    StdPrint(1, _s_obj_12010, 1);
L2: 

    /** 	puts(1,"S_MODE=>")*/
    EPuts(1, _6679); // DJP 

    /** 	switch SymTab[s][S_MODE] do*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _6680 = (int)*(((s1_ptr)_2)->base + _s_12007);
    _2 = (int)SEQ_PTR(_6680);
    _6681 = (int)*(((s1_ptr)_2)->base + 3);
    _6680 = NOVALUE;
    if (IS_SEQUENCE(_6681) ){
        goto L3; // [76] 124
    }
    if(!IS_ATOM_INT(_6681)){
        if( (DBL_PTR(_6681)->dbl != (double) ((int) DBL_PTR(_6681)->dbl) ) ){
            goto L3; // [76] 124
        }
        _0 = (int) DBL_PTR(_6681)->dbl;
    }
    else {
        _0 = _6681;
    };
    _6681 = NOVALUE;
    switch ( _0 ){ 

        /** 		case M_NORMAL then*/
        case 1:

        /** 			puts(1,"M_NORMAL")*/
        EPuts(1, _6684); // DJP 
        goto L3; // [90] 124

        /** 		case M_TEMP then*/
        case 3:

        /** 			puts(1,"M_TEMP")*/
        EPuts(1, _6685); // DJP 
        goto L3; // [101] 124

        /** 		case M_CONSTANT then*/
        case 2:

        /** 			puts(1,"M_CONSTANT")*/
        EPuts(1, _6686); // DJP 
        goto L3; // [112] 124

        /** 		case M_BLOCK then*/
        case 4:

        /** 			puts(1,"M_BLOCK")*/
        EPuts(1, _6687); // DJP 
    ;}L3: 

    /** 	puts(1,{10,10})*/
    EPuts(1, _114); // DJP 

    /** end procedure*/
    DeRef(_s_obj_12010);
    return;
    ;
}


int _25symtab_entry(int _x_12120)
{
    int _6727 = NOVALUE;
    int _6726 = NOVALUE;
    int _6725 = NOVALUE;
    int _6724 = NOVALUE;
    int _6723 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return length(x) = SIZEOF_ROUTINE_ENTRY or*/
    if (IS_SEQUENCE(_x_12120)){
            _6723 = SEQ_PTR(_x_12120)->length;
    }
    else {
        _6723 = 1;
    }
    _6724 = (_6723 == _25SIZEOF_ROUTINE_ENTRY_12038);
    _6723 = NOVALUE;
    if (IS_SEQUENCE(_x_12120)){
            _6725 = SEQ_PTR(_x_12120)->length;
    }
    else {
        _6725 = 1;
    }
    _6726 = (_6725 == _25SIZEOF_VAR_ENTRY_12041);
    _6725 = NOVALUE;
    _6727 = (_6724 != 0 || _6726 != 0);
    _6724 = NOVALUE;
    _6726 = NOVALUE;
    DeRefDS(_x_12120);
    return _6727;
    ;
}


int _25symtab_index(int _x_12128)
{
    int _6736 = NOVALUE;
    int _6735 = NOVALUE;
    int _6734 = NOVALUE;
    int _6733 = NOVALUE;
    int _6732 = NOVALUE;
    int _6731 = NOVALUE;
    int _6729 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_12128)) {
        _1 = (long)(DBL_PTR(_x_12128)->dbl);
        if (UNIQUE(DBL_PTR(_x_12128)) && (DBL_PTR(_x_12128)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_12128);
        _x_12128 = _1;
    }

    /** 	if x = 0 then*/
    if (_x_12128 != 0)
    goto L1; // [5] 18

    /** 		return TRUE -- NULL value*/
    return _5TRUE_244;
L1: 

    /** 	if x < 0 or x > length(SymTab) then*/
    _6729 = (_x_12128 < 0);
    if (_6729 != 0) {
        goto L2; // [24] 42
    }
    if (IS_SEQUENCE(_26SymTab_11138)){
            _6731 = SEQ_PTR(_26SymTab_11138)->length;
    }
    else {
        _6731 = 1;
    }
    _6732 = (_x_12128 > _6731);
    _6731 = NOVALUE;
    if (_6732 == 0)
    {
        DeRef(_6732);
        _6732 = NOVALUE;
        goto L3; // [38] 51
    }
    else{
        DeRef(_6732);
        _6732 = NOVALUE;
    }
L2: 

    /** 		return FALSE*/
    DeRef(_6729);
    _6729 = NOVALUE;
    return _5FALSE_242;
L3: 

    /** 	return find(length(SymTab[x]), {SIZEOF_VAR_ENTRY, SIZEOF_ROUTINE_ENTRY,*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _6733 = (int)*(((s1_ptr)_2)->base + _x_12128);
    if (IS_SEQUENCE(_6733)){
            _6734 = SEQ_PTR(_6733)->length;
    }
    else {
        _6734 = 1;
    }
    _6733 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _25SIZEOF_VAR_ENTRY_12041;
    *((int *)(_2+8)) = _25SIZEOF_ROUTINE_ENTRY_12038;
    *((int *)(_2+12)) = _25SIZEOF_TEMP_ENTRY_12047;
    *((int *)(_2+16)) = _25SIZEOF_BLOCK_ENTRY_12044;
    _6735 = MAKE_SEQ(_1);
    _6736 = find_from(_6734, _6735, 1);
    _6734 = NOVALUE;
    DeRefDS(_6735);
    _6735 = NOVALUE;
    DeRef(_6729);
    _6729 = NOVALUE;
    _6733 = NOVALUE;
    return _6736;
    ;
}


int _25temp_index(int _x_12146)
{
    int _6740 = NOVALUE;
    int _6739 = NOVALUE;
    int _6738 = NOVALUE;
    int _6737 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_12146)) {
        _1 = (long)(DBL_PTR(_x_12146)->dbl);
        if (UNIQUE(DBL_PTR(_x_12146)) && (DBL_PTR(_x_12146)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_12146);
        _x_12146 = _1;
    }

    /** 	return x >= 0 and x <= length(SymTab)*/
    _6737 = (_x_12146 >= 0);
    if (IS_SEQUENCE(_26SymTab_11138)){
            _6738 = SEQ_PTR(_26SymTab_11138)->length;
    }
    else {
        _6738 = 1;
    }
    _6739 = (_x_12146 <= _6738);
    _6738 = NOVALUE;
    _6740 = (_6737 != 0 && _6739 != 0);
    _6737 = NOVALUE;
    _6739 = NOVALUE;
    return _6740;
    ;
}


int _25token(int _t_12157)
{
    int _6785 = NOVALUE;
    int _6784 = NOVALUE;
    int _6783 = NOVALUE;
    int _6782 = NOVALUE;
    int _6781 = NOVALUE;
    int _6780 = NOVALUE;
    int _6779 = NOVALUE;
    int _6778 = NOVALUE;
    int _6777 = NOVALUE;
    int _6776 = NOVALUE;
    int _6774 = NOVALUE;
    int _6773 = NOVALUE;
    int _6772 = NOVALUE;
    int _6771 = NOVALUE;
    int _6770 = NOVALUE;
    int _6769 = NOVALUE;
    int _6768 = NOVALUE;
    int _6767 = NOVALUE;
    int _6766 = NOVALUE;
    int _6765 = NOVALUE;
    int _6764 = NOVALUE;
    int _6763 = NOVALUE;
    int _6762 = NOVALUE;
    int _6761 = NOVALUE;
    int _6760 = NOVALUE;
    int _6759 = NOVALUE;
    int _6758 = NOVALUE;
    int _6757 = NOVALUE;
    int _6756 = NOVALUE;
    int _6755 = NOVALUE;
    int _6754 = NOVALUE;
    int _6753 = NOVALUE;
    int _6752 = NOVALUE;
    int _6751 = NOVALUE;
    int _6750 = NOVALUE;
    int _6749 = NOVALUE;
    int _6748 = NOVALUE;
    int _6746 = NOVALUE;
    int _6745 = NOVALUE;
    int _6743 = NOVALUE;
    int _6742 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(t) then*/
    _6742 = IS_ATOM(_t_12157);
    if (_6742 == 0)
    {
        _6742 = NOVALUE;
        goto L1; // [6] 18
    }
    else{
        _6742 = NOVALUE;
    }

    /** 		return FALSE*/
    DeRef(_t_12157);
    return _5FALSE_242;
L1: 

    /** 	if length(t) != 2 then*/
    if (IS_SEQUENCE(_t_12157)){
            _6743 = SEQ_PTR(_t_12157)->length;
    }
    else {
        _6743 = 1;
    }
    if (_6743 == 2)
    goto L2; // [23] 36

    /** 		return FALSE*/
    DeRef(_t_12157);
    return _5FALSE_242;
L2: 

    /** 	if not integer(t[T_ID]) then*/
    _2 = (int)SEQ_PTR(_t_12157);
    _6745 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_6745))
    _6746 = 1;
    else if (IS_ATOM_DBL(_6745))
    _6746 = IS_ATOM_INT(DoubleToInt(_6745));
    else
    _6746 = 0;
    _6745 = NOVALUE;
    if (_6746 != 0)
    goto L3; // [47] 59
    _6746 = NOVALUE;

    /** 		return FALSE*/
    DeRef(_t_12157);
    return _5FALSE_242;
L3: 

    /** 	if t[T_ID] = VARIABLE and (t[T_SYM] < 0 or symtab_index(t[T_SYM])) then*/
    _2 = (int)SEQ_PTR(_t_12157);
    _6748 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_6748)) {
        _6749 = (_6748 == -100);
    }
    else {
        _6749 = binary_op(EQUALS, _6748, -100);
    }
    _6748 = NOVALUE;
    if (IS_ATOM_INT(_6749)) {
        if (_6749 == 0) {
            goto L4; // [73] 118
        }
    }
    else {
        if (DBL_PTR(_6749)->dbl == 0.0) {
            goto L4; // [73] 118
        }
    }
    _2 = (int)SEQ_PTR(_t_12157);
    _6751 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_6751)) {
        _6752 = (_6751 < 0);
    }
    else {
        _6752 = binary_op(LESS, _6751, 0);
    }
    _6751 = NOVALUE;
    if (IS_ATOM_INT(_6752)) {
        if (_6752 != 0) {
            DeRef(_6753);
            _6753 = 1;
            goto L5; // [87] 105
        }
    }
    else {
        if (DBL_PTR(_6752)->dbl != 0.0) {
            DeRef(_6753);
            _6753 = 1;
            goto L5; // [87] 105
        }
    }
    _2 = (int)SEQ_PTR(_t_12157);
    _6754 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_6754);
    _6755 = _25symtab_index(_6754);
    _6754 = NOVALUE;
    DeRef(_6753);
    if (IS_ATOM_INT(_6755))
    _6753 = (_6755 != 0);
    else
    _6753 = DBL_PTR(_6755)->dbl != 0.0;
L5: 
    if (_6753 == 0)
    {
        _6753 = NOVALUE;
        goto L4; // [106] 118
    }
    else{
        _6753 = NOVALUE;
    }

    /** 		return TRUE*/
    DeRef(_t_12157);
    DeRef(_6749);
    _6749 = NOVALUE;
    DeRef(_6752);
    _6752 = NOVALUE;
    DeRef(_6755);
    _6755 = NOVALUE;
    return _5TRUE_244;
L4: 

    /** 	if QUESTION_MARK <= t[T_ID] and t[T_ID] <= -1 then*/
    _2 = (int)SEQ_PTR(_t_12157);
    _6756 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_6756)) {
        _6757 = (-31 <= _6756);
    }
    else {
        _6757 = binary_op(LESSEQ, -31, _6756);
    }
    _6756 = NOVALUE;
    if (IS_ATOM_INT(_6757)) {
        if (_6757 == 0) {
            goto L6; // [132] 159
        }
    }
    else {
        if (DBL_PTR(_6757)->dbl == 0.0) {
            goto L6; // [132] 159
        }
    }
    _2 = (int)SEQ_PTR(_t_12157);
    _6759 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_6759)) {
        _6760 = (_6759 <= -1);
    }
    else {
        _6760 = binary_op(LESSEQ, _6759, -1);
    }
    _6759 = NOVALUE;
    if (_6760 == 0) {
        DeRef(_6760);
        _6760 = NOVALUE;
        goto L6; // [147] 159
    }
    else {
        if (!IS_ATOM_INT(_6760) && DBL_PTR(_6760)->dbl == 0.0){
            DeRef(_6760);
            _6760 = NOVALUE;
            goto L6; // [147] 159
        }
        DeRef(_6760);
        _6760 = NOVALUE;
    }
    DeRef(_6760);
    _6760 = NOVALUE;

    /** 		return TRUE*/
    DeRef(_t_12157);
    DeRef(_6749);
    _6749 = NOVALUE;
    DeRef(_6752);
    _6752 = NOVALUE;
    DeRef(_6755);
    _6755 = NOVALUE;
    DeRef(_6757);
    _6757 = NOVALUE;
    return _5TRUE_244;
L6: 

    /** 	if t[T_ID] >= 1 and t[T_ID] <= MAX_OPCODE then*/
    _2 = (int)SEQ_PTR(_t_12157);
    _6761 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_6761)) {
        _6762 = (_6761 >= 1);
    }
    else {
        _6762 = binary_op(GREATEREQ, _6761, 1);
    }
    _6761 = NOVALUE;
    if (IS_ATOM_INT(_6762)) {
        if (_6762 == 0) {
            goto L7; // [171] 200
        }
    }
    else {
        if (DBL_PTR(_6762)->dbl == 0.0) {
            goto L7; // [171] 200
        }
    }
    _2 = (int)SEQ_PTR(_t_12157);
    _6764 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_6764)) {
        _6765 = (_6764 <= 213);
    }
    else {
        _6765 = binary_op(LESSEQ, _6764, 213);
    }
    _6764 = NOVALUE;
    if (_6765 == 0) {
        DeRef(_6765);
        _6765 = NOVALUE;
        goto L7; // [188] 200
    }
    else {
        if (!IS_ATOM_INT(_6765) && DBL_PTR(_6765)->dbl == 0.0){
            DeRef(_6765);
            _6765 = NOVALUE;
            goto L7; // [188] 200
        }
        DeRef(_6765);
        _6765 = NOVALUE;
    }
    DeRef(_6765);
    _6765 = NOVALUE;

    /** 		return TRUE*/
    DeRef(_t_12157);
    DeRef(_6749);
    _6749 = NOVALUE;
    DeRef(_6752);
    _6752 = NOVALUE;
    DeRef(_6755);
    _6755 = NOVALUE;
    DeRef(_6757);
    _6757 = NOVALUE;
    DeRef(_6762);
    _6762 = NOVALUE;
    return _5TRUE_244;
L7: 

    /** 	if END <= t[T_ID] and t[T_ID] <=  ROUTINE then*/
    _2 = (int)SEQ_PTR(_t_12157);
    _6766 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_6766)) {
        _6767 = (402 <= _6766);
    }
    else {
        _6767 = binary_op(LESSEQ, 402, _6766);
    }
    _6766 = NOVALUE;
    if (IS_ATOM_INT(_6767)) {
        if (_6767 == 0) {
            goto L8; // [214] 306
        }
    }
    else {
        if (DBL_PTR(_6767)->dbl == 0.0) {
            goto L8; // [214] 306
        }
    }
    _2 = (int)SEQ_PTR(_t_12157);
    _6769 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_6769)) {
        _6770 = (_6769 <= 432);
    }
    else {
        _6770 = binary_op(LESSEQ, _6769, 432);
    }
    _6769 = NOVALUE;
    if (_6770 == 0) {
        DeRef(_6770);
        _6770 = NOVALUE;
        goto L8; // [231] 306
    }
    else {
        if (!IS_ATOM_INT(_6770) && DBL_PTR(_6770)->dbl == 0.0){
            DeRef(_6770);
            _6770 = NOVALUE;
            goto L8; // [231] 306
        }
        DeRef(_6770);
        _6770 = NOVALUE;
    }
    DeRef(_6770);
    _6770 = NOVALUE;

    /** 		if t[T_ID] != IGNORED and t[T_ID] < 500 and symtab_index(t[T_SYM]) = 0 then*/
    _2 = (int)SEQ_PTR(_t_12157);
    _6771 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_6771)) {
        _6772 = (_6771 != 509);
    }
    else {
        _6772 = binary_op(NOTEQ, _6771, 509);
    }
    _6771 = NOVALUE;
    if (IS_ATOM_INT(_6772)) {
        if (_6772 == 0) {
            DeRef(_6773);
            _6773 = 0;
            goto L9; // [248] 266
        }
    }
    else {
        if (DBL_PTR(_6772)->dbl == 0.0) {
            DeRef(_6773);
            _6773 = 0;
            goto L9; // [248] 266
        }
    }
    _2 = (int)SEQ_PTR(_t_12157);
    _6774 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_6774)) {
        _6776 = (_6774 < 500);
    }
    else {
        _6776 = binary_op(LESS, _6774, 500);
    }
    _6774 = NOVALUE;
    DeRef(_6773);
    if (IS_ATOM_INT(_6776))
    _6773 = (_6776 != 0);
    else
    _6773 = DBL_PTR(_6776)->dbl != 0.0;
L9: 
    if (_6773 == 0) {
        goto LA; // [266] 297
    }
    _2 = (int)SEQ_PTR(_t_12157);
    _6778 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_6778);
    _6779 = _25symtab_index(_6778);
    _6778 = NOVALUE;
    if (IS_ATOM_INT(_6779)) {
        _6780 = (_6779 == 0);
    }
    else {
        _6780 = binary_op(EQUALS, _6779, 0);
    }
    DeRef(_6779);
    _6779 = NOVALUE;
    if (_6780 == 0) {
        DeRef(_6780);
        _6780 = NOVALUE;
        goto LA; // [285] 297
    }
    else {
        if (!IS_ATOM_INT(_6780) && DBL_PTR(_6780)->dbl == 0.0){
            DeRef(_6780);
            _6780 = NOVALUE;
            goto LA; // [285] 297
        }
        DeRef(_6780);
        _6780 = NOVALUE;
    }
    DeRef(_6780);
    _6780 = NOVALUE;

    /** 			return FALSE*/
    DeRef(_t_12157);
    DeRef(_6749);
    _6749 = NOVALUE;
    DeRef(_6752);
    _6752 = NOVALUE;
    DeRef(_6755);
    _6755 = NOVALUE;
    DeRef(_6757);
    _6757 = NOVALUE;
    DeRef(_6762);
    _6762 = NOVALUE;
    DeRef(_6767);
    _6767 = NOVALUE;
    DeRef(_6772);
    _6772 = NOVALUE;
    DeRef(_6776);
    _6776 = NOVALUE;
    return _5FALSE_242;
LA: 

    /** 		return TRUE*/
    DeRef(_t_12157);
    DeRef(_6749);
    _6749 = NOVALUE;
    DeRef(_6752);
    _6752 = NOVALUE;
    DeRef(_6755);
    _6755 = NOVALUE;
    DeRef(_6757);
    _6757 = NOVALUE;
    DeRef(_6762);
    _6762 = NOVALUE;
    DeRef(_6767);
    _6767 = NOVALUE;
    DeRef(_6772);
    _6772 = NOVALUE;
    DeRef(_6776);
    _6776 = NOVALUE;
    return _5TRUE_244;
L8: 

    /** 	if FUNC <= t[T_ID] and t[T_ID] <= NAMESPACE then*/
    _2 = (int)SEQ_PTR(_t_12157);
    _6781 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_6781)) {
        _6782 = (501 <= _6781);
    }
    else {
        _6782 = binary_op(LESSEQ, 501, _6781);
    }
    _6781 = NOVALUE;
    if (IS_ATOM_INT(_6782)) {
        if (_6782 == 0) {
            goto LB; // [320] 349
        }
    }
    else {
        if (DBL_PTR(_6782)->dbl == 0.0) {
            goto LB; // [320] 349
        }
    }
    _2 = (int)SEQ_PTR(_t_12157);
    _6784 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_6784)) {
        _6785 = (_6784 <= 523);
    }
    else {
        _6785 = binary_op(LESSEQ, _6784, 523);
    }
    _6784 = NOVALUE;
    if (_6785 == 0) {
        DeRef(_6785);
        _6785 = NOVALUE;
        goto LB; // [337] 349
    }
    else {
        if (!IS_ATOM_INT(_6785) && DBL_PTR(_6785)->dbl == 0.0){
            DeRef(_6785);
            _6785 = NOVALUE;
            goto LB; // [337] 349
        }
        DeRef(_6785);
        _6785 = NOVALUE;
    }
    DeRef(_6785);
    _6785 = NOVALUE;

    /** 		return TRUE*/
    DeRef(_t_12157);
    DeRef(_6749);
    _6749 = NOVALUE;
    DeRef(_6752);
    _6752 = NOVALUE;
    DeRef(_6755);
    _6755 = NOVALUE;
    DeRef(_6757);
    _6757 = NOVALUE;
    DeRef(_6762);
    _6762 = NOVALUE;
    DeRef(_6767);
    _6767 = NOVALUE;
    DeRef(_6772);
    _6772 = NOVALUE;
    DeRef(_6776);
    _6776 = NOVALUE;
    DeRef(_6782);
    _6782 = NOVALUE;
    return _5TRUE_244;
LB: 

    /** 	return FALSE*/
    DeRef(_t_12157);
    DeRef(_6749);
    _6749 = NOVALUE;
    DeRef(_6752);
    _6752 = NOVALUE;
    DeRef(_6755);
    _6755 = NOVALUE;
    DeRef(_6757);
    _6757 = NOVALUE;
    DeRef(_6762);
    _6762 = NOVALUE;
    DeRef(_6767);
    _6767 = NOVALUE;
    DeRef(_6772);
    _6772 = NOVALUE;
    DeRef(_6776);
    _6776 = NOVALUE;
    DeRef(_6782);
    _6782 = NOVALUE;
    return _5FALSE_242;
    ;
}


int _25sequence_of_tokens(int _x_12231)
{
    int _t_12232 = NOVALUE;
    int _6787 = NOVALUE;
    int _6786 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(x) then*/
    _6786 = IS_ATOM(_x_12231);
    if (_6786 == 0)
    {
        _6786 = NOVALUE;
        goto L1; // [6] 18
    }
    else{
        _6786 = NOVALUE;
    }

    /** 		return FALSE*/
    DeRef(_x_12231);
    DeRef(_t_12232);
    return _5FALSE_242;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_12231)){
            _6787 = SEQ_PTR(_x_12231)->length;
    }
    else {
        _6787 = 1;
    }
    {
        int _i_12237;
        _i_12237 = 1;
L2: 
        if (_i_12237 > _6787){
            goto L3; // [23] 48
        }

        /** 		type_i = i*/
        _25type_i_11887 = _i_12237;

        /** 		t = x[i]*/
        DeRef(_t_12232);
        _2 = (int)SEQ_PTR(_x_12231);
        _t_12232 = (int)*(((s1_ptr)_2)->base + _i_12237);
        Ref(_t_12232);

        /** 	end for*/
        _i_12237 = _i_12237 + 1;
        goto L2; // [43] 30
L3: 
        ;
    }

    /** 	return TRUE*/
    DeRef(_x_12231);
    DeRef(_t_12232);
    return _5TRUE_244;
    ;
}


int _25sequence_of_opcodes(int _s_12243)
{
    int _oc_12244 = NOVALUE;
    int _6790 = NOVALUE;
    int _6789 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(s) then*/
    _6789 = IS_ATOM(_s_12243);
    if (_6789 == 0)
    {
        _6789 = NOVALUE;
        goto L1; // [6] 18
    }
    else{
        _6789 = NOVALUE;
    }

    /** 		return FALSE*/
    DeRef(_s_12243);
    return _5FALSE_242;
L1: 

    /** 	for i = 1 to length(s) do*/
    if (IS_SEQUENCE(_s_12243)){
            _6790 = SEQ_PTR(_s_12243)->length;
    }
    else {
        _6790 = 1;
    }
    {
        int _i_12249;
        _i_12249 = 1;
L2: 
        if (_i_12249 > _6790){
            goto L3; // [23] 45
        }

        /** 		oc = s[i]*/
        _2 = (int)SEQ_PTR(_s_12243);
        _oc_12244 = (int)*(((s1_ptr)_2)->base + _i_12249);
        if (!IS_ATOM_INT(_oc_12244)){
            _oc_12244 = (long)DBL_PTR(_oc_12244)->dbl;
        }

        /** 	end for*/
        _i_12249 = _i_12249 + 1;
        goto L2; // [40] 30
L3: 
        ;
    }

    /** 	return TRUE*/
    DeRef(_s_12243);
    return _5TRUE_244;
    ;
}


int _25file(int _f_12255)
{
    int _6794 = NOVALUE;
    int _6793 = NOVALUE;
    int _6792 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_f_12255)) {
        _1 = (long)(DBL_PTR(_f_12255)->dbl);
        if (UNIQUE(DBL_PTR(_f_12255)) && (DBL_PTR(_f_12255)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_f_12255);
        _f_12255 = _1;
    }

    /** 	return f >= -1 and f < 100 -- rough limit*/
    _6792 = (_f_12255 >= -1);
    _6793 = (_f_12255 < 100);
    _6794 = (_6792 != 0 && _6793 != 0);
    _6792 = NOVALUE;
    _6793 = NOVALUE;
    return _6794;
    ;
}


int _25op_code_or_negative_one(int _x_12370)
{
    int _6840 = NOVALUE;
    int _6839 = NOVALUE;
    int _6838 = NOVALUE;
    int _6837 = NOVALUE;
    int _6836 = NOVALUE;
    int _6835 = NOVALUE;
    int _6834 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return integer(x) and (x=-1 or (1 <= x and x <= MAX_OPCODE))*/
    if (IS_ATOM_INT(_x_12370))
    _6834 = 1;
    else if (IS_ATOM_DBL(_x_12370))
    _6834 = IS_ATOM_INT(DoubleToInt(_x_12370));
    else
    _6834 = 0;
    if (IS_ATOM_INT(_x_12370)) {
        _6835 = (_x_12370 == -1);
    }
    else {
        _6835 = binary_op(EQUALS, _x_12370, -1);
    }
    if (IS_ATOM_INT(_x_12370)) {
        _6836 = (1 <= _x_12370);
    }
    else {
        _6836 = binary_op(LESSEQ, 1, _x_12370);
    }
    if (IS_ATOM_INT(_x_12370)) {
        _6837 = (_x_12370 <= 213);
    }
    else {
        _6837 = binary_op(LESSEQ, _x_12370, 213);
    }
    if (IS_ATOM_INT(_6836) && IS_ATOM_INT(_6837)) {
        _6838 = (_6836 != 0 && _6837 != 0);
    }
    else {
        _6838 = binary_op(AND, _6836, _6837);
    }
    DeRef(_6836);
    _6836 = NOVALUE;
    DeRef(_6837);
    _6837 = NOVALUE;
    if (IS_ATOM_INT(_6835) && IS_ATOM_INT(_6838)) {
        _6839 = (_6835 != 0 || _6838 != 0);
    }
    else {
        _6839 = binary_op(OR, _6835, _6838);
    }
    DeRef(_6835);
    _6835 = NOVALUE;
    DeRef(_6838);
    _6838 = NOVALUE;
    if (IS_ATOM_INT(_6839)) {
        _6840 = (_6834 != 0 && _6839 != 0);
    }
    else {
        _6840 = binary_op(AND, _6834, _6839);
    }
    _6834 = NOVALUE;
    DeRef(_6839);
    _6839 = NOVALUE;
    DeRef(_x_12370);
    return _6840;
    ;
}


int _25symtab_pointer(int _x_64252)
{
    int _32351 = NOVALUE;
    int _32350 = NOVALUE;
    int _32349 = NOVALUE;
    int _32348 = NOVALUE;
    int _32347 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_64252)) {
        _1 = (long)(DBL_PTR(_x_64252)->dbl);
        if (UNIQUE(DBL_PTR(_x_64252)) && (DBL_PTR(_x_64252)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_64252);
        _x_64252 = _1;
    }

    /** 	return x = -1 or symtab_index(x) or forward_reference(x)*/
    _32347 = (_x_64252 == -1);
    _32348 = _25symtab_index(_x_64252);
    if (IS_ATOM_INT(_32348)) {
        _32349 = (_32347 != 0 || _32348 != 0);
    }
    else {
        _32349 = binary_op(OR, _32347, _32348);
    }
    _32347 = NOVALUE;
    DeRef(_32348);
    _32348 = NOVALUE;
    _32350 = _29forward_reference(_x_64252);
    if (IS_ATOM_INT(_32349) && IS_ATOM_INT(_32350)) {
        _32351 = (_32349 != 0 || _32350 != 0);
    }
    else {
        _32351 = binary_op(OR, _32349, _32350);
    }
    DeRef(_32349);
    _32349 = NOVALUE;
    DeRef(_32350);
    _32350 = NOVALUE;
    return _32351;
    ;
}



// 0x22A9CA33
