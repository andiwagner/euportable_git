// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _52hashfn(int _name_47116)
{
    int _len_47117 = NOVALUE;
    int _val_47118 = NOVALUE;
    int _int_47119 = NOVALUE;
    int _25079 = NOVALUE;
    int _25078 = NOVALUE;
    int _25075 = NOVALUE;
    int _25074 = NOVALUE;
    int _25063 = NOVALUE;
    int _25059 = NOVALUE;
    int _0, _1, _2;
    

    /** 	len = length(name)*/
    if (IS_SEQUENCE(_name_47116)){
            _len_47117 = SEQ_PTR(_name_47116)->length;
    }
    else {
        _len_47117 = 1;
    }

    /** 	val = name[1]*/
    _2 = (int)SEQ_PTR(_name_47116);
    _val_47118 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_val_47118))
    _val_47118 = (long)DBL_PTR(_val_47118)->dbl;

    /** 	int = name[$]*/
    if (IS_SEQUENCE(_name_47116)){
            _25059 = SEQ_PTR(_name_47116)->length;
    }
    else {
        _25059 = 1;
    }
    _2 = (int)SEQ_PTR(_name_47116);
    _int_47119 = (int)*(((s1_ptr)_2)->base + _25059);
    if (!IS_ATOM_INT(_int_47119))
    _int_47119 = (long)DBL_PTR(_int_47119)->dbl;

    /** 	int *= 256*/
    _int_47119 = _int_47119 * 256;

    /** 	val *= 2*/
    _val_47118 = _val_47118 + _val_47118;

    /** 	val += int + len*/
    _25063 = _int_47119 + _len_47117;
    if ((long)((unsigned long)_25063 + (unsigned long)HIGH_BITS) >= 0) 
    _25063 = NewDouble((double)_25063);
    if (IS_ATOM_INT(_25063)) {
        _val_47118 = _val_47118 + _25063;
    }
    else {
        _val_47118 = NewDouble((double)_val_47118 + DBL_PTR(_25063)->dbl);
    }
    DeRef(_25063);
    _25063 = NOVALUE;
    if (!IS_ATOM_INT(_val_47118)) {
        _1 = (long)(DBL_PTR(_val_47118)->dbl);
        if (UNIQUE(DBL_PTR(_val_47118)) && (DBL_PTR(_val_47118)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_val_47118);
        _val_47118 = _1;
    }

    /** 	if len = 3 then*/
    if (_len_47117 != 3)
    goto L1; // [51] 78

    /** 		val *= 32*/
    _val_47118 = _val_47118 * 32;

    /** 		int = name[2]*/
    _2 = (int)SEQ_PTR(_name_47116);
    _int_47119 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_int_47119))
    _int_47119 = (long)DBL_PTR(_int_47119)->dbl;

    /** 		val += int*/
    _val_47118 = _val_47118 + _int_47119;
    goto L2; // [75] 133
L1: 

    /** 	elsif len > 3 then*/
    if (_len_47117 <= 3)
    goto L3; // [80] 132

    /** 		val *= 32*/
    _val_47118 = _val_47118 * 32;

    /** 		int = name[2]*/
    _2 = (int)SEQ_PTR(_name_47116);
    _int_47119 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_int_47119))
    _int_47119 = (long)DBL_PTR(_int_47119)->dbl;

    /** 		val += int*/
    _val_47118 = _val_47118 + _int_47119;

    /** 		val *= 32*/
    _val_47118 = _val_47118 * 32;

    /** 		int = name[$-1]*/
    if (IS_SEQUENCE(_name_47116)){
            _25074 = SEQ_PTR(_name_47116)->length;
    }
    else {
        _25074 = 1;
    }
    _25075 = _25074 - 1;
    _25074 = NOVALUE;
    _2 = (int)SEQ_PTR(_name_47116);
    _int_47119 = (int)*(((s1_ptr)_2)->base + _25075);
    if (!IS_ATOM_INT(_int_47119))
    _int_47119 = (long)DBL_PTR(_int_47119)->dbl;

    /** 		val += int*/
    _val_47118 = _val_47118 + _int_47119;
L3: 
L2: 

    /** 	return remainder(val, NBUCKETS) + 1*/
    _25078 = (_val_47118 % 2003);
    _25079 = _25078 + 1;
    _25078 = NOVALUE;
    DeRefDS(_name_47116);
    DeRef(_25075);
    _25075 = NOVALUE;
    return _25079;
    ;
}


void _52remove_symbol(int _sym_47148)
{
    int _hash_47149 = NOVALUE;
    int _st_ptr_47150 = NOVALUE;
    int _25094 = NOVALUE;
    int _25093 = NOVALUE;
    int _25091 = NOVALUE;
    int _25090 = NOVALUE;
    int _25089 = NOVALUE;
    int _25087 = NOVALUE;
    int _25085 = NOVALUE;
    int _25084 = NOVALUE;
    int _25083 = NOVALUE;
    int _25080 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_sym_47148)) {
        _1 = (long)(DBL_PTR(_sym_47148)->dbl);
        if (UNIQUE(DBL_PTR(_sym_47148)) && (DBL_PTR(_sym_47148)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_47148);
        _sym_47148 = _1;
    }

    /** 	hash = SymTab[sym][S_HASHVAL]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25080 = (int)*(((s1_ptr)_2)->base + _sym_47148);
    _2 = (int)SEQ_PTR(_25080);
    _hash_47149 = (int)*(((s1_ptr)_2)->base + 11);
    if (!IS_ATOM_INT(_hash_47149)){
        _hash_47149 = (long)DBL_PTR(_hash_47149)->dbl;
    }
    _25080 = NOVALUE;

    /** 	st_ptr = buckets[hash]*/
    _2 = (int)SEQ_PTR(_52buckets_47095);
    _st_ptr_47150 = (int)*(((s1_ptr)_2)->base + _hash_47149);
    if (!IS_ATOM_INT(_st_ptr_47150))
    _st_ptr_47150 = (long)DBL_PTR(_st_ptr_47150)->dbl;

    /** 	while st_ptr and st_ptr != sym do*/
L1: 
    if (_st_ptr_47150 == 0) {
        goto L2; // [32] 65
    }
    _25084 = (_st_ptr_47150 != _sym_47148);
    if (_25084 == 0)
    {
        DeRef(_25084);
        _25084 = NOVALUE;
        goto L2; // [41] 65
    }
    else{
        DeRef(_25084);
        _25084 = NOVALUE;
    }

    /** 		st_ptr = SymTab[st_ptr][S_SAMEHASH]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25085 = (int)*(((s1_ptr)_2)->base + _st_ptr_47150);
    _2 = (int)SEQ_PTR(_25085);
    _st_ptr_47150 = (int)*(((s1_ptr)_2)->base + 9);
    if (!IS_ATOM_INT(_st_ptr_47150)){
        _st_ptr_47150 = (long)DBL_PTR(_st_ptr_47150)->dbl;
    }
    _25085 = NOVALUE;

    /** 	end while*/
    goto L1; // [62] 32
L2: 

    /** 	if st_ptr then*/
    if (_st_ptr_47150 == 0)
    {
        goto L3; // [67] 134
    }
    else{
    }

    /** 		if st_ptr = buckets[hash] then*/
    _2 = (int)SEQ_PTR(_52buckets_47095);
    _25087 = (int)*(((s1_ptr)_2)->base + _hash_47149);
    if (binary_op_a(NOTEQ, _st_ptr_47150, _25087)){
        _25087 = NOVALUE;
        goto L4; // [78] 105
    }
    _25087 = NOVALUE;

    /** 			buckets[hash] = SymTab[st_ptr][S_SAMEHASH]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25089 = (int)*(((s1_ptr)_2)->base + _st_ptr_47150);
    _2 = (int)SEQ_PTR(_25089);
    _25090 = (int)*(((s1_ptr)_2)->base + 9);
    _25089 = NOVALUE;
    Ref(_25090);
    _2 = (int)SEQ_PTR(_52buckets_47095);
    _2 = (int)(((s1_ptr)_2)->base + _hash_47149);
    _1 = *(int *)_2;
    *(int *)_2 = _25090;
    if( _1 != _25090 ){
        DeRef(_1);
    }
    _25090 = NOVALUE;
    goto L5; // [102] 133
L4: 

    /** 			SymTab[st_ptr][S_SAMEHASH] = SymTab[sym][S_SAMEHASH]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_st_ptr_47150 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25093 = (int)*(((s1_ptr)_2)->base + _sym_47148);
    _2 = (int)SEQ_PTR(_25093);
    _25094 = (int)*(((s1_ptr)_2)->base + 9);
    _25093 = NOVALUE;
    Ref(_25094);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 9);
    _1 = *(int *)_2;
    *(int *)_2 = _25094;
    if( _1 != _25094 ){
        DeRef(_1);
    }
    _25094 = NOVALUE;
    _25091 = NOVALUE;
L5: 
L3: 

    /** end procedure*/
    return;
    ;
}


int _52NewBasicEntry(int _name_47182, int _varnum_47183, int _scope_47184, int _token_47185, int _hashval_47186, int _samehash_47188, int _type_sym_47190)
{
    int _new_47191 = NOVALUE;
    int _25103 = NOVALUE;
    int _25101 = NOVALUE;
    int _25100 = NOVALUE;
    int _25099 = NOVALUE;
    int _25098 = NOVALUE;
    int _25097 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_varnum_47183)) {
        _1 = (long)(DBL_PTR(_varnum_47183)->dbl);
        if (UNIQUE(DBL_PTR(_varnum_47183)) && (DBL_PTR(_varnum_47183)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_varnum_47183);
        _varnum_47183 = _1;
    }
    if (!IS_ATOM_INT(_scope_47184)) {
        _1 = (long)(DBL_PTR(_scope_47184)->dbl);
        if (UNIQUE(DBL_PTR(_scope_47184)) && (DBL_PTR(_scope_47184)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_scope_47184);
        _scope_47184 = _1;
    }
    if (!IS_ATOM_INT(_token_47185)) {
        _1 = (long)(DBL_PTR(_token_47185)->dbl);
        if (UNIQUE(DBL_PTR(_token_47185)) && (DBL_PTR(_token_47185)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_token_47185);
        _token_47185 = _1;
    }
    if (!IS_ATOM_INT(_hashval_47186)) {
        _1 = (long)(DBL_PTR(_hashval_47186)->dbl);
        if (UNIQUE(DBL_PTR(_hashval_47186)) && (DBL_PTR(_hashval_47186)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_hashval_47186);
        _hashval_47186 = _1;
    }
    if (!IS_ATOM_INT(_samehash_47188)) {
        _1 = (long)(DBL_PTR(_samehash_47188)->dbl);
        if (UNIQUE(DBL_PTR(_samehash_47188)) && (DBL_PTR(_samehash_47188)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_samehash_47188);
        _samehash_47188 = _1;
    }
    if (!IS_ATOM_INT(_type_sym_47190)) {
        _1 = (long)(DBL_PTR(_type_sym_47190)->dbl);
        if (UNIQUE(DBL_PTR(_type_sym_47190)) && (DBL_PTR(_type_sym_47190)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_type_sym_47190);
        _type_sym_47190 = _1;
    }

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L1; // [19] 33
    }
    else{
    }

    /** 		new = repeat(0, SIZEOF_ROUTINE_ENTRY)*/
    DeRef(_new_47191);
    _new_47191 = Repeat(0, _25SIZEOF_ROUTINE_ENTRY_12038);
    goto L2; // [30] 42
L1: 

    /** 		new = repeat(0, SIZEOF_VAR_ENTRY)*/
    DeRef(_new_47191);
    _new_47191 = Repeat(0, _25SIZEOF_VAR_ENTRY_12041);
L2: 

    /** 	new[S_NEXT] = 0*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	new[S_NAME] = name*/
    RefDS(_name_47182);
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_NAME_11913))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_NAME_11913);
    _1 = *(int *)_2;
    *(int *)_2 = _name_47182;
    DeRef(_1);

    /** 	new[S_SCOPE] = scope*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _scope_47184;
    DeRef(_1);

    /** 	new[S_MODE] = M_NORMAL*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);

    /** 	new[S_USAGE] = U_UNUSED*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	new[S_FILE_NO] = current_file_no*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_FILE_NO_11909))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    _1 = *(int *)_2;
    *(int *)_2 = _25current_file_no_12262;
    DeRef(_1);

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L3; // [102] 327
    }
    else{
    }

    /** 		new[S_GTYPE] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);

    /** 		new[S_GTYPE_NEW] = TYPE_NULL*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 38);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		new[S_SEQ_ELEM] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);

    /** 		new[S_SEQ_ELEM_NEW] = TYPE_NULL -- starting point for ORing*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 40);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		new[S_ARG_TYPE] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 43);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);

    /** 		new[S_ARG_TYPE_NEW] = TYPE_NULL*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 44);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		new[S_ARG_SEQ_ELEM] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 45);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);

    /** 		new[S_ARG_SEQ_ELEM_NEW] = TYPE_NULL*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 46);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		new[S_ARG_MIN] = NOVALUE*/
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 47);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);

    /** 		new[S_ARG_MIN_NEW] = -NOVALUE*/
    if (IS_ATOM_INT(_25NOVALUE_12115)) {
        if ((unsigned long)_25NOVALUE_12115 == 0xC0000000)
        _25097 = (int)NewDouble((double)-0xC0000000);
        else
        _25097 = - _25NOVALUE_12115;
    }
    else {
        _25097 = unary_op(UMINUS, _25NOVALUE_12115);
    }
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 49);
    _1 = *(int *)_2;
    *(int *)_2 = _25097;
    if( _1 != _25097 ){
        DeRef(_1);
    }
    _25097 = NOVALUE;

    /** 		new[S_ARG_SEQ_LEN] = NOVALUE*/
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 51);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);

    /** 		new[S_ARG_SEQ_LEN_NEW] = -NOVALUE*/
    if (IS_ATOM_INT(_25NOVALUE_12115)) {
        if ((unsigned long)_25NOVALUE_12115 == 0xC0000000)
        _25098 = (int)NewDouble((double)-0xC0000000);
        else
        _25098 = - _25NOVALUE_12115;
    }
    else {
        _25098 = unary_op(UMINUS, _25NOVALUE_12115);
    }
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 52);
    _1 = *(int *)_2;
    *(int *)_2 = _25098;
    if( _1 != _25098 ){
        DeRef(_1);
    }
    _25098 = NOVALUE;

    /** 		new[S_SEQ_LEN] = NOVALUE*/
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);

    /** 		new[S_SEQ_LEN_NEW] = -NOVALUE -- no idea yet*/
    if (IS_ATOM_INT(_25NOVALUE_12115)) {
        if ((unsigned long)_25NOVALUE_12115 == 0xC0000000)
        _25099 = (int)NewDouble((double)-0xC0000000);
        else
        _25099 = - _25NOVALUE_12115;
    }
    else {
        _25099 = unary_op(UMINUS, _25NOVALUE_12115);
    }
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 39);
    _1 = *(int *)_2;
    *(int *)_2 = _25099;
    if( _1 != _25099 ){
        DeRef(_1);
    }
    _25099 = NOVALUE;

    /** 		new[S_NREFS] = 0*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		new[S_ONE_REF] = TRUE          -- assume TRUE until we find otherwise*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 35);
    _1 = *(int *)_2;
    *(int *)_2 = _5TRUE_244;
    DeRef(_1);

    /** 		new[S_RI_TARGET] = 0*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 53);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		new[S_OBJ_MIN] = MININT*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 30);
    _1 = *(int *)_2;
    *(int *)_2 = -1073741824;
    DeRef(_1);

    /** 		new[S_OBJ_MIN_NEW] = -NOVALUE -- no idea yet*/
    if (IS_ATOM_INT(_25NOVALUE_12115)) {
        if ((unsigned long)_25NOVALUE_12115 == 0xC0000000)
        _25100 = (int)NewDouble((double)-0xC0000000);
        else
        _25100 = - _25NOVALUE_12115;
    }
    else {
        _25100 = unary_op(UMINUS, _25NOVALUE_12115);
    }
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 41);
    _1 = *(int *)_2;
    *(int *)_2 = _25100;
    if( _1 != _25100 ){
        DeRef(_1);
    }
    _25100 = NOVALUE;

    /** 		new[S_OBJ_MAX] = MAXINT*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 31);
    _1 = *(int *)_2;
    *(int *)_2 = 1073741823;
    DeRef(_1);

    /** 		new[S_OBJ_MAX_NEW] = -NOVALUE -- missing from C code? (not needed)*/
    if (IS_ATOM_INT(_25NOVALUE_12115)) {
        if ((unsigned long)_25NOVALUE_12115 == 0xC0000000)
        _25101 = (int)NewDouble((double)-0xC0000000);
        else
        _25101 = - _25NOVALUE_12115;
    }
    else {
        _25101 = unary_op(UMINUS, _25NOVALUE_12115);
    }
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 42);
    _1 = *(int *)_2;
    *(int *)_2 = _25101;
    if( _1 != _25101 ){
        DeRef(_1);
    }
    _25101 = NOVALUE;
L3: 

    /** 	new[S_TOKEN] = token*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_TOKEN_11918))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    _1 = *(int *)_2;
    *(int *)_2 = _token_47185;
    DeRef(_1);

    /** 	new[S_VARNUM] = varnum*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 16);
    _1 = *(int *)_2;
    *(int *)_2 = _varnum_47183;
    DeRef(_1);

    /** 	new[S_INITLEVEL] = -1*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 14);
    _1 = *(int *)_2;
    *(int *)_2 = -1;
    DeRef(_1);

    /** 	new[S_VTYPE] = type_sym*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 15);
    _1 = *(int *)_2;
    *(int *)_2 = _type_sym_47190;
    DeRef(_1);

    /** 	new[S_HASHVAL] = hashval*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 11);
    _1 = *(int *)_2;
    *(int *)_2 = _hashval_47186;
    DeRef(_1);

    /** 	new[S_SAMEHASH] = samehash*/
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 9);
    _1 = *(int *)_2;
    *(int *)_2 = _samehash_47188;
    DeRef(_1);

    /** 	new[S_OBJ] = NOVALUE -- important*/
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(_new_47191);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47191 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);

    /** 	SymTab = append(SymTab, new)*/
    RefDS(_new_47191);
    Append(&_26SymTab_11138, _26SymTab_11138, _new_47191);

    /** 	return length(SymTab)*/
    if (IS_SEQUENCE(_26SymTab_11138)){
            _25103 = SEQ_PTR(_26SymTab_11138)->length;
    }
    else {
        _25103 = 1;
    }
    DeRefDS(_name_47182);
    DeRefDS(_new_47191);
    return _25103;
    ;
}


int _52NewEntry(int _name_47270, int _varnum_47271, int _scope_47272, int _token_47273, int _hashval_47274, int _samehash_47276, int _type_sym_47278)
{
    int _new_47280 = NOVALUE;
    int _25105 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_varnum_47271)) {
        _1 = (long)(DBL_PTR(_varnum_47271)->dbl);
        if (UNIQUE(DBL_PTR(_varnum_47271)) && (DBL_PTR(_varnum_47271)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_varnum_47271);
        _varnum_47271 = _1;
    }
    if (!IS_ATOM_INT(_scope_47272)) {
        _1 = (long)(DBL_PTR(_scope_47272)->dbl);
        if (UNIQUE(DBL_PTR(_scope_47272)) && (DBL_PTR(_scope_47272)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_scope_47272);
        _scope_47272 = _1;
    }
    if (!IS_ATOM_INT(_token_47273)) {
        _1 = (long)(DBL_PTR(_token_47273)->dbl);
        if (UNIQUE(DBL_PTR(_token_47273)) && (DBL_PTR(_token_47273)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_token_47273);
        _token_47273 = _1;
    }
    if (!IS_ATOM_INT(_hashval_47274)) {
        _1 = (long)(DBL_PTR(_hashval_47274)->dbl);
        if (UNIQUE(DBL_PTR(_hashval_47274)) && (DBL_PTR(_hashval_47274)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_hashval_47274);
        _hashval_47274 = _1;
    }
    if (!IS_ATOM_INT(_samehash_47276)) {
        _1 = (long)(DBL_PTR(_samehash_47276)->dbl);
        if (UNIQUE(DBL_PTR(_samehash_47276)) && (DBL_PTR(_samehash_47276)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_samehash_47276);
        _samehash_47276 = _1;
    }
    if (!IS_ATOM_INT(_type_sym_47278)) {
        _1 = (long)(DBL_PTR(_type_sym_47278)->dbl);
        if (UNIQUE(DBL_PTR(_type_sym_47278)) && (DBL_PTR(_type_sym_47278)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_type_sym_47278);
        _type_sym_47278 = _1;
    }

    /** 	symtab_index new = NewBasicEntry( name, varnum, scope, token, hashval, samehash, type_sym )*/
    RefDS(_name_47270);
    _new_47280 = _52NewBasicEntry(_name_47270, _varnum_47271, _scope_47272, _token_47273, _hashval_47274, _samehash_47276, _type_sym_47278);
    if (!IS_ATOM_INT(_new_47280)) {
        _1 = (long)(DBL_PTR(_new_47280)->dbl);
        if (UNIQUE(DBL_PTR(_new_47280)) && (DBL_PTR(_new_47280)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_new_47280);
        _new_47280 = _1;
    }

    /** 	if last_sym then*/
    if (_52last_sym_47110 == 0)
    {
        goto L1; // [33] 54
    }
    else{
    }

    /** 		SymTab[last_sym][S_NEXT] = new*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_52last_sym_47110 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _new_47280;
    DeRef(_1);
    _25105 = NOVALUE;
L1: 

    /** 	last_sym = new*/
    _52last_sym_47110 = _new_47280;

    /** 	if type_sym < 0 then*/
    if (_type_sym_47278 >= 0)
    goto L2; // [63] 76

    /** 		register_forward_type( last_sym, type_sym )*/
    _29register_forward_type(_52last_sym_47110, _type_sym_47278);
L2: 

    /** 	return last_sym*/
    DeRefDS(_name_47270);
    return _52last_sym_47110;
    ;
}


int _52tmp_alloc()
{
    int _new_entry_47295 = NOVALUE;
    int _25119 = NOVALUE;
    int _25117 = NOVALUE;
    int _25114 = NOVALUE;
    int _25111 = NOVALUE;
    int _25110 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence new_entry = repeat( 0, SIZEOF_TEMP_ENTRY )*/
    DeRef(_new_entry_47295);
    _new_entry_47295 = Repeat(0, _25SIZEOF_TEMP_ENTRY_12047);

    /** 	new_entry[S_USAGE] = T_UNKNOWN*/
    _2 = (int)SEQ_PTR(_new_entry_47295);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_entry_47295 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    *(int *)_2 = 4;

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L1; // [23] 132
    }
    else{
    }

    /** 		new_entry[S_GTYPE] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_new_entry_47295);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_entry_47295 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    *(int *)_2 = 16;

    /** 		new_entry[S_OBJ_MIN] = MININT*/
    _2 = (int)SEQ_PTR(_new_entry_47295);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_entry_47295 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 30);
    *(int *)_2 = -1073741824;

    /** 		new_entry[S_OBJ_MAX] = MAXINT*/
    _2 = (int)SEQ_PTR(_new_entry_47295);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_entry_47295 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 31);
    *(int *)_2 = 1073741823;

    /** 		new_entry[S_SEQ_LEN] = NOVALUE*/
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(_new_entry_47295);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_entry_47295 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    *(int *)_2 = _25NOVALUE_12115;

    /** 		new_entry[S_SEQ_ELEM] = TYPE_OBJECT  -- other fields set later*/
    _2 = (int)SEQ_PTR(_new_entry_47295);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_entry_47295 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);

    /** 		if length(temp_name_type)+1 = 8087 then*/
    if (IS_SEQUENCE(_25temp_name_type_12348)){
            _25110 = SEQ_PTR(_25temp_name_type_12348)->length;
    }
    else {
        _25110 = 1;
    }
    _25111 = _25110 + 1;
    _25110 = NOVALUE;
    if (_25111 != 8087)
    goto L2; // [87] 106

    /** 			temp_name_type = append(temp_name_type, {0, 0})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 0;
    _25114 = MAKE_SEQ(_1);
    RefDS(_25114);
    Append(&_25temp_name_type_12348, _25temp_name_type_12348, _25114);
    DeRefDS(_25114);
    _25114 = NOVALUE;
L2: 

    /** 		temp_name_type = append(temp_name_type, TYPES_OBNL)*/
    RefDS(_53TYPES_OBNL_46485);
    Append(&_25temp_name_type_12348, _25temp_name_type_12348, _53TYPES_OBNL_46485);

    /** 		new_entry[S_TEMP_NAME] = length(temp_name_type)*/
    if (IS_SEQUENCE(_25temp_name_type_12348)){
            _25117 = SEQ_PTR(_25temp_name_type_12348)->length;
    }
    else {
        _25117 = 1;
    }
    _2 = (int)SEQ_PTR(_new_entry_47295);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_entry_47295 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 34);
    _1 = *(int *)_2;
    *(int *)_2 = _25117;
    if( _1 != _25117 ){
        DeRef(_1);
    }
    _25117 = NOVALUE;
L1: 

    /** 	SymTab = append(SymTab, new_entry )*/
    RefDS(_new_entry_47295);
    Append(&_26SymTab_11138, _26SymTab_11138, _new_entry_47295);

    /** 	return length( SymTab )*/
    if (IS_SEQUENCE(_26SymTab_11138)){
            _25119 = SEQ_PTR(_26SymTab_11138)->length;
    }
    else {
        _25119 = 1;
    }
    DeRefDS(_new_entry_47295);
    DeRef(_25111);
    _25111 = NOVALUE;
    return _25119;
    ;
}


void _52DefinedYet(int _sym_47364)
{
    int _25139 = NOVALUE;
    int _25138 = NOVALUE;
    int _25137 = NOVALUE;
    int _25135 = NOVALUE;
    int _25134 = NOVALUE;
    int _25132 = NOVALUE;
    int _25131 = NOVALUE;
    int _25130 = NOVALUE;
    int _25129 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_47364)) {
        _1 = (long)(DBL_PTR(_sym_47364)->dbl);
        if (UNIQUE(DBL_PTR(_sym_47364)) && (DBL_PTR(_sym_47364)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_47364);
        _sym_47364 = _1;
    }

    /** 	if not find(SymTab[sym][S_SCOPE],*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25129 = (int)*(((s1_ptr)_2)->base + _sym_47364);
    _2 = (int)SEQ_PTR(_25129);
    _25130 = (int)*(((s1_ptr)_2)->base + 4);
    _25129 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 9;
    *((int *)(_2+8)) = 10;
    *((int *)(_2+12)) = 7;
    _25131 = MAKE_SEQ(_1);
    _25132 = find_from(_25130, _25131, 1);
    _25130 = NOVALUE;
    DeRefDS(_25131);
    _25131 = NOVALUE;
    if (_25132 != 0)
    goto L1; // [34] 82
    _25132 = NOVALUE;

    /** 		if SymTab[sym][S_FILE_NO] = current_file_no then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25134 = (int)*(((s1_ptr)_2)->base + _sym_47364);
    _2 = (int)SEQ_PTR(_25134);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _25135 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _25135 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _25134 = NOVALUE;
    if (binary_op_a(NOTEQ, _25135, _25current_file_no_12262)){
        _25135 = NOVALUE;
        goto L2; // [53] 81
    }
    _25135 = NOVALUE;

    /** 			CompileErr(31, {SymTab[sym][S_NAME]})*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25137 = (int)*(((s1_ptr)_2)->base + _sym_47364);
    _2 = (int)SEQ_PTR(_25137);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _25138 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _25138 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _25137 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_25138);
    *((int *)(_2+4)) = _25138;
    _25139 = MAKE_SEQ(_1);
    _25138 = NOVALUE;
    _43CompileErr(31, _25139, 0);
    _25139 = NOVALUE;
L2: 
L1: 

    /** end procedure*/
    return;
    ;
}


int _52name_ext(int _s_47391)
{
    int _25146 = NOVALUE;
    int _25145 = NOVALUE;
    int _25144 = NOVALUE;
    int _25143 = NOVALUE;
    int _25141 = NOVALUE;
    int _25140 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = length(s) to 1 by -1 do*/
    if (IS_SEQUENCE(_s_47391)){
            _25140 = SEQ_PTR(_s_47391)->length;
    }
    else {
        _25140 = 1;
    }
    {
        int _i_47393;
        _i_47393 = _25140;
L1: 
        if (_i_47393 < 1){
            goto L2; // [8] 55
        }

        /** 		if find(s[i], "/\\:") then*/
        _2 = (int)SEQ_PTR(_s_47391);
        _25141 = (int)*(((s1_ptr)_2)->base + _i_47393);
        _25143 = find_from(_25141, _25142, 1);
        _25141 = NOVALUE;
        if (_25143 == 0)
        {
            _25143 = NOVALUE;
            goto L3; // [26] 48
        }
        else{
            _25143 = NOVALUE;
        }

        /** 			return s[i+1 .. $]*/
        _25144 = _i_47393 + 1;
        if (IS_SEQUENCE(_s_47391)){
                _25145 = SEQ_PTR(_s_47391)->length;
        }
        else {
            _25145 = 1;
        }
        rhs_slice_target = (object_ptr)&_25146;
        RHS_Slice(_s_47391, _25144, _25145);
        DeRefDS(_s_47391);
        _25144 = NOVALUE;
        return _25146;
L3: 

        /** 	end for*/
        _i_47393 = _i_47393 + -1;
        goto L1; // [50] 15
L2: 
        ;
    }

    /** 	return s*/
    DeRef(_25144);
    _25144 = NOVALUE;
    DeRef(_25146);
    _25146 = NOVALUE;
    return _s_47391;
    ;
}


int _52NewStringSym(int _s_47410)
{
    int _p_47412 = NOVALUE;
    int _tp_47413 = NOVALUE;
    int _prev_47414 = NOVALUE;
    int _search_count_47415 = NOVALUE;
    int _25190 = NOVALUE;
    int _25188 = NOVALUE;
    int _25187 = NOVALUE;
    int _25186 = NOVALUE;
    int _25184 = NOVALUE;
    int _25183 = NOVALUE;
    int _25180 = NOVALUE;
    int _25178 = NOVALUE;
    int _25176 = NOVALUE;
    int _25175 = NOVALUE;
    int _25174 = NOVALUE;
    int _25172 = NOVALUE;
    int _25170 = NOVALUE;
    int _25168 = NOVALUE;
    int _25166 = NOVALUE;
    int _25163 = NOVALUE;
    int _25161 = NOVALUE;
    int _25160 = NOVALUE;
    int _25159 = NOVALUE;
    int _25157 = NOVALUE;
    int _25155 = NOVALUE;
    int _25154 = NOVALUE;
    int _25153 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	integer search_count*/

    /** 	tp = literal_init*/
    _tp_47413 = _52literal_init_47109;

    /** 	prev = 0*/
    _prev_47414 = 0;

    /** 	search_count = 0*/
    _search_count_47415 = 0;

    /** 	while tp != 0 do*/
L1: 
    if (_tp_47413 == 0)
    goto L2; // [31] 170

    /** 		search_count += 1*/
    _search_count_47415 = _search_count_47415 + 1;

    /** 		if search_count > SEARCH_LIMIT then  -- avoid n-squared algorithm*/
    if (binary_op_a(LESSEQ, _search_count_47415, _52SEARCH_LIMIT_47402)){
        goto L3; // [45] 54
    }

    /** 			exit*/
    goto L2; // [51] 170
L3: 

    /** 		if equal(s, SymTab[tp][S_OBJ]) then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25153 = (int)*(((s1_ptr)_2)->base + _tp_47413);
    _2 = (int)SEQ_PTR(_25153);
    _25154 = (int)*(((s1_ptr)_2)->base + 1);
    _25153 = NOVALUE;
    if (_s_47410 == _25154)
    _25155 = 1;
    else if (IS_ATOM_INT(_s_47410) && IS_ATOM_INT(_25154))
    _25155 = 0;
    else
    _25155 = (compare(_s_47410, _25154) == 0);
    _25154 = NOVALUE;
    if (_25155 == 0)
    {
        _25155 = NOVALUE;
        goto L4; // [72] 142
    }
    else{
        _25155 = NOVALUE;
    }

    /** 			if tp != literal_init then*/
    if (_tp_47413 == _52literal_init_47109)
    goto L5; // [79] 135

    /** 				SymTab[prev][S_NEXT] = SymTab[tp][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_prev_47414 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25159 = (int)*(((s1_ptr)_2)->base + _tp_47413);
    _2 = (int)SEQ_PTR(_25159);
    _25160 = (int)*(((s1_ptr)_2)->base + 2);
    _25159 = NOVALUE;
    Ref(_25160);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _25160;
    if( _1 != _25160 ){
        DeRef(_1);
    }
    _25160 = NOVALUE;
    _25157 = NOVALUE;

    /** 				SymTab[tp][S_NEXT] = literal_init*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_tp_47413 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _52literal_init_47109;
    DeRef(_1);
    _25161 = NOVALUE;

    /** 				literal_init = tp*/
    _52literal_init_47109 = _tp_47413;
L5: 

    /** 			return tp*/
    DeRefDS(_s_47410);
    return _tp_47413;
L4: 

    /** 		prev = tp*/
    _prev_47414 = _tp_47413;

    /** 		tp = SymTab[tp][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25163 = (int)*(((s1_ptr)_2)->base + _tp_47413);
    _2 = (int)SEQ_PTR(_25163);
    _tp_47413 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_tp_47413)){
        _tp_47413 = (long)DBL_PTR(_tp_47413)->dbl;
    }
    _25163 = NOVALUE;

    /** 	end while*/
    goto L1; // [167] 31
L2: 

    /** 	p = tmp_alloc()*/
    _p_47412 = _52tmp_alloc();
    if (!IS_ATOM_INT(_p_47412)) {
        _1 = (long)(DBL_PTR(_p_47412)->dbl);
        if (UNIQUE(DBL_PTR(_p_47412)) && (DBL_PTR(_p_47412)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_p_47412);
        _p_47412 = _1;
    }

    /** 	SymTab[p][S_OBJ] = s*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47412 + ((s1_ptr)_2)->base);
    RefDS(_s_47410);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _s_47410;
    DeRef(_1);
    _25166 = NOVALUE;

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L6; // [196] 346
    }
    else{
    }

    /** 		SymTab[p][S_MODE] = M_TEMP    -- override CONSTANT for compile*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47412 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 3;
    DeRef(_1);
    _25168 = NOVALUE;

    /** 		SymTab[p][S_GTYPE] = TYPE_SEQUENCE*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47412 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = 8;
    DeRef(_1);
    _25170 = NOVALUE;

    /** 		SymTab[p][S_SEQ_LEN] = length(s)*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47412 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_s_47410)){
            _25174 = SEQ_PTR(_s_47410)->length;
    }
    else {
        _25174 = 1;
    }
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _25174;
    if( _1 != _25174 ){
        DeRef(_1);
    }
    _25174 = NOVALUE;
    _25172 = NOVALUE;

    /** 		if SymTab[p][S_SEQ_LEN] > 0 then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25175 = (int)*(((s1_ptr)_2)->base + _p_47412);
    _2 = (int)SEQ_PTR(_25175);
    _25176 = (int)*(((s1_ptr)_2)->base + 32);
    _25175 = NOVALUE;
    if (binary_op_a(LESSEQ, _25176, 0)){
        _25176 = NOVALUE;
        goto L7; // [265] 289
    }
    _25176 = NOVALUE;

    /** 			SymTab[p][S_SEQ_ELEM] = TYPE_INTEGER*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47412 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _25178 = NOVALUE;
    goto L8; // [286] 307
L7: 

    /** 			SymTab[p][S_SEQ_ELEM] = TYPE_NULL*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47412 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _25180 = NOVALUE;
L8: 

    /** 		c_printf("int _%d;\n", SymTab[p][S_TEMP_NAME])*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25183 = (int)*(((s1_ptr)_2)->base + _p_47412);
    _2 = (int)SEQ_PTR(_25183);
    _25184 = (int)*(((s1_ptr)_2)->base + 34);
    _25183 = NOVALUE;
    RefDS(_25182);
    Ref(_25184);
    _53c_printf(_25182, _25184);
    _25184 = NOVALUE;

    /** 		c_hprintf("extern int _%d;\n", SymTab[p][S_TEMP_NAME])*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25186 = (int)*(((s1_ptr)_2)->base + _p_47412);
    _2 = (int)SEQ_PTR(_25186);
    _25187 = (int)*(((s1_ptr)_2)->base + 34);
    _25186 = NOVALUE;
    RefDS(_25185);
    Ref(_25187);
    _53c_hprintf(_25185, _25187);
    _25187 = NOVALUE;
    goto L9; // [343] 364
L6: 

    /** 		SymTab[p][S_MODE] = M_CONSTANT*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47412 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _25188 = NOVALUE;
L9: 

    /** 	SymTab[p][S_NEXT] = literal_init*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47412 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _52literal_init_47109;
    DeRef(_1);
    _25190 = NOVALUE;

    /** 	literal_init = p*/
    _52literal_init_47109 = _p_47412;

    /** 	return p*/
    DeRefDS(_s_47410);
    return _p_47412;
    ;
}


int _52NewIntSym(int _int_val_47508)
{
    int _p_47510 = NOVALUE;
    int _x_47511 = NOVALUE;
    int _25209 = NOVALUE;
    int _25207 = NOVALUE;
    int _25203 = NOVALUE;
    int _25201 = NOVALUE;
    int _25199 = NOVALUE;
    int _25197 = NOVALUE;
    int _25195 = NOVALUE;
    int _25193 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_int_val_47508)) {
        _1 = (long)(DBL_PTR(_int_val_47508)->dbl);
        if (UNIQUE(DBL_PTR(_int_val_47508)) && (DBL_PTR(_int_val_47508)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_int_val_47508);
        _int_val_47508 = _1;
    }

    /** 	integer x*/

    /** 	x = find(int_val, lastintval)*/
    _x_47511 = find_from(_int_val_47508, _52lastintval_47111, 1);

    /** 	if x then*/
    if (_x_47511 == 0)
    {
        goto L1; // [16] 34
    }
    else{
    }

    /** 		return lastintsym[x]  -- saves space, helps Translator reduce code size*/
    _2 = (int)SEQ_PTR(_52lastintsym_47112);
    _25193 = (int)*(((s1_ptr)_2)->base + _x_47511);
    return _25193;
    goto L2; // [31] 180
L1: 

    /** 		p = tmp_alloc()*/
    _p_47510 = _52tmp_alloc();
    if (!IS_ATOM_INT(_p_47510)) {
        _1 = (long)(DBL_PTR(_p_47510)->dbl);
        if (UNIQUE(DBL_PTR(_p_47510)) && (DBL_PTR(_p_47510)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_p_47510);
        _p_47510 = _1;
    }

    /** 		SymTab[p][S_MODE] = M_CONSTANT*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47510 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _25195 = NOVALUE;

    /** 		SymTab[p][S_OBJ] = int_val*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47510 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _int_val_47508;
    DeRef(_1);
    _25197 = NOVALUE;

    /** 		if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L3; // [77] 128
    }
    else{
    }

    /** 			SymTab[p][S_OBJ_MIN] = int_val*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47510 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 30);
    _1 = *(int *)_2;
    *(int *)_2 = _int_val_47508;
    DeRef(_1);
    _25199 = NOVALUE;

    /** 			SymTab[p][S_OBJ_MAX] = int_val*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47510 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 31);
    _1 = *(int *)_2;
    *(int *)_2 = _int_val_47508;
    DeRef(_1);
    _25201 = NOVALUE;

    /** 			SymTab[p][S_GTYPE] = TYPE_INTEGER*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47510 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _25203 = NOVALUE;
L3: 

    /** 		lastintval = prepend(lastintval, int_val)*/
    Prepend(&_52lastintval_47111, _52lastintval_47111, _int_val_47508);

    /** 		lastintsym = prepend(lastintsym, p)*/
    Prepend(&_52lastintsym_47112, _52lastintsym_47112, _p_47510);

    /** 		if length(lastintval) > SEARCH_LIMIT then*/
    if (IS_SEQUENCE(_52lastintval_47111)){
            _25207 = SEQ_PTR(_52lastintval_47111)->length;
    }
    else {
        _25207 = 1;
    }
    if (binary_op_a(LESSEQ, _25207, _52SEARCH_LIMIT_47402)){
        _25207 = NOVALUE;
        goto L4; // [153] 173
    }
    _25207 = NOVALUE;

    /** 			lastintval = lastintval[1..floor(SEARCH_LIMIT/2)]*/
    if (IS_ATOM_INT(_52SEARCH_LIMIT_47402)) {
        _25209 = _52SEARCH_LIMIT_47402 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _52SEARCH_LIMIT_47402, 2);
        _25209 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    rhs_slice_target = (object_ptr)&_52lastintval_47111;
    RHS_Slice(_52lastintval_47111, 1, _25209);
L4: 

    /** 		return p*/
    _25193 = NOVALUE;
    DeRef(_25209);
    _25209 = NOVALUE;
    return _p_47510;
L2: 
    ;
}


int _52NewDoubleSym(int _d_47550)
{
    int _p_47552 = NOVALUE;
    int _tp_47553 = NOVALUE;
    int _prev_47554 = NOVALUE;
    int _search_count_47555 = NOVALUE;
    int _25239 = NOVALUE;
    int _25238 = NOVALUE;
    int _25237 = NOVALUE;
    int _25236 = NOVALUE;
    int _25235 = NOVALUE;
    int _25233 = NOVALUE;
    int _25231 = NOVALUE;
    int _25229 = NOVALUE;
    int _25227 = NOVALUE;
    int _25224 = NOVALUE;
    int _25222 = NOVALUE;
    int _25221 = NOVALUE;
    int _25220 = NOVALUE;
    int _25218 = NOVALUE;
    int _25216 = NOVALUE;
    int _25215 = NOVALUE;
    int _25214 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	integer search_count*/

    /** 	tp = literal_init*/
    _tp_47553 = _52literal_init_47109;

    /** 	prev = 0*/
    _prev_47554 = 0;

    /** 	search_count = 0*/
    _search_count_47555 = 0;

    /** 	while tp != 0 do*/
L1: 
    if (_tp_47553 == 0)
    goto L2; // [29] 168

    /** 		search_count += 1*/
    _search_count_47555 = _search_count_47555 + 1;

    /** 		if search_count > SEARCH_LIMIT then  -- avoid n-squared algorithm*/
    if (binary_op_a(LESSEQ, _search_count_47555, _52SEARCH_LIMIT_47402)){
        goto L3; // [43] 52
    }

    /** 			exit*/
    goto L2; // [49] 168
L3: 

    /** 		if equal(d, SymTab[tp][S_OBJ]) then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25214 = (int)*(((s1_ptr)_2)->base + _tp_47553);
    _2 = (int)SEQ_PTR(_25214);
    _25215 = (int)*(((s1_ptr)_2)->base + 1);
    _25214 = NOVALUE;
    if (_d_47550 == _25215)
    _25216 = 1;
    else if (IS_ATOM_INT(_d_47550) && IS_ATOM_INT(_25215))
    _25216 = 0;
    else
    _25216 = (compare(_d_47550, _25215) == 0);
    _25215 = NOVALUE;
    if (_25216 == 0)
    {
        _25216 = NOVALUE;
        goto L4; // [70] 140
    }
    else{
        _25216 = NOVALUE;
    }

    /** 			if tp != literal_init then*/
    if (_tp_47553 == _52literal_init_47109)
    goto L5; // [77] 133

    /** 				SymTab[prev][S_NEXT] = SymTab[tp][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_prev_47554 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25220 = (int)*(((s1_ptr)_2)->base + _tp_47553);
    _2 = (int)SEQ_PTR(_25220);
    _25221 = (int)*(((s1_ptr)_2)->base + 2);
    _25220 = NOVALUE;
    Ref(_25221);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _25221;
    if( _1 != _25221 ){
        DeRef(_1);
    }
    _25221 = NOVALUE;
    _25218 = NOVALUE;

    /** 				SymTab[tp][S_NEXT] = literal_init*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_tp_47553 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _52literal_init_47109;
    DeRef(_1);
    _25222 = NOVALUE;

    /** 				literal_init = tp*/
    _52literal_init_47109 = _tp_47553;
L5: 

    /** 			return tp*/
    DeRef(_d_47550);
    return _tp_47553;
L4: 

    /** 		prev = tp*/
    _prev_47554 = _tp_47553;

    /** 		tp = SymTab[tp][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25224 = (int)*(((s1_ptr)_2)->base + _tp_47553);
    _2 = (int)SEQ_PTR(_25224);
    _tp_47553 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_tp_47553)){
        _tp_47553 = (long)DBL_PTR(_tp_47553)->dbl;
    }
    _25224 = NOVALUE;

    /** 	end while*/
    goto L1; // [165] 29
L2: 

    /** 	p = tmp_alloc()*/
    _p_47552 = _52tmp_alloc();
    if (!IS_ATOM_INT(_p_47552)) {
        _1 = (long)(DBL_PTR(_p_47552)->dbl);
        if (UNIQUE(DBL_PTR(_p_47552)) && (DBL_PTR(_p_47552)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_p_47552);
        _p_47552 = _1;
    }

    /** 	SymTab[p][S_MODE] = M_CONSTANT*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47552 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _25227 = NOVALUE;

    /** 	SymTab[p][S_OBJ] = d*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47552 + ((s1_ptr)_2)->base);
    Ref(_d_47550);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _d_47550;
    DeRef(_1);
    _25229 = NOVALUE;

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L6; // [211] 285
    }
    else{
    }

    /** 		SymTab[p][S_MODE] = M_TEMP  -- override CONSTANT for compile*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47552 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 3;
    DeRef(_1);
    _25231 = NOVALUE;

    /** 		SymTab[p][S_GTYPE] = TYPE_DOUBLE*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47552 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _25233 = NOVALUE;

    /** 		c_printf("int _%d;\n", SymTab[p][S_TEMP_NAME])*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25235 = (int)*(((s1_ptr)_2)->base + _p_47552);
    _2 = (int)SEQ_PTR(_25235);
    _25236 = (int)*(((s1_ptr)_2)->base + 34);
    _25235 = NOVALUE;
    RefDS(_25182);
    Ref(_25236);
    _53c_printf(_25182, _25236);
    _25236 = NOVALUE;

    /** 		c_hprintf("extern int _%d;\n", SymTab[p][S_TEMP_NAME])*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25237 = (int)*(((s1_ptr)_2)->base + _p_47552);
    _2 = (int)SEQ_PTR(_25237);
    _25238 = (int)*(((s1_ptr)_2)->base + 34);
    _25237 = NOVALUE;
    RefDS(_25185);
    Ref(_25238);
    _53c_hprintf(_25185, _25238);
    _25238 = NOVALUE;
L6: 

    /** 	SymTab[p][S_NEXT] = literal_init*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47552 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _52literal_init_47109;
    DeRef(_1);
    _25239 = NOVALUE;

    /** 	literal_init = p*/
    _52literal_init_47109 = _p_47552;

    /** 	return p*/
    DeRef(_d_47550);
    return _p_47552;
    ;
}


int _52NewTempSym(int _inlining_47624)
{
    int _p_47626 = NOVALUE;
    int _q_47627 = NOVALUE;
    int _25288 = NOVALUE;
    int _25286 = NOVALUE;
    int _25284 = NOVALUE;
    int _25282 = NOVALUE;
    int _25280 = NOVALUE;
    int _25278 = NOVALUE;
    int _25277 = NOVALUE;
    int _25276 = NOVALUE;
    int _25274 = NOVALUE;
    int _25273 = NOVALUE;
    int _25272 = NOVALUE;
    int _25270 = NOVALUE;
    int _25268 = NOVALUE;
    int _25265 = NOVALUE;
    int _25264 = NOVALUE;
    int _25263 = NOVALUE;
    int _25261 = NOVALUE;
    int _25259 = NOVALUE;
    int _25258 = NOVALUE;
    int _25257 = NOVALUE;
    int _25255 = NOVALUE;
    int _25253 = NOVALUE;
    int _25248 = NOVALUE;
    int _25247 = NOVALUE;
    int _25246 = NOVALUE;
    int _25245 = NOVALUE;
    int _25244 = NOVALUE;
    int _25243 = NOVALUE;
    int _25241 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_inlining_47624)) {
        _1 = (long)(DBL_PTR(_inlining_47624)->dbl);
        if (UNIQUE(DBL_PTR(_inlining_47624)) && (DBL_PTR(_inlining_47624)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_inlining_47624);
        _inlining_47624 = _1;
    }

    /** 	if inlining then*/
    if (_inlining_47624 == 0)
    {
        goto L1; // [5] 85
    }
    else{
    }

    /** 		p = SymTab[CurrentSub][S_TEMPS]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25241 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_25241);
    if (!IS_ATOM_INT(_25S_TEMPS_11958)){
        _p_47626 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TEMPS_11958)->dbl));
    }
    else{
        _p_47626 = (int)*(((s1_ptr)_2)->base + _25S_TEMPS_11958);
    }
    if (!IS_ATOM_INT(_p_47626)){
        _p_47626 = (long)DBL_PTR(_p_47626)->dbl;
    }
    _25241 = NOVALUE;

    /** 		while p != 0 and SymTab[p][S_SCOPE] != FREE do*/
L2: 
    _25243 = (_p_47626 != 0);
    if (_25243 == 0) {
        goto L3; // [35] 93
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25245 = (int)*(((s1_ptr)_2)->base + _p_47626);
    _2 = (int)SEQ_PTR(_25245);
    _25246 = (int)*(((s1_ptr)_2)->base + 4);
    _25245 = NOVALUE;
    if (IS_ATOM_INT(_25246)) {
        _25247 = (_25246 != 0);
    }
    else {
        _25247 = binary_op(NOTEQ, _25246, 0);
    }
    _25246 = NOVALUE;
    if (_25247 <= 0) {
        if (_25247 == 0) {
            DeRef(_25247);
            _25247 = NOVALUE;
            goto L3; // [58] 93
        }
        else {
            if (!IS_ATOM_INT(_25247) && DBL_PTR(_25247)->dbl == 0.0){
                DeRef(_25247);
                _25247 = NOVALUE;
                goto L3; // [58] 93
            }
            DeRef(_25247);
            _25247 = NOVALUE;
        }
    }
    DeRef(_25247);
    _25247 = NOVALUE;

    /** 			p = SymTab[p][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25248 = (int)*(((s1_ptr)_2)->base + _p_47626);
    _2 = (int)SEQ_PTR(_25248);
    _p_47626 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_p_47626)){
        _p_47626 = (long)DBL_PTR(_p_47626)->dbl;
    }
    _25248 = NOVALUE;

    /** 		end while*/
    goto L2; // [79] 31
    goto L3; // [82] 93
L1: 

    /** 		p = 0*/
    _p_47626 = 0;
L3: 

    /** 	if p = 0 then*/
    if (_p_47626 != 0)
    goto L4; // [97] 213

    /** 		temps_allocated += 1*/
    _52temps_allocated_47621 = _52temps_allocated_47621 + 1;

    /** 		p = tmp_alloc()*/
    _p_47626 = _52tmp_alloc();
    if (!IS_ATOM_INT(_p_47626)) {
        _1 = (long)(DBL_PTR(_p_47626)->dbl);
        if (UNIQUE(DBL_PTR(_p_47626)) && (DBL_PTR(_p_47626)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_p_47626);
        _p_47626 = _1;
    }

    /** 		SymTab[p][S_MODE] = M_TEMP*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47626 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 3;
    DeRef(_1);
    _25253 = NOVALUE;

    /** 		SymTab[p][S_NEXT] = SymTab[CurrentSub][S_TEMPS]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47626 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25257 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_25257);
    if (!IS_ATOM_INT(_25S_TEMPS_11958)){
        _25258 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TEMPS_11958)->dbl));
    }
    else{
        _25258 = (int)*(((s1_ptr)_2)->base + _25S_TEMPS_11958);
    }
    _25257 = NOVALUE;
    Ref(_25258);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _25258;
    if( _1 != _25258 ){
        DeRef(_1);
    }
    _25258 = NOVALUE;
    _25255 = NOVALUE;

    /** 		SymTab[CurrentSub][S_TEMPS] = p*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25CurrentSub_12270 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_TEMPS_11958))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TEMPS_11958)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_TEMPS_11958);
    _1 = *(int *)_2;
    *(int *)_2 = _p_47626;
    DeRef(_1);
    _25259 = NOVALUE;

    /** 		if inlining then*/
    if (_inlining_47624 == 0)
    {
        goto L5; // [181] 343
    }
    else{
    }

    /** 			SymTab[CurrentSub][S_STACK_SPACE] += 1*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25CurrentSub_12270 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!IS_ATOM_INT(_25S_STACK_SPACE_11973)){
        _25263 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_STACK_SPACE_11973)->dbl));
    }
    else{
        _25263 = (int)*(((s1_ptr)_2)->base + _25S_STACK_SPACE_11973);
    }
    _25261 = NOVALUE;
    if (IS_ATOM_INT(_25263)) {
        _25264 = _25263 + 1;
        if (_25264 > MAXINT){
            _25264 = NewDouble((double)_25264);
        }
    }
    else
    _25264 = binary_op(PLUS, 1, _25263);
    _25263 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_STACK_SPACE_11973))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_STACK_SPACE_11973)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_STACK_SPACE_11973);
    _1 = *(int *)_2;
    *(int *)_2 = _25264;
    if( _1 != _25264 ){
        DeRef(_1);
    }
    _25264 = NOVALUE;
    _25261 = NOVALUE;
    goto L5; // [210] 343
L4: 

    /** 	elsif TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L6; // [217] 342
    }
    else{
    }

    /** 		SymTab[p][S_SCOPE] = DELETED*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47626 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _25265 = NOVALUE;

    /** 		q = tmp_alloc()*/
    _q_47627 = _52tmp_alloc();
    if (!IS_ATOM_INT(_q_47627)) {
        _1 = (long)(DBL_PTR(_q_47627)->dbl);
        if (UNIQUE(DBL_PTR(_q_47627)) && (DBL_PTR(_q_47627)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_q_47627);
        _q_47627 = _1;
    }

    /** 		SymTab[q][S_MODE] = M_TEMP*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_q_47627 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 3;
    DeRef(_1);
    _25268 = NOVALUE;

    /** 		SymTab[q][S_TEMP_NAME] = SymTab[p][S_TEMP_NAME]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_q_47627 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25272 = (int)*(((s1_ptr)_2)->base + _p_47626);
    _2 = (int)SEQ_PTR(_25272);
    _25273 = (int)*(((s1_ptr)_2)->base + 34);
    _25272 = NOVALUE;
    Ref(_25273);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 34);
    _1 = *(int *)_2;
    *(int *)_2 = _25273;
    if( _1 != _25273 ){
        DeRef(_1);
    }
    _25273 = NOVALUE;
    _25270 = NOVALUE;

    /** 		SymTab[q][S_NEXT] = SymTab[CurrentSub][S_TEMPS]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_q_47627 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25276 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_25276);
    if (!IS_ATOM_INT(_25S_TEMPS_11958)){
        _25277 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TEMPS_11958)->dbl));
    }
    else{
        _25277 = (int)*(((s1_ptr)_2)->base + _25S_TEMPS_11958);
    }
    _25276 = NOVALUE;
    Ref(_25277);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _25277;
    if( _1 != _25277 ){
        DeRef(_1);
    }
    _25277 = NOVALUE;
    _25274 = NOVALUE;

    /** 		SymTab[CurrentSub][S_TEMPS] = q*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25CurrentSub_12270 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_TEMPS_11958))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TEMPS_11958)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_TEMPS_11958);
    _1 = *(int *)_2;
    *(int *)_2 = _q_47627;
    DeRef(_1);
    _25278 = NOVALUE;

    /** 		p = q*/
    _p_47626 = _q_47627;
L6: 
L5: 

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L7; // [347] 385
    }
    else{
    }

    /** 		SymTab[p][S_GTYPE] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47626 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);
    _25280 = NOVALUE;

    /** 		SymTab[p][S_SEQ_ELEM] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47626 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);
    _25282 = NOVALUE;
L7: 

    /** 	SymTab[p][S_OBJ] = NOVALUE*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47626 + ((s1_ptr)_2)->base);
    Ref(_25NOVALUE_12115);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _25NOVALUE_12115;
    DeRef(_1);
    _25284 = NOVALUE;

    /** 	SymTab[p][S_USAGE] = T_UNKNOWN*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47626 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 4;
    DeRef(_1);
    _25286 = NOVALUE;

    /** 	SymTab[p][S_SCOPE] = IN_USE*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47626 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _25288 = NOVALUE;

    /** 	return p*/
    DeRef(_25243);
    _25243 = NOVALUE;
    return _p_47626;
    ;
}


void _52InitSymTab()
{
    int _hashval_47743 = NOVALUE;
    int _len_47744 = NOVALUE;
    int _s_47746 = NOVALUE;
    int _st_index_47747 = NOVALUE;
    int _kname_47748 = NOVALUE;
    int _fixups_47749 = NOVALUE;
    int _si_47889 = NOVALUE;
    int _sj_47890 = NOVALUE;
    int _25876 = NOVALUE;
    int _25875 = NOVALUE;
    int _25404 = NOVALUE;
    int _25403 = NOVALUE;
    int _25402 = NOVALUE;
    int _25401 = NOVALUE;
    int _25400 = NOVALUE;
    int _25398 = NOVALUE;
    int _25397 = NOVALUE;
    int _25396 = NOVALUE;
    int _25395 = NOVALUE;
    int _25393 = NOVALUE;
    int _25391 = NOVALUE;
    int _25389 = NOVALUE;
    int _25388 = NOVALUE;
    int _25386 = NOVALUE;
    int _25384 = NOVALUE;
    int _25382 = NOVALUE;
    int _25381 = NOVALUE;
    int _25379 = NOVALUE;
    int _25378 = NOVALUE;
    int _25377 = NOVALUE;
    int _25376 = NOVALUE;
    int _25375 = NOVALUE;
    int _25372 = NOVALUE;
    int _25371 = NOVALUE;
    int _25370 = NOVALUE;
    int _25368 = NOVALUE;
    int _25367 = NOVALUE;
    int _25366 = NOVALUE;
    int _25364 = NOVALUE;
    int _25363 = NOVALUE;
    int _25362 = NOVALUE;
    int _25359 = NOVALUE;
    int _25357 = NOVALUE;
    int _25355 = NOVALUE;
    int _25354 = NOVALUE;
    int _25351 = NOVALUE;
    int _25350 = NOVALUE;
    int _25348 = NOVALUE;
    int _25346 = NOVALUE;
    int _25344 = NOVALUE;
    int _25341 = NOVALUE;
    int _25340 = NOVALUE;
    int _25339 = NOVALUE;
    int _25336 = NOVALUE;
    int _25335 = NOVALUE;
    int _25333 = NOVALUE;
    int _25332 = NOVALUE;
    int _25330 = NOVALUE;
    int _25329 = NOVALUE;
    int _25328 = NOVALUE;
    int _25326 = NOVALUE;
    int _25324 = NOVALUE;
    int _25323 = NOVALUE;
    int _25321 = NOVALUE;
    int _25320 = NOVALUE;
    int _25319 = NOVALUE;
    int _25317 = NOVALUE;
    int _25316 = NOVALUE;
    int _25315 = NOVALUE;
    int _25313 = NOVALUE;
    int _25312 = NOVALUE;
    int _25311 = NOVALUE;
    int _25309 = NOVALUE;
    int _25308 = NOVALUE;
    int _25307 = NOVALUE;
    int _25306 = NOVALUE;
    int _25305 = NOVALUE;
    int _25304 = NOVALUE;
    int _25303 = NOVALUE;
    int _25302 = NOVALUE;
    int _25301 = NOVALUE;
    int _25300 = NOVALUE;
    int _25298 = NOVALUE;
    int _25297 = NOVALUE;
    int _25296 = NOVALUE;
    int _25295 = NOVALUE;
    int _25291 = NOVALUE;
    int _25290 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	sequence kname, fixups = {}*/
    RefDS(_22682);
    DeRefi(_fixups_47749);
    _fixups_47749 = _22682;

    /** 	for k = 1 to length(keylist) do*/
    if (IS_SEQUENCE(_63keylist_23599)){
            _25290 = SEQ_PTR(_63keylist_23599)->length;
    }
    else {
        _25290 = 1;
    }
    {
        int _k_47751;
        _k_47751 = 1;
L1: 
        if (_k_47751 > _25290){
            goto L2; // [15] 560
        }

        /** 		kname = keylist[k][K_NAME]*/
        _2 = (int)SEQ_PTR(_63keylist_23599);
        _25291 = (int)*(((s1_ptr)_2)->base + _k_47751);
        DeRef(_kname_47748);
        _2 = (int)SEQ_PTR(_25291);
        _kname_47748 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_kname_47748);
        _25291 = NOVALUE;

        /** 		len = length(kname)*/
        if (IS_SEQUENCE(_kname_47748)){
                _len_47744 = SEQ_PTR(_kname_47748)->length;
        }
        else {
            _len_47744 = 1;
        }

        /** 		hashval = hashfn(kname)*/
        RefDS(_kname_47748);
        _hashval_47743 = _52hashfn(_kname_47748);
        if (!IS_ATOM_INT(_hashval_47743)) {
            _1 = (long)(DBL_PTR(_hashval_47743)->dbl);
            if (UNIQUE(DBL_PTR(_hashval_47743)) && (DBL_PTR(_hashval_47743)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_hashval_47743);
            _hashval_47743 = _1;
        }

        /** 		st_index = NewEntry(kname,*/
        _2 = (int)SEQ_PTR(_63keylist_23599);
        _25295 = (int)*(((s1_ptr)_2)->base + _k_47751);
        _2 = (int)SEQ_PTR(_25295);
        _25296 = (int)*(((s1_ptr)_2)->base + 2);
        _25295 = NOVALUE;
        _2 = (int)SEQ_PTR(_63keylist_23599);
        _25297 = (int)*(((s1_ptr)_2)->base + _k_47751);
        _2 = (int)SEQ_PTR(_25297);
        _25298 = (int)*(((s1_ptr)_2)->base + 3);
        _25297 = NOVALUE;
        RefDS(_kname_47748);
        Ref(_25296);
        Ref(_25298);
        _st_index_47747 = _52NewEntry(_kname_47748, 0, _25296, _25298, _hashval_47743, 0, 0);
        _25296 = NOVALUE;
        _25298 = NOVALUE;
        if (!IS_ATOM_INT(_st_index_47747)) {
            _1 = (long)(DBL_PTR(_st_index_47747)->dbl);
            if (UNIQUE(DBL_PTR(_st_index_47747)) && (DBL_PTR(_st_index_47747)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_st_index_47747);
            _st_index_47747 = _1;
        }

        /** 		if find(keylist[k][K_TOKEN], RTN_TOKS) then*/
        _2 = (int)SEQ_PTR(_63keylist_23599);
        _25300 = (int)*(((s1_ptr)_2)->base + _k_47751);
        _2 = (int)SEQ_PTR(_25300);
        _25301 = (int)*(((s1_ptr)_2)->base + 3);
        _25300 = NOVALUE;
        _25302 = find_from(_25301, _28RTN_TOKS_11857, 1);
        _25301 = NOVALUE;
        if (_25302 == 0)
        {
            _25302 = NOVALUE;
            goto L3; // [110] 325
        }
        else{
            _25302 = NOVALUE;
        }

        /** 			SymTab[st_index] = SymTab[st_index] &*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _25303 = (int)*(((s1_ptr)_2)->base + _st_index_47747);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _25304 = (int)*(((s1_ptr)_2)->base + _st_index_47747);
        if (IS_SEQUENCE(_25304)){
                _25305 = SEQ_PTR(_25304)->length;
        }
        else {
            _25305 = 1;
        }
        _25304 = NOVALUE;
        _25306 = _25SIZEOF_ROUTINE_ENTRY_12038 - _25305;
        _25305 = NOVALUE;
        _25307 = Repeat(0, _25306);
        _25306 = NOVALUE;
        if (IS_SEQUENCE(_25303) && IS_ATOM(_25307)) {
        }
        else if (IS_ATOM(_25303) && IS_SEQUENCE(_25307)) {
            Ref(_25303);
            Prepend(&_25308, _25307, _25303);
        }
        else {
            Concat((object_ptr)&_25308, _25303, _25307);
            _25303 = NOVALUE;
        }
        _25303 = NOVALUE;
        DeRefDS(_25307);
        _25307 = NOVALUE;
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _st_index_47747);
        _1 = *(int *)_2;
        *(int *)_2 = _25308;
        if( _1 != _25308 ){
            DeRef(_1);
        }
        _25308 = NOVALUE;

        /** 			SymTab[st_index][S_NUM_ARGS] = keylist[k][K_NUM_ARGS]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_st_index_47747 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_63keylist_23599);
        _25311 = (int)*(((s1_ptr)_2)->base + _k_47751);
        _2 = (int)SEQ_PTR(_25311);
        _25312 = (int)*(((s1_ptr)_2)->base + 5);
        _25311 = NOVALUE;
        Ref(_25312);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_25S_NUM_ARGS_11964))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
        _1 = *(int *)_2;
        *(int *)_2 = _25312;
        if( _1 != _25312 ){
            DeRef(_1);
        }
        _25312 = NOVALUE;
        _25309 = NOVALUE;

        /** 			SymTab[st_index][S_OPCODE] = keylist[k][K_OPCODE]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_st_index_47747 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_63keylist_23599);
        _25315 = (int)*(((s1_ptr)_2)->base + _k_47751);
        _2 = (int)SEQ_PTR(_25315);
        _25316 = (int)*(((s1_ptr)_2)->base + 4);
        _25315 = NOVALUE;
        Ref(_25316);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 21);
        _1 = *(int *)_2;
        *(int *)_2 = _25316;
        if( _1 != _25316 ){
            DeRef(_1);
        }
        _25316 = NOVALUE;
        _25313 = NOVALUE;

        /** 			SymTab[st_index][S_EFFECT] = keylist[k][K_EFFECT]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_st_index_47747 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_63keylist_23599);
        _25319 = (int)*(((s1_ptr)_2)->base + _k_47751);
        _2 = (int)SEQ_PTR(_25319);
        _25320 = (int)*(((s1_ptr)_2)->base + 6);
        _25319 = NOVALUE;
        Ref(_25320);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 23);
        _1 = *(int *)_2;
        *(int *)_2 = _25320;
        if( _1 != _25320 ){
            DeRef(_1);
        }
        _25320 = NOVALUE;
        _25317 = NOVALUE;

        /** 			SymTab[st_index][S_REFLIST] = {}*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_st_index_47747 + ((s1_ptr)_2)->base);
        RefDS(_22682);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 24);
        _1 = *(int *)_2;
        *(int *)_2 = _22682;
        DeRef(_1);
        _25321 = NOVALUE;

        /** 			if length(keylist[k]) > K_EFFECT then*/
        _2 = (int)SEQ_PTR(_63keylist_23599);
        _25323 = (int)*(((s1_ptr)_2)->base + _k_47751);
        if (IS_SEQUENCE(_25323)){
                _25324 = SEQ_PTR(_25323)->length;
        }
        else {
            _25324 = 1;
        }
        _25323 = NOVALUE;
        if (_25324 <= 6)
        goto L4; // [259] 324

        /** 			    SymTab[st_index][S_CODE] = keylist[k][K_CODE]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_st_index_47747 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_63keylist_23599);
        _25328 = (int)*(((s1_ptr)_2)->base + _k_47751);
        _2 = (int)SEQ_PTR(_25328);
        _25329 = (int)*(((s1_ptr)_2)->base + 7);
        _25328 = NOVALUE;
        Ref(_25329);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_25S_CODE_11925))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _25S_CODE_11925);
        _1 = *(int *)_2;
        *(int *)_2 = _25329;
        if( _1 != _25329 ){
            DeRef(_1);
        }
        _25329 = NOVALUE;
        _25326 = NOVALUE;

        /** 			    SymTab[st_index][S_DEF_ARGS] = keylist[k][K_DEF_ARGS]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_st_index_47747 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_63keylist_23599);
        _25332 = (int)*(((s1_ptr)_2)->base + _k_47751);
        _2 = (int)SEQ_PTR(_25332);
        _25333 = (int)*(((s1_ptr)_2)->base + 8);
        _25332 = NOVALUE;
        Ref(_25333);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 28);
        _1 = *(int *)_2;
        *(int *)_2 = _25333;
        if( _1 != _25333 ){
            DeRef(_1);
        }
        _25333 = NOVALUE;
        _25330 = NOVALUE;

        /** 			    fixups &= st_index*/
        Append(&_fixups_47749, _fixups_47749, _st_index_47747);
L4: 
L3: 

        /** 		if keylist[k][K_TOKEN] = PROC then*/
        _2 = (int)SEQ_PTR(_63keylist_23599);
        _25335 = (int)*(((s1_ptr)_2)->base + _k_47751);
        _2 = (int)SEQ_PTR(_25335);
        _25336 = (int)*(((s1_ptr)_2)->base + 3);
        _25335 = NOVALUE;
        if (binary_op_a(NOTEQ, _25336, 27)){
            _25336 = NOVALUE;
            goto L5; // [341] 365
        }
        _25336 = NOVALUE;

        /** 			if equal(kname, "<TopLevel>") then*/
        if (_kname_47748 == _25338)
        _25339 = 1;
        else if (IS_ATOM_INT(_kname_47748) && IS_ATOM_INT(_25338))
        _25339 = 0;
        else
        _25339 = (compare(_kname_47748, _25338) == 0);
        if (_25339 == 0)
        {
            _25339 = NOVALUE;
            goto L6; // [351] 462
        }
        else{
            _25339 = NOVALUE;
        }

        /** 				TopLevelSub = st_index*/
        _25TopLevelSub_12269 = _st_index_47747;
        goto L6; // [362] 462
L5: 

        /** 		elsif keylist[k][K_TOKEN] = TYPE then*/
        _2 = (int)SEQ_PTR(_63keylist_23599);
        _25340 = (int)*(((s1_ptr)_2)->base + _k_47751);
        _2 = (int)SEQ_PTR(_25340);
        _25341 = (int)*(((s1_ptr)_2)->base + 3);
        _25340 = NOVALUE;
        if (binary_op_a(NOTEQ, _25341, 504)){
            _25341 = NOVALUE;
            goto L7; // [381] 461
        }
        _25341 = NOVALUE;

        /** 			if equal(kname, "object") then*/
        if (_kname_47748 == _25343)
        _25344 = 1;
        else if (IS_ATOM_INT(_kname_47748) && IS_ATOM_INT(_25343))
        _25344 = 0;
        else
        _25344 = (compare(_kname_47748, _25343) == 0);
        if (_25344 == 0)
        {
            _25344 = NOVALUE;
            goto L8; // [391] 404
        }
        else{
            _25344 = NOVALUE;
        }

        /** 				object_type = st_index*/
        _52object_type_47099 = _st_index_47747;
        goto L9; // [401] 460
L8: 

        /** 			elsif equal(kname, "atom") then*/
        if (_kname_47748 == _25345)
        _25346 = 1;
        else if (IS_ATOM_INT(_kname_47748) && IS_ATOM_INT(_25345))
        _25346 = 0;
        else
        _25346 = (compare(_kname_47748, _25345) == 0);
        if (_25346 == 0)
        {
            _25346 = NOVALUE;
            goto LA; // [410] 423
        }
        else{
            _25346 = NOVALUE;
        }

        /** 				atom_type = st_index*/
        _52atom_type_47101 = _st_index_47747;
        goto L9; // [420] 460
LA: 

        /** 			elsif equal(kname, "integer") then*/
        if (_kname_47748 == _25347)
        _25348 = 1;
        else if (IS_ATOM_INT(_kname_47748) && IS_ATOM_INT(_25347))
        _25348 = 0;
        else
        _25348 = (compare(_kname_47748, _25347) == 0);
        if (_25348 == 0)
        {
            _25348 = NOVALUE;
            goto LB; // [429] 442
        }
        else{
            _25348 = NOVALUE;
        }

        /** 				integer_type = st_index*/
        _52integer_type_47105 = _st_index_47747;
        goto L9; // [439] 460
LB: 

        /** 			elsif equal(kname, "sequence") then*/
        if (_kname_47748 == _25349)
        _25350 = 1;
        else if (IS_ATOM_INT(_kname_47748) && IS_ATOM_INT(_25349))
        _25350 = 0;
        else
        _25350 = (compare(_kname_47748, _25349) == 0);
        if (_25350 == 0)
        {
            _25350 = NOVALUE;
            goto LC; // [448] 459
        }
        else{
            _25350 = NOVALUE;
        }

        /** 				sequence_type = st_index*/
        _52sequence_type_47103 = _st_index_47747;
LC: 
L9: 
L7: 
L6: 

        /** 		if buckets[hashval] = 0 then*/
        _2 = (int)SEQ_PTR(_52buckets_47095);
        _25351 = (int)*(((s1_ptr)_2)->base + _hashval_47743);
        if (binary_op_a(NOTEQ, _25351, 0)){
            _25351 = NOVALUE;
            goto LD; // [470] 485
        }
        _25351 = NOVALUE;

        /** 			buckets[hashval] = st_index*/
        _2 = (int)SEQ_PTR(_52buckets_47095);
        _2 = (int)(((s1_ptr)_2)->base + _hashval_47743);
        _1 = *(int *)_2;
        *(int *)_2 = _st_index_47747;
        DeRef(_1);
        goto LE; // [482] 553
LD: 

        /** 			s = buckets[hashval]*/
        _2 = (int)SEQ_PTR(_52buckets_47095);
        _s_47746 = (int)*(((s1_ptr)_2)->base + _hashval_47743);
        if (!IS_ATOM_INT(_s_47746)){
            _s_47746 = (long)DBL_PTR(_s_47746)->dbl;
        }

        /** 			while SymTab[s][S_SAMEHASH] != 0 do*/
LF: 
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _25354 = (int)*(((s1_ptr)_2)->base + _s_47746);
        _2 = (int)SEQ_PTR(_25354);
        _25355 = (int)*(((s1_ptr)_2)->base + 9);
        _25354 = NOVALUE;
        if (binary_op_a(EQUALS, _25355, 0)){
            _25355 = NOVALUE;
            goto L10; // [512] 537
        }
        _25355 = NOVALUE;

        /** 				s = SymTab[s][S_SAMEHASH]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _25357 = (int)*(((s1_ptr)_2)->base + _s_47746);
        _2 = (int)SEQ_PTR(_25357);
        _s_47746 = (int)*(((s1_ptr)_2)->base + 9);
        if (!IS_ATOM_INT(_s_47746)){
            _s_47746 = (long)DBL_PTR(_s_47746)->dbl;
        }
        _25357 = NOVALUE;

        /** 			end while*/
        goto LF; // [534] 500
L10: 

        /** 			SymTab[s][S_SAMEHASH] = st_index*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_s_47746 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 9);
        _1 = *(int *)_2;
        *(int *)_2 = _st_index_47747;
        DeRef(_1);
        _25359 = NOVALUE;
LE: 

        /** 	end for*/
        _k_47751 = _k_47751 + 1;
        goto L1; // [555] 22
L2: 
        ;
    }

    /** 	file_start_sym = length(SymTab)*/
    if (IS_SEQUENCE(_26SymTab_11138)){
            _25file_start_sym_12268 = SEQ_PTR(_26SymTab_11138)->length;
    }
    else {
        _25file_start_sym_12268 = 1;
    }

    /** 	sequence si, sj*/

    /** 	CurrentSub = TopLevelSub*/
    _25CurrentSub_12270 = _25TopLevelSub_12269;

    /** 	for i=1 to length(fixups) do*/
    if (IS_SEQUENCE(_fixups_47749)){
            _25362 = SEQ_PTR(_fixups_47749)->length;
    }
    else {
        _25362 = 1;
    }
    {
        int _i_47894;
        _i_47894 = 1;
L11: 
        if (_i_47894 > _25362){
            goto L12; // [585] 945
        }

        /** 	    si = SymTab[fixups[i]][S_CODE] -- seq of either 0's or sequences of tokens*/
        _2 = (int)SEQ_PTR(_fixups_47749);
        _25363 = (int)*(((s1_ptr)_2)->base + _i_47894);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _25364 = (int)*(((s1_ptr)_2)->base + _25363);
        DeRef(_si_47889);
        _2 = (int)SEQ_PTR(_25364);
        if (!IS_ATOM_INT(_25S_CODE_11925)){
            _si_47889 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
        }
        else{
            _si_47889 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
        }
        Ref(_si_47889);
        _25364 = NOVALUE;

        /** 	    for j=1 to length(si) do*/
        if (IS_SEQUENCE(_si_47889)){
                _25366 = SEQ_PTR(_si_47889)->length;
        }
        else {
            _25366 = 1;
        }
        {
            int _j_47902;
            _j_47902 = 1;
L13: 
            if (_j_47902 > _25366){
                goto L14; // [617] 919
            }

            /** 	        if sequence(si[j]) then*/
            _2 = (int)SEQ_PTR(_si_47889);
            _25367 = (int)*(((s1_ptr)_2)->base + _j_47902);
            _25368 = IS_SEQUENCE(_25367);
            _25367 = NOVALUE;
            if (_25368 == 0)
            {
                _25368 = NOVALUE;
                goto L15; // [633] 912
            }
            else{
                _25368 = NOVALUE;
            }

            /** 	            sj = si[j] -- a sequence of tokens*/
            DeRef(_sj_47890);
            _2 = (int)SEQ_PTR(_si_47889);
            _sj_47890 = (int)*(((s1_ptr)_2)->base + _j_47902);
            Ref(_sj_47890);

            /** 				for ij=1 to length(sj) do*/
            if (IS_SEQUENCE(_sj_47890)){
                    _25370 = SEQ_PTR(_sj_47890)->length;
            }
            else {
                _25370 = 1;
            }
            {
                int _ij_47909;
                _ij_47909 = 1;
L16: 
                if (_ij_47909 > _25370){
                    goto L17; // [649] 905
                }

                /** 	                switch sj[ij][T_ID] with fallthru do*/
                _2 = (int)SEQ_PTR(_sj_47890);
                _25371 = (int)*(((s1_ptr)_2)->base + _ij_47909);
                _2 = (int)SEQ_PTR(_25371);
                _25372 = (int)*(((s1_ptr)_2)->base + 1);
                _25371 = NOVALUE;
                if (IS_SEQUENCE(_25372) ){
                    goto L18; // [668] 898
                }
                if(!IS_ATOM_INT(_25372)){
                    if( (DBL_PTR(_25372)->dbl != (double) ((int) DBL_PTR(_25372)->dbl) ) ){
                        goto L18; // [668] 898
                    }
                    _0 = (int) DBL_PTR(_25372)->dbl;
                }
                else {
                    _0 = _25372;
                };
                _25372 = NOVALUE;
                switch ( _0 ){ 

                    /** 	                    case ATOM then -- must create a lasting temp*/
                    case 502:

                    /** 	                    	if integer(sj[ij][T_SYM]) then*/
                    _2 = (int)SEQ_PTR(_sj_47890);
                    _25375 = (int)*(((s1_ptr)_2)->base + _ij_47909);
                    _2 = (int)SEQ_PTR(_25375);
                    _25376 = (int)*(((s1_ptr)_2)->base + 2);
                    _25375 = NOVALUE;
                    if (IS_ATOM_INT(_25376))
                    _25377 = 1;
                    else if (IS_ATOM_DBL(_25376))
                    _25377 = IS_ATOM_INT(DoubleToInt(_25376));
                    else
                    _25377 = 0;
                    _25376 = NOVALUE;
                    if (_25377 == 0)
                    {
                        _25377 = NOVALUE;
                        goto L19; // [692] 716
                    }
                    else{
                        _25377 = NOVALUE;
                    }

                    /** 								st_index = NewIntSym(sj[ij][T_SYM])*/
                    _2 = (int)SEQ_PTR(_sj_47890);
                    _25378 = (int)*(((s1_ptr)_2)->base + _ij_47909);
                    _2 = (int)SEQ_PTR(_25378);
                    _25379 = (int)*(((s1_ptr)_2)->base + 2);
                    _25378 = NOVALUE;
                    Ref(_25379);
                    _st_index_47747 = _52NewIntSym(_25379);
                    _25379 = NOVALUE;
                    if (!IS_ATOM_INT(_st_index_47747)) {
                        _1 = (long)(DBL_PTR(_st_index_47747)->dbl);
                        if (UNIQUE(DBL_PTR(_st_index_47747)) && (DBL_PTR(_st_index_47747)->cleanup != 0))
                        RTFatal("Cannot assign value with a destructor to an integer");                        DeRefDS(_st_index_47747);
                        _st_index_47747 = _1;
                    }
                    goto L1A; // [713] 735
L19: 

                    /** 								st_index = NewDoubleSym(sj[ij][T_SYM])*/
                    _2 = (int)SEQ_PTR(_sj_47890);
                    _25381 = (int)*(((s1_ptr)_2)->base + _ij_47909);
                    _2 = (int)SEQ_PTR(_25381);
                    _25382 = (int)*(((s1_ptr)_2)->base + 2);
                    _25381 = NOVALUE;
                    Ref(_25382);
                    _st_index_47747 = _52NewDoubleSym(_25382);
                    _25382 = NOVALUE;
                    if (!IS_ATOM_INT(_st_index_47747)) {
                        _1 = (long)(DBL_PTR(_st_index_47747)->dbl);
                        if (UNIQUE(DBL_PTR(_st_index_47747)) && (DBL_PTR(_st_index_47747)->cleanup != 0))
                        RTFatal("Cannot assign value with a destructor to an integer");                        DeRefDS(_st_index_47747);
                        _st_index_47747 = _1;
                    }
L1A: 

                    /** 							SymTab[st_index][S_SCOPE] = IN_USE -- TempKeep()*/
                    _2 = (int)SEQ_PTR(_26SymTab_11138);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        _26SymTab_11138 = MAKE_SEQ(_2);
                    }
                    _3 = (int)(_st_index_47747 + ((s1_ptr)_2)->base);
                    _2 = (int)SEQ_PTR(*(int *)_3);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        *(int *)_3 = MAKE_SEQ(_2);
                    }
                    _2 = (int)(((s1_ptr)_2)->base + 4);
                    _1 = *(int *)_2;
                    *(int *)_2 = 1;
                    DeRef(_1);
                    _25384 = NOVALUE;

                    /** 							sj[ij][T_SYM] = st_index*/
                    _2 = (int)SEQ_PTR(_sj_47890);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        _sj_47890 = MAKE_SEQ(_2);
                    }
                    _3 = (int)(_ij_47909 + ((s1_ptr)_2)->base);
                    _2 = (int)SEQ_PTR(*(int *)_3);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        *(int *)_3 = MAKE_SEQ(_2);
                    }
                    _2 = (int)(((s1_ptr)_2)->base + 2);
                    _1 = *(int *)_2;
                    *(int *)_2 = _st_index_47747;
                    DeRef(_1);
                    _25386 = NOVALUE;

                    /** 							break*/
                    goto L18; // [769] 898

                    /** 						case STRING then -- same*/
                    case 503:

                    /** 	                    	st_index = NewStringSym(sj[ij][T_SYM])*/
                    _2 = (int)SEQ_PTR(_sj_47890);
                    _25388 = (int)*(((s1_ptr)_2)->base + _ij_47909);
                    _2 = (int)SEQ_PTR(_25388);
                    _25389 = (int)*(((s1_ptr)_2)->base + 2);
                    _25388 = NOVALUE;
                    Ref(_25389);
                    _st_index_47747 = _52NewStringSym(_25389);
                    _25389 = NOVALUE;
                    if (!IS_ATOM_INT(_st_index_47747)) {
                        _1 = (long)(DBL_PTR(_st_index_47747)->dbl);
                        if (UNIQUE(DBL_PTR(_st_index_47747)) && (DBL_PTR(_st_index_47747)->cleanup != 0))
                        RTFatal("Cannot assign value with a destructor to an integer");                        DeRefDS(_st_index_47747);
                        _st_index_47747 = _1;
                    }

                    /** 							SymTab[st_index][S_SCOPE] = IN_USE -- TempKeep()*/
                    _2 = (int)SEQ_PTR(_26SymTab_11138);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        _26SymTab_11138 = MAKE_SEQ(_2);
                    }
                    _3 = (int)(_st_index_47747 + ((s1_ptr)_2)->base);
                    _2 = (int)SEQ_PTR(*(int *)_3);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        *(int *)_3 = MAKE_SEQ(_2);
                    }
                    _2 = (int)(((s1_ptr)_2)->base + 4);
                    _1 = *(int *)_2;
                    *(int *)_2 = 1;
                    DeRef(_1);
                    _25391 = NOVALUE;

                    /** 							sj[ij][T_SYM] = st_index*/
                    _2 = (int)SEQ_PTR(_sj_47890);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        _sj_47890 = MAKE_SEQ(_2);
                    }
                    _3 = (int)(_ij_47909 + ((s1_ptr)_2)->base);
                    _2 = (int)SEQ_PTR(*(int *)_3);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        *(int *)_3 = MAKE_SEQ(_2);
                    }
                    _2 = (int)(((s1_ptr)_2)->base + 2);
                    _1 = *(int *)_2;
                    *(int *)_2 = _st_index_47747;
                    DeRef(_1);
                    _25393 = NOVALUE;

                    /** 							break*/
                    goto L18; // [825] 898

                    /** 						case BUILT_IN then -- name of a builtin in econd field*/
                    case 511:

                    /**                             sj[ij] = keyfind(sj[ij][T_SYM],-1)*/
                    _2 = (int)SEQ_PTR(_sj_47890);
                    _25395 = (int)*(((s1_ptr)_2)->base + _ij_47909);
                    _2 = (int)SEQ_PTR(_25395);
                    _25396 = (int)*(((s1_ptr)_2)->base + 2);
                    _25395 = NOVALUE;
                    Ref(_25396);
                    DeRef(_25875);
                    _25875 = _25396;
                    _25876 = _52hashfn(_25875);
                    _25875 = NOVALUE;
                    Ref(_25396);
                    _25397 = _52keyfind(_25396, -1, _25current_file_no_12262, 0, _25876);
                    _25396 = NOVALUE;
                    _25876 = NOVALUE;
                    _2 = (int)SEQ_PTR(_sj_47890);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        _sj_47890 = MAKE_SEQ(_2);
                    }
                    _2 = (int)(((s1_ptr)_2)->base + _ij_47909);
                    _1 = *(int *)_2;
                    *(int *)_2 = _25397;
                    if( _1 != _25397 ){
                        DeRef(_1);
                    }
                    _25397 = NOVALUE;

                    /** 							break*/
                    goto L18; // [866] 898

                    /** 						case DEF_PARAM then*/
                    case 510:

                    /** 							sj[ij][T_SYM] &= fixups[i]*/
                    _2 = (int)SEQ_PTR(_sj_47890);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        _sj_47890 = MAKE_SEQ(_2);
                    }
                    _3 = (int)(_ij_47909 + ((s1_ptr)_2)->base);
                    _2 = (int)SEQ_PTR(_fixups_47749);
                    _25400 = (int)*(((s1_ptr)_2)->base + _i_47894);
                    _2 = (int)SEQ_PTR(*(int *)_3);
                    _25401 = (int)*(((s1_ptr)_2)->base + 2);
                    _25398 = NOVALUE;
                    if (IS_SEQUENCE(_25401) && IS_ATOM(_25400)) {
                        Append(&_25402, _25401, _25400);
                    }
                    else if (IS_ATOM(_25401) && IS_SEQUENCE(_25400)) {
                    }
                    else {
                        Concat((object_ptr)&_25402, _25401, _25400);
                        _25401 = NOVALUE;
                    }
                    _25401 = NOVALUE;
                    _25400 = NOVALUE;
                    _2 = (int)SEQ_PTR(*(int *)_3);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        *(int *)_3 = MAKE_SEQ(_2);
                    }
                    _2 = (int)(((s1_ptr)_2)->base + 2);
                    _1 = *(int *)_2;
                    *(int *)_2 = _25402;
                    if( _1 != _25402 ){
                        DeRef(_1);
                    }
                    _25402 = NOVALUE;
                    _25398 = NOVALUE;
                ;}L18: 

                /** 				end for*/
                _ij_47909 = _ij_47909 + 1;
                goto L16; // [900] 656
L17: 
                ;
            }

            /** 				si[j] = sj*/
            RefDS(_sj_47890);
            _2 = (int)SEQ_PTR(_si_47889);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _si_47889 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_47902);
            _1 = *(int *)_2;
            *(int *)_2 = _sj_47890;
            DeRef(_1);
L15: 

            /** 		end for*/
            _j_47902 = _j_47902 + 1;
            goto L13; // [914] 624
L14: 
            ;
        }

        /** 		SymTab[fixups[i]][S_CODE] = si*/
        _2 = (int)SEQ_PTR(_fixups_47749);
        _25403 = (int)*(((s1_ptr)_2)->base + _i_47894);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_25403 + ((s1_ptr)_2)->base);
        RefDS(_si_47889);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_25S_CODE_11925))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _25S_CODE_11925);
        _1 = *(int *)_2;
        *(int *)_2 = _si_47889;
        DeRef(_1);
        _25404 = NOVALUE;

        /** 	end for*/
        _i_47894 = _i_47894 + 1;
        goto L11; // [940] 592
L12: 
        ;
    }

    /** end procedure*/
    DeRef(_kname_47748);
    DeRefi(_fixups_47749);
    DeRef(_si_47889);
    DeRef(_sj_47890);
    _25323 = NOVALUE;
    _25304 = NOVALUE;
    _25363 = NOVALUE;
    _25403 = NOVALUE;
    return;
    ;
}


void _52add_ref(int _tok_47977)
{
    int _s_47979 = NOVALUE;
    int _25420 = NOVALUE;
    int _25419 = NOVALUE;
    int _25417 = NOVALUE;
    int _25416 = NOVALUE;
    int _25415 = NOVALUE;
    int _25413 = NOVALUE;
    int _25412 = NOVALUE;
    int _25411 = NOVALUE;
    int _25410 = NOVALUE;
    int _25409 = NOVALUE;
    int _25408 = NOVALUE;
    int _25407 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	s = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_47977);
    _s_47979 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_47979)){
        _s_47979 = (long)DBL_PTR(_s_47979)->dbl;
    }

    /** 	if s != CurrentSub and -- ignore self-ref's*/
    _25407 = (_s_47979 != _25CurrentSub_12270);
    if (_25407 == 0) {
        goto L1; // [19] 98
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25409 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_25409);
    _25410 = (int)*(((s1_ptr)_2)->base + 24);
    _25409 = NOVALUE;
    _25411 = find_from(_s_47979, _25410, 1);
    _25410 = NOVALUE;
    _25412 = (_25411 == 0);
    _25411 = NOVALUE;
    if (_25412 == 0)
    {
        DeRef(_25412);
        _25412 = NOVALUE;
        goto L1; // [46] 98
    }
    else{
        DeRef(_25412);
        _25412 = NOVALUE;
    }

    /** 		SymTab[s][S_NREFS] += 1*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_s_47979 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _25415 = (int)*(((s1_ptr)_2)->base + 12);
    _25413 = NOVALUE;
    if (IS_ATOM_INT(_25415)) {
        _25416 = _25415 + 1;
        if (_25416 > MAXINT){
            _25416 = NewDouble((double)_25416);
        }
    }
    else
    _25416 = binary_op(PLUS, 1, _25415);
    _25415 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = _25416;
    if( _1 != _25416 ){
        DeRef(_1);
    }
    _25416 = NOVALUE;
    _25413 = NOVALUE;

    /** 		SymTab[CurrentSub][S_REFLIST] &= s*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25CurrentSub_12270 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _25419 = (int)*(((s1_ptr)_2)->base + 24);
    _25417 = NOVALUE;
    if (IS_SEQUENCE(_25419) && IS_ATOM(_s_47979)) {
        Append(&_25420, _25419, _s_47979);
    }
    else if (IS_ATOM(_25419) && IS_SEQUENCE(_s_47979)) {
    }
    else {
        Concat((object_ptr)&_25420, _25419, _s_47979);
        _25419 = NOVALUE;
    }
    _25419 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 24);
    _1 = *(int *)_2;
    *(int *)_2 = _25420;
    if( _1 != _25420 ){
        DeRef(_1);
    }
    _25420 = NOVALUE;
    _25417 = NOVALUE;
L1: 

    /** end procedure*/
    DeRef(_tok_47977);
    DeRef(_25407);
    _25407 = NOVALUE;
    return;
    ;
}


void _52mark_all(int _attribute_48009)
{
    int _p_48012 = NOVALUE;
    int _sym_file_48019 = NOVALUE;
    int _scope_48036 = NOVALUE;
    int _25452 = NOVALUE;
    int _25451 = NOVALUE;
    int _25450 = NOVALUE;
    int _25448 = NOVALUE;
    int _25446 = NOVALUE;
    int _25445 = NOVALUE;
    int _25444 = NOVALUE;
    int _25443 = NOVALUE;
    int _25442 = NOVALUE;
    int _25440 = NOVALUE;
    int _25439 = NOVALUE;
    int _25438 = NOVALUE;
    int _25437 = NOVALUE;
    int _25433 = NOVALUE;
    int _25432 = NOVALUE;
    int _25431 = NOVALUE;
    int _25429 = NOVALUE;
    int _25428 = NOVALUE;
    int _25426 = NOVALUE;
    int _25424 = NOVALUE;
    int _25421 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	if just_mark_everything_from then*/
    if (_52just_mark_everything_from_48006 == 0)
    {
        goto L1; // [7] 270
    }
    else{
    }

    /** 		symtab_pointer p = SymTab[just_mark_everything_from][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25421 = (int)*(((s1_ptr)_2)->base + _52just_mark_everything_from_48006);
    _2 = (int)SEQ_PTR(_25421);
    _p_48012 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_p_48012)){
        _p_48012 = (long)DBL_PTR(_p_48012)->dbl;
    }
    _25421 = NOVALUE;

    /** 		while p != 0 do*/
L2: 
    if (_p_48012 == 0)
    goto L3; // [33] 269

    /** 			integer sym_file = SymTab[p][S_FILE_NO]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25424 = (int)*(((s1_ptr)_2)->base + _p_48012);
    _2 = (int)SEQ_PTR(_25424);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _sym_file_48019 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _sym_file_48019 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    if (!IS_ATOM_INT(_sym_file_48019)){
        _sym_file_48019 = (long)DBL_PTR(_sym_file_48019)->dbl;
    }
    _25424 = NOVALUE;

    /** 			just_mark_everything_from = p*/
    _52just_mark_everything_from_48006 = _p_48012;

    /** 			if sym_file = current_file_no or find( sym_file, recheck_files ) then*/
    _25426 = (_sym_file_48019 == _25current_file_no_12262);
    if (_25426 != 0) {
        goto L4; // [68] 84
    }
    _25428 = find_from(_sym_file_48019, _52recheck_files_48079, 1);
    if (_25428 == 0)
    {
        _25428 = NOVALUE;
        goto L5; // [80] 108
    }
    else{
        _25428 = NOVALUE;
    }
L4: 

    /** 				SymTab[p][attribute] += 1*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_48012 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _25431 = (int)*(((s1_ptr)_2)->base + _attribute_48009);
    _25429 = NOVALUE;
    if (IS_ATOM_INT(_25431)) {
        _25432 = _25431 + 1;
        if (_25432 > MAXINT){
            _25432 = NewDouble((double)_25432);
        }
    }
    else
    _25432 = binary_op(PLUS, 1, _25431);
    _25431 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _attribute_48009);
    _1 = *(int *)_2;
    *(int *)_2 = _25432;
    if( _1 != _25432 ){
        DeRef(_1);
    }
    _25432 = NOVALUE;
    _25429 = NOVALUE;
    goto L6; // [105] 246
L5: 

    /** 				integer scope = SymTab[p][S_SCOPE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25433 = (int)*(((s1_ptr)_2)->base + _p_48012);
    _2 = (int)SEQ_PTR(_25433);
    _scope_48036 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_scope_48036)){
        _scope_48036 = (long)DBL_PTR(_scope_48036)->dbl;
    }
    _25433 = NOVALUE;

    /** 				switch scope with fallthru do*/
    _0 = _scope_48036;
    switch ( _0 ){ 

        /** 					case SC_PUBLIC then*/
        case 13:

        /** 						if and_bits( DIRECT_OR_PUBLIC_INCLUDE, include_matrix[current_file_no][sym_file] ) then*/
        _2 = (int)SEQ_PTR(_26include_matrix_11145);
        _25437 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
        _2 = (int)SEQ_PTR(_25437);
        _25438 = (int)*(((s1_ptr)_2)->base + _sym_file_48019);
        _25437 = NOVALUE;
        if (IS_ATOM_INT(_25438)) {
            {unsigned long tu;
                 tu = (unsigned long)6 & (unsigned long)_25438;
                 _25439 = MAKE_UINT(tu);
            }
        }
        else {
            _25439 = binary_op(AND_BITS, 6, _25438);
        }
        _25438 = NOVALUE;
        if (_25439 == 0) {
            DeRef(_25439);
            _25439 = NOVALUE;
            goto L7; // [155] 243
        }
        else {
            if (!IS_ATOM_INT(_25439) && DBL_PTR(_25439)->dbl == 0.0){
                DeRef(_25439);
                _25439 = NOVALUE;
                goto L7; // [155] 243
            }
            DeRef(_25439);
            _25439 = NOVALUE;
        }
        DeRef(_25439);
        _25439 = NOVALUE;

        /** 							SymTab[p][attribute] += 1*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_p_48012 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _25442 = (int)*(((s1_ptr)_2)->base + _attribute_48009);
        _25440 = NOVALUE;
        if (IS_ATOM_INT(_25442)) {
            _25443 = _25442 + 1;
            if (_25443 > MAXINT){
                _25443 = NewDouble((double)_25443);
            }
        }
        else
        _25443 = binary_op(PLUS, 1, _25442);
        _25442 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _attribute_48009);
        _1 = *(int *)_2;
        *(int *)_2 = _25443;
        if( _1 != _25443 ){
            DeRef(_1);
        }
        _25443 = NOVALUE;
        _25440 = NOVALUE;

        /** 						break*/
        goto L7; // [182] 243

        /** 					case SC_EXPORT then*/
        case 11:

        /** 						if not and_bits( DIRECT_INCLUDE, include_matrix[current_file_no][sym_file] ) then*/
        _2 = (int)SEQ_PTR(_26include_matrix_11145);
        _25444 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
        _2 = (int)SEQ_PTR(_25444);
        _25445 = (int)*(((s1_ptr)_2)->base + _sym_file_48019);
        _25444 = NOVALUE;
        if (IS_ATOM_INT(_25445)) {
            {unsigned long tu;
                 tu = (unsigned long)2 & (unsigned long)_25445;
                 _25446 = MAKE_UINT(tu);
            }
        }
        else {
            _25446 = binary_op(AND_BITS, 2, _25445);
        }
        _25445 = NOVALUE;
        if (IS_ATOM_INT(_25446)) {
            if (_25446 != 0){
                DeRef(_25446);
                _25446 = NOVALUE;
                goto L8; // [208] 216
            }
        }
        else {
            if (DBL_PTR(_25446)->dbl != 0.0){
                DeRef(_25446);
                _25446 = NOVALUE;
                goto L8; // [208] 216
            }
        }
        DeRef(_25446);
        _25446 = NOVALUE;

        /** 							break*/
        goto L9; // [213] 217
L8: 
L9: 

        /** 					case SC_GLOBAL then*/
        case 6:

        /** 						SymTab[p][attribute] += 1*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_p_48012 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _25450 = (int)*(((s1_ptr)_2)->base + _attribute_48009);
        _25448 = NOVALUE;
        if (IS_ATOM_INT(_25450)) {
            _25451 = _25450 + 1;
            if (_25451 > MAXINT){
                _25451 = NewDouble((double)_25451);
            }
        }
        else
        _25451 = binary_op(PLUS, 1, _25450);
        _25450 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _attribute_48009);
        _1 = *(int *)_2;
        *(int *)_2 = _25451;
        if( _1 != _25451 ){
            DeRef(_1);
        }
        _25451 = NOVALUE;
        _25448 = NOVALUE;
    ;}L7: 
L6: 

    /** 			p = SymTab[p][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25452 = (int)*(((s1_ptr)_2)->base + _p_48012);
    _2 = (int)SEQ_PTR(_25452);
    _p_48012 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_p_48012)){
        _p_48012 = (long)DBL_PTR(_p_48012)->dbl;
    }
    _25452 = NOVALUE;

    /** 		end while*/
    goto L2; // [266] 33
L3: 
L1: 

    /** end procedure*/
    DeRef(_25426);
    _25426 = NOVALUE;
    return;
    ;
}


void _52mark_final_targets()
{
    int _marked_48094 = NOVALUE;
    int _25458 = NOVALUE;
    int _25456 = NOVALUE;
    int _25455 = NOVALUE;
    int _25454 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if just_mark_everything_from then*/
    if (_52just_mark_everything_from_48006 == 0)
    {
        goto L1; // [5] 44
    }
    else{
    }

    /** 		if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L2; // [12] 25
    }
    else{
    }

    /** 			mark_all( S_RI_TARGET )*/
    _52mark_all(53);
    goto L3; // [22] 152
L2: 

    /** 		elsif BIND then*/
    if (_25BIND_11877 == 0)
    {
        goto L3; // [29] 152
    }
    else{
    }

    /** 			mark_all( S_NREFS )*/
    _52mark_all(12);
    goto L3; // [41] 152
L1: 

    /** 	elsif length( recheck_targets ) then*/
    if (IS_SEQUENCE(_52recheck_targets_48078)){
            _25454 = SEQ_PTR(_52recheck_targets_48078)->length;
    }
    else {
        _25454 = 1;
    }
    if (_25454 == 0)
    {
        _25454 = NOVALUE;
        goto L4; // [51] 151
    }
    else{
        _25454 = NOVALUE;
    }

    /** 		for i = length( recheck_targets ) to 1 by -1 do*/
    if (IS_SEQUENCE(_52recheck_targets_48078)){
            _25455 = SEQ_PTR(_52recheck_targets_48078)->length;
    }
    else {
        _25455 = 1;
    }
    {
        int _i_48092;
        _i_48092 = _25455;
L5: 
        if (_i_48092 < 1){
            goto L6; // [61] 150
        }

        /** 			integer marked = 0*/
        _marked_48094 = 0;

        /** 			if TRANSLATE then*/
        if (_25TRANSLATE_11874 == 0)
        {
            goto L7; // [77] 100
        }
        else{
        }

        /** 				marked = MarkTargets( recheck_targets[i], S_RI_TARGET )*/
        _2 = (int)SEQ_PTR(_52recheck_targets_48078);
        _25456 = (int)*(((s1_ptr)_2)->base + _i_48092);
        Ref(_25456);
        _marked_48094 = _52MarkTargets(_25456, 53);
        _25456 = NOVALUE;
        if (!IS_ATOM_INT(_marked_48094)) {
            _1 = (long)(DBL_PTR(_marked_48094)->dbl);
            if (UNIQUE(DBL_PTR(_marked_48094)) && (DBL_PTR(_marked_48094)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_marked_48094);
            _marked_48094 = _1;
        }
        goto L8; // [97] 126
L7: 

        /** 			elsif BIND then*/
        if (_25BIND_11877 == 0)
        {
            goto L9; // [104] 125
        }
        else{
        }

        /** 				marked = MarkTargets( recheck_targets[i], S_NREFS )*/
        _2 = (int)SEQ_PTR(_52recheck_targets_48078);
        _25458 = (int)*(((s1_ptr)_2)->base + _i_48092);
        Ref(_25458);
        _marked_48094 = _52MarkTargets(_25458, 12);
        _25458 = NOVALUE;
        if (!IS_ATOM_INT(_marked_48094)) {
            _1 = (long)(DBL_PTR(_marked_48094)->dbl);
            if (UNIQUE(DBL_PTR(_marked_48094)) && (DBL_PTR(_marked_48094)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_marked_48094);
            _marked_48094 = _1;
        }
L9: 
L8: 

        /** 			if marked then*/
        if (_marked_48094 == 0)
        {
            goto LA; // [128] 141
        }
        else{
        }

        /** 				recheck_targets = remove( recheck_targets, i )*/
        {
            s1_ptr assign_space = SEQ_PTR(_52recheck_targets_48078);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_i_48092)) ? _i_48092 : (long)(DBL_PTR(_i_48092)->dbl);
            int stop = (IS_ATOM_INT(_i_48092)) ? _i_48092 : (long)(DBL_PTR(_i_48092)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_52recheck_targets_48078), start, &_52recheck_targets_48078 );
                }
                else Tail(SEQ_PTR(_52recheck_targets_48078), stop+1, &_52recheck_targets_48078);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_52recheck_targets_48078), start, &_52recheck_targets_48078);
            }
            else {
                assign_slice_seq = &assign_space;
                _52recheck_targets_48078 = Remove_elements(start, stop, (SEQ_PTR(_52recheck_targets_48078)->ref == 1));
            }
        }
LA: 

        /** 		end for*/
        _i_48092 = _i_48092 + -1;
        goto L5; // [145] 68
L6: 
        ;
    }
L4: 
L3: 

    /** end procedure*/
    return;
    ;
}


int _52is_routine(int _sym_48112)
{
    int _tok_48113 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer tok = sym_token( sym )*/
    _tok_48113 = _52sym_token(_sym_48112);
    if (!IS_ATOM_INT(_tok_48113)) {
        _1 = (long)(DBL_PTR(_tok_48113)->dbl);
        if (UNIQUE(DBL_PTR(_tok_48113)) && (DBL_PTR(_tok_48113)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_tok_48113);
        _tok_48113 = _1;
    }

    /** 	switch tok do*/
    _0 = _tok_48113;
    switch ( _0 ){ 

        /** 		case FUNC, PROC, TYPE then*/
        case 501:
        case 27:
        case 504:

        /** 			return 1*/
        return 1;
        goto L1; // [32] 45

        /** 		case else*/
        default:

        /** 			return 0*/
        return 0;
    ;}L1: 
    ;
}


int _52is_visible(int _sym_48126, int _from_file_48127)
{
    int _scope_48128 = NOVALUE;
    int _sym_file_48131 = NOVALUE;
    int _visible_mask_48136 = NOVALUE;
    int _25472 = NOVALUE;
    int _25471 = NOVALUE;
    int _25470 = NOVALUE;
    int _25469 = NOVALUE;
    int _25465 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer scope = sym_scope( sym )*/
    _scope_48128 = _52sym_scope(_sym_48126);
    if (!IS_ATOM_INT(_scope_48128)) {
        _1 = (long)(DBL_PTR(_scope_48128)->dbl);
        if (UNIQUE(DBL_PTR(_scope_48128)) && (DBL_PTR(_scope_48128)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_scope_48128);
        _scope_48128 = _1;
    }

    /** 	integer sym_file = SymTab[sym][S_FILE_NO]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25465 = (int)*(((s1_ptr)_2)->base + _sym_48126);
    _2 = (int)SEQ_PTR(_25465);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _sym_file_48131 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _sym_file_48131 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    if (!IS_ATOM_INT(_sym_file_48131)){
        _sym_file_48131 = (long)DBL_PTR(_sym_file_48131)->dbl;
    }
    _25465 = NOVALUE;

    /** 	switch scope do*/
    _0 = _scope_48128;
    switch ( _0 ){ 

        /** 		case SC_PUBLIC then*/
        case 13:

        /** 			visible_mask = DIRECT_OR_PUBLIC_INCLUDE*/
        _visible_mask_48136 = 6;
        goto L1; // [49] 93

        /** 		case SC_EXPORT then*/
        case 11:

        /** 			visible_mask = DIRECT_INCLUDE*/
        _visible_mask_48136 = 2;
        goto L1; // [64] 93

        /** 		case SC_GLOBAL then*/
        case 6:

        /** 			return 1*/
        return 1;
        goto L1; // [76] 93

        /** 		case else*/
        default:

        /** 			return from_file = sym_file*/
        _25469 = (_from_file_48127 == _sym_file_48131);
        return _25469;
    ;}L1: 

    /** 	return and_bits( visible_mask, include_matrix[from_file][sym_file] )*/
    _2 = (int)SEQ_PTR(_26include_matrix_11145);
    _25470 = (int)*(((s1_ptr)_2)->base + _from_file_48127);
    _2 = (int)SEQ_PTR(_25470);
    _25471 = (int)*(((s1_ptr)_2)->base + _sym_file_48131);
    _25470 = NOVALUE;
    if (IS_ATOM_INT(_25471)) {
        {unsigned long tu;
             tu = (unsigned long)_visible_mask_48136 & (unsigned long)_25471;
             _25472 = MAKE_UINT(tu);
        }
    }
    else {
        _25472 = binary_op(AND_BITS, _visible_mask_48136, _25471);
    }
    _25471 = NOVALUE;
    DeRef(_25469);
    _25469 = NOVALUE;
    return _25472;
    ;
}


int _52MarkTargets(int _s_48156, int _attribute_48157)
{
    int _p_48159 = NOVALUE;
    int _sname_48160 = NOVALUE;
    int _string_48161 = NOVALUE;
    int _colon_48162 = NOVALUE;
    int _h_48163 = NOVALUE;
    int _scope_48164 = NOVALUE;
    int _found_48185 = NOVALUE;
    int _25524 = NOVALUE;
    int _25520 = NOVALUE;
    int _25518 = NOVALUE;
    int _25517 = NOVALUE;
    int _25516 = NOVALUE;
    int _25515 = NOVALUE;
    int _25513 = NOVALUE;
    int _25512 = NOVALUE;
    int _25511 = NOVALUE;
    int _25510 = NOVALUE;
    int _25509 = NOVALUE;
    int _25507 = NOVALUE;
    int _25506 = NOVALUE;
    int _25505 = NOVALUE;
    int _25503 = NOVALUE;
    int _25501 = NOVALUE;
    int _25499 = NOVALUE;
    int _25498 = NOVALUE;
    int _25497 = NOVALUE;
    int _25496 = NOVALUE;
    int _25494 = NOVALUE;
    int _25493 = NOVALUE;
    int _25492 = NOVALUE;
    int _25491 = NOVALUE;
    int _25489 = NOVALUE;
    int _25488 = NOVALUE;
    int _25484 = NOVALUE;
    int _25483 = NOVALUE;
    int _25482 = NOVALUE;
    int _25481 = NOVALUE;
    int _25480 = NOVALUE;
    int _25479 = NOVALUE;
    int _25478 = NOVALUE;
    int _25477 = NOVALUE;
    int _25476 = NOVALUE;
    int _25475 = NOVALUE;
    int _25474 = NOVALUE;
    int _25473 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_s_48156)) {
        _1 = (long)(DBL_PTR(_s_48156)->dbl);
        if (UNIQUE(DBL_PTR(_s_48156)) && (DBL_PTR(_s_48156)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_48156);
        _s_48156 = _1;
    }
    if (!IS_ATOM_INT(_attribute_48157)) {
        _1 = (long)(DBL_PTR(_attribute_48157)->dbl);
        if (UNIQUE(DBL_PTR(_attribute_48157)) && (DBL_PTR(_attribute_48157)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_attribute_48157);
        _attribute_48157 = _1;
    }

    /** 	sequence sname*/

    /** 	sequence string*/

    /** 	integer colon, h*/

    /** 	integer scope*/

    /** 	if (SymTab[s][S_MODE] = M_TEMP or*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25473 = (int)*(((s1_ptr)_2)->base + _s_48156);
    _2 = (int)SEQ_PTR(_25473);
    _25474 = (int)*(((s1_ptr)_2)->base + 3);
    _25473 = NOVALUE;
    if (IS_ATOM_INT(_25474)) {
        _25475 = (_25474 == 3);
    }
    else {
        _25475 = binary_op(EQUALS, _25474, 3);
    }
    _25474 = NOVALUE;
    if (IS_ATOM_INT(_25475)) {
        if (_25475 != 0) {
            _25476 = 1;
            goto L1; // [33] 59
        }
    }
    else {
        if (DBL_PTR(_25475)->dbl != 0.0) {
            _25476 = 1;
            goto L1; // [33] 59
        }
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25477 = (int)*(((s1_ptr)_2)->base + _s_48156);
    _2 = (int)SEQ_PTR(_25477);
    _25478 = (int)*(((s1_ptr)_2)->base + 3);
    _25477 = NOVALUE;
    if (IS_ATOM_INT(_25478)) {
        _25479 = (_25478 == 2);
    }
    else {
        _25479 = binary_op(EQUALS, _25478, 2);
    }
    _25478 = NOVALUE;
    DeRef(_25476);
    if (IS_ATOM_INT(_25479))
    _25476 = (_25479 != 0);
    else
    _25476 = DBL_PTR(_25479)->dbl != 0.0;
L1: 
    if (_25476 == 0) {
        goto L2; // [59] 440
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25481 = (int)*(((s1_ptr)_2)->base + _s_48156);
    _2 = (int)SEQ_PTR(_25481);
    _25482 = (int)*(((s1_ptr)_2)->base + 1);
    _25481 = NOVALUE;
    _25483 = IS_SEQUENCE(_25482);
    _25482 = NOVALUE;
    if (_25483 == 0)
    {
        _25483 = NOVALUE;
        goto L2; // [79] 440
    }
    else{
        _25483 = NOVALUE;
    }

    /** 		integer found = 0*/
    _found_48185 = 0;

    /** 		string = SymTab[s][S_OBJ]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25484 = (int)*(((s1_ptr)_2)->base + _s_48156);
    DeRef(_string_48161);
    _2 = (int)SEQ_PTR(_25484);
    _string_48161 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_string_48161);
    _25484 = NOVALUE;

    /** 		colon = find(':', string)*/
    _colon_48162 = find_from(58, _string_48161, 1);

    /** 		if colon = 0 then*/
    if (_colon_48162 != 0)
    goto L3; // [112] 126

    /** 			sname = string*/
    RefDS(_string_48161);
    DeRef(_sname_48160);
    _sname_48160 = _string_48161;
    goto L4; // [123] 200
L3: 

    /** 			sname = string[colon+1..$]  -- ignore namespace part*/
    _25488 = _colon_48162 + 1;
    if (_25488 > MAXINT){
        _25488 = NewDouble((double)_25488);
    }
    if (IS_SEQUENCE(_string_48161)){
            _25489 = SEQ_PTR(_string_48161)->length;
    }
    else {
        _25489 = 1;
    }
    rhs_slice_target = (object_ptr)&_sname_48160;
    RHS_Slice(_string_48161, _25488, _25489);

    /** 			while length(sname) and sname[1] = ' ' or sname[1] = '\t' do*/
L5: 
    if (IS_SEQUENCE(_sname_48160)){
            _25491 = SEQ_PTR(_sname_48160)->length;
    }
    else {
        _25491 = 1;
    }
    if (_25491 == 0) {
        _25492 = 0;
        goto L6; // [148] 164
    }
    _2 = (int)SEQ_PTR(_sname_48160);
    _25493 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_25493)) {
        _25494 = (_25493 == 32);
    }
    else {
        _25494 = binary_op(EQUALS, _25493, 32);
    }
    _25493 = NOVALUE;
    if (IS_ATOM_INT(_25494))
    _25492 = (_25494 != 0);
    else
    _25492 = DBL_PTR(_25494)->dbl != 0.0;
L6: 
    if (_25492 != 0) {
        goto L7; // [164] 181
    }
    _2 = (int)SEQ_PTR(_sname_48160);
    _25496 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_25496)) {
        _25497 = (_25496 == 9);
    }
    else {
        _25497 = binary_op(EQUALS, _25496, 9);
    }
    _25496 = NOVALUE;
    if (_25497 <= 0) {
        if (_25497 == 0) {
            DeRef(_25497);
            _25497 = NOVALUE;
            goto L8; // [177] 199
        }
        else {
            if (!IS_ATOM_INT(_25497) && DBL_PTR(_25497)->dbl == 0.0){
                DeRef(_25497);
                _25497 = NOVALUE;
                goto L8; // [177] 199
            }
            DeRef(_25497);
            _25497 = NOVALUE;
        }
    }
    DeRef(_25497);
    _25497 = NOVALUE;
L7: 

    /** 				sname = tail( sname, length( sname ) -1 )*/
    if (IS_SEQUENCE(_sname_48160)){
            _25498 = SEQ_PTR(_sname_48160)->length;
    }
    else {
        _25498 = 1;
    }
    _25499 = _25498 - 1;
    _25498 = NOVALUE;
    {
        int len = SEQ_PTR(_sname_48160)->length;
        int size = (IS_ATOM_INT(_25499)) ? _25499 : (long)(DBL_PTR(_25499)->dbl);
        if (size <= 0) {
            DeRef(_sname_48160);
            _sname_48160 = MAKE_SEQ(NewS1(0));
        }
        else if (len <= size) {
            RefDS(_sname_48160);
            DeRef(_sname_48160);
            _sname_48160 = _sname_48160;
        }
        else Tail(SEQ_PTR(_sname_48160), len-size+1, &_sname_48160);
    }
    _25499 = NOVALUE;

    /** 			end while*/
    goto L5; // [196] 145
L8: 
L4: 

    /** 		if length(sname) = 0 then*/
    if (IS_SEQUENCE(_sname_48160)){
            _25501 = SEQ_PTR(_sname_48160)->length;
    }
    else {
        _25501 = 1;
    }
    if (_25501 != 0)
    goto L9; // [207] 218

    /** 			return 1*/
    DeRefDS(_sname_48160);
    DeRef(_string_48161);
    DeRef(_25488);
    _25488 = NOVALUE;
    DeRef(_25475);
    _25475 = NOVALUE;
    DeRef(_25479);
    _25479 = NOVALUE;
    DeRef(_25494);
    _25494 = NOVALUE;
    return 1;
L9: 

    /** 		h = buckets[hashfn(sname)]*/
    RefDS(_sname_48160);
    _25503 = _52hashfn(_sname_48160);
    _2 = (int)SEQ_PTR(_52buckets_47095);
    if (!IS_ATOM_INT(_25503)){
        _h_48163 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25503)->dbl));
    }
    else{
        _h_48163 = (int)*(((s1_ptr)_2)->base + _25503);
    }
    if (!IS_ATOM_INT(_h_48163))
    _h_48163 = (long)DBL_PTR(_h_48163)->dbl;

    /** 		while h do*/
LA: 
    if (_h_48163 == 0)
    {
        goto LB; // [235] 381
    }
    else{
    }

    /** 			if equal(sname, SymTab[h][S_NAME]) then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25505 = (int)*(((s1_ptr)_2)->base + _h_48163);
    _2 = (int)SEQ_PTR(_25505);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _25506 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _25506 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _25505 = NOVALUE;
    if (_sname_48160 == _25506)
    _25507 = 1;
    else if (IS_ATOM_INT(_sname_48160) && IS_ATOM_INT(_25506))
    _25507 = 0;
    else
    _25507 = (compare(_sname_48160, _25506) == 0);
    _25506 = NOVALUE;
    if (_25507 == 0)
    {
        _25507 = NOVALUE;
        goto LC; // [256] 360
    }
    else{
        _25507 = NOVALUE;
    }

    /** 				if attribute = S_NREFS then*/
    if (_attribute_48157 != 12)
    goto LD; // [263] 289

    /** 					if BIND then*/
    if (_25BIND_11877 == 0)
    {
        goto LE; // [271] 359
    }
    else{
    }

    /** 						add_ref({PROC, h})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 27;
    ((int *)_2)[2] = _h_48163;
    _25509 = MAKE_SEQ(_1);
    _52add_ref(_25509);
    _25509 = NOVALUE;
    goto LE; // [286] 359
LD: 

    /** 				elsif is_routine( h ) and is_visible( h, current_file_no ) then*/
    _25510 = _52is_routine(_h_48163);
    if (IS_ATOM_INT(_25510)) {
        if (_25510 == 0) {
            goto LF; // [295] 358
        }
    }
    else {
        if (DBL_PTR(_25510)->dbl == 0.0) {
            goto LF; // [295] 358
        }
    }
    _25512 = _52is_visible(_h_48163, _25current_file_no_12262);
    if (_25512 == 0) {
        DeRef(_25512);
        _25512 = NOVALUE;
        goto LF; // [307] 358
    }
    else {
        if (!IS_ATOM_INT(_25512) && DBL_PTR(_25512)->dbl == 0.0){
            DeRef(_25512);
            _25512 = NOVALUE;
            goto LF; // [307] 358
        }
        DeRef(_25512);
        _25512 = NOVALUE;
    }
    DeRef(_25512);
    _25512 = NOVALUE;

    /** 					SymTab[h][attribute] += 1*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_h_48163 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _25515 = (int)*(((s1_ptr)_2)->base + _attribute_48157);
    _25513 = NOVALUE;
    if (IS_ATOM_INT(_25515)) {
        _25516 = _25515 + 1;
        if (_25516 > MAXINT){
            _25516 = NewDouble((double)_25516);
        }
    }
    else
    _25516 = binary_op(PLUS, 1, _25515);
    _25515 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _attribute_48157);
    _1 = *(int *)_2;
    *(int *)_2 = _25516;
    if( _1 != _25516 ){
        DeRef(_1);
    }
    _25516 = NOVALUE;
    _25513 = NOVALUE;

    /** 					if current_file_no = SymTab[h][S_FILE_NO] then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25517 = (int)*(((s1_ptr)_2)->base + _h_48163);
    _2 = (int)SEQ_PTR(_25517);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _25518 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _25518 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _25517 = NOVALUE;
    if (binary_op_a(NOTEQ, _25current_file_no_12262, _25518)){
        _25518 = NOVALUE;
        goto L10; // [347] 357
    }
    _25518 = NOVALUE;

    /** 						found = 1*/
    _found_48185 = 1;
L10: 
LF: 
LE: 
LC: 

    /** 			h = SymTab[h][S_SAMEHASH]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25520 = (int)*(((s1_ptr)_2)->base + _h_48163);
    _2 = (int)SEQ_PTR(_25520);
    _h_48163 = (int)*(((s1_ptr)_2)->base + 9);
    if (!IS_ATOM_INT(_h_48163)){
        _h_48163 = (long)DBL_PTR(_h_48163)->dbl;
    }
    _25520 = NOVALUE;

    /** 		end while*/
    goto LA; // [378] 235
LB: 

    /** 		if not found then*/
    if (_found_48185 != 0)
    goto L11; // [383] 429

    /** 			just_mark_everything_from = TopLevelSub*/
    _52just_mark_everything_from_48006 = _25TopLevelSub_12269;

    /** 			recheck_targets &= s*/
    Append(&_52recheck_targets_48078, _52recheck_targets_48078, _s_48156);

    /** 			if not find( current_file_no, recheck_files ) then*/
    _25524 = find_from(_25current_file_no_12262, _52recheck_files_48079, 1);
    if (_25524 != 0)
    goto L12; // [414] 428
    _25524 = NOVALUE;

    /** 				recheck_files &= current_file_no*/
    Append(&_52recheck_files_48079, _52recheck_files_48079, _25current_file_no_12262);
L12: 
L11: 

    /** 		return found*/
    DeRef(_sname_48160);
    DeRef(_string_48161);
    DeRef(_25488);
    _25488 = NOVALUE;
    DeRef(_25475);
    _25475 = NOVALUE;
    DeRef(_25479);
    _25479 = NOVALUE;
    DeRef(_25503);
    _25503 = NOVALUE;
    DeRef(_25494);
    _25494 = NOVALUE;
    DeRef(_25510);
    _25510 = NOVALUE;
    return _found_48185;
    goto L13; // [437] 469
L2: 

    /** 		if not just_mark_everything_from then*/
    if (_52just_mark_everything_from_48006 != 0)
    goto L14; // [444] 457

    /** 			just_mark_everything_from = TopLevelSub*/
    _52just_mark_everything_from_48006 = _25TopLevelSub_12269;
L14: 

    /** 		mark_all( attribute )*/
    _52mark_all(_attribute_48157);

    /** 		return 1*/
    DeRef(_sname_48160);
    DeRef(_string_48161);
    DeRef(_25488);
    _25488 = NOVALUE;
    DeRef(_25475);
    _25475 = NOVALUE;
    DeRef(_25479);
    _25479 = NOVALUE;
    DeRef(_25503);
    _25503 = NOVALUE;
    DeRef(_25494);
    _25494 = NOVALUE;
    DeRef(_25510);
    _25510 = NOVALUE;
    return 1;
L13: 
    ;
}


void _52resolve_unincluded_globals(int _ok_48270)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_ok_48270)) {
        _1 = (long)(DBL_PTR(_ok_48270)->dbl);
        if (UNIQUE(DBL_PTR(_ok_48270)) && (DBL_PTR(_ok_48270)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ok_48270);
        _ok_48270 = _1;
    }

    /** 	Resolve_unincluded_globals = ok*/
    _52Resolve_unincluded_globals_48267 = _ok_48270;

    /** end procedure*/
    return;
    ;
}


int _52get_resolve_unincluded_globals()
{
    int _0, _1, _2;
    

    /** 	return Resolve_unincluded_globals*/
    return _52Resolve_unincluded_globals_48267;
    ;
}


int _52keyfind(int _word_48276, int _file_no_48277, int _scanning_file_48278, int _namespace_ok_48281, int _hashval_48282)
{
    int _msg_48284 = NOVALUE;
    int _b_name_48285 = NOVALUE;
    int _scope_48286 = NOVALUE;
    int _defined_48287 = NOVALUE;
    int _ix_48288 = NOVALUE;
    int _st_ptr_48290 = NOVALUE;
    int _st_builtin_48291 = NOVALUE;
    int _tok_48293 = NOVALUE;
    int _gtok_48294 = NOVALUE;
    int _any_symbol_48299 = NOVALUE;
    int _tok_file_48467 = NOVALUE;
    int _good_48474 = NOVALUE;
    int _include_type_48484 = NOVALUE;
    int _msg_file_48540 = NOVALUE;
    int _25721 = NOVALUE;
    int _25720 = NOVALUE;
    int _25718 = NOVALUE;
    int _25716 = NOVALUE;
    int _25715 = NOVALUE;
    int _25714 = NOVALUE;
    int _25713 = NOVALUE;
    int _25712 = NOVALUE;
    int _25710 = NOVALUE;
    int _25708 = NOVALUE;
    int _25707 = NOVALUE;
    int _25706 = NOVALUE;
    int _25705 = NOVALUE;
    int _25704 = NOVALUE;
    int _25703 = NOVALUE;
    int _25702 = NOVALUE;
    int _25701 = NOVALUE;
    int _25699 = NOVALUE;
    int _25698 = NOVALUE;
    int _25697 = NOVALUE;
    int _25696 = NOVALUE;
    int _25695 = NOVALUE;
    int _25694 = NOVALUE;
    int _25693 = NOVALUE;
    int _25692 = NOVALUE;
    int _25691 = NOVALUE;
    int _25690 = NOVALUE;
    int _25689 = NOVALUE;
    int _25688 = NOVALUE;
    int _25687 = NOVALUE;
    int _25686 = NOVALUE;
    int _25685 = NOVALUE;
    int _25684 = NOVALUE;
    int _25683 = NOVALUE;
    int _25681 = NOVALUE;
    int _25680 = NOVALUE;
    int _25677 = NOVALUE;
    int _25673 = NOVALUE;
    int _25671 = NOVALUE;
    int _25670 = NOVALUE;
    int _25669 = NOVALUE;
    int _25668 = NOVALUE;
    int _25667 = NOVALUE;
    int _25665 = NOVALUE;
    int _25664 = NOVALUE;
    int _25663 = NOVALUE;
    int _25662 = NOVALUE;
    int _25660 = NOVALUE;
    int _25657 = NOVALUE;
    int _25656 = NOVALUE;
    int _25655 = NOVALUE;
    int _25654 = NOVALUE;
    int _25652 = NOVALUE;
    int _25649 = NOVALUE;
    int _25648 = NOVALUE;
    int _25647 = NOVALUE;
    int _25646 = NOVALUE;
    int _25645 = NOVALUE;
    int _25644 = NOVALUE;
    int _25643 = NOVALUE;
    int _25640 = NOVALUE;
    int _25639 = NOVALUE;
    int _25637 = NOVALUE;
    int _25635 = NOVALUE;
    int _25633 = NOVALUE;
    int _25632 = NOVALUE;
    int _25631 = NOVALUE;
    int _25627 = NOVALUE;
    int _25626 = NOVALUE;
    int _25621 = NOVALUE;
    int _25619 = NOVALUE;
    int _25617 = NOVALUE;
    int _25616 = NOVALUE;
    int _25612 = NOVALUE;
    int _25611 = NOVALUE;
    int _25609 = NOVALUE;
    int _25608 = NOVALUE;
    int _25606 = NOVALUE;
    int _25605 = NOVALUE;
    int _25604 = NOVALUE;
    int _25603 = NOVALUE;
    int _25602 = NOVALUE;
    int _25600 = NOVALUE;
    int _25599 = NOVALUE;
    int _25598 = NOVALUE;
    int _25597 = NOVALUE;
    int _25596 = NOVALUE;
    int _25595 = NOVALUE;
    int _25594 = NOVALUE;
    int _25593 = NOVALUE;
    int _25592 = NOVALUE;
    int _25591 = NOVALUE;
    int _25590 = NOVALUE;
    int _25589 = NOVALUE;
    int _25588 = NOVALUE;
    int _25587 = NOVALUE;
    int _25586 = NOVALUE;
    int _25585 = NOVALUE;
    int _25584 = NOVALUE;
    int _25583 = NOVALUE;
    int _25582 = NOVALUE;
    int _25581 = NOVALUE;
    int _25580 = NOVALUE;
    int _25579 = NOVALUE;
    int _25577 = NOVALUE;
    int _25576 = NOVALUE;
    int _25574 = NOVALUE;
    int _25573 = NOVALUE;
    int _25572 = NOVALUE;
    int _25571 = NOVALUE;
    int _25570 = NOVALUE;
    int _25568 = NOVALUE;
    int _25567 = NOVALUE;
    int _25566 = NOVALUE;
    int _25564 = NOVALUE;
    int _25563 = NOVALUE;
    int _25562 = NOVALUE;
    int _25561 = NOVALUE;
    int _25560 = NOVALUE;
    int _25559 = NOVALUE;
    int _25558 = NOVALUE;
    int _25556 = NOVALUE;
    int _25555 = NOVALUE;
    int _25550 = NOVALUE;
    int _25547 = NOVALUE;
    int _25546 = NOVALUE;
    int _25545 = NOVALUE;
    int _25544 = NOVALUE;
    int _25543 = NOVALUE;
    int _25542 = NOVALUE;
    int _25541 = NOVALUE;
    int _25540 = NOVALUE;
    int _25539 = NOVALUE;
    int _25538 = NOVALUE;
    int _25537 = NOVALUE;
    int _25536 = NOVALUE;
    int _25535 = NOVALUE;
    int _25534 = NOVALUE;
    int _25533 = NOVALUE;
    int _25530 = NOVALUE;
    int _25529 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_file_no_48277)) {
        _1 = (long)(DBL_PTR(_file_no_48277)->dbl);
        if (UNIQUE(DBL_PTR(_file_no_48277)) && (DBL_PTR(_file_no_48277)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_file_no_48277);
        _file_no_48277 = _1;
    }
    if (!IS_ATOM_INT(_scanning_file_48278)) {
        _1 = (long)(DBL_PTR(_scanning_file_48278)->dbl);
        if (UNIQUE(DBL_PTR(_scanning_file_48278)) && (DBL_PTR(_scanning_file_48278)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_scanning_file_48278);
        _scanning_file_48278 = _1;
    }
    if (!IS_ATOM_INT(_namespace_ok_48281)) {
        _1 = (long)(DBL_PTR(_namespace_ok_48281)->dbl);
        if (UNIQUE(DBL_PTR(_namespace_ok_48281)) && (DBL_PTR(_namespace_ok_48281)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_namespace_ok_48281);
        _namespace_ok_48281 = _1;
    }
    if (!IS_ATOM_INT(_hashval_48282)) {
        _1 = (long)(DBL_PTR(_hashval_48282)->dbl);
        if (UNIQUE(DBL_PTR(_hashval_48282)) && (DBL_PTR(_hashval_48282)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_hashval_48282);
        _hashval_48282 = _1;
    }

    /** 	dup_globals = {}*/
    RefDS(_22682);
    DeRef(_52dup_globals_48262);
    _52dup_globals_48262 = _22682;

    /** 	dup_overrides = {}*/
    RefDS(_22682);
    DeRefi(_52dup_overrides_48263);
    _52dup_overrides_48263 = _22682;

    /** 	in_include_path = {}*/
    RefDS(_22682);
    DeRef(_52in_include_path_48264);
    _52in_include_path_48264 = _22682;

    /** 	symbol_resolution_warning = ""*/
    RefDS(_22682);
    DeRef(_25symbol_resolution_warning_12382);
    _25symbol_resolution_warning_12382 = _22682;

    /** 	st_builtin = 0*/
    _st_builtin_48291 = 0;

    /** 	ifdef EUDIS then*/

    /** 		bucket_hits[hashval] += 1*/
    _2 = (int)SEQ_PTR(_52bucket_hits_47106);
    _25529 = (int)*(((s1_ptr)_2)->base + _hashval_48282);
    if (IS_ATOM_INT(_25529)) {
        _25530 = _25529 + 1;
        if (_25530 > MAXINT){
            _25530 = NewDouble((double)_25530);
        }
    }
    else
    _25530 = binary_op(PLUS, 1, _25529);
    _25529 = NOVALUE;
    _2 = (int)SEQ_PTR(_52bucket_hits_47106);
    _2 = (int)(((s1_ptr)_2)->base + _hashval_48282);
    _1 = *(int *)_2;
    *(int *)_2 = _25530;
    if( _1 != _25530 ){
        DeRef(_1);
    }
    _25530 = NOVALUE;

    /** 	st_ptr = buckets[hashval]*/
    _2 = (int)SEQ_PTR(_52buckets_47095);
    _st_ptr_48290 = (int)*(((s1_ptr)_2)->base + _hashval_48282);
    if (!IS_ATOM_INT(_st_ptr_48290)){
        _st_ptr_48290 = (long)DBL_PTR(_st_ptr_48290)->dbl;
    }

    /** 	integer any_symbol = namespace_ok = -1*/
    _any_symbol_48299 = (_namespace_ok_48281 == -1);

    /** 	while st_ptr do*/
L1: 
    if (_st_ptr_48290 == 0)
    {
        goto L2; // [85] 1049
    }
    else{
    }

    /** 		if SymTab[st_ptr][S_SCOPE] != SC_UNDEFINED */
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25533 = (int)*(((s1_ptr)_2)->base + _st_ptr_48290);
    _2 = (int)SEQ_PTR(_25533);
    _25534 = (int)*(((s1_ptr)_2)->base + 4);
    _25533 = NOVALUE;
    if (IS_ATOM_INT(_25534)) {
        _25535 = (_25534 != 9);
    }
    else {
        _25535 = binary_op(NOTEQ, _25534, 9);
    }
    _25534 = NOVALUE;
    if (IS_ATOM_INT(_25535)) {
        if (_25535 == 0) {
            DeRef(_25536);
            _25536 = 0;
            goto L3; // [108] 132
        }
    }
    else {
        if (DBL_PTR(_25535)->dbl == 0.0) {
            DeRef(_25536);
            _25536 = 0;
            goto L3; // [108] 132
        }
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25537 = (int)*(((s1_ptr)_2)->base + _st_ptr_48290);
    _2 = (int)SEQ_PTR(_25537);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _25538 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _25538 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _25537 = NOVALUE;
    if (_word_48276 == _25538)
    _25539 = 1;
    else if (IS_ATOM_INT(_word_48276) && IS_ATOM_INT(_25538))
    _25539 = 0;
    else
    _25539 = (compare(_word_48276, _25538) == 0);
    _25538 = NOVALUE;
    DeRef(_25536);
    _25536 = (_25539 != 0);
L3: 
    if (_25536 == 0) {
        goto L4; // [132] 1028
    }
    if (_any_symbol_48299 != 0) {
        DeRef(_25541);
        _25541 = 1;
        goto L5; // [136] 166
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25542 = (int)*(((s1_ptr)_2)->base + _st_ptr_48290);
    _2 = (int)SEQ_PTR(_25542);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _25543 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _25543 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _25542 = NOVALUE;
    if (IS_ATOM_INT(_25543)) {
        _25544 = (_25543 == 523);
    }
    else {
        _25544 = binary_op(EQUALS, _25543, 523);
    }
    _25543 = NOVALUE;
    if (IS_ATOM_INT(_25544)) {
        _25545 = (_namespace_ok_48281 == _25544);
    }
    else {
        _25545 = binary_op(EQUALS, _namespace_ok_48281, _25544);
    }
    DeRef(_25544);
    _25544 = NOVALUE;
    if (IS_ATOM_INT(_25545))
    _25541 = (_25545 != 0);
    else
    _25541 = DBL_PTR(_25545)->dbl != 0.0;
L5: 
    if (_25541 == 0)
    {
        _25541 = NOVALUE;
        goto L4; // [167] 1028
    }
    else{
        _25541 = NOVALUE;
    }

    /** 			tok = {SymTab[st_ptr][S_TOKEN], st_ptr}*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25546 = (int)*(((s1_ptr)_2)->base + _st_ptr_48290);
    _2 = (int)SEQ_PTR(_25546);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _25547 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _25547 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _25546 = NOVALUE;
    Ref(_25547);
    DeRef(_tok_48293);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _25547;
    ((int *)_2)[2] = _st_ptr_48290;
    _tok_48293 = MAKE_SEQ(_1);
    _25547 = NOVALUE;

    /** 			if file_no = -1 then*/
    if (_file_no_48277 != -1)
    goto L6; // [190] 730

    /** 				scope = SymTab[st_ptr][S_SCOPE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25550 = (int)*(((s1_ptr)_2)->base + _st_ptr_48290);
    _2 = (int)SEQ_PTR(_25550);
    _scope_48286 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_scope_48286)){
        _scope_48286 = (long)DBL_PTR(_scope_48286)->dbl;
    }
    _25550 = NOVALUE;

    /** 				switch scope with fallthru do*/
    _0 = _scope_48286;
    switch ( _0 ){ 

        /** 				case SC_OVERRIDE then*/
        case 12:

        /** 					dup_overrides &= st_ptr*/
        Append(&_52dup_overrides_48263, _52dup_overrides_48263, _st_ptr_48290);

        /** 					break*/
        goto L7; // [231] 1027

        /** 				case SC_PREDEF then*/
        case 7:

        /** 					st_builtin = st_ptr*/
        _st_builtin_48291 = _st_ptr_48290;

        /** 					break*/
        goto L7; // [246] 1027

        /** 				case SC_GLOBAL then*/
        case 6:

        /** 					if scanning_file = SymTab[st_ptr][S_FILE_NO] then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _25555 = (int)*(((s1_ptr)_2)->base + _st_ptr_48290);
        _2 = (int)SEQ_PTR(_25555);
        if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
            _25556 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
        }
        else{
            _25556 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
        }
        _25555 = NOVALUE;
        if (binary_op_a(NOTEQ, _scanning_file_48278, _25556)){
            _25556 = NOVALUE;
            goto L8; // [266] 290
        }
        _25556 = NOVALUE;

        /** 						if BIND then*/
        if (_25BIND_11877 == 0)
        {
            goto L9; // [274] 283
        }
        else{
        }

        /** 							add_ref(tok)*/
        Ref(_tok_48293);
        _52add_ref(_tok_48293);
L9: 

        /** 						return tok*/
        DeRefDS(_word_48276);
        DeRef(_msg_48284);
        DeRef(_b_name_48285);
        DeRef(_gtok_48294);
        DeRef(_25545);
        _25545 = NOVALUE;
        DeRef(_25535);
        _25535 = NOVALUE;
        return _tok_48293;
L8: 

        /** 					if Resolve_unincluded_globals */
        if (_52Resolve_unincluded_globals_48267 != 0) {
            _25558 = 1;
            goto LA; // [294] 338
        }
        _2 = (int)SEQ_PTR(_26finished_files_11141);
        _25559 = (int)*(((s1_ptr)_2)->base + _scanning_file_48278);
        if (_25559 == 0) {
            _25560 = 0;
            goto LB; // [304] 334
        }
        _2 = (int)SEQ_PTR(_26include_matrix_11145);
        _25561 = (int)*(((s1_ptr)_2)->base + _scanning_file_48278);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _25562 = (int)*(((s1_ptr)_2)->base + _st_ptr_48290);
        _2 = (int)SEQ_PTR(_25562);
        if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
            _25563 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
        }
        else{
            _25563 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
        }
        _25562 = NOVALUE;
        _2 = (int)SEQ_PTR(_25561);
        if (!IS_ATOM_INT(_25563)){
            _25564 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25563)->dbl));
        }
        else{
            _25564 = (int)*(((s1_ptr)_2)->base + _25563);
        }
        _25561 = NOVALUE;
        if (IS_ATOM_INT(_25564))
        _25560 = (_25564 != 0);
        else
        _25560 = DBL_PTR(_25564)->dbl != 0.0;
LB: 
        _25558 = (_25560 != 0);
LA: 
        if (_25558 != 0) {
            goto LC; // [338] 365
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _25566 = (int)*(((s1_ptr)_2)->base + _st_ptr_48290);
        _2 = (int)SEQ_PTR(_25566);
        if (!IS_ATOM_INT(_25S_TOKEN_11918)){
            _25567 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
        }
        else{
            _25567 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
        }
        _25566 = NOVALUE;
        if (IS_ATOM_INT(_25567)) {
            _25568 = (_25567 == 523);
        }
        else {
            _25568 = binary_op(EQUALS, _25567, 523);
        }
        _25567 = NOVALUE;
        if (_25568 == 0) {
            DeRef(_25568);
            _25568 = NOVALUE;
            goto L7; // [361] 1027
        }
        else {
            if (!IS_ATOM_INT(_25568) && DBL_PTR(_25568)->dbl == 0.0){
                DeRef(_25568);
                _25568 = NOVALUE;
                goto L7; // [361] 1027
            }
            DeRef(_25568);
            _25568 = NOVALUE;
        }
        DeRef(_25568);
        _25568 = NOVALUE;
LC: 

        /** 						gtok = tok*/
        Ref(_tok_48293);
        DeRef(_gtok_48294);
        _gtok_48294 = _tok_48293;

        /** 						dup_globals &= st_ptr*/
        Append(&_52dup_globals_48262, _52dup_globals_48262, _st_ptr_48290);

        /** 						in_include_path &= include_matrix[scanning_file][SymTab[st_ptr][S_FILE_NO]] != 0*/
        _2 = (int)SEQ_PTR(_26include_matrix_11145);
        _25570 = (int)*(((s1_ptr)_2)->base + _scanning_file_48278);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _25571 = (int)*(((s1_ptr)_2)->base + _st_ptr_48290);
        _2 = (int)SEQ_PTR(_25571);
        if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
            _25572 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
        }
        else{
            _25572 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
        }
        _25571 = NOVALUE;
        _2 = (int)SEQ_PTR(_25570);
        if (!IS_ATOM_INT(_25572)){
            _25573 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25572)->dbl));
        }
        else{
            _25573 = (int)*(((s1_ptr)_2)->base + _25572);
        }
        _25570 = NOVALUE;
        if (IS_ATOM_INT(_25573)) {
            _25574 = (_25573 != 0);
        }
        else {
            _25574 = binary_op(NOTEQ, _25573, 0);
        }
        _25573 = NOVALUE;
        if (IS_SEQUENCE(_52in_include_path_48264) && IS_ATOM(_25574)) {
            Ref(_25574);
            Append(&_52in_include_path_48264, _52in_include_path_48264, _25574);
        }
        else if (IS_ATOM(_52in_include_path_48264) && IS_SEQUENCE(_25574)) {
        }
        else {
            Concat((object_ptr)&_52in_include_path_48264, _52in_include_path_48264, _25574);
        }
        DeRef(_25574);
        _25574 = NOVALUE;

        /** 					break*/
        goto L7; // [415] 1027

        /** 				case SC_PUBLIC, SC_EXPORT then*/
        case 13:
        case 11:

        /** 					if scanning_file = SymTab[st_ptr][S_FILE_NO] then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _25576 = (int)*(((s1_ptr)_2)->base + _st_ptr_48290);
        _2 = (int)SEQ_PTR(_25576);
        if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
            _25577 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
        }
        else{
            _25577 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
        }
        _25576 = NOVALUE;
        if (binary_op_a(NOTEQ, _scanning_file_48278, _25577)){
            _25577 = NOVALUE;
            goto LD; // [437] 461
        }
        _25577 = NOVALUE;

        /** 						if BIND then*/
        if (_25BIND_11877 == 0)
        {
            goto LE; // [445] 454
        }
        else{
        }

        /** 							add_ref(tok)*/
        Ref(_tok_48293);
        _52add_ref(_tok_48293);
LE: 

        /** 						return tok*/
        DeRefDS(_word_48276);
        DeRef(_msg_48284);
        DeRef(_b_name_48285);
        DeRef(_gtok_48294);
        DeRef(_25545);
        _25545 = NOVALUE;
        DeRef(_25535);
        _25535 = NOVALUE;
        _25559 = NOVALUE;
        _25564 = NOVALUE;
        _25563 = NOVALUE;
        _25572 = NOVALUE;
        return _tok_48293;
LD: 

        /** 					if (finished_files[scanning_file] -- everything this file needs has been read in*/
        _2 = (int)SEQ_PTR(_26finished_files_11141);
        _25579 = (int)*(((s1_ptr)_2)->base + _scanning_file_48278);
        if (_25579 != 0) {
            _25580 = 1;
            goto LF; // [469] 503
        }
        if (_namespace_ok_48281 == 0) {
            _25581 = 0;
            goto L10; // [473] 499
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _25582 = (int)*(((s1_ptr)_2)->base + _st_ptr_48290);
        _2 = (int)SEQ_PTR(_25582);
        if (!IS_ATOM_INT(_25S_TOKEN_11918)){
            _25583 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
        }
        else{
            _25583 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
        }
        _25582 = NOVALUE;
        if (IS_ATOM_INT(_25583)) {
            _25584 = (_25583 == 523);
        }
        else {
            _25584 = binary_op(EQUALS, _25583, 523);
        }
        _25583 = NOVALUE;
        if (IS_ATOM_INT(_25584))
        _25581 = (_25584 != 0);
        else
        _25581 = DBL_PTR(_25584)->dbl != 0.0;
L10: 
        _25580 = (_25581 != 0);
LF: 
        if (_25580 == 0) {
            goto L7; // [503] 1027
        }
        _25586 = (_scope_48286 == 13);
        if (_25586 == 0) {
            _25587 = 0;
            goto L11; // [513] 549
        }
        _2 = (int)SEQ_PTR(_26include_matrix_11145);
        _25588 = (int)*(((s1_ptr)_2)->base + _scanning_file_48278);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _25589 = (int)*(((s1_ptr)_2)->base + _st_ptr_48290);
        _2 = (int)SEQ_PTR(_25589);
        if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
            _25590 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
        }
        else{
            _25590 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
        }
        _25589 = NOVALUE;
        _2 = (int)SEQ_PTR(_25588);
        if (!IS_ATOM_INT(_25590)){
            _25591 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25590)->dbl));
        }
        else{
            _25591 = (int)*(((s1_ptr)_2)->base + _25590);
        }
        _25588 = NOVALUE;
        if (IS_ATOM_INT(_25591)) {
            {unsigned long tu;
                 tu = (unsigned long)6 & (unsigned long)_25591;
                 _25592 = MAKE_UINT(tu);
            }
        }
        else {
            _25592 = binary_op(AND_BITS, 6, _25591);
        }
        _25591 = NOVALUE;
        if (IS_ATOM_INT(_25592))
        _25587 = (_25592 != 0);
        else
        _25587 = DBL_PTR(_25592)->dbl != 0.0;
L11: 
        if (_25587 != 0) {
            DeRef(_25593);
            _25593 = 1;
            goto L12; // [549] 599
        }
        _25594 = (_scope_48286 == 11);
        if (_25594 == 0) {
            _25595 = 0;
            goto L13; // [559] 595
        }
        _2 = (int)SEQ_PTR(_26include_matrix_11145);
        _25596 = (int)*(((s1_ptr)_2)->base + _scanning_file_48278);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _25597 = (int)*(((s1_ptr)_2)->base + _st_ptr_48290);
        _2 = (int)SEQ_PTR(_25597);
        if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
            _25598 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
        }
        else{
            _25598 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
        }
        _25597 = NOVALUE;
        _2 = (int)SEQ_PTR(_25596);
        if (!IS_ATOM_INT(_25598)){
            _25599 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25598)->dbl));
        }
        else{
            _25599 = (int)*(((s1_ptr)_2)->base + _25598);
        }
        _25596 = NOVALUE;
        if (IS_ATOM_INT(_25599)) {
            {unsigned long tu;
                 tu = (unsigned long)2 & (unsigned long)_25599;
                 _25600 = MAKE_UINT(tu);
            }
        }
        else {
            _25600 = binary_op(AND_BITS, 2, _25599);
        }
        _25599 = NOVALUE;
        if (IS_ATOM_INT(_25600))
        _25595 = (_25600 != 0);
        else
        _25595 = DBL_PTR(_25600)->dbl != 0.0;
L13: 
        DeRef(_25593);
        _25593 = (_25595 != 0);
L12: 
        if (_25593 == 0)
        {
            _25593 = NOVALUE;
            goto L7; // [600] 1027
        }
        else{
            _25593 = NOVALUE;
        }

        /** 						gtok = tok*/
        Ref(_tok_48293);
        DeRef(_gtok_48294);
        _gtok_48294 = _tok_48293;

        /** 						dup_globals &= st_ptr*/
        Append(&_52dup_globals_48262, _52dup_globals_48262, _st_ptr_48290);

        /** 						in_include_path &= include_matrix[scanning_file][SymTab[st_ptr][S_FILE_NO]] != 0 --symbol_in_include_path( st_ptr, scanning_file, {} )*/
        _2 = (int)SEQ_PTR(_26include_matrix_11145);
        _25602 = (int)*(((s1_ptr)_2)->base + _scanning_file_48278);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _25603 = (int)*(((s1_ptr)_2)->base + _st_ptr_48290);
        _2 = (int)SEQ_PTR(_25603);
        if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
            _25604 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
        }
        else{
            _25604 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
        }
        _25603 = NOVALUE;
        _2 = (int)SEQ_PTR(_25602);
        if (!IS_ATOM_INT(_25604)){
            _25605 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25604)->dbl));
        }
        else{
            _25605 = (int)*(((s1_ptr)_2)->base + _25604);
        }
        _25602 = NOVALUE;
        if (IS_ATOM_INT(_25605)) {
            _25606 = (_25605 != 0);
        }
        else {
            _25606 = binary_op(NOTEQ, _25605, 0);
        }
        _25605 = NOVALUE;
        if (IS_SEQUENCE(_52in_include_path_48264) && IS_ATOM(_25606)) {
            Ref(_25606);
            Append(&_52in_include_path_48264, _52in_include_path_48264, _25606);
        }
        else if (IS_ATOM(_52in_include_path_48264) && IS_SEQUENCE(_25606)) {
        }
        else {
            Concat((object_ptr)&_52in_include_path_48264, _52in_include_path_48264, _25606);
        }
        DeRef(_25606);
        _25606 = NOVALUE;

        /** ifdef STDDEBUG then*/

        /** 					break*/
        goto L7; // [655] 1027

        /** 				case SC_LOCAL then*/
        case 5:

        /** 					if scanning_file = SymTab[st_ptr][S_FILE_NO] then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _25608 = (int)*(((s1_ptr)_2)->base + _st_ptr_48290);
        _2 = (int)SEQ_PTR(_25608);
        if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
            _25609 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
        }
        else{
            _25609 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
        }
        _25608 = NOVALUE;
        if (binary_op_a(NOTEQ, _scanning_file_48278, _25609)){
            _25609 = NOVALUE;
            goto L7; // [675] 1027
        }
        _25609 = NOVALUE;

        /** 						if BIND then*/
        if (_25BIND_11877 == 0)
        {
            goto L14; // [683] 692
        }
        else{
        }

        /** 							add_ref(tok)*/
        Ref(_tok_48293);
        _52add_ref(_tok_48293);
L14: 

        /** 						return tok*/
        DeRefDS(_word_48276);
        DeRef(_msg_48284);
        DeRef(_b_name_48285);
        DeRef(_gtok_48294);
        DeRef(_25545);
        _25545 = NOVALUE;
        DeRef(_25535);
        _25535 = NOVALUE;
        _25559 = NOVALUE;
        _25564 = NOVALUE;
        _25563 = NOVALUE;
        _25579 = NOVALUE;
        _25572 = NOVALUE;
        DeRef(_25586);
        _25586 = NOVALUE;
        DeRef(_25584);
        _25584 = NOVALUE;
        DeRef(_25594);
        _25594 = NOVALUE;
        _25590 = NOVALUE;
        DeRef(_25592);
        _25592 = NOVALUE;
        _25598 = NOVALUE;
        DeRef(_25600);
        _25600 = NOVALUE;
        _25604 = NOVALUE;
        return _tok_48293;

        /** 					break*/
        goto L7; // [701] 1027

        /** 				case else*/
        default:

        /** 					if BIND then*/
        if (_25BIND_11877 == 0)
        {
            goto L15; // [711] 720
        }
        else{
        }

        /** 						add_ref(tok)*/
        Ref(_tok_48293);
        _52add_ref(_tok_48293);
L15: 

        /** 					return tok -- keyword, private*/
        DeRefDS(_word_48276);
        DeRef(_msg_48284);
        DeRef(_b_name_48285);
        DeRef(_gtok_48294);
        DeRef(_25545);
        _25545 = NOVALUE;
        DeRef(_25535);
        _25535 = NOVALUE;
        _25559 = NOVALUE;
        _25564 = NOVALUE;
        _25563 = NOVALUE;
        _25579 = NOVALUE;
        _25572 = NOVALUE;
        DeRef(_25586);
        _25586 = NOVALUE;
        DeRef(_25584);
        _25584 = NOVALUE;
        DeRef(_25594);
        _25594 = NOVALUE;
        _25590 = NOVALUE;
        DeRef(_25592);
        _25592 = NOVALUE;
        _25598 = NOVALUE;
        DeRef(_25600);
        _25600 = NOVALUE;
        _25604 = NOVALUE;
        return _tok_48293;
    ;}    goto L7; // [727] 1027
L6: 

    /** 				scope = SymTab[tok[T_SYM]][S_SCOPE]*/
    _2 = (int)SEQ_PTR(_tok_48293);
    _25611 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_25611)){
        _25612 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25611)->dbl));
    }
    else{
        _25612 = (int)*(((s1_ptr)_2)->base + _25611);
    }
    _2 = (int)SEQ_PTR(_25612);
    _scope_48286 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_scope_48286)){
        _scope_48286 = (long)DBL_PTR(_scope_48286)->dbl;
    }
    _25612 = NOVALUE;

    /** 				if not file_no then*/
    if (_file_no_48277 != 0)
    goto L16; // [754] 788

    /** 					if scope = SC_PREDEF then*/
    if (_scope_48286 != 7)
    goto L17; // [761] 1026

    /** 						if BIND then*/
    if (_25BIND_11877 == 0)
    {
        goto L18; // [769] 778
    }
    else{
    }

    /** 							add_ref( tok )*/
    Ref(_tok_48293);
    _52add_ref(_tok_48293);
L18: 

    /** 						return tok*/
    DeRefDS(_word_48276);
    DeRef(_msg_48284);
    DeRef(_b_name_48285);
    DeRef(_gtok_48294);
    DeRef(_25545);
    _25545 = NOVALUE;
    DeRef(_25535);
    _25535 = NOVALUE;
    _25559 = NOVALUE;
    _25564 = NOVALUE;
    _25563 = NOVALUE;
    _25579 = NOVALUE;
    _25572 = NOVALUE;
    DeRef(_25586);
    _25586 = NOVALUE;
    DeRef(_25584);
    _25584 = NOVALUE;
    DeRef(_25594);
    _25594 = NOVALUE;
    _25590 = NOVALUE;
    DeRef(_25592);
    _25592 = NOVALUE;
    _25611 = NOVALUE;
    _25598 = NOVALUE;
    DeRef(_25600);
    _25600 = NOVALUE;
    _25604 = NOVALUE;
    return _tok_48293;
    goto L17; // [785] 1026
L16: 

    /** 					integer tok_file = SymTab[tok[T_SYM]][S_FILE_NO]*/
    _2 = (int)SEQ_PTR(_tok_48293);
    _25616 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_25616)){
        _25617 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25616)->dbl));
    }
    else{
        _25617 = (int)*(((s1_ptr)_2)->base + _25616);
    }
    _2 = (int)SEQ_PTR(_25617);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _tok_file_48467 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _tok_file_48467 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    if (!IS_ATOM_INT(_tok_file_48467)){
        _tok_file_48467 = (long)DBL_PTR(_tok_file_48467)->dbl;
    }
    _25617 = NOVALUE;

    /** 					integer good = 0*/
    _good_48474 = 0;

    /** 					if scope = SC_PRIVATE or scope = SC_PREDEF then*/
    _25619 = (_scope_48286 == 3);
    if (_25619 != 0) {
        goto L19; // [823] 956
    }
    _25621 = (_scope_48286 == 7);
    if (_25621 == 0)
    {
        DeRef(_25621);
        _25621 = NOVALUE;
        goto L1A; // [834] 841
    }
    else{
        DeRef(_25621);
        _25621 = NOVALUE;
    }
    goto L19; // [838] 956
L1A: 

    /** 					elsif file_no = tok_file then*/
    if (_file_no_48277 != _tok_file_48467)
    goto L1B; // [843] 855

    /** 						good = 1*/
    _good_48474 = 1;
    goto L19; // [852] 956
L1B: 

    /** 						integer include_type = 0*/
    _include_type_48484 = 0;

    /** 						switch scope do*/
    _0 = _scope_48286;
    switch ( _0 ){ 

        /** 							case SC_GLOBAL then*/
        case 6:

        /** 								if Resolve_unincluded_globals then*/
        if (_52Resolve_unincluded_globals_48267 == 0)
        {
            goto L1C; // [875] 890
        }
        else{
        }

        /** 									include_type = ANY_INCLUDE*/
        _include_type_48484 = 7;
        goto L1D; // [887] 935
L1C: 

        /** 									include_type = DIRECT_OR_PUBLIC_INCLUDE*/
        _include_type_48484 = 6;
        goto L1D; // [900] 935

        /** 							case SC_PUBLIC then*/
        case 13:

        /** 								if tok_file != file_no then*/
        if (_tok_file_48467 == _file_no_48277)
        goto L1E; // [908] 924

        /** 									include_type = PUBLIC_INCLUDE*/
        _include_type_48484 = 4;
        goto L1F; // [921] 934
L1E: 

        /** 									include_type = DIRECT_OR_PUBLIC_INCLUDE*/
        _include_type_48484 = 6;
L1F: 
    ;}L1D: 

    /** 						good = and_bits( include_type, include_matrix[file_no][tok_file] )*/
    _2 = (int)SEQ_PTR(_26include_matrix_11145);
    _25626 = (int)*(((s1_ptr)_2)->base + _file_no_48277);
    _2 = (int)SEQ_PTR(_25626);
    _25627 = (int)*(((s1_ptr)_2)->base + _tok_file_48467);
    _25626 = NOVALUE;
    if (IS_ATOM_INT(_25627)) {
        {unsigned long tu;
             tu = (unsigned long)_include_type_48484 & (unsigned long)_25627;
             _good_48474 = MAKE_UINT(tu);
        }
    }
    else {
        _good_48474 = binary_op(AND_BITS, _include_type_48484, _25627);
    }
    _25627 = NOVALUE;
    if (!IS_ATOM_INT(_good_48474)) {
        _1 = (long)(DBL_PTR(_good_48474)->dbl);
        if (UNIQUE(DBL_PTR(_good_48474)) && (DBL_PTR(_good_48474)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_good_48474);
        _good_48474 = _1;
    }
L19: 

    /** 					if good then*/
    if (_good_48474 == 0)
    {
        goto L20; // [958] 1023
    }
    else{
    }

    /** 						if file_no = tok_file then*/
    if (_file_no_48277 != _tok_file_48467)
    goto L21; // [963] 987

    /** 							if BIND then*/
    if (_25BIND_11877 == 0)
    {
        goto L22; // [971] 980
    }
    else{
    }

    /** 								add_ref(tok)*/
    Ref(_tok_48293);
    _52add_ref(_tok_48293);
L22: 

    /** 							return tok*/
    DeRefDS(_word_48276);
    DeRef(_msg_48284);
    DeRef(_b_name_48285);
    DeRef(_gtok_48294);
    DeRef(_25545);
    _25545 = NOVALUE;
    DeRef(_25535);
    _25535 = NOVALUE;
    _25559 = NOVALUE;
    _25564 = NOVALUE;
    _25563 = NOVALUE;
    _25579 = NOVALUE;
    _25572 = NOVALUE;
    DeRef(_25586);
    _25586 = NOVALUE;
    DeRef(_25584);
    _25584 = NOVALUE;
    DeRef(_25594);
    _25594 = NOVALUE;
    _25590 = NOVALUE;
    DeRef(_25592);
    _25592 = NOVALUE;
    _25611 = NOVALUE;
    _25598 = NOVALUE;
    DeRef(_25600);
    _25600 = NOVALUE;
    _25604 = NOVALUE;
    _25616 = NOVALUE;
    DeRef(_25619);
    _25619 = NOVALUE;
    return _tok_48293;
L21: 

    /** 						gtok = tok*/
    Ref(_tok_48293);
    DeRef(_gtok_48294);
    _gtok_48294 = _tok_48293;

    /** 						dup_globals &= st_ptr*/
    Append(&_52dup_globals_48262, _52dup_globals_48262, _st_ptr_48290);

    /** 						in_include_path &= include_matrix[scanning_file][tok_file] != 0*/
    _2 = (int)SEQ_PTR(_26include_matrix_11145);
    _25631 = (int)*(((s1_ptr)_2)->base + _scanning_file_48278);
    _2 = (int)SEQ_PTR(_25631);
    _25632 = (int)*(((s1_ptr)_2)->base + _tok_file_48467);
    _25631 = NOVALUE;
    if (IS_ATOM_INT(_25632)) {
        _25633 = (_25632 != 0);
    }
    else {
        _25633 = binary_op(NOTEQ, _25632, 0);
    }
    _25632 = NOVALUE;
    if (IS_SEQUENCE(_52in_include_path_48264) && IS_ATOM(_25633)) {
        Ref(_25633);
        Append(&_52in_include_path_48264, _52in_include_path_48264, _25633);
    }
    else if (IS_ATOM(_52in_include_path_48264) && IS_SEQUENCE(_25633)) {
    }
    else {
        Concat((object_ptr)&_52in_include_path_48264, _52in_include_path_48264, _25633);
    }
    DeRef(_25633);
    _25633 = NOVALUE;
L20: 
L17: 
L7: 
L4: 

    /** 		st_ptr = SymTab[st_ptr][S_SAMEHASH]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25635 = (int)*(((s1_ptr)_2)->base + _st_ptr_48290);
    _2 = (int)SEQ_PTR(_25635);
    _st_ptr_48290 = (int)*(((s1_ptr)_2)->base + 9);
    if (!IS_ATOM_INT(_st_ptr_48290)){
        _st_ptr_48290 = (long)DBL_PTR(_st_ptr_48290)->dbl;
    }
    _25635 = NOVALUE;

    /** 	end while*/
    goto L1; // [1046] 85
L2: 

    /** 	if length(dup_overrides) then*/
    if (IS_SEQUENCE(_52dup_overrides_48263)){
            _25637 = SEQ_PTR(_52dup_overrides_48263)->length;
    }
    else {
        _25637 = 1;
    }
    if (_25637 == 0)
    {
        _25637 = NOVALUE;
        goto L23; // [1056] 1109
    }
    else{
        _25637 = NOVALUE;
    }

    /** 		st_ptr = dup_overrides[1]*/
    _2 = (int)SEQ_PTR(_52dup_overrides_48263);
    _st_ptr_48290 = (int)*(((s1_ptr)_2)->base + 1);

    /** 		tok = {SymTab[st_ptr][S_TOKEN], st_ptr}*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25639 = (int)*(((s1_ptr)_2)->base + _st_ptr_48290);
    _2 = (int)SEQ_PTR(_25639);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _25640 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _25640 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _25639 = NOVALUE;
    Ref(_25640);
    DeRef(_tok_48293);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _25640;
    ((int *)_2)[2] = _st_ptr_48290;
    _tok_48293 = MAKE_SEQ(_1);
    _25640 = NOVALUE;

    /** 			if BIND then*/
    if (_25BIND_11877 == 0)
    {
        goto L24; // [1091] 1100
    }
    else{
    }

    /** 				add_ref(tok)*/
    RefDS(_tok_48293);
    _52add_ref(_tok_48293);
L24: 

    /** 			return tok*/
    DeRefDS(_word_48276);
    DeRef(_msg_48284);
    DeRef(_b_name_48285);
    DeRef(_gtok_48294);
    DeRef(_25545);
    _25545 = NOVALUE;
    DeRef(_25535);
    _25535 = NOVALUE;
    _25559 = NOVALUE;
    _25564 = NOVALUE;
    _25563 = NOVALUE;
    _25579 = NOVALUE;
    _25572 = NOVALUE;
    DeRef(_25586);
    _25586 = NOVALUE;
    DeRef(_25584);
    _25584 = NOVALUE;
    DeRef(_25594);
    _25594 = NOVALUE;
    _25590 = NOVALUE;
    DeRef(_25592);
    _25592 = NOVALUE;
    _25611 = NOVALUE;
    _25598 = NOVALUE;
    DeRef(_25600);
    _25600 = NOVALUE;
    _25604 = NOVALUE;
    _25616 = NOVALUE;
    DeRef(_25619);
    _25619 = NOVALUE;
    return _tok_48293;
    goto L25; // [1106] 1336
L23: 

    /** 	elsif st_builtin != 0 then*/
    if (_st_builtin_48291 == 0)
    goto L26; // [1111] 1335

    /** 		if length(dup_globals) and find(SymTab[st_builtin][S_NAME], builtin_warnings) = 0 then*/
    if (IS_SEQUENCE(_52dup_globals_48262)){
            _25643 = SEQ_PTR(_52dup_globals_48262)->length;
    }
    else {
        _25643 = 1;
    }
    if (_25643 == 0) {
        goto L27; // [1122] 1295
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25645 = (int)*(((s1_ptr)_2)->base + _st_builtin_48291);
    _2 = (int)SEQ_PTR(_25645);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _25646 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _25646 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _25645 = NOVALUE;
    _25647 = find_from(_25646, _52builtin_warnings_48266, 1);
    _25646 = NOVALUE;
    _25648 = (_25647 == 0);
    _25647 = NOVALUE;
    if (_25648 == 0)
    {
        DeRef(_25648);
        _25648 = NOVALUE;
        goto L27; // [1150] 1295
    }
    else{
        DeRef(_25648);
        _25648 = NOVALUE;
    }

    /** 			sequence msg_file */

    /** 			b_name = SymTab[st_builtin][S_NAME]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25649 = (int)*(((s1_ptr)_2)->base + _st_builtin_48291);
    DeRef(_b_name_48285);
    _2 = (int)SEQ_PTR(_25649);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _b_name_48285 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _b_name_48285 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    Ref(_b_name_48285);
    _25649 = NOVALUE;

    /** 			builtin_warnings = append(builtin_warnings, b_name)*/
    RefDS(_b_name_48285);
    Append(&_52builtin_warnings_48266, _52builtin_warnings_48266, _b_name_48285);

    /** 			if length(dup_globals) > 1 then*/
    if (IS_SEQUENCE(_52dup_globals_48262)){
            _25652 = SEQ_PTR(_52dup_globals_48262)->length;
    }
    else {
        _25652 = 1;
    }
    if (_25652 <= 1)
    goto L28; // [1186] 1200

    /** 				msg = "\n"*/
    RefDS(_22834);
    DeRef(_msg_48284);
    _msg_48284 = _22834;
    goto L29; // [1197] 1208
L28: 

    /** 				msg = ""*/
    RefDS(_22682);
    DeRef(_msg_48284);
    _msg_48284 = _22682;
L29: 

    /** 			for i = 1 to length(dup_globals) do*/
    if (IS_SEQUENCE(_52dup_globals_48262)){
            _25654 = SEQ_PTR(_52dup_globals_48262)->length;
    }
    else {
        _25654 = 1;
    }
    {
        int _i_48551;
        _i_48551 = 1;
L2A: 
        if (_i_48551 > _25654){
            goto L2B; // [1215] 1271
        }

        /** 				msg_file = known_files[SymTab[dup_globals[i]][S_FILE_NO]]*/
        _2 = (int)SEQ_PTR(_52dup_globals_48262);
        _25655 = (int)*(((s1_ptr)_2)->base + _i_48551);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!IS_ATOM_INT(_25655)){
            _25656 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25655)->dbl));
        }
        else{
            _25656 = (int)*(((s1_ptr)_2)->base + _25655);
        }
        _2 = (int)SEQ_PTR(_25656);
        if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
            _25657 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
        }
        else{
            _25657 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
        }
        _25656 = NOVALUE;
        DeRef(_msg_file_48540);
        _2 = (int)SEQ_PTR(_26known_files_11139);
        if (!IS_ATOM_INT(_25657)){
            _msg_file_48540 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25657)->dbl));
        }
        else{
            _msg_file_48540 = (int)*(((s1_ptr)_2)->base + _25657);
        }
        Ref(_msg_file_48540);

        /** 				msg &= "    " & msg_file & "\n"*/
        {
            int concat_list[3];

            concat_list[0] = _22834;
            concat_list[1] = _msg_file_48540;
            concat_list[2] = _25659;
            Concat_N((object_ptr)&_25660, concat_list, 3);
        }
        Concat((object_ptr)&_msg_48284, _msg_48284, _25660);
        DeRefDS(_25660);
        _25660 = NOVALUE;

        /** 			end for*/
        _i_48551 = _i_48551 + 1;
        goto L2A; // [1266] 1222
L2B: 
        ;
    }

    /** 			Warning(234, builtin_chosen_warning_flag, {b_name, known_files[scanning_file], msg})*/
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _25662 = (int)*(((s1_ptr)_2)->base + _scanning_file_48278);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_b_name_48285);
    *((int *)(_2+4)) = _b_name_48285;
    Ref(_25662);
    *((int *)(_2+8)) = _25662;
    RefDS(_msg_48284);
    *((int *)(_2+12)) = _msg_48284;
    _25663 = MAKE_SEQ(_1);
    _25662 = NOVALUE;
    _43Warning(234, 8, _25663);
    _25663 = NOVALUE;
L27: 
    DeRef(_msg_file_48540);
    _msg_file_48540 = NOVALUE;

    /** 		tok = {SymTab[st_builtin][S_TOKEN], st_builtin}*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25664 = (int)*(((s1_ptr)_2)->base + _st_builtin_48291);
    _2 = (int)SEQ_PTR(_25664);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _25665 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _25665 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _25664 = NOVALUE;
    Ref(_25665);
    DeRef(_tok_48293);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _25665;
    ((int *)_2)[2] = _st_builtin_48291;
    _tok_48293 = MAKE_SEQ(_1);
    _25665 = NOVALUE;

    /** 		if BIND then*/
    if (_25BIND_11877 == 0)
    {
        goto L2C; // [1319] 1328
    }
    else{
    }

    /** 			add_ref(tok)*/
    RefDS(_tok_48293);
    _52add_ref(_tok_48293);
L2C: 

    /** 		return tok*/
    DeRefDS(_word_48276);
    DeRef(_msg_48284);
    DeRef(_b_name_48285);
    DeRef(_gtok_48294);
    DeRef(_25545);
    _25545 = NOVALUE;
    DeRef(_25535);
    _25535 = NOVALUE;
    _25559 = NOVALUE;
    _25564 = NOVALUE;
    _25563 = NOVALUE;
    _25579 = NOVALUE;
    _25572 = NOVALUE;
    DeRef(_25586);
    _25586 = NOVALUE;
    DeRef(_25584);
    _25584 = NOVALUE;
    DeRef(_25594);
    _25594 = NOVALUE;
    _25590 = NOVALUE;
    DeRef(_25592);
    _25592 = NOVALUE;
    _25611 = NOVALUE;
    _25598 = NOVALUE;
    DeRef(_25600);
    _25600 = NOVALUE;
    _25604 = NOVALUE;
    _25616 = NOVALUE;
    DeRef(_25619);
    _25619 = NOVALUE;
    _25655 = NOVALUE;
    _25657 = NOVALUE;
    return _tok_48293;
L26: 
L25: 

    /** ifdef STDDEBUG then*/

    /** 	if length(dup_globals) > 1 and find( 1, in_include_path ) then*/
    if (IS_SEQUENCE(_52dup_globals_48262)){
            _25667 = SEQ_PTR(_52dup_globals_48262)->length;
    }
    else {
        _25667 = 1;
    }
    _25668 = (_25667 > 1);
    _25667 = NOVALUE;
    if (_25668 == 0) {
        goto L2D; // [1349] 1468
    }
    _25670 = find_from(1, _52in_include_path_48264, 1);
    if (_25670 == 0)
    {
        _25670 = NOVALUE;
        goto L2D; // [1361] 1468
    }
    else{
        _25670 = NOVALUE;
    }

    /** 		ix = 1*/
    _ix_48288 = 1;

    /** 		while ix <= length(dup_globals) do*/
L2E: 
    if (IS_SEQUENCE(_52dup_globals_48262)){
            _25671 = SEQ_PTR(_52dup_globals_48262)->length;
    }
    else {
        _25671 = 1;
    }
    if (_ix_48288 > _25671)
    goto L2F; // [1379] 1427

    /** 			if in_include_path[ix] then*/
    _2 = (int)SEQ_PTR(_52in_include_path_48264);
    _25673 = (int)*(((s1_ptr)_2)->base + _ix_48288);
    if (_25673 == 0) {
        _25673 = NOVALUE;
        goto L30; // [1391] 1403
    }
    else {
        if (!IS_ATOM_INT(_25673) && DBL_PTR(_25673)->dbl == 0.0){
            _25673 = NOVALUE;
            goto L30; // [1391] 1403
        }
        _25673 = NOVALUE;
    }
    _25673 = NOVALUE;

    /** 				ix += 1*/
    _ix_48288 = _ix_48288 + 1;
    goto L2E; // [1400] 1374
L30: 

    /** 				dup_globals     = remove( dup_globals, ix )*/
    {
        s1_ptr assign_space = SEQ_PTR(_52dup_globals_48262);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_ix_48288)) ? _ix_48288 : (long)(DBL_PTR(_ix_48288)->dbl);
        int stop = (IS_ATOM_INT(_ix_48288)) ? _ix_48288 : (long)(DBL_PTR(_ix_48288)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_52dup_globals_48262), start, &_52dup_globals_48262 );
            }
            else Tail(SEQ_PTR(_52dup_globals_48262), stop+1, &_52dup_globals_48262);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_52dup_globals_48262), start, &_52dup_globals_48262);
        }
        else {
            assign_slice_seq = &assign_space;
            _52dup_globals_48262 = Remove_elements(start, stop, (SEQ_PTR(_52dup_globals_48262)->ref == 1));
        }
    }

    /** 				in_include_path = remove( in_include_path, ix )*/
    {
        s1_ptr assign_space = SEQ_PTR(_52in_include_path_48264);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_ix_48288)) ? _ix_48288 : (long)(DBL_PTR(_ix_48288)->dbl);
        int stop = (IS_ATOM_INT(_ix_48288)) ? _ix_48288 : (long)(DBL_PTR(_ix_48288)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_52in_include_path_48264), start, &_52in_include_path_48264 );
            }
            else Tail(SEQ_PTR(_52in_include_path_48264), stop+1, &_52in_include_path_48264);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_52in_include_path_48264), start, &_52in_include_path_48264);
        }
        else {
            assign_slice_seq = &assign_space;
            _52in_include_path_48264 = Remove_elements(start, stop, (SEQ_PTR(_52in_include_path_48264)->ref == 1));
        }
    }

    /** 		end while*/
    goto L2E; // [1424] 1374
L2F: 

    /** 		if length(dup_globals) = 1 then*/
    if (IS_SEQUENCE(_52dup_globals_48262)){
            _25677 = SEQ_PTR(_52dup_globals_48262)->length;
    }
    else {
        _25677 = 1;
    }
    if (_25677 != 1)
    goto L31; // [1434] 1467

    /** 				st_ptr = dup_globals[1]*/
    _2 = (int)SEQ_PTR(_52dup_globals_48262);
    _st_ptr_48290 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_st_ptr_48290)){
        _st_ptr_48290 = (long)DBL_PTR(_st_ptr_48290)->dbl;
    }

    /** 				gtok = {SymTab[st_ptr][S_TOKEN], st_ptr}*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25680 = (int)*(((s1_ptr)_2)->base + _st_ptr_48290);
    _2 = (int)SEQ_PTR(_25680);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _25681 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _25681 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _25680 = NOVALUE;
    Ref(_25681);
    DeRef(_gtok_48294);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _25681;
    ((int *)_2)[2] = _st_ptr_48290;
    _gtok_48294 = MAKE_SEQ(_1);
    _25681 = NOVALUE;
L31: 
L2D: 

    /** ifdef STDDEBUG then*/

    /** 	if length(dup_globals) = 1 and st_builtin = 0 then*/
    if (IS_SEQUENCE(_52dup_globals_48262)){
            _25683 = SEQ_PTR(_52dup_globals_48262)->length;
    }
    else {
        _25683 = 1;
    }
    _25684 = (_25683 == 1);
    _25683 = NOVALUE;
    if (_25684 == 0) {
        goto L32; // [1481] 1658
    }
    _25686 = (_st_builtin_48291 == 0);
    if (_25686 == 0)
    {
        DeRef(_25686);
        _25686 = NOVALUE;
        goto L32; // [1490] 1658
    }
    else{
        DeRef(_25686);
        _25686 = NOVALUE;
    }

    /** 		if BIND then*/
    if (_25BIND_11877 == 0)
    {
        goto L33; // [1497] 1508
    }
    else{
    }

    /** 			add_ref(gtok)*/
    Ref(_gtok_48294);
    _52add_ref(_gtok_48294);
L33: 

    /** 		if not in_include_path[1] and*/
    _2 = (int)SEQ_PTR(_52in_include_path_48264);
    _25687 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_25687)) {
        _25688 = (_25687 == 0);
    }
    else {
        _25688 = unary_op(NOT, _25687);
    }
    _25687 = NOVALUE;
    if (IS_ATOM_INT(_25688)) {
        if (_25688 == 0) {
            goto L34; // [1519] 1651
        }
    }
    else {
        if (DBL_PTR(_25688)->dbl == 0.0) {
            goto L34; // [1519] 1651
        }
    }
    _2 = (int)SEQ_PTR(_gtok_48294);
    _25690 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_25690)){
        _25691 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25690)->dbl));
    }
    else{
        _25691 = (int)*(((s1_ptr)_2)->base + _25690);
    }
    _2 = (int)SEQ_PTR(_25691);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _25692 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _25692 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _25691 = NOVALUE;
    Ref(_25692);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _scanning_file_48278;
    ((int *)_2)[2] = _25692;
    _25693 = MAKE_SEQ(_1);
    _25692 = NOVALUE;
    _25694 = find_from(_25693, _52include_warnings_48265, 1);
    DeRefDS(_25693);
    _25693 = NOVALUE;
    _25695 = (_25694 == 0);
    _25694 = NOVALUE;
    if (_25695 == 0)
    {
        DeRef(_25695);
        _25695 = NOVALUE;
        goto L34; // [1558] 1651
    }
    else{
        DeRef(_25695);
        _25695 = NOVALUE;
    }

    /** 			include_warnings = prepend( include_warnings,*/
    _2 = (int)SEQ_PTR(_gtok_48294);
    _25696 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_25696)){
        _25697 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25696)->dbl));
    }
    else{
        _25697 = (int)*(((s1_ptr)_2)->base + _25696);
    }
    _2 = (int)SEQ_PTR(_25697);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _25698 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _25698 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _25697 = NOVALUE;
    Ref(_25698);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _scanning_file_48278;
    ((int *)_2)[2] = _25698;
    _25699 = MAKE_SEQ(_1);
    _25698 = NOVALUE;
    RefDS(_25699);
    Prepend(&_52include_warnings_48265, _52include_warnings_48265, _25699);
    DeRefDS(_25699);
    _25699 = NOVALUE;

    /** ifdef STDDEBUG then*/

    /** 				symbol_resolution_warning = GetMsgText(233,0,*/
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _25701 = (int)*(((s1_ptr)_2)->base + _scanning_file_48278);
    Ref(_25701);
    _25702 = _52name_ext(_25701);
    _25701 = NOVALUE;
    _2 = (int)SEQ_PTR(_gtok_48294);
    _25703 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_25703)){
        _25704 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25703)->dbl));
    }
    else{
        _25704 = (int)*(((s1_ptr)_2)->base + _25703);
    }
    _2 = (int)SEQ_PTR(_25704);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _25705 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _25705 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _25704 = NOVALUE;
    _2 = (int)SEQ_PTR(_26known_files_11139);
    if (!IS_ATOM_INT(_25705)){
        _25706 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25705)->dbl));
    }
    else{
        _25706 = (int)*(((s1_ptr)_2)->base + _25705);
    }
    Ref(_25706);
    _25707 = _52name_ext(_25706);
    _25706 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _25702;
    *((int *)(_2+8)) = _25line_number_12263;
    RefDS(_word_48276);
    *((int *)(_2+12)) = _word_48276;
    *((int *)(_2+16)) = _25707;
    _25708 = MAKE_SEQ(_1);
    _25707 = NOVALUE;
    _25702 = NOVALUE;
    _0 = _44GetMsgText(233, 0, _25708);
    DeRef(_25symbol_resolution_warning_12382);
    _25symbol_resolution_warning_12382 = _0;
    _25708 = NOVALUE;
L34: 

    /** 		return gtok*/
    DeRefDS(_word_48276);
    DeRef(_msg_48284);
    DeRef(_b_name_48285);
    DeRef(_tok_48293);
    _25655 = NOVALUE;
    DeRef(_25594);
    _25594 = NOVALUE;
    DeRef(_25668);
    _25668 = NOVALUE;
    _25559 = NOVALUE;
    _25611 = NOVALUE;
    _25598 = NOVALUE;
    _25705 = NOVALUE;
    _25703 = NOVALUE;
    DeRef(_25619);
    _25619 = NOVALUE;
    _25604 = NOVALUE;
    _25563 = NOVALUE;
    DeRef(_25586);
    _25586 = NOVALUE;
    _25579 = NOVALUE;
    DeRef(_25688);
    _25688 = NOVALUE;
    _25564 = NOVALUE;
    _25590 = NOVALUE;
    DeRef(_25600);
    _25600 = NOVALUE;
    DeRef(_25545);
    _25545 = NOVALUE;
    DeRef(_25535);
    _25535 = NOVALUE;
    _25572 = NOVALUE;
    DeRef(_25584);
    _25584 = NOVALUE;
    DeRef(_25684);
    _25684 = NOVALUE;
    DeRef(_25592);
    _25592 = NOVALUE;
    _25616 = NOVALUE;
    _25690 = NOVALUE;
    _25657 = NOVALUE;
    _25696 = NOVALUE;
    return _gtok_48294;
L32: 

    /** 	if length(dup_globals) = 0 then*/
    if (IS_SEQUENCE(_52dup_globals_48262)){
            _25710 = SEQ_PTR(_52dup_globals_48262)->length;
    }
    else {
        _25710 = 1;
    }
    if (_25710 != 0)
    goto L35; // [1665] 1739

    /** 		defined = SC_UNDEFINED*/
    _defined_48287 = 9;

    /** 		if fwd_line_number then*/
    if (_25fwd_line_number_12264 == 0)
    {
        goto L36; // [1682] 1711
    }
    else{
    }

    /** 			last_ForwardLine     = ForwardLine*/
    Ref(_43ForwardLine_49533);
    DeRef(_43last_ForwardLine_49535);
    _43last_ForwardLine_49535 = _43ForwardLine_49533;

    /** 			last_forward_bp      = forward_bp*/
    _43last_forward_bp_49539 = _43forward_bp_49537;

    /** 			last_fwd_line_number = fwd_line_number*/
    _25last_fwd_line_number_12266 = _25fwd_line_number_12264;
L36: 

    /** 		ForwardLine = ThisLine*/
    Ref(_43ThisLine_49532);
    DeRef(_43ForwardLine_49533);
    _43ForwardLine_49533 = _43ThisLine_49532;

    /** 		forward_bp = bp*/
    _43forward_bp_49537 = _43bp_49536;

    /** 		fwd_line_number = line_number*/
    _25fwd_line_number_12264 = _25line_number_12263;
    goto L37; // [1736] 1782
L35: 

    /** 	elsif length(dup_globals) then*/
    if (IS_SEQUENCE(_52dup_globals_48262)){
            _25712 = SEQ_PTR(_52dup_globals_48262)->length;
    }
    else {
        _25712 = 1;
    }
    if (_25712 == 0)
    {
        _25712 = NOVALUE;
        goto L38; // [1746] 1761
    }
    else{
        _25712 = NOVALUE;
    }

    /** 		defined = SC_MULTIPLY_DEFINED*/
    _defined_48287 = 10;
    goto L37; // [1758] 1782
L38: 

    /** 	elsif length(dup_overrides) then*/
    if (IS_SEQUENCE(_52dup_overrides_48263)){
            _25713 = SEQ_PTR(_52dup_overrides_48263)->length;
    }
    else {
        _25713 = 1;
    }
    if (_25713 == 0)
    {
        _25713 = NOVALUE;
        goto L39; // [1768] 1781
    }
    else{
        _25713 = NOVALUE;
    }

    /** 		defined = SC_OVERRIDE*/
    _defined_48287 = 12;
L39: 
L37: 

    /** 	if No_new_entry then*/
    if (_52No_new_entry_48273 == 0)
    {
        goto L3A; // [1786] 1809
    }
    else{
    }

    /** 		return {IGNORED,word,defined,dup_globals}*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 509;
    RefDS(_word_48276);
    *((int *)(_2+8)) = _word_48276;
    *((int *)(_2+12)) = _defined_48287;
    RefDS(_52dup_globals_48262);
    *((int *)(_2+16)) = _52dup_globals_48262;
    _25714 = MAKE_SEQ(_1);
    DeRefDS(_word_48276);
    DeRef(_msg_48284);
    DeRef(_b_name_48285);
    DeRef(_tok_48293);
    DeRef(_gtok_48294);
    _25655 = NOVALUE;
    DeRef(_25594);
    _25594 = NOVALUE;
    DeRef(_25668);
    _25668 = NOVALUE;
    _25559 = NOVALUE;
    _25611 = NOVALUE;
    _25598 = NOVALUE;
    _25705 = NOVALUE;
    _25703 = NOVALUE;
    DeRef(_25619);
    _25619 = NOVALUE;
    _25604 = NOVALUE;
    _25563 = NOVALUE;
    DeRef(_25586);
    _25586 = NOVALUE;
    _25579 = NOVALUE;
    DeRef(_25688);
    _25688 = NOVALUE;
    _25564 = NOVALUE;
    _25590 = NOVALUE;
    DeRef(_25600);
    _25600 = NOVALUE;
    DeRef(_25545);
    _25545 = NOVALUE;
    DeRef(_25535);
    _25535 = NOVALUE;
    _25572 = NOVALUE;
    DeRef(_25584);
    _25584 = NOVALUE;
    DeRef(_25684);
    _25684 = NOVALUE;
    DeRef(_25592);
    _25592 = NOVALUE;
    _25616 = NOVALUE;
    _25690 = NOVALUE;
    _25657 = NOVALUE;
    _25696 = NOVALUE;
    return _25714;
L3A: 

    /** 	tok = {VARIABLE, NewEntry(word, 0, defined,*/
    _2 = (int)SEQ_PTR(_52buckets_47095);
    _25715 = (int)*(((s1_ptr)_2)->base + _hashval_48282);
    RefDS(_word_48276);
    Ref(_25715);
    _25716 = _52NewEntry(_word_48276, 0, _defined_48287, -100, _hashval_48282, _25715, 0);
    _25715 = NOVALUE;
    DeRef(_tok_48293);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -100;
    ((int *)_2)[2] = _25716;
    _tok_48293 = MAKE_SEQ(_1);
    _25716 = NOVALUE;

    /** 	buckets[hashval] = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_48293);
    _25718 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_25718);
    _2 = (int)SEQ_PTR(_52buckets_47095);
    _2 = (int)(((s1_ptr)_2)->base + _hashval_48282);
    _1 = *(int *)_2;
    *(int *)_2 = _25718;
    if( _1 != _25718 ){
        DeRef(_1);
    }
    _25718 = NOVALUE;

    /** 	if file_no != -1 then*/
    if (_file_no_48277 == -1)
    goto L3B; // [1853] 1879

    /** 		SymTab[tok[T_SYM]][S_FILE_NO] = file_no*/
    _2 = (int)SEQ_PTR(_tok_48293);
    _25720 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25720))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25720)->dbl));
    else
    _3 = (int)(_25720 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_FILE_NO_11909))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    _1 = *(int *)_2;
    *(int *)_2 = _file_no_48277;
    DeRef(_1);
    _25721 = NOVALUE;
L3B: 

    /** 	return tok  -- no ref on newly declared symbol*/
    DeRefDS(_word_48276);
    DeRef(_msg_48284);
    DeRef(_b_name_48285);
    DeRef(_gtok_48294);
    _25720 = NOVALUE;
    _25655 = NOVALUE;
    DeRef(_25594);
    _25594 = NOVALUE;
    DeRef(_25668);
    _25668 = NOVALUE;
    _25559 = NOVALUE;
    _25611 = NOVALUE;
    _25598 = NOVALUE;
    _25705 = NOVALUE;
    _25703 = NOVALUE;
    DeRef(_25619);
    _25619 = NOVALUE;
    _25604 = NOVALUE;
    _25563 = NOVALUE;
    DeRef(_25586);
    _25586 = NOVALUE;
    _25579 = NOVALUE;
    DeRef(_25688);
    _25688 = NOVALUE;
    DeRef(_25714);
    _25714 = NOVALUE;
    _25564 = NOVALUE;
    _25590 = NOVALUE;
    DeRef(_25600);
    _25600 = NOVALUE;
    DeRef(_25545);
    _25545 = NOVALUE;
    DeRef(_25535);
    _25535 = NOVALUE;
    _25572 = NOVALUE;
    DeRef(_25584);
    _25584 = NOVALUE;
    DeRef(_25684);
    _25684 = NOVALUE;
    DeRef(_25592);
    _25592 = NOVALUE;
    _25616 = NOVALUE;
    _25690 = NOVALUE;
    _25657 = NOVALUE;
    _25696 = NOVALUE;
    return _tok_48293;
    ;
}


void _52Hide(int _s_48688)
{
    int _prev_48690 = NOVALUE;
    int _p_48691 = NOVALUE;
    int _25741 = NOVALUE;
    int _25740 = NOVALUE;
    int _25739 = NOVALUE;
    int _25737 = NOVALUE;
    int _25736 = NOVALUE;
    int _25735 = NOVALUE;
    int _25734 = NOVALUE;
    int _25733 = NOVALUE;
    int _25729 = NOVALUE;
    int _25728 = NOVALUE;
    int _25727 = NOVALUE;
    int _25726 = NOVALUE;
    int _25724 = NOVALUE;
    int _25723 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_s_48688)) {
        _1 = (long)(DBL_PTR(_s_48688)->dbl);
        if (UNIQUE(DBL_PTR(_s_48688)) && (DBL_PTR(_s_48688)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_48688);
        _s_48688 = _1;
    }

    /** 	p = buckets[SymTab[s][S_HASHVAL]]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25723 = (int)*(((s1_ptr)_2)->base + _s_48688);
    _2 = (int)SEQ_PTR(_25723);
    _25724 = (int)*(((s1_ptr)_2)->base + 11);
    _25723 = NOVALUE;
    _2 = (int)SEQ_PTR(_52buckets_47095);
    if (!IS_ATOM_INT(_25724)){
        _p_48691 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25724)->dbl));
    }
    else{
        _p_48691 = (int)*(((s1_ptr)_2)->base + _25724);
    }
    if (!IS_ATOM_INT(_p_48691)){
        _p_48691 = (long)DBL_PTR(_p_48691)->dbl;
    }

    /** 	prev = 0*/
    _prev_48690 = 0;

    /** 	while p != s and p != 0 do*/
L1: 
    _25726 = (_p_48691 != _s_48688);
    if (_25726 == 0) {
        goto L2; // [41] 81
    }
    _25728 = (_p_48691 != 0);
    if (_25728 == 0)
    {
        DeRef(_25728);
        _25728 = NOVALUE;
        goto L2; // [50] 81
    }
    else{
        DeRef(_25728);
        _25728 = NOVALUE;
    }

    /** 		prev = p*/
    _prev_48690 = _p_48691;

    /** 		p = SymTab[p][S_SAMEHASH]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25729 = (int)*(((s1_ptr)_2)->base + _p_48691);
    _2 = (int)SEQ_PTR(_25729);
    _p_48691 = (int)*(((s1_ptr)_2)->base + 9);
    if (!IS_ATOM_INT(_p_48691)){
        _p_48691 = (long)DBL_PTR(_p_48691)->dbl;
    }
    _25729 = NOVALUE;

    /** 	end while*/
    goto L1; // [78] 37
L2: 

    /** 	if p = 0 then*/
    if (_p_48691 != 0)
    goto L3; // [83] 93

    /** 		return -- already hidden*/
    _25724 = NOVALUE;
    DeRef(_25726);
    _25726 = NOVALUE;
    return;
L3: 

    /** 	if prev = 0 then*/
    if (_prev_48690 != 0)
    goto L4; // [95] 134

    /** 		buckets[SymTab[s][S_HASHVAL]] = SymTab[s][S_SAMEHASH]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25733 = (int)*(((s1_ptr)_2)->base + _s_48688);
    _2 = (int)SEQ_PTR(_25733);
    _25734 = (int)*(((s1_ptr)_2)->base + 11);
    _25733 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25735 = (int)*(((s1_ptr)_2)->base + _s_48688);
    _2 = (int)SEQ_PTR(_25735);
    _25736 = (int)*(((s1_ptr)_2)->base + 9);
    _25735 = NOVALUE;
    Ref(_25736);
    _2 = (int)SEQ_PTR(_52buckets_47095);
    if (!IS_ATOM_INT(_25734))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25734)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25734);
    _1 = *(int *)_2;
    *(int *)_2 = _25736;
    if( _1 != _25736 ){
        DeRef(_1);
    }
    _25736 = NOVALUE;
    goto L5; // [131] 162
L4: 

    /** 		SymTab[prev][S_SAMEHASH] = SymTab[s][S_SAMEHASH]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_prev_48690 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25739 = (int)*(((s1_ptr)_2)->base + _s_48688);
    _2 = (int)SEQ_PTR(_25739);
    _25740 = (int)*(((s1_ptr)_2)->base + 9);
    _25739 = NOVALUE;
    Ref(_25740);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 9);
    _1 = *(int *)_2;
    *(int *)_2 = _25740;
    if( _1 != _25740 ){
        DeRef(_1);
    }
    _25740 = NOVALUE;
    _25737 = NOVALUE;
L5: 

    /** 	SymTab[s][S_SAMEHASH] = 0*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_s_48688 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 9);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _25741 = NOVALUE;

    /** end procedure*/
    _25724 = NOVALUE;
    DeRef(_25726);
    _25726 = NOVALUE;
    _25734 = NOVALUE;
    return;
    ;
}


void _52Show(int _s_48733)
{
    int _p_48735 = NOVALUE;
    int _25753 = NOVALUE;
    int _25752 = NOVALUE;
    int _25750 = NOVALUE;
    int _25749 = NOVALUE;
    int _25747 = NOVALUE;
    int _25746 = NOVALUE;
    int _25744 = NOVALUE;
    int _25743 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_s_48733)) {
        _1 = (long)(DBL_PTR(_s_48733)->dbl);
        if (UNIQUE(DBL_PTR(_s_48733)) && (DBL_PTR(_s_48733)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_48733);
        _s_48733 = _1;
    }

    /** 	p = buckets[SymTab[s][S_HASHVAL]]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25743 = (int)*(((s1_ptr)_2)->base + _s_48733);
    _2 = (int)SEQ_PTR(_25743);
    _25744 = (int)*(((s1_ptr)_2)->base + 11);
    _25743 = NOVALUE;
    _2 = (int)SEQ_PTR(_52buckets_47095);
    if (!IS_ATOM_INT(_25744)){
        _p_48735 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25744)->dbl));
    }
    else{
        _p_48735 = (int)*(((s1_ptr)_2)->base + _25744);
    }
    if (!IS_ATOM_INT(_p_48735)){
        _p_48735 = (long)DBL_PTR(_p_48735)->dbl;
    }

    /** 	if SymTab[s][S_SAMEHASH] or p = s then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25746 = (int)*(((s1_ptr)_2)->base + _s_48733);
    _2 = (int)SEQ_PTR(_25746);
    _25747 = (int)*(((s1_ptr)_2)->base + 9);
    _25746 = NOVALUE;
    if (IS_ATOM_INT(_25747)) {
        if (_25747 != 0) {
            goto L1; // [39] 52
        }
    }
    else {
        if (DBL_PTR(_25747)->dbl != 0.0) {
            goto L1; // [39] 52
        }
    }
    _25749 = (_p_48735 == _s_48733);
    if (_25749 == 0)
    {
        DeRef(_25749);
        _25749 = NOVALUE;
        goto L2; // [48] 58
    }
    else{
        DeRef(_25749);
        _25749 = NOVALUE;
    }
L1: 

    /** 		return*/
    _25744 = NOVALUE;
    _25747 = NOVALUE;
    return;
L2: 

    /** 	SymTab[s][S_SAMEHASH] = p*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_s_48733 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 9);
    _1 = *(int *)_2;
    *(int *)_2 = _p_48735;
    DeRef(_1);
    _25750 = NOVALUE;

    /** 	buckets[SymTab[s][S_HASHVAL]] = s*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25752 = (int)*(((s1_ptr)_2)->base + _s_48733);
    _2 = (int)SEQ_PTR(_25752);
    _25753 = (int)*(((s1_ptr)_2)->base + 11);
    _25752 = NOVALUE;
    _2 = (int)SEQ_PTR(_52buckets_47095);
    if (!IS_ATOM_INT(_25753))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25753)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25753);
    _1 = *(int *)_2;
    *(int *)_2 = _s_48733;
    DeRef(_1);

    /** end procedure*/
    _25744 = NOVALUE;
    _25747 = NOVALUE;
    _25753 = NOVALUE;
    return;
    ;
}


void _52hide_params(int _s_48759)
{
    int _param_48761 = NOVALUE;
    int _25756 = NOVALUE;
    int _25755 = NOVALUE;
    int _25754 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_s_48759)) {
        _1 = (long)(DBL_PTR(_s_48759)->dbl);
        if (UNIQUE(DBL_PTR(_s_48759)) && (DBL_PTR(_s_48759)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_48759);
        _s_48759 = _1;
    }

    /** 	symtab_index param = s*/
    _param_48761 = _s_48759;

    /** 	for i = 1 to SymTab[s][S_NUM_ARGS] do*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25754 = (int)*(((s1_ptr)_2)->base + _s_48759);
    _2 = (int)SEQ_PTR(_25754);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _25755 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _25755 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    _25754 = NOVALUE;
    {
        int _i_48763;
        _i_48763 = 1;
L1: 
        if (binary_op_a(GREATER, _i_48763, _25755)){
            goto L2; // [24] 59
        }

        /** 		param = SymTab[s][S_NEXT]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _25756 = (int)*(((s1_ptr)_2)->base + _s_48759);
        _2 = (int)SEQ_PTR(_25756);
        _param_48761 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_param_48761)){
            _param_48761 = (long)DBL_PTR(_param_48761)->dbl;
        }
        _25756 = NOVALUE;

        /** 		Hide( param )*/
        _52Hide(_param_48761);

        /** 	end for*/
        _0 = _i_48763;
        if (IS_ATOM_INT(_i_48763)) {
            _i_48763 = _i_48763 + 1;
            if ((long)((unsigned long)_i_48763 +(unsigned long) HIGH_BITS) >= 0){
                _i_48763 = NewDouble((double)_i_48763);
            }
        }
        else {
            _i_48763 = binary_op_a(PLUS, _i_48763, 1);
        }
        DeRef(_0);
        goto L1; // [54] 31
L2: 
        ;
        DeRef(_i_48763);
    }

    /** end procedure*/
    _25755 = NOVALUE;
    return;
    ;
}


void _52show_params(int _s_48775)
{
    int _param_48777 = NOVALUE;
    int _25760 = NOVALUE;
    int _25759 = NOVALUE;
    int _25758 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_s_48775)) {
        _1 = (long)(DBL_PTR(_s_48775)->dbl);
        if (UNIQUE(DBL_PTR(_s_48775)) && (DBL_PTR(_s_48775)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_48775);
        _s_48775 = _1;
    }

    /** 	symtab_index param = s*/
    _param_48777 = _s_48775;

    /** 	for i = 1 to SymTab[s][S_NUM_ARGS] do*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25758 = (int)*(((s1_ptr)_2)->base + _s_48775);
    _2 = (int)SEQ_PTR(_25758);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _25759 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _25759 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    _25758 = NOVALUE;
    {
        int _i_48779;
        _i_48779 = 1;
L1: 
        if (binary_op_a(GREATER, _i_48779, _25759)){
            goto L2; // [24] 59
        }

        /** 		param = SymTab[s][S_NEXT]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _25760 = (int)*(((s1_ptr)_2)->base + _s_48775);
        _2 = (int)SEQ_PTR(_25760);
        _param_48777 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_param_48777)){
            _param_48777 = (long)DBL_PTR(_param_48777)->dbl;
        }
        _25760 = NOVALUE;

        /** 		Show( param )*/
        _52Show(_param_48777);

        /** 	end for*/
        _0 = _i_48779;
        if (IS_ATOM_INT(_i_48779)) {
            _i_48779 = _i_48779 + 1;
            if ((long)((unsigned long)_i_48779 +(unsigned long) HIGH_BITS) >= 0){
                _i_48779 = NewDouble((double)_i_48779);
            }
        }
        else {
            _i_48779 = binary_op_a(PLUS, _i_48779, 1);
        }
        DeRef(_0);
        goto L1; // [54] 31
L2: 
        ;
        DeRef(_i_48779);
    }

    /** end procedure*/
    _25759 = NOVALUE;
    return;
    ;
}


void _52LintCheck(int _s_48791)
{
    int _warn_level_48792 = NOVALUE;
    int _file_48793 = NOVALUE;
    int _vscope_48794 = NOVALUE;
    int _vname_48795 = NOVALUE;
    int _vusage_48796 = NOVALUE;
    int _25823 = NOVALUE;
    int _25822 = NOVALUE;
    int _25821 = NOVALUE;
    int _25820 = NOVALUE;
    int _25819 = NOVALUE;
    int _25818 = NOVALUE;
    int _25815 = NOVALUE;
    int _25814 = NOVALUE;
    int _25813 = NOVALUE;
    int _25812 = NOVALUE;
    int _25811 = NOVALUE;
    int _25810 = NOVALUE;
    int _25807 = NOVALUE;
    int _25806 = NOVALUE;
    int _25805 = NOVALUE;
    int _25804 = NOVALUE;
    int _25803 = NOVALUE;
    int _25802 = NOVALUE;
    int _25800 = NOVALUE;
    int _25798 = NOVALUE;
    int _25797 = NOVALUE;
    int _25795 = NOVALUE;
    int _25794 = NOVALUE;
    int _25792 = NOVALUE;
    int _25791 = NOVALUE;
    int _25790 = NOVALUE;
    int _25789 = NOVALUE;
    int _25786 = NOVALUE;
    int _25785 = NOVALUE;
    int _25781 = NOVALUE;
    int _25778 = NOVALUE;
    int _25777 = NOVALUE;
    int _25776 = NOVALUE;
    int _25775 = NOVALUE;
    int _25772 = NOVALUE;
    int _25771 = NOVALUE;
    int _25766 = NOVALUE;
    int _25764 = NOVALUE;
    int _25762 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_s_48791)) {
        _1 = (long)(DBL_PTR(_s_48791)->dbl);
        if (UNIQUE(DBL_PTR(_s_48791)) && (DBL_PTR(_s_48791)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_48791);
        _s_48791 = _1;
    }

    /** 	vusage = SymTab[s][S_USAGE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25762 = (int)*(((s1_ptr)_2)->base + _s_48791);
    _2 = (int)SEQ_PTR(_25762);
    _vusage_48796 = (int)*(((s1_ptr)_2)->base + 5);
    if (!IS_ATOM_INT(_vusage_48796)){
        _vusage_48796 = (long)DBL_PTR(_vusage_48796)->dbl;
    }
    _25762 = NOVALUE;

    /** 	vscope = SymTab[s][S_SCOPE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25764 = (int)*(((s1_ptr)_2)->base + _s_48791);
    _2 = (int)SEQ_PTR(_25764);
    _vscope_48794 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_vscope_48794)){
        _vscope_48794 = (long)DBL_PTR(_vscope_48794)->dbl;
    }
    _25764 = NOVALUE;

    /** 	vname = SymTab[s][S_NAME]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25766 = (int)*(((s1_ptr)_2)->base + _s_48791);
    DeRef(_vname_48795);
    _2 = (int)SEQ_PTR(_25766);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _vname_48795 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _vname_48795 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    Ref(_vname_48795);
    _25766 = NOVALUE;

    /** 	switch vusage do*/
    _0 = _vusage_48796;
    switch ( _0 ){ 

        /** 		case U_UNUSED then*/
        case 0:

        /** 			warn_level = 1*/
        _warn_level_48792 = 1;
        goto L1; // [67] 193

        /** 		case U_WRITTEN then -- Set but never read*/
        case 2:

        /** 			warn_level = 2*/
        _warn_level_48792 = 2;

        /** 			if vscope > SC_LOCAL then*/
        if (_vscope_48794 <= 5)
        goto L2; // [82] 94

        /** 				warn_level = 0 */
        _warn_level_48792 = 0;
        goto L1; // [91] 193
L2: 

        /** 			elsif SymTab[s][S_MODE] = M_CONSTANT then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _25771 = (int)*(((s1_ptr)_2)->base + _s_48791);
        _2 = (int)SEQ_PTR(_25771);
        _25772 = (int)*(((s1_ptr)_2)->base + 3);
        _25771 = NOVALUE;
        if (binary_op_a(NOTEQ, _25772, 2)){
            _25772 = NOVALUE;
            goto L1; // [110] 193
        }
        _25772 = NOVALUE;

        /** 				if not Strict_is_on then*/
        if (_25Strict_is_on_12328 != 0)
        goto L1; // [118] 193

        /** 					warn_level = 0 */
        _warn_level_48792 = 0;
        goto L1; // [129] 193

        /** 		case U_READ then -- Read but never set*/
        case 1:

        /** 			if SymTab[s][S_VARNUM] >= SymTab[CurrentSub][S_NUM_ARGS] then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _25775 = (int)*(((s1_ptr)_2)->base + _s_48791);
        _2 = (int)SEQ_PTR(_25775);
        _25776 = (int)*(((s1_ptr)_2)->base + 16);
        _25775 = NOVALUE;
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _25777 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
        _2 = (int)SEQ_PTR(_25777);
        if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
            _25778 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
        }
        else{
            _25778 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
        }
        _25777 = NOVALUE;
        if (binary_op_a(LESS, _25776, _25778)){
            _25776 = NOVALUE;
            _25778 = NOVALUE;
            goto L3; // [163] 175
        }
        _25776 = NOVALUE;
        _25778 = NOVALUE;

        /** 		    	warn_level = 3*/
        _warn_level_48792 = 3;
        goto L1; // [172] 193
L3: 

        /** 		    	warn_level = 0*/
        _warn_level_48792 = 0;
        goto L1; // [181] 193

        /** 	    case else*/
        default:

        /** 	    	warn_level = 0*/
        _warn_level_48792 = 0;
    ;}L1: 

    /** 	if warn_level = 0 then*/
    if (_warn_level_48792 != 0)
    goto L4; // [197] 207

    /** 		return*/
    DeRef(_file_48793);
    DeRef(_vname_48795);
    return;
L4: 

    /** 	file = abbreviate_path(known_files[current_file_no])*/
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _25781 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    Ref(_25781);
    RefDS(_22682);
    _0 = _file_48793;
    _file_48793 = _9abbreviate_path(_25781, _22682);
    DeRef(_0);
    _25781 = NOVALUE;

    /** 	if warn_level = 3 then*/
    if (_warn_level_48792 != 3)
    goto L5; // [226] 308

    /** 		if vscope = SC_LOCAL then*/
    if (_vscope_48794 != 5)
    goto L6; // [234] 275

    /** 			if current_file_no = SymTab[s][S_FILE_NO] then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25785 = (int)*(((s1_ptr)_2)->base + _s_48791);
    _2 = (int)SEQ_PTR(_25785);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _25786 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _25786 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _25785 = NOVALUE;
    if (binary_op_a(NOTEQ, _25current_file_no_12262, _25786)){
        _25786 = NOVALUE;
        goto L7; // [254] 602
    }
    _25786 = NOVALUE;

    /** 				Warning(226, no_value_warning_flag, {file,  vname})*/
    RefDS(_vname_48795);
    RefDS(_file_48793);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _file_48793;
    ((int *)_2)[2] = _vname_48795;
    _25789 = MAKE_SEQ(_1);
    _43Warning(226, 32, _25789);
    _25789 = NOVALUE;
    goto L7; // [272] 602
L6: 

    /** 			Warning(227, no_value_warning_flag, {file,  vname, SymTab[CurrentSub][S_NAME]})*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25790 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_25790);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _25791 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _25791 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _25790 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_file_48793);
    *((int *)(_2+4)) = _file_48793;
    RefDS(_vname_48795);
    *((int *)(_2+8)) = _vname_48795;
    Ref(_25791);
    *((int *)(_2+12)) = _25791;
    _25792 = MAKE_SEQ(_1);
    _25791 = NOVALUE;
    _43Warning(227, 32, _25792);
    _25792 = NOVALUE;
    goto L7; // [305] 602
L5: 

    /** 		if vscope = SC_LOCAL then*/
    if (_vscope_48794 != 5)
    goto L8; // [312] 412

    /** 			if current_file_no = SymTab[s][S_FILE_NO] then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25794 = (int)*(((s1_ptr)_2)->base + _s_48791);
    _2 = (int)SEQ_PTR(_25794);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _25795 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _25795 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _25794 = NOVALUE;
    if (binary_op_a(NOTEQ, _25current_file_no_12262, _25795)){
        _25795 = NOVALUE;
        goto L9; // [332] 601
    }
    _25795 = NOVALUE;

    /** 				if SymTab[s][S_MODE] = M_CONSTANT then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25797 = (int)*(((s1_ptr)_2)->base + _s_48791);
    _2 = (int)SEQ_PTR(_25797);
    _25798 = (int)*(((s1_ptr)_2)->base + 3);
    _25797 = NOVALUE;
    if (binary_op_a(NOTEQ, _25798, 2)){
        _25798 = NOVALUE;
        goto LA; // [352] 372
    }
    _25798 = NOVALUE;

    /** 					Warning(228, not_used_warning_flag, {file,  vname})*/
    RefDS(_vname_48795);
    RefDS(_file_48793);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _file_48793;
    ((int *)_2)[2] = _vname_48795;
    _25800 = MAKE_SEQ(_1);
    _43Warning(228, 16, _25800);
    _25800 = NOVALUE;
    goto L9; // [369] 601
LA: 

    /** 				elsif warn_level = 1 then*/
    if (_warn_level_48792 != 1)
    goto LB; // [374] 394

    /** 					Warning(229, not_used_warning_flag, {file,  vname})*/
    RefDS(_vname_48795);
    RefDS(_file_48793);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _file_48793;
    ((int *)_2)[2] = _vname_48795;
    _25802 = MAKE_SEQ(_1);
    _43Warning(229, 16, _25802);
    _25802 = NOVALUE;
    goto L9; // [391] 601
LB: 

    /** 					Warning(320, not_used_warning_flag, {file,  vname})*/
    RefDS(_vname_48795);
    RefDS(_file_48793);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _file_48793;
    ((int *)_2)[2] = _vname_48795;
    _25803 = MAKE_SEQ(_1);
    _43Warning(320, 16, _25803);
    _25803 = NOVALUE;
    goto L9; // [409] 601
L8: 

    /** 			if SymTab[s][S_VARNUM] < SymTab[CurrentSub][S_NUM_ARGS] then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25804 = (int)*(((s1_ptr)_2)->base + _s_48791);
    _2 = (int)SEQ_PTR(_25804);
    _25805 = (int)*(((s1_ptr)_2)->base + 16);
    _25804 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25806 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_25806);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _25807 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _25807 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    _25806 = NOVALUE;
    if (binary_op_a(GREATEREQ, _25805, _25807)){
        _25805 = NOVALUE;
        _25807 = NOVALUE;
        goto LC; // [440] 523
    }
    _25805 = NOVALUE;
    _25807 = NOVALUE;

    /** 				if warn_level = 1 then*/
    if (_warn_level_48792 != 1)
    goto LD; // [446] 490

    /** 					if Strict_is_on then*/
    if (_25Strict_is_on_12328 == 0)
    {
        goto LE; // [454] 600
    }
    else{
    }

    /** 						Warning(230, not_used_warning_flag, {file,  vname, SymTab[CurrentSub][S_NAME]})*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25810 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_25810);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _25811 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _25811 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _25810 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_file_48793);
    *((int *)(_2+4)) = _file_48793;
    RefDS(_vname_48795);
    *((int *)(_2+8)) = _vname_48795;
    Ref(_25811);
    *((int *)(_2+12)) = _25811;
    _25812 = MAKE_SEQ(_1);
    _25811 = NOVALUE;
    _43Warning(230, 16, _25812);
    _25812 = NOVALUE;
    goto LE; // [487] 600
LD: 

    /** 					Warning(321, not_used_warning_flag, {file,  vname, SymTab[CurrentSub][S_NAME]})*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25813 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_25813);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _25814 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _25814 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _25813 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_file_48793);
    *((int *)(_2+4)) = _file_48793;
    RefDS(_vname_48795);
    *((int *)(_2+8)) = _vname_48795;
    Ref(_25814);
    *((int *)(_2+12)) = _25814;
    _25815 = MAKE_SEQ(_1);
    _25814 = NOVALUE;
    _43Warning(321, 16, _25815);
    _25815 = NOVALUE;
    goto LE; // [520] 600
LC: 

    /** 				if warn_level = 1 then*/
    if (_warn_level_48792 != 1)
    goto LF; // [525] 569

    /** 					if Strict_is_on then*/
    if (_25Strict_is_on_12328 == 0)
    {
        goto L10; // [533] 599
    }
    else{
    }

    /** 						Warning(231, not_used_warning_flag, {file,  vname, SymTab[CurrentSub][S_NAME]})*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25818 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_25818);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _25819 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _25819 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _25818 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_file_48793);
    *((int *)(_2+4)) = _file_48793;
    RefDS(_vname_48795);
    *((int *)(_2+8)) = _vname_48795;
    Ref(_25819);
    *((int *)(_2+12)) = _25819;
    _25820 = MAKE_SEQ(_1);
    _25819 = NOVALUE;
    _43Warning(231, 16, _25820);
    _25820 = NOVALUE;
    goto L10; // [566] 599
LF: 

    /** 					Warning(322, not_used_warning_flag, {file,  vname, SymTab[CurrentSub][S_NAME]})*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25821 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_25821);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _25822 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _25822 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _25821 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_file_48793);
    *((int *)(_2+4)) = _file_48793;
    RefDS(_vname_48795);
    *((int *)(_2+8)) = _vname_48795;
    Ref(_25822);
    *((int *)(_2+12)) = _25822;
    _25823 = MAKE_SEQ(_1);
    _25822 = NOVALUE;
    _43Warning(322, 16, _25823);
    _25823 = NOVALUE;
L10: 
LE: 
L9: 
L7: 

    /** end procedure*/
    DeRef(_file_48793);
    DeRef(_vname_48795);
    return;
    ;
}


void _52HideLocals()
{
    int _s_48964 = NOVALUE;
    int _25834 = NOVALUE;
    int _25832 = NOVALUE;
    int _25831 = NOVALUE;
    int _25830 = NOVALUE;
    int _25829 = NOVALUE;
    int _25828 = NOVALUE;
    int _25827 = NOVALUE;
    int _25826 = NOVALUE;
    int _25825 = NOVALUE;
    int _25824 = NOVALUE;
    int _0, _1, _2;
    

    /** 	s = file_start_sym*/
    _s_48964 = _25file_start_sym_12268;

    /** 	while s do*/
L1: 
    if (_s_48964 == 0)
    {
        goto L2; // [15] 117
    }
    else{
    }

    /** 		if SymTab[s][S_SCOPE] = SC_LOCAL and*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25824 = (int)*(((s1_ptr)_2)->base + _s_48964);
    _2 = (int)SEQ_PTR(_25824);
    _25825 = (int)*(((s1_ptr)_2)->base + 4);
    _25824 = NOVALUE;
    if (IS_ATOM_INT(_25825)) {
        _25826 = (_25825 == 5);
    }
    else {
        _25826 = binary_op(EQUALS, _25825, 5);
    }
    _25825 = NOVALUE;
    if (IS_ATOM_INT(_25826)) {
        if (_25826 == 0) {
            goto L3; // [38] 96
        }
    }
    else {
        if (DBL_PTR(_25826)->dbl == 0.0) {
            goto L3; // [38] 96
        }
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25828 = (int)*(((s1_ptr)_2)->base + _s_48964);
    _2 = (int)SEQ_PTR(_25828);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _25829 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _25829 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _25828 = NOVALUE;
    if (IS_ATOM_INT(_25829)) {
        _25830 = (_25829 == _25current_file_no_12262);
    }
    else {
        _25830 = binary_op(EQUALS, _25829, _25current_file_no_12262);
    }
    _25829 = NOVALUE;
    if (_25830 == 0) {
        DeRef(_25830);
        _25830 = NOVALUE;
        goto L3; // [61] 96
    }
    else {
        if (!IS_ATOM_INT(_25830) && DBL_PTR(_25830)->dbl == 0.0){
            DeRef(_25830);
            _25830 = NOVALUE;
            goto L3; // [61] 96
        }
        DeRef(_25830);
        _25830 = NOVALUE;
    }
    DeRef(_25830);
    _25830 = NOVALUE;

    /** 			Hide(s)*/
    _52Hide(_s_48964);

    /** 			if SymTab[s][S_TOKEN] = VARIABLE then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25831 = (int)*(((s1_ptr)_2)->base + _s_48964);
    _2 = (int)SEQ_PTR(_25831);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _25832 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _25832 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _25831 = NOVALUE;
    if (binary_op_a(NOTEQ, _25832, -100)){
        _25832 = NOVALUE;
        goto L4; // [85] 95
    }
    _25832 = NOVALUE;

    /** 				LintCheck(s)*/
    _52LintCheck(_s_48964);
L4: 
L3: 

    /** 		s = SymTab[s][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25834 = (int)*(((s1_ptr)_2)->base + _s_48964);
    _2 = (int)SEQ_PTR(_25834);
    _s_48964 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_48964)){
        _s_48964 = (long)DBL_PTR(_s_48964)->dbl;
    }
    _25834 = NOVALUE;

    /** 	end while*/
    goto L1; // [114] 15
L2: 

    /** end procedure*/
    DeRef(_25826);
    _25826 = NOVALUE;
    return;
    ;
}


int _52sym_name(int _sym_48995)
{
    int _25837 = NOVALUE;
    int _25836 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_48995)) {
        _1 = (long)(DBL_PTR(_sym_48995)->dbl);
        if (UNIQUE(DBL_PTR(_sym_48995)) && (DBL_PTR(_sym_48995)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_48995);
        _sym_48995 = _1;
    }

    /** 	return SymTab[sym][S_NAME]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25836 = (int)*(((s1_ptr)_2)->base + _sym_48995);
    _2 = (int)SEQ_PTR(_25836);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _25837 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _25837 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _25836 = NOVALUE;
    Ref(_25837);
    return _25837;
    ;
}


int _52sym_token(int _sym_49003)
{
    int _25839 = NOVALUE;
    int _25838 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_49003)) {
        _1 = (long)(DBL_PTR(_sym_49003)->dbl);
        if (UNIQUE(DBL_PTR(_sym_49003)) && (DBL_PTR(_sym_49003)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_49003);
        _sym_49003 = _1;
    }

    /** 	return SymTab[sym][S_TOKEN]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25838 = (int)*(((s1_ptr)_2)->base + _sym_49003);
    _2 = (int)SEQ_PTR(_25838);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _25839 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _25839 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _25838 = NOVALUE;
    Ref(_25839);
    return _25839;
    ;
}


int _52sym_scope(int _sym_49011)
{
    int _25841 = NOVALUE;
    int _25840 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_49011)) {
        _1 = (long)(DBL_PTR(_sym_49011)->dbl);
        if (UNIQUE(DBL_PTR(_sym_49011)) && (DBL_PTR(_sym_49011)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_49011);
        _sym_49011 = _1;
    }

    /** 	return SymTab[sym][S_SCOPE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25840 = (int)*(((s1_ptr)_2)->base + _sym_49011);
    _2 = (int)SEQ_PTR(_25840);
    _25841 = (int)*(((s1_ptr)_2)->base + 4);
    _25840 = NOVALUE;
    Ref(_25841);
    return _25841;
    ;
}


int _52sym_mode(int _sym_49019)
{
    int _25843 = NOVALUE;
    int _25842 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_49019)) {
        _1 = (long)(DBL_PTR(_sym_49019)->dbl);
        if (UNIQUE(DBL_PTR(_sym_49019)) && (DBL_PTR(_sym_49019)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_49019);
        _sym_49019 = _1;
    }

    /** 	return SymTab[sym][S_MODE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25842 = (int)*(((s1_ptr)_2)->base + _sym_49019);
    _2 = (int)SEQ_PTR(_25842);
    _25843 = (int)*(((s1_ptr)_2)->base + 3);
    _25842 = NOVALUE;
    Ref(_25843);
    return _25843;
    ;
}


int _52sym_obj(int _sym_49027)
{
    int _25845 = NOVALUE;
    int _25844 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_49027)) {
        _1 = (long)(DBL_PTR(_sym_49027)->dbl);
        if (UNIQUE(DBL_PTR(_sym_49027)) && (DBL_PTR(_sym_49027)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_49027);
        _sym_49027 = _1;
    }

    /** 	return SymTab[sym][S_OBJ]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25844 = (int)*(((s1_ptr)_2)->base + _sym_49027);
    _2 = (int)SEQ_PTR(_25844);
    _25845 = (int)*(((s1_ptr)_2)->base + 1);
    _25844 = NOVALUE;
    Ref(_25845);
    return _25845;
    ;
}


int _52sym_next(int _sym_49035)
{
    int _25847 = NOVALUE;
    int _25846 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_49035)) {
        _1 = (long)(DBL_PTR(_sym_49035)->dbl);
        if (UNIQUE(DBL_PTR(_sym_49035)) && (DBL_PTR(_sym_49035)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_49035);
        _sym_49035 = _1;
    }

    /** 	return SymTab[sym][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25846 = (int)*(((s1_ptr)_2)->base + _sym_49035);
    _2 = (int)SEQ_PTR(_25846);
    _25847 = (int)*(((s1_ptr)_2)->base + 2);
    _25846 = NOVALUE;
    Ref(_25847);
    return _25847;
    ;
}


int _52sym_block(int _sym_49043)
{
    int _25849 = NOVALUE;
    int _25848 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_49043)) {
        _1 = (long)(DBL_PTR(_sym_49043)->dbl);
        if (UNIQUE(DBL_PTR(_sym_49043)) && (DBL_PTR(_sym_49043)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_49043);
        _sym_49043 = _1;
    }

    /** 	return SymTab[sym][S_BLOCK]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25848 = (int)*(((s1_ptr)_2)->base + _sym_49043);
    _2 = (int)SEQ_PTR(_25848);
    if (!IS_ATOM_INT(_25S_BLOCK_11933)){
        _25849 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_BLOCK_11933)->dbl));
    }
    else{
        _25849 = (int)*(((s1_ptr)_2)->base + _25S_BLOCK_11933);
    }
    _25848 = NOVALUE;
    Ref(_25849);
    return _25849;
    ;
}


int _52sym_next_in_block(int _sym_49051)
{
    int _25851 = NOVALUE;
    int _25850 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_49051)) {
        _1 = (long)(DBL_PTR(_sym_49051)->dbl);
        if (UNIQUE(DBL_PTR(_sym_49051)) && (DBL_PTR(_sym_49051)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_49051);
        _sym_49051 = _1;
    }

    /** 	return SymTab[sym][S_NEXT_IN_BLOCK]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25850 = (int)*(((s1_ptr)_2)->base + _sym_49051);
    _2 = (int)SEQ_PTR(_25850);
    if (!IS_ATOM_INT(_25S_NEXT_IN_BLOCK_11905)){
        _25851 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NEXT_IN_BLOCK_11905)->dbl));
    }
    else{
        _25851 = (int)*(((s1_ptr)_2)->base + _25S_NEXT_IN_BLOCK_11905);
    }
    _25850 = NOVALUE;
    Ref(_25851);
    return _25851;
    ;
}


int _52sym_usage(int _sym_49059)
{
    int _25853 = NOVALUE;
    int _25852 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_49059)) {
        _1 = (long)(DBL_PTR(_sym_49059)->dbl);
        if (UNIQUE(DBL_PTR(_sym_49059)) && (DBL_PTR(_sym_49059)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_49059);
        _sym_49059 = _1;
    }

    /** 	return SymTab[sym][S_USAGE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25852 = (int)*(((s1_ptr)_2)->base + _sym_49059);
    _2 = (int)SEQ_PTR(_25852);
    _25853 = (int)*(((s1_ptr)_2)->base + 5);
    _25852 = NOVALUE;
    Ref(_25853);
    return _25853;
    ;
}


int _52calc_stack_required(int _sub_49067)
{
    int _required_49068 = NOVALUE;
    int _arg_49073 = NOVALUE;
    int _25873 = NOVALUE;
    int _25869 = NOVALUE;
    int _25867 = NOVALUE;
    int _25865 = NOVALUE;
    int _25864 = NOVALUE;
    int _25863 = NOVALUE;
    int _25862 = NOVALUE;
    int _25861 = NOVALUE;
    int _25859 = NOVALUE;
    int _25858 = NOVALUE;
    int _25856 = NOVALUE;
    int _25854 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sub_49067)) {
        _1 = (long)(DBL_PTR(_sub_49067)->dbl);
        if (UNIQUE(DBL_PTR(_sub_49067)) && (DBL_PTR(_sub_49067)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sub_49067);
        _sub_49067 = _1;
    }

    /** 	integer required = SymTab[sub][S_NUM_ARGS]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25854 = (int)*(((s1_ptr)_2)->base + _sub_49067);
    _2 = (int)SEQ_PTR(_25854);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _required_49068 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _required_49068 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    if (!IS_ATOM_INT(_required_49068)){
        _required_49068 = (long)DBL_PTR(_required_49068)->dbl;
    }
    _25854 = NOVALUE;

    /** 	integer arg = SymTab[sub][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25856 = (int)*(((s1_ptr)_2)->base + _sub_49067);
    _2 = (int)SEQ_PTR(_25856);
    _arg_49073 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_arg_49073)){
        _arg_49073 = (long)DBL_PTR(_arg_49073)->dbl;
    }
    _25856 = NOVALUE;

    /** 	for i = 1 to required do*/
    _25858 = _required_49068;
    {
        int _i_49079;
        _i_49079 = 1;
L1: 
        if (_i_49079 > _25858){
            goto L2; // [40] 70
        }

        /** 		arg = SymTab[arg][S_NEXT]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _25859 = (int)*(((s1_ptr)_2)->base + _arg_49073);
        _2 = (int)SEQ_PTR(_25859);
        _arg_49073 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_arg_49073)){
            _arg_49073 = (long)DBL_PTR(_arg_49073)->dbl;
        }
        _25859 = NOVALUE;

        /** 	end for*/
        _i_49079 = _i_49079 + 1;
        goto L1; // [65] 47
L2: 
        ;
    }

    /** 	while arg != 0 and SymTab[arg][S_SCOPE] <= SC_PRIVATE do*/
L3: 
    _25861 = (_arg_49073 != 0);
    if (_25861 == 0) {
        goto L4; // [79] 132
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25863 = (int)*(((s1_ptr)_2)->base + _arg_49073);
    _2 = (int)SEQ_PTR(_25863);
    _25864 = (int)*(((s1_ptr)_2)->base + 4);
    _25863 = NOVALUE;
    if (IS_ATOM_INT(_25864)) {
        _25865 = (_25864 <= 3);
    }
    else {
        _25865 = binary_op(LESSEQ, _25864, 3);
    }
    _25864 = NOVALUE;
    if (_25865 <= 0) {
        if (_25865 == 0) {
            DeRef(_25865);
            _25865 = NOVALUE;
            goto L4; // [102] 132
        }
        else {
            if (!IS_ATOM_INT(_25865) && DBL_PTR(_25865)->dbl == 0.0){
                DeRef(_25865);
                _25865 = NOVALUE;
                goto L4; // [102] 132
            }
            DeRef(_25865);
            _25865 = NOVALUE;
        }
    }
    DeRef(_25865);
    _25865 = NOVALUE;

    /** 		required += 1*/
    _required_49068 = _required_49068 + 1;

    /** 		arg = SymTab[arg][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25867 = (int)*(((s1_ptr)_2)->base + _arg_49073);
    _2 = (int)SEQ_PTR(_25867);
    _arg_49073 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_arg_49073)){
        _arg_49073 = (long)DBL_PTR(_arg_49073)->dbl;
    }
    _25867 = NOVALUE;

    /** 	end while*/
    goto L3; // [129] 75
L4: 

    /** 	arg = SymTab[sub][S_TEMPS]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25869 = (int)*(((s1_ptr)_2)->base + _sub_49067);
    _2 = (int)SEQ_PTR(_25869);
    if (!IS_ATOM_INT(_25S_TEMPS_11958)){
        _arg_49073 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TEMPS_11958)->dbl));
    }
    else{
        _arg_49073 = (int)*(((s1_ptr)_2)->base + _25S_TEMPS_11958);
    }
    if (!IS_ATOM_INT(_arg_49073)){
        _arg_49073 = (long)DBL_PTR(_arg_49073)->dbl;
    }
    _25869 = NOVALUE;

    /** 	while arg != 0 do*/
L5: 
    if (_arg_49073 == 0)
    goto L6; // [153] 184

    /** 		required += 1*/
    _required_49068 = _required_49068 + 1;

    /** 		arg = SymTab[arg][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _25873 = (int)*(((s1_ptr)_2)->base + _arg_49073);
    _2 = (int)SEQ_PTR(_25873);
    _arg_49073 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_arg_49073)){
        _arg_49073 = (long)DBL_PTR(_arg_49073)->dbl;
    }
    _25873 = NOVALUE;

    /** 	end while*/
    goto L5; // [181] 153
L6: 

    /** 	return required*/
    DeRef(_25861);
    _25861 = NOVALUE;
    return _required_49068;
    ;
}



// 0x5AE8B0EB
