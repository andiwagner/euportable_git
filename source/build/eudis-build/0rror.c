// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _43screen_output(int _f_49543, int _msg_49544)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_f_49543)) {
        _1 = (long)(DBL_PTR(_f_49543)->dbl);
        if (UNIQUE(DBL_PTR(_f_49543)) && (DBL_PTR(_f_49543)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_f_49543);
        _f_49543 = _1;
    }

    /** 	puts(f, msg)*/
    EPuts(_f_49543, _msg_49544); // DJP 

    /** end procedure*/
    DeRefDS(_msg_49544);
    return;
    ;
}


void _43Warning(int _msg_49547, int _mask_49548, int _args_49549)
{
    int _orig_mask_49550 = NOVALUE;
    int _text_49551 = NOVALUE;
    int _w_name_49552 = NOVALUE;
    int _26086 = NOVALUE;
    int _26084 = NOVALUE;
    int _26081 = NOVALUE;
    int _26078 = NOVALUE;
    int _26073 = NOVALUE;
    int _26071 = NOVALUE;
    int _26070 = NOVALUE;
    int _26069 = NOVALUE;
    int _26068 = NOVALUE;
    int _26066 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_mask_49548)) {
        _1 = (long)(DBL_PTR(_mask_49548)->dbl);
        if (UNIQUE(DBL_PTR(_mask_49548)) && (DBL_PTR(_mask_49548)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mask_49548);
        _mask_49548 = _1;
    }

    /** 	if display_warnings = 0 then*/
    if (_43display_warnings_49531 != 0)
    goto L1; // [9] 19

    /** 		return*/
    DeRef(_msg_49547);
    DeRefDS(_args_49549);
    DeRef(_text_49551);
    DeRef(_w_name_49552);
    return;
L1: 

    /** 	if not Strict_is_on or Strict_Override then*/
    _26066 = (_25Strict_is_on_12328 == 0);
    if (_26066 != 0) {
        goto L2; // [26] 37
    }
    if (_25Strict_Override_12329 == 0)
    {
        goto L3; // [33] 56
    }
    else{
    }
L2: 

    /** 		if find(mask, strict_only_warnings) then*/
    _26068 = find_from(_mask_49548, _25strict_only_warnings_12326, 1);
    if (_26068 == 0)
    {
        _26068 = NOVALUE;
        goto L4; // [46] 55
    }
    else{
        _26068 = NOVALUE;
    }

    /** 			return*/
    DeRef(_msg_49547);
    DeRefDS(_args_49549);
    DeRef(_text_49551);
    DeRef(_w_name_49552);
    DeRef(_26066);
    _26066 = NOVALUE;
    return;
L4: 
L3: 

    /** 	orig_mask = mask -- =0 for non maskable warnings - none implemented so far*/
    _orig_mask_49550 = _mask_49548;

    /** 	if Strict_is_on and Strict_Override = 0 then*/
    if (_25Strict_is_on_12328 == 0) {
        goto L5; // [65] 85
    }
    _26070 = (_25Strict_Override_12329 == 0);
    if (_26070 == 0)
    {
        DeRef(_26070);
        _26070 = NOVALUE;
        goto L5; // [76] 85
    }
    else{
        DeRef(_26070);
        _26070 = NOVALUE;
    }

    /** 		mask = 0*/
    _mask_49548 = 0;
L5: 

    /** 	if mask = 0 or and_bits(OpWarning, mask) then*/
    _26071 = (_mask_49548 == 0);
    if (_26071 != 0) {
        goto L6; // [91] 106
    }
    {unsigned long tu;
         tu = (unsigned long)_25OpWarning_12330 & (unsigned long)_mask_49548;
         _26073 = MAKE_UINT(tu);
    }
    if (_26073 == 0) {
        DeRef(_26073);
        _26073 = NOVALUE;
        goto L7; // [102] 213
    }
    else {
        if (!IS_ATOM_INT(_26073) && DBL_PTR(_26073)->dbl == 0.0){
            DeRef(_26073);
            _26073 = NOVALUE;
            goto L7; // [102] 213
        }
        DeRef(_26073);
        _26073 = NOVALUE;
    }
    DeRef(_26073);
    _26073 = NOVALUE;
L6: 

    /** 		if orig_mask != 0 then*/
    if (_orig_mask_49550 == 0)
    goto L8; // [108] 122

    /** 			orig_mask = find(orig_mask,warning_flags)*/
    _orig_mask_49550 = find_from(_orig_mask_49550, _25warning_flags_12305, 1);
L8: 

    /** 		if orig_mask != 0 then*/
    if (_orig_mask_49550 == 0)
    goto L9; // [124] 145

    /** 			w_name = "{ " & warning_names[orig_mask] & " }"*/
    _2 = (int)SEQ_PTR(_25warning_names_12307);
    _26078 = (int)*(((s1_ptr)_2)->base + _orig_mask_49550);
    {
        int concat_list[3];

        concat_list[0] = _26079;
        concat_list[1] = _26078;
        concat_list[2] = _26077;
        Concat_N((object_ptr)&_w_name_49552, concat_list, 3);
    }
    _26078 = NOVALUE;
    goto LA; // [142] 153
L9: 

    /** 			w_name = "" -- not maskable*/
    RefDS(_22682);
    DeRef(_w_name_49552);
    _w_name_49552 = _22682;
LA: 

    /** 		if atom(msg) then*/
    _26081 = IS_ATOM(_msg_49547);
    if (_26081 == 0)
    {
        _26081 = NOVALUE;
        goto LB; // [158] 170
    }
    else{
        _26081 = NOVALUE;
    }

    /** 			msg = GetMsgText(msg, 1, args)*/
    Ref(_msg_49547);
    RefDS(_args_49549);
    _0 = _msg_49547;
    _msg_49547 = _44GetMsgText(_msg_49547, 1, _args_49549);
    DeRef(_0);
LB: 

    /** 		text = GetMsgText(204, 0, {w_name, msg})*/
    Ref(_msg_49547);
    RefDS(_w_name_49552);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _w_name_49552;
    ((int *)_2)[2] = _msg_49547;
    _26084 = MAKE_SEQ(_1);
    _0 = _text_49551;
    _text_49551 = _44GetMsgText(204, 0, _26084);
    DeRef(_0);
    _26084 = NOVALUE;

    /** 		if find(text, warning_list) then*/
    _26086 = find_from(_text_49551, _43warning_list_49540, 1);
    if (_26086 == 0)
    {
        _26086 = NOVALUE;
        goto LC; // [195] 204
    }
    else{
        _26086 = NOVALUE;
    }

    /** 			return -- duplicate*/
    DeRef(_msg_49547);
    DeRefDS(_args_49549);
    DeRefDS(_text_49551);
    DeRefDS(_w_name_49552);
    DeRef(_26066);
    _26066 = NOVALUE;
    DeRef(_26071);
    _26071 = NOVALUE;
    return;
LC: 

    /** 		warning_list = append(warning_list, text)*/
    RefDS(_text_49551);
    Append(&_43warning_list_49540, _43warning_list_49540, _text_49551);
L7: 

    /** end procedure*/
    DeRef(_msg_49547);
    DeRefDS(_args_49549);
    DeRef(_text_49551);
    DeRef(_w_name_49552);
    DeRef(_26066);
    _26066 = NOVALUE;
    DeRef(_26071);
    _26071 = NOVALUE;
    return;
    ;
}


void _43Log_warnings(int _policy_49598)
{
    int _26094 = NOVALUE;
    int _26093 = NOVALUE;
    int _26092 = NOVALUE;
    int _26091 = NOVALUE;
    int _26089 = NOVALUE;
    int _26088 = NOVALUE;
    int _0, _1, _2;
    

    /** 	display_warnings = 1*/
    _43display_warnings_49531 = 1;

    /** 	if sequence(policy) then*/
    _26088 = IS_SEQUENCE(_policy_49598);
    if (_26088 == 0)
    {
        _26088 = NOVALUE;
        goto L1; // [11] 34
    }
    else{
        _26088 = NOVALUE;
    }

    /** 		if length(policy)=0 then*/
    if (IS_SEQUENCE(_policy_49598)){
            _26089 = SEQ_PTR(_policy_49598)->length;
    }
    else {
        _26089 = 1;
    }
    if (_26089 != 0)
    goto L2; // [19] 82

    /** 			policy = STDERR*/
    DeRef(_policy_49598);
    _policy_49598 = 2;
    goto L2; // [31] 82
L1: 

    /** 		if policy >= 0 and policy < STDERR+1 then*/
    if (IS_ATOM_INT(_policy_49598)) {
        _26091 = (_policy_49598 >= 0);
    }
    else {
        _26091 = binary_op(GREATEREQ, _policy_49598, 0);
    }
    if (IS_ATOM_INT(_26091)) {
        if (_26091 == 0) {
            goto L3; // [40] 68
        }
    }
    else {
        if (DBL_PTR(_26091)->dbl == 0.0) {
            goto L3; // [40] 68
        }
    }
    _26093 = 3;
    if (IS_ATOM_INT(_policy_49598)) {
        _26094 = (_policy_49598 < 3);
    }
    else {
        _26094 = binary_op(LESS, _policy_49598, 3);
    }
    _26093 = NOVALUE;
    if (_26094 == 0) {
        DeRef(_26094);
        _26094 = NOVALUE;
        goto L3; // [55] 68
    }
    else {
        if (!IS_ATOM_INT(_26094) && DBL_PTR(_26094)->dbl == 0.0){
            DeRef(_26094);
            _26094 = NOVALUE;
            goto L3; // [55] 68
        }
        DeRef(_26094);
        _26094 = NOVALUE;
    }
    DeRef(_26094);
    _26094 = NOVALUE;

    /** 			policy  = STDERR*/
    DeRef(_policy_49598);
    _policy_49598 = 2;
    goto L4; // [65] 81
L3: 

    /** 		elsif policy < 0 then*/
    if (binary_op_a(GREATEREQ, _policy_49598, 0)){
        goto L5; // [70] 80
    }

    /** 			display_warnings = 0*/
    _43display_warnings_49531 = 0;
L5: 
L4: 
L2: 

    /** end procedure*/
    DeRef(_policy_49598);
    DeRef(_26091);
    _26091 = NOVALUE;
    return;
    ;
}


int _43ShowWarnings()
{
    int _c_49617 = NOVALUE;
    int _errfile_49618 = NOVALUE;
    int _twf_49619 = NOVALUE;
    int _26127 = NOVALUE;
    int _26123 = NOVALUE;
    int _26122 = NOVALUE;
    int _26121 = NOVALUE;
    int _26120 = NOVALUE;
    int _26119 = NOVALUE;
    int _26118 = NOVALUE;
    int _26116 = NOVALUE;
    int _26115 = NOVALUE;
    int _26114 = NOVALUE;
    int _26112 = NOVALUE;
    int _26111 = NOVALUE;
    int _26110 = NOVALUE;
    int _26109 = NOVALUE;
    int _26107 = NOVALUE;
    int _26102 = NOVALUE;
    int _26100 = NOVALUE;
    int _26099 = NOVALUE;
    int _26098 = NOVALUE;
    int _26096 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if display_warnings = 0 or length(warning_list) = 0 then*/
    _26096 = (_43display_warnings_49531 == 0);
    if (_26096 != 0) {
        goto L1; // [9] 27
    }
    if (IS_SEQUENCE(_43warning_list_49540)){
            _26098 = SEQ_PTR(_43warning_list_49540)->length;
    }
    else {
        _26098 = 1;
    }
    _26099 = (_26098 == 0);
    _26098 = NOVALUE;
    if (_26099 == 0)
    {
        DeRef(_26099);
        _26099 = NOVALUE;
        goto L2; // [23] 39
    }
    else{
        DeRef(_26099);
        _26099 = NOVALUE;
    }
L1: 

    /** 		return length(warning_list)*/
    if (IS_SEQUENCE(_43warning_list_49540)){
            _26100 = SEQ_PTR(_43warning_list_49540)->length;
    }
    else {
        _26100 = 1;
    }
    DeRef(_26096);
    _26096 = NOVALUE;
    return _26100;
L2: 

    /** 	if TempErrFile > 0 then*/
    if (_43TempErrFile_49529 <= 0)
    goto L3; // [43] 57

    /** 		errfile = TempErrFile*/
    _errfile_49618 = _43TempErrFile_49529;
    goto L4; // [54] 67
L3: 

    /** 		errfile = STDERR*/
    _errfile_49618 = 2;
L4: 

    /** 	if not integer(TempWarningName) then*/
    if (IS_ATOM_INT(_25TempWarningName_12276))
    _26102 = 1;
    else if (IS_ATOM_DBL(_25TempWarningName_12276))
    _26102 = IS_ATOM_INT(DoubleToInt(_25TempWarningName_12276));
    else
    _26102 = 0;
    if (_26102 != 0)
    goto L5; // [74] 179
    _26102 = NOVALUE;

    /** 		twf = open(TempWarningName,"w")*/
    _twf_49619 = EOpen(_25TempWarningName_12276, _22788, 0);

    /** 		if twf = -1 then*/
    if (_twf_49619 != -1)
    goto L6; // [88] 136

    /** 			ShowMsg(errfile, 205, {TempWarningName})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_25TempWarningName_12276);
    *((int *)(_2+4)) = _25TempWarningName_12276;
    _26107 = MAKE_SEQ(_1);
    _44ShowMsg(_errfile_49618, 205, _26107, 1);
    _26107 = NOVALUE;

    /** 			if errfile != STDERR then*/
    if (_errfile_49618 == 2)
    goto L7; // [112] 173

    /** 				ShowMsg(STDERR, 205, {TempWarningName})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_25TempWarningName_12276);
    *((int *)(_2+4)) = _25TempWarningName_12276;
    _26109 = MAKE_SEQ(_1);
    _44ShowMsg(2, 205, _26109, 1);
    _26109 = NOVALUE;
    goto L7; // [133] 173
L6: 

    /** 			for i = 1 to length(warning_list) do*/
    if (IS_SEQUENCE(_43warning_list_49540)){
            _26110 = SEQ_PTR(_43warning_list_49540)->length;
    }
    else {
        _26110 = 1;
    }
    {
        int _i_49651;
        _i_49651 = 1;
L8: 
        if (_i_49651 > _26110){
            goto L9; // [143] 168
        }

        /** 				puts(twf, warning_list[i])*/
        _2 = (int)SEQ_PTR(_43warning_list_49540);
        _26111 = (int)*(((s1_ptr)_2)->base + _i_49651);
        EPuts(_twf_49619, _26111); // DJP 
        _26111 = NOVALUE;

        /** 			end for*/
        _i_49651 = _i_49651 + 1;
        goto L8; // [163] 150
L9: 
        ;
    }

    /** 		    close(twf)*/
    EClose(_twf_49619);
L7: 

    /** 		TempWarningName = 99 -- Flag that we have done this already.*/
    DeRef(_25TempWarningName_12276);
    _25TempWarningName_12276 = 99;
L5: 

    /** 	if batch_job = 0 or errfile != STDERR then*/
    _26112 = (_25batch_job_12275 == 0);
    if (_26112 != 0) {
        goto LA; // [187] 204
    }
    _26114 = (_errfile_49618 != 2);
    if (_26114 == 0)
    {
        DeRef(_26114);
        _26114 = NOVALUE;
        goto LB; // [200] 311
    }
    else{
        DeRef(_26114);
        _26114 = NOVALUE;
    }
LA: 

    /** 		for i = 1 to length(warning_list) do*/
    if (IS_SEQUENCE(_43warning_list_49540)){
            _26115 = SEQ_PTR(_43warning_list_49540)->length;
    }
    else {
        _26115 = 1;
    }
    {
        int _i_49662;
        _i_49662 = 1;
LC: 
        if (_i_49662 > _26115){
            goto LD; // [211] 310
        }

        /** 			puts(errfile, warning_list[i])*/
        _2 = (int)SEQ_PTR(_43warning_list_49540);
        _26116 = (int)*(((s1_ptr)_2)->base + _i_49662);
        EPuts(_errfile_49618, _26116); // DJP 
        _26116 = NOVALUE;

        /** 			if errfile = STDERR then*/
        if (_errfile_49618 != 2)
        goto LE; // [235] 303

        /** 				if remainder(i, 20) = 0 and batch_job = 0 and test_only = 0 then*/
        _26118 = (_i_49662 % 20);
        _26119 = (_26118 == 0);
        _26118 = NOVALUE;
        if (_26119 == 0) {
            _26120 = 0;
            goto LF; // [249] 263
        }
        _26121 = (_25batch_job_12275 == 0);
        _26120 = (_26121 != 0);
LF: 
        if (_26120 == 0) {
            goto L10; // [263] 302
        }
        _26123 = (_25test_only_12274 == 0);
        if (_26123 == 0)
        {
            DeRef(_26123);
            _26123 = NOVALUE;
            goto L10; // [274] 302
        }
        else{
            DeRef(_26123);
            _26123 = NOVALUE;
        }

        /** 					ShowMsg(errfile, 206)*/
        RefDS(_22682);
        _44ShowMsg(_errfile_49618, 206, _22682, 1);

        /** 					c = getc(0)*/
        if (0 != last_r_file_no) {
            last_r_file_ptr = which_file(0, EF_READ);
            last_r_file_no = 0;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _c_49617 = getKBchar();
            }
            else
            _c_49617 = getc(last_r_file_ptr);
        }
        else
        _c_49617 = getc(last_r_file_ptr);

        /** 					if c = 'q' then*/
        if (_c_49617 != 113)
        goto L11; // [292] 301

        /** 						exit*/
        goto LD; // [298] 310
L11: 
L10: 
LE: 

        /** 		end for*/
        _i_49662 = _i_49662 + 1;
        goto LC; // [305] 218
LD: 
        ;
    }
LB: 

    /** 	return length(warning_list)*/
    if (IS_SEQUENCE(_43warning_list_49540)){
            _26127 = SEQ_PTR(_43warning_list_49540)->length;
    }
    else {
        _26127 = 1;
    }
    DeRef(_26096);
    _26096 = NOVALUE;
    DeRef(_26112);
    _26112 = NOVALUE;
    DeRef(_26121);
    _26121 = NOVALUE;
    DeRef(_26119);
    _26119 = NOVALUE;
    return _26127;
    ;
}


void _43ShowDefines(int _errfile_49685)
{
    int _c_49686 = NOVALUE;
    int _26142 = NOVALUE;
    int _26141 = NOVALUE;
    int _26139 = NOVALUE;
    int _26138 = NOVALUE;
    int _26135 = NOVALUE;
    int _26134 = NOVALUE;
    int _26133 = NOVALUE;
    int _26132 = NOVALUE;
    int _26131 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_errfile_49685)) {
        _1 = (long)(DBL_PTR(_errfile_49685)->dbl);
        if (UNIQUE(DBL_PTR(_errfile_49685)) && (DBL_PTR(_errfile_49685)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_errfile_49685);
        _errfile_49685 = _1;
    }

    /** 	if errfile=0 then*/
    if (_errfile_49685 != 0)
    goto L1; // [5] 19

    /** 		errfile = STDERR*/
    _errfile_49685 = 2;
L1: 

    /** 	puts(errfile, format("\n--- [1] ---\n", {GetMsgText(207,0)}))*/
    RefDS(_22682);
    _26131 = _44GetMsgText(207, 0, _22682);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _26131;
    _26132 = MAKE_SEQ(_1);
    _26131 = NOVALUE;
    RefDS(_26129);
    _26133 = _4format(_26129, _26132);
    _26132 = NOVALUE;
    EPuts(_errfile_49685, _26133); // DJP 
    DeRef(_26133);
    _26133 = NOVALUE;

    /** 	for i = 1 to length(OpDefines) do*/
    if (IS_SEQUENCE(_25OpDefines_12336)){
            _26134 = SEQ_PTR(_25OpDefines_12336)->length;
    }
    else {
        _26134 = 1;
    }
    {
        int _i_49698;
        _i_49698 = 1;
L2: 
        if (_i_49698 > _26134){
            goto L3; // [46] 98
        }

        /** 		if find(OpDefines[i], {"_PLAT_START", "_PLAT_STOP"}) = 0 then*/
        _2 = (int)SEQ_PTR(_25OpDefines_12336);
        _26135 = (int)*(((s1_ptr)_2)->base + _i_49698);
        RefDS(_26137);
        RefDS(_26136);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _26136;
        ((int *)_2)[2] = _26137;
        _26138 = MAKE_SEQ(_1);
        _26139 = find_from(_26135, _26138, 1);
        _26135 = NOVALUE;
        DeRefDS(_26138);
        _26138 = NOVALUE;
        if (_26139 != 0)
        goto L4; // [70] 91

        /** 			printf(errfile, "%s\n", {OpDefines[i]})*/
        _2 = (int)SEQ_PTR(_25OpDefines_12336);
        _26141 = (int)*(((s1_ptr)_2)->base + _i_49698);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_26141);
        *((int *)(_2+4)) = _26141;
        _26142 = MAKE_SEQ(_1);
        _26141 = NOVALUE;
        EPrintf(_errfile_49685, _26007, _26142);
        DeRefDS(_26142);
        _26142 = NOVALUE;
L4: 

        /** 	end for*/
        _i_49698 = _i_49698 + 1;
        goto L2; // [93] 53
L3: 
        ;
    }

    /** 	puts(errfile, "-------------------\n")*/
    EPuts(_errfile_49685, _26143); // DJP 

    /** end procedure*/
    return;
    ;
}


void _43Cleanup(int _status_49715)
{
    int _w_49716 = NOVALUE;
    int _show_error_49717 = NOVALUE;
    int _26158 = NOVALUE;
    int _26157 = NOVALUE;
    int _26155 = NOVALUE;
    int _26154 = NOVALUE;
    int _26153 = NOVALUE;
    int _26152 = NOVALUE;
    int _26151 = NOVALUE;
    int _26150 = NOVALUE;
    int _26149 = NOVALUE;
    int _26148 = NOVALUE;
    int _26144 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_status_49715)) {
        _1 = (long)(DBL_PTR(_status_49715)->dbl);
        if (UNIQUE(DBL_PTR(_status_49715)) && (DBL_PTR(_status_49715)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_status_49715);
        _status_49715 = _1;
    }

    /** 	integer w, show_error = 0*/
    _show_error_49717 = 0;

    /** 	ifdef EU_EX then*/

    /** 	show_error = 1*/
    _show_error_49717 = 1;

    /** 	if object(src_file) = 0 then*/
    if( NOVALUE == _25src_file_12404 ){
        _26144 = 0;
    }
    else{
        _26144 = 1;
    }
    if (_26144 != 0)
    goto L1; // [20] 34

    /** 		src_file = -1*/
    _25src_file_12404 = -1;
    goto L2; // [31] 57
L1: 

    /** 	elsif src_file >= 0 then*/
    if (_25src_file_12404 < 0)
    goto L3; // [38] 56

    /** 		close(src_file)*/
    EClose(_25src_file_12404);

    /** 		src_file = -1*/
    _25src_file_12404 = -1;
L3: 
L2: 

    /** 	w = ShowWarnings()*/
    _w_49716 = _43ShowWarnings();
    if (!IS_ATOM_INT(_w_49716)) {
        _1 = (long)(DBL_PTR(_w_49716)->dbl);
        if (UNIQUE(DBL_PTR(_w_49716)) && (DBL_PTR(_w_49716)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_w_49716);
        _w_49716 = _1;
    }

    /** 	if not TRANSLATE and (BIND or show_error) and (w or Errors) then*/
    _26148 = (_25TRANSLATE_11874 == 0);
    if (_26148 == 0) {
        _26149 = 0;
        goto L4; // [71] 89
    }
    if (_25BIND_11877 != 0) {
        _26150 = 1;
        goto L5; // [77] 85
    }
    _26150 = (_show_error_49717 != 0);
L5: 
    _26149 = (_26150 != 0);
L4: 
    if (_26149 == 0) {
        goto L6; // [89] 148
    }
    if (_w_49716 != 0) {
        DeRef(_26152);
        _26152 = 1;
        goto L7; // [93] 103
    }
    _26152 = (_43Errors_49528 != 0);
L7: 
    if (_26152 == 0)
    {
        _26152 = NOVALUE;
        goto L6; // [104] 148
    }
    else{
        _26152 = NOVALUE;
    }

    /** 		if not batch_job and not test_only then*/
    _26153 = (_25batch_job_12275 == 0);
    if (_26153 == 0) {
        goto L8; // [114] 147
    }
    _26155 = (_25test_only_12274 == 0);
    if (_26155 == 0)
    {
        DeRef(_26155);
        _26155 = NOVALUE;
        goto L8; // [124] 147
    }
    else{
        DeRef(_26155);
        _26155 = NOVALUE;
    }

    /** 			screen_output(STDERR, GetMsgText(208,0))*/
    RefDS(_22682);
    _26157 = _44GetMsgText(208, 0, _22682);
    _43screen_output(2, _26157);
    _26157 = NOVALUE;

    /** 			getc(0) -- wait*/
    if (0 != last_r_file_no) {
        last_r_file_ptr = which_file(0, EF_READ);
        last_r_file_no = 0;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _26158 = getKBchar();
        }
        else
        _26158 = getc(last_r_file_ptr);
    }
    else
    _26158 = getc(last_r_file_ptr);
L8: 
L6: 

    /** 	cleanup_open_includes()*/
    _60cleanup_open_includes();

    /** 	abort(status)*/
    UserCleanup(_status_49715);

    /** end procedure*/
    DeRef(_26148);
    _26148 = NOVALUE;
    DeRef(_26153);
    _26153 = NOVALUE;
    return;
    ;
}


void _43OpenErrFile()
{
    int _26166 = NOVALUE;
    int _26165 = NOVALUE;
    int _26162 = NOVALUE;
    int _0, _1, _2;
    

    /**     if TempErrFile != -1 then*/
    if (_43TempErrFile_49529 == -1)
    goto L1; // [5] 19

    /** 		TempErrFile = open(TempErrName, "w")*/
    _43TempErrFile_49529 = EOpen(_43TempErrName_49530, _22788, 0);
L1: 

    /** 	if TempErrFile = -1 then*/
    if (_43TempErrFile_49529 != -1)
    goto L2; // [23] 64

    /** 		if length(TempErrName) > 0 then*/
    _26162 = 6;

    /** 			screen_output(STDERR, GetMsgText(209, 0, {TempErrName}))*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_43TempErrName_49530);
    *((int *)(_2+4)) = _43TempErrName_49530;
    _26165 = MAKE_SEQ(_1);
    _26166 = _44GetMsgText(209, 0, _26165);
    _26165 = NOVALUE;
    _43screen_output(2, _26166);
    _26166 = NOVALUE;

    /** 		abort(1) -- with no clean up*/
    UserCleanup(1);
L2: 

    /** end procedure*/
    return;
    ;
}


void _43ShowErr(int _f_49766)
{
    int _msg_inlined_screen_output_at_41_49779 = NOVALUE;
    int _26174 = NOVALUE;
    int _26173 = NOVALUE;
    int _26172 = NOVALUE;
    int _26169 = NOVALUE;
    int _26167 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(known_files) = 0 then*/
    if (IS_SEQUENCE(_26known_files_11139)){
            _26167 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _26167 = 1;
    }
    if (_26167 != 0)
    goto L1; // [10] 20

    /** 		return*/
    return;
L1: 

    /** 	if ThisLine[1] = END_OF_FILE_CHAR then*/
    _2 = (int)SEQ_PTR(_43ThisLine_49532);
    _26169 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _26169, 26)){
        _26169 = NOVALUE;
        goto L2; // [30] 62
    }
    _26169 = NOVALUE;

    /** 		screen_output(f, GetMsgText(210,0))*/
    RefDS(_22682);
    _26172 = _44GetMsgText(210, 0, _22682);
    DeRef(_msg_inlined_screen_output_at_41_49779);
    _msg_inlined_screen_output_at_41_49779 = _26172;
    _26172 = NOVALUE;

    /** 	puts(f, msg)*/
    EPuts(_f_49766, _msg_inlined_screen_output_at_41_49779); // DJP 

    /** end procedure*/
    goto L3; // [54] 57
L3: 
    DeRef(_msg_inlined_screen_output_at_41_49779);
    _msg_inlined_screen_output_at_41_49779 = NOVALUE;
    goto L4; // [59] 79
L2: 

    /** 		screen_output(f, ThisLine)*/

    /** 	puts(f, msg)*/
    EPuts(_f_49766, _43ThisLine_49532); // DJP 

    /** end procedure*/
    goto L5; // [75] 78
L5: 
L4: 

    /** 	for i = 1 to bp-2 do -- bp-1 points to last character read*/
    _26173 = _43bp_49536 - 2;
    if ((long)((unsigned long)_26173 +(unsigned long) HIGH_BITS) >= 0){
        _26173 = NewDouble((double)_26173);
    }
    {
        int _i_49783;
        _i_49783 = 1;
L6: 
        if (binary_op_a(GREATER, _i_49783, _26173)){
            goto L7; // [87] 141
        }

        /** 		if ThisLine[i] = '\t' then*/
        _2 = (int)SEQ_PTR(_43ThisLine_49532);
        if (!IS_ATOM_INT(_i_49783)){
            _26174 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_49783)->dbl));
        }
        else{
            _26174 = (int)*(((s1_ptr)_2)->base + _i_49783);
        }
        if (binary_op_a(NOTEQ, _26174, 9)){
            _26174 = NOVALUE;
            goto L8; // [102] 121
        }
        _26174 = NOVALUE;

        /** 			screen_output(f, "\t")*/

        /** 	puts(f, msg)*/
        EPuts(_f_49766, _24499); // DJP 

        /** end procedure*/
        goto L9; // [115] 134
        goto L9; // [118] 134
L8: 

        /** 			screen_output(f, " ")*/

        /** 	puts(f, msg)*/
        EPuts(_f_49766, _23968); // DJP 

        /** end procedure*/
        goto LA; // [130] 133
LA: 
L9: 

        /** 	end for*/
        _0 = _i_49783;
        if (IS_ATOM_INT(_i_49783)) {
            _i_49783 = _i_49783 + 1;
            if ((long)((unsigned long)_i_49783 +(unsigned long) HIGH_BITS) >= 0){
                _i_49783 = NewDouble((double)_i_49783);
            }
        }
        else {
            _i_49783 = binary_op_a(PLUS, _i_49783, 1);
        }
        DeRef(_0);
        goto L6; // [136] 94
L7: 
        ;
        DeRef(_i_49783);
    }

    /** 	screen_output(f, "^\n\n")*/

    /** 	puts(f, msg)*/
    EPuts(_f_49766, _26176); // DJP 

    /** end procedure*/
    goto LB; // [150] 153
LB: 

    /** end procedure*/
    DeRef(_26173);
    _26173 = NOVALUE;
    return;
    ;
}


void _43CompileErr(int _msg_49795, int _args_49796, int _preproc_49797)
{
    int _errmsg_49798 = NOVALUE;
    int _26197 = NOVALUE;
    int _26193 = NOVALUE;
    int _26192 = NOVALUE;
    int _26191 = NOVALUE;
    int _26190 = NOVALUE;
    int _26189 = NOVALUE;
    int _26188 = NOVALUE;
    int _26186 = NOVALUE;
    int _26185 = NOVALUE;
    int _26183 = NOVALUE;
    int _26182 = NOVALUE;
    int _26181 = NOVALUE;
    int _26177 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_preproc_49797)) {
        _1 = (long)(DBL_PTR(_preproc_49797)->dbl);
        if (UNIQUE(DBL_PTR(_preproc_49797)) && (DBL_PTR(_preproc_49797)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_preproc_49797);
        _preproc_49797 = _1;
    }

    /** 	if integer(msg) then*/
    if (IS_ATOM_INT(_msg_49795))
    _26177 = 1;
    else if (IS_ATOM_DBL(_msg_49795))
    _26177 = IS_ATOM_INT(DoubleToInt(_msg_49795));
    else
    _26177 = 0;
    if (_26177 == 0)
    {
        _26177 = NOVALUE;
        goto L1; // [8] 20
    }
    else{
        _26177 = NOVALUE;
    }

    /** 		msg = GetMsgText(msg)*/
    Ref(_msg_49795);
    RefDS(_22682);
    _0 = _msg_49795;
    _msg_49795 = _44GetMsgText(_msg_49795, 1, _22682);
    DeRef(_0);
L1: 

    /** 	msg = format(msg, args)*/
    Ref(_msg_49795);
    Ref(_args_49796);
    _0 = _msg_49795;
    _msg_49795 = _4format(_msg_49795, _args_49796);
    DeRef(_0);

    /** 	Errors += 1*/
    _43Errors_49528 = _43Errors_49528 + 1;

    /** 	if not preproc and length(known_files) then*/
    _26181 = (_preproc_49797 == 0);
    if (_26181 == 0) {
        goto L2; // [40] 78
    }
    if (IS_SEQUENCE(_26known_files_11139)){
            _26183 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _26183 = 1;
    }
    if (_26183 == 0)
    {
        _26183 = NOVALUE;
        goto L2; // [50] 78
    }
    else{
        _26183 = NOVALUE;
    }

    /** 		errmsg = sprintf("%s:%d\n%s\n", {known_files[current_file_no],*/
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _26185 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_26185);
    *((int *)(_2+4)) = _26185;
    *((int *)(_2+8)) = _25line_number_12263;
    Ref(_msg_49795);
    *((int *)(_2+12)) = _msg_49795;
    _26186 = MAKE_SEQ(_1);
    _26185 = NOVALUE;
    DeRef(_errmsg_49798);
    _errmsg_49798 = EPrintf(-9999999, _26184, _26186);
    DeRefDS(_26186);
    _26186 = NOVALUE;
    goto L3; // [75] 121
L2: 

    /** 		errmsg = msg*/
    Ref(_msg_49795);
    DeRef(_errmsg_49798);
    _errmsg_49798 = _msg_49795;

    /** 		if length(msg) > 0 and msg[$] != '\n' then*/
    if (IS_SEQUENCE(_msg_49795)){
            _26188 = SEQ_PTR(_msg_49795)->length;
    }
    else {
        _26188 = 1;
    }
    _26189 = (_26188 > 0);
    _26188 = NOVALUE;
    if (_26189 == 0) {
        goto L4; // [94] 120
    }
    if (IS_SEQUENCE(_msg_49795)){
            _26191 = SEQ_PTR(_msg_49795)->length;
    }
    else {
        _26191 = 1;
    }
    _2 = (int)SEQ_PTR(_msg_49795);
    _26192 = (int)*(((s1_ptr)_2)->base + _26191);
    if (IS_ATOM_INT(_26192)) {
        _26193 = (_26192 != 10);
    }
    else {
        _26193 = binary_op(NOTEQ, _26192, 10);
    }
    _26192 = NOVALUE;
    if (_26193 == 0) {
        DeRef(_26193);
        _26193 = NOVALUE;
        goto L4; // [110] 120
    }
    else {
        if (!IS_ATOM_INT(_26193) && DBL_PTR(_26193)->dbl == 0.0){
            DeRef(_26193);
            _26193 = NOVALUE;
            goto L4; // [110] 120
        }
        DeRef(_26193);
        _26193 = NOVALUE;
    }
    DeRef(_26193);
    _26193 = NOVALUE;

    /** 			errmsg &= '\n'*/
    Append(&_errmsg_49798, _errmsg_49798, 10);
L4: 
L3: 

    /** 	if not preproc then*/
    if (_preproc_49797 != 0)
    goto L5; // [123] 131

    /** 		OpenErrFile() -- exits if error filename is ""*/
    _43OpenErrFile();
L5: 

    /** 	screen_output(STDERR, errmsg)*/
    RefDS(_errmsg_49798);
    _43screen_output(2, _errmsg_49798);

    /** 	if not preproc then*/
    if (_preproc_49797 != 0)
    goto L6; // [143] 196

    /** 		ShowErr(STDERR)*/
    _43ShowErr(2);

    /** 		puts(TempErrFile, errmsg)*/
    EPuts(_43TempErrFile_49529, _errmsg_49798); // DJP 

    /** 		ShowErr(TempErrFile)*/
    _43ShowErr(_43TempErrFile_49529);

    /** 		ShowWarnings()*/
    _26197 = _43ShowWarnings();

    /** 		ShowDefines(TempErrFile)*/
    _43ShowDefines(_43TempErrFile_49529);

    /** 		close(TempErrFile)*/
    EClose(_43TempErrFile_49529);

    /** 		TempErrFile = -2*/
    _43TempErrFile_49529 = -2;

    /** 		Cleanup(1)*/
    _43Cleanup(1);
L6: 

    /** end procedure*/
    DeRef(_msg_49795);
    DeRef(_args_49796);
    DeRef(_errmsg_49798);
    DeRef(_26181);
    _26181 = NOVALUE;
    DeRef(_26189);
    _26189 = NOVALUE;
    DeRef(_26197);
    _26197 = NOVALUE;
    return;
    ;
}


void _43InternalErr(int _msgno_49841, int _args_49842)
{
    int _msg_49843 = NOVALUE;
    int _26214 = NOVALUE;
    int _26212 = NOVALUE;
    int _26211 = NOVALUE;
    int _26210 = NOVALUE;
    int _26209 = NOVALUE;
    int _26208 = NOVALUE;
    int _26207 = NOVALUE;
    int _26206 = NOVALUE;
    int _26205 = NOVALUE;
    int _26204 = NOVALUE;
    int _26203 = NOVALUE;
    int _26199 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_msgno_49841)) {
        _1 = (long)(DBL_PTR(_msgno_49841)->dbl);
        if (UNIQUE(DBL_PTR(_msgno_49841)) && (DBL_PTR(_msgno_49841)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_msgno_49841);
        _msgno_49841 = _1;
    }

    /** 	if atom(args) then*/
    _26199 = IS_ATOM(_args_49842);
    if (_26199 == 0)
    {
        _26199 = NOVALUE;
        goto L1; // [8] 18
    }
    else{
        _26199 = NOVALUE;
    }

    /** 		args = {args}*/
    _0 = _args_49842;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_args_49842);
    *((int *)(_2+4)) = _args_49842;
    _args_49842 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	msg = GetMsgText(msgno, 1, args)*/
    Ref(_args_49842);
    _0 = _msg_49843;
    _msg_49843 = _44GetMsgText(_msgno_49841, 1, _args_49842);
    DeRef(_0);

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L2; // [32] 56
    }
    else{
    }

    /** 		screen_output(STDERR, GetMsgText(211, 1, {msg}))*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_msg_49843);
    *((int *)(_2+4)) = _msg_49843;
    _26203 = MAKE_SEQ(_1);
    _26204 = _44GetMsgText(211, 1, _26203);
    _26203 = NOVALUE;
    _43screen_output(2, _26204);
    _26204 = NOVALUE;
    goto L3; // [53] 87
L2: 

    /** 		screen_output(STDERR, GetMsgText(212, 1, {known_files[current_file_no], line_number, msg}))*/
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _26205 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_26205);
    *((int *)(_2+4)) = _26205;
    *((int *)(_2+8)) = _25line_number_12263;
    RefDS(_msg_49843);
    *((int *)(_2+12)) = _msg_49843;
    _26206 = MAKE_SEQ(_1);
    _26205 = NOVALUE;
    _26207 = _44GetMsgText(212, 1, _26206);
    _26206 = NOVALUE;
    _43screen_output(2, _26207);
    _26207 = NOVALUE;
L3: 

    /** 	if not batch_job and not test_only then*/
    _26208 = (_25batch_job_12275 == 0);
    if (_26208 == 0) {
        goto L4; // [94] 127
    }
    _26210 = (_25test_only_12274 == 0);
    if (_26210 == 0)
    {
        DeRef(_26210);
        _26210 = NOVALUE;
        goto L4; // [104] 127
    }
    else{
        DeRef(_26210);
        _26210 = NOVALUE;
    }

    /** 		screen_output(STDERR, GetMsgText(208, 0))*/
    RefDS(_22682);
    _26211 = _44GetMsgText(208, 0, _22682);
    _43screen_output(2, _26211);
    _26211 = NOVALUE;

    /** 		getc(0)*/
    if (0 != last_r_file_no) {
        last_r_file_ptr = which_file(0, EF_READ);
        last_r_file_no = 0;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _26212 = getKBchar();
        }
        else
        _26212 = getc(last_r_file_ptr);
    }
    else
    _26212 = getc(last_r_file_ptr);
L4: 

    /** 	machine_proc(67, GetMsgText(213))*/
    RefDS(_22682);
    _26214 = _44GetMsgText(213, 1, _22682);
    machine(67, _26214);
    DeRef(_26214);
    _26214 = NOVALUE;

    /** end procedure*/
    DeRef(_args_49842);
    DeRef(_msg_49843);
    DeRef(_26208);
    _26208 = NOVALUE;
    return;
    ;
}



// 0x689C450E
