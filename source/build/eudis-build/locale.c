// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _45get_text(int _MsgNum_20581, int _LocalQuals_20582, int _DBBase_20583)
{
    int _db_res_20585 = NOVALUE;
    int _lMsgText_20586 = NOVALUE;
    int _dbname_20587 = NOVALUE;
    int _11640 = NOVALUE;
    int _11638 = NOVALUE;
    int _11636 = NOVALUE;
    int _11635 = NOVALUE;
    int _11634 = NOVALUE;
    int _11632 = NOVALUE;
    int _11631 = NOVALUE;
    int _11630 = NOVALUE;
    int _11624 = NOVALUE;
    int _11623 = NOVALUE;
    int _11622 = NOVALUE;
    int _11615 = NOVALUE;
    int _11612 = NOVALUE;
    int _11610 = NOVALUE;
    int _11608 = NOVALUE;
    int _11607 = NOVALUE;
    int _11606 = NOVALUE;
    int _11605 = NOVALUE;
    int _0, _1, _2;
    

    /** 	db_res = -1*/
    _db_res_20585 = -1;

    /** 	lMsgText = 0*/
    DeRef(_lMsgText_20586);
    _lMsgText_20586 = 0;

    /** 	if string(LocalQuals) and length(LocalQuals) > 0 then*/
    RefDS(_LocalQuals_20582);
    _11605 = _5string(_LocalQuals_20582);
    if (IS_ATOM_INT(_11605)) {
        if (_11605 == 0) {
            goto L1; // [23] 45
        }
    }
    else {
        if (DBL_PTR(_11605)->dbl == 0.0) {
            goto L1; // [23] 45
        }
    }
    if (IS_SEQUENCE(_LocalQuals_20582)){
            _11607 = SEQ_PTR(_LocalQuals_20582)->length;
    }
    else {
        _11607 = 1;
    }
    _11608 = (_11607 > 0);
    _11607 = NOVALUE;
    if (_11608 == 0)
    {
        DeRef(_11608);
        _11608 = NOVALUE;
        goto L1; // [35] 45
    }
    else{
        DeRef(_11608);
        _11608 = NOVALUE;
    }

    /** 		LocalQuals = {LocalQuals}*/
    _0 = _LocalQuals_20582;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_LocalQuals_20582);
    *((int *)(_2+4)) = _LocalQuals_20582;
    _LocalQuals_20582 = MAKE_SEQ(_1);
    DeRefDS(_0);
L1: 

    /** 	for i = 1 to length(LocalQuals) do*/
    if (IS_SEQUENCE(_LocalQuals_20582)){
            _11610 = SEQ_PTR(_LocalQuals_20582)->length;
    }
    else {
        _11610 = 1;
    }
    {
        int _i_20596;
        _i_20596 = 1;
L2: 
        if (_i_20596 > _11610){
            goto L3; // [50] 142
        }

        /** 		dbname = DBBase & "_" & LocalQuals[i] & ".edb"*/
        _2 = (int)SEQ_PTR(_LocalQuals_20582);
        _11612 = (int)*(((s1_ptr)_2)->base + _i_20596);
        {
            int concat_list[4];

            concat_list[0] = _11613;
            concat_list[1] = _11612;
            concat_list[2] = _11611;
            concat_list[3] = _DBBase_20583;
            Concat_N((object_ptr)&_dbname_20587, concat_list, 4);
        }
        _11612 = NOVALUE;

        /** 		db_res = eds:db_select( filesys:locate_file( dbname ), eds:DB_LOCK_READ_ONLY)*/
        RefDS(_dbname_20587);
        RefDS(_5);
        RefDS(_5);
        _11615 = _9locate_file(_dbname_20587, _5, _5);
        _db_res_20585 = _48db_select(_11615, 3);
        _11615 = NOVALUE;
        if (!IS_ATOM_INT(_db_res_20585)) {
            _1 = (long)(DBL_PTR(_db_res_20585)->dbl);
            if (UNIQUE(DBL_PTR(_db_res_20585)) && (DBL_PTR(_db_res_20585)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_db_res_20585);
            _db_res_20585 = _1;
        }

        /** 		if db_res = eds:DB_OK then*/
        if (_db_res_20585 != 0)
        goto L4; // [91] 135

        /** 			db_res = eds:db_select_table("1")*/
        RefDS(_11618);
        _db_res_20585 = _48db_select_table(_11618);
        if (!IS_ATOM_INT(_db_res_20585)) {
            _1 = (long)(DBL_PTR(_db_res_20585)->dbl);
            if (UNIQUE(DBL_PTR(_db_res_20585)) && (DBL_PTR(_db_res_20585)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_db_res_20585);
            _db_res_20585 = _1;
        }

        /** 			if db_res = eds:DB_OK then*/
        if (_db_res_20585 != 0)
        goto L5; // [107] 134

        /** 				lMsgText = eds:db_fetch_record(MsgNum)*/
        RefDS(_48current_table_name_17845);
        _0 = _lMsgText_20586;
        _lMsgText_20586 = _48db_fetch_record(_MsgNum_20581, _48current_table_name_17845);
        DeRef(_0);

        /** 				if sequence(lMsgText) then*/
        _11622 = IS_SEQUENCE(_lMsgText_20586);
        if (_11622 == 0)
        {
            _11622 = NOVALUE;
            goto L6; // [125] 133
        }
        else{
            _11622 = NOVALUE;
        }

        /** 					exit*/
        goto L3; // [130] 142
L6: 
L5: 
L4: 

        /** 	end for*/
        _i_20596 = _i_20596 + 1;
        goto L2; // [137] 57
L3: 
        ;
    }

    /** 	if atom(lMsgText) then*/
    _11623 = IS_ATOM(_lMsgText_20586);
    if (_11623 == 0)
    {
        _11623 = NOVALUE;
        goto L7; // [147] 291
    }
    else{
        _11623 = NOVALUE;
    }

    /** 		dbname = filesys:locate_file( DBBase & ".edb" )*/
    Concat((object_ptr)&_11624, _DBBase_20583, _11613);
    RefDS(_5);
    RefDS(_5);
    _0 = _dbname_20587;
    _dbname_20587 = _9locate_file(_11624, _5, _5);
    DeRef(_0);
    _11624 = NOVALUE;

    /** 		db_res = eds:db_select(	dbname, DB_LOCK_READ_ONLY)*/
    RefDS(_dbname_20587);
    _db_res_20585 = _48db_select(_dbname_20587, 3);
    if (!IS_ATOM_INT(_db_res_20585)) {
        _1 = (long)(DBL_PTR(_db_res_20585)->dbl);
        if (UNIQUE(DBL_PTR(_db_res_20585)) && (DBL_PTR(_db_res_20585)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_db_res_20585);
        _db_res_20585 = _1;
    }

    /** 		if db_res = eds:DB_OK then*/
    if (_db_res_20585 != 0)
    goto L8; // [179] 290

    /** 			db_res = eds:db_select_table("1")*/
    RefDS(_11618);
    _db_res_20585 = _48db_select_table(_11618);
    if (!IS_ATOM_INT(_db_res_20585)) {
        _1 = (long)(DBL_PTR(_db_res_20585)->dbl);
        if (UNIQUE(DBL_PTR(_db_res_20585)) && (DBL_PTR(_db_res_20585)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_db_res_20585);
        _db_res_20585 = _1;
    }

    /** 			if db_res = eds:DB_OK then*/
    if (_db_res_20585 != 0)
    goto L9; // [195] 289

    /** 				for i = 1 to length(LocalQuals) do*/
    if (IS_SEQUENCE(_LocalQuals_20582)){
            _11630 = SEQ_PTR(_LocalQuals_20582)->length;
    }
    else {
        _11630 = 1;
    }
    {
        int _i_20625;
        _i_20625 = 1;
LA: 
        if (_i_20625 > _11630){
            goto LB; // [204] 248
        }

        /** 					lMsgText = eds:db_fetch_record({LocalQuals[i],MsgNum})*/
        _2 = (int)SEQ_PTR(_LocalQuals_20582);
        _11631 = (int)*(((s1_ptr)_2)->base + _i_20625);
        Ref(_11631);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _11631;
        ((int *)_2)[2] = _MsgNum_20581;
        _11632 = MAKE_SEQ(_1);
        _11631 = NOVALUE;
        RefDS(_48current_table_name_17845);
        _0 = _lMsgText_20586;
        _lMsgText_20586 = _48db_fetch_record(_11632, _48current_table_name_17845);
        DeRef(_0);
        _11632 = NOVALUE;

        /** 					if sequence(lMsgText) then*/
        _11634 = IS_SEQUENCE(_lMsgText_20586);
        if (_11634 == 0)
        {
            _11634 = NOVALUE;
            goto LC; // [233] 241
        }
        else{
            _11634 = NOVALUE;
        }

        /** 						exit*/
        goto LB; // [238] 248
LC: 

        /** 				end for*/
        _i_20625 = _i_20625 + 1;
        goto LA; // [243] 211
LB: 
        ;
    }

    /** 				if atom(lMsgText) then*/
    _11635 = IS_ATOM(_lMsgText_20586);
    if (_11635 == 0)
    {
        _11635 = NOVALUE;
        goto LD; // [253] 270
    }
    else{
        _11635 = NOVALUE;
    }

    /** 					lMsgText = eds:db_fetch_record({"",MsgNum})*/
    RefDS(_5);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5;
    ((int *)_2)[2] = _MsgNum_20581;
    _11636 = MAKE_SEQ(_1);
    RefDS(_48current_table_name_17845);
    _0 = _lMsgText_20586;
    _lMsgText_20586 = _48db_fetch_record(_11636, _48current_table_name_17845);
    DeRef(_0);
    _11636 = NOVALUE;
LD: 

    /** 				if atom(lMsgText) then*/
    _11638 = IS_ATOM(_lMsgText_20586);
    if (_11638 == 0)
    {
        _11638 = NOVALUE;
        goto LE; // [275] 288
    }
    else{
        _11638 = NOVALUE;
    }

    /** 					lMsgText = eds:db_fetch_record(MsgNum)*/
    RefDS(_48current_table_name_17845);
    _0 = _lMsgText_20586;
    _lMsgText_20586 = _48db_fetch_record(_MsgNum_20581, _48current_table_name_17845);
    DeRef(_0);
LE: 
L9: 
L8: 
L7: 

    /** 	if atom(lMsgText) then*/
    _11640 = IS_ATOM(_lMsgText_20586);
    if (_11640 == 0)
    {
        _11640 = NOVALUE;
        goto LF; // [296] 308
    }
    else{
        _11640 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_LocalQuals_20582);
    DeRefDS(_DBBase_20583);
    DeRef(_lMsgText_20586);
    DeRef(_dbname_20587);
    DeRef(_11605);
    _11605 = NOVALUE;
    return 0;
    goto L10; // [305] 315
LF: 

    /** 		return lMsgText*/
    DeRefDS(_LocalQuals_20582);
    DeRefDS(_DBBase_20583);
    DeRef(_dbname_20587);
    DeRef(_11605);
    _11605 = NOVALUE;
    return _lMsgText_20586;
L10: 
    ;
}



// 0x7096555D
