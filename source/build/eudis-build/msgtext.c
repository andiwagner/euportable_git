// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _44GetMsgText(int _MsgNum_21643, int _WithNum_21644, int _Args_21645)
{
    int _idx_21646 = NOVALUE;
    int _msgtext_21647 = NOVALUE;
    int _12650 = NOVALUE;
    int _12649 = NOVALUE;
    int _12645 = NOVALUE;
    int _12644 = NOVALUE;
    int _12642 = NOVALUE;
    int _12640 = NOVALUE;
    int _12638 = NOVALUE;
    int _12637 = NOVALUE;
    int _12636 = NOVALUE;
    int _12635 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_MsgNum_21643)) {
        _1 = (long)(DBL_PTR(_MsgNum_21643)->dbl);
        if (UNIQUE(DBL_PTR(_MsgNum_21643)) && (DBL_PTR(_MsgNum_21643)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_MsgNum_21643);
        _MsgNum_21643 = _1;
    }

    /** 	integer idx = 1*/
    _idx_21646 = 1;

    /** 	msgtext = get_text( MsgNum, LocalizeQual, LocalDB )*/
    RefDS(_26LocalizeQual_11159);
    RefDS(_26LocalDB_11160);
    _0 = _msgtext_21647;
    _msgtext_21647 = _45get_text(_MsgNum_21643, _26LocalizeQual_11159, _26LocalDB_11160);
    DeRef(_0);

    /** 	if atom(msgtext) then*/
    _12635 = IS_ATOM(_msgtext_21647);
    if (_12635 == 0)
    {
        _12635 = NOVALUE;
        goto L1; // [27] 90
    }
    else{
        _12635 = NOVALUE;
    }

    /** 		for i = 1 to length(StdErrMsgs) do*/
    _12636 = 358;
    {
        int _i_21655;
        _i_21655 = 1;
L2: 
        if (_i_21655 > 358){
            goto L3; // [37] 77
        }

        /** 			if StdErrMsgs[i][1] = MsgNum then*/
        _2 = (int)SEQ_PTR(_44StdErrMsgs_20642);
        _12637 = (int)*(((s1_ptr)_2)->base + _i_21655);
        _2 = (int)SEQ_PTR(_12637);
        _12638 = (int)*(((s1_ptr)_2)->base + 1);
        _12637 = NOVALUE;
        if (binary_op_a(NOTEQ, _12638, _MsgNum_21643)){
            _12638 = NOVALUE;
            goto L4; // [56] 70
        }
        _12638 = NOVALUE;

        /** 				idx = i*/
        _idx_21646 = _i_21655;

        /** 				exit*/
        goto L3; // [67] 77
L4: 

        /** 		end for*/
        _i_21655 = _i_21655 + 1;
        goto L2; // [72] 44
L3: 
        ;
    }

    /** 		msgtext = StdErrMsgs[idx][2]*/
    _2 = (int)SEQ_PTR(_44StdErrMsgs_20642);
    _12640 = (int)*(((s1_ptr)_2)->base + _idx_21646);
    DeRef(_msgtext_21647);
    _2 = (int)SEQ_PTR(_12640);
    _msgtext_21647 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_msgtext_21647);
    _12640 = NOVALUE;
L1: 

    /** 	if atom(Args) or length(Args) != 0 then*/
    _12642 = IS_ATOM(_Args_21645);
    if (_12642 != 0) {
        goto L5; // [95] 111
    }
    if (IS_SEQUENCE(_Args_21645)){
            _12644 = SEQ_PTR(_Args_21645)->length;
    }
    else {
        _12644 = 1;
    }
    _12645 = (_12644 != 0);
    _12644 = NOVALUE;
    if (_12645 == 0)
    {
        DeRef(_12645);
        _12645 = NOVALUE;
        goto L6; // [107] 119
    }
    else{
        DeRef(_12645);
        _12645 = NOVALUE;
    }
L5: 

    /** 		msgtext = format(msgtext, Args)*/
    Ref(_msgtext_21647);
    Ref(_Args_21645);
    _0 = _msgtext_21647;
    _msgtext_21647 = _4format(_msgtext_21647, _Args_21645);
    DeRef(_0);
L6: 

    /** 	if WithNum != 0 then*/
    if (_WithNum_21644 == 0)
    goto L7; // [121] 142

    /** 		return sprintf("<%04d>:: %s", {MsgNum, msgtext})*/
    Ref(_msgtext_21647);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _MsgNum_21643;
    ((int *)_2)[2] = _msgtext_21647;
    _12649 = MAKE_SEQ(_1);
    _12650 = EPrintf(-9999999, _12648, _12649);
    DeRefDS(_12649);
    _12649 = NOVALUE;
    DeRef(_Args_21645);
    DeRef(_msgtext_21647);
    return _12650;
    goto L8; // [139] 149
L7: 

    /** 		return msgtext*/
    DeRef(_Args_21645);
    DeRef(_12650);
    _12650 = NOVALUE;
    return _msgtext_21647;
L8: 
    ;
}


void _44ShowMsg(int _Cons_21678, int _Msg_21679, int _Args_21680, int _NL_21681)
{
    int _12657 = NOVALUE;
    int _12656 = NOVALUE;
    int _12654 = NOVALUE;
    int _12652 = NOVALUE;
    int _12651 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(Msg) then*/
    _12651 = 1;
    if (_12651 == 0)
    {
        _12651 = NOVALUE;
        goto L1; // [10] 25
    }
    else{
        _12651 = NOVALUE;
    }

    /** 		Msg = GetMsgText(floor(Msg), 0)*/
    _12652 = e_floor(_Msg_21679);
    RefDS(_5);
    _Msg_21679 = _44GetMsgText(_12652, 0, _5);
    _12652 = NOVALUE;
L1: 

    /** 	if atom(Args) or length(Args) != 0 then*/
    _12654 = IS_ATOM(_Args_21680);
    if (_12654 != 0) {
        goto L2; // [30] 46
    }
    if (IS_SEQUENCE(_Args_21680)){
            _12656 = SEQ_PTR(_Args_21680)->length;
    }
    else {
        _12656 = 1;
    }
    _12657 = (_12656 != 0);
    _12656 = NOVALUE;
    if (_12657 == 0)
    {
        DeRef(_12657);
        _12657 = NOVALUE;
        goto L3; // [42] 54
    }
    else{
        DeRef(_12657);
        _12657 = NOVALUE;
    }
L2: 

    /** 		Msg = format(Msg, Args)*/
    Ref(_Msg_21679);
    Ref(_Args_21680);
    _0 = _Msg_21679;
    _Msg_21679 = _4format(_Msg_21679, _Args_21680);
    DeRef(_0);
L3: 

    /** 	puts(Cons, Msg)*/
    EPuts(_Cons_21678, _Msg_21679); // DJP 

    /** 	if NL then*/
    if (_NL_21681 == 0)
    {
        goto L4; // [61] 70
    }
    else{
    }

    /** 		puts(Cons, '\n')*/
    EPuts(_Cons_21678, 10); // DJP 
L4: 

    /** end procedure*/
    DeRef(_Msg_21679);
    DeRef(_Args_21680);
    return;
    ;
}



// 0x7EC7144D
