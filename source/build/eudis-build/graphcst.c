// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _42color(int _x_14951)
{
    int _8317 = NOVALUE;
    int _8316 = NOVALUE;
    int _8315 = NOVALUE;
    int _8314 = NOVALUE;
    int _8313 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(x) and x >= 0 and x <= 255 then*/
    if (IS_ATOM_INT(_x_14951))
    _8313 = 1;
    else if (IS_ATOM_DBL(_x_14951))
    _8313 = IS_ATOM_INT(DoubleToInt(_x_14951));
    else
    _8313 = 0;
    if (_8313 == 0) {
        _8314 = 0;
        goto L1; // [6] 18
    }
    if (IS_ATOM_INT(_x_14951)) {
        _8315 = (_x_14951 >= 0);
    }
    else {
        _8315 = binary_op(GREATEREQ, _x_14951, 0);
    }
    if (IS_ATOM_INT(_8315))
    _8314 = (_8315 != 0);
    else
    _8314 = DBL_PTR(_8315)->dbl != 0.0;
L1: 
    if (_8314 == 0) {
        goto L2; // [18] 39
    }
    if (IS_ATOM_INT(_x_14951)) {
        _8317 = (_x_14951 <= 255);
    }
    else {
        _8317 = binary_op(LESSEQ, _x_14951, 255);
    }
    if (_8317 == 0) {
        DeRef(_8317);
        _8317 = NOVALUE;
        goto L2; // [27] 39
    }
    else {
        if (!IS_ATOM_INT(_8317) && DBL_PTR(_8317)->dbl == 0.0){
            DeRef(_8317);
            _8317 = NOVALUE;
            goto L2; // [27] 39
        }
        DeRef(_8317);
        _8317 = NOVALUE;
    }
    DeRef(_8317);
    _8317 = NOVALUE;

    /** 		return 1*/
    DeRef(_x_14951);
    DeRef(_8315);
    _8315 = NOVALUE;
    return 1;
    goto L3; // [36] 46
L2: 

    /** 		return 0*/
    DeRef(_x_14951);
    DeRef(_8315);
    _8315 = NOVALUE;
    return 0;
L3: 
    ;
}


int _42mixture(int _s_14961)
{
    int _8326 = NOVALUE;
    int _8324 = NOVALUE;
    int _8322 = NOVALUE;
    int _8321 = NOVALUE;
    int _8319 = NOVALUE;
    int _8318 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(s) then*/
    _8318 = IS_ATOM(_s_14961);
    if (_8318 == 0)
    {
        _8318 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _8318 = NOVALUE;
    }

    /** 		return 0*/
    DeRef(_s_14961);
    return 0;
L1: 

    /** 	if length(s) != 3 then*/
    if (IS_SEQUENCE(_s_14961)){
            _8319 = SEQ_PTR(_s_14961)->length;
    }
    else {
        _8319 = 1;
    }
    if (_8319 == 3)
    goto L2; // [21] 32

    /** 		return 0*/
    DeRef(_s_14961);
    return 0;
L2: 

    /** 	for i=1 to 3 do*/
    {
        int _i_14968;
        _i_14968 = 1;
L3: 
        if (_i_14968 > 3){
            goto L4; // [34] 87
        }

        /** 		if not integer(s[i]) then*/
        _2 = (int)SEQ_PTR(_s_14961);
        _8321 = (int)*(((s1_ptr)_2)->base + _i_14968);
        if (IS_ATOM_INT(_8321))
        _8322 = 1;
        else if (IS_ATOM_DBL(_8321))
        _8322 = IS_ATOM_INT(DoubleToInt(_8321));
        else
        _8322 = 0;
        _8321 = NOVALUE;
        if (_8322 != 0)
        goto L5; // [50] 60
        _8322 = NOVALUE;

        /** 			return 0*/
        DeRef(_s_14961);
        return 0;
L5: 

        /**   		if and_bits(s[i],#FFFFFFC0) then*/
        _2 = (int)SEQ_PTR(_s_14961);
        _8324 = (int)*(((s1_ptr)_2)->base + _i_14968);
        _8326 = binary_op(AND_BITS, _8324, _8325);
        _8324 = NOVALUE;
        if (_8326 == 0) {
            DeRef(_8326);
            _8326 = NOVALUE;
            goto L6; // [70] 80
        }
        else {
            if (!IS_ATOM_INT(_8326) && DBL_PTR(_8326)->dbl == 0.0){
                DeRef(_8326);
                _8326 = NOVALUE;
                goto L6; // [70] 80
            }
            DeRef(_8326);
            _8326 = NOVALUE;
        }
        DeRef(_8326);
        _8326 = NOVALUE;

        /**   			return 0*/
        DeRef(_s_14961);
        return 0;
L6: 

        /** 	end for*/
        _i_14968 = _i_14968 + 1;
        goto L3; // [82] 41
L4: 
        ;
    }

    /** 	return 1*/
    DeRef(_s_14961);
    return 1;
    ;
}


int _42video_config()
{
    int _8327 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_VIDEO_CONFIG, 0)*/
    _8327 = machine(13, 0);
    return _8327;
    ;
}



// 0x0D30CEB4
