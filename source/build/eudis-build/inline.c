// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _67advance(int _pc_53738, int _code_53739)
{
    int _27948 = NOVALUE;
    int _27946 = NOVALUE;
    int _0, _1, _2;
    

    /** 	prev_pc = pc*/
    _67prev_pc_53723 = _pc_53738;

    /** 	if pc > length( code ) then*/
    if (IS_SEQUENCE(_code_53739)){
            _27946 = SEQ_PTR(_code_53739)->length;
    }
    else {
        _27946 = 1;
    }
    if (_pc_53738 <= _27946)
    goto L1; // [15] 26

    /** 		return pc*/
    DeRefDS(_code_53739);
    return _pc_53738;
L1: 

    /** 	return shift:advance( pc, code )*/
    RefDS(_code_53739);
    _27948 = _65advance(_pc_53738, _code_53739);
    DeRefDS(_code_53739);
    return _27948;
    ;
}


void _67shift(int _start_53746, int _amount_53747, int _bound_53748)
{
    int _temp_LineTable_53749 = NOVALUE;
    int _temp_Code_53751 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_amount_53747)) {
        _1 = (long)(DBL_PTR(_amount_53747)->dbl);
        if (UNIQUE(DBL_PTR(_amount_53747)) && (DBL_PTR(_amount_53747)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_amount_53747);
        _amount_53747 = _1;
    }

    /** 		temp_LineTable = LineTable,*/
    RefDS(_25LineTable_12356);
    DeRef(_temp_LineTable_53749);
    _temp_LineTable_53749 = _25LineTable_12356;

    /** 		temp_Code = Code*/
    RefDS(_25Code_12355);
    DeRef(_temp_Code_53751);
    _temp_Code_53751 = _25Code_12355;

    /** 	LineTable = {}*/
    RefDS(_22682);
    DeRefDS(_25LineTable_12356);
    _25LineTable_12356 = _22682;

    /** 	Code = inline_code*/
    RefDS(_67inline_code_53715);
    DeRefDS(_25Code_12355);
    _25Code_12355 = _67inline_code_53715;

    /** 	inline_code = {}*/
    RefDS(_22682);
    DeRefDS(_67inline_code_53715);
    _67inline_code_53715 = _22682;

    /** 	shift:shift( start, amount, bound )*/
    _65shift(_start_53746, _amount_53747, _bound_53748);

    /** 	LineTable = temp_LineTable*/
    RefDS(_temp_LineTable_53749);
    DeRefDS(_25LineTable_12356);
    _25LineTable_12356 = _temp_LineTable_53749;

    /** 	inline_code = Code*/
    RefDS(_25Code_12355);
    DeRefDS(_67inline_code_53715);
    _67inline_code_53715 = _25Code_12355;

    /** 	Code = temp_Code*/
    RefDS(_temp_Code_53751);
    DeRefDS(_25Code_12355);
    _25Code_12355 = _temp_Code_53751;

    /** end procedure*/
    DeRefDS(_temp_LineTable_53749);
    DeRefDS(_temp_Code_53751);
    return;
    ;
}


void _67insert_code(int _code_53760, int _index_53761)
{
    int _27950 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_index_53761)) {
        _1 = (long)(DBL_PTR(_index_53761)->dbl);
        if (UNIQUE(DBL_PTR(_index_53761)) && (DBL_PTR(_index_53761)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_index_53761);
        _index_53761 = _1;
    }

    /** 	inline_code = splice( inline_code, code, index )*/
    {
        s1_ptr assign_space;
        insert_pos = _index_53761;
        if (insert_pos <= 0) {
            Concat(&_67inline_code_53715,_code_53760,_67inline_code_53715);
        }
        else if (insert_pos > SEQ_PTR(_67inline_code_53715)->length){
            Concat(&_67inline_code_53715,_67inline_code_53715,_code_53760);
        }
        else if (IS_SEQUENCE(_code_53760)) {
            if( _67inline_code_53715 != _67inline_code_53715 || SEQ_PTR( _67inline_code_53715 )->ref != 1 ){
                DeRef( _67inline_code_53715 );
                RefDS( _67inline_code_53715 );
            }
            assign_space = Add_internal_space( _67inline_code_53715, insert_pos,((s1_ptr)SEQ_PTR(_code_53760))->length);
            assign_slice_seq = &assign_space;
            assign_space = Copy_elements( insert_pos, SEQ_PTR(_code_53760), _67inline_code_53715 == _67inline_code_53715 );
            _67inline_code_53715 = MAKE_SEQ( assign_space );
        }
        else {
            if( _67inline_code_53715 == _67inline_code_53715 && SEQ_PTR( _67inline_code_53715 )->ref == 1 ){
                _67inline_code_53715 = Insert( _67inline_code_53715, _code_53760, insert_pos);
            }
            else {
                DeRef( _67inline_code_53715 );
                RefDS( _67inline_code_53715 );
                _67inline_code_53715 = Insert( _67inline_code_53715, _code_53760, insert_pos);
            }
        }
    }

    /** 	shift( index, length( code ) )*/
    if (IS_SEQUENCE(_code_53760)){
            _27950 = SEQ_PTR(_code_53760)->length;
    }
    else {
        _27950 = 1;
    }
    _67shift(_index_53761, _27950, _index_53761);
    _27950 = NOVALUE;

    /** end procedure*/
    DeRefDS(_code_53760);
    return;
    ;
}


void _67replace_code(int _code_53766, int _start_53767, int _finish_53768)
{
    int _27955 = NOVALUE;
    int _27954 = NOVALUE;
    int _27953 = NOVALUE;
    int _27952 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_start_53767)) {
        _1 = (long)(DBL_PTR(_start_53767)->dbl);
        if (UNIQUE(DBL_PTR(_start_53767)) && (DBL_PTR(_start_53767)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_53767);
        _start_53767 = _1;
    }
    if (!IS_ATOM_INT(_finish_53768)) {
        _1 = (long)(DBL_PTR(_finish_53768)->dbl);
        if (UNIQUE(DBL_PTR(_finish_53768)) && (DBL_PTR(_finish_53768)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_finish_53768);
        _finish_53768 = _1;
    }

    /** 	inline_code = replace( inline_code, code, start, finish )*/
    {
        int p1 = _67inline_code_53715;
        int p2 = _code_53766;
        int p3 = _start_53767;
        int p4 = _finish_53768;
        struct replace_block replace_params;
        replace_params.copy_to   = &p1;
        replace_params.copy_from = &p2;
        replace_params.start     = &p3;
        replace_params.stop      = &p4;
        replace_params.target    = &_67inline_code_53715;
        Replace( &replace_params );
    }

    /** 	shift( start , length( code ) - (finish - start + 1), finish )*/
    if (IS_SEQUENCE(_code_53766)){
            _27952 = SEQ_PTR(_code_53766)->length;
    }
    else {
        _27952 = 1;
    }
    _27953 = _finish_53768 - _start_53767;
    if ((long)((unsigned long)_27953 +(unsigned long) HIGH_BITS) >= 0){
        _27953 = NewDouble((double)_27953);
    }
    if (IS_ATOM_INT(_27953)) {
        _27954 = _27953 + 1;
        if (_27954 > MAXINT){
            _27954 = NewDouble((double)_27954);
        }
    }
    else
    _27954 = binary_op(PLUS, 1, _27953);
    DeRef(_27953);
    _27953 = NOVALUE;
    if (IS_ATOM_INT(_27954)) {
        _27955 = _27952 - _27954;
        if ((long)((unsigned long)_27955 +(unsigned long) HIGH_BITS) >= 0){
            _27955 = NewDouble((double)_27955);
        }
    }
    else {
        _27955 = NewDouble((double)_27952 - DBL_PTR(_27954)->dbl);
    }
    _27952 = NOVALUE;
    DeRef(_27954);
    _27954 = NOVALUE;
    _67shift(_start_53767, _27955, _finish_53768);
    _27955 = NOVALUE;

    /** end procedure*/
    DeRefDS(_code_53766);
    return;
    ;
}


void _67defer()
{
    int _dx_53776 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer dx = find( inline_sub, deferred_inline_decisions )*/
    _dx_53776 = find_from(_67inline_sub_53729, _67deferred_inline_decisions_53731, 1);

    /** 	if not dx then*/
    if (_dx_53776 != 0)
    goto L1; // [14] 36

    /** 		deferred_inline_decisions &= inline_sub*/
    Append(&_67deferred_inline_decisions_53731, _67deferred_inline_decisions_53731, _67inline_sub_53729);

    /** 		deferred_inline_calls = append( deferred_inline_calls, {} )*/
    RefDS(_22682);
    Append(&_67deferred_inline_calls_53732, _67deferred_inline_calls_53732, _22682);
L1: 

    /** end procedure*/
    return;
    ;
}


int _67new_inline_temp(int _sym_53785)
{
    int _27961 = NOVALUE;
    int _0, _1, _2;
    

    /** 	inline_temps &= sym*/
    Append(&_67inline_temps_53717, _67inline_temps_53717, _sym_53785);

    /** 	return length( inline_temps )*/
    if (IS_SEQUENCE(_67inline_temps_53717)){
            _27961 = SEQ_PTR(_67inline_temps_53717)->length;
    }
    else {
        _27961 = 1;
    }
    return _27961;
    ;
}


int _67get_inline_temp(int _sym_53791)
{
    int _temp_num_53792 = NOVALUE;
    int _27965 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer temp_num = find( sym, inline_params )*/
    _temp_num_53792 = find_from(_sym_53791, _67inline_params_53720, 1);

    /** 	if temp_num then*/
    if (_temp_num_53792 == 0)
    {
        goto L1; // [14] 24
    }
    else{
    }

    /** 		return temp_num*/
    return _temp_num_53792;
L1: 

    /** 	temp_num = find( sym, proc_vars )*/
    _temp_num_53792 = find_from(_sym_53791, _67proc_vars_53716, 1);

    /** 	if temp_num then*/
    if (_temp_num_53792 == 0)
    {
        goto L2; // [35] 45
    }
    else{
    }

    /** 		return temp_num*/
    return _temp_num_53792;
L2: 

    /** 	temp_num = find( sym, inline_temps )*/
    _temp_num_53792 = find_from(_sym_53791, _67inline_temps_53717, 1);

    /** 	if temp_num then*/
    if (_temp_num_53792 == 0)
    {
        goto L3; // [56] 66
    }
    else{
    }

    /** 		return temp_num*/
    return _temp_num_53792;
L3: 

    /** 	return new_inline_temp( sym )*/
    _27965 = _67new_inline_temp(_sym_53791);
    return _27965;
    ;
}


int _67generic_symbol(int _sym_53803)
{
    int _inline_type_53804 = NOVALUE;
    int _px_53805 = NOVALUE;
    int _eentry_53812 = NOVALUE;
    int _27974 = NOVALUE;
    int _27973 = NOVALUE;
    int _27972 = NOVALUE;
    int _27971 = NOVALUE;
    int _27969 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer px = find( sym, inline_params )*/
    _px_53805 = find_from(_sym_53803, _67inline_params_53720, 1);

    /** 	if px then*/
    if (_px_53805 == 0)
    {
        goto L1; // [14] 29
    }
    else{
    }

    /** 		inline_type = INLINE_PARAM*/
    _inline_type_53804 = 1;
    goto L2; // [26] 112
L1: 

    /** 		px = find( sym, proc_vars )*/
    _px_53805 = find_from(_sym_53803, _67proc_vars_53716, 1);

    /** 		if px then*/
    if (_px_53805 == 0)
    {
        goto L3; // [40] 55
    }
    else{
    }

    /** 			inline_type = INLINE_VAR*/
    _inline_type_53804 = 6;
    goto L4; // [52] 111
L3: 

    /** 			sequence eentry = SymTab[sym]*/
    DeRef(_eentry_53812);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _eentry_53812 = (int)*(((s1_ptr)_2)->base + _sym_53803);
    Ref(_eentry_53812);

    /** 			if is_literal( sym ) or eentry[S_SCOPE] > SC_PRIVATE then*/
    _27969 = _67is_literal(_sym_53803);
    if (IS_ATOM_INT(_27969)) {
        if (_27969 != 0) {
            goto L5; // [71] 92
        }
    }
    else {
        if (DBL_PTR(_27969)->dbl != 0.0) {
            goto L5; // [71] 92
        }
    }
    _2 = (int)SEQ_PTR(_eentry_53812);
    _27971 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_27971)) {
        _27972 = (_27971 > 3);
    }
    else {
        _27972 = binary_op(GREATER, _27971, 3);
    }
    _27971 = NOVALUE;
    if (_27972 == 0) {
        DeRef(_27972);
        _27972 = NOVALUE;
        goto L6; // [88] 99
    }
    else {
        if (!IS_ATOM_INT(_27972) && DBL_PTR(_27972)->dbl == 0.0){
            DeRef(_27972);
            _27972 = NOVALUE;
            goto L6; // [88] 99
        }
        DeRef(_27972);
        _27972 = NOVALUE;
    }
    DeRef(_27972);
    _27972 = NOVALUE;
L5: 

    /** 				return sym*/
    DeRef(_eentry_53812);
    DeRef(_27969);
    _27969 = NOVALUE;
    return _sym_53803;
L6: 

    /** 			inline_type = INLINE_TEMP*/
    _inline_type_53804 = 2;
    DeRef(_eentry_53812);
    _eentry_53812 = NOVALUE;
L4: 
L2: 

    /** 	return { inline_type, get_inline_temp( sym ) }*/
    _27973 = _67get_inline_temp(_sym_53803);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _inline_type_53804;
    ((int *)_2)[2] = _27973;
    _27974 = MAKE_SEQ(_1);
    _27973 = NOVALUE;
    DeRef(_27969);
    _27969 = NOVALUE;
    return _27974;
    ;
}


int _67adjust_symbol(int _pc_53827)
{
    int _sym_53829 = NOVALUE;
    int _eentry_53835 = NOVALUE;
    int _27982 = NOVALUE;
    int _27980 = NOVALUE;
    int _27979 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_pc_53827)) {
        _1 = (long)(DBL_PTR(_pc_53827)->dbl);
        if (UNIQUE(DBL_PTR(_pc_53827)) && (DBL_PTR(_pc_53827)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pc_53827);
        _pc_53827 = _1;
    }

    /** 	symtab_index sym = inline_code[pc]*/
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    _sym_53829 = (int)*(((s1_ptr)_2)->base + _pc_53827);
    if (!IS_ATOM_INT(_sym_53829)){
        _sym_53829 = (long)DBL_PTR(_sym_53829)->dbl;
    }

    /** 	if sym < 0 then*/
    if (_sym_53829 >= 0)
    goto L1; // [15] 28

    /** 		return 0*/
    DeRef(_eentry_53835);
    return 0;
    goto L2; // [25] 41
L1: 

    /** 	elsif not sym then*/
    if (_sym_53829 != 0)
    goto L3; // [30] 40

    /** 		return 1*/
    DeRef(_eentry_53835);
    return 1;
L3: 
L2: 

    /** 	sequence eentry = SymTab[sym]*/
    DeRef(_eentry_53835);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _eentry_53835 = (int)*(((s1_ptr)_2)->base + _sym_53829);
    Ref(_eentry_53835);

    /** 	if is_literal( sym ) then*/
    _27979 = _67is_literal(_sym_53829);
    if (_27979 == 0) {
        DeRef(_27979);
        _27979 = NOVALUE;
        goto L4; // [57] 69
    }
    else {
        if (!IS_ATOM_INT(_27979) && DBL_PTR(_27979)->dbl == 0.0){
            DeRef(_27979);
            _27979 = NOVALUE;
            goto L4; // [57] 69
        }
        DeRef(_27979);
        _27979 = NOVALUE;
    }
    DeRef(_27979);
    _27979 = NOVALUE;

    /** 		return 1*/
    DeRefDS(_eentry_53835);
    return 1;
    goto L5; // [66] 95
L4: 

    /** 	elsif eentry[S_SCOPE] = SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_eentry_53835);
    _27980 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _27980, 9)){
        _27980 = NOVALUE;
        goto L6; // [79] 94
    }
    _27980 = NOVALUE;

    /** 		defer()*/
    _67defer();

    /** 		return 0*/
    DeRefDS(_eentry_53835);
    return 0;
L6: 
L5: 

    /** 	inline_code[pc] = generic_symbol( sym )*/
    _27982 = _67generic_symbol(_sym_53829);
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _67inline_code_53715 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _pc_53827);
    _1 = *(int *)_2;
    *(int *)_2 = _27982;
    if( _1 != _27982 ){
        DeRef(_1);
    }
    _27982 = NOVALUE;

    /** 	return 1*/
    DeRef(_eentry_53835);
    return 1;
    ;
}


int _67check_for_param(int _pc_53849)
{
    int _px_53850 = NOVALUE;
    int _27985 = NOVALUE;
    int _27983 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_pc_53849)) {
        _1 = (long)(DBL_PTR(_pc_53849)->dbl);
        if (UNIQUE(DBL_PTR(_pc_53849)) && (DBL_PTR(_pc_53849)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pc_53849);
        _pc_53849 = _1;
    }

    /** 	integer px = find( inline_code[pc], inline_params )*/
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    _27983 = (int)*(((s1_ptr)_2)->base + _pc_53849);
    _px_53850 = find_from(_27983, _67inline_params_53720, 1);
    _27983 = NOVALUE;

    /** 	if px then*/
    if (_px_53850 == 0)
    {
        goto L1; // [20] 51
    }
    else{
    }

    /** 		if not find( px, assigned_params ) then*/
    _27985 = find_from(_px_53850, _67assigned_params_53721, 1);
    if (_27985 != 0)
    goto L2; // [32] 44
    _27985 = NOVALUE;

    /** 			assigned_params &= px*/
    Append(&_67assigned_params_53721, _67assigned_params_53721, _px_53850);
L2: 

    /** 		return 1*/
    return 1;
L1: 

    /** 	return 0*/
    return 0;
    ;
}


void _67check_target(int _pc_53860, int _op_53861)
{
    int _targets_53862 = NOVALUE;
    int _27994 = NOVALUE;
    int _27993 = NOVALUE;
    int _27992 = NOVALUE;
    int _27991 = NOVALUE;
    int _27990 = NOVALUE;
    int _27988 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence targets = op_info[op][OP_TARGET]*/
    _2 = (int)SEQ_PTR(_65op_info_27470);
    _27988 = (int)*(((s1_ptr)_2)->base + _op_53861);
    DeRef(_targets_53862);
    _2 = (int)SEQ_PTR(_27988);
    _targets_53862 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_targets_53862);
    _27988 = NOVALUE;

    /** 	if length( targets ) then*/
    if (IS_SEQUENCE(_targets_53862)){
            _27990 = SEQ_PTR(_targets_53862)->length;
    }
    else {
        _27990 = 1;
    }
    if (_27990 == 0)
    {
        _27990 = NOVALUE;
        goto L1; // [26] 72
    }
    else{
        _27990 = NOVALUE;
    }

    /** 	for i = 1 to length( targets ) do*/
    if (IS_SEQUENCE(_targets_53862)){
            _27991 = SEQ_PTR(_targets_53862)->length;
    }
    else {
        _27991 = 1;
    }
    {
        int _i_53870;
        _i_53870 = 1;
L2: 
        if (_i_53870 > _27991){
            goto L3; // [34] 71
        }

        /** 			if check_for_param( pc + targets[i] ) then*/
        _2 = (int)SEQ_PTR(_targets_53862);
        _27992 = (int)*(((s1_ptr)_2)->base + _i_53870);
        if (IS_ATOM_INT(_27992)) {
            _27993 = _pc_53860 + _27992;
            if ((long)((unsigned long)_27993 + (unsigned long)HIGH_BITS) >= 0) 
            _27993 = NewDouble((double)_27993);
        }
        else {
            _27993 = binary_op(PLUS, _pc_53860, _27992);
        }
        _27992 = NOVALUE;
        _27994 = _67check_for_param(_27993);
        _27993 = NOVALUE;
        if (_27994 == 0) {
            DeRef(_27994);
            _27994 = NOVALUE;
            goto L4; // [55] 64
        }
        else {
            if (!IS_ATOM_INT(_27994) && DBL_PTR(_27994)->dbl == 0.0){
                DeRef(_27994);
                _27994 = NOVALUE;
                goto L4; // [55] 64
            }
            DeRef(_27994);
            _27994 = NOVALUE;
        }
        DeRef(_27994);
        _27994 = NOVALUE;

        /** 				return*/
        DeRefDS(_targets_53862);
        return;
L4: 

        /** 		end for*/
        _i_53870 = _i_53870 + 1;
        goto L2; // [66] 41
L3: 
        ;
    }
L1: 

    /** end procedure*/
    DeRef(_targets_53862);
    return;
    ;
}


int _67adjust_il(int _pc_53878, int _op_53879)
{
    int _addr_53887 = NOVALUE;
    int _sub_53893 = NOVALUE;
    int _28019 = NOVALUE;
    int _28018 = NOVALUE;
    int _28017 = NOVALUE;
    int _28016 = NOVALUE;
    int _28015 = NOVALUE;
    int _28014 = NOVALUE;
    int _28013 = NOVALUE;
    int _28012 = NOVALUE;
    int _28011 = NOVALUE;
    int _28010 = NOVALUE;
    int _28009 = NOVALUE;
    int _28008 = NOVALUE;
    int _28007 = NOVALUE;
    int _28006 = NOVALUE;
    int _28005 = NOVALUE;
    int _28004 = NOVALUE;
    int _28002 = NOVALUE;
    int _28001 = NOVALUE;
    int _27999 = NOVALUE;
    int _27998 = NOVALUE;
    int _27997 = NOVALUE;
    int _27996 = NOVALUE;
    int _27995 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to op_info[op][OP_SIZE] - 1 do*/
    _2 = (int)SEQ_PTR(_65op_info_27470);
    _27995 = (int)*(((s1_ptr)_2)->base + _op_53879);
    _2 = (int)SEQ_PTR(_27995);
    _27996 = (int)*(((s1_ptr)_2)->base + 2);
    _27995 = NOVALUE;
    if (IS_ATOM_INT(_27996)) {
        _27997 = _27996 - 1;
        if ((long)((unsigned long)_27997 +(unsigned long) HIGH_BITS) >= 0){
            _27997 = NewDouble((double)_27997);
        }
    }
    else {
        _27997 = binary_op(MINUS, _27996, 1);
    }
    _27996 = NOVALUE;
    {
        int _i_53881;
        _i_53881 = 1;
L1: 
        if (binary_op_a(GREATER, _i_53881, _27997)){
            goto L2; // [23] 222
        }

        /** 		integer addr = find( i, op_info[op][OP_ADDR] )*/
        _2 = (int)SEQ_PTR(_65op_info_27470);
        _27998 = (int)*(((s1_ptr)_2)->base + _op_53879);
        _2 = (int)SEQ_PTR(_27998);
        _27999 = (int)*(((s1_ptr)_2)->base + 3);
        _27998 = NOVALUE;
        _addr_53887 = find_from(_i_53881, _27999, 1);
        _27999 = NOVALUE;

        /** 		integer sub  = find( i, op_info[op][OP_SUB] )*/
        _2 = (int)SEQ_PTR(_65op_info_27470);
        _28001 = (int)*(((s1_ptr)_2)->base + _op_53879);
        _2 = (int)SEQ_PTR(_28001);
        _28002 = (int)*(((s1_ptr)_2)->base + 5);
        _28001 = NOVALUE;
        _sub_53893 = find_from(_i_53881, _28002, 1);
        _28002 = NOVALUE;

        /** 		if addr then*/
        if (_addr_53887 == 0)
        {
            goto L3; // [70] 123
        }
        else{
        }

        /** 			if integer( inline_code[pc+i] ) then*/
        if (IS_ATOM_INT(_i_53881)) {
            _28004 = _pc_53878 + _i_53881;
        }
        else {
            _28004 = NewDouble((double)_pc_53878 + DBL_PTR(_i_53881)->dbl);
        }
        _2 = (int)SEQ_PTR(_67inline_code_53715);
        if (!IS_ATOM_INT(_28004)){
            _28005 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_28004)->dbl));
        }
        else{
            _28005 = (int)*(((s1_ptr)_2)->base + _28004);
        }
        if (IS_ATOM_INT(_28005))
        _28006 = 1;
        else if (IS_ATOM_DBL(_28005))
        _28006 = IS_ATOM_INT(DoubleToInt(_28005));
        else
        _28006 = 0;
        _28005 = NOVALUE;
        if (_28006 == 0)
        {
            _28006 = NOVALUE;
            goto L4; // [88] 213
        }
        else{
            _28006 = NOVALUE;
        }

        /** 				inline_code[pc + i] = { INLINE_ADDR, inline_code[pc + i] }*/
        if (IS_ATOM_INT(_i_53881)) {
            _28007 = _pc_53878 + _i_53881;
            if ((long)((unsigned long)_28007 + (unsigned long)HIGH_BITS) >= 0) 
            _28007 = NewDouble((double)_28007);
        }
        else {
            _28007 = NewDouble((double)_pc_53878 + DBL_PTR(_i_53881)->dbl);
        }
        if (IS_ATOM_INT(_i_53881)) {
            _28008 = _pc_53878 + _i_53881;
        }
        else {
            _28008 = NewDouble((double)_pc_53878 + DBL_PTR(_i_53881)->dbl);
        }
        _2 = (int)SEQ_PTR(_67inline_code_53715);
        if (!IS_ATOM_INT(_28008)){
            _28009 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_28008)->dbl));
        }
        else{
            _28009 = (int)*(((s1_ptr)_2)->base + _28008);
        }
        Ref(_28009);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = 4;
        ((int *)_2)[2] = _28009;
        _28010 = MAKE_SEQ(_1);
        _28009 = NOVALUE;
        _2 = (int)SEQ_PTR(_67inline_code_53715);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _67inline_code_53715 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_28007))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_28007)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _28007);
        _1 = *(int *)_2;
        *(int *)_2 = _28010;
        if( _1 != _28010 ){
            DeRef(_1);
        }
        _28010 = NOVALUE;
        goto L4; // [120] 213
L3: 

        /** 		elsif sub then*/
        if (_sub_53893 == 0)
        {
            goto L5; // [125] 149
        }
        else{
        }

        /** 			inline_code[pc+i] = {INLINE_SUB}*/
        if (IS_ATOM_INT(_i_53881)) {
            _28011 = _pc_53878 + _i_53881;
            if ((long)((unsigned long)_28011 + (unsigned long)HIGH_BITS) >= 0) 
            _28011 = NewDouble((double)_28011);
        }
        else {
            _28011 = NewDouble((double)_pc_53878 + DBL_PTR(_i_53881)->dbl);
        }
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = 5;
        _28012 = MAKE_SEQ(_1);
        _2 = (int)SEQ_PTR(_67inline_code_53715);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _67inline_code_53715 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_28011))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_28011)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _28011);
        _1 = *(int *)_2;
        *(int *)_2 = _28012;
        if( _1 != _28012 ){
            DeRef(_1);
        }
        _28012 = NOVALUE;
        goto L4; // [146] 213
L5: 

        /** 			if op != STARTLINE and op != COVERAGE_LINE and op != COVERAGE_ROUTINE then*/
        _28013 = (_op_53879 != 58);
        if (_28013 == 0) {
            _28014 = 0;
            goto L6; // [157] 171
        }
        _28015 = (_op_53879 != 210);
        _28014 = (_28015 != 0);
L6: 
        if (_28014 == 0) {
            goto L7; // [171] 212
        }
        _28017 = (_op_53879 != 211);
        if (_28017 == 0)
        {
            DeRef(_28017);
            _28017 = NOVALUE;
            goto L7; // [182] 212
        }
        else{
            DeRef(_28017);
            _28017 = NOVALUE;
        }

        /** 				check_target( pc, op )*/
        _67check_target(_pc_53878, _op_53879);

        /** 				if not adjust_symbol( pc + i ) then*/
        if (IS_ATOM_INT(_i_53881)) {
            _28018 = _pc_53878 + _i_53881;
            if ((long)((unsigned long)_28018 + (unsigned long)HIGH_BITS) >= 0) 
            _28018 = NewDouble((double)_28018);
        }
        else {
            _28018 = NewDouble((double)_pc_53878 + DBL_PTR(_i_53881)->dbl);
        }
        _28019 = _67adjust_symbol(_28018);
        _28018 = NOVALUE;
        if (IS_ATOM_INT(_28019)) {
            if (_28019 != 0){
                DeRef(_28019);
                _28019 = NOVALUE;
                goto L8; // [201] 211
            }
        }
        else {
            if (DBL_PTR(_28019)->dbl != 0.0){
                DeRef(_28019);
                _28019 = NOVALUE;
                goto L8; // [201] 211
            }
        }
        DeRef(_28019);
        _28019 = NOVALUE;

        /** 					return 0*/
        DeRef(_i_53881);
        DeRef(_28004);
        _28004 = NOVALUE;
        DeRef(_27997);
        _27997 = NOVALUE;
        DeRef(_28007);
        _28007 = NOVALUE;
        DeRef(_28008);
        _28008 = NOVALUE;
        DeRef(_28011);
        _28011 = NOVALUE;
        DeRef(_28013);
        _28013 = NOVALUE;
        DeRef(_28015);
        _28015 = NOVALUE;
        return 0;
L8: 
L7: 
L4: 

        /** 	end for*/
        _0 = _i_53881;
        if (IS_ATOM_INT(_i_53881)) {
            _i_53881 = _i_53881 + 1;
            if ((long)((unsigned long)_i_53881 +(unsigned long) HIGH_BITS) >= 0){
                _i_53881 = NewDouble((double)_i_53881);
            }
        }
        else {
            _i_53881 = binary_op_a(PLUS, _i_53881, 1);
        }
        DeRef(_0);
        goto L1; // [217] 30
L2: 
        ;
        DeRef(_i_53881);
    }

    /** 	return 1*/
    DeRef(_28004);
    _28004 = NOVALUE;
    DeRef(_27997);
    _27997 = NOVALUE;
    DeRef(_28007);
    _28007 = NOVALUE;
    DeRef(_28008);
    _28008 = NOVALUE;
    DeRef(_28011);
    _28011 = NOVALUE;
    DeRef(_28013);
    _28013 = NOVALUE;
    DeRef(_28015);
    _28015 = NOVALUE;
    return 1;
    ;
}


int _67is_temp(int _sym_53928)
{
    int _28030 = NOVALUE;
    int _28029 = NOVALUE;
    int _28028 = NOVALUE;
    int _28027 = NOVALUE;
    int _28026 = NOVALUE;
    int _28025 = NOVALUE;
    int _28024 = NOVALUE;
    int _28023 = NOVALUE;
    int _28022 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sym <= 0 then*/
    if (_sym_53928 > 0)
    goto L1; // [5] 16

    /** 		return 0*/
    return 0;
L1: 

    /** 	return (SymTab[sym][S_MODE] = M_TEMP) and (not TRANSLATE or equal( NOVALUE, SymTab[sym][S_OBJ]) )*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28022 = (int)*(((s1_ptr)_2)->base + _sym_53928);
    _2 = (int)SEQ_PTR(_28022);
    _28023 = (int)*(((s1_ptr)_2)->base + 3);
    _28022 = NOVALUE;
    if (IS_ATOM_INT(_28023)) {
        _28024 = (_28023 == 3);
    }
    else {
        _28024 = binary_op(EQUALS, _28023, 3);
    }
    _28023 = NOVALUE;
    _28025 = (_25TRANSLATE_11874 == 0);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28026 = (int)*(((s1_ptr)_2)->base + _sym_53928);
    _2 = (int)SEQ_PTR(_28026);
    _28027 = (int)*(((s1_ptr)_2)->base + 1);
    _28026 = NOVALUE;
    if (_25NOVALUE_12115 == _28027)
    _28028 = 1;
    else if (IS_ATOM_INT(_25NOVALUE_12115) && IS_ATOM_INT(_28027))
    _28028 = 0;
    else
    _28028 = (compare(_25NOVALUE_12115, _28027) == 0);
    _28027 = NOVALUE;
    _28029 = (_28025 != 0 || _28028 != 0);
    _28025 = NOVALUE;
    _28028 = NOVALUE;
    if (IS_ATOM_INT(_28024)) {
        _28030 = (_28024 != 0 && _28029 != 0);
    }
    else {
        _28030 = binary_op(AND, _28024, _28029);
    }
    DeRef(_28024);
    _28024 = NOVALUE;
    _28029 = NOVALUE;
    return _28030;
    ;
}


int _67is_literal(int _sym_53950)
{
    int _mode_53953 = NOVALUE;
    int _28045 = NOVALUE;
    int _28044 = NOVALUE;
    int _28043 = NOVALUE;
    int _28042 = NOVALUE;
    int _28041 = NOVALUE;
    int _28040 = NOVALUE;
    int _28038 = NOVALUE;
    int _28037 = NOVALUE;
    int _28036 = NOVALUE;
    int _28035 = NOVALUE;
    int _28034 = NOVALUE;
    int _28032 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sym <= 0 then*/
    if (_sym_53950 > 0)
    goto L1; // [5] 16

    /** 		return 0*/
    return 0;
L1: 

    /** 	integer mode = SymTab[sym][S_MODE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28032 = (int)*(((s1_ptr)_2)->base + _sym_53950);
    _2 = (int)SEQ_PTR(_28032);
    _mode_53953 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_mode_53953)){
        _mode_53953 = (long)DBL_PTR(_mode_53953)->dbl;
    }
    _28032 = NOVALUE;

    /** 	if (mode = M_CONSTANT and eu:compare( NOVALUE, SymTab[sym][S_OBJ]) ) */
    _28034 = (_mode_53953 == 2);
    if (_28034 == 0) {
        _28035 = 0;
        goto L2; // [40] 66
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28036 = (int)*(((s1_ptr)_2)->base + _sym_53950);
    _2 = (int)SEQ_PTR(_28036);
    _28037 = (int)*(((s1_ptr)_2)->base + 1);
    _28036 = NOVALUE;
    if (IS_ATOM_INT(_25NOVALUE_12115) && IS_ATOM_INT(_28037)){
        _28038 = (_25NOVALUE_12115 < _28037) ? -1 : (_25NOVALUE_12115 > _28037);
    }
    else{
        _28038 = compare(_25NOVALUE_12115, _28037);
    }
    _28037 = NOVALUE;
    _28035 = (_28038 != 0);
L2: 
    if (_28035 != 0) {
        goto L3; // [66] 117
    }
    if (_25TRANSLATE_11874 == 0) {
        _28040 = 0;
        goto L4; // [72] 86
    }
    _28041 = (_mode_53953 == 3);
    _28040 = (_28041 != 0);
L4: 
    if (_28040 == 0) {
        DeRef(_28042);
        _28042 = 0;
        goto L5; // [86] 112
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28043 = (int)*(((s1_ptr)_2)->base + _sym_53950);
    _2 = (int)SEQ_PTR(_28043);
    _28044 = (int)*(((s1_ptr)_2)->base + 1);
    _28043 = NOVALUE;
    if (IS_ATOM_INT(_28044) && IS_ATOM_INT(_25NOVALUE_12115)){
        _28045 = (_28044 < _25NOVALUE_12115) ? -1 : (_28044 > _25NOVALUE_12115);
    }
    else{
        _28045 = compare(_28044, _25NOVALUE_12115);
    }
    _28044 = NOVALUE;
    _28042 = (_28045 != 0);
L5: 
    if (_28042 == 0)
    {
        _28042 = NOVALUE;
        goto L6; // [113] 126
    }
    else{
        _28042 = NOVALUE;
    }
L3: 

    /** 		return 1*/
    DeRef(_28034);
    _28034 = NOVALUE;
    DeRef(_28041);
    _28041 = NOVALUE;
    return 1;
    goto L7; // [123] 133
L6: 

    /** 		return 0*/
    DeRef(_28034);
    _28034 = NOVALUE;
    DeRef(_28041);
    _28041 = NOVALUE;
    return 0;
L7: 
    ;
}


int _67returnf(int _pc_54000)
{
    int _retsym_54002 = NOVALUE;
    int _code_54035 = NOVALUE;
    int _ret_pc_54036 = NOVALUE;
    int _code_54083 = NOVALUE;
    int _ret_pc_54098 = NOVALUE;
    int _28121 = NOVALUE;
    int _28120 = NOVALUE;
    int _28118 = NOVALUE;
    int _28116 = NOVALUE;
    int _28115 = NOVALUE;
    int _28113 = NOVALUE;
    int _28112 = NOVALUE;
    int _28110 = NOVALUE;
    int _28109 = NOVALUE;
    int _28108 = NOVALUE;
    int _28106 = NOVALUE;
    int _28105 = NOVALUE;
    int _28104 = NOVALUE;
    int _28102 = NOVALUE;
    int _28100 = NOVALUE;
    int _28099 = NOVALUE;
    int _28097 = NOVALUE;
    int _28096 = NOVALUE;
    int _28094 = NOVALUE;
    int _28093 = NOVALUE;
    int _28092 = NOVALUE;
    int _28090 = NOVALUE;
    int _28089 = NOVALUE;
    int _28088 = NOVALUE;
    int _28087 = NOVALUE;
    int _28086 = NOVALUE;
    int _28085 = NOVALUE;
    int _28084 = NOVALUE;
    int _28083 = NOVALUE;
    int _28082 = NOVALUE;
    int _28081 = NOVALUE;
    int _28080 = NOVALUE;
    int _28079 = NOVALUE;
    int _28077 = NOVALUE;
    int _28075 = NOVALUE;
    int _28074 = NOVALUE;
    int _28073 = NOVALUE;
    int _28072 = NOVALUE;
    int _28071 = NOVALUE;
    int _28070 = NOVALUE;
    int _28069 = NOVALUE;
    int _28068 = NOVALUE;
    int _28067 = NOVALUE;
    int _28065 = NOVALUE;
    int _28064 = NOVALUE;
    int _28063 = NOVALUE;
    int _28061 = NOVALUE;
    int _28060 = NOVALUE;
    int _28059 = NOVALUE;
    int _28058 = NOVALUE;
    int _28057 = NOVALUE;
    int _28056 = NOVALUE;
    int _28054 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	symtab_index retsym = inline_code[pc+3]*/
    _28054 = _pc_54000 + 3;
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    _retsym_54002 = (int)*(((s1_ptr)_2)->base + _28054);
    if (!IS_ATOM_INT(_retsym_54002)){
        _retsym_54002 = (long)DBL_PTR(_retsym_54002)->dbl;
    }

    /** 	if equal( inline_code[$], BADRETURNF ) then*/
    if (IS_SEQUENCE(_67inline_code_53715)){
            _28056 = SEQ_PTR(_67inline_code_53715)->length;
    }
    else {
        _28056 = 1;
    }
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    _28057 = (int)*(((s1_ptr)_2)->base + _28056);
    if (_28057 == 43)
    _28058 = 1;
    else if (IS_ATOM_INT(_28057) && IS_ATOM_INT(43))
    _28058 = 0;
    else
    _28058 = (compare(_28057, 43) == 0);
    _28057 = NOVALUE;
    if (_28058 == 0)
    {
        _28058 = NOVALUE;
        goto L1; // [34] 102
    }
    else{
        _28058 = NOVALUE;
    }

    /** 		if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L2; // [41] 60
    }
    else{
    }

    /** 			inline_code[$] = NOP1*/
    if (IS_SEQUENCE(_67inline_code_53715)){
            _28059 = SEQ_PTR(_67inline_code_53715)->length;
    }
    else {
        _28059 = 1;
    }
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _67inline_code_53715 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _28059);
    _1 = *(int *)_2;
    *(int *)_2 = 159;
    DeRef(_1);
    goto L3; // [57] 101
L2: 

    /** 		elsif SymTab[inline_sub][S_TOKEN] = PROC then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28060 = (int)*(((s1_ptr)_2)->base + _67inline_sub_53729);
    _2 = (int)SEQ_PTR(_28060);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _28061 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _28061 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _28060 = NOVALUE;
    if (binary_op_a(NOTEQ, _28061, 27)){
        _28061 = NOVALUE;
        goto L4; // [78] 100
    }
    _28061 = NOVALUE;

    /** 			replace_code( {}, length(inline_code), length(inline_code) )*/
    if (IS_SEQUENCE(_67inline_code_53715)){
            _28063 = SEQ_PTR(_67inline_code_53715)->length;
    }
    else {
        _28063 = 1;
    }
    if (IS_SEQUENCE(_67inline_code_53715)){
            _28064 = SEQ_PTR(_67inline_code_53715)->length;
    }
    else {
        _28064 = 1;
    }
    RefDS(_22682);
    _67replace_code(_22682, _28063, _28064);
    _28063 = NOVALUE;
    _28064 = NOVALUE;
L4: 
L3: 
L1: 

    /** 	if is_temp( retsym ) */
    _28065 = _67is_temp(_retsym_54002);
    if (IS_ATOM_INT(_28065)) {
        if (_28065 != 0) {
            goto L5; // [108] 150
        }
    }
    else {
        if (DBL_PTR(_28065)->dbl != 0.0) {
            goto L5; // [108] 150
        }
    }
    _28067 = _67is_literal(_retsym_54002);
    if (IS_ATOM_INT(_28067)) {
        _28068 = (_28067 == 0);
    }
    else {
        _28068 = unary_op(NOT, _28067);
    }
    DeRef(_28067);
    _28067 = NOVALUE;
    if (IS_ATOM_INT(_28068)) {
        if (_28068 == 0) {
            DeRef(_28069);
            _28069 = 0;
            goto L6; // [119] 145
        }
    }
    else {
        if (DBL_PTR(_28068)->dbl == 0.0) {
            DeRef(_28069);
            _28069 = 0;
            goto L6; // [119] 145
        }
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28070 = (int)*(((s1_ptr)_2)->base + _retsym_54002);
    _2 = (int)SEQ_PTR(_28070);
    _28071 = (int)*(((s1_ptr)_2)->base + 4);
    _28070 = NOVALUE;
    if (IS_ATOM_INT(_28071)) {
        _28072 = (_28071 <= 3);
    }
    else {
        _28072 = binary_op(LESSEQ, _28071, 3);
    }
    _28071 = NOVALUE;
    DeRef(_28069);
    if (IS_ATOM_INT(_28072))
    _28069 = (_28072 != 0);
    else
    _28069 = DBL_PTR(_28072)->dbl != 0.0;
L6: 
    if (_28069 == 0)
    {
        _28069 = NOVALUE;
        goto L7; // [146] 415
    }
    else{
        _28069 = NOVALUE;
    }
L5: 

    /** 		sequence code = {}*/
    RefDS(_22682);
    DeRef(_code_54035);
    _code_54035 = _22682;

    /** 		integer ret_pc = 0*/
    _ret_pc_54036 = 0;

    /** 		if not (find( retsym, inline_params ) or find( retsym, proc_vars )) then*/
    _28073 = find_from(_retsym_54002, _67inline_params_53720, 1);
    if (_28073 != 0) {
        DeRef(_28074);
        _28074 = 1;
        goto L8; // [171] 186
    }
    _28075 = find_from(_retsym_54002, _67proc_vars_53716, 1);
    _28074 = (_28075 != 0);
L8: 
    if (_28074 != 0)
    goto L9; // [186] 206
    _28074 = NOVALUE;

    /** 			ret_pc = rfind( generic_symbol( retsym ), inline_code, pc )*/
    _28077 = _67generic_symbol(_retsym_54002);
    RefDS(_67inline_code_53715);
    _ret_pc_54036 = _7rfind(_28077, _67inline_code_53715, _pc_54000);
    _28077 = NOVALUE;
    if (!IS_ATOM_INT(_ret_pc_54036)) {
        _1 = (long)(DBL_PTR(_ret_pc_54036)->dbl);
        if (UNIQUE(DBL_PTR(_ret_pc_54036)) && (DBL_PTR(_ret_pc_54036)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret_pc_54036);
        _ret_pc_54036 = _1;
    }
L9: 

    /** 		if ret_pc and eu:compare( inline_code[ret_pc-1], PRIVATE_INIT_CHECK ) then*/
    if (_ret_pc_54036 == 0) {
        goto LA; // [208] 289
    }
    _28080 = _ret_pc_54036 - 1;
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    _28081 = (int)*(((s1_ptr)_2)->base + _28080);
    if (IS_ATOM_INT(_28081) && IS_ATOM_INT(30)){
        _28082 = (_28081 < 30) ? -1 : (_28081 > 30);
    }
    else{
        _28082 = compare(_28081, 30);
    }
    _28081 = NOVALUE;
    if (_28082 == 0)
    {
        _28082 = NOVALUE;
        goto LA; // [229] 289
    }
    else{
        _28082 = NOVALUE;
    }

    /** 			inline_code[ret_pc] = {INLINE_TARGET}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 3;
    _28083 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _67inline_code_53715 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _ret_pc_54036);
    _1 = *(int *)_2;
    *(int *)_2 = _28083;
    if( _1 != _28083 ){
        DeRef(_1);
    }
    _28083 = NOVALUE;

    /** 			if equal( inline_code[ret_pc-1], REF_TEMP ) then*/
    _28084 = _ret_pc_54036 - 1;
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    _28085 = (int)*(((s1_ptr)_2)->base + _28084);
    if (_28085 == 207)
    _28086 = 1;
    else if (IS_ATOM_INT(_28085) && IS_ATOM_INT(207))
    _28086 = 0;
    else
    _28086 = (compare(_28085, 207) == 0);
    _28085 = NOVALUE;
    if (_28086 == 0)
    {
        _28086 = NOVALUE;
        goto LB; // [264] 310
    }
    else{
        _28086 = NOVALUE;
    }

    /** 				inline_code[ret_pc-2] = {INLINE_TARGET}*/
    _28087 = _ret_pc_54036 - 2;
    if ((long)((unsigned long)_28087 +(unsigned long) HIGH_BITS) >= 0){
        _28087 = NewDouble((double)_28087);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 3;
    _28088 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _67inline_code_53715 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_28087))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_28087)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _28087);
    _1 = *(int *)_2;
    *(int *)_2 = _28088;
    if( _1 != _28088 ){
        DeRef(_1);
    }
    _28088 = NOVALUE;
    goto LB; // [286] 310
LA: 

    /** 			code = {ASSIGN, generic_symbol( retsym ), {INLINE_TARGET}}*/
    _28089 = _67generic_symbol(_retsym_54002);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 3;
    _28090 = MAKE_SEQ(_1);
    _0 = _code_54035;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 18;
    *((int *)(_2+8)) = _28089;
    *((int *)(_2+12)) = _28090;
    _code_54035 = MAKE_SEQ(_1);
    DeRef(_0);
    _28090 = NOVALUE;
    _28089 = NOVALUE;
LB: 

    /** 		if pc != length( inline_code ) - ( 3 + TRANSLATE ) then*/
    if (IS_SEQUENCE(_67inline_code_53715)){
            _28092 = SEQ_PTR(_67inline_code_53715)->length;
    }
    else {
        _28092 = 1;
    }
    _28093 = 3 + _25TRANSLATE_11874;
    _28094 = _28092 - _28093;
    _28092 = NOVALUE;
    _28093 = NOVALUE;
    if (_pc_54000 == _28094)
    goto LC; // [327] 350

    /** 			code &= { ELSE, {INLINE_ADDR, -1 }}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 4;
    ((int *)_2)[2] = -1;
    _28096 = MAKE_SEQ(_1);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 23;
    ((int *)_2)[2] = _28096;
    _28097 = MAKE_SEQ(_1);
    _28096 = NOVALUE;
    Concat((object_ptr)&_code_54035, _code_54035, _28097);
    DeRefDS(_28097);
    _28097 = NOVALUE;
LC: 

    /** 		replace_code( code, pc, pc + 3 )*/
    _28099 = _pc_54000 + 3;
    if ((long)((unsigned long)_28099 + (unsigned long)HIGH_BITS) >= 0) 
    _28099 = NewDouble((double)_28099);
    RefDS(_code_54035);
    _67replace_code(_code_54035, _pc_54000, _28099);
    _28099 = NOVALUE;

    /** 		ret_pc = find( { INLINE_ADDR, -1 }, inline_code, pc )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 4;
    ((int *)_2)[2] = -1;
    _28100 = MAKE_SEQ(_1);
    _ret_pc_54036 = find_from(_28100, _67inline_code_53715, _pc_54000);
    DeRefDS(_28100);
    _28100 = NOVALUE;

    /** 		if ret_pc then*/
    if (_ret_pc_54036 == 0)
    {
        goto LD; // [378] 404
    }
    else{
    }

    /** 			inline_code[ret_pc][2] = length(inline_code) + 1*/
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _67inline_code_53715 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ret_pc_54036 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_67inline_code_53715)){
            _28104 = SEQ_PTR(_67inline_code_53715)->length;
    }
    else {
        _28104 = 1;
    }
    _28105 = _28104 + 1;
    _28104 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _28105;
    if( _1 != _28105 ){
        DeRef(_1);
    }
    _28105 = NOVALUE;
    _28102 = NOVALUE;
LD: 

    /** 		return 1*/
    DeRef(_code_54035);
    DeRef(_28054);
    _28054 = NOVALUE;
    DeRef(_28065);
    _28065 = NOVALUE;
    DeRef(_28068);
    _28068 = NOVALUE;
    DeRef(_28080);
    _28080 = NOVALUE;
    DeRef(_28072);
    _28072 = NOVALUE;
    DeRef(_28084);
    _28084 = NOVALUE;
    DeRef(_28087);
    _28087 = NOVALUE;
    DeRef(_28094);
    _28094 = NOVALUE;
    return 1;
    goto LE; // [412] 534
L7: 

    /** 		sequence code = {ASSIGN, retsym, {INLINE_TARGET}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 3;
    _28106 = MAKE_SEQ(_1);
    _0 = _code_54083;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 18;
    *((int *)(_2+8)) = _retsym_54002;
    *((int *)(_2+12)) = _28106;
    _code_54083 = MAKE_SEQ(_1);
    DeRef(_0);
    _28106 = NOVALUE;

    /** 		if pc != length( inline_code ) - ( 3 + TRANSLATE ) then*/
    if (IS_SEQUENCE(_67inline_code_53715)){
            _28108 = SEQ_PTR(_67inline_code_53715)->length;
    }
    else {
        _28108 = 1;
    }
    _28109 = 3 + _25TRANSLATE_11874;
    _28110 = _28108 - _28109;
    _28108 = NOVALUE;
    _28109 = NOVALUE;
    if (_pc_54000 == _28110)
    goto LF; // [448] 471

    /** 			code &= { ELSE, {INLINE_ADDR, -1 }}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 4;
    ((int *)_2)[2] = -1;
    _28112 = MAKE_SEQ(_1);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 23;
    ((int *)_2)[2] = _28112;
    _28113 = MAKE_SEQ(_1);
    _28112 = NOVALUE;
    Concat((object_ptr)&_code_54083, _code_54083, _28113);
    DeRefDS(_28113);
    _28113 = NOVALUE;
LF: 

    /** 		replace_code( code, pc, pc + 3 )*/
    _28115 = _pc_54000 + 3;
    if ((long)((unsigned long)_28115 + (unsigned long)HIGH_BITS) >= 0) 
    _28115 = NewDouble((double)_28115);
    RefDS(_code_54083);
    _67replace_code(_code_54083, _pc_54000, _28115);
    _28115 = NOVALUE;

    /** 		integer ret_pc = find( { INLINE_ADDR, -1 }, inline_code, pc )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 4;
    ((int *)_2)[2] = -1;
    _28116 = MAKE_SEQ(_1);
    _ret_pc_54098 = find_from(_28116, _67inline_code_53715, _pc_54000);
    DeRefDS(_28116);
    _28116 = NOVALUE;

    /** 		if ret_pc then*/
    if (_ret_pc_54098 == 0)
    {
        goto L10; // [499] 525
    }
    else{
    }

    /** 			inline_code[ret_pc][2] = length(inline_code) + 1*/
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _67inline_code_53715 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ret_pc_54098 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_67inline_code_53715)){
            _28120 = SEQ_PTR(_67inline_code_53715)->length;
    }
    else {
        _28120 = 1;
    }
    _28121 = _28120 + 1;
    _28120 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _28121;
    if( _1 != _28121 ){
        DeRef(_1);
    }
    _28121 = NOVALUE;
    _28118 = NOVALUE;
L10: 

    /** 		return 1*/
    DeRef(_code_54083);
    DeRef(_28054);
    _28054 = NOVALUE;
    DeRef(_28065);
    _28065 = NOVALUE;
    DeRef(_28068);
    _28068 = NOVALUE;
    DeRef(_28080);
    _28080 = NOVALUE;
    DeRef(_28072);
    _28072 = NOVALUE;
    DeRef(_28084);
    _28084 = NOVALUE;
    DeRef(_28087);
    _28087 = NOVALUE;
    DeRef(_28094);
    _28094 = NOVALUE;
    DeRef(_28110);
    _28110 = NOVALUE;
    return 1;
LE: 

    /** 	return 0*/
    DeRef(_28054);
    _28054 = NOVALUE;
    DeRef(_28065);
    _28065 = NOVALUE;
    DeRef(_28068);
    _28068 = NOVALUE;
    DeRef(_28080);
    _28080 = NOVALUE;
    DeRef(_28072);
    _28072 = NOVALUE;
    DeRef(_28084);
    _28084 = NOVALUE;
    DeRef(_28087);
    _28087 = NOVALUE;
    DeRef(_28094);
    _28094 = NOVALUE;
    DeRef(_28110);
    _28110 = NOVALUE;
    return 0;
    ;
}


int _67inline_op(int _pc_54108)
{
    int _op_54109 = NOVALUE;
    int _code_54114 = NOVALUE;
    int _stlen_54147 = NOVALUE;
    int _file_54152 = NOVALUE;
    int _ok_54157 = NOVALUE;
    int _original_table_54180 = NOVALUE;
    int _jump_table_54184 = NOVALUE;
    int _28183 = NOVALUE;
    int _28182 = NOVALUE;
    int _28181 = NOVALUE;
    int _28180 = NOVALUE;
    int _28179 = NOVALUE;
    int _28178 = NOVALUE;
    int _28177 = NOVALUE;
    int _28176 = NOVALUE;
    int _28175 = NOVALUE;
    int _28174 = NOVALUE;
    int _28173 = NOVALUE;
    int _28172 = NOVALUE;
    int _28171 = NOVALUE;
    int _28168 = NOVALUE;
    int _28167 = NOVALUE;
    int _28166 = NOVALUE;
    int _28165 = NOVALUE;
    int _28163 = NOVALUE;
    int _28161 = NOVALUE;
    int _28160 = NOVALUE;
    int _28158 = NOVALUE;
    int _28154 = NOVALUE;
    int _28153 = NOVALUE;
    int _28152 = NOVALUE;
    int _28151 = NOVALUE;
    int _28150 = NOVALUE;
    int _28149 = NOVALUE;
    int _28146 = NOVALUE;
    int _28145 = NOVALUE;
    int _28143 = NOVALUE;
    int _28142 = NOVALUE;
    int _28140 = NOVALUE;
    int _28138 = NOVALUE;
    int _28137 = NOVALUE;
    int _28136 = NOVALUE;
    int _28135 = NOVALUE;
    int _28134 = NOVALUE;
    int _28133 = NOVALUE;
    int _28132 = NOVALUE;
    int _28130 = NOVALUE;
    int _28129 = NOVALUE;
    int _28128 = NOVALUE;
    int _28126 = NOVALUE;
    int _28125 = NOVALUE;
    int _28124 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	integer op = inline_code[pc]*/
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    _op_54109 = (int)*(((s1_ptr)_2)->base + _pc_54108);
    if (!IS_ATOM_INT(_op_54109))
    _op_54109 = (long)DBL_PTR(_op_54109)->dbl;

    /** 	if op = RETURNP then*/
    if (_op_54109 != 29)
    goto L1; // [15] 152

    /** 		sequence code = ""*/
    RefDS(_22682);
    DeRef(_code_54114);
    _code_54114 = _22682;

    /** 		if pc != length( inline_code ) - 1 - TRANSLATE then*/
    if (IS_SEQUENCE(_67inline_code_53715)){
            _28124 = SEQ_PTR(_67inline_code_53715)->length;
    }
    else {
        _28124 = 1;
    }
    _28125 = _28124 - 1;
    _28124 = NOVALUE;
    _28126 = _28125 - _25TRANSLATE_11874;
    _28125 = NOVALUE;
    if (_pc_54108 == _28126)
    goto L2; // [43] 94

    /** 			code = { ELSE, {INLINE_ADDR, length( inline_code ) + 1 }}*/
    if (IS_SEQUENCE(_67inline_code_53715)){
            _28128 = SEQ_PTR(_67inline_code_53715)->length;
    }
    else {
        _28128 = 1;
    }
    _28129 = _28128 + 1;
    _28128 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 4;
    ((int *)_2)[2] = _28129;
    _28130 = MAKE_SEQ(_1);
    _28129 = NOVALUE;
    DeRefDS(_code_54114);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 23;
    ((int *)_2)[2] = _28130;
    _code_54114 = MAKE_SEQ(_1);
    _28130 = NOVALUE;

    /** 			if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L3; // [74] 136
    }
    else{
    }

    /** 				inline_code[$] = NOP1*/
    if (IS_SEQUENCE(_67inline_code_53715)){
            _28132 = SEQ_PTR(_67inline_code_53715)->length;
    }
    else {
        _28132 = 1;
    }
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _67inline_code_53715 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _28132);
    _1 = *(int *)_2;
    *(int *)_2 = 159;
    DeRef(_1);
    goto L3; // [91] 136
L2: 

    /** 		elsif TRANSLATE and inline_code[$] = BADRETURNF then*/
    if (_25TRANSLATE_11874 == 0) {
        goto L4; // [98] 135
    }
    if (IS_SEQUENCE(_67inline_code_53715)){
            _28134 = SEQ_PTR(_67inline_code_53715)->length;
    }
    else {
        _28134 = 1;
    }
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    _28135 = (int)*(((s1_ptr)_2)->base + _28134);
    if (IS_ATOM_INT(_28135)) {
        _28136 = (_28135 == 43);
    }
    else {
        _28136 = binary_op(EQUALS, _28135, 43);
    }
    _28135 = NOVALUE;
    if (_28136 == 0) {
        DeRef(_28136);
        _28136 = NOVALUE;
        goto L4; // [118] 135
    }
    else {
        if (!IS_ATOM_INT(_28136) && DBL_PTR(_28136)->dbl == 0.0){
            DeRef(_28136);
            _28136 = NOVALUE;
            goto L4; // [118] 135
        }
        DeRef(_28136);
        _28136 = NOVALUE;
    }
    DeRef(_28136);
    _28136 = NOVALUE;

    /** 			inline_code[$] = NOP1*/
    if (IS_SEQUENCE(_67inline_code_53715)){
            _28137 = SEQ_PTR(_67inline_code_53715)->length;
    }
    else {
        _28137 = 1;
    }
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _67inline_code_53715 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _28137);
    _1 = *(int *)_2;
    *(int *)_2 = 159;
    DeRef(_1);
L4: 
L3: 

    /** 		replace_code( code, pc, pc + 2 )*/
    _28138 = _pc_54108 + 2;
    if ((long)((unsigned long)_28138 + (unsigned long)HIGH_BITS) >= 0) 
    _28138 = NewDouble((double)_28138);
    RefDS(_code_54114);
    _67replace_code(_code_54114, _pc_54108, _28138);
    _28138 = NOVALUE;
    DeRefDS(_code_54114);
    _code_54114 = NOVALUE;
    goto L5; // [149] 534
L1: 

    /** 	elsif op = RETURNF then*/
    if (_op_54109 != 28)
    goto L6; // [156] 173

    /** 		return returnf( pc )*/
    _28140 = _67returnf(_pc_54108);
    DeRef(_28126);
    _28126 = NOVALUE;
    return _28140;
    goto L5; // [170] 534
L6: 

    /** 	elsif op = ROUTINE_ID then*/
    if (_op_54109 != 134)
    goto L7; // [177] 275

    /** 		integer*/

    /** 			stlen = inline_code[pc+2+TRANSLATE],*/
    _28142 = _pc_54108 + 2;
    if ((long)((unsigned long)_28142 + (unsigned long)HIGH_BITS) >= 0) 
    _28142 = NewDouble((double)_28142);
    if (IS_ATOM_INT(_28142)) {
        _28143 = _28142 + _25TRANSLATE_11874;
    }
    else {
        _28143 = NewDouble(DBL_PTR(_28142)->dbl + (double)_25TRANSLATE_11874);
    }
    DeRef(_28142);
    _28142 = NOVALUE;
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    if (!IS_ATOM_INT(_28143)){
        _stlen_54147 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_28143)->dbl));
    }
    else{
        _stlen_54147 = (int)*(((s1_ptr)_2)->base + _28143);
    }
    if (!IS_ATOM_INT(_stlen_54147))
    _stlen_54147 = (long)DBL_PTR(_stlen_54147)->dbl;

    /** 			file  = inline_code[pc+4+TRANSLATE],*/
    _28145 = _pc_54108 + 4;
    if ((long)((unsigned long)_28145 + (unsigned long)HIGH_BITS) >= 0) 
    _28145 = NewDouble((double)_28145);
    if (IS_ATOM_INT(_28145)) {
        _28146 = _28145 + _25TRANSLATE_11874;
    }
    else {
        _28146 = NewDouble(DBL_PTR(_28145)->dbl + (double)_25TRANSLATE_11874);
    }
    DeRef(_28145);
    _28145 = NOVALUE;
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    if (!IS_ATOM_INT(_28146)){
        _file_54152 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_28146)->dbl));
    }
    else{
        _file_54152 = (int)*(((s1_ptr)_2)->base + _28146);
    }
    if (!IS_ATOM_INT(_file_54152))
    _file_54152 = (long)DBL_PTR(_file_54152)->dbl;

    /** 			ok    = adjust_il( pc, op )*/
    _ok_54157 = _67adjust_il(_pc_54108, _op_54109);
    if (!IS_ATOM_INT(_ok_54157)) {
        _1 = (long)(DBL_PTR(_ok_54157)->dbl);
        if (UNIQUE(DBL_PTR(_ok_54157)) && (DBL_PTR(_ok_54157)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ok_54157);
        _ok_54157 = _1;
    }

    /** 		inline_code[pc+2+TRANSLATE] = stlen*/
    _28149 = _pc_54108 + 2;
    if ((long)((unsigned long)_28149 + (unsigned long)HIGH_BITS) >= 0) 
    _28149 = NewDouble((double)_28149);
    if (IS_ATOM_INT(_28149)) {
        _28150 = _28149 + _25TRANSLATE_11874;
    }
    else {
        _28150 = NewDouble(DBL_PTR(_28149)->dbl + (double)_25TRANSLATE_11874);
    }
    DeRef(_28149);
    _28149 = NOVALUE;
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _67inline_code_53715 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_28150))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_28150)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _28150);
    _1 = *(int *)_2;
    *(int *)_2 = _stlen_54147;
    DeRef(_1);

    /** 		inline_code[pc+4+TRANSLATE] = file*/
    _28151 = _pc_54108 + 4;
    if ((long)((unsigned long)_28151 + (unsigned long)HIGH_BITS) >= 0) 
    _28151 = NewDouble((double)_28151);
    if (IS_ATOM_INT(_28151)) {
        _28152 = _28151 + _25TRANSLATE_11874;
    }
    else {
        _28152 = NewDouble(DBL_PTR(_28151)->dbl + (double)_25TRANSLATE_11874);
    }
    DeRef(_28151);
    _28151 = NOVALUE;
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _67inline_code_53715 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_28152))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_28152)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _28152);
    _1 = *(int *)_2;
    *(int *)_2 = _file_54152;
    DeRef(_1);

    /** 		return ok*/
    DeRef(_28140);
    _28140 = NOVALUE;
    DeRef(_28126);
    _28126 = NOVALUE;
    DeRef(_28143);
    _28143 = NOVALUE;
    DeRef(_28146);
    _28146 = NOVALUE;
    DeRef(_28150);
    _28150 = NOVALUE;
    DeRef(_28152);
    _28152 = NOVALUE;
    return _ok_54157;
    goto L5; // [272] 534
L7: 

    /** 	elsif op_info[op][OP_SIZE_TYPE] = FIXED_SIZE then*/
    _2 = (int)SEQ_PTR(_65op_info_27470);
    _28153 = (int)*(((s1_ptr)_2)->base + _op_54109);
    _2 = (int)SEQ_PTR(_28153);
    _28154 = (int)*(((s1_ptr)_2)->base + 1);
    _28153 = NOVALUE;
    if (binary_op_a(NOTEQ, _28154, 1)){
        _28154 = NOVALUE;
        goto L8; // [291] 399
    }
    _28154 = NOVALUE;

    /** 		switch op do*/
    _0 = _op_54109;
    switch ( _0 ){ 

        /** 			case SWITCH, SWITCH_RT, SWITCH_I, SWITCH_SPI then*/
        case 185:
        case 202:
        case 193:
        case 192:

        /** 				symtab_index original_table = inline_code[pc + 3]*/
        _28158 = _pc_54108 + 3;
        _2 = (int)SEQ_PTR(_67inline_code_53715);
        _original_table_54180 = (int)*(((s1_ptr)_2)->base + _28158);
        if (!IS_ATOM_INT(_original_table_54180)){
            _original_table_54180 = (long)DBL_PTR(_original_table_54180)->dbl;
        }

        /** 				symtab_index jump_table = NewStringSym( {-2, length(SymTab) } )*/
        if (IS_SEQUENCE(_26SymTab_11138)){
                _28160 = SEQ_PTR(_26SymTab_11138)->length;
        }
        else {
            _28160 = 1;
        }
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = -2;
        ((int *)_2)[2] = _28160;
        _28161 = MAKE_SEQ(_1);
        _28160 = NOVALUE;
        _jump_table_54184 = _52NewStringSym(_28161);
        _28161 = NOVALUE;
        if (!IS_ATOM_INT(_jump_table_54184)) {
            _1 = (long)(DBL_PTR(_jump_table_54184)->dbl);
            if (UNIQUE(DBL_PTR(_jump_table_54184)) && (DBL_PTR(_jump_table_54184)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_jump_table_54184);
            _jump_table_54184 = _1;
        }

        /** 				SymTab[jump_table][S_OBJ] = SymTab[original_table][S_OBJ]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_jump_table_54184 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _28165 = (int)*(((s1_ptr)_2)->base + _original_table_54180);
        _2 = (int)SEQ_PTR(_28165);
        _28166 = (int)*(((s1_ptr)_2)->base + 1);
        _28165 = NOVALUE;
        Ref(_28166);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 1);
        _1 = *(int *)_2;
        *(int *)_2 = _28166;
        if( _1 != _28166 ){
            DeRef(_1);
        }
        _28166 = NOVALUE;
        _28163 = NOVALUE;

        /** 				inline_code[pc+3] = jump_table*/
        _28167 = _pc_54108 + 3;
        _2 = (int)SEQ_PTR(_67inline_code_53715);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _67inline_code_53715 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _28167);
        _1 = *(int *)_2;
        *(int *)_2 = _jump_table_54184;
        DeRef(_1);
    ;}
    /** 		return adjust_il( pc, op )*/
    _28168 = _67adjust_il(_pc_54108, _op_54109);
    DeRef(_28140);
    _28140 = NOVALUE;
    DeRef(_28126);
    _28126 = NOVALUE;
    DeRef(_28158);
    _28158 = NOVALUE;
    DeRef(_28143);
    _28143 = NOVALUE;
    DeRef(_28146);
    _28146 = NOVALUE;
    DeRef(_28150);
    _28150 = NOVALUE;
    DeRef(_28152);
    _28152 = NOVALUE;
    DeRef(_28167);
    _28167 = NOVALUE;
    return _28168;
    goto L5; // [396] 534
L8: 

    /** 		switch op with fallthru do*/
    _0 = _op_54109;
    switch ( _0 ){ 

        /** 			case REF_TEMP then*/
        case 207:

        /** 				inline_code[pc+1] = {INLINE_TARGET}*/
        _28171 = _pc_54108 + 1;
        if (_28171 > MAXINT){
            _28171 = NewDouble((double)_28171);
        }
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = 3;
        _28172 = MAKE_SEQ(_1);
        _2 = (int)SEQ_PTR(_67inline_code_53715);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _67inline_code_53715 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_28171))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_28171)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _28171);
        _1 = *(int *)_2;
        *(int *)_2 = _28172;
        if( _1 != _28172 ){
            DeRef(_1);
        }
        _28172 = NOVALUE;

        /** 			case CONCAT_N then*/
        case 157:
        case 31:

        /** 				if check_for_param( pc + 2 + inline_code[pc+1] ) then*/
        _28173 = _pc_54108 + 2;
        if ((long)((unsigned long)_28173 + (unsigned long)HIGH_BITS) >= 0) 
        _28173 = NewDouble((double)_28173);
        _28174 = _pc_54108 + 1;
        _2 = (int)SEQ_PTR(_67inline_code_53715);
        _28175 = (int)*(((s1_ptr)_2)->base + _28174);
        if (IS_ATOM_INT(_28173) && IS_ATOM_INT(_28175)) {
            _28176 = _28173 + _28175;
            if ((long)((unsigned long)_28176 + (unsigned long)HIGH_BITS) >= 0) 
            _28176 = NewDouble((double)_28176);
        }
        else {
            _28176 = binary_op(PLUS, _28173, _28175);
        }
        DeRef(_28173);
        _28173 = NOVALUE;
        _28175 = NOVALUE;
        _28177 = _67check_for_param(_28176);
        _28176 = NOVALUE;
        if (_28177 == 0) {
            DeRef(_28177);
            _28177 = NOVALUE;
            goto L9; // [458] 462
        }
        else {
            if (!IS_ATOM_INT(_28177) && DBL_PTR(_28177)->dbl == 0.0){
                DeRef(_28177);
                _28177 = NOVALUE;
                goto L9; // [458] 462
            }
            DeRef(_28177);
            _28177 = NOVALUE;
        }
        DeRef(_28177);
        _28177 = NOVALUE;
L9: 

        /** 				for i = pc + 2 to pc + 2 + inline_code[pc+1] do*/
        _28178 = _pc_54108 + 2;
        if ((long)((unsigned long)_28178 + (unsigned long)HIGH_BITS) >= 0) 
        _28178 = NewDouble((double)_28178);
        _28179 = _pc_54108 + 2;
        if ((long)((unsigned long)_28179 + (unsigned long)HIGH_BITS) >= 0) 
        _28179 = NewDouble((double)_28179);
        _28180 = _pc_54108 + 1;
        _2 = (int)SEQ_PTR(_67inline_code_53715);
        _28181 = (int)*(((s1_ptr)_2)->base + _28180);
        if (IS_ATOM_INT(_28179) && IS_ATOM_INT(_28181)) {
            _28182 = _28179 + _28181;
            if ((long)((unsigned long)_28182 + (unsigned long)HIGH_BITS) >= 0) 
            _28182 = NewDouble((double)_28182);
        }
        else {
            _28182 = binary_op(PLUS, _28179, _28181);
        }
        DeRef(_28179);
        _28179 = NOVALUE;
        _28181 = NOVALUE;
        {
            int _i_54217;
            Ref(_28178);
            _i_54217 = _28178;
LA: 
            if (binary_op_a(GREATER, _i_54217, _28182)){
                goto LB; // [486] 516
            }

            /** 					if not adjust_symbol( i ) then*/
            Ref(_i_54217);
            _28183 = _67adjust_symbol(_i_54217);
            if (IS_ATOM_INT(_28183)) {
                if (_28183 != 0){
                    DeRef(_28183);
                    _28183 = NOVALUE;
                    goto LC; // [499] 509
                }
            }
            else {
                if (DBL_PTR(_28183)->dbl != 0.0){
                    DeRef(_28183);
                    _28183 = NOVALUE;
                    goto LC; // [499] 509
                }
            }
            DeRef(_28183);
            _28183 = NOVALUE;

            /** 						return 0*/
            DeRef(_i_54217);
            DeRef(_28140);
            _28140 = NOVALUE;
            DeRef(_28126);
            _28126 = NOVALUE;
            DeRef(_28158);
            _28158 = NOVALUE;
            DeRef(_28143);
            _28143 = NOVALUE;
            DeRef(_28146);
            _28146 = NOVALUE;
            DeRef(_28150);
            _28150 = NOVALUE;
            DeRef(_28152);
            _28152 = NOVALUE;
            DeRef(_28167);
            _28167 = NOVALUE;
            DeRef(_28168);
            _28168 = NOVALUE;
            DeRef(_28171);
            _28171 = NOVALUE;
            DeRef(_28178);
            _28178 = NOVALUE;
            DeRef(_28174);
            _28174 = NOVALUE;
            DeRef(_28180);
            _28180 = NOVALUE;
            DeRef(_28182);
            _28182 = NOVALUE;
            return 0;
LC: 

            /** 				end for*/
            _0 = _i_54217;
            if (IS_ATOM_INT(_i_54217)) {
                _i_54217 = _i_54217 + 1;
                if ((long)((unsigned long)_i_54217 +(unsigned long) HIGH_BITS) >= 0){
                    _i_54217 = NewDouble((double)_i_54217);
                }
            }
            else {
                _i_54217 = binary_op_a(PLUS, _i_54217, 1);
            }
            DeRef(_0);
            goto LA; // [511] 493
LB: 
            ;
            DeRef(_i_54217);
        }

        /** 				return 1*/
        DeRef(_28140);
        _28140 = NOVALUE;
        DeRef(_28126);
        _28126 = NOVALUE;
        DeRef(_28158);
        _28158 = NOVALUE;
        DeRef(_28143);
        _28143 = NOVALUE;
        DeRef(_28146);
        _28146 = NOVALUE;
        DeRef(_28150);
        _28150 = NOVALUE;
        DeRef(_28152);
        _28152 = NOVALUE;
        DeRef(_28167);
        _28167 = NOVALUE;
        DeRef(_28168);
        _28168 = NOVALUE;
        DeRef(_28171);
        _28171 = NOVALUE;
        DeRef(_28178);
        _28178 = NOVALUE;
        DeRef(_28174);
        _28174 = NOVALUE;
        DeRef(_28180);
        _28180 = NOVALUE;
        DeRef(_28182);
        _28182 = NOVALUE;
        return 1;

        /** 			case else*/
        default:

        /** 				return 0*/
        DeRef(_28140);
        _28140 = NOVALUE;
        DeRef(_28126);
        _28126 = NOVALUE;
        DeRef(_28158);
        _28158 = NOVALUE;
        DeRef(_28143);
        _28143 = NOVALUE;
        DeRef(_28146);
        _28146 = NOVALUE;
        DeRef(_28150);
        _28150 = NOVALUE;
        DeRef(_28152);
        _28152 = NOVALUE;
        DeRef(_28167);
        _28167 = NOVALUE;
        DeRef(_28168);
        _28168 = NOVALUE;
        DeRef(_28171);
        _28171 = NOVALUE;
        DeRef(_28178);
        _28178 = NOVALUE;
        DeRef(_28174);
        _28174 = NOVALUE;
        DeRef(_28180);
        _28180 = NOVALUE;
        DeRef(_28182);
        _28182 = NOVALUE;
        return 0;
    ;}L5: 

    /** 	return 1*/
    DeRef(_28140);
    _28140 = NOVALUE;
    DeRef(_28126);
    _28126 = NOVALUE;
    DeRef(_28158);
    _28158 = NOVALUE;
    DeRef(_28143);
    _28143 = NOVALUE;
    DeRef(_28146);
    _28146 = NOVALUE;
    DeRef(_28150);
    _28150 = NOVALUE;
    DeRef(_28152);
    _28152 = NOVALUE;
    DeRef(_28167);
    _28167 = NOVALUE;
    DeRef(_28168);
    _28168 = NOVALUE;
    DeRef(_28171);
    _28171 = NOVALUE;
    DeRef(_28178);
    _28178 = NOVALUE;
    DeRef(_28174);
    _28174 = NOVALUE;
    DeRef(_28180);
    _28180 = NOVALUE;
    DeRef(_28182);
    _28182 = NOVALUE;
    return 1;
    ;
}


void _67restore_code()
{
    int _28185 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length( temp_code ) then*/
    if (IS_SEQUENCE(_67temp_code_54227)){
            _28185 = SEQ_PTR(_67temp_code_54227)->length;
    }
    else {
        _28185 = 1;
    }
    if (_28185 == 0)
    {
        _28185 = NOVALUE;
        goto L1; // [8] 21
    }
    else{
        _28185 = NOVALUE;
    }

    /** 		Code = temp_code*/
    RefDS(_67temp_code_54227);
    DeRef(_25Code_12355);
    _25Code_12355 = _67temp_code_54227;
L1: 

    /** end procedure*/
    return;
    ;
}


void _67check_inline(int _sub_54236)
{
    int _pc_54265 = NOVALUE;
    int _s_54267 = NOVALUE;
    int _backpatch_op_54305 = NOVALUE;
    int _op_54309 = NOVALUE;
    int _rtn_idx_54320 = NOVALUE;
    int _args_54325 = NOVALUE;
    int _args_54357 = NOVALUE;
    int _values_54386 = NOVALUE;
    int _28272 = NOVALUE;
    int _28271 = NOVALUE;
    int _28269 = NOVALUE;
    int _28266 = NOVALUE;
    int _28264 = NOVALUE;
    int _28263 = NOVALUE;
    int _28262 = NOVALUE;
    int _28260 = NOVALUE;
    int _28259 = NOVALUE;
    int _28258 = NOVALUE;
    int _28257 = NOVALUE;
    int _28256 = NOVALUE;
    int _28255 = NOVALUE;
    int _28254 = NOVALUE;
    int _28253 = NOVALUE;
    int _28252 = NOVALUE;
    int _28250 = NOVALUE;
    int _28249 = NOVALUE;
    int _28248 = NOVALUE;
    int _28247 = NOVALUE;
    int _28246 = NOVALUE;
    int _28244 = NOVALUE;
    int _28243 = NOVALUE;
    int _28242 = NOVALUE;
    int _28241 = NOVALUE;
    int _28240 = NOVALUE;
    int _28238 = NOVALUE;
    int _28237 = NOVALUE;
    int _28236 = NOVALUE;
    int _28235 = NOVALUE;
    int _28234 = NOVALUE;
    int _28233 = NOVALUE;
    int _28232 = NOVALUE;
    int _28231 = NOVALUE;
    int _28230 = NOVALUE;
    int _28229 = NOVALUE;
    int _28228 = NOVALUE;
    int _28227 = NOVALUE;
    int _28226 = NOVALUE;
    int _28225 = NOVALUE;
    int _28223 = NOVALUE;
    int _28220 = NOVALUE;
    int _28215 = NOVALUE;
    int _28213 = NOVALUE;
    int _28210 = NOVALUE;
    int _28209 = NOVALUE;
    int _28208 = NOVALUE;
    int _28207 = NOVALUE;
    int _28206 = NOVALUE;
    int _28205 = NOVALUE;
    int _28204 = NOVALUE;
    int _28203 = NOVALUE;
    int _28201 = NOVALUE;
    int _28199 = NOVALUE;
    int _28198 = NOVALUE;
    int _28196 = NOVALUE;
    int _28194 = NOVALUE;
    int _28192 = NOVALUE;
    int _28190 = NOVALUE;
    int _28189 = NOVALUE;
    int _28188 = NOVALUE;
    int _28187 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_sub_54236)) {
        _1 = (long)(DBL_PTR(_sub_54236)->dbl);
        if (UNIQUE(DBL_PTR(_sub_54236)) && (DBL_PTR(_sub_54236)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sub_54236);
        _sub_54236 = _1;
    }

    /** 	if OpTrace or SymTab[sub][S_TOKEN] = TYPE then*/
    if (_25OpTrace_12332 != 0) {
        goto L1; // [7] 34
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28187 = (int)*(((s1_ptr)_2)->base + _sub_54236);
    _2 = (int)SEQ_PTR(_28187);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _28188 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _28188 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _28187 = NOVALUE;
    if (IS_ATOM_INT(_28188)) {
        _28189 = (_28188 == 504);
    }
    else {
        _28189 = binary_op(EQUALS, _28188, 504);
    }
    _28188 = NOVALUE;
    if (_28189 == 0) {
        DeRef(_28189);
        _28189 = NOVALUE;
        goto L2; // [30] 40
    }
    else {
        if (!IS_ATOM_INT(_28189) && DBL_PTR(_28189)->dbl == 0.0){
            DeRef(_28189);
            _28189 = NOVALUE;
            goto L2; // [30] 40
        }
        DeRef(_28189);
        _28189 = NOVALUE;
    }
    DeRef(_28189);
    _28189 = NOVALUE;
L1: 

    /** 		return*/
    DeRefi(_backpatch_op_54305);
    return;
L2: 

    /** 	inline_sub      = sub*/
    _67inline_sub_53729 = _sub_54236;

    /** 	if get_fwdref_count() then*/
    _28190 = _29get_fwdref_count();
    if (_28190 == 0) {
        DeRef(_28190);
        _28190 = NOVALUE;
        goto L3; // [52] 65
    }
    else {
        if (!IS_ATOM_INT(_28190) && DBL_PTR(_28190)->dbl == 0.0){
            DeRef(_28190);
            _28190 = NOVALUE;
            goto L3; // [52] 65
        }
        DeRef(_28190);
        _28190 = NOVALUE;
    }
    DeRef(_28190);
    _28190 = NOVALUE;

    /** 		defer()*/
    _67defer();

    /** 		return*/
    DeRefi(_backpatch_op_54305);
    return;
L3: 

    /** 	temp_code = ""*/
    RefDS(_22682);
    DeRef(_67temp_code_54227);
    _67temp_code_54227 = _22682;

    /** 	if sub != CurrentSub then*/
    if (_sub_54236 == _25CurrentSub_12270)
    goto L4; // [76] 99

    /** 		Code = SymTab[sub][S_CODE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28192 = (int)*(((s1_ptr)_2)->base + _sub_54236);
    DeRef(_25Code_12355);
    _2 = (int)SEQ_PTR(_28192);
    if (!IS_ATOM_INT(_25S_CODE_11925)){
        _25Code_12355 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    }
    else{
        _25Code_12355 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
    }
    Ref(_25Code_12355);
    _28192 = NOVALUE;
    goto L5; // [96] 109
L4: 

    /** 		temp_code = Code*/
    RefDS(_25Code_12355);
    DeRef(_67temp_code_54227);
    _67temp_code_54227 = _25Code_12355;
L5: 

    /** 	if length(Code) > OpInline then*/
    if (IS_SEQUENCE(_25Code_12355)){
            _28194 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _28194 = 1;
    }
    if (_28194 <= _25OpInline_12340)
    goto L6; // [118] 128

    /** 		return*/
    DeRefi(_backpatch_op_54305);
    return;
L6: 

    /** 	inline_code     = Code*/
    RefDS(_25Code_12355);
    DeRef(_67inline_code_53715);
    _67inline_code_53715 = _25Code_12355;

    /** 	return_gotos    = 0*/
    _67return_gotos_53724 = 0;

    /** 	prev_pc         = 1*/
    _67prev_pc_53723 = 1;

    /** 	proc_vars       = {}*/
    RefDS(_22682);
    DeRefi(_67proc_vars_53716);
    _67proc_vars_53716 = _22682;

    /** 	inline_temps    = {}*/
    RefDS(_22682);
    DeRef(_67inline_temps_53717);
    _67inline_temps_53717 = _22682;

    /** 	inline_params   = {}*/
    RefDS(_22682);
    DeRefi(_67inline_params_53720);
    _67inline_params_53720 = _22682;

    /** 	assigned_params = {}*/
    RefDS(_22682);
    DeRef(_67assigned_params_53721);
    _67assigned_params_53721 = _22682;

    /** 	integer pc = 1*/
    _pc_54265 = 1;

    /** 	symtab_index s = SymTab[sub][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28196 = (int)*(((s1_ptr)_2)->base + _sub_54236);
    _2 = (int)SEQ_PTR(_28196);
    _s_54267 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_54267)){
        _s_54267 = (long)DBL_PTR(_s_54267)->dbl;
    }
    _28196 = NOVALUE;

    /** 	for p = 1 to SymTab[sub][S_NUM_ARGS] do*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28198 = (int)*(((s1_ptr)_2)->base + _sub_54236);
    _2 = (int)SEQ_PTR(_28198);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _28199 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _28199 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    _28198 = NOVALUE;
    {
        int _p_54273;
        _p_54273 = 1;
L7: 
        if (binary_op_a(GREATER, _p_54273, _28199)){
            goto L8; // [210] 248
        }

        /** 		inline_params &= s*/
        Append(&_67inline_params_53720, _67inline_params_53720, _s_54267);

        /** 		s = SymTab[s][S_NEXT]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _28201 = (int)*(((s1_ptr)_2)->base + _s_54267);
        _2 = (int)SEQ_PTR(_28201);
        _s_54267 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_s_54267)){
            _s_54267 = (long)DBL_PTR(_s_54267)->dbl;
        }
        _28201 = NOVALUE;

        /** 	end for*/
        _0 = _p_54273;
        if (IS_ATOM_INT(_p_54273)) {
            _p_54273 = _p_54273 + 1;
            if ((long)((unsigned long)_p_54273 +(unsigned long) HIGH_BITS) >= 0){
                _p_54273 = NewDouble((double)_p_54273);
            }
        }
        else {
            _p_54273 = binary_op_a(PLUS, _p_54273, 1);
        }
        DeRef(_0);
        goto L7; // [243] 217
L8: 
        ;
        DeRef(_p_54273);
    }

    /** 	while s != 0 and */
L9: 
    _28203 = (_s_54267 != 0);
    if (_28203 == 0) {
        goto LA; // [257] 335
    }
    _28205 = _52sym_scope(_s_54267);
    if (IS_ATOM_INT(_28205)) {
        _28206 = (_28205 <= 3);
    }
    else {
        _28206 = binary_op(LESSEQ, _28205, 3);
    }
    DeRef(_28205);
    _28205 = NOVALUE;
    if (IS_ATOM_INT(_28206)) {
        if (_28206 != 0) {
            DeRef(_28207);
            _28207 = 1;
            goto LB; // [271] 289
        }
    }
    else {
        if (DBL_PTR(_28206)->dbl != 0.0) {
            DeRef(_28207);
            _28207 = 1;
            goto LB; // [271] 289
        }
    }
    _28208 = _52sym_scope(_s_54267);
    if (IS_ATOM_INT(_28208)) {
        _28209 = (_28208 == 9);
    }
    else {
        _28209 = binary_op(EQUALS, _28208, 9);
    }
    DeRef(_28208);
    _28208 = NOVALUE;
    DeRef(_28207);
    if (IS_ATOM_INT(_28209))
    _28207 = (_28209 != 0);
    else
    _28207 = DBL_PTR(_28209)->dbl != 0.0;
LB: 
    if (_28207 == 0)
    {
        _28207 = NOVALUE;
        goto LA; // [290] 335
    }
    else{
        _28207 = NOVALUE;
    }

    /** 		if sym_scope( s ) != SC_UNDEFINED then*/
    _28210 = _52sym_scope(_s_54267);
    if (binary_op_a(EQUALS, _28210, 9)){
        DeRef(_28210);
        _28210 = NOVALUE;
        goto LC; // [301] 314
    }
    DeRef(_28210);
    _28210 = NOVALUE;

    /** 			proc_vars &= s*/
    Append(&_67proc_vars_53716, _67proc_vars_53716, _s_54267);
LC: 

    /** 		s = SymTab[s][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28213 = (int)*(((s1_ptr)_2)->base + _s_54267);
    _2 = (int)SEQ_PTR(_28213);
    _s_54267 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_54267)){
        _s_54267 = (long)DBL_PTR(_s_54267)->dbl;
    }
    _28213 = NOVALUE;

    /** 	end while*/
    goto L9; // [332] 253
LA: 

    /** 	sequence backpatch_op = {}*/
    RefDS(_22682);
    DeRefi(_backpatch_op_54305);
    _backpatch_op_54305 = _22682;

    /** 	while pc < length( inline_code ) do*/
LD: 
    if (IS_SEQUENCE(_67inline_code_53715)){
            _28215 = SEQ_PTR(_67inline_code_53715)->length;
    }
    else {
        _28215 = 1;
    }
    if (_pc_54265 >= _28215)
    goto LE; // [352] 869

    /** 		integer op = inline_code[pc]*/
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    _op_54309 = (int)*(((s1_ptr)_2)->base + _pc_54265);
    if (!IS_ATOM_INT(_op_54309))
    _op_54309 = (long)DBL_PTR(_op_54309)->dbl;

    /** 		switch op do*/
    _0 = _op_54309;
    switch ( _0 ){ 

        /** 			case PROC_FORWARD, FUNC_FORWARD then*/
        case 195:
        case 196:

        /** 				defer()*/
        _67defer();

        /** 				restore_code()*/
        _67restore_code();

        /** 				return*/
        DeRefi(_backpatch_op_54305);
        _28199 = NOVALUE;
        DeRef(_28203);
        _28203 = NOVALUE;
        DeRef(_28206);
        _28206 = NOVALUE;
        DeRef(_28209);
        _28209 = NOVALUE;
        return;
        goto LF; // [390] 851

        /** 			case PROC, FUNC then*/
        case 27:
        case 501:

        /** 				symtab_index rtn_idx = inline_code[pc+1]*/
        _28220 = _pc_54265 + 1;
        _2 = (int)SEQ_PTR(_67inline_code_53715);
        _rtn_idx_54320 = (int)*(((s1_ptr)_2)->base + _28220);
        if (!IS_ATOM_INT(_rtn_idx_54320)){
            _rtn_idx_54320 = (long)DBL_PTR(_rtn_idx_54320)->dbl;
        }

        /** 				if rtn_idx = sub then*/
        if (_rtn_idx_54320 != _sub_54236)
        goto L10; // [414] 428

        /** 					restore_code()*/
        _67restore_code();

        /** 					return*/
        DeRefi(_backpatch_op_54305);
        _28199 = NOVALUE;
        DeRef(_28203);
        _28203 = NOVALUE;
        _28220 = NOVALUE;
        DeRef(_28206);
        _28206 = NOVALUE;
        DeRef(_28209);
        _28209 = NOVALUE;
        return;
L10: 

        /** 				integer args = SymTab[rtn_idx][S_NUM_ARGS]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _28223 = (int)*(((s1_ptr)_2)->base + _rtn_idx_54320);
        _2 = (int)SEQ_PTR(_28223);
        if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
            _args_54325 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
        }
        else{
            _args_54325 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
        }
        if (!IS_ATOM_INT(_args_54325)){
            _args_54325 = (long)DBL_PTR(_args_54325)->dbl;
        }
        _28223 = NOVALUE;

        /** 				if SymTab[rtn_idx][S_TOKEN] != PROC and check_for_param( pc + args + 2 ) then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _28225 = (int)*(((s1_ptr)_2)->base + _rtn_idx_54320);
        _2 = (int)SEQ_PTR(_28225);
        if (!IS_ATOM_INT(_25S_TOKEN_11918)){
            _28226 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
        }
        else{
            _28226 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
        }
        _28225 = NOVALUE;
        if (IS_ATOM_INT(_28226)) {
            _28227 = (_28226 != 27);
        }
        else {
            _28227 = binary_op(NOTEQ, _28226, 27);
        }
        _28226 = NOVALUE;
        if (IS_ATOM_INT(_28227)) {
            if (_28227 == 0) {
                goto L11; // [464] 485
            }
        }
        else {
            if (DBL_PTR(_28227)->dbl == 0.0) {
                goto L11; // [464] 485
            }
        }
        _28229 = _pc_54265 + _args_54325;
        if ((long)((unsigned long)_28229 + (unsigned long)HIGH_BITS) >= 0) 
        _28229 = NewDouble((double)_28229);
        if (IS_ATOM_INT(_28229)) {
            _28230 = _28229 + 2;
            if ((long)((unsigned long)_28230 + (unsigned long)HIGH_BITS) >= 0) 
            _28230 = NewDouble((double)_28230);
        }
        else {
            _28230 = NewDouble(DBL_PTR(_28229)->dbl + (double)2);
        }
        DeRef(_28229);
        _28229 = NOVALUE;
        _28231 = _67check_for_param(_28230);
        _28230 = NOVALUE;
        if (_28231 == 0) {
            DeRef(_28231);
            _28231 = NOVALUE;
            goto L11; // [481] 485
        }
        else {
            if (!IS_ATOM_INT(_28231) && DBL_PTR(_28231)->dbl == 0.0){
                DeRef(_28231);
                _28231 = NOVALUE;
                goto L11; // [481] 485
            }
            DeRef(_28231);
            _28231 = NOVALUE;
        }
        DeRef(_28231);
        _28231 = NOVALUE;
L11: 

        /** 				for i = 2 to args + 1 + (SymTab[rtn_idx][S_TOKEN] != PROC) do*/
        _28232 = _args_54325 + 1;
        if (_28232 > MAXINT){
            _28232 = NewDouble((double)_28232);
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _28233 = (int)*(((s1_ptr)_2)->base + _rtn_idx_54320);
        _2 = (int)SEQ_PTR(_28233);
        if (!IS_ATOM_INT(_25S_TOKEN_11918)){
            _28234 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
        }
        else{
            _28234 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
        }
        _28233 = NOVALUE;
        if (IS_ATOM_INT(_28234)) {
            _28235 = (_28234 != 27);
        }
        else {
            _28235 = binary_op(NOTEQ, _28234, 27);
        }
        _28234 = NOVALUE;
        if (IS_ATOM_INT(_28232) && IS_ATOM_INT(_28235)) {
            _28236 = _28232 + _28235;
            if ((long)((unsigned long)_28236 + (unsigned long)HIGH_BITS) >= 0) 
            _28236 = NewDouble((double)_28236);
        }
        else {
            _28236 = binary_op(PLUS, _28232, _28235);
        }
        DeRef(_28232);
        _28232 = NOVALUE;
        DeRef(_28235);
        _28235 = NOVALUE;
        {
            int _i_54342;
            _i_54342 = 2;
L12: 
            if (binary_op_a(GREATER, _i_54342, _28236)){
                goto L13; // [513] 550
            }

            /** 					if not adjust_symbol( pc + i ) then */
            if (IS_ATOM_INT(_i_54342)) {
                _28237 = _pc_54265 + _i_54342;
                if ((long)((unsigned long)_28237 + (unsigned long)HIGH_BITS) >= 0) 
                _28237 = NewDouble((double)_28237);
            }
            else {
                _28237 = NewDouble((double)_pc_54265 + DBL_PTR(_i_54342)->dbl);
            }
            _28238 = _67adjust_symbol(_28237);
            _28237 = NOVALUE;
            if (IS_ATOM_INT(_28238)) {
                if (_28238 != 0){
                    DeRef(_28238);
                    _28238 = NOVALUE;
                    goto L14; // [530] 543
                }
            }
            else {
                if (DBL_PTR(_28238)->dbl != 0.0){
                    DeRef(_28238);
                    _28238 = NOVALUE;
                    goto L14; // [530] 543
                }
            }
            DeRef(_28238);
            _28238 = NOVALUE;

            /** 						defer()*/
            _67defer();

            /** 						return*/
            DeRef(_i_54342);
            DeRefi(_backpatch_op_54305);
            _28199 = NOVALUE;
            DeRef(_28203);
            _28203 = NOVALUE;
            DeRef(_28220);
            _28220 = NOVALUE;
            DeRef(_28206);
            _28206 = NOVALUE;
            DeRef(_28209);
            _28209 = NOVALUE;
            DeRef(_28227);
            _28227 = NOVALUE;
            DeRef(_28236);
            _28236 = NOVALUE;
            return;
L14: 

            /** 				end for*/
            _0 = _i_54342;
            if (IS_ATOM_INT(_i_54342)) {
                _i_54342 = _i_54342 + 1;
                if ((long)((unsigned long)_i_54342 +(unsigned long) HIGH_BITS) >= 0){
                    _i_54342 = NewDouble((double)_i_54342);
                }
            }
            else {
                _i_54342 = binary_op_a(PLUS, _i_54342, 1);
            }
            DeRef(_0);
            goto L12; // [545] 520
L13: 
            ;
            DeRef(_i_54342);
        }
        goto LF; // [552] 851

        /** 			case RIGHT_BRACE_N then*/
        case 31:

        /** 				sequence args = inline_code[pc+2..inline_code[pc+1] + pc + 1]*/
        _28240 = _pc_54265 + 2;
        if ((long)((unsigned long)_28240 + (unsigned long)HIGH_BITS) >= 0) 
        _28240 = NewDouble((double)_28240);
        _28241 = _pc_54265 + 1;
        _2 = (int)SEQ_PTR(_67inline_code_53715);
        _28242 = (int)*(((s1_ptr)_2)->base + _28241);
        if (IS_ATOM_INT(_28242)) {
            _28243 = _28242 + _pc_54265;
            if ((long)((unsigned long)_28243 + (unsigned long)HIGH_BITS) >= 0) 
            _28243 = NewDouble((double)_28243);
        }
        else {
            _28243 = binary_op(PLUS, _28242, _pc_54265);
        }
        _28242 = NOVALUE;
        if (IS_ATOM_INT(_28243)) {
            _28244 = _28243 + 1;
        }
        else
        _28244 = binary_op(PLUS, 1, _28243);
        DeRef(_28243);
        _28243 = NOVALUE;
        rhs_slice_target = (object_ptr)&_args_54357;
        RHS_Slice(_67inline_code_53715, _28240, _28244);

        /** 				for i = 1 to length(args) - 1 do*/
        if (IS_SEQUENCE(_args_54357)){
                _28246 = SEQ_PTR(_args_54357)->length;
        }
        else {
            _28246 = 1;
        }
        _28247 = _28246 - 1;
        _28246 = NOVALUE;
        {
            int _i_54365;
            _i_54365 = 1;
L15: 
            if (_i_54365 > _28247){
                goto L16; // [598] 644
            }

            /** 					if find( args[i], args, i + 1 ) then*/
            _2 = (int)SEQ_PTR(_args_54357);
            _28248 = (int)*(((s1_ptr)_2)->base + _i_54365);
            _28249 = _i_54365 + 1;
            _28250 = find_from(_28248, _args_54357, _28249);
            _28248 = NOVALUE;
            _28249 = NOVALUE;
            if (_28250 == 0)
            {
                _28250 = NOVALUE;
                goto L17; // [620] 637
            }
            else{
                _28250 = NOVALUE;
            }

            /** 						defer()*/
            _67defer();

            /** 						restore_code()*/
            _67restore_code();

            /** 						return*/
            DeRefDS(_args_54357);
            DeRefi(_backpatch_op_54305);
            _28199 = NOVALUE;
            DeRef(_28203);
            _28203 = NOVALUE;
            DeRef(_28220);
            _28220 = NOVALUE;
            DeRef(_28206);
            _28206 = NOVALUE;
            DeRef(_28209);
            _28209 = NOVALUE;
            DeRef(_28240);
            _28240 = NOVALUE;
            DeRef(_28227);
            _28227 = NOVALUE;
            DeRef(_28236);
            _28236 = NOVALUE;
            DeRef(_28241);
            _28241 = NOVALUE;
            DeRef(_28244);
            _28244 = NOVALUE;
            DeRef(_28247);
            _28247 = NOVALUE;
            return;
L17: 

            /** 				end for*/
            _i_54365 = _i_54365 + 1;
            goto L15; // [639] 605
L16: 
            ;
        }

        /** 				goto "inline op"*/
        DeRef(_args_54357);
        _args_54357 = NOVALUE;
        goto G18;
        goto LF; // [654] 851

        /** 			case RIGHT_BRACE_2 then*/
        case 85:

        /** 				if equal( inline_code[pc+1], inline_code[pc+2] ) then*/
        _28252 = _pc_54265 + 1;
        _2 = (int)SEQ_PTR(_67inline_code_53715);
        _28253 = (int)*(((s1_ptr)_2)->base + _28252);
        _28254 = _pc_54265 + 2;
        _2 = (int)SEQ_PTR(_67inline_code_53715);
        _28255 = (int)*(((s1_ptr)_2)->base + _28254);
        if (_28253 == _28255)
        _28256 = 1;
        else if (IS_ATOM_INT(_28253) && IS_ATOM_INT(_28255))
        _28256 = 0;
        else
        _28256 = (compare(_28253, _28255) == 0);
        _28253 = NOVALUE;
        _28255 = NOVALUE;
        if (_28256 == 0)
        {
            _28256 = NOVALUE;
            goto L19; // [686] 703
        }
        else{
            _28256 = NOVALUE;
        }

        /** 					defer()*/
        _67defer();

        /** 					restore_code()*/
        _67restore_code();

        /** 					return*/
        DeRefi(_backpatch_op_54305);
        _28199 = NOVALUE;
        DeRef(_28203);
        _28203 = NOVALUE;
        DeRef(_28220);
        _28220 = NOVALUE;
        DeRef(_28206);
        _28206 = NOVALUE;
        DeRef(_28209);
        _28209 = NOVALUE;
        DeRef(_28240);
        _28240 = NOVALUE;
        DeRef(_28227);
        _28227 = NOVALUE;
        DeRef(_28236);
        _28236 = NOVALUE;
        DeRef(_28241);
        _28241 = NOVALUE;
        DeRef(_28244);
        _28244 = NOVALUE;
        DeRef(_28247);
        _28247 = NOVALUE;
        _28252 = NOVALUE;
        _28254 = NOVALUE;
        return;
L19: 

        /** 				goto "inline op"*/
        goto G18;
        goto LF; // [711] 851

        /** 			case EXIT_BLOCK then*/
        case 206:

        /** 				replace_code( "", pc, pc + 1 )*/
        _28257 = _pc_54265 + 1;
        if (_28257 > MAXINT){
            _28257 = NewDouble((double)_28257);
        }
        RefDS(_22682);
        _67replace_code(_22682, _pc_54265, _28257);
        _28257 = NOVALUE;

        /** 				continue*/
        goto LD; // [732] 347
        goto LF; // [734] 851

        /** 			case SWITCH_RT then*/
        case 202:

        /** 				sequence values = SymTab[inline_code[pc+2]][S_OBJ]*/
        _28258 = _pc_54265 + 2;
        _2 = (int)SEQ_PTR(_67inline_code_53715);
        _28259 = (int)*(((s1_ptr)_2)->base + _28258);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!IS_ATOM_INT(_28259)){
            _28260 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_28259)->dbl));
        }
        else{
            _28260 = (int)*(((s1_ptr)_2)->base + _28259);
        }
        DeRef(_values_54386);
        _2 = (int)SEQ_PTR(_28260);
        _values_54386 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_values_54386);
        _28260 = NOVALUE;

        /** 				for i = 1 to length( values ) do*/
        if (IS_SEQUENCE(_values_54386)){
                _28262 = SEQ_PTR(_values_54386)->length;
        }
        else {
            _28262 = 1;
        }
        {
            int _i_54394;
            _i_54394 = 1;
L1A: 
            if (_i_54394 > _28262){
                goto L1B; // [771] 811
            }

            /** 					if sequence( values[i] ) then*/
            _2 = (int)SEQ_PTR(_values_54386);
            _28263 = (int)*(((s1_ptr)_2)->base + _i_54394);
            _28264 = IS_SEQUENCE(_28263);
            _28263 = NOVALUE;
            if (_28264 == 0)
            {
                _28264 = NOVALUE;
                goto L1C; // [787] 804
            }
            else{
                _28264 = NOVALUE;
            }

            /** 						defer()*/
            _67defer();

            /** 						restore_code()*/
            _67restore_code();

            /** 						return*/
            DeRefDS(_values_54386);
            DeRefi(_backpatch_op_54305);
            _28199 = NOVALUE;
            DeRef(_28203);
            _28203 = NOVALUE;
            DeRef(_28220);
            _28220 = NOVALUE;
            DeRef(_28206);
            _28206 = NOVALUE;
            DeRef(_28209);
            _28209 = NOVALUE;
            DeRef(_28240);
            _28240 = NOVALUE;
            DeRef(_28227);
            _28227 = NOVALUE;
            DeRef(_28236);
            _28236 = NOVALUE;
            DeRef(_28241);
            _28241 = NOVALUE;
            DeRef(_28244);
            _28244 = NOVALUE;
            DeRef(_28247);
            _28247 = NOVALUE;
            DeRef(_28252);
            _28252 = NOVALUE;
            DeRef(_28258);
            _28258 = NOVALUE;
            DeRef(_28254);
            _28254 = NOVALUE;
            _28259 = NOVALUE;
            return;
L1C: 

            /** 				end for*/
            _i_54394 = _i_54394 + 1;
            goto L1A; // [806] 778
L1B: 
            ;
        }

        /** 				backpatch_op = append( backpatch_op, pc )*/
        Append(&_backpatch_op_54305, _backpatch_op_54305, _pc_54265);
        DeRef(_values_54386);
        _values_54386 = NOVALUE;

        /** 			case else*/
        default:

        /** 			label "inline op"*/
G18:

        /** 				if not inline_op( pc ) then*/
        _28266 = _67inline_op(_pc_54265);
        if (IS_ATOM_INT(_28266)) {
            if (_28266 != 0){
                DeRef(_28266);
                _28266 = NOVALUE;
                goto L1D; // [833] 850
            }
        }
        else {
            if (DBL_PTR(_28266)->dbl != 0.0){
                DeRef(_28266);
                _28266 = NOVALUE;
                goto L1D; // [833] 850
            }
        }
        DeRef(_28266);
        _28266 = NOVALUE;

        /** 					defer()*/
        _67defer();

        /** 					restore_code()*/
        _67restore_code();

        /** 					return*/
        DeRefi(_backpatch_op_54305);
        _28199 = NOVALUE;
        DeRef(_28203);
        _28203 = NOVALUE;
        DeRef(_28220);
        _28220 = NOVALUE;
        DeRef(_28206);
        _28206 = NOVALUE;
        DeRef(_28209);
        _28209 = NOVALUE;
        DeRef(_28240);
        _28240 = NOVALUE;
        DeRef(_28227);
        _28227 = NOVALUE;
        DeRef(_28236);
        _28236 = NOVALUE;
        DeRef(_28241);
        _28241 = NOVALUE;
        DeRef(_28244);
        _28244 = NOVALUE;
        DeRef(_28247);
        _28247 = NOVALUE;
        DeRef(_28252);
        _28252 = NOVALUE;
        DeRef(_28258);
        _28258 = NOVALUE;
        DeRef(_28254);
        _28254 = NOVALUE;
        _28259 = NOVALUE;
        return;
L1D: 
    ;}LF: 

    /** 		pc = advance( pc, inline_code )*/
    RefDS(_67inline_code_53715);
    _pc_54265 = _67advance(_pc_54265, _67inline_code_53715);
    if (!IS_ATOM_INT(_pc_54265)) {
        _1 = (long)(DBL_PTR(_pc_54265)->dbl);
        if (UNIQUE(DBL_PTR(_pc_54265)) && (DBL_PTR(_pc_54265)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pc_54265);
        _pc_54265 = _1;
    }

    /** 	end while*/
    goto LD; // [866] 347
LE: 

    /** 	SymTab[sub][S_INLINE] = { sort( assigned_params ), inline_code, backpatch_op }*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sub_54236 + ((s1_ptr)_2)->base);
    RefDS(_67assigned_params_53721);
    _28271 = _22sort(_67assigned_params_53721, 1);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _28271;
    RefDS(_67inline_code_53715);
    *((int *)(_2+8)) = _67inline_code_53715;
    RefDS(_backpatch_op_54305);
    *((int *)(_2+12)) = _backpatch_op_54305;
    _28272 = MAKE_SEQ(_1);
    _28271 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 29);
    _1 = *(int *)_2;
    *(int *)_2 = _28272;
    if( _1 != _28272 ){
        DeRef(_1);
    }
    _28272 = NOVALUE;
    _28269 = NOVALUE;

    /** 	restore_code()*/
    _67restore_code();

    /** end procedure*/
    DeRefDSi(_backpatch_op_54305);
    _28199 = NOVALUE;
    DeRef(_28203);
    _28203 = NOVALUE;
    DeRef(_28220);
    _28220 = NOVALUE;
    DeRef(_28206);
    _28206 = NOVALUE;
    DeRef(_28209);
    _28209 = NOVALUE;
    DeRef(_28240);
    _28240 = NOVALUE;
    DeRef(_28227);
    _28227 = NOVALUE;
    DeRef(_28236);
    _28236 = NOVALUE;
    DeRef(_28241);
    _28241 = NOVALUE;
    DeRef(_28244);
    _28244 = NOVALUE;
    DeRef(_28247);
    _28247 = NOVALUE;
    DeRef(_28252);
    _28252 = NOVALUE;
    DeRef(_28258);
    _28258 = NOVALUE;
    DeRef(_28254);
    _28254 = NOVALUE;
    _28259 = NOVALUE;
    return;
    ;
}


void _67replace_temp(int _pc_54414)
{
    int _temp_num_54415 = NOVALUE;
    int _needed_54418 = NOVALUE;
    int _28285 = NOVALUE;
    int _28284 = NOVALUE;
    int _28283 = NOVALUE;
    int _28282 = NOVALUE;
    int _28280 = NOVALUE;
    int _28278 = NOVALUE;
    int _28275 = NOVALUE;
    int _28273 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer temp_num = inline_code[pc][2]*/
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    _28273 = (int)*(((s1_ptr)_2)->base + _pc_54414);
    _2 = (int)SEQ_PTR(_28273);
    _temp_num_54415 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_temp_num_54415)){
        _temp_num_54415 = (long)DBL_PTR(_temp_num_54415)->dbl;
    }
    _28273 = NOVALUE;

    /** 	integer needed = temp_num - length( inline_temps )*/
    if (IS_SEQUENCE(_67inline_temps_53717)){
            _28275 = SEQ_PTR(_67inline_temps_53717)->length;
    }
    else {
        _28275 = 1;
    }
    _needed_54418 = _temp_num_54415 - _28275;
    _28275 = NOVALUE;

    /** 	if needed > 0 then*/
    if (_needed_54418 <= 0)
    goto L1; // [30] 47

    /** 		inline_temps &= repeat( 0, needed )*/
    _28278 = Repeat(0, _needed_54418);
    Concat((object_ptr)&_67inline_temps_53717, _67inline_temps_53717, _28278);
    DeRefDS(_28278);
    _28278 = NOVALUE;
L1: 

    /** 	if not inline_temps[temp_num] then*/
    _2 = (int)SEQ_PTR(_67inline_temps_53717);
    _28280 = (int)*(((s1_ptr)_2)->base + _temp_num_54415);
    if (IS_ATOM_INT(_28280)) {
        if (_28280 != 0){
            _28280 = NOVALUE;
            goto L2; // [55] 100
        }
    }
    else {
        if (DBL_PTR(_28280)->dbl != 0.0){
            _28280 = NOVALUE;
            goto L2; // [55] 100
        }
    }
    _28280 = NOVALUE;

    /** 		if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L3; // [62] 84
    }
    else{
    }

    /** 			inline_temps[temp_num] = new_inline_var( -temp_num, 0 )*/
    if ((unsigned long)_temp_num_54415 == 0xC0000000)
    _28282 = (int)NewDouble((double)-0xC0000000);
    else
    _28282 = - _temp_num_54415;
    _28283 = _67new_inline_var(_28282, 0);
    _28282 = NOVALUE;
    _2 = (int)SEQ_PTR(_67inline_temps_53717);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _67inline_temps_53717 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _temp_num_54415);
    _1 = *(int *)_2;
    *(int *)_2 = _28283;
    if( _1 != _28283 ){
        DeRef(_1);
    }
    _28283 = NOVALUE;
    goto L4; // [81] 99
L3: 

    /** 			inline_temps[temp_num] = NewTempSym( TRUE )*/
    _28284 = _52NewTempSym(_5TRUE_244);
    _2 = (int)SEQ_PTR(_67inline_temps_53717);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _67inline_temps_53717 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _temp_num_54415);
    _1 = *(int *)_2;
    *(int *)_2 = _28284;
    if( _1 != _28284 ){
        DeRef(_1);
    }
    _28284 = NOVALUE;
L4: 
L2: 

    /** 	inline_code[pc] = inline_temps[temp_num]*/
    _2 = (int)SEQ_PTR(_67inline_temps_53717);
    _28285 = (int)*(((s1_ptr)_2)->base + _temp_num_54415);
    Ref(_28285);
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _67inline_code_53715 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _pc_54414);
    _1 = *(int *)_2;
    *(int *)_2 = _28285;
    if( _1 != _28285 ){
        DeRef(_1);
    }
    _28285 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


int _67get_param_sym(int _pc_54440)
{
    int _il_54441 = NOVALUE;
    int _px_54449 = NOVALUE;
    int _28292 = NOVALUE;
    int _28289 = NOVALUE;
    int _28288 = NOVALUE;
    int _28287 = NOVALUE;
    int _0, _1, _2;
    

    /** 	object il = inline_code[pc]*/
    DeRef(_il_54441);
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    _il_54441 = (int)*(((s1_ptr)_2)->base + _pc_54440);
    Ref(_il_54441);

    /** 	if integer( il ) then*/
    if (IS_ATOM_INT(_il_54441))
    _28287 = 1;
    else if (IS_ATOM_DBL(_il_54441))
    _28287 = IS_ATOM_INT(DoubleToInt(_il_54441));
    else
    _28287 = 0;
    if (_28287 == 0)
    {
        _28287 = NOVALUE;
        goto L1; // [16] 34
    }
    else{
        _28287 = NOVALUE;
    }

    /** 		return inline_code[pc]*/
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    _28288 = (int)*(((s1_ptr)_2)->base + _pc_54440);
    Ref(_28288);
    DeRef(_il_54441);
    return _28288;
    goto L2; // [31] 53
L1: 

    /** 	elsif length( il ) = 1 then*/
    if (IS_SEQUENCE(_il_54441)){
            _28289 = SEQ_PTR(_il_54441)->length;
    }
    else {
        _28289 = 1;
    }
    if (_28289 != 1)
    goto L3; // [39] 52

    /** 		return inline_target*/
    DeRef(_il_54441);
    _28288 = NOVALUE;
    return _67inline_target_53722;
L3: 
L2: 

    /** 	integer px = il[2]*/
    _2 = (int)SEQ_PTR(_il_54441);
    _px_54449 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_px_54449)){
        _px_54449 = (long)DBL_PTR(_px_54449)->dbl;
    }

    /** 	return passed_params[px]*/
    _2 = (int)SEQ_PTR(_67passed_params_53718);
    _28292 = (int)*(((s1_ptr)_2)->base + _px_54449);
    Ref(_28292);
    DeRef(_il_54441);
    _28288 = NOVALUE;
    return _28292;
    ;
}


int _67get_original_sym(int _pc_54454)
{
    int _il_54455 = NOVALUE;
    int _px_54463 = NOVALUE;
    int _28299 = NOVALUE;
    int _28296 = NOVALUE;
    int _28295 = NOVALUE;
    int _28294 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_pc_54454)) {
        _1 = (long)(DBL_PTR(_pc_54454)->dbl);
        if (UNIQUE(DBL_PTR(_pc_54454)) && (DBL_PTR(_pc_54454)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pc_54454);
        _pc_54454 = _1;
    }

    /** 	object il = inline_code[pc]*/
    DeRef(_il_54455);
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    _il_54455 = (int)*(((s1_ptr)_2)->base + _pc_54454);
    Ref(_il_54455);

    /** 	if integer( il ) then*/
    if (IS_ATOM_INT(_il_54455))
    _28294 = 1;
    else if (IS_ATOM_DBL(_il_54455))
    _28294 = IS_ATOM_INT(DoubleToInt(_il_54455));
    else
    _28294 = 0;
    if (_28294 == 0)
    {
        _28294 = NOVALUE;
        goto L1; // [16] 34
    }
    else{
        _28294 = NOVALUE;
    }

    /** 		return inline_code[pc]*/
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    _28295 = (int)*(((s1_ptr)_2)->base + _pc_54454);
    Ref(_28295);
    DeRef(_il_54455);
    return _28295;
    goto L2; // [31] 53
L1: 

    /** 	elsif length( il ) = 1 then*/
    if (IS_SEQUENCE(_il_54455)){
            _28296 = SEQ_PTR(_il_54455)->length;
    }
    else {
        _28296 = 1;
    }
    if (_28296 != 1)
    goto L3; // [39] 52

    /** 		return inline_target*/
    DeRef(_il_54455);
    _28295 = NOVALUE;
    return _67inline_target_53722;
L3: 
L2: 

    /** 	integer px = il[2]*/
    _2 = (int)SEQ_PTR(_il_54455);
    _px_54463 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_px_54463)){
        _px_54463 = (long)DBL_PTR(_px_54463)->dbl;
    }

    /** 	return original_params[px]*/
    _2 = (int)SEQ_PTR(_67original_params_53719);
    _28299 = (int)*(((s1_ptr)_2)->base + _px_54463);
    Ref(_28299);
    DeRef(_il_54455);
    _28295 = NOVALUE;
    return _28299;
    ;
}


void _67replace_var(int _pc_54472)
{
    int _28303 = NOVALUE;
    int _28302 = NOVALUE;
    int _28301 = NOVALUE;
    int _0, _1, _2;
    

    /** 	inline_code[pc] = proc_vars[inline_code[pc][2]]*/
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    _28301 = (int)*(((s1_ptr)_2)->base + _pc_54472);
    _2 = (int)SEQ_PTR(_28301);
    _28302 = (int)*(((s1_ptr)_2)->base + 2);
    _28301 = NOVALUE;
    _2 = (int)SEQ_PTR(_67proc_vars_53716);
    if (!IS_ATOM_INT(_28302)){
        _28303 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_28302)->dbl));
    }
    else{
        _28303 = (int)*(((s1_ptr)_2)->base + _28302);
    }
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _67inline_code_53715 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _pc_54472);
    _1 = *(int *)_2;
    *(int *)_2 = _28303;
    if( _1 != _28303 ){
        DeRef(_1);
    }
    _28303 = NOVALUE;

    /** end procedure*/
    _28302 = NOVALUE;
    return;
    ;
}


void _67fix_switch_rt(int _pc_54478)
{
    int _value_table_54480 = NOVALUE;
    int _jump_table_54487 = NOVALUE;
    int _28323 = NOVALUE;
    int _28322 = NOVALUE;
    int _28321 = NOVALUE;
    int _28320 = NOVALUE;
    int _28319 = NOVALUE;
    int _28318 = NOVALUE;
    int _28316 = NOVALUE;
    int _28315 = NOVALUE;
    int _28314 = NOVALUE;
    int _28313 = NOVALUE;
    int _28312 = NOVALUE;
    int _28310 = NOVALUE;
    int _28308 = NOVALUE;
    int _28307 = NOVALUE;
    int _28305 = NOVALUE;
    int _28304 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	symtab_index value_table = NewStringSym( {-1, length(SymTab)} )*/
    if (IS_SEQUENCE(_26SymTab_11138)){
            _28304 = SEQ_PTR(_26SymTab_11138)->length;
    }
    else {
        _28304 = 1;
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = _28304;
    _28305 = MAKE_SEQ(_1);
    _28304 = NOVALUE;
    _value_table_54480 = _52NewStringSym(_28305);
    _28305 = NOVALUE;
    if (!IS_ATOM_INT(_value_table_54480)) {
        _1 = (long)(DBL_PTR(_value_table_54480)->dbl);
        if (UNIQUE(DBL_PTR(_value_table_54480)) && (DBL_PTR(_value_table_54480)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_value_table_54480);
        _value_table_54480 = _1;
    }

    /** 	symtab_index jump_table  = NewStringSym( {-1, length(SymTab)} )*/
    if (IS_SEQUENCE(_26SymTab_11138)){
            _28307 = SEQ_PTR(_26SymTab_11138)->length;
    }
    else {
        _28307 = 1;
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = _28307;
    _28308 = MAKE_SEQ(_1);
    _28307 = NOVALUE;
    _jump_table_54487 = _52NewStringSym(_28308);
    _28308 = NOVALUE;
    if (!IS_ATOM_INT(_jump_table_54487)) {
        _1 = (long)(DBL_PTR(_jump_table_54487)->dbl);
        if (UNIQUE(DBL_PTR(_jump_table_54487)) && (DBL_PTR(_jump_table_54487)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_jump_table_54487);
        _jump_table_54487 = _1;
    }

    /** 	SymTab[value_table][S_OBJ] = SymTab[inline_code[pc+2]][S_OBJ]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_value_table_54480 + ((s1_ptr)_2)->base);
    _28312 = _pc_54478 + 2;
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    _28313 = (int)*(((s1_ptr)_2)->base + _28312);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_28313)){
        _28314 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_28313)->dbl));
    }
    else{
        _28314 = (int)*(((s1_ptr)_2)->base + _28313);
    }
    _2 = (int)SEQ_PTR(_28314);
    _28315 = (int)*(((s1_ptr)_2)->base + 1);
    _28314 = NOVALUE;
    Ref(_28315);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _28315;
    if( _1 != _28315 ){
        DeRef(_1);
    }
    _28315 = NOVALUE;
    _28310 = NOVALUE;

    /** 	SymTab[jump_table][S_OBJ]  = SymTab[inline_code[pc+3]][S_OBJ]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_jump_table_54487 + ((s1_ptr)_2)->base);
    _28318 = _pc_54478 + 3;
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    _28319 = (int)*(((s1_ptr)_2)->base + _28318);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_28319)){
        _28320 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_28319)->dbl));
    }
    else{
        _28320 = (int)*(((s1_ptr)_2)->base + _28319);
    }
    _2 = (int)SEQ_PTR(_28320);
    _28321 = (int)*(((s1_ptr)_2)->base + 1);
    _28320 = NOVALUE;
    Ref(_28321);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _28321;
    if( _1 != _28321 ){
        DeRef(_1);
    }
    _28321 = NOVALUE;
    _28316 = NOVALUE;

    /** 	inline_code[pc+2] = value_table*/
    _28322 = _pc_54478 + 2;
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _67inline_code_53715 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _28322);
    _1 = *(int *)_2;
    *(int *)_2 = _value_table_54480;
    DeRef(_1);

    /** 	inline_code[pc+3] = jump_table*/
    _28323 = _pc_54478 + 3;
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _67inline_code_53715 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _28323);
    _1 = *(int *)_2;
    *(int *)_2 = _jump_table_54487;
    DeRef(_1);

    /** end procedure*/
    _28322 = NOVALUE;
    _28312 = NOVALUE;
    _28313 = NOVALUE;
    _28318 = NOVALUE;
    _28319 = NOVALUE;
    _28323 = NOVALUE;
    return;
    ;
}


void _67fixup_special_op(int _pc_54517)
{
    int _op_54518 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_pc_54517)) {
        _1 = (long)(DBL_PTR(_pc_54517)->dbl);
        if (UNIQUE(DBL_PTR(_pc_54517)) && (DBL_PTR(_pc_54517)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pc_54517);
        _pc_54517 = _1;
    }

    /** 	integer op = inline_code[pc]*/
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    _op_54518 = (int)*(((s1_ptr)_2)->base + _pc_54517);
    if (!IS_ATOM_INT(_op_54518))
    _op_54518 = (long)DBL_PTR(_op_54518)->dbl;

    /** 	switch op with fallthru do*/
    _0 = _op_54518;
    switch ( _0 ){ 

        /** 		case SWITCH_RT then*/
        case 202:

        /** 			fix_switch_rt( pc )*/
        _67fix_switch_rt(_pc_54517);

        /** 			break*/
        goto L1; // [29] 32
    ;}L1: 

    /** end procedure*/
    return;
    ;
}


int _67new_inline_var(int _ps_54529, int _reuse_54530)
{
    int _var_54532 = NOVALUE;
    int _vtype_54533 = NOVALUE;
    int _name_54534 = NOVALUE;
    int _s_54536 = NOVALUE;
    int _28386 = NOVALUE;
    int _28385 = NOVALUE;
    int _28383 = NOVALUE;
    int _28380 = NOVALUE;
    int _28379 = NOVALUE;
    int _28377 = NOVALUE;
    int _28374 = NOVALUE;
    int _28373 = NOVALUE;
    int _28372 = NOVALUE;
    int _28370 = NOVALUE;
    int _28365 = NOVALUE;
    int _28360 = NOVALUE;
    int _28359 = NOVALUE;
    int _28358 = NOVALUE;
    int _28357 = NOVALUE;
    int _28354 = NOVALUE;
    int _28352 = NOVALUE;
    int _28349 = NOVALUE;
    int _28344 = NOVALUE;
    int _28343 = NOVALUE;
    int _28342 = NOVALUE;
    int _28341 = NOVALUE;
    int _28340 = NOVALUE;
    int _28337 = NOVALUE;
    int _28336 = NOVALUE;
    int _28335 = NOVALUE;
    int _28334 = NOVALUE;
    int _28333 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_ps_54529)) {
        _1 = (long)(DBL_PTR(_ps_54529)->dbl);
        if (UNIQUE(DBL_PTR(_ps_54529)) && (DBL_PTR(_ps_54529)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ps_54529);
        _ps_54529 = _1;
    }

    /** 		var = 0, */
    _var_54532 = 0;

    /** 	sequence name*/

    /** 	if reuse then*/

    /** 	if not var then*/

    /** 		if ps > 0 then*/
    if (_ps_54529 <= 0)
    goto L1; // [45] 222

    /** 			s = ps*/
    _s_54536 = _ps_54529;

    /** 			if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L2; // [60] 102
    }
    else{
    }

    /** 				name = sprintf( "%s_inlined_%s", {SymTab[s][S_NAME], SymTab[inline_sub][S_NAME] })*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28333 = (int)*(((s1_ptr)_2)->base + _s_54536);
    _2 = (int)SEQ_PTR(_28333);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _28334 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _28334 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _28333 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28335 = (int)*(((s1_ptr)_2)->base + _67inline_sub_53729);
    _2 = (int)SEQ_PTR(_28335);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _28336 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _28336 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _28335 = NOVALUE;
    Ref(_28336);
    Ref(_28334);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _28334;
    ((int *)_2)[2] = _28336;
    _28337 = MAKE_SEQ(_1);
    _28336 = NOVALUE;
    _28334 = NOVALUE;
    DeRefi(_name_54534);
    _name_54534 = EPrintf(-9999999, _28332, _28337);
    DeRefDS(_28337);
    _28337 = NOVALUE;
    goto L3; // [99] 139
L2: 

    /** 				name = sprintf( "%s (from inlined routine '%s'", {SymTab[s][S_NAME], SymTab[inline_sub][S_NAME] })*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28340 = (int)*(((s1_ptr)_2)->base + _s_54536);
    _2 = (int)SEQ_PTR(_28340);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _28341 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _28341 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _28340 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28342 = (int)*(((s1_ptr)_2)->base + _67inline_sub_53729);
    _2 = (int)SEQ_PTR(_28342);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _28343 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _28343 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _28342 = NOVALUE;
    Ref(_28343);
    Ref(_28341);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _28341;
    ((int *)_2)[2] = _28343;
    _28344 = MAKE_SEQ(_1);
    _28343 = NOVALUE;
    _28341 = NOVALUE;
    DeRefi(_name_54534);
    _name_54534 = EPrintf(-9999999, _28339, _28344);
    DeRefDS(_28344);
    _28344 = NOVALUE;
L3: 

    /** 			if reuse then*/
    if (_reuse_54530 == 0)
    {
        goto L4; // [141] 163
    }
    else{
    }

    /** 				if not TRANSLATE then*/
    if (_25TRANSLATE_11874 != 0)
    goto L5; // [148] 203

    /** 					name &= ")"*/
    Concat((object_ptr)&_name_54534, _name_54534, _27118);
    goto L5; // [160] 203
L4: 

    /** 				if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L6; // [167] 187
    }
    else{
    }

    /** 					name &= sprintf( "_at_%d", inline_start)*/
    _28349 = EPrintf(-9999999, _28348, _67inline_start_53727);
    Concat((object_ptr)&_name_54534, _name_54534, _28349);
    DeRefDS(_28349);
    _28349 = NOVALUE;
    goto L7; // [184] 202
L6: 

    /** 					name &= sprintf( " at %d)", inline_start)*/
    _28352 = EPrintf(-9999999, _28351, _67inline_start_53727);
    Concat((object_ptr)&_name_54534, _name_54534, _28352);
    DeRefDS(_28352);
    _28352 = NOVALUE;
L7: 
L5: 

    /** 			vtype = SymTab[s][S_VTYPE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28354 = (int)*(((s1_ptr)_2)->base + _s_54536);
    _2 = (int)SEQ_PTR(_28354);
    _vtype_54533 = (int)*(((s1_ptr)_2)->base + 15);
    if (!IS_ATOM_INT(_vtype_54533)){
        _vtype_54533 = (long)DBL_PTR(_vtype_54533)->dbl;
    }
    _28354 = NOVALUE;
    goto L8; // [219] 286
L1: 

    /** 			name = sprintf( "%s_%d", {SymTab[inline_sub][S_NAME], -ps})*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28357 = (int)*(((s1_ptr)_2)->base + _67inline_sub_53729);
    _2 = (int)SEQ_PTR(_28357);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _28358 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _28358 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _28357 = NOVALUE;
    if ((unsigned long)_ps_54529 == 0xC0000000)
    _28359 = (int)NewDouble((double)-0xC0000000);
    else
    _28359 = - _ps_54529;
    Ref(_28358);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _28358;
    ((int *)_2)[2] = _28359;
    _28360 = MAKE_SEQ(_1);
    _28359 = NOVALUE;
    _28358 = NOVALUE;
    DeRefi(_name_54534);
    _name_54534 = EPrintf(-9999999, _28356, _28360);
    DeRefDS(_28360);
    _28360 = NOVALUE;

    /** 			if reuse then*/
    if (_reuse_54530 == 0)
    {
        goto L9; // [251] 263
    }
    else{
    }

    /** 				name &= "__tmp"*/
    Concat((object_ptr)&_name_54534, _name_54534, _28362);
    goto LA; // [260] 276
L9: 

    /** 				name &= sprintf( "__tmp_at%d", inline_start)*/
    _28365 = EPrintf(-9999999, _28364, _67inline_start_53727);
    Concat((object_ptr)&_name_54534, _name_54534, _28365);
    DeRefDS(_28365);
    _28365 = NOVALUE;
LA: 

    /** 			vtype = object_type*/
    _vtype_54533 = _52object_type_47099;
L8: 

    /** 		if CurrentSub = TopLevelSub then*/
    if (_25CurrentSub_12270 != _25TopLevelSub_12269)
    goto LB; // [292] 325

    /** 			var = NewEntry( name, varnum, SC_LOCAL, VARIABLE, INLINE_HASHVAL, 0, vtype )*/
    RefDS(_name_54534);
    _var_54532 = _52NewEntry(_name_54534, _67varnum_53726, 5, -100, 2004, 0, _vtype_54533);
    if (!IS_ATOM_INT(_var_54532)) {
        _1 = (long)(DBL_PTR(_var_54532)->dbl);
        if (UNIQUE(DBL_PTR(_var_54532)) && (DBL_PTR(_var_54532)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_var_54532);
        _var_54532 = _1;
    }
    goto LC; // [322] 416
LB: 

    /** 			var = NewBasicEntry( name, varnum, SC_PRIVATE, VARIABLE, INLINE_HASHVAL, 0, vtype )*/
    RefDS(_name_54534);
    _var_54532 = _52NewBasicEntry(_name_54534, _67varnum_53726, 3, -100, 2004, 0, _vtype_54533);
    if (!IS_ATOM_INT(_var_54532)) {
        _1 = (long)(DBL_PTR(_var_54532)->dbl);
        if (UNIQUE(DBL_PTR(_var_54532)) && (DBL_PTR(_var_54532)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_var_54532);
        _var_54532 = _1;
    }

    /** 			SymTab[var][S_NEXT] = SymTab[last_param][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_var_54532 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28372 = (int)*(((s1_ptr)_2)->base + _67last_param_53730);
    _2 = (int)SEQ_PTR(_28372);
    _28373 = (int)*(((s1_ptr)_2)->base + 2);
    _28372 = NOVALUE;
    Ref(_28373);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _28373;
    if( _1 != _28373 ){
        DeRef(_1);
    }
    _28373 = NOVALUE;
    _28370 = NOVALUE;

    /** 			SymTab[last_param][S_NEXT] = var*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_67last_param_53730 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _var_54532;
    DeRef(_1);
    _28374 = NOVALUE;

    /** 			if last_param = last_sym then*/
    if (_67last_param_53730 != _52last_sym_47110)
    goto LD; // [403] 415

    /** 				last_sym = var*/
    _52last_sym_47110 = _var_54532;
LD: 
LC: 

    /** 		if deferred_inlining then*/
    if (_67deferred_inlining_53725 == 0)
    {
        goto LE; // [420] 451
    }
    else{
    }

    /** 			SymTab[CurrentSub][S_STACK_SPACE] += 1*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25CurrentSub_12270 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!IS_ATOM_INT(_25S_STACK_SPACE_11973)){
        _28379 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_STACK_SPACE_11973)->dbl));
    }
    else{
        _28379 = (int)*(((s1_ptr)_2)->base + _25S_STACK_SPACE_11973);
    }
    _28377 = NOVALUE;
    if (IS_ATOM_INT(_28379)) {
        _28380 = _28379 + 1;
        if (_28380 > MAXINT){
            _28380 = NewDouble((double)_28380);
        }
    }
    else
    _28380 = binary_op(PLUS, 1, _28379);
    _28379 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_STACK_SPACE_11973))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_STACK_SPACE_11973)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_STACK_SPACE_11973);
    _1 = *(int *)_2;
    *(int *)_2 = _28380;
    if( _1 != _28380 ){
        DeRef(_1);
    }
    _28380 = NOVALUE;
    _28377 = NOVALUE;
    goto LF; // [448] 471
LE: 

    /** 			if param_num != -1 then*/
    if (_30param_num_55180 == -1)
    goto L10; // [455] 470

    /** 				param_num += 1*/
    _30param_num_55180 = _30param_num_55180 + 1;
L10: 
LF: 

    /** 		SymTab[var][S_USAGE] = U_USED*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_var_54532 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 3;
    DeRef(_1);
    _28383 = NOVALUE;

    /** 		if reuse then*/
    if (_reuse_54530 == 0)
    {
        goto L11; // [490] 515
    }
    else{
    }

    /** 			map:nested_put( inline_var_map, {CurrentSub, ps }, var )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _25CurrentSub_12270;
    ((int *)_2)[2] = _ps_54529;
    _28385 = MAKE_SEQ(_1);
    Ref(_67inline_var_map_53734);
    _32nested_put(_67inline_var_map_53734, _28385, _var_54532, 1, _32threshold_size_13318);
    _28385 = NOVALUE;
L11: 

    /** 	Block_var( var )*/
    _66Block_var(_var_54532);

    /** 	if BIND then*/
    if (_25BIND_11877 == 0)
    {
        goto L12; // [525] 540
    }
    else{
    }

    /** 		add_ref( {VARIABLE, var} )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -100;
    ((int *)_2)[2] = _var_54532;
    _28386 = MAKE_SEQ(_1);
    _52add_ref(_28386);
    _28386 = NOVALUE;
L12: 

    /** 	return var*/
    DeRefi(_name_54534);
    return _var_54532;
    ;
}


int _67get_inlined_code(int _sub_54666, int _start_54667, int _deferred_54668)
{
    int _is_proc_54669 = NOVALUE;
    int _backpatches_54687 = NOVALUE;
    int _prolog_54693 = NOVALUE;
    int _epilog_54694 = NOVALUE;
    int _s_54710 = NOVALUE;
    int _last_sym_54733 = NOVALUE;
    int _int_sym_54760 = NOVALUE;
    int _param_54768 = NOVALUE;
    int _ax_54771 = NOVALUE;
    int _var_54778 = NOVALUE;
    int _final_target_54793 = NOVALUE;
    int _var_54812 = NOVALUE;
    int _create_target_var_54825 = NOVALUE;
    int _check_pc_54848 = NOVALUE;
    int _op_54852 = NOVALUE;
    int _sym_54861 = NOVALUE;
    int _check_result_54866 = NOVALUE;
    int _inline_type_54943 = NOVALUE;
    int _replace_param_1__tmp_at1341_54954 = NOVALUE;
    int _28540 = NOVALUE;
    int _28539 = NOVALUE;
    int _28538 = NOVALUE;
    int _28537 = NOVALUE;
    int _28536 = NOVALUE;
    int _28535 = NOVALUE;
    int _28534 = NOVALUE;
    int _28533 = NOVALUE;
    int _28531 = NOVALUE;
    int _28528 = NOVALUE;
    int _28525 = NOVALUE;
    int _28524 = NOVALUE;
    int _28523 = NOVALUE;
    int _28522 = NOVALUE;
    int _28521 = NOVALUE;
    int _28520 = NOVALUE;
    int _28519 = NOVALUE;
    int _28518 = NOVALUE;
    int _28514 = NOVALUE;
    int _28513 = NOVALUE;
    int _28512 = NOVALUE;
    int _28511 = NOVALUE;
    int _28507 = NOVALUE;
    int _28506 = NOVALUE;
    int _28505 = NOVALUE;
    int _28504 = NOVALUE;
    int _28503 = NOVALUE;
    int _28502 = NOVALUE;
    int _28501 = NOVALUE;
    int _28499 = NOVALUE;
    int _28498 = NOVALUE;
    int _28497 = NOVALUE;
    int _28496 = NOVALUE;
    int _28495 = NOVALUE;
    int _28494 = NOVALUE;
    int _28493 = NOVALUE;
    int _28492 = NOVALUE;
    int _28491 = NOVALUE;
    int _28490 = NOVALUE;
    int _28489 = NOVALUE;
    int _28487 = NOVALUE;
    int _28486 = NOVALUE;
    int _28484 = NOVALUE;
    int _28483 = NOVALUE;
    int _28481 = NOVALUE;
    int _28480 = NOVALUE;
    int _28477 = NOVALUE;
    int _28476 = NOVALUE;
    int _28474 = NOVALUE;
    int _28472 = NOVALUE;
    int _28467 = NOVALUE;
    int _28463 = NOVALUE;
    int _28460 = NOVALUE;
    int _28456 = NOVALUE;
    int _28449 = NOVALUE;
    int _28448 = NOVALUE;
    int _28447 = NOVALUE;
    int _28446 = NOVALUE;
    int _28445 = NOVALUE;
    int _28444 = NOVALUE;
    int _28443 = NOVALUE;
    int _28441 = NOVALUE;
    int _28436 = NOVALUE;
    int _28433 = NOVALUE;
    int _28428 = NOVALUE;
    int _28427 = NOVALUE;
    int _28425 = NOVALUE;
    int _28424 = NOVALUE;
    int _28423 = NOVALUE;
    int _28420 = NOVALUE;
    int _28419 = NOVALUE;
    int _28418 = NOVALUE;
    int _28417 = NOVALUE;
    int _28416 = NOVALUE;
    int _28415 = NOVALUE;
    int _28414 = NOVALUE;
    int _28412 = NOVALUE;
    int _28411 = NOVALUE;
    int _28410 = NOVALUE;
    int _28408 = NOVALUE;
    int _28406 = NOVALUE;
    int _28405 = NOVALUE;
    int _28404 = NOVALUE;
    int _28403 = NOVALUE;
    int _28402 = NOVALUE;
    int _28401 = NOVALUE;
    int _28400 = NOVALUE;
    int _28397 = NOVALUE;
    int _28396 = NOVALUE;
    int _28394 = NOVALUE;
    int _28393 = NOVALUE;
    int _28391 = NOVALUE;
    int _28390 = NOVALUE;
    int _28388 = NOVALUE;
    int _28387 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_sub_54666)) {
        _1 = (long)(DBL_PTR(_sub_54666)->dbl);
        if (UNIQUE(DBL_PTR(_sub_54666)) && (DBL_PTR(_sub_54666)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sub_54666);
        _sub_54666 = _1;
    }
    if (!IS_ATOM_INT(_start_54667)) {
        _1 = (long)(DBL_PTR(_start_54667)->dbl);
        if (UNIQUE(DBL_PTR(_start_54667)) && (DBL_PTR(_start_54667)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_54667);
        _start_54667 = _1;
    }
    if (!IS_ATOM_INT(_deferred_54668)) {
        _1 = (long)(DBL_PTR(_deferred_54668)->dbl);
        if (UNIQUE(DBL_PTR(_deferred_54668)) && (DBL_PTR(_deferred_54668)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_deferred_54668);
        _deferred_54668 = _1;
    }

    /** 	integer is_proc = SymTab[sub][S_TOKEN] = PROC*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28387 = (int)*(((s1_ptr)_2)->base + _sub_54666);
    _2 = (int)SEQ_PTR(_28387);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _28388 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _28388 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _28387 = NOVALUE;
    if (IS_ATOM_INT(_28388)) {
        _is_proc_54669 = (_28388 == 27);
    }
    else {
        _is_proc_54669 = binary_op(EQUALS, _28388, 27);
    }
    _28388 = NOVALUE;
    if (!IS_ATOM_INT(_is_proc_54669)) {
        _1 = (long)(DBL_PTR(_is_proc_54669)->dbl);
        if (UNIQUE(DBL_PTR(_is_proc_54669)) && (DBL_PTR(_is_proc_54669)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_is_proc_54669);
        _is_proc_54669 = _1;
    }

    /** 	clear_inline_targets()*/
    _37clear_inline_targets();

    /** 	inline_temps = {}*/
    RefDS(_22682);
    DeRef(_67inline_temps_53717);
    _67inline_temps_53717 = _22682;

    /** 	inline_params = {}*/
    RefDS(_22682);
    DeRefi(_67inline_params_53720);
    _67inline_params_53720 = _22682;

    /** 	assigned_params      = SymTab[sub][S_INLINE][1]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28390 = (int)*(((s1_ptr)_2)->base + _sub_54666);
    _2 = (int)SEQ_PTR(_28390);
    _28391 = (int)*(((s1_ptr)_2)->base + 29);
    _28390 = NOVALUE;
    DeRef(_67assigned_params_53721);
    _2 = (int)SEQ_PTR(_28391);
    _67assigned_params_53721 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_67assigned_params_53721);
    _28391 = NOVALUE;

    /** 	inline_code          = SymTab[sub][S_INLINE][2]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28393 = (int)*(((s1_ptr)_2)->base + _sub_54666);
    _2 = (int)SEQ_PTR(_28393);
    _28394 = (int)*(((s1_ptr)_2)->base + 29);
    _28393 = NOVALUE;
    DeRef(_67inline_code_53715);
    _2 = (int)SEQ_PTR(_28394);
    _67inline_code_53715 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_67inline_code_53715);
    _28394 = NOVALUE;

    /** 	sequence backpatches = SymTab[sub][S_INLINE][3]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28396 = (int)*(((s1_ptr)_2)->base + _sub_54666);
    _2 = (int)SEQ_PTR(_28396);
    _28397 = (int)*(((s1_ptr)_2)->base + 29);
    _28396 = NOVALUE;
    DeRef(_backpatches_54687);
    _2 = (int)SEQ_PTR(_28397);
    _backpatches_54687 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_backpatches_54687);
    _28397 = NOVALUE;

    /** 	passed_params = {}*/
    RefDS(_22682);
    DeRef(_67passed_params_53718);
    _67passed_params_53718 = _22682;

    /** 	original_params = {}*/
    RefDS(_22682);
    DeRef(_67original_params_53719);
    _67original_params_53719 = _22682;

    /** 	proc_vars = {}*/
    RefDS(_22682);
    DeRefi(_67proc_vars_53716);
    _67proc_vars_53716 = _22682;

    /** 	sequence prolog = {}*/
    RefDS(_22682);
    DeRefi(_prolog_54693);
    _prolog_54693 = _22682;

    /** 	sequence epilog = {}*/
    RefDS(_22682);
    DeRef(_epilog_54694);
    _epilog_54694 = _22682;

    /** 	Start_block( EXIT_BLOCK, sprintf("Inline-%s from %s @ %d", */
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28400 = (int)*(((s1_ptr)_2)->base + _sub_54666);
    _2 = (int)SEQ_PTR(_28400);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _28401 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _28401 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _28400 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28402 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_28402);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _28403 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _28403 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _28402 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_28401);
    *((int *)(_2+4)) = _28401;
    Ref(_28403);
    *((int *)(_2+8)) = _28403;
    *((int *)(_2+12)) = _start_54667;
    _28404 = MAKE_SEQ(_1);
    _28403 = NOVALUE;
    _28401 = NOVALUE;
    _28405 = EPrintf(-9999999, _28399, _28404);
    DeRefDS(_28404);
    _28404 = NOVALUE;
    _66Start_block(206, _28405);
    _28405 = NOVALUE;

    /** 	symtab_index s = SymTab[sub][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28406 = (int)*(((s1_ptr)_2)->base + _sub_54666);
    _2 = (int)SEQ_PTR(_28406);
    _s_54710 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_54710)){
        _s_54710 = (long)DBL_PTR(_s_54710)->dbl;
    }
    _28406 = NOVALUE;

    /** 	varnum = SymTab[CurrentSub][S_NUM_ARGS]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28408 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_28408);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _67varnum_53726 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _67varnum_53726 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    if (!IS_ATOM_INT(_67varnum_53726)){
        _67varnum_53726 = (long)DBL_PTR(_67varnum_53726)->dbl;
    }
    _28408 = NOVALUE;

    /** 	inline_start = start*/
    _67inline_start_53727 = _start_54667;

    /** 	last_param = CurrentSub*/
    _67last_param_53730 = _25CurrentSub_12270;

    /** 	for p = 1 to SymTab[CurrentSub][S_NUM_ARGS] do*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28410 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_28410);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _28411 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _28411 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    _28410 = NOVALUE;
    {
        int _p_54722;
        _p_54722 = 1;
L1: 
        if (binary_op_a(GREATER, _p_54722, _28411)){
            goto L2; // [250] 282
        }

        /** 		last_param = SymTab[last_param][S_NEXT]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _28412 = (int)*(((s1_ptr)_2)->base + _67last_param_53730);
        _2 = (int)SEQ_PTR(_28412);
        _67last_param_53730 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_67last_param_53730)){
            _67last_param_53730 = (long)DBL_PTR(_67last_param_53730)->dbl;
        }
        _28412 = NOVALUE;

        /** 	end for*/
        _0 = _p_54722;
        if (IS_ATOM_INT(_p_54722)) {
            _p_54722 = _p_54722 + 1;
            if ((long)((unsigned long)_p_54722 +(unsigned long) HIGH_BITS) >= 0){
                _p_54722 = NewDouble((double)_p_54722);
            }
        }
        else {
            _p_54722 = binary_op_a(PLUS, _p_54722, 1);
        }
        DeRef(_0);
        goto L1; // [277] 257
L2: 
        ;
        DeRef(_p_54722);
    }

    /** 	symtab_index last_sym = last_param*/
    _last_sym_54733 = _67last_param_53730;

    /** 	while last_sym and */
L3: 
    if (_last_sym_54733 == 0) {
        goto L4; // [296] 368
    }
    _28415 = _52sym_scope(_last_sym_54733);
    if (IS_ATOM_INT(_28415)) {
        _28416 = (_28415 <= 3);
    }
    else {
        _28416 = binary_op(LESSEQ, _28415, 3);
    }
    DeRef(_28415);
    _28415 = NOVALUE;
    if (IS_ATOM_INT(_28416)) {
        if (_28416 != 0) {
            DeRef(_28417);
            _28417 = 1;
            goto L5; // [310] 328
        }
    }
    else {
        if (DBL_PTR(_28416)->dbl != 0.0) {
            DeRef(_28417);
            _28417 = 1;
            goto L5; // [310] 328
        }
    }
    _28418 = _52sym_scope(_last_sym_54733);
    if (IS_ATOM_INT(_28418)) {
        _28419 = (_28418 == 9);
    }
    else {
        _28419 = binary_op(EQUALS, _28418, 9);
    }
    DeRef(_28418);
    _28418 = NOVALUE;
    DeRef(_28417);
    if (IS_ATOM_INT(_28419))
    _28417 = (_28419 != 0);
    else
    _28417 = DBL_PTR(_28419)->dbl != 0.0;
L5: 
    if (_28417 == 0)
    {
        _28417 = NOVALUE;
        goto L4; // [329] 368
    }
    else{
        _28417 = NOVALUE;
    }

    /** 		last_param = last_sym*/
    _67last_param_53730 = _last_sym_54733;

    /** 		last_sym = SymTab[last_sym][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28420 = (int)*(((s1_ptr)_2)->base + _last_sym_54733);
    _2 = (int)SEQ_PTR(_28420);
    _last_sym_54733 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_last_sym_54733)){
        _last_sym_54733 = (long)DBL_PTR(_last_sym_54733)->dbl;
    }
    _28420 = NOVALUE;

    /** 		varnum += 1*/
    _67varnum_53726 = _67varnum_53726 + 1;

    /** 	end while*/
    goto L3; // [365] 296
L4: 

    /** 	for p = SymTab[sub][S_NUM_ARGS] to 1 by -1 do*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28423 = (int)*(((s1_ptr)_2)->base + _sub_54666);
    _2 = (int)SEQ_PTR(_28423);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _28424 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _28424 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    _28423 = NOVALUE;
    {
        int _p_54751;
        Ref(_28424);
        _p_54751 = _28424;
L6: 
        if (binary_op_a(LESS, _p_54751, 1)){
            goto L7; // [382] 407
        }

        /** 		passed_params = prepend( passed_params, Pop() )*/
        _28425 = _37Pop();
        Ref(_28425);
        Prepend(&_67passed_params_53718, _67passed_params_53718, _28425);
        DeRef(_28425);
        _28425 = NOVALUE;

        /** 	end for*/
        _0 = _p_54751;
        if (IS_ATOM_INT(_p_54751)) {
            _p_54751 = _p_54751 + -1;
            if ((long)((unsigned long)_p_54751 +(unsigned long) HIGH_BITS) >= 0){
                _p_54751 = NewDouble((double)_p_54751);
            }
        }
        else {
            _p_54751 = binary_op_a(PLUS, _p_54751, -1);
        }
        DeRef(_0);
        goto L6; // [402] 389
L7: 
        ;
        DeRef(_p_54751);
    }

    /** 	original_params = passed_params*/
    RefDS(_67passed_params_53718);
    DeRef(_67original_params_53719);
    _67original_params_53719 = _67passed_params_53718;

    /** 	symtab_index int_sym = 0*/
    _int_sym_54760 = 0;

    /** 	for p = 1 to SymTab[sub][S_NUM_ARGS] do*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28427 = (int)*(((s1_ptr)_2)->base + _sub_54666);
    _2 = (int)SEQ_PTR(_28427);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _28428 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _28428 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    _28427 = NOVALUE;
    {
        int _p_54762;
        _p_54762 = 1;
L8: 
        if (binary_op_a(GREATER, _p_54762, _28428)){
            goto L9; // [437] 575
        }

        /** 		symtab_index param = passed_params[p]*/
        _2 = (int)SEQ_PTR(_67passed_params_53718);
        if (!IS_ATOM_INT(_p_54762)){
            _param_54768 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_p_54762)->dbl));
        }
        else{
            _param_54768 = (int)*(((s1_ptr)_2)->base + _p_54762);
        }
        if (!IS_ATOM_INT(_param_54768)){
            _param_54768 = (long)DBL_PTR(_param_54768)->dbl;
        }

        /** 		inline_params &= s*/
        Append(&_67inline_params_53720, _67inline_params_53720, _s_54710);

        /** 		integer ax = find( p, assigned_params )*/
        _ax_54771 = find_from(_p_54762, _67assigned_params_53721, 1);

        /** 		if ax or is_temp( param ) then*/
        if (_ax_54771 != 0) {
            goto LA; // [473] 486
        }
        _28433 = _67is_temp(_param_54768);
        if (_28433 == 0) {
            DeRef(_28433);
            _28433 = NOVALUE;
            goto LB; // [482] 548
        }
        else {
            if (!IS_ATOM_INT(_28433) && DBL_PTR(_28433)->dbl == 0.0){
                DeRef(_28433);
                _28433 = NOVALUE;
                goto LB; // [482] 548
            }
            DeRef(_28433);
            _28433 = NOVALUE;
        }
        DeRef(_28433);
        _28433 = NOVALUE;
LA: 

        /** 			varnum += 1*/
        _67varnum_53726 = _67varnum_53726 + 1;

        /** 			symtab_index var = new_inline_var( s, 0 )*/
        _var_54778 = _67new_inline_var(_s_54710, 0);
        if (!IS_ATOM_INT(_var_54778)) {
            _1 = (long)(DBL_PTR(_var_54778)->dbl);
            if (UNIQUE(DBL_PTR(_var_54778)) && (DBL_PTR(_var_54778)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_var_54778);
            _var_54778 = _1;
        }

        /** 			prolog &= {ASSIGN, param, var}*/
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = 18;
        *((int *)(_2+8)) = _param_54768;
        *((int *)(_2+12)) = _var_54778;
        _28436 = MAKE_SEQ(_1);
        Concat((object_ptr)&_prolog_54693, _prolog_54693, _28436);
        DeRefDS(_28436);
        _28436 = NOVALUE;

        /** 			if not int_sym then*/
        if (_int_sym_54760 != 0)
        goto LC; // [519] 531

        /** 				int_sym = NewIntSym( 0 )*/
        _int_sym_54760 = _52NewIntSym(0);
        if (!IS_ATOM_INT(_int_sym_54760)) {
            _1 = (long)(DBL_PTR(_int_sym_54760)->dbl);
            if (UNIQUE(DBL_PTR(_int_sym_54760)) && (DBL_PTR(_int_sym_54760)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_int_sym_54760);
            _int_sym_54760 = _1;
        }
LC: 

        /** 			inline_start += 3*/
        _67inline_start_53727 = _67inline_start_53727 + 3;

        /** 			passed_params[p] = var*/
        _2 = (int)SEQ_PTR(_67passed_params_53718);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _67passed_params_53718 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_p_54762))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_p_54762)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _p_54762);
        _1 = *(int *)_2;
        *(int *)_2 = _var_54778;
        DeRef(_1);
LB: 

        /** 		s = SymTab[s][S_NEXT]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _28441 = (int)*(((s1_ptr)_2)->base + _s_54710);
        _2 = (int)SEQ_PTR(_28441);
        _s_54710 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_s_54710)){
            _s_54710 = (long)DBL_PTR(_s_54710)->dbl;
        }
        _28441 = NOVALUE;

        /** 	end for*/
        _0 = _p_54762;
        if (IS_ATOM_INT(_p_54762)) {
            _p_54762 = _p_54762 + 1;
            if ((long)((unsigned long)_p_54762 +(unsigned long) HIGH_BITS) >= 0){
                _p_54762 = NewDouble((double)_p_54762);
            }
        }
        else {
            _p_54762 = binary_op_a(PLUS, _p_54762, 1);
        }
        DeRef(_0);
        goto L8; // [570] 444
L9: 
        ;
        DeRef(_p_54762);
    }

    /** 	symtab_index final_target = 0*/
    _final_target_54793 = 0;

    /** 	while s and */
LD: 
    if (_s_54710 == 0) {
        goto LE; // [587] 699
    }
    _28444 = _52sym_scope(_s_54710);
    if (IS_ATOM_INT(_28444)) {
        _28445 = (_28444 <= 3);
    }
    else {
        _28445 = binary_op(LESSEQ, _28444, 3);
    }
    DeRef(_28444);
    _28444 = NOVALUE;
    if (IS_ATOM_INT(_28445)) {
        if (_28445 != 0) {
            DeRef(_28446);
            _28446 = 1;
            goto LF; // [601] 619
        }
    }
    else {
        if (DBL_PTR(_28445)->dbl != 0.0) {
            DeRef(_28446);
            _28446 = 1;
            goto LF; // [601] 619
        }
    }
    _28447 = _52sym_scope(_s_54710);
    if (IS_ATOM_INT(_28447)) {
        _28448 = (_28447 == 9);
    }
    else {
        _28448 = binary_op(EQUALS, _28447, 9);
    }
    DeRef(_28447);
    _28447 = NOVALUE;
    DeRef(_28446);
    if (IS_ATOM_INT(_28448))
    _28446 = (_28448 != 0);
    else
    _28446 = DBL_PTR(_28448)->dbl != 0.0;
LF: 
    if (_28446 == 0)
    {
        _28446 = NOVALUE;
        goto LE; // [620] 699
    }
    else{
        _28446 = NOVALUE;
    }

    /** 		if sym_scope( s ) != SC_UNDEFINED then*/
    _28449 = _52sym_scope(_s_54710);
    if (binary_op_a(EQUALS, _28449, 9)){
        DeRef(_28449);
        _28449 = NOVALUE;
        goto L10; // [631] 676
    }
    DeRef(_28449);
    _28449 = NOVALUE;

    /** 			varnum += 1*/
    _67varnum_53726 = _67varnum_53726 + 1;

    /** 			symtab_index var = new_inline_var( s, 0 )*/
    _var_54812 = _67new_inline_var(_s_54710, 0);
    if (!IS_ATOM_INT(_var_54812)) {
        _1 = (long)(DBL_PTR(_var_54812)->dbl);
        if (UNIQUE(DBL_PTR(_var_54812)) && (DBL_PTR(_var_54812)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_var_54812);
        _var_54812 = _1;
    }

    /** 			proc_vars &= var*/
    Append(&_67proc_vars_53716, _67proc_vars_53716, _var_54812);

    /** 			if int_sym = 0 then*/
    if (_int_sym_54760 != 0)
    goto L11; // [662] 675

    /** 				int_sym = NewIntSym( 0 )*/
    _int_sym_54760 = _52NewIntSym(0);
    if (!IS_ATOM_INT(_int_sym_54760)) {
        _1 = (long)(DBL_PTR(_int_sym_54760)->dbl);
        if (UNIQUE(DBL_PTR(_int_sym_54760)) && (DBL_PTR(_int_sym_54760)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_int_sym_54760);
        _int_sym_54760 = _1;
    }
L11: 
L10: 

    /** 		s = SymTab[s][S_NEXT]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28456 = (int)*(((s1_ptr)_2)->base + _s_54710);
    _2 = (int)SEQ_PTR(_28456);
    _s_54710 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_54710)){
        _s_54710 = (long)DBL_PTR(_s_54710)->dbl;
    }
    _28456 = NOVALUE;

    /** 	end while*/
    goto LD; // [696] 587
LE: 

    /** 	if not is_proc then*/
    if (_is_proc_54669 != 0)
    goto L12; // [701] 831

    /** 		integer create_target_var = 1*/
    _create_target_var_54825 = 1;

    /** 		if deferred then*/
    if (_deferred_54668 == 0)
    {
        goto L13; // [711] 751
    }
    else{
    }

    /** 			inline_target = Pop()*/
    _0 = _37Pop();
    _67inline_target_53722 = _0;
    if (!IS_ATOM_INT(_67inline_target_53722)) {
        _1 = (long)(DBL_PTR(_67inline_target_53722)->dbl);
        if (UNIQUE(DBL_PTR(_67inline_target_53722)) && (DBL_PTR(_67inline_target_53722)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_67inline_target_53722);
        _67inline_target_53722 = _1;
    }

    /** 			if is_temp( inline_target ) then*/
    _28460 = _67is_temp(_67inline_target_53722);
    if (_28460 == 0) {
        DeRef(_28460);
        _28460 = NOVALUE;
        goto L14; // [729] 744
    }
    else {
        if (!IS_ATOM_INT(_28460) && DBL_PTR(_28460)->dbl == 0.0){
            DeRef(_28460);
            _28460 = NOVALUE;
            goto L14; // [729] 744
        }
        DeRef(_28460);
        _28460 = NOVALUE;
    }
    DeRef(_28460);
    _28460 = NOVALUE;

    /** 				final_target = inline_target*/
    _final_target_54793 = _67inline_target_53722;
    goto L15; // [741] 750
L14: 

    /** 				create_target_var = 0*/
    _create_target_var_54825 = 0;
L15: 
L13: 

    /** 		if create_target_var then*/
    if (_create_target_var_54825 == 0)
    {
        goto L16; // [753] 816
    }
    else{
    }

    /** 			varnum += 1*/
    _67varnum_53726 = _67varnum_53726 + 1;

    /** 			if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L17; // [768] 806
    }
    else{
    }

    /** 				inline_target = new_inline_var( sub, 0 )*/
    _0 = _67new_inline_var(_sub_54666, 0);
    _67inline_target_53722 = _0;
    if (!IS_ATOM_INT(_67inline_target_53722)) {
        _1 = (long)(DBL_PTR(_67inline_target_53722)->dbl);
        if (UNIQUE(DBL_PTR(_67inline_target_53722)) && (DBL_PTR(_67inline_target_53722)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_67inline_target_53722);
        _67inline_target_53722 = _1;
    }

    /** 				SymTab[inline_target][S_VTYPE] = object_type*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_67inline_target_53722 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 15);
    _1 = *(int *)_2;
    *(int *)_2 = _52object_type_47099;
    DeRef(_1);
    _28463 = NOVALUE;

    /** 				Pop_block_var()*/
    _66Pop_block_var();
    goto L18; // [803] 815
L17: 

    /** 				inline_target = NewTempSym()*/
    _0 = _52NewTempSym(0);
    _67inline_target_53722 = _0;
    if (!IS_ATOM_INT(_67inline_target_53722)) {
        _1 = (long)(DBL_PTR(_67inline_target_53722)->dbl);
        if (UNIQUE(DBL_PTR(_67inline_target_53722)) && (DBL_PTR(_67inline_target_53722)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_67inline_target_53722);
        _67inline_target_53722 = _1;
    }
L18: 
L16: 

    /** 		proc_vars &= inline_target*/
    Append(&_67proc_vars_53716, _67proc_vars_53716, _67inline_target_53722);
    goto L19; // [828] 837
L12: 

    /** 		inline_target = 0*/
    _67inline_target_53722 = 0;
L19: 

    /** 	integer check_pc = 1*/
    _check_pc_54848 = 1;

    /** 	while length(inline_code) > check_pc do*/
L1A: 
    if (IS_SEQUENCE(_67inline_code_53715)){
            _28467 = SEQ_PTR(_67inline_code_53715)->length;
    }
    else {
        _28467 = 1;
    }
    if (_28467 <= _check_pc_54848)
    goto L1B; // [852] 1216

    /** 		integer op = inline_code[check_pc]*/
    _2 = (int)SEQ_PTR(_67inline_code_53715);
    _op_54852 = (int)*(((s1_ptr)_2)->base + _check_pc_54848);
    if (!IS_ATOM_INT(_op_54852))
    _op_54852 = (long)DBL_PTR(_op_54852)->dbl;

    /** 		switch op with fallthru do*/
    _0 = _op_54852;
    switch ( _0 ){ 

        /** 			case ATOM_CHECK then*/
        case 101:
        case 97:
        case 96:

        /** 				symtab_index sym = get_original_sym( check_pc + 1 )*/
        _28472 = _check_pc_54848 + 1;
        if (_28472 > MAXINT){
            _28472 = NewDouble((double)_28472);
        }
        _sym_54861 = _67get_original_sym(_28472);
        _28472 = NOVALUE;
        if (!IS_ATOM_INT(_sym_54861)) {
            _1 = (long)(DBL_PTR(_sym_54861)->dbl);
            if (UNIQUE(DBL_PTR(_sym_54861)) && (DBL_PTR(_sym_54861)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_sym_54861);
            _sym_54861 = _1;
        }

        /** 				if is_literal( sym ) then*/
        _28474 = _67is_literal(_sym_54861);
        if (_28474 == 0) {
            DeRef(_28474);
            _28474 = NOVALUE;
            goto L1C; // [897] 1010
        }
        else {
            if (!IS_ATOM_INT(_28474) && DBL_PTR(_28474)->dbl == 0.0){
                DeRef(_28474);
                _28474 = NOVALUE;
                goto L1C; // [897] 1010
            }
            DeRef(_28474);
            _28474 = NOVALUE;
        }
        DeRef(_28474);
        _28474 = NOVALUE;

        /** 					integer check_result*/

        /** 					if op = INTEGER_CHECK then*/
        if (_op_54852 != 96)
        goto L1D; // [906] 930

        /** 						check_result = integer( SymTab[sym][S_OBJ] )*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _28476 = (int)*(((s1_ptr)_2)->base + _sym_54861);
        _2 = (int)SEQ_PTR(_28476);
        _28477 = (int)*(((s1_ptr)_2)->base + 1);
        _28476 = NOVALUE;
        if (IS_ATOM_INT(_28477))
        _check_result_54866 = 1;
        else if (IS_ATOM_DBL(_28477))
        _check_result_54866 = IS_ATOM_INT(DoubleToInt(_28477));
        else
        _check_result_54866 = 0;
        _28477 = NOVALUE;
        goto L1E; // [927] 976
L1D: 

        /** 					elsif op = SEQUENCE_CHECK then*/
        if (_op_54852 != 97)
        goto L1F; // [934] 958

        /** 						check_result = sequence( SymTab[sym][S_OBJ] )*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _28480 = (int)*(((s1_ptr)_2)->base + _sym_54861);
        _2 = (int)SEQ_PTR(_28480);
        _28481 = (int)*(((s1_ptr)_2)->base + 1);
        _28480 = NOVALUE;
        _check_result_54866 = IS_SEQUENCE(_28481);
        _28481 = NOVALUE;
        goto L1E; // [955] 976
L1F: 

        /** 						check_result = atom( SymTab[sym][S_OBJ] )*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _28483 = (int)*(((s1_ptr)_2)->base + _sym_54861);
        _2 = (int)SEQ_PTR(_28483);
        _28484 = (int)*(((s1_ptr)_2)->base + 1);
        _28483 = NOVALUE;
        _check_result_54866 = IS_ATOM(_28484);
        _28484 = NOVALUE;
L1E: 

        /** 					if check_result then*/
        if (_check_result_54866 == 0)
        {
            goto L20; // [980] 997
        }
        else{
        }

        /** 						replace_code( {}, check_pc, check_pc+1 )*/
        _28486 = _check_pc_54848 + 1;
        if (_28486 > MAXINT){
            _28486 = NewDouble((double)_28486);
        }
        RefDS(_22682);
        _67replace_code(_22682, _check_pc_54848, _28486);
        _28486 = NOVALUE;
        goto L21; // [994] 1005
L20: 

        /** 						CompileErr(146)*/
        RefDS(_22682);
        _43CompileErr(146, _22682, 0);
L21: 
        goto L22; // [1007] 1172
L1C: 

        /** 				elsif not is_temp( sym ) then*/
        _28487 = _67is_temp(_sym_54861);
        if (IS_ATOM_INT(_28487)) {
            if (_28487 != 0){
                DeRef(_28487);
                _28487 = NOVALUE;
                goto L23; // [1016] 1165
            }
        }
        else {
            if (DBL_PTR(_28487)->dbl != 0.0){
                DeRef(_28487);
                _28487 = NOVALUE;
                goto L23; // [1016] 1165
            }
        }
        DeRef(_28487);
        _28487 = NOVALUE;

        /** 					if (op = INTEGER_CHECK and SymTab[sym][S_VTYPE] = integer_type )*/
        _28489 = (_op_54852 == 96);
        if (_28489 == 0) {
            _28490 = 0;
            goto L24; // [1027] 1053
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _28491 = (int)*(((s1_ptr)_2)->base + _sym_54861);
        _2 = (int)SEQ_PTR(_28491);
        _28492 = (int)*(((s1_ptr)_2)->base + 15);
        _28491 = NOVALUE;
        if (IS_ATOM_INT(_28492)) {
            _28493 = (_28492 == _52integer_type_47105);
        }
        else {
            _28493 = binary_op(EQUALS, _28492, _52integer_type_47105);
        }
        _28492 = NOVALUE;
        if (IS_ATOM_INT(_28493))
        _28490 = (_28493 != 0);
        else
        _28490 = DBL_PTR(_28493)->dbl != 0.0;
L24: 
        if (_28490 != 0) {
            _28494 = 1;
            goto L25; // [1053] 1093
        }
        _28495 = (_op_54852 == 97);
        if (_28495 == 0) {
            _28496 = 0;
            goto L26; // [1063] 1089
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _28497 = (int)*(((s1_ptr)_2)->base + _sym_54861);
        _2 = (int)SEQ_PTR(_28497);
        _28498 = (int)*(((s1_ptr)_2)->base + 15);
        _28497 = NOVALUE;
        if (IS_ATOM_INT(_28498)) {
            _28499 = (_28498 == _52sequence_type_47103);
        }
        else {
            _28499 = binary_op(EQUALS, _28498, _52sequence_type_47103);
        }
        _28498 = NOVALUE;
        if (IS_ATOM_INT(_28499))
        _28496 = (_28499 != 0);
        else
        _28496 = DBL_PTR(_28499)->dbl != 0.0;
L26: 
        _28494 = (_28496 != 0);
L25: 
        if (_28494 != 0) {
            goto L27; // [1093] 1141
        }
        _28501 = (_op_54852 == 101);
        if (_28501 == 0) {
            DeRef(_28502);
            _28502 = 0;
            goto L28; // [1103] 1136
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _28503 = (int)*(((s1_ptr)_2)->base + _sym_54861);
        _2 = (int)SEQ_PTR(_28503);
        _28504 = (int)*(((s1_ptr)_2)->base + 15);
        _28503 = NOVALUE;
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _52integer_type_47105;
        ((int *)_2)[2] = _52atom_type_47101;
        _28505 = MAKE_SEQ(_1);
        _28506 = find_from(_28504, _28505, 1);
        _28504 = NOVALUE;
        DeRefDS(_28505);
        _28505 = NOVALUE;
        _28502 = (_28506 != 0);
L28: 
        if (_28502 == 0)
        {
            _28502 = NOVALUE;
            goto L29; // [1137] 1155
        }
        else{
            _28502 = NOVALUE;
        }
L27: 

        /** 						replace_code( {}, check_pc, check_pc+1 )*/
        _28507 = _check_pc_54848 + 1;
        if (_28507 > MAXINT){
            _28507 = NewDouble((double)_28507);
        }
        RefDS(_22682);
        _67replace_code(_22682, _check_pc_54848, _28507);
        _28507 = NOVALUE;
        goto L22; // [1152] 1172
L29: 

        /** 						check_pc += 2*/
        _check_pc_54848 = _check_pc_54848 + 2;
        goto L22; // [1162] 1172
L23: 

        /** 					check_pc += 2*/
        _check_pc_54848 = _check_pc_54848 + 2;
L22: 

        /** 				continue*/
        goto L1A; // [1178] 847

        /** 			case STARTLINE then*/
        case 58:

        /** 				check_pc += 2*/
        _check_pc_54848 = _check_pc_54848 + 2;

        /** 				continue*/
        goto L1A; // [1196] 847

        /** 			case else*/
        default:

        /** 				exit*/
        goto L1B; // [1206] 1216
    ;}
    /** 	end while*/
    goto L1A; // [1213] 847
L1B: 

    /** 	for pc = 1 to length( inline_code ) do*/
    if (IS_SEQUENCE(_67inline_code_53715)){
            _28511 = SEQ_PTR(_67inline_code_53715)->length;
    }
    else {
        _28511 = 1;
    }
    {
        int _pc_54938;
        _pc_54938 = 1;
L2A: 
        if (_pc_54938 > _28511){
            goto L2B; // [1223] 1420
        }

        /** 		if sequence( inline_code[pc] ) then*/
        _2 = (int)SEQ_PTR(_67inline_code_53715);
        _28512 = (int)*(((s1_ptr)_2)->base + _pc_54938);
        _28513 = IS_SEQUENCE(_28512);
        _28512 = NOVALUE;
        if (_28513 == 0)
        {
            _28513 = NOVALUE;
            goto L2C; // [1241] 1411
        }
        else{
            _28513 = NOVALUE;
        }

        /** 			integer inline_type = inline_code[pc][1]*/
        _2 = (int)SEQ_PTR(_67inline_code_53715);
        _28514 = (int)*(((s1_ptr)_2)->base + _pc_54938);
        _2 = (int)SEQ_PTR(_28514);
        _inline_type_54943 = (int)*(((s1_ptr)_2)->base + 1);
        if (!IS_ATOM_INT(_inline_type_54943)){
            _inline_type_54943 = (long)DBL_PTR(_inline_type_54943)->dbl;
        }
        _28514 = NOVALUE;

        /** 			switch inline_type do*/
        _0 = _inline_type_54943;
        switch ( _0 ){ 

            /** 				case INLINE_SUB then*/
            case 5:

            /** 					inline_code[pc] = CurrentSub*/
            _2 = (int)SEQ_PTR(_67inline_code_53715);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _67inline_code_53715 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _pc_54938);
            _1 = *(int *)_2;
            *(int *)_2 = _25CurrentSub_12270;
            DeRef(_1);
            goto L2D; // [1279] 1410

            /** 				case INLINE_VAR then*/
            case 6:

            /** 					replace_var( pc )*/
            _67replace_var(_pc_54938);

            /** 					break*/
            goto L2D; // [1292] 1410
            goto L2D; // [1294] 1410

            /** 				case INLINE_TEMP then*/
            case 2:

            /** 					replace_temp( pc )*/
            _67replace_temp(_pc_54938);
            goto L2D; // [1305] 1410

            /** 				case INLINE_PARAM then*/
            case 1:

            /** 					replace_param( pc )*/

            /** 	inline_code[pc] = get_param_sym( pc )*/
            _0 = _replace_param_1__tmp_at1341_54954;
            _replace_param_1__tmp_at1341_54954 = _67get_param_sym(_pc_54938);
            DeRef(_0);
            Ref(_replace_param_1__tmp_at1341_54954);
            _2 = (int)SEQ_PTR(_67inline_code_53715);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _67inline_code_53715 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _pc_54938);
            _1 = *(int *)_2;
            *(int *)_2 = _replace_param_1__tmp_at1341_54954;
            DeRef(_1);

            /** end procedure*/
            goto L2E; // [1327] 1330
L2E: 
            DeRef(_replace_param_1__tmp_at1341_54954);
            _replace_param_1__tmp_at1341_54954 = NOVALUE;
            goto L2D; // [1332] 1410

            /** 				case INLINE_ADDR then*/
            case 4:

            /** 					inline_code[pc] = inline_start + inline_code[pc][2]*/
            _2 = (int)SEQ_PTR(_67inline_code_53715);
            _28518 = (int)*(((s1_ptr)_2)->base + _pc_54938);
            _2 = (int)SEQ_PTR(_28518);
            _28519 = (int)*(((s1_ptr)_2)->base + 2);
            _28518 = NOVALUE;
            if (IS_ATOM_INT(_28519)) {
                _28520 = _67inline_start_53727 + _28519;
                if ((long)((unsigned long)_28520 + (unsigned long)HIGH_BITS) >= 0) 
                _28520 = NewDouble((double)_28520);
            }
            else {
                _28520 = binary_op(PLUS, _67inline_start_53727, _28519);
            }
            _28519 = NOVALUE;
            _2 = (int)SEQ_PTR(_67inline_code_53715);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _67inline_code_53715 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _pc_54938);
            _1 = *(int *)_2;
            *(int *)_2 = _28520;
            if( _1 != _28520 ){
                DeRef(_1);
            }
            _28520 = NOVALUE;
            goto L2D; // [1362] 1410

            /** 				case INLINE_TARGET then*/
            case 3:

            /** 					inline_code[pc] = inline_target*/
            _2 = (int)SEQ_PTR(_67inline_code_53715);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _67inline_code_53715 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _pc_54938);
            _1 = *(int *)_2;
            *(int *)_2 = _67inline_target_53722;
            DeRef(_1);

            /** 					add_inline_target( pc + inline_start )*/
            _28521 = _pc_54938 + _67inline_start_53727;
            if ((long)((unsigned long)_28521 + (unsigned long)HIGH_BITS) >= 0) 
            _28521 = NewDouble((double)_28521);
            _37add_inline_target(_28521);
            _28521 = NOVALUE;

            /** 					break*/
            goto L2D; // [1391] 1410
            goto L2D; // [1393] 1410

            /** 				case else*/
            default:

            /** 					InternalErr( 265, {inline_type} )*/
            _1 = NewS1(1);
            _2 = (int)((s1_ptr)_1)->base;
            *((int *)(_2+4)) = _inline_type_54943;
            _28522 = MAKE_SEQ(_1);
            _43InternalErr(265, _28522);
            _28522 = NOVALUE;
        ;}L2D: 
L2C: 

        /** 	end for*/
        _pc_54938 = _pc_54938 + 1;
        goto L2A; // [1415] 1230
L2B: 
        ;
    }

    /** 	for i = 1 to length(backpatches) do*/
    if (IS_SEQUENCE(_backpatches_54687)){
            _28523 = SEQ_PTR(_backpatches_54687)->length;
    }
    else {
        _28523 = 1;
    }
    {
        int _i_54966;
        _i_54966 = 1;
L2F: 
        if (_i_54966 > _28523){
            goto L30; // [1425] 1448
        }

        /** 		fixup_special_op( backpatches[i] )*/
        _2 = (int)SEQ_PTR(_backpatches_54687);
        _28524 = (int)*(((s1_ptr)_2)->base + _i_54966);
        Ref(_28524);
        _67fixup_special_op(_28524);
        _28524 = NOVALUE;

        /** 	end for*/
        _i_54966 = _i_54966 + 1;
        goto L2F; // [1443] 1432
L30: 
        ;
    }

    /** 	epilog &= End_inline_block( EXIT_BLOCK )*/
    _28525 = _66End_inline_block(206);
    if (IS_SEQUENCE(_epilog_54694) && IS_ATOM(_28525)) {
        Ref(_28525);
        Append(&_epilog_54694, _epilog_54694, _28525);
    }
    else if (IS_ATOM(_epilog_54694) && IS_SEQUENCE(_28525)) {
    }
    else {
        Concat((object_ptr)&_epilog_54694, _epilog_54694, _28525);
    }
    DeRef(_28525);
    _28525 = NOVALUE;

    /** 	if is_proc then*/
    if (_is_proc_54669 == 0)
    {
        goto L31; // [1462] 1472
    }
    else{
    }

    /** 		clear_op()*/
    _37clear_op();
    goto L32; // [1469] 1595
L31: 

    /** 		if not deferred then*/
    if (_deferred_54668 != 0)
    goto L33; // [1474] 1489

    /** 			Push( inline_target )*/
    _37Push(_67inline_target_53722);

    /** 			inlined_function()*/
    _37inlined_function();
L33: 

    /** 		if final_target then*/
    if (_final_target_54793 == 0)
    {
        goto L34; // [1491] 1521
    }
    else{
    }

    /** 			epilog &= { ASSIGN, inline_target, final_target }*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 18;
    *((int *)(_2+8)) = _67inline_target_53722;
    *((int *)(_2+12)) = _final_target_54793;
    _28528 = MAKE_SEQ(_1);
    Concat((object_ptr)&_epilog_54694, _epilog_54694, _28528);
    DeRefDS(_28528);
    _28528 = NOVALUE;

    /** 			emit_temp( final_target, NEW_REFERENCE )*/
    _37emit_temp(_final_target_54793, 1);
    goto L35; // [1518] 1594
L34: 

    /** 			emit_temp( inline_target, NEW_REFERENCE )*/
    _37emit_temp(_67inline_target_53722, 1);

    /** 			if not TRANSLATE then*/
    if (_25TRANSLATE_11874 != 0)
    goto L36; // [1535] 1593

    /** 				epilog &= { ELSE, 0, PRIVATE_INIT_CHECK, inline_target }*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 23;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 30;
    *((int *)(_2+16)) = _67inline_target_53722;
    _28531 = MAKE_SEQ(_1);
    Concat((object_ptr)&_epilog_54694, _epilog_54694, _28531);
    DeRefDS(_28531);
    _28531 = NOVALUE;

    /** 				epilog[$-2] = length(inline_code) + length(epilog) + inline_start + 1*/
    if (IS_SEQUENCE(_epilog_54694)){
            _28533 = SEQ_PTR(_epilog_54694)->length;
    }
    else {
        _28533 = 1;
    }
    _28534 = _28533 - 2;
    _28533 = NOVALUE;
    if (IS_SEQUENCE(_67inline_code_53715)){
            _28535 = SEQ_PTR(_67inline_code_53715)->length;
    }
    else {
        _28535 = 1;
    }
    if (IS_SEQUENCE(_epilog_54694)){
            _28536 = SEQ_PTR(_epilog_54694)->length;
    }
    else {
        _28536 = 1;
    }
    _28537 = _28535 + _28536;
    if ((long)((unsigned long)_28537 + (unsigned long)HIGH_BITS) >= 0) 
    _28537 = NewDouble((double)_28537);
    _28535 = NOVALUE;
    _28536 = NOVALUE;
    if (IS_ATOM_INT(_28537)) {
        _28538 = _28537 + _67inline_start_53727;
        if ((long)((unsigned long)_28538 + (unsigned long)HIGH_BITS) >= 0) 
        _28538 = NewDouble((double)_28538);
    }
    else {
        _28538 = NewDouble(DBL_PTR(_28537)->dbl + (double)_67inline_start_53727);
    }
    DeRef(_28537);
    _28537 = NOVALUE;
    if (IS_ATOM_INT(_28538)) {
        _28539 = _28538 + 1;
        if (_28539 > MAXINT){
            _28539 = NewDouble((double)_28539);
        }
    }
    else
    _28539 = binary_op(PLUS, 1, _28538);
    DeRef(_28538);
    _28538 = NOVALUE;
    _2 = (int)SEQ_PTR(_epilog_54694);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _epilog_54694 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _28534);
    _1 = *(int *)_2;
    *(int *)_2 = _28539;
    if( _1 != _28539 ){
        DeRef(_1);
    }
    _28539 = NOVALUE;
L36: 
L35: 
L32: 

    /** 	return prolog & inline_code & epilog*/
    {
        int concat_list[3];

        concat_list[0] = _epilog_54694;
        concat_list[1] = _67inline_code_53715;
        concat_list[2] = _prolog_54693;
        Concat_N((object_ptr)&_28540, concat_list, 3);
    }
    DeRef(_backpatches_54687);
    DeRefDSi(_prolog_54693);
    DeRefDS(_epilog_54694);
    _28411 = NOVALUE;
    _28424 = NOVALUE;
    DeRef(_28416);
    _28416 = NOVALUE;
    DeRef(_28419);
    _28419 = NOVALUE;
    _28428 = NOVALUE;
    DeRef(_28489);
    _28489 = NOVALUE;
    DeRef(_28445);
    _28445 = NOVALUE;
    DeRef(_28448);
    _28448 = NOVALUE;
    DeRef(_28495);
    _28495 = NOVALUE;
    DeRef(_28493);
    _28493 = NOVALUE;
    DeRef(_28501);
    _28501 = NOVALUE;
    DeRef(_28499);
    _28499 = NOVALUE;
    DeRef(_28534);
    _28534 = NOVALUE;
    return _28540;
    ;
}


void _67defer_call()
{
    int _defer_55006 = NOVALUE;
    int _28543 = NOVALUE;
    int _28542 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer defer = find( inline_sub, deferred_inline_decisions )*/
    _defer_55006 = find_from(_67inline_sub_53729, _67deferred_inline_decisions_53731, 1);

    /** 	if defer then*/
    if (_defer_55006 == 0)
    {
        goto L1; // [14] 36
    }
    else{
    }

    /** 		deferred_inline_calls[defer] &= CurrentSub*/
    _2 = (int)SEQ_PTR(_67deferred_inline_calls_53732);
    _28542 = (int)*(((s1_ptr)_2)->base + _defer_55006);
    if (IS_SEQUENCE(_28542) && IS_ATOM(_25CurrentSub_12270)) {
        Append(&_28543, _28542, _25CurrentSub_12270);
    }
    else if (IS_ATOM(_28542) && IS_SEQUENCE(_25CurrentSub_12270)) {
    }
    else {
        Concat((object_ptr)&_28543, _28542, _25CurrentSub_12270);
        _28542 = NOVALUE;
    }
    _28542 = NOVALUE;
    _2 = (int)SEQ_PTR(_67deferred_inline_calls_53732);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _67deferred_inline_calls_53732 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _defer_55006);
    _1 = *(int *)_2;
    *(int *)_2 = _28543;
    if( _1 != _28543 ){
        DeRef(_1);
    }
    _28543 = NOVALUE;
L1: 

    /** end procedure*/
    return;
    ;
}


void _67emit_or_inline()
{
    int _sub_55015 = NOVALUE;
    int _code_55034 = NOVALUE;
    int _28550 = NOVALUE;
    int _28549 = NOVALUE;
    int _28547 = NOVALUE;
    int _28546 = NOVALUE;
    int _28545 = NOVALUE;
    int _0, _1, _2;
    

    /** 	symtab_index sub = op_info1*/
    _sub_55015 = _37op_info1_51268;

    /** 	inline_sub = sub*/
    _67inline_sub_53729 = _sub_55015;

    /** 	if Parser_mode != PAM_NORMAL then*/
    if (_25Parser_mode_12388 == 0)
    goto L1; // [23] 42

    /** 		emit_op( PROC )*/
    _37emit_op(27);

    /** 		return*/
    DeRef(_code_55034);
    return;
    goto L2; // [39] 90
L1: 

    /** 	elsif atom( SymTab[sub][S_INLINE] ) or has_forward_params(sub) then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _28545 = (int)*(((s1_ptr)_2)->base + _sub_55015);
    _2 = (int)SEQ_PTR(_28545);
    _28546 = (int)*(((s1_ptr)_2)->base + 29);
    _28545 = NOVALUE;
    _28547 = IS_ATOM(_28546);
    _28546 = NOVALUE;
    if (_28547 != 0) {
        goto L3; // [59] 72
    }
    _28549 = _37has_forward_params(_sub_55015);
    if (_28549 == 0) {
        DeRef(_28549);
        _28549 = NOVALUE;
        goto L4; // [68] 89
    }
    else {
        if (!IS_ATOM_INT(_28549) && DBL_PTR(_28549)->dbl == 0.0){
            DeRef(_28549);
            _28549 = NOVALUE;
            goto L4; // [68] 89
        }
        DeRef(_28549);
        _28549 = NOVALUE;
    }
    DeRef(_28549);
    _28549 = NOVALUE;
L3: 

    /** 		defer_call()*/
    _67defer_call();

    /** 		emit_op( PROC )*/
    _37emit_op(27);

    /** 		return*/
    DeRef(_code_55034);
    return;
L4: 
L2: 

    /** 	sequence code = get_inlined_code( sub, length(Code) )*/
    if (IS_SEQUENCE(_25Code_12355)){
            _28550 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _28550 = 1;
    }
    _0 = _code_55034;
    _code_55034 = _67get_inlined_code(_sub_55015, _28550, 0);
    DeRef(_0);
    _28550 = NOVALUE;

    /** 	emit_inline( code )*/
    RefDS(_code_55034);
    _37emit_inline(_code_55034);

    /** 	clear_last()*/
    _37clear_last();

    /** end procedure*/
    DeRefDS(_code_55034);
    return;
    ;
}


void _67inline_deferred_calls()
{
    int _sub_55048 = NOVALUE;
    int _ix_55060 = NOVALUE;
    int _calling_sub_55062 = NOVALUE;
    int _code_55076 = NOVALUE;
    int _calls_55077 = NOVALUE;
    int _is_func_55081 = NOVALUE;
    int _offset_55088 = NOVALUE;
    int _op_55099 = NOVALUE;
    int _size_55102 = NOVALUE;
    int _28608 = NOVALUE;
    int _28606 = NOVALUE;
    int _28604 = NOVALUE;
    int _28603 = NOVALUE;
    int _28602 = NOVALUE;
    int _28600 = NOVALUE;
    int _28599 = NOVALUE;
    int _28598 = NOVALUE;
    int _28597 = NOVALUE;
    int _28596 = NOVALUE;
    int _28595 = NOVALUE;
    int _28594 = NOVALUE;
    int _28593 = NOVALUE;
    int _28592 = NOVALUE;
    int _28591 = NOVALUE;
    int _28589 = NOVALUE;
    int _28588 = NOVALUE;
    int _28587 = NOVALUE;
    int _28586 = NOVALUE;
    int _28584 = NOVALUE;
    int _28583 = NOVALUE;
    int _28582 = NOVALUE;
    int _28580 = NOVALUE;
    int _28578 = NOVALUE;
    int _28576 = NOVALUE;
    int _28574 = NOVALUE;
    int _28573 = NOVALUE;
    int _28572 = NOVALUE;
    int _28571 = NOVALUE;
    int _28569 = NOVALUE;
    int _28568 = NOVALUE;
    int _28565 = NOVALUE;
    int _28563 = NOVALUE;
    int _28561 = NOVALUE;
    int _28560 = NOVALUE;
    int _28559 = NOVALUE;
    int _28558 = NOVALUE;
    int _28557 = NOVALUE;
    int _28556 = NOVALUE;
    int _28554 = NOVALUE;
    int _28553 = NOVALUE;
    int _28552 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	deferred_inlining = 1*/
    _67deferred_inlining_53725 = 1;

    /** 	for i = 1 to length( deferred_inline_decisions ) do*/
    if (IS_SEQUENCE(_67deferred_inline_decisions_53731)){
            _28552 = SEQ_PTR(_67deferred_inline_decisions_53731)->length;
    }
    else {
        _28552 = 1;
    }
    {
        int _i_55043;
        _i_55043 = 1;
L1: 
        if (_i_55043 > _28552){
            goto L2; // [13] 476
        }

        /** 		if length( deferred_inline_calls[i] ) then*/
        _2 = (int)SEQ_PTR(_67deferred_inline_calls_53732);
        _28553 = (int)*(((s1_ptr)_2)->base + _i_55043);
        if (IS_SEQUENCE(_28553)){
                _28554 = SEQ_PTR(_28553)->length;
        }
        else {
            _28554 = 1;
        }
        _28553 = NOVALUE;
        if (_28554 == 0)
        {
            _28554 = NOVALUE;
            goto L3; // [31] 467
        }
        else{
            _28554 = NOVALUE;
        }

        /** 			integer sub = deferred_inline_decisions[i]*/
        _2 = (int)SEQ_PTR(_67deferred_inline_decisions_53731);
        _sub_55048 = (int)*(((s1_ptr)_2)->base + _i_55043);

        /** 			check_inline( sub )*/
        _67check_inline(_sub_55048);

        /** 			if atom( SymTab[sub][S_INLINE] ) then*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _28556 = (int)*(((s1_ptr)_2)->base + _sub_55048);
        _2 = (int)SEQ_PTR(_28556);
        _28557 = (int)*(((s1_ptr)_2)->base + 29);
        _28556 = NOVALUE;
        _28558 = IS_ATOM(_28557);
        _28557 = NOVALUE;
        if (_28558 == 0)
        {
            _28558 = NOVALUE;
            goto L4; // [64] 74
        }
        else{
            _28558 = NOVALUE;
        }

        /** 				continue*/
        goto L5; // [71] 471
L4: 

        /** 			for cx = 1 to length( deferred_inline_calls[i] ) do*/
        _2 = (int)SEQ_PTR(_67deferred_inline_calls_53732);
        _28559 = (int)*(((s1_ptr)_2)->base + _i_55043);
        if (IS_SEQUENCE(_28559)){
                _28560 = SEQ_PTR(_28559)->length;
        }
        else {
            _28560 = 1;
        }
        _28559 = NOVALUE;
        {
            int _cx_55057;
            _cx_55057 = 1;
L6: 
            if (_cx_55057 > _28560){
                goto L7; // [85] 466
            }

            /** 				integer ix = 1*/
            _ix_55060 = 1;

            /** 				symtab_index calling_sub = deferred_inline_calls[i][cx]*/
            _2 = (int)SEQ_PTR(_67deferred_inline_calls_53732);
            _28561 = (int)*(((s1_ptr)_2)->base + _i_55043);
            _2 = (int)SEQ_PTR(_28561);
            _calling_sub_55062 = (int)*(((s1_ptr)_2)->base + _cx_55057);
            if (!IS_ATOM_INT(_calling_sub_55062)){
                _calling_sub_55062 = (long)DBL_PTR(_calling_sub_55062)->dbl;
            }
            _28561 = NOVALUE;

            /** 				CurrentSub = calling_sub*/
            _25CurrentSub_12270 = _calling_sub_55062;

            /** 				Code = SymTab[calling_sub][S_CODE]*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _28563 = (int)*(((s1_ptr)_2)->base + _calling_sub_55062);
            DeRef(_25Code_12355);
            _2 = (int)SEQ_PTR(_28563);
            if (!IS_ATOM_INT(_25S_CODE_11925)){
                _25Code_12355 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
            }
            else{
                _25Code_12355 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
            }
            Ref(_25Code_12355);
            _28563 = NOVALUE;

            /** 				LineTable = SymTab[calling_sub][S_LINETAB]*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _28565 = (int)*(((s1_ptr)_2)->base + _calling_sub_55062);
            DeRef(_25LineTable_12356);
            _2 = (int)SEQ_PTR(_28565);
            if (!IS_ATOM_INT(_25S_LINETAB_11948)){
                _25LineTable_12356 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_LINETAB_11948)->dbl));
            }
            else{
                _25LineTable_12356 = (int)*(((s1_ptr)_2)->base + _25S_LINETAB_11948);
            }
            Ref(_25LineTable_12356);
            _28565 = NOVALUE;

            /** 				sequence code = {}*/
            RefDS(_22682);
            DeRef(_code_55076);
            _code_55076 = _22682;

            /** 				sequence calls = find_ops( 1, PROC )*/
            RefDS(_25Code_12355);
            _0 = _calls_55077;
            _calls_55077 = _65find_ops(1, 27, _25Code_12355);
            DeRef(_0);

            /** 				integer is_func = SymTab[sub][S_TOKEN] != PROC */
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            _28568 = (int)*(((s1_ptr)_2)->base + _sub_55048);
            _2 = (int)SEQ_PTR(_28568);
            if (!IS_ATOM_INT(_25S_TOKEN_11918)){
                _28569 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
            }
            else{
                _28569 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
            }
            _28568 = NOVALUE;
            if (IS_ATOM_INT(_28569)) {
                _is_func_55081 = (_28569 != 27);
            }
            else {
                _is_func_55081 = binary_op(NOTEQ, _28569, 27);
            }
            _28569 = NOVALUE;
            if (!IS_ATOM_INT(_is_func_55081)) {
                _1 = (long)(DBL_PTR(_is_func_55081)->dbl);
                if (UNIQUE(DBL_PTR(_is_func_55081)) && (DBL_PTR(_is_func_55081)->cleanup != 0))
                RTFatal("Cannot assign value with a destructor to an integer");                DeRefDS(_is_func_55081);
                _is_func_55081 = _1;
            }

            /** 				integer offset = 0*/
            _offset_55088 = 0;

            /** 				for o = 1 to length( calls ) do*/
            if (IS_SEQUENCE(_calls_55077)){
                    _28571 = SEQ_PTR(_calls_55077)->length;
            }
            else {
                _28571 = 1;
            }
            {
                int _o_55090;
                _o_55090 = 1;
L8: 
                if (_o_55090 > _28571){
                    goto L9; // [203] 423
                }

                /** 					if calls[o][2][2] = sub then*/
                _2 = (int)SEQ_PTR(_calls_55077);
                _28572 = (int)*(((s1_ptr)_2)->base + _o_55090);
                _2 = (int)SEQ_PTR(_28572);
                _28573 = (int)*(((s1_ptr)_2)->base + 2);
                _28572 = NOVALUE;
                _2 = (int)SEQ_PTR(_28573);
                _28574 = (int)*(((s1_ptr)_2)->base + 2);
                _28573 = NOVALUE;
                if (binary_op_a(NOTEQ, _28574, _sub_55048)){
                    _28574 = NOVALUE;
                    goto LA; // [224] 414
                }
                _28574 = NOVALUE;

                /** 						ix = calls[o][1]*/
                _2 = (int)SEQ_PTR(_calls_55077);
                _28576 = (int)*(((s1_ptr)_2)->base + _o_55090);
                _2 = (int)SEQ_PTR(_28576);
                _ix_55060 = (int)*(((s1_ptr)_2)->base + 1);
                if (!IS_ATOM_INT(_ix_55060)){
                    _ix_55060 = (long)DBL_PTR(_ix_55060)->dbl;
                }
                _28576 = NOVALUE;

                /** 						sequence op = calls[o][2]*/
                _2 = (int)SEQ_PTR(_calls_55077);
                _28578 = (int)*(((s1_ptr)_2)->base + _o_55090);
                DeRef(_op_55099);
                _2 = (int)SEQ_PTR(_28578);
                _op_55099 = (int)*(((s1_ptr)_2)->base + 2);
                Ref(_op_55099);
                _28578 = NOVALUE;

                /** 						integer size = length( op ) - 1*/
                if (IS_SEQUENCE(_op_55099)){
                        _28580 = SEQ_PTR(_op_55099)->length;
                }
                else {
                    _28580 = 1;
                }
                _size_55102 = _28580 - 1;
                _28580 = NOVALUE;

                /** 						if is_func then*/
                if (_is_func_55081 == 0)
                {
                    goto LB; // [263] 289
                }
                else{
                }

                /** 							Push( op[$] )*/
                if (IS_SEQUENCE(_op_55099)){
                        _28582 = SEQ_PTR(_op_55099)->length;
                }
                else {
                    _28582 = 1;
                }
                _2 = (int)SEQ_PTR(_op_55099);
                _28583 = (int)*(((s1_ptr)_2)->base + _28582);
                Ref(_28583);
                _37Push(_28583);
                _28583 = NOVALUE;

                /** 							op = remove( op, length(op) )*/
                if (IS_SEQUENCE(_op_55099)){
                        _28584 = SEQ_PTR(_op_55099)->length;
                }
                else {
                    _28584 = 1;
                }
                {
                    s1_ptr assign_space = SEQ_PTR(_op_55099);
                    int len = assign_space->length;
                    int start = (IS_ATOM_INT(_28584)) ? _28584 : (long)(DBL_PTR(_28584)->dbl);
                    int stop = (IS_ATOM_INT(_28584)) ? _28584 : (long)(DBL_PTR(_28584)->dbl);
                    if (stop > len){
                        stop = len;
                    }
                    if (start > len || start > stop || stop<1) {
                    }
                    else if (start < 2) {
                        if (stop >= len) {
                            Head( SEQ_PTR(_op_55099), start, &_op_55099 );
                        }
                        else Tail(SEQ_PTR(_op_55099), stop+1, &_op_55099);
                    }
                    else if (stop >= len){
                        Head(SEQ_PTR(_op_55099), start, &_op_55099);
                    }
                    else {
                        assign_slice_seq = &assign_space;
                        _op_55099 = Remove_elements(start, stop, (SEQ_PTR(_op_55099)->ref == 1));
                    }
                }
                _28584 = NOVALUE;
                _28584 = NOVALUE;
LB: 

                /** 						for p = 3 to length( op ) do*/
                if (IS_SEQUENCE(_op_55099)){
                        _28586 = SEQ_PTR(_op_55099)->length;
                }
                else {
                    _28586 = 1;
                }
                {
                    int _p_55112;
                    _p_55112 = 3;
LC: 
                    if (_p_55112 > _28586){
                        goto LD; // [294] 317
                    }

                    /** 							Push( op[p] )*/
                    _2 = (int)SEQ_PTR(_op_55099);
                    _28587 = (int)*(((s1_ptr)_2)->base + _p_55112);
                    Ref(_28587);
                    _37Push(_28587);
                    _28587 = NOVALUE;

                    /** 						end for*/
                    _p_55112 = _p_55112 + 1;
                    goto LC; // [312] 301
LD: 
                    ;
                }

                /** 						code = get_inlined_code( sub, ix + offset - 1, 1 )*/
                _28588 = _ix_55060 + _offset_55088;
                if ((long)((unsigned long)_28588 + (unsigned long)HIGH_BITS) >= 0) 
                _28588 = NewDouble((double)_28588);
                if (IS_ATOM_INT(_28588)) {
                    _28589 = _28588 - 1;
                    if ((long)((unsigned long)_28589 +(unsigned long) HIGH_BITS) >= 0){
                        _28589 = NewDouble((double)_28589);
                    }
                }
                else {
                    _28589 = NewDouble(DBL_PTR(_28588)->dbl - (double)1);
                }
                DeRef(_28588);
                _28588 = NOVALUE;
                _0 = _code_55076;
                _code_55076 = _67get_inlined_code(_sub_55048, _28589, 1);
                DeRef(_0);
                _28589 = NOVALUE;

                /** 						shift:replace_code( repeat( NOP1, length(code) ), ix + offset, ix + offset + size )*/
                if (IS_SEQUENCE(_code_55076)){
                        _28591 = SEQ_PTR(_code_55076)->length;
                }
                else {
                    _28591 = 1;
                }
                _28592 = Repeat(159, _28591);
                _28591 = NOVALUE;
                _28593 = _ix_55060 + _offset_55088;
                if ((long)((unsigned long)_28593 + (unsigned long)HIGH_BITS) >= 0) 
                _28593 = NewDouble((double)_28593);
                _28594 = _ix_55060 + _offset_55088;
                if ((long)((unsigned long)_28594 + (unsigned long)HIGH_BITS) >= 0) 
                _28594 = NewDouble((double)_28594);
                if (IS_ATOM_INT(_28594)) {
                    _28595 = _28594 + _size_55102;
                    if ((long)((unsigned long)_28595 + (unsigned long)HIGH_BITS) >= 0) 
                    _28595 = NewDouble((double)_28595);
                }
                else {
                    _28595 = NewDouble(DBL_PTR(_28594)->dbl + (double)_size_55102);
                }
                DeRef(_28594);
                _28594 = NOVALUE;
                _65replace_code(_28592, _28593, _28595);
                _28592 = NOVALUE;
                _28593 = NOVALUE;
                _28595 = NOVALUE;

                /** 						Code = eu:replace( Code, code, ix + offset, ix + offset + length( code ) -1 )*/
                _28596 = _ix_55060 + _offset_55088;
                if ((long)((unsigned long)_28596 + (unsigned long)HIGH_BITS) >= 0) 
                _28596 = NewDouble((double)_28596);
                _28597 = _ix_55060 + _offset_55088;
                if ((long)((unsigned long)_28597 + (unsigned long)HIGH_BITS) >= 0) 
                _28597 = NewDouble((double)_28597);
                if (IS_SEQUENCE(_code_55076)){
                        _28598 = SEQ_PTR(_code_55076)->length;
                }
                else {
                    _28598 = 1;
                }
                if (IS_ATOM_INT(_28597)) {
                    _28599 = _28597 + _28598;
                    if ((long)((unsigned long)_28599 + (unsigned long)HIGH_BITS) >= 0) 
                    _28599 = NewDouble((double)_28599);
                }
                else {
                    _28599 = NewDouble(DBL_PTR(_28597)->dbl + (double)_28598);
                }
                DeRef(_28597);
                _28597 = NOVALUE;
                _28598 = NOVALUE;
                if (IS_ATOM_INT(_28599)) {
                    _28600 = _28599 - 1;
                    if ((long)((unsigned long)_28600 +(unsigned long) HIGH_BITS) >= 0){
                        _28600 = NewDouble((double)_28600);
                    }
                }
                else {
                    _28600 = NewDouble(DBL_PTR(_28599)->dbl - (double)1);
                }
                DeRef(_28599);
                _28599 = NOVALUE;
                {
                    int p1 = _25Code_12355;
                    int p2 = _code_55076;
                    int p3 = _28596;
                    int p4 = _28600;
                    struct replace_block replace_params;
                    replace_params.copy_to   = &p1;
                    replace_params.copy_from = &p2;
                    replace_params.start     = &p3;
                    replace_params.stop      = &p4;
                    replace_params.target    = &_25Code_12355;
                    Replace( &replace_params );
                }
                DeRef(_28596);
                _28596 = NOVALUE;
                DeRef(_28600);
                _28600 = NOVALUE;

                /** 						offset += length(code) - size - 1*/
                if (IS_SEQUENCE(_code_55076)){
                        _28602 = SEQ_PTR(_code_55076)->length;
                }
                else {
                    _28602 = 1;
                }
                _28603 = _28602 - _size_55102;
                if ((long)((unsigned long)_28603 +(unsigned long) HIGH_BITS) >= 0){
                    _28603 = NewDouble((double)_28603);
                }
                _28602 = NOVALUE;
                if (IS_ATOM_INT(_28603)) {
                    _28604 = _28603 - 1;
                    if ((long)((unsigned long)_28604 +(unsigned long) HIGH_BITS) >= 0){
                        _28604 = NewDouble((double)_28604);
                    }
                }
                else {
                    _28604 = NewDouble(DBL_PTR(_28603)->dbl - (double)1);
                }
                DeRef(_28603);
                _28603 = NOVALUE;
                if (IS_ATOM_INT(_28604)) {
                    _offset_55088 = _offset_55088 + _28604;
                }
                else {
                    _offset_55088 = NewDouble((double)_offset_55088 + DBL_PTR(_28604)->dbl);
                }
                DeRef(_28604);
                _28604 = NOVALUE;
                if (!IS_ATOM_INT(_offset_55088)) {
                    _1 = (long)(DBL_PTR(_offset_55088)->dbl);
                    if (UNIQUE(DBL_PTR(_offset_55088)) && (DBL_PTR(_offset_55088)->cleanup != 0))
                    RTFatal("Cannot assign value with a destructor to an integer");                    DeRefDS(_offset_55088);
                    _offset_55088 = _1;
                }
LA: 
                DeRef(_op_55099);
                _op_55099 = NOVALUE;

                /** 				end for*/
                _o_55090 = _o_55090 + 1;
                goto L8; // [418] 210
L9: 
                ;
            }

            /** 				SymTab[calling_sub][S_CODE] = Code*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _26SymTab_11138 = MAKE_SEQ(_2);
            }
            _3 = (int)(_calling_sub_55062 + ((s1_ptr)_2)->base);
            RefDS(_25Code_12355);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            if (!IS_ATOM_INT(_25S_CODE_11925))
            _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
            else
            _2 = (int)(((s1_ptr)_2)->base + _25S_CODE_11925);
            _1 = *(int *)_2;
            *(int *)_2 = _25Code_12355;
            DeRef(_1);
            _28606 = NOVALUE;

            /** 				SymTab[calling_sub][S_LINETAB] = LineTable*/
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _26SymTab_11138 = MAKE_SEQ(_2);
            }
            _3 = (int)(_calling_sub_55062 + ((s1_ptr)_2)->base);
            RefDS(_25LineTable_12356);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            if (!IS_ATOM_INT(_25S_LINETAB_11948))
            _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_LINETAB_11948)->dbl));
            else
            _2 = (int)(((s1_ptr)_2)->base + _25S_LINETAB_11948);
            _1 = *(int *)_2;
            *(int *)_2 = _25LineTable_12356;
            DeRef(_1);
            _28608 = NOVALUE;
            DeRef(_code_55076);
            _code_55076 = NOVALUE;
            DeRef(_calls_55077);
            _calls_55077 = NOVALUE;

            /** 			end for*/
            _cx_55057 = _cx_55057 + 1;
            goto L6; // [461] 92
L7: 
            ;
        }
L3: 

        /** 	end for*/
L5: 
        _i_55043 = _i_55043 + 1;
        goto L1; // [471] 20
L2: 
        ;
    }

    /** end procedure*/
    _28553 = NOVALUE;
    _28559 = NOVALUE;
    return;
    ;
}



// 0xFCE433FC
