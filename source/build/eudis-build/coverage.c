// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _49check_coverage()
{
    int _25882 = NOVALUE;
    int _25881 = NOVALUE;
    int _25880 = NOVALUE;
    int _25879 = NOVALUE;
    int _25878 = NOVALUE;
    int _25877 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = length( file_coverage ) + 1 to length( known_files ) do*/
    if (IS_SEQUENCE(_49file_coverage_49114)){
            _25877 = SEQ_PTR(_49file_coverage_49114)->length;
    }
    else {
        _25877 = 1;
    }
    _25878 = _25877 + 1;
    _25877 = NOVALUE;
    if (IS_SEQUENCE(_26known_files_11139)){
            _25879 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _25879 = 1;
    }
    {
        int _i_49125;
        _i_49125 = _25878;
L1: 
        if (_i_49125 > _25879){
            goto L2; // [17] 58
        }

        /** 		file_coverage &= find( canonical_path( known_files[i],,1 ), covered_files )*/
        _2 = (int)SEQ_PTR(_26known_files_11139);
        _25880 = (int)*(((s1_ptr)_2)->base + _i_49125);
        Ref(_25880);
        _25881 = _9canonical_path(_25880, 0, 1);
        _25880 = NOVALUE;
        _25882 = find_from(_25881, _49covered_files_49113, 1);
        DeRef(_25881);
        _25881 = NOVALUE;
        Append(&_49file_coverage_49114, _49file_coverage_49114, _25882);
        _25882 = NOVALUE;

        /** 	end for*/
        _i_49125 = _i_49125 + 1;
        goto L1; // [53] 24
L2: 
        ;
    }

    /** end procedure*/
    DeRef(_25878);
    _25878 = NOVALUE;
    return;
    ;
}


void _49init_coverage()
{
    int _cmd_49149 = NOVALUE;
    int _25900 = NOVALUE;
    int _25899 = NOVALUE;
    int _25897 = NOVALUE;
    int _25896 = NOVALUE;
    int _25895 = NOVALUE;
    int _25893 = NOVALUE;
    int _25891 = NOVALUE;
    int _25890 = NOVALUE;
    int _25888 = NOVALUE;
    int _25887 = NOVALUE;
    int _25886 = NOVALUE;
    int _25885 = NOVALUE;
    int _25884 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if initialized_coverage then*/
    if (_49initialized_coverage_49121 == 0)
    {
        goto L1; // [5] 14
    }
    else{
    }

    /** 		return*/
    return;
L1: 

    /** 	initialized_coverage = 1*/
    _49initialized_coverage_49121 = 1;

    /** 	for i = 1 to length( file_coverage ) do*/
    if (IS_SEQUENCE(_49file_coverage_49114)){
            _25884 = SEQ_PTR(_49file_coverage_49114)->length;
    }
    else {
        _25884 = 1;
    }
    {
        int _i_49140;
        _i_49140 = 1;
L2: 
        if (_i_49140 > _25884){
            goto L3; // [26] 67
        }

        /** 		file_coverage[i] = find( canonical_path( known_files[i],,1 ), covered_files )*/
        _2 = (int)SEQ_PTR(_26known_files_11139);
        _25885 = (int)*(((s1_ptr)_2)->base + _i_49140);
        Ref(_25885);
        _25886 = _9canonical_path(_25885, 0, 1);
        _25885 = NOVALUE;
        _25887 = find_from(_25886, _49covered_files_49113, 1);
        DeRef(_25886);
        _25886 = NOVALUE;
        _2 = (int)SEQ_PTR(_49file_coverage_49114);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _49file_coverage_49114 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_49140);
        *(int *)_2 = _25887;
        if( _1 != _25887 ){
        }
        _25887 = NOVALUE;

        /** 	end for*/
        _i_49140 = _i_49140 + 1;
        goto L2; // [62] 33
L3: 
        ;
    }

    /** 	if equal( coverage_db_name, "" ) then*/
    if (_49coverage_db_name_49115 == _22682)
    _25888 = 1;
    else if (IS_ATOM_INT(_49coverage_db_name_49115) && IS_ATOM_INT(_22682))
    _25888 = 0;
    else
    _25888 = (compare(_49coverage_db_name_49115, _22682) == 0);
    if (_25888 == 0)
    {
        _25888 = NOVALUE;
        goto L4; // [75] 107
    }
    else{
        _25888 = NOVALUE;
    }

    /** 		sequence cmd = command_line()*/
    DeRef(_cmd_49149);
    _cmd_49149 = Command_Line();

    /** 		coverage_db_name = canonical_path( filebase( cmd[2] ) & "-cvg.edb" )*/
    _2 = (int)SEQ_PTR(_cmd_49149);
    _25890 = (int)*(((s1_ptr)_2)->base + 2);
    RefDS(_25890);
    _25891 = _9filebase(_25890);
    _25890 = NOVALUE;
    if (IS_SEQUENCE(_25891) && IS_ATOM(_25892)) {
    }
    else if (IS_ATOM(_25891) && IS_SEQUENCE(_25892)) {
        Ref(_25891);
        Prepend(&_25893, _25892, _25891);
    }
    else {
        Concat((object_ptr)&_25893, _25891, _25892);
        DeRef(_25891);
        _25891 = NOVALUE;
    }
    DeRef(_25891);
    _25891 = NOVALUE;
    _0 = _9canonical_path(_25893, 0, 0);
    DeRefDS(_49coverage_db_name_49115);
    _49coverage_db_name_49115 = _0;
    _25893 = NOVALUE;
L4: 
    DeRef(_cmd_49149);
    _cmd_49149 = NOVALUE;

    /** 	if coverage_erase and file_exists( coverage_db_name ) then*/
    if (_49coverage_erase_49116 == 0) {
        goto L5; // [113] 153
    }
    RefDS(_49coverage_db_name_49115);
    _25896 = _9file_exists(_49coverage_db_name_49115);
    if (_25896 == 0) {
        DeRef(_25896);
        _25896 = NOVALUE;
        goto L5; // [124] 153
    }
    else {
        if (!IS_ATOM_INT(_25896) && DBL_PTR(_25896)->dbl == 0.0){
            DeRef(_25896);
            _25896 = NOVALUE;
            goto L5; // [124] 153
        }
        DeRef(_25896);
        _25896 = NOVALUE;
    }
    DeRef(_25896);
    _25896 = NOVALUE;

    /** 		if not delete_file( coverage_db_name ) then*/
    RefDS(_49coverage_db_name_49115);
    _25897 = _9delete_file(_49coverage_db_name_49115);
    if (IS_ATOM_INT(_25897)) {
        if (_25897 != 0){
            DeRef(_25897);
            _25897 = NOVALUE;
            goto L6; // [135] 152
        }
    }
    else {
        if (DBL_PTR(_25897)->dbl != 0.0){
            DeRef(_25897);
            _25897 = NOVALUE;
            goto L6; // [135] 152
        }
    }
    DeRef(_25897);
    _25897 = NOVALUE;

    /** 			CompileErr( 335, { coverage_db_name } )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_49coverage_db_name_49115);
    *((int *)(_2+4)) = _49coverage_db_name_49115;
    _25899 = MAKE_SEQ(_1);
    _43CompileErr(335, _25899, 0);
    _25899 = NOVALUE;
L6: 
L5: 

    /** 	if db_open( coverage_db_name ) = DB_OK then*/
    RefDS(_49coverage_db_name_49115);
    _25900 = _48db_open(_49coverage_db_name_49115, 0);
    if (binary_op_a(NOTEQ, _25900, 0)){
        DeRef(_25900);
        _25900 = NOVALUE;
        goto L7; // [166] 179
    }
    DeRef(_25900);
    _25900 = NOVALUE;

    /** 		read_coverage_db()*/
    _49read_coverage_db();

    /** 		db_close()*/
    _48db_close();
L7: 

    /** end procedure*/
    return;
    ;
}


void _49write_map(int _coverage_49178, int _table_name_49179)
{
    int _keys_49201 = NOVALUE;
    int _rec_49206 = NOVALUE;
    int _val_49210 = NOVALUE;
    int _32380 = NOVALUE;
    int _25917 = NOVALUE;
    int _25914 = NOVALUE;
    int _25912 = NOVALUE;
    int _25911 = NOVALUE;
    int _25909 = NOVALUE;
    int _25908 = NOVALUE;
    int _25906 = NOVALUE;
    int _25904 = NOVALUE;
    int _25902 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if db_select( coverage_db_name, DB_LOCK_EXCLUSIVE) = DB_OK then*/
    RefDS(_49coverage_db_name_49115);
    _25902 = _48db_select(_49coverage_db_name_49115, 2);
    if (binary_op_a(NOTEQ, _25902, 0)){
        DeRef(_25902);
        _25902 = NOVALUE;
        goto L1; // [16] 61
    }
    DeRef(_25902);
    _25902 = NOVALUE;

    /** 		if db_select_table( table_name ) != DB_OK then*/
    RefDS(_table_name_49179);
    _25904 = _48db_select_table(_table_name_49179);
    if (binary_op_a(EQUALS, _25904, 0)){
        DeRef(_25904);
        _25904 = NOVALUE;
        goto L2; // [28] 73
    }
    DeRef(_25904);
    _25904 = NOVALUE;

    /** 			if db_create_table( table_name ) != DB_OK then*/
    RefDS(_table_name_49179);
    _25906 = _48db_create_table(_table_name_49179, 50);
    if (binary_op_a(EQUALS, _25906, 0)){
        DeRef(_25906);
        _25906 = NOVALUE;
        goto L2; // [41] 73
    }
    DeRef(_25906);
    _25906 = NOVALUE;

    /** 				CompileErr( 336, {table_name} )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_table_name_49179);
    *((int *)(_2+4)) = _table_name_49179;
    _25908 = MAKE_SEQ(_1);
    _43CompileErr(336, _25908, 0);
    _25908 = NOVALUE;
    goto L2; // [58] 73
L1: 

    /** 		CompileErr( 336, {table_name} )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_table_name_49179);
    *((int *)(_2+4)) = _table_name_49179;
    _25909 = MAKE_SEQ(_1);
    _43CompileErr(336, _25909, 0);
    _25909 = NOVALUE;
L2: 

    /** 	sequence keys = map:keys( coverage )*/
    Ref(_coverage_49178);
    _0 = _keys_49201;
    _keys_49201 = _32keys(_coverage_49178, 0);
    DeRef(_0);

    /** 	for i = 1 to length( keys ) do*/
    if (IS_SEQUENCE(_keys_49201)){
            _25911 = SEQ_PTR(_keys_49201)->length;
    }
    else {
        _25911 = 1;
    }
    {
        int _i_49204;
        _i_49204 = 1;
L3: 
        if (_i_49204 > _25911){
            goto L4; // [87] 167
        }

        /** 		integer rec = db_find_key( keys[i] )*/
        _2 = (int)SEQ_PTR(_keys_49201);
        _25912 = (int)*(((s1_ptr)_2)->base + _i_49204);
        Ref(_25912);
        RefDS(_48current_table_name_17845);
        _rec_49206 = _48db_find_key(_25912, _48current_table_name_17845);
        _25912 = NOVALUE;
        if (!IS_ATOM_INT(_rec_49206)) {
            _1 = (long)(DBL_PTR(_rec_49206)->dbl);
            if (UNIQUE(DBL_PTR(_rec_49206)) && (DBL_PTR(_rec_49206)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_rec_49206);
            _rec_49206 = _1;
        }

        /** 		integer val = map:get( coverage, keys[i] )*/
        _2 = (int)SEQ_PTR(_keys_49201);
        _25914 = (int)*(((s1_ptr)_2)->base + _i_49204);
        Ref(_coverage_49178);
        Ref(_25914);
        _val_49210 = _32get(_coverage_49178, _25914, 0);
        _25914 = NOVALUE;
        if (!IS_ATOM_INT(_val_49210)) {
            _1 = (long)(DBL_PTR(_val_49210)->dbl);
            if (UNIQUE(DBL_PTR(_val_49210)) && (DBL_PTR(_val_49210)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_val_49210);
            _val_49210 = _1;
        }

        /** 		if rec > 0 then*/
        if (_rec_49206 <= 0)
        goto L5; // [125] 141

        /** 			db_replace_data( rec, val )*/
        RefDS(_48current_table_name_17845);
        _48db_replace_data(_rec_49206, _val_49210, _48current_table_name_17845);
        goto L6; // [138] 158
L5: 

        /** 			db_insert( keys[i], val )*/
        _2 = (int)SEQ_PTR(_keys_49201);
        _25917 = (int)*(((s1_ptr)_2)->base + _i_49204);
        Ref(_25917);
        RefDS(_48current_table_name_17845);
        _32380 = _48db_insert(_25917, _val_49210, _48current_table_name_17845);
        _25917 = NOVALUE;
        DeRef(_32380);
        _32380 = NOVALUE;
L6: 

        /** 	end for*/
        _i_49204 = _i_49204 + 1;
        goto L3; // [162] 94
L4: 
        ;
    }

    /** end procedure*/
    DeRef(_coverage_49178);
    DeRefDS(_table_name_49179);
    DeRef(_keys_49201);
    return;
    ;
}


int _49write_coverage_db()
{
    int _25932 = NOVALUE;
    int _25931 = NOVALUE;
    int _25930 = NOVALUE;
    int _25929 = NOVALUE;
    int _25928 = NOVALUE;
    int _25927 = NOVALUE;
    int _25926 = NOVALUE;
    int _25925 = NOVALUE;
    int _25922 = NOVALUE;
    int _25920 = NOVALUE;
    int _25918 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if wrote_coverage then*/
    if (_49wrote_coverage_49219 == 0)
    {
        goto L1; // [5] 15
    }
    else{
    }

    /** 		return 1*/
    return 1;
L1: 

    /** 	wrote_coverage = 1*/
    _49wrote_coverage_49219 = 1;

    /** 	init_coverage()*/
    _49init_coverage();

    /** 	if not length( covered_files ) then*/
    if (IS_SEQUENCE(_49covered_files_49113)){
            _25918 = SEQ_PTR(_49covered_files_49113)->length;
    }
    else {
        _25918 = 1;
    }
    if (_25918 != 0)
    goto L2; // [31] 41
    _25918 = NOVALUE;

    /** 		return 1*/
    return 1;
L2: 

    /** 	if DB_OK != db_open( coverage_db_name, DB_LOCK_EXCLUSIVE) then*/
    RefDS(_49coverage_db_name_49115);
    _25920 = _48db_open(_49coverage_db_name_49115, 2);
    if (binary_op_a(EQUALS, 0, _25920)){
        DeRef(_25920);
        _25920 = NOVALUE;
        goto L3; // [54] 97
    }
    DeRef(_25920);
    _25920 = NOVALUE;

    /** 		if DB_OK != db_create( coverage_db_name ) then*/
    RefDS(_49coverage_db_name_49115);
    _25922 = _48db_create(_49coverage_db_name_49115, 0, 5, 5);
    if (binary_op_a(EQUALS, 0, _25922)){
        DeRef(_25922);
        _25922 = NOVALUE;
        goto L4; // [73] 96
    }
    DeRef(_25922);
    _25922 = NOVALUE;

    /** 			printf(2, "error opening %s\n", {coverage_db_name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_49coverage_db_name_49115);
    *((int *)(_2+4)) = _49coverage_db_name_49115;
    _25925 = MAKE_SEQ(_1);
    EPrintf(2, _25924, _25925);
    DeRefDS(_25925);
    _25925 = NOVALUE;

    /** 			return 0*/
    return 0;
L4: 
L3: 

    /** 	process_lines()*/
    _49process_lines();

    /** 	for tx = 1 to length( routine_map ) do*/
    if (IS_SEQUENCE(_49routine_map_49119)){
            _25926 = SEQ_PTR(_49routine_map_49119)->length;
    }
    else {
        _25926 = 1;
    }
    {
        int _tx_49241;
        _tx_49241 = 1;
L5: 
        if (_tx_49241 > _25926){
            goto L6; // [108] 166
        }

        /** 		write_map( routine_map[tx], 'r' & covered_files[tx] )*/
        _2 = (int)SEQ_PTR(_49routine_map_49119);
        _25927 = (int)*(((s1_ptr)_2)->base + _tx_49241);
        _2 = (int)SEQ_PTR(_49covered_files_49113);
        _25928 = (int)*(((s1_ptr)_2)->base + _tx_49241);
        if (IS_SEQUENCE(114) && IS_ATOM(_25928)) {
        }
        else if (IS_ATOM(114) && IS_SEQUENCE(_25928)) {
            Prepend(&_25929, _25928, 114);
        }
        else {
            Concat((object_ptr)&_25929, 114, _25928);
        }
        _25928 = NOVALUE;
        Ref(_25927);
        _49write_map(_25927, _25929);
        _25927 = NOVALUE;
        _25929 = NOVALUE;

        /** 		write_map( line_map[tx],    'l' & covered_files[tx] )*/
        _2 = (int)SEQ_PTR(_49line_map_49118);
        _25930 = (int)*(((s1_ptr)_2)->base + _tx_49241);
        _2 = (int)SEQ_PTR(_49covered_files_49113);
        _25931 = (int)*(((s1_ptr)_2)->base + _tx_49241);
        if (IS_SEQUENCE(108) && IS_ATOM(_25931)) {
        }
        else if (IS_ATOM(108) && IS_SEQUENCE(_25931)) {
            Prepend(&_25932, _25931, 108);
        }
        else {
            Concat((object_ptr)&_25932, 108, _25931);
        }
        _25931 = NOVALUE;
        Ref(_25930);
        _49write_map(_25930, _25932);
        _25930 = NOVALUE;
        _25932 = NOVALUE;

        /** 	end for*/
        _tx_49241 = _tx_49241 + 1;
        goto L5; // [161] 115
L6: 
        ;
    }

    /** 	db_close()*/
    _48db_close();

    /** 	routine_map = {}*/
    RefDS(_22682);
    DeRef(_49routine_map_49119);
    _49routine_map_49119 = _22682;

    /** 	line_map    = {}*/
    RefDS(_22682);
    DeRef(_49line_map_49118);
    _49line_map_49118 = _22682;

    /** 	return 1*/
    return 1;
    ;
}


void _49read_coverage_db()
{
    int _tables_49252 = NOVALUE;
    int _name_49258 = NOVALUE;
    int _fx_49262 = NOVALUE;
    int _the_map_49269 = NOVALUE;
    int _32379 = NOVALUE;
    int _25948 = NOVALUE;
    int _25947 = NOVALUE;
    int _25946 = NOVALUE;
    int _25942 = NOVALUE;
    int _25941 = NOVALUE;
    int _25940 = NOVALUE;
    int _25936 = NOVALUE;
    int _25935 = NOVALUE;
    int _25934 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence tables = db_table_list()*/
    _0 = _tables_49252;
    _tables_49252 = _48db_table_list();
    DeRef(_0);

    /** 	for i = 1 to length( tables ) do*/
    if (IS_SEQUENCE(_tables_49252)){
            _25934 = SEQ_PTR(_tables_49252)->length;
    }
    else {
        _25934 = 1;
    }
    {
        int _i_49256;
        _i_49256 = 1;
L1: 
        if (_i_49256 > _25934){
            goto L2; // [13] 161
        }

        /** 		sequence name = tables[i][2..$]*/
        _2 = (int)SEQ_PTR(_tables_49252);
        _25935 = (int)*(((s1_ptr)_2)->base + _i_49256);
        if (IS_SEQUENCE(_25935)){
                _25936 = SEQ_PTR(_25935)->length;
        }
        else {
            _25936 = 1;
        }
        rhs_slice_target = (object_ptr)&_name_49258;
        RHS_Slice(_25935, 2, _25936);
        _25935 = NOVALUE;

        /** 		integer fx = find( name, covered_files )*/
        _fx_49262 = find_from(_name_49258, _49covered_files_49113, 1);

        /** 		if not fx then*/
        if (_fx_49262 != 0)
        goto L3; // [45] 55

        /** 			continue*/
        DeRefDS(_name_49258);
        _name_49258 = NOVALUE;
        DeRef(_the_map_49269);
        _the_map_49269 = NOVALUE;
        goto L4; // [52] 156
L3: 

        /** 		db_select_table( tables[i] )*/
        _2 = (int)SEQ_PTR(_tables_49252);
        _25940 = (int)*(((s1_ptr)_2)->base + _i_49256);
        Ref(_25940);
        _32379 = _48db_select_table(_25940);
        _25940 = NOVALUE;
        DeRef(_32379);
        _32379 = NOVALUE;

        /** 		if tables[i][1] = 'r' then*/
        _2 = (int)SEQ_PTR(_tables_49252);
        _25941 = (int)*(((s1_ptr)_2)->base + _i_49256);
        _2 = (int)SEQ_PTR(_25941);
        _25942 = (int)*(((s1_ptr)_2)->base + 1);
        _25941 = NOVALUE;
        if (binary_op_a(NOTEQ, _25942, 114)){
            _25942 = NOVALUE;
            goto L5; // [77] 92
        }
        _25942 = NOVALUE;

        /** 			the_map = routine_map[fx]*/
        DeRef(_the_map_49269);
        _2 = (int)SEQ_PTR(_49routine_map_49119);
        _the_map_49269 = (int)*(((s1_ptr)_2)->base + _fx_49262);
        Ref(_the_map_49269);
        goto L6; // [89] 101
L5: 

        /** 			the_map = line_map[fx]*/
        DeRef(_the_map_49269);
        _2 = (int)SEQ_PTR(_49line_map_49118);
        _the_map_49269 = (int)*(((s1_ptr)_2)->base + _fx_49262);
        Ref(_the_map_49269);
L6: 

        /** 		for j = 1 to db_table_size() do*/
        RefDS(_48current_table_name_17845);
        _25946 = _48db_table_size(_48current_table_name_17845);
        {
            int _j_49278;
            _j_49278 = 1;
L7: 
            if (binary_op_a(GREATER, _j_49278, _25946)){
                goto L8; // [109] 152
            }

            /** 			map:put( the_map, db_record_key( j ), db_record_data( j ), map:ADD )*/
            Ref(_j_49278);
            RefDS(_48current_table_name_17845);
            _25947 = _48db_record_key(_j_49278, _48current_table_name_17845);
            Ref(_j_49278);
            RefDS(_48current_table_name_17845);
            _25948 = _48db_record_data(_j_49278, _48current_table_name_17845);
            Ref(_the_map_49269);
            _32put(_the_map_49269, _25947, _25948, 2, _32threshold_size_13318);
            _25947 = NOVALUE;
            _25948 = NOVALUE;

            /** 		end for*/
            _0 = _j_49278;
            if (IS_ATOM_INT(_j_49278)) {
                _j_49278 = _j_49278 + 1;
                if ((long)((unsigned long)_j_49278 +(unsigned long) HIGH_BITS) >= 0){
                    _j_49278 = NewDouble((double)_j_49278);
                }
            }
            else {
                _j_49278 = binary_op_a(PLUS, _j_49278, 1);
            }
            DeRef(_0);
            goto L7; // [147] 116
L8: 
            ;
            DeRef(_j_49278);
        }
        DeRef(_name_49258);
        _name_49258 = NOVALUE;
        DeRef(_the_map_49269);
        _the_map_49269 = NOVALUE;

        /** 	end for*/
L4: 
        _i_49256 = _i_49256 + 1;
        goto L1; // [156] 20
L2: 
        ;
    }

    /** end procedure*/
    DeRef(_tables_49252);
    DeRef(_25946);
    _25946 = NOVALUE;
    return;
    ;
}


void _49coverage_db(int _name_49287)
{
    int _0, _1, _2;
    

    /** 	coverage_db_name = name*/
    RefDS(_name_49287);
    DeRef(_49coverage_db_name_49115);
    _49coverage_db_name_49115 = _name_49287;

    /** end procedure*/
    DeRefDS(_name_49287);
    return;
    ;
}


int _49coverage_on()
{
    int _25949 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return file_coverage[current_file_no]*/
    _2 = (int)SEQ_PTR(_49file_coverage_49114);
    _25949 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    return _25949;
    ;
}


void _49new_covered_path(int _name_49299)
{
    int _25955 = NOVALUE;
    int _25953 = NOVALUE;
    int _0, _1, _2;
    

    /** 	covered_files = append( covered_files, name )*/
    RefDS(_name_49299);
    Append(&_49covered_files_49113, _49covered_files_49113, _name_49299);

    /** 	routine_map &= map:new()*/
    _25953 = _32new(690);
    if (IS_SEQUENCE(_49routine_map_49119) && IS_ATOM(_25953)) {
        Ref(_25953);
        Append(&_49routine_map_49119, _49routine_map_49119, _25953);
    }
    else if (IS_ATOM(_49routine_map_49119) && IS_SEQUENCE(_25953)) {
    }
    else {
        Concat((object_ptr)&_49routine_map_49119, _49routine_map_49119, _25953);
    }
    DeRef(_25953);
    _25953 = NOVALUE;

    /** 	line_map    &= map:new()*/
    _25955 = _32new(690);
    if (IS_SEQUENCE(_49line_map_49118) && IS_ATOM(_25955)) {
        Ref(_25955);
        Append(&_49line_map_49118, _49line_map_49118, _25955);
    }
    else if (IS_ATOM(_49line_map_49118) && IS_SEQUENCE(_25955)) {
    }
    else {
        Concat((object_ptr)&_49line_map_49118, _49line_map_49118, _25955);
    }
    DeRef(_25955);
    _25955 = NOVALUE;

    /** end procedure*/
    DeRefDS(_name_49299);
    return;
    ;
}


void _49add_coverage(int _cover_this_49307)
{
    int _path_49308 = NOVALUE;
    int _files_49317 = NOVALUE;
    int _subpath_49345 = NOVALUE;
    int _25990 = NOVALUE;
    int _25989 = NOVALUE;
    int _25988 = NOVALUE;
    int _25987 = NOVALUE;
    int _25986 = NOVALUE;
    int _25985 = NOVALUE;
    int _25984 = NOVALUE;
    int _25983 = NOVALUE;
    int _25982 = NOVALUE;
    int _25981 = NOVALUE;
    int _25980 = NOVALUE;
    int _25979 = NOVALUE;
    int _25977 = NOVALUE;
    int _25976 = NOVALUE;
    int _25975 = NOVALUE;
    int _25974 = NOVALUE;
    int _25973 = NOVALUE;
    int _25972 = NOVALUE;
    int _25971 = NOVALUE;
    int _25970 = NOVALUE;
    int _25968 = NOVALUE;
    int _25967 = NOVALUE;
    int _25966 = NOVALUE;
    int _25965 = NOVALUE;
    int _25964 = NOVALUE;
    int _25963 = NOVALUE;
    int _25962 = NOVALUE;
    int _25961 = NOVALUE;
    int _25958 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence path = canonical_path( cover_this,, CORRECT )*/
    RefDS(_cover_this_49307);
    _0 = _path_49308;
    _path_49308 = _9canonical_path(_cover_this_49307, 0, 2);
    DeRef(_0);

    /** 	if file_type( path ) = FILETYPE_DIRECTORY then*/
    RefDS(_path_49308);
    _25958 = _9file_type(_path_49308);
    if (binary_op_a(NOTEQ, _25958, 2)){
        DeRef(_25958);
        _25958 = NOVALUE;
        goto L1; // [23] 211
    }
    DeRef(_25958);
    _25958 = NOVALUE;

    /** 		sequence files = dir( path  )*/
    RefDS(_path_49308);
    _0 = _files_49317;
    _files_49317 = _9dir(_path_49308);
    DeRef(_0);

    /** 		for i = 1 to length( files ) do*/
    if (IS_SEQUENCE(_files_49317)){
            _25961 = SEQ_PTR(_files_49317)->length;
    }
    else {
        _25961 = 1;
    }
    {
        int _i_49321;
        _i_49321 = 1;
L2: 
        if (_i_49321 > _25961){
            goto L3; // [40] 206
        }

        /** 			if find( 'd', files[i][D_ATTRIBUTES] ) then*/
        _2 = (int)SEQ_PTR(_files_49317);
        _25962 = (int)*(((s1_ptr)_2)->base + _i_49321);
        _2 = (int)SEQ_PTR(_25962);
        _25963 = (int)*(((s1_ptr)_2)->base + 2);
        _25962 = NOVALUE;
        _25964 = find_from(100, _25963, 1);
        _25963 = NOVALUE;
        if (_25964 == 0)
        {
            _25964 = NOVALUE;
            goto L4; // [64] 118
        }
        else{
            _25964 = NOVALUE;
        }

        /** 				if not eu:find(files[i][D_NAME], {".", ".."}) then*/
        _2 = (int)SEQ_PTR(_files_49317);
        _25965 = (int)*(((s1_ptr)_2)->base + _i_49321);
        _2 = (int)SEQ_PTR(_25965);
        _25966 = (int)*(((s1_ptr)_2)->base + 1);
        _25965 = NOVALUE;
        RefDS(_23766);
        RefDS(_23767);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _23767;
        ((int *)_2)[2] = _23766;
        _25967 = MAKE_SEQ(_1);
        _25968 = find_from(_25966, _25967, 1);
        _25966 = NOVALUE;
        DeRefDS(_25967);
        _25967 = NOVALUE;
        if (_25968 != 0)
        goto L5; // [88] 199
        _25968 = NOVALUE;

        /** 					add_coverage( cover_this & SLASH & files[i][D_NAME] )*/
        _2 = (int)SEQ_PTR(_files_49317);
        _25970 = (int)*(((s1_ptr)_2)->base + _i_49321);
        _2 = (int)SEQ_PTR(_25970);
        _25971 = (int)*(((s1_ptr)_2)->base + 1);
        _25970 = NOVALUE;
        {
            int concat_list[3];

            concat_list[0] = _25971;
            concat_list[1] = 92;
            concat_list[2] = _cover_this_49307;
            Concat_N((object_ptr)&_25972, concat_list, 3);
        }
        _25971 = NOVALUE;
        _49add_coverage(_25972);
        _25972 = NOVALUE;
        goto L5; // [115] 199
L4: 

        /** 			elsif regex:has_match( eu_file, files[i][D_NAME] ) then*/
        _2 = (int)SEQ_PTR(_files_49317);
        _25973 = (int)*(((s1_ptr)_2)->base + _i_49321);
        _2 = (int)SEQ_PTR(_25973);
        _25974 = (int)*(((s1_ptr)_2)->base + 1);
        _25973 = NOVALUE;
        Ref(_49eu_file_49293);
        Ref(_25974);
        _25975 = _50has_match(_49eu_file_49293, _25974, 1, 0);
        _25974 = NOVALUE;
        if (_25975 == 0) {
            DeRef(_25975);
            _25975 = NOVALUE;
            goto L6; // [139] 196
        }
        else {
            if (!IS_ATOM_INT(_25975) && DBL_PTR(_25975)->dbl == 0.0){
                DeRef(_25975);
                _25975 = NOVALUE;
                goto L6; // [139] 196
            }
            DeRef(_25975);
            _25975 = NOVALUE;
        }
        DeRef(_25975);
        _25975 = NOVALUE;

        /** 				sequence subpath = path & SLASH & files[i][D_NAME]*/
        _2 = (int)SEQ_PTR(_files_49317);
        _25976 = (int)*(((s1_ptr)_2)->base + _i_49321);
        _2 = (int)SEQ_PTR(_25976);
        _25977 = (int)*(((s1_ptr)_2)->base + 1);
        _25976 = NOVALUE;
        {
            int concat_list[3];

            concat_list[0] = _25977;
            concat_list[1] = 92;
            concat_list[2] = _path_49308;
            Concat_N((object_ptr)&_subpath_49345, concat_list, 3);
        }
        _25977 = NOVALUE;

        /** 				if not find( subpath, covered_files ) and not excluded( subpath ) then*/
        _25979 = find_from(_subpath_49345, _49covered_files_49113, 1);
        _25980 = (_25979 == 0);
        _25979 = NOVALUE;
        if (_25980 == 0) {
            goto L7; // [174] 195
        }
        RefDS(_subpath_49345);
        _25982 = _49excluded(_subpath_49345);
        if (IS_ATOM_INT(_25982)) {
            _25983 = (_25982 == 0);
        }
        else {
            _25983 = unary_op(NOT, _25982);
        }
        DeRef(_25982);
        _25982 = NOVALUE;
        if (_25983 == 0) {
            DeRef(_25983);
            _25983 = NOVALUE;
            goto L7; // [186] 195
        }
        else {
            if (!IS_ATOM_INT(_25983) && DBL_PTR(_25983)->dbl == 0.0){
                DeRef(_25983);
                _25983 = NOVALUE;
                goto L7; // [186] 195
            }
            DeRef(_25983);
            _25983 = NOVALUE;
        }
        DeRef(_25983);
        _25983 = NOVALUE;

        /** 					new_covered_path( subpath )*/
        RefDS(_subpath_49345);
        _49new_covered_path(_subpath_49345);
L7: 
L6: 
        DeRef(_subpath_49345);
        _subpath_49345 = NOVALUE;
L5: 

        /** 		end for*/
        _i_49321 = _i_49321 + 1;
        goto L2; // [201] 47
L3: 
        ;
    }
    DeRef(_files_49317);
    _files_49317 = NOVALUE;
    goto L8; // [208] 262
L1: 

    /** 	elsif regex:has_match( eu_file, path ) and*/
    Ref(_49eu_file_49293);
    RefDS(_path_49308);
    _25984 = _50has_match(_49eu_file_49293, _path_49308, 1, 0);
    if (IS_ATOM_INT(_25984)) {
        if (_25984 == 0) {
            DeRef(_25985);
            _25985 = 0;
            goto L9; // [222] 240
        }
    }
    else {
        if (DBL_PTR(_25984)->dbl == 0.0) {
            DeRef(_25985);
            _25985 = 0;
            goto L9; // [222] 240
        }
    }
    _25986 = find_from(_path_49308, _49covered_files_49113, 1);
    _25987 = (_25986 == 0);
    _25986 = NOVALUE;
    DeRef(_25985);
    _25985 = (_25987 != 0);
L9: 
    if (_25985 == 0) {
        goto LA; // [240] 261
    }
    RefDS(_path_49308);
    _25989 = _49excluded(_path_49308);
    if (IS_ATOM_INT(_25989)) {
        _25990 = (_25989 == 0);
    }
    else {
        _25990 = unary_op(NOT, _25989);
    }
    DeRef(_25989);
    _25989 = NOVALUE;
    if (_25990 == 0) {
        DeRef(_25990);
        _25990 = NOVALUE;
        goto LA; // [252] 261
    }
    else {
        if (!IS_ATOM_INT(_25990) && DBL_PTR(_25990)->dbl == 0.0){
            DeRef(_25990);
            _25990 = NOVALUE;
            goto LA; // [252] 261
        }
        DeRef(_25990);
        _25990 = NOVALUE;
    }
    DeRef(_25990);
    _25990 = NOVALUE;

    /** 		new_covered_path( path )*/
    RefDS(_path_49308);
    _49new_covered_path(_path_49308);
LA: 
L8: 

    /** end procedure*/
    DeRefDS(_cover_this_49307);
    DeRef(_path_49308);
    DeRef(_25980);
    _25980 = NOVALUE;
    DeRef(_25984);
    _25984 = NOVALUE;
    DeRef(_25987);
    _25987 = NOVALUE;
    return;
    ;
}


int _49excluded(int _file_49369)
{
    int _25993 = NOVALUE;
    int _25992 = NOVALUE;
    int _25991 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length( exclusion_patterns ) do*/
    if (IS_SEQUENCE(_49exclusion_patterns_49117)){
            _25991 = SEQ_PTR(_49exclusion_patterns_49117)->length;
    }
    else {
        _25991 = 1;
    }
    {
        int _i_49371;
        _i_49371 = 1;
L1: 
        if (_i_49371 > _25991){
            goto L2; // [10] 49
        }

        /** 		if regex:has_match( exclusion_patterns[i], file ) then*/
        _2 = (int)SEQ_PTR(_49exclusion_patterns_49117);
        _25992 = (int)*(((s1_ptr)_2)->base + _i_49371);
        Ref(_25992);
        RefDS(_file_49369);
        _25993 = _50has_match(_25992, _file_49369, 1, 0);
        _25992 = NOVALUE;
        if (_25993 == 0) {
            DeRef(_25993);
            _25993 = NOVALUE;
            goto L3; // [32] 42
        }
        else {
            if (!IS_ATOM_INT(_25993) && DBL_PTR(_25993)->dbl == 0.0){
                DeRef(_25993);
                _25993 = NOVALUE;
                goto L3; // [32] 42
            }
            DeRef(_25993);
            _25993 = NOVALUE;
        }
        DeRef(_25993);
        _25993 = NOVALUE;

        /** 			return 1*/
        DeRefDS(_file_49369);
        return 1;
L3: 

        /** 	end for*/
        _i_49371 = _i_49371 + 1;
        goto L1; // [44] 17
L2: 
        ;
    }

    /** 	return 0*/
    DeRefDS(_file_49369);
    return 0;
    ;
}


void _49coverage_exclude(int _patterns_49378)
{
    int _ex_49383 = NOVALUE;
    int _fx_49390 = NOVALUE;
    int _26011 = NOVALUE;
    int _26010 = NOVALUE;
    int _26009 = NOVALUE;
    int _26008 = NOVALUE;
    int _26002 = NOVALUE;
    int _26001 = NOVALUE;
    int _25999 = NOVALUE;
    int _25997 = NOVALUE;
    int _25995 = NOVALUE;
    int _25994 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length( patterns ) do*/
    if (IS_SEQUENCE(_patterns_49378)){
            _25994 = SEQ_PTR(_patterns_49378)->length;
    }
    else {
        _25994 = 1;
    }
    {
        int _i_49380;
        _i_49380 = 1;
L1: 
        if (_i_49380 > _25994){
            goto L2; // [8] 161
        }

        /** 		regex ex = regex:new( patterns[i] )*/
        _2 = (int)SEQ_PTR(_patterns_49378);
        _25995 = (int)*(((s1_ptr)_2)->base + _i_49380);
        Ref(_25995);
        _0 = _ex_49383;
        _ex_49383 = _50new(_25995, 0);
        DeRef(_0);
        _25995 = NOVALUE;

        /** 		if regex( ex ) then*/
        Ref(_ex_49383);
        _25997 = _50regex(_ex_49383);
        if (_25997 == 0) {
            DeRef(_25997);
            _25997 = NOVALUE;
            goto L3; // [32] 127
        }
        else {
            if (!IS_ATOM_INT(_25997) && DBL_PTR(_25997)->dbl == 0.0){
                DeRef(_25997);
                _25997 = NOVALUE;
                goto L3; // [32] 127
            }
            DeRef(_25997);
            _25997 = NOVALUE;
        }
        DeRef(_25997);
        _25997 = NOVALUE;

        /** 			exclusion_patterns = append( exclusion_patterns, ex )*/
        Ref(_ex_49383);
        Append(&_49exclusion_patterns_49117, _49exclusion_patterns_49117, _ex_49383);

        /** 			integer fx = 1*/
        _fx_49390 = 1;

        /** 			while fx <= length( covered_files ) do*/
L4: 
        if (IS_SEQUENCE(_49covered_files_49113)){
                _25999 = SEQ_PTR(_49covered_files_49113)->length;
        }
        else {
            _25999 = 1;
        }
        if (_fx_49390 > _25999)
        goto L5; // [58] 122

        /** 				if regex:has_match( ex, covered_files[fx] ) then*/
        _2 = (int)SEQ_PTR(_49covered_files_49113);
        _26001 = (int)*(((s1_ptr)_2)->base + _fx_49390);
        Ref(_ex_49383);
        Ref(_26001);
        _26002 = _50has_match(_ex_49383, _26001, 1, 0);
        _26001 = NOVALUE;
        if (_26002 == 0) {
            DeRef(_26002);
            _26002 = NOVALUE;
            goto L6; // [77] 110
        }
        else {
            if (!IS_ATOM_INT(_26002) && DBL_PTR(_26002)->dbl == 0.0){
                DeRef(_26002);
                _26002 = NOVALUE;
                goto L6; // [77] 110
            }
            DeRef(_26002);
            _26002 = NOVALUE;
        }
        DeRef(_26002);
        _26002 = NOVALUE;

        /** 					covered_files = remove( covered_files, fx )*/
        {
            s1_ptr assign_space = SEQ_PTR(_49covered_files_49113);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_fx_49390)) ? _fx_49390 : (long)(DBL_PTR(_fx_49390)->dbl);
            int stop = (IS_ATOM_INT(_fx_49390)) ? _fx_49390 : (long)(DBL_PTR(_fx_49390)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_49covered_files_49113), start, &_49covered_files_49113 );
                }
                else Tail(SEQ_PTR(_49covered_files_49113), stop+1, &_49covered_files_49113);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_49covered_files_49113), start, &_49covered_files_49113);
            }
            else {
                assign_slice_seq = &assign_space;
                _49covered_files_49113 = Remove_elements(start, stop, (SEQ_PTR(_49covered_files_49113)->ref == 1));
            }
        }

        /** 					routine_map   = remove( routine_map, fx )*/
        {
            s1_ptr assign_space = SEQ_PTR(_49routine_map_49119);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_fx_49390)) ? _fx_49390 : (long)(DBL_PTR(_fx_49390)->dbl);
            int stop = (IS_ATOM_INT(_fx_49390)) ? _fx_49390 : (long)(DBL_PTR(_fx_49390)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_49routine_map_49119), start, &_49routine_map_49119 );
                }
                else Tail(SEQ_PTR(_49routine_map_49119), stop+1, &_49routine_map_49119);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_49routine_map_49119), start, &_49routine_map_49119);
            }
            else {
                assign_slice_seq = &assign_space;
                _49routine_map_49119 = Remove_elements(start, stop, (SEQ_PTR(_49routine_map_49119)->ref == 1));
            }
        }

        /** 					line_map      = remove( line_map, fx )*/
        {
            s1_ptr assign_space = SEQ_PTR(_49line_map_49118);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_fx_49390)) ? _fx_49390 : (long)(DBL_PTR(_fx_49390)->dbl);
            int stop = (IS_ATOM_INT(_fx_49390)) ? _fx_49390 : (long)(DBL_PTR(_fx_49390)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_49line_map_49118), start, &_49line_map_49118 );
                }
                else Tail(SEQ_PTR(_49line_map_49118), stop+1, &_49line_map_49118);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_49line_map_49118), start, &_49line_map_49118);
            }
            else {
                assign_slice_seq = &assign_space;
                _49line_map_49118 = Remove_elements(start, stop, (SEQ_PTR(_49line_map_49118)->ref == 1));
            }
        }
        goto L4; // [107] 53
L6: 

        /** 					fx += 1*/
        _fx_49390 = _fx_49390 + 1;

        /** 			end while*/
        goto L4; // [119] 53
L5: 
        goto L7; // [124] 152
L3: 

        /** 			printf( 2,"%s\n", { GetMsgText( 339, 1, {patterns[i]}) } )*/
        _2 = (int)SEQ_PTR(_patterns_49378);
        _26008 = (int)*(((s1_ptr)_2)->base + _i_49380);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_26008);
        *((int *)(_2+4)) = _26008;
        _26009 = MAKE_SEQ(_1);
        _26008 = NOVALUE;
        _26010 = _44GetMsgText(339, 1, _26009);
        _26009 = NOVALUE;
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _26010;
        _26011 = MAKE_SEQ(_1);
        _26010 = NOVALUE;
        EPrintf(2, _26007, _26011);
        DeRefDS(_26011);
        _26011 = NOVALUE;
L7: 
        DeRef(_ex_49383);
        _ex_49383 = NOVALUE;

        /** 	end for*/
        _i_49380 = _i_49380 + 1;
        goto L1; // [156] 15
L2: 
        ;
    }

    /** end procedure*/
    DeRefDS(_patterns_49378);
    return;
    ;
}


void _49new_coverage_db()
{
    int _0, _1, _2;
    

    /** 	coverage_erase = 1*/
    _49coverage_erase_49116 = 1;

    /** end procedure*/
    return;
    ;
}


void _49include_line(int _line_number_49413)
{
    int _26012 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_line_number_49413)) {
        _1 = (long)(DBL_PTR(_line_number_49413)->dbl);
        if (UNIQUE(DBL_PTR(_line_number_49413)) && (DBL_PTR(_line_number_49413)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_line_number_49413);
        _line_number_49413 = _1;
    }

    /** 	if coverage_on() then*/
    _26012 = _49coverage_on();
    if (_26012 == 0) {
        DeRef(_26012);
        _26012 = NOVALUE;
        goto L1; // [8] 34
    }
    else {
        if (!IS_ATOM_INT(_26012) && DBL_PTR(_26012)->dbl == 0.0){
            DeRef(_26012);
            _26012 = NOVALUE;
            goto L1; // [8] 34
        }
        DeRef(_26012);
        _26012 = NOVALUE;
    }
    DeRef(_26012);
    _26012 = NOVALUE;

    /** 		emit_op( COVERAGE_LINE )*/
    _37emit_op(210);

    /** 		emit_addr( gline_number )*/
    _37emit_addr(_25gline_number_12267);

    /** 		included_lines &= line_number*/
    Append(&_49included_lines_49120, _49included_lines_49120, _line_number_49413);
L1: 

    /** end procedure*/
    return;
    ;
}


void _49include_routine()
{
    int _file_no_49429 = NOVALUE;
    int _26019 = NOVALUE;
    int _26018 = NOVALUE;
    int _26017 = NOVALUE;
    int _26015 = NOVALUE;
    int _26014 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if coverage_on() then*/
    _26014 = _49coverage_on();
    if (_26014 == 0) {
        DeRef(_26014);
        _26014 = NOVALUE;
        goto L1; // [6] 73
    }
    else {
        if (!IS_ATOM_INT(_26014) && DBL_PTR(_26014)->dbl == 0.0){
            DeRef(_26014);
            _26014 = NOVALUE;
            goto L1; // [6] 73
        }
        DeRef(_26014);
        _26014 = NOVALUE;
    }
    DeRef(_26014);
    _26014 = NOVALUE;

    /** 		emit_op( COVERAGE_ROUTINE )*/
    _37emit_op(211);

    /** 		emit_addr( CurrentSub )*/
    _37emit_addr(_25CurrentSub_12270);

    /** 		integer file_no = SymTab[CurrentSub][S_FILE_NO]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _26015 = (int)*(((s1_ptr)_2)->base + _25CurrentSub_12270);
    _2 = (int)SEQ_PTR(_26015);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _file_no_49429 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _file_no_49429 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    if (!IS_ATOM_INT(_file_no_49429)){
        _file_no_49429 = (long)DBL_PTR(_file_no_49429)->dbl;
    }
    _26015 = NOVALUE;

    /** 		map:put( routine_map[file_coverage[file_no]], sym_name( CurrentSub ), 0, map:ADD )*/
    _2 = (int)SEQ_PTR(_49file_coverage_49114);
    _26017 = (int)*(((s1_ptr)_2)->base + _file_no_49429);
    _2 = (int)SEQ_PTR(_49routine_map_49119);
    _26018 = (int)*(((s1_ptr)_2)->base + _26017);
    _26019 = _52sym_name(_25CurrentSub_12270);
    Ref(_26018);
    _32put(_26018, _26019, 0, 2, _32threshold_size_13318);
    _26018 = NOVALUE;
    _26019 = NOVALUE;
L1: 

    /** end procedure*/
    _26017 = NOVALUE;
    return;
    ;
}


void _49process_lines()
{
    int _sline_49457 = NOVALUE;
    int _file_49461 = NOVALUE;
    int _line_49471 = NOVALUE;
    int _26037 = NOVALUE;
    int _26035 = NOVALUE;
    int _26034 = NOVALUE;
    int _26033 = NOVALUE;
    int _26032 = NOVALUE;
    int _26031 = NOVALUE;
    int _26029 = NOVALUE;
    int _26027 = NOVALUE;
    int _26026 = NOVALUE;
    int _26024 = NOVALUE;
    int _26023 = NOVALUE;
    int _26022 = NOVALUE;
    int _26020 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length( included_lines ) then*/
    if (IS_SEQUENCE(_49included_lines_49120)){
            _26020 = SEQ_PTR(_49included_lines_49120)->length;
    }
    else {
        _26020 = 1;
    }
    if (_26020 != 0)
    goto L1; // [8] 17
    _26020 = NOVALUE;

    /** 		return*/
    return;
L1: 

    /** 	if atom(slist[$]) then*/
    if (IS_SEQUENCE(_25slist_12357)){
            _26022 = SEQ_PTR(_25slist_12357)->length;
    }
    else {
        _26022 = 1;
    }
    _2 = (int)SEQ_PTR(_25slist_12357);
    _26023 = (int)*(((s1_ptr)_2)->base + _26022);
    _26024 = IS_ATOM(_26023);
    _26023 = NOVALUE;
    if (_26024 == 0)
    {
        _26024 = NOVALUE;
        goto L2; // [31] 45
    }
    else{
        _26024 = NOVALUE;
    }

    /** 		slist = s_expand( slist )*/
    RefDS(_25slist_12357);
    _0 = _60s_expand(_25slist_12357);
    DeRefDS(_25slist_12357);
    _25slist_12357 = _0;
L2: 

    /** 	for i = 1 to length( included_lines ) do*/
    if (IS_SEQUENCE(_49included_lines_49120)){
            _26026 = SEQ_PTR(_49included_lines_49120)->length;
    }
    else {
        _26026 = 1;
    }
    {
        int _i_49455;
        _i_49455 = 1;
L3: 
        if (_i_49455 > _26026){
            goto L4; // [52] 161
        }

        /** 		sequence sline = slist[included_lines[i]]*/
        _2 = (int)SEQ_PTR(_49included_lines_49120);
        _26027 = (int)*(((s1_ptr)_2)->base + _i_49455);
        DeRef(_sline_49457);
        _2 = (int)SEQ_PTR(_25slist_12357);
        _sline_49457 = (int)*(((s1_ptr)_2)->base + _26027);
        Ref(_sline_49457);

        /** 		integer file = file_coverage[sline[LOCAL_FILE_NO]]*/
        _2 = (int)SEQ_PTR(_sline_49457);
        _26029 = (int)*(((s1_ptr)_2)->base + 3);
        _2 = (int)SEQ_PTR(_49file_coverage_49114);
        if (!IS_ATOM_INT(_26029)){
            _file_49461 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_26029)->dbl));
        }
        else{
            _file_49461 = (int)*(((s1_ptr)_2)->base + _26029);
        }

        /** 		if file and file <= length( line_map ) and line_map[file] then*/
        if (_file_49461 == 0) {
            _26031 = 0;
            goto L5; // [91] 108
        }
        if (IS_SEQUENCE(_49line_map_49118)){
                _26032 = SEQ_PTR(_49line_map_49118)->length;
        }
        else {
            _26032 = 1;
        }
        _26033 = (_file_49461 <= _26032);
        _26032 = NOVALUE;
        _26031 = (_26033 != 0);
L5: 
        if (_26031 == 0) {
            goto L6; // [108] 150
        }
        _2 = (int)SEQ_PTR(_49line_map_49118);
        _26035 = (int)*(((s1_ptr)_2)->base + _file_49461);
        if (_26035 == 0) {
            _26035 = NOVALUE;
            goto L6; // [119] 150
        }
        else {
            if (!IS_ATOM_INT(_26035) && DBL_PTR(_26035)->dbl == 0.0){
                _26035 = NOVALUE;
                goto L6; // [119] 150
            }
            _26035 = NOVALUE;
        }
        _26035 = NOVALUE;

        /** 			integer line = sline[LINE]*/
        _2 = (int)SEQ_PTR(_sline_49457);
        _line_49471 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_line_49471))
        _line_49471 = (long)DBL_PTR(_line_49471)->dbl;

        /** 			map:put( line_map[file], line, 0, map:ADD )*/
        _2 = (int)SEQ_PTR(_49line_map_49118);
        _26037 = (int)*(((s1_ptr)_2)->base + _file_49461);
        Ref(_26037);
        _32put(_26037, _line_49471, 0, 2, _32threshold_size_13318);
        _26037 = NOVALUE;
L6: 
        DeRef(_sline_49457);
        _sline_49457 = NOVALUE;

        /** 	end for*/
        _i_49455 = _i_49455 + 1;
        goto L3; // [156] 59
L4: 
        ;
    }

    /** end procedure*/
    _26027 = NOVALUE;
    _26029 = NOVALUE;
    DeRef(_26033);
    _26033 = NOVALUE;
    return;
    ;
}


void _49cover_line(int _gline_number_49477)
{
    int _sline_49487 = NOVALUE;
    int _file_49490 = NOVALUE;
    int _line_49495 = NOVALUE;
    int _26046 = NOVALUE;
    int _26043 = NOVALUE;
    int _26040 = NOVALUE;
    int _26039 = NOVALUE;
    int _26038 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_gline_number_49477)) {
        _1 = (long)(DBL_PTR(_gline_number_49477)->dbl);
        if (UNIQUE(DBL_PTR(_gline_number_49477)) && (DBL_PTR(_gline_number_49477)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_gline_number_49477);
        _gline_number_49477 = _1;
    }

    /** 	if atom(slist[$]) then*/
    if (IS_SEQUENCE(_25slist_12357)){
            _26038 = SEQ_PTR(_25slist_12357)->length;
    }
    else {
        _26038 = 1;
    }
    _2 = (int)SEQ_PTR(_25slist_12357);
    _26039 = (int)*(((s1_ptr)_2)->base + _26038);
    _26040 = IS_ATOM(_26039);
    _26039 = NOVALUE;
    if (_26040 == 0)
    {
        _26040 = NOVALUE;
        goto L1; // [17] 31
    }
    else{
        _26040 = NOVALUE;
    }

    /** 		slist = s_expand(slist)*/
    RefDS(_25slist_12357);
    _0 = _60s_expand(_25slist_12357);
    DeRefDS(_25slist_12357);
    _25slist_12357 = _0;
L1: 

    /** 	sequence sline = slist[gline_number]*/
    DeRef(_sline_49487);
    _2 = (int)SEQ_PTR(_25slist_12357);
    _sline_49487 = (int)*(((s1_ptr)_2)->base + _gline_number_49477);
    Ref(_sline_49487);

    /** 	integer file = file_coverage[sline[LOCAL_FILE_NO]]*/
    _2 = (int)SEQ_PTR(_sline_49487);
    _26043 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_49file_coverage_49114);
    if (!IS_ATOM_INT(_26043)){
        _file_49490 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_26043)->dbl));
    }
    else{
        _file_49490 = (int)*(((s1_ptr)_2)->base + _26043);
    }

    /** 	if file then*/
    if (_file_49490 == 0)
    {
        goto L2; // [57] 88
    }
    else{
    }

    /** 		integer line = sline[LINE]*/
    _2 = (int)SEQ_PTR(_sline_49487);
    _line_49495 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_line_49495))
    _line_49495 = (long)DBL_PTR(_line_49495)->dbl;

    /** 		map:put( line_map[file], line, 1, map:ADD )*/
    _2 = (int)SEQ_PTR(_49line_map_49118);
    _26046 = (int)*(((s1_ptr)_2)->base + _file_49490);
    Ref(_26046);
    _32put(_26046, _line_49495, 1, 2, _32threshold_size_13318);
    _26046 = NOVALUE;
L2: 

    /** end procedure*/
    DeRef(_sline_49487);
    _26043 = NOVALUE;
    return;
    ;
}


void _49cover_routine(int _sub_49502)
{
    int _file_no_49503 = NOVALUE;
    int _26051 = NOVALUE;
    int _26050 = NOVALUE;
    int _26049 = NOVALUE;
    int _26047 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sub_49502)) {
        _1 = (long)(DBL_PTR(_sub_49502)->dbl);
        if (UNIQUE(DBL_PTR(_sub_49502)) && (DBL_PTR(_sub_49502)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sub_49502);
        _sub_49502 = _1;
    }

    /** 	integer file_no = SymTab[sub][S_FILE_NO]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _26047 = (int)*(((s1_ptr)_2)->base + _sub_49502);
    _2 = (int)SEQ_PTR(_26047);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _file_no_49503 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _file_no_49503 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    if (!IS_ATOM_INT(_file_no_49503)){
        _file_no_49503 = (long)DBL_PTR(_file_no_49503)->dbl;
    }
    _26047 = NOVALUE;

    /** 	map:put( routine_map[file_coverage[file_no]], sym_name( sub ), 1, map:ADD )*/
    _2 = (int)SEQ_PTR(_49file_coverage_49114);
    _26049 = (int)*(((s1_ptr)_2)->base + _file_no_49503);
    _2 = (int)SEQ_PTR(_49routine_map_49119);
    _26050 = (int)*(((s1_ptr)_2)->base + _26049);
    _26051 = _52sym_name(_sub_49502);
    Ref(_26050);
    _32put(_26050, _26051, 1, 2, _32threshold_size_13318);
    _26050 = NOVALUE;
    _26051 = NOVALUE;

    /** end procedure*/
    _26049 = NOVALUE;
    return;
    ;
}


int _49has_coverage()
{
    int _26052 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return length( covered_files )*/
    if (IS_SEQUENCE(_49covered_files_49113)){
            _26052 = SEQ_PTR(_49covered_files_49113)->length;
    }
    else {
        _26052 = 1;
    }
    return _26052;
    ;
}



// 0xF997E574
