// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _53c_putc(int _c_46501)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_c_46501)) {
        _1 = (long)(DBL_PTR(_c_46501)->dbl);
        if (UNIQUE(DBL_PTR(_c_46501)) && (DBL_PTR(_c_46501)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_46501);
        _c_46501 = _1;
    }

    /** 	if emit_c_output then*/
    if (_53emit_c_output_46488 == 0)
    {
        goto L1; // [7] 23
    }
    else{
    }

    /** 		puts(c_code, c)*/
    EPuts(_53c_code_46491, _c_46501); // DJP 

    /** 		update_checksum( c )*/
    _54update_checksum(_c_46501);
L1: 

    /** end procedure*/
    return;
    ;
}


void _53c_hputs(int _c_source_46506)
{
    int _0, _1, _2;
    

    /** 	if emit_c_output then*/
    if (_53emit_c_output_46488 == 0)
    {
        goto L1; // [7] 18
    }
    else{
    }

    /** 		puts(c_h, c_source)    */
    EPuts(_53c_h_46492, _c_source_46506); // DJP 
L1: 

    /** end procedure*/
    DeRefDS(_c_source_46506);
    return;
    ;
}


void _53c_puts(int _c_source_46510)
{
    int _0, _1, _2;
    

    /** 	if emit_c_output then*/
    if (_53emit_c_output_46488 == 0)
    {
        goto L1; // [7] 23
    }
    else{
    }

    /** 		puts(c_code, c_source)*/
    EPuts(_53c_code_46491, _c_source_46510); // DJP 

    /** 		update_checksum( c_source )*/
    RefDS(_c_source_46510);
    _54update_checksum(_c_source_46510);
L1: 

    /** end procedure*/
    DeRefDS(_c_source_46510);
    return;
    ;
}


void _53c_hprintf(int _format_46515, int _value_46516)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_value_46516)) {
        _1 = (long)(DBL_PTR(_value_46516)->dbl);
        if (UNIQUE(DBL_PTR(_value_46516)) && (DBL_PTR(_value_46516)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_value_46516);
        _value_46516 = _1;
    }

    /** 	if emit_c_output then*/
    if (_53emit_c_output_46488 == 0)
    {
        goto L1; // [9] 21
    }
    else{
    }

    /** 		printf(c_h, format, value)*/
    EPrintf(_53c_h_46492, _format_46515, _value_46516);
L1: 

    /** end procedure*/
    DeRefDS(_format_46515);
    return;
    ;
}


void _53c_printf(int _format_46520, int _value_46521)
{
    int _text_46523 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if emit_c_output then*/
    if (_53emit_c_output_46488 == 0)
    {
        goto L1; // [7] 29
    }
    else{
    }

    /** 		sequence text = sprintf( format, value )*/
    DeRefi(_text_46523);
    _text_46523 = EPrintf(-9999999, _format_46520, _value_46521);

    /** 		puts(c_code, text)*/
    EPuts(_53c_code_46491, _text_46523); // DJP 

    /** 		update_checksum( text )*/
    RefDS(_text_46523);
    _54update_checksum(_text_46523);
L1: 
    DeRefi(_text_46523);
    _text_46523 = NOVALUE;

    /** end procedure*/
    DeRefDS(_format_46520);
    DeRef(_value_46521);
    return;
    ;
}


void _53c_printf8(int _value_46534)
{
    int _buff_46535 = NOVALUE;
    int _neg_46536 = NOVALUE;
    int _p_46537 = NOVALUE;
    int _24792 = NOVALUE;
    int _24791 = NOVALUE;
    int _24790 = NOVALUE;
    int _24788 = NOVALUE;
    int _24787 = NOVALUE;
    int _24785 = NOVALUE;
    int _24784 = NOVALUE;
    int _24782 = NOVALUE;
    int _24781 = NOVALUE;
    int _24779 = NOVALUE;
    int _24777 = NOVALUE;
    int _24775 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if emit_c_output then*/
    if (_53emit_c_output_46488 == 0)
    {
        goto L1; // [5] 217
    }
    else{
    }

    /** 		neg = 0*/
    _neg_46536 = 0;

    /** 		buff = sprintf("%.16e", value)*/
    DeRef(_buff_46535);
    _buff_46535 = EPrintf(-9999999, _24773, _value_46534);

    /** 		if length(buff) < 10 then*/
    if (IS_SEQUENCE(_buff_46535)){
            _24775 = SEQ_PTR(_buff_46535)->length;
    }
    else {
        _24775 = 1;
    }
    if (_24775 >= 10)
    goto L2; // [24] 209

    /** 			p = 1*/
    _p_46537 = 1;

    /** 			while p <= length(buff) do*/
L3: 
    if (IS_SEQUENCE(_buff_46535)){
            _24777 = SEQ_PTR(_buff_46535)->length;
    }
    else {
        _24777 = 1;
    }
    if (_p_46537 > _24777)
    goto L4; // [41] 208

    /** 				if buff[p] = '-' then*/
    _2 = (int)SEQ_PTR(_buff_46535);
    _24779 = (int)*(((s1_ptr)_2)->base + _p_46537);
    if (binary_op_a(NOTEQ, _24779, 45)){
        _24779 = NOVALUE;
        goto L5; // [51] 63
    }
    _24779 = NOVALUE;

    /** 					neg = 1*/
    _neg_46536 = 1;
    goto L6; // [60] 197
L5: 

    /** 				elsif buff[p] = 'i' or buff[p] = 'I' then*/
    _2 = (int)SEQ_PTR(_buff_46535);
    _24781 = (int)*(((s1_ptr)_2)->base + _p_46537);
    if (IS_ATOM_INT(_24781)) {
        _24782 = (_24781 == 105);
    }
    else {
        _24782 = binary_op(EQUALS, _24781, 105);
    }
    _24781 = NOVALUE;
    if (IS_ATOM_INT(_24782)) {
        if (_24782 != 0) {
            goto L7; // [73] 90
        }
    }
    else {
        if (DBL_PTR(_24782)->dbl != 0.0) {
            goto L7; // [73] 90
        }
    }
    _2 = (int)SEQ_PTR(_buff_46535);
    _24784 = (int)*(((s1_ptr)_2)->base + _p_46537);
    if (IS_ATOM_INT(_24784)) {
        _24785 = (_24784 == 73);
    }
    else {
        _24785 = binary_op(EQUALS, _24784, 73);
    }
    _24784 = NOVALUE;
    if (_24785 == 0) {
        DeRef(_24785);
        _24785 = NOVALUE;
        goto L8; // [86] 114
    }
    else {
        if (!IS_ATOM_INT(_24785) && DBL_PTR(_24785)->dbl == 0.0){
            DeRef(_24785);
            _24785 = NOVALUE;
            goto L8; // [86] 114
        }
        DeRef(_24785);
        _24785 = NOVALUE;
    }
    DeRef(_24785);
    _24785 = NOVALUE;
L7: 

    /** 					buff = CREATE_INF*/
    RefDS(_53CREATE_INF_46526);
    DeRef(_buff_46535);
    _buff_46535 = _53CREATE_INF_46526;

    /** 					if neg then*/
    if (_neg_46536 == 0)
    {
        goto L4; // [97] 208
    }
    else{
    }

    /** 						buff = prepend(buff, '-')*/
    Prepend(&_buff_46535, _buff_46535, 45);

    /** 					exit*/
    goto L4; // [109] 208
    goto L6; // [111] 197
L8: 

    /** 				elsif buff[p] = 'n' or buff[p] = 'N' then*/
    _2 = (int)SEQ_PTR(_buff_46535);
    _24787 = (int)*(((s1_ptr)_2)->base + _p_46537);
    if (IS_ATOM_INT(_24787)) {
        _24788 = (_24787 == 110);
    }
    else {
        _24788 = binary_op(EQUALS, _24787, 110);
    }
    _24787 = NOVALUE;
    if (IS_ATOM_INT(_24788)) {
        if (_24788 != 0) {
            goto L9; // [124] 141
        }
    }
    else {
        if (DBL_PTR(_24788)->dbl != 0.0) {
            goto L9; // [124] 141
        }
    }
    _2 = (int)SEQ_PTR(_buff_46535);
    _24790 = (int)*(((s1_ptr)_2)->base + _p_46537);
    if (IS_ATOM_INT(_24790)) {
        _24791 = (_24790 == 78);
    }
    else {
        _24791 = binary_op(EQUALS, _24790, 78);
    }
    _24790 = NOVALUE;
    if (_24791 == 0) {
        DeRef(_24791);
        _24791 = NOVALUE;
        goto LA; // [137] 196
    }
    else {
        if (!IS_ATOM_INT(_24791) && DBL_PTR(_24791)->dbl == 0.0){
            DeRef(_24791);
            _24791 = NOVALUE;
            goto LA; // [137] 196
        }
        DeRef(_24791);
        _24791 = NOVALUE;
    }
    DeRef(_24791);
    _24791 = NOVALUE;
L9: 

    /** 					ifdef UNIX then*/

    /** 						if sequence(wat_path) then*/
    _24792 = IS_SEQUENCE(_25wat_path_12343);
    if (_24792 == 0)
    {
        _24792 = NOVALUE;
        goto LB; // [150] 173
    }
    else{
        _24792 = NOVALUE;
    }

    /** 							buff = CREATE_NAN2*/
    RefDS(_53CREATE_NAN2_46530);
    DeRef(_buff_46535);
    _buff_46535 = _53CREATE_NAN2_46530;

    /** 							if not neg then*/
    if (_neg_46536 != 0)
    goto L4; // [160] 208

    /** 								buff = prepend(buff, '-')*/
    Prepend(&_buff_46535, _buff_46535, 45);
    goto L4; // [170] 208
LB: 

    /** 							buff = CREATE_NAN1*/
    RefDS(_53CREATE_NAN1_46528);
    DeRef(_buff_46535);
    _buff_46535 = _53CREATE_NAN1_46528;

    /** 							if neg then*/
    if (_neg_46536 == 0)
    {
        goto L4; // [180] 208
    }
    else{
    }

    /** 								buff = prepend(buff, '-')*/
    Prepend(&_buff_46535, _buff_46535, 45);

    /** 						exit*/
    goto L4; // [193] 208
LA: 
L6: 

    /** 				p += 1*/
    _p_46537 = _p_46537 + 1;

    /** 			end while*/
    goto L3; // [205] 38
L4: 
L2: 

    /** 		puts(c_code, buff)*/
    EPuts(_53c_code_46491, _buff_46535); // DJP 
L1: 

    /** end procedure*/
    DeRef(_value_46534);
    DeRef(_buff_46535);
    DeRef(_24782);
    _24782 = NOVALUE;
    DeRef(_24788);
    _24788 = NOVALUE;
    return;
    ;
}


void _53adjust_indent_before(int _stmt_46580)
{
    int _i_46581 = NOVALUE;
    int _lb_46583 = NOVALUE;
    int _rb_46584 = NOVALUE;
    int _24809 = NOVALUE;
    int _24807 = NOVALUE;
    int _24805 = NOVALUE;
    int _24799 = NOVALUE;
    int _24798 = NOVALUE;
    int _0, _1, _2;
    

    /** 	lb = FALSE*/
    _lb_46583 = _5FALSE_242;

    /** 	rb = FALSE*/
    _rb_46584 = _5FALSE_242;

    /** 	for p = 1 to length(stmt) do*/
    if (IS_SEQUENCE(_stmt_46580)){
            _24798 = SEQ_PTR(_stmt_46580)->length;
    }
    else {
        _24798 = 1;
    }
    {
        int _p_46588;
        _p_46588 = 1;
L1: 
        if (_p_46588 > _24798){
            goto L2; // [22] 102
        }

        /** 		switch stmt[p] do*/
        _2 = (int)SEQ_PTR(_stmt_46580);
        _24799 = (int)*(((s1_ptr)_2)->base + _p_46588);
        if (IS_SEQUENCE(_24799) ){
            goto L3; // [35] 95
        }
        if(!IS_ATOM_INT(_24799)){
            if( (DBL_PTR(_24799)->dbl != (double) ((int) DBL_PTR(_24799)->dbl) ) ){
                goto L3; // [35] 95
            }
            _0 = (int) DBL_PTR(_24799)->dbl;
        }
        else {
            _0 = _24799;
        };
        _24799 = NOVALUE;
        switch ( _0 ){ 

            /** 			case '\n' then*/
            case 10:

            /** 				exit*/
            goto L2; // [46] 102
            goto L3; // [48] 95

            /** 			case  '}' then*/
            case 125:

            /** 				rb = TRUE*/
            _rb_46584 = _5TRUE_244;

            /** 				if lb then*/
            if (_lb_46583 == 0)
            {
                goto L3; // [63] 95
            }
            else{
            }

            /** 					exit*/
            goto L2; // [68] 102
            goto L3; // [71] 95

            /** 			case '{' then*/
            case 123:

            /** 				lb = TRUE*/
            _lb_46583 = _5TRUE_244;

            /** 				if rb then */
            if (_rb_46584 == 0)
            {
                goto L4; // [86] 94
            }
            else{
            }

            /** 					exit*/
            goto L2; // [91] 102
L4: 
        ;}L3: 

        /** 	end for*/
        _p_46588 = _p_46588 + 1;
        goto L1; // [97] 29
L2: 
        ;
    }

    /** 	if rb then*/
    if (_rb_46584 == 0)
    {
        goto L5; // [104] 122
    }
    else{
    }

    /** 		if not lb then*/
    if (_lb_46583 != 0)
    goto L6; // [109] 121

    /** 			indent -= 4*/
    _53indent_46574 = _53indent_46574 - 4;
L6: 
L5: 

    /** 	i = indent + temp_indent*/
    _i_46581 = _53indent_46574 + _53temp_indent_46575;

    /** 	while i >= length(big_blanks) do*/
L7: 
    if (IS_SEQUENCE(_53big_blanks_46576)){
            _24805 = SEQ_PTR(_53big_blanks_46576)->length;
    }
    else {
        _24805 = 1;
    }
    if (_i_46581 < _24805)
    goto L8; // [140] 163

    /** 		c_puts(big_blanks)*/
    RefDS(_53big_blanks_46576);
    _53c_puts(_53big_blanks_46576);

    /** 		i -= length(big_blanks)*/
    if (IS_SEQUENCE(_53big_blanks_46576)){
            _24807 = SEQ_PTR(_53big_blanks_46576)->length;
    }
    else {
        _24807 = 1;
    }
    _i_46581 = _i_46581 - _24807;
    _24807 = NOVALUE;

    /** 	end while*/
    goto L7; // [160] 137
L8: 

    /** 	c_puts(big_blanks[1..i])*/
    rhs_slice_target = (object_ptr)&_24809;
    RHS_Slice(_53big_blanks_46576, 1, _i_46581);
    _53c_puts(_24809);
    _24809 = NOVALUE;

    /** 	temp_indent = 0    */
    _53temp_indent_46575 = 0;

    /** end procedure*/
    DeRefDS(_stmt_46580);
    return;
    ;
}


void _53adjust_indent_after(int _stmt_46613)
{
    int _24830 = NOVALUE;
    int _24829 = NOVALUE;
    int _24827 = NOVALUE;
    int _24825 = NOVALUE;
    int _24824 = NOVALUE;
    int _24821 = NOVALUE;
    int _24819 = NOVALUE;
    int _24818 = NOVALUE;
    int _24815 = NOVALUE;
    int _24811 = NOVALUE;
    int _24810 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for p = 1 to length(stmt) do*/
    if (IS_SEQUENCE(_stmt_46613)){
            _24810 = SEQ_PTR(_stmt_46613)->length;
    }
    else {
        _24810 = 1;
    }
    {
        int _p_46615;
        _p_46615 = 1;
L1: 
        if (_p_46615 > _24810){
            goto L2; // [8] 61
        }

        /** 		switch stmt[p] do*/
        _2 = (int)SEQ_PTR(_stmt_46613);
        _24811 = (int)*(((s1_ptr)_2)->base + _p_46615);
        if (IS_SEQUENCE(_24811) ){
            goto L3; // [21] 54
        }
        if(!IS_ATOM_INT(_24811)){
            if( (DBL_PTR(_24811)->dbl != (double) ((int) DBL_PTR(_24811)->dbl) ) ){
                goto L3; // [21] 54
            }
            _0 = (int) DBL_PTR(_24811)->dbl;
        }
        else {
            _0 = _24811;
        };
        _24811 = NOVALUE;
        switch ( _0 ){ 

            /** 			case '\n' then*/
            case 10:

            /** 				exit*/
            goto L2; // [32] 61
            goto L3; // [34] 54

            /** 			case '{' then*/
            case 123:

            /** 				indent += 4*/
            _53indent_46574 = _53indent_46574 + 4;

            /** 				return*/
            DeRefDS(_stmt_46613);
            return;
        ;}L3: 

        /** 	end for*/
        _p_46615 = _p_46615 + 1;
        goto L1; // [56] 15
L2: 
        ;
    }

    /** 	if length(stmt) < 3 then*/
    if (IS_SEQUENCE(_stmt_46613)){
            _24815 = SEQ_PTR(_stmt_46613)->length;
    }
    else {
        _24815 = 1;
    }
    if (_24815 >= 3)
    goto L4; // [66] 76

    /** 		return*/
    DeRefDS(_stmt_46613);
    return;
L4: 

    /** 	if not equal("if ", stmt[1..3]) then*/
    rhs_slice_target = (object_ptr)&_24818;
    RHS_Slice(_stmt_46613, 1, 3);
    if (_24817 == _24818)
    _24819 = 1;
    else if (IS_ATOM_INT(_24817) && IS_ATOM_INT(_24818))
    _24819 = 0;
    else
    _24819 = (compare(_24817, _24818) == 0);
    DeRefDS(_24818);
    _24818 = NOVALUE;
    if (_24819 != 0)
    goto L5; // [87] 96
    _24819 = NOVALUE;

    /** 		return*/
    DeRefDS(_stmt_46613);
    return;
L5: 

    /** 	if length(stmt) < 5 then*/
    if (IS_SEQUENCE(_stmt_46613)){
            _24821 = SEQ_PTR(_stmt_46613)->length;
    }
    else {
        _24821 = 1;
    }
    if (_24821 >= 5)
    goto L6; // [101] 111

    /** 		return*/
    DeRefDS(_stmt_46613);
    return;
L6: 

    /** 	if not equal("else", stmt[1..4]) then*/
    rhs_slice_target = (object_ptr)&_24824;
    RHS_Slice(_stmt_46613, 1, 4);
    if (_24823 == _24824)
    _24825 = 1;
    else if (IS_ATOM_INT(_24823) && IS_ATOM_INT(_24824))
    _24825 = 0;
    else
    _24825 = (compare(_24823, _24824) == 0);
    DeRefDS(_24824);
    _24824 = NOVALUE;
    if (_24825 != 0)
    goto L7; // [122] 131
    _24825 = NOVALUE;

    /** 		return*/
    DeRefDS(_stmt_46613);
    return;
L7: 

    /** 	if not find(stmt[5], {" \n"}) then*/
    _2 = (int)SEQ_PTR(_stmt_46613);
    _24827 = (int)*(((s1_ptr)_2)->base + 5);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_24828);
    *((int *)(_2+4)) = _24828;
    _24829 = MAKE_SEQ(_1);
    _24830 = find_from(_24827, _24829, 1);
    _24827 = NOVALUE;
    DeRefDS(_24829);
    _24829 = NOVALUE;
    if (_24830 != 0)
    goto L8; // [146] 155
    _24830 = NOVALUE;

    /** 		return*/
    DeRefDS(_stmt_46613);
    return;
L8: 

    /** 	temp_indent = 4*/
    _53temp_indent_46575 = 4;

    /** end procedure*/
    DeRefDS(_stmt_46613);
    return;
    ;
}



// 0x98AA609B
