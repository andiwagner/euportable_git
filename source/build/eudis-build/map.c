// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _32map(int _obj_p_13324)
{
    int _m__13328 = NOVALUE;
    int _7381 = NOVALUE;
    int _7380 = NOVALUE;
    int _7379 = NOVALUE;
    int _7378 = NOVALUE;
    int _7377 = NOVALUE;
    int _7376 = NOVALUE;
    int _7375 = NOVALUE;
    int _7374 = NOVALUE;
    int _7373 = NOVALUE;
    int _7372 = NOVALUE;
    int _7370 = NOVALUE;
    int _7369 = NOVALUE;
    int _7368 = NOVALUE;
    int _7367 = NOVALUE;
    int _7365 = NOVALUE;
    int _7364 = NOVALUE;
    int _7363 = NOVALUE;
    int _7362 = NOVALUE;
    int _7360 = NOVALUE;
    int _7359 = NOVALUE;
    int _7358 = NOVALUE;
    int _7357 = NOVALUE;
    int _7356 = NOVALUE;
    int _7355 = NOVALUE;
    int _7354 = NOVALUE;
    int _7353 = NOVALUE;
    int _7352 = NOVALUE;
    int _7351 = NOVALUE;
    int _7349 = NOVALUE;
    int _7347 = NOVALUE;
    int _7346 = NOVALUE;
    int _7344 = NOVALUE;
    int _7342 = NOVALUE;
    int _7341 = NOVALUE;
    int _7339 = NOVALUE;
    int _7338 = NOVALUE;
    int _7336 = NOVALUE;
    int _7334 = NOVALUE;
    int _7332 = NOVALUE;
    int _7329 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not eumem:valid(obj_p, "") then return 0 end if*/
    Ref(_obj_p_13324);
    RefDS(_5);
    _7329 = _33valid(_obj_p_13324, _5);
    if (IS_ATOM_INT(_7329)) {
        if (_7329 != 0){
            DeRef(_7329);
            _7329 = NOVALUE;
            goto L1; // [8] 16
        }
    }
    else {
        if (DBL_PTR(_7329)->dbl != 0.0){
            DeRef(_7329);
            _7329 = NOVALUE;
            goto L1; // [8] 16
        }
    }
    DeRef(_7329);
    _7329 = NOVALUE;
    DeRef(_obj_p_13324);
    DeRef(_m__13328);
    return 0;
L1: 

    /** 	object m_*/

    /** 	m_ = eumem:ram_space[obj_p]*/
    DeRef(_m__13328);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!IS_ATOM_INT(_obj_p_13324)){
        _m__13328 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_obj_p_13324)->dbl));
    }
    else{
        _m__13328 = (int)*(((s1_ptr)_2)->base + _obj_p_13324);
    }
    Ref(_m__13328);

    /** 	if not sequence(m_) then return 0 end if*/
    _7332 = IS_SEQUENCE(_m__13328);
    if (_7332 != 0)
    goto L2; // [31] 39
    _7332 = NOVALUE;
    DeRef(_obj_p_13324);
    DeRef(_m__13328);
    return 0;
L2: 

    /** 	if length(m_) < 6 then return 0 end if*/
    if (IS_SEQUENCE(_m__13328)){
            _7334 = SEQ_PTR(_m__13328)->length;
    }
    else {
        _7334 = 1;
    }
    if (_7334 >= 6)
    goto L3; // [44] 53
    DeRef(_obj_p_13324);
    DeRef(_m__13328);
    return 0;
L3: 

    /** 	if length(m_) > 7 then return 0 end if*/
    if (IS_SEQUENCE(_m__13328)){
            _7336 = SEQ_PTR(_m__13328)->length;
    }
    else {
        _7336 = 1;
    }
    if (_7336 <= 7)
    goto L4; // [58] 67
    DeRef(_obj_p_13324);
    DeRef(_m__13328);
    return 0;
L4: 

    /** 	if not equal(m_[TYPE_TAG], type_is_map) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__13328);
    _7338 = (int)*(((s1_ptr)_2)->base + 1);
    if (_7338 == _32type_is_map_13297)
    _7339 = 1;
    else if (IS_ATOM_INT(_7338) && IS_ATOM_INT(_32type_is_map_13297))
    _7339 = 0;
    else
    _7339 = (compare(_7338, _32type_is_map_13297) == 0);
    _7338 = NOVALUE;
    if (_7339 != 0)
    goto L5; // [79] 87
    _7339 = NOVALUE;
    DeRef(_obj_p_13324);
    DeRef(_m__13328);
    return 0;
L5: 

    /** 	if not integer(m_[ELEMENT_COUNT]) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__13328);
    _7341 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_7341))
    _7342 = 1;
    else if (IS_ATOM_DBL(_7341))
    _7342 = IS_ATOM_INT(DoubleToInt(_7341));
    else
    _7342 = 0;
    _7341 = NOVALUE;
    if (_7342 != 0)
    goto L6; // [98] 106
    _7342 = NOVALUE;
    DeRef(_obj_p_13324);
    DeRef(_m__13328);
    return 0;
L6: 

    /** 	if m_[ELEMENT_COUNT] < 0 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__13328);
    _7344 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(GREATEREQ, _7344, 0)){
        _7344 = NOVALUE;
        goto L7; // [114] 123
    }
    _7344 = NOVALUE;
    DeRef(_obj_p_13324);
    DeRef(_m__13328);
    return 0;
L7: 

    /** 	if not integer(m_[IN_USE]) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__13328);
    _7346 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_7346))
    _7347 = 1;
    else if (IS_ATOM_DBL(_7346))
    _7347 = IS_ATOM_INT(DoubleToInt(_7346));
    else
    _7347 = 0;
    _7346 = NOVALUE;
    if (_7347 != 0)
    goto L8; // [134] 142
    _7347 = NOVALUE;
    DeRef(_obj_p_13324);
    DeRef(_m__13328);
    return 0;
L8: 

    /** 	if m_[IN_USE] < 0		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__13328);
    _7349 = (int)*(((s1_ptr)_2)->base + 3);
    if (binary_op_a(GREATEREQ, _7349, 0)){
        _7349 = NOVALUE;
        goto L9; // [150] 159
    }
    _7349 = NOVALUE;
    DeRef(_obj_p_13324);
    DeRef(_m__13328);
    return 0;
L9: 

    /** 	if equal(m_[MAP_TYPE],SMALLMAP) then*/
    _2 = (int)SEQ_PTR(_m__13328);
    _7351 = (int)*(((s1_ptr)_2)->base + 4);
    if (_7351 == 115)
    _7352 = 1;
    else if (IS_ATOM_INT(_7351) && IS_ATOM_INT(115))
    _7352 = 0;
    else
    _7352 = (compare(_7351, 115) == 0);
    _7351 = NOVALUE;
    if (_7352 == 0)
    {
        _7352 = NOVALUE;
        goto LA; // [171] 312
    }
    else{
        _7352 = NOVALUE;
    }

    /** 		if atom(m_[KEY_LIST]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__13328);
    _7353 = (int)*(((s1_ptr)_2)->base + 5);
    _7354 = IS_ATOM(_7353);
    _7353 = NOVALUE;
    if (_7354 == 0)
    {
        _7354 = NOVALUE;
        goto LB; // [185] 193
    }
    else{
        _7354 = NOVALUE;
    }
    DeRef(_obj_p_13324);
    DeRef(_m__13328);
    return 0;
LB: 

    /** 		if atom(m_[VALUE_LIST]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__13328);
    _7355 = (int)*(((s1_ptr)_2)->base + 6);
    _7356 = IS_ATOM(_7355);
    _7355 = NOVALUE;
    if (_7356 == 0)
    {
        _7356 = NOVALUE;
        goto LC; // [204] 212
    }
    else{
        _7356 = NOVALUE;
    }
    DeRef(_obj_p_13324);
    DeRef(_m__13328);
    return 0;
LC: 

    /** 		if atom(m_[FREE_LIST]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__13328);
    _7357 = (int)*(((s1_ptr)_2)->base + 7);
    _7358 = IS_ATOM(_7357);
    _7357 = NOVALUE;
    if (_7358 == 0)
    {
        _7358 = NOVALUE;
        goto LD; // [223] 231
    }
    else{
        _7358 = NOVALUE;
    }
    DeRef(_obj_p_13324);
    DeRef(_m__13328);
    return 0;
LD: 

    /** 		if length(m_[KEY_LIST]) = 0  then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__13328);
    _7359 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7359)){
            _7360 = SEQ_PTR(_7359)->length;
    }
    else {
        _7360 = 1;
    }
    _7359 = NOVALUE;
    if (_7360 != 0)
    goto LE; // [242] 251
    DeRef(_obj_p_13324);
    DeRef(_m__13328);
    _7359 = NOVALUE;
    return 0;
LE: 

    /** 		if length(m_[KEY_LIST]) != length(m_[VALUE_LIST]) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__13328);
    _7362 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7362)){
            _7363 = SEQ_PTR(_7362)->length;
    }
    else {
        _7363 = 1;
    }
    _7362 = NOVALUE;
    _2 = (int)SEQ_PTR(_m__13328);
    _7364 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_7364)){
            _7365 = SEQ_PTR(_7364)->length;
    }
    else {
        _7365 = 1;
    }
    _7364 = NOVALUE;
    if (_7363 == _7365)
    goto LF; // [271] 280
    DeRef(_obj_p_13324);
    DeRef(_m__13328);
    _7359 = NOVALUE;
    _7362 = NOVALUE;
    _7364 = NOVALUE;
    return 0;
LF: 

    /** 		if length(m_[KEY_LIST]) != length(m_[FREE_LIST]) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__13328);
    _7367 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7367)){
            _7368 = SEQ_PTR(_7367)->length;
    }
    else {
        _7368 = 1;
    }
    _7367 = NOVALUE;
    _2 = (int)SEQ_PTR(_m__13328);
    _7369 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7369)){
            _7370 = SEQ_PTR(_7369)->length;
    }
    else {
        _7370 = 1;
    }
    _7369 = NOVALUE;
    if (_7368 == _7370)
    goto L10; // [300] 404
    DeRef(_obj_p_13324);
    DeRef(_m__13328);
    _7359 = NOVALUE;
    _7362 = NOVALUE;
    _7364 = NOVALUE;
    _7367 = NOVALUE;
    _7369 = NOVALUE;
    return 0;
    goto L10; // [309] 404
LA: 

    /** 	elsif  equal(m_[MAP_TYPE],LARGEMAP) then*/
    _2 = (int)SEQ_PTR(_m__13328);
    _7372 = (int)*(((s1_ptr)_2)->base + 4);
    if (_7372 == 76)
    _7373 = 1;
    else if (IS_ATOM_INT(_7372) && IS_ATOM_INT(76))
    _7373 = 0;
    else
    _7373 = (compare(_7372, 76) == 0);
    _7372 = NOVALUE;
    if (_7373 == 0)
    {
        _7373 = NOVALUE;
        goto L11; // [324] 397
    }
    else{
        _7373 = NOVALUE;
    }

    /** 		if atom(m_[KEY_BUCKETS]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__13328);
    _7374 = (int)*(((s1_ptr)_2)->base + 5);
    _7375 = IS_ATOM(_7374);
    _7374 = NOVALUE;
    if (_7375 == 0)
    {
        _7375 = NOVALUE;
        goto L12; // [338] 346
    }
    else{
        _7375 = NOVALUE;
    }
    DeRef(_obj_p_13324);
    DeRef(_m__13328);
    _7359 = NOVALUE;
    _7362 = NOVALUE;
    _7364 = NOVALUE;
    _7367 = NOVALUE;
    _7369 = NOVALUE;
    return 0;
L12: 

    /** 		if atom(m_[VALUE_BUCKETS]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__13328);
    _7376 = (int)*(((s1_ptr)_2)->base + 6);
    _7377 = IS_ATOM(_7376);
    _7376 = NOVALUE;
    if (_7377 == 0)
    {
        _7377 = NOVALUE;
        goto L13; // [357] 365
    }
    else{
        _7377 = NOVALUE;
    }
    DeRef(_obj_p_13324);
    DeRef(_m__13328);
    _7359 = NOVALUE;
    _7362 = NOVALUE;
    _7364 = NOVALUE;
    _7367 = NOVALUE;
    _7369 = NOVALUE;
    return 0;
L13: 

    /** 		if length(m_[KEY_BUCKETS]) != length(m_[VALUE_BUCKETS])	then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__13328);
    _7378 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7378)){
            _7379 = SEQ_PTR(_7378)->length;
    }
    else {
        _7379 = 1;
    }
    _7378 = NOVALUE;
    _2 = (int)SEQ_PTR(_m__13328);
    _7380 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_7380)){
            _7381 = SEQ_PTR(_7380)->length;
    }
    else {
        _7381 = 1;
    }
    _7380 = NOVALUE;
    if (_7379 == _7381)
    goto L10; // [385] 404
    DeRef(_obj_p_13324);
    DeRef(_m__13328);
    _7359 = NOVALUE;
    _7362 = NOVALUE;
    _7364 = NOVALUE;
    _7367 = NOVALUE;
    _7369 = NOVALUE;
    _7378 = NOVALUE;
    _7380 = NOVALUE;
    return 0;
    goto L10; // [394] 404
L11: 

    /** 		return 0*/
    DeRef(_obj_p_13324);
    DeRef(_m__13328);
    _7359 = NOVALUE;
    _7362 = NOVALUE;
    _7364 = NOVALUE;
    _7367 = NOVALUE;
    _7369 = NOVALUE;
    _7378 = NOVALUE;
    _7380 = NOVALUE;
    return 0;
L10: 

    /** 	return 1*/
    DeRef(_obj_p_13324);
    DeRef(_m__13328);
    _7359 = NOVALUE;
    _7362 = NOVALUE;
    _7364 = NOVALUE;
    _7367 = NOVALUE;
    _7369 = NOVALUE;
    _7378 = NOVALUE;
    _7380 = NOVALUE;
    return 1;
    ;
}


int _32calc_hash(int _key_p_13404, int _max_hash_p_13405)
{
    int _ret__13406 = NOVALUE;
    int _7385 = NOVALUE;
    int _7384 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_max_hash_p_13405)) {
        _1 = (long)(DBL_PTR(_max_hash_p_13405)->dbl);
        if (UNIQUE(DBL_PTR(_max_hash_p_13405)) && (DBL_PTR(_max_hash_p_13405)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_max_hash_p_13405);
        _max_hash_p_13405 = _1;
    }

    /**     ret_ = hash(key_p, stdhash:HSIEH30)*/
    _ret__13406 = calc_hash(_key_p_13404, -6);
    if (!IS_ATOM_INT(_ret__13406)) {
        _1 = (long)(DBL_PTR(_ret__13406)->dbl);
        if (UNIQUE(DBL_PTR(_ret__13406)) && (DBL_PTR(_ret__13406)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret__13406);
        _ret__13406 = _1;
    }

    /** 	return remainder(ret_, max_hash_p) + 1 -- 1-based*/
    _7384 = (_ret__13406 % _max_hash_p_13405);
    _7385 = _7384 + 1;
    if (_7385 > MAXINT){
        _7385 = NewDouble((double)_7385);
    }
    _7384 = NOVALUE;
    DeRef(_key_p_13404);
    return _7385;
    ;
}


int _32threshold(int _new_value_p_13412)
{
    int _old_value__13415 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_new_value_p_13412)) {
        _1 = (long)(DBL_PTR(_new_value_p_13412)->dbl);
        if (UNIQUE(DBL_PTR(_new_value_p_13412)) && (DBL_PTR(_new_value_p_13412)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_new_value_p_13412);
        _new_value_p_13412 = _1;
    }

    /** 	if new_value_p < 1 then*/
    if (_new_value_p_13412 >= 1)
    goto L1; // [5] 18

    /** 		return threshold_size*/
    return _32threshold_size_13318;
L1: 

    /** 	integer old_value_ = threshold_size*/
    _old_value__13415 = _32threshold_size_13318;

    /** 	threshold_size = new_value_p*/
    _32threshold_size_13318 = _new_value_p_13412;

    /** 	return old_value_*/
    return _old_value__13415;
    ;
}


int _32type_of(int _the_map_p_13418)
{
    int _7388 = NOVALUE;
    int _7387 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return eumem:ram_space[the_map_p][MAP_TYPE]*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!IS_ATOM_INT(_the_map_p_13418)){
        _7387 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_13418)->dbl));
    }
    else{
        _7387 = (int)*(((s1_ptr)_2)->base + _the_map_p_13418);
    }
    _2 = (int)SEQ_PTR(_7387);
    _7388 = (int)*(((s1_ptr)_2)->base + 4);
    _7387 = NOVALUE;
    Ref(_7388);
    DeRef(_the_map_p_13418);
    return _7388;
    ;
}


void _32rehash(int _the_map_p_13423, int _requested_bucket_size_p_13424)
{
    int _size__13425 = NOVALUE;
    int _index_2__13426 = NOVALUE;
    int _old_key_buckets__13427 = NOVALUE;
    int _old_val_buckets__13428 = NOVALUE;
    int _new_key_buckets__13429 = NOVALUE;
    int _new_val_buckets__13430 = NOVALUE;
    int _key__13431 = NOVALUE;
    int _value__13432 = NOVALUE;
    int _pos_13433 = NOVALUE;
    int _new_keys_13434 = NOVALUE;
    int _in_use_13435 = NOVALUE;
    int _elem_count_13436 = NOVALUE;
    int _calc_hash_1__tmp_at237_13479 = NOVALUE;
    int _calc_hash_inlined_calc_hash_at_237_13478 = NOVALUE;
    int _ret__inlined_calc_hash_at_237_13477 = NOVALUE;
    int _7448 = NOVALUE;
    int _7447 = NOVALUE;
    int _7446 = NOVALUE;
    int _7445 = NOVALUE;
    int _7444 = NOVALUE;
    int _7443 = NOVALUE;
    int _7442 = NOVALUE;
    int _7441 = NOVALUE;
    int _7440 = NOVALUE;
    int _7438 = NOVALUE;
    int _7437 = NOVALUE;
    int _7436 = NOVALUE;
    int _7433 = NOVALUE;
    int _7432 = NOVALUE;
    int _7430 = NOVALUE;
    int _7429 = NOVALUE;
    int _7428 = NOVALUE;
    int _7427 = NOVALUE;
    int _7425 = NOVALUE;
    int _7423 = NOVALUE;
    int _7421 = NOVALUE;
    int _7418 = NOVALUE;
    int _7416 = NOVALUE;
    int _7415 = NOVALUE;
    int _7414 = NOVALUE;
    int _7413 = NOVALUE;
    int _7411 = NOVALUE;
    int _7409 = NOVALUE;
    int _7407 = NOVALUE;
    int _7406 = NOVALUE;
    int _7404 = NOVALUE;
    int _7402 = NOVALUE;
    int _7399 = NOVALUE;
    int _7397 = NOVALUE;
    int _7396 = NOVALUE;
    int _7395 = NOVALUE;
    int _7394 = NOVALUE;
    int _7393 = NOVALUE;
    int _7390 = NOVALUE;
    int _7389 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_the_map_p_13423)) {
        _1 = (long)(DBL_PTR(_the_map_p_13423)->dbl);
        if (UNIQUE(DBL_PTR(_the_map_p_13423)) && (DBL_PTR(_the_map_p_13423)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map_p_13423);
        _the_map_p_13423 = _1;
    }
    if (!IS_ATOM_INT(_requested_bucket_size_p_13424)) {
        _1 = (long)(DBL_PTR(_requested_bucket_size_p_13424)->dbl);
        if (UNIQUE(DBL_PTR(_requested_bucket_size_p_13424)) && (DBL_PTR(_requested_bucket_size_p_13424)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_requested_bucket_size_p_13424);
        _requested_bucket_size_p_13424 = _1;
    }

    /** 	if eumem:ram_space[the_map_p][MAP_TYPE] = SMALLMAP then*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    _7389 = (int)*(((s1_ptr)_2)->base + _the_map_p_13423);
    _2 = (int)SEQ_PTR(_7389);
    _7390 = (int)*(((s1_ptr)_2)->base + 4);
    _7389 = NOVALUE;
    if (binary_op_a(NOTEQ, _7390, 115)){
        _7390 = NOVALUE;
        goto L1; // [19] 29
    }
    _7390 = NOVALUE;

    /** 		return -- small maps are not hashed.*/
    DeRef(_old_key_buckets__13427);
    DeRef(_old_val_buckets__13428);
    DeRef(_new_key_buckets__13429);
    DeRef(_new_val_buckets__13430);
    DeRef(_key__13431);
    DeRef(_value__13432);
    DeRef(_new_keys_13434);
    return;
L1: 

    /** 	if requested_bucket_size_p <= 0 then*/
    if (_requested_bucket_size_p_13424 > 0)
    goto L2; // [31] 66

    /** 		size_ = floor(length(eumem:ram_space[the_map_p][KEY_BUCKETS]) * 3.5) + 1*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    _7393 = (int)*(((s1_ptr)_2)->base + _the_map_p_13423);
    _2 = (int)SEQ_PTR(_7393);
    _7394 = (int)*(((s1_ptr)_2)->base + 5);
    _7393 = NOVALUE;
    if (IS_SEQUENCE(_7394)){
            _7395 = SEQ_PTR(_7394)->length;
    }
    else {
        _7395 = 1;
    }
    _7394 = NOVALUE;
    _7396 = NewDouble((double)_7395 * DBL_PTR(_6967)->dbl);
    _7395 = NOVALUE;
    _7397 = unary_op(FLOOR, _7396);
    DeRefDS(_7396);
    _7396 = NOVALUE;
    if (IS_ATOM_INT(_7397)) {
        _size__13425 = _7397 + 1;
    }
    else
    { // coercing _size__13425 to an integer 1
        _size__13425 = 1+(long)(DBL_PTR(_7397)->dbl);
        if( !IS_ATOM_INT(_size__13425) ){
            _size__13425 = (object)DBL_PTR(_size__13425)->dbl;
        }
    }
    DeRef(_7397);
    _7397 = NOVALUE;
    goto L3; // [63] 72
L2: 

    /** 		size_ = requested_bucket_size_p*/
    _size__13425 = _requested_bucket_size_p_13424;
L3: 

    /** 	size_ = primes:next_prime(size_, -size_, 2)	-- Allow up to 2 seconds to calc next prime.*/
    if ((unsigned long)_size__13425 == 0xC0000000)
    _7399 = (int)NewDouble((double)-0xC0000000);
    else
    _7399 = - _size__13425;
    _size__13425 = _34next_prime(_size__13425, _7399, 2);
    _7399 = NOVALUE;
    if (!IS_ATOM_INT(_size__13425)) {
        _1 = (long)(DBL_PTR(_size__13425)->dbl);
        if (UNIQUE(DBL_PTR(_size__13425)) && (DBL_PTR(_size__13425)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size__13425);
        _size__13425 = _1;
    }

    /** 	if size_ < 0 then*/
    if (_size__13425 >= 0)
    goto L4; // [89] 99

    /** 		return  -- don't do anything. New size would take too long.*/
    DeRef(_old_key_buckets__13427);
    DeRef(_old_val_buckets__13428);
    DeRef(_new_key_buckets__13429);
    DeRef(_new_val_buckets__13430);
    DeRef(_key__13431);
    DeRef(_value__13432);
    DeRef(_new_keys_13434);
    _7394 = NOVALUE;
    return;
L4: 

    /** 	old_key_buckets_ = eumem:ram_space[the_map_p][KEY_BUCKETS]*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    _7402 = (int)*(((s1_ptr)_2)->base + _the_map_p_13423);
    DeRef(_old_key_buckets__13427);
    _2 = (int)SEQ_PTR(_7402);
    _old_key_buckets__13427 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_old_key_buckets__13427);
    _7402 = NOVALUE;

    /** 	old_val_buckets_ = eumem:ram_space[the_map_p][VALUE_BUCKETS]*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    _7404 = (int)*(((s1_ptr)_2)->base + _the_map_p_13423);
    DeRef(_old_val_buckets__13428);
    _2 = (int)SEQ_PTR(_7404);
    _old_val_buckets__13428 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_old_val_buckets__13428);
    _7404 = NOVALUE;

    /** 	new_key_buckets_ = repeat(repeat(1, threshold_size + 1), size_)*/
    _7406 = _32threshold_size_13318 + 1;
    _7407 = Repeat(1, _7406);
    _7406 = NOVALUE;
    DeRef(_new_key_buckets__13429);
    _new_key_buckets__13429 = Repeat(_7407, _size__13425);
    DeRefDS(_7407);
    _7407 = NOVALUE;

    /** 	new_val_buckets_ = repeat(repeat(0, threshold_size), size_)*/
    _7409 = Repeat(0, _32threshold_size_13318);
    DeRef(_new_val_buckets__13430);
    _new_val_buckets__13430 = Repeat(_7409, _size__13425);
    DeRefDS(_7409);
    _7409 = NOVALUE;

    /** 	elem_count = eumem:ram_space[the_map_p][ELEMENT_COUNT]*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    _7411 = (int)*(((s1_ptr)_2)->base + _the_map_p_13423);
    _2 = (int)SEQ_PTR(_7411);
    _elem_count_13436 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_elem_count_13436)){
        _elem_count_13436 = (long)DBL_PTR(_elem_count_13436)->dbl;
    }
    _7411 = NOVALUE;

    /** 	in_use = 0*/
    _in_use_13435 = 0;

    /** 	eumem:ram_space[the_map_p] = 0*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _33ram_space_12557 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_13423);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	for index = 1 to length(old_key_buckets_) do*/
    if (IS_SEQUENCE(_old_key_buckets__13427)){
            _7413 = SEQ_PTR(_old_key_buckets__13427)->length;
    }
    else {
        _7413 = 1;
    }
    {
        int _index_13466;
        _index_13466 = 1;
L5: 
        if (_index_13466 > _7413){
            goto L6; // [193] 385
        }

        /** 		for entry_idx = 1 to length(old_key_buckets_[index]) do*/
        _2 = (int)SEQ_PTR(_old_key_buckets__13427);
        _7414 = (int)*(((s1_ptr)_2)->base + _index_13466);
        if (IS_SEQUENCE(_7414)){
                _7415 = SEQ_PTR(_7414)->length;
        }
        else {
            _7415 = 1;
        }
        _7414 = NOVALUE;
        {
            int _entry_idx_13469;
            _entry_idx_13469 = 1;
L7: 
            if (_entry_idx_13469 > _7415){
                goto L8; // [209] 378
            }

            /** 			key_ = old_key_buckets_[index][entry_idx]*/
            _2 = (int)SEQ_PTR(_old_key_buckets__13427);
            _7416 = (int)*(((s1_ptr)_2)->base + _index_13466);
            DeRef(_key__13431);
            _2 = (int)SEQ_PTR(_7416);
            _key__13431 = (int)*(((s1_ptr)_2)->base + _entry_idx_13469);
            Ref(_key__13431);
            _7416 = NOVALUE;

            /** 			value_ = old_val_buckets_[index][entry_idx]*/
            _2 = (int)SEQ_PTR(_old_val_buckets__13428);
            _7418 = (int)*(((s1_ptr)_2)->base + _index_13466);
            DeRef(_value__13432);
            _2 = (int)SEQ_PTR(_7418);
            _value__13432 = (int)*(((s1_ptr)_2)->base + _entry_idx_13469);
            Ref(_value__13432);
            _7418 = NOVALUE;

            /** 			index_2_ = calc_hash(key_, size_)*/

            /**     ret_ = hash(key_p, stdhash:HSIEH30)*/
            DeRef(_ret__inlined_calc_hash_at_237_13477);
            _ret__inlined_calc_hash_at_237_13477 = calc_hash(_key__13431, -6);
            if (!IS_ATOM_INT(_ret__inlined_calc_hash_at_237_13477)) {
                _1 = (long)(DBL_PTR(_ret__inlined_calc_hash_at_237_13477)->dbl);
                if (UNIQUE(DBL_PTR(_ret__inlined_calc_hash_at_237_13477)) && (DBL_PTR(_ret__inlined_calc_hash_at_237_13477)->cleanup != 0))
                RTFatal("Cannot assign value with a destructor to an integer");                DeRefDS(_ret__inlined_calc_hash_at_237_13477);
                _ret__inlined_calc_hash_at_237_13477 = _1;
            }

            /** 	return remainder(ret_, max_hash_p) + 1 -- 1-based*/
            _calc_hash_1__tmp_at237_13479 = (_ret__inlined_calc_hash_at_237_13477 % _size__13425);
            _index_2__13426 = _calc_hash_1__tmp_at237_13479 + 1;
            DeRef(_ret__inlined_calc_hash_at_237_13477);
            _ret__inlined_calc_hash_at_237_13477 = NOVALUE;
            if (!IS_ATOM_INT(_index_2__13426)) {
                _1 = (long)(DBL_PTR(_index_2__13426)->dbl);
                if (UNIQUE(DBL_PTR(_index_2__13426)) && (DBL_PTR(_index_2__13426)->cleanup != 0))
                RTFatal("Cannot assign value with a destructor to an integer");                DeRefDS(_index_2__13426);
                _index_2__13426 = _1;
            }

            /** 			new_keys = new_key_buckets_[index_2_]*/
            DeRef(_new_keys_13434);
            _2 = (int)SEQ_PTR(_new_key_buckets__13429);
            _new_keys_13434 = (int)*(((s1_ptr)_2)->base + _index_2__13426);
            RefDS(_new_keys_13434);

            /** 			pos = new_keys[$]*/
            if (IS_SEQUENCE(_new_keys_13434)){
                    _7421 = SEQ_PTR(_new_keys_13434)->length;
            }
            else {
                _7421 = 1;
            }
            _2 = (int)SEQ_PTR(_new_keys_13434);
            _pos_13433 = (int)*(((s1_ptr)_2)->base + _7421);
            if (!IS_ATOM_INT(_pos_13433))
            _pos_13433 = (long)DBL_PTR(_pos_13433)->dbl;

            /** 			if length(new_keys) = pos then*/
            if (IS_SEQUENCE(_new_keys_13434)){
                    _7423 = SEQ_PTR(_new_keys_13434)->length;
            }
            else {
                _7423 = 1;
            }
            if (_7423 != _pos_13433)
            goto L9; // [285] 322

            /** 				new_keys &= repeat(pos, threshold_size)*/
            _7425 = Repeat(_pos_13433, _32threshold_size_13318);
            Concat((object_ptr)&_new_keys_13434, _new_keys_13434, _7425);
            DeRefDS(_7425);
            _7425 = NOVALUE;

            /** 				new_val_buckets_[index_2_] &= repeat(0, threshold_size)*/
            _7427 = Repeat(0, _32threshold_size_13318);
            _2 = (int)SEQ_PTR(_new_val_buckets__13430);
            _7428 = (int)*(((s1_ptr)_2)->base + _index_2__13426);
            if (IS_SEQUENCE(_7428) && IS_ATOM(_7427)) {
            }
            else if (IS_ATOM(_7428) && IS_SEQUENCE(_7427)) {
                Ref(_7428);
                Prepend(&_7429, _7427, _7428);
            }
            else {
                Concat((object_ptr)&_7429, _7428, _7427);
                _7428 = NOVALUE;
            }
            _7428 = NOVALUE;
            DeRefDS(_7427);
            _7427 = NOVALUE;
            _2 = (int)SEQ_PTR(_new_val_buckets__13430);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_val_buckets__13430 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _index_2__13426);
            _1 = *(int *)_2;
            *(int *)_2 = _7429;
            if( _1 != _7429 ){
                DeRef(_1);
            }
            _7429 = NOVALUE;
L9: 

            /** 			new_keys[pos] = key_*/
            Ref(_key__13431);
            _2 = (int)SEQ_PTR(_new_keys_13434);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_keys_13434 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _pos_13433);
            _1 = *(int *)_2;
            *(int *)_2 = _key__13431;
            DeRef(_1);

            /** 			new_val_buckets_[index_2_][pos] = value_*/
            _2 = (int)SEQ_PTR(_new_val_buckets__13430);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_val_buckets__13430 = MAKE_SEQ(_2);
            }
            _3 = (int)(_index_2__13426 + ((s1_ptr)_2)->base);
            Ref(_value__13432);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _pos_13433);
            _1 = *(int *)_2;
            *(int *)_2 = _value__13432;
            DeRef(_1);
            _7430 = NOVALUE;

            /** 			new_keys[$] = pos + 1*/
            if (IS_SEQUENCE(_new_keys_13434)){
                    _7432 = SEQ_PTR(_new_keys_13434)->length;
            }
            else {
                _7432 = 1;
            }
            _7433 = _pos_13433 + 1;
            if (_7433 > MAXINT){
                _7433 = NewDouble((double)_7433);
            }
            _2 = (int)SEQ_PTR(_new_keys_13434);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_keys_13434 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _7432);
            _1 = *(int *)_2;
            *(int *)_2 = _7433;
            if( _1 != _7433 ){
                DeRef(_1);
            }
            _7433 = NOVALUE;

            /** 			new_key_buckets_[index_2_] = new_keys*/
            RefDS(_new_keys_13434);
            _2 = (int)SEQ_PTR(_new_key_buckets__13429);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_key_buckets__13429 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _index_2__13426);
            _1 = *(int *)_2;
            *(int *)_2 = _new_keys_13434;
            DeRefDS(_1);

            /** 			if pos = 1 then*/
            if (_pos_13433 != 1)
            goto LA; // [360] 371

            /** 				in_use += 1*/
            _in_use_13435 = _in_use_13435 + 1;
LA: 

            /** 		end for*/
            _entry_idx_13469 = _entry_idx_13469 + 1;
            goto L7; // [373] 216
L8: 
            ;
        }

        /** 	end for*/
        _index_13466 = _index_13466 + 1;
        goto L5; // [380] 200
L6: 
        ;
    }

    /** 	for index = 1 to length(new_key_buckets_) do*/
    if (IS_SEQUENCE(_new_key_buckets__13429)){
            _7436 = SEQ_PTR(_new_key_buckets__13429)->length;
    }
    else {
        _7436 = 1;
    }
    {
        int _index_13499;
        _index_13499 = 1;
LB: 
        if (_index_13499 > _7436){
            goto LC; // [390] 463
        }

        /** 		pos = new_key_buckets_[index][$]*/
        _2 = (int)SEQ_PTR(_new_key_buckets__13429);
        _7437 = (int)*(((s1_ptr)_2)->base + _index_13499);
        if (IS_SEQUENCE(_7437)){
                _7438 = SEQ_PTR(_7437)->length;
        }
        else {
            _7438 = 1;
        }
        _2 = (int)SEQ_PTR(_7437);
        _pos_13433 = (int)*(((s1_ptr)_2)->base + _7438);
        if (!IS_ATOM_INT(_pos_13433)){
            _pos_13433 = (long)DBL_PTR(_pos_13433)->dbl;
        }
        _7437 = NOVALUE;

        /** 		new_key_buckets_[index] = remove(new_key_buckets_[index], pos, */
        _2 = (int)SEQ_PTR(_new_key_buckets__13429);
        _7440 = (int)*(((s1_ptr)_2)->base + _index_13499);
        _2 = (int)SEQ_PTR(_new_key_buckets__13429);
        _7441 = (int)*(((s1_ptr)_2)->base + _index_13499);
        if (IS_SEQUENCE(_7441)){
                _7442 = SEQ_PTR(_7441)->length;
        }
        else {
            _7442 = 1;
        }
        _7441 = NOVALUE;
        {
            s1_ptr assign_space = SEQ_PTR(_7440);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_pos_13433)) ? _pos_13433 : (long)(DBL_PTR(_pos_13433)->dbl);
            int stop = (IS_ATOM_INT(_7442)) ? _7442 : (long)(DBL_PTR(_7442)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
                RefDS(_7440);
                DeRef(_7443);
                _7443 = _7440;
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_7440), start, &_7443 );
                }
                else Tail(SEQ_PTR(_7440), stop+1, &_7443);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_7440), start, &_7443);
            }
            else {
                assign_slice_seq = &assign_space;
                _1 = Remove_elements(start, stop, 0);
                DeRef(_7443);
                _7443 = _1;
            }
        }
        _7440 = NOVALUE;
        _7442 = NOVALUE;
        _2 = (int)SEQ_PTR(_new_key_buckets__13429);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _new_key_buckets__13429 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index_13499);
        _1 = *(int *)_2;
        *(int *)_2 = _7443;
        if( _1 != _7443 ){
            DeRefDS(_1);
        }
        _7443 = NOVALUE;

        /** 		new_val_buckets_[index] = remove(new_val_buckets_[index], pos, */
        _2 = (int)SEQ_PTR(_new_val_buckets__13430);
        _7444 = (int)*(((s1_ptr)_2)->base + _index_13499);
        _2 = (int)SEQ_PTR(_new_val_buckets__13430);
        _7445 = (int)*(((s1_ptr)_2)->base + _index_13499);
        if (IS_SEQUENCE(_7445)){
                _7446 = SEQ_PTR(_7445)->length;
        }
        else {
            _7446 = 1;
        }
        _7445 = NOVALUE;
        {
            s1_ptr assign_space = SEQ_PTR(_7444);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_pos_13433)) ? _pos_13433 : (long)(DBL_PTR(_pos_13433)->dbl);
            int stop = (IS_ATOM_INT(_7446)) ? _7446 : (long)(DBL_PTR(_7446)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
                RefDS(_7444);
                DeRef(_7447);
                _7447 = _7444;
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_7444), start, &_7447 );
                }
                else Tail(SEQ_PTR(_7444), stop+1, &_7447);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_7444), start, &_7447);
            }
            else {
                assign_slice_seq = &assign_space;
                _1 = Remove_elements(start, stop, 0);
                DeRef(_7447);
                _7447 = _1;
            }
        }
        _7444 = NOVALUE;
        _7446 = NOVALUE;
        _2 = (int)SEQ_PTR(_new_val_buckets__13430);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _new_val_buckets__13430 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index_13499);
        _1 = *(int *)_2;
        *(int *)_2 = _7447;
        if( _1 != _7447 ){
            DeRef(_1);
        }
        _7447 = NOVALUE;

        /** 	end for*/
        _index_13499 = _index_13499 + 1;
        goto LB; // [458] 397
LC: 
        ;
    }

    /** 	eumem:ram_space[the_map_p] = { */
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_32type_is_map_13297);
    *((int *)(_2+4)) = _32type_is_map_13297;
    *((int *)(_2+8)) = _elem_count_13436;
    *((int *)(_2+12)) = _in_use_13435;
    *((int *)(_2+16)) = 76;
    RefDS(_new_key_buckets__13429);
    *((int *)(_2+20)) = _new_key_buckets__13429;
    RefDS(_new_val_buckets__13430);
    *((int *)(_2+24)) = _new_val_buckets__13430;
    _7448 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _33ram_space_12557 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_13423);
    _1 = *(int *)_2;
    *(int *)_2 = _7448;
    if( _1 != _7448 ){
        DeRef(_1);
    }
    _7448 = NOVALUE;

    /** end procedure*/
    DeRef(_old_key_buckets__13427);
    DeRef(_old_val_buckets__13428);
    DeRefDS(_new_key_buckets__13429);
    DeRefDS(_new_val_buckets__13430);
    DeRef(_key__13431);
    DeRef(_value__13432);
    DeRef(_new_keys_13434);
    _7394 = NOVALUE;
    _7414 = NOVALUE;
    _7441 = NOVALUE;
    _7445 = NOVALUE;
    return;
    ;
}


int _32new(int _initial_size_p_13515)
{
    int _buckets__13517 = NOVALUE;
    int _new_map__13518 = NOVALUE;
    int _temp_map__13519 = NOVALUE;
    int _7461 = NOVALUE;
    int _7460 = NOVALUE;
    int _7459 = NOVALUE;
    int _7457 = NOVALUE;
    int _7456 = NOVALUE;
    int _7453 = NOVALUE;
    int _7452 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_initial_size_p_13515)) {
        _1 = (long)(DBL_PTR(_initial_size_p_13515)->dbl);
        if (UNIQUE(DBL_PTR(_initial_size_p_13515)) && (DBL_PTR(_initial_size_p_13515)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_initial_size_p_13515);
        _initial_size_p_13515 = _1;
    }

    /** 	if initial_size_p < 3 then*/
    if (_initial_size_p_13515 >= 3)
    goto L1; // [5] 15

    /** 		initial_size_p = 3*/
    _initial_size_p_13515 = 3;
L1: 

    /** 	if initial_size_p > threshold_size then*/
    if (_initial_size_p_13515 <= _32threshold_size_13318)
    goto L2; // [19] 75

    /** 		buckets_ = floor((initial_size_p + threshold_size - 1) / threshold_size)*/
    _7452 = _initial_size_p_13515 + _32threshold_size_13318;
    if ((long)((unsigned long)_7452 + (unsigned long)HIGH_BITS) >= 0) 
    _7452 = NewDouble((double)_7452);
    if (IS_ATOM_INT(_7452)) {
        _7453 = _7452 - 1;
        if ((long)((unsigned long)_7453 +(unsigned long) HIGH_BITS) >= 0){
            _7453 = NewDouble((double)_7453);
        }
    }
    else {
        _7453 = NewDouble(DBL_PTR(_7452)->dbl - (double)1);
    }
    DeRef(_7452);
    _7452 = NOVALUE;
    if (IS_ATOM_INT(_7453)) {
        if (_32threshold_size_13318 > 0 && _7453 >= 0) {
            _buckets__13517 = _7453 / _32threshold_size_13318;
        }
        else {
            temp_dbl = floor((double)_7453 / (double)_32threshold_size_13318);
            _buckets__13517 = (long)temp_dbl;
        }
    }
    else {
        _2 = binary_op(DIVIDE, _7453, _32threshold_size_13318);
        _buckets__13517 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_7453);
    _7453 = NOVALUE;
    if (!IS_ATOM_INT(_buckets__13517)) {
        _1 = (long)(DBL_PTR(_buckets__13517)->dbl);
        if (UNIQUE(DBL_PTR(_buckets__13517)) && (DBL_PTR(_buckets__13517)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_buckets__13517);
        _buckets__13517 = _1;
    }

    /** 		buckets_ = primes:next_prime(buckets_)*/
    _buckets__13517 = _34next_prime(_buckets__13517, -1, 1);
    if (!IS_ATOM_INT(_buckets__13517)) {
        _1 = (long)(DBL_PTR(_buckets__13517)->dbl);
        if (UNIQUE(DBL_PTR(_buckets__13517)) && (DBL_PTR(_buckets__13517)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_buckets__13517);
        _buckets__13517 = _1;
    }

    /** 		new_map_ = { type_is_map, 0, 0, LARGEMAP, repeat({}, buckets_), repeat({}, buckets_) }*/
    _7456 = Repeat(_5, _buckets__13517);
    _7457 = Repeat(_5, _buckets__13517);
    _0 = _new_map__13518;
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_32type_is_map_13297);
    *((int *)(_2+4)) = _32type_is_map_13297;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 76;
    *((int *)(_2+20)) = _7456;
    *((int *)(_2+24)) = _7457;
    _new_map__13518 = MAKE_SEQ(_1);
    DeRef(_0);
    _7457 = NOVALUE;
    _7456 = NOVALUE;
    goto L3; // [72] 100
L2: 

    /** 		new_map_ = {*/
    _7459 = Repeat(_32init_small_map_key_13319, _initial_size_p_13515);
    _7460 = Repeat(0, _initial_size_p_13515);
    _7461 = Repeat(0, _initial_size_p_13515);
    _0 = _new_map__13518;
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_32type_is_map_13297);
    *((int *)(_2+4)) = _32type_is_map_13297;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 115;
    *((int *)(_2+20)) = _7459;
    *((int *)(_2+24)) = _7460;
    *((int *)(_2+28)) = _7461;
    _new_map__13518 = MAKE_SEQ(_1);
    DeRef(_0);
    _7461 = NOVALUE;
    _7460 = NOVALUE;
    _7459 = NOVALUE;
L3: 

    /** 	temp_map_ = eumem:malloc()*/
    _0 = _temp_map__13519;
    _temp_map__13519 = _33malloc(1, 1);
    DeRef(_0);

    /** 	eumem:ram_space[temp_map_] = new_map_*/
    RefDS(_new_map__13518);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _33ram_space_12557 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_temp_map__13519))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_temp_map__13519)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _temp_map__13519);
    _1 = *(int *)_2;
    *(int *)_2 = _new_map__13518;
    DeRef(_1);

    /** 	return temp_map_*/
    DeRefDS(_new_map__13518);
    return _temp_map__13519;
    ;
}


int _32new_extra(int _the_map_p_13539, int _initial_size_p_13540)
{
    int _7465 = NOVALUE;
    int _7464 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_initial_size_p_13540)) {
        _1 = (long)(DBL_PTR(_initial_size_p_13540)->dbl);
        if (UNIQUE(DBL_PTR(_initial_size_p_13540)) && (DBL_PTR(_initial_size_p_13540)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_initial_size_p_13540);
        _initial_size_p_13540 = _1;
    }

    /** 	if map(the_map_p) then*/
    Ref(_the_map_p_13539);
    _7464 = _32map(_the_map_p_13539);
    if (_7464 == 0) {
        DeRef(_7464);
        _7464 = NOVALUE;
        goto L1; // [9] 21
    }
    else {
        if (!IS_ATOM_INT(_7464) && DBL_PTR(_7464)->dbl == 0.0){
            DeRef(_7464);
            _7464 = NOVALUE;
            goto L1; // [9] 21
        }
        DeRef(_7464);
        _7464 = NOVALUE;
    }
    DeRef(_7464);
    _7464 = NOVALUE;

    /** 		return the_map_p*/
    return _the_map_p_13539;
    goto L2; // [18] 32
L1: 

    /** 		return new(initial_size_p)*/
    _7465 = _32new(_initial_size_p_13540);
    DeRef(_the_map_p_13539);
    return _7465;
L2: 
    ;
}


int _32compare(int _map_1_p_13547, int _map_2_p_13548, int _scope_p_13549)
{
    int _data_set_1__13550 = NOVALUE;
    int _data_set_2__13551 = NOVALUE;
    int _7477 = NOVALUE;
    int _7471 = NOVALUE;
    int _7469 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_scope_p_13549)) {
        _1 = (long)(DBL_PTR(_scope_p_13549)->dbl);
        if (UNIQUE(DBL_PTR(_scope_p_13549)) && (DBL_PTR(_scope_p_13549)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_scope_p_13549);
        _scope_p_13549 = _1;
    }

    /** 	if map_1_p = map_2_p then*/
    if (binary_op_a(NOTEQ, _map_1_p_13547, _map_2_p_13548)){
        goto L1; // [5] 16
    }

    /** 		return 0*/
    DeRef(_map_1_p_13547);
    DeRef(_map_2_p_13548);
    DeRef(_data_set_1__13550);
    DeRef(_data_set_2__13551);
    return 0;
L1: 

    /** 	switch scope_p do*/
    _0 = _scope_p_13549;
    switch ( _0 ){ 

        /** 		case 'v', 'V' then*/
        case 118:
        case 86:

        /** 			data_set_1_ = stdsort:sort(values(map_1_p))*/
        Ref(_map_1_p_13547);
        _7469 = _32values(_map_1_p_13547, 0, 0);
        _0 = _data_set_1__13550;
        _data_set_1__13550 = _22sort(_7469, 1);
        DeRef(_0);
        _7469 = NOVALUE;

        /** 			data_set_2_ = stdsort:sort(values(map_2_p))*/
        Ref(_map_2_p_13548);
        _7471 = _32values(_map_2_p_13548, 0, 0);
        _0 = _data_set_2__13551;
        _data_set_2__13551 = _22sort(_7471, 1);
        DeRef(_0);
        _7471 = NOVALUE;
        goto L2; // [59] 110

        /** 		case 'k', 'K' then*/
        case 107:
        case 75:

        /** 			data_set_1_ = keys(map_1_p, 1)*/
        Ref(_map_1_p_13547);
        _0 = _data_set_1__13550;
        _data_set_1__13550 = _32keys(_map_1_p_13547, 1);
        DeRef(_0);

        /** 			data_set_2_ = keys(map_2_p, 1)*/
        Ref(_map_2_p_13548);
        _0 = _data_set_2__13551;
        _data_set_2__13551 = _32keys(_map_2_p_13548, 1);
        DeRef(_0);
        goto L2; // [85] 110

        /** 		case else*/
        default:

        /** 			data_set_1_ = pairs(map_1_p, 1)*/
        Ref(_map_1_p_13547);
        _0 = _data_set_1__13550;
        _data_set_1__13550 = _32pairs(_map_1_p_13547, 1);
        DeRef(_0);

        /** 			data_set_2_ = pairs(map_2_p, 1)*/
        Ref(_map_2_p_13548);
        _0 = _data_set_2__13551;
        _data_set_2__13551 = _32pairs(_map_2_p_13548, 1);
        DeRef(_0);
    ;}L2: 

    /** 	if equal(data_set_1_, data_set_2_) then*/
    if (_data_set_1__13550 == _data_set_2__13551)
    _7477 = 1;
    else if (IS_ATOM_INT(_data_set_1__13550) && IS_ATOM_INT(_data_set_2__13551))
    _7477 = 0;
    else
    _7477 = (compare(_data_set_1__13550, _data_set_2__13551) == 0);
    if (_7477 == 0)
    {
        _7477 = NOVALUE;
        goto L3; // [120] 130
    }
    else{
        _7477 = NOVALUE;
    }

    /** 		return 1*/
    DeRef(_map_1_p_13547);
    DeRef(_map_2_p_13548);
    DeRefDS(_data_set_1__13550);
    DeRefDS(_data_set_2__13551);
    return 1;
L3: 

    /** 	return -1*/
    DeRef(_map_1_p_13547);
    DeRef(_map_2_p_13548);
    DeRef(_data_set_1__13550);
    DeRef(_data_set_2__13551);
    return -1;
    ;
}


int _32has(int _the_map_p_13577, int _the_key_p_13578)
{
    int _index__13579 = NOVALUE;
    int _pos__13580 = NOVALUE;
    int _from__13581 = NOVALUE;
    int _calc_hash_1__tmp_at40_13593 = NOVALUE;
    int _calc_hash_inlined_calc_hash_at_40_13592 = NOVALUE;
    int _ret__inlined_calc_hash_at_40_13591 = NOVALUE;
    int _max_hash_p_inlined_calc_hash_at_37_13590 = NOVALUE;
    int _7501 = NOVALUE;
    int _7499 = NOVALUE;
    int _7498 = NOVALUE;
    int _7495 = NOVALUE;
    int _7494 = NOVALUE;
    int _7493 = NOVALUE;
    int _7491 = NOVALUE;
    int _7490 = NOVALUE;
    int _7488 = NOVALUE;
    int _7486 = NOVALUE;
    int _7485 = NOVALUE;
    int _7484 = NOVALUE;
    int _7483 = NOVALUE;
    int _7482 = NOVALUE;
    int _7481 = NOVALUE;
    int _7479 = NOVALUE;
    int _7478 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_the_map_p_13577)) {
        _1 = (long)(DBL_PTR(_the_map_p_13577)->dbl);
        if (UNIQUE(DBL_PTR(_the_map_p_13577)) && (DBL_PTR(_the_map_p_13577)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map_p_13577);
        _the_map_p_13577 = _1;
    }

    /** 	if eumem:ram_space[the_map_p][MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    _7478 = (int)*(((s1_ptr)_2)->base + _the_map_p_13577);
    _2 = (int)SEQ_PTR(_7478);
    _7479 = (int)*(((s1_ptr)_2)->base + 4);
    _7478 = NOVALUE;
    if (binary_op_a(NOTEQ, _7479, 76)){
        _7479 = NOVALUE;
        goto L1; // [17] 94
    }
    _7479 = NOVALUE;

    /** 		index_ = calc_hash(the_key_p, length(eumem:ram_space[the_map_p][KEY_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    _7481 = (int)*(((s1_ptr)_2)->base + _the_map_p_13577);
    _2 = (int)SEQ_PTR(_7481);
    _7482 = (int)*(((s1_ptr)_2)->base + 5);
    _7481 = NOVALUE;
    if (IS_SEQUENCE(_7482)){
            _7483 = SEQ_PTR(_7482)->length;
    }
    else {
        _7483 = 1;
    }
    _7482 = NOVALUE;
    _max_hash_p_inlined_calc_hash_at_37_13590 = _7483;
    _7483 = NOVALUE;

    /**     ret_ = hash(key_p, stdhash:HSIEH30)*/
    DeRef(_ret__inlined_calc_hash_at_40_13591);
    _ret__inlined_calc_hash_at_40_13591 = calc_hash(_the_key_p_13578, -6);
    if (!IS_ATOM_INT(_ret__inlined_calc_hash_at_40_13591)) {
        _1 = (long)(DBL_PTR(_ret__inlined_calc_hash_at_40_13591)->dbl);
        if (UNIQUE(DBL_PTR(_ret__inlined_calc_hash_at_40_13591)) && (DBL_PTR(_ret__inlined_calc_hash_at_40_13591)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret__inlined_calc_hash_at_40_13591);
        _ret__inlined_calc_hash_at_40_13591 = _1;
    }

    /** 	return remainder(ret_, max_hash_p) + 1 -- 1-based*/
    _calc_hash_1__tmp_at40_13593 = (_ret__inlined_calc_hash_at_40_13591 % _max_hash_p_inlined_calc_hash_at_37_13590);
    _index__13579 = _calc_hash_1__tmp_at40_13593 + 1;
    DeRef(_ret__inlined_calc_hash_at_40_13591);
    _ret__inlined_calc_hash_at_40_13591 = NOVALUE;
    if (!IS_ATOM_INT(_index__13579)) {
        _1 = (long)(DBL_PTR(_index__13579)->dbl);
        if (UNIQUE(DBL_PTR(_index__13579)) && (DBL_PTR(_index__13579)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_index__13579);
        _index__13579 = _1;
    }

    /** 		pos_ = find(the_key_p, eumem:ram_space[the_map_p][KEY_BUCKETS][index_])*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    _7484 = (int)*(((s1_ptr)_2)->base + _the_map_p_13577);
    _2 = (int)SEQ_PTR(_7484);
    _7485 = (int)*(((s1_ptr)_2)->base + 5);
    _7484 = NOVALUE;
    _2 = (int)SEQ_PTR(_7485);
    _7486 = (int)*(((s1_ptr)_2)->base + _index__13579);
    _7485 = NOVALUE;
    _pos__13580 = find_from(_the_key_p_13578, _7486, 1);
    _7486 = NOVALUE;
    goto L2; // [91] 215
L1: 

    /** 		if equal(the_key_p, init_small_map_key) then*/
    if (_the_key_p_13578 == _32init_small_map_key_13319)
    _7488 = 1;
    else if (IS_ATOM_INT(_the_key_p_13578) && IS_ATOM_INT(_32init_small_map_key_13319))
    _7488 = 0;
    else
    _7488 = (compare(_the_key_p_13578, _32init_small_map_key_13319) == 0);
    if (_7488 == 0)
    {
        _7488 = NOVALUE;
        goto L3; // [100] 194
    }
    else{
        _7488 = NOVALUE;
    }

    /** 			from_ = 1*/
    _from__13581 = 1;

    /** 			while from_ > 0 do*/
L4: 
    if (_from__13581 <= 0)
    goto L5; // [113] 214

    /** 				pos_ = find(the_key_p, eumem:ram_space[the_map_p][KEY_LIST], from_)*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    _7490 = (int)*(((s1_ptr)_2)->base + _the_map_p_13577);
    _2 = (int)SEQ_PTR(_7490);
    _7491 = (int)*(((s1_ptr)_2)->base + 5);
    _7490 = NOVALUE;
    _pos__13580 = find_from(_the_key_p_13578, _7491, _from__13581);
    _7491 = NOVALUE;

    /** 				if pos_ then*/
    if (_pos__13580 == 0)
    {
        goto L6; // [138] 173
    }
    else{
    }

    /** 					if eumem:ram_space[the_map_p][FREE_LIST][pos_] = 1 then*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    _7493 = (int)*(((s1_ptr)_2)->base + _the_map_p_13577);
    _2 = (int)SEQ_PTR(_7493);
    _7494 = (int)*(((s1_ptr)_2)->base + 7);
    _7493 = NOVALUE;
    _2 = (int)SEQ_PTR(_7494);
    _7495 = (int)*(((s1_ptr)_2)->base + _pos__13580);
    _7494 = NOVALUE;
    if (binary_op_a(NOTEQ, _7495, 1)){
        _7495 = NOVALUE;
        goto L7; // [159] 180
    }
    _7495 = NOVALUE;

    /** 						return 1*/
    DeRef(_the_key_p_13578);
    _7482 = NOVALUE;
    return 1;
    goto L7; // [170] 180
L6: 

    /** 					return 0*/
    DeRef(_the_key_p_13578);
    _7482 = NOVALUE;
    return 0;
L7: 

    /** 				from_ = pos_ + 1*/
    _from__13581 = _pos__13580 + 1;

    /** 			end while*/
    goto L4; // [188] 113
    goto L5; // [191] 214
L3: 

    /** 			pos_ = find(the_key_p, eumem:ram_space[the_map_p][KEY_LIST])*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    _7498 = (int)*(((s1_ptr)_2)->base + _the_map_p_13577);
    _2 = (int)SEQ_PTR(_7498);
    _7499 = (int)*(((s1_ptr)_2)->base + 5);
    _7498 = NOVALUE;
    _pos__13580 = find_from(_the_key_p_13578, _7499, 1);
    _7499 = NOVALUE;
L5: 
L2: 

    /** 	return (pos_  != 0)	*/
    _7501 = (_pos__13580 != 0);
    DeRef(_the_key_p_13578);
    _7482 = NOVALUE;
    return _7501;
    ;
}


int _32get(int _the_map_p_13621, int _the_key_p_13622, int _default_value_p_13623)
{
    int _bucket__13624 = NOVALUE;
    int _pos__13625 = NOVALUE;
    int _from__13626 = NOVALUE;
    int _themap_13627 = NOVALUE;
    int _thekeys_13632 = NOVALUE;
    int _calc_hash_1__tmp_at44_13639 = NOVALUE;
    int _calc_hash_inlined_calc_hash_at_44_13638 = NOVALUE;
    int _ret__inlined_calc_hash_at_44_13637 = NOVALUE;
    int _max_hash_p_inlined_calc_hash_at_41_13636 = NOVALUE;
    int _7526 = NOVALUE;
    int _7525 = NOVALUE;
    int _7523 = NOVALUE;
    int _7521 = NOVALUE;
    int _7520 = NOVALUE;
    int _7518 = NOVALUE;
    int _7517 = NOVALUE;
    int _7515 = NOVALUE;
    int _7513 = NOVALUE;
    int _7512 = NOVALUE;
    int _7511 = NOVALUE;
    int _7510 = NOVALUE;
    int _7507 = NOVALUE;
    int _7506 = NOVALUE;
    int _7503 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_the_map_p_13621)) {
        _1 = (long)(DBL_PTR(_the_map_p_13621)->dbl);
        if (UNIQUE(DBL_PTR(_the_map_p_13621)) && (DBL_PTR(_the_map_p_13621)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map_p_13621);
        _the_map_p_13621 = _1;
    }

    /** 	themap = eumem:ram_space[the_map_p]*/
    DeRef(_themap_13627);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    _themap_13627 = (int)*(((s1_ptr)_2)->base + _the_map_p_13621);
    Ref(_themap_13627);

    /** 	if themap[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_themap_13627);
    _7503 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7503, 76)){
        _7503 = NOVALUE;
        goto L1; // [21] 121
    }
    _7503 = NOVALUE;

    /** 		sequence thekeys*/

    /** 		thekeys = themap[KEY_BUCKETS]*/
    DeRef(_thekeys_13632);
    _2 = (int)SEQ_PTR(_themap_13627);
    _thekeys_13632 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_thekeys_13632);

    /** 		bucket_ = calc_hash(the_key_p, length(thekeys))*/
    if (IS_SEQUENCE(_thekeys_13632)){
            _7506 = SEQ_PTR(_thekeys_13632)->length;
    }
    else {
        _7506 = 1;
    }
    _max_hash_p_inlined_calc_hash_at_41_13636 = _7506;
    _7506 = NOVALUE;

    /**     ret_ = hash(key_p, stdhash:HSIEH30)*/
    DeRef(_ret__inlined_calc_hash_at_44_13637);
    _ret__inlined_calc_hash_at_44_13637 = calc_hash(_the_key_p_13622, -6);
    if (!IS_ATOM_INT(_ret__inlined_calc_hash_at_44_13637)) {
        _1 = (long)(DBL_PTR(_ret__inlined_calc_hash_at_44_13637)->dbl);
        if (UNIQUE(DBL_PTR(_ret__inlined_calc_hash_at_44_13637)) && (DBL_PTR(_ret__inlined_calc_hash_at_44_13637)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret__inlined_calc_hash_at_44_13637);
        _ret__inlined_calc_hash_at_44_13637 = _1;
    }

    /** 	return remainder(ret_, max_hash_p) + 1 -- 1-based*/
    _calc_hash_1__tmp_at44_13639 = (_ret__inlined_calc_hash_at_44_13637 % _max_hash_p_inlined_calc_hash_at_41_13636);
    _bucket__13624 = _calc_hash_1__tmp_at44_13639 + 1;
    DeRef(_ret__inlined_calc_hash_at_44_13637);
    _ret__inlined_calc_hash_at_44_13637 = NOVALUE;
    if (!IS_ATOM_INT(_bucket__13624)) {
        _1 = (long)(DBL_PTR(_bucket__13624)->dbl);
        if (UNIQUE(DBL_PTR(_bucket__13624)) && (DBL_PTR(_bucket__13624)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bucket__13624);
        _bucket__13624 = _1;
    }

    /** 		pos_ = find(the_key_p, thekeys[bucket_])*/
    _2 = (int)SEQ_PTR(_thekeys_13632);
    _7507 = (int)*(((s1_ptr)_2)->base + _bucket__13624);
    _pos__13625 = find_from(_the_key_p_13622, _7507, 1);
    _7507 = NOVALUE;

    /** 		if pos_ > 0 then*/
    if (_pos__13625 <= 0)
    goto L2; // [85] 110

    /** 			return themap[VALUE_BUCKETS][bucket_][pos_]*/
    _2 = (int)SEQ_PTR(_themap_13627);
    _7510 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_7510);
    _7511 = (int)*(((s1_ptr)_2)->base + _bucket__13624);
    _7510 = NOVALUE;
    _2 = (int)SEQ_PTR(_7511);
    _7512 = (int)*(((s1_ptr)_2)->base + _pos__13625);
    _7511 = NOVALUE;
    Ref(_7512);
    DeRefDS(_thekeys_13632);
    DeRef(_the_key_p_13622);
    DeRef(_default_value_p_13623);
    DeRefDS(_themap_13627);
    return _7512;
L2: 

    /** 		return default_value_p*/
    DeRef(_thekeys_13632);
    DeRef(_the_key_p_13622);
    DeRef(_themap_13627);
    _7512 = NOVALUE;
    return _default_value_p_13623;
    goto L3; // [118] 256
L1: 

    /** 		if equal(the_key_p, init_small_map_key) then*/
    if (_the_key_p_13622 == _32init_small_map_key_13319)
    _7513 = 1;
    else if (IS_ATOM_INT(_the_key_p_13622) && IS_ATOM_INT(_32init_small_map_key_13319))
    _7513 = 0;
    else
    _7513 = (compare(_the_key_p_13622, _32init_small_map_key_13319) == 0);
    if (_7513 == 0)
    {
        _7513 = NOVALUE;
        goto L4; // [127] 219
    }
    else{
        _7513 = NOVALUE;
    }

    /** 			from_ = 1*/
    _from__13626 = 1;

    /** 			while from_ > 0 do*/
L5: 
    if (_from__13626 <= 0)
    goto L6; // [140] 255

    /** 				pos_ = find(the_key_p, themap[KEY_LIST], from_)*/
    _2 = (int)SEQ_PTR(_themap_13627);
    _7515 = (int)*(((s1_ptr)_2)->base + 5);
    _pos__13625 = find_from(_the_key_p_13622, _7515, _from__13626);
    _7515 = NOVALUE;

    /** 				if pos_ then*/
    if (_pos__13625 == 0)
    {
        goto L7; // [159] 198
    }
    else{
    }

    /** 					if themap[FREE_LIST][pos_] = 1 then*/
    _2 = (int)SEQ_PTR(_themap_13627);
    _7517 = (int)*(((s1_ptr)_2)->base + 7);
    _2 = (int)SEQ_PTR(_7517);
    _7518 = (int)*(((s1_ptr)_2)->base + _pos__13625);
    _7517 = NOVALUE;
    if (binary_op_a(NOTEQ, _7518, 1)){
        _7518 = NOVALUE;
        goto L8; // [174] 205
    }
    _7518 = NOVALUE;

    /** 						return themap[VALUE_LIST][pos_]*/
    _2 = (int)SEQ_PTR(_themap_13627);
    _7520 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_7520);
    _7521 = (int)*(((s1_ptr)_2)->base + _pos__13625);
    _7520 = NOVALUE;
    Ref(_7521);
    DeRef(_the_key_p_13622);
    DeRef(_default_value_p_13623);
    DeRefDS(_themap_13627);
    _7512 = NOVALUE;
    return _7521;
    goto L8; // [195] 205
L7: 

    /** 					return default_value_p*/
    DeRef(_the_key_p_13622);
    DeRef(_themap_13627);
    _7512 = NOVALUE;
    _7521 = NOVALUE;
    return _default_value_p_13623;
L8: 

    /** 				from_ = pos_ + 1*/
    _from__13626 = _pos__13625 + 1;

    /** 			end while*/
    goto L5; // [213] 140
    goto L6; // [216] 255
L4: 

    /** 			pos_ = find(the_key_p, themap[KEY_LIST])*/
    _2 = (int)SEQ_PTR(_themap_13627);
    _7523 = (int)*(((s1_ptr)_2)->base + 5);
    _pos__13625 = find_from(_the_key_p_13622, _7523, 1);
    _7523 = NOVALUE;

    /** 			if pos_  then*/
    if (_pos__13625 == 0)
    {
        goto L9; // [234] 254
    }
    else{
    }

    /** 				return themap[VALUE_LIST][pos_]*/
    _2 = (int)SEQ_PTR(_themap_13627);
    _7525 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_7525);
    _7526 = (int)*(((s1_ptr)_2)->base + _pos__13625);
    _7525 = NOVALUE;
    Ref(_7526);
    DeRef(_the_key_p_13622);
    DeRef(_default_value_p_13623);
    DeRefDS(_themap_13627);
    _7512 = NOVALUE;
    _7521 = NOVALUE;
    return _7526;
L9: 
L6: 
L3: 

    /** 	return default_value_p*/
    DeRef(_the_key_p_13622);
    DeRef(_themap_13627);
    _7512 = NOVALUE;
    _7521 = NOVALUE;
    _7526 = NOVALUE;
    return _default_value_p_13623;
    ;
}


int _32nested_get(int _the_map_p_13671, int _the_keys_p_13672, int _default_value_p_13673)
{
    int _val__13678 = NOVALUE;
    int _7536 = NOVALUE;
    int _7535 = NOVALUE;
    int _7533 = NOVALUE;
    int _7531 = NOVALUE;
    int _7529 = NOVALUE;
    int _7528 = NOVALUE;
    int _7527 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length( the_keys_p ) - 1 do*/
    if (IS_SEQUENCE(_the_keys_p_13672)){
            _7527 = SEQ_PTR(_the_keys_p_13672)->length;
    }
    else {
        _7527 = 1;
    }
    _7528 = _7527 - 1;
    _7527 = NOVALUE;
    {
        int _i_13675;
        _i_13675 = 1;
L1: 
        if (_i_13675 > _7528){
            goto L2; // [12] 74
        }

        /** 		object val_ = get( the_map_p, the_keys_p[1], 0 )*/
        _2 = (int)SEQ_PTR(_the_keys_p_13672);
        _7529 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_the_map_p_13671);
        Ref(_7529);
        _0 = _val__13678;
        _val__13678 = _32get(_the_map_p_13671, _7529, 0);
        DeRef(_0);
        _7529 = NOVALUE;

        /** 		if not map( val_ ) then*/
        Ref(_val__13678);
        _7531 = _32map(_val__13678);
        if (IS_ATOM_INT(_7531)) {
            if (_7531 != 0){
                DeRef(_7531);
                _7531 = NOVALUE;
                goto L3; // [37] 49
            }
        }
        else {
            if (DBL_PTR(_7531)->dbl != 0.0){
                DeRef(_7531);
                _7531 = NOVALUE;
                goto L3; // [37] 49
            }
        }
        DeRef(_7531);
        _7531 = NOVALUE;

        /** 			return default_value_p*/
        DeRef(_val__13678);
        DeRef(_the_map_p_13671);
        DeRefDS(_the_keys_p_13672);
        DeRef(_7528);
        _7528 = NOVALUE;
        return _default_value_p_13673;
        goto L4; // [46] 65
L3: 

        /** 			the_map_p = val_*/
        Ref(_val__13678);
        DeRef(_the_map_p_13671);
        _the_map_p_13671 = _val__13678;

        /** 			the_keys_p = the_keys_p[2..$]*/
        if (IS_SEQUENCE(_the_keys_p_13672)){
                _7533 = SEQ_PTR(_the_keys_p_13672)->length;
        }
        else {
            _7533 = 1;
        }
        rhs_slice_target = (object_ptr)&_the_keys_p_13672;
        RHS_Slice(_the_keys_p_13672, 2, _7533);
L4: 
        DeRef(_val__13678);
        _val__13678 = NOVALUE;

        /** 	end for*/
        _i_13675 = _i_13675 + 1;
        goto L1; // [69] 19
L2: 
        ;
    }

    /** 	return get( the_map_p, the_keys_p[1], default_value_p )*/
    _2 = (int)SEQ_PTR(_the_keys_p_13672);
    _7535 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_the_map_p_13671);
    Ref(_7535);
    Ref(_default_value_p_13673);
    _7536 = _32get(_the_map_p_13671, _7535, _default_value_p_13673);
    _7535 = NOVALUE;
    DeRef(_the_map_p_13671);
    DeRefDS(_the_keys_p_13672);
    DeRef(_default_value_p_13673);
    DeRef(_7528);
    _7528 = NOVALUE;
    return _7536;
    ;
}


void _32put(int _the_map_p_13691, int _the_key_p_13692, int _the_value_p_13693, int _operation_p_13694, int _trigger_p_13695)
{
    int _index__13696 = NOVALUE;
    int _bucket__13697 = NOVALUE;
    int _average_length__13698 = NOVALUE;
    int _from__13699 = NOVALUE;
    int _map_data_13700 = NOVALUE;
    int _calc_hash_1__tmp_at50_13711 = NOVALUE;
    int _calc_hash_inlined_calc_hash_at_50_13710 = NOVALUE;
    int _ret__inlined_calc_hash_at_50_13709 = NOVALUE;
    int _max_hash_p_inlined_calc_hash_at_47_13708 = NOVALUE;
    int _data_13743 = NOVALUE;
    int _msg_inlined_crash_at_358_13759 = NOVALUE;
    int _msg_inlined_crash_at_405_13765 = NOVALUE;
    int _tmp_seqk_13779 = NOVALUE;
    int _tmp_seqv_13787 = NOVALUE;
    int _msg_inlined_crash_at_781_13823 = NOVALUE;
    int _msg_inlined_crash_at_1173_13886 = NOVALUE;
    int _7670 = NOVALUE;
    int _7669 = NOVALUE;
    int _7667 = NOVALUE;
    int _7666 = NOVALUE;
    int _7665 = NOVALUE;
    int _7664 = NOVALUE;
    int _7662 = NOVALUE;
    int _7661 = NOVALUE;
    int _7660 = NOVALUE;
    int _7658 = NOVALUE;
    int _7657 = NOVALUE;
    int _7656 = NOVALUE;
    int _7654 = NOVALUE;
    int _7653 = NOVALUE;
    int _7652 = NOVALUE;
    int _7650 = NOVALUE;
    int _7649 = NOVALUE;
    int _7648 = NOVALUE;
    int _7646 = NOVALUE;
    int _7644 = NOVALUE;
    int _7638 = NOVALUE;
    int _7637 = NOVALUE;
    int _7636 = NOVALUE;
    int _7635 = NOVALUE;
    int _7633 = NOVALUE;
    int _7631 = NOVALUE;
    int _7630 = NOVALUE;
    int _7629 = NOVALUE;
    int _7628 = NOVALUE;
    int _7627 = NOVALUE;
    int _7626 = NOVALUE;
    int _7623 = NOVALUE;
    int _7621 = NOVALUE;
    int _7618 = NOVALUE;
    int _7616 = NOVALUE;
    int _7613 = NOVALUE;
    int _7612 = NOVALUE;
    int _7610 = NOVALUE;
    int _7607 = NOVALUE;
    int _7606 = NOVALUE;
    int _7603 = NOVALUE;
    int _7600 = NOVALUE;
    int _7598 = NOVALUE;
    int _7596 = NOVALUE;
    int _7593 = NOVALUE;
    int _7591 = NOVALUE;
    int _7590 = NOVALUE;
    int _7589 = NOVALUE;
    int _7588 = NOVALUE;
    int _7587 = NOVALUE;
    int _7586 = NOVALUE;
    int _7585 = NOVALUE;
    int _7584 = NOVALUE;
    int _7583 = NOVALUE;
    int _7577 = NOVALUE;
    int _7575 = NOVALUE;
    int _7574 = NOVALUE;
    int _7572 = NOVALUE;
    int _7570 = NOVALUE;
    int _7567 = NOVALUE;
    int _7566 = NOVALUE;
    int _7565 = NOVALUE;
    int _7564 = NOVALUE;
    int _7562 = NOVALUE;
    int _7561 = NOVALUE;
    int _7560 = NOVALUE;
    int _7558 = NOVALUE;
    int _7557 = NOVALUE;
    int _7556 = NOVALUE;
    int _7554 = NOVALUE;
    int _7553 = NOVALUE;
    int _7552 = NOVALUE;
    int _7550 = NOVALUE;
    int _7548 = NOVALUE;
    int _7543 = NOVALUE;
    int _7542 = NOVALUE;
    int _7541 = NOVALUE;
    int _7540 = NOVALUE;
    int _7538 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_the_map_p_13691)) {
        _1 = (long)(DBL_PTR(_the_map_p_13691)->dbl);
        if (UNIQUE(DBL_PTR(_the_map_p_13691)) && (DBL_PTR(_the_map_p_13691)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map_p_13691);
        _the_map_p_13691 = _1;
    }
    if (!IS_ATOM_INT(_operation_p_13694)) {
        _1 = (long)(DBL_PTR(_operation_p_13694)->dbl);
        if (UNIQUE(DBL_PTR(_operation_p_13694)) && (DBL_PTR(_operation_p_13694)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_operation_p_13694);
        _operation_p_13694 = _1;
    }
    if (!IS_ATOM_INT(_trigger_p_13695)) {
        _1 = (long)(DBL_PTR(_trigger_p_13695)->dbl);
        if (UNIQUE(DBL_PTR(_trigger_p_13695)) && (DBL_PTR(_trigger_p_13695)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_trigger_p_13695);
        _trigger_p_13695 = _1;
    }

    /** 	sequence map_data = eumem:ram_space[the_map_p]*/
    DeRef(_map_data_13700);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    _map_data_13700 = (int)*(((s1_ptr)_2)->base + _the_map_p_13691);
    Ref(_map_data_13700);

    /** 	eumem:ram_space[the_map_p] = 0*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _33ram_space_12557 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_13691);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	if map_data[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_map_data_13700);
    _7538 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7538, 76)){
        _7538 = NOVALUE;
        goto L1; // [33] 670
    }
    _7538 = NOVALUE;

    /** 		bucket_ = calc_hash(the_key_p,  length(map_data[KEY_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_map_data_13700);
    _7540 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7540)){
            _7541 = SEQ_PTR(_7540)->length;
    }
    else {
        _7541 = 1;
    }
    _7540 = NOVALUE;
    _max_hash_p_inlined_calc_hash_at_47_13708 = _7541;
    _7541 = NOVALUE;

    /**     ret_ = hash(key_p, stdhash:HSIEH30)*/
    DeRef(_ret__inlined_calc_hash_at_50_13709);
    _ret__inlined_calc_hash_at_50_13709 = calc_hash(_the_key_p_13692, -6);
    if (!IS_ATOM_INT(_ret__inlined_calc_hash_at_50_13709)) {
        _1 = (long)(DBL_PTR(_ret__inlined_calc_hash_at_50_13709)->dbl);
        if (UNIQUE(DBL_PTR(_ret__inlined_calc_hash_at_50_13709)) && (DBL_PTR(_ret__inlined_calc_hash_at_50_13709)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret__inlined_calc_hash_at_50_13709);
        _ret__inlined_calc_hash_at_50_13709 = _1;
    }

    /** 	return remainder(ret_, max_hash_p) + 1 -- 1-based*/
    _calc_hash_1__tmp_at50_13711 = (_ret__inlined_calc_hash_at_50_13709 % _max_hash_p_inlined_calc_hash_at_47_13708);
    _bucket__13697 = _calc_hash_1__tmp_at50_13711 + 1;
    DeRef(_ret__inlined_calc_hash_at_50_13709);
    _ret__inlined_calc_hash_at_50_13709 = NOVALUE;
    if (!IS_ATOM_INT(_bucket__13697)) {
        _1 = (long)(DBL_PTR(_bucket__13697)->dbl);
        if (UNIQUE(DBL_PTR(_bucket__13697)) && (DBL_PTR(_bucket__13697)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bucket__13697);
        _bucket__13697 = _1;
    }

    /** 		index_ = find(the_key_p, map_data[KEY_BUCKETS][bucket_])*/
    _2 = (int)SEQ_PTR(_map_data_13700);
    _7542 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_7542);
    _7543 = (int)*(((s1_ptr)_2)->base + _bucket__13697);
    _7542 = NOVALUE;
    _index__13696 = find_from(_the_key_p_13692, _7543, 1);
    _7543 = NOVALUE;

    /** 		if index_ > 0 then*/
    if (_index__13696 <= 0)
    goto L2; // [97] 392

    /** 			switch operation_p do*/
    _0 = _operation_p_13694;
    switch ( _0 ){ 

        /** 				case PUT then*/
        case 1:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] = the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_13700);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_13700 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__13697 + ((s1_ptr)_2)->base);
        _7548 = NOVALUE;
        Ref(_the_value_p_13693);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__13696);
        _1 = *(int *)_2;
        *(int *)_2 = _the_value_p_13693;
        DeRef(_1);
        _7548 = NOVALUE;
        goto L3; // [130] 378

        /** 				case ADD then*/
        case 2:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] += the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_13700);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_13700 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__13697 + ((s1_ptr)_2)->base);
        _7550 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _7552 = (int)*(((s1_ptr)_2)->base + _index__13696);
        _7550 = NOVALUE;
        if (IS_ATOM_INT(_7552) && IS_ATOM_INT(_the_value_p_13693)) {
            _7553 = _7552 + _the_value_p_13693;
            if ((long)((unsigned long)_7553 + (unsigned long)HIGH_BITS) >= 0) 
            _7553 = NewDouble((double)_7553);
        }
        else {
            _7553 = binary_op(PLUS, _7552, _the_value_p_13693);
        }
        _7552 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__13696);
        _1 = *(int *)_2;
        *(int *)_2 = _7553;
        if( _1 != _7553 ){
            DeRef(_1);
        }
        _7553 = NOVALUE;
        _7550 = NOVALUE;
        goto L3; // [162] 378

        /** 				case SUBTRACT then*/
        case 3:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] -= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_13700);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_13700 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__13697 + ((s1_ptr)_2)->base);
        _7554 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _7556 = (int)*(((s1_ptr)_2)->base + _index__13696);
        _7554 = NOVALUE;
        if (IS_ATOM_INT(_7556) && IS_ATOM_INT(_the_value_p_13693)) {
            _7557 = _7556 - _the_value_p_13693;
            if ((long)((unsigned long)_7557 +(unsigned long) HIGH_BITS) >= 0){
                _7557 = NewDouble((double)_7557);
            }
        }
        else {
            _7557 = binary_op(MINUS, _7556, _the_value_p_13693);
        }
        _7556 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__13696);
        _1 = *(int *)_2;
        *(int *)_2 = _7557;
        if( _1 != _7557 ){
            DeRef(_1);
        }
        _7557 = NOVALUE;
        _7554 = NOVALUE;
        goto L3; // [194] 378

        /** 				case MULTIPLY then*/
        case 4:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] *= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_13700);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_13700 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__13697 + ((s1_ptr)_2)->base);
        _7558 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _7560 = (int)*(((s1_ptr)_2)->base + _index__13696);
        _7558 = NOVALUE;
        if (IS_ATOM_INT(_7560) && IS_ATOM_INT(_the_value_p_13693)) {
            if (_7560 == (short)_7560 && _the_value_p_13693 <= INT15 && _the_value_p_13693 >= -INT15)
            _7561 = _7560 * _the_value_p_13693;
            else
            _7561 = NewDouble(_7560 * (double)_the_value_p_13693);
        }
        else {
            _7561 = binary_op(MULTIPLY, _7560, _the_value_p_13693);
        }
        _7560 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__13696);
        _1 = *(int *)_2;
        *(int *)_2 = _7561;
        if( _1 != _7561 ){
            DeRef(_1);
        }
        _7561 = NOVALUE;
        _7558 = NOVALUE;
        goto L3; // [226] 378

        /** 				case DIVIDE then*/
        case 5:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] /= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_13700);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_13700 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__13697 + ((s1_ptr)_2)->base);
        _7562 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _7564 = (int)*(((s1_ptr)_2)->base + _index__13696);
        _7562 = NOVALUE;
        if (IS_ATOM_INT(_7564) && IS_ATOM_INT(_the_value_p_13693)) {
            _7565 = (_7564 % _the_value_p_13693) ? NewDouble((double)_7564 / _the_value_p_13693) : (_7564 / _the_value_p_13693);
        }
        else {
            _7565 = binary_op(DIVIDE, _7564, _the_value_p_13693);
        }
        _7564 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__13696);
        _1 = *(int *)_2;
        *(int *)_2 = _7565;
        if( _1 != _7565 ){
            DeRef(_1);
        }
        _7565 = NOVALUE;
        _7562 = NOVALUE;
        goto L3; // [258] 378

        /** 				case APPEND then*/
        case 6:

        /** 					sequence data = map_data[VALUE_BUCKETS][bucket_][index_]*/
        _2 = (int)SEQ_PTR(_map_data_13700);
        _7566 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_7566);
        _7567 = (int)*(((s1_ptr)_2)->base + _bucket__13697);
        _7566 = NOVALUE;
        DeRef(_data_13743);
        _2 = (int)SEQ_PTR(_7567);
        _data_13743 = (int)*(((s1_ptr)_2)->base + _index__13696);
        Ref(_data_13743);
        _7567 = NOVALUE;

        /** 					data = append( data, the_value_p )*/
        Ref(_the_value_p_13693);
        Append(&_data_13743, _data_13743, _the_value_p_13693);

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] = data*/
        _2 = (int)SEQ_PTR(_map_data_13700);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_13700 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__13697 + ((s1_ptr)_2)->base);
        _7570 = NOVALUE;
        RefDS(_data_13743);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__13696);
        _1 = *(int *)_2;
        *(int *)_2 = _data_13743;
        DeRef(_1);
        _7570 = NOVALUE;
        DeRefDS(_data_13743);
        _data_13743 = NOVALUE;
        goto L3; // [308] 378

        /** 				case CONCAT then*/
        case 7:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] &= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_13700);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_13700 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__13697 + ((s1_ptr)_2)->base);
        _7572 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _7574 = (int)*(((s1_ptr)_2)->base + _index__13696);
        _7572 = NOVALUE;
        if (IS_SEQUENCE(_7574) && IS_ATOM(_the_value_p_13693)) {
            Ref(_the_value_p_13693);
            Append(&_7575, _7574, _the_value_p_13693);
        }
        else if (IS_ATOM(_7574) && IS_SEQUENCE(_the_value_p_13693)) {
            Ref(_7574);
            Prepend(&_7575, _the_value_p_13693, _7574);
        }
        else {
            Concat((object_ptr)&_7575, _7574, _the_value_p_13693);
            _7574 = NOVALUE;
        }
        _7574 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__13696);
        _1 = *(int *)_2;
        *(int *)_2 = _7575;
        if( _1 != _7575 ){
            DeRef(_1);
        }
        _7575 = NOVALUE;
        _7572 = NOVALUE;
        goto L3; // [340] 378

        /** 				case LEAVE then*/
        case 8:

        /** 					operation_p = operation_p*/
        _operation_p_13694 = _operation_p_13694;
        goto L3; // [351] 378

        /** 				case else*/
        default:

        /** 					error:crash("Unknown operation given to map.e:put()")*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_358_13759);
        _msg_inlined_crash_at_358_13759 = EPrintf(-9999999, _7576, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_358_13759);

        /** end procedure*/
        goto L4; // [372] 375
L4: 
        DeRefi(_msg_inlined_crash_at_358_13759);
        _msg_inlined_crash_at_358_13759 = NOVALUE;
    ;}L3: 

    /** 			eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_13700);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _33ram_space_12557 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_13691);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_13700;
    DeRef(_1);

    /** 			return*/
    DeRef(_tmp_seqk_13779);
    DeRef(_tmp_seqv_13787);
    DeRef(_the_key_p_13692);
    DeRef(_the_value_p_13693);
    DeRef(_average_length__13698);
    DeRefDS(_map_data_13700);
    _7540 = NOVALUE;
    return;
L2: 

    /** 		if not eu:find(operation_p, INIT_OPERATIONS) then*/
    _7577 = find_from(_operation_p_13694, _32INIT_OPERATIONS_13314, 1);
    if (_7577 != 0)
    goto L5; // [401] 425
    _7577 = NOVALUE;

    /** 				error:crash("Inappropriate initial operation given to map.e:put()")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_405_13765);
    _msg_inlined_crash_at_405_13765 = EPrintf(-9999999, _7579, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_405_13765);

    /** end procedure*/
    goto L6; // [419] 422
L6: 
    DeRefi(_msg_inlined_crash_at_405_13765);
    _msg_inlined_crash_at_405_13765 = NOVALUE;
L5: 

    /** 		if operation_p = LEAVE then*/
    if (_operation_p_13694 != 8)
    goto L7; // [429] 447

    /** 			eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_13700);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _33ram_space_12557 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_13691);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_13700;
    DeRef(_1);

    /** 			return*/
    DeRef(_tmp_seqk_13779);
    DeRef(_tmp_seqv_13787);
    DeRef(_the_key_p_13692);
    DeRef(_the_value_p_13693);
    DeRef(_average_length__13698);
    DeRefDS(_map_data_13700);
    _7540 = NOVALUE;
    return;
L7: 

    /** 		if operation_p = APPEND then*/
    if (_operation_p_13694 != 6)
    goto L8; // [451] 462

    /** 			the_value_p = { the_value_p }*/
    _0 = _the_value_p_13693;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_the_value_p_13693);
    *((int *)(_2+4)) = _the_value_p_13693;
    _the_value_p_13693 = MAKE_SEQ(_1);
    DeRef(_0);
L8: 

    /** 		map_data[IN_USE] += (length(map_data[KEY_BUCKETS][bucket_]) = 0)*/
    _2 = (int)SEQ_PTR(_map_data_13700);
    _7583 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_7583);
    _7584 = (int)*(((s1_ptr)_2)->base + _bucket__13697);
    _7583 = NOVALUE;
    if (IS_SEQUENCE(_7584)){
            _7585 = SEQ_PTR(_7584)->length;
    }
    else {
        _7585 = 1;
    }
    _7584 = NOVALUE;
    _7586 = (_7585 == 0);
    _7585 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_13700);
    _7587 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_7587)) {
        _7588 = _7587 + _7586;
        if ((long)((unsigned long)_7588 + (unsigned long)HIGH_BITS) >= 0) 
        _7588 = NewDouble((double)_7588);
    }
    else {
        _7588 = binary_op(PLUS, _7587, _7586);
    }
    _7587 = NOVALUE;
    _7586 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_13700);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_13700 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _7588;
    if( _1 != _7588 ){
        DeRef(_1);
    }
    _7588 = NOVALUE;

    /** 		map_data[ELEMENT_COUNT] += 1 -- elementCount		*/
    _2 = (int)SEQ_PTR(_map_data_13700);
    _7589 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_7589)) {
        _7590 = _7589 + 1;
        if (_7590 > MAXINT){
            _7590 = NewDouble((double)_7590);
        }
    }
    else
    _7590 = binary_op(PLUS, 1, _7589);
    _7589 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_13700);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_13700 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _7590;
    if( _1 != _7590 ){
        DeRef(_1);
    }
    _7590 = NOVALUE;

    /** 		sequence tmp_seqk*/

    /** 		tmp_seqk = map_data[KEY_BUCKETS][bucket_]*/
    _2 = (int)SEQ_PTR(_map_data_13700);
    _7591 = (int)*(((s1_ptr)_2)->base + 5);
    DeRef(_tmp_seqk_13779);
    _2 = (int)SEQ_PTR(_7591);
    _tmp_seqk_13779 = (int)*(((s1_ptr)_2)->base + _bucket__13697);
    Ref(_tmp_seqk_13779);
    _7591 = NOVALUE;

    /** 		map_data[KEY_BUCKETS][bucket_] = 0*/
    _2 = (int)SEQ_PTR(_map_data_13700);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_13700 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__13697);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _7593 = NOVALUE;

    /** 		tmp_seqk = append( tmp_seqk, the_key_p)*/
    Ref(_the_key_p_13692);
    Append(&_tmp_seqk_13779, _tmp_seqk_13779, _the_key_p_13692);

    /** 		map_data[KEY_BUCKETS][bucket_] = tmp_seqk*/
    _2 = (int)SEQ_PTR(_map_data_13700);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_13700 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    RefDS(_tmp_seqk_13779);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__13697);
    _1 = *(int *)_2;
    *(int *)_2 = _tmp_seqk_13779;
    DeRef(_1);
    _7596 = NOVALUE;

    /** 		sequence tmp_seqv*/

    /** 		tmp_seqv = map_data[VALUE_BUCKETS][bucket_]*/
    _2 = (int)SEQ_PTR(_map_data_13700);
    _7598 = (int)*(((s1_ptr)_2)->base + 6);
    DeRef(_tmp_seqv_13787);
    _2 = (int)SEQ_PTR(_7598);
    _tmp_seqv_13787 = (int)*(((s1_ptr)_2)->base + _bucket__13697);
    Ref(_tmp_seqv_13787);
    _7598 = NOVALUE;

    /** 		map_data[VALUE_BUCKETS][bucket_] = 0*/
    _2 = (int)SEQ_PTR(_map_data_13700);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_13700 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__13697);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _7600 = NOVALUE;

    /** 		tmp_seqv = append( tmp_seqv, the_value_p)*/
    Ref(_the_value_p_13693);
    Append(&_tmp_seqv_13787, _tmp_seqv_13787, _the_value_p_13693);

    /** 		map_data[VALUE_BUCKETS][bucket_] = tmp_seqv*/
    _2 = (int)SEQ_PTR(_map_data_13700);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_13700 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    RefDS(_tmp_seqv_13787);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__13697);
    _1 = *(int *)_2;
    *(int *)_2 = _tmp_seqv_13787;
    DeRef(_1);
    _7603 = NOVALUE;

    /** 		eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_13700);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _33ram_space_12557 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_13691);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_13700;
    DeRef(_1);

    /** 		if trigger_p > 0 then*/
    if (_trigger_p_13695 <= 0)
    goto L9; // [617] 660

    /** 			average_length_ = map_data[ELEMENT_COUNT] / map_data[IN_USE]*/
    _2 = (int)SEQ_PTR(_map_data_13700);
    _7606 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_map_data_13700);
    _7607 = (int)*(((s1_ptr)_2)->base + 3);
    DeRef(_average_length__13698);
    if (IS_ATOM_INT(_7606) && IS_ATOM_INT(_7607)) {
        _average_length__13698 = (_7606 % _7607) ? NewDouble((double)_7606 / _7607) : (_7606 / _7607);
    }
    else {
        _average_length__13698 = binary_op(DIVIDE, _7606, _7607);
    }
    _7606 = NOVALUE;
    _7607 = NOVALUE;

    /** 			if (average_length_ >= trigger_p) then*/
    if (binary_op_a(LESS, _average_length__13698, _trigger_p_13695)){
        goto LA; // [641] 659
    }

    /** 				map_data = {}*/
    RefDS(_5);
    DeRefDS(_map_data_13700);
    _map_data_13700 = _5;

    /** 				rehash(the_map_p)*/
    _32rehash(_the_map_p_13691, 0);
LA: 
L9: 

    /** 		return*/
    DeRef(_tmp_seqk_13779);
    DeRef(_tmp_seqv_13787);
    DeRef(_the_key_p_13692);
    DeRef(_the_value_p_13693);
    DeRef(_average_length__13698);
    DeRef(_map_data_13700);
    _7540 = NOVALUE;
    _7584 = NOVALUE;
    return;
    goto LB; // [667] 1206
L1: 

    /** 		if equal(the_key_p, init_small_map_key) then*/
    if (_the_key_p_13692 == _32init_small_map_key_13319)
    _7610 = 1;
    else if (IS_ATOM_INT(_the_key_p_13692) && IS_ATOM_INT(_32init_small_map_key_13319))
    _7610 = 0;
    else
    _7610 = (compare(_the_key_p_13692, _32init_small_map_key_13319) == 0);
    if (_7610 == 0)
    {
        _7610 = NOVALUE;
        goto LC; // [676] 746
    }
    else{
        _7610 = NOVALUE;
    }

    /** 			from_ = 1*/
    _from__13699 = 1;

    /** 			while index_ > 0 with entry do*/
    goto LD; // [686] 725
LE: 
    if (_index__13696 <= 0)
    goto LF; // [691] 760

    /** 				if map_data[FREE_LIST][index_] = 1 then*/
    _2 = (int)SEQ_PTR(_map_data_13700);
    _7612 = (int)*(((s1_ptr)_2)->base + 7);
    _2 = (int)SEQ_PTR(_7612);
    _7613 = (int)*(((s1_ptr)_2)->base + _index__13696);
    _7612 = NOVALUE;
    if (binary_op_a(NOTEQ, _7613, 1)){
        _7613 = NOVALUE;
        goto L10; // [707] 716
    }
    _7613 = NOVALUE;

    /** 					exit*/
    goto LF; // [713] 760
L10: 

    /** 				from_ = index_ + 1*/
    _from__13699 = _index__13696 + 1;

    /** 			  entry*/
LD: 

    /** 				index_ = find(the_key_p, map_data[KEY_LIST], from_)*/
    _2 = (int)SEQ_PTR(_map_data_13700);
    _7616 = (int)*(((s1_ptr)_2)->base + 5);
    _index__13696 = find_from(_the_key_p_13692, _7616, _from__13699);
    _7616 = NOVALUE;

    /** 			end while*/
    goto LE; // [740] 689
    goto LF; // [743] 760
LC: 

    /** 			index_ = find(the_key_p, map_data[KEY_LIST])*/
    _2 = (int)SEQ_PTR(_map_data_13700);
    _7618 = (int)*(((s1_ptr)_2)->base + 5);
    _index__13696 = find_from(_the_key_p_13692, _7618, 1);
    _7618 = NOVALUE;
LF: 

    /** 		if index_ = 0 then*/
    if (_index__13696 != 0)
    goto L11; // [764] 962

    /** 			if not eu:find(operation_p, INIT_OPERATIONS) then*/
    _7621 = find_from(_operation_p_13694, _32INIT_OPERATIONS_13314, 1);
    if (_7621 != 0)
    goto L12; // [777] 801
    _7621 = NOVALUE;

    /** 					error:crash("Inappropriate initial operation given to map.e:put()")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_781_13823);
    _msg_inlined_crash_at_781_13823 = EPrintf(-9999999, _7579, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_781_13823);

    /** end procedure*/
    goto L13; // [795] 798
L13: 
    DeRefi(_msg_inlined_crash_at_781_13823);
    _msg_inlined_crash_at_781_13823 = NOVALUE;
L12: 

    /** 			index_ = find(0, map_data[FREE_LIST])*/
    _2 = (int)SEQ_PTR(_map_data_13700);
    _7623 = (int)*(((s1_ptr)_2)->base + 7);
    _index__13696 = find_from(0, _7623, 1);
    _7623 = NOVALUE;

    /** 			if index_ = 0 then*/
    if (_index__13696 != 0)
    goto L14; // [816] 870

    /** 				eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_13700);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _33ram_space_12557 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_13691);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_13700;
    DeRef(_1);

    /** 				map_data = {}*/
    RefDS(_5);
    DeRefDS(_map_data_13700);
    _map_data_13700 = _5;

    /** 				convert_to_large_map(the_map_p)*/
    _32convert_to_large_map(_the_map_p_13691);

    /** 				put(the_map_p, the_key_p, the_value_p, operation_p, trigger_p)*/
    DeRef(_7626);
    _7626 = _the_map_p_13691;
    Ref(_the_key_p_13692);
    DeRef(_7627);
    _7627 = _the_key_p_13692;
    Ref(_the_value_p_13693);
    DeRef(_7628);
    _7628 = _the_value_p_13693;
    DeRef(_7629);
    _7629 = _operation_p_13694;
    DeRef(_7630);
    _7630 = _trigger_p_13695;
    _32put(_7626, _7627, _7628, _7629, _7630);
    _7626 = NOVALUE;
    _7627 = NOVALUE;
    _7628 = NOVALUE;
    _7629 = NOVALUE;
    _7630 = NOVALUE;

    /** 				return*/
    DeRef(_the_key_p_13692);
    DeRef(_the_value_p_13693);
    DeRef(_average_length__13698);
    DeRefDS(_map_data_13700);
    _7540 = NOVALUE;
    _7584 = NOVALUE;
    return;
L14: 

    /** 			map_data[KEY_LIST][index_] = the_key_p*/
    _2 = (int)SEQ_PTR(_map_data_13700);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_13700 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    Ref(_the_key_p_13692);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__13696);
    _1 = *(int *)_2;
    *(int *)_2 = _the_key_p_13692;
    DeRef(_1);
    _7631 = NOVALUE;

    /** 			map_data[FREE_LIST][index_] = 1*/
    _2 = (int)SEQ_PTR(_map_data_13700);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_13700 = MAKE_SEQ(_2);
    }
    _3 = (int)(7 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__13696);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _7633 = NOVALUE;

    /** 			map_data[IN_USE] += 1*/
    _2 = (int)SEQ_PTR(_map_data_13700);
    _7635 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_7635)) {
        _7636 = _7635 + 1;
        if (_7636 > MAXINT){
            _7636 = NewDouble((double)_7636);
        }
    }
    else
    _7636 = binary_op(PLUS, 1, _7635);
    _7635 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_13700);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_13700 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _7636;
    if( _1 != _7636 ){
        DeRef(_1);
    }
    _7636 = NOVALUE;

    /** 			map_data[ELEMENT_COUNT] += 1*/
    _2 = (int)SEQ_PTR(_map_data_13700);
    _7637 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_7637)) {
        _7638 = _7637 + 1;
        if (_7638 > MAXINT){
            _7638 = NewDouble((double)_7638);
        }
    }
    else
    _7638 = binary_op(PLUS, 1, _7637);
    _7637 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_13700);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_13700 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _7638;
    if( _1 != _7638 ){
        DeRef(_1);
    }
    _7638 = NOVALUE;

    /** 			if operation_p = APPEND then*/
    if (_operation_p_13694 != 6)
    goto L15; // [932] 943

    /** 				the_value_p = { the_value_p }*/
    _0 = _the_value_p_13693;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_the_value_p_13693);
    *((int *)(_2+4)) = _the_value_p_13693;
    _the_value_p_13693 = MAKE_SEQ(_1);
    DeRef(_0);
L15: 

    /** 			if operation_p != LEAVE then*/
    if (_operation_p_13694 == 8)
    goto L16; // [947] 961

    /** 				operation_p = PUT	-- Initially, nearly everything is a PUT.*/
    _operation_p_13694 = 1;
L16: 
L11: 

    /** 		switch operation_p do*/
    _0 = _operation_p_13694;
    switch ( _0 ){ 

        /** 			case PUT then*/
        case 1:

        /** 				map_data[VALUE_LIST][index_] = the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_13700);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_13700 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        Ref(_the_value_p_13693);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__13696);
        _1 = *(int *)_2;
        *(int *)_2 = _the_value_p_13693;
        DeRef(_1);
        _7644 = NOVALUE;
        goto L17; // [986] 1192

        /** 			case ADD then*/
        case 2:

        /** 				map_data[VALUE_LIST][index_] += the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_13700);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_13700 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _7648 = (int)*(((s1_ptr)_2)->base + _index__13696);
        _7646 = NOVALUE;
        if (IS_ATOM_INT(_7648) && IS_ATOM_INT(_the_value_p_13693)) {
            _7649 = _7648 + _the_value_p_13693;
            if ((long)((unsigned long)_7649 + (unsigned long)HIGH_BITS) >= 0) 
            _7649 = NewDouble((double)_7649);
        }
        else {
            _7649 = binary_op(PLUS, _7648, _the_value_p_13693);
        }
        _7648 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__13696);
        _1 = *(int *)_2;
        *(int *)_2 = _7649;
        if( _1 != _7649 ){
            DeRef(_1);
        }
        _7649 = NOVALUE;
        _7646 = NOVALUE;
        goto L17; // [1013] 1192

        /** 			case SUBTRACT then*/
        case 3:

        /** 				map_data[VALUE_LIST][index_] -= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_13700);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_13700 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _7652 = (int)*(((s1_ptr)_2)->base + _index__13696);
        _7650 = NOVALUE;
        if (IS_ATOM_INT(_7652) && IS_ATOM_INT(_the_value_p_13693)) {
            _7653 = _7652 - _the_value_p_13693;
            if ((long)((unsigned long)_7653 +(unsigned long) HIGH_BITS) >= 0){
                _7653 = NewDouble((double)_7653);
            }
        }
        else {
            _7653 = binary_op(MINUS, _7652, _the_value_p_13693);
        }
        _7652 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__13696);
        _1 = *(int *)_2;
        *(int *)_2 = _7653;
        if( _1 != _7653 ){
            DeRef(_1);
        }
        _7653 = NOVALUE;
        _7650 = NOVALUE;
        goto L17; // [1040] 1192

        /** 			case MULTIPLY then*/
        case 4:

        /** 				map_data[VALUE_LIST][index_] *= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_13700);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_13700 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _7656 = (int)*(((s1_ptr)_2)->base + _index__13696);
        _7654 = NOVALUE;
        if (IS_ATOM_INT(_7656) && IS_ATOM_INT(_the_value_p_13693)) {
            if (_7656 == (short)_7656 && _the_value_p_13693 <= INT15 && _the_value_p_13693 >= -INT15)
            _7657 = _7656 * _the_value_p_13693;
            else
            _7657 = NewDouble(_7656 * (double)_the_value_p_13693);
        }
        else {
            _7657 = binary_op(MULTIPLY, _7656, _the_value_p_13693);
        }
        _7656 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__13696);
        _1 = *(int *)_2;
        *(int *)_2 = _7657;
        if( _1 != _7657 ){
            DeRef(_1);
        }
        _7657 = NOVALUE;
        _7654 = NOVALUE;
        goto L17; // [1067] 1192

        /** 			case DIVIDE then*/
        case 5:

        /** 				map_data[VALUE_LIST][index_] /= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_13700);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_13700 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _7660 = (int)*(((s1_ptr)_2)->base + _index__13696);
        _7658 = NOVALUE;
        if (IS_ATOM_INT(_7660) && IS_ATOM_INT(_the_value_p_13693)) {
            _7661 = (_7660 % _the_value_p_13693) ? NewDouble((double)_7660 / _the_value_p_13693) : (_7660 / _the_value_p_13693);
        }
        else {
            _7661 = binary_op(DIVIDE, _7660, _the_value_p_13693);
        }
        _7660 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__13696);
        _1 = *(int *)_2;
        *(int *)_2 = _7661;
        if( _1 != _7661 ){
            DeRef(_1);
        }
        _7661 = NOVALUE;
        _7658 = NOVALUE;
        goto L17; // [1094] 1192

        /** 			case APPEND then*/
        case 6:

        /** 				map_data[VALUE_LIST][index_] = append( map_data[VALUE_LIST][index_], the_value_p )*/
        _2 = (int)SEQ_PTR(_map_data_13700);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_13700 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_map_data_13700);
        _7664 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_7664);
        _7665 = (int)*(((s1_ptr)_2)->base + _index__13696);
        _7664 = NOVALUE;
        Ref(_the_value_p_13693);
        Append(&_7666, _7665, _the_value_p_13693);
        _7665 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__13696);
        _1 = *(int *)_2;
        *(int *)_2 = _7666;
        if( _1 != _7666 ){
            DeRef(_1);
        }
        _7666 = NOVALUE;
        _7662 = NOVALUE;
        goto L17; // [1127] 1192

        /** 			case CONCAT then*/
        case 7:

        /** 				map_data[VALUE_LIST][index_] &= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_13700);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_13700 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _7669 = (int)*(((s1_ptr)_2)->base + _index__13696);
        _7667 = NOVALUE;
        if (IS_SEQUENCE(_7669) && IS_ATOM(_the_value_p_13693)) {
            Ref(_the_value_p_13693);
            Append(&_7670, _7669, _the_value_p_13693);
        }
        else if (IS_ATOM(_7669) && IS_SEQUENCE(_the_value_p_13693)) {
            Ref(_7669);
            Prepend(&_7670, _the_value_p_13693, _7669);
        }
        else {
            Concat((object_ptr)&_7670, _7669, _the_value_p_13693);
            _7669 = NOVALUE;
        }
        _7669 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__13696);
        _1 = *(int *)_2;
        *(int *)_2 = _7670;
        if( _1 != _7670 ){
            DeRef(_1);
        }
        _7670 = NOVALUE;
        _7667 = NOVALUE;
        goto L17; // [1154] 1192

        /** 			case LEAVE then*/
        case 8:

        /** 				operation_p = operation_p*/
        _operation_p_13694 = _operation_p_13694;
        goto L17; // [1165] 1192

        /** 			case else*/
        default:

        /** 				error:crash("Unknown operation given to map.e:put()")*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_1173_13886);
        _msg_inlined_crash_at_1173_13886 = EPrintf(-9999999, _7576, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_1173_13886);

        /** end procedure*/
        goto L18; // [1186] 1189
L18: 
        DeRefi(_msg_inlined_crash_at_1173_13886);
        _msg_inlined_crash_at_1173_13886 = NOVALUE;
    ;}L17: 

    /** 		eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_13700);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _33ram_space_12557 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_13691);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_13700;
    DeRef(_1);

    /** 		return*/
    DeRef(_the_key_p_13692);
    DeRef(_the_value_p_13693);
    DeRef(_average_length__13698);
    DeRefDS(_map_data_13700);
    _7540 = NOVALUE;
    _7584 = NOVALUE;
    return;
LB: 

    /** end procedure*/
    DeRef(_the_key_p_13692);
    DeRef(_the_value_p_13693);
    DeRef(_average_length__13698);
    DeRef(_map_data_13700);
    _7540 = NOVALUE;
    _7584 = NOVALUE;
    return;
    ;
}


void _32nested_put(int _the_map_p_13889, int _the_keys_p_13890, int _the_value_p_13891, int _operation_p_13892, int _trigger_p_13893)
{
    int _temp_map__13894 = NOVALUE;
    int _7683 = NOVALUE;
    int _7682 = NOVALUE;
    int _7681 = NOVALUE;
    int _7680 = NOVALUE;
    int _7679 = NOVALUE;
    int _7678 = NOVALUE;
    int _7677 = NOVALUE;
    int _7675 = NOVALUE;
    int _7674 = NOVALUE;
    int _7673 = NOVALUE;
    int _7671 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_operation_p_13892)) {
        _1 = (long)(DBL_PTR(_operation_p_13892)->dbl);
        if (UNIQUE(DBL_PTR(_operation_p_13892)) && (DBL_PTR(_operation_p_13892)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_operation_p_13892);
        _operation_p_13892 = _1;
    }
    if (!IS_ATOM_INT(_trigger_p_13893)) {
        _1 = (long)(DBL_PTR(_trigger_p_13893)->dbl);
        if (UNIQUE(DBL_PTR(_trigger_p_13893)) && (DBL_PTR(_trigger_p_13893)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_trigger_p_13893);
        _trigger_p_13893 = _1;
    }

    /** 	if length( the_keys_p ) = 1 then*/
    if (IS_SEQUENCE(_the_keys_p_13890)){
            _7671 = SEQ_PTR(_the_keys_p_13890)->length;
    }
    else {
        _7671 = 1;
    }
    if (_7671 != 1)
    goto L1; // [12] 32

    /** 		put( the_map_p, the_keys_p[1], the_value_p, operation_p, trigger_p )*/
    _2 = (int)SEQ_PTR(_the_keys_p_13890);
    _7673 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_the_map_p_13889);
    Ref(_7673);
    Ref(_the_value_p_13891);
    _32put(_the_map_p_13889, _7673, _the_value_p_13891, _operation_p_13892, _trigger_p_13893);
    _7673 = NOVALUE;
    goto L2; // [29] 94
L1: 

    /** 		temp_map_ = new_extra( get( the_map_p, the_keys_p[1] ) )*/
    _2 = (int)SEQ_PTR(_the_keys_p_13890);
    _7674 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_the_map_p_13889);
    Ref(_7674);
    _7675 = _32get(_the_map_p_13889, _7674, 0);
    _7674 = NOVALUE;
    _0 = _temp_map__13894;
    _temp_map__13894 = _32new_extra(_7675, 690);
    DeRef(_0);
    _7675 = NOVALUE;

    /** 		nested_put( temp_map_, the_keys_p[2..$], the_value_p, operation_p, trigger_p )*/
    if (IS_SEQUENCE(_the_keys_p_13890)){
            _7677 = SEQ_PTR(_the_keys_p_13890)->length;
    }
    else {
        _7677 = 1;
    }
    rhs_slice_target = (object_ptr)&_7678;
    RHS_Slice(_the_keys_p_13890, 2, _7677);
    Ref(_temp_map__13894);
    DeRef(_7679);
    _7679 = _temp_map__13894;
    Ref(_the_value_p_13891);
    DeRef(_7680);
    _7680 = _the_value_p_13891;
    DeRef(_7681);
    _7681 = _operation_p_13892;
    DeRef(_7682);
    _7682 = _trigger_p_13893;
    _32nested_put(_7679, _7678, _7680, _7681, _7682);
    _7679 = NOVALUE;
    _7678 = NOVALUE;
    _7680 = NOVALUE;
    _7681 = NOVALUE;
    _7682 = NOVALUE;

    /** 		put( the_map_p, the_keys_p[1], temp_map_, PUT, trigger_p )*/
    _2 = (int)SEQ_PTR(_the_keys_p_13890);
    _7683 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_the_map_p_13889);
    Ref(_7683);
    Ref(_temp_map__13894);
    _32put(_the_map_p_13889, _7683, _temp_map__13894, 1, _trigger_p_13893);
    _7683 = NOVALUE;
L2: 

    /** end procedure*/
    DeRef(_the_map_p_13889);
    DeRefDS(_the_keys_p_13890);
    DeRef(_the_value_p_13891);
    DeRef(_temp_map__13894);
    return;
    ;
}


void _32remove(int _the_map_p_13912, int _the_key_p_13913)
{
    int _index__13914 = NOVALUE;
    int _bucket__13915 = NOVALUE;
    int _temp_map__13916 = NOVALUE;
    int _from__13917 = NOVALUE;
    int _calc_hash_1__tmp_at44_13928 = NOVALUE;
    int _calc_hash_inlined_calc_hash_at_44_13927 = NOVALUE;
    int _ret__inlined_calc_hash_at_44_13926 = NOVALUE;
    int _max_hash_p_inlined_calc_hash_at_41_13925 = NOVALUE;
    int _7748 = NOVALUE;
    int _7747 = NOVALUE;
    int _7746 = NOVALUE;
    int _7745 = NOVALUE;
    int _7743 = NOVALUE;
    int _7741 = NOVALUE;
    int _7739 = NOVALUE;
    int _7737 = NOVALUE;
    int _7736 = NOVALUE;
    int _7734 = NOVALUE;
    int _7731 = NOVALUE;
    int _7730 = NOVALUE;
    int _7729 = NOVALUE;
    int _7728 = NOVALUE;
    int _7727 = NOVALUE;
    int _7726 = NOVALUE;
    int _7725 = NOVALUE;
    int _7724 = NOVALUE;
    int _7723 = NOVALUE;
    int _7722 = NOVALUE;
    int _7721 = NOVALUE;
    int _7720 = NOVALUE;
    int _7719 = NOVALUE;
    int _7717 = NOVALUE;
    int _7716 = NOVALUE;
    int _7715 = NOVALUE;
    int _7714 = NOVALUE;
    int _7713 = NOVALUE;
    int _7712 = NOVALUE;
    int _7711 = NOVALUE;
    int _7710 = NOVALUE;
    int _7709 = NOVALUE;
    int _7708 = NOVALUE;
    int _7707 = NOVALUE;
    int _7705 = NOVALUE;
    int _7703 = NOVALUE;
    int _7701 = NOVALUE;
    int _7700 = NOVALUE;
    int _7699 = NOVALUE;
    int _7697 = NOVALUE;
    int _7696 = NOVALUE;
    int _7695 = NOVALUE;
    int _7694 = NOVALUE;
    int _7693 = NOVALUE;
    int _7690 = NOVALUE;
    int _7689 = NOVALUE;
    int _7688 = NOVALUE;
    int _7687 = NOVALUE;
    int _7685 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__13916);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!IS_ATOM_INT(_the_map_p_13912)){
        _temp_map__13916 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_13912)->dbl));
    }
    else{
        _temp_map__13916 = (int)*(((s1_ptr)_2)->base + _the_map_p_13912);
    }
    Ref(_temp_map__13916);

    /** 	eumem:ram_space[the_map_p] = 0*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _33ram_space_12557 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_the_map_p_13912))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_13912)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_13912);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__13916);
    _7685 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7685, 76)){
        _7685 = NOVALUE;
        goto L1; // [27] 337
    }
    _7685 = NOVALUE;

    /** 		bucket_ = calc_hash(the_key_p, length(temp_map_[KEY_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_temp_map__13916);
    _7687 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7687)){
            _7688 = SEQ_PTR(_7687)->length;
    }
    else {
        _7688 = 1;
    }
    _7687 = NOVALUE;
    _max_hash_p_inlined_calc_hash_at_41_13925 = _7688;
    _7688 = NOVALUE;

    /**     ret_ = hash(key_p, stdhash:HSIEH30)*/
    DeRef(_ret__inlined_calc_hash_at_44_13926);
    _ret__inlined_calc_hash_at_44_13926 = calc_hash(_the_key_p_13913, -6);
    if (!IS_ATOM_INT(_ret__inlined_calc_hash_at_44_13926)) {
        _1 = (long)(DBL_PTR(_ret__inlined_calc_hash_at_44_13926)->dbl);
        if (UNIQUE(DBL_PTR(_ret__inlined_calc_hash_at_44_13926)) && (DBL_PTR(_ret__inlined_calc_hash_at_44_13926)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret__inlined_calc_hash_at_44_13926);
        _ret__inlined_calc_hash_at_44_13926 = _1;
    }

    /** 	return remainder(ret_, max_hash_p) + 1 -- 1-based*/
    _calc_hash_1__tmp_at44_13928 = (_ret__inlined_calc_hash_at_44_13926 % _max_hash_p_inlined_calc_hash_at_41_13925);
    _bucket__13915 = _calc_hash_1__tmp_at44_13928 + 1;
    DeRef(_ret__inlined_calc_hash_at_44_13926);
    _ret__inlined_calc_hash_at_44_13926 = NOVALUE;
    if (!IS_ATOM_INT(_bucket__13915)) {
        _1 = (long)(DBL_PTR(_bucket__13915)->dbl);
        if (UNIQUE(DBL_PTR(_bucket__13915)) && (DBL_PTR(_bucket__13915)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bucket__13915);
        _bucket__13915 = _1;
    }

    /** 		index_ = find(the_key_p, temp_map_[KEY_BUCKETS][bucket_])*/
    _2 = (int)SEQ_PTR(_temp_map__13916);
    _7689 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_7689);
    _7690 = (int)*(((s1_ptr)_2)->base + _bucket__13915);
    _7689 = NOVALUE;
    _index__13914 = find_from(_the_key_p_13913, _7690, 1);
    _7690 = NOVALUE;

    /** 		if index_ != 0 then*/
    if (_index__13914 == 0)
    goto L2; // [91] 477

    /** 			temp_map_[ELEMENT_COUNT] -= 1*/
    _2 = (int)SEQ_PTR(_temp_map__13916);
    _7693 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_7693)) {
        _7694 = _7693 - 1;
        if ((long)((unsigned long)_7694 +(unsigned long) HIGH_BITS) >= 0){
            _7694 = NewDouble((double)_7694);
        }
    }
    else {
        _7694 = binary_op(MINUS, _7693, 1);
    }
    _7693 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__13916);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__13916 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _7694;
    if( _1 != _7694 ){
        DeRef(_1);
    }
    _7694 = NOVALUE;

    /** 			if length(temp_map_[KEY_BUCKETS][bucket_]) = 1 then*/
    _2 = (int)SEQ_PTR(_temp_map__13916);
    _7695 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_7695);
    _7696 = (int)*(((s1_ptr)_2)->base + _bucket__13915);
    _7695 = NOVALUE;
    if (IS_SEQUENCE(_7696)){
            _7697 = SEQ_PTR(_7696)->length;
    }
    else {
        _7697 = 1;
    }
    _7696 = NOVALUE;
    if (_7697 != 1)
    goto L3; // [126] 175

    /** 				temp_map_[IN_USE] -= 1*/
    _2 = (int)SEQ_PTR(_temp_map__13916);
    _7699 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_7699)) {
        _7700 = _7699 - 1;
        if ((long)((unsigned long)_7700 +(unsigned long) HIGH_BITS) >= 0){
            _7700 = NewDouble((double)_7700);
        }
    }
    else {
        _7700 = binary_op(MINUS, _7699, 1);
    }
    _7699 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__13916);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__13916 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _7700;
    if( _1 != _7700 ){
        DeRef(_1);
    }
    _7700 = NOVALUE;

    /** 				temp_map_[KEY_BUCKETS][bucket_] = {}*/
    _2 = (int)SEQ_PTR(_temp_map__13916);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__13916 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    RefDS(_5);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__13915);
    _1 = *(int *)_2;
    *(int *)_2 = _5;
    DeRef(_1);
    _7701 = NOVALUE;

    /** 				temp_map_[VALUE_BUCKETS][bucket_] = {}*/
    _2 = (int)SEQ_PTR(_temp_map__13916);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__13916 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    RefDS(_5);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__13915);
    _1 = *(int *)_2;
    *(int *)_2 = _5;
    DeRef(_1);
    _7703 = NOVALUE;
    goto L4; // [172] 292
L3: 

    /** 				temp_map_[VALUE_BUCKETS][bucket_] = temp_map_[VALUE_BUCKETS][bucket_][1 .. index_-1] & */
    _2 = (int)SEQ_PTR(_temp_map__13916);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__13916 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_temp_map__13916);
    _7707 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_7707);
    _7708 = (int)*(((s1_ptr)_2)->base + _bucket__13915);
    _7707 = NOVALUE;
    _7709 = _index__13914 - 1;
    rhs_slice_target = (object_ptr)&_7710;
    RHS_Slice(_7708, 1, _7709);
    _7708 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__13916);
    _7711 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_7711);
    _7712 = (int)*(((s1_ptr)_2)->base + _bucket__13915);
    _7711 = NOVALUE;
    _7713 = _index__13914 + 1;
    if (_7713 > MAXINT){
        _7713 = NewDouble((double)_7713);
    }
    if (IS_SEQUENCE(_7712)){
            _7714 = SEQ_PTR(_7712)->length;
    }
    else {
        _7714 = 1;
    }
    rhs_slice_target = (object_ptr)&_7715;
    RHS_Slice(_7712, _7713, _7714);
    _7712 = NOVALUE;
    Concat((object_ptr)&_7716, _7710, _7715);
    DeRefDS(_7710);
    _7710 = NOVALUE;
    DeRef(_7710);
    _7710 = NOVALUE;
    DeRefDS(_7715);
    _7715 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__13915);
    _1 = *(int *)_2;
    *(int *)_2 = _7716;
    if( _1 != _7716 ){
        DeRef(_1);
    }
    _7716 = NOVALUE;
    _7705 = NOVALUE;

    /** 				temp_map_[KEY_BUCKETS][bucket_] = temp_map_[KEY_BUCKETS][bucket_][1 .. index_-1] & */
    _2 = (int)SEQ_PTR(_temp_map__13916);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__13916 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_temp_map__13916);
    _7719 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_7719);
    _7720 = (int)*(((s1_ptr)_2)->base + _bucket__13915);
    _7719 = NOVALUE;
    _7721 = _index__13914 - 1;
    rhs_slice_target = (object_ptr)&_7722;
    RHS_Slice(_7720, 1, _7721);
    _7720 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__13916);
    _7723 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_7723);
    _7724 = (int)*(((s1_ptr)_2)->base + _bucket__13915);
    _7723 = NOVALUE;
    _7725 = _index__13914 + 1;
    if (_7725 > MAXINT){
        _7725 = NewDouble((double)_7725);
    }
    if (IS_SEQUENCE(_7724)){
            _7726 = SEQ_PTR(_7724)->length;
    }
    else {
        _7726 = 1;
    }
    rhs_slice_target = (object_ptr)&_7727;
    RHS_Slice(_7724, _7725, _7726);
    _7724 = NOVALUE;
    Concat((object_ptr)&_7728, _7722, _7727);
    DeRefDS(_7722);
    _7722 = NOVALUE;
    DeRef(_7722);
    _7722 = NOVALUE;
    DeRefDS(_7727);
    _7727 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__13915);
    _1 = *(int *)_2;
    *(int *)_2 = _7728;
    if( _1 != _7728 ){
        DeRef(_1);
    }
    _7728 = NOVALUE;
    _7717 = NOVALUE;
L4: 

    /** 			if temp_map_[ELEMENT_COUNT] < floor(51 * threshold_size / 100) then*/
    _2 = (int)SEQ_PTR(_temp_map__13916);
    _7729 = (int)*(((s1_ptr)_2)->base + 2);
    if (_32threshold_size_13318 <= INT15 && _32threshold_size_13318 >= -INT15)
    _7730 = 51 * _32threshold_size_13318;
    else
    _7730 = NewDouble(51 * (double)_32threshold_size_13318);
    if (IS_ATOM_INT(_7730)) {
        if (100 > 0 && _7730 >= 0) {
            _7731 = _7730 / 100;
        }
        else {
            temp_dbl = floor((double)_7730 / (double)100);
            if (_7730 != MININT)
            _7731 = (long)temp_dbl;
            else
            _7731 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _7730, 100);
        _7731 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_7730);
    _7730 = NOVALUE;
    if (binary_op_a(GREATEREQ, _7729, _7731)){
        _7729 = NOVALUE;
        DeRef(_7731);
        _7731 = NOVALUE;
        goto L2; // [310] 477
    }
    _7729 = NOVALUE;
    DeRef(_7731);
    _7731 = NOVALUE;

    /** 				eumem:ram_space[the_map_p] = temp_map_*/
    RefDS(_temp_map__13916);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _33ram_space_12557 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_the_map_p_13912))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_13912)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_13912);
    _1 = *(int *)_2;
    *(int *)_2 = _temp_map__13916;
    DeRef(_1);

    /** 				convert_to_small_map(the_map_p)*/
    Ref(_the_map_p_13912);
    _32convert_to_small_map(_the_map_p_13912);

    /** 				return*/
    DeRef(_the_map_p_13912);
    DeRef(_the_key_p_13913);
    DeRefDS(_temp_map__13916);
    _7687 = NOVALUE;
    _7696 = NOVALUE;
    DeRef(_7709);
    _7709 = NOVALUE;
    DeRef(_7721);
    _7721 = NOVALUE;
    DeRef(_7713);
    _7713 = NOVALUE;
    DeRef(_7725);
    _7725 = NOVALUE;
    return;
    goto L2; // [334] 477
L1: 

    /** 		from_ = 1*/
    _from__13917 = 1;

    /** 		while from_ > 0 do*/
L5: 
    if (_from__13917 <= 0)
    goto L6; // [347] 476

    /** 			index_ = find(the_key_p, temp_map_[KEY_LIST], from_)*/
    _2 = (int)SEQ_PTR(_temp_map__13916);
    _7734 = (int)*(((s1_ptr)_2)->base + 5);
    _index__13914 = find_from(_the_key_p_13913, _7734, _from__13917);
    _7734 = NOVALUE;

    /** 			if index_ then*/
    if (_index__13914 == 0)
    {
        goto L6; // [366] 476
    }
    else{
    }

    /** 				if temp_map_[FREE_LIST][index_] = 1 then*/
    _2 = (int)SEQ_PTR(_temp_map__13916);
    _7736 = (int)*(((s1_ptr)_2)->base + 7);
    _2 = (int)SEQ_PTR(_7736);
    _7737 = (int)*(((s1_ptr)_2)->base + _index__13914);
    _7736 = NOVALUE;
    if (binary_op_a(NOTEQ, _7737, 1)){
        _7737 = NOVALUE;
        goto L7; // [381] 465
    }
    _7737 = NOVALUE;

    /** 					temp_map_[FREE_LIST][index_] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__13916);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__13916 = MAKE_SEQ(_2);
    }
    _3 = (int)(7 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__13914);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _7739 = NOVALUE;

    /** 					temp_map_[KEY_LIST][index_] = init_small_map_key*/
    _2 = (int)SEQ_PTR(_temp_map__13916);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__13916 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    RefDS(_32init_small_map_key_13319);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__13914);
    _1 = *(int *)_2;
    *(int *)_2 = _32init_small_map_key_13319;
    DeRef(_1);
    _7741 = NOVALUE;

    /** 					temp_map_[VALUE_LIST][index_] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__13916);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__13916 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__13914);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _7743 = NOVALUE;

    /** 					temp_map_[IN_USE] -= 1*/
    _2 = (int)SEQ_PTR(_temp_map__13916);
    _7745 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_7745)) {
        _7746 = _7745 - 1;
        if ((long)((unsigned long)_7746 +(unsigned long) HIGH_BITS) >= 0){
            _7746 = NewDouble((double)_7746);
        }
    }
    else {
        _7746 = binary_op(MINUS, _7745, 1);
    }
    _7745 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__13916);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__13916 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _7746;
    if( _1 != _7746 ){
        DeRef(_1);
    }
    _7746 = NOVALUE;

    /** 					temp_map_[ELEMENT_COUNT] -= 1*/
    _2 = (int)SEQ_PTR(_temp_map__13916);
    _7747 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_7747)) {
        _7748 = _7747 - 1;
        if ((long)((unsigned long)_7748 +(unsigned long) HIGH_BITS) >= 0){
            _7748 = NewDouble((double)_7748);
        }
    }
    else {
        _7748 = binary_op(MINUS, _7747, 1);
    }
    _7747 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__13916);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__13916 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _7748;
    if( _1 != _7748 ){
        DeRef(_1);
    }
    _7748 = NOVALUE;
    goto L7; // [457] 465

    /** 				exit*/
    goto L6; // [462] 476
L7: 

    /** 			from_ = index_ + 1*/
    _from__13917 = _index__13914 + 1;

    /** 		end while*/
    goto L5; // [473] 347
L6: 
L2: 

    /** 	eumem:ram_space[the_map_p] = temp_map_*/
    RefDS(_temp_map__13916);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _33ram_space_12557 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_the_map_p_13912))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_13912)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_13912);
    _1 = *(int *)_2;
    *(int *)_2 = _temp_map__13916;
    DeRef(_1);

    /** end procedure*/
    DeRef(_the_map_p_13912);
    DeRef(_the_key_p_13913);
    DeRefDS(_temp_map__13916);
    _7687 = NOVALUE;
    _7696 = NOVALUE;
    DeRef(_7709);
    _7709 = NOVALUE;
    DeRef(_7721);
    _7721 = NOVALUE;
    DeRef(_7713);
    _7713 = NOVALUE;
    DeRef(_7725);
    _7725 = NOVALUE;
    return;
    ;
}


void _32clear(int _the_map_p_14002)
{
    int _temp_map__14003 = NOVALUE;
    int _7767 = NOVALUE;
    int _7766 = NOVALUE;
    int _7765 = NOVALUE;
    int _7764 = NOVALUE;
    int _7763 = NOVALUE;
    int _7762 = NOVALUE;
    int _7761 = NOVALUE;
    int _7760 = NOVALUE;
    int _7759 = NOVALUE;
    int _7758 = NOVALUE;
    int _7757 = NOVALUE;
    int _7756 = NOVALUE;
    int _7755 = NOVALUE;
    int _7754 = NOVALUE;
    int _7753 = NOVALUE;
    int _7751 = NOVALUE;
    int _0, _1, _2;
    

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__14003);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!IS_ATOM_INT(_the_map_p_14002)){
        _temp_map__14003 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_14002)->dbl));
    }
    else{
        _temp_map__14003 = (int)*(((s1_ptr)_2)->base + _the_map_p_14002);
    }
    Ref(_temp_map__14003);

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__14003);
    _7751 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7751, 76)){
        _7751 = NOVALUE;
        goto L1; // [19] 84
    }
    _7751 = NOVALUE;

    /** 		temp_map_[ELEMENT_COUNT] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__14003);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14003 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		temp_map_[IN_USE] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__14003);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14003 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		temp_map_[KEY_BUCKETS] = repeat({}, length(temp_map_[KEY_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_temp_map__14003);
    _7753 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7753)){
            _7754 = SEQ_PTR(_7753)->length;
    }
    else {
        _7754 = 1;
    }
    _7753 = NOVALUE;
    _7755 = Repeat(_5, _7754);
    _7754 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14003);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14003 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _7755;
    if( _1 != _7755 ){
        DeRef(_1);
    }
    _7755 = NOVALUE;

    /** 		temp_map_[VALUE_BUCKETS] = repeat({}, length(temp_map_[VALUE_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_temp_map__14003);
    _7756 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_7756)){
            _7757 = SEQ_PTR(_7756)->length;
    }
    else {
        _7757 = 1;
    }
    _7756 = NOVALUE;
    _7758 = Repeat(_5, _7757);
    _7757 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14003);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14003 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _7758;
    if( _1 != _7758 ){
        DeRef(_1);
    }
    _7758 = NOVALUE;
    goto L2; // [81] 164
L1: 

    /** 		temp_map_[ELEMENT_COUNT] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__14003);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14003 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		temp_map_[IN_USE] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__14003);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14003 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		temp_map_[KEY_LIST] = repeat(init_small_map_key, length(temp_map_[KEY_LIST]))*/
    _2 = (int)SEQ_PTR(_temp_map__14003);
    _7759 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7759)){
            _7760 = SEQ_PTR(_7759)->length;
    }
    else {
        _7760 = 1;
    }
    _7759 = NOVALUE;
    _7761 = Repeat(_32init_small_map_key_13319, _7760);
    _7760 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14003);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14003 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _7761;
    if( _1 != _7761 ){
        DeRef(_1);
    }
    _7761 = NOVALUE;

    /** 		temp_map_[VALUE_LIST] = repeat(0, length(temp_map_[VALUE_LIST]))*/
    _2 = (int)SEQ_PTR(_temp_map__14003);
    _7762 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_7762)){
            _7763 = SEQ_PTR(_7762)->length;
    }
    else {
        _7763 = 1;
    }
    _7762 = NOVALUE;
    _7764 = Repeat(0, _7763);
    _7763 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14003);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14003 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _7764;
    if( _1 != _7764 ){
        DeRef(_1);
    }
    _7764 = NOVALUE;

    /** 		temp_map_[FREE_LIST] = repeat(0, length(temp_map_[FREE_LIST]))*/
    _2 = (int)SEQ_PTR(_temp_map__14003);
    _7765 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7765)){
            _7766 = SEQ_PTR(_7765)->length;
    }
    else {
        _7766 = 1;
    }
    _7765 = NOVALUE;
    _7767 = Repeat(0, _7766);
    _7766 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14003);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14003 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 7);
    _1 = *(int *)_2;
    *(int *)_2 = _7767;
    if( _1 != _7767 ){
        DeRef(_1);
    }
    _7767 = NOVALUE;
L2: 

    /** 	eumem:ram_space[the_map_p] = temp_map_*/
    RefDS(_temp_map__14003);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _33ram_space_12557 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_the_map_p_14002))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_14002)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_14002);
    _1 = *(int *)_2;
    *(int *)_2 = _temp_map__14003;
    DeRef(_1);

    /** end procedure*/
    DeRef(_the_map_p_14002);
    DeRefDS(_temp_map__14003);
    _7753 = NOVALUE;
    _7756 = NOVALUE;
    _7759 = NOVALUE;
    _7762 = NOVALUE;
    _7765 = NOVALUE;
    return;
    ;
}


int _32size(int _the_map_p_14026)
{
    int _7769 = NOVALUE;
    int _7768 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return eumem:ram_space[the_map_p][ELEMENT_COUNT]*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!IS_ATOM_INT(_the_map_p_14026)){
        _7768 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_14026)->dbl));
    }
    else{
        _7768 = (int)*(((s1_ptr)_2)->base + _the_map_p_14026);
    }
    _2 = (int)SEQ_PTR(_7768);
    _7769 = (int)*(((s1_ptr)_2)->base + 2);
    _7768 = NOVALUE;
    Ref(_7769);
    DeRef(_the_map_p_14026);
    return _7769;
    ;
}


int _32statistics(int _the_map_p_14044)
{
    int _statistic_set__14045 = NOVALUE;
    int _lengths__14046 = NOVALUE;
    int _length__14047 = NOVALUE;
    int _temp_map__14048 = NOVALUE;
    int _7806 = NOVALUE;
    int _7805 = NOVALUE;
    int _7804 = NOVALUE;
    int _7803 = NOVALUE;
    int _7802 = NOVALUE;
    int _7801 = NOVALUE;
    int _7800 = NOVALUE;
    int _7799 = NOVALUE;
    int _7798 = NOVALUE;
    int _7797 = NOVALUE;
    int _7796 = NOVALUE;
    int _7795 = NOVALUE;
    int _7792 = NOVALUE;
    int _7790 = NOVALUE;
    int _7787 = NOVALUE;
    int _7786 = NOVALUE;
    int _7785 = NOVALUE;
    int _7784 = NOVALUE;
    int _7782 = NOVALUE;
    int _7781 = NOVALUE;
    int _7780 = NOVALUE;
    int _7779 = NOVALUE;
    int _7777 = NOVALUE;
    int _0, _1, _2;
    

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__14048);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!IS_ATOM_INT(_the_map_p_14044)){
        _temp_map__14048 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_14044)->dbl));
    }
    else{
        _temp_map__14048 = (int)*(((s1_ptr)_2)->base + _the_map_p_14044);
    }
    Ref(_temp_map__14048);

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__14048);
    _7777 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7777, 76)){
        _7777 = NOVALUE;
        goto L1; // [19] 194
    }
    _7777 = NOVALUE;

    /** 		statistic_set_ = { */
    _2 = (int)SEQ_PTR(_temp_map__14048);
    _7779 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_temp_map__14048);
    _7780 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_temp_map__14048);
    _7781 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7781)){
            _7782 = SEQ_PTR(_7781)->length;
    }
    else {
        _7782 = 1;
    }
    _7781 = NOVALUE;
    _0 = _statistic_set__14045;
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_7779);
    *((int *)(_2+4)) = _7779;
    Ref(_7780);
    *((int *)(_2+8)) = _7780;
    *((int *)(_2+12)) = _7782;
    *((int *)(_2+16)) = 0;
    *((int *)(_2+20)) = 1073741823;
    *((int *)(_2+24)) = 0;
    *((int *)(_2+28)) = 0;
    _statistic_set__14045 = MAKE_SEQ(_1);
    DeRef(_0);
    _7782 = NOVALUE;
    _7780 = NOVALUE;
    _7779 = NOVALUE;

    /** 		lengths_ = {}*/
    RefDS(_5);
    DeRefi(_lengths__14046);
    _lengths__14046 = _5;

    /** 		for i = 1 to length(temp_map_[KEY_BUCKETS]) do*/
    _2 = (int)SEQ_PTR(_temp_map__14048);
    _7784 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7784)){
            _7785 = SEQ_PTR(_7784)->length;
    }
    else {
        _7785 = 1;
    }
    _7784 = NOVALUE;
    {
        int _i_14059;
        _i_14059 = 1;
L2: 
        if (_i_14059 > _7785){
            goto L3; // [74] 158
        }

        /** 			length_ = length(temp_map_[KEY_BUCKETS][i])*/
        _2 = (int)SEQ_PTR(_temp_map__14048);
        _7786 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_7786);
        _7787 = (int)*(((s1_ptr)_2)->base + _i_14059);
        _7786 = NOVALUE;
        if (IS_SEQUENCE(_7787)){
                _length__14047 = SEQ_PTR(_7787)->length;
        }
        else {
            _length__14047 = 1;
        }
        _7787 = NOVALUE;

        /** 			if length_ > 0 then*/
        if (_length__14047 <= 0)
        goto L4; // [98] 151

        /** 				if length_ > statistic_set_[LARGEST_BUCKET] then*/
        _2 = (int)SEQ_PTR(_statistic_set__14045);
        _7790 = (int)*(((s1_ptr)_2)->base + 4);
        if (binary_op_a(LESSEQ, _length__14047, _7790)){
            _7790 = NOVALUE;
            goto L5; // [110] 123
        }
        _7790 = NOVALUE;

        /** 					statistic_set_[LARGEST_BUCKET] = length_*/
        _2 = (int)SEQ_PTR(_statistic_set__14045);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _statistic_set__14045 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 4);
        _1 = *(int *)_2;
        *(int *)_2 = _length__14047;
        DeRef(_1);
L5: 

        /** 				if length_ < statistic_set_[SMALLEST_BUCKET] then*/
        _2 = (int)SEQ_PTR(_statistic_set__14045);
        _7792 = (int)*(((s1_ptr)_2)->base + 5);
        if (binary_op_a(GREATEREQ, _length__14047, _7792)){
            _7792 = NOVALUE;
            goto L6; // [131] 144
        }
        _7792 = NOVALUE;

        /** 					statistic_set_[SMALLEST_BUCKET] = length_*/
        _2 = (int)SEQ_PTR(_statistic_set__14045);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _statistic_set__14045 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = _length__14047;
        DeRef(_1);
L6: 

        /** 				lengths_ &= length_*/
        Append(&_lengths__14046, _lengths__14046, _length__14047);
L4: 

        /** 		end for*/
        _i_14059 = _i_14059 + 1;
        goto L2; // [153] 81
L3: 
        ;
    }

    /** 		statistic_set_[AVERAGE_BUCKET] = stats:average(lengths_)*/
    RefDS(_lengths__14046);
    _7795 = _35average(_lengths__14046, 1);
    _2 = (int)SEQ_PTR(_statistic_set__14045);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _statistic_set__14045 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _7795;
    if( _1 != _7795 ){
        DeRef(_1);
    }
    _7795 = NOVALUE;

    /** 		statistic_set_[STDEV_BUCKET] = stats:stdev(lengths_)*/
    RefDS(_lengths__14046);
    _7796 = _35stdev(_lengths__14046, 1, 2);
    _2 = (int)SEQ_PTR(_statistic_set__14045);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _statistic_set__14045 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 7);
    _1 = *(int *)_2;
    *(int *)_2 = _7796;
    if( _1 != _7796 ){
        DeRef(_1);
    }
    _7796 = NOVALUE;
    goto L7; // [191] 255
L1: 

    /** 		statistic_set_ = {*/
    _2 = (int)SEQ_PTR(_temp_map__14048);
    _7797 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_temp_map__14048);
    _7798 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_temp_map__14048);
    _7799 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7799)){
            _7800 = SEQ_PTR(_7799)->length;
    }
    else {
        _7800 = 1;
    }
    _7799 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14048);
    _7801 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7801)){
            _7802 = SEQ_PTR(_7801)->length;
    }
    else {
        _7802 = 1;
    }
    _7801 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14048);
    _7803 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7803)){
            _7804 = SEQ_PTR(_7803)->length;
    }
    else {
        _7804 = 1;
    }
    _7803 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14048);
    _7805 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7805)){
            _7806 = SEQ_PTR(_7805)->length;
    }
    else {
        _7806 = 1;
    }
    _7805 = NOVALUE;
    _0 = _statistic_set__14045;
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_7797);
    *((int *)(_2+4)) = _7797;
    Ref(_7798);
    *((int *)(_2+8)) = _7798;
    *((int *)(_2+12)) = _7800;
    *((int *)(_2+16)) = _7802;
    *((int *)(_2+20)) = _7804;
    *((int *)(_2+24)) = _7806;
    *((int *)(_2+28)) = 0;
    _statistic_set__14045 = MAKE_SEQ(_1);
    DeRef(_0);
    _7806 = NOVALUE;
    _7804 = NOVALUE;
    _7802 = NOVALUE;
    _7800 = NOVALUE;
    _7798 = NOVALUE;
    _7797 = NOVALUE;
L7: 

    /** 	return statistic_set_*/
    DeRef(_the_map_p_14044);
    DeRefi(_lengths__14046);
    DeRef(_temp_map__14048);
    _7784 = NOVALUE;
    _7787 = NOVALUE;
    _7781 = NOVALUE;
    _7799 = NOVALUE;
    _7801 = NOVALUE;
    _7803 = NOVALUE;
    _7805 = NOVALUE;
    return _statistic_set__14045;
    ;
}


int _32keys(int _the_map_p_14090, int _sorted_result_14091)
{
    int _buckets__14092 = NOVALUE;
    int _current_bucket__14093 = NOVALUE;
    int _results__14094 = NOVALUE;
    int _pos__14095 = NOVALUE;
    int _temp_map__14096 = NOVALUE;
    int _7831 = NOVALUE;
    int _7829 = NOVALUE;
    int _7828 = NOVALUE;
    int _7826 = NOVALUE;
    int _7825 = NOVALUE;
    int _7824 = NOVALUE;
    int _7823 = NOVALUE;
    int _7821 = NOVALUE;
    int _7820 = NOVALUE;
    int _7819 = NOVALUE;
    int _7818 = NOVALUE;
    int _7816 = NOVALUE;
    int _7814 = NOVALUE;
    int _7811 = NOVALUE;
    int _7809 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sorted_result_14091)) {
        _1 = (long)(DBL_PTR(_sorted_result_14091)->dbl);
        if (UNIQUE(DBL_PTR(_sorted_result_14091)) && (DBL_PTR(_sorted_result_14091)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sorted_result_14091);
        _sorted_result_14091 = _1;
    }

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__14096);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!IS_ATOM_INT(_the_map_p_14090)){
        _temp_map__14096 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_14090)->dbl));
    }
    else{
        _temp_map__14096 = (int)*(((s1_ptr)_2)->base + _the_map_p_14090);
    }
    Ref(_temp_map__14096);

    /** 	results_ = repeat(0, temp_map_[ELEMENT_COUNT])*/
    _2 = (int)SEQ_PTR(_temp_map__14096);
    _7809 = (int)*(((s1_ptr)_2)->base + 2);
    DeRef(_results__14094);
    _results__14094 = Repeat(0, _7809);
    _7809 = NOVALUE;

    /** 	pos_ = 1*/
    _pos__14095 = 1;

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__14096);
    _7811 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7811, 76)){
        _7811 = NOVALUE;
        goto L1; // [38] 119
    }
    _7811 = NOVALUE;

    /** 		buckets_ = temp_map_[KEY_BUCKETS]*/
    DeRef(_buckets__14092);
    _2 = (int)SEQ_PTR(_temp_map__14096);
    _buckets__14092 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_buckets__14092);

    /** 		for index = 1 to length(buckets_) do*/
    if (IS_SEQUENCE(_buckets__14092)){
            _7814 = SEQ_PTR(_buckets__14092)->length;
    }
    else {
        _7814 = 1;
    }
    {
        int _index_14105;
        _index_14105 = 1;
L2: 
        if (_index_14105 > _7814){
            goto L3; // [57] 116
        }

        /** 			current_bucket_ = buckets_[index]*/
        DeRef(_current_bucket__14093);
        _2 = (int)SEQ_PTR(_buckets__14092);
        _current_bucket__14093 = (int)*(((s1_ptr)_2)->base + _index_14105);
        Ref(_current_bucket__14093);

        /** 			if length(current_bucket_) > 0 then*/
        if (IS_SEQUENCE(_current_bucket__14093)){
                _7816 = SEQ_PTR(_current_bucket__14093)->length;
        }
        else {
            _7816 = 1;
        }
        if (_7816 <= 0)
        goto L4; // [77] 109

        /** 				results_[pos_ .. pos_ + length(current_bucket_) - 1] = current_bucket_*/
        if (IS_SEQUENCE(_current_bucket__14093)){
                _7818 = SEQ_PTR(_current_bucket__14093)->length;
        }
        else {
            _7818 = 1;
        }
        _7819 = _pos__14095 + _7818;
        if ((long)((unsigned long)_7819 + (unsigned long)HIGH_BITS) >= 0) 
        _7819 = NewDouble((double)_7819);
        _7818 = NOVALUE;
        if (IS_ATOM_INT(_7819)) {
            _7820 = _7819 - 1;
        }
        else {
            _7820 = NewDouble(DBL_PTR(_7819)->dbl - (double)1);
        }
        DeRef(_7819);
        _7819 = NOVALUE;
        assign_slice_seq = (s1_ptr *)&_results__14094;
        AssignSlice(_pos__14095, _7820, _current_bucket__14093);
        DeRef(_7820);
        _7820 = NOVALUE;

        /** 				pos_ += length(current_bucket_)*/
        if (IS_SEQUENCE(_current_bucket__14093)){
                _7821 = SEQ_PTR(_current_bucket__14093)->length;
        }
        else {
            _7821 = 1;
        }
        _pos__14095 = _pos__14095 + _7821;
        _7821 = NOVALUE;
L4: 

        /** 		end for*/
        _index_14105 = _index_14105 + 1;
        goto L2; // [111] 64
L3: 
        ;
    }
    goto L5; // [116] 184
L1: 

    /** 		for index = 1 to length(temp_map_[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_temp_map__14096);
    _7823 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7823)){
            _7824 = SEQ_PTR(_7823)->length;
    }
    else {
        _7824 = 1;
    }
    _7823 = NOVALUE;
    {
        int _index_14118;
        _index_14118 = 1;
L6: 
        if (_index_14118 > _7824){
            goto L7; // [130] 183
        }

        /** 			if temp_map_[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_temp_map__14096);
        _7825 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_7825);
        _7826 = (int)*(((s1_ptr)_2)->base + _index_14118);
        _7825 = NOVALUE;
        if (binary_op_a(EQUALS, _7826, 0)){
            _7826 = NOVALUE;
            goto L8; // [149] 176
        }
        _7826 = NOVALUE;

        /** 				results_[pos_] = temp_map_[KEY_LIST][index]*/
        _2 = (int)SEQ_PTR(_temp_map__14096);
        _7828 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_7828);
        _7829 = (int)*(((s1_ptr)_2)->base + _index_14118);
        _7828 = NOVALUE;
        Ref(_7829);
        _2 = (int)SEQ_PTR(_results__14094);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _results__14094 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _pos__14095);
        _1 = *(int *)_2;
        *(int *)_2 = _7829;
        if( _1 != _7829 ){
            DeRef(_1);
        }
        _7829 = NOVALUE;

        /** 				pos_ += 1*/
        _pos__14095 = _pos__14095 + 1;
L8: 

        /** 		end for*/
        _index_14118 = _index_14118 + 1;
        goto L6; // [178] 137
L7: 
        ;
    }
L5: 

    /** 	if sorted_result then*/
    if (_sorted_result_14091 == 0)
    {
        goto L9; // [186] 203
    }
    else{
    }

    /** 		return stdsort:sort(results_)*/
    RefDS(_results__14094);
    _7831 = _22sort(_results__14094, 1);
    DeRef(_the_map_p_14090);
    DeRef(_buckets__14092);
    DeRef(_current_bucket__14093);
    DeRefDS(_results__14094);
    DeRef(_temp_map__14096);
    _7823 = NOVALUE;
    return _7831;
    goto LA; // [200] 210
L9: 

    /** 		return results_*/
    DeRef(_the_map_p_14090);
    DeRef(_buckets__14092);
    DeRef(_current_bucket__14093);
    DeRef(_temp_map__14096);
    _7823 = NOVALUE;
    DeRef(_7831);
    _7831 = NOVALUE;
    return _results__14094;
LA: 
    ;
}


int _32values(int _the_map_14133, int _keys_14134, int _default_values_14135)
{
    int _buckets__14159 = NOVALUE;
    int _bucket__14160 = NOVALUE;
    int _results__14161 = NOVALUE;
    int _pos__14162 = NOVALUE;
    int _temp_map__14163 = NOVALUE;
    int _7871 = NOVALUE;
    int _7870 = NOVALUE;
    int _7868 = NOVALUE;
    int _7867 = NOVALUE;
    int _7866 = NOVALUE;
    int _7865 = NOVALUE;
    int _7863 = NOVALUE;
    int _7862 = NOVALUE;
    int _7861 = NOVALUE;
    int _7860 = NOVALUE;
    int _7858 = NOVALUE;
    int _7856 = NOVALUE;
    int _7853 = NOVALUE;
    int _7851 = NOVALUE;
    int _7849 = NOVALUE;
    int _7848 = NOVALUE;
    int _7847 = NOVALUE;
    int _7846 = NOVALUE;
    int _7844 = NOVALUE;
    int _7843 = NOVALUE;
    int _7842 = NOVALUE;
    int _7841 = NOVALUE;
    int _7840 = NOVALUE;
    int _7839 = NOVALUE;
    int _7837 = NOVALUE;
    int _7836 = NOVALUE;
    int _7834 = NOVALUE;
    int _7833 = NOVALUE;
    int _7832 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(keys) then*/
    _7832 = IS_SEQUENCE(_keys_14134);
    if (_7832 == 0)
    {
        _7832 = NOVALUE;
        goto L1; // [6] 116
    }
    else{
        _7832 = NOVALUE;
    }

    /** 		if atom(default_values) then*/
    _7833 = IS_ATOM(_default_values_14135);
    if (_7833 == 0)
    {
        _7833 = NOVALUE;
        goto L2; // [14] 29
    }
    else{
        _7833 = NOVALUE;
    }

    /** 			default_values = repeat(default_values, length(keys))*/
    if (IS_SEQUENCE(_keys_14134)){
            _7834 = SEQ_PTR(_keys_14134)->length;
    }
    else {
        _7834 = 1;
    }
    _0 = _default_values_14135;
    _default_values_14135 = Repeat(_default_values_14135, _7834);
    DeRef(_0);
    _7834 = NOVALUE;
    goto L3; // [26] 70
L2: 

    /** 		elsif length(default_values) < length(keys) then*/
    if (IS_SEQUENCE(_default_values_14135)){
            _7836 = SEQ_PTR(_default_values_14135)->length;
    }
    else {
        _7836 = 1;
    }
    if (IS_SEQUENCE(_keys_14134)){
            _7837 = SEQ_PTR(_keys_14134)->length;
    }
    else {
        _7837 = 1;
    }
    if (_7836 >= _7837)
    goto L4; // [37] 69

    /** 			default_values &= repeat(default_values[$], length(keys) - length(default_values))*/
    if (IS_SEQUENCE(_default_values_14135)){
            _7839 = SEQ_PTR(_default_values_14135)->length;
    }
    else {
        _7839 = 1;
    }
    _2 = (int)SEQ_PTR(_default_values_14135);
    _7840 = (int)*(((s1_ptr)_2)->base + _7839);
    if (IS_SEQUENCE(_keys_14134)){
            _7841 = SEQ_PTR(_keys_14134)->length;
    }
    else {
        _7841 = 1;
    }
    if (IS_SEQUENCE(_default_values_14135)){
            _7842 = SEQ_PTR(_default_values_14135)->length;
    }
    else {
        _7842 = 1;
    }
    _7843 = _7841 - _7842;
    _7841 = NOVALUE;
    _7842 = NOVALUE;
    _7844 = Repeat(_7840, _7843);
    _7840 = NOVALUE;
    _7843 = NOVALUE;
    if (IS_SEQUENCE(_default_values_14135) && IS_ATOM(_7844)) {
    }
    else if (IS_ATOM(_default_values_14135) && IS_SEQUENCE(_7844)) {
        Ref(_default_values_14135);
        Prepend(&_default_values_14135, _7844, _default_values_14135);
    }
    else {
        Concat((object_ptr)&_default_values_14135, _default_values_14135, _7844);
    }
    DeRefDS(_7844);
    _7844 = NOVALUE;
L4: 
L3: 

    /** 		for i = 1 to length(keys) do*/
    if (IS_SEQUENCE(_keys_14134)){
            _7846 = SEQ_PTR(_keys_14134)->length;
    }
    else {
        _7846 = 1;
    }
    {
        int _i_14154;
        _i_14154 = 1;
L5: 
        if (_i_14154 > _7846){
            goto L6; // [75] 109
        }

        /** 			keys[i] = get(the_map, keys[i], default_values[i])*/
        _2 = (int)SEQ_PTR(_keys_14134);
        _7847 = (int)*(((s1_ptr)_2)->base + _i_14154);
        _2 = (int)SEQ_PTR(_default_values_14135);
        _7848 = (int)*(((s1_ptr)_2)->base + _i_14154);
        Ref(_the_map_14133);
        Ref(_7847);
        Ref(_7848);
        _7849 = _32get(_the_map_14133, _7847, _7848);
        _7847 = NOVALUE;
        _7848 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys_14134);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys_14134 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_14154);
        _1 = *(int *)_2;
        *(int *)_2 = _7849;
        if( _1 != _7849 ){
            DeRef(_1);
        }
        _7849 = NOVALUE;

        /** 		end for*/
        _i_14154 = _i_14154 + 1;
        goto L5; // [104] 82
L6: 
        ;
    }

    /** 		return keys*/
    DeRef(_the_map_14133);
    DeRef(_default_values_14135);
    DeRef(_buckets__14159);
    DeRef(_bucket__14160);
    DeRef(_results__14161);
    DeRef(_temp_map__14163);
    return _keys_14134;
L1: 

    /** 	sequence buckets_*/

    /** 	sequence bucket_*/

    /** 	sequence results_*/

    /** 	integer pos_*/

    /** 	sequence temp_map_*/

    /** 	temp_map_ = eumem:ram_space[the_map]*/
    DeRef(_temp_map__14163);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!IS_ATOM_INT(_the_map_14133)){
        _temp_map__14163 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_14133)->dbl));
    }
    else{
        _temp_map__14163 = (int)*(((s1_ptr)_2)->base + _the_map_14133);
    }
    Ref(_temp_map__14163);

    /** 	results_ = repeat(0, temp_map_[ELEMENT_COUNT])*/
    _2 = (int)SEQ_PTR(_temp_map__14163);
    _7851 = (int)*(((s1_ptr)_2)->base + 2);
    DeRef(_results__14161);
    _results__14161 = Repeat(0, _7851);
    _7851 = NOVALUE;

    /** 	pos_ = 1*/
    _pos__14162 = 1;

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__14163);
    _7853 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7853, 76)){
        _7853 = NOVALUE;
        goto L7; // [161] 242
    }
    _7853 = NOVALUE;

    /** 		buckets_ = temp_map_[VALUE_BUCKETS]*/
    DeRef(_buckets__14159);
    _2 = (int)SEQ_PTR(_temp_map__14163);
    _buckets__14159 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_buckets__14159);

    /** 		for index = 1 to length(buckets_) do*/
    if (IS_SEQUENCE(_buckets__14159)){
            _7856 = SEQ_PTR(_buckets__14159)->length;
    }
    else {
        _7856 = 1;
    }
    {
        int _index_14172;
        _index_14172 = 1;
L8: 
        if (_index_14172 > _7856){
            goto L9; // [180] 239
        }

        /** 			bucket_ = buckets_[index]*/
        DeRef(_bucket__14160);
        _2 = (int)SEQ_PTR(_buckets__14159);
        _bucket__14160 = (int)*(((s1_ptr)_2)->base + _index_14172);
        Ref(_bucket__14160);

        /** 			if length(bucket_) > 0 then*/
        if (IS_SEQUENCE(_bucket__14160)){
                _7858 = SEQ_PTR(_bucket__14160)->length;
        }
        else {
            _7858 = 1;
        }
        if (_7858 <= 0)
        goto LA; // [200] 232

        /** 				results_[pos_ .. pos_ + length(bucket_) - 1] = bucket_*/
        if (IS_SEQUENCE(_bucket__14160)){
                _7860 = SEQ_PTR(_bucket__14160)->length;
        }
        else {
            _7860 = 1;
        }
        _7861 = _pos__14162 + _7860;
        if ((long)((unsigned long)_7861 + (unsigned long)HIGH_BITS) >= 0) 
        _7861 = NewDouble((double)_7861);
        _7860 = NOVALUE;
        if (IS_ATOM_INT(_7861)) {
            _7862 = _7861 - 1;
        }
        else {
            _7862 = NewDouble(DBL_PTR(_7861)->dbl - (double)1);
        }
        DeRef(_7861);
        _7861 = NOVALUE;
        assign_slice_seq = (s1_ptr *)&_results__14161;
        AssignSlice(_pos__14162, _7862, _bucket__14160);
        DeRef(_7862);
        _7862 = NOVALUE;

        /** 				pos_ += length(bucket_)*/
        if (IS_SEQUENCE(_bucket__14160)){
                _7863 = SEQ_PTR(_bucket__14160)->length;
        }
        else {
            _7863 = 1;
        }
        _pos__14162 = _pos__14162 + _7863;
        _7863 = NOVALUE;
LA: 

        /** 		end for*/
        _index_14172 = _index_14172 + 1;
        goto L8; // [234] 187
L9: 
        ;
    }
    goto LB; // [239] 307
L7: 

    /** 		for index = 1 to length(temp_map_[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_temp_map__14163);
    _7865 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7865)){
            _7866 = SEQ_PTR(_7865)->length;
    }
    else {
        _7866 = 1;
    }
    _7865 = NOVALUE;
    {
        int _index_14185;
        _index_14185 = 1;
LC: 
        if (_index_14185 > _7866){
            goto LD; // [253] 306
        }

        /** 			if temp_map_[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_temp_map__14163);
        _7867 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_7867);
        _7868 = (int)*(((s1_ptr)_2)->base + _index_14185);
        _7867 = NOVALUE;
        if (binary_op_a(EQUALS, _7868, 0)){
            _7868 = NOVALUE;
            goto LE; // [272] 299
        }
        _7868 = NOVALUE;

        /** 				results_[pos_] = temp_map_[VALUE_LIST][index]*/
        _2 = (int)SEQ_PTR(_temp_map__14163);
        _7870 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_7870);
        _7871 = (int)*(((s1_ptr)_2)->base + _index_14185);
        _7870 = NOVALUE;
        Ref(_7871);
        _2 = (int)SEQ_PTR(_results__14161);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _results__14161 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _pos__14162);
        _1 = *(int *)_2;
        *(int *)_2 = _7871;
        if( _1 != _7871 ){
            DeRef(_1);
        }
        _7871 = NOVALUE;

        /** 				pos_ += 1*/
        _pos__14162 = _pos__14162 + 1;
LE: 

        /** 		end for*/
        _index_14185 = _index_14185 + 1;
        goto LC; // [301] 260
LD: 
        ;
    }
LB: 

    /** 	return results_*/
    DeRef(_the_map_14133);
    DeRef(_keys_14134);
    DeRef(_default_values_14135);
    DeRef(_buckets__14159);
    DeRef(_bucket__14160);
    DeRef(_temp_map__14163);
    _7865 = NOVALUE;
    return _results__14161;
    ;
}


int _32pairs(int _the_map_p_14197, int _sorted_result_14198)
{
    int _key_bucket__14199 = NOVALUE;
    int _value_bucket__14200 = NOVALUE;
    int _results__14201 = NOVALUE;
    int _pos__14202 = NOVALUE;
    int _temp_map__14203 = NOVALUE;
    int _7907 = NOVALUE;
    int _7905 = NOVALUE;
    int _7904 = NOVALUE;
    int _7902 = NOVALUE;
    int _7901 = NOVALUE;
    int _7900 = NOVALUE;
    int _7898 = NOVALUE;
    int _7896 = NOVALUE;
    int _7895 = NOVALUE;
    int _7894 = NOVALUE;
    int _7893 = NOVALUE;
    int _7891 = NOVALUE;
    int _7889 = NOVALUE;
    int _7888 = NOVALUE;
    int _7886 = NOVALUE;
    int _7885 = NOVALUE;
    int _7883 = NOVALUE;
    int _7881 = NOVALUE;
    int _7880 = NOVALUE;
    int _7879 = NOVALUE;
    int _7877 = NOVALUE;
    int _7875 = NOVALUE;
    int _7874 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_sorted_result_14198)) {
        _1 = (long)(DBL_PTR(_sorted_result_14198)->dbl);
        if (UNIQUE(DBL_PTR(_sorted_result_14198)) && (DBL_PTR(_sorted_result_14198)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sorted_result_14198);
        _sorted_result_14198 = _1;
    }

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__14203);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!IS_ATOM_INT(_the_map_p_14197)){
        _temp_map__14203 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_14197)->dbl));
    }
    else{
        _temp_map__14203 = (int)*(((s1_ptr)_2)->base + _the_map_p_14197);
    }
    Ref(_temp_map__14203);

    /** 	results_ = repeat({ 0, 0 }, temp_map_[ELEMENT_COUNT])*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 0;
    _7874 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_temp_map__14203);
    _7875 = (int)*(((s1_ptr)_2)->base + 2);
    DeRef(_results__14201);
    _results__14201 = Repeat(_7874, _7875);
    DeRefDS(_7874);
    _7874 = NOVALUE;
    _7875 = NOVALUE;

    /** 	pos_ = 1*/
    _pos__14202 = 1;

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__14203);
    _7877 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7877, 76)){
        _7877 = NOVALUE;
        goto L1; // [42] 157
    }
    _7877 = NOVALUE;

    /** 		for index = 1 to length(temp_map_[KEY_BUCKETS]) do*/
    _2 = (int)SEQ_PTR(_temp_map__14203);
    _7879 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7879)){
            _7880 = SEQ_PTR(_7879)->length;
    }
    else {
        _7880 = 1;
    }
    _7879 = NOVALUE;
    {
        int _index_14212;
        _index_14212 = 1;
L2: 
        if (_index_14212 > _7880){
            goto L3; // [57] 154
        }

        /** 			key_bucket_ = temp_map_[KEY_BUCKETS][index]*/
        _2 = (int)SEQ_PTR(_temp_map__14203);
        _7881 = (int)*(((s1_ptr)_2)->base + 5);
        DeRef(_key_bucket__14199);
        _2 = (int)SEQ_PTR(_7881);
        _key_bucket__14199 = (int)*(((s1_ptr)_2)->base + _index_14212);
        Ref(_key_bucket__14199);
        _7881 = NOVALUE;

        /** 			value_bucket_ = temp_map_[VALUE_BUCKETS][index]*/
        _2 = (int)SEQ_PTR(_temp_map__14203);
        _7883 = (int)*(((s1_ptr)_2)->base + 6);
        DeRef(_value_bucket__14200);
        _2 = (int)SEQ_PTR(_7883);
        _value_bucket__14200 = (int)*(((s1_ptr)_2)->base + _index_14212);
        Ref(_value_bucket__14200);
        _7883 = NOVALUE;

        /** 			for j = 1 to length(key_bucket_) do*/
        if (IS_SEQUENCE(_key_bucket__14199)){
                _7885 = SEQ_PTR(_key_bucket__14199)->length;
        }
        else {
            _7885 = 1;
        }
        {
            int _j_14220;
            _j_14220 = 1;
L4: 
            if (_j_14220 > _7885){
                goto L5; // [97] 147
            }

            /** 				results_[pos_][1] = key_bucket_[j]*/
            _2 = (int)SEQ_PTR(_results__14201);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _results__14201 = MAKE_SEQ(_2);
            }
            _3 = (int)(_pos__14202 + ((s1_ptr)_2)->base);
            _2 = (int)SEQ_PTR(_key_bucket__14199);
            _7888 = (int)*(((s1_ptr)_2)->base + _j_14220);
            Ref(_7888);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 1);
            _1 = *(int *)_2;
            *(int *)_2 = _7888;
            if( _1 != _7888 ){
                DeRef(_1);
            }
            _7888 = NOVALUE;
            _7886 = NOVALUE;

            /** 				results_[pos_][2] = value_bucket_[j]*/
            _2 = (int)SEQ_PTR(_results__14201);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _results__14201 = MAKE_SEQ(_2);
            }
            _3 = (int)(_pos__14202 + ((s1_ptr)_2)->base);
            _2 = (int)SEQ_PTR(_value_bucket__14200);
            _7891 = (int)*(((s1_ptr)_2)->base + _j_14220);
            Ref(_7891);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 2);
            _1 = *(int *)_2;
            *(int *)_2 = _7891;
            if( _1 != _7891 ){
                DeRef(_1);
            }
            _7891 = NOVALUE;
            _7889 = NOVALUE;

            /** 				pos_ += 1*/
            _pos__14202 = _pos__14202 + 1;

            /** 			end for*/
            _j_14220 = _j_14220 + 1;
            goto L4; // [142] 104
L5: 
            ;
        }

        /** 		end for*/
        _index_14212 = _index_14212 + 1;
        goto L2; // [149] 64
L3: 
        ;
    }
    goto L6; // [154] 248
L1: 

    /** 		for index = 1 to length(temp_map_[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_temp_map__14203);
    _7893 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7893)){
            _7894 = SEQ_PTR(_7893)->length;
    }
    else {
        _7894 = 1;
    }
    _7893 = NOVALUE;
    {
        int _index_14231;
        _index_14231 = 1;
L7: 
        if (_index_14231 > _7894){
            goto L8; // [168] 247
        }

        /** 			if temp_map_[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_temp_map__14203);
        _7895 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_7895);
        _7896 = (int)*(((s1_ptr)_2)->base + _index_14231);
        _7895 = NOVALUE;
        if (binary_op_a(EQUALS, _7896, 0)){
            _7896 = NOVALUE;
            goto L9; // [187] 240
        }
        _7896 = NOVALUE;

        /** 				results_[pos_][1] = temp_map_[KEY_LIST][index]*/
        _2 = (int)SEQ_PTR(_results__14201);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _results__14201 = MAKE_SEQ(_2);
        }
        _3 = (int)(_pos__14202 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_temp_map__14203);
        _7900 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_7900);
        _7901 = (int)*(((s1_ptr)_2)->base + _index_14231);
        _7900 = NOVALUE;
        Ref(_7901);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 1);
        _1 = *(int *)_2;
        *(int *)_2 = _7901;
        if( _1 != _7901 ){
            DeRef(_1);
        }
        _7901 = NOVALUE;
        _7898 = NOVALUE;

        /** 				results_[pos_][2] = temp_map_[VALUE_LIST][index]*/
        _2 = (int)SEQ_PTR(_results__14201);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _results__14201 = MAKE_SEQ(_2);
        }
        _3 = (int)(_pos__14202 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_temp_map__14203);
        _7904 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_7904);
        _7905 = (int)*(((s1_ptr)_2)->base + _index_14231);
        _7904 = NOVALUE;
        Ref(_7905);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 2);
        _1 = *(int *)_2;
        *(int *)_2 = _7905;
        if( _1 != _7905 ){
            DeRef(_1);
        }
        _7905 = NOVALUE;
        _7902 = NOVALUE;

        /** 				pos_ += 1*/
        _pos__14202 = _pos__14202 + 1;
L9: 

        /** 		end for	*/
        _index_14231 = _index_14231 + 1;
        goto L7; // [242] 175
L8: 
        ;
    }
L6: 

    /** 	if sorted_result then*/
    if (_sorted_result_14198 == 0)
    {
        goto LA; // [250] 267
    }
    else{
    }

    /** 		return stdsort:sort(results_)*/
    RefDS(_results__14201);
    _7907 = _22sort(_results__14201, 1);
    DeRef(_the_map_p_14197);
    DeRef(_key_bucket__14199);
    DeRef(_value_bucket__14200);
    DeRefDS(_results__14201);
    DeRef(_temp_map__14203);
    _7879 = NOVALUE;
    _7893 = NOVALUE;
    return _7907;
    goto LB; // [264] 274
LA: 

    /** 		return results_*/
    DeRef(_the_map_p_14197);
    DeRef(_key_bucket__14199);
    DeRef(_value_bucket__14200);
    DeRef(_temp_map__14203);
    _7879 = NOVALUE;
    _7893 = NOVALUE;
    DeRef(_7907);
    _7907 = NOVALUE;
    return _results__14201;
LB: 
    ;
}


void _32optimize(int _the_map_p_14252, int _max_p_14253, int _grow_p_14254)
{
    int _stats__14256 = NOVALUE;
    int _next_guess__14257 = NOVALUE;
    int _prev_guess_14258 = NOVALUE;
    int _7928 = NOVALUE;
    int _7927 = NOVALUE;
    int _7925 = NOVALUE;
    int _7924 = NOVALUE;
    int _7923 = NOVALUE;
    int _7922 = NOVALUE;
    int _7921 = NOVALUE;
    int _7919 = NOVALUE;
    int _7917 = NOVALUE;
    int _7916 = NOVALUE;
    int _7915 = NOVALUE;
    int _7914 = NOVALUE;
    int _7910 = NOVALUE;
    int _7909 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_max_p_14253)) {
        _1 = (long)(DBL_PTR(_max_p_14253)->dbl);
        if (UNIQUE(DBL_PTR(_max_p_14253)) && (DBL_PTR(_max_p_14253)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_max_p_14253);
        _max_p_14253 = _1;
    }

    /** 	if eumem:ram_space[the_map_p][MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!IS_ATOM_INT(_the_map_p_14252)){
        _7909 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_14252)->dbl));
    }
    else{
        _7909 = (int)*(((s1_ptr)_2)->base + _the_map_p_14252);
    }
    _2 = (int)SEQ_PTR(_7909);
    _7910 = (int)*(((s1_ptr)_2)->base + 4);
    _7909 = NOVALUE;
    if (binary_op_a(NOTEQ, _7910, 76)){
        _7910 = NOVALUE;
        goto L1; // [17] 192
    }
    _7910 = NOVALUE;

    /** 		if grow_p < 1 then*/
    if (binary_op_a(GREATEREQ, _grow_p_14254, 1)){
        goto L2; // [23] 33
    }

    /** 			grow_p = 1.333*/
    RefDS(_7908);
    DeRef(_grow_p_14254);
    _grow_p_14254 = _7908;
L2: 

    /** 		if max_p < 3 then*/
    if (_max_p_14253 >= 3)
    goto L3; // [35] 45

    /** 			max_p = 3*/
    _max_p_14253 = 3;
L3: 

    /** 		next_guess_ = math:max({1, floor(eumem:ram_space[the_map_p][ELEMENT_COUNT] / max_p)})*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!IS_ATOM_INT(_the_map_p_14252)){
        _7914 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_14252)->dbl));
    }
    else{
        _7914 = (int)*(((s1_ptr)_2)->base + _the_map_p_14252);
    }
    _2 = (int)SEQ_PTR(_7914);
    _7915 = (int)*(((s1_ptr)_2)->base + 2);
    _7914 = NOVALUE;
    if (IS_ATOM_INT(_7915)) {
        if (_max_p_14253 > 0 && _7915 >= 0) {
            _7916 = _7915 / _max_p_14253;
        }
        else {
            temp_dbl = floor((double)_7915 / (double)_max_p_14253);
            if (_7915 != MININT)
            _7916 = (long)temp_dbl;
            else
            _7916 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _7915, _max_p_14253);
        _7916 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _7915 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = _7916;
    _7917 = MAKE_SEQ(_1);
    _7916 = NOVALUE;
    _next_guess__14257 = _18max(_7917);
    _7917 = NOVALUE;
    if (!IS_ATOM_INT(_next_guess__14257)) {
        _1 = (long)(DBL_PTR(_next_guess__14257)->dbl);
        if (UNIQUE(DBL_PTR(_next_guess__14257)) && (DBL_PTR(_next_guess__14257)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_next_guess__14257);
        _next_guess__14257 = _1;
    }

    /** 		while 1 with entry do*/
    goto L4; // [75] 172
L5: 

    /** 			if stats_[LARGEST_BUCKET] <= max_p then*/
    _2 = (int)SEQ_PTR(_stats__14256);
    _7919 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(GREATER, _7919, _max_p_14253)){
        _7919 = NOVALUE;
        goto L6; // [88] 97
    }
    _7919 = NOVALUE;

    /** 				exit -- Largest is now smaller than the maximum I wanted.*/
    goto L7; // [94] 191
L6: 

    /** 			if stats_[LARGEST_BUCKET] <= (stats_[STDEV_BUCKET]*3 + stats_[AVERAGE_BUCKET]) then*/
    _2 = (int)SEQ_PTR(_stats__14256);
    _7921 = (int)*(((s1_ptr)_2)->base + 4);
    _2 = (int)SEQ_PTR(_stats__14256);
    _7922 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_ATOM_INT(_7922)) {
        if (_7922 == (short)_7922)
        _7923 = _7922 * 3;
        else
        _7923 = NewDouble(_7922 * (double)3);
    }
    else {
        _7923 = binary_op(MULTIPLY, _7922, 3);
    }
    _7922 = NOVALUE;
    _2 = (int)SEQ_PTR(_stats__14256);
    _7924 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_ATOM_INT(_7923) && IS_ATOM_INT(_7924)) {
        _7925 = _7923 + _7924;
        if ((long)((unsigned long)_7925 + (unsigned long)HIGH_BITS) >= 0) 
        _7925 = NewDouble((double)_7925);
    }
    else {
        _7925 = binary_op(PLUS, _7923, _7924);
    }
    DeRef(_7923);
    _7923 = NOVALUE;
    _7924 = NOVALUE;
    if (binary_op_a(GREATER, _7921, _7925)){
        _7921 = NOVALUE;
        DeRef(_7925);
        _7925 = NOVALUE;
        goto L8; // [125] 134
    }
    _7921 = NOVALUE;
    DeRef(_7925);
    _7925 = NOVALUE;

    /** 				exit -- Largest is smaller than is statistically expected.*/
    goto L7; // [131] 191
L8: 

    /** 			prev_guess = next_guess_*/
    _prev_guess_14258 = _next_guess__14257;

    /** 			next_guess_ = floor(stats_[NUM_BUCKETS] * grow_p)*/
    _2 = (int)SEQ_PTR(_stats__14256);
    _7927 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_7927) && IS_ATOM_INT(_grow_p_14254)) {
        if (_7927 == (short)_7927 && _grow_p_14254 <= INT15 && _grow_p_14254 >= -INT15)
        _7928 = _7927 * _grow_p_14254;
        else
        _7928 = NewDouble(_7927 * (double)_grow_p_14254);
    }
    else {
        _7928 = binary_op(MULTIPLY, _7927, _grow_p_14254);
    }
    _7927 = NOVALUE;
    if (IS_ATOM_INT(_7928))
    _next_guess__14257 = e_floor(_7928);
    else
    _next_guess__14257 = unary_op(FLOOR, _7928);
    DeRef(_7928);
    _7928 = NOVALUE;
    if (!IS_ATOM_INT(_next_guess__14257)) {
        _1 = (long)(DBL_PTR(_next_guess__14257)->dbl);
        if (UNIQUE(DBL_PTR(_next_guess__14257)) && (DBL_PTR(_next_guess__14257)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_next_guess__14257);
        _next_guess__14257 = _1;
    }

    /** 			if prev_guess = next_guess_ then*/
    if (_prev_guess_14258 != _next_guess__14257)
    goto L9; // [158] 169

    /** 				next_guess_ += 1*/
    _next_guess__14257 = _next_guess__14257 + 1;
L9: 

    /** 		entry*/
L4: 

    /** 			rehash(the_map_p, next_guess_)*/
    Ref(_the_map_p_14252);
    _32rehash(_the_map_p_14252, _next_guess__14257);

    /** 			stats_ = statistics(the_map_p)*/
    Ref(_the_map_p_14252);
    _0 = _stats__14256;
    _stats__14256 = _32statistics(_the_map_p_14252);
    DeRef(_0);

    /** 		end while*/
    goto L5; // [188] 78
L7: 
L1: 

    /** end procedure*/
    DeRef(_the_map_p_14252);
    DeRef(_grow_p_14254);
    DeRef(_stats__14256);
    return;
    ;
}


int _32load_map(int _input_file_name_14292)
{
    int _file_handle_14293 = NOVALUE;
    int _line_in_14294 = NOVALUE;
    int _logical_line_14295 = NOVALUE;
    int _has_comment_14296 = NOVALUE;
    int _delim_pos_14297 = NOVALUE;
    int _data_value_14298 = NOVALUE;
    int _data_key_14299 = NOVALUE;
    int _conv_res_14300 = NOVALUE;
    int _new_map_14301 = NOVALUE;
    int _line_conts_14302 = NOVALUE;
    int _value_inlined_value_at_327_14368 = NOVALUE;
    int _value_inlined_value_at_496_14398 = NOVALUE;
    int _in_quote_14433 = NOVALUE;
    int _last_in_14434 = NOVALUE;
    int _cur_in_14435 = NOVALUE;
    int _seek_1__tmp_at885_14463 = NOVALUE;
    int _seek_inlined_seek_at_885_14462 = NOVALUE;
    int _8058 = NOVALUE;
    int _8057 = NOVALUE;
    int _8056 = NOVALUE;
    int _8055 = NOVALUE;
    int _8050 = NOVALUE;
    int _8048 = NOVALUE;
    int _8047 = NOVALUE;
    int _8045 = NOVALUE;
    int _8044 = NOVALUE;
    int _8043 = NOVALUE;
    int _8042 = NOVALUE;
    int _8034 = NOVALUE;
    int _8032 = NOVALUE;
    int _8025 = NOVALUE;
    int _8024 = NOVALUE;
    int _8023 = NOVALUE;
    int _8022 = NOVALUE;
    int _8018 = NOVALUE;
    int _8017 = NOVALUE;
    int _8013 = NOVALUE;
    int _8012 = NOVALUE;
    int _8009 = NOVALUE;
    int _8008 = NOVALUE;
    int _8006 = NOVALUE;
    int _7998 = NOVALUE;
    int _7997 = NOVALUE;
    int _7996 = NOVALUE;
    int _7995 = NOVALUE;
    int _7994 = NOVALUE;
    int _7993 = NOVALUE;
    int _7992 = NOVALUE;
    int _7991 = NOVALUE;
    int _7989 = NOVALUE;
    int _7988 = NOVALUE;
    int _7987 = NOVALUE;
    int _7984 = NOVALUE;
    int _7983 = NOVALUE;
    int _7981 = NOVALUE;
    int _7979 = NOVALUE;
    int _7978 = NOVALUE;
    int _7967 = NOVALUE;
    int _7965 = NOVALUE;
    int _7964 = NOVALUE;
    int _7959 = NOVALUE;
    int _7956 = NOVALUE;
    int _7953 = NOVALUE;
    int _7950 = NOVALUE;
    int _7947 = NOVALUE;
    int _7946 = NOVALUE;
    int _7942 = NOVALUE;
    int _7940 = NOVALUE;
    int _7934 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence line_conts =   ",${"*/
    RefDS(_7933);
    DeRefi(_line_conts_14302);
    _line_conts_14302 = _7933;

    /** 	if sequence(input_file_name) then*/
    _7934 = IS_SEQUENCE(_input_file_name_14292);
    if (_7934 == 0)
    {
        _7934 = NOVALUE;
        goto L1; // [13] 26
    }
    else{
        _7934 = NOVALUE;
    }

    /** 		file_handle = open(input_file_name, "rb")*/
    _file_handle_14293 = EOpen(_input_file_name_14292, _1138, 0);
    goto L2; // [23] 34
L1: 

    /** 		file_handle = input_file_name*/
    Ref(_input_file_name_14292);
    _file_handle_14293 = _input_file_name_14292;
    if (!IS_ATOM_INT(_file_handle_14293)) {
        _1 = (long)(DBL_PTR(_file_handle_14293)->dbl);
        if (UNIQUE(DBL_PTR(_file_handle_14293)) && (DBL_PTR(_file_handle_14293)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_file_handle_14293);
        _file_handle_14293 = _1;
    }
L2: 

    /** 	if file_handle = -1 then*/
    if (_file_handle_14293 != -1)
    goto L3; // [38] 49

    /** 		return -1*/
    DeRef(_input_file_name_14292);
    DeRef(_line_in_14294);
    DeRef(_logical_line_14295);
    DeRef(_data_value_14298);
    DeRef(_data_key_14299);
    DeRef(_conv_res_14300);
    DeRef(_new_map_14301);
    DeRefi(_line_conts_14302);
    return -1;
L3: 

    /** 	new_map = new(threshold_size) -- Assume a small map initially.*/
    _0 = _new_map_14301;
    _new_map_14301 = _32new(_32threshold_size_13318);
    DeRef(_0);

    /** 	for i = 1 to 10 do*/
    {
        int _i_14312;
        _i_14312 = 1;
L4: 
        if (_i_14312 > 10){
            goto L5; // [59] 118
        }

        /** 		delim_pos = getc(file_handle)*/
        if (_file_handle_14293 != last_r_file_no) {
            last_r_file_ptr = which_file(_file_handle_14293, EF_READ);
            last_r_file_no = _file_handle_14293;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _delim_pos_14297 = getKBchar();
            }
            else
            _delim_pos_14297 = getc(last_r_file_ptr);
        }
        else
        _delim_pos_14297 = getc(last_r_file_ptr);

        /** 		if delim_pos = -1 then */
        if (_delim_pos_14297 != -1)
        goto L6; // [73] 82

        /** 			exit*/
        goto L5; // [79] 118
L6: 

        /** 		if not t_print(delim_pos) then */
        _7940 = _5t_print(_delim_pos_14297);
        if (IS_ATOM_INT(_7940)) {
            if (_7940 != 0){
                DeRef(_7940);
                _7940 = NOVALUE;
                goto L7; // [88] 106
            }
        }
        else {
            if (DBL_PTR(_7940)->dbl != 0.0){
                DeRef(_7940);
                _7940 = NOVALUE;
                goto L7; // [88] 106
            }
        }
        DeRef(_7940);
        _7940 = NOVALUE;

        /** 	    	if not t_space(delim_pos) then*/
        _7942 = _5t_space(_delim_pos_14297);
        if (IS_ATOM_INT(_7942)) {
            if (_7942 != 0){
                DeRef(_7942);
                _7942 = NOVALUE;
                goto L8; // [97] 105
            }
        }
        else {
            if (DBL_PTR(_7942)->dbl != 0.0){
                DeRef(_7942);
                _7942 = NOVALUE;
                goto L8; // [97] 105
            }
        }
        DeRef(_7942);
        _7942 = NOVALUE;

        /** 	    		exit*/
        goto L5; // [102] 118
L8: 
L7: 

        /** 	    delim_pos = -1*/
        _delim_pos_14297 = -1;

        /** 	end for*/
        _i_14312 = _i_14312 + 1;
        goto L4; // [113] 66
L5: 
        ;
    }

    /** 	if delim_pos = -1 then*/
    if (_delim_pos_14297 != -1)
    goto L9; // [122] 881

    /** 		close(file_handle)*/
    EClose(_file_handle_14293);

    /** 		file_handle = open(input_file_name, "r")*/
    _file_handle_14293 = EOpen(_input_file_name_14292, _1081, 0);

    /** 		while sequence(logical_line) with entry do*/
    goto LA; // [139] 558
LB: 
    _7946 = IS_SEQUENCE(_logical_line_14295);
    if (_7946 == 0)
    {
        _7946 = NOVALUE;
        goto LC; // [147] 1022
    }
    else{
        _7946 = NOVALUE;
    }

    /** 			logical_line = search:match_replace({-2}, logical_line, "\\#3D")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -2;
    _7947 = MAKE_SEQ(_1);
    Ref(_logical_line_14295);
    RefDS(_7948);
    _0 = _logical_line_14295;
    _logical_line_14295 = _7match_replace(_7947, _logical_line_14295, _7948, 0);
    DeRef(_0);
    _7947 = NOVALUE;

    /** 			logical_line = search:match_replace({-3}, logical_line, "\\#23")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -3;
    _7950 = MAKE_SEQ(_1);
    Ref(_logical_line_14295);
    RefDS(_7951);
    _0 = _logical_line_14295;
    _logical_line_14295 = _7match_replace(_7950, _logical_line_14295, _7951, 0);
    DeRef(_0);
    _7950 = NOVALUE;

    /** 			logical_line = search:match_replace({-4}, logical_line, "\\#24")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -4;
    _7953 = MAKE_SEQ(_1);
    Ref(_logical_line_14295);
    RefDS(_7954);
    _0 = _logical_line_14295;
    _logical_line_14295 = _7match_replace(_7953, _logical_line_14295, _7954, 0);
    DeRef(_0);
    _7953 = NOVALUE;

    /** 			logical_line = search:match_replace({-5}, logical_line, "\\#2C")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -5;
    _7956 = MAKE_SEQ(_1);
    Ref(_logical_line_14295);
    RefDS(_7957);
    _0 = _logical_line_14295;
    _logical_line_14295 = _7match_replace(_7956, _logical_line_14295, _7957, 0);
    DeRef(_0);
    _7956 = NOVALUE;

    /** 			logical_line = search:match_replace({-6}, logical_line, "\\-")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -6;
    _7959 = MAKE_SEQ(_1);
    Ref(_logical_line_14295);
    RefDS(_7960);
    _0 = _logical_line_14295;
    _logical_line_14295 = _7match_replace(_7959, _logical_line_14295, _7960, 0);
    DeRef(_0);
    _7959 = NOVALUE;

    /** 			delim_pos = find('=', logical_line)*/
    _delim_pos_14297 = find_from(61, _logical_line_14295, 1);

    /** 			if delim_pos > 0 then*/
    if (_delim_pos_14297 <= 0)
    goto LD; // [224] 555

    /** 				data_key = text:trim(logical_line[1..delim_pos-1])*/
    _7964 = _delim_pos_14297 - 1;
    rhs_slice_target = (object_ptr)&_7965;
    RHS_Slice(_logical_line_14295, 1, _7964);
    RefDS(_4443);
    _0 = _data_key_14299;
    _data_key_14299 = _4trim(_7965, _4443, 0);
    DeRef(_0);
    _7965 = NOVALUE;

    /** 				if length(data_key) > 0 then*/
    if (IS_SEQUENCE(_data_key_14299)){
            _7967 = SEQ_PTR(_data_key_14299)->length;
    }
    else {
        _7967 = 1;
    }
    if (_7967 <= 0)
    goto LE; // [250] 554

    /** 					data_key = search:match_replace("\\#2C", data_key, ",")*/
    RefDS(_7957);
    Ref(_data_key_14299);
    RefDS(_7969);
    _0 = _data_key_14299;
    _data_key_14299 = _7match_replace(_7957, _data_key_14299, _7969, 0);
    DeRef(_0);

    /** 					data_key = search:match_replace("\\#24", data_key, "$")*/
    RefDS(_7954);
    Ref(_data_key_14299);
    RefDS(_7971);
    _0 = _data_key_14299;
    _data_key_14299 = _7match_replace(_7954, _data_key_14299, _7971, 0);
    DeRef(_0);

    /** 					data_key = search:match_replace("\\#22", data_key, "\"")*/
    RefDS(_7973);
    Ref(_data_key_14299);
    RefDS(_5440);
    _0 = _data_key_14299;
    _data_key_14299 = _7match_replace(_7973, _data_key_14299, _5440, 0);
    DeRef(_0);

    /** 					data_key = search:match_replace("\\#3D", data_key, "=")*/
    RefDS(_7948);
    Ref(_data_key_14299);
    RefDS(_3010);
    _0 = _data_key_14299;
    _data_key_14299 = _7match_replace(_7948, _data_key_14299, _3010, 0);
    DeRef(_0);

    /** 					data_key = search:match_replace("\\#23", data_key, "#")*/
    RefDS(_7951);
    Ref(_data_key_14299);
    RefDS(_291);
    _0 = _data_key_14299;
    _data_key_14299 = _7match_replace(_7951, _data_key_14299, _291, 0);
    DeRef(_0);

    /** 					data_key = search:match_replace("\\-", data_key, "-")*/
    RefDS(_7960);
    Ref(_data_key_14299);
    RefDS(_6143);
    _0 = _data_key_14299;
    _data_key_14299 = _7match_replace(_7960, _data_key_14299, _6143, 0);
    DeRef(_0);

    /** 					if not t_alpha(data_key[1]) then*/
    _2 = (int)SEQ_PTR(_data_key_14299);
    _7978 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_7978);
    _7979 = _5t_alpha(_7978);
    _7978 = NOVALUE;
    if (IS_ATOM_INT(_7979)) {
        if (_7979 != 0){
            DeRef(_7979);
            _7979 = NOVALUE;
            goto LF; // [318] 372
        }
    }
    else {
        if (DBL_PTR(_7979)->dbl != 0.0){
            DeRef(_7979);
            _7979 = NOVALUE;
            goto LF; // [318] 372
        }
    }
    DeRef(_7979);
    _7979 = NOVALUE;

    /** 						conv_res = stdget:value(data_key,,stdget:GET_LONG_ANSWER)*/
    if (!IS_ATOM_INT(_15GET_LONG_ANSWER_3205)) {
        _1 = (long)(DBL_PTR(_15GET_LONG_ANSWER_3205)->dbl);
        if (UNIQUE(DBL_PTR(_15GET_LONG_ANSWER_3205)) && (DBL_PTR(_15GET_LONG_ANSWER_3205)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_15GET_LONG_ANSWER_3205);
        _15GET_LONG_ANSWER_3205 = _1;
    }

    /** 	return get_value(st, start_point, answer)*/
    Ref(_data_key_14299);
    _0 = _conv_res_14300;
    _conv_res_14300 = _15get_value(_data_key_14299, 1, _15GET_LONG_ANSWER_3205);
    DeRef(_0);

    /** 						if conv_res[1] = stdget:GET_SUCCESS then*/
    _2 = (int)SEQ_PTR(_conv_res_14300);
    _7981 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _7981, 0)){
        _7981 = NOVALUE;
        goto L10; // [346] 371
    }
    _7981 = NOVALUE;

    /** 							if conv_res[3] = length(data_key) then*/
    _2 = (int)SEQ_PTR(_conv_res_14300);
    _7983 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_SEQUENCE(_data_key_14299)){
            _7984 = SEQ_PTR(_data_key_14299)->length;
    }
    else {
        _7984 = 1;
    }
    if (binary_op_a(NOTEQ, _7983, _7984)){
        _7983 = NOVALUE;
        _7984 = NOVALUE;
        goto L11; // [359] 370
    }
    _7983 = NOVALUE;
    _7984 = NOVALUE;

    /** 								data_key = conv_res[2]*/
    DeRef(_data_key_14299);
    _2 = (int)SEQ_PTR(_conv_res_14300);
    _data_key_14299 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_data_key_14299);
L11: 
L10: 
LF: 

    /** 					data_value = text:trim(logical_line[delim_pos+1..$])*/
    _7987 = _delim_pos_14297 + 1;
    if (_7987 > MAXINT){
        _7987 = NewDouble((double)_7987);
    }
    if (IS_SEQUENCE(_logical_line_14295)){
            _7988 = SEQ_PTR(_logical_line_14295)->length;
    }
    else {
        _7988 = 1;
    }
    rhs_slice_target = (object_ptr)&_7989;
    RHS_Slice(_logical_line_14295, _7987, _7988);
    RefDS(_4443);
    _0 = _data_value_14298;
    _data_value_14298 = _4trim(_7989, _4443, 0);
    DeRef(_0);
    _7989 = NOVALUE;

    /** 					if data_value[1] = '\"' and data_value[$] = '\"' then*/
    _2 = (int)SEQ_PTR(_data_value_14298);
    _7991 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_7991)) {
        _7992 = (_7991 == 34);
    }
    else {
        _7992 = binary_op(EQUALS, _7991, 34);
    }
    _7991 = NOVALUE;
    if (IS_ATOM_INT(_7992)) {
        if (_7992 == 0) {
            goto L12; // [402] 436
        }
    }
    else {
        if (DBL_PTR(_7992)->dbl == 0.0) {
            goto L12; // [402] 436
        }
    }
    if (IS_SEQUENCE(_data_value_14298)){
            _7994 = SEQ_PTR(_data_value_14298)->length;
    }
    else {
        _7994 = 1;
    }
    _2 = (int)SEQ_PTR(_data_value_14298);
    _7995 = (int)*(((s1_ptr)_2)->base + _7994);
    if (IS_ATOM_INT(_7995)) {
        _7996 = (_7995 == 34);
    }
    else {
        _7996 = binary_op(EQUALS, _7995, 34);
    }
    _7995 = NOVALUE;
    if (_7996 == 0) {
        DeRef(_7996);
        _7996 = NOVALUE;
        goto L12; // [418] 436
    }
    else {
        if (!IS_ATOM_INT(_7996) && DBL_PTR(_7996)->dbl == 0.0){
            DeRef(_7996);
            _7996 = NOVALUE;
            goto L12; // [418] 436
        }
        DeRef(_7996);
        _7996 = NOVALUE;
    }
    DeRef(_7996);
    _7996 = NOVALUE;

    /** 					    data_value = data_value[2..$-1]*/
    if (IS_SEQUENCE(_data_value_14298)){
            _7997 = SEQ_PTR(_data_value_14298)->length;
    }
    else {
        _7997 = 1;
    }
    _7998 = _7997 - 1;
    _7997 = NOVALUE;
    rhs_slice_target = (object_ptr)&_data_value_14298;
    RHS_Slice(_data_value_14298, 2, _7998);
L12: 

    /** 					data_value = search:match_replace("\\#2C", data_value, ",")*/
    RefDS(_7957);
    Ref(_data_value_14298);
    RefDS(_7969);
    _0 = _data_value_14298;
    _data_value_14298 = _7match_replace(_7957, _data_value_14298, _7969, 0);
    DeRef(_0);

    /** 					data_value = search:match_replace("\\#24", data_value, "$")*/
    RefDS(_7954);
    Ref(_data_value_14298);
    RefDS(_7971);
    _0 = _data_value_14298;
    _data_value_14298 = _7match_replace(_7954, _data_value_14298, _7971, 0);
    DeRef(_0);

    /** 					data_value = search:match_replace("\\#22", data_value, "\"")*/
    RefDS(_7973);
    Ref(_data_value_14298);
    RefDS(_5440);
    _0 = _data_value_14298;
    _data_value_14298 = _7match_replace(_7973, _data_value_14298, _5440, 0);
    DeRef(_0);

    /** 					data_value = search:match_replace("\\#3D", data_value, "=")*/
    RefDS(_7948);
    Ref(_data_value_14298);
    RefDS(_3010);
    _0 = _data_value_14298;
    _data_value_14298 = _7match_replace(_7948, _data_value_14298, _3010, 0);
    DeRef(_0);

    /** 					data_value = search:match_replace("\\#23", data_value, "#")*/
    RefDS(_7951);
    Ref(_data_value_14298);
    RefDS(_291);
    _0 = _data_value_14298;
    _data_value_14298 = _7match_replace(_7951, _data_value_14298, _291, 0);
    DeRef(_0);

    /** 					data_value = search:match_replace("\\-", data_value, "-")*/
    RefDS(_7960);
    Ref(_data_value_14298);
    RefDS(_6143);
    _0 = _data_value_14298;
    _data_value_14298 = _7match_replace(_7960, _data_value_14298, _6143, 0);
    DeRef(_0);

    /** 					conv_res = stdget:value(data_value,,stdget:GET_LONG_ANSWER)*/
    if (!IS_ATOM_INT(_15GET_LONG_ANSWER_3205)) {
        _1 = (long)(DBL_PTR(_15GET_LONG_ANSWER_3205)->dbl);
        if (UNIQUE(DBL_PTR(_15GET_LONG_ANSWER_3205)) && (DBL_PTR(_15GET_LONG_ANSWER_3205)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_15GET_LONG_ANSWER_3205);
        _15GET_LONG_ANSWER_3205 = _1;
    }

    /** 	return get_value(st, start_point, answer)*/
    Ref(_data_value_14298);
    _0 = _conv_res_14300;
    _conv_res_14300 = _15get_value(_data_value_14298, 1, _15GET_LONG_ANSWER_3205);
    DeRef(_0);

    /** 					if conv_res[1] = stdget:GET_SUCCESS then*/
    _2 = (int)SEQ_PTR(_conv_res_14300);
    _8006 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _8006, 0)){
        _8006 = NOVALUE;
        goto L13; // [515] 540
    }
    _8006 = NOVALUE;

    /** 						if conv_res[3] = length(data_value) then*/
    _2 = (int)SEQ_PTR(_conv_res_14300);
    _8008 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_SEQUENCE(_data_value_14298)){
            _8009 = SEQ_PTR(_data_value_14298)->length;
    }
    else {
        _8009 = 1;
    }
    if (binary_op_a(NOTEQ, _8008, _8009)){
        _8008 = NOVALUE;
        _8009 = NOVALUE;
        goto L14; // [528] 539
    }
    _8008 = NOVALUE;
    _8009 = NOVALUE;

    /** 							data_value = conv_res[2]*/
    DeRef(_data_value_14298);
    _2 = (int)SEQ_PTR(_conv_res_14300);
    _data_value_14298 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_data_value_14298);
L14: 
L13: 

    /** 					put(new_map, data_key, data_value)*/
    Ref(_new_map_14301);
    Ref(_data_key_14299);
    Ref(_data_value_14298);
    _32put(_new_map_14301, _data_key_14299, _data_value_14298, 1, _32threshold_size_13318);
LE: 
LD: 

    /** 		entry*/
LA: 

    /** 			logical_line = -1*/
    DeRef(_logical_line_14295);
    _logical_line_14295 = -1;

    /** 			while sequence(line_in) with entry do*/
    goto L15; // [565] 690
L16: 
    _8012 = IS_SEQUENCE(_line_in_14294);
    if (_8012 == 0)
    {
        _8012 = NOVALUE;
        goto LB; // [573] 142
    }
    else{
        _8012 = NOVALUE;
    }

    /** 				if atom(logical_line) then*/
    _8013 = IS_ATOM(_logical_line_14295);
    if (_8013 == 0)
    {
        _8013 = NOVALUE;
        goto L17; // [581] 590
    }
    else{
        _8013 = NOVALUE;
    }

    /** 					logical_line = ""*/
    RefDS(_5);
    DeRef(_logical_line_14295);
    _logical_line_14295 = _5;
L17: 

    /** 				has_comment = match("--", line_in)*/
    _has_comment_14296 = e_match_from(_8014, _line_in_14294, 1);

    /** 				if has_comment != 0 then*/
    if (_has_comment_14296 == 0)
    goto L18; // [599] 623

    /** 					line_in = text:trim(line_in[1..has_comment-1])*/
    _8017 = _has_comment_14296 - 1;
    rhs_slice_target = (object_ptr)&_8018;
    RHS_Slice(_line_in_14294, 1, _8017);
    RefDS(_4443);
    _0 = _line_in_14294;
    _line_in_14294 = _4trim(_8018, _4443, 0);
    DeRef(_0);
    _8018 = NOVALUE;
    goto L19; // [620] 632
L18: 

    /** 					line_in = text:trim(line_in)*/
    Ref(_line_in_14294);
    RefDS(_4443);
    _0 = _line_in_14294;
    _line_in_14294 = _4trim(_line_in_14294, _4443, 0);
    DeRef(_0);
L19: 

    /** 				logical_line &= line_in*/
    if (IS_SEQUENCE(_logical_line_14295) && IS_ATOM(_line_in_14294)) {
        Ref(_line_in_14294);
        Append(&_logical_line_14295, _logical_line_14295, _line_in_14294);
    }
    else if (IS_ATOM(_logical_line_14295) && IS_SEQUENCE(_line_in_14294)) {
        Ref(_logical_line_14295);
        Prepend(&_logical_line_14295, _line_in_14294, _logical_line_14295);
    }
    else {
        Concat((object_ptr)&_logical_line_14295, _logical_line_14295, _line_in_14294);
    }

    /** 				if length(line_in) then*/
    if (IS_SEQUENCE(_line_in_14294)){
            _8022 = SEQ_PTR(_line_in_14294)->length;
    }
    else {
        _8022 = 1;
    }
    if (_8022 == 0)
    {
        _8022 = NOVALUE;
        goto L1A; // [643] 687
    }
    else{
        _8022 = NOVALUE;
    }

    /** 					if not find(line_in[$], line_conts) then*/
    if (IS_SEQUENCE(_line_in_14294)){
            _8023 = SEQ_PTR(_line_in_14294)->length;
    }
    else {
        _8023 = 1;
    }
    _2 = (int)SEQ_PTR(_line_in_14294);
    _8024 = (int)*(((s1_ptr)_2)->base + _8023);
    _8025 = find_from(_8024, _line_conts_14302, 1);
    _8024 = NOVALUE;
    if (_8025 != 0)
    goto L1B; // [660] 686
    _8025 = NOVALUE;

    /** 						logical_line = search:match_replace(`",$"`, logical_line, "")*/
    RefDS(_8027);
    RefDS(_logical_line_14295);
    RefDS(_5);
    _0 = _logical_line_14295;
    _logical_line_14295 = _7match_replace(_8027, _logical_line_14295, _5, 0);
    DeRefDS(_0);

    /** 						logical_line = search:match_replace(`,$`, logical_line, "")*/
    RefDS(_8029);
    Ref(_logical_line_14295);
    RefDS(_5);
    _0 = _logical_line_14295;
    _logical_line_14295 = _7match_replace(_8029, _logical_line_14295, _5, 0);
    DeRef(_0);

    /** 						exit*/
    goto LB; // [683] 142
L1B: 
L1A: 

    /** 			entry*/
L15: 

    /** 				line_in = gets(file_handle)*/
    DeRef(_line_in_14294);
    _line_in_14294 = EGets(_file_handle_14293);

    /**                 integer in_quote = 0, last_in = -1, cur_in = -1*/
    _in_quote_14433 = 0;
    _last_in_14434 = -1;
    _cur_in_14435 = -1;

    /**                 if not equal(line_in, -1) then*/
    if (_line_in_14294 == -1)
    _8032 = 1;
    else if (IS_ATOM_INT(_line_in_14294) && IS_ATOM_INT(-1))
    _8032 = 0;
    else
    _8032 = (compare(_line_in_14294, -1) == 0);
    if (_8032 != 0)
    goto L1C; // [712] 866
    _8032 = NOVALUE;

    /**                     for i = 1 to length(line_in) do*/
    if (IS_SEQUENCE(_line_in_14294)){
            _8034 = SEQ_PTR(_line_in_14294)->length;
    }
    else {
        _8034 = 1;
    }
    {
        int _i_14440;
        _i_14440 = 1;
L1D: 
        if (_i_14440 > _8034){
            goto L1E; // [720] 865
        }

        /**                         cur_in = line_in[i]*/
        _2 = (int)SEQ_PTR(_line_in_14294);
        _cur_in_14435 = (int)*(((s1_ptr)_2)->base + _i_14440);
        if (!IS_ATOM_INT(_cur_in_14435)){
            _cur_in_14435 = (long)DBL_PTR(_cur_in_14435)->dbl;
        }

        /**                         if cur_in = '"' then*/
        if (_cur_in_14435 != 34)
        goto L1F; // [737] 749

        /**                             in_quote = not in_quote*/
        _in_quote_14433 = (_in_quote_14433 == 0);
        goto L20; // [746] 847
L1F: 

        /**                         elsif in_quote then*/
        if (_in_quote_14433 == 0)
        {
            goto L21; // [751] 846
        }
        else{
        }

        /**                             if cur_in = '=' then*/
        if (_cur_in_14435 != 61)
        goto L22; // [756] 768

        /**                                 cur_in = -2 */
        _cur_in_14435 = -2;
        goto L23; // [765] 845
L22: 

        /**                             elsif cur_in = '#' then*/
        if (_cur_in_14435 != 35)
        goto L24; // [770] 782

        /**                                 cur_in = -3 */
        _cur_in_14435 = -3;
        goto L23; // [779] 845
L24: 

        /**                             elsif cur_in = '$' then*/
        if (_cur_in_14435 != 36)
        goto L25; // [784] 796

        /**                                 cur_in = -4*/
        _cur_in_14435 = -4;
        goto L23; // [793] 845
L25: 

        /**                             elsif cur_in = ',' then*/
        if (_cur_in_14435 != 44)
        goto L26; // [798] 810

        /**                                 cur_in = -5*/
        _cur_in_14435 = -5;
        goto L23; // [807] 845
L26: 

        /**                             elsif cur_in = '-' and last_in = '-' then*/
        _8042 = (_cur_in_14435 == 45);
        if (_8042 == 0) {
            goto L27; // [816] 844
        }
        _8044 = (_last_in_14434 == 45);
        if (_8044 == 0)
        {
            DeRef(_8044);
            _8044 = NOVALUE;
            goto L27; // [825] 844
        }
        else{
            DeRef(_8044);
            _8044 = NOVALUE;
        }

        /**                                 cur_in = -6*/
        _cur_in_14435 = -6;

        /**                                 line_in[i-1] = -6*/
        _8045 = _i_14440 - 1;
        _2 = (int)SEQ_PTR(_line_in_14294);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _line_in_14294 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _8045);
        _1 = *(int *)_2;
        *(int *)_2 = -6;
        DeRef(_1);
L27: 
L23: 
L21: 
L20: 

        /**                         line_in[i] = cur_in*/
        _2 = (int)SEQ_PTR(_line_in_14294);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _line_in_14294 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_14440);
        _1 = *(int *)_2;
        *(int *)_2 = _cur_in_14435;
        DeRef(_1);

        /**                         last_in = cur_in*/
        _last_in_14434 = _cur_in_14435;

        /**                     end for*/
        _i_14440 = _i_14440 + 1;
        goto L1D; // [860] 727
L1E: 
        ;
    }
L1C: 

    /** 			end while*/
    goto L16; // [870] 568

    /** 		end while*/
    goto LB; // [875] 142
    goto LC; // [878] 1022
L9: 

    /** 		io:seek(file_handle, 0)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at885_14463);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _file_handle_14293;
    ((int *)_2)[2] = 0;
    _seek_1__tmp_at885_14463 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_885_14462 = machine(19, _seek_1__tmp_at885_14463);
    DeRefi(_seek_1__tmp_at885_14463);
    _seek_1__tmp_at885_14463 = NOVALUE;

    /** 		line_in  = serialize:deserialize(file_handle)*/
    _0 = _line_in_14294;
    _line_in_14294 = _24deserialize(_file_handle_14293, 1);
    DeRef(_0);

    /** 		if atom(line_in) then*/
    _8047 = IS_ATOM(_line_in_14294);
    if (_8047 == 0)
    {
        _8047 = NOVALUE;
        goto L28; // [908] 918
    }
    else{
        _8047 = NOVALUE;
    }

    /** 			return -2*/
    DeRef(_input_file_name_14292);
    DeRef(_line_in_14294);
    DeRef(_logical_line_14295);
    DeRef(_data_value_14298);
    DeRef(_data_key_14299);
    DeRef(_conv_res_14300);
    DeRef(_new_map_14301);
    DeRefi(_line_conts_14302);
    DeRef(_7964);
    _7964 = NOVALUE;
    DeRef(_7987);
    _7987 = NOVALUE;
    DeRef(_7998);
    _7998 = NOVALUE;
    DeRef(_7992);
    _7992 = NOVALUE;
    DeRef(_8017);
    _8017 = NOVALUE;
    DeRef(_8042);
    _8042 = NOVALUE;
    DeRef(_8045);
    _8045 = NOVALUE;
    return -2;
L28: 

    /** 		if length(line_in) > 1 then*/
    if (IS_SEQUENCE(_line_in_14294)){
            _8048 = SEQ_PTR(_line_in_14294)->length;
    }
    else {
        _8048 = 1;
    }
    if (_8048 <= 1)
    goto L29; // [923] 1014

    /** 			switch line_in[1] do*/
    _2 = (int)SEQ_PTR(_line_in_14294);
    _8050 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_8050) ){
        goto L2A; // [933] 1002
    }
    if(!IS_ATOM_INT(_8050)){
        if( (DBL_PTR(_8050)->dbl != (double) ((int) DBL_PTR(_8050)->dbl) ) ){
            goto L2A; // [933] 1002
        }
        _0 = (int) DBL_PTR(_8050)->dbl;
    }
    else {
        _0 = _8050;
    };
    _8050 = NOVALUE;
    switch ( _0 ){ 

        /** 				case 1, 2 then*/
        case 1:
        case 2:

        /** 					data_key   = serialize:deserialize(file_handle)*/
        _0 = _data_key_14299;
        _data_key_14299 = _24deserialize(_file_handle_14293, 1);
        DeRef(_0);

        /** 					data_value =  serialize:deserialize(file_handle)*/
        _0 = _data_value_14298;
        _data_value_14298 = _24deserialize(_file_handle_14293, 1);
        DeRef(_0);

        /** 					for i = 1 to length(data_key) do*/
        if (IS_SEQUENCE(_data_key_14299)){
                _8055 = SEQ_PTR(_data_key_14299)->length;
        }
        else {
            _8055 = 1;
        }
        {
            int _i_14477;
            _i_14477 = 1;
L2B: 
            if (_i_14477 > _8055){
                goto L2C; // [963] 998
            }

            /** 						put(new_map, data_key[i], data_value[i])*/
            _2 = (int)SEQ_PTR(_data_key_14299);
            _8056 = (int)*(((s1_ptr)_2)->base + _i_14477);
            _2 = (int)SEQ_PTR(_data_value_14298);
            _8057 = (int)*(((s1_ptr)_2)->base + _i_14477);
            Ref(_new_map_14301);
            Ref(_8056);
            Ref(_8057);
            _32put(_new_map_14301, _8056, _8057, 1, _32threshold_size_13318);
            _8056 = NOVALUE;
            _8057 = NOVALUE;

            /** 					end for*/
            _i_14477 = _i_14477 + 1;
            goto L2B; // [993] 970
L2C: 
            ;
        }
        goto L2D; // [998] 1021

        /** 				case else*/
        default:
L2A: 

        /** 					return -2*/
        DeRef(_input_file_name_14292);
        DeRef(_line_in_14294);
        DeRef(_logical_line_14295);
        DeRef(_data_value_14298);
        DeRef(_data_key_14299);
        DeRef(_conv_res_14300);
        DeRef(_new_map_14301);
        DeRefi(_line_conts_14302);
        DeRef(_7964);
        _7964 = NOVALUE;
        DeRef(_7987);
        _7987 = NOVALUE;
        DeRef(_7998);
        _7998 = NOVALUE;
        DeRef(_7992);
        _7992 = NOVALUE;
        DeRef(_8017);
        _8017 = NOVALUE;
        DeRef(_8042);
        _8042 = NOVALUE;
        DeRef(_8045);
        _8045 = NOVALUE;
        return -2;
    ;}    goto L2D; // [1011] 1021
L29: 

    /** 			return -2*/
    DeRef(_input_file_name_14292);
    DeRef(_line_in_14294);
    DeRef(_logical_line_14295);
    DeRef(_data_value_14298);
    DeRef(_data_key_14299);
    DeRef(_conv_res_14300);
    DeRef(_new_map_14301);
    DeRefi(_line_conts_14302);
    DeRef(_7964);
    _7964 = NOVALUE;
    DeRef(_7987);
    _7987 = NOVALUE;
    DeRef(_7998);
    _7998 = NOVALUE;
    DeRef(_7992);
    _7992 = NOVALUE;
    DeRef(_8017);
    _8017 = NOVALUE;
    DeRef(_8042);
    _8042 = NOVALUE;
    DeRef(_8045);
    _8045 = NOVALUE;
    return -2;
L2D: 
LC: 

    /** 	if sequence(input_file_name) then*/
    _8058 = IS_SEQUENCE(_input_file_name_14292);
    if (_8058 == 0)
    {
        _8058 = NOVALUE;
        goto L2E; // [1027] 1035
    }
    else{
        _8058 = NOVALUE;
    }

    /** 		close(file_handle)*/
    EClose(_file_handle_14293);
L2E: 

    /** 	optimize(new_map)*/
    Ref(_new_map_14301);
    RefDS(_7908);
    _32optimize(_new_map_14301, _32threshold_size_13318, _7908);

    /** 	return new_map*/
    DeRef(_input_file_name_14292);
    DeRef(_line_in_14294);
    DeRef(_logical_line_14295);
    DeRef(_data_value_14298);
    DeRef(_data_key_14299);
    DeRef(_conv_res_14300);
    DeRefi(_line_conts_14302);
    DeRef(_7964);
    _7964 = NOVALUE;
    DeRef(_7987);
    _7987 = NOVALUE;
    DeRef(_7998);
    _7998 = NOVALUE;
    DeRef(_7992);
    _7992 = NOVALUE;
    DeRef(_8017);
    _8017 = NOVALUE;
    DeRef(_8042);
    _8042 = NOVALUE;
    DeRef(_8045);
    _8045 = NOVALUE;
    return _new_map_14301;
    ;
}


int _32save_map(int _the_map__14490, int _file_name_p_14491, int _type__14492)
{
    int _file_handle__14493 = NOVALUE;
    int _keys__14494 = NOVALUE;
    int _values__14495 = NOVALUE;
    int _pretty_sprint_inlined_pretty_sprint_at_178_14528 = NOVALUE;
    int _options_inlined_pretty_sprint_at_175_14527 = NOVALUE;
    int _x_inlined_pretty_sprint_at_172_14526 = NOVALUE;
    int _pretty_sprint_inlined_pretty_sprint_at_332_14546 = NOVALUE;
    int _options_inlined_pretty_sprint_at_329_14545 = NOVALUE;
    int _x_inlined_pretty_sprint_at_326_14544 = NOVALUE;
    int _8111 = NOVALUE;
    int _8110 = NOVALUE;
    int _8109 = NOVALUE;
    int _8108 = NOVALUE;
    int _8107 = NOVALUE;
    int _8105 = NOVALUE;
    int _8104 = NOVALUE;
    int _8103 = NOVALUE;
    int _8102 = NOVALUE;
    int _8101 = NOVALUE;
    int _8100 = NOVALUE;
    int _8099 = NOVALUE;
    int _8098 = NOVALUE;
    int _8097 = NOVALUE;
    int _8096 = NOVALUE;
    int _8095 = NOVALUE;
    int _8094 = NOVALUE;
    int _8093 = NOVALUE;
    int _8092 = NOVALUE;
    int _8091 = NOVALUE;
    int _8090 = NOVALUE;
    int _8089 = NOVALUE;
    int _8088 = NOVALUE;
    int _8086 = NOVALUE;
    int _8085 = NOVALUE;
    int _8084 = NOVALUE;
    int _8083 = NOVALUE;
    int _8082 = NOVALUE;
    int _8081 = NOVALUE;
    int _8080 = NOVALUE;
    int _8079 = NOVALUE;
    int _8078 = NOVALUE;
    int _8077 = NOVALUE;
    int _8076 = NOVALUE;
    int _8075 = NOVALUE;
    int _8074 = NOVALUE;
    int _8073 = NOVALUE;
    int _8072 = NOVALUE;
    int _8071 = NOVALUE;
    int _8070 = NOVALUE;
    int _8068 = NOVALUE;
    int _8060 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_type__14492)) {
        _1 = (long)(DBL_PTR(_type__14492)->dbl);
        if (UNIQUE(DBL_PTR(_type__14492)) && (DBL_PTR(_type__14492)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_type__14492);
        _type__14492 = _1;
    }

    /** 	integer file_handle_ = -2*/
    _file_handle__14493 = -2;

    /** 	if sequence(file_name_p) then*/
    _8060 = IS_SEQUENCE(_file_name_p_14491);
    if (_8060 == 0)
    {
        _8060 = NOVALUE;
        goto L1; // [13] 45
    }
    else{
        _8060 = NOVALUE;
    }

    /** 		if type_ = SM_TEXT then*/
    if (_type__14492 != 1)
    goto L2; // [20] 34

    /** 			file_handle_ = open(file_name_p, "w")*/
    _file_handle__14493 = EOpen(_file_name_p_14491, _1123, 0);
    goto L3; // [31] 53
L2: 

    /** 			file_handle_ = open(file_name_p, "wb")*/
    _file_handle__14493 = EOpen(_file_name_p_14491, _1183, 0);
    goto L3; // [42] 53
L1: 

    /** 		file_handle_ = file_name_p*/
    Ref(_file_name_p_14491);
    _file_handle__14493 = _file_name_p_14491;
    if (!IS_ATOM_INT(_file_handle__14493)) {
        _1 = (long)(DBL_PTR(_file_handle__14493)->dbl);
        if (UNIQUE(DBL_PTR(_file_handle__14493)) && (DBL_PTR(_file_handle__14493)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_file_handle__14493);
        _file_handle__14493 = _1;
    }
L3: 

    /** 	if file_handle_ < 0 then*/
    if (_file_handle__14493 >= 0)
    goto L4; // [55] 66

    /** 		return -1*/
    DeRef(_the_map__14490);
    DeRef(_file_name_p_14491);
    DeRef(_keys__14494);
    DeRef(_values__14495);
    return -1;
L4: 

    /** 	keys_ = keys(the_map_)*/
    Ref(_the_map__14490);
    _0 = _keys__14494;
    _keys__14494 = _32keys(_the_map__14490, 0);
    DeRef(_0);

    /** 	values_ = values(the_map_)*/
    Ref(_the_map__14490);
    _0 = _values__14495;
    _values__14495 = _32values(_the_map__14490, 0, 0);
    DeRef(_0);

    /** 	if type_ = SM_RAW then*/
    if (_type__14492 != 2)
    goto L5; // [89] 141

    /** 		puts(file_handle_, serialize:serialize({*/
    _8068 = _10now_gmt();
    RefDS(_8069);
    _8070 = _10format(_8068, _8069);
    _8068 = NOVALUE;
    _8071 = _31version_string(0);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 2;
    *((int *)(_2+8)) = _8070;
    *((int *)(_2+12)) = _8071;
    _8072 = MAKE_SEQ(_1);
    _8071 = NOVALUE;
    _8070 = NOVALUE;
    _8073 = _24serialize(_8072);
    _8072 = NOVALUE;
    EPuts(_file_handle__14493, _8073); // DJP 
    DeRef(_8073);
    _8073 = NOVALUE;

    /** 		puts(file_handle_, serialize:serialize(keys_))*/
    RefDS(_keys__14494);
    _8074 = _24serialize(_keys__14494);
    EPuts(_file_handle__14493, _8074); // DJP 
    DeRef(_8074);
    _8074 = NOVALUE;

    /** 		puts(file_handle_, serialize:serialize(values_))*/
    RefDS(_values__14495);
    _8075 = _24serialize(_values__14495);
    EPuts(_file_handle__14493, _8075); // DJP 
    DeRef(_8075);
    _8075 = NOVALUE;
    goto L6; // [138] 487
L5: 

    /** 		for i = 1 to length(keys_) do*/
    if (IS_SEQUENCE(_keys__14494)){
            _8076 = SEQ_PTR(_keys__14494)->length;
    }
    else {
        _8076 = 1;
    }
    {
        int _i_14521;
        _i_14521 = 1;
L7: 
        if (_i_14521 > _8076){
            goto L8; // [146] 486
        }

        /** 			keys_[i] = pretty:pretty_sprint(keys_[i], {2,0,1,0,"%d","%.15g",32,127,1,0})*/
        _2 = (int)SEQ_PTR(_keys__14494);
        _8077 = (int)*(((s1_ptr)_2)->base + _i_14521);
        _1 = NewS1(10);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = 2;
        *((int *)(_2+8)) = 0;
        *((int *)(_2+12)) = 1;
        *((int *)(_2+16)) = 0;
        RefDS(_740);
        *((int *)(_2+20)) = _740;
        RefDS(_5919);
        *((int *)(_2+24)) = _5919;
        *((int *)(_2+28)) = 32;
        *((int *)(_2+32)) = 127;
        *((int *)(_2+36)) = 1;
        *((int *)(_2+40)) = 0;
        _8078 = MAKE_SEQ(_1);
        Ref(_8077);
        DeRef(_x_inlined_pretty_sprint_at_172_14526);
        _x_inlined_pretty_sprint_at_172_14526 = _8077;
        _8077 = NOVALUE;
        DeRef(_options_inlined_pretty_sprint_at_175_14527);
        _options_inlined_pretty_sprint_at_175_14527 = _8078;
        _8078 = NOVALUE;

        /** 	pretty_printing = 0*/
        _23pretty_printing_8552 = 0;

        /** 	pretty( x, options )*/
        Ref(_x_inlined_pretty_sprint_at_172_14526);
        RefDS(_options_inlined_pretty_sprint_at_175_14527);
        _23pretty(_x_inlined_pretty_sprint_at_172_14526, _options_inlined_pretty_sprint_at_175_14527);

        /** 	return pretty_line*/
        RefDS(_23pretty_line_8555);
        DeRef(_pretty_sprint_inlined_pretty_sprint_at_178_14528);
        _pretty_sprint_inlined_pretty_sprint_at_178_14528 = _23pretty_line_8555;
        DeRef(_x_inlined_pretty_sprint_at_172_14526);
        _x_inlined_pretty_sprint_at_172_14526 = NOVALUE;
        DeRef(_options_inlined_pretty_sprint_at_175_14527);
        _options_inlined_pretty_sprint_at_175_14527 = NOVALUE;
        RefDS(_pretty_sprint_inlined_pretty_sprint_at_178_14528);
        _2 = (int)SEQ_PTR(_keys__14494);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__14494 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_14521);
        _1 = *(int *)_2;
        *(int *)_2 = _pretty_sprint_inlined_pretty_sprint_at_178_14528;
        DeRef(_1);

        /** 			keys_[i] = search:match_replace("#", keys_[i], "\\#23")*/
        _2 = (int)SEQ_PTR(_keys__14494);
        _8079 = (int)*(((s1_ptr)_2)->base + _i_14521);
        RefDS(_291);
        Ref(_8079);
        RefDS(_7951);
        _8080 = _7match_replace(_291, _8079, _7951, 0);
        _8079 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__14494);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__14494 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_14521);
        _1 = *(int *)_2;
        *(int *)_2 = _8080;
        if( _1 != _8080 ){
            DeRef(_1);
        }
        _8080 = NOVALUE;

        /** 			keys_[i] = search:match_replace("\"", keys_[i], "\\#22")*/
        _2 = (int)SEQ_PTR(_keys__14494);
        _8081 = (int)*(((s1_ptr)_2)->base + _i_14521);
        RefDS(_5440);
        Ref(_8081);
        RefDS(_7973);
        _8082 = _7match_replace(_5440, _8081, _7973, 0);
        _8081 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__14494);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__14494 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_14521);
        _1 = *(int *)_2;
        *(int *)_2 = _8082;
        if( _1 != _8082 ){
            DeRef(_1);
        }
        _8082 = NOVALUE;

        /** 			keys_[i] = search:match_replace("$", keys_[i], "\\#24")*/
        _2 = (int)SEQ_PTR(_keys__14494);
        _8083 = (int)*(((s1_ptr)_2)->base + _i_14521);
        RefDS(_7971);
        Ref(_8083);
        RefDS(_7954);
        _8084 = _7match_replace(_7971, _8083, _7954, 0);
        _8083 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__14494);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__14494 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_14521);
        _1 = *(int *)_2;
        *(int *)_2 = _8084;
        if( _1 != _8084 ){
            DeRef(_1);
        }
        _8084 = NOVALUE;

        /** 			keys_[i] = search:match_replace(",", keys_[i], "\\#2C")*/
        _2 = (int)SEQ_PTR(_keys__14494);
        _8085 = (int)*(((s1_ptr)_2)->base + _i_14521);
        RefDS(_7969);
        Ref(_8085);
        RefDS(_7957);
        _8086 = _7match_replace(_7969, _8085, _7957, 0);
        _8085 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__14494);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__14494 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_14521);
        _1 = *(int *)_2;
        *(int *)_2 = _8086;
        if( _1 != _8086 ){
            DeRef(_1);
        }
        _8086 = NOVALUE;

        /** 			keys_[i] = search:match_replace("=", keys_[i], "\\#3D")*/
        _2 = (int)SEQ_PTR(_keys__14494);
        _8088 = (int)*(((s1_ptr)_2)->base + _i_14521);
        RefDS(_3010);
        Ref(_8088);
        RefDS(_7948);
        _8089 = _7match_replace(_3010, _8088, _7948, 0);
        _8088 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__14494);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__14494 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_14521);
        _1 = *(int *)_2;
        *(int *)_2 = _8089;
        if( _1 != _8089 ){
            DeRef(_1);
        }
        _8089 = NOVALUE;

        /** 			keys_[i] = search:match_replace("-", keys_[i], "\\-")*/
        _2 = (int)SEQ_PTR(_keys__14494);
        _8090 = (int)*(((s1_ptr)_2)->base + _i_14521);
        RefDS(_6143);
        Ref(_8090);
        RefDS(_7960);
        _8091 = _7match_replace(_6143, _8090, _7960, 0);
        _8090 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__14494);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__14494 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_14521);
        _1 = *(int *)_2;
        *(int *)_2 = _8091;
        if( _1 != _8091 ){
            DeRef(_1);
        }
        _8091 = NOVALUE;

        /** 			values_[i] = pretty:pretty_sprint(values_[i], {2,0,1,0,"%d","%.15g",32,127,1,0})*/
        _2 = (int)SEQ_PTR(_values__14495);
        _8092 = (int)*(((s1_ptr)_2)->base + _i_14521);
        _1 = NewS1(10);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = 2;
        *((int *)(_2+8)) = 0;
        *((int *)(_2+12)) = 1;
        *((int *)(_2+16)) = 0;
        RefDS(_740);
        *((int *)(_2+20)) = _740;
        RefDS(_5919);
        *((int *)(_2+24)) = _5919;
        *((int *)(_2+28)) = 32;
        *((int *)(_2+32)) = 127;
        *((int *)(_2+36)) = 1;
        *((int *)(_2+40)) = 0;
        _8093 = MAKE_SEQ(_1);
        Ref(_8092);
        DeRef(_x_inlined_pretty_sprint_at_326_14544);
        _x_inlined_pretty_sprint_at_326_14544 = _8092;
        _8092 = NOVALUE;
        DeRef(_options_inlined_pretty_sprint_at_329_14545);
        _options_inlined_pretty_sprint_at_329_14545 = _8093;
        _8093 = NOVALUE;

        /** 	pretty_printing = 0*/
        _23pretty_printing_8552 = 0;

        /** 	pretty( x, options )*/
        Ref(_x_inlined_pretty_sprint_at_326_14544);
        RefDS(_options_inlined_pretty_sprint_at_329_14545);
        _23pretty(_x_inlined_pretty_sprint_at_326_14544, _options_inlined_pretty_sprint_at_329_14545);

        /** 	return pretty_line*/
        RefDS(_23pretty_line_8555);
        DeRef(_pretty_sprint_inlined_pretty_sprint_at_332_14546);
        _pretty_sprint_inlined_pretty_sprint_at_332_14546 = _23pretty_line_8555;
        DeRef(_x_inlined_pretty_sprint_at_326_14544);
        _x_inlined_pretty_sprint_at_326_14544 = NOVALUE;
        DeRef(_options_inlined_pretty_sprint_at_329_14545);
        _options_inlined_pretty_sprint_at_329_14545 = NOVALUE;
        RefDS(_pretty_sprint_inlined_pretty_sprint_at_332_14546);
        _2 = (int)SEQ_PTR(_values__14495);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__14495 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_14521);
        _1 = *(int *)_2;
        *(int *)_2 = _pretty_sprint_inlined_pretty_sprint_at_332_14546;
        DeRef(_1);

        /** 			values_[i] = search:match_replace("#", values_[i], "\\#23")*/
        _2 = (int)SEQ_PTR(_values__14495);
        _8094 = (int)*(((s1_ptr)_2)->base + _i_14521);
        RefDS(_291);
        Ref(_8094);
        RefDS(_7951);
        _8095 = _7match_replace(_291, _8094, _7951, 0);
        _8094 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__14495);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__14495 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_14521);
        _1 = *(int *)_2;
        *(int *)_2 = _8095;
        if( _1 != _8095 ){
            DeRef(_1);
        }
        _8095 = NOVALUE;

        /** 			values_[i] = search:match_replace("\"", values_[i], "\\#22")*/
        _2 = (int)SEQ_PTR(_values__14495);
        _8096 = (int)*(((s1_ptr)_2)->base + _i_14521);
        RefDS(_5440);
        Ref(_8096);
        RefDS(_7973);
        _8097 = _7match_replace(_5440, _8096, _7973, 0);
        _8096 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__14495);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__14495 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_14521);
        _1 = *(int *)_2;
        *(int *)_2 = _8097;
        if( _1 != _8097 ){
            DeRef(_1);
        }
        _8097 = NOVALUE;

        /** 			values_[i] = search:match_replace("$", values_[i], "\\#24")*/
        _2 = (int)SEQ_PTR(_values__14495);
        _8098 = (int)*(((s1_ptr)_2)->base + _i_14521);
        RefDS(_7971);
        Ref(_8098);
        RefDS(_7954);
        _8099 = _7match_replace(_7971, _8098, _7954, 0);
        _8098 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__14495);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__14495 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_14521);
        _1 = *(int *)_2;
        *(int *)_2 = _8099;
        if( _1 != _8099 ){
            DeRef(_1);
        }
        _8099 = NOVALUE;

        /** 			values_[i] = search:match_replace(",", values_[i], "\\#2C")*/
        _2 = (int)SEQ_PTR(_values__14495);
        _8100 = (int)*(((s1_ptr)_2)->base + _i_14521);
        RefDS(_7969);
        Ref(_8100);
        RefDS(_7957);
        _8101 = _7match_replace(_7969, _8100, _7957, 0);
        _8100 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__14495);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__14495 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_14521);
        _1 = *(int *)_2;
        *(int *)_2 = _8101;
        if( _1 != _8101 ){
            DeRef(_1);
        }
        _8101 = NOVALUE;

        /** 			values_[i] = search:match_replace("=", values_[i], "\\#3D")*/
        _2 = (int)SEQ_PTR(_values__14495);
        _8102 = (int)*(((s1_ptr)_2)->base + _i_14521);
        RefDS(_3010);
        Ref(_8102);
        RefDS(_7948);
        _8103 = _7match_replace(_3010, _8102, _7948, 0);
        _8102 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__14495);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__14495 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_14521);
        _1 = *(int *)_2;
        *(int *)_2 = _8103;
        if( _1 != _8103 ){
            DeRef(_1);
        }
        _8103 = NOVALUE;

        /** 			values_[i] = search:match_replace("-", values_[i], "\\-")*/
        _2 = (int)SEQ_PTR(_values__14495);
        _8104 = (int)*(((s1_ptr)_2)->base + _i_14521);
        RefDS(_6143);
        Ref(_8104);
        RefDS(_7960);
        _8105 = _7match_replace(_6143, _8104, _7960, 0);
        _8104 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__14495);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__14495 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_14521);
        _1 = *(int *)_2;
        *(int *)_2 = _8105;
        if( _1 != _8105 ){
            DeRef(_1);
        }
        _8105 = NOVALUE;

        /** 			printf(file_handle_, "%s = %s\n", {keys_[i], values_[i]})*/
        _2 = (int)SEQ_PTR(_keys__14494);
        _8107 = (int)*(((s1_ptr)_2)->base + _i_14521);
        _2 = (int)SEQ_PTR(_values__14495);
        _8108 = (int)*(((s1_ptr)_2)->base + _i_14521);
        Ref(_8108);
        Ref(_8107);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _8107;
        ((int *)_2)[2] = _8108;
        _8109 = MAKE_SEQ(_1);
        _8108 = NOVALUE;
        _8107 = NOVALUE;
        EPrintf(_file_handle__14493, _8106, _8109);
        DeRefDS(_8109);
        _8109 = NOVALUE;

        /** 		end for*/
        _i_14521 = _i_14521 + 1;
        goto L7; // [481] 153
L8: 
        ;
    }
L6: 

    /** 	if sequence(file_name_p) then*/
    _8110 = IS_SEQUENCE(_file_name_p_14491);
    if (_8110 == 0)
    {
        _8110 = NOVALUE;
        goto L9; // [492] 500
    }
    else{
        _8110 = NOVALUE;
    }

    /** 		close(file_handle_)*/
    EClose(_file_handle__14493);
L9: 

    /** 	return length(keys_)*/
    if (IS_SEQUENCE(_keys__14494)){
            _8111 = SEQ_PTR(_keys__14494)->length;
    }
    else {
        _8111 = 1;
    }
    DeRef(_the_map__14490);
    DeRef(_file_name_p_14491);
    DeRefDS(_keys__14494);
    DeRef(_values__14495);
    return _8111;
    ;
}


int _32copy(int _source_map_14568, int _dest_map_14569, int _put_operation_14570)
{
    int _keys_set_14573 = NOVALUE;
    int _value_set_14574 = NOVALUE;
    int _source_data_14575 = NOVALUE;
    int _temp_map_14607 = NOVALUE;
    int _8135 = NOVALUE;
    int _8133 = NOVALUE;
    int _8132 = NOVALUE;
    int _8131 = NOVALUE;
    int _8130 = NOVALUE;
    int _8128 = NOVALUE;
    int _8127 = NOVALUE;
    int _8126 = NOVALUE;
    int _8125 = NOVALUE;
    int _8124 = NOVALUE;
    int _8123 = NOVALUE;
    int _8122 = NOVALUE;
    int _8120 = NOVALUE;
    int _8118 = NOVALUE;
    int _8117 = NOVALUE;
    int _8116 = NOVALUE;
    int _8114 = NOVALUE;
    int _8112 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_put_operation_14570)) {
        _1 = (long)(DBL_PTR(_put_operation_14570)->dbl);
        if (UNIQUE(DBL_PTR(_put_operation_14570)) && (DBL_PTR(_put_operation_14570)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_put_operation_14570);
        _put_operation_14570 = _1;
    }

    /** 	if map(dest_map) then*/
    Ref(_dest_map_14569);
    _8112 = _32map(_dest_map_14569);
    if (_8112 == 0) {
        DeRef(_8112);
        _8112 = NOVALUE;
        goto L1; // [9] 219
    }
    else {
        if (!IS_ATOM_INT(_8112) && DBL_PTR(_8112)->dbl == 0.0){
            DeRef(_8112);
            _8112 = NOVALUE;
            goto L1; // [9] 219
        }
        DeRef(_8112);
        _8112 = NOVALUE;
    }
    DeRef(_8112);
    _8112 = NOVALUE;

    /** 		sequence keys_set*/

    /** 		sequence value_set		*/

    /** 		sequence source_data*/

    /** 		source_data = eumem:ram_space[source_map]	*/
    DeRef(_source_data_14575);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!IS_ATOM_INT(_source_map_14568)){
        _source_data_14575 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_source_map_14568)->dbl));
    }
    else{
        _source_data_14575 = (int)*(((s1_ptr)_2)->base + _source_map_14568);
    }
    Ref(_source_data_14575);

    /** 		if source_data[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_source_data_14575);
    _8114 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _8114, 76)){
        _8114 = NOVALUE;
        goto L2; // [36] 134
    }
    _8114 = NOVALUE;

    /** 			for index = 1 to length(source_data[KEY_BUCKETS]) do*/
    _2 = (int)SEQ_PTR(_source_data_14575);
    _8116 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_8116)){
            _8117 = SEQ_PTR(_8116)->length;
    }
    else {
        _8117 = 1;
    }
    _8116 = NOVALUE;
    {
        int _index_14581;
        _index_14581 = 1;
L3: 
        if (_index_14581 > _8117){
            goto L4; // [51] 131
        }

        /** 				keys_set = source_data[KEY_BUCKETS][index]*/
        _2 = (int)SEQ_PTR(_source_data_14575);
        _8118 = (int)*(((s1_ptr)_2)->base + 5);
        DeRef(_keys_set_14573);
        _2 = (int)SEQ_PTR(_8118);
        _keys_set_14573 = (int)*(((s1_ptr)_2)->base + _index_14581);
        Ref(_keys_set_14573);
        _8118 = NOVALUE;

        /** 				value_set = source_data[VALUE_BUCKETS][index]*/
        _2 = (int)SEQ_PTR(_source_data_14575);
        _8120 = (int)*(((s1_ptr)_2)->base + 6);
        DeRef(_value_set_14574);
        _2 = (int)SEQ_PTR(_8120);
        _value_set_14574 = (int)*(((s1_ptr)_2)->base + _index_14581);
        Ref(_value_set_14574);
        _8120 = NOVALUE;

        /** 				for j = 1 to length(keys_set) do*/
        if (IS_SEQUENCE(_keys_set_14573)){
                _8122 = SEQ_PTR(_keys_set_14573)->length;
        }
        else {
            _8122 = 1;
        }
        {
            int _j_14589;
            _j_14589 = 1;
L5: 
            if (_j_14589 > _8122){
                goto L6; // [91] 124
            }

            /** 					put(dest_map, keys_set[j], value_set[j], put_operation)*/
            _2 = (int)SEQ_PTR(_keys_set_14573);
            _8123 = (int)*(((s1_ptr)_2)->base + _j_14589);
            _2 = (int)SEQ_PTR(_value_set_14574);
            _8124 = (int)*(((s1_ptr)_2)->base + _j_14589);
            Ref(_dest_map_14569);
            Ref(_8123);
            Ref(_8124);
            _32put(_dest_map_14569, _8123, _8124, _put_operation_14570, _32threshold_size_13318);
            _8123 = NOVALUE;
            _8124 = NOVALUE;

            /** 				end for*/
            _j_14589 = _j_14589 + 1;
            goto L5; // [119] 98
L6: 
            ;
        }

        /** 			end for*/
        _index_14581 = _index_14581 + 1;
        goto L3; // [126] 58
L4: 
        ;
    }
    goto L7; // [131] 208
L2: 

    /** 			for index = 1 to length(source_data[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_source_data_14575);
    _8125 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_8125)){
            _8126 = SEQ_PTR(_8125)->length;
    }
    else {
        _8126 = 1;
    }
    _8125 = NOVALUE;
    {
        int _index_14595;
        _index_14595 = 1;
L8: 
        if (_index_14595 > _8126){
            goto L9; // [145] 207
        }

        /** 				if source_data[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_source_data_14575);
        _8127 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_8127);
        _8128 = (int)*(((s1_ptr)_2)->base + _index_14595);
        _8127 = NOVALUE;
        if (binary_op_a(EQUALS, _8128, 0)){
            _8128 = NOVALUE;
            goto LA; // [164] 200
        }
        _8128 = NOVALUE;

        /** 					put(dest_map, source_data[KEY_LIST][index], */
        _2 = (int)SEQ_PTR(_source_data_14575);
        _8130 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_8130);
        _8131 = (int)*(((s1_ptr)_2)->base + _index_14595);
        _8130 = NOVALUE;
        _2 = (int)SEQ_PTR(_source_data_14575);
        _8132 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_8132);
        _8133 = (int)*(((s1_ptr)_2)->base + _index_14595);
        _8132 = NOVALUE;
        Ref(_dest_map_14569);
        Ref(_8131);
        Ref(_8133);
        _32put(_dest_map_14569, _8131, _8133, _put_operation_14570, _32threshold_size_13318);
        _8131 = NOVALUE;
        _8133 = NOVALUE;
LA: 

        /** 			end for*/
        _index_14595 = _index_14595 + 1;
        goto L8; // [202] 152
L9: 
        ;
    }
L7: 

    /** 		return dest_map*/
    DeRef(_keys_set_14573);
    DeRef(_value_set_14574);
    DeRef(_source_data_14575);
    DeRef(_source_map_14568);
    _8116 = NOVALUE;
    _8125 = NOVALUE;
    return _dest_map_14569;
    goto LB; // [216] 249
L1: 

    /** 		atom temp_map = eumem:malloc()*/
    _0 = _temp_map_14607;
    _temp_map_14607 = _33malloc(1, 1);
    DeRef(_0);

    /** 	 	eumem:ram_space[temp_map] = eumem:ram_space[source_map]*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!IS_ATOM_INT(_source_map_14568)){
        _8135 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_source_map_14568)->dbl));
    }
    else{
        _8135 = (int)*(((s1_ptr)_2)->base + _source_map_14568);
    }
    Ref(_8135);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _33ram_space_12557 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_temp_map_14607))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_temp_map_14607)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _temp_map_14607);
    _1 = *(int *)_2;
    *(int *)_2 = _8135;
    if( _1 != _8135 ){
        DeRef(_1);
    }
    _8135 = NOVALUE;

    /** 		return temp_map*/
    DeRef(_source_map_14568);
    DeRef(_dest_map_14569);
    _8116 = NOVALUE;
    _8125 = NOVALUE;
    return _temp_map_14607;
    DeRef(_temp_map_14607);
    _temp_map_14607 = NOVALUE;
LB: 
    ;
}


int _32new_from_kvpairs(int _kv_pairs_14612)
{
    int _new_map_14613 = NOVALUE;
    int _8147 = NOVALUE;
    int _8146 = NOVALUE;
    int _8145 = NOVALUE;
    int _8144 = NOVALUE;
    int _8142 = NOVALUE;
    int _8141 = NOVALUE;
    int _8140 = NOVALUE;
    int _8138 = NOVALUE;
    int _8137 = NOVALUE;
    int _8136 = NOVALUE;
    int _0, _1, _2;
    

    /** 	new_map = new( floor(7 * length(kv_pairs) / 2) )*/
    if (IS_SEQUENCE(_kv_pairs_14612)){
            _8136 = SEQ_PTR(_kv_pairs_14612)->length;
    }
    else {
        _8136 = 1;
    }
    if (_8136 <= INT15)
    _8137 = 7 * _8136;
    else
    _8137 = NewDouble(7 * (double)_8136);
    _8136 = NOVALUE;
    if (IS_ATOM_INT(_8137)) {
        _8138 = _8137 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _8137, 2);
        _8138 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    DeRef(_8137);
    _8137 = NOVALUE;
    _0 = _new_map_14613;
    _new_map_14613 = _32new(_8138);
    DeRef(_0);
    _8138 = NOVALUE;

    /** 	for i = 1 to length(kv_pairs) do*/
    if (IS_SEQUENCE(_kv_pairs_14612)){
            _8140 = SEQ_PTR(_kv_pairs_14612)->length;
    }
    else {
        _8140 = 1;
    }
    {
        int _i_14619;
        _i_14619 = 1;
L1: 
        if (_i_14619 > _8140){
            goto L2; // [25] 82
        }

        /** 		if length(kv_pairs[i]) = 2 then*/
        _2 = (int)SEQ_PTR(_kv_pairs_14612);
        _8141 = (int)*(((s1_ptr)_2)->base + _i_14619);
        if (IS_SEQUENCE(_8141)){
                _8142 = SEQ_PTR(_8141)->length;
        }
        else {
            _8142 = 1;
        }
        _8141 = NOVALUE;
        if (_8142 != 2)
        goto L3; // [41] 75

        /** 			put(new_map, kv_pairs[i][1], kv_pairs[i][2])*/
        _2 = (int)SEQ_PTR(_kv_pairs_14612);
        _8144 = (int)*(((s1_ptr)_2)->base + _i_14619);
        _2 = (int)SEQ_PTR(_8144);
        _8145 = (int)*(((s1_ptr)_2)->base + 1);
        _8144 = NOVALUE;
        _2 = (int)SEQ_PTR(_kv_pairs_14612);
        _8146 = (int)*(((s1_ptr)_2)->base + _i_14619);
        _2 = (int)SEQ_PTR(_8146);
        _8147 = (int)*(((s1_ptr)_2)->base + 2);
        _8146 = NOVALUE;
        Ref(_new_map_14613);
        Ref(_8145);
        Ref(_8147);
        _32put(_new_map_14613, _8145, _8147, 1, _32threshold_size_13318);
        _8145 = NOVALUE;
        _8147 = NOVALUE;
L3: 

        /** 	end for*/
        _i_14619 = _i_14619 + 1;
        goto L1; // [77] 32
L2: 
        ;
    }

    /** 	return new_map	*/
    DeRefDS(_kv_pairs_14612);
    _8141 = NOVALUE;
    return _new_map_14613;
    ;
}


int _32new_from_string(int _kv_string_14631)
{
    int _8149 = NOVALUE;
    int _8148 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return new_from_kvpairs( text:keyvalues (kv_string) )*/
    RefDS(_kv_string_14631);
    RefDS(_5263);
    RefDS(_5264);
    RefDS(_5265);
    RefDS(_121);
    _8148 = _4keyvalues(_kv_string_14631, _5263, _5264, _5265, _121, 1);
    _8149 = _32new_from_kvpairs(_8148);
    _8148 = NOVALUE;
    DeRefDS(_kv_string_14631);
    return _8149;
    ;
}


int _32for_each(int _source_map_14636, int _user_rid_14637, int _user_data_14638, int _in_sorted_order_14639, int _signal_boundary_14640)
{
    int _lKV_14641 = NOVALUE;
    int _lRes_14642 = NOVALUE;
    int _progress_code_14643 = NOVALUE;
    int _8168 = NOVALUE;
    int _8166 = NOVALUE;
    int _8165 = NOVALUE;
    int _8164 = NOVALUE;
    int _8163 = NOVALUE;
    int _8162 = NOVALUE;
    int _8160 = NOVALUE;
    int _8159 = NOVALUE;
    int _8158 = NOVALUE;
    int _8157 = NOVALUE;
    int _8156 = NOVALUE;
    int _8155 = NOVALUE;
    int _8154 = NOVALUE;
    int _8153 = NOVALUE;
    int _8152 = NOVALUE;
    int _8151 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_user_rid_14637)) {
        _1 = (long)(DBL_PTR(_user_rid_14637)->dbl);
        if (UNIQUE(DBL_PTR(_user_rid_14637)) && (DBL_PTR(_user_rid_14637)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_user_rid_14637);
        _user_rid_14637 = _1;
    }
    if (!IS_ATOM_INT(_in_sorted_order_14639)) {
        _1 = (long)(DBL_PTR(_in_sorted_order_14639)->dbl);
        if (UNIQUE(DBL_PTR(_in_sorted_order_14639)) && (DBL_PTR(_in_sorted_order_14639)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_in_sorted_order_14639);
        _in_sorted_order_14639 = _1;
    }
    if (!IS_ATOM_INT(_signal_boundary_14640)) {
        _1 = (long)(DBL_PTR(_signal_boundary_14640)->dbl);
        if (UNIQUE(DBL_PTR(_signal_boundary_14640)) && (DBL_PTR(_signal_boundary_14640)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_signal_boundary_14640);
        _signal_boundary_14640 = _1;
    }

    /** 	lKV = pairs(source_map, in_sorted_order)	*/
    Ref(_source_map_14636);
    _0 = _lKV_14641;
    _lKV_14641 = _32pairs(_source_map_14636, _in_sorted_order_14639);
    DeRef(_0);

    /** 	if length(lKV) = 0 and signal_boundary != 0 then*/
    if (IS_SEQUENCE(_lKV_14641)){
            _8151 = SEQ_PTR(_lKV_14641)->length;
    }
    else {
        _8151 = 1;
    }
    _8152 = (_8151 == 0);
    _8151 = NOVALUE;
    if (_8152 == 0) {
        goto L1; // [25] 55
    }
    _8154 = (_signal_boundary_14640 != 0);
    if (_8154 == 0)
    {
        DeRef(_8154);
        _8154 = NOVALUE;
        goto L1; // [34] 55
    }
    else{
        DeRef(_8154);
        _8154 = NOVALUE;
    }

    /** 		return call_func(user_rid, {0,0,user_data,0} )*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    Ref(_user_data_14638);
    *((int *)(_2+12)) = _user_data_14638;
    *((int *)(_2+16)) = 0;
    _8155 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_8155);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_user_rid_14637].addr;
    Ref(*(int *)(_2+4));
    Ref(*(int *)(_2+8));
    Ref(*(int *)(_2+12));
    Ref(*(int *)(_2+16));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4), 
                        *(int *)(_2+8), 
                        *(int *)(_2+12), 
                        *(int *)(_2+16)
                         );
    DeRef(_8156);
    _8156 = _1;
    DeRefDS(_8155);
    _8155 = NOVALUE;
    DeRef(_source_map_14636);
    DeRef(_user_data_14638);
    DeRef(_lKV_14641);
    DeRef(_lRes_14642);
    DeRef(_8152);
    _8152 = NOVALUE;
    return _8156;
L1: 

    /** 	for i = 1 to length(lKV) do*/
    if (IS_SEQUENCE(_lKV_14641)){
            _8157 = SEQ_PTR(_lKV_14641)->length;
    }
    else {
        _8157 = 1;
    }
    {
        int _i_14653;
        _i_14653 = 1;
L2: 
        if (_i_14653 > _8157){
            goto L3; // [60] 154
        }

        /** 		if i = length(lKV) and signal_boundary then*/
        if (IS_SEQUENCE(_lKV_14641)){
                _8158 = SEQ_PTR(_lKV_14641)->length;
        }
        else {
            _8158 = 1;
        }
        _8159 = (_i_14653 == _8158);
        _8158 = NOVALUE;
        if (_8159 == 0) {
            goto L4; // [76] 94
        }
        if (_signal_boundary_14640 == 0)
        {
            goto L4; // [81] 94
        }
        else{
        }

        /** 			progress_code = -i*/
        _progress_code_14643 = - _i_14653;
        goto L5; // [91] 100
L4: 

        /** 			progress_code = i*/
        _progress_code_14643 = _i_14653;
L5: 

        /** 		lRes = call_func(user_rid, {lKV[i][1], lKV[i][2], user_data, progress_code})*/
        _2 = (int)SEQ_PTR(_lKV_14641);
        _8162 = (int)*(((s1_ptr)_2)->base + _i_14653);
        _2 = (int)SEQ_PTR(_8162);
        _8163 = (int)*(((s1_ptr)_2)->base + 1);
        _8162 = NOVALUE;
        _2 = (int)SEQ_PTR(_lKV_14641);
        _8164 = (int)*(((s1_ptr)_2)->base + _i_14653);
        _2 = (int)SEQ_PTR(_8164);
        _8165 = (int)*(((s1_ptr)_2)->base + 2);
        _8164 = NOVALUE;
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_8163);
        *((int *)(_2+4)) = _8163;
        Ref(_8165);
        *((int *)(_2+8)) = _8165;
        Ref(_user_data_14638);
        *((int *)(_2+12)) = _user_data_14638;
        *((int *)(_2+16)) = _progress_code_14643;
        _8166 = MAKE_SEQ(_1);
        _8165 = NOVALUE;
        _8163 = NOVALUE;
        _1 = (int)SEQ_PTR(_8166);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_user_rid_14637].addr;
        Ref(*(int *)(_2+4));
        Ref(*(int *)(_2+8));
        Ref(*(int *)(_2+12));
        Ref(*(int *)(_2+16));
        _1 = (*(int (*)())_0)(
                            *(int *)(_2+4), 
                            *(int *)(_2+8), 
                            *(int *)(_2+12), 
                            *(int *)(_2+16)
                             );
        DeRef(_lRes_14642);
        _lRes_14642 = _1;
        DeRefDS(_8166);
        _8166 = NOVALUE;

        /** 		if not equal(lRes, 0) then*/
        if (_lRes_14642 == 0)
        _8168 = 1;
        else if (IS_ATOM_INT(_lRes_14642) && IS_ATOM_INT(0))
        _8168 = 0;
        else
        _8168 = (compare(_lRes_14642, 0) == 0);
        if (_8168 != 0)
        goto L6; // [137] 147
        _8168 = NOVALUE;

        /** 			return lRes*/
        DeRef(_source_map_14636);
        DeRef(_user_data_14638);
        DeRefDS(_lKV_14641);
        DeRef(_8152);
        _8152 = NOVALUE;
        DeRef(_8159);
        _8159 = NOVALUE;
        DeRef(_8156);
        _8156 = NOVALUE;
        return _lRes_14642;
L6: 

        /** 	end for*/
        _i_14653 = _i_14653 + 1;
        goto L2; // [149] 67
L3: 
        ;
    }

    /** 	return 0*/
    DeRef(_source_map_14636);
    DeRef(_user_data_14638);
    DeRef(_lKV_14641);
    DeRef(_lRes_14642);
    DeRef(_8152);
    _8152 = NOVALUE;
    DeRef(_8159);
    _8159 = NOVALUE;
    DeRef(_8156);
    _8156 = NOVALUE;
    return 0;
    ;
}


void _32convert_to_large_map(int _the_map__14672)
{
    int _temp_map__14673 = NOVALUE;
    int _map_handle__14674 = NOVALUE;
    int _8181 = NOVALUE;
    int _8180 = NOVALUE;
    int _8179 = NOVALUE;
    int _8178 = NOVALUE;
    int _8177 = NOVALUE;
    int _8175 = NOVALUE;
    int _8174 = NOVALUE;
    int _8173 = NOVALUE;
    int _8172 = NOVALUE;
    int _0, _1, _2;
    

    /** 	temp_map_ = eumem:ram_space[the_map_]*/
    DeRef(_temp_map__14673);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    _temp_map__14673 = (int)*(((s1_ptr)_2)->base + _the_map__14672);
    Ref(_temp_map__14673);

    /** 	map_handle_ = new()*/
    _0 = _map_handle__14674;
    _map_handle__14674 = _32new(690);
    DeRef(_0);

    /** 	for index = 1 to length(temp_map_[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_temp_map__14673);
    _8172 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_8172)){
            _8173 = SEQ_PTR(_8172)->length;
    }
    else {
        _8173 = 1;
    }
    _8172 = NOVALUE;
    {
        int _index_14678;
        _index_14678 = 1;
L1: 
        if (_index_14678 > _8173){
            goto L2; // [30] 94
        }

        /** 		if temp_map_[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_temp_map__14673);
        _8174 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_8174);
        _8175 = (int)*(((s1_ptr)_2)->base + _index_14678);
        _8174 = NOVALUE;
        if (binary_op_a(EQUALS, _8175, 0)){
            _8175 = NOVALUE;
            goto L3; // [49] 87
        }
        _8175 = NOVALUE;

        /** 			put(map_handle_, temp_map_[KEY_LIST][index], temp_map_[VALUE_LIST][index])*/
        _2 = (int)SEQ_PTR(_temp_map__14673);
        _8177 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_8177);
        _8178 = (int)*(((s1_ptr)_2)->base + _index_14678);
        _8177 = NOVALUE;
        _2 = (int)SEQ_PTR(_temp_map__14673);
        _8179 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_8179);
        _8180 = (int)*(((s1_ptr)_2)->base + _index_14678);
        _8179 = NOVALUE;
        Ref(_map_handle__14674);
        Ref(_8178);
        Ref(_8180);
        _32put(_map_handle__14674, _8178, _8180, 1, _32threshold_size_13318);
        _8178 = NOVALUE;
        _8180 = NOVALUE;
L3: 

        /** 	end for*/
        _index_14678 = _index_14678 + 1;
        goto L1; // [89] 37
L2: 
        ;
    }

    /** 	eumem:ram_space[the_map_] = eumem:ram_space[map_handle_]*/
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!IS_ATOM_INT(_map_handle__14674)){
        _8181 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_map_handle__14674)->dbl));
    }
    else{
        _8181 = (int)*(((s1_ptr)_2)->base + _map_handle__14674);
    }
    Ref(_8181);
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _33ram_space_12557 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map__14672);
    _1 = *(int *)_2;
    *(int *)_2 = _8181;
    if( _1 != _8181 ){
        DeRef(_1);
    }
    _8181 = NOVALUE;

    /** end procedure*/
    DeRef(_temp_map__14673);
    DeRef(_map_handle__14674);
    _8172 = NOVALUE;
    return;
    ;
}


void _32convert_to_small_map(int _the_map__14692)
{
    int _keys__14693 = NOVALUE;
    int _values__14694 = NOVALUE;
    int _8190 = NOVALUE;
    int _8189 = NOVALUE;
    int _8188 = NOVALUE;
    int _8187 = NOVALUE;
    int _8186 = NOVALUE;
    int _8185 = NOVALUE;
    int _8184 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_the_map__14692)) {
        _1 = (long)(DBL_PTR(_the_map__14692)->dbl);
        if (UNIQUE(DBL_PTR(_the_map__14692)) && (DBL_PTR(_the_map__14692)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map__14692);
        _the_map__14692 = _1;
    }

    /** 	keys_ = keys(the_map_)*/
    _0 = _keys__14693;
    _keys__14693 = _32keys(_the_map__14692, 0);
    DeRef(_0);

    /** 	values_ = values(the_map_)*/
    _0 = _values__14694;
    _values__14694 = _32values(_the_map__14692, 0, 0);
    DeRef(_0);

    /** 	eumem:ram_space[the_map_] = {*/
    _8184 = Repeat(_32init_small_map_key_13319, _32threshold_size_13318);
    _8185 = Repeat(0, _32threshold_size_13318);
    _8186 = Repeat(0, _32threshold_size_13318);
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_32type_is_map_13297);
    *((int *)(_2+4)) = _32type_is_map_13297;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 115;
    *((int *)(_2+20)) = _8184;
    *((int *)(_2+24)) = _8185;
    *((int *)(_2+28)) = _8186;
    _8187 = MAKE_SEQ(_1);
    _8186 = NOVALUE;
    _8185 = NOVALUE;
    _8184 = NOVALUE;
    _2 = (int)SEQ_PTR(_33ram_space_12557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _33ram_space_12557 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map__14692);
    _1 = *(int *)_2;
    *(int *)_2 = _8187;
    if( _1 != _8187 ){
        DeRef(_1);
    }
    _8187 = NOVALUE;

    /** 	for i = 1 to length(keys_) do*/
    if (IS_SEQUENCE(_keys__14693)){
            _8188 = SEQ_PTR(_keys__14693)->length;
    }
    else {
        _8188 = 1;
    }
    {
        int _i_14702;
        _i_14702 = 1;
L1: 
        if (_i_14702 > _8188){
            goto L2; // [63] 96
        }

        /** 		put(the_map_, keys_[i], values_[i], PUT, 0)*/
        _2 = (int)SEQ_PTR(_keys__14693);
        _8189 = (int)*(((s1_ptr)_2)->base + _i_14702);
        _2 = (int)SEQ_PTR(_values__14694);
        _8190 = (int)*(((s1_ptr)_2)->base + _i_14702);
        Ref(_8189);
        Ref(_8190);
        _32put(_the_map__14692, _8189, _8190, 1, 0);
        _8189 = NOVALUE;
        _8190 = NOVALUE;

        /** 	end for*/
        _i_14702 = _i_14702 + 1;
        goto L1; // [91] 70
L2: 
        ;
    }

    /** end procedure*/
    DeRef(_keys__14693);
    DeRef(_values__14694);
    return;
    ;
}



// 0x4B117BEE
