// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _7find_all(int _needle_737, int _haystack_738, int _start_739)
{
    int _kx_740 = NOVALUE;
    int _314 = NOVALUE;
    int _313 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer kx = 0*/
    _kx_740 = 0;

    /** 	while start with entry do*/
    goto L1; // [12] 39
L2: 
    if (_start_739 == 0)
    {
        goto L3; // [15] 51
    }
    else{
    }

    /** 		kx += 1*/
    _kx_740 = _kx_740 + 1;

    /** 		haystack[kx] = start*/
    _2 = (int)SEQ_PTR(_haystack_738);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _haystack_738 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _kx_740);
    _1 = *(int *)_2;
    *(int *)_2 = _start_739;
    DeRef(_1);

    /** 		start += 1*/
    _start_739 = _start_739 + 1;

    /** 	entry*/
L1: 

    /** 		start = find(needle, haystack, start)*/
    _start_739 = find_from(_needle_737, _haystack_738, _start_739);

    /** 	end while*/
    goto L2; // [48] 15
L3: 

    /** 	haystack = remove( haystack, kx+1, length( haystack ) )*/
    _313 = _kx_740 + 1;
    if (_313 > MAXINT){
        _313 = NewDouble((double)_313);
    }
    if (IS_SEQUENCE(_haystack_738)){
            _314 = SEQ_PTR(_haystack_738)->length;
    }
    else {
        _314 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_haystack_738);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_313)) ? _313 : (long)(DBL_PTR(_313)->dbl);
        int stop = (IS_ATOM_INT(_314)) ? _314 : (long)(DBL_PTR(_314)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_haystack_738), start, &_haystack_738 );
            }
            else Tail(SEQ_PTR(_haystack_738), stop+1, &_haystack_738);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_haystack_738), start, &_haystack_738);
        }
        else {
            assign_slice_seq = &assign_space;
            _haystack_738 = Remove_elements(start, stop, (SEQ_PTR(_haystack_738)->ref == 1));
        }
    }
    DeRef(_313);
    _313 = NOVALUE;
    _314 = NOVALUE;

    /** 	return haystack*/
    return _haystack_738;
    ;
}


int _7rfind(int _needle_856, int _haystack_857, int _start_858)
{
    int _len_860 = NOVALUE;
    int _376 = NOVALUE;
    int _375 = NOVALUE;
    int _372 = NOVALUE;
    int _371 = NOVALUE;
    int _369 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer len = length(haystack)*/
    if (IS_SEQUENCE(_haystack_857)){
            _len_860 = SEQ_PTR(_haystack_857)->length;
    }
    else {
        _len_860 = 1;
    }

    /** 	if start = 0 then start = len end if*/
    if (_start_858 != 0)
    goto L1; // [12] 20
    _start_858 = _len_860;
L1: 

    /** 	if (start > len) or (len + start < 1) then*/
    _369 = (_start_858 > _len_860);
    if (_369 != 0) {
        goto L2; // [26] 43
    }
    _371 = _len_860 + _start_858;
    if ((long)((unsigned long)_371 + (unsigned long)HIGH_BITS) >= 0) 
    _371 = NewDouble((double)_371);
    if (IS_ATOM_INT(_371)) {
        _372 = (_371 < 1);
    }
    else {
        _372 = (DBL_PTR(_371)->dbl < (double)1);
    }
    DeRef(_371);
    _371 = NOVALUE;
    if (_372 == 0)
    {
        DeRef(_372);
        _372 = NOVALUE;
        goto L3; // [39] 50
    }
    else{
        DeRef(_372);
        _372 = NOVALUE;
    }
L2: 

    /** 		return 0*/
    DeRef(_needle_856);
    DeRefDS(_haystack_857);
    DeRef(_369);
    _369 = NOVALUE;
    return 0;
L3: 

    /** 	if start < 1 then*/
    if (_start_858 >= 1)
    goto L4; // [52] 63

    /** 		start = len + start*/
    _start_858 = _len_860 + _start_858;
L4: 

    /** 	for i = start to 1 by -1 do*/
    {
        int _i_873;
        _i_873 = _start_858;
L5: 
        if (_i_873 < 1){
            goto L6; // [65] 99
        }

        /** 		if equal(haystack[i], needle) then*/
        _2 = (int)SEQ_PTR(_haystack_857);
        _375 = (int)*(((s1_ptr)_2)->base + _i_873);
        if (_375 == _needle_856)
        _376 = 1;
        else if (IS_ATOM_INT(_375) && IS_ATOM_INT(_needle_856))
        _376 = 0;
        else
        _376 = (compare(_375, _needle_856) == 0);
        _375 = NOVALUE;
        if (_376 == 0)
        {
            _376 = NOVALUE;
            goto L7; // [82] 92
        }
        else{
            _376 = NOVALUE;
        }

        /** 			return i*/
        DeRef(_needle_856);
        DeRefDS(_haystack_857);
        DeRef(_369);
        _369 = NOVALUE;
        return _i_873;
L7: 

        /** 	end for*/
        _i_873 = _i_873 + -1;
        goto L5; // [94] 72
L6: 
        ;
    }

    /** 	return 0*/
    DeRef(_needle_856);
    DeRefDS(_haystack_857);
    DeRef(_369);
    _369 = NOVALUE;
    return 0;
    ;
}


int _7find_replace(int _needle_879, int _haystack_880, int _replacement_881, int _max_882)
{
    int _posn_883 = NOVALUE;
    int _380 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer posn = 0*/
    _posn_883 = 0;

    /** 	while posn != 0 entry do */
    goto L1; // [12] 45
L2: 
    if (_posn_883 == 0)
    goto L3; // [15] 61

    /** 		haystack[posn] = replacement*/
    Ref(_replacement_881);
    _2 = (int)SEQ_PTR(_haystack_880);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _haystack_880 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _posn_883);
    _1 = *(int *)_2;
    *(int *)_2 = _replacement_881;
    DeRef(_1);

    /** 		max -= 1*/
    _max_882 = _max_882 - 1;

    /** 		if max = 0 then*/
    if (_max_882 != 0)
    goto L4; // [33] 42

    /** 			exit*/
    goto L3; // [39] 61
L4: 

    /** 	entry*/
L1: 

    /** 		posn = find(needle, haystack, posn + 1)*/
    _380 = _posn_883 + 1;
    if (_380 > MAXINT){
        _380 = NewDouble((double)_380);
    }
    _posn_883 = find_from(_needle_879, _haystack_880, _380);
    DeRef(_380);
    _380 = NOVALUE;

    /** 	end while*/
    goto L2; // [58] 15
L3: 

    /** 	return haystack*/
    DeRef(_needle_879);
    DeRefi(_replacement_881);
    return _haystack_880;
    ;
}


int _7match_replace(int _needle_893, int _haystack_894, int _replacement_895, int _max_896)
{
    int _posn_897 = NOVALUE;
    int _needle_len_898 = NOVALUE;
    int _replacement_len_899 = NOVALUE;
    int _scan_from_900 = NOVALUE;
    int _cnt_901 = NOVALUE;
    int _392 = NOVALUE;
    int _389 = NOVALUE;
    int _387 = NOVALUE;
    int _385 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if max < 0 then*/

    /** 	cnt = length(haystack)*/
    if (IS_SEQUENCE(_haystack_894)){
            _cnt_901 = SEQ_PTR(_haystack_894)->length;
    }
    else {
        _cnt_901 = 1;
    }

    /** 	if max != 0 then*/

    /** 	if atom(needle) then*/
    _385 = IS_ATOM(_needle_893);
    if (_385 == 0)
    {
        _385 = NOVALUE;
        goto L1; // [40] 50
    }
    else{
        _385 = NOVALUE;
    }

    /** 		needle = {needle}*/
    _0 = _needle_893;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_needle_893);
    *((int *)(_2+4)) = _needle_893;
    _needle_893 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	if atom(replacement) then*/
    _387 = IS_ATOM(_replacement_895);
    if (_387 == 0)
    {
        _387 = NOVALUE;
        goto L2; // [55] 65
    }
    else{
        _387 = NOVALUE;
    }

    /** 		replacement = {replacement}*/
    _0 = _replacement_895;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_replacement_895);
    *((int *)(_2+4)) = _replacement_895;
    _replacement_895 = MAKE_SEQ(_1);
    DeRef(_0);
L2: 

    /** 	needle_len = length(needle) - 1*/
    if (IS_SEQUENCE(_needle_893)){
            _389 = SEQ_PTR(_needle_893)->length;
    }
    else {
        _389 = 1;
    }
    _needle_len_898 = _389 - 1;
    _389 = NOVALUE;

    /** 	replacement_len = length(replacement)*/
    if (IS_SEQUENCE(_replacement_895)){
            _replacement_len_899 = SEQ_PTR(_replacement_895)->length;
    }
    else {
        _replacement_len_899 = 1;
    }

    /** 	scan_from = 1*/
    _scan_from_900 = 1;

    /** 	while posn with entry do*/
    goto L3; // [86] 132
L4: 
    if (_posn_897 == 0)
    {
        goto L5; // [91] 144
    }
    else{
    }

    /** 		haystack = replace(haystack, replacement, posn, posn + needle_len)*/
    _392 = _posn_897 + _needle_len_898;
    if ((long)((unsigned long)_392 + (unsigned long)HIGH_BITS) >= 0) 
    _392 = NewDouble((double)_392);
    {
        int p1 = _haystack_894;
        int p2 = _replacement_895;
        int p3 = _posn_897;
        int p4 = _392;
        struct replace_block replace_params;
        replace_params.copy_to   = &p1;
        replace_params.copy_from = &p2;
        replace_params.start     = &p3;
        replace_params.stop      = &p4;
        replace_params.target    = &_haystack_894;
        Replace( &replace_params );
    }
    DeRef(_392);
    _392 = NOVALUE;

    /** 		cnt -= 1*/
    _cnt_901 = _cnt_901 - 1;

    /** 		if cnt = 0 then*/
    if (_cnt_901 != 0)
    goto L6; // [114] 123

    /** 			exit*/
    goto L5; // [120] 144
L6: 

    /** 		scan_from = posn + replacement_len*/
    _scan_from_900 = _posn_897 + _replacement_len_899;

    /** 	entry*/
L3: 

    /** 		posn = match(needle, haystack, scan_from)*/
    _posn_897 = e_match_from(_needle_893, _haystack_894, _scan_from_900);

    /** 	end while*/
    goto L4; // [141] 89
L5: 

    /** 	return haystack*/
    DeRef(_needle_893);
    DeRef(_replacement_895);
    return _haystack_894;
    ;
}


int _7binary_search(int _needle_926, int _haystack_927, int _start_point_928, int _end_point_929)
{
    int _lo_930 = NOVALUE;
    int _hi_931 = NOVALUE;
    int _mid_932 = NOVALUE;
    int _c_933 = NOVALUE;
    int _418 = NOVALUE;
    int _410 = NOVALUE;
    int _408 = NOVALUE;
    int _405 = NOVALUE;
    int _404 = NOVALUE;
    int _403 = NOVALUE;
    int _402 = NOVALUE;
    int _399 = NOVALUE;
    int _0, _1, _2;
    

    /** 	lo = start_point*/
    _lo_930 = 1;

    /** 	if end_point <= 0 then*/
    if (_end_point_929 > 0)
    goto L1; // [14] 30

    /** 		hi = length(haystack) + end_point*/
    if (IS_SEQUENCE(_haystack_927)){
            _399 = SEQ_PTR(_haystack_927)->length;
    }
    else {
        _399 = 1;
    }
    _hi_931 = _399 + _end_point_929;
    _399 = NOVALUE;
    goto L2; // [27] 36
L1: 

    /** 		hi = end_point*/
    _hi_931 = _end_point_929;
L2: 

    /** 	if lo<1 then*/
    if (_lo_930 >= 1)
    goto L3; // [38] 48

    /** 		lo=1*/
    _lo_930 = 1;
L3: 

    /** 	if lo > hi and length(haystack) > 0 then*/
    _402 = (_lo_930 > _hi_931);
    if (_402 == 0) {
        goto L4; // [56] 77
    }
    if (IS_SEQUENCE(_haystack_927)){
            _404 = SEQ_PTR(_haystack_927)->length;
    }
    else {
        _404 = 1;
    }
    _405 = (_404 > 0);
    _404 = NOVALUE;
    if (_405 == 0)
    {
        DeRef(_405);
        _405 = NOVALUE;
        goto L4; // [68] 77
    }
    else{
        DeRef(_405);
        _405 = NOVALUE;
    }

    /** 		hi = length(haystack)*/
    if (IS_SEQUENCE(_haystack_927)){
            _hi_931 = SEQ_PTR(_haystack_927)->length;
    }
    else {
        _hi_931 = 1;
    }
L4: 

    /** 	mid = start_point*/
    _mid_932 = _start_point_928;

    /** 	c = 0*/
    _c_933 = 0;

    /** 	while lo <= hi do*/
L5: 
    if (_lo_930 > _hi_931)
    goto L6; // [92] 160

    /** 		mid = floor((lo + hi) / 2)*/
    _408 = _lo_930 + _hi_931;
    if ((long)((unsigned long)_408 + (unsigned long)HIGH_BITS) >= 0) 
    _408 = NewDouble((double)_408);
    if (IS_ATOM_INT(_408)) {
        _mid_932 = _408 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _408, 2);
        _mid_932 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    DeRef(_408);
    _408 = NOVALUE;
    if (!IS_ATOM_INT(_mid_932)) {
        _1 = (long)(DBL_PTR(_mid_932)->dbl);
        if (UNIQUE(DBL_PTR(_mid_932)) && (DBL_PTR(_mid_932)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mid_932);
        _mid_932 = _1;
    }

    /** 		c = eu:compare(needle, haystack[mid])*/
    _2 = (int)SEQ_PTR(_haystack_927);
    _410 = (int)*(((s1_ptr)_2)->base + _mid_932);
    if (IS_ATOM_INT(_needle_926) && IS_ATOM_INT(_410)){
        _c_933 = (_needle_926 < _410) ? -1 : (_needle_926 > _410);
    }
    else{
        _c_933 = compare(_needle_926, _410);
    }
    _410 = NOVALUE;

    /** 		if c < 0 then*/
    if (_c_933 >= 0)
    goto L7; // [120] 133

    /** 			hi = mid - 1*/
    _hi_931 = _mid_932 - 1;
    goto L5; // [130] 92
L7: 

    /** 		elsif c > 0 then*/
    if (_c_933 <= 0)
    goto L8; // [135] 148

    /** 			lo = mid + 1*/
    _lo_930 = _mid_932 + 1;
    goto L5; // [145] 92
L8: 

    /** 			return mid*/
    DeRefDS(_haystack_927);
    DeRef(_402);
    _402 = NOVALUE;
    return _mid_932;

    /** 	end while*/
    goto L5; // [157] 92
L6: 

    /** 	if c > 0 then*/
    if (_c_933 <= 0)
    goto L9; // [162] 173

    /** 		mid += 1*/
    _mid_932 = _mid_932 + 1;
L9: 

    /** 	return -mid*/
    if ((unsigned long)_mid_932 == 0xC0000000)
    _418 = (int)NewDouble((double)-0xC0000000);
    else
    _418 = - _mid_932;
    DeRefDS(_haystack_927);
    DeRef(_402);
    _402 = NOVALUE;
    return _418;
    ;
}


int _7begins(int _sub_text_1014, int _full_text_1015)
{
    int _456 = NOVALUE;
    int _455 = NOVALUE;
    int _454 = NOVALUE;
    int _452 = NOVALUE;
    int _451 = NOVALUE;
    int _450 = NOVALUE;
    int _449 = NOVALUE;
    int _448 = NOVALUE;
    int _446 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(full_text) = 0 then*/
    if (IS_SEQUENCE(_full_text_1015)){
            _446 = SEQ_PTR(_full_text_1015)->length;
    }
    else {
        _446 = 1;
    }
    if (_446 != 0)
    goto L1; // [8] 19

    /** 		return 0*/
    DeRef(_sub_text_1014);
    DeRefDS(_full_text_1015);
    return 0;
L1: 

    /** 	if atom(sub_text) then*/
    _448 = IS_ATOM(_sub_text_1014);
    if (_448 == 0)
    {
        _448 = NOVALUE;
        goto L2; // [24] 57
    }
    else{
        _448 = NOVALUE;
    }

    /** 		if equal(sub_text, full_text[1]) then*/
    _2 = (int)SEQ_PTR(_full_text_1015);
    _449 = (int)*(((s1_ptr)_2)->base + 1);
    if (_sub_text_1014 == _449)
    _450 = 1;
    else if (IS_ATOM_INT(_sub_text_1014) && IS_ATOM_INT(_449))
    _450 = 0;
    else
    _450 = (compare(_sub_text_1014, _449) == 0);
    _449 = NOVALUE;
    if (_450 == 0)
    {
        _450 = NOVALUE;
        goto L3; // [37] 49
    }
    else{
        _450 = NOVALUE;
    }

    /** 			return 1*/
    DeRef(_sub_text_1014);
    DeRefDS(_full_text_1015);
    return 1;
    goto L4; // [46] 56
L3: 

    /** 			return 0*/
    DeRef(_sub_text_1014);
    DeRefDS(_full_text_1015);
    return 0;
L4: 
L2: 

    /** 	if length(sub_text) > length(full_text) then*/
    if (IS_SEQUENCE(_sub_text_1014)){
            _451 = SEQ_PTR(_sub_text_1014)->length;
    }
    else {
        _451 = 1;
    }
    if (IS_SEQUENCE(_full_text_1015)){
            _452 = SEQ_PTR(_full_text_1015)->length;
    }
    else {
        _452 = 1;
    }
    if (_451 <= _452)
    goto L5; // [65] 76

    /** 		return 0*/
    DeRef(_sub_text_1014);
    DeRefDS(_full_text_1015);
    return 0;
L5: 

    /** 	if equal(sub_text, full_text[1.. length(sub_text)]) then*/
    if (IS_SEQUENCE(_sub_text_1014)){
            _454 = SEQ_PTR(_sub_text_1014)->length;
    }
    else {
        _454 = 1;
    }
    rhs_slice_target = (object_ptr)&_455;
    RHS_Slice(_full_text_1015, 1, _454);
    if (_sub_text_1014 == _455)
    _456 = 1;
    else if (IS_ATOM_INT(_sub_text_1014) && IS_ATOM_INT(_455))
    _456 = 0;
    else
    _456 = (compare(_sub_text_1014, _455) == 0);
    DeRefDS(_455);
    _455 = NOVALUE;
    if (_456 == 0)
    {
        _456 = NOVALUE;
        goto L6; // [90] 102
    }
    else{
        _456 = NOVALUE;
    }

    /** 		return 1*/
    DeRef(_sub_text_1014);
    DeRefDS(_full_text_1015);
    return 1;
    goto L7; // [99] 109
L6: 

    /** 		return 0*/
    DeRef(_sub_text_1014);
    DeRefDS(_full_text_1015);
    return 0;
L7: 
    ;
}


int _7ends(int _sub_text_1036, int _full_text_1037)
{
    int _472 = NOVALUE;
    int _471 = NOVALUE;
    int _470 = NOVALUE;
    int _469 = NOVALUE;
    int _468 = NOVALUE;
    int _467 = NOVALUE;
    int _466 = NOVALUE;
    int _464 = NOVALUE;
    int _463 = NOVALUE;
    int _462 = NOVALUE;
    int _461 = NOVALUE;
    int _460 = NOVALUE;
    int _459 = NOVALUE;
    int _457 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(full_text) = 0 then*/
    if (IS_SEQUENCE(_full_text_1037)){
            _457 = SEQ_PTR(_full_text_1037)->length;
    }
    else {
        _457 = 1;
    }
    if (_457 != 0)
    goto L1; // [8] 19

    /** 		return 0*/
    DeRef(_sub_text_1036);
    DeRefDS(_full_text_1037);
    return 0;
L1: 

    /** 	if atom(sub_text) then*/
    _459 = IS_ATOM(_sub_text_1036);
    if (_459 == 0)
    {
        _459 = NOVALUE;
        goto L2; // [24] 60
    }
    else{
        _459 = NOVALUE;
    }

    /** 		if equal(sub_text, full_text[$]) then*/
    if (IS_SEQUENCE(_full_text_1037)){
            _460 = SEQ_PTR(_full_text_1037)->length;
    }
    else {
        _460 = 1;
    }
    _2 = (int)SEQ_PTR(_full_text_1037);
    _461 = (int)*(((s1_ptr)_2)->base + _460);
    if (_sub_text_1036 == _461)
    _462 = 1;
    else if (IS_ATOM_INT(_sub_text_1036) && IS_ATOM_INT(_461))
    _462 = 0;
    else
    _462 = (compare(_sub_text_1036, _461) == 0);
    _461 = NOVALUE;
    if (_462 == 0)
    {
        _462 = NOVALUE;
        goto L3; // [40] 52
    }
    else{
        _462 = NOVALUE;
    }

    /** 			return 1*/
    DeRef(_sub_text_1036);
    DeRefDS(_full_text_1037);
    return 1;
    goto L4; // [49] 59
L3: 

    /** 			return 0*/
    DeRef(_sub_text_1036);
    DeRefDS(_full_text_1037);
    return 0;
L4: 
L2: 

    /** 	if length(sub_text) > length(full_text) then*/
    if (IS_SEQUENCE(_sub_text_1036)){
            _463 = SEQ_PTR(_sub_text_1036)->length;
    }
    else {
        _463 = 1;
    }
    if (IS_SEQUENCE(_full_text_1037)){
            _464 = SEQ_PTR(_full_text_1037)->length;
    }
    else {
        _464 = 1;
    }
    if (_463 <= _464)
    goto L5; // [68] 79

    /** 		return 0*/
    DeRef(_sub_text_1036);
    DeRefDS(_full_text_1037);
    return 0;
L5: 

    /** 	if equal(sub_text, full_text[$ - length(sub_text) + 1 .. $]) then*/
    if (IS_SEQUENCE(_full_text_1037)){
            _466 = SEQ_PTR(_full_text_1037)->length;
    }
    else {
        _466 = 1;
    }
    if (IS_SEQUENCE(_sub_text_1036)){
            _467 = SEQ_PTR(_sub_text_1036)->length;
    }
    else {
        _467 = 1;
    }
    _468 = _466 - _467;
    _466 = NOVALUE;
    _467 = NOVALUE;
    _469 = _468 + 1;
    _468 = NOVALUE;
    if (IS_SEQUENCE(_full_text_1037)){
            _470 = SEQ_PTR(_full_text_1037)->length;
    }
    else {
        _470 = 1;
    }
    rhs_slice_target = (object_ptr)&_471;
    RHS_Slice(_full_text_1037, _469, _470);
    if (_sub_text_1036 == _471)
    _472 = 1;
    else if (IS_ATOM_INT(_sub_text_1036) && IS_ATOM_INT(_471))
    _472 = 0;
    else
    _472 = (compare(_sub_text_1036, _471) == 0);
    DeRefDS(_471);
    _471 = NOVALUE;
    if (_472 == 0)
    {
        _472 = NOVALUE;
        goto L6; // [107] 119
    }
    else{
        _472 = NOVALUE;
    }

    /** 		return 1*/
    DeRef(_sub_text_1036);
    DeRefDS(_full_text_1037);
    _469 = NOVALUE;
    return 1;
    goto L7; // [116] 126
L6: 

    /** 		return 0*/
    DeRef(_sub_text_1036);
    DeRefDS(_full_text_1037);
    DeRef(_469);
    _469 = NOVALUE;
    return 0;
L7: 
    ;
}



// 0xE4A11F83
