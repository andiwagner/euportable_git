// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _29clear_fwd_refs()
{
    int _0, _1, _2;
    

    /** 	fwdref_count = 0*/
    _29fwdref_count_62576 = 0;

    /** end procedure*/
    return;
    ;
}


int _29get_fwdref_count()
{
    int _0, _1, _2;
    

    /** 	return fwdref_count*/
    return _29fwdref_count_62576;
    ;
}


void _29set_glabel_block(int _ref_62583, int _block_62585)
{
    int _31526 = NOVALUE;
    int _31525 = NOVALUE;
    int _31523 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_ref_62583)) {
        _1 = (long)(DBL_PTR(_ref_62583)->dbl);
        if (UNIQUE(DBL_PTR(_ref_62583)) && (DBL_PTR(_ref_62583)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_62583);
        _ref_62583 = _1;
    }
    if (!IS_ATOM_INT(_block_62585)) {
        _1 = (long)(DBL_PTR(_block_62585)->dbl);
        if (UNIQUE(DBL_PTR(_block_62585)) && (DBL_PTR(_block_62585)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_block_62585);
        _block_62585 = _1;
    }

    /** 	forward_references[ref][FR_DATA] &= block*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_62583 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _31525 = (int)*(((s1_ptr)_2)->base + 12);
    _31523 = NOVALUE;
    if (IS_SEQUENCE(_31525) && IS_ATOM(_block_62585)) {
        Append(&_31526, _31525, _block_62585);
    }
    else if (IS_ATOM(_31525) && IS_SEQUENCE(_block_62585)) {
    }
    else {
        Concat((object_ptr)&_31526, _31525, _block_62585);
        _31525 = NOVALUE;
    }
    _31525 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = _31526;
    if( _1 != _31526 ){
        DeRef(_1);
    }
    _31526 = NOVALUE;
    _31523 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


void _29replace_code(int _code_62597, int _start_62598, int _finish_62599, int _subprog_62600)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_finish_62599)) {
        _1 = (long)(DBL_PTR(_finish_62599)->dbl);
        if (UNIQUE(DBL_PTR(_finish_62599)) && (DBL_PTR(_finish_62599)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_finish_62599);
        _finish_62599 = _1;
    }
    if (!IS_ATOM_INT(_subprog_62600)) {
        _1 = (long)(DBL_PTR(_subprog_62600)->dbl);
        if (UNIQUE(DBL_PTR(_subprog_62600)) && (DBL_PTR(_subprog_62600)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subprog_62600);
        _subprog_62600 = _1;
    }

    /** 	shifting_sub = subprog*/
    _29shifting_sub_62575 = _subprog_62600;

    /** 	shift:replace_code( code, start, finish )*/
    RefDS(_code_62597);
    _65replace_code(_code_62597, _start_62598, _finish_62599);

    /** 	shifting_sub = 0*/
    _29shifting_sub_62575 = 0;

    /** end procedure*/
    DeRefDS(_code_62597);
    return;
    ;
}


void _29resolved_reference(int _ref_62603)
{
    int _file_62604 = NOVALUE;
    int _subprog_62607 = NOVALUE;
    int _tx_62610 = NOVALUE;
    int _ax_62611 = NOVALUE;
    int _sp_62612 = NOVALUE;
    int _r_62627 = NOVALUE;
    int _r_62645 = NOVALUE;
    int _31550 = NOVALUE;
    int _31549 = NOVALUE;
    int _31548 = NOVALUE;
    int _31546 = NOVALUE;
    int _31543 = NOVALUE;
    int _31541 = NOVALUE;
    int _31539 = NOVALUE;
    int _31538 = NOVALUE;
    int _31536 = NOVALUE;
    int _31534 = NOVALUE;
    int _31532 = NOVALUE;
    int _31531 = NOVALUE;
    int _31529 = NOVALUE;
    int _31527 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 		file    = forward_references[ref][FR_FILE],*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    _31527 = (int)*(((s1_ptr)_2)->base + _ref_62603);
    _2 = (int)SEQ_PTR(_31527);
    _file_62604 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_file_62604)){
        _file_62604 = (long)DBL_PTR(_file_62604)->dbl;
    }
    _31527 = NOVALUE;

    /** 		subprog = forward_references[ref][FR_SUBPROG]*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    _31529 = (int)*(((s1_ptr)_2)->base + _ref_62603);
    _2 = (int)SEQ_PTR(_31529);
    _subprog_62607 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_subprog_62607)){
        _subprog_62607 = (long)DBL_PTR(_subprog_62607)->dbl;
    }
    _31529 = NOVALUE;

    /** 		tx = 0,*/
    _tx_62610 = 0;

    /** 		ax = 0,*/
    _ax_62611 = 0;

    /** 		sp = 0*/
    _sp_62612 = 0;

    /** 	if forward_references[ref][FR_SUBPROG] = TopLevelSub then*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    _31531 = (int)*(((s1_ptr)_2)->base + _ref_62603);
    _2 = (int)SEQ_PTR(_31531);
    _31532 = (int)*(((s1_ptr)_2)->base + 4);
    _31531 = NOVALUE;
    if (binary_op_a(NOTEQ, _31532, _25TopLevelSub_12269)){
        _31532 = NOVALUE;
        goto L1; // [66] 86
    }
    _31532 = NOVALUE;

    /** 		tx = find( ref, toplevel_references[file] )*/
    _2 = (int)SEQ_PTR(_29toplevel_references_62548);
    _31534 = (int)*(((s1_ptr)_2)->base + _file_62604);
    _tx_62610 = find_from(_ref_62603, _31534, 1);
    _31534 = NOVALUE;
    goto L2; // [83] 117
L1: 

    /** 		sp = find( subprog, active_subprogs[file] )*/
    _2 = (int)SEQ_PTR(_29active_subprogs_62546);
    _31536 = (int)*(((s1_ptr)_2)->base + _file_62604);
    _sp_62612 = find_from(_subprog_62607, _31536, 1);
    _31536 = NOVALUE;

    /** 		ax = find( ref, active_references[file][sp] )*/
    _2 = (int)SEQ_PTR(_29active_references_62547);
    _31538 = (int)*(((s1_ptr)_2)->base + _file_62604);
    _2 = (int)SEQ_PTR(_31538);
    _31539 = (int)*(((s1_ptr)_2)->base + _sp_62612);
    _31538 = NOVALUE;
    _ax_62611 = find_from(_ref_62603, _31539, 1);
    _31539 = NOVALUE;
L2: 

    /** 	if ax then*/
    if (_ax_62611 == 0)
    {
        goto L3; // [119] 259
    }
    else{
    }

    /** 		sequence r = active_references[file][sp] */
    _2 = (int)SEQ_PTR(_29active_references_62547);
    _31541 = (int)*(((s1_ptr)_2)->base + _file_62604);
    DeRef(_r_62627);
    _2 = (int)SEQ_PTR(_31541);
    _r_62627 = (int)*(((s1_ptr)_2)->base + _sp_62612);
    Ref(_r_62627);
    _31541 = NOVALUE;

    /** 		active_references[file][sp] = 0*/
    _2 = (int)SEQ_PTR(_29active_references_62547);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29active_references_62547 = MAKE_SEQ(_2);
    }
    _3 = (int)(_file_62604 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _sp_62612);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _31543 = NOVALUE;

    /** 		r = remove( r, ax )*/
    {
        s1_ptr assign_space = SEQ_PTR(_r_62627);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_ax_62611)) ? _ax_62611 : (long)(DBL_PTR(_ax_62611)->dbl);
        int stop = (IS_ATOM_INT(_ax_62611)) ? _ax_62611 : (long)(DBL_PTR(_ax_62611)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_r_62627), start, &_r_62627 );
            }
            else Tail(SEQ_PTR(_r_62627), stop+1, &_r_62627);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_r_62627), start, &_r_62627);
        }
        else {
            assign_slice_seq = &assign_space;
            _r_62627 = Remove_elements(start, stop, (SEQ_PTR(_r_62627)->ref == 1));
        }
    }

    /** 		active_references[file][sp] = r*/
    _2 = (int)SEQ_PTR(_29active_references_62547);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29active_references_62547 = MAKE_SEQ(_2);
    }
    _3 = (int)(_file_62604 + ((s1_ptr)_2)->base);
    RefDS(_r_62627);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _sp_62612);
    _1 = *(int *)_2;
    *(int *)_2 = _r_62627;
    DeRef(_1);
    _31546 = NOVALUE;

    /** 		if not length( active_references[file][sp] ) then*/
    _2 = (int)SEQ_PTR(_29active_references_62547);
    _31548 = (int)*(((s1_ptr)_2)->base + _file_62604);
    _2 = (int)SEQ_PTR(_31548);
    _31549 = (int)*(((s1_ptr)_2)->base + _sp_62612);
    _31548 = NOVALUE;
    if (IS_SEQUENCE(_31549)){
            _31550 = SEQ_PTR(_31549)->length;
    }
    else {
        _31550 = 1;
    }
    _31549 = NOVALUE;
    if (_31550 != 0)
    goto L4; // [184] 254
    _31550 = NOVALUE;

    /** 			r = active_references[file]*/
    DeRefDS(_r_62627);
    _2 = (int)SEQ_PTR(_29active_references_62547);
    _r_62627 = (int)*(((s1_ptr)_2)->base + _file_62604);
    Ref(_r_62627);

    /** 			active_references[file] = 0*/
    _2 = (int)SEQ_PTR(_29active_references_62547);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29active_references_62547 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _file_62604);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 			r = remove( r, sp )*/
    {
        s1_ptr assign_space = SEQ_PTR(_r_62627);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_sp_62612)) ? _sp_62612 : (long)(DBL_PTR(_sp_62612)->dbl);
        int stop = (IS_ATOM_INT(_sp_62612)) ? _sp_62612 : (long)(DBL_PTR(_sp_62612)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_r_62627), start, &_r_62627 );
            }
            else Tail(SEQ_PTR(_r_62627), stop+1, &_r_62627);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_r_62627), start, &_r_62627);
        }
        else {
            assign_slice_seq = &assign_space;
            _r_62627 = Remove_elements(start, stop, (SEQ_PTR(_r_62627)->ref == 1));
        }
    }

    /** 			active_references[file] = r*/
    RefDS(_r_62627);
    _2 = (int)SEQ_PTR(_29active_references_62547);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29active_references_62547 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _file_62604);
    _1 = *(int *)_2;
    *(int *)_2 = _r_62627;
    DeRef(_1);

    /** 			r = active_subprogs[file]*/
    DeRefDS(_r_62627);
    _2 = (int)SEQ_PTR(_29active_subprogs_62546);
    _r_62627 = (int)*(((s1_ptr)_2)->base + _file_62604);
    Ref(_r_62627);

    /** 			active_subprogs[file] = 0*/
    _2 = (int)SEQ_PTR(_29active_subprogs_62546);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29active_subprogs_62546 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _file_62604);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 			r = remove( r,   sp )*/
    {
        s1_ptr assign_space = SEQ_PTR(_r_62627);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_sp_62612)) ? _sp_62612 : (long)(DBL_PTR(_sp_62612)->dbl);
        int stop = (IS_ATOM_INT(_sp_62612)) ? _sp_62612 : (long)(DBL_PTR(_sp_62612)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_r_62627), start, &_r_62627 );
            }
            else Tail(SEQ_PTR(_r_62627), stop+1, &_r_62627);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_r_62627), start, &_r_62627);
        }
        else {
            assign_slice_seq = &assign_space;
            _r_62627 = Remove_elements(start, stop, (SEQ_PTR(_r_62627)->ref == 1));
        }
    }

    /** 			active_subprogs[file] = r*/
    RefDS(_r_62627);
    _2 = (int)SEQ_PTR(_29active_subprogs_62546);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29active_subprogs_62546 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _file_62604);
    _1 = *(int *)_2;
    *(int *)_2 = _r_62627;
    DeRef(_1);
L4: 
    DeRef(_r_62627);
    _r_62627 = NOVALUE;
    goto L5; // [256] 309
L3: 

    /** 	elsif tx then*/
    if (_tx_62610 == 0)
    {
        goto L6; // [261] 302
    }
    else{
    }

    /** 		sequence r = toplevel_references[file]*/
    DeRef(_r_62645);
    _2 = (int)SEQ_PTR(_29toplevel_references_62548);
    _r_62645 = (int)*(((s1_ptr)_2)->base + _file_62604);
    Ref(_r_62645);

    /** 		toplevel_references[file] = 0*/
    _2 = (int)SEQ_PTR(_29toplevel_references_62548);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29toplevel_references_62548 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _file_62604);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		r = remove( r, tx )*/
    {
        s1_ptr assign_space = SEQ_PTR(_r_62645);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_tx_62610)) ? _tx_62610 : (long)(DBL_PTR(_tx_62610)->dbl);
        int stop = (IS_ATOM_INT(_tx_62610)) ? _tx_62610 : (long)(DBL_PTR(_tx_62610)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_r_62645), start, &_r_62645 );
            }
            else Tail(SEQ_PTR(_r_62645), stop+1, &_r_62645);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_r_62645), start, &_r_62645);
        }
        else {
            assign_slice_seq = &assign_space;
            _r_62645 = Remove_elements(start, stop, (SEQ_PTR(_r_62645)->ref == 1));
        }
    }

    /** 		toplevel_references[file] = r*/
    RefDS(_r_62645);
    _2 = (int)SEQ_PTR(_29toplevel_references_62548);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29toplevel_references_62548 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _file_62604);
    _1 = *(int *)_2;
    *(int *)_2 = _r_62645;
    DeRef(_1);
    DeRefDS(_r_62645);
    _r_62645 = NOVALUE;
    goto L5; // [299] 309
L6: 

    /** 		InternalErr( 260 )*/
    RefDS(_22682);
    _43InternalErr(260, _22682);
L5: 

    /** 	inactive_references &= ref*/
    Append(&_29inactive_references_62549, _29inactive_references_62549, _ref_62603);

    /** 	forward_references[ref] = 0*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _ref_62603);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** end procedure*/
    _31549 = NOVALUE;
    return;
    ;
}


void _29set_code(int _ref_62659)
{
    int _31566 = NOVALUE;
    int _31564 = NOVALUE;
    int _31562 = NOVALUE;
    int _31559 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	patch_code_sub = forward_references[ref][FR_SUBPROG]*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    _31559 = (int)*(((s1_ptr)_2)->base + _ref_62659);
    _2 = (int)SEQ_PTR(_31559);
    _29patch_code_sub_62654 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_29patch_code_sub_62654)){
        _29patch_code_sub_62654 = (long)DBL_PTR(_29patch_code_sub_62654)->dbl;
    }
    _31559 = NOVALUE;

    /** 	if patch_code_sub != CurrentSub then*/
    if (_29patch_code_sub_62654 == _25CurrentSub_12270)
    goto L1; // [25] 121

    /** 		patch_code_temp = Code*/
    RefDS(_25Code_12355);
    DeRef(_29patch_code_temp_62651);
    _29patch_code_temp_62651 = _25Code_12355;

    /** 		patch_linetab_temp = LineTable*/
    RefDS(_25LineTable_12356);
    DeRef(_29patch_linetab_temp_62652);
    _29patch_linetab_temp_62652 = _25LineTable_12356;

    /** 		Code = SymTab[patch_code_sub][S_CODE]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31562 = (int)*(((s1_ptr)_2)->base + _29patch_code_sub_62654);
    DeRefDS(_25Code_12355);
    _2 = (int)SEQ_PTR(_31562);
    if (!IS_ATOM_INT(_25S_CODE_11925)){
        _25Code_12355 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    }
    else{
        _25Code_12355 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
    }
    Ref(_25Code_12355);
    _31562 = NOVALUE;

    /** 		SymTab[patch_code_sub][S_CODE] = 0*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_29patch_code_sub_62654 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_CODE_11925))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_CODE_11925);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _31564 = NOVALUE;

    /** 		LineTable = SymTab[patch_code_sub][S_LINETAB]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31566 = (int)*(((s1_ptr)_2)->base + _29patch_code_sub_62654);
    DeRefDS(_25LineTable_12356);
    _2 = (int)SEQ_PTR(_31566);
    if (!IS_ATOM_INT(_25S_LINETAB_11948)){
        _25LineTable_12356 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_LINETAB_11948)->dbl));
    }
    else{
        _25LineTable_12356 = (int)*(((s1_ptr)_2)->base + _25S_LINETAB_11948);
    }
    Ref(_25LineTable_12356);
    _31566 = NOVALUE;

    /** 		patch_current_sub = CurrentSub*/
    _29patch_current_sub_62656 = _25CurrentSub_12270;

    /** 		CurrentSub = patch_code_sub*/
    _25CurrentSub_12270 = _29patch_code_sub_62654;
    goto L2; // [118] 131
L1: 

    /** 		patch_current_sub = patch_code_sub*/
    _29patch_current_sub_62656 = _29patch_code_sub_62654;
L2: 

    /** end procedure*/
    return;
    ;
}


void _29reset_code()
{
    int _31570 = NOVALUE;
    int _31568 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	SymTab[patch_code_sub][S_CODE] = Code*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_29patch_code_sub_62654 + ((s1_ptr)_2)->base);
    RefDS(_25Code_12355);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_CODE_11925))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_CODE_11925);
    _1 = *(int *)_2;
    *(int *)_2 = _25Code_12355;
    DeRef(_1);
    _31568 = NOVALUE;

    /** 	SymTab[patch_code_sub][S_LINETAB] = LineTable*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_29patch_code_sub_62654 + ((s1_ptr)_2)->base);
    RefDS(_25LineTable_12356);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_LINETAB_11948))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_LINETAB_11948)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_LINETAB_11948);
    _1 = *(int *)_2;
    *(int *)_2 = _25LineTable_12356;
    DeRef(_1);
    _31570 = NOVALUE;

    /** 	if patch_code_sub != patch_current_sub then*/
    if (_29patch_code_sub_62654 == _29patch_current_sub_62656)
    goto L1; // [45] 77

    /** 		CurrentSub = patch_current_sub*/
    _25CurrentSub_12270 = _29patch_current_sub_62656;

    /** 		Code = patch_code_temp*/
    RefDS(_29patch_code_temp_62651);
    DeRefDS(_25Code_12355);
    _25Code_12355 = _29patch_code_temp_62651;

    /** 		LineTable = patch_linetab_temp*/
    RefDS(_29patch_linetab_temp_62652);
    DeRefDS(_25LineTable_12356);
    _25LineTable_12356 = _29patch_linetab_temp_62652;
L1: 

    /** 	patch_code_temp = {}*/
    RefDS(_22682);
    DeRef(_29patch_code_temp_62651);
    _29patch_code_temp_62651 = _22682;

    /** 	patch_linetab_temp = {}*/
    RefDS(_22682);
    DeRef(_29patch_linetab_temp_62652);
    _29patch_linetab_temp_62652 = _22682;

    /** end procedure*/
    return;
    ;
}


void _29set_data(int _ref_62703, int _data_62704)
{
    int _31573 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_ref_62703)) {
        _1 = (long)(DBL_PTR(_ref_62703)->dbl);
        if (UNIQUE(DBL_PTR(_ref_62703)) && (DBL_PTR(_ref_62703)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_62703);
        _ref_62703 = _1;
    }

    /** 	forward_references[ref][FR_DATA] = data*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_62703 + ((s1_ptr)_2)->base);
    Ref(_data_62704);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = _data_62704;
    DeRef(_1);
    _31573 = NOVALUE;

    /** end procedure*/
    DeRef(_data_62704);
    return;
    ;
}


void _29add_data(int _ref_62709, int _data_62710)
{
    int _31579 = NOVALUE;
    int _31578 = NOVALUE;
    int _31577 = NOVALUE;
    int _31575 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_ref_62709)) {
        _1 = (long)(DBL_PTR(_ref_62709)->dbl);
        if (UNIQUE(DBL_PTR(_ref_62709)) && (DBL_PTR(_ref_62709)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_62709);
        _ref_62709 = _1;
    }

    /** 	forward_references[ref][FR_DATA] = append( forward_references[ref][FR_DATA], data )*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_62709 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    _31577 = (int)*(((s1_ptr)_2)->base + _ref_62709);
    _2 = (int)SEQ_PTR(_31577);
    _31578 = (int)*(((s1_ptr)_2)->base + 12);
    _31577 = NOVALUE;
    Ref(_data_62710);
    Append(&_31579, _31578, _data_62710);
    _31578 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = _31579;
    if( _1 != _31579 ){
        DeRef(_1);
    }
    _31579 = NOVALUE;
    _31575 = NOVALUE;

    /** end procedure*/
    DeRef(_data_62710);
    return;
    ;
}


void _29set_line(int _ref_62718, int _line_no_62719, int _this_line_62720, int _bp_62721)
{
    int _31584 = NOVALUE;
    int _31582 = NOVALUE;
    int _31580 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_ref_62718)) {
        _1 = (long)(DBL_PTR(_ref_62718)->dbl);
        if (UNIQUE(DBL_PTR(_ref_62718)) && (DBL_PTR(_ref_62718)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_62718);
        _ref_62718 = _1;
    }
    if (!IS_ATOM_INT(_line_no_62719)) {
        _1 = (long)(DBL_PTR(_line_no_62719)->dbl);
        if (UNIQUE(DBL_PTR(_line_no_62719)) && (DBL_PTR(_line_no_62719)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_line_no_62719);
        _line_no_62719 = _1;
    }
    if (!IS_ATOM_INT(_bp_62721)) {
        _1 = (long)(DBL_PTR(_bp_62721)->dbl);
        if (UNIQUE(DBL_PTR(_bp_62721)) && (DBL_PTR(_bp_62721)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bp_62721);
        _bp_62721 = _1;
    }

    /** 	forward_references[ref][FR_LINE] = line_no*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_62718 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _line_no_62719;
    DeRef(_1);
    _31580 = NOVALUE;

    /** 	forward_references[ref][FR_THISLINE] = this_line*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_62718 + ((s1_ptr)_2)->base);
    RefDS(_this_line_62720);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 7);
    _1 = *(int *)_2;
    *(int *)_2 = _this_line_62720;
    DeRef(_1);
    _31582 = NOVALUE;

    /** 	forward_references[ref][FR_BP] = bp*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_62718 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 8);
    _1 = *(int *)_2;
    *(int *)_2 = _bp_62721;
    DeRef(_1);
    _31584 = NOVALUE;

    /** end procedure*/
    DeRefDS(_this_line_62720);
    return;
    ;
}


void _29add_private_symbol(int _sym_62733, int _name_62734)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_62733)) {
        _1 = (long)(DBL_PTR(_sym_62733)->dbl);
        if (UNIQUE(DBL_PTR(_sym_62733)) && (DBL_PTR(_sym_62733)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_62733);
        _sym_62733 = _1;
    }

    /** 	fwd_private_sym &= sym*/
    Append(&_29fwd_private_sym_62728, _29fwd_private_sym_62728, _sym_62733);

    /** 	fwd_private_name = append( fwd_private_name, name )*/
    RefDS(_name_62734);
    Append(&_29fwd_private_name_62729, _29fwd_private_name_62729, _name_62734);

    /** end procedure*/
    DeRefDS(_name_62734);
    return;
    ;
}


void _29patch_forward_goto(int _tok_62742, int _ref_62743)
{
    int _fr_62744 = NOVALUE;
    int _31600 = NOVALUE;
    int _31599 = NOVALUE;
    int _31598 = NOVALUE;
    int _31597 = NOVALUE;
    int _31596 = NOVALUE;
    int _31595 = NOVALUE;
    int _31594 = NOVALUE;
    int _31593 = NOVALUE;
    int _31591 = NOVALUE;
    int _31590 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence fr = forward_references[ref]*/
    DeRef(_fr_62744);
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    _fr_62744 = (int)*(((s1_ptr)_2)->base + _ref_62743);
    Ref(_fr_62744);

    /** 	set_code( ref )*/
    _29set_code(_ref_62743);

    /** 	shifting_sub = fr[FR_SUBPROG]*/
    _2 = (int)SEQ_PTR(_fr_62744);
    _29shifting_sub_62575 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_29shifting_sub_62575))
    _29shifting_sub_62575 = (long)DBL_PTR(_29shifting_sub_62575)->dbl;

    /** 	if length( fr[FR_DATA] ) = 2 then*/
    _2 = (int)SEQ_PTR(_fr_62744);
    _31590 = (int)*(((s1_ptr)_2)->base + 12);
    if (IS_SEQUENCE(_31590)){
            _31591 = SEQ_PTR(_31590)->length;
    }
    else {
        _31591 = 1;
    }
    _31590 = NOVALUE;
    if (_31591 != 2)
    goto L1; // [37] 68

    /** 		prep_forward_error( ref )*/
    _29prep_forward_error(_ref_62743);

    /** 		CompileErr( 156, { fr[FR_DATA][2] })*/
    _2 = (int)SEQ_PTR(_fr_62744);
    _31593 = (int)*(((s1_ptr)_2)->base + 12);
    _2 = (int)SEQ_PTR(_31593);
    _31594 = (int)*(((s1_ptr)_2)->base + 2);
    _31593 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_31594);
    *((int *)(_2+4)) = _31594;
    _31595 = MAKE_SEQ(_1);
    _31594 = NOVALUE;
    _43CompileErr(156, _31595, 0);
    _31595 = NOVALUE;
L1: 

    /** 	Goto_block(  fr[FR_DATA][1], fr[FR_DATA][3], fr[FR_PC] )*/
    _2 = (int)SEQ_PTR(_fr_62744);
    _31596 = (int)*(((s1_ptr)_2)->base + 12);
    _2 = (int)SEQ_PTR(_31596);
    _31597 = (int)*(((s1_ptr)_2)->base + 1);
    _31596 = NOVALUE;
    _2 = (int)SEQ_PTR(_fr_62744);
    _31598 = (int)*(((s1_ptr)_2)->base + 12);
    _2 = (int)SEQ_PTR(_31598);
    _31599 = (int)*(((s1_ptr)_2)->base + 3);
    _31598 = NOVALUE;
    _2 = (int)SEQ_PTR(_fr_62744);
    _31600 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_31597);
    Ref(_31599);
    Ref(_31600);
    _66Goto_block(_31597, _31599, _31600);
    _31597 = NOVALUE;
    _31599 = NOVALUE;
    _31600 = NOVALUE;

    /** 	shifting_sub = 0*/
    _29shifting_sub_62575 = 0;

    /** 	reset_code()*/
    _29reset_code();

    /** 	resolved_reference( ref )*/
    _29resolved_reference(_ref_62743);

    /** end procedure*/
    DeRefDS(_fr_62744);
    _31590 = NOVALUE;
    return;
    ;
}


void _29patch_forward_call(int _tok_62765, int _ref_62766)
{
    int _fr_62767 = NOVALUE;
    int _sub_62770 = NOVALUE;
    int _defarg_62776 = NOVALUE;
    int _paramsym_62780 = NOVALUE;
    int _old_62783 = NOVALUE;
    int _tx_62787 = NOVALUE;
    int _code_sub_62797 = NOVALUE;
    int _args_62799 = NOVALUE;
    int _is_func_62804 = NOVALUE;
    int _real_file_62818 = NOVALUE;
    int _code_62822 = NOVALUE;
    int _temp_sub_62824 = NOVALUE;
    int _pc_62826 = NOVALUE;
    int _next_pc_62828 = NOVALUE;
    int _supplied_args_62829 = NOVALUE;
    int _name_62832 = NOVALUE;
    int _old_temps_allocated_62856 = NOVALUE;
    int _temp_target_62865 = NOVALUE;
    int _converted_code_62868 = NOVALUE;
    int _target_62884 = NOVALUE;
    int _has_defaults_62890 = NOVALUE;
    int _goto_target_62891 = NOVALUE;
    int _defarg_62894 = NOVALUE;
    int _code_len_62895 = NOVALUE;
    int _extra_default_args_62897 = NOVALUE;
    int _param_sym_62900 = NOVALUE;
    int _params_62901 = NOVALUE;
    int _orig_code_62903 = NOVALUE;
    int _orig_linetable_62904 = NOVALUE;
    int _ar_sp_62907 = NOVALUE;
    int _pre_refs_62911 = NOVALUE;
    int _old_fwd_params_62926 = NOVALUE;
    int _temp_shifting_sub_62967 = NOVALUE;
    int _new_code_62971 = NOVALUE;
    int _routine_type_62980 = NOVALUE;
    int _32352 = NOVALUE;
    int _31736 = NOVALUE;
    int _31735 = NOVALUE;
    int _31734 = NOVALUE;
    int _31732 = NOVALUE;
    int _31731 = NOVALUE;
    int _31730 = NOVALUE;
    int _31729 = NOVALUE;
    int _31728 = NOVALUE;
    int _31727 = NOVALUE;
    int _31726 = NOVALUE;
    int _31725 = NOVALUE;
    int _31724 = NOVALUE;
    int _31723 = NOVALUE;
    int _31722 = NOVALUE;
    int _31721 = NOVALUE;
    int _31720 = NOVALUE;
    int _31718 = NOVALUE;
    int _31717 = NOVALUE;
    int _31716 = NOVALUE;
    int _31715 = NOVALUE;
    int _31714 = NOVALUE;
    int _31713 = NOVALUE;
    int _31712 = NOVALUE;
    int _31711 = NOVALUE;
    int _31709 = NOVALUE;
    int _31706 = NOVALUE;
    int _31705 = NOVALUE;
    int _31704 = NOVALUE;
    int _31703 = NOVALUE;
    int _31699 = NOVALUE;
    int _31698 = NOVALUE;
    int _31697 = NOVALUE;
    int _31696 = NOVALUE;
    int _31695 = NOVALUE;
    int _31693 = NOVALUE;
    int _31692 = NOVALUE;
    int _31691 = NOVALUE;
    int _31690 = NOVALUE;
    int _31689 = NOVALUE;
    int _31688 = NOVALUE;
    int _31686 = NOVALUE;
    int _31685 = NOVALUE;
    int _31683 = NOVALUE;
    int _31682 = NOVALUE;
    int _31681 = NOVALUE;
    int _31680 = NOVALUE;
    int _31678 = NOVALUE;
    int _31676 = NOVALUE;
    int _31675 = NOVALUE;
    int _31674 = NOVALUE;
    int _31672 = NOVALUE;
    int _31671 = NOVALUE;
    int _31669 = NOVALUE;
    int _31667 = NOVALUE;
    int _31664 = NOVALUE;
    int _31660 = NOVALUE;
    int _31658 = NOVALUE;
    int _31657 = NOVALUE;
    int _31655 = NOVALUE;
    int _31654 = NOVALUE;
    int _31653 = NOVALUE;
    int _31652 = NOVALUE;
    int _31650 = NOVALUE;
    int _31649 = NOVALUE;
    int _31648 = NOVALUE;
    int _31647 = NOVALUE;
    int _31646 = NOVALUE;
    int _31644 = NOVALUE;
    int _31643 = NOVALUE;
    int _31642 = NOVALUE;
    int _31641 = NOVALUE;
    int _31640 = NOVALUE;
    int _31639 = NOVALUE;
    int _31638 = NOVALUE;
    int _31637 = NOVALUE;
    int _31636 = NOVALUE;
    int _31634 = NOVALUE;
    int _31633 = NOVALUE;
    int _31632 = NOVALUE;
    int _31631 = NOVALUE;
    int _31630 = NOVALUE;
    int _31627 = NOVALUE;
    int _31623 = NOVALUE;
    int _31622 = NOVALUE;
    int _31621 = NOVALUE;
    int _31620 = NOVALUE;
    int _31619 = NOVALUE;
    int _31618 = NOVALUE;
    int _31616 = NOVALUE;
    int _31613 = NOVALUE;
    int _31611 = NOVALUE;
    int _31610 = NOVALUE;
    int _31608 = NOVALUE;
    int _31605 = NOVALUE;
    int _31604 = NOVALUE;
    int _31603 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	sequence fr = forward_references[ref]*/
    DeRef(_fr_62767);
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    _fr_62767 = (int)*(((s1_ptr)_2)->base + _ref_62766);
    Ref(_fr_62767);

    /** 	symtab_index sub = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_62765);
    _sub_62770 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_sub_62770)){
        _sub_62770 = (long)DBL_PTR(_sub_62770)->dbl;
    }

    /** 	if sequence( fr[FR_DATA] ) then*/
    _2 = (int)SEQ_PTR(_fr_62767);
    _31603 = (int)*(((s1_ptr)_2)->base + 12);
    _31604 = IS_SEQUENCE(_31603);
    _31603 = NOVALUE;
    if (_31604 == 0)
    {
        _31604 = NOVALUE;
        goto L1; // [34] 121
    }
    else{
        _31604 = NOVALUE;
    }

    /** 		sequence defarg = fr[FR_DATA][1]*/
    _2 = (int)SEQ_PTR(_fr_62767);
    _31605 = (int)*(((s1_ptr)_2)->base + 12);
    DeRef(_defarg_62776);
    _2 = (int)SEQ_PTR(_31605);
    _defarg_62776 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_defarg_62776);
    _31605 = NOVALUE;

    /** 		symtab_index paramsym = defarg[2]*/
    _2 = (int)SEQ_PTR(_defarg_62776);
    _paramsym_62780 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_paramsym_62780)){
        _paramsym_62780 = (long)DBL_PTR(_paramsym_62780)->dbl;
    }

    /** 		token old = { RECORDED, defarg[3] }*/
    _2 = (int)SEQ_PTR(_defarg_62776);
    _31608 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_31608);
    DeRef(_old_62783);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 508;
    ((int *)_2)[2] = _31608;
    _old_62783 = MAKE_SEQ(_1);
    _31608 = NOVALUE;

    /** 		integer tx = find( old, SymTab[paramsym][S_CODE] )*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31610 = (int)*(((s1_ptr)_2)->base + _paramsym_62780);
    _2 = (int)SEQ_PTR(_31610);
    if (!IS_ATOM_INT(_25S_CODE_11925)){
        _31611 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    }
    else{
        _31611 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
    }
    _31610 = NOVALUE;
    _tx_62787 = find_from(_old_62783, _31611, 1);
    _31611 = NOVALUE;

    /** 		SymTab[paramsym][S_CODE][tx] = tok*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_paramsym_62780 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_CODE_11925))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    else
    _3 = (int)(_25S_CODE_11925 + ((s1_ptr)_2)->base);
    _31613 = NOVALUE;
    Ref(_tok_62765);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _tx_62787);
    _1 = *(int *)_2;
    *(int *)_2 = _tok_62765;
    DeRef(_1);
    _31613 = NOVALUE;

    /** 		resolved_reference( ref )*/
    _29resolved_reference(_ref_62766);

    /** 		return*/
    DeRefDS(_defarg_62776);
    DeRefDS(_old_62783);
    DeRef(_tok_62765);
    DeRefDS(_fr_62767);
    DeRef(_code_62822);
    DeRef(_name_62832);
    DeRef(_params_62901);
    DeRef(_orig_code_62903);
    DeRef(_orig_linetable_62904);
    DeRef(_old_fwd_params_62926);
    DeRef(_new_code_62971);
    return;
L1: 
    DeRef(_defarg_62776);
    _defarg_62776 = NOVALUE;
    DeRef(_old_62783);
    _old_62783 = NOVALUE;

    /** 	integer code_sub = fr[FR_SUBPROG]*/
    _2 = (int)SEQ_PTR(_fr_62767);
    _code_sub_62797 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_code_sub_62797))
    _code_sub_62797 = (long)DBL_PTR(_code_sub_62797)->dbl;

    /** 	integer args = SymTab[sub][S_NUM_ARGS]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31616 = (int)*(((s1_ptr)_2)->base + _sub_62770);
    _2 = (int)SEQ_PTR(_31616);
    if (!IS_ATOM_INT(_25S_NUM_ARGS_11964)){
        _args_62799 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NUM_ARGS_11964)->dbl));
    }
    else{
        _args_62799 = (int)*(((s1_ptr)_2)->base + _25S_NUM_ARGS_11964);
    }
    if (!IS_ATOM_INT(_args_62799)){
        _args_62799 = (long)DBL_PTR(_args_62799)->dbl;
    }
    _31616 = NOVALUE;

    /** 	integer is_func = (SymTab[sub][S_TOKEN] = FUNC) or (SymTab[sub][S_TOKEN] = TYPE)*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31618 = (int)*(((s1_ptr)_2)->base + _sub_62770);
    _2 = (int)SEQ_PTR(_31618);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _31619 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _31619 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _31618 = NOVALUE;
    if (IS_ATOM_INT(_31619)) {
        _31620 = (_31619 == 501);
    }
    else {
        _31620 = binary_op(EQUALS, _31619, 501);
    }
    _31619 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31621 = (int)*(((s1_ptr)_2)->base + _sub_62770);
    _2 = (int)SEQ_PTR(_31621);
    if (!IS_ATOM_INT(_25S_TOKEN_11918)){
        _31622 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    }
    else{
        _31622 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    }
    _31621 = NOVALUE;
    if (IS_ATOM_INT(_31622)) {
        _31623 = (_31622 == 504);
    }
    else {
        _31623 = binary_op(EQUALS, _31622, 504);
    }
    _31622 = NOVALUE;
    if (IS_ATOM_INT(_31620) && IS_ATOM_INT(_31623)) {
        _is_func_62804 = (_31620 != 0 || _31623 != 0);
    }
    else {
        _is_func_62804 = binary_op(OR, _31620, _31623);
    }
    DeRef(_31620);
    _31620 = NOVALUE;
    DeRef(_31623);
    _31623 = NOVALUE;
    if (!IS_ATOM_INT(_is_func_62804)) {
        _1 = (long)(DBL_PTR(_is_func_62804)->dbl);
        if (UNIQUE(DBL_PTR(_is_func_62804)) && (DBL_PTR(_is_func_62804)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_is_func_62804);
        _is_func_62804 = _1;
    }

    /** 	integer real_file = current_file_no*/
    _real_file_62818 = _25current_file_no_12262;

    /** 	current_file_no = fr[FR_FILE]*/
    _2 = (int)SEQ_PTR(_fr_62767);
    _25current_file_no_12262 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_25current_file_no_12262)){
        _25current_file_no_12262 = (long)DBL_PTR(_25current_file_no_12262)->dbl;
    }

    /** 	set_code( ref )*/
    _29set_code(_ref_62766);

    /** 	sequence code = Code*/
    RefDS(_25Code_12355);
    DeRef(_code_62822);
    _code_62822 = _25Code_12355;

    /** 	integer temp_sub = CurrentSub*/
    _temp_sub_62824 = _25CurrentSub_12270;

    /** 	integer pc = fr[FR_PC]*/
    _2 = (int)SEQ_PTR(_fr_62767);
    _pc_62826 = (int)*(((s1_ptr)_2)->base + 5);
    if (!IS_ATOM_INT(_pc_62826))
    _pc_62826 = (long)DBL_PTR(_pc_62826)->dbl;

    /** 	integer next_pc = pc*/
    _next_pc_62828 = _pc_62826;

    /** 	integer supplied_args = code[pc+2]*/
    _31627 = _pc_62826 + 2;
    _2 = (int)SEQ_PTR(_code_62822);
    _supplied_args_62829 = (int)*(((s1_ptr)_2)->base + _31627);
    if (!IS_ATOM_INT(_supplied_args_62829))
    _supplied_args_62829 = (long)DBL_PTR(_supplied_args_62829)->dbl;

    /** 	sequence name = fr[FR_NAME]*/
    DeRef(_name_62832);
    _2 = (int)SEQ_PTR(_fr_62767);
    _name_62832 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_name_62832);

    /** 	if Code[pc] != FUNC_FORWARD and Code[pc] != PROC_FORWARD then*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _31630 = (int)*(((s1_ptr)_2)->base + _pc_62826);
    if (IS_ATOM_INT(_31630)) {
        _31631 = (_31630 != 196);
    }
    else {
        _31631 = binary_op(NOTEQ, _31630, 196);
    }
    _31630 = NOVALUE;
    if (IS_ATOM_INT(_31631)) {
        if (_31631 == 0) {
            goto L2; // [280] 350
        }
    }
    else {
        if (DBL_PTR(_31631)->dbl == 0.0) {
            goto L2; // [280] 350
        }
    }
    _2 = (int)SEQ_PTR(_25Code_12355);
    _31633 = (int)*(((s1_ptr)_2)->base + _pc_62826);
    if (IS_ATOM_INT(_31633)) {
        _31634 = (_31633 != 195);
    }
    else {
        _31634 = binary_op(NOTEQ, _31633, 195);
    }
    _31633 = NOVALUE;
    if (_31634 == 0) {
        DeRef(_31634);
        _31634 = NOVALUE;
        goto L2; // [297] 350
    }
    else {
        if (!IS_ATOM_INT(_31634) && DBL_PTR(_31634)->dbl == 0.0){
            DeRef(_31634);
            _31634 = NOVALUE;
            goto L2; // [297] 350
        }
        DeRef(_31634);
        _31634 = NOVALUE;
    }
    DeRef(_31634);
    _31634 = NOVALUE;

    /** 		prep_forward_error( ref )*/
    _29prep_forward_error(_ref_62766);

    /** 		CompileErr( "The forward call to [4] wasn't where we thought it would be: [1]:[2]:[3]",*/
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _31636 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    _2 = (int)SEQ_PTR(_fr_62767);
    _31637 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_31637);
    _31638 = _52sym_name(_31637);
    _31637 = NOVALUE;
    _2 = (int)SEQ_PTR(_fr_62767);
    _31639 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_fr_62767);
    _31640 = (int)*(((s1_ptr)_2)->base + 2);
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_31636);
    *((int *)(_2+4)) = _31636;
    *((int *)(_2+8)) = _31638;
    Ref(_31639);
    *((int *)(_2+12)) = _31639;
    Ref(_31640);
    *((int *)(_2+16)) = _31640;
    _31641 = MAKE_SEQ(_1);
    _31640 = NOVALUE;
    _31639 = NOVALUE;
    _31638 = NOVALUE;
    _31636 = NOVALUE;
    RefDS(_31635);
    _43CompileErr(_31635, _31641, 0);
    _31641 = NOVALUE;
L2: 

    /** 	integer old_temps_allocated = temps_allocated*/
    _old_temps_allocated_62856 = _52temps_allocated_47621;

    /** 	temps_allocated = 0*/
    _52temps_allocated_47621 = 0;

    /** 	if is_func and fr[FR_OP] = PROC then*/
    if (_is_func_62804 == 0) {
        goto L3; // [368] 458
    }
    _2 = (int)SEQ_PTR(_fr_62767);
    _31643 = (int)*(((s1_ptr)_2)->base + 10);
    if (IS_ATOM_INT(_31643)) {
        _31644 = (_31643 == 27);
    }
    else {
        _31644 = binary_op(EQUALS, _31643, 27);
    }
    _31643 = NOVALUE;
    if (_31644 == 0) {
        DeRef(_31644);
        _31644 = NOVALUE;
        goto L3; // [385] 458
    }
    else {
        if (!IS_ATOM_INT(_31644) && DBL_PTR(_31644)->dbl == 0.0){
            DeRef(_31644);
            _31644 = NOVALUE;
            goto L3; // [385] 458
        }
        DeRef(_31644);
        _31644 = NOVALUE;
    }
    DeRef(_31644);
    _31644 = NOVALUE;

    /** 		symtab_index temp_target = NewTempSym()*/
    _temp_target_62865 = _52NewTempSym(0);
    if (!IS_ATOM_INT(_temp_target_62865)) {
        _1 = (long)(DBL_PTR(_temp_target_62865)->dbl);
        if (UNIQUE(DBL_PTR(_temp_target_62865)) && (DBL_PTR(_temp_target_62865)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_temp_target_62865);
        _temp_target_62865 = _1;
    }

    /** 		sequence converted_code = */
    _31646 = _pc_62826 + 1;
    if (_31646 > MAXINT){
        _31646 = NewDouble((double)_31646);
    }
    _31647 = _pc_62826 + 2;
    if ((long)((unsigned long)_31647 + (unsigned long)HIGH_BITS) >= 0) 
    _31647 = NewDouble((double)_31647);
    if (IS_ATOM_INT(_31647)) {
        _31648 = _31647 + _supplied_args_62829;
    }
    else {
        _31648 = NewDouble(DBL_PTR(_31647)->dbl + (double)_supplied_args_62829);
    }
    DeRef(_31647);
    _31647 = NOVALUE;
    rhs_slice_target = (object_ptr)&_31649;
    RHS_Slice(_25Code_12355, _31646, _31648);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 208;
    ((int *)_2)[2] = _temp_target_62865;
    _31650 = MAKE_SEQ(_1);
    {
        int concat_list[4];

        concat_list[0] = _31650;
        concat_list[1] = _temp_target_62865;
        concat_list[2] = _31649;
        concat_list[3] = 196;
        Concat_N((object_ptr)&_converted_code_62868, concat_list, 4);
    }
    DeRefDS(_31650);
    _31650 = NOVALUE;
    DeRefDS(_31649);
    _31649 = NOVALUE;

    /** 		replace_code( converted_code, pc, pc + 2 + supplied_args, code_sub )*/
    _31652 = _pc_62826 + 2;
    if ((long)((unsigned long)_31652 + (unsigned long)HIGH_BITS) >= 0) 
    _31652 = NewDouble((double)_31652);
    if (IS_ATOM_INT(_31652)) {
        _31653 = _31652 + _supplied_args_62829;
        if ((long)((unsigned long)_31653 + (unsigned long)HIGH_BITS) >= 0) 
        _31653 = NewDouble((double)_31653);
    }
    else {
        _31653 = NewDouble(DBL_PTR(_31652)->dbl + (double)_supplied_args_62829);
    }
    DeRef(_31652);
    _31652 = NOVALUE;
    RefDS(_converted_code_62868);
    _29replace_code(_converted_code_62868, _pc_62826, _31653, _code_sub_62797);
    _31653 = NOVALUE;

    /** 		code = Code*/
    RefDS(_25Code_12355);
    DeRef(_code_62822);
    _code_62822 = _25Code_12355;
L3: 
    DeRef(_converted_code_62868);
    _converted_code_62868 = NOVALUE;

    /** 	next_pc +=*/
    _31654 = 3 + _supplied_args_62829;
    if ((long)((unsigned long)_31654 + (unsigned long)HIGH_BITS) >= 0) 
    _31654 = NewDouble((double)_31654);
    if (IS_ATOM_INT(_31654)) {
        _31655 = _31654 + _is_func_62804;
        if ((long)((unsigned long)_31655 + (unsigned long)HIGH_BITS) >= 0) 
        _31655 = NewDouble((double)_31655);
    }
    else {
        _31655 = NewDouble(DBL_PTR(_31654)->dbl + (double)_is_func_62804);
    }
    DeRef(_31654);
    _31654 = NOVALUE;
    if (IS_ATOM_INT(_31655)) {
        _next_pc_62828 = _next_pc_62828 + _31655;
    }
    else {
        _next_pc_62828 = NewDouble((double)_next_pc_62828 + DBL_PTR(_31655)->dbl);
    }
    DeRef(_31655);
    _31655 = NOVALUE;
    if (!IS_ATOM_INT(_next_pc_62828)) {
        _1 = (long)(DBL_PTR(_next_pc_62828)->dbl);
        if (UNIQUE(DBL_PTR(_next_pc_62828)) && (DBL_PTR(_next_pc_62828)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_next_pc_62828);
        _next_pc_62828 = _1;
    }

    /** 	integer target*/

    /** 	if is_func then*/
    if (_is_func_62804 == 0)
    {
        goto L4; // [480] 502
    }
    else{
    }

    /** 		target = Code[pc + 3 + supplied_args]*/
    _31657 = _pc_62826 + 3;
    if ((long)((unsigned long)_31657 + (unsigned long)HIGH_BITS) >= 0) 
    _31657 = NewDouble((double)_31657);
    if (IS_ATOM_INT(_31657)) {
        _31658 = _31657 + _supplied_args_62829;
    }
    else {
        _31658 = NewDouble(DBL_PTR(_31657)->dbl + (double)_supplied_args_62829);
    }
    DeRef(_31657);
    _31657 = NOVALUE;
    _2 = (int)SEQ_PTR(_25Code_12355);
    if (!IS_ATOM_INT(_31658)){
        _target_62884 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31658)->dbl));
    }
    else{
        _target_62884 = (int)*(((s1_ptr)_2)->base + _31658);
    }
    if (!IS_ATOM_INT(_target_62884)){
        _target_62884 = (long)DBL_PTR(_target_62884)->dbl;
    }
L4: 

    /** 	integer has_defaults = 0*/
    _has_defaults_62890 = 0;

    /** 	integer goto_target = length( code ) + 1*/
    if (IS_SEQUENCE(_code_62822)){
            _31660 = SEQ_PTR(_code_62822)->length;
    }
    else {
        _31660 = 1;
    }
    _goto_target_62891 = _31660 + 1;
    _31660 = NOVALUE;

    /** 	integer defarg = 0*/
    _defarg_62894 = 0;

    /** 	integer code_len = length(code)*/
    if (IS_SEQUENCE(_code_62822)){
            _code_len_62895 = SEQ_PTR(_code_62822)->length;
    }
    else {
        _code_len_62895 = 1;
    }

    /** 	integer extra_default_args = 0*/
    _extra_default_args_62897 = 0;

    /** 	set_dont_read( 1 )*/
    _60set_dont_read(1);

    /** 	reset_private_lists()*/

    /** 	fwd_private_sym  = {}*/
    RefDS(_22682);
    DeRefi(_29fwd_private_sym_62728);
    _29fwd_private_sym_62728 = _22682;

    /** 	fwd_private_name = {}*/
    RefDS(_22682);
    DeRef(_29fwd_private_name_62729);
    _29fwd_private_name_62729 = _22682;

    /** end procedure*/
    goto L5; // [554] 557
L5: 

    /** 	integer param_sym = sub*/
    _param_sym_62900 = _sub_62770;

    /** 	sequence params = repeat( 0, args )*/
    DeRef(_params_62901);
    _params_62901 = Repeat(0, _args_62799);

    /** 	sequence orig_code = code*/
    RefDS(_code_62822);
    DeRef(_orig_code_62903);
    _orig_code_62903 = _code_62822;

    /** 	sequence orig_linetable = LineTable*/
    RefDS(_25LineTable_12356);
    DeRef(_orig_linetable_62904);
    _orig_linetable_62904 = _25LineTable_12356;

    /** 	Code = {}*/
    RefDS(_22682);
    DeRef(_25Code_12355);
    _25Code_12355 = _22682;

    /** 	integer ar_sp = find( code_sub, active_subprogs[current_file_no] )*/
    _2 = (int)SEQ_PTR(_29active_subprogs_62546);
    _31664 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    _ar_sp_62907 = find_from(_code_sub_62797, _31664, 1);
    _31664 = NOVALUE;

    /** 	integer pre_refs*/

    /** 	if code_sub = TopLevelSub then*/
    if (_code_sub_62797 != _25TopLevelSub_12269)
    goto L6; // [614] 634

    /** 		pre_refs = length( toplevel_references[current_file_no] )*/
    _2 = (int)SEQ_PTR(_29toplevel_references_62548);
    _31667 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    if (IS_SEQUENCE(_31667)){
            _pre_refs_62911 = SEQ_PTR(_31667)->length;
    }
    else {
        _pre_refs_62911 = 1;
    }
    _31667 = NOVALUE;
    goto L7; // [631] 667
L6: 

    /** 		ar_sp = find( code_sub, active_subprogs[current_file_no] )*/
    _2 = (int)SEQ_PTR(_29active_subprogs_62546);
    _31669 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    _ar_sp_62907 = find_from(_code_sub_62797, _31669, 1);
    _31669 = NOVALUE;

    /** 		pre_refs = length( active_references[current_file_no][ar_sp] )*/
    _2 = (int)SEQ_PTR(_29active_references_62547);
    _31671 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    _2 = (int)SEQ_PTR(_31671);
    _31672 = (int)*(((s1_ptr)_2)->base + _ar_sp_62907);
    _31671 = NOVALUE;
    if (IS_SEQUENCE(_31672)){
            _pre_refs_62911 = SEQ_PTR(_31672)->length;
    }
    else {
        _pre_refs_62911 = 1;
    }
    _31672 = NOVALUE;
L7: 

    /** 	sequence old_fwd_params = {}*/
    RefDS(_22682);
    DeRef(_old_fwd_params_62926);
    _old_fwd_params_62926 = _22682;

    /** 	for i = pc + 3 to pc + args + 2 do*/
    _31674 = _pc_62826 + 3;
    if ((long)((unsigned long)_31674 + (unsigned long)HIGH_BITS) >= 0) 
    _31674 = NewDouble((double)_31674);
    _31675 = _pc_62826 + _args_62799;
    if ((long)((unsigned long)_31675 + (unsigned long)HIGH_BITS) >= 0) 
    _31675 = NewDouble((double)_31675);
    if (IS_ATOM_INT(_31675)) {
        _31676 = _31675 + 2;
        if ((long)((unsigned long)_31676 + (unsigned long)HIGH_BITS) >= 0) 
        _31676 = NewDouble((double)_31676);
    }
    else {
        _31676 = NewDouble(DBL_PTR(_31675)->dbl + (double)2);
    }
    DeRef(_31675);
    _31675 = NOVALUE;
    {
        int _i_62928;
        Ref(_31674);
        _i_62928 = _31674;
L8: 
        if (binary_op_a(GREATER, _i_62928, _31676)){
            goto L9; // [688] 849
        }

        /** 		defarg += 1*/
        _defarg_62894 = _defarg_62894 + 1;

        /** 		param_sym = SymTab[param_sym][S_NEXT]*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _31678 = (int)*(((s1_ptr)_2)->base + _param_sym_62900);
        _2 = (int)SEQ_PTR(_31678);
        _param_sym_62900 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_param_sym_62900)){
            _param_sym_62900 = (long)DBL_PTR(_param_sym_62900)->dbl;
        }
        _31678 = NOVALUE;

        /** 		if defarg > supplied_args or i > length( code ) or not code[i] then*/
        _31680 = (_defarg_62894 > _supplied_args_62829);
        if (_31680 != 0) {
            _31681 = 1;
            goto LA; // [723] 738
        }
        if (IS_SEQUENCE(_code_62822)){
                _31682 = SEQ_PTR(_code_62822)->length;
        }
        else {
            _31682 = 1;
        }
        if (IS_ATOM_INT(_i_62928)) {
            _31683 = (_i_62928 > _31682);
        }
        else {
            _31683 = (DBL_PTR(_i_62928)->dbl > (double)_31682);
        }
        _31682 = NOVALUE;
        _31681 = (_31683 != 0);
LA: 
        if (_31681 != 0) {
            goto LB; // [738] 754
        }
        _2 = (int)SEQ_PTR(_code_62822);
        if (!IS_ATOM_INT(_i_62928)){
            _31685 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_62928)->dbl));
        }
        else{
            _31685 = (int)*(((s1_ptr)_2)->base + _i_62928);
        }
        if (IS_ATOM_INT(_31685)) {
            _31686 = (_31685 == 0);
        }
        else {
            _31686 = unary_op(NOT, _31685);
        }
        _31685 = NOVALUE;
        if (_31686 == 0) {
            DeRef(_31686);
            _31686 = NOVALUE;
            goto LC; // [750] 804
        }
        else {
            if (!IS_ATOM_INT(_31686) && DBL_PTR(_31686)->dbl == 0.0){
                DeRef(_31686);
                _31686 = NOVALUE;
                goto LC; // [750] 804
            }
            DeRef(_31686);
            _31686 = NOVALUE;
        }
        DeRef(_31686);
        _31686 = NOVALUE;
LB: 

        /** 			has_defaults = 1*/
        _has_defaults_62890 = 1;

        /** 			extra_default_args += 1*/
        _extra_default_args_62897 = _extra_default_args_62897 + 1;

        /** 			show_params( sub )*/
        _52show_params(_sub_62770);

        /** 			set_error_info( ref )*/
        _29set_error_info(_ref_62766);

        /** 			Parse_default_arg(sub, defarg, fwd_private_name, fwd_private_sym) --call_proc( parse_arg_rid, { sub, defarg, fwd_private_name, fwd_private_sym } )*/
        RefDS(_29fwd_private_name_62729);
        RefDS(_29fwd_private_sym_62728);
        _30Parse_default_arg(_sub_62770, _defarg_62894, _29fwd_private_name_62729, _29fwd_private_sym_62728);

        /** 			hide_params( sub )*/
        _52hide_params(_sub_62770);

        /** 			params[defarg] = Pop()*/
        _31688 = _37Pop();
        _2 = (int)SEQ_PTR(_params_62901);
        _2 = (int)(((s1_ptr)_2)->base + _defarg_62894);
        _1 = *(int *)_2;
        *(int *)_2 = _31688;
        if( _1 != _31688 ){
            DeRef(_1);
        }
        _31688 = NOVALUE;
        goto LD; // [801] 842
LC: 

        /** 			extra_default_args = 0*/
        _extra_default_args_62897 = 0;

        /** 			add_private_symbol( code[i], SymTab[param_sym][S_NAME] )*/
        _2 = (int)SEQ_PTR(_code_62822);
        if (!IS_ATOM_INT(_i_62928)){
            _31689 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_62928)->dbl));
        }
        else{
            _31689 = (int)*(((s1_ptr)_2)->base + _i_62928);
        }
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _31690 = (int)*(((s1_ptr)_2)->base + _param_sym_62900);
        _2 = (int)SEQ_PTR(_31690);
        if (!IS_ATOM_INT(_25S_NAME_11913)){
            _31691 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
        }
        else{
            _31691 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
        }
        _31690 = NOVALUE;
        Ref(_31689);
        Ref(_31691);
        _29add_private_symbol(_31689, _31691);
        _31689 = NOVALUE;
        _31691 = NOVALUE;

        /** 			params[defarg] = code[i]*/
        _2 = (int)SEQ_PTR(_code_62822);
        if (!IS_ATOM_INT(_i_62928)){
            _31692 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_62928)->dbl));
        }
        else{
            _31692 = (int)*(((s1_ptr)_2)->base + _i_62928);
        }
        Ref(_31692);
        _2 = (int)SEQ_PTR(_params_62901);
        _2 = (int)(((s1_ptr)_2)->base + _defarg_62894);
        _1 = *(int *)_2;
        *(int *)_2 = _31692;
        if( _1 != _31692 ){
            DeRef(_1);
        }
        _31692 = NOVALUE;
LD: 

        /** 	end for*/
        _0 = _i_62928;
        if (IS_ATOM_INT(_i_62928)) {
            _i_62928 = _i_62928 + 1;
            if ((long)((unsigned long)_i_62928 +(unsigned long) HIGH_BITS) >= 0){
                _i_62928 = NewDouble((double)_i_62928);
            }
        }
        else {
            _i_62928 = binary_op_a(PLUS, _i_62928, 1);
        }
        DeRef(_0);
        goto L8; // [844] 695
L9: 
        ;
        DeRef(_i_62928);
    }

    /** 	SymTab[code_sub][S_STACK_SPACE] += temps_allocated*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_code_sub_62797 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!IS_ATOM_INT(_25S_STACK_SPACE_11973)){
        _31695 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_STACK_SPACE_11973)->dbl));
    }
    else{
        _31695 = (int)*(((s1_ptr)_2)->base + _25S_STACK_SPACE_11973);
    }
    _31693 = NOVALUE;
    if (IS_ATOM_INT(_31695)) {
        _31696 = _31695 + _52temps_allocated_47621;
        if ((long)((unsigned long)_31696 + (unsigned long)HIGH_BITS) >= 0) 
        _31696 = NewDouble((double)_31696);
    }
    else {
        _31696 = binary_op(PLUS, _31695, _52temps_allocated_47621);
    }
    _31695 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_STACK_SPACE_11973))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_STACK_SPACE_11973)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_STACK_SPACE_11973);
    _1 = *(int *)_2;
    *(int *)_2 = _31696;
    if( _1 != _31696 ){
        DeRef(_1);
    }
    _31696 = NOVALUE;
    _31693 = NOVALUE;

    /** 	temps_allocated = old_temps_allocated*/
    _52temps_allocated_47621 = _old_temps_allocated_62856;

    /** 	integer temp_shifting_sub = shifting_sub*/
    _temp_shifting_sub_62967 = _29shifting_sub_62575;

    /** 	shift( -pc, pc-1 )*/
    if ((unsigned long)_pc_62826 == 0xC0000000)
    _31697 = (int)NewDouble((double)-0xC0000000);
    else
    _31697 = - _pc_62826;
    _31698 = _pc_62826 - 1;
    if ((long)((unsigned long)_31698 +(unsigned long) HIGH_BITS) >= 0){
        _31698 = NewDouble((double)_31698);
    }
    Ref(_31697);
    DeRef(_32352);
    _32352 = _31697;
    _65shift(_31697, _31698, _32352);
    _31697 = NOVALUE;
    _31698 = NOVALUE;
    _32352 = NOVALUE;

    /** 	sequence new_code = Code*/
    RefDS(_25Code_12355);
    DeRef(_new_code_62971);
    _new_code_62971 = _25Code_12355;

    /** 	Code = orig_code*/
    RefDS(_orig_code_62903);
    DeRefDS(_25Code_12355);
    _25Code_12355 = _orig_code_62903;

    /** 	LineTable = orig_linetable*/
    RefDS(_orig_linetable_62904);
    DeRef(_25LineTable_12356);
    _25LineTable_12356 = _orig_linetable_62904;

    /** 	set_dont_read( 0 )*/
    _60set_dont_read(0);

    /** 	current_file_no = real_file*/
    _25current_file_no_12262 = _real_file_62818;

    /** 	if args != ( supplied_args + extra_default_args ) then*/
    _31699 = _supplied_args_62829 + _extra_default_args_62897;
    if ((long)((unsigned long)_31699 + (unsigned long)HIGH_BITS) >= 0) 
    _31699 = NewDouble((double)_31699);
    if (binary_op_a(EQUALS, _args_62799, _31699)){
        DeRef(_31699);
        _31699 = NOVALUE;
        goto LE; // [946] 1028
    }
    DeRef(_31699);
    _31699 = NOVALUE;

    /** 		sequence routine_type*/

    /** 		if is_func then */
    if (_is_func_62804 == 0)
    {
        goto LF; // [954] 967
    }
    else{
    }

    /** 			routine_type = "function"*/
    RefDS(_27051);
    DeRefi(_routine_type_62980);
    _routine_type_62980 = _27051;
    goto L10; // [964] 975
LF: 

    /** 			routine_type = "procedure"*/
    RefDS(_27105);
    DeRefi(_routine_type_62980);
    _routine_type_62980 = _27105;
L10: 

    /** 		current_file_no = fr[FR_FILE]*/
    _2 = (int)SEQ_PTR(_fr_62767);
    _25current_file_no_12262 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_25current_file_no_12262)){
        _25current_file_no_12262 = (long)DBL_PTR(_25current_file_no_12262)->dbl;
    }

    /** 		line_number = fr[FR_LINE]*/
    _2 = (int)SEQ_PTR(_fr_62767);
    _25line_number_12263 = (int)*(((s1_ptr)_2)->base + 6);
    if (!IS_ATOM_INT(_25line_number_12263)){
        _25line_number_12263 = (long)DBL_PTR(_25line_number_12263)->dbl;
    }

    /** 		CompileErr( 158,*/
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _31703 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    _31704 = _supplied_args_62829 + _extra_default_args_62897;
    if ((long)((unsigned long)_31704 + (unsigned long)HIGH_BITS) >= 0) 
    _31704 = NewDouble((double)_31704);
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_31703);
    *((int *)(_2+4)) = _31703;
    *((int *)(_2+8)) = _25line_number_12263;
    RefDS(_routine_type_62980);
    *((int *)(_2+12)) = _routine_type_62980;
    RefDS(_name_62832);
    *((int *)(_2+16)) = _name_62832;
    *((int *)(_2+20)) = _args_62799;
    *((int *)(_2+24)) = _31704;
    _31705 = MAKE_SEQ(_1);
    _31704 = NOVALUE;
    _31703 = NOVALUE;
    _43CompileErr(158, _31705, 0);
    _31705 = NOVALUE;
LE: 
    DeRefi(_routine_type_62980);
    _routine_type_62980 = NOVALUE;

    /** 	new_code &= PROC & sub & params*/
    {
        int concat_list[3];

        concat_list[0] = _params_62901;
        concat_list[1] = _sub_62770;
        concat_list[2] = 27;
        Concat_N((object_ptr)&_31706, concat_list, 3);
    }
    Concat((object_ptr)&_new_code_62971, _new_code_62971, _31706);
    DeRefDS(_31706);
    _31706 = NOVALUE;

    /** 	if is_func then*/
    if (_is_func_62804 == 0)
    {
        goto L11; // [1046] 1058
    }
    else{
    }

    /** 		new_code &= target*/
    Append(&_new_code_62971, _new_code_62971, _target_62884);
L11: 

    /** 	replace_code( new_code, pc, next_pc - 1, code_sub )*/
    _31709 = _next_pc_62828 - 1;
    if ((long)((unsigned long)_31709 +(unsigned long) HIGH_BITS) >= 0){
        _31709 = NewDouble((double)_31709);
    }
    RefDS(_new_code_62971);
    _29replace_code(_new_code_62971, _pc_62826, _31709, _code_sub_62797);
    _31709 = NOVALUE;

    /** 	if code_sub = TopLevelSub then*/
    if (_code_sub_62797 != _25TopLevelSub_12269)
    goto L12; // [1074] 1161

    /** 		for i = pre_refs + 1 to length( toplevel_references[fr[FR_FILE]] ) do*/
    _31711 = _pre_refs_62911 + 1;
    if (_31711 > MAXINT){
        _31711 = NewDouble((double)_31711);
    }
    _2 = (int)SEQ_PTR(_fr_62767);
    _31712 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_29toplevel_references_62548);
    if (!IS_ATOM_INT(_31712)){
        _31713 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31712)->dbl));
    }
    else{
        _31713 = (int)*(((s1_ptr)_2)->base + _31712);
    }
    if (IS_SEQUENCE(_31713)){
            _31714 = SEQ_PTR(_31713)->length;
    }
    else {
        _31714 = 1;
    }
    _31713 = NOVALUE;
    {
        int _i_63004;
        Ref(_31711);
        _i_63004 = _31711;
L13: 
        if (binary_op_a(GREATER, _i_63004, _31714)){
            goto L14; // [1101] 1158
        }

        /** 			forward_references[toplevel_references[fr[FR_FILE]][i]][FR_PC] += pc - 1*/
        _2 = (int)SEQ_PTR(_fr_62767);
        _31715 = (int)*(((s1_ptr)_2)->base + 3);
        _2 = (int)SEQ_PTR(_29toplevel_references_62548);
        if (!IS_ATOM_INT(_31715)){
            _31716 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31715)->dbl));
        }
        else{
            _31716 = (int)*(((s1_ptr)_2)->base + _31715);
        }
        _2 = (int)SEQ_PTR(_31716);
        if (!IS_ATOM_INT(_i_63004)){
            _31717 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_63004)->dbl));
        }
        else{
            _31717 = (int)*(((s1_ptr)_2)->base + _i_63004);
        }
        _31716 = NOVALUE;
        _2 = (int)SEQ_PTR(_29forward_references_62545);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _29forward_references_62545 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_31717))
        _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_31717)->dbl));
        else
        _3 = (int)(_31717 + ((s1_ptr)_2)->base);
        _31720 = _pc_62826 - 1;
        if ((long)((unsigned long)_31720 +(unsigned long) HIGH_BITS) >= 0){
            _31720 = NewDouble((double)_31720);
        }
        _2 = (int)SEQ_PTR(*(int *)_3);
        _31721 = (int)*(((s1_ptr)_2)->base + 5);
        _31718 = NOVALUE;
        if (IS_ATOM_INT(_31721) && IS_ATOM_INT(_31720)) {
            _31722 = _31721 + _31720;
            if ((long)((unsigned long)_31722 + (unsigned long)HIGH_BITS) >= 0) 
            _31722 = NewDouble((double)_31722);
        }
        else {
            _31722 = binary_op(PLUS, _31721, _31720);
        }
        _31721 = NOVALUE;
        DeRef(_31720);
        _31720 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = _31722;
        if( _1 != _31722 ){
            DeRef(_1);
        }
        _31722 = NOVALUE;
        _31718 = NOVALUE;

        /** 		end for*/
        _0 = _i_63004;
        if (IS_ATOM_INT(_i_63004)) {
            _i_63004 = _i_63004 + 1;
            if ((long)((unsigned long)_i_63004 +(unsigned long) HIGH_BITS) >= 0){
                _i_63004 = NewDouble((double)_i_63004);
            }
        }
        else {
            _i_63004 = binary_op_a(PLUS, _i_63004, 1);
        }
        DeRef(_0);
        goto L13; // [1153] 1108
L14: 
        ;
        DeRef(_i_63004);
    }
    goto L15; // [1158] 1250
L12: 

    /** 		for i = pre_refs + 1 to length( active_references[fr[FR_FILE]][ar_sp] ) do*/
    _31723 = _pre_refs_62911 + 1;
    if (_31723 > MAXINT){
        _31723 = NewDouble((double)_31723);
    }
    _2 = (int)SEQ_PTR(_fr_62767);
    _31724 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_29active_references_62547);
    if (!IS_ATOM_INT(_31724)){
        _31725 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31724)->dbl));
    }
    else{
        _31725 = (int)*(((s1_ptr)_2)->base + _31724);
    }
    _2 = (int)SEQ_PTR(_31725);
    _31726 = (int)*(((s1_ptr)_2)->base + _ar_sp_62907);
    _31725 = NOVALUE;
    if (IS_SEQUENCE(_31726)){
            _31727 = SEQ_PTR(_31726)->length;
    }
    else {
        _31727 = 1;
    }
    _31726 = NOVALUE;
    {
        int _i_63019;
        Ref(_31723);
        _i_63019 = _31723;
L16: 
        if (binary_op_a(GREATER, _i_63019, _31727)){
            goto L17; // [1188] 1249
        }

        /** 			forward_references[active_references[fr[FR_FILE]][ar_sp][i]][FR_PC] += pc - 1*/
        _2 = (int)SEQ_PTR(_fr_62767);
        _31728 = (int)*(((s1_ptr)_2)->base + 3);
        _2 = (int)SEQ_PTR(_29active_references_62547);
        if (!IS_ATOM_INT(_31728)){
            _31729 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31728)->dbl));
        }
        else{
            _31729 = (int)*(((s1_ptr)_2)->base + _31728);
        }
        _2 = (int)SEQ_PTR(_31729);
        _31730 = (int)*(((s1_ptr)_2)->base + _ar_sp_62907);
        _31729 = NOVALUE;
        _2 = (int)SEQ_PTR(_31730);
        if (!IS_ATOM_INT(_i_63019)){
            _31731 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_63019)->dbl));
        }
        else{
            _31731 = (int)*(((s1_ptr)_2)->base + _i_63019);
        }
        _31730 = NOVALUE;
        _2 = (int)SEQ_PTR(_29forward_references_62545);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _29forward_references_62545 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_31731))
        _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_31731)->dbl));
        else
        _3 = (int)(_31731 + ((s1_ptr)_2)->base);
        _31734 = _pc_62826 - 1;
        if ((long)((unsigned long)_31734 +(unsigned long) HIGH_BITS) >= 0){
            _31734 = NewDouble((double)_31734);
        }
        _2 = (int)SEQ_PTR(*(int *)_3);
        _31735 = (int)*(((s1_ptr)_2)->base + 5);
        _31732 = NOVALUE;
        if (IS_ATOM_INT(_31735) && IS_ATOM_INT(_31734)) {
            _31736 = _31735 + _31734;
            if ((long)((unsigned long)_31736 + (unsigned long)HIGH_BITS) >= 0) 
            _31736 = NewDouble((double)_31736);
        }
        else {
            _31736 = binary_op(PLUS, _31735, _31734);
        }
        _31735 = NOVALUE;
        DeRef(_31734);
        _31734 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = _31736;
        if( _1 != _31736 ){
            DeRef(_1);
        }
        _31736 = NOVALUE;
        _31732 = NOVALUE;

        /** 		end for*/
        _0 = _i_63019;
        if (IS_ATOM_INT(_i_63019)) {
            _i_63019 = _i_63019 + 1;
            if ((long)((unsigned long)_i_63019 +(unsigned long) HIGH_BITS) >= 0){
                _i_63019 = NewDouble((double)_i_63019);
            }
        }
        else {
            _i_63019 = binary_op_a(PLUS, _i_63019, 1);
        }
        DeRef(_0);
        goto L16; // [1244] 1195
L17: 
        ;
        DeRef(_i_63019);
    }
L15: 

    /** 	reset_code()*/
    _29reset_code();

    /** 	resolved_reference( ref )*/
    _29resolved_reference(_ref_62766);

    /** end procedure*/
    DeRef(_tok_62765);
    DeRef(_fr_62767);
    DeRef(_code_62822);
    DeRef(_name_62832);
    DeRef(_params_62901);
    DeRef(_orig_code_62903);
    DeRef(_orig_linetable_62904);
    DeRef(_old_fwd_params_62926);
    DeRef(_new_code_62971);
    _31724 = NOVALUE;
    _31728 = NOVALUE;
    DeRef(_31627);
    _31627 = NOVALUE;
    _31717 = NOVALUE;
    DeRef(_31631);
    _31631 = NOVALUE;
    _31731 = NOVALUE;
    DeRef(_31683);
    _31683 = NOVALUE;
    DeRef(_31674);
    _31674 = NOVALUE;
    DeRef(_31658);
    _31658 = NOVALUE;
    DeRef(_31646);
    _31646 = NOVALUE;
    DeRef(_31680);
    _31680 = NOVALUE;
    DeRef(_31648);
    _31648 = NOVALUE;
    DeRef(_31711);
    _31711 = NOVALUE;
    DeRef(_31723);
    _31723 = NOVALUE;
    DeRef(_31676);
    _31676 = NOVALUE;
    _31712 = NOVALUE;
    _31713 = NOVALUE;
    _31672 = NOVALUE;
    _31715 = NOVALUE;
    _31667 = NOVALUE;
    _31726 = NOVALUE;
    return;
    ;
}


void _29set_error_info(int _ref_63036)
{
    int _fr_63037 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence fr = forward_references[ref]*/
    DeRef(_fr_63037);
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    _fr_63037 = (int)*(((s1_ptr)_2)->base + _ref_63036);
    Ref(_fr_63037);

    /** 	ThisLine        = fr[FR_THISLINE]*/
    DeRef(_43ThisLine_49532);
    _2 = (int)SEQ_PTR(_fr_63037);
    _43ThisLine_49532 = (int)*(((s1_ptr)_2)->base + 7);
    Ref(_43ThisLine_49532);

    /** 	bp              = fr[FR_BP]*/
    _2 = (int)SEQ_PTR(_fr_63037);
    _43bp_49536 = (int)*(((s1_ptr)_2)->base + 8);
    if (!IS_ATOM_INT(_43bp_49536)){
        _43bp_49536 = (long)DBL_PTR(_43bp_49536)->dbl;
    }

    /** 	line_number     = fr[FR_LINE]*/
    _2 = (int)SEQ_PTR(_fr_63037);
    _25line_number_12263 = (int)*(((s1_ptr)_2)->base + 6);
    if (!IS_ATOM_INT(_25line_number_12263)){
        _25line_number_12263 = (long)DBL_PTR(_25line_number_12263)->dbl;
    }

    /** 	current_file_no = fr[FR_FILE]*/
    _2 = (int)SEQ_PTR(_fr_63037);
    _25current_file_no_12262 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_25current_file_no_12262)){
        _25current_file_no_12262 = (long)DBL_PTR(_25current_file_no_12262)->dbl;
    }

    /** end procedure*/
    DeRefDS(_fr_63037);
    return;
    ;
}


void _29patch_forward_variable(int _tok_63050, int _ref_63051)
{
    int _fr_63052 = NOVALUE;
    int _sym_63055 = NOVALUE;
    int _pc_63107 = NOVALUE;
    int _vx_63111 = NOVALUE;
    int _d_63128 = NOVALUE;
    int _param_63138 = NOVALUE;
    int _old_63141 = NOVALUE;
    int _new_63146 = NOVALUE;
    int _31793 = NOVALUE;
    int _31792 = NOVALUE;
    int _31791 = NOVALUE;
    int _31789 = NOVALUE;
    int _31786 = NOVALUE;
    int _31784 = NOVALUE;
    int _31783 = NOVALUE;
    int _31782 = NOVALUE;
    int _31781 = NOVALUE;
    int _31779 = NOVALUE;
    int _31778 = NOVALUE;
    int _31777 = NOVALUE;
    int _31776 = NOVALUE;
    int _31775 = NOVALUE;
    int _31773 = NOVALUE;
    int _31771 = NOVALUE;
    int _31768 = NOVALUE;
    int _31767 = NOVALUE;
    int _31766 = NOVALUE;
    int _31764 = NOVALUE;
    int _31763 = NOVALUE;
    int _31762 = NOVALUE;
    int _31761 = NOVALUE;
    int _31759 = NOVALUE;
    int _31757 = NOVALUE;
    int _31756 = NOVALUE;
    int _31755 = NOVALUE;
    int _31754 = NOVALUE;
    int _31753 = NOVALUE;
    int _31752 = NOVALUE;
    int _31751 = NOVALUE;
    int _31750 = NOVALUE;
    int _31749 = NOVALUE;
    int _31748 = NOVALUE;
    int _31747 = NOVALUE;
    int _31746 = NOVALUE;
    int _31745 = NOVALUE;
    int _31744 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	sequence fr = forward_references[ref]*/
    DeRef(_fr_63052);
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    _fr_63052 = (int)*(((s1_ptr)_2)->base + _ref_63051);
    Ref(_fr_63052);

    /** 	symtab_index sym = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_63050);
    _sym_63055 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_sym_63055)){
        _sym_63055 = (long)DBL_PTR(_sym_63055)->dbl;
    }

    /** 	if SymTab[sym][S_FILE_NO] = fr[FR_FILE] */
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31744 = (int)*(((s1_ptr)_2)->base + _sym_63055);
    _2 = (int)SEQ_PTR(_31744);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _31745 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _31745 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _31744 = NOVALUE;
    _2 = (int)SEQ_PTR(_fr_63052);
    _31746 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_31745) && IS_ATOM_INT(_31746)) {
        _31747 = (_31745 == _31746);
    }
    else {
        _31747 = binary_op(EQUALS, _31745, _31746);
    }
    _31745 = NOVALUE;
    _31746 = NOVALUE;
    if (IS_ATOM_INT(_31747)) {
        if (_31747 == 0) {
            goto L1; // [47] 73
        }
    }
    else {
        if (DBL_PTR(_31747)->dbl == 0.0) {
            goto L1; // [47] 73
        }
    }
    _2 = (int)SEQ_PTR(_fr_63052);
    _31749 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_31749)) {
        _31750 = (_31749 == _25TopLevelSub_12269);
    }
    else {
        _31750 = binary_op(EQUALS, _31749, _25TopLevelSub_12269);
    }
    _31749 = NOVALUE;
    if (_31750 == 0) {
        DeRef(_31750);
        _31750 = NOVALUE;
        goto L1; // [64] 73
    }
    else {
        if (!IS_ATOM_INT(_31750) && DBL_PTR(_31750)->dbl == 0.0){
            DeRef(_31750);
            _31750 = NOVALUE;
            goto L1; // [64] 73
        }
        DeRef(_31750);
        _31750 = NOVALUE;
    }
    DeRef(_31750);
    _31750 = NOVALUE;

    /** 		return*/
    DeRef(_tok_63050);
    DeRef(_fr_63052);
    DeRef(_31747);
    _31747 = NOVALUE;
    return;
L1: 

    /** 	if fr[FR_OP] = ASSIGN and SymTab[sym][S_MODE] = M_CONSTANT then*/
    _2 = (int)SEQ_PTR(_fr_63052);
    _31751 = (int)*(((s1_ptr)_2)->base + 10);
    if (IS_ATOM_INT(_31751)) {
        _31752 = (_31751 == 18);
    }
    else {
        _31752 = binary_op(EQUALS, _31751, 18);
    }
    _31751 = NOVALUE;
    if (IS_ATOM_INT(_31752)) {
        if (_31752 == 0) {
            goto L2; // [87] 126
        }
    }
    else {
        if (DBL_PTR(_31752)->dbl == 0.0) {
            goto L2; // [87] 126
        }
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31754 = (int)*(((s1_ptr)_2)->base + _sym_63055);
    _2 = (int)SEQ_PTR(_31754);
    _31755 = (int)*(((s1_ptr)_2)->base + 3);
    _31754 = NOVALUE;
    if (IS_ATOM_INT(_31755)) {
        _31756 = (_31755 == 2);
    }
    else {
        _31756 = binary_op(EQUALS, _31755, 2);
    }
    _31755 = NOVALUE;
    if (_31756 == 0) {
        DeRef(_31756);
        _31756 = NOVALUE;
        goto L2; // [110] 126
    }
    else {
        if (!IS_ATOM_INT(_31756) && DBL_PTR(_31756)->dbl == 0.0){
            DeRef(_31756);
            _31756 = NOVALUE;
            goto L2; // [110] 126
        }
        DeRef(_31756);
        _31756 = NOVALUE;
    }
    DeRef(_31756);
    _31756 = NOVALUE;

    /** 		prep_forward_error( ref )*/
    _29prep_forward_error(_ref_63051);

    /** 		CompileErr( 110 )*/
    RefDS(_22682);
    _43CompileErr(110, _22682, 0);
L2: 

    /** 	if fr[FR_OP] = ASSIGN then*/
    _2 = (int)SEQ_PTR(_fr_63052);
    _31757 = (int)*(((s1_ptr)_2)->base + 10);
    if (binary_op_a(NOTEQ, _31757, 18)){
        _31757 = NOVALUE;
        goto L3; // [136] 176
    }
    _31757 = NOVALUE;

    /** 		SymTab[sym][S_USAGE] = or_bits( U_WRITTEN, SymTab[sym][S_USAGE] )*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_63055 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31761 = (int)*(((s1_ptr)_2)->base + _sym_63055);
    _2 = (int)SEQ_PTR(_31761);
    _31762 = (int)*(((s1_ptr)_2)->base + 5);
    _31761 = NOVALUE;
    if (IS_ATOM_INT(_31762)) {
        {unsigned long tu;
             tu = (unsigned long)2 | (unsigned long)_31762;
             _31763 = MAKE_UINT(tu);
        }
    }
    else {
        _31763 = binary_op(OR_BITS, 2, _31762);
    }
    _31762 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _31763;
    if( _1 != _31763 ){
        DeRef(_1);
    }
    _31763 = NOVALUE;
    _31759 = NOVALUE;
    goto L4; // [173] 210
L3: 

    /** 		SymTab[sym][S_USAGE] = or_bits( U_READ, SymTab[sym][S_USAGE] )*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_63055 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31766 = (int)*(((s1_ptr)_2)->base + _sym_63055);
    _2 = (int)SEQ_PTR(_31766);
    _31767 = (int)*(((s1_ptr)_2)->base + 5);
    _31766 = NOVALUE;
    if (IS_ATOM_INT(_31767)) {
        {unsigned long tu;
             tu = (unsigned long)1 | (unsigned long)_31767;
             _31768 = MAKE_UINT(tu);
        }
    }
    else {
        _31768 = binary_op(OR_BITS, 1, _31767);
    }
    _31767 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _31768;
    if( _1 != _31768 ){
        DeRef(_1);
    }
    _31768 = NOVALUE;
    _31764 = NOVALUE;
L4: 

    /** 	set_code( ref )*/
    _29set_code(_ref_63051);

    /** 	integer pc = fr[FR_PC]*/
    _2 = (int)SEQ_PTR(_fr_63052);
    _pc_63107 = (int)*(((s1_ptr)_2)->base + 5);
    if (!IS_ATOM_INT(_pc_63107))
    _pc_63107 = (long)DBL_PTR(_pc_63107)->dbl;

    /** 	if pc < 1 then*/
    if (_pc_63107 >= 1)
    goto L5; // [225] 235

    /** 		pc = 1*/
    _pc_63107 = 1;
L5: 

    /** 	integer vx = find( -ref, Code, pc )*/
    if ((unsigned long)_ref_63051 == 0xC0000000)
    _31771 = (int)NewDouble((double)-0xC0000000);
    else
    _31771 = - _ref_63051;
    _vx_63111 = find_from(_31771, _25Code_12355, _pc_63107);
    DeRef(_31771);
    _31771 = NOVALUE;

    /** 	if vx then*/
    if (_vx_63111 == 0)
    {
        goto L6; // [249] 291
    }
    else{
    }

    /** 		while vx do*/
L7: 
    if (_vx_63111 == 0)
    {
        goto L8; // [257] 285
    }
    else{
    }

    /** 			Code[vx] = sym*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _25Code_12355 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _vx_63111);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_63055;
    DeRef(_1);

    /** 			vx = find( -ref, Code, vx )*/
    if ((unsigned long)_ref_63051 == 0xC0000000)
    _31773 = (int)NewDouble((double)-0xC0000000);
    else
    _31773 = - _ref_63051;
    _vx_63111 = find_from(_31773, _25Code_12355, _vx_63111);
    DeRef(_31773);
    _31773 = NOVALUE;

    /** 		end while*/
    goto L7; // [282] 257
L8: 

    /** 		resolved_reference( ref )*/
    _29resolved_reference(_ref_63051);
L6: 

    /** 	if sequence( fr[FR_DATA] ) then*/
    _2 = (int)SEQ_PTR(_fr_63052);
    _31775 = (int)*(((s1_ptr)_2)->base + 12);
    _31776 = IS_SEQUENCE(_31775);
    _31775 = NOVALUE;
    if (_31776 == 0)
    {
        _31776 = NOVALUE;
        goto L9; // [302] 438
    }
    else{
        _31776 = NOVALUE;
    }

    /** 		for i = 1 to length( fr[FR_DATA] ) do*/
    _2 = (int)SEQ_PTR(_fr_63052);
    _31777 = (int)*(((s1_ptr)_2)->base + 12);
    if (IS_SEQUENCE(_31777)){
            _31778 = SEQ_PTR(_31777)->length;
    }
    else {
        _31778 = 1;
    }
    _31777 = NOVALUE;
    {
        int _i_63125;
        _i_63125 = 1;
LA: 
        if (_i_63125 > _31778){
            goto LB; // [316] 432
        }

        /** 			object d = fr[FR_DATA][i]*/
        _2 = (int)SEQ_PTR(_fr_63052);
        _31779 = (int)*(((s1_ptr)_2)->base + 12);
        DeRef(_d_63128);
        _2 = (int)SEQ_PTR(_31779);
        _d_63128 = (int)*(((s1_ptr)_2)->base + _i_63125);
        Ref(_d_63128);
        _31779 = NOVALUE;

        /** 			if sequence( d ) and d[1] = PAM_RECORD then*/
        _31781 = IS_SEQUENCE(_d_63128);
        if (_31781 == 0) {
            goto LC; // [340] 421
        }
        _2 = (int)SEQ_PTR(_d_63128);
        _31783 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_31783)) {
            _31784 = (_31783 == 1);
        }
        else {
            _31784 = binary_op(EQUALS, _31783, 1);
        }
        _31783 = NOVALUE;
        if (_31784 == 0) {
            DeRef(_31784);
            _31784 = NOVALUE;
            goto LC; // [355] 421
        }
        else {
            if (!IS_ATOM_INT(_31784) && DBL_PTR(_31784)->dbl == 0.0){
                DeRef(_31784);
                _31784 = NOVALUE;
                goto LC; // [355] 421
            }
            DeRef(_31784);
            _31784 = NOVALUE;
        }
        DeRef(_31784);
        _31784 = NOVALUE;

        /** 				symtab_index param = d[2]*/
        _2 = (int)SEQ_PTR(_d_63128);
        _param_63138 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_param_63138)){
            _param_63138 = (long)DBL_PTR(_param_63138)->dbl;
        }

        /** 				token old = {RECORDED, d[3]}*/
        _2 = (int)SEQ_PTR(_d_63128);
        _31786 = (int)*(((s1_ptr)_2)->base + 3);
        Ref(_31786);
        DeRef(_old_63141);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = 508;
        ((int *)_2)[2] = _31786;
        _old_63141 = MAKE_SEQ(_1);
        _31786 = NOVALUE;

        /** 				token new = {VARIABLE, sym}*/
        DeRefi(_new_63146);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = -100;
        ((int *)_2)[2] = _sym_63055;
        _new_63146 = MAKE_SEQ(_1);

        /** 				SymTab[param][S_CODE] = find_replace( old, SymTab[param][S_CODE], new )*/
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        _3 = (int)(_param_63138 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        _31791 = (int)*(((s1_ptr)_2)->base + _param_63138);
        _2 = (int)SEQ_PTR(_31791);
        if (!IS_ATOM_INT(_25S_CODE_11925)){
            _31792 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
        }
        else{
            _31792 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
        }
        _31791 = NOVALUE;
        RefDS(_old_63141);
        Ref(_31792);
        RefDS(_new_63146);
        _31793 = _7find_replace(_old_63141, _31792, _new_63146, 0);
        _31792 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_25S_CODE_11925))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _25S_CODE_11925);
        _1 = *(int *)_2;
        *(int *)_2 = _31793;
        if( _1 != _31793 ){
            DeRef(_1);
        }
        _31793 = NOVALUE;
        _31789 = NOVALUE;
LC: 
        DeRef(_old_63141);
        _old_63141 = NOVALUE;
        DeRefi(_new_63146);
        _new_63146 = NOVALUE;
        DeRef(_d_63128);
        _d_63128 = NOVALUE;

        /** 		end for*/
        _i_63125 = _i_63125 + 1;
        goto LA; // [427] 323
LB: 
        ;
    }

    /** 		resolved_reference( ref )*/
    _29resolved_reference(_ref_63051);
L9: 

    /** 	reset_code()*/
    _29reset_code();

    /** end procedure*/
    DeRef(_tok_63050);
    DeRef(_fr_63052);
    _31777 = NOVALUE;
    DeRef(_31752);
    _31752 = NOVALUE;
    DeRef(_31747);
    _31747 = NOVALUE;
    return;
    ;
}


void _29patch_forward_init_check(int _tok_63162, int _ref_63163)
{
    int _fr_63164 = NOVALUE;
    int _31801 = NOVALUE;
    int _31800 = NOVALUE;
    int _31799 = NOVALUE;
    int _31797 = NOVALUE;
    int _31796 = NOVALUE;
    int _31795 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence fr = forward_references[ref]*/
    DeRef(_fr_63164);
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    _fr_63164 = (int)*(((s1_ptr)_2)->base + _ref_63163);
    Ref(_fr_63164);

    /** 	set_code( ref )*/
    _29set_code(_ref_63163);

    /** 	if sequence( fr[FR_DATA] ) then*/
    _2 = (int)SEQ_PTR(_fr_63164);
    _31795 = (int)*(((s1_ptr)_2)->base + 12);
    _31796 = IS_SEQUENCE(_31795);
    _31795 = NOVALUE;
    if (_31796 == 0)
    {
        _31796 = NOVALUE;
        goto L1; // [29] 40
    }
    else{
        _31796 = NOVALUE;
    }

    /** 		resolved_reference( ref )*/
    _29resolved_reference(_ref_63163);
    goto L2; // [37] 91
L1: 

    /** 	elsif fr[FR_PC] > 0 then*/
    _2 = (int)SEQ_PTR(_fr_63164);
    _31797 = (int)*(((s1_ptr)_2)->base + 5);
    if (binary_op_a(LESSEQ, _31797, 0)){
        _31797 = NOVALUE;
        goto L3; // [48] 84
    }
    _31797 = NOVALUE;

    /** 		Code[fr[FR_PC]+1] = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_fr_63164);
    _31799 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_ATOM_INT(_31799)) {
        _31800 = _31799 + 1;
        if (_31800 > MAXINT){
            _31800 = NewDouble((double)_31800);
        }
    }
    else
    _31800 = binary_op(PLUS, 1, _31799);
    _31799 = NOVALUE;
    _2 = (int)SEQ_PTR(_tok_63162);
    _31801 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_31801);
    _2 = (int)SEQ_PTR(_25Code_12355);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _25Code_12355 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_31800))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_31800)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _31800);
    _1 = *(int *)_2;
    *(int *)_2 = _31801;
    if( _1 != _31801 ){
        DeRef(_1);
    }
    _31801 = NOVALUE;

    /** 		resolved_reference( ref )*/
    _29resolved_reference(_ref_63163);
    goto L2; // [81] 91
L3: 

    /** 		forward_error( tok, ref )*/
    Ref(_tok_63162);
    _29forward_error(_tok_63162, _ref_63163);
L2: 

    /** 	reset_code()*/
    _29reset_code();

    /** end procedure*/
    DeRef(_tok_63162);
    DeRef(_fr_63164);
    DeRef(_31800);
    _31800 = NOVALUE;
    return;
    ;
}


int _29expected_name(int _id_63181)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_id_63181)) {
        _1 = (long)(DBL_PTR(_id_63181)->dbl);
        if (UNIQUE(DBL_PTR(_id_63181)) && (DBL_PTR(_id_63181)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_id_63181);
        _id_63181 = _1;
    }

    /** 	switch id with fallthru do*/
    _0 = _id_63181;
    switch ( _0 ){ 

        /** 		case PROC then*/
        case 27:
        case 195:

        /** 			return "a procedure"*/
        RefDS(_27103);
        return _27103;

        /** 		case FUNC then*/
        case 501:
        case 196:

        /** 			return "a function"*/
        RefDS(_27049);
        return _27049;

        /** 		case VARIABLE then*/
        case -100:

        /** 			return "a variable, constant or enum"*/
        RefDS(_31804);
        return _31804;

        /** 		case else*/
        default:

        /** 			return "something"*/
        RefDS(_31805);
        return _31805;
    ;}    ;
}


void _29patch_forward_type(int _tok_63198, int _ref_63199)
{
    int _fr_63200 = NOVALUE;
    int _syms_63202 = NOVALUE;
    int _31817 = NOVALUE;
    int _31816 = NOVALUE;
    int _31814 = NOVALUE;
    int _31813 = NOVALUE;
    int _31812 = NOVALUE;
    int _31810 = NOVALUE;
    int _31809 = NOVALUE;
    int _31808 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	sequence fr = forward_references[ref]*/
    DeRef(_fr_63200);
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    _fr_63200 = (int)*(((s1_ptr)_2)->base + _ref_63199);
    Ref(_fr_63200);

    /** 	sequence syms = fr[FR_DATA]*/
    DeRef(_syms_63202);
    _2 = (int)SEQ_PTR(_fr_63200);
    _syms_63202 = (int)*(((s1_ptr)_2)->base + 12);
    Ref(_syms_63202);

    /** 	for i = 2 to length( syms ) do*/
    if (IS_SEQUENCE(_syms_63202)){
            _31808 = SEQ_PTR(_syms_63202)->length;
    }
    else {
        _31808 = 1;
    }
    {
        int _i_63205;
        _i_63205 = 2;
L1: 
        if (_i_63205 > _31808){
            goto L2; // [28] 104
        }

        /** 		SymTab[syms[i]][S_VTYPE] = tok[T_SYM]*/
        _2 = (int)SEQ_PTR(_syms_63202);
        _31809 = (int)*(((s1_ptr)_2)->base + _i_63205);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_31809))
        _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_31809)->dbl));
        else
        _3 = (int)(_31809 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_tok_63198);
        _31812 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_31812);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 15);
        _1 = *(int *)_2;
        *(int *)_2 = _31812;
        if( _1 != _31812 ){
            DeRef(_1);
        }
        _31812 = NOVALUE;
        _31810 = NOVALUE;

        /** 		if TRANSLATE then*/
        if (_25TRANSLATE_11874 == 0)
        {
            goto L3; // [64] 97
        }
        else{
        }

        /** 			SymTab[syms[i]][S_GTYPE] = CompileType(tok[T_SYM])*/
        _2 = (int)SEQ_PTR(_syms_63202);
        _31813 = (int)*(((s1_ptr)_2)->base + _i_63205);
        _2 = (int)SEQ_PTR(_26SymTab_11138);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26SymTab_11138 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_31813))
        _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_31813)->dbl));
        else
        _3 = (int)(_31813 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_tok_63198);
        _31816 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_31816);
        _31817 = _30CompileType(_31816);
        _31816 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 36);
        _1 = *(int *)_2;
        *(int *)_2 = _31817;
        if( _1 != _31817 ){
            DeRef(_1);
        }
        _31817 = NOVALUE;
        _31814 = NOVALUE;
L3: 

        /** 	end for*/
        _i_63205 = _i_63205 + 1;
        goto L1; // [99] 35
L2: 
        ;
    }

    /** 	resolved_reference( ref )*/
    _29resolved_reference(_ref_63199);

    /** end procedure*/
    DeRef(_tok_63198);
    DeRef(_fr_63200);
    DeRef(_syms_63202);
    _31809 = NOVALUE;
    _31813 = NOVALUE;
    return;
    ;
}


void _29patch_forward_case(int _tok_63228, int _ref_63229)
{
    int _fr_63230 = NOVALUE;
    int _switch_pc_63232 = NOVALUE;
    int _case_sym_63235 = NOVALUE;
    int _case_values_63264 = NOVALUE;
    int _cx_63269 = NOVALUE;
    int _negative_63277 = NOVALUE;
    int _31855 = NOVALUE;
    int _31854 = NOVALUE;
    int _31853 = NOVALUE;
    int _31852 = NOVALUE;
    int _31851 = NOVALUE;
    int _31850 = NOVALUE;
    int _31848 = NOVALUE;
    int _31846 = NOVALUE;
    int _31845 = NOVALUE;
    int _31843 = NOVALUE;
    int _31842 = NOVALUE;
    int _31839 = NOVALUE;
    int _31837 = NOVALUE;
    int _31836 = NOVALUE;
    int _31835 = NOVALUE;
    int _31834 = NOVALUE;
    int _31833 = NOVALUE;
    int _31832 = NOVALUE;
    int _31831 = NOVALUE;
    int _31830 = NOVALUE;
    int _31829 = NOVALUE;
    int _31827 = NOVALUE;
    int _31826 = NOVALUE;
    int _31825 = NOVALUE;
    int _31824 = NOVALUE;
    int _31822 = NOVALUE;
    int _31820 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	sequence fr = forward_references[ref]*/
    DeRef(_fr_63230);
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    _fr_63230 = (int)*(((s1_ptr)_2)->base + _ref_63229);
    Ref(_fr_63230);

    /** 	integer switch_pc = fr[FR_DATA]*/
    _2 = (int)SEQ_PTR(_fr_63230);
    _switch_pc_63232 = (int)*(((s1_ptr)_2)->base + 12);
    if (!IS_ATOM_INT(_switch_pc_63232))
    _switch_pc_63232 = (long)DBL_PTR(_switch_pc_63232)->dbl;

    /** 	if fr[FR_SUBPROG] = TopLevelSub then*/
    _2 = (int)SEQ_PTR(_fr_63230);
    _31820 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _31820, _25TopLevelSub_12269)){
        _31820 = NOVALUE;
        goto L1; // [31] 52
    }
    _31820 = NOVALUE;

    /** 		case_sym = Code[switch_pc + 2]*/
    _31822 = _switch_pc_63232 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _case_sym_63235 = (int)*(((s1_ptr)_2)->base + _31822);
    if (!IS_ATOM_INT(_case_sym_63235)){
        _case_sym_63235 = (long)DBL_PTR(_case_sym_63235)->dbl;
    }
    goto L2; // [49] 83
L1: 

    /** 		case_sym = SymTab[fr[FR_SUBPROG]][S_CODE][switch_pc + 2]*/
    _2 = (int)SEQ_PTR(_fr_63230);
    _31824 = (int)*(((s1_ptr)_2)->base + 4);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_31824)){
        _31825 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31824)->dbl));
    }
    else{
        _31825 = (int)*(((s1_ptr)_2)->base + _31824);
    }
    _2 = (int)SEQ_PTR(_31825);
    if (!IS_ATOM_INT(_25S_CODE_11925)){
        _31826 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    }
    else{
        _31826 = (int)*(((s1_ptr)_2)->base + _25S_CODE_11925);
    }
    _31825 = NOVALUE;
    _31827 = _switch_pc_63232 + 2;
    _2 = (int)SEQ_PTR(_31826);
    _case_sym_63235 = (int)*(((s1_ptr)_2)->base + _31827);
    if (!IS_ATOM_INT(_case_sym_63235)){
        _case_sym_63235 = (long)DBL_PTR(_case_sym_63235)->dbl;
    }
    _31826 = NOVALUE;
L2: 

    /** 	if SymTab[tok[T_SYM]][S_FILE_NO] = fr[FR_FILE] and fr[FR_SUBPROG] = TopLevelSub then*/
    _2 = (int)SEQ_PTR(_tok_63228);
    _31829 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_31829)){
        _31830 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31829)->dbl));
    }
    else{
        _31830 = (int)*(((s1_ptr)_2)->base + _31829);
    }
    _2 = (int)SEQ_PTR(_31830);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _31831 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _31831 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    _31830 = NOVALUE;
    _2 = (int)SEQ_PTR(_fr_63230);
    _31832 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_31831) && IS_ATOM_INT(_31832)) {
        _31833 = (_31831 == _31832);
    }
    else {
        _31833 = binary_op(EQUALS, _31831, _31832);
    }
    _31831 = NOVALUE;
    _31832 = NOVALUE;
    if (IS_ATOM_INT(_31833)) {
        if (_31833 == 0) {
            goto L3; // [113] 139
        }
    }
    else {
        if (DBL_PTR(_31833)->dbl == 0.0) {
            goto L3; // [113] 139
        }
    }
    _2 = (int)SEQ_PTR(_fr_63230);
    _31835 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_31835)) {
        _31836 = (_31835 == _25TopLevelSub_12269);
    }
    else {
        _31836 = binary_op(EQUALS, _31835, _25TopLevelSub_12269);
    }
    _31835 = NOVALUE;
    if (_31836 == 0) {
        DeRef(_31836);
        _31836 = NOVALUE;
        goto L3; // [130] 139
    }
    else {
        if (!IS_ATOM_INT(_31836) && DBL_PTR(_31836)->dbl == 0.0){
            DeRef(_31836);
            _31836 = NOVALUE;
            goto L3; // [130] 139
        }
        DeRef(_31836);
        _31836 = NOVALUE;
    }
    DeRef(_31836);
    _31836 = NOVALUE;

    /** 		return*/
    DeRef(_tok_63228);
    DeRef(_fr_63230);
    DeRef(_case_values_63264);
    DeRef(_31822);
    _31822 = NOVALUE;
    _31824 = NOVALUE;
    _31829 = NOVALUE;
    DeRef(_31827);
    _31827 = NOVALUE;
    DeRef(_31833);
    _31833 = NOVALUE;
    return;
L3: 

    /** 	sequence case_values = SymTab[case_sym][S_OBJ]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31837 = (int)*(((s1_ptr)_2)->base + _case_sym_63235);
    DeRef(_case_values_63264);
    _2 = (int)SEQ_PTR(_31837);
    _case_values_63264 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_case_values_63264);
    _31837 = NOVALUE;

    /** 	integer cx = find( { ref }, case_values )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _ref_63229;
    _31839 = MAKE_SEQ(_1);
    _cx_63269 = find_from(_31839, _case_values_63264, 1);
    DeRefDS(_31839);
    _31839 = NOVALUE;

    /** 	if not cx then*/
    if (_cx_63269 != 0)
    goto L4; // [170] 188

    /** 		cx = find( { -ref }, case_values )*/
    if ((unsigned long)_ref_63229 == 0xC0000000)
    _31842 = (int)NewDouble((double)-0xC0000000);
    else
    _31842 = - _ref_63229;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _31842;
    _31843 = MAKE_SEQ(_1);
    _31842 = NOVALUE;
    _cx_63269 = find_from(_31843, _case_values_63264, 1);
    DeRefDS(_31843);
    _31843 = NOVALUE;
L4: 

    /**  	ifdef DEBUG then	*/

    /** 	integer negative = 0*/
    _negative_63277 = 0;

    /** 	if case_values[cx][1] < 0 then*/
    _2 = (int)SEQ_PTR(_case_values_63264);
    _31845 = (int)*(((s1_ptr)_2)->base + _cx_63269);
    _2 = (int)SEQ_PTR(_31845);
    _31846 = (int)*(((s1_ptr)_2)->base + 1);
    _31845 = NOVALUE;
    if (binary_op_a(GREATEREQ, _31846, 0)){
        _31846 = NOVALUE;
        goto L5; // [205] 234
    }
    _31846 = NOVALUE;

    /** 		negative = 1*/
    _negative_63277 = 1;

    /** 		case_values[cx][1] *= -1*/
    _2 = (int)SEQ_PTR(_case_values_63264);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _case_values_63264 = MAKE_SEQ(_2);
    }
    _3 = (int)(_cx_63269 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _31850 = (int)*(((s1_ptr)_2)->base + 1);
    _31848 = NOVALUE;
    if (IS_ATOM_INT(_31850)) {
        if (_31850 == (short)_31850)
        _31851 = _31850 * -1;
        else
        _31851 = NewDouble(_31850 * (double)-1);
    }
    else {
        _31851 = binary_op(MULTIPLY, _31850, -1);
    }
    _31850 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _31851;
    if( _1 != _31851 ){
        DeRef(_1);
    }
    _31851 = NOVALUE;
    _31848 = NOVALUE;
L5: 

    /** 	if negative then*/
    if (_negative_63277 == 0)
    {
        goto L6; // [236] 257
    }
    else{
    }

    /** 		case_values[cx] = - tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_63228);
    _31852 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_31852)) {
        if ((unsigned long)_31852 == 0xC0000000)
        _31853 = (int)NewDouble((double)-0xC0000000);
        else
        _31853 = - _31852;
    }
    else {
        _31853 = unary_op(UMINUS, _31852);
    }
    _31852 = NOVALUE;
    _2 = (int)SEQ_PTR(_case_values_63264);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _case_values_63264 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _cx_63269);
    _1 = *(int *)_2;
    *(int *)_2 = _31853;
    if( _1 != _31853 ){
        DeRef(_1);
    }
    _31853 = NOVALUE;
    goto L7; // [254] 270
L6: 

    /** 		case_values[cx] = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_63228);
    _31854 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_31854);
    _2 = (int)SEQ_PTR(_case_values_63264);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _case_values_63264 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _cx_63269);
    _1 = *(int *)_2;
    *(int *)_2 = _31854;
    if( _1 != _31854 ){
        DeRef(_1);
    }
    _31854 = NOVALUE;
L7: 

    /** 	SymTab[case_sym][S_OBJ] = case_values*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_case_sym_63235 + ((s1_ptr)_2)->base);
    RefDS(_case_values_63264);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _case_values_63264;
    DeRef(_1);
    _31855 = NOVALUE;

    /** 	resolved_reference( ref )*/
    _29resolved_reference(_ref_63229);

    /** end procedure*/
    DeRef(_tok_63228);
    DeRef(_fr_63230);
    DeRefDS(_case_values_63264);
    DeRef(_31822);
    _31822 = NOVALUE;
    _31824 = NOVALUE;
    _31829 = NOVALUE;
    DeRef(_31827);
    _31827 = NOVALUE;
    DeRef(_31833);
    _31833 = NOVALUE;
    return;
    ;
}


void _29patch_forward_type_check(int _tok_63300, int _ref_63301)
{
    int _fr_63302 = NOVALUE;
    int _which_type_63305 = NOVALUE;
    int _var_63307 = NOVALUE;
    int _pc_63340 = NOVALUE;
    int _with_type_check_63342 = NOVALUE;
    int _c_63372 = NOVALUE;
    int _subprog_inlined_insert_code_at_344_63381 = NOVALUE;
    int _code_inlined_insert_code_at_341_63380 = NOVALUE;
    int _subprog_inlined_insert_code_at_429_63397 = NOVALUE;
    int _code_inlined_insert_code_at_426_63396 = NOVALUE;
    int _subprog_inlined_insert_code_at_493_63407 = NOVALUE;
    int _code_inlined_insert_code_at_490_63406 = NOVALUE;
    int _subprog_inlined_insert_code_at_557_63417 = NOVALUE;
    int _code_inlined_insert_code_at_554_63416 = NOVALUE;
    int _start_pc_63424 = NOVALUE;
    int _subprog_inlined_insert_code_at_667_63441 = NOVALUE;
    int _code_inlined_insert_code_at_664_63440 = NOVALUE;
    int _c_63444 = NOVALUE;
    int _subprog_inlined_insert_code_at_765_63460 = NOVALUE;
    int _code_inlined_insert_code_at_762_63459 = NOVALUE;
    int _start_pc_63471 = NOVALUE;
    int _subprog_inlined_insert_code_at_912_63491 = NOVALUE;
    int _code_inlined_insert_code_at_909_63490 = NOVALUE;
    int _subprog_inlined_insert_code_at_1015_63512 = NOVALUE;
    int _code_inlined_insert_code_at_1012_63511 = NOVALUE;
    int _31945 = NOVALUE;
    int _31944 = NOVALUE;
    int _31943 = NOVALUE;
    int _31942 = NOVALUE;
    int _31941 = NOVALUE;
    int _31940 = NOVALUE;
    int _31939 = NOVALUE;
    int _31937 = NOVALUE;
    int _31935 = NOVALUE;
    int _31934 = NOVALUE;
    int _31933 = NOVALUE;
    int _31932 = NOVALUE;
    int _31931 = NOVALUE;
    int _31930 = NOVALUE;
    int _31929 = NOVALUE;
    int _31927 = NOVALUE;
    int _31926 = NOVALUE;
    int _31925 = NOVALUE;
    int _31924 = NOVALUE;
    int _31923 = NOVALUE;
    int _31922 = NOVALUE;
    int _31920 = NOVALUE;
    int _31919 = NOVALUE;
    int _31918 = NOVALUE;
    int _31917 = NOVALUE;
    int _31915 = NOVALUE;
    int _31914 = NOVALUE;
    int _31911 = NOVALUE;
    int _31910 = NOVALUE;
    int _31908 = NOVALUE;
    int _31907 = NOVALUE;
    int _31906 = NOVALUE;
    int _31905 = NOVALUE;
    int _31904 = NOVALUE;
    int _31903 = NOVALUE;
    int _31901 = NOVALUE;
    int _31900 = NOVALUE;
    int _31897 = NOVALUE;
    int _31896 = NOVALUE;
    int _31893 = NOVALUE;
    int _31892 = NOVALUE;
    int _31888 = NOVALUE;
    int _31887 = NOVALUE;
    int _31885 = NOVALUE;
    int _31884 = NOVALUE;
    int _31882 = NOVALUE;
    int _31881 = NOVALUE;
    int _31878 = NOVALUE;
    int _31875 = NOVALUE;
    int _31873 = NOVALUE;
    int _31870 = NOVALUE;
    int _31869 = NOVALUE;
    int _31866 = NOVALUE;
    int _31861 = NOVALUE;
    int _31860 = NOVALUE;
    int _31858 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	sequence fr = forward_references[ref]*/
    DeRef(_fr_63302);
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    _fr_63302 = (int)*(((s1_ptr)_2)->base + _ref_63301);
    Ref(_fr_63302);

    /** 	if fr[FR_OP] = TYPE_CHECK_FORWARD then*/
    _2 = (int)SEQ_PTR(_fr_63302);
    _31858 = (int)*(((s1_ptr)_2)->base + 10);
    if (binary_op_a(NOTEQ, _31858, 197)){
        _31858 = NOVALUE;
        goto L1; // [23] 88
    }
    _31858 = NOVALUE;

    /** 		which_type = SymTab[tok[T_SYM]][S_VTYPE]*/
    _2 = (int)SEQ_PTR(_tok_63300);
    _31860 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_31860)){
        _31861 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31860)->dbl));
    }
    else{
        _31861 = (int)*(((s1_ptr)_2)->base + _31860);
    }
    _2 = (int)SEQ_PTR(_31861);
    _which_type_63305 = (int)*(((s1_ptr)_2)->base + 15);
    if (!IS_ATOM_INT(_which_type_63305)){
        _which_type_63305 = (long)DBL_PTR(_which_type_63305)->dbl;
    }
    _31861 = NOVALUE;

    /** 		if not which_type then*/
    if (_which_type_63305 != 0)
    goto L2; // [51] 74

    /** 			which_type = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_63300);
    _which_type_63305 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_which_type_63305)){
        _which_type_63305 = (long)DBL_PTR(_which_type_63305)->dbl;
    }

    /** 			var = 0*/
    _var_63307 = 0;
    goto L3; // [71] 150
L2: 

    /** 			var = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_63300);
    _var_63307 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_var_63307)){
        _var_63307 = (long)DBL_PTR(_var_63307)->dbl;
    }
    goto L3; // [85] 150
L1: 

    /** 	elsif fr[FR_OP] = TYPE then*/
    _2 = (int)SEQ_PTR(_fr_63302);
    _31866 = (int)*(((s1_ptr)_2)->base + 10);
    if (binary_op_a(NOTEQ, _31866, 504)){
        _31866 = NOVALUE;
        goto L4; // [98] 122
    }
    _31866 = NOVALUE;

    /** 		which_type = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_63300);
    _which_type_63305 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_which_type_63305)){
        _which_type_63305 = (long)DBL_PTR(_which_type_63305)->dbl;
    }

    /** 		var = 0*/
    _var_63307 = 0;
    goto L3; // [119] 150
L4: 

    /** 		prep_forward_error( ref )*/
    _29prep_forward_error(_ref_63301);

    /** 		InternalErr( 262, { TYPE_CHECK, TYPE_CHECK_FORWARD, fr[FR_OP] })*/
    _2 = (int)SEQ_PTR(_fr_63302);
    _31869 = (int)*(((s1_ptr)_2)->base + 10);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 65;
    *((int *)(_2+8)) = 197;
    Ref(_31869);
    *((int *)(_2+12)) = _31869;
    _31870 = MAKE_SEQ(_1);
    _31869 = NOVALUE;
    _43InternalErr(262, _31870);
    _31870 = NOVALUE;
L3: 

    /** 	if which_type < 0 then*/
    if (_which_type_63305 >= 0)
    goto L5; // [154] 164

    /** 		return*/
    DeRef(_tok_63300);
    DeRef(_fr_63302);
    _31860 = NOVALUE;
    return;
L5: 

    /** 	set_code( ref )*/
    _29set_code(_ref_63301);

    /** 	integer pc = fr[FR_PC]*/
    _2 = (int)SEQ_PTR(_fr_63302);
    _pc_63340 = (int)*(((s1_ptr)_2)->base + 5);
    if (!IS_ATOM_INT(_pc_63340))
    _pc_63340 = (long)DBL_PTR(_pc_63340)->dbl;

    /** 	integer with_type_check = Code[pc + 2]*/
    _31873 = _pc_63340 + 2;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _with_type_check_63342 = (int)*(((s1_ptr)_2)->base + _31873);
    if (!IS_ATOM_INT(_with_type_check_63342)){
        _with_type_check_63342 = (long)DBL_PTR(_with_type_check_63342)->dbl;
    }

    /** 	if Code[pc] != TYPE_CHECK_FORWARD then*/
    _2 = (int)SEQ_PTR(_25Code_12355);
    _31875 = (int)*(((s1_ptr)_2)->base + _pc_63340);
    if (binary_op_a(EQUALS, _31875, 197)){
        _31875 = NOVALUE;
        goto L6; // [201] 212
    }
    _31875 = NOVALUE;

    /** 		forward_error( tok, ref )*/
    Ref(_tok_63300);
    _29forward_error(_tok_63300, _ref_63301);
L6: 

    /** 	if not var then*/
    if (_var_63307 != 0)
    goto L7; // [216] 234

    /** 		var = Code[pc+1]*/
    _31878 = _pc_63340 + 1;
    _2 = (int)SEQ_PTR(_25Code_12355);
    _var_63307 = (int)*(((s1_ptr)_2)->base + _31878);
    if (!IS_ATOM_INT(_var_63307)){
        _var_63307 = (long)DBL_PTR(_var_63307)->dbl;
    }
L7: 

    /** 	if var < 0 then*/
    if (_var_63307 >= 0)
    goto L8; // [236] 246

    /** 		return*/
    DeRef(_tok_63300);
    DeRef(_fr_63302);
    _31860 = NOVALUE;
    DeRef(_31873);
    _31873 = NOVALUE;
    DeRef(_31878);
    _31878 = NOVALUE;
    return;
L8: 

    /** 	replace_code( {}, pc, pc + 2, fr[FR_SUBPROG])*/
    _31881 = _pc_63340 + 2;
    if ((long)((unsigned long)_31881 + (unsigned long)HIGH_BITS) >= 0) 
    _31881 = NewDouble((double)_31881);
    _2 = (int)SEQ_PTR(_fr_63302);
    _31882 = (int)*(((s1_ptr)_2)->base + 4);
    RefDS(_22682);
    Ref(_31882);
    _29replace_code(_22682, _pc_63340, _31881, _31882);
    _31881 = NOVALUE;
    _31882 = NOVALUE;

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L9; // [268] 376
    }
    else{
    }

    /** 		if with_type_check then*/
    if (_with_type_check_63342 == 0)
    {
        goto LA; // [273] 795
    }
    else{
    }

    /** 			if which_type != object_type then*/
    if (_which_type_63305 == _52object_type_47099)
    goto LA; // [280] 795

    /** 				if SymTab[which_type][S_EFFECT] then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31884 = (int)*(((s1_ptr)_2)->base + _which_type_63305);
    _2 = (int)SEQ_PTR(_31884);
    _31885 = (int)*(((s1_ptr)_2)->base + 23);
    _31884 = NOVALUE;
    if (_31885 == 0) {
        _31885 = NOVALUE;
        goto LB; // [298] 369
    }
    else {
        if (!IS_ATOM_INT(_31885) && DBL_PTR(_31885)->dbl == 0.0){
            _31885 = NOVALUE;
            goto LB; // [298] 369
        }
        _31885 = NOVALUE;
    }
    _31885 = NOVALUE;

    /** 					integer c = NewTempSym()*/
    _c_63372 = _52NewTempSym(0);
    if (!IS_ATOM_INT(_c_63372)) {
        _1 = (long)(DBL_PTR(_c_63372)->dbl);
        if (UNIQUE(DBL_PTR(_c_63372)) && (DBL_PTR(_c_63372)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_63372);
        _c_63372 = _1;
    }

    /** 					insert_code( { PROC, which_type, var, c, TYPE_CHECK }, pc, fr[FR_SUBPROG] )*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 27;
    *((int *)(_2+8)) = _which_type_63305;
    *((int *)(_2+12)) = _var_63307;
    *((int *)(_2+16)) = _c_63372;
    *((int *)(_2+20)) = 65;
    _31887 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_fr_63302);
    _31888 = (int)*(((s1_ptr)_2)->base + 4);
    DeRefi(_code_inlined_insert_code_at_341_63380);
    _code_inlined_insert_code_at_341_63380 = _31887;
    _31887 = NOVALUE;
    Ref(_31888);
    DeRef(_subprog_inlined_insert_code_at_344_63381);
    _subprog_inlined_insert_code_at_344_63381 = _31888;
    _31888 = NOVALUE;
    if (!IS_ATOM_INT(_subprog_inlined_insert_code_at_344_63381)) {
        _1 = (long)(DBL_PTR(_subprog_inlined_insert_code_at_344_63381)->dbl);
        if (UNIQUE(DBL_PTR(_subprog_inlined_insert_code_at_344_63381)) && (DBL_PTR(_subprog_inlined_insert_code_at_344_63381)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subprog_inlined_insert_code_at_344_63381);
        _subprog_inlined_insert_code_at_344_63381 = _1;
    }

    /** 	shifting_sub = subprog*/
    _29shifting_sub_62575 = _subprog_inlined_insert_code_at_344_63381;

    /** 	shift:insert_code( code, index )*/
    RefDS(_code_inlined_insert_code_at_341_63380);
    _65insert_code(_code_inlined_insert_code_at_341_63380, _pc_63340);

    /** 	shifting_sub = 0*/
    _29shifting_sub_62575 = 0;

    /** end procedure*/
    goto LC; // [357] 360
LC: 
    DeRefi(_code_inlined_insert_code_at_341_63380);
    _code_inlined_insert_code_at_341_63380 = NOVALUE;
    DeRef(_subprog_inlined_insert_code_at_344_63381);
    _subprog_inlined_insert_code_at_344_63381 = NOVALUE;

    /** 					pc += 5*/
    _pc_63340 = _pc_63340 + 5;
LB: 
    goto LA; // [373] 795
L9: 

    /** 		if with_type_check then*/
    if (_with_type_check_63342 == 0)
    {
        goto LD; // [378] 794
    }
    else{
    }

    /** 			if which_type = object_type then*/
    if (_which_type_63305 != _52object_type_47099)
    goto LE; // [385] 392
    goto LF; // [389] 793
LE: 

    /** 				if which_type = integer_type then*/
    if (_which_type_63305 != _52integer_type_47105)
    goto L10; // [396] 456

    /** 					insert_code( { INTEGER_CHECK, var }, pc, fr[FR_SUBPROG] )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 96;
    ((int *)_2)[2] = _var_63307;
    _31892 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_fr_63302);
    _31893 = (int)*(((s1_ptr)_2)->base + 4);
    DeRefi(_code_inlined_insert_code_at_426_63396);
    _code_inlined_insert_code_at_426_63396 = _31892;
    _31892 = NOVALUE;
    Ref(_31893);
    DeRef(_subprog_inlined_insert_code_at_429_63397);
    _subprog_inlined_insert_code_at_429_63397 = _31893;
    _31893 = NOVALUE;
    if (!IS_ATOM_INT(_subprog_inlined_insert_code_at_429_63397)) {
        _1 = (long)(DBL_PTR(_subprog_inlined_insert_code_at_429_63397)->dbl);
        if (UNIQUE(DBL_PTR(_subprog_inlined_insert_code_at_429_63397)) && (DBL_PTR(_subprog_inlined_insert_code_at_429_63397)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subprog_inlined_insert_code_at_429_63397);
        _subprog_inlined_insert_code_at_429_63397 = _1;
    }

    /** 	shifting_sub = subprog*/
    _29shifting_sub_62575 = _subprog_inlined_insert_code_at_429_63397;

    /** 	shift:insert_code( code, index )*/
    RefDS(_code_inlined_insert_code_at_426_63396);
    _65insert_code(_code_inlined_insert_code_at_426_63396, _pc_63340);

    /** 	shifting_sub = 0*/
    _29shifting_sub_62575 = 0;

    /** end procedure*/
    goto L11; // [442] 445
L11: 
    DeRefi(_code_inlined_insert_code_at_426_63396);
    _code_inlined_insert_code_at_426_63396 = NOVALUE;
    DeRef(_subprog_inlined_insert_code_at_429_63397);
    _subprog_inlined_insert_code_at_429_63397 = NOVALUE;

    /** 					pc += 2*/
    _pc_63340 = _pc_63340 + 2;
    goto L12; // [453] 792
L10: 

    /** 				elsif which_type = sequence_type then*/
    if (_which_type_63305 != _52sequence_type_47103)
    goto L13; // [460] 520

    /** 					insert_code( { SEQUENCE_CHECK, var }, pc, fr[FR_SUBPROG])*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 97;
    ((int *)_2)[2] = _var_63307;
    _31896 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_fr_63302);
    _31897 = (int)*(((s1_ptr)_2)->base + 4);
    DeRefi(_code_inlined_insert_code_at_490_63406);
    _code_inlined_insert_code_at_490_63406 = _31896;
    _31896 = NOVALUE;
    Ref(_31897);
    DeRef(_subprog_inlined_insert_code_at_493_63407);
    _subprog_inlined_insert_code_at_493_63407 = _31897;
    _31897 = NOVALUE;
    if (!IS_ATOM_INT(_subprog_inlined_insert_code_at_493_63407)) {
        _1 = (long)(DBL_PTR(_subprog_inlined_insert_code_at_493_63407)->dbl);
        if (UNIQUE(DBL_PTR(_subprog_inlined_insert_code_at_493_63407)) && (DBL_PTR(_subprog_inlined_insert_code_at_493_63407)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subprog_inlined_insert_code_at_493_63407);
        _subprog_inlined_insert_code_at_493_63407 = _1;
    }

    /** 	shifting_sub = subprog*/
    _29shifting_sub_62575 = _subprog_inlined_insert_code_at_493_63407;

    /** 	shift:insert_code( code, index )*/
    RefDS(_code_inlined_insert_code_at_490_63406);
    _65insert_code(_code_inlined_insert_code_at_490_63406, _pc_63340);

    /** 	shifting_sub = 0*/
    _29shifting_sub_62575 = 0;

    /** end procedure*/
    goto L14; // [506] 509
L14: 
    DeRefi(_code_inlined_insert_code_at_490_63406);
    _code_inlined_insert_code_at_490_63406 = NOVALUE;
    DeRef(_subprog_inlined_insert_code_at_493_63407);
    _subprog_inlined_insert_code_at_493_63407 = NOVALUE;

    /** 					pc += 2*/
    _pc_63340 = _pc_63340 + 2;
    goto L12; // [517] 792
L13: 

    /** 				elsif which_type = atom_type then*/
    if (_which_type_63305 != _52atom_type_47101)
    goto L15; // [524] 584

    /** 					insert_code( { ATOM_CHECK, var }, pc, fr[FR_SUBPROG] )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 101;
    ((int *)_2)[2] = _var_63307;
    _31900 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_fr_63302);
    _31901 = (int)*(((s1_ptr)_2)->base + 4);
    DeRefi(_code_inlined_insert_code_at_554_63416);
    _code_inlined_insert_code_at_554_63416 = _31900;
    _31900 = NOVALUE;
    Ref(_31901);
    DeRef(_subprog_inlined_insert_code_at_557_63417);
    _subprog_inlined_insert_code_at_557_63417 = _31901;
    _31901 = NOVALUE;
    if (!IS_ATOM_INT(_subprog_inlined_insert_code_at_557_63417)) {
        _1 = (long)(DBL_PTR(_subprog_inlined_insert_code_at_557_63417)->dbl);
        if (UNIQUE(DBL_PTR(_subprog_inlined_insert_code_at_557_63417)) && (DBL_PTR(_subprog_inlined_insert_code_at_557_63417)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subprog_inlined_insert_code_at_557_63417);
        _subprog_inlined_insert_code_at_557_63417 = _1;
    }

    /** 	shifting_sub = subprog*/
    _29shifting_sub_62575 = _subprog_inlined_insert_code_at_557_63417;

    /** 	shift:insert_code( code, index )*/
    RefDS(_code_inlined_insert_code_at_554_63416);
    _65insert_code(_code_inlined_insert_code_at_554_63416, _pc_63340);

    /** 	shifting_sub = 0*/
    _29shifting_sub_62575 = 0;

    /** end procedure*/
    goto L16; // [570] 573
L16: 
    DeRefi(_code_inlined_insert_code_at_554_63416);
    _code_inlined_insert_code_at_554_63416 = NOVALUE;
    DeRef(_subprog_inlined_insert_code_at_557_63417);
    _subprog_inlined_insert_code_at_557_63417 = NOVALUE;

    /** 					pc += 2*/
    _pc_63340 = _pc_63340 + 2;
    goto L12; // [581] 792
L15: 

    /** 				elsif SymTab[which_type][S_NEXT] then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31903 = (int)*(((s1_ptr)_2)->base + _which_type_63305);
    _2 = (int)SEQ_PTR(_31903);
    _31904 = (int)*(((s1_ptr)_2)->base + 2);
    _31903 = NOVALUE;
    if (_31904 == 0) {
        _31904 = NOVALUE;
        goto L17; // [598] 789
    }
    else {
        if (!IS_ATOM_INT(_31904) && DBL_PTR(_31904)->dbl == 0.0){
            _31904 = NOVALUE;
            goto L17; // [598] 789
        }
        _31904 = NOVALUE;
    }
    _31904 = NOVALUE;

    /** 					integer start_pc = pc*/
    _start_pc_63424 = _pc_63340;

    /** 					if SymTab[SymTab[which_type][S_NEXT]][S_VTYPE] = integer_type then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31905 = (int)*(((s1_ptr)_2)->base + _which_type_63305);
    _2 = (int)SEQ_PTR(_31905);
    _31906 = (int)*(((s1_ptr)_2)->base + 2);
    _31905 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_31906)){
        _31907 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31906)->dbl));
    }
    else{
        _31907 = (int)*(((s1_ptr)_2)->base + _31906);
    }
    _2 = (int)SEQ_PTR(_31907);
    _31908 = (int)*(((s1_ptr)_2)->base + 15);
    _31907 = NOVALUE;
    if (binary_op_a(NOTEQ, _31908, _52integer_type_47105)){
        _31908 = NOVALUE;
        goto L18; // [634] 692
    }
    _31908 = NOVALUE;

    /** 						insert_code( { INTEGER_CHECK, var }, pc, fr[FR_SUBPROG] )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 96;
    ((int *)_2)[2] = _var_63307;
    _31910 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_fr_63302);
    _31911 = (int)*(((s1_ptr)_2)->base + 4);
    DeRefi(_code_inlined_insert_code_at_664_63440);
    _code_inlined_insert_code_at_664_63440 = _31910;
    _31910 = NOVALUE;
    Ref(_31911);
    DeRef(_subprog_inlined_insert_code_at_667_63441);
    _subprog_inlined_insert_code_at_667_63441 = _31911;
    _31911 = NOVALUE;
    if (!IS_ATOM_INT(_subprog_inlined_insert_code_at_667_63441)) {
        _1 = (long)(DBL_PTR(_subprog_inlined_insert_code_at_667_63441)->dbl);
        if (UNIQUE(DBL_PTR(_subprog_inlined_insert_code_at_667_63441)) && (DBL_PTR(_subprog_inlined_insert_code_at_667_63441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subprog_inlined_insert_code_at_667_63441);
        _subprog_inlined_insert_code_at_667_63441 = _1;
    }

    /** 	shifting_sub = subprog*/
    _29shifting_sub_62575 = _subprog_inlined_insert_code_at_667_63441;

    /** 	shift:insert_code( code, index )*/
    RefDS(_code_inlined_insert_code_at_664_63440);
    _65insert_code(_code_inlined_insert_code_at_664_63440, _pc_63340);

    /** 	shifting_sub = 0*/
    _29shifting_sub_62575 = 0;

    /** end procedure*/
    goto L19; // [680] 683
L19: 
    DeRefi(_code_inlined_insert_code_at_664_63440);
    _code_inlined_insert_code_at_664_63440 = NOVALUE;
    DeRef(_subprog_inlined_insert_code_at_667_63441);
    _subprog_inlined_insert_code_at_667_63441 = NOVALUE;

    /** 						pc += 2*/
    _pc_63340 = _pc_63340 + 2;
L18: 

    /** 					symtab_index c = NewTempSym()*/
    _c_63444 = _52NewTempSym(0);
    if (!IS_ATOM_INT(_c_63444)) {
        _1 = (long)(DBL_PTR(_c_63444)->dbl);
        if (UNIQUE(DBL_PTR(_c_63444)) && (DBL_PTR(_c_63444)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_63444);
        _c_63444 = _1;
    }

    /** 					SymTab[fr[FR_SUBPROG]][S_STACK_SPACE] += 1*/
    _2 = (int)SEQ_PTR(_fr_63302);
    _31914 = (int)*(((s1_ptr)_2)->base + 4);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_31914))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_31914)->dbl));
    else
    _3 = (int)(_31914 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!IS_ATOM_INT(_25S_STACK_SPACE_11973)){
        _31917 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_STACK_SPACE_11973)->dbl));
    }
    else{
        _31917 = (int)*(((s1_ptr)_2)->base + _25S_STACK_SPACE_11973);
    }
    _31915 = NOVALUE;
    if (IS_ATOM_INT(_31917)) {
        _31918 = _31917 + 1;
        if (_31918 > MAXINT){
            _31918 = NewDouble((double)_31918);
        }
    }
    else
    _31918 = binary_op(PLUS, 1, _31917);
    _31917 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_STACK_SPACE_11973))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_STACK_SPACE_11973)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_STACK_SPACE_11973);
    _1 = *(int *)_2;
    *(int *)_2 = _31918;
    if( _1 != _31918 ){
        DeRef(_1);
    }
    _31918 = NOVALUE;
    _31915 = NOVALUE;

    /** 					insert_code( { PROC, which_type, var, c, TYPE_CHECK }, pc, fr[FR_SUBPROG] )*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 27;
    *((int *)(_2+8)) = _which_type_63305;
    *((int *)(_2+12)) = _var_63307;
    *((int *)(_2+16)) = _c_63444;
    *((int *)(_2+20)) = 65;
    _31919 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_fr_63302);
    _31920 = (int)*(((s1_ptr)_2)->base + 4);
    DeRefi(_code_inlined_insert_code_at_762_63459);
    _code_inlined_insert_code_at_762_63459 = _31919;
    _31919 = NOVALUE;
    Ref(_31920);
    DeRef(_subprog_inlined_insert_code_at_765_63460);
    _subprog_inlined_insert_code_at_765_63460 = _31920;
    _31920 = NOVALUE;
    if (!IS_ATOM_INT(_subprog_inlined_insert_code_at_765_63460)) {
        _1 = (long)(DBL_PTR(_subprog_inlined_insert_code_at_765_63460)->dbl);
        if (UNIQUE(DBL_PTR(_subprog_inlined_insert_code_at_765_63460)) && (DBL_PTR(_subprog_inlined_insert_code_at_765_63460)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subprog_inlined_insert_code_at_765_63460);
        _subprog_inlined_insert_code_at_765_63460 = _1;
    }

    /** 	shifting_sub = subprog*/
    _29shifting_sub_62575 = _subprog_inlined_insert_code_at_765_63460;

    /** 	shift:insert_code( code, index )*/
    RefDS(_code_inlined_insert_code_at_762_63459);
    _65insert_code(_code_inlined_insert_code_at_762_63459, _pc_63340);

    /** 	shifting_sub = 0*/
    _29shifting_sub_62575 = 0;

    /** end procedure*/
    goto L1A; // [777] 780
L1A: 
    DeRefi(_code_inlined_insert_code_at_762_63459);
    _code_inlined_insert_code_at_762_63459 = NOVALUE;
    DeRef(_subprog_inlined_insert_code_at_765_63460);
    _subprog_inlined_insert_code_at_765_63460 = NOVALUE;

    /** 					pc += 4*/
    _pc_63340 = _pc_63340 + 4;
L17: 
L12: 
LF: 
LD: 
LA: 

    /** 	if (TRANSLATE or not with_type_check) and SymTab[which_type][S_NEXT] then*/
    if (_25TRANSLATE_11874 != 0) {
        _31922 = 1;
        goto L1B; // [799] 810
    }
    _31923 = (_with_type_check_63342 == 0);
    _31922 = (_31923 != 0);
L1B: 
    if (_31922 == 0) {
        goto L1C; // [810] 1041
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31925 = (int)*(((s1_ptr)_2)->base + _which_type_63305);
    _2 = (int)SEQ_PTR(_31925);
    _31926 = (int)*(((s1_ptr)_2)->base + 2);
    _31925 = NOVALUE;
    if (_31926 == 0) {
        _31926 = NOVALUE;
        goto L1C; // [827] 1041
    }
    else {
        if (!IS_ATOM_INT(_31926) && DBL_PTR(_31926)->dbl == 0.0){
            _31926 = NOVALUE;
            goto L1C; // [827] 1041
        }
        _31926 = NOVALUE;
    }
    _31926 = NOVALUE;

    /** 		integer start_pc = pc*/
    _start_pc_63471 = _pc_63340;

    /** 		if which_type = sequence_type or*/
    _31927 = (_which_type_63305 == _52sequence_type_47103);
    if (_31927 != 0) {
        goto L1D; // [843] 882
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31929 = (int)*(((s1_ptr)_2)->base + _which_type_63305);
    _2 = (int)SEQ_PTR(_31929);
    _31930 = (int)*(((s1_ptr)_2)->base + 2);
    _31929 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_31930)){
        _31931 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31930)->dbl));
    }
    else{
        _31931 = (int)*(((s1_ptr)_2)->base + _31930);
    }
    _2 = (int)SEQ_PTR(_31931);
    _31932 = (int)*(((s1_ptr)_2)->base + 15);
    _31931 = NOVALUE;
    if (IS_ATOM_INT(_31932)) {
        _31933 = (_31932 == _52sequence_type_47103);
    }
    else {
        _31933 = binary_op(EQUALS, _31932, _52sequence_type_47103);
    }
    _31932 = NOVALUE;
    if (_31933 == 0) {
        DeRef(_31933);
        _31933 = NOVALUE;
        goto L1E; // [878] 938
    }
    else {
        if (!IS_ATOM_INT(_31933) && DBL_PTR(_31933)->dbl == 0.0){
            DeRef(_31933);
            _31933 = NOVALUE;
            goto L1E; // [878] 938
        }
        DeRef(_31933);
        _31933 = NOVALUE;
    }
    DeRef(_31933);
    _31933 = NOVALUE;
L1D: 

    /** 			insert_code( { SEQUENCE_CHECK, var }, pc, fr[FR_SUBPROG] )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 97;
    ((int *)_2)[2] = _var_63307;
    _31934 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_fr_63302);
    _31935 = (int)*(((s1_ptr)_2)->base + 4);
    DeRefi(_code_inlined_insert_code_at_909_63490);
    _code_inlined_insert_code_at_909_63490 = _31934;
    _31934 = NOVALUE;
    Ref(_31935);
    DeRef(_subprog_inlined_insert_code_at_912_63491);
    _subprog_inlined_insert_code_at_912_63491 = _31935;
    _31935 = NOVALUE;
    if (!IS_ATOM_INT(_subprog_inlined_insert_code_at_912_63491)) {
        _1 = (long)(DBL_PTR(_subprog_inlined_insert_code_at_912_63491)->dbl);
        if (UNIQUE(DBL_PTR(_subprog_inlined_insert_code_at_912_63491)) && (DBL_PTR(_subprog_inlined_insert_code_at_912_63491)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subprog_inlined_insert_code_at_912_63491);
        _subprog_inlined_insert_code_at_912_63491 = _1;
    }

    /** 	shifting_sub = subprog*/
    _29shifting_sub_62575 = _subprog_inlined_insert_code_at_912_63491;

    /** 	shift:insert_code( code, index )*/
    RefDS(_code_inlined_insert_code_at_909_63490);
    _65insert_code(_code_inlined_insert_code_at_909_63490, _pc_63340);

    /** 	shifting_sub = 0*/
    _29shifting_sub_62575 = 0;

    /** end procedure*/
    goto L1F; // [924] 927
L1F: 
    DeRefi(_code_inlined_insert_code_at_909_63490);
    _code_inlined_insert_code_at_909_63490 = NOVALUE;
    DeRef(_subprog_inlined_insert_code_at_912_63491);
    _subprog_inlined_insert_code_at_912_63491 = NOVALUE;

    /** 			pc += 2*/
    _pc_63340 = _pc_63340 + 2;
    goto L20; // [935] 1040
L1E: 

    /** 		elsif which_type = integer_type or*/
    _31937 = (_which_type_63305 == _52integer_type_47105);
    if (_31937 != 0) {
        goto L21; // [946] 985
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _31939 = (int)*(((s1_ptr)_2)->base + _which_type_63305);
    _2 = (int)SEQ_PTR(_31939);
    _31940 = (int)*(((s1_ptr)_2)->base + 2);
    _31939 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_31940)){
        _31941 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31940)->dbl));
    }
    else{
        _31941 = (int)*(((s1_ptr)_2)->base + _31940);
    }
    _2 = (int)SEQ_PTR(_31941);
    _31942 = (int)*(((s1_ptr)_2)->base + 15);
    _31941 = NOVALUE;
    if (IS_ATOM_INT(_31942)) {
        _31943 = (_31942 == _52integer_type_47105);
    }
    else {
        _31943 = binary_op(EQUALS, _31942, _52integer_type_47105);
    }
    _31942 = NOVALUE;
    if (_31943 == 0) {
        DeRef(_31943);
        _31943 = NOVALUE;
        goto L22; // [981] 1039
    }
    else {
        if (!IS_ATOM_INT(_31943) && DBL_PTR(_31943)->dbl == 0.0){
            DeRef(_31943);
            _31943 = NOVALUE;
            goto L22; // [981] 1039
        }
        DeRef(_31943);
        _31943 = NOVALUE;
    }
    DeRef(_31943);
    _31943 = NOVALUE;
L21: 

    /** 			insert_code( { INTEGER_CHECK, var }, pc, fr[FR_SUBPROG] )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 96;
    ((int *)_2)[2] = _var_63307;
    _31944 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_fr_63302);
    _31945 = (int)*(((s1_ptr)_2)->base + 4);
    DeRefi(_code_inlined_insert_code_at_1012_63511);
    _code_inlined_insert_code_at_1012_63511 = _31944;
    _31944 = NOVALUE;
    Ref(_31945);
    DeRef(_subprog_inlined_insert_code_at_1015_63512);
    _subprog_inlined_insert_code_at_1015_63512 = _31945;
    _31945 = NOVALUE;
    if (!IS_ATOM_INT(_subprog_inlined_insert_code_at_1015_63512)) {
        _1 = (long)(DBL_PTR(_subprog_inlined_insert_code_at_1015_63512)->dbl);
        if (UNIQUE(DBL_PTR(_subprog_inlined_insert_code_at_1015_63512)) && (DBL_PTR(_subprog_inlined_insert_code_at_1015_63512)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subprog_inlined_insert_code_at_1015_63512);
        _subprog_inlined_insert_code_at_1015_63512 = _1;
    }

    /** 	shifting_sub = subprog*/
    _29shifting_sub_62575 = _subprog_inlined_insert_code_at_1015_63512;

    /** 	shift:insert_code( code, index )*/
    RefDS(_code_inlined_insert_code_at_1012_63511);
    _65insert_code(_code_inlined_insert_code_at_1012_63511, _pc_63340);

    /** 	shifting_sub = 0*/
    _29shifting_sub_62575 = 0;

    /** end procedure*/
    goto L23; // [1027] 1030
L23: 
    DeRefi(_code_inlined_insert_code_at_1012_63511);
    _code_inlined_insert_code_at_1012_63511 = NOVALUE;
    DeRef(_subprog_inlined_insert_code_at_1015_63512);
    _subprog_inlined_insert_code_at_1015_63512 = NOVALUE;

    /** 			pc += 4*/
    _pc_63340 = _pc_63340 + 4;
L22: 
L20: 
L1C: 

    /** 	resolved_reference( ref )*/
    _29resolved_reference(_ref_63301);

    /** 	reset_code()*/
    _29reset_code();

    /** end procedure*/
    DeRef(_tok_63300);
    DeRef(_fr_63302);
    _31860 = NOVALUE;
    DeRef(_31873);
    _31873 = NOVALUE;
    DeRef(_31878);
    _31878 = NOVALUE;
    _31906 = NOVALUE;
    _31914 = NOVALUE;
    DeRef(_31923);
    _31923 = NOVALUE;
    DeRef(_31927);
    _31927 = NOVALUE;
    _31930 = NOVALUE;
    DeRef(_31937);
    _31937 = NOVALUE;
    _31940 = NOVALUE;
    return;
    ;
}


void _29prep_forward_error(int _ref_63516)
{
    int _31953 = NOVALUE;
    int _31951 = NOVALUE;
    int _31949 = NOVALUE;
    int _31947 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_ref_63516)) {
        _1 = (long)(DBL_PTR(_ref_63516)->dbl);
        if (UNIQUE(DBL_PTR(_ref_63516)) && (DBL_PTR(_ref_63516)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_63516);
        _ref_63516 = _1;
    }

    /** 	ThisLine = forward_references[ref][FR_THISLINE]*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    _31947 = (int)*(((s1_ptr)_2)->base + _ref_63516);
    DeRef(_43ThisLine_49532);
    _2 = (int)SEQ_PTR(_31947);
    _43ThisLine_49532 = (int)*(((s1_ptr)_2)->base + 7);
    Ref(_43ThisLine_49532);
    _31947 = NOVALUE;

    /** 	bp = forward_references[ref][FR_BP]*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    _31949 = (int)*(((s1_ptr)_2)->base + _ref_63516);
    _2 = (int)SEQ_PTR(_31949);
    _43bp_49536 = (int)*(((s1_ptr)_2)->base + 8);
    if (!IS_ATOM_INT(_43bp_49536)){
        _43bp_49536 = (long)DBL_PTR(_43bp_49536)->dbl;
    }
    _31949 = NOVALUE;

    /** 	line_number = forward_references[ref][FR_LINE]*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    _31951 = (int)*(((s1_ptr)_2)->base + _ref_63516);
    _2 = (int)SEQ_PTR(_31951);
    _25line_number_12263 = (int)*(((s1_ptr)_2)->base + 6);
    if (!IS_ATOM_INT(_25line_number_12263)){
        _25line_number_12263 = (long)DBL_PTR(_25line_number_12263)->dbl;
    }
    _31951 = NOVALUE;

    /** 	current_file_no = forward_references[ref][FR_FILE]*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    _31953 = (int)*(((s1_ptr)_2)->base + _ref_63516);
    _2 = (int)SEQ_PTR(_31953);
    _25current_file_no_12262 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_25current_file_no_12262)){
        _25current_file_no_12262 = (long)DBL_PTR(_25current_file_no_12262)->dbl;
    }
    _31953 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


void _29forward_error(int _tok_63532, int _ref_63533)
{
    int _31960 = NOVALUE;
    int _31959 = NOVALUE;
    int _31958 = NOVALUE;
    int _31957 = NOVALUE;
    int _31956 = NOVALUE;
    int _31955 = NOVALUE;
    int _0, _1, _2;
    

    /** 	prep_forward_error( ref )*/
    _29prep_forward_error(_ref_63533);

    /** 	CompileErr(68, { expected_name( forward_references[ref][FR_TYPE] ),*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    _31955 = (int)*(((s1_ptr)_2)->base + _ref_63533);
    _2 = (int)SEQ_PTR(_31955);
    _31956 = (int)*(((s1_ptr)_2)->base + 1);
    _31955 = NOVALUE;
    Ref(_31956);
    _31957 = _29expected_name(_31956);
    _31956 = NOVALUE;
    _2 = (int)SEQ_PTR(_tok_63532);
    _31958 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_31958);
    _31959 = _29expected_name(_31958);
    _31958 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _31957;
    ((int *)_2)[2] = _31959;
    _31960 = MAKE_SEQ(_1);
    _31959 = NOVALUE;
    _31957 = NOVALUE;
    _43CompileErr(68, _31960, 0);
    _31960 = NOVALUE;

    /** end procedure*/
    DeRef(_tok_63532);
    return;
    ;
}


int _29find_reference(int _fr_63544)
{
    int _name_63545 = NOVALUE;
    int _file_63547 = NOVALUE;
    int _ns_file_63549 = NOVALUE;
    int _ix_63550 = NOVALUE;
    int _ns_63553 = NOVALUE;
    int _ns_tok_63557 = NOVALUE;
    int _tok_63569 = NOVALUE;
    int _31971 = NOVALUE;
    int _31968 = NOVALUE;
    int _31966 = NOVALUE;
    int _31964 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence name = fr[FR_NAME]*/
    DeRef(_name_63545);
    _2 = (int)SEQ_PTR(_fr_63544);
    _name_63545 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_name_63545);

    /** 	integer file  = fr[FR_FILE]*/
    _2 = (int)SEQ_PTR(_fr_63544);
    _file_63547 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_file_63547))
    _file_63547 = (long)DBL_PTR(_file_63547)->dbl;

    /** 	integer ns_file = -1*/
    _ns_file_63549 = -1;

    /** 	integer ix = find( ':', name )*/
    _ix_63550 = find_from(58, _name_63545, 1);

    /** 	if ix then*/
    if (_ix_63550 == 0)
    {
        goto L1; // [35] 91
    }
    else{
    }

    /** 		sequence ns = name[1..ix-1]*/
    _31964 = _ix_63550 - 1;
    rhs_slice_target = (object_ptr)&_ns_63553;
    RHS_Slice(_name_63545, 1, _31964);

    /** 		token ns_tok = keyfind( ns, ns_file, file, 1, fr[FR_HASHVAL] )*/
    _2 = (int)SEQ_PTR(_fr_63544);
    _31966 = (int)*(((s1_ptr)_2)->base + 11);
    RefDS(_ns_63553);
    Ref(_31966);
    _0 = _ns_tok_63557;
    _ns_tok_63557 = _52keyfind(_ns_63553, -1, _file_63547, 1, _31966);
    DeRef(_0);
    _31966 = NOVALUE;

    /** 		if ns_tok[T_ID] != NAMESPACE then*/
    _2 = (int)SEQ_PTR(_ns_tok_63557);
    _31968 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _31968, 523)){
        _31968 = NOVALUE;
        goto L2; // [75] 86
    }
    _31968 = NOVALUE;

    /** 			return ns_tok*/
    DeRefDS(_ns_63553);
    DeRefDS(_fr_63544);
    DeRefDS(_name_63545);
    DeRef(_tok_63569);
    _31964 = NOVALUE;
    return _ns_tok_63557;
L2: 
    DeRef(_ns_63553);
    _ns_63553 = NOVALUE;
    DeRef(_ns_tok_63557);
    _ns_tok_63557 = NOVALUE;
    goto L3; // [88] 100
L1: 

    /** 		ns_file = fr[FR_QUALIFIED]*/
    _2 = (int)SEQ_PTR(_fr_63544);
    _ns_file_63549 = (int)*(((s1_ptr)_2)->base + 9);
    if (!IS_ATOM_INT(_ns_file_63549))
    _ns_file_63549 = (long)DBL_PTR(_ns_file_63549)->dbl;
L3: 

    /** 	No_new_entry = 1*/
    _52No_new_entry_48273 = 1;

    /** 	object tok = keyfind( name, ns_file, file, , fr[FR_HASHVAL] )*/
    _2 = (int)SEQ_PTR(_fr_63544);
    _31971 = (int)*(((s1_ptr)_2)->base + 11);
    RefDS(_name_63545);
    Ref(_31971);
    _0 = _tok_63569;
    _tok_63569 = _52keyfind(_name_63545, _ns_file_63549, _file_63547, 0, _31971);
    DeRef(_0);
    _31971 = NOVALUE;

    /** 	No_new_entry = 0*/
    _52No_new_entry_48273 = 0;

    /** 	return tok*/
    DeRefDS(_fr_63544);
    DeRefDS(_name_63545);
    DeRef(_31964);
    _31964 = NOVALUE;
    return _tok_63569;
    ;
}


void _29register_forward_type(int _sym_63577, int _ref_63578)
{
    int _31978 = NOVALUE;
    int _31977 = NOVALUE;
    int _31975 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_sym_63577)) {
        _1 = (long)(DBL_PTR(_sym_63577)->dbl);
        if (UNIQUE(DBL_PTR(_sym_63577)) && (DBL_PTR(_sym_63577)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_63577);
        _sym_63577 = _1;
    }
    if (!IS_ATOM_INT(_ref_63578)) {
        _1 = (long)(DBL_PTR(_ref_63578)->dbl);
        if (UNIQUE(DBL_PTR(_ref_63578)) && (DBL_PTR(_ref_63578)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_63578);
        _ref_63578 = _1;
    }

    /** 	if ref < 0 then*/
    if (_ref_63578 >= 0)
    goto L1; // [7] 19

    /** 		ref = -ref*/
    _ref_63578 = - _ref_63578;
L1: 

    /** 	forward_references[ref][FR_DATA] &= sym*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63578 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _31977 = (int)*(((s1_ptr)_2)->base + 12);
    _31975 = NOVALUE;
    if (IS_SEQUENCE(_31977) && IS_ATOM(_sym_63577)) {
        Append(&_31978, _31977, _sym_63577);
    }
    else if (IS_ATOM(_31977) && IS_SEQUENCE(_sym_63577)) {
    }
    else {
        Concat((object_ptr)&_31978, _31977, _sym_63577);
        _31977 = NOVALUE;
    }
    _31977 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = _31978;
    if( _1 != _31978 ){
        DeRef(_1);
    }
    _31978 = NOVALUE;
    _31975 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


int _29forward_reference(int _ref_63588)
{
    int _31991 = NOVALUE;
    int _31990 = NOVALUE;
    int _31989 = NOVALUE;
    int _31988 = NOVALUE;
    int _31987 = NOVALUE;
    int _31986 = NOVALUE;
    int _31985 = NOVALUE;
    int _31983 = NOVALUE;
    int _31982 = NOVALUE;
    int _31981 = NOVALUE;
    int _31980 = NOVALUE;
    int _31979 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_ref_63588)) {
        _1 = (long)(DBL_PTR(_ref_63588)->dbl);
        if (UNIQUE(DBL_PTR(_ref_63588)) && (DBL_PTR(_ref_63588)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_63588);
        _ref_63588 = _1;
    }

    /** 	if 0 > ref and ref >= -length( forward_references ) then*/
    _31979 = (0 > _ref_63588);
    if (_31979 == 0) {
        goto L1; // [9] 95
    }
    if (IS_SEQUENCE(_29forward_references_62545)){
            _31981 = SEQ_PTR(_29forward_references_62545)->length;
    }
    else {
        _31981 = 1;
    }
    _31982 = - _31981;
    _31983 = (_ref_63588 >= _31982);
    _31982 = NOVALUE;
    if (_31983 == 0)
    {
        DeRef(_31983);
        _31983 = NOVALUE;
        goto L1; // [26] 95
    }
    else{
        DeRef(_31983);
        _31983 = NOVALUE;
    }

    /** 		ref = -ref*/
    _ref_63588 = - _ref_63588;

    /** 		if integer(forward_references[ref][FR_FILE]) and*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    _31985 = (int)*(((s1_ptr)_2)->base + _ref_63588);
    _2 = (int)SEQ_PTR(_31985);
    _31986 = (int)*(((s1_ptr)_2)->base + 3);
    _31985 = NOVALUE;
    if (IS_ATOM_INT(_31986))
    _31987 = 1;
    else if (IS_ATOM_DBL(_31986))
    _31987 = IS_ATOM_INT(DoubleToInt(_31986));
    else
    _31987 = 0;
    _31986 = NOVALUE;
    if (_31987 == 0) {
        goto L2; // [53] 85
    }
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    _31989 = (int)*(((s1_ptr)_2)->base + _ref_63588);
    _2 = (int)SEQ_PTR(_31989);
    _31990 = (int)*(((s1_ptr)_2)->base + 5);
    _31989 = NOVALUE;
    if (IS_ATOM_INT(_31990))
    _31991 = 1;
    else if (IS_ATOM_DBL(_31990))
    _31991 = IS_ATOM_INT(DoubleToInt(_31990));
    else
    _31991 = 0;
    _31990 = NOVALUE;
    if (_31991 == 0)
    {
        _31991 = NOVALUE;
        goto L2; // [73] 85
    }
    else{
        _31991 = NOVALUE;
    }

    /** 				return 1*/
    DeRef(_31979);
    _31979 = NOVALUE;
    return 1;
    goto L3; // [82] 102
L2: 

    /** 			return 0*/
    DeRef(_31979);
    _31979 = NOVALUE;
    return 0;
    goto L3; // [92] 102
L1: 

    /** 		return 0*/
    DeRef(_31979);
    _31979 = NOVALUE;
    return 0;
L3: 
    ;
}


int _29new_forward_reference(int _fwd_op_63608, int _sym_63610, int _op_63611)
{
    int _ref_63612 = NOVALUE;
    int _len_63613 = NOVALUE;
    int _hashval_63643 = NOVALUE;
    int _default_sym_63718 = NOVALUE;
    int _param_63721 = NOVALUE;
    int _set_data_2__tmp_at616_63738 = NOVALUE;
    int _set_data_1__tmp_at616_63737 = NOVALUE;
    int _data_inlined_set_data_at_613_63736 = NOVALUE;
    int _32064 = NOVALUE;
    int _32063 = NOVALUE;
    int _32062 = NOVALUE;
    int _32059 = NOVALUE;
    int _32057 = NOVALUE;
    int _32056 = NOVALUE;
    int _32054 = NOVALUE;
    int _32053 = NOVALUE;
    int _32052 = NOVALUE;
    int _32050 = NOVALUE;
    int _32048 = NOVALUE;
    int _32046 = NOVALUE;
    int _32043 = NOVALUE;
    int _32042 = NOVALUE;
    int _32040 = NOVALUE;
    int _32038 = NOVALUE;
    int _32036 = NOVALUE;
    int _32034 = NOVALUE;
    int _32033 = NOVALUE;
    int _32032 = NOVALUE;
    int _32030 = NOVALUE;
    int _32027 = NOVALUE;
    int _32025 = NOVALUE;
    int _32023 = NOVALUE;
    int _32022 = NOVALUE;
    int _32021 = NOVALUE;
    int _32020 = NOVALUE;
    int _32018 = NOVALUE;
    int _32015 = NOVALUE;
    int _32014 = NOVALUE;
    int _32013 = NOVALUE;
    int _32011 = NOVALUE;
    int _32010 = NOVALUE;
    int _32009 = NOVALUE;
    int _32008 = NOVALUE;
    int _32006 = NOVALUE;
    int _32005 = NOVALUE;
    int _32004 = NOVALUE;
    int _32003 = NOVALUE;
    int _32001 = NOVALUE;
    int _31998 = NOVALUE;
    int _31997 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_fwd_op_63608)) {
        _1 = (long)(DBL_PTR(_fwd_op_63608)->dbl);
        if (UNIQUE(DBL_PTR(_fwd_op_63608)) && (DBL_PTR(_fwd_op_63608)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fwd_op_63608);
        _fwd_op_63608 = _1;
    }
    if (!IS_ATOM_INT(_sym_63610)) {
        _1 = (long)(DBL_PTR(_sym_63610)->dbl);
        if (UNIQUE(DBL_PTR(_sym_63610)) && (DBL_PTR(_sym_63610)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_63610);
        _sym_63610 = _1;
    }
    if (!IS_ATOM_INT(_op_63611)) {
        _1 = (long)(DBL_PTR(_op_63611)->dbl);
        if (UNIQUE(DBL_PTR(_op_63611)) && (DBL_PTR(_op_63611)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_op_63611);
        _op_63611 = _1;
    }

    /** 		len = length( inactive_references )*/
    if (IS_SEQUENCE(_29inactive_references_62549)){
            _len_63613 = SEQ_PTR(_29inactive_references_62549)->length;
    }
    else {
        _len_63613 = 1;
    }

    /** 	if len then*/
    if (_len_63613 == 0)
    {
        goto L1; // [16] 39
    }
    else{
    }

    /** 		ref = inactive_references[len]*/
    _2 = (int)SEQ_PTR(_29inactive_references_62549);
    _ref_63612 = (int)*(((s1_ptr)_2)->base + _len_63613);
    if (!IS_ATOM_INT(_ref_63612))
    _ref_63612 = (long)DBL_PTR(_ref_63612)->dbl;

    /** 		inactive_references = remove( inactive_references, len, len )*/
    {
        s1_ptr assign_space = SEQ_PTR(_29inactive_references_62549);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_len_63613)) ? _len_63613 : (long)(DBL_PTR(_len_63613)->dbl);
        int stop = (IS_ATOM_INT(_len_63613)) ? _len_63613 : (long)(DBL_PTR(_len_63613)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_29inactive_references_62549), start, &_29inactive_references_62549 );
            }
            else Tail(SEQ_PTR(_29inactive_references_62549), stop+1, &_29inactive_references_62549);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_29inactive_references_62549), start, &_29inactive_references_62549);
        }
        else {
            assign_slice_seq = &assign_space;
            _29inactive_references_62549 = Remove_elements(start, stop, (SEQ_PTR(_29inactive_references_62549)->ref == 1));
        }
    }
    goto L2; // [36] 55
L1: 

    /** 		forward_references &= 0*/
    Append(&_29forward_references_62545, _29forward_references_62545, 0);

    /** 		ref = length( forward_references )*/
    if (IS_SEQUENCE(_29forward_references_62545)){
            _ref_63612 = SEQ_PTR(_29forward_references_62545)->length;
    }
    else {
        _ref_63612 = 1;
    }
L2: 

    /** 	forward_references[ref] = repeat( 0, FR_SIZE )*/
    _31997 = Repeat(0, 12);
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _ref_63612);
    _1 = *(int *)_2;
    *(int *)_2 = _31997;
    if( _1 != _31997 ){
        DeRef(_1);
    }
    _31997 = NOVALUE;

    /** 	forward_references[ref][FR_TYPE]      = fwd_op*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63612 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _fwd_op_63608;
    DeRef(_1);
    _31998 = NOVALUE;

    /** 	if sym < 0 then*/
    if (_sym_63610 >= 0)
    goto L3; // [88] 155

    /** 		forward_references[ref][FR_NAME] = forward_references[-sym][FR_NAME]*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63612 + ((s1_ptr)_2)->base);
    if ((unsigned long)_sym_63610 == 0xC0000000)
    _32003 = (int)NewDouble((double)-0xC0000000);
    else
    _32003 = - _sym_63610;
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!IS_ATOM_INT(_32003)){
        _32004 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32003)->dbl));
    }
    else{
        _32004 = (int)*(((s1_ptr)_2)->base + _32003);
    }
    _2 = (int)SEQ_PTR(_32004);
    _32005 = (int)*(((s1_ptr)_2)->base + 2);
    _32004 = NOVALUE;
    Ref(_32005);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _32005;
    if( _1 != _32005 ){
        DeRef(_1);
    }
    _32005 = NOVALUE;
    _32001 = NOVALUE;

    /** 		forward_references[ref][FR_HASHVAL] = forward_references[-sym][FR_HASHVAL]*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63612 + ((s1_ptr)_2)->base);
    if ((unsigned long)_sym_63610 == 0xC0000000)
    _32008 = (int)NewDouble((double)-0xC0000000);
    else
    _32008 = - _sym_63610;
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!IS_ATOM_INT(_32008)){
        _32009 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32008)->dbl));
    }
    else{
        _32009 = (int)*(((s1_ptr)_2)->base + _32008);
    }
    _2 = (int)SEQ_PTR(_32009);
    _32010 = (int)*(((s1_ptr)_2)->base + 11);
    _32009 = NOVALUE;
    Ref(_32010);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 11);
    _1 = *(int *)_2;
    *(int *)_2 = _32010;
    if( _1 != _32010 ){
        DeRef(_1);
    }
    _32010 = NOVALUE;
    _32006 = NOVALUE;
    goto L4; // [152] 262
L3: 

    /** 		forward_references[ref][FR_NAME] = SymTab[sym][S_NAME]*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63612 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _32013 = (int)*(((s1_ptr)_2)->base + _sym_63610);
    _2 = (int)SEQ_PTR(_32013);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _32014 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _32014 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _32013 = NOVALUE;
    Ref(_32014);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _32014;
    if( _1 != _32014 ){
        DeRef(_1);
    }
    _32014 = NOVALUE;
    _32011 = NOVALUE;

    /** 		integer hashval = SymTab[sym][S_HASHVAL]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _32015 = (int)*(((s1_ptr)_2)->base + _sym_63610);
    _2 = (int)SEQ_PTR(_32015);
    _hashval_63643 = (int)*(((s1_ptr)_2)->base + 11);
    if (!IS_ATOM_INT(_hashval_63643)){
        _hashval_63643 = (long)DBL_PTR(_hashval_63643)->dbl;
    }
    _32015 = NOVALUE;

    /** 		if 0 = hashval then*/
    if (0 != _hashval_63643)
    goto L5; // [200] 238

    /** 			forward_references[ref][FR_HASHVAL] = hashfn( forward_references[ref][FR_NAME] )*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63612 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    _32020 = (int)*(((s1_ptr)_2)->base + _ref_63612);
    _2 = (int)SEQ_PTR(_32020);
    _32021 = (int)*(((s1_ptr)_2)->base + 2);
    _32020 = NOVALUE;
    Ref(_32021);
    _32022 = _52hashfn(_32021);
    _32021 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 11);
    _1 = *(int *)_2;
    *(int *)_2 = _32022;
    if( _1 != _32022 ){
        DeRef(_1);
    }
    _32022 = NOVALUE;
    _32018 = NOVALUE;
    goto L6; // [235] 259
L5: 

    /** 			forward_references[ref][FR_HASHVAL] = hashval*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63612 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 11);
    _1 = *(int *)_2;
    *(int *)_2 = _hashval_63643;
    DeRef(_1);
    _32023 = NOVALUE;

    /** 			remove_symbol( sym )*/
    _52remove_symbol(_sym_63610);
L6: 
L4: 

    /** 	forward_references[ref][FR_FILE]      = current_file_no*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63612 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _25current_file_no_12262;
    DeRef(_1);
    _32025 = NOVALUE;

    /** 	forward_references[ref][FR_SUBPROG]   = CurrentSub*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63612 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _25CurrentSub_12270;
    DeRef(_1);
    _32027 = NOVALUE;

    /** 	if fwd_op != TYPE then*/
    if (_fwd_op_63608 == 504)
    goto L7; // [300] 329

    /** 		forward_references[ref][FR_PC]        = length( Code ) + 1*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63612 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_25Code_12355)){
            _32032 = SEQ_PTR(_25Code_12355)->length;
    }
    else {
        _32032 = 1;
    }
    _32033 = _32032 + 1;
    _32032 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _32033;
    if( _1 != _32033 ){
        DeRef(_1);
    }
    _32033 = NOVALUE;
    _32030 = NOVALUE;
L7: 

    /** 	forward_references[ref][FR_LINE]      = fwd_line_number*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63612 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _25fwd_line_number_12264;
    DeRef(_1);
    _32034 = NOVALUE;

    /** 	forward_references[ref][FR_THISLINE]  = ForwardLine*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63612 + ((s1_ptr)_2)->base);
    Ref(_43ForwardLine_49533);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 7);
    _1 = *(int *)_2;
    *(int *)_2 = _43ForwardLine_49533;
    DeRef(_1);
    _32036 = NOVALUE;

    /** 	forward_references[ref][FR_BP]        = forward_bp*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63612 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 8);
    _1 = *(int *)_2;
    *(int *)_2 = _43forward_bp_49537;
    DeRef(_1);
    _32038 = NOVALUE;

    /** 	forward_references[ref][FR_QUALIFIED] = get_qualified_fwd()*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63612 + ((s1_ptr)_2)->base);
    _32042 = _60get_qualified_fwd();
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 9);
    _1 = *(int *)_2;
    *(int *)_2 = _32042;
    if( _1 != _32042 ){
        DeRef(_1);
    }
    _32042 = NOVALUE;
    _32040 = NOVALUE;

    /** 	forward_references[ref][FR_OP]        = op*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63612 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 10);
    _1 = *(int *)_2;
    *(int *)_2 = _op_63611;
    DeRef(_1);
    _32043 = NOVALUE;

    /** 	if op = GOTO then*/
    if (_op_63611 != 188)
    goto L8; // [417] 441

    /** 		forward_references[ref][FR_DATA] = { sym }*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63612 + ((s1_ptr)_2)->base);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _sym_63610;
    _32048 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = _32048;
    if( _1 != _32048 ){
        DeRef(_1);
    }
    _32048 = NOVALUE;
    _32046 = NOVALUE;
L8: 

    /** 	if CurrentSub = TopLevelSub then*/
    if (_25CurrentSub_12270 != _25TopLevelSub_12269)
    goto L9; // [447] 509

    /** 		if length( toplevel_references ) < current_file_no then*/
    if (IS_SEQUENCE(_29toplevel_references_62548)){
            _32050 = SEQ_PTR(_29toplevel_references_62548)->length;
    }
    else {
        _32050 = 1;
    }
    if (_32050 >= _25current_file_no_12262)
    goto LA; // [460] 488

    /** 			toplevel_references &= repeat( {}, current_file_no - length( toplevel_references ) )*/
    if (IS_SEQUENCE(_29toplevel_references_62548)){
            _32052 = SEQ_PTR(_29toplevel_references_62548)->length;
    }
    else {
        _32052 = 1;
    }
    _32053 = _25current_file_no_12262 - _32052;
    _32052 = NOVALUE;
    _32054 = Repeat(_22682, _32053);
    _32053 = NOVALUE;
    Concat((object_ptr)&_29toplevel_references_62548, _29toplevel_references_62548, _32054);
    DeRefDS(_32054);
    _32054 = NOVALUE;
LA: 

    /** 		toplevel_references[current_file_no] &= ref*/
    _2 = (int)SEQ_PTR(_29toplevel_references_62548);
    _32056 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    if (IS_SEQUENCE(_32056) && IS_ATOM(_ref_63612)) {
        Append(&_32057, _32056, _ref_63612);
    }
    else if (IS_ATOM(_32056) && IS_SEQUENCE(_ref_63612)) {
    }
    else {
        Concat((object_ptr)&_32057, _32056, _ref_63612);
        _32056 = NOVALUE;
    }
    _32056 = NOVALUE;
    _2 = (int)SEQ_PTR(_29toplevel_references_62548);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29toplevel_references_62548 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _25current_file_no_12262);
    _1 = *(int *)_2;
    *(int *)_2 = _32057;
    if( _1 != _32057 ){
        DeRef(_1);
    }
    _32057 = NOVALUE;
    goto LB; // [506] 635
L9: 

    /** 		add_active_reference( ref )*/
    _29add_active_reference(_ref_63612, _25current_file_no_12262);

    /** 		if Parser_mode = PAM_RECORD then*/
    if (_25Parser_mode_12388 != 1)
    goto LC; // [523] 632

    /** 			symtab_pointer default_sym = CurrentSub*/
    _default_sym_63718 = _25CurrentSub_12270;

    /** 			symtab_pointer param = 0*/
    _param_63721 = 0;

    /** 			while default_sym with entry do*/
    goto LD; // [545] 574
LE: 
    if (_default_sym_63718 == 0)
    {
        goto LF; // [548] 587
    }
    else{
    }

    /** 				if sym_scope( default_sym ) = SC_PRIVATE then*/
    _32059 = _52sym_scope(_default_sym_63718);
    if (binary_op_a(NOTEQ, _32059, 3)){
        DeRef(_32059);
        _32059 = NOVALUE;
        goto L10; // [559] 571
    }
    DeRef(_32059);
    _32059 = NOVALUE;

    /** 					param = default_sym*/
    _param_63721 = _default_sym_63718;
L10: 

    /** 			entry*/
LD: 

    /** 				default_sym = sym_next( default_sym )*/
    _default_sym_63718 = _52sym_next(_default_sym_63718);
    if (!IS_ATOM_INT(_default_sym_63718)) {
        _1 = (long)(DBL_PTR(_default_sym_63718)->dbl);
        if (UNIQUE(DBL_PTR(_default_sym_63718)) && (DBL_PTR(_default_sym_63718)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_default_sym_63718);
        _default_sym_63718 = _1;
    }

    /** 			end while*/
    goto LE; // [584] 548
LF: 

    /** 			set_data( ref, {{ PAM_RECORD, param, length( Recorded_sym ) }} )*/
    if (IS_SEQUENCE(_25Recorded_sym_12391)){
            _32062 = SEQ_PTR(_25Recorded_sym_12391)->length;
    }
    else {
        _32062 = 1;
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 1;
    *((int *)(_2+8)) = _param_63721;
    *((int *)(_2+12)) = _32062;
    _32063 = MAKE_SEQ(_1);
    _32062 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _32063;
    _32064 = MAKE_SEQ(_1);
    _32063 = NOVALUE;
    DeRef(_data_inlined_set_data_at_613_63736);
    _data_inlined_set_data_at_613_63736 = _32064;
    _32064 = NOVALUE;

    /** 	forward_references[ref][FR_DATA] = data*/
    _2 = (int)SEQ_PTR(_29forward_references_62545);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29forward_references_62545 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63612 + ((s1_ptr)_2)->base);
    RefDS(_data_inlined_set_data_at_613_63736);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = _data_inlined_set_data_at_613_63736;
    DeRef(_1);

    /** end procedure*/
    goto L11; // [626] 629
L11: 
    DeRef(_data_inlined_set_data_at_613_63736);
    _data_inlined_set_data_at_613_63736 = NOVALUE;
LC: 
LB: 

    /** 	fwdref_count += 1*/
    _29fwdref_count_62576 = _29fwdref_count_62576 + 1;

    /** 	return ref*/
    DeRef(_32003);
    _32003 = NOVALUE;
    DeRef(_32008);
    _32008 = NOVALUE;
    return _ref_63612;
    ;
}


void _29add_active_reference(int _ref_63742, int _file_no_63743)
{
    int _sp_63757 = NOVALUE;
    int _32088 = NOVALUE;
    int _32087 = NOVALUE;
    int _32085 = NOVALUE;
    int _32084 = NOVALUE;
    int _32083 = NOVALUE;
    int _32081 = NOVALUE;
    int _32080 = NOVALUE;
    int _32079 = NOVALUE;
    int _32076 = NOVALUE;
    int _32074 = NOVALUE;
    int _32073 = NOVALUE;
    int _32072 = NOVALUE;
    int _32070 = NOVALUE;
    int _32069 = NOVALUE;
    int _32068 = NOVALUE;
    int _32066 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	if length( active_references ) < file_no then*/
    if (IS_SEQUENCE(_29active_references_62547)){
            _32066 = SEQ_PTR(_29active_references_62547)->length;
    }
    else {
        _32066 = 1;
    }
    if (_32066 >= _file_no_63743)
    goto L1; // [12] 59

    /** 		active_references &= repeat( {}, file_no - length( active_references ) )*/
    if (IS_SEQUENCE(_29active_references_62547)){
            _32068 = SEQ_PTR(_29active_references_62547)->length;
    }
    else {
        _32068 = 1;
    }
    _32069 = _file_no_63743 - _32068;
    _32068 = NOVALUE;
    _32070 = Repeat(_22682, _32069);
    _32069 = NOVALUE;
    Concat((object_ptr)&_29active_references_62547, _29active_references_62547, _32070);
    DeRefDS(_32070);
    _32070 = NOVALUE;

    /** 		active_subprogs   &= repeat( {}, file_no - length( active_subprogs ) )*/
    if (IS_SEQUENCE(_29active_subprogs_62546)){
            _32072 = SEQ_PTR(_29active_subprogs_62546)->length;
    }
    else {
        _32072 = 1;
    }
    _32073 = _file_no_63743 - _32072;
    _32072 = NOVALUE;
    _32074 = Repeat(_22682, _32073);
    _32073 = NOVALUE;
    Concat((object_ptr)&_29active_subprogs_62546, _29active_subprogs_62546, _32074);
    DeRefDS(_32074);
    _32074 = NOVALUE;
L1: 

    /** 	integer sp = find( CurrentSub, active_subprogs[file_no] )*/
    _2 = (int)SEQ_PTR(_29active_subprogs_62546);
    _32076 = (int)*(((s1_ptr)_2)->base + _file_no_63743);
    _sp_63757 = find_from(_25CurrentSub_12270, _32076, 1);
    _32076 = NOVALUE;

    /** 	if not sp then*/
    if (_sp_63757 != 0)
    goto L2; // [76] 127

    /** 		active_subprogs[file_no] &= CurrentSub*/
    _2 = (int)SEQ_PTR(_29active_subprogs_62546);
    _32079 = (int)*(((s1_ptr)_2)->base + _file_no_63743);
    if (IS_SEQUENCE(_32079) && IS_ATOM(_25CurrentSub_12270)) {
        Append(&_32080, _32079, _25CurrentSub_12270);
    }
    else if (IS_ATOM(_32079) && IS_SEQUENCE(_25CurrentSub_12270)) {
    }
    else {
        Concat((object_ptr)&_32080, _32079, _25CurrentSub_12270);
        _32079 = NOVALUE;
    }
    _32079 = NOVALUE;
    _2 = (int)SEQ_PTR(_29active_subprogs_62546);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29active_subprogs_62546 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _file_no_63743);
    _1 = *(int *)_2;
    *(int *)_2 = _32080;
    if( _1 != _32080 ){
        DeRef(_1);
    }
    _32080 = NOVALUE;

    /** 		sp = length( active_subprogs[file_no] )*/
    _2 = (int)SEQ_PTR(_29active_subprogs_62546);
    _32081 = (int)*(((s1_ptr)_2)->base + _file_no_63743);
    if (IS_SEQUENCE(_32081)){
            _sp_63757 = SEQ_PTR(_32081)->length;
    }
    else {
        _sp_63757 = 1;
    }
    _32081 = NOVALUE;

    /** 		active_references[file_no] = append( active_references[file_no], {} )*/
    _2 = (int)SEQ_PTR(_29active_references_62547);
    _32083 = (int)*(((s1_ptr)_2)->base + _file_no_63743);
    RefDS(_22682);
    Append(&_32084, _32083, _22682);
    _32083 = NOVALUE;
    _2 = (int)SEQ_PTR(_29active_references_62547);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29active_references_62547 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _file_no_63743);
    _1 = *(int *)_2;
    *(int *)_2 = _32084;
    if( _1 != _32084 ){
        DeRef(_1);
    }
    _32084 = NOVALUE;
L2: 

    /** 	active_references[file_no][sp] &= ref*/
    _2 = (int)SEQ_PTR(_29active_references_62547);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _29active_references_62547 = MAKE_SEQ(_2);
    }
    _3 = (int)(_file_no_63743 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _32087 = (int)*(((s1_ptr)_2)->base + _sp_63757);
    _32085 = NOVALUE;
    if (IS_SEQUENCE(_32087) && IS_ATOM(_ref_63742)) {
        Append(&_32088, _32087, _ref_63742);
    }
    else if (IS_ATOM(_32087) && IS_SEQUENCE(_ref_63742)) {
    }
    else {
        Concat((object_ptr)&_32088, _32087, _ref_63742);
        _32087 = NOVALUE;
    }
    _32087 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _sp_63757);
    _1 = *(int *)_2;
    *(int *)_2 = _32088;
    if( _1 != _32088 ){
        DeRef(_1);
    }
    _32088 = NOVALUE;
    _32085 = NOVALUE;

    /** end procedure*/
    _32081 = NOVALUE;
    return;
    ;
}


int _29resolve_file(int _refs_63794, int _report_errors_63795, int _unincluded_ok_63796)
{
    int _errors_63797 = NOVALUE;
    int _ref_63801 = NOVALUE;
    int _fr_63803 = NOVALUE;
    int _tok_63816 = NOVALUE;
    int _code_sub_63824 = NOVALUE;
    int _fr_type_63826 = NOVALUE;
    int _sym_tok_63828 = NOVALUE;
    int _32142 = NOVALUE;
    int _32141 = NOVALUE;
    int _32140 = NOVALUE;
    int _32139 = NOVALUE;
    int _32138 = NOVALUE;
    int _32137 = NOVALUE;
    int _32132 = NOVALUE;
    int _32131 = NOVALUE;
    int _32130 = NOVALUE;
    int _32128 = NOVALUE;
    int _32127 = NOVALUE;
    int _32124 = NOVALUE;
    int _32123 = NOVALUE;
    int _32122 = NOVALUE;
    int _32118 = NOVALUE;
    int _32117 = NOVALUE;
    int _32109 = NOVALUE;
    int _32107 = NOVALUE;
    int _32106 = NOVALUE;
    int _32105 = NOVALUE;
    int _32104 = NOVALUE;
    int _32103 = NOVALUE;
    int _32102 = NOVALUE;
    int _32099 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence errors = {}*/
    RefDS(_22682);
    DeRefi(_errors_63797);
    _errors_63797 = _22682;

    /** 	for ar = length( refs ) to 1 by -1 do*/
    if (IS_SEQUENCE(_refs_63794)){
            _32099 = SEQ_PTR(_refs_63794)->length;
    }
    else {
        _32099 = 1;
    }
    {
        int _ar_63799;
        _ar_63799 = _32099;
L1: 
        if (_ar_63799 < 1){
            goto L2; // [19] 491
        }

        /** 		integer ref = refs[ar]*/
        _2 = (int)SEQ_PTR(_refs_63794);
        _ref_63801 = (int)*(((s1_ptr)_2)->base + _ar_63799);
        if (!IS_ATOM_INT(_ref_63801))
        _ref_63801 = (long)DBL_PTR(_ref_63801)->dbl;

        /** 		sequence fr = forward_references[ref]*/
        DeRef(_fr_63803);
        _2 = (int)SEQ_PTR(_29forward_references_62545);
        _fr_63803 = (int)*(((s1_ptr)_2)->base + _ref_63801);
        Ref(_fr_63803);

        /** 		if include_matrix[fr[FR_FILE]][current_file_no] = NOT_INCLUDED and not unincluded_ok then*/
        _2 = (int)SEQ_PTR(_fr_63803);
        _32102 = (int)*(((s1_ptr)_2)->base + 3);
        _2 = (int)SEQ_PTR(_26include_matrix_11145);
        if (!IS_ATOM_INT(_32102)){
            _32103 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32102)->dbl));
        }
        else{
            _32103 = (int)*(((s1_ptr)_2)->base + _32102);
        }
        _2 = (int)SEQ_PTR(_32103);
        _32104 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
        _32103 = NOVALUE;
        if (IS_ATOM_INT(_32104)) {
            _32105 = (_32104 == 0);
        }
        else {
            _32105 = binary_op(EQUALS, _32104, 0);
        }
        _32104 = NOVALUE;
        if (IS_ATOM_INT(_32105)) {
            if (_32105 == 0) {
                goto L3; // [68] 86
            }
        }
        else {
            if (DBL_PTR(_32105)->dbl == 0.0) {
                goto L3; // [68] 86
            }
        }
        _32107 = (_unincluded_ok_63796 == 0);
        if (_32107 == 0)
        {
            DeRef(_32107);
            _32107 = NOVALUE;
            goto L3; // [76] 86
        }
        else{
            DeRef(_32107);
            _32107 = NOVALUE;
        }

        /** 			continue*/
        DeRef(_fr_63803);
        _fr_63803 = NOVALUE;
        DeRef(_tok_63816);
        _tok_63816 = NOVALUE;
        goto L4; // [83] 486
L3: 

        /** 		token tok = find_reference( fr )*/
        RefDS(_fr_63803);
        _0 = _tok_63816;
        _tok_63816 = _29find_reference(_fr_63803);
        DeRef(_0);

        /** 		if tok[T_ID] = IGNORED then*/
        _2 = (int)SEQ_PTR(_tok_63816);
        _32109 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(NOTEQ, _32109, 509)){
            _32109 = NOVALUE;
            goto L5; // [102] 119
        }
        _32109 = NOVALUE;

        /** 			errors &= ref*/
        Append(&_errors_63797, _errors_63797, _ref_63801);

        /** 			continue*/
        DeRefDS(_fr_63803);
        _fr_63803 = NOVALUE;
        DeRef(_tok_63816);
        _tok_63816 = NOVALUE;
        goto L4; // [116] 486
L5: 

        /** 		integer code_sub = fr[FR_SUBPROG]*/
        _2 = (int)SEQ_PTR(_fr_63803);
        _code_sub_63824 = (int)*(((s1_ptr)_2)->base + 4);
        if (!IS_ATOM_INT(_code_sub_63824))
        _code_sub_63824 = (long)DBL_PTR(_code_sub_63824)->dbl;

        /** 		integer fr_type  = fr[FR_TYPE]*/
        _2 = (int)SEQ_PTR(_fr_63803);
        _fr_type_63826 = (int)*(((s1_ptr)_2)->base + 1);
        if (!IS_ATOM_INT(_fr_type_63826))
        _fr_type_63826 = (long)DBL_PTR(_fr_type_63826)->dbl;

        /** 		integer sym_tok*/

        /** 		switch fr_type label "fr_type" do*/
        _0 = _fr_type_63826;
        switch ( _0 ){ 

            /** 			case PROC, FUNC then*/
            case 27:
            case 501:

            /** 				sym_tok = SymTab[tok[T_SYM]][S_TOKEN]*/
            _2 = (int)SEQ_PTR(_tok_63816);
            _32117 = (int)*(((s1_ptr)_2)->base + 2);
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            if (!IS_ATOM_INT(_32117)){
                _32118 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32117)->dbl));
            }
            else{
                _32118 = (int)*(((s1_ptr)_2)->base + _32117);
            }
            _2 = (int)SEQ_PTR(_32118);
            if (!IS_ATOM_INT(_25S_TOKEN_11918)){
                _sym_tok_63828 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
            }
            else{
                _sym_tok_63828 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
            }
            if (!IS_ATOM_INT(_sym_tok_63828)){
                _sym_tok_63828 = (long)DBL_PTR(_sym_tok_63828)->dbl;
            }
            _32118 = NOVALUE;

            /** 				if sym_tok = TYPE then*/
            if (_sym_tok_63828 != 504)
            goto L6; // [176] 190

            /** 					sym_tok = FUNC*/
            _sym_tok_63828 = 501;
L6: 

            /** 				if sym_tok != fr_type then*/
            if (_sym_tok_63828 == _fr_type_63826)
            goto L7; // [192] 226

            /** 					if sym_tok != FUNC and fr_type != PROC then*/
            _32122 = (_sym_tok_63828 != 501);
            if (_32122 == 0) {
                goto L8; // [204] 225
            }
            _32124 = (_fr_type_63826 != 27);
            if (_32124 == 0)
            {
                DeRef(_32124);
                _32124 = NOVALUE;
                goto L8; // [215] 225
            }
            else{
                DeRef(_32124);
                _32124 = NOVALUE;
            }

            /** 						forward_error( tok, ref )*/
            Ref(_tok_63816);
            _29forward_error(_tok_63816, _ref_63801);
L8: 
L7: 

            /** 				switch sym_tok do*/
            _0 = _sym_tok_63828;
            switch ( _0 ){ 

                /** 					case PROC, FUNC then*/
                case 27:
                case 501:

                /** 						patch_forward_call( tok, ref )*/
                Ref(_tok_63816);
                _29patch_forward_call(_tok_63816, _ref_63801);

                /** 						break "fr_type"*/
                goto L9; // [247] 456
                goto L9; // [249] 456

                /** 					case else*/
                default:

                /** 						forward_error( tok, ref )*/
                Ref(_tok_63816);
                _29forward_error(_tok_63816, _ref_63801);
            ;}            goto L9; // [262] 456

            /** 			case VARIABLE then*/
            case -100:

            /** 				sym_tok = SymTab[tok[T_SYM]][S_TOKEN]*/
            _2 = (int)SEQ_PTR(_tok_63816);
            _32127 = (int)*(((s1_ptr)_2)->base + 2);
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            if (!IS_ATOM_INT(_32127)){
                _32128 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32127)->dbl));
            }
            else{
                _32128 = (int)*(((s1_ptr)_2)->base + _32127);
            }
            _2 = (int)SEQ_PTR(_32128);
            if (!IS_ATOM_INT(_25S_TOKEN_11918)){
                _sym_tok_63828 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
            }
            else{
                _sym_tok_63828 = (int)*(((s1_ptr)_2)->base + _25S_TOKEN_11918);
            }
            if (!IS_ATOM_INT(_sym_tok_63828)){
                _sym_tok_63828 = (long)DBL_PTR(_sym_tok_63828)->dbl;
            }
            _32128 = NOVALUE;

            /** 				if SymTab[tok[T_SYM]][S_SCOPE] = SC_UNDEFINED then*/
            _2 = (int)SEQ_PTR(_tok_63816);
            _32130 = (int)*(((s1_ptr)_2)->base + 2);
            _2 = (int)SEQ_PTR(_26SymTab_11138);
            if (!IS_ATOM_INT(_32130)){
                _32131 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32130)->dbl));
            }
            else{
                _32131 = (int)*(((s1_ptr)_2)->base + _32130);
            }
            _2 = (int)SEQ_PTR(_32131);
            _32132 = (int)*(((s1_ptr)_2)->base + 4);
            _32131 = NOVALUE;
            if (binary_op_a(NOTEQ, _32132, 9)){
                _32132 = NOVALUE;
                goto LA; // [312] 329
            }
            _32132 = NOVALUE;

            /** 					errors &= ref*/
            Append(&_errors_63797, _errors_63797, _ref_63801);

            /** 					continue*/
            DeRef(_fr_63803);
            _fr_63803 = NOVALUE;
            DeRef(_tok_63816);
            _tok_63816 = NOVALUE;
            goto L4; // [326] 486
LA: 

            /** 				switch sym_tok do*/
            _0 = _sym_tok_63828;
            switch ( _0 ){ 

                /** 					case CONSTANT, ENUM, VARIABLE then*/
                case 417:
                case 427:
                case -100:

                /** 						patch_forward_variable( tok, ref )*/
                Ref(_tok_63816);
                _29patch_forward_variable(_tok_63816, _ref_63801);

                /** 						break "fr_type"*/
                goto L9; // [352] 456
                goto L9; // [354] 456

                /** 					case else*/
                default:

                /** 						forward_error( tok, ref )*/
                Ref(_tok_63816);
                _29forward_error(_tok_63816, _ref_63801);
            ;}            goto L9; // [367] 456

            /** 			case TYPE_CHECK then*/
            case 65:

            /** 				patch_forward_type_check( tok, ref )*/
            Ref(_tok_63816);
            _29patch_forward_type_check(_tok_63816, _ref_63801);
            goto L9; // [379] 456

            /** 			case GLOBAL_INIT_CHECK then*/
            case 109:

            /** 				patch_forward_init_check( tok, ref )*/
            Ref(_tok_63816);
            _29patch_forward_init_check(_tok_63816, _ref_63801);
            goto L9; // [391] 456

            /** 			case CASE then*/
            case 186:

            /** 				patch_forward_case( tok, ref )*/
            Ref(_tok_63816);
            _29patch_forward_case(_tok_63816, _ref_63801);
            goto L9; // [403] 456

            /** 			case TYPE then*/
            case 504:

            /** 				patch_forward_type( tok, ref )*/
            Ref(_tok_63816);
            _29patch_forward_type(_tok_63816, _ref_63801);
            goto L9; // [415] 456

            /** 			case GOTO then*/
            case 188:

            /** 				patch_forward_goto( tok, ref )*/
            Ref(_tok_63816);
            _29patch_forward_goto(_tok_63816, _ref_63801);
            goto L9; // [427] 456

            /** 			case else*/
            default:

            /** 				InternalErr( 263, {fr[FR_TYPE], fr[FR_NAME]})*/
            _2 = (int)SEQ_PTR(_fr_63803);
            _32137 = (int)*(((s1_ptr)_2)->base + 1);
            _2 = (int)SEQ_PTR(_fr_63803);
            _32138 = (int)*(((s1_ptr)_2)->base + 2);
            Ref(_32138);
            Ref(_32137);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _32137;
            ((int *)_2)[2] = _32138;
            _32139 = MAKE_SEQ(_1);
            _32138 = NOVALUE;
            _32137 = NOVALUE;
            _43InternalErr(263, _32139);
            _32139 = NOVALUE;
        ;}L9: 

        /** 		if report_errors and sequence( forward_references[ref] ) then*/
        if (_report_errors_63795 == 0) {
            goto LB; // [458] 482
        }
        _2 = (int)SEQ_PTR(_29forward_references_62545);
        _32141 = (int)*(((s1_ptr)_2)->base + _ref_63801);
        _32142 = IS_SEQUENCE(_32141);
        _32141 = NOVALUE;
        if (_32142 == 0)
        {
            _32142 = NOVALUE;
            goto LB; // [472] 482
        }
        else{
            _32142 = NOVALUE;
        }

        /** 			errors &= ref*/
        Append(&_errors_63797, _errors_63797, _ref_63801);
LB: 
        DeRef(_fr_63803);
        _fr_63803 = NOVALUE;
        DeRef(_tok_63816);
        _tok_63816 = NOVALUE;

        /** 	end for*/
L4: 
        _ar_63799 = _ar_63799 + -1;
        goto L1; // [486] 26
L2: 
        ;
    }

    /** 	return errors*/
    DeRefDS(_refs_63794);
    _32102 = NOVALUE;
    _32117 = NOVALUE;
    DeRef(_32105);
    _32105 = NOVALUE;
    DeRef(_32122);
    _32122 = NOVALUE;
    _32127 = NOVALUE;
    _32130 = NOVALUE;
    return _errors_63797;
    ;
}


int _29file_name_based_symindex_compare(int _si1_63906, int _si2_63907)
{
    int _fn1_63928 = NOVALUE;
    int _fn2_63933 = NOVALUE;
    int _32171 = NOVALUE;
    int _32170 = NOVALUE;
    int _32169 = NOVALUE;
    int _32168 = NOVALUE;
    int _32167 = NOVALUE;
    int _32166 = NOVALUE;
    int _32165 = NOVALUE;
    int _32164 = NOVALUE;
    int _32163 = NOVALUE;
    int _32162 = NOVALUE;
    int _32161 = NOVALUE;
    int _32160 = NOVALUE;
    int _32158 = NOVALUE;
    int _32156 = NOVALUE;
    int _32155 = NOVALUE;
    int _32154 = NOVALUE;
    int _32153 = NOVALUE;
    int _32152 = NOVALUE;
    int _32151 = NOVALUE;
    int _32150 = NOVALUE;
    int _32149 = NOVALUE;
    int _32148 = NOVALUE;
    int _32147 = NOVALUE;
    int _32145 = NOVALUE;
    int _32144 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_si1_63906)) {
        _1 = (long)(DBL_PTR(_si1_63906)->dbl);
        if (UNIQUE(DBL_PTR(_si1_63906)) && (DBL_PTR(_si1_63906)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_si1_63906);
        _si1_63906 = _1;
    }
    if (!IS_ATOM_INT(_si2_63907)) {
        _1 = (long)(DBL_PTR(_si2_63907)->dbl);
        if (UNIQUE(DBL_PTR(_si2_63907)) && (DBL_PTR(_si2_63907)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_si2_63907);
        _si2_63907 = _1;
    }

    /** 	if not symtab_index(si1) or not symtab_index(si2) then*/
    _32144 = _25symtab_index(_si1_63906);
    if (IS_ATOM_INT(_32144)) {
        _32145 = (_32144 == 0);
    }
    else {
        _32145 = unary_op(NOT, _32144);
    }
    DeRef(_32144);
    _32144 = NOVALUE;
    if (IS_ATOM_INT(_32145)) {
        if (_32145 != 0) {
            goto L1; // [14] 30
        }
    }
    else {
        if (DBL_PTR(_32145)->dbl != 0.0) {
            goto L1; // [14] 30
        }
    }
    _32147 = _25symtab_index(_si2_63907);
    if (IS_ATOM_INT(_32147)) {
        _32148 = (_32147 == 0);
    }
    else {
        _32148 = unary_op(NOT, _32147);
    }
    DeRef(_32147);
    _32147 = NOVALUE;
    if (_32148 == 0) {
        DeRef(_32148);
        _32148 = NOVALUE;
        goto L2; // [26] 37
    }
    else {
        if (!IS_ATOM_INT(_32148) && DBL_PTR(_32148)->dbl == 0.0){
            DeRef(_32148);
            _32148 = NOVALUE;
            goto L2; // [26] 37
        }
        DeRef(_32148);
        _32148 = NOVALUE;
    }
    DeRef(_32148);
    _32148 = NOVALUE;
L1: 

    /** 		return 1 -- put non symbols last*/
    DeRef(_32145);
    _32145 = NOVALUE;
    return 1;
L2: 

    /** 	if S_FILE_NO <= length(SymTab[si1]) and S_FILE_NO <= length(SymTab[si2]) then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _32149 = (int)*(((s1_ptr)_2)->base + _si1_63906);
    if (IS_SEQUENCE(_32149)){
            _32150 = SEQ_PTR(_32149)->length;
    }
    else {
        _32150 = 1;
    }
    _32149 = NOVALUE;
    if (IS_ATOM_INT(_25S_FILE_NO_11909)) {
        _32151 = (_25S_FILE_NO_11909 <= _32150);
    }
    else {
        _32151 = binary_op(LESSEQ, _25S_FILE_NO_11909, _32150);
    }
    _32150 = NOVALUE;
    if (IS_ATOM_INT(_32151)) {
        if (_32151 == 0) {
            goto L3; // [54] 186
        }
    }
    else {
        if (DBL_PTR(_32151)->dbl == 0.0) {
            goto L3; // [54] 186
        }
    }
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _32153 = (int)*(((s1_ptr)_2)->base + _si2_63907);
    if (IS_SEQUENCE(_32153)){
            _32154 = SEQ_PTR(_32153)->length;
    }
    else {
        _32154 = 1;
    }
    _32153 = NOVALUE;
    if (IS_ATOM_INT(_25S_FILE_NO_11909)) {
        _32155 = (_25S_FILE_NO_11909 <= _32154);
    }
    else {
        _32155 = binary_op(LESSEQ, _25S_FILE_NO_11909, _32154);
    }
    _32154 = NOVALUE;
    if (_32155 == 0) {
        DeRef(_32155);
        _32155 = NOVALUE;
        goto L3; // [74] 186
    }
    else {
        if (!IS_ATOM_INT(_32155) && DBL_PTR(_32155)->dbl == 0.0){
            DeRef(_32155);
            _32155 = NOVALUE;
            goto L3; // [74] 186
        }
        DeRef(_32155);
        _32155 = NOVALUE;
    }
    DeRef(_32155);
    _32155 = NOVALUE;

    /** 		integer fn1 = SymTab[si1][S_FILE_NO], fn2 = SymTab[si2][S_FILE_NO]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _32156 = (int)*(((s1_ptr)_2)->base + _si1_63906);
    _2 = (int)SEQ_PTR(_32156);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _fn1_63928 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _fn1_63928 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    if (!IS_ATOM_INT(_fn1_63928)){
        _fn1_63928 = (long)DBL_PTR(_fn1_63928)->dbl;
    }
    _32156 = NOVALUE;
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _32158 = (int)*(((s1_ptr)_2)->base + _si2_63907);
    _2 = (int)SEQ_PTR(_32158);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _fn2_63933 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _fn2_63933 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    if (!IS_ATOM_INT(_fn2_63933)){
        _fn2_63933 = (long)DBL_PTR(_fn2_63933)->dbl;
    }
    _32158 = NOVALUE;

    /** 		if find(1,{fn1,fn2} > length(known_files) or {fn1,fn2} <= 0) then*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _fn1_63928;
    ((int *)_2)[2] = _fn2_63933;
    _32160 = MAKE_SEQ(_1);
    if (IS_SEQUENCE(_26known_files_11139)){
            _32161 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _32161 = 1;
    }
    _32162 = binary_op(GREATER, _32160, _32161);
    DeRefDS(_32160);
    _32160 = NOVALUE;
    _32161 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _fn1_63928;
    ((int *)_2)[2] = _fn2_63933;
    _32163 = MAKE_SEQ(_1);
    _32164 = binary_op(LESSEQ, _32163, 0);
    DeRefDS(_32163);
    _32163 = NOVALUE;
    _32165 = binary_op(OR, _32162, _32164);
    DeRefDS(_32162);
    _32162 = NOVALUE;
    DeRefDS(_32164);
    _32164 = NOVALUE;
    _32166 = find_from(1, _32165, 1);
    DeRefDS(_32165);
    _32165 = NOVALUE;
    if (_32166 == 0)
    {
        _32166 = NOVALUE;
        goto L4; // [139] 149
    }
    else{
        _32166 = NOVALUE;
    }

    /** 			return 1*/
    DeRef(_32145);
    _32145 = NOVALUE;
    _32149 = NOVALUE;
    DeRef(_32151);
    _32151 = NOVALUE;
    _32153 = NOVALUE;
    return 1;
L4: 

    /** 		return compare(abbreviate_path(known_files[fn1]),*/
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _32167 = (int)*(((s1_ptr)_2)->base + _fn1_63928);
    Ref(_32167);
    RefDS(_22682);
    _32168 = _9abbreviate_path(_32167, _22682);
    _32167 = NOVALUE;
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _32169 = (int)*(((s1_ptr)_2)->base + _fn2_63933);
    Ref(_32169);
    RefDS(_22682);
    _32170 = _9abbreviate_path(_32169, _22682);
    _32169 = NOVALUE;
    if (IS_ATOM_INT(_32168) && IS_ATOM_INT(_32170)){
        _32171 = (_32168 < _32170) ? -1 : (_32168 > _32170);
    }
    else{
        _32171 = compare(_32168, _32170);
    }
    DeRef(_32168);
    _32168 = NOVALUE;
    DeRef(_32170);
    _32170 = NOVALUE;
    DeRef(_32145);
    _32145 = NOVALUE;
    _32149 = NOVALUE;
    DeRef(_32151);
    _32151 = NOVALUE;
    _32153 = NOVALUE;
    return _32171;
    goto L5; // [183] 193
L3: 

    /** 		return 1 -- put non-names last*/
    DeRef(_32145);
    _32145 = NOVALUE;
    _32149 = NOVALUE;
    DeRef(_32151);
    _32151 = NOVALUE;
    _32153 = NOVALUE;
    return 1;
L5: 
    ;
}


void _29Resolve_forward_references(int _report_errors_63959)
{
    int _errors_63960 = NOVALUE;
    int _unincluded_ok_63961 = NOVALUE;
    int _msg_64022 = NOVALUE;
    int _errloc_64023 = NOVALUE;
    int _ref_64028 = NOVALUE;
    int _tok_64044 = NOVALUE;
    int _THIS_SCOPE_64046 = NOVALUE;
    int _THESE_GLOBALS_64047 = NOVALUE;
    int _syms_64105 = NOVALUE;
    int _s_64126 = NOVALUE;
    int _32302 = NOVALUE;
    int _32300 = NOVALUE;
    int _32295 = NOVALUE;
    int _32292 = NOVALUE;
    int _32290 = NOVALUE;
    int _32289 = NOVALUE;
    int _32288 = NOVALUE;
    int _32287 = NOVALUE;
    int _32286 = NOVALUE;
    int _32285 = NOVALUE;
    int _32284 = NOVALUE;
    int _32282 = NOVALUE;
    int _32281 = NOVALUE;
    int _32280 = NOVALUE;
    int _32278 = NOVALUE;
    int _32276 = NOVALUE;
    int _32275 = NOVALUE;
    int _32274 = NOVALUE;
    int _32273 = NOVALUE;
    int _32272 = NOVALUE;
    int _32271 = NOVALUE;
    int _32268 = NOVALUE;
    int _32264 = NOVALUE;
    int _32263 = NOVALUE;
    int _32262 = NOVALUE;
    int _32261 = NOVALUE;
    int _32260 = NOVALUE;
    int _32259 = NOVALUE;
    int _32256 = NOVALUE;
    int _32255 = NOVALUE;
    int _32254 = NOVALUE;
    int _32253 = NOVALUE;
    int _32252 = NOVALUE;
    int _32251 = NOVALUE;
    int _32248 = NOVALUE;
    int _32247 = NOVALUE;
    int _32246 = NOVALUE;
    int _32245 = NOVALUE;
    int _32244 = NOVALUE;
    int _32243 = NOVALUE;
    int _32242 = NOVALUE;
    int _32241 = NOVALUE;
    int _32240 = NOVALUE;
    int _32239 = NOVALUE;
    int _32236 = NOVALUE;
    int _32234 = NOVALUE;
    int _32231 = NOVALUE;
    int _32229 = NOVALUE;
    int _32227 = NOVALUE;
    int _32226 = NOVALUE;
    int _32224 = NOVALUE;
    int _32223 = NOVALUE;
    int _32222 = NOVALUE;
    int _32221 = NOVALUE;
    int _32220 = NOVALUE;
    int _32218 = NOVALUE;
    int _32217 = NOVALUE;
    int _32215 = NOVALUE;
    int _32214 = NOVALUE;
    int _32212 = NOVALUE;
    int _32211 = NOVALUE;
    int _32209 = NOVALUE;
    int _32208 = NOVALUE;
    int _32207 = NOVALUE;
    int _32206 = NOVALUE;
    int _32205 = NOVALUE;
    int _32204 = NOVALUE;
    int _32203 = NOVALUE;
    int _32202 = NOVALUE;
    int _32201 = NOVALUE;
    int _32200 = NOVALUE;
    int _32199 = NOVALUE;
    int _32198 = NOVALUE;
    int _32197 = NOVALUE;
    int _32196 = NOVALUE;
    int _32195 = NOVALUE;
    int _32194 = NOVALUE;
    int _32192 = NOVALUE;
    int _32191 = NOVALUE;
    int _32190 = NOVALUE;
    int _32189 = NOVALUE;
    int _32187 = NOVALUE;
    int _32186 = NOVALUE;
    int _32184 = NOVALUE;
    int _32183 = NOVALUE;
    int _32182 = NOVALUE;
    int _32181 = NOVALUE;
    int _32179 = NOVALUE;
    int _32178 = NOVALUE;
    int _32177 = NOVALUE;
    int _32176 = NOVALUE;
    int _32174 = NOVALUE;
    int _32173 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_report_errors_63959)) {
        _1 = (long)(DBL_PTR(_report_errors_63959)->dbl);
        if (UNIQUE(DBL_PTR(_report_errors_63959)) && (DBL_PTR(_report_errors_63959)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_report_errors_63959);
        _report_errors_63959 = _1;
    }

    /** 	sequence errors = {}*/
    RefDS(_22682);
    DeRef(_errors_63960);
    _errors_63960 = _22682;

    /** 	integer unincluded_ok = get_resolve_unincluded_globals()*/
    _unincluded_ok_63961 = _52get_resolve_unincluded_globals();
    if (!IS_ATOM_INT(_unincluded_ok_63961)) {
        _1 = (long)(DBL_PTR(_unincluded_ok_63961)->dbl);
        if (UNIQUE(DBL_PTR(_unincluded_ok_63961)) && (DBL_PTR(_unincluded_ok_63961)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_unincluded_ok_63961);
        _unincluded_ok_63961 = _1;
    }

    /** 	if length( active_references ) < length( known_files ) then*/
    if (IS_SEQUENCE(_29active_references_62547)){
            _32173 = SEQ_PTR(_29active_references_62547)->length;
    }
    else {
        _32173 = 1;
    }
    if (IS_SEQUENCE(_26known_files_11139)){
            _32174 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _32174 = 1;
    }
    if (_32173 >= _32174)
    goto L1; // [29] 86

    /** 		active_references &= repeat( {}, length( known_files ) - length( active_references ) )*/
    if (IS_SEQUENCE(_26known_files_11139)){
            _32176 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _32176 = 1;
    }
    if (IS_SEQUENCE(_29active_references_62547)){
            _32177 = SEQ_PTR(_29active_references_62547)->length;
    }
    else {
        _32177 = 1;
    }
    _32178 = _32176 - _32177;
    _32176 = NOVALUE;
    _32177 = NOVALUE;
    _32179 = Repeat(_22682, _32178);
    _32178 = NOVALUE;
    Concat((object_ptr)&_29active_references_62547, _29active_references_62547, _32179);
    DeRefDS(_32179);
    _32179 = NOVALUE;

    /** 		active_subprogs   &= repeat( {}, length( known_files ) - length( active_subprogs ) )*/
    if (IS_SEQUENCE(_26known_files_11139)){
            _32181 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _32181 = 1;
    }
    if (IS_SEQUENCE(_29active_subprogs_62546)){
            _32182 = SEQ_PTR(_29active_subprogs_62546)->length;
    }
    else {
        _32182 = 1;
    }
    _32183 = _32181 - _32182;
    _32181 = NOVALUE;
    _32182 = NOVALUE;
    _32184 = Repeat(_22682, _32183);
    _32183 = NOVALUE;
    Concat((object_ptr)&_29active_subprogs_62546, _29active_subprogs_62546, _32184);
    DeRefDS(_32184);
    _32184 = NOVALUE;
L1: 

    /** 	if length( toplevel_references ) < length( known_files ) then*/
    if (IS_SEQUENCE(_29toplevel_references_62548)){
            _32186 = SEQ_PTR(_29toplevel_references_62548)->length;
    }
    else {
        _32186 = 1;
    }
    if (IS_SEQUENCE(_26known_files_11139)){
            _32187 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _32187 = 1;
    }
    if (_32186 >= _32187)
    goto L2; // [98] 129

    /** 		toplevel_references &= repeat( {}, length( known_files ) - length( toplevel_references ) )*/
    if (IS_SEQUENCE(_26known_files_11139)){
            _32189 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _32189 = 1;
    }
    if (IS_SEQUENCE(_29toplevel_references_62548)){
            _32190 = SEQ_PTR(_29toplevel_references_62548)->length;
    }
    else {
        _32190 = 1;
    }
    _32191 = _32189 - _32190;
    _32189 = NOVALUE;
    _32190 = NOVALUE;
    _32192 = Repeat(_22682, _32191);
    _32191 = NOVALUE;
    Concat((object_ptr)&_29toplevel_references_62548, _29toplevel_references_62548, _32192);
    DeRefDS(_32192);
    _32192 = NOVALUE;
L2: 

    /** 	for i = 1 to length( active_subprogs ) do*/
    if (IS_SEQUENCE(_29active_subprogs_62546)){
            _32194 = SEQ_PTR(_29active_subprogs_62546)->length;
    }
    else {
        _32194 = 1;
    }
    {
        int _i_63993;
        _i_63993 = 1;
L3: 
        if (_i_63993 > _32194){
            goto L4; // [136] 280
        }

        /** 		if (length( active_subprogs[i] ) or length(toplevel_references[i])) */
        _2 = (int)SEQ_PTR(_29active_subprogs_62546);
        _32195 = (int)*(((s1_ptr)_2)->base + _i_63993);
        if (IS_SEQUENCE(_32195)){
                _32196 = SEQ_PTR(_32195)->length;
        }
        else {
            _32196 = 1;
        }
        _32195 = NOVALUE;
        if (_32196 != 0) {
            _32197 = 1;
            goto L5; // [154] 171
        }
        _2 = (int)SEQ_PTR(_29toplevel_references_62548);
        _32198 = (int)*(((s1_ptr)_2)->base + _i_63993);
        if (IS_SEQUENCE(_32198)){
                _32199 = SEQ_PTR(_32198)->length;
        }
        else {
            _32199 = 1;
        }
        _32198 = NOVALUE;
        _32197 = (_32199 != 0);
L5: 
        if (_32197 == 0) {
            goto L6; // [171] 273
        }
        _32201 = (_i_63993 == _25current_file_no_12262);
        if (_32201 != 0) {
            _32202 = 1;
            goto L7; // [181] 195
        }
        _2 = (int)SEQ_PTR(_26finished_files_11141);
        _32203 = (int)*(((s1_ptr)_2)->base + _i_63993);
        _32202 = (_32203 != 0);
L7: 
        if (_32202 != 0) {
            DeRef(_32204);
            _32204 = 1;
            goto L8; // [195] 203
        }
        _32204 = (_unincluded_ok_63961 != 0);
L8: 
        if (_32204 == 0)
        {
            _32204 = NOVALUE;
            goto L6; // [204] 273
        }
        else{
            _32204 = NOVALUE;
        }

        /** 			for j = length( active_references[i] ) to 1 by -1 do*/
        _2 = (int)SEQ_PTR(_29active_references_62547);
        _32205 = (int)*(((s1_ptr)_2)->base + _i_63993);
        if (IS_SEQUENCE(_32205)){
                _32206 = SEQ_PTR(_32205)->length;
        }
        else {
            _32206 = 1;
        }
        _32205 = NOVALUE;
        {
            int _j_64009;
            _j_64009 = _32206;
L9: 
            if (_j_64009 < 1){
                goto LA; // [218] 254
            }

            /** 				errors &= resolve_file( active_references[i][j], report_errors, unincluded_ok )*/
            _2 = (int)SEQ_PTR(_29active_references_62547);
            _32207 = (int)*(((s1_ptr)_2)->base + _i_63993);
            _2 = (int)SEQ_PTR(_32207);
            _32208 = (int)*(((s1_ptr)_2)->base + _j_64009);
            _32207 = NOVALUE;
            Ref(_32208);
            _32209 = _29resolve_file(_32208, _report_errors_63959, _unincluded_ok_63961);
            _32208 = NOVALUE;
            if (IS_SEQUENCE(_errors_63960) && IS_ATOM(_32209)) {
                Ref(_32209);
                Append(&_errors_63960, _errors_63960, _32209);
            }
            else if (IS_ATOM(_errors_63960) && IS_SEQUENCE(_32209)) {
            }
            else {
                Concat((object_ptr)&_errors_63960, _errors_63960, _32209);
            }
            DeRef(_32209);
            _32209 = NOVALUE;

            /** 			end for*/
            _j_64009 = _j_64009 + -1;
            goto L9; // [249] 225
LA: 
            ;
        }

        /** 			errors &= resolve_file( toplevel_references[i], report_errors, unincluded_ok )*/
        _2 = (int)SEQ_PTR(_29toplevel_references_62548);
        _32211 = (int)*(((s1_ptr)_2)->base + _i_63993);
        Ref(_32211);
        _32212 = _29resolve_file(_32211, _report_errors_63959, _unincluded_ok_63961);
        _32211 = NOVALUE;
        if (IS_SEQUENCE(_errors_63960) && IS_ATOM(_32212)) {
            Ref(_32212);
            Append(&_errors_63960, _errors_63960, _32212);
        }
        else if (IS_ATOM(_errors_63960) && IS_SEQUENCE(_32212)) {
        }
        else {
            Concat((object_ptr)&_errors_63960, _errors_63960, _32212);
        }
        DeRef(_32212);
        _32212 = NOVALUE;
L6: 

        /** 	end for*/
        _i_63993 = _i_63993 + 1;
        goto L3; // [275] 143
L4: 
        ;
    }

    /** 	if report_errors and length( errors ) then*/
    if (_report_errors_63959 == 0) {
        goto LB; // [282] 900
    }
    if (IS_SEQUENCE(_errors_63960)){
            _32215 = SEQ_PTR(_errors_63960)->length;
    }
    else {
        _32215 = 1;
    }
    if (_32215 == 0)
    {
        _32215 = NOVALUE;
        goto LB; // [290] 900
    }
    else{
        _32215 = NOVALUE;
    }

    /** 		sequence msg = ""*/
    RefDS(_22682);
    DeRefi(_msg_64022);
    _msg_64022 = _22682;

    /** 		sequence errloc = "Internal Error - Unknown Error Message"*/
    RefDS(_32216);
    DeRefi(_errloc_64023);
    _errloc_64023 = _32216;

    /** 		for e = length(errors) to 1 by -1 do*/
    if (IS_SEQUENCE(_errors_63960)){
            _32217 = SEQ_PTR(_errors_63960)->length;
    }
    else {
        _32217 = 1;
    }
    {
        int _e_64026;
        _e_64026 = _32217;
LC: 
        if (_e_64026 < 1){
            goto LD; // [312] 874
        }

        /** 			sequence ref = forward_references[errors[e]]*/
        _2 = (int)SEQ_PTR(_errors_63960);
        _32218 = (int)*(((s1_ptr)_2)->base + _e_64026);
        DeRef(_ref_64028);
        _2 = (int)SEQ_PTR(_29forward_references_62545);
        if (!IS_ATOM_INT(_32218)){
            _ref_64028 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32218)->dbl));
        }
        else{
            _ref_64028 = (int)*(((s1_ptr)_2)->base + _32218);
        }
        Ref(_ref_64028);

        /** 			if (ref[FR_TYPE] = TYPE_CHECK and ref[FR_OP] = TYPE_CHECK) or ref[FR_TYPE] = GLOBAL_INIT_CHECK then*/
        _2 = (int)SEQ_PTR(_ref_64028);
        _32220 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_32220)) {
            _32221 = (_32220 == 65);
        }
        else {
            _32221 = binary_op(EQUALS, _32220, 65);
        }
        _32220 = NOVALUE;
        if (IS_ATOM_INT(_32221)) {
            if (_32221 == 0) {
                DeRef(_32222);
                _32222 = 0;
                goto LE; // [347] 367
            }
        }
        else {
            if (DBL_PTR(_32221)->dbl == 0.0) {
                DeRef(_32222);
                _32222 = 0;
                goto LE; // [347] 367
            }
        }
        _2 = (int)SEQ_PTR(_ref_64028);
        _32223 = (int)*(((s1_ptr)_2)->base + 10);
        if (IS_ATOM_INT(_32223)) {
            _32224 = (_32223 == 65);
        }
        else {
            _32224 = binary_op(EQUALS, _32223, 65);
        }
        _32223 = NOVALUE;
        DeRef(_32222);
        if (IS_ATOM_INT(_32224))
        _32222 = (_32224 != 0);
        else
        _32222 = DBL_PTR(_32224)->dbl != 0.0;
LE: 
        if (_32222 != 0) {
            goto LF; // [367] 388
        }
        _2 = (int)SEQ_PTR(_ref_64028);
        _32226 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_32226)) {
            _32227 = (_32226 == 109);
        }
        else {
            _32227 = binary_op(EQUALS, _32226, 109);
        }
        _32226 = NOVALUE;
        if (_32227 == 0) {
            DeRef(_32227);
            _32227 = NOVALUE;
            goto L10; // [384] 397
        }
        else {
            if (!IS_ATOM_INT(_32227) && DBL_PTR(_32227)->dbl == 0.0){
                DeRef(_32227);
                _32227 = NOVALUE;
                goto L10; // [384] 397
            }
            DeRef(_32227);
            _32227 = NOVALUE;
        }
        DeRef(_32227);
        _32227 = NOVALUE;
LF: 

        /** 				continue*/
        DeRef(_ref_64028);
        _ref_64028 = NOVALUE;
        goto L11; // [392] 869
        goto L12; // [394] 827
L10: 

        /** 				object tok = find_reference(ref)*/
        RefDS(_ref_64028);
        _0 = _tok_64044;
        _tok_64044 = _29find_reference(_ref_64028);
        DeRef(_0);

        /** 				integer THIS_SCOPE = 3*/
        _THIS_SCOPE_64046 = 3;

        /** 				integer THESE_GLOBALS = 4*/
        _THESE_GLOBALS_64047 = 4;

        /** 				if tok[T_ID] = IGNORED then*/
        _2 = (int)SEQ_PTR(_tok_64044);
        _32229 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(NOTEQ, _32229, 509)){
            _32229 = NOVALUE;
            goto L13; // [423] 798
        }
        _32229 = NOVALUE;

        /** 					switch tok[THIS_SCOPE] do*/
        _2 = (int)SEQ_PTR(_tok_64044);
        _32231 = (int)*(((s1_ptr)_2)->base + 3);
        if (IS_SEQUENCE(_32231) ){
            goto L14; // [433] 794
        }
        if(!IS_ATOM_INT(_32231)){
            if( (DBL_PTR(_32231)->dbl != (double) ((int) DBL_PTR(_32231)->dbl) ) ){
                goto L14; // [433] 794
            }
            _0 = (int) DBL_PTR(_32231)->dbl;
        }
        else {
            _0 = _32231;
        };
        _32231 = NOVALUE;
        switch ( _0 ){ 

            /** 						case SC_UNDEFINED then*/
            case 9:

            /** 							if ref[FR_QUALIFIED] != -1 then*/
            _2 = (int)SEQ_PTR(_ref_64028);
            _32234 = (int)*(((s1_ptr)_2)->base + 9);
            if (binary_op_a(EQUALS, _32234, -1)){
                _32234 = NOVALUE;
                goto L15; // [450] 580
            }
            _32234 = NOVALUE;

            /** 								if ref[FR_QUALIFIED] > 0 then*/
            _2 = (int)SEQ_PTR(_ref_64028);
            _32236 = (int)*(((s1_ptr)_2)->base + 9);
            if (binary_op_a(LESSEQ, _32236, 0)){
                _32236 = NOVALUE;
                goto L16; // [462] 535
            }
            _32236 = NOVALUE;

            /** 									errloc = sprintf("\t\'%s\' (%s:%d) was not declared in \'%s\'.\n", */
            _2 = (int)SEQ_PTR(_ref_64028);
            _32239 = (int)*(((s1_ptr)_2)->base + 2);
            _2 = (int)SEQ_PTR(_ref_64028);
            _32240 = (int)*(((s1_ptr)_2)->base + 3);
            _2 = (int)SEQ_PTR(_26known_files_11139);
            if (!IS_ATOM_INT(_32240)){
                _32241 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32240)->dbl));
            }
            else{
                _32241 = (int)*(((s1_ptr)_2)->base + _32240);
            }
            Ref(_32241);
            RefDS(_22682);
            _32242 = _9abbreviate_path(_32241, _22682);
            _32241 = NOVALUE;
            _2 = (int)SEQ_PTR(_ref_64028);
            _32243 = (int)*(((s1_ptr)_2)->base + 6);
            _2 = (int)SEQ_PTR(_ref_64028);
            _32244 = (int)*(((s1_ptr)_2)->base + 9);
            _2 = (int)SEQ_PTR(_26known_files_11139);
            if (!IS_ATOM_INT(_32244)){
                _32245 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32244)->dbl));
            }
            else{
                _32245 = (int)*(((s1_ptr)_2)->base + _32244);
            }
            Ref(_32245);
            RefDS(_22682);
            _32246 = _9abbreviate_path(_32245, _22682);
            _32245 = NOVALUE;
            _32247 = _7find_replace(92, _32246, 47, 0);
            _32246 = NOVALUE;
            _1 = NewS1(4);
            _2 = (int)((s1_ptr)_1)->base;
            Ref(_32239);
            *((int *)(_2+4)) = _32239;
            *((int *)(_2+8)) = _32242;
            Ref(_32243);
            *((int *)(_2+12)) = _32243;
            *((int *)(_2+16)) = _32247;
            _32248 = MAKE_SEQ(_1);
            _32247 = NOVALUE;
            _32243 = NOVALUE;
            _32242 = NOVALUE;
            _32239 = NOVALUE;
            DeRefi(_errloc_64023);
            _errloc_64023 = EPrintf(-9999999, _32238, _32248);
            DeRefDS(_32248);
            _32248 = NOVALUE;
            goto L17; // [532] 797
L16: 

            /** 									errloc = sprintf("\t\'%s\' (%s:%d) is not a builtin.\n", */
            _2 = (int)SEQ_PTR(_ref_64028);
            _32251 = (int)*(((s1_ptr)_2)->base + 2);
            _2 = (int)SEQ_PTR(_ref_64028);
            _32252 = (int)*(((s1_ptr)_2)->base + 3);
            _2 = (int)SEQ_PTR(_26known_files_11139);
            if (!IS_ATOM_INT(_32252)){
                _32253 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32252)->dbl));
            }
            else{
                _32253 = (int)*(((s1_ptr)_2)->base + _32252);
            }
            Ref(_32253);
            RefDS(_22682);
            _32254 = _9abbreviate_path(_32253, _22682);
            _32253 = NOVALUE;
            _2 = (int)SEQ_PTR(_ref_64028);
            _32255 = (int)*(((s1_ptr)_2)->base + 6);
            _1 = NewS1(3);
            _2 = (int)((s1_ptr)_1)->base;
            Ref(_32251);
            *((int *)(_2+4)) = _32251;
            *((int *)(_2+8)) = _32254;
            Ref(_32255);
            *((int *)(_2+12)) = _32255;
            _32256 = MAKE_SEQ(_1);
            _32255 = NOVALUE;
            _32254 = NOVALUE;
            _32251 = NOVALUE;
            DeRefi(_errloc_64023);
            _errloc_64023 = EPrintf(-9999999, _32250, _32256);
            DeRefDS(_32256);
            _32256 = NOVALUE;
            goto L17; // [577] 797
L15: 

            /** 								errloc = sprintf("\t\'%s\' (%s:%d) has not been declared.\n", */
            _2 = (int)SEQ_PTR(_ref_64028);
            _32259 = (int)*(((s1_ptr)_2)->base + 2);
            _2 = (int)SEQ_PTR(_ref_64028);
            _32260 = (int)*(((s1_ptr)_2)->base + 3);
            _2 = (int)SEQ_PTR(_26known_files_11139);
            if (!IS_ATOM_INT(_32260)){
                _32261 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32260)->dbl));
            }
            else{
                _32261 = (int)*(((s1_ptr)_2)->base + _32260);
            }
            Ref(_32261);
            RefDS(_22682);
            _32262 = _9abbreviate_path(_32261, _22682);
            _32261 = NOVALUE;
            _2 = (int)SEQ_PTR(_ref_64028);
            _32263 = (int)*(((s1_ptr)_2)->base + 6);
            _1 = NewS1(3);
            _2 = (int)((s1_ptr)_1)->base;
            Ref(_32259);
            *((int *)(_2+4)) = _32259;
            *((int *)(_2+8)) = _32262;
            Ref(_32263);
            *((int *)(_2+12)) = _32263;
            _32264 = MAKE_SEQ(_1);
            _32263 = NOVALUE;
            _32262 = NOVALUE;
            _32259 = NOVALUE;
            DeRefi(_errloc_64023);
            _errloc_64023 = EPrintf(-9999999, _32258, _32264);
            DeRefDS(_32264);
            _32264 = NOVALUE;
            goto L17; // [622] 797

            /** 						case SC_MULTIPLY_DEFINED then*/
            case 10:

            /** 							sequence syms = tok[THESE_GLOBALS] -- there should be no forward references in here.*/
            DeRef(_syms_64105);
            _2 = (int)SEQ_PTR(_tok_64044);
            _syms_64105 = (int)*(((s1_ptr)_2)->base + _THESE_GLOBALS_64047);
            Ref(_syms_64105);

            /** 							syms = custom_sort(routine_id("file_name_based_symindex_compare"), syms,, ASCENDING)*/
            _32268 = CRoutineId(1354, 29, _32267);
            RefDS(_syms_64105);
            RefDS(_22682);
            _0 = _syms_64105;
            _syms_64105 = _22custom_sort(_32268, _syms_64105, _22682, 1);
            DeRefDS(_0);
            _32268 = NOVALUE;

            /** 							errloc = sprintf("\t\'%s\' (%s:%d) has been declared more than once.\n", */
            _2 = (int)SEQ_PTR(_ref_64028);
            _32271 = (int)*(((s1_ptr)_2)->base + 2);
            _2 = (int)SEQ_PTR(_ref_64028);
            _32272 = (int)*(((s1_ptr)_2)->base + 3);
            _2 = (int)SEQ_PTR(_26known_files_11139);
            if (!IS_ATOM_INT(_32272)){
                _32273 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32272)->dbl));
            }
            else{
                _32273 = (int)*(((s1_ptr)_2)->base + _32272);
            }
            Ref(_32273);
            RefDS(_22682);
            _32274 = _9abbreviate_path(_32273, _22682);
            _32273 = NOVALUE;
            _2 = (int)SEQ_PTR(_ref_64028);
            _32275 = (int)*(((s1_ptr)_2)->base + 6);
            _1 = NewS1(3);
            _2 = (int)((s1_ptr)_1)->base;
            Ref(_32271);
            *((int *)(_2+4)) = _32271;
            *((int *)(_2+8)) = _32274;
            Ref(_32275);
            *((int *)(_2+12)) = _32275;
            _32276 = MAKE_SEQ(_1);
            _32275 = NOVALUE;
            _32274 = NOVALUE;
            _32271 = NOVALUE;
            DeRefi(_errloc_64023);
            _errloc_64023 = EPrintf(-9999999, _32270, _32276);
            DeRefDS(_32276);
            _32276 = NOVALUE;

            /** 							for si = 1 to length(syms) do*/
            if (IS_SEQUENCE(_syms_64105)){
                    _32278 = SEQ_PTR(_syms_64105)->length;
            }
            else {
                _32278 = 1;
            }
            {
                int _si_64123;
                _si_64123 = 1;
L18: 
                if (_si_64123 > _32278){
                    goto L19; // [700] 788
                }

                /** 								symtab_index s = syms[si] */
                _2 = (int)SEQ_PTR(_syms_64105);
                _s_64126 = (int)*(((s1_ptr)_2)->base + _si_64123);
                if (!IS_ATOM_INT(_s_64126)){
                    _s_64126 = (long)DBL_PTR(_s_64126)->dbl;
                }

                /** 								if equal(ref[FR_NAME], sym_name(s)) then*/
                _2 = (int)SEQ_PTR(_ref_64028);
                _32280 = (int)*(((s1_ptr)_2)->base + 2);
                _32281 = _52sym_name(_s_64126);
                if (_32280 == _32281)
                _32282 = 1;
                else if (IS_ATOM_INT(_32280) && IS_ATOM_INT(_32281))
                _32282 = 0;
                else
                _32282 = (compare(_32280, _32281) == 0);
                _32280 = NOVALUE;
                DeRef(_32281);
                _32281 = NOVALUE;
                if (_32282 == 0)
                {
                    _32282 = NOVALUE;
                    goto L1A; // [731] 779
                }
                else{
                    _32282 = NOVALUE;
                }

                /** 									errloc &= sprintf("\t\tin %s\n", */
                _2 = (int)SEQ_PTR(_26SymTab_11138);
                _32284 = (int)*(((s1_ptr)_2)->base + _s_64126);
                _2 = (int)SEQ_PTR(_32284);
                if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
                    _32285 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
                }
                else{
                    _32285 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
                }
                _32284 = NOVALUE;
                _2 = (int)SEQ_PTR(_26known_files_11139);
                if (!IS_ATOM_INT(_32285)){
                    _32286 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32285)->dbl));
                }
                else{
                    _32286 = (int)*(((s1_ptr)_2)->base + _32285);
                }
                Ref(_32286);
                RefDS(_22682);
                _32287 = _9abbreviate_path(_32286, _22682);
                _32286 = NOVALUE;
                _32288 = _7find_replace(92, _32287, 47, 0);
                _32287 = NOVALUE;
                _1 = NewS1(1);
                _2 = (int)((s1_ptr)_1)->base;
                *((int *)(_2+4)) = _32288;
                _32289 = MAKE_SEQ(_1);
                _32288 = NOVALUE;
                _32290 = EPrintf(-9999999, _32283, _32289);
                DeRefDS(_32289);
                _32289 = NOVALUE;
                Concat((object_ptr)&_errloc_64023, _errloc_64023, _32290);
                DeRefDS(_32290);
                _32290 = NOVALUE;
L1A: 

                /** 							end for*/
                _si_64123 = _si_64123 + 1;
                goto L18; // [783] 707
L19: 
                ;
            }
            DeRef(_syms_64105);
            _syms_64105 = NOVALUE;
            goto L17; // [790] 797

            /** 						case else */
            default:
L14: 
        ;}L17: 
L13: 

        /** 				if not match(errloc, msg) then*/
        _32292 = e_match_from(_errloc_64023, _msg_64022, 1);
        if (_32292 != 0)
        goto L1B; // [805] 824
        _32292 = NOVALUE;

        /** 					msg &= errloc*/
        Concat((object_ptr)&_msg_64022, _msg_64022, _errloc_64023);

        /** 					prep_forward_error( errors[e] )*/
        _2 = (int)SEQ_PTR(_errors_63960);
        _32295 = (int)*(((s1_ptr)_2)->base + _e_64026);
        Ref(_32295);
        _29prep_forward_error(_32295);
        _32295 = NOVALUE;
L1B: 
        DeRef(_tok_64044);
        _tok_64044 = NOVALUE;
L12: 

        /** 			ThisLine    = ref[FR_THISLINE]*/
        DeRef(_43ThisLine_49532);
        _2 = (int)SEQ_PTR(_ref_64028);
        _43ThisLine_49532 = (int)*(((s1_ptr)_2)->base + 7);
        Ref(_43ThisLine_49532);

        /** 			bp          = ref[FR_BP]*/
        _2 = (int)SEQ_PTR(_ref_64028);
        _43bp_49536 = (int)*(((s1_ptr)_2)->base + 8);
        if (!IS_ATOM_INT(_43bp_49536)){
            _43bp_49536 = (long)DBL_PTR(_43bp_49536)->dbl;
        }

        /** 			CurrentSub  = ref[FR_SUBPROG]*/
        _2 = (int)SEQ_PTR(_ref_64028);
        _25CurrentSub_12270 = (int)*(((s1_ptr)_2)->base + 4);
        if (!IS_ATOM_INT(_25CurrentSub_12270)){
            _25CurrentSub_12270 = (long)DBL_PTR(_25CurrentSub_12270)->dbl;
        }

        /** 			line_number = ref[FR_LINE]*/
        _2 = (int)SEQ_PTR(_ref_64028);
        _25line_number_12263 = (int)*(((s1_ptr)_2)->base + 6);
        if (!IS_ATOM_INT(_25line_number_12263)){
            _25line_number_12263 = (long)DBL_PTR(_25line_number_12263)->dbl;
        }
        DeRefDS(_ref_64028);
        _ref_64028 = NOVALUE;

        /** 		end for*/
L11: 
        _e_64026 = _e_64026 + -1;
        goto LC; // [869] 319
LD: 
        ;
    }

    /** 		if length(msg) > 0 then*/
    if (IS_SEQUENCE(_msg_64022)){
            _32300 = SEQ_PTR(_msg_64022)->length;
    }
    else {
        _32300 = 1;
    }
    if (_32300 <= 0)
    goto L1C; // [879] 895

    /** 			CompileErr( 74, {msg} )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_msg_64022);
    *((int *)(_2+4)) = _msg_64022;
    _32302 = MAKE_SEQ(_1);
    _43CompileErr(74, _32302, 0);
    _32302 = NOVALUE;
L1C: 
    DeRefi(_msg_64022);
    _msg_64022 = NOVALUE;
    DeRefi(_errloc_64023);
    _errloc_64023 = NOVALUE;
    goto L1D; // [897] 935
LB: 

    /** 	elsif report_errors then*/
    if (_report_errors_63959 == 0)
    {
        goto L1E; // [902] 934
    }
    else{
    }

    /** 		forward_references  = {}*/
    RefDS(_22682);
    DeRef(_29forward_references_62545);
    _29forward_references_62545 = _22682;

    /** 		active_references   = {}*/
    RefDS(_22682);
    DeRef(_29active_references_62547);
    _29active_references_62547 = _22682;

    /** 		toplevel_references = {}*/
    RefDS(_22682);
    DeRef(_29toplevel_references_62548);
    _29toplevel_references_62548 = _22682;

    /** 		inactive_references = {}*/
    RefDS(_22682);
    DeRef(_29inactive_references_62549);
    _29inactive_references_62549 = _22682;
L1E: 
L1D: 

    /** 	clear_last()*/
    _37clear_last();

    /** end procedure*/
    DeRef(_errors_63960);
    _32195 = NOVALUE;
    _32198 = NOVALUE;
    DeRef(_32201);
    _32201 = NOVALUE;
    _32203 = NOVALUE;
    _32205 = NOVALUE;
    _32218 = NOVALUE;
    _32285 = NOVALUE;
    DeRef(_32221);
    _32221 = NOVALUE;
    DeRef(_32224);
    _32224 = NOVALUE;
    _32240 = NOVALUE;
    _32252 = NOVALUE;
    _32260 = NOVALUE;
    _32244 = NOVALUE;
    _32272 = NOVALUE;
    return;
    ;
}


void _29shift_these(int _refs_64170, int _pc_64171, int _amount_64172)
{
    int _fr_64176 = NOVALUE;
    int _32320 = NOVALUE;
    int _32319 = NOVALUE;
    int _32318 = NOVALUE;
    int _32317 = NOVALUE;
    int _32316 = NOVALUE;
    int _32315 = NOVALUE;
    int _32314 = NOVALUE;
    int _32313 = NOVALUE;
    int _32312 = NOVALUE;
    int _32311 = NOVALUE;
    int _32309 = NOVALUE;
    int _32307 = NOVALUE;
    int _32306 = NOVALUE;
    int _32304 = NOVALUE;
    int _32303 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = length( refs ) to 1 by -1 do*/
    if (IS_SEQUENCE(_refs_64170)){
            _32303 = SEQ_PTR(_refs_64170)->length;
    }
    else {
        _32303 = 1;
    }
    {
        int _i_64174;
        _i_64174 = _32303;
L1: 
        if (_i_64174 < 1){
            goto L2; // [12] 159
        }

        /** 		sequence fr = forward_references[refs[i]]*/
        _2 = (int)SEQ_PTR(_refs_64170);
        _32304 = (int)*(((s1_ptr)_2)->base + _i_64174);
        DeRef(_fr_64176);
        _2 = (int)SEQ_PTR(_29forward_references_62545);
        if (!IS_ATOM_INT(_32304)){
            _fr_64176 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32304)->dbl));
        }
        else{
            _fr_64176 = (int)*(((s1_ptr)_2)->base + _32304);
        }
        Ref(_fr_64176);

        /** 		forward_references[refs[i]] = 0*/
        _2 = (int)SEQ_PTR(_refs_64170);
        _32306 = (int)*(((s1_ptr)_2)->base + _i_64174);
        _2 = (int)SEQ_PTR(_29forward_references_62545);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _29forward_references_62545 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_32306))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_32306)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _32306);
        _1 = *(int *)_2;
        *(int *)_2 = 0;
        DeRef(_1);

        /** 		if fr[FR_SUBPROG] = shifting_sub then*/
        _2 = (int)SEQ_PTR(_fr_64176);
        _32307 = (int)*(((s1_ptr)_2)->base + 4);
        if (binary_op_a(NOTEQ, _32307, _29shifting_sub_62575)){
            _32307 = NOVALUE;
            goto L3; // [55] 138
        }
        _32307 = NOVALUE;

        /** 			if fr[FR_PC] >= pc then*/
        _2 = (int)SEQ_PTR(_fr_64176);
        _32309 = (int)*(((s1_ptr)_2)->base + 5);
        if (binary_op_a(LESS, _32309, _pc_64171)){
            _32309 = NOVALUE;
            goto L4; // [67] 137
        }
        _32309 = NOVALUE;

        /** 				fr[FR_PC] += amount*/
        _2 = (int)SEQ_PTR(_fr_64176);
        _32311 = (int)*(((s1_ptr)_2)->base + 5);
        if (IS_ATOM_INT(_32311)) {
            _32312 = _32311 + _amount_64172;
            if ((long)((unsigned long)_32312 + (unsigned long)HIGH_BITS) >= 0) 
            _32312 = NewDouble((double)_32312);
        }
        else {
            _32312 = binary_op(PLUS, _32311, _amount_64172);
        }
        _32311 = NOVALUE;
        _2 = (int)SEQ_PTR(_fr_64176);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _fr_64176 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = _32312;
        if( _1 != _32312 ){
            DeRef(_1);
        }
        _32312 = NOVALUE;

        /** 				if fr[FR_TYPE] = CASE*/
        _2 = (int)SEQ_PTR(_fr_64176);
        _32313 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_32313)) {
            _32314 = (_32313 == 186);
        }
        else {
            _32314 = binary_op(EQUALS, _32313, 186);
        }
        _32313 = NOVALUE;
        if (IS_ATOM_INT(_32314)) {
            if (_32314 == 0) {
                goto L5; // [101] 136
            }
        }
        else {
            if (DBL_PTR(_32314)->dbl == 0.0) {
                goto L5; // [101] 136
            }
        }
        _2 = (int)SEQ_PTR(_fr_64176);
        _32316 = (int)*(((s1_ptr)_2)->base + 12);
        if (IS_ATOM_INT(_32316)) {
            _32317 = (_32316 >= _pc_64171);
        }
        else {
            _32317 = binary_op(GREATEREQ, _32316, _pc_64171);
        }
        _32316 = NOVALUE;
        if (_32317 == 0) {
            DeRef(_32317);
            _32317 = NOVALUE;
            goto L5; // [116] 136
        }
        else {
            if (!IS_ATOM_INT(_32317) && DBL_PTR(_32317)->dbl == 0.0){
                DeRef(_32317);
                _32317 = NOVALUE;
                goto L5; // [116] 136
            }
            DeRef(_32317);
            _32317 = NOVALUE;
        }
        DeRef(_32317);
        _32317 = NOVALUE;

        /** 					fr[FR_DATA] += amount*/
        _2 = (int)SEQ_PTR(_fr_64176);
        _32318 = (int)*(((s1_ptr)_2)->base + 12);
        if (IS_ATOM_INT(_32318)) {
            _32319 = _32318 + _amount_64172;
            if ((long)((unsigned long)_32319 + (unsigned long)HIGH_BITS) >= 0) 
            _32319 = NewDouble((double)_32319);
        }
        else {
            _32319 = binary_op(PLUS, _32318, _amount_64172);
        }
        _32318 = NOVALUE;
        _2 = (int)SEQ_PTR(_fr_64176);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _fr_64176 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 12);
        _1 = *(int *)_2;
        *(int *)_2 = _32319;
        if( _1 != _32319 ){
            DeRef(_1);
        }
        _32319 = NOVALUE;
L5: 
L4: 
L3: 

        /** 		forward_references[refs[i]] = fr*/
        _2 = (int)SEQ_PTR(_refs_64170);
        _32320 = (int)*(((s1_ptr)_2)->base + _i_64174);
        RefDS(_fr_64176);
        _2 = (int)SEQ_PTR(_29forward_references_62545);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _29forward_references_62545 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_32320))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_32320)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _32320);
        _1 = *(int *)_2;
        *(int *)_2 = _fr_64176;
        DeRef(_1);
        DeRefDS(_fr_64176);
        _fr_64176 = NOVALUE;

        /** 	end for*/
        _i_64174 = _i_64174 + -1;
        goto L1; // [154] 19
L2: 
        ;
    }

    /** end procedure*/
    DeRefDS(_refs_64170);
    _32304 = NOVALUE;
    _32306 = NOVALUE;
    _32320 = NOVALUE;
    DeRef(_32314);
    _32314 = NOVALUE;
    return;
    ;
}


void _29shift_top(int _refs_64200, int _pc_64201, int _amount_64202)
{
    int _fr_64206 = NOVALUE;
    int _32336 = NOVALUE;
    int _32335 = NOVALUE;
    int _32334 = NOVALUE;
    int _32333 = NOVALUE;
    int _32332 = NOVALUE;
    int _32331 = NOVALUE;
    int _32330 = NOVALUE;
    int _32329 = NOVALUE;
    int _32328 = NOVALUE;
    int _32327 = NOVALUE;
    int _32325 = NOVALUE;
    int _32324 = NOVALUE;
    int _32322 = NOVALUE;
    int _32321 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = length( refs ) to 1 by -1 do*/
    if (IS_SEQUENCE(_refs_64200)){
            _32321 = SEQ_PTR(_refs_64200)->length;
    }
    else {
        _32321 = 1;
    }
    {
        int _i_64204;
        _i_64204 = _32321;
L1: 
        if (_i_64204 < 1){
            goto L2; // [12] 144
        }

        /** 		sequence fr = forward_references[refs[i]]*/
        _2 = (int)SEQ_PTR(_refs_64200);
        _32322 = (int)*(((s1_ptr)_2)->base + _i_64204);
        DeRef(_fr_64206);
        _2 = (int)SEQ_PTR(_29forward_references_62545);
        if (!IS_ATOM_INT(_32322)){
            _fr_64206 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32322)->dbl));
        }
        else{
            _fr_64206 = (int)*(((s1_ptr)_2)->base + _32322);
        }
        Ref(_fr_64206);

        /** 		forward_references[refs[i]] = 0*/
        _2 = (int)SEQ_PTR(_refs_64200);
        _32324 = (int)*(((s1_ptr)_2)->base + _i_64204);
        _2 = (int)SEQ_PTR(_29forward_references_62545);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _29forward_references_62545 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_32324))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_32324)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _32324);
        _1 = *(int *)_2;
        *(int *)_2 = 0;
        DeRef(_1);

        /** 		if fr[FR_PC] >= pc then*/
        _2 = (int)SEQ_PTR(_fr_64206);
        _32325 = (int)*(((s1_ptr)_2)->base + 5);
        if (binary_op_a(LESS, _32325, _pc_64201)){
            _32325 = NOVALUE;
            goto L3; // [53] 123
        }
        _32325 = NOVALUE;

        /** 			fr[FR_PC] += amount*/
        _2 = (int)SEQ_PTR(_fr_64206);
        _32327 = (int)*(((s1_ptr)_2)->base + 5);
        if (IS_ATOM_INT(_32327)) {
            _32328 = _32327 + _amount_64202;
            if ((long)((unsigned long)_32328 + (unsigned long)HIGH_BITS) >= 0) 
            _32328 = NewDouble((double)_32328);
        }
        else {
            _32328 = binary_op(PLUS, _32327, _amount_64202);
        }
        _32327 = NOVALUE;
        _2 = (int)SEQ_PTR(_fr_64206);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _fr_64206 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = _32328;
        if( _1 != _32328 ){
            DeRef(_1);
        }
        _32328 = NOVALUE;

        /** 			if fr[FR_TYPE] = CASE*/
        _2 = (int)SEQ_PTR(_fr_64206);
        _32329 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_32329)) {
            _32330 = (_32329 == 186);
        }
        else {
            _32330 = binary_op(EQUALS, _32329, 186);
        }
        _32329 = NOVALUE;
        if (IS_ATOM_INT(_32330)) {
            if (_32330 == 0) {
                goto L4; // [87] 122
            }
        }
        else {
            if (DBL_PTR(_32330)->dbl == 0.0) {
                goto L4; // [87] 122
            }
        }
        _2 = (int)SEQ_PTR(_fr_64206);
        _32332 = (int)*(((s1_ptr)_2)->base + 12);
        if (IS_ATOM_INT(_32332)) {
            _32333 = (_32332 >= _pc_64201);
        }
        else {
            _32333 = binary_op(GREATEREQ, _32332, _pc_64201);
        }
        _32332 = NOVALUE;
        if (_32333 == 0) {
            DeRef(_32333);
            _32333 = NOVALUE;
            goto L4; // [102] 122
        }
        else {
            if (!IS_ATOM_INT(_32333) && DBL_PTR(_32333)->dbl == 0.0){
                DeRef(_32333);
                _32333 = NOVALUE;
                goto L4; // [102] 122
            }
            DeRef(_32333);
            _32333 = NOVALUE;
        }
        DeRef(_32333);
        _32333 = NOVALUE;

        /** 				fr[FR_DATA] += amount*/
        _2 = (int)SEQ_PTR(_fr_64206);
        _32334 = (int)*(((s1_ptr)_2)->base + 12);
        if (IS_ATOM_INT(_32334)) {
            _32335 = _32334 + _amount_64202;
            if ((long)((unsigned long)_32335 + (unsigned long)HIGH_BITS) >= 0) 
            _32335 = NewDouble((double)_32335);
        }
        else {
            _32335 = binary_op(PLUS, _32334, _amount_64202);
        }
        _32334 = NOVALUE;
        _2 = (int)SEQ_PTR(_fr_64206);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _fr_64206 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 12);
        _1 = *(int *)_2;
        *(int *)_2 = _32335;
        if( _1 != _32335 ){
            DeRef(_1);
        }
        _32335 = NOVALUE;
L4: 
L3: 

        /** 		forward_references[refs[i]] = fr*/
        _2 = (int)SEQ_PTR(_refs_64200);
        _32336 = (int)*(((s1_ptr)_2)->base + _i_64204);
        RefDS(_fr_64206);
        _2 = (int)SEQ_PTR(_29forward_references_62545);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _29forward_references_62545 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_32336))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_32336)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _32336);
        _1 = *(int *)_2;
        *(int *)_2 = _fr_64206;
        DeRef(_1);
        DeRefDS(_fr_64206);
        _fr_64206 = NOVALUE;

        /** 	end for*/
        _i_64204 = _i_64204 + -1;
        goto L1; // [139] 19
L2: 
        ;
    }

    /** end procedure*/
    DeRefDS(_refs_64200);
    _32322 = NOVALUE;
    _32324 = NOVALUE;
    _32336 = NOVALUE;
    DeRef(_32330);
    _32330 = NOVALUE;
    return;
    ;
}


void _29shift_fwd_refs(int _pc_64227, int _amount_64228)
{
    int _file_64239 = NOVALUE;
    int _sp_64244 = NOVALUE;
    int _32346 = NOVALUE;
    int _32345 = NOVALUE;
    int _32343 = NOVALUE;
    int _32341 = NOVALUE;
    int _32340 = NOVALUE;
    int _32339 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_pc_64227)) {
        _1 = (long)(DBL_PTR(_pc_64227)->dbl);
        if (UNIQUE(DBL_PTR(_pc_64227)) && (DBL_PTR(_pc_64227)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pc_64227);
        _pc_64227 = _1;
    }
    if (!IS_ATOM_INT(_amount_64228)) {
        _1 = (long)(DBL_PTR(_amount_64228)->dbl);
        if (UNIQUE(DBL_PTR(_amount_64228)) && (DBL_PTR(_amount_64228)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_amount_64228);
        _amount_64228 = _1;
    }

    /** 	if not shifting_sub then*/
    if (_29shifting_sub_62575 != 0)
    goto L1; // [9] 18

    /** 		return*/
    return;
L1: 

    /** 	if shifting_sub = TopLevelSub then*/
    if (_29shifting_sub_62575 != _25TopLevelSub_12269)
    goto L2; // [24] 65

    /** 		for file = 1 to length( toplevel_references ) do*/
    if (IS_SEQUENCE(_29toplevel_references_62548)){
            _32339 = SEQ_PTR(_29toplevel_references_62548)->length;
    }
    else {
        _32339 = 1;
    }
    {
        int _file_64235;
        _file_64235 = 1;
L3: 
        if (_file_64235 > _32339){
            goto L4; // [35] 62
        }

        /** 			shift_top( toplevel_references[file], pc, amount )*/
        _2 = (int)SEQ_PTR(_29toplevel_references_62548);
        _32340 = (int)*(((s1_ptr)_2)->base + _file_64235);
        Ref(_32340);
        _29shift_top(_32340, _pc_64227, _amount_64228);
        _32340 = NOVALUE;

        /** 		end for*/
        _file_64235 = _file_64235 + 1;
        goto L3; // [57] 42
L4: 
        ;
    }
    goto L5; // [62] 118
L2: 

    /** 		integer file = SymTab[shifting_sub][S_FILE_NO]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _32341 = (int)*(((s1_ptr)_2)->base + _29shifting_sub_62575);
    _2 = (int)SEQ_PTR(_32341);
    if (!IS_ATOM_INT(_25S_FILE_NO_11909)){
        _file_64239 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    }
    else{
        _file_64239 = (int)*(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    }
    if (!IS_ATOM_INT(_file_64239)){
        _file_64239 = (long)DBL_PTR(_file_64239)->dbl;
    }
    _32341 = NOVALUE;

    /** 		integer sp   = find( shifting_sub, active_subprogs[file] )*/
    _2 = (int)SEQ_PTR(_29active_subprogs_62546);
    _32343 = (int)*(((s1_ptr)_2)->base + _file_64239);
    _sp_64244 = find_from(_29shifting_sub_62575, _32343, 1);
    _32343 = NOVALUE;

    /** 		shift_these( active_references[file][sp], pc, amount )*/
    _2 = (int)SEQ_PTR(_29active_references_62547);
    _32345 = (int)*(((s1_ptr)_2)->base + _file_64239);
    _2 = (int)SEQ_PTR(_32345);
    _32346 = (int)*(((s1_ptr)_2)->base + _sp_64244);
    _32345 = NOVALUE;
    Ref(_32346);
    _29shift_these(_32346, _pc_64227, _amount_64228);
    _32346 = NOVALUE;
L5: 

    /** end procedure*/
    return;
    ;
}



// 0xF4F9100D
