// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _10time()
{
    int _ptra_3294 = NOVALUE;
    int _valhi_3295 = NOVALUE;
    int _vallow_3296 = NOVALUE;
    int _deltahi_3297 = NOVALUE;
    int _deltalow_3298 = NOVALUE;
    int _1556 = NOVALUE;
    int _1554 = NOVALUE;
    int _1553 = NOVALUE;
    int _1552 = NOVALUE;
    int _1549 = NOVALUE;
    int _1544 = NOVALUE;
    int _1542 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		atom ptra, valhi, vallow, deltahi, deltalow*/

    /** 		deltahi = 27111902*/
    _deltahi_3297 = 27111902;

    /** 		deltalow = 3577643008*/
    RefDS(_1540);
    DeRef(_deltalow_3298);
    _deltalow_3298 = _1540;

    /** 		ptra = machine:allocate(8)*/
    _0 = _ptra_3294;
    _ptra_3294 = _12allocate(8, 0);
    DeRef(_0);

    /** 		c_proc(time_, {ptra})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_ptra_3294);
    *((int *)(_2+4)) = _ptra_3294;
    _1542 = MAKE_SEQ(_1);
    call_c(0, _10time__3276, _1542);
    DeRefDS(_1542);
    _1542 = NOVALUE;

    /** 		vallow = peek4u(ptra)*/
    DeRef(_vallow_3296);
    if (IS_ATOM_INT(_ptra_3294)) {
        _vallow_3296 = *(unsigned long *)_ptra_3294;
        if ((unsigned)_vallow_3296 > (unsigned)MAXINT)
        _vallow_3296 = NewDouble((double)(unsigned long)_vallow_3296);
    }
    else {
        _vallow_3296 = *(unsigned long *)(unsigned long)(DBL_PTR(_ptra_3294)->dbl);
        if ((unsigned)_vallow_3296 > (unsigned)MAXINT)
        _vallow_3296 = NewDouble((double)(unsigned long)_vallow_3296);
    }

    /** 		valhi = peek4u(ptra+4)*/
    if (IS_ATOM_INT(_ptra_3294)) {
        _1544 = _ptra_3294 + 4;
        if ((long)((unsigned long)_1544 + (unsigned long)HIGH_BITS) >= 0) 
        _1544 = NewDouble((double)_1544);
    }
    else {
        _1544 = NewDouble(DBL_PTR(_ptra_3294)->dbl + (double)4);
    }
    DeRef(_valhi_3295);
    if (IS_ATOM_INT(_1544)) {
        _valhi_3295 = *(unsigned long *)_1544;
        if ((unsigned)_valhi_3295 > (unsigned)MAXINT)
        _valhi_3295 = NewDouble((double)(unsigned long)_valhi_3295);
    }
    else {
        _valhi_3295 = *(unsigned long *)(unsigned long)(DBL_PTR(_1544)->dbl);
        if ((unsigned)_valhi_3295 > (unsigned)MAXINT)
        _valhi_3295 = NewDouble((double)(unsigned long)_valhi_3295);
    }
    DeRef(_1544);
    _1544 = NOVALUE;

    /** 		machine:free(ptra)*/
    Ref(_ptra_3294);
    _12free(_ptra_3294);

    /** 		vallow -= deltalow*/
    _0 = _vallow_3296;
    if (IS_ATOM_INT(_vallow_3296)) {
        _vallow_3296 = NewDouble((double)_vallow_3296 - DBL_PTR(_deltalow_3298)->dbl);
    }
    else {
        _vallow_3296 = NewDouble(DBL_PTR(_vallow_3296)->dbl - DBL_PTR(_deltalow_3298)->dbl);
    }
    DeRef(_0);

    /** 		valhi -= deltahi*/
    _0 = _valhi_3295;
    if (IS_ATOM_INT(_valhi_3295)) {
        _valhi_3295 = _valhi_3295 - 27111902;
        if ((long)((unsigned long)_valhi_3295 +(unsigned long) HIGH_BITS) >= 0){
            _valhi_3295 = NewDouble((double)_valhi_3295);
        }
    }
    else {
        _valhi_3295 = NewDouble(DBL_PTR(_valhi_3295)->dbl - (double)27111902);
    }
    DeRef(_0);

    /** 		if vallow < 0 then*/
    if (binary_op_a(GREATEREQ, _vallow_3296, 0)){
        goto L1; // [67] 88
    }

    /** 			vallow += power(2, 32)*/
    _1549 = power(2, 32);
    _0 = _vallow_3296;
    if (IS_ATOM_INT(_1549)) {
        _vallow_3296 = NewDouble(DBL_PTR(_vallow_3296)->dbl + (double)_1549);
    }
    else
    _vallow_3296 = NewDouble(DBL_PTR(_vallow_3296)->dbl + DBL_PTR(_1549)->dbl);
    DeRefDS(_0);
    DeRef(_1549);
    _1549 = NOVALUE;

    /** 			valhi -= 1*/
    _0 = _valhi_3295;
    if (IS_ATOM_INT(_valhi_3295)) {
        _valhi_3295 = _valhi_3295 - 1;
        if ((long)((unsigned long)_valhi_3295 +(unsigned long) HIGH_BITS) >= 0){
            _valhi_3295 = NewDouble((double)_valhi_3295);
        }
    }
    else {
        _valhi_3295 = NewDouble(DBL_PTR(_valhi_3295)->dbl - (double)1);
    }
    DeRef(_0);
L1: 

    /** 		return floor(((valhi * power(2,32)) + vallow) / 10000000)*/
    _1552 = power(2, 32);
    if (IS_ATOM_INT(_valhi_3295) && IS_ATOM_INT(_1552)) {
        if (_valhi_3295 == (short)_valhi_3295 && _1552 <= INT15 && _1552 >= -INT15)
        _1553 = _valhi_3295 * _1552;
        else
        _1553 = NewDouble(_valhi_3295 * (double)_1552);
    }
    else {
        if (IS_ATOM_INT(_valhi_3295)) {
            _1553 = NewDouble((double)_valhi_3295 * DBL_PTR(_1552)->dbl);
        }
        else {
            if (IS_ATOM_INT(_1552)) {
                _1553 = NewDouble(DBL_PTR(_valhi_3295)->dbl * (double)_1552);
            }
            else
            _1553 = NewDouble(DBL_PTR(_valhi_3295)->dbl * DBL_PTR(_1552)->dbl);
        }
    }
    DeRef(_1552);
    _1552 = NOVALUE;
    if (IS_ATOM_INT(_1553) && IS_ATOM_INT(_vallow_3296)) {
        _1554 = _1553 + _vallow_3296;
        if ((long)((unsigned long)_1554 + (unsigned long)HIGH_BITS) >= 0) 
        _1554 = NewDouble((double)_1554);
    }
    else {
        if (IS_ATOM_INT(_1553)) {
            _1554 = NewDouble((double)_1553 + DBL_PTR(_vallow_3296)->dbl);
        }
        else {
            if (IS_ATOM_INT(_vallow_3296)) {
                _1554 = NewDouble(DBL_PTR(_1553)->dbl + (double)_vallow_3296);
            }
            else
            _1554 = NewDouble(DBL_PTR(_1553)->dbl + DBL_PTR(_vallow_3296)->dbl);
        }
    }
    DeRef(_1553);
    _1553 = NOVALUE;
    if (IS_ATOM_INT(_1554)) {
        if (10000000 > 0 && _1554 >= 0) {
            _1556 = _1554 / 10000000;
        }
        else {
            temp_dbl = floor((double)_1554 / (double)10000000);
            if (_1554 != MININT)
            _1556 = (long)temp_dbl;
            else
            _1556 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _1554, 10000000);
        _1556 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_1554);
    _1554 = NOVALUE;
    DeRef(_ptra_3294);
    DeRef(_valhi_3295);
    DeRef(_vallow_3296);
    DeRef(_deltalow_3298);
    return _1556;
    ;
}


int _10gmtime(int _time_3320)
{
    int _ret_3321 = NOVALUE;
    int _timep_3322 = NOVALUE;
    int _tm_p_3323 = NOVALUE;
    int _n_3324 = NOVALUE;
    int _1562 = NOVALUE;
    int _1561 = NOVALUE;
    int _1558 = NOVALUE;
    int _0, _1, _2;
    

    /** 	timep = machine:allocate(4)*/
    _0 = _timep_3322;
    _timep_3322 = _12allocate(4, 0);
    DeRef(_0);

    /** 	poke4(timep, time)*/
    if (IS_ATOM_INT(_timep_3322)){
        poke4_addr = (unsigned long *)_timep_3322;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_timep_3322)->dbl);
    }
    if (IS_ATOM_INT(_time_3320)) {
        *poke4_addr = (unsigned long)_time_3320;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_time_3320)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	tm_p = c_func(gmtime_, {timep})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_timep_3322);
    *((int *)(_2+4)) = _timep_3322;
    _1558 = MAKE_SEQ(_1);
    DeRef(_tm_p_3323);
    _tm_p_3323 = call_c(1, _10gmtime__3270, _1558);
    DeRefDS(_1558);
    _1558 = NOVALUE;

    /** 	machine:free(timep)*/
    Ref(_timep_3322);
    _12free(_timep_3322);

    /** 	ret = repeat(0, 9)*/
    DeRef(_ret_3321);
    _ret_3321 = Repeat(0, 9);

    /** 	n = 0*/
    _n_3324 = 0;

    /** 	for i = 1 to 9 do*/
    {
        int _i_3330;
        _i_3330 = 1;
L1: 
        if (_i_3330 > 9){
            goto L2; // [44] 77
        }

        /** 		ret[i] = peek4s(tm_p+n)*/
        if (IS_ATOM_INT(_tm_p_3323)) {
            _1561 = _tm_p_3323 + _n_3324;
            if ((long)((unsigned long)_1561 + (unsigned long)HIGH_BITS) >= 0) 
            _1561 = NewDouble((double)_1561);
        }
        else {
            _1561 = NewDouble(DBL_PTR(_tm_p_3323)->dbl + (double)_n_3324);
        }
        if (IS_ATOM_INT(_1561)) {
            _1562 = *(unsigned long *)_1561;
            if (_1562 < MININT || _1562 > MAXINT)
            _1562 = NewDouble((double)(long)_1562);
        }
        else {
            _1562 = *(unsigned long *)(unsigned long)(DBL_PTR(_1561)->dbl);
            if (_1562 < MININT || _1562 > MAXINT)
            _1562 = NewDouble((double)(long)_1562);
        }
        DeRef(_1561);
        _1561 = NOVALUE;
        _2 = (int)SEQ_PTR(_ret_3321);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _ret_3321 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_3330);
        _1 = *(int *)_2;
        *(int *)_2 = _1562;
        if( _1 != _1562 ){
            DeRef(_1);
        }
        _1562 = NOVALUE;

        /** 		n = n + 4*/
        _n_3324 = _n_3324 + 4;

        /** 	end for*/
        _i_3330 = _i_3330 + 1;
        goto L1; // [72] 51
L2: 
        ;
    }

    /** 	return ret*/
    DeRef(_time_3320);
    DeRef(_timep_3322);
    DeRef(_tm_p_3323);
    return _ret_3321;
    ;
}


int _10tolower(int _x_3347)
{
    int _1575 = NOVALUE;
    int _1574 = NOVALUE;
    int _1573 = NOVALUE;
    int _1572 = NOVALUE;
    int _1571 = NOVALUE;
    int _1570 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return x + (x >= 'A' and x <= 'Z') * ('a' - 'A')*/
    _1570 = binary_op(GREATEREQ, _x_3347, 65);
    _1571 = binary_op(LESSEQ, _x_3347, 90);
    _1572 = binary_op(AND, _1570, _1571);
    DeRefDS(_1570);
    _1570 = NOVALUE;
    DeRefDS(_1571);
    _1571 = NOVALUE;
    _1573 = 32;
    _1574 = binary_op(MULTIPLY, _1572, 32);
    DeRefDS(_1572);
    _1572 = NOVALUE;
    _1573 = NOVALUE;
    _1575 = binary_op(PLUS, _x_3347, _1574);
    DeRefDS(_1574);
    _1574 = NOVALUE;
    DeRefDS(_x_3347);
    return _1575;
    ;
}


int _10isLeap(int _year_3356)
{
    int _ly_3357 = NOVALUE;
    int _1594 = NOVALUE;
    int _1593 = NOVALUE;
    int _1591 = NOVALUE;
    int _1590 = NOVALUE;
    int _1589 = NOVALUE;
    int _1588 = NOVALUE;
    int _1587 = NOVALUE;
    int _1586 = NOVALUE;
    int _1585 = NOVALUE;
    int _1582 = NOVALUE;
    int _1580 = NOVALUE;
    int _1579 = NOVALUE;
    int _0, _1, _2;
    

    /** 		ly = (remainder(year, {4, 100, 400, 3200, 80000})=0)*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 4;
    *((int *)(_2+8)) = 100;
    *((int *)(_2+12)) = 400;
    *((int *)(_2+16)) = 3200;
    *((int *)(_2+20)) = 80000;
    _1579 = MAKE_SEQ(_1);
    _1580 = binary_op(REMAINDER, _year_3356, _1579);
    DeRefDS(_1579);
    _1579 = NOVALUE;
    DeRefi(_ly_3357);
    _ly_3357 = binary_op(EQUALS, _1580, 0);
    DeRefDS(_1580);
    _1580 = NOVALUE;

    /** 		if not ly[1] then return 0 end if*/
    _2 = (int)SEQ_PTR(_ly_3357);
    _1582 = (int)*(((s1_ptr)_2)->base + 1);
    if (_1582 != 0)
    goto L1; // [29] 37
    _1582 = NOVALUE;
    DeRefDSi(_ly_3357);
    return 0;
L1: 

    /** 		if year <= Gregorian_Reformation then*/
    if (_year_3356 > 1752)
    goto L2; // [39] 52

    /** 				return 1 -- ly[1] can't possibly be 0 here so set shortcut as '1'.*/
    DeRefi(_ly_3357);
    return 1;
    goto L3; // [49] 95
L2: 

    /** 				return ly[1] - ly[2] + ly[3] - ly[4] + ly[5]*/
    _2 = (int)SEQ_PTR(_ly_3357);
    _1585 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_ly_3357);
    _1586 = (int)*(((s1_ptr)_2)->base + 2);
    _1587 = _1585 - _1586;
    if ((long)((unsigned long)_1587 +(unsigned long) HIGH_BITS) >= 0){
        _1587 = NewDouble((double)_1587);
    }
    _1585 = NOVALUE;
    _1586 = NOVALUE;
    _2 = (int)SEQ_PTR(_ly_3357);
    _1588 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_1587)) {
        _1589 = _1587 + _1588;
        if ((long)((unsigned long)_1589 + (unsigned long)HIGH_BITS) >= 0) 
        _1589 = NewDouble((double)_1589);
    }
    else {
        _1589 = NewDouble(DBL_PTR(_1587)->dbl + (double)_1588);
    }
    DeRef(_1587);
    _1587 = NOVALUE;
    _1588 = NOVALUE;
    _2 = (int)SEQ_PTR(_ly_3357);
    _1590 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_1589)) {
        _1591 = _1589 - _1590;
        if ((long)((unsigned long)_1591 +(unsigned long) HIGH_BITS) >= 0){
            _1591 = NewDouble((double)_1591);
        }
    }
    else {
        _1591 = NewDouble(DBL_PTR(_1589)->dbl - (double)_1590);
    }
    DeRef(_1589);
    _1589 = NOVALUE;
    _1590 = NOVALUE;
    _2 = (int)SEQ_PTR(_ly_3357);
    _1593 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_ATOM_INT(_1591)) {
        _1594 = _1591 + _1593;
        if ((long)((unsigned long)_1594 + (unsigned long)HIGH_BITS) >= 0) 
        _1594 = NewDouble((double)_1594);
    }
    else {
        _1594 = NewDouble(DBL_PTR(_1591)->dbl + (double)_1593);
    }
    DeRef(_1591);
    _1591 = NOVALUE;
    _1593 = NOVALUE;
    DeRefDSi(_ly_3357);
    return _1594;
L3: 
    ;
}


int _10daysInMonth(int _year_3382, int _month_3383)
{
    int _1602 = NOVALUE;
    int _1601 = NOVALUE;
    int _1600 = NOVALUE;
    int _1599 = NOVALUE;
    int _1597 = NOVALUE;
    int _1596 = NOVALUE;
    int _1595 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_month_3383)) {
        _1 = (long)(DBL_PTR(_month_3383)->dbl);
        if (UNIQUE(DBL_PTR(_month_3383)) && (DBL_PTR(_month_3383)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_month_3383);
        _month_3383 = _1;
    }

    /** 	if year = Gregorian_Reformation and month = 9 then*/
    _1595 = (_year_3382 == 1752);
    if (_1595 == 0) {
        goto L1; // [11] 32
    }
    _1597 = (_month_3383 == 9);
    if (_1597 == 0)
    {
        DeRef(_1597);
        _1597 = NOVALUE;
        goto L1; // [20] 32
    }
    else{
        DeRef(_1597);
        _1597 = NOVALUE;
    }

    /** 		return 19*/
    DeRef(_1595);
    _1595 = NOVALUE;
    return 19;
    goto L2; // [29] 70
L1: 

    /** 	elsif month != 2 then*/
    if (_month_3383 == 2)
    goto L3; // [34] 51

    /** 		return DaysPerMonth[month]*/
    _2 = (int)SEQ_PTR(_10DaysPerMonth_3338);
    _1599 = (int)*(((s1_ptr)_2)->base + _month_3383);
    Ref(_1599);
    DeRef(_1595);
    _1595 = NOVALUE;
    return _1599;
    goto L2; // [48] 70
L3: 

    /** 		return DaysPerMonth[month] + isLeap(year)*/
    _2 = (int)SEQ_PTR(_10DaysPerMonth_3338);
    _1600 = (int)*(((s1_ptr)_2)->base + _month_3383);
    _1601 = _10isLeap(_year_3382);
    if (IS_ATOM_INT(_1600) && IS_ATOM_INT(_1601)) {
        _1602 = _1600 + _1601;
        if ((long)((unsigned long)_1602 + (unsigned long)HIGH_BITS) >= 0) 
        _1602 = NewDouble((double)_1602);
    }
    else {
        _1602 = binary_op(PLUS, _1600, _1601);
    }
    _1600 = NOVALUE;
    DeRef(_1601);
    _1601 = NOVALUE;
    DeRef(_1595);
    _1595 = NOVALUE;
    _1599 = NOVALUE;
    return _1602;
L2: 
    ;
}


int _10julianDayOfYear(int _ymd_3406)
{
    int _year_3407 = NOVALUE;
    int _month_3408 = NOVALUE;
    int _day_3409 = NOVALUE;
    int _d_3410 = NOVALUE;
    int _1618 = NOVALUE;
    int _1617 = NOVALUE;
    int _1616 = NOVALUE;
    int _1613 = NOVALUE;
    int _1612 = NOVALUE;
    int _0, _1, _2;
    

    /** 	year = ymd[1]*/
    _2 = (int)SEQ_PTR(_ymd_3406);
    _year_3407 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_year_3407)){
        _year_3407 = (long)DBL_PTR(_year_3407)->dbl;
    }

    /** 	month = ymd[2]*/
    _2 = (int)SEQ_PTR(_ymd_3406);
    _month_3408 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_month_3408)){
        _month_3408 = (long)DBL_PTR(_month_3408)->dbl;
    }

    /** 	day = ymd[3]*/
    _2 = (int)SEQ_PTR(_ymd_3406);
    _day_3409 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_day_3409)){
        _day_3409 = (long)DBL_PTR(_day_3409)->dbl;
    }

    /** 	if month = 1 then return day end if*/
    if (_month_3408 != 1)
    goto L1; // [27] 36
    DeRef(_ymd_3406);
    return _day_3409;
L1: 

    /** 	d = 0*/
    _d_3410 = 0;

    /** 	for i = 1 to month - 1 do*/
    _1612 = _month_3408 - 1;
    if ((long)((unsigned long)_1612 +(unsigned long) HIGH_BITS) >= 0){
        _1612 = NewDouble((double)_1612);
    }
    {
        int _i_3417;
        _i_3417 = 1;
L2: 
        if (binary_op_a(GREATER, _i_3417, _1612)){
            goto L3; // [47] 74
        }

        /** 		d += daysInMonth(year, i)*/
        Ref(_i_3417);
        _1613 = _10daysInMonth(_year_3407, _i_3417);
        if (IS_ATOM_INT(_1613)) {
            _d_3410 = _d_3410 + _1613;
        }
        else {
            _d_3410 = binary_op(PLUS, _d_3410, _1613);
        }
        DeRef(_1613);
        _1613 = NOVALUE;
        if (!IS_ATOM_INT(_d_3410)) {
            _1 = (long)(DBL_PTR(_d_3410)->dbl);
            if (UNIQUE(DBL_PTR(_d_3410)) && (DBL_PTR(_d_3410)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_d_3410);
            _d_3410 = _1;
        }

        /** 	end for*/
        _0 = _i_3417;
        if (IS_ATOM_INT(_i_3417)) {
            _i_3417 = _i_3417 + 1;
            if ((long)((unsigned long)_i_3417 +(unsigned long) HIGH_BITS) >= 0){
                _i_3417 = NewDouble((double)_i_3417);
            }
        }
        else {
            _i_3417 = binary_op_a(PLUS, _i_3417, 1);
        }
        DeRef(_0);
        goto L2; // [69] 54
L3: 
        ;
        DeRef(_i_3417);
    }

    /** 	d += day*/
    _d_3410 = _d_3410 + _day_3409;

    /** 	if year = Gregorian_Reformation and month = 9 then*/
    _1616 = (_year_3407 == 1752);
    if (_1616 == 0) {
        goto L4; // [86] 128
    }
    _1618 = (_month_3408 == 9);
    if (_1618 == 0)
    {
        DeRef(_1618);
        _1618 = NOVALUE;
        goto L4; // [95] 128
    }
    else{
        DeRef(_1618);
        _1618 = NOVALUE;
    }

    /** 		if day > 13 then*/
    if (_day_3409 <= 13)
    goto L5; // [100] 113

    /** 			d -= 11*/
    _d_3410 = _d_3410 - 11;
    goto L6; // [110] 127
L5: 

    /** 		elsif day > 2 then*/
    if (_day_3409 <= 2)
    goto L7; // [115] 126

    /** 			return 0*/
    DeRef(_ymd_3406);
    DeRef(_1612);
    _1612 = NOVALUE;
    DeRef(_1616);
    _1616 = NOVALUE;
    return 0;
L7: 
L6: 
L4: 

    /** 	return d*/
    DeRef(_ymd_3406);
    DeRef(_1612);
    _1612 = NOVALUE;
    DeRef(_1616);
    _1616 = NOVALUE;
    return _d_3410;
    ;
}


int _10julianDay(int _ymd_3433)
{
    int _year_3434 = NOVALUE;
    int _j_3435 = NOVALUE;
    int _greg00_3436 = NOVALUE;
    int _1647 = NOVALUE;
    int _1644 = NOVALUE;
    int _1641 = NOVALUE;
    int _1640 = NOVALUE;
    int _1639 = NOVALUE;
    int _1638 = NOVALUE;
    int _1637 = NOVALUE;
    int _1636 = NOVALUE;
    int _1635 = NOVALUE;
    int _1634 = NOVALUE;
    int _1632 = NOVALUE;
    int _1631 = NOVALUE;
    int _1630 = NOVALUE;
    int _1629 = NOVALUE;
    int _1628 = NOVALUE;
    int _1627 = NOVALUE;
    int _1626 = NOVALUE;
    int _0, _1, _2;
    

    /** 	year = ymd[1]*/
    _2 = (int)SEQ_PTR(_ymd_3433);
    _year_3434 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_year_3434)){
        _year_3434 = (long)DBL_PTR(_year_3434)->dbl;
    }

    /** 	j = julianDayOfYear(ymd)*/
    Ref(_ymd_3433);
    _j_3435 = _10julianDayOfYear(_ymd_3433);
    if (!IS_ATOM_INT(_j_3435)) {
        _1 = (long)(DBL_PTR(_j_3435)->dbl);
        if (UNIQUE(DBL_PTR(_j_3435)) && (DBL_PTR(_j_3435)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_j_3435);
        _j_3435 = _1;
    }

    /** 	year  -= 1*/
    _year_3434 = _year_3434 - 1;

    /** 	greg00 = year - Gregorian_Reformation00*/
    _greg00_3436 = _year_3434 - 1700;

    /** 	j += (*/
    if (_year_3434 <= INT15 && _year_3434 >= -INT15)
    _1626 = 365 * _year_3434;
    else
    _1626 = NewDouble(365 * (double)_year_3434);
    if (4 > 0 && _year_3434 >= 0) {
        _1627 = _year_3434 / 4;
    }
    else {
        temp_dbl = floor((double)_year_3434 / (double)4);
        _1627 = (long)temp_dbl;
    }
    if (IS_ATOM_INT(_1626)) {
        _1628 = _1626 + _1627;
        if ((long)((unsigned long)_1628 + (unsigned long)HIGH_BITS) >= 0) 
        _1628 = NewDouble((double)_1628);
    }
    else {
        _1628 = NewDouble(DBL_PTR(_1626)->dbl + (double)_1627);
    }
    DeRef(_1626);
    _1626 = NOVALUE;
    _1627 = NOVALUE;
    _1629 = (_greg00_3436 > 0);
    if (100 > 0 && _greg00_3436 >= 0) {
        _1630 = _greg00_3436 / 100;
    }
    else {
        temp_dbl = floor((double)_greg00_3436 / (double)100);
        _1630 = (long)temp_dbl;
    }
    _1631 = - _1630;
    _1632 = (_greg00_3436 % 400) ? NewDouble((double)_greg00_3436 / 400) : (_greg00_3436 / 400);
    if (IS_ATOM_INT(_1632)) {
        _1634 = NewDouble((double)_1632 + DBL_PTR(_1633)->dbl);
    }
    else {
        _1634 = NewDouble(DBL_PTR(_1632)->dbl + DBL_PTR(_1633)->dbl);
    }
    DeRef(_1632);
    _1632 = NOVALUE;
    _1635 = unary_op(FLOOR, _1634);
    DeRefDS(_1634);
    _1634 = NOVALUE;
    if (IS_ATOM_INT(_1635)) {
        _1636 = _1631 + _1635;
        if ((long)((unsigned long)_1636 + (unsigned long)HIGH_BITS) >= 0) 
        _1636 = NewDouble((double)_1636);
    }
    else {
        _1636 = binary_op(PLUS, _1631, _1635);
    }
    _1631 = NOVALUE;
    DeRef(_1635);
    _1635 = NOVALUE;
    if (IS_ATOM_INT(_1636)) {
        if (_1636 <= INT15 && _1636 >= -INT15)
        _1637 = _1629 * _1636;
        else
        _1637 = NewDouble(_1629 * (double)_1636);
    }
    else {
        _1637 = binary_op(MULTIPLY, _1629, _1636);
    }
    _1629 = NOVALUE;
    DeRef(_1636);
    _1636 = NOVALUE;
    if (IS_ATOM_INT(_1628) && IS_ATOM_INT(_1637)) {
        _1638 = _1628 + _1637;
        if ((long)((unsigned long)_1638 + (unsigned long)HIGH_BITS) >= 0) 
        _1638 = NewDouble((double)_1638);
    }
    else {
        _1638 = binary_op(PLUS, _1628, _1637);
    }
    DeRef(_1628);
    _1628 = NOVALUE;
    DeRef(_1637);
    _1637 = NOVALUE;
    _1639 = (_year_3434 >= 1752);
    _1640 = 11 * _1639;
    _1639 = NOVALUE;
    if (IS_ATOM_INT(_1638)) {
        _1641 = _1638 - _1640;
        if ((long)((unsigned long)_1641 +(unsigned long) HIGH_BITS) >= 0){
            _1641 = NewDouble((double)_1641);
        }
    }
    else {
        _1641 = binary_op(MINUS, _1638, _1640);
    }
    DeRef(_1638);
    _1638 = NOVALUE;
    _1640 = NOVALUE;
    if (IS_ATOM_INT(_1641)) {
        _j_3435 = _j_3435 + _1641;
    }
    else {
        _j_3435 = binary_op(PLUS, _j_3435, _1641);
    }
    DeRef(_1641);
    _1641 = NOVALUE;
    if (!IS_ATOM_INT(_j_3435)) {
        _1 = (long)(DBL_PTR(_j_3435)->dbl);
        if (UNIQUE(DBL_PTR(_j_3435)) && (DBL_PTR(_j_3435)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_j_3435);
        _j_3435 = _1;
    }

    /** 	if year >= 3200 then*/
    if (_year_3434 < 3200)
    goto L1; // [97] 133

    /** 		j -= floor(year/ 3200)*/
    if (3200 > 0 && _year_3434 >= 0) {
        _1644 = _year_3434 / 3200;
    }
    else {
        temp_dbl = floor((double)_year_3434 / (double)3200);
        _1644 = (long)temp_dbl;
    }
    _j_3435 = _j_3435 - _1644;
    _1644 = NOVALUE;

    /** 		if year >= 80000 then*/
    if (_year_3434 < 80000)
    goto L2; // [115] 132

    /** 			j += floor(year/80000)*/
    if (80000 > 0 && _year_3434 >= 0) {
        _1647 = _year_3434 / 80000;
    }
    else {
        temp_dbl = floor((double)_year_3434 / (double)80000);
        _1647 = (long)temp_dbl;
    }
    _j_3435 = _j_3435 + _1647;
    _1647 = NOVALUE;
L2: 
L1: 

    /** 	return j*/
    DeRef(_ymd_3433);
    DeRef(_1630);
    _1630 = NOVALUE;
    return _j_3435;
    ;
}


int _10datetimeToSeconds(int _dt_3523)
{
    int _1691 = NOVALUE;
    int _1690 = NOVALUE;
    int _1689 = NOVALUE;
    int _1688 = NOVALUE;
    int _1687 = NOVALUE;
    int _1686 = NOVALUE;
    int _1685 = NOVALUE;
    int _1684 = NOVALUE;
    int _1683 = NOVALUE;
    int _1682 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return julianDay(dt) * DayLengthInSeconds + (dt[4] * 60 + dt[5]) * 60 + dt[6]*/
    Ref(_dt_3523);
    _1682 = _10julianDay(_dt_3523);
    if (IS_ATOM_INT(_1682)) {
        _1683 = NewDouble(_1682 * (double)86400);
    }
    else {
        _1683 = binary_op(MULTIPLY, _1682, 86400);
    }
    DeRef(_1682);
    _1682 = NOVALUE;
    _2 = (int)SEQ_PTR(_dt_3523);
    _1684 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_1684)) {
        if (_1684 == (short)_1684)
        _1685 = _1684 * 60;
        else
        _1685 = NewDouble(_1684 * (double)60);
    }
    else {
        _1685 = binary_op(MULTIPLY, _1684, 60);
    }
    _1684 = NOVALUE;
    _2 = (int)SEQ_PTR(_dt_3523);
    _1686 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_ATOM_INT(_1685) && IS_ATOM_INT(_1686)) {
        _1687 = _1685 + _1686;
        if ((long)((unsigned long)_1687 + (unsigned long)HIGH_BITS) >= 0) 
        _1687 = NewDouble((double)_1687);
    }
    else {
        _1687 = binary_op(PLUS, _1685, _1686);
    }
    DeRef(_1685);
    _1685 = NOVALUE;
    _1686 = NOVALUE;
    if (IS_ATOM_INT(_1687)) {
        if (_1687 == (short)_1687)
        _1688 = _1687 * 60;
        else
        _1688 = NewDouble(_1687 * (double)60);
    }
    else {
        _1688 = binary_op(MULTIPLY, _1687, 60);
    }
    DeRef(_1687);
    _1687 = NOVALUE;
    if (IS_ATOM_INT(_1683) && IS_ATOM_INT(_1688)) {
        _1689 = _1683 + _1688;
        if ((long)((unsigned long)_1689 + (unsigned long)HIGH_BITS) >= 0) 
        _1689 = NewDouble((double)_1689);
    }
    else {
        _1689 = binary_op(PLUS, _1683, _1688);
    }
    DeRef(_1683);
    _1683 = NOVALUE;
    DeRef(_1688);
    _1688 = NOVALUE;
    _2 = (int)SEQ_PTR(_dt_3523);
    _1690 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_ATOM_INT(_1689) && IS_ATOM_INT(_1690)) {
        _1691 = _1689 + _1690;
        if ((long)((unsigned long)_1691 + (unsigned long)HIGH_BITS) >= 0) 
        _1691 = NewDouble((double)_1691);
    }
    else {
        _1691 = binary_op(PLUS, _1689, _1690);
    }
    DeRef(_1689);
    _1689 = NOVALUE;
    _1690 = NOVALUE;
    DeRef(_dt_3523);
    return _1691;
    ;
}


int _10from_date(int _src_3701)
{
    int _1818 = NOVALUE;
    int _1817 = NOVALUE;
    int _1816 = NOVALUE;
    int _1815 = NOVALUE;
    int _1814 = NOVALUE;
    int _1813 = NOVALUE;
    int _1812 = NOVALUE;
    int _1810 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return {src[YEAR]+1900, src[MONTH], src[DAY], src[HOUR], src[MINUTE], src[SECOND]}*/
    _2 = (int)SEQ_PTR(_src_3701);
    _1810 = (int)*(((s1_ptr)_2)->base + 1);
    _1812 = _1810 + 1900;
    if ((long)((unsigned long)_1812 + (unsigned long)HIGH_BITS) >= 0) 
    _1812 = NewDouble((double)_1812);
    _1810 = NOVALUE;
    _2 = (int)SEQ_PTR(_src_3701);
    _1813 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_src_3701);
    _1814 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_src_3701);
    _1815 = (int)*(((s1_ptr)_2)->base + 4);
    _2 = (int)SEQ_PTR(_src_3701);
    _1816 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_src_3701);
    _1817 = (int)*(((s1_ptr)_2)->base + 6);
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _1812;
    *((int *)(_2+8)) = _1813;
    *((int *)(_2+12)) = _1814;
    *((int *)(_2+16)) = _1815;
    *((int *)(_2+20)) = _1816;
    *((int *)(_2+24)) = _1817;
    _1818 = MAKE_SEQ(_1);
    _1817 = NOVALUE;
    _1816 = NOVALUE;
    _1815 = NOVALUE;
    _1814 = NOVALUE;
    _1813 = NOVALUE;
    _1812 = NOVALUE;
    DeRefDSi(_src_3701);
    return _1818;
    ;
}


int _10now_gmt()
{
    int _t1_3717 = NOVALUE;
    int _1831 = NOVALUE;
    int _1830 = NOVALUE;
    int _1829 = NOVALUE;
    int _1828 = NOVALUE;
    int _1827 = NOVALUE;
    int _1826 = NOVALUE;
    int _1825 = NOVALUE;
    int _1824 = NOVALUE;
    int _1823 = NOVALUE;
    int _1821 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence t1 = gmtime(time())*/
    _1821 = _10time();
    _0 = _t1_3717;
    _t1_3717 = _10gmtime(_1821);
    DeRef(_0);
    _1821 = NOVALUE;

    /** 	return { */
    _2 = (int)SEQ_PTR(_t1_3717);
    _1823 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_ATOM_INT(_1823)) {
        _1824 = _1823 + 1900;
        if ((long)((unsigned long)_1824 + (unsigned long)HIGH_BITS) >= 0) 
        _1824 = NewDouble((double)_1824);
    }
    else {
        _1824 = binary_op(PLUS, _1823, 1900);
    }
    _1823 = NOVALUE;
    _2 = (int)SEQ_PTR(_t1_3717);
    _1825 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_ATOM_INT(_1825)) {
        _1826 = _1825 + 1;
        if (_1826 > MAXINT){
            _1826 = NewDouble((double)_1826);
        }
    }
    else
    _1826 = binary_op(PLUS, 1, _1825);
    _1825 = NOVALUE;
    _2 = (int)SEQ_PTR(_t1_3717);
    _1827 = (int)*(((s1_ptr)_2)->base + 4);
    _2 = (int)SEQ_PTR(_t1_3717);
    _1828 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_t1_3717);
    _1829 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_t1_3717);
    _1830 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _1824;
    *((int *)(_2+8)) = _1826;
    Ref(_1827);
    *((int *)(_2+12)) = _1827;
    Ref(_1828);
    *((int *)(_2+16)) = _1828;
    Ref(_1829);
    *((int *)(_2+20)) = _1829;
    Ref(_1830);
    *((int *)(_2+24)) = _1830;
    _1831 = MAKE_SEQ(_1);
    _1830 = NOVALUE;
    _1829 = NOVALUE;
    _1828 = NOVALUE;
    _1827 = NOVALUE;
    _1826 = NOVALUE;
    _1824 = NOVALUE;
    DeRefDS(_t1_3717);
    return _1831;
    ;
}


int _10new(int _year_3731, int _month_3732, int _day_3733, int _hour_3734, int _minute_3735, int _second_3736)
{
    int _d_3737 = NOVALUE;
    int _now_1__tmp_at41_3744 = NOVALUE;
    int _now_inlined_now_at_41_3743 = NOVALUE;
    int _1834 = NOVALUE;
    int _1833 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_year_3731)) {
        _1 = (long)(DBL_PTR(_year_3731)->dbl);
        if (UNIQUE(DBL_PTR(_year_3731)) && (DBL_PTR(_year_3731)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_year_3731);
        _year_3731 = _1;
    }
    if (!IS_ATOM_INT(_month_3732)) {
        _1 = (long)(DBL_PTR(_month_3732)->dbl);
        if (UNIQUE(DBL_PTR(_month_3732)) && (DBL_PTR(_month_3732)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_month_3732);
        _month_3732 = _1;
    }
    if (!IS_ATOM_INT(_day_3733)) {
        _1 = (long)(DBL_PTR(_day_3733)->dbl);
        if (UNIQUE(DBL_PTR(_day_3733)) && (DBL_PTR(_day_3733)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_day_3733);
        _day_3733 = _1;
    }
    if (!IS_ATOM_INT(_hour_3734)) {
        _1 = (long)(DBL_PTR(_hour_3734)->dbl);
        if (UNIQUE(DBL_PTR(_hour_3734)) && (DBL_PTR(_hour_3734)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_hour_3734);
        _hour_3734 = _1;
    }
    if (!IS_ATOM_INT(_minute_3735)) {
        _1 = (long)(DBL_PTR(_minute_3735)->dbl);
        if (UNIQUE(DBL_PTR(_minute_3735)) && (DBL_PTR(_minute_3735)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_minute_3735);
        _minute_3735 = _1;
    }

    /** 	d = {year, month, day, hour, minute, second}*/
    _0 = _d_3737;
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _year_3731;
    *((int *)(_2+8)) = _month_3732;
    *((int *)(_2+12)) = _day_3733;
    *((int *)(_2+16)) = _hour_3734;
    *((int *)(_2+20)) = _minute_3735;
    Ref(_second_3736);
    *((int *)(_2+24)) = _second_3736;
    _d_3737 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 	if equal(d, {0,0,0,0,0,0}) then*/
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 0;
    *((int *)(_2+20)) = 0;
    *((int *)(_2+24)) = 0;
    _1833 = MAKE_SEQ(_1);
    if (_d_3737 == _1833)
    _1834 = 1;
    else if (IS_ATOM_INT(_d_3737) && IS_ATOM_INT(_1833))
    _1834 = 0;
    else
    _1834 = (compare(_d_3737, _1833) == 0);
    DeRefDS(_1833);
    _1833 = NOVALUE;
    if (_1834 == 0)
    {
        _1834 = NOVALUE;
        goto L1; // [37] 60
    }
    else{
        _1834 = NOVALUE;
    }

    /** 		return now()*/

    /** 	return from_date(date())*/
    DeRefi(_now_1__tmp_at41_3744);
    _now_1__tmp_at41_3744 = Date();
    RefDS(_now_1__tmp_at41_3744);
    _0 = _now_inlined_now_at_41_3743;
    _now_inlined_now_at_41_3743 = _10from_date(_now_1__tmp_at41_3744);
    DeRef(_0);
    DeRefi(_now_1__tmp_at41_3744);
    _now_1__tmp_at41_3744 = NOVALUE;
    DeRef(_second_3736);
    DeRef(_d_3737);
    return _now_inlined_now_at_41_3743;
    goto L2; // [57] 67
L1: 

    /** 		return d*/
    DeRef(_second_3736);
    return _d_3737;
L2: 
    ;
}


int _10format(int _d_3800, int _pattern_3801)
{
    int _in_fmt_3803 = NOVALUE;
    int _ch_3804 = NOVALUE;
    int _tmp_3805 = NOVALUE;
    int _res_3806 = NOVALUE;
    int _weeks_day_4__tmp_at67_3822 = NOVALUE;
    int _weeks_day_3__tmp_at67_3821 = NOVALUE;
    int _weeks_day_2__tmp_at67_3820 = NOVALUE;
    int _weeks_day_1__tmp_at67_3819 = NOVALUE;
    int _weeks_day_inlined_weeks_day_at_67_3818 = NOVALUE;
    int _weeks_day_4__tmp_at113_3832 = NOVALUE;
    int _weeks_day_3__tmp_at113_3831 = NOVALUE;
    int _weeks_day_2__tmp_at113_3830 = NOVALUE;
    int _weeks_day_1__tmp_at113_3829 = NOVALUE;
    int _weeks_day_inlined_weeks_day_at_113_3828 = NOVALUE;
    int _to_unix_1__tmp_at608_3937 = NOVALUE;
    int _to_unix_inlined_to_unix_at_608_3936 = NOVALUE;
    int _weeks_day_4__tmp_at665_3954 = NOVALUE;
    int _weeks_day_3__tmp_at665_3953 = NOVALUE;
    int _weeks_day_2__tmp_at665_3952 = NOVALUE;
    int _weeks_day_1__tmp_at665_3951 = NOVALUE;
    int _weeks_day_inlined_weeks_day_at_665_3950 = NOVALUE;
    int _weeks_day_4__tmp_at709_3965 = NOVALUE;
    int _weeks_day_3__tmp_at709_3964 = NOVALUE;
    int _weeks_day_2__tmp_at709_3963 = NOVALUE;
    int _weeks_day_1__tmp_at709_3962 = NOVALUE;
    int _weeks_day_inlined_weeks_day_at_709_3961 = NOVALUE;
    int _weeks_day_4__tmp_at758_3977 = NOVALUE;
    int _weeks_day_3__tmp_at758_3976 = NOVALUE;
    int _weeks_day_2__tmp_at758_3975 = NOVALUE;
    int _weeks_day_1__tmp_at758_3974 = NOVALUE;
    int _weeks_day_inlined_weeks_day_at_758_3973 = NOVALUE;
    int _1979 = NOVALUE;
    int _1978 = NOVALUE;
    int _1973 = NOVALUE;
    int _1972 = NOVALUE;
    int _1971 = NOVALUE;
    int _1970 = NOVALUE;
    int _1968 = NOVALUE;
    int _1964 = NOVALUE;
    int _1963 = NOVALUE;
    int _1959 = NOVALUE;
    int _1958 = NOVALUE;
    int _1951 = NOVALUE;
    int _1950 = NOVALUE;
    int _1946 = NOVALUE;
    int _1942 = NOVALUE;
    int _1941 = NOVALUE;
    int _1939 = NOVALUE;
    int _1938 = NOVALUE;
    int _1936 = NOVALUE;
    int _1932 = NOVALUE;
    int _1930 = NOVALUE;
    int _1928 = NOVALUE;
    int _1924 = NOVALUE;
    int _1923 = NOVALUE;
    int _1919 = NOVALUE;
    int _1918 = NOVALUE;
    int _1914 = NOVALUE;
    int _1906 = NOVALUE;
    int _1905 = NOVALUE;
    int _1901 = NOVALUE;
    int _1900 = NOVALUE;
    int _1896 = NOVALUE;
    int _1888 = NOVALUE;
    int _1887 = NOVALUE;
    int _1884 = NOVALUE;
    int _1883 = NOVALUE;
    int _1880 = NOVALUE;
    int _1879 = NOVALUE;
    int _1878 = NOVALUE;
    int _1874 = NOVALUE;
    int _1873 = NOVALUE;
    int _1870 = NOVALUE;
    int _1869 = NOVALUE;
    int _1866 = NOVALUE;
    int _1863 = NOVALUE;
    int _1858 = NOVALUE;
    int _0, _1, _2;
    

    /** 	in_fmt = 0*/
    _in_fmt_3803 = 0;

    /** 	res = ""*/
    RefDS(_5);
    DeRef(_res_3806);
    _res_3806 = _5;

    /** 	for i = 1 to length(pattern) do*/
    _1858 = 12;
    {
        int _i_3808;
        _i_3808 = 1;
L1: 
        if (_i_3808 > 12){
            goto L2; // [20] 903
        }

        /** 		ch = pattern[i]*/
        _2 = (int)SEQ_PTR(_pattern_3801);
        _ch_3804 = (int)*(((s1_ptr)_2)->base + _i_3808);

        /** 		if in_fmt then*/
        if (_in_fmt_3803 == 0)
        {
            goto L3; // [35] 875
        }
        else{
        }

        /** 			in_fmt = 0*/
        _in_fmt_3803 = 0;

        /** 			if ch = '%' then*/
        if (_ch_3804 != 37)
        goto L4; // [45] 58

        /** 				res &= '%'*/
        Append(&_res_3806, _res_3806, 37);
        goto L5; // [55] 896
L4: 

        /** 			elsif ch = 'a' then*/
        if (_ch_3804 != 97)
        goto L6; // [60] 104

        /** 				res &= day_abbrs[weeks_day(d)]*/

        /** 	return remainder(julianDay(dt)-1+4094, 7) + 1*/
        Ref(_d_3800);
        _0 = _weeks_day_1__tmp_at67_3819;
        _weeks_day_1__tmp_at67_3819 = _10julianDay(_d_3800);
        DeRef(_0);
        DeRef(_weeks_day_2__tmp_at67_3820);
        if (IS_ATOM_INT(_weeks_day_1__tmp_at67_3819)) {
            _weeks_day_2__tmp_at67_3820 = _weeks_day_1__tmp_at67_3819 - 1;
            if ((long)((unsigned long)_weeks_day_2__tmp_at67_3820 +(unsigned long) HIGH_BITS) >= 0){
                _weeks_day_2__tmp_at67_3820 = NewDouble((double)_weeks_day_2__tmp_at67_3820);
            }
        }
        else {
            _weeks_day_2__tmp_at67_3820 = binary_op(MINUS, _weeks_day_1__tmp_at67_3819, 1);
        }
        DeRef(_weeks_day_3__tmp_at67_3821);
        if (IS_ATOM_INT(_weeks_day_2__tmp_at67_3820)) {
            _weeks_day_3__tmp_at67_3821 = _weeks_day_2__tmp_at67_3820 + 4094;
            if ((long)((unsigned long)_weeks_day_3__tmp_at67_3821 + (unsigned long)HIGH_BITS) >= 0) 
            _weeks_day_3__tmp_at67_3821 = NewDouble((double)_weeks_day_3__tmp_at67_3821);
        }
        else {
            _weeks_day_3__tmp_at67_3821 = binary_op(PLUS, _weeks_day_2__tmp_at67_3820, 4094);
        }
        DeRef(_weeks_day_4__tmp_at67_3822);
        if (IS_ATOM_INT(_weeks_day_3__tmp_at67_3821)) {
            _weeks_day_4__tmp_at67_3822 = (_weeks_day_3__tmp_at67_3821 % 7);
        }
        else {
            _weeks_day_4__tmp_at67_3822 = binary_op(REMAINDER, _weeks_day_3__tmp_at67_3821, 7);
        }
        DeRef(_weeks_day_inlined_weeks_day_at_67_3818);
        if (IS_ATOM_INT(_weeks_day_4__tmp_at67_3822)) {
            _weeks_day_inlined_weeks_day_at_67_3818 = _weeks_day_4__tmp_at67_3822 + 1;
            if (_weeks_day_inlined_weeks_day_at_67_3818 > MAXINT){
                _weeks_day_inlined_weeks_day_at_67_3818 = NewDouble((double)_weeks_day_inlined_weeks_day_at_67_3818);
            }
        }
        else
        _weeks_day_inlined_weeks_day_at_67_3818 = binary_op(PLUS, 1, _weeks_day_4__tmp_at67_3822);
        DeRef(_weeks_day_1__tmp_at67_3819);
        _weeks_day_1__tmp_at67_3819 = NOVALUE;
        DeRef(_weeks_day_2__tmp_at67_3820);
        _weeks_day_2__tmp_at67_3820 = NOVALUE;
        DeRef(_weeks_day_3__tmp_at67_3821);
        _weeks_day_3__tmp_at67_3821 = NOVALUE;
        DeRef(_weeks_day_4__tmp_at67_3822);
        _weeks_day_4__tmp_at67_3822 = NOVALUE;
        _2 = (int)SEQ_PTR(_10day_abbrs_3588);
        if (!IS_ATOM_INT(_weeks_day_inlined_weeks_day_at_67_3818)){
            _1863 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_weeks_day_inlined_weeks_day_at_67_3818)->dbl));
        }
        else{
            _1863 = (int)*(((s1_ptr)_2)->base + _weeks_day_inlined_weeks_day_at_67_3818);
        }
        Concat((object_ptr)&_res_3806, _res_3806, _1863);
        _1863 = NOVALUE;
        goto L5; // [101] 896
L6: 

        /** 			elsif ch = 'A' then*/
        if (_ch_3804 != 65)
        goto L7; // [106] 150

        /** 				res &= day_names[weeks_day(d)]*/

        /** 	return remainder(julianDay(dt)-1+4094, 7) + 1*/
        Ref(_d_3800);
        _0 = _weeks_day_1__tmp_at113_3829;
        _weeks_day_1__tmp_at113_3829 = _10julianDay(_d_3800);
        DeRef(_0);
        DeRef(_weeks_day_2__tmp_at113_3830);
        if (IS_ATOM_INT(_weeks_day_1__tmp_at113_3829)) {
            _weeks_day_2__tmp_at113_3830 = _weeks_day_1__tmp_at113_3829 - 1;
            if ((long)((unsigned long)_weeks_day_2__tmp_at113_3830 +(unsigned long) HIGH_BITS) >= 0){
                _weeks_day_2__tmp_at113_3830 = NewDouble((double)_weeks_day_2__tmp_at113_3830);
            }
        }
        else {
            _weeks_day_2__tmp_at113_3830 = binary_op(MINUS, _weeks_day_1__tmp_at113_3829, 1);
        }
        DeRef(_weeks_day_3__tmp_at113_3831);
        if (IS_ATOM_INT(_weeks_day_2__tmp_at113_3830)) {
            _weeks_day_3__tmp_at113_3831 = _weeks_day_2__tmp_at113_3830 + 4094;
            if ((long)((unsigned long)_weeks_day_3__tmp_at113_3831 + (unsigned long)HIGH_BITS) >= 0) 
            _weeks_day_3__tmp_at113_3831 = NewDouble((double)_weeks_day_3__tmp_at113_3831);
        }
        else {
            _weeks_day_3__tmp_at113_3831 = binary_op(PLUS, _weeks_day_2__tmp_at113_3830, 4094);
        }
        DeRef(_weeks_day_4__tmp_at113_3832);
        if (IS_ATOM_INT(_weeks_day_3__tmp_at113_3831)) {
            _weeks_day_4__tmp_at113_3832 = (_weeks_day_3__tmp_at113_3831 % 7);
        }
        else {
            _weeks_day_4__tmp_at113_3832 = binary_op(REMAINDER, _weeks_day_3__tmp_at113_3831, 7);
        }
        DeRef(_weeks_day_inlined_weeks_day_at_113_3828);
        if (IS_ATOM_INT(_weeks_day_4__tmp_at113_3832)) {
            _weeks_day_inlined_weeks_day_at_113_3828 = _weeks_day_4__tmp_at113_3832 + 1;
            if (_weeks_day_inlined_weeks_day_at_113_3828 > MAXINT){
                _weeks_day_inlined_weeks_day_at_113_3828 = NewDouble((double)_weeks_day_inlined_weeks_day_at_113_3828);
            }
        }
        else
        _weeks_day_inlined_weeks_day_at_113_3828 = binary_op(PLUS, 1, _weeks_day_4__tmp_at113_3832);
        DeRef(_weeks_day_1__tmp_at113_3829);
        _weeks_day_1__tmp_at113_3829 = NOVALUE;
        DeRef(_weeks_day_2__tmp_at113_3830);
        _weeks_day_2__tmp_at113_3830 = NOVALUE;
        DeRef(_weeks_day_3__tmp_at113_3831);
        _weeks_day_3__tmp_at113_3831 = NOVALUE;
        DeRef(_weeks_day_4__tmp_at113_3832);
        _weeks_day_4__tmp_at113_3832 = NOVALUE;
        _2 = (int)SEQ_PTR(_10day_names_3579);
        if (!IS_ATOM_INT(_weeks_day_inlined_weeks_day_at_113_3828)){
            _1866 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_weeks_day_inlined_weeks_day_at_113_3828)->dbl));
        }
        else{
            _1866 = (int)*(((s1_ptr)_2)->base + _weeks_day_inlined_weeks_day_at_113_3828);
        }
        Concat((object_ptr)&_res_3806, _res_3806, _1866);
        _1866 = NOVALUE;
        goto L5; // [147] 896
L7: 

        /** 			elsif ch = 'b' then*/
        if (_ch_3804 != 98)
        goto L8; // [152] 177

        /** 				res &= month_abbrs[d[MONTH]]*/
        _2 = (int)SEQ_PTR(_d_3800);
        _1869 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_10month_abbrs_3566);
        if (!IS_ATOM_INT(_1869)){
            _1870 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_1869)->dbl));
        }
        else{
            _1870 = (int)*(((s1_ptr)_2)->base + _1869);
        }
        Concat((object_ptr)&_res_3806, _res_3806, _1870);
        _1870 = NOVALUE;
        goto L5; // [174] 896
L8: 

        /** 			elsif ch = 'B' then*/
        if (_ch_3804 != 66)
        goto L9; // [179] 204

        /** 				res &= month_names[d[MONTH]]*/
        _2 = (int)SEQ_PTR(_d_3800);
        _1873 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_10month_names_3552);
        if (!IS_ATOM_INT(_1873)){
            _1874 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_1873)->dbl));
        }
        else{
            _1874 = (int)*(((s1_ptr)_2)->base + _1873);
        }
        Concat((object_ptr)&_res_3806, _res_3806, _1874);
        _1874 = NOVALUE;
        goto L5; // [201] 896
L9: 

        /** 			elsif ch = 'C' then*/
        if (_ch_3804 != 67)
        goto LA; // [206] 233

        /** 				res &= sprintf("%02d", d[YEAR] / 100)*/
        _2 = (int)SEQ_PTR(_d_3800);
        _1878 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_1878)) {
            _1879 = (_1878 % 100) ? NewDouble((double)_1878 / 100) : (_1878 / 100);
        }
        else {
            _1879 = binary_op(DIVIDE, _1878, 100);
        }
        _1878 = NOVALUE;
        _1880 = EPrintf(-9999999, _1877, _1879);
        DeRef(_1879);
        _1879 = NOVALUE;
        Concat((object_ptr)&_res_3806, _res_3806, _1880);
        DeRefDS(_1880);
        _1880 = NOVALUE;
        goto L5; // [230] 896
LA: 

        /** 			elsif ch = 'd' then*/
        if (_ch_3804 != 100)
        goto LB; // [235] 258

        /** 				res &= sprintf("%02d", d[DAY])*/
        _2 = (int)SEQ_PTR(_d_3800);
        _1883 = (int)*(((s1_ptr)_2)->base + 3);
        _1884 = EPrintf(-9999999, _1877, _1883);
        _1883 = NOVALUE;
        Concat((object_ptr)&_res_3806, _res_3806, _1884);
        DeRefDS(_1884);
        _1884 = NOVALUE;
        goto L5; // [255] 896
LB: 

        /** 			elsif ch = 'H' then*/
        if (_ch_3804 != 72)
        goto LC; // [260] 283

        /** 				res &= sprintf("%02d", d[HOUR])*/
        _2 = (int)SEQ_PTR(_d_3800);
        _1887 = (int)*(((s1_ptr)_2)->base + 4);
        _1888 = EPrintf(-9999999, _1877, _1887);
        _1887 = NOVALUE;
        Concat((object_ptr)&_res_3806, _res_3806, _1888);
        DeRefDS(_1888);
        _1888 = NOVALUE;
        goto L5; // [280] 896
LC: 

        /** 			elsif ch = 'I' then*/
        if (_ch_3804 != 73)
        goto LD; // [285] 340

        /** 				tmp = d[HOUR]*/
        _2 = (int)SEQ_PTR(_d_3800);
        _tmp_3805 = (int)*(((s1_ptr)_2)->base + 4);
        if (!IS_ATOM_INT(_tmp_3805)){
            _tmp_3805 = (long)DBL_PTR(_tmp_3805)->dbl;
        }

        /** 				if tmp > 12 then*/
        if (_tmp_3805 <= 12)
        goto LE; // [301] 314

        /** 					tmp -= 12*/
        _tmp_3805 = _tmp_3805 - 12;
        goto LF; // [311] 327
LE: 

        /** 				elsif tmp = 0 then*/
        if (_tmp_3805 != 0)
        goto L10; // [316] 326

        /** 					tmp = 12*/
        _tmp_3805 = 12;
L10: 
LF: 

        /** 				res &= sprintf("%02d", tmp)*/
        _1896 = EPrintf(-9999999, _1877, _tmp_3805);
        Concat((object_ptr)&_res_3806, _res_3806, _1896);
        DeRefDS(_1896);
        _1896 = NOVALUE;
        goto L5; // [337] 896
LD: 

        /** 			elsif ch = 'j' then*/
        if (_ch_3804 != 106)
        goto L11; // [342] 363

        /** 				res &= sprintf("%d", julianDayOfYear(d))*/
        Ref(_d_3800);
        _1900 = _10julianDayOfYear(_d_3800);
        _1901 = EPrintf(-9999999, _740, _1900);
        DeRef(_1900);
        _1900 = NOVALUE;
        Concat((object_ptr)&_res_3806, _res_3806, _1901);
        DeRefDS(_1901);
        _1901 = NOVALUE;
        goto L5; // [360] 896
L11: 

        /** 			elsif ch = 'k' then*/
        if (_ch_3804 != 107)
        goto L12; // [365] 388

        /** 				res &= sprintf("%d", d[HOUR])*/
        _2 = (int)SEQ_PTR(_d_3800);
        _1905 = (int)*(((s1_ptr)_2)->base + 4);
        _1906 = EPrintf(-9999999, _740, _1905);
        _1905 = NOVALUE;
        Concat((object_ptr)&_res_3806, _res_3806, _1906);
        DeRefDS(_1906);
        _1906 = NOVALUE;
        goto L5; // [385] 896
L12: 

        /** 			elsif ch = 'l' then*/
        if (_ch_3804 != 108)
        goto L13; // [390] 445

        /** 				tmp = d[HOUR]*/
        _2 = (int)SEQ_PTR(_d_3800);
        _tmp_3805 = (int)*(((s1_ptr)_2)->base + 4);
        if (!IS_ATOM_INT(_tmp_3805)){
            _tmp_3805 = (long)DBL_PTR(_tmp_3805)->dbl;
        }

        /** 				if tmp > 12 then*/
        if (_tmp_3805 <= 12)
        goto L14; // [406] 419

        /** 					tmp -= 12*/
        _tmp_3805 = _tmp_3805 - 12;
        goto L15; // [416] 432
L14: 

        /** 				elsif tmp = 0 then*/
        if (_tmp_3805 != 0)
        goto L16; // [421] 431

        /** 					tmp = 12*/
        _tmp_3805 = 12;
L16: 
L15: 

        /** 				res &= sprintf("%d", tmp)*/
        _1914 = EPrintf(-9999999, _740, _tmp_3805);
        Concat((object_ptr)&_res_3806, _res_3806, _1914);
        DeRefDS(_1914);
        _1914 = NOVALUE;
        goto L5; // [442] 896
L13: 

        /** 			elsif ch = 'm' then*/
        if (_ch_3804 != 109)
        goto L17; // [447] 470

        /** 				res &= sprintf("%02d", d[MONTH])*/
        _2 = (int)SEQ_PTR(_d_3800);
        _1918 = (int)*(((s1_ptr)_2)->base + 2);
        _1919 = EPrintf(-9999999, _1877, _1918);
        _1918 = NOVALUE;
        Concat((object_ptr)&_res_3806, _res_3806, _1919);
        DeRefDS(_1919);
        _1919 = NOVALUE;
        goto L5; // [467] 896
L17: 

        /** 			elsif ch = 'M' then*/
        if (_ch_3804 != 77)
        goto L18; // [472] 495

        /** 				res &= sprintf("%02d", d[MINUTE])*/
        _2 = (int)SEQ_PTR(_d_3800);
        _1923 = (int)*(((s1_ptr)_2)->base + 5);
        _1924 = EPrintf(-9999999, _1877, _1923);
        _1923 = NOVALUE;
        Concat((object_ptr)&_res_3806, _res_3806, _1924);
        DeRefDS(_1924);
        _1924 = NOVALUE;
        goto L5; // [492] 896
L18: 

        /** 			elsif ch = 'p' then*/
        if (_ch_3804 != 112)
        goto L19; // [497] 544

        /** 				if d[HOUR] <= 12 then*/
        _2 = (int)SEQ_PTR(_d_3800);
        _1928 = (int)*(((s1_ptr)_2)->base + 4);
        if (binary_op_a(GREATER, _1928, 12)){
            _1928 = NOVALUE;
            goto L1A; // [509] 528
        }
        _1928 = NOVALUE;

        /** 					res &= ampm[1]*/
        _2 = (int)SEQ_PTR(_10ampm_3597);
        _1930 = (int)*(((s1_ptr)_2)->base + 1);
        Concat((object_ptr)&_res_3806, _res_3806, _1930);
        _1930 = NOVALUE;
        goto L5; // [525] 896
L1A: 

        /** 					res &= ampm[2]*/
        _2 = (int)SEQ_PTR(_10ampm_3597);
        _1932 = (int)*(((s1_ptr)_2)->base + 2);
        Concat((object_ptr)&_res_3806, _res_3806, _1932);
        _1932 = NOVALUE;
        goto L5; // [541] 896
L19: 

        /** 			elsif ch = 'P' then*/
        if (_ch_3804 != 80)
        goto L1B; // [546] 601

        /** 				if d[HOUR] <= 12 then*/
        _2 = (int)SEQ_PTR(_d_3800);
        _1936 = (int)*(((s1_ptr)_2)->base + 4);
        if (binary_op_a(GREATER, _1936, 12)){
            _1936 = NOVALUE;
            goto L1C; // [558] 581
        }
        _1936 = NOVALUE;

        /** 					res &= tolower(ampm[1])*/
        _2 = (int)SEQ_PTR(_10ampm_3597);
        _1938 = (int)*(((s1_ptr)_2)->base + 1);
        RefDS(_1938);
        _1939 = _10tolower(_1938);
        _1938 = NOVALUE;
        if (IS_SEQUENCE(_res_3806) && IS_ATOM(_1939)) {
            Ref(_1939);
            Append(&_res_3806, _res_3806, _1939);
        }
        else if (IS_ATOM(_res_3806) && IS_SEQUENCE(_1939)) {
        }
        else {
            Concat((object_ptr)&_res_3806, _res_3806, _1939);
        }
        DeRef(_1939);
        _1939 = NOVALUE;
        goto L5; // [578] 896
L1C: 

        /** 					res &= tolower(ampm[2])*/
        _2 = (int)SEQ_PTR(_10ampm_3597);
        _1941 = (int)*(((s1_ptr)_2)->base + 2);
        RefDS(_1941);
        _1942 = _10tolower(_1941);
        _1941 = NOVALUE;
        if (IS_SEQUENCE(_res_3806) && IS_ATOM(_1942)) {
            Ref(_1942);
            Append(&_res_3806, _res_3806, _1942);
        }
        else if (IS_ATOM(_res_3806) && IS_SEQUENCE(_1942)) {
        }
        else {
            Concat((object_ptr)&_res_3806, _res_3806, _1942);
        }
        DeRef(_1942);
        _1942 = NOVALUE;
        goto L5; // [598] 896
L1B: 

        /** 			elsif ch = 's' then*/
        if (_ch_3804 != 115)
        goto L1D; // [603] 633

        /** 				res &= sprintf("%d", to_unix(d))*/

        /** 	return datetimeToSeconds(dt) - EPOCH_1970*/
        Ref(_d_3800);
        _0 = _to_unix_1__tmp_at608_3937;
        _to_unix_1__tmp_at608_3937 = _10datetimeToSeconds(_d_3800);
        DeRef(_0);
        DeRef(_to_unix_inlined_to_unix_at_608_3936);
        _to_unix_inlined_to_unix_at_608_3936 = binary_op(MINUS, _to_unix_1__tmp_at608_3937, _10EPOCH_1970_3341);
        DeRef(_to_unix_1__tmp_at608_3937);
        _to_unix_1__tmp_at608_3937 = NOVALUE;
        _1946 = EPrintf(-9999999, _740, _to_unix_inlined_to_unix_at_608_3936);
        Concat((object_ptr)&_res_3806, _res_3806, _1946);
        DeRefDS(_1946);
        _1946 = NOVALUE;
        goto L5; // [630] 896
L1D: 

        /** 			elsif ch = 'S' then*/
        if (_ch_3804 != 83)
        goto L1E; // [635] 658

        /** 				res &= sprintf("%02d", d[SECOND])*/
        _2 = (int)SEQ_PTR(_d_3800);
        _1950 = (int)*(((s1_ptr)_2)->base + 6);
        _1951 = EPrintf(-9999999, _1877, _1950);
        _1950 = NOVALUE;
        Concat((object_ptr)&_res_3806, _res_3806, _1951);
        DeRefDS(_1951);
        _1951 = NOVALUE;
        goto L5; // [655] 896
L1E: 

        /** 			elsif ch = 'u' then*/
        if (_ch_3804 != 117)
        goto L1F; // [660] 751

        /** 				tmp = weeks_day(d)*/

        /** 	return remainder(julianDay(dt)-1+4094, 7) + 1*/
        Ref(_d_3800);
        _0 = _weeks_day_1__tmp_at665_3951;
        _weeks_day_1__tmp_at665_3951 = _10julianDay(_d_3800);
        DeRef(_0);
        DeRef(_weeks_day_2__tmp_at665_3952);
        if (IS_ATOM_INT(_weeks_day_1__tmp_at665_3951)) {
            _weeks_day_2__tmp_at665_3952 = _weeks_day_1__tmp_at665_3951 - 1;
            if ((long)((unsigned long)_weeks_day_2__tmp_at665_3952 +(unsigned long) HIGH_BITS) >= 0){
                _weeks_day_2__tmp_at665_3952 = NewDouble((double)_weeks_day_2__tmp_at665_3952);
            }
        }
        else {
            _weeks_day_2__tmp_at665_3952 = binary_op(MINUS, _weeks_day_1__tmp_at665_3951, 1);
        }
        DeRef(_weeks_day_3__tmp_at665_3953);
        if (IS_ATOM_INT(_weeks_day_2__tmp_at665_3952)) {
            _weeks_day_3__tmp_at665_3953 = _weeks_day_2__tmp_at665_3952 + 4094;
            if ((long)((unsigned long)_weeks_day_3__tmp_at665_3953 + (unsigned long)HIGH_BITS) >= 0) 
            _weeks_day_3__tmp_at665_3953 = NewDouble((double)_weeks_day_3__tmp_at665_3953);
        }
        else {
            _weeks_day_3__tmp_at665_3953 = binary_op(PLUS, _weeks_day_2__tmp_at665_3952, 4094);
        }
        DeRef(_weeks_day_4__tmp_at665_3954);
        if (IS_ATOM_INT(_weeks_day_3__tmp_at665_3953)) {
            _weeks_day_4__tmp_at665_3954 = (_weeks_day_3__tmp_at665_3953 % 7);
        }
        else {
            _weeks_day_4__tmp_at665_3954 = binary_op(REMAINDER, _weeks_day_3__tmp_at665_3953, 7);
        }
        if (IS_ATOM_INT(_weeks_day_4__tmp_at665_3954)) {
            _tmp_3805 = _weeks_day_4__tmp_at665_3954 + 1;
        }
        else
        { // coercing _tmp_3805 to an integer 1
            _tmp_3805 = binary_op(PLUS, 1, _weeks_day_4__tmp_at665_3954);
            if( !IS_ATOM_INT(_tmp_3805) ){
                _tmp_3805 = (object)DBL_PTR(_tmp_3805)->dbl;
            }
        }
        DeRef(_weeks_day_1__tmp_at665_3951);
        _weeks_day_1__tmp_at665_3951 = NOVALUE;
        DeRef(_weeks_day_2__tmp_at665_3952);
        _weeks_day_2__tmp_at665_3952 = NOVALUE;
        DeRef(_weeks_day_3__tmp_at665_3953);
        _weeks_day_3__tmp_at665_3953 = NOVALUE;
        DeRef(_weeks_day_4__tmp_at665_3954);
        _weeks_day_4__tmp_at665_3954 = NOVALUE;
        if (!IS_ATOM_INT(_tmp_3805)) {
            _1 = (long)(DBL_PTR(_tmp_3805)->dbl);
            if (UNIQUE(DBL_PTR(_tmp_3805)) && (DBL_PTR(_tmp_3805)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_tmp_3805);
            _tmp_3805 = _1;
        }

        /** 				if tmp = 1 then*/
        if (_tmp_3805 != 1)
        goto L20; // [695] 708

        /** 					res &= "7" -- Sunday*/
        Concat((object_ptr)&_res_3806, _res_3806, _1956);
        goto L5; // [705] 896
L20: 

        /** 					res &= sprintf("%d", weeks_day(d) - 1)*/

        /** 	return remainder(julianDay(dt)-1+4094, 7) + 1*/
        Ref(_d_3800);
        _0 = _weeks_day_1__tmp_at709_3962;
        _weeks_day_1__tmp_at709_3962 = _10julianDay(_d_3800);
        DeRef(_0);
        DeRef(_weeks_day_2__tmp_at709_3963);
        if (IS_ATOM_INT(_weeks_day_1__tmp_at709_3962)) {
            _weeks_day_2__tmp_at709_3963 = _weeks_day_1__tmp_at709_3962 - 1;
            if ((long)((unsigned long)_weeks_day_2__tmp_at709_3963 +(unsigned long) HIGH_BITS) >= 0){
                _weeks_day_2__tmp_at709_3963 = NewDouble((double)_weeks_day_2__tmp_at709_3963);
            }
        }
        else {
            _weeks_day_2__tmp_at709_3963 = binary_op(MINUS, _weeks_day_1__tmp_at709_3962, 1);
        }
        DeRef(_weeks_day_3__tmp_at709_3964);
        if (IS_ATOM_INT(_weeks_day_2__tmp_at709_3963)) {
            _weeks_day_3__tmp_at709_3964 = _weeks_day_2__tmp_at709_3963 + 4094;
            if ((long)((unsigned long)_weeks_day_3__tmp_at709_3964 + (unsigned long)HIGH_BITS) >= 0) 
            _weeks_day_3__tmp_at709_3964 = NewDouble((double)_weeks_day_3__tmp_at709_3964);
        }
        else {
            _weeks_day_3__tmp_at709_3964 = binary_op(PLUS, _weeks_day_2__tmp_at709_3963, 4094);
        }
        DeRef(_weeks_day_4__tmp_at709_3965);
        if (IS_ATOM_INT(_weeks_day_3__tmp_at709_3964)) {
            _weeks_day_4__tmp_at709_3965 = (_weeks_day_3__tmp_at709_3964 % 7);
        }
        else {
            _weeks_day_4__tmp_at709_3965 = binary_op(REMAINDER, _weeks_day_3__tmp_at709_3964, 7);
        }
        DeRef(_weeks_day_inlined_weeks_day_at_709_3961);
        if (IS_ATOM_INT(_weeks_day_4__tmp_at709_3965)) {
            _weeks_day_inlined_weeks_day_at_709_3961 = _weeks_day_4__tmp_at709_3965 + 1;
            if (_weeks_day_inlined_weeks_day_at_709_3961 > MAXINT){
                _weeks_day_inlined_weeks_day_at_709_3961 = NewDouble((double)_weeks_day_inlined_weeks_day_at_709_3961);
            }
        }
        else
        _weeks_day_inlined_weeks_day_at_709_3961 = binary_op(PLUS, 1, _weeks_day_4__tmp_at709_3965);
        DeRef(_weeks_day_1__tmp_at709_3962);
        _weeks_day_1__tmp_at709_3962 = NOVALUE;
        DeRef(_weeks_day_2__tmp_at709_3963);
        _weeks_day_2__tmp_at709_3963 = NOVALUE;
        DeRef(_weeks_day_3__tmp_at709_3964);
        _weeks_day_3__tmp_at709_3964 = NOVALUE;
        DeRef(_weeks_day_4__tmp_at709_3965);
        _weeks_day_4__tmp_at709_3965 = NOVALUE;
        if (IS_ATOM_INT(_weeks_day_inlined_weeks_day_at_709_3961)) {
            _1958 = _weeks_day_inlined_weeks_day_at_709_3961 - 1;
            if ((long)((unsigned long)_1958 +(unsigned long) HIGH_BITS) >= 0){
                _1958 = NewDouble((double)_1958);
            }
        }
        else {
            _1958 = binary_op(MINUS, _weeks_day_inlined_weeks_day_at_709_3961, 1);
        }
        _1959 = EPrintf(-9999999, _740, _1958);
        DeRef(_1958);
        _1958 = NOVALUE;
        Concat((object_ptr)&_res_3806, _res_3806, _1959);
        DeRefDS(_1959);
        _1959 = NOVALUE;
        goto L5; // [748] 896
L1F: 

        /** 			elsif ch = 'w' then*/
        if (_ch_3804 != 119)
        goto L21; // [753] 799

        /** 				res &= sprintf("%d", weeks_day(d) - 1)*/

        /** 	return remainder(julianDay(dt)-1+4094, 7) + 1*/
        Ref(_d_3800);
        _0 = _weeks_day_1__tmp_at758_3974;
        _weeks_day_1__tmp_at758_3974 = _10julianDay(_d_3800);
        DeRef(_0);
        DeRef(_weeks_day_2__tmp_at758_3975);
        if (IS_ATOM_INT(_weeks_day_1__tmp_at758_3974)) {
            _weeks_day_2__tmp_at758_3975 = _weeks_day_1__tmp_at758_3974 - 1;
            if ((long)((unsigned long)_weeks_day_2__tmp_at758_3975 +(unsigned long) HIGH_BITS) >= 0){
                _weeks_day_2__tmp_at758_3975 = NewDouble((double)_weeks_day_2__tmp_at758_3975);
            }
        }
        else {
            _weeks_day_2__tmp_at758_3975 = binary_op(MINUS, _weeks_day_1__tmp_at758_3974, 1);
        }
        DeRef(_weeks_day_3__tmp_at758_3976);
        if (IS_ATOM_INT(_weeks_day_2__tmp_at758_3975)) {
            _weeks_day_3__tmp_at758_3976 = _weeks_day_2__tmp_at758_3975 + 4094;
            if ((long)((unsigned long)_weeks_day_3__tmp_at758_3976 + (unsigned long)HIGH_BITS) >= 0) 
            _weeks_day_3__tmp_at758_3976 = NewDouble((double)_weeks_day_3__tmp_at758_3976);
        }
        else {
            _weeks_day_3__tmp_at758_3976 = binary_op(PLUS, _weeks_day_2__tmp_at758_3975, 4094);
        }
        DeRef(_weeks_day_4__tmp_at758_3977);
        if (IS_ATOM_INT(_weeks_day_3__tmp_at758_3976)) {
            _weeks_day_4__tmp_at758_3977 = (_weeks_day_3__tmp_at758_3976 % 7);
        }
        else {
            _weeks_day_4__tmp_at758_3977 = binary_op(REMAINDER, _weeks_day_3__tmp_at758_3976, 7);
        }
        DeRef(_weeks_day_inlined_weeks_day_at_758_3973);
        if (IS_ATOM_INT(_weeks_day_4__tmp_at758_3977)) {
            _weeks_day_inlined_weeks_day_at_758_3973 = _weeks_day_4__tmp_at758_3977 + 1;
            if (_weeks_day_inlined_weeks_day_at_758_3973 > MAXINT){
                _weeks_day_inlined_weeks_day_at_758_3973 = NewDouble((double)_weeks_day_inlined_weeks_day_at_758_3973);
            }
        }
        else
        _weeks_day_inlined_weeks_day_at_758_3973 = binary_op(PLUS, 1, _weeks_day_4__tmp_at758_3977);
        DeRef(_weeks_day_1__tmp_at758_3974);
        _weeks_day_1__tmp_at758_3974 = NOVALUE;
        DeRef(_weeks_day_2__tmp_at758_3975);
        _weeks_day_2__tmp_at758_3975 = NOVALUE;
        DeRef(_weeks_day_3__tmp_at758_3976);
        _weeks_day_3__tmp_at758_3976 = NOVALUE;
        DeRef(_weeks_day_4__tmp_at758_3977);
        _weeks_day_4__tmp_at758_3977 = NOVALUE;
        if (IS_ATOM_INT(_weeks_day_inlined_weeks_day_at_758_3973)) {
            _1963 = _weeks_day_inlined_weeks_day_at_758_3973 - 1;
            if ((long)((unsigned long)_1963 +(unsigned long) HIGH_BITS) >= 0){
                _1963 = NewDouble((double)_1963);
            }
        }
        else {
            _1963 = binary_op(MINUS, _weeks_day_inlined_weeks_day_at_758_3973, 1);
        }
        _1964 = EPrintf(-9999999, _740, _1963);
        DeRef(_1963);
        _1963 = NOVALUE;
        Concat((object_ptr)&_res_3806, _res_3806, _1964);
        DeRefDS(_1964);
        _1964 = NOVALUE;
        goto L5; // [796] 896
L21: 

        /** 			elsif ch = 'y' then*/
        if (_ch_3804 != 121)
        goto L22; // [801] 846

        /** 			   tmp = floor(d[YEAR] / 100)*/
        _2 = (int)SEQ_PTR(_d_3800);
        _1968 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_1968)) {
            if (100 > 0 && _1968 >= 0) {
                _tmp_3805 = _1968 / 100;
            }
            else {
                temp_dbl = floor((double)_1968 / (double)100);
                _tmp_3805 = (long)temp_dbl;
            }
        }
        else {
            _2 = binary_op(DIVIDE, _1968, 100);
            _tmp_3805 = unary_op(FLOOR, _2);
            DeRef(_2);
        }
        _1968 = NOVALUE;
        if (!IS_ATOM_INT(_tmp_3805)) {
            _1 = (long)(DBL_PTR(_tmp_3805)->dbl);
            if (UNIQUE(DBL_PTR(_tmp_3805)) && (DBL_PTR(_tmp_3805)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_tmp_3805);
            _tmp_3805 = _1;
        }

        /** 			   res &= sprintf("%02d", d[YEAR] - (tmp * 100))*/
        _2 = (int)SEQ_PTR(_d_3800);
        _1970 = (int)*(((s1_ptr)_2)->base + 1);
        if (_tmp_3805 == (short)_tmp_3805)
        _1971 = _tmp_3805 * 100;
        else
        _1971 = NewDouble(_tmp_3805 * (double)100);
        if (IS_ATOM_INT(_1970) && IS_ATOM_INT(_1971)) {
            _1972 = _1970 - _1971;
            if ((long)((unsigned long)_1972 +(unsigned long) HIGH_BITS) >= 0){
                _1972 = NewDouble((double)_1972);
            }
        }
        else {
            _1972 = binary_op(MINUS, _1970, _1971);
        }
        _1970 = NOVALUE;
        DeRef(_1971);
        _1971 = NOVALUE;
        _1973 = EPrintf(-9999999, _1877, _1972);
        DeRef(_1972);
        _1972 = NOVALUE;
        Concat((object_ptr)&_res_3806, _res_3806, _1973);
        DeRefDS(_1973);
        _1973 = NOVALUE;
        goto L5; // [843] 896
L22: 

        /** 			elsif ch = 'Y' then*/
        if (_ch_3804 != 89)
        goto L5; // [848] 896

        /** 				res &= sprintf("%04d", d[YEAR])*/
        _2 = (int)SEQ_PTR(_d_3800);
        _1978 = (int)*(((s1_ptr)_2)->base + 1);
        _1979 = EPrintf(-9999999, _1977, _1978);
        _1978 = NOVALUE;
        Concat((object_ptr)&_res_3806, _res_3806, _1979);
        DeRefDS(_1979);
        _1979 = NOVALUE;
        goto L5; // [868] 896
        goto L5; // [872] 896
L3: 

        /** 		elsif ch = '%' then*/
        if (_ch_3804 != 37)
        goto L23; // [877] 889

        /** 			in_fmt = 1*/
        _in_fmt_3803 = 1;
        goto L5; // [886] 896
L23: 

        /** 			res &= ch*/
        Append(&_res_3806, _res_3806, _ch_3804);
L5: 

        /** 	end for*/
        _i_3808 = _i_3808 + 1;
        goto L1; // [898] 27
L2: 
        ;
    }

    /** 	return res*/
    DeRef(_d_3800);
    DeRefDSi(_pattern_3801);
    _1869 = NOVALUE;
    _1873 = NOVALUE;
    return _res_3806;
    ;
}



// 0x66CCE283
