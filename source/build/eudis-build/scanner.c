// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _60set_qualified_fwd(int _fwd_24705)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_fwd_24705)) {
        _1 = (long)(DBL_PTR(_fwd_24705)->dbl);
        if (UNIQUE(DBL_PTR(_fwd_24705)) && (DBL_PTR(_fwd_24705)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fwd_24705);
        _fwd_24705 = _1;
    }

    /** 	qualified_fwd = fwd*/
    _60qualified_fwd_24702 = _fwd_24705;

    /** end procedure*/
    return;
    ;
}


int _60get_qualified_fwd()
{
    int _fwd_24708 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer fwd = qualified_fwd*/
    _fwd_24708 = _60qualified_fwd_24702;

    /** 	set_qualified_fwd( -1 )*/

    /** 	qualified_fwd = fwd*/
    _60qualified_fwd_24702 = -1;

    /** end procedure*/
    goto L1; // [17] 20
L1: 

    /** 	return fwd*/
    return _fwd_24708;
    ;
}


void _60InitLex()
{
    int _14324 = NOVALUE;
    int _14323 = NOVALUE;
    int _14322 = NOVALUE;
    int _14320 = NOVALUE;
    int _14319 = NOVALUE;
    int _14318 = NOVALUE;
    int _14317 = NOVALUE;
    int _0, _1, _2;
    

    /** 	gline_number = 0*/
    _25gline_number_12267 = 0;

    /** 	line_number = 0*/
    _25line_number_12263 = 0;

    /** 	IncludeStk = {}*/
    RefDS(_5);
    DeRef(_60IncludeStk_24679);
    _60IncludeStk_24679 = _5;

    /** 	char_class = repeat(ILLEGAL_CHAR, 255)  -- we screen out the 0 character*/
    DeRefi(_60char_class_24677);
    _60char_class_24677 = Repeat(-20, 255);

    /** 	char_class['0'..'9'] = DIGIT*/
    assign_slice_seq = (s1_ptr *)&_60char_class_24677;
    AssignSlice(48, 57, -7);

    /** 	char_class['_']      = DIGIT*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 95);
    *(int *)_2 = -7;

    /** 	char_class['a'..'z'] = LETTER*/
    assign_slice_seq = (s1_ptr *)&_60char_class_24677;
    AssignSlice(97, 122, -2);

    /** 	char_class['A'..'Z'] = LETTER*/
    assign_slice_seq = (s1_ptr *)&_60char_class_24677;
    AssignSlice(65, 90, -2);

    /** 	char_class[KEYWORD_BASE+1..KEYWORD_BASE+NUM_KEYWORDS] = KEYWORD*/
    _14317 = 129;
    _14318 = 152;
    assign_slice_seq = (s1_ptr *)&_60char_class_24677;
    AssignSlice(129, 152, -10);
    _14317 = NOVALUE;
    _14318 = NOVALUE;

    /** 	char_class[BUILTIN_BASE+1..BUILTIN_BASE+NUM_BUILTINS] = BUILTIN*/
    _14319 = 171;
    _14320 = 234;
    assign_slice_seq = (s1_ptr *)&_60char_class_24677;
    AssignSlice(171, 234, -9);
    _14319 = NOVALUE;
    _14320 = NOVALUE;

    /** 	char_class[' '] = BLANK*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 32);
    *(int *)_2 = -8;

    /** 	char_class['\t'] = BLANK*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 9);
    *(int *)_2 = -8;

    /** 	char_class['+'] = PLUS*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 43);
    *(int *)_2 = 11;

    /** 	char_class['-'] = MINUS*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 45);
    *(int *)_2 = 10;

    /** 	char_class['*'] = res:MULTIPLY*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 42);
    *(int *)_2 = 13;

    /** 	char_class['/'] = res:DIVIDE*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 47);
    *(int *)_2 = 14;

    /** 	char_class['='] = EQUALS*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 61);
    *(int *)_2 = 3;

    /** 	char_class['<'] = LESS*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 60);
    *(int *)_2 = 1;

    /** 	char_class['>'] = GREATER*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 62);
    *(int *)_2 = 6;

    /** 	char_class['\''] = SINGLE_QUOTE*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 39);
    *(int *)_2 = -5;

    /** 	char_class['"'] = DOUBLE_QUOTE*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 34);
    *(int *)_2 = -4;

    /** 	char_class['`'] = BACK_QUOTE*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 96);
    *(int *)_2 = -12;

    /** 	char_class['.'] = DOT*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 46);
    *(int *)_2 = -3;

    /** 	char_class[':'] = COLON*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 58);
    *(int *)_2 = -23;

    /** 	char_class['\r'] = NEWLINE*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 13);
    *(int *)_2 = -6;

    /** 	char_class['\n'] = NEWLINE*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 10);
    *(int *)_2 = -6;

    /** 	char_class['!'] = BANG*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 33);
    *(int *)_2 = -1;

    /** 	char_class['{'] = LEFT_BRACE*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 123);
    *(int *)_2 = -24;

    /** 	char_class['}'] = RIGHT_BRACE*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 125);
    *(int *)_2 = -25;

    /** 	char_class['('] = LEFT_ROUND*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 40);
    *(int *)_2 = -26;

    /** 	char_class[')'] = RIGHT_ROUND*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 41);
    *(int *)_2 = -27;

    /** 	char_class['['] = LEFT_SQUARE*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 91);
    *(int *)_2 = -28;

    /** 	char_class[']'] = RIGHT_SQUARE*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 93);
    *(int *)_2 = -29;

    /** 	char_class['$'] = DOLLAR*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 36);
    *(int *)_2 = -22;

    /** 	char_class[','] = COMMA*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 44);
    *(int *)_2 = -30;

    /** 	char_class['&'] = res:CONCAT*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 38);
    *(int *)_2 = 15;

    /** 	char_class['?'] = QUESTION_MARK*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 63);
    *(int *)_2 = -31;

    /** 	char_class['#'] = NUMBER_SIGN*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 35);
    *(int *)_2 = -11;

    /** 	char_class[END_OF_FILE_CHAR] = END_OF_FILE*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _2 = (int)(((s1_ptr)_2)->base + 26);
    *(int *)_2 = -21;

    /** 	id_char = repeat(FALSE, 255)*/
    DeRefi(_60id_char_24678);
    _60id_char_24678 = Repeat(_5FALSE_242, 255);

    /** 	for i = 1 to 255 do*/
    {
        int _i_24756;
        _i_24756 = 1;
L1: 
        if (_i_24756 > 255){
            goto L2; // [407] 456
        }

        /** 		if find(char_class[i], {LETTER, DIGIT}) then*/
        _2 = (int)SEQ_PTR(_60char_class_24677);
        _14322 = (int)*(((s1_ptr)_2)->base + _i_24756);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = -2;
        ((int *)_2)[2] = -7;
        _14323 = MAKE_SEQ(_1);
        _14324 = find_from(_14322, _14323, 1);
        _14322 = NOVALUE;
        DeRefDS(_14323);
        _14323 = NOVALUE;
        if (_14324 == 0)
        {
            _14324 = NOVALUE;
            goto L3; // [435] 449
        }
        else{
            _14324 = NOVALUE;
        }

        /** 			id_char[i] = TRUE*/
        _2 = (int)SEQ_PTR(_60id_char_24678);
        _2 = (int)(((s1_ptr)_2)->base + _i_24756);
        *(int *)_2 = _5TRUE_244;
L3: 

        /** 	end for*/
        _i_24756 = _i_24756 + 1;
        goto L1; // [451] 414
L2: 
        ;
    }

    /** 	default_namespaces = {0}*/
    _0 = _60default_namespaces_24676;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    _60default_namespaces_24676 = MAKE_SEQ(_1);
    DeRef(_0);

    /** end procedure*/
    return;
    ;
}


void _60ResetTP()
{
    int _0, _1, _2;
    

    /** 	OpTrace = FALSE*/
    _25OpTrace_12332 = _5FALSE_242;

    /** 	OpProfileStatement = FALSE*/
    _25OpProfileStatement_12334 = _5FALSE_242;

    /** 	OpProfileTime = FALSE*/
    _25OpProfileTime_12335 = _5FALSE_242;

    /** 	AnyStatementProfile = FALSE*/
    _26AnyStatementProfile_11163 = _5FALSE_242;

    /** 	AnyTimeProfile = FALSE*/
    _26AnyTimeProfile_11162 = _5FALSE_242;

    /** end procedure*/
    return;
    ;
}


int _60pack_source(int _src_24786)
{
    int _start_24787 = NOVALUE;
    int _14348 = NOVALUE;
    int _14347 = NOVALUE;
    int _14346 = NOVALUE;
    int _14345 = NOVALUE;
    int _14343 = NOVALUE;
    int _14341 = NOVALUE;
    int _14340 = NOVALUE;
    int _14339 = NOVALUE;
    int _14335 = NOVALUE;
    int _14333 = NOVALUE;
    int _14332 = NOVALUE;
    int _14329 = NOVALUE;
    int _14328 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if equal(src, 0) then*/
    if (_src_24786 == 0)
    _14328 = 1;
    else if (IS_ATOM_INT(_src_24786) && IS_ATOM_INT(0))
    _14328 = 0;
    else
    _14328 = (compare(_src_24786, 0) == 0);
    if (_14328 == 0)
    {
        _14328 = NOVALUE;
        goto L1; // [7] 17
    }
    else{
        _14328 = NOVALUE;
    }

    /** 		return 0*/
    DeRef(_src_24786);
    return 0;
L1: 

    /** 	if length(src) >= SOURCE_CHUNK then*/
    if (IS_SEQUENCE(_src_24786)){
            _14329 = SEQ_PTR(_src_24786)->length;
    }
    else {
        _14329 = 1;
    }
    if (_14329 < 10000)
    goto L2; // [22] 34

    /** 		src = src[1..100] -- enough for trace or profile display*/
    rhs_slice_target = (object_ptr)&_src_24786;
    RHS_Slice(_src_24786, 1, 100);
L2: 

    /** 	if current_source_next + length(src) >= SOURCE_CHUNK then*/
    if (IS_SEQUENCE(_src_24786)){
            _14332 = SEQ_PTR(_src_24786)->length;
    }
    else {
        _14332 = 1;
    }
    _14333 = _60current_source_next_24782 + _14332;
    if ((long)((unsigned long)_14333 + (unsigned long)HIGH_BITS) >= 0) 
    _14333 = NewDouble((double)_14333);
    _14332 = NOVALUE;
    if (binary_op_a(LESS, _14333, 10000)){
        DeRef(_14333);
        _14333 = NOVALUE;
        goto L3; // [45] 94
    }
    DeRef(_14333);
    _14333 = NOVALUE;

    /** 		current_source = allocate(SOURCE_CHUNK + LINE_BUFLEN)*/
    _14335 = 10400;
    _0 = _12allocate(10400, 0);
    DeRef(_60current_source_24781);
    _60current_source_24781 = _0;
    _14335 = NOVALUE;

    /** 		if current_source = 0 then*/
    if (binary_op_a(NOTEQ, _60current_source_24781, 0)){
        goto L4; // [64] 76
    }

    /** 			CompileErr(123)*/
    RefDS(_22682);
    _43CompileErr(123, _22682, 0);
L4: 

    /** 		all_source = append(all_source, current_source)*/
    Ref(_60current_source_24781);
    Append(&_26all_source_11164, _26all_source_11164, _60current_source_24781);

    /** 		current_source_next = 1*/
    _60current_source_next_24782 = 1;
L3: 

    /** 	start = current_source_next*/
    _start_24787 = _60current_source_next_24782;

    /** 	poke(current_source+current_source_next, src)*/
    if (IS_ATOM_INT(_60current_source_24781)) {
        _14339 = _60current_source_24781 + _60current_source_next_24782;
        if ((long)((unsigned long)_14339 + (unsigned long)HIGH_BITS) >= 0) 
        _14339 = NewDouble((double)_14339);
    }
    else {
        _14339 = NewDouble(DBL_PTR(_60current_source_24781)->dbl + (double)_60current_source_next_24782);
    }
    if (IS_ATOM_INT(_14339)){
        poke_addr = (unsigned char *)_14339;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_14339)->dbl);
    }
    if (IS_ATOM_INT(_src_24786)) {
        *poke_addr = (unsigned char)_src_24786;
    }
    else if (IS_ATOM(_src_24786)) {
        _1 = (signed char)DBL_PTR(_src_24786)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_src_24786);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }
    DeRef(_14339);
    _14339 = NOVALUE;

    /** 	current_source_next += length(src)-1*/
    if (IS_SEQUENCE(_src_24786)){
            _14340 = SEQ_PTR(_src_24786)->length;
    }
    else {
        _14340 = 1;
    }
    _14341 = _14340 - 1;
    _14340 = NOVALUE;
    _60current_source_next_24782 = _60current_source_next_24782 + _14341;
    _14341 = NOVALUE;

    /** 	poke(current_source+current_source_next, 0) -- overwrite \n*/
    if (IS_ATOM_INT(_60current_source_24781)) {
        _14343 = _60current_source_24781 + _60current_source_next_24782;
        if ((long)((unsigned long)_14343 + (unsigned long)HIGH_BITS) >= 0) 
        _14343 = NewDouble((double)_14343);
    }
    else {
        _14343 = NewDouble(DBL_PTR(_60current_source_24781)->dbl + (double)_60current_source_next_24782);
    }
    if (IS_ATOM_INT(_14343)){
        poke_addr = (unsigned char *)_14343;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_14343)->dbl);
    }
    *poke_addr = (unsigned char)0;
    DeRef(_14343);
    _14343 = NOVALUE;

    /** 	current_source_next += 1*/
    _60current_source_next_24782 = _60current_source_next_24782 + 1;

    /** 	return start + SOURCE_CHUNK * (length(all_source)-1)*/
    if (IS_SEQUENCE(_26all_source_11164)){
            _14345 = SEQ_PTR(_26all_source_11164)->length;
    }
    else {
        _14345 = 1;
    }
    _14346 = _14345 - 1;
    _14345 = NOVALUE;
    if (_14346 <= INT15)
    _14347 = 10000 * _14346;
    else
    _14347 = NewDouble(10000 * (double)_14346);
    _14346 = NOVALUE;
    if (IS_ATOM_INT(_14347)) {
        _14348 = _start_24787 + _14347;
        if ((long)((unsigned long)_14348 + (unsigned long)HIGH_BITS) >= 0) 
        _14348 = NewDouble((double)_14348);
    }
    else {
        _14348 = NewDouble((double)_start_24787 + DBL_PTR(_14347)->dbl);
    }
    DeRef(_14347);
    _14347 = NOVALUE;
    DeRef(_src_24786);
    return _14348;
    ;
}


int _60fetch_line(int _start_24820)
{
    int _line_24821 = NOVALUE;
    int _memdata_24822 = NOVALUE;
    int _c_24823 = NOVALUE;
    int _chunk_24824 = NOVALUE;
    int _p_24825 = NOVALUE;
    int _n_24826 = NOVALUE;
    int _m_24827 = NOVALUE;
    int _14373 = NOVALUE;
    int _14372 = NOVALUE;
    int _14370 = NOVALUE;
    int _14368 = NOVALUE;
    int _14362 = NOVALUE;
    int _14360 = NOVALUE;
    int _14356 = NOVALUE;
    int _14354 = NOVALUE;
    int _14351 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_start_24820)) {
        _1 = (long)(DBL_PTR(_start_24820)->dbl);
        if (UNIQUE(DBL_PTR(_start_24820)) && (DBL_PTR(_start_24820)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_24820);
        _start_24820 = _1;
    }

    /** 	if start = 0 then*/
    if (_start_24820 != 0)
    goto L1; // [5] 16

    /** 		return ""*/
    RefDS(_5);
    DeRef(_line_24821);
    DeRefi(_memdata_24822);
    DeRef(_p_24825);
    return _5;
L1: 

    /** 	line = repeat(0, LINE_BUFLEN)*/
    DeRef(_line_24821);
    _line_24821 = Repeat(0, 400);

    /** 	n = 0*/
    _n_24826 = 0;

    /** 	chunk = 1+floor(start / SOURCE_CHUNK)*/
    if (10000 > 0 && _start_24820 >= 0) {
        _14351 = _start_24820 / 10000;
    }
    else {
        temp_dbl = floor((double)_start_24820 / (double)10000);
        _14351 = (long)temp_dbl;
    }
    _chunk_24824 = _14351 + 1;
    _14351 = NOVALUE;

    /** 	start = remainder(start, SOURCE_CHUNK)*/
    _start_24820 = (_start_24820 % 10000);

    /** 	p = all_source[chunk] + start*/
    _2 = (int)SEQ_PTR(_26all_source_11164);
    _14354 = (int)*(((s1_ptr)_2)->base + _chunk_24824);
    DeRef(_p_24825);
    if (IS_ATOM_INT(_14354)) {
        _p_24825 = _14354 + _start_24820;
        if ((long)((unsigned long)_p_24825 + (unsigned long)HIGH_BITS) >= 0) 
        _p_24825 = NewDouble((double)_p_24825);
    }
    else {
        _p_24825 = NewDouble(DBL_PTR(_14354)->dbl + (double)_start_24820);
    }
    _14354 = NOVALUE;

    /** 	memdata = peek({p, LINE_BUFLEN})*/
    Ref(_p_24825);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _p_24825;
    ((int *)_2)[2] = 400;
    _14356 = MAKE_SEQ(_1);
    DeRefi(_memdata_24822);
    _1 = (int)SEQ_PTR(_14356);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _memdata_24822 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_14356);
    _14356 = NOVALUE;

    /** 	p += LINE_BUFLEN*/
    _0 = _p_24825;
    if (IS_ATOM_INT(_p_24825)) {
        _p_24825 = _p_24825 + 400;
        if ((long)((unsigned long)_p_24825 + (unsigned long)HIGH_BITS) >= 0) 
        _p_24825 = NewDouble((double)_p_24825);
    }
    else {
        _p_24825 = NewDouble(DBL_PTR(_p_24825)->dbl + (double)400);
    }
    DeRef(_0);

    /** 	m = 0*/
    _m_24827 = 0;

    /** 	while TRUE do*/
L2: 
    if (_5TRUE_244 == 0)
    {
        goto L3; // [84] 179
    }
    else{
    }

    /** 		m += 1*/
    _m_24827 = _m_24827 + 1;

    /** 		if m > length(memdata) then*/
    if (IS_SEQUENCE(_memdata_24822)){
            _14360 = SEQ_PTR(_memdata_24822)->length;
    }
    else {
        _14360 = 1;
    }
    if (_m_24827 <= _14360)
    goto L4; // [98] 125

    /** 			memdata = peek({p, LINE_BUFLEN})*/
    Ref(_p_24825);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _p_24825;
    ((int *)_2)[2] = 400;
    _14362 = MAKE_SEQ(_1);
    DeRefDSi(_memdata_24822);
    _1 = (int)SEQ_PTR(_14362);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _memdata_24822 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_14362);
    _14362 = NOVALUE;

    /** 			p += LINE_BUFLEN*/
    _0 = _p_24825;
    if (IS_ATOM_INT(_p_24825)) {
        _p_24825 = _p_24825 + 400;
        if ((long)((unsigned long)_p_24825 + (unsigned long)HIGH_BITS) >= 0) 
        _p_24825 = NewDouble((double)_p_24825);
    }
    else {
        _p_24825 = NewDouble(DBL_PTR(_p_24825)->dbl + (double)400);
    }
    DeRef(_0);

    /** 			m = 1*/
    _m_24827 = 1;
L4: 

    /** 		c = memdata[m]*/
    _2 = (int)SEQ_PTR(_memdata_24822);
    _c_24823 = (int)*(((s1_ptr)_2)->base + _m_24827);

    /** 		if c = 0 then*/
    if (_c_24823 != 0)
    goto L5; // [133] 142

    /** 			exit*/
    goto L3; // [139] 179
L5: 

    /** 		n += 1*/
    _n_24826 = _n_24826 + 1;

    /** 		if n > length(line) then*/
    if (IS_SEQUENCE(_line_24821)){
            _14368 = SEQ_PTR(_line_24821)->length;
    }
    else {
        _14368 = 1;
    }
    if (_n_24826 <= _14368)
    goto L6; // [153] 168

    /** 			line &= repeat(0, LINE_BUFLEN)*/
    _14370 = Repeat(0, 400);
    Concat((object_ptr)&_line_24821, _line_24821, _14370);
    DeRefDS(_14370);
    _14370 = NOVALUE;
L6: 

    /** 		line[n] = c*/
    _2 = (int)SEQ_PTR(_line_24821);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _line_24821 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _n_24826);
    _1 = *(int *)_2;
    *(int *)_2 = _c_24823;
    DeRef(_1);

    /** 	end while*/
    goto L2; // [176] 82
L3: 

    /** 	line = remove( line, n+1, length( line ) )*/
    _14372 = _n_24826 + 1;
    if (_14372 > MAXINT){
        _14372 = NewDouble((double)_14372);
    }
    if (IS_SEQUENCE(_line_24821)){
            _14373 = SEQ_PTR(_line_24821)->length;
    }
    else {
        _14373 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_line_24821);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_14372)) ? _14372 : (long)(DBL_PTR(_14372)->dbl);
        int stop = (IS_ATOM_INT(_14373)) ? _14373 : (long)(DBL_PTR(_14373)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_line_24821), start, &_line_24821 );
            }
            else Tail(SEQ_PTR(_line_24821), stop+1, &_line_24821);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_line_24821), start, &_line_24821);
        }
        else {
            assign_slice_seq = &assign_space;
            _line_24821 = Remove_elements(start, stop, (SEQ_PTR(_line_24821)->ref == 1));
        }
    }
    DeRef(_14372);
    _14372 = NOVALUE;
    _14373 = NOVALUE;

    /** 	return line*/
    DeRefi(_memdata_24822);
    DeRef(_p_24825);
    return _line_24821;
    ;
}


void _60AppendSourceLine()
{
    int _new_24863 = NOVALUE;
    int _old_24864 = NOVALUE;
    int _options_24865 = NOVALUE;
    int _src_24866 = NOVALUE;
    int _14414 = NOVALUE;
    int _14410 = NOVALUE;
    int _14408 = NOVALUE;
    int _14407 = NOVALUE;
    int _14404 = NOVALUE;
    int _14403 = NOVALUE;
    int _14402 = NOVALUE;
    int _14401 = NOVALUE;
    int _14400 = NOVALUE;
    int _14399 = NOVALUE;
    int _14398 = NOVALUE;
    int _14397 = NOVALUE;
    int _14396 = NOVALUE;
    int _14395 = NOVALUE;
    int _14394 = NOVALUE;
    int _14393 = NOVALUE;
    int _14392 = NOVALUE;
    int _14391 = NOVALUE;
    int _14390 = NOVALUE;
    int _14389 = NOVALUE;
    int _14388 = NOVALUE;
    int _14387 = NOVALUE;
    int _14385 = NOVALUE;
    int _14384 = NOVALUE;
    int _14383 = NOVALUE;
    int _14381 = NOVALUE;
    int _14376 = NOVALUE;
    int _14375 = NOVALUE;
    int _0, _1, _2;
    

    /** 	src = 0*/
    DeRef(_src_24866);
    _src_24866 = 0;

    /** 	options = 0*/
    _options_24865 = 0;

    /** 	if TRANSLATE or OpTrace or OpProfileStatement or OpProfileTime then*/
    if (_25TRANSLATE_11874 != 0) {
        _14375 = 1;
        goto L1; // [15] 25
    }
    _14375 = (_25OpTrace_12332 != 0);
L1: 
    if (_14375 != 0) {
        _14376 = 1;
        goto L2; // [25] 35
    }
    _14376 = (_25OpProfileStatement_12334 != 0);
L2: 
    if (_14376 != 0) {
        goto L3; // [35] 46
    }
    if (_25OpProfileTime_12335 == 0)
    {
        goto L4; // [42] 136
    }
    else{
    }
L3: 

    /** 		src = ThisLine*/
    Ref(_43ThisLine_49532);
    DeRef(_src_24866);
    _src_24866 = _43ThisLine_49532;

    /** 		if OpTrace then*/
    if (_25OpTrace_12332 == 0)
    {
        goto L5; // [57] 70
    }
    else{
    }

    /** 			options = SOP_TRACE*/
    _options_24865 = 1;
L5: 

    /** 		if OpProfileTime then*/
    if (_25OpProfileTime_12335 == 0)
    {
        goto L6; // [74] 88
    }
    else{
    }

    /** 			options = or_bits(options, SOP_PROFILE_TIME)*/
    {unsigned long tu;
         tu = (unsigned long)_options_24865 | (unsigned long)2;
         _options_24865 = MAKE_UINT(tu);
    }
    if (!IS_ATOM_INT(_options_24865)) {
        _1 = (long)(DBL_PTR(_options_24865)->dbl);
        if (UNIQUE(DBL_PTR(_options_24865)) && (DBL_PTR(_options_24865)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_options_24865);
        _options_24865 = _1;
    }
L6: 

    /** 		if OpProfileStatement then*/
    if (_25OpProfileStatement_12334 == 0)
    {
        goto L7; // [92] 106
    }
    else{
    }

    /** 			options = or_bits(options, SOP_PROFILE_STATEMENT)*/
    {unsigned long tu;
         tu = (unsigned long)_options_24865 | (unsigned long)4;
         _options_24865 = MAKE_UINT(tu);
    }
    if (!IS_ATOM_INT(_options_24865)) {
        _1 = (long)(DBL_PTR(_options_24865)->dbl);
        if (UNIQUE(DBL_PTR(_options_24865)) && (DBL_PTR(_options_24865)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_options_24865);
        _options_24865 = _1;
    }
L7: 

    /** 		if OpProfileStatement or OpProfileTime then*/
    if (_25OpProfileStatement_12334 != 0) {
        goto L8; // [110] 121
    }
    if (_25OpProfileTime_12335 == 0)
    {
        goto L9; // [117] 135
    }
    else{
    }
L8: 

    /** 			src = {0,0,0,0} & src*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 0;
    _14381 = MAKE_SEQ(_1);
    if (IS_SEQUENCE(_14381) && IS_ATOM(_src_24866)) {
        Ref(_src_24866);
        Append(&_src_24866, _14381, _src_24866);
    }
    else if (IS_ATOM(_14381) && IS_SEQUENCE(_src_24866)) {
    }
    else {
        Concat((object_ptr)&_src_24866, _14381, _src_24866);
        DeRefDS(_14381);
        _14381 = NOVALUE;
    }
    DeRef(_14381);
    _14381 = NOVALUE;
L9: 
L4: 

    /** 	if length(slist) then*/
    if (IS_SEQUENCE(_25slist_12357)){
            _14383 = SEQ_PTR(_25slist_12357)->length;
    }
    else {
        _14383 = 1;
    }
    if (_14383 == 0)
    {
        _14383 = NOVALUE;
        goto LA; // [143] 345
    }
    else{
        _14383 = NOVALUE;
    }

    /** 		old = slist[$-1]*/
    if (IS_SEQUENCE(_25slist_12357)){
            _14384 = SEQ_PTR(_25slist_12357)->length;
    }
    else {
        _14384 = 1;
    }
    _14385 = _14384 - 1;
    _14384 = NOVALUE;
    DeRef(_old_24864);
    _2 = (int)SEQ_PTR(_25slist_12357);
    _old_24864 = (int)*(((s1_ptr)_2)->base + _14385);
    Ref(_old_24864);

    /** 		if equal(src, old[SRC]) and*/
    _2 = (int)SEQ_PTR(_old_24864);
    _14387 = (int)*(((s1_ptr)_2)->base + 1);
    if (_src_24866 == _14387)
    _14388 = 1;
    else if (IS_ATOM_INT(_src_24866) && IS_ATOM_INT(_14387))
    _14388 = 0;
    else
    _14388 = (compare(_src_24866, _14387) == 0);
    _14387 = NOVALUE;
    if (_14388 == 0) {
        _14389 = 0;
        goto LB; // [175] 195
    }
    _2 = (int)SEQ_PTR(_old_24864);
    _14390 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_14390)) {
        _14391 = (_25current_file_no_12262 == _14390);
    }
    else {
        _14391 = binary_op(EQUALS, _25current_file_no_12262, _14390);
    }
    _14390 = NOVALUE;
    if (IS_ATOM_INT(_14391))
    _14389 = (_14391 != 0);
    else
    _14389 = DBL_PTR(_14391)->dbl != 0.0;
LB: 
    if (_14389 == 0) {
        _14392 = 0;
        goto LC; // [195] 232
    }
    _2 = (int)SEQ_PTR(_old_24864);
    _14393 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_14393)) {
        _14394 = _14393 + 1;
        if (_14394 > MAXINT){
            _14394 = NewDouble((double)_14394);
        }
    }
    else
    _14394 = binary_op(PLUS, 1, _14393);
    _14393 = NOVALUE;
    if (IS_SEQUENCE(_25slist_12357)){
            _14395 = SEQ_PTR(_25slist_12357)->length;
    }
    else {
        _14395 = 1;
    }
    _2 = (int)SEQ_PTR(_25slist_12357);
    _14396 = (int)*(((s1_ptr)_2)->base + _14395);
    if (IS_ATOM_INT(_14394) && IS_ATOM_INT(_14396)) {
        _14397 = _14394 + _14396;
        if ((long)((unsigned long)_14397 + (unsigned long)HIGH_BITS) >= 0) 
        _14397 = NewDouble((double)_14397);
    }
    else {
        _14397 = binary_op(PLUS, _14394, _14396);
    }
    DeRef(_14394);
    _14394 = NOVALUE;
    _14396 = NOVALUE;
    if (IS_ATOM_INT(_14397)) {
        _14398 = (_25line_number_12263 == _14397);
    }
    else {
        _14398 = binary_op(EQUALS, _25line_number_12263, _14397);
    }
    DeRef(_14397);
    _14397 = NOVALUE;
    if (IS_ATOM_INT(_14398))
    _14392 = (_14398 != 0);
    else
    _14392 = DBL_PTR(_14398)->dbl != 0.0;
LC: 
    if (_14392 == 0) {
        goto LD; // [232] 272
    }
    _2 = (int)SEQ_PTR(_old_24864);
    _14400 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_14400)) {
        _14401 = (_options_24865 == _14400);
    }
    else {
        _14401 = binary_op(EQUALS, _options_24865, _14400);
    }
    _14400 = NOVALUE;
    if (_14401 == 0) {
        DeRef(_14401);
        _14401 = NOVALUE;
        goto LD; // [247] 272
    }
    else {
        if (!IS_ATOM_INT(_14401) && DBL_PTR(_14401)->dbl == 0.0){
            DeRef(_14401);
            _14401 = NOVALUE;
            goto LD; // [247] 272
        }
        DeRef(_14401);
        _14401 = NOVALUE;
    }
    DeRef(_14401);
    _14401 = NOVALUE;

    /** 			slist[$] += 1*/
    if (IS_SEQUENCE(_25slist_12357)){
            _14402 = SEQ_PTR(_25slist_12357)->length;
    }
    else {
        _14402 = 1;
    }
    _2 = (int)SEQ_PTR(_25slist_12357);
    _14403 = (int)*(((s1_ptr)_2)->base + _14402);
    if (IS_ATOM_INT(_14403)) {
        _14404 = _14403 + 1;
        if (_14404 > MAXINT){
            _14404 = NewDouble((double)_14404);
        }
    }
    else
    _14404 = binary_op(PLUS, 1, _14403);
    _14403 = NOVALUE;
    _2 = (int)SEQ_PTR(_25slist_12357);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _25slist_12357 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _14402);
    _1 = *(int *)_2;
    *(int *)_2 = _14404;
    if( _1 != _14404 ){
        DeRef(_1);
    }
    _14404 = NOVALUE;
    goto LE; // [269] 371
LD: 

    /** 			src = pack_source(src)*/
    Ref(_src_24866);
    _0 = _src_24866;
    _src_24866 = _60pack_source(_src_24866);
    DeRef(_0);

    /** 			new = {src, line_number, current_file_no, options}*/
    _0 = _new_24863;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_src_24866);
    *((int *)(_2+4)) = _src_24866;
    *((int *)(_2+8)) = _25line_number_12263;
    *((int *)(_2+12)) = _25current_file_no_12262;
    *((int *)(_2+16)) = _options_24865;
    _new_24863 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 			if slist[$] = 0 then*/
    if (IS_SEQUENCE(_25slist_12357)){
            _14407 = SEQ_PTR(_25slist_12357)->length;
    }
    else {
        _14407 = 1;
    }
    _2 = (int)SEQ_PTR(_25slist_12357);
    _14408 = (int)*(((s1_ptr)_2)->base + _14407);
    if (binary_op_a(NOTEQ, _14408, 0)){
        _14408 = NOVALUE;
        goto LF; // [302] 320
    }
    _14408 = NOVALUE;

    /** 				slist[$] = new*/
    if (IS_SEQUENCE(_25slist_12357)){
            _14410 = SEQ_PTR(_25slist_12357)->length;
    }
    else {
        _14410 = 1;
    }
    RefDS(_new_24863);
    _2 = (int)SEQ_PTR(_25slist_12357);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _25slist_12357 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _14410);
    _1 = *(int *)_2;
    *(int *)_2 = _new_24863;
    DeRef(_1);
    goto L10; // [317] 331
LF: 

    /** 				slist = append(slist, new)*/
    RefDS(_new_24863);
    Append(&_25slist_12357, _25slist_12357, _new_24863);
L10: 

    /** 			slist = append(slist, 0)*/
    Append(&_25slist_12357, _25slist_12357, 0);
    goto LE; // [342] 371
LA: 

    /** 		src = pack_source(src)*/
    Ref(_src_24866);
    _0 = _src_24866;
    _src_24866 = _60pack_source(_src_24866);
    DeRef(_0);

    /** 		slist = {{src, line_number, current_file_no, options}, 0}*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_src_24866);
    *((int *)(_2+4)) = _src_24866;
    *((int *)(_2+8)) = _25line_number_12263;
    *((int *)(_2+12)) = _25current_file_no_12262;
    *((int *)(_2+16)) = _options_24865;
    _14414 = MAKE_SEQ(_1);
    DeRef(_25slist_12357);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _14414;
    ((int *)_2)[2] = 0;
    _25slist_12357 = MAKE_SEQ(_1);
    _14414 = NOVALUE;
LE: 

    /** end procedure*/
    DeRef(_new_24863);
    DeRef(_old_24864);
    DeRef(_src_24866);
    DeRef(_14385);
    _14385 = NOVALUE;
    DeRef(_14398);
    _14398 = NOVALUE;
    DeRef(_14391);
    _14391 = NOVALUE;
    return;
    ;
}


int _60s_expand(int _slist_24955)
{
    int _new_slist_24956 = NOVALUE;
    int _14428 = NOVALUE;
    int _14427 = NOVALUE;
    int _14426 = NOVALUE;
    int _14425 = NOVALUE;
    int _14423 = NOVALUE;
    int _14422 = NOVALUE;
    int _14421 = NOVALUE;
    int _14419 = NOVALUE;
    int _14418 = NOVALUE;
    int _14417 = NOVALUE;
    int _14416 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	new_slist = {}*/
    RefDS(_5);
    DeRef(_new_slist_24956);
    _new_slist_24956 = _5;

    /** 	for i = 1 to length(slist) do*/
    if (IS_SEQUENCE(_slist_24955)){
            _14416 = SEQ_PTR(_slist_24955)->length;
    }
    else {
        _14416 = 1;
    }
    {
        int _i_24958;
        _i_24958 = 1;
L1: 
        if (_i_24958 > _14416){
            goto L2; // [15] 114
        }

        /** 		if sequence(slist[i]) then*/
        _2 = (int)SEQ_PTR(_slist_24955);
        _14417 = (int)*(((s1_ptr)_2)->base + _i_24958);
        _14418 = IS_SEQUENCE(_14417);
        _14417 = NOVALUE;
        if (_14418 == 0)
        {
            _14418 = NOVALUE;
            goto L3; // [31] 47
        }
        else{
            _14418 = NOVALUE;
        }

        /** 			new_slist = append(new_slist, slist[i])*/
        _2 = (int)SEQ_PTR(_slist_24955);
        _14419 = (int)*(((s1_ptr)_2)->base + _i_24958);
        Ref(_14419);
        Append(&_new_slist_24956, _new_slist_24956, _14419);
        _14419 = NOVALUE;
        goto L4; // [44] 107
L3: 

        /** 			for j = 1 to slist[i] do*/
        _2 = (int)SEQ_PTR(_slist_24955);
        _14421 = (int)*(((s1_ptr)_2)->base + _i_24958);
        {
            int _j_24967;
            _j_24967 = 1;
L5: 
            if (binary_op_a(GREATER, _j_24967, _14421)){
                goto L6; // [53] 106
            }

            /** 				slist[i-1][LINE] += 1*/
            _14422 = _i_24958 - 1;
            _2 = (int)SEQ_PTR(_slist_24955);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _slist_24955 = MAKE_SEQ(_2);
            }
            _3 = (int)(_14422 + ((s1_ptr)_2)->base);
            _2 = (int)SEQ_PTR(*(int *)_3);
            _14425 = (int)*(((s1_ptr)_2)->base + 2);
            _14423 = NOVALUE;
            if (IS_ATOM_INT(_14425)) {
                _14426 = _14425 + 1;
                if (_14426 > MAXINT){
                    _14426 = NewDouble((double)_14426);
                }
            }
            else
            _14426 = binary_op(PLUS, 1, _14425);
            _14425 = NOVALUE;
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 2);
            _1 = *(int *)_2;
            *(int *)_2 = _14426;
            if( _1 != _14426 ){
                DeRef(_1);
            }
            _14426 = NOVALUE;
            _14423 = NOVALUE;

            /** 				new_slist = append(new_slist, slist[i-1])*/
            _14427 = _i_24958 - 1;
            _2 = (int)SEQ_PTR(_slist_24955);
            _14428 = (int)*(((s1_ptr)_2)->base + _14427);
            Ref(_14428);
            Append(&_new_slist_24956, _new_slist_24956, _14428);
            _14428 = NOVALUE;

            /** 			end for*/
            _0 = _j_24967;
            if (IS_ATOM_INT(_j_24967)) {
                _j_24967 = _j_24967 + 1;
                if ((long)((unsigned long)_j_24967 +(unsigned long) HIGH_BITS) >= 0){
                    _j_24967 = NewDouble((double)_j_24967);
                }
            }
            else {
                _j_24967 = binary_op_a(PLUS, _j_24967, 1);
            }
            DeRef(_0);
            goto L5; // [101] 60
L6: 
            ;
            DeRef(_j_24967);
        }
L4: 

        /** 	end for*/
        _i_24958 = _i_24958 + 1;
        goto L1; // [109] 22
L2: 
        ;
    }

    /** 	return new_slist*/
    DeRefDS(_slist_24955);
    _14421 = NOVALUE;
    DeRef(_14422);
    _14422 = NOVALUE;
    DeRef(_14427);
    _14427 = NOVALUE;
    return _new_slist_24956;
    ;
}


void _60set_dont_read(int _read_24982)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_read_24982)) {
        _1 = (long)(DBL_PTR(_read_24982)->dbl);
        if (UNIQUE(DBL_PTR(_read_24982)) && (DBL_PTR(_read_24982)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_read_24982);
        _read_24982 = _1;
    }

    /** 	dont_read = read*/
    _60dont_read_24979 = _read_24982;

    /** end procedure*/
    return;
    ;
}


void _60read_line()
{
    int _n_24985 = NOVALUE;
    int _14442 = NOVALUE;
    int _14441 = NOVALUE;
    int _14439 = NOVALUE;
    int _14438 = NOVALUE;
    int _14436 = NOVALUE;
    int _14435 = NOVALUE;
    int _0, _1, _2;
    

    /** 	line_number += 1*/
    _25line_number_12263 = _25line_number_12263 + 1;

    /** 	gline_number += 1*/
    _25gline_number_12267 = _25gline_number_12267 + 1;

    /** 	if dont_read then*/
    if (_60dont_read_24979 == 0)
    {
        goto L1; // [25] 36
    }
    else{
    }

    /** 		ThisLine = -1*/
    DeRef(_43ThisLine_49532);
    _43ThisLine_49532 = -1;
    goto L2; // [33] 108
L1: 

    /** 	elsif src_file < 0 then*/
    if (_25src_file_12404 >= 0)
    goto L3; // [40] 52

    /** 		ThisLine = -1*/
    DeRef(_43ThisLine_49532);
    _43ThisLine_49532 = -1;
    goto L2; // [49] 108
L3: 

    /** 		ThisLine = gets(src_file)*/
    DeRef(_43ThisLine_49532);
    _43ThisLine_49532 = EGets(_25src_file_12404);

    /** 		if sequence(ThisLine) and ends( {13,10}, ThisLine ) then*/
    _14435 = IS_SEQUENCE(_43ThisLine_49532);
    if (_14435 == 0) {
        goto L4; // [66] 107
    }
    RefDS(_14437);
    Ref(_43ThisLine_49532);
    _14438 = _7ends(_14437, _43ThisLine_49532);
    if (_14438 == 0) {
        DeRef(_14438);
        _14438 = NOVALUE;
        goto L4; // [78] 107
    }
    else {
        if (!IS_ATOM_INT(_14438) && DBL_PTR(_14438)->dbl == 0.0){
            DeRef(_14438);
            _14438 = NOVALUE;
            goto L4; // [78] 107
        }
        DeRef(_14438);
        _14438 = NOVALUE;
    }
    DeRef(_14438);
    _14438 = NOVALUE;

    /** 			ThisLine = remove(ThisLine, length(ThisLine))*/
    if (IS_SEQUENCE(_43ThisLine_49532)){
            _14439 = SEQ_PTR(_43ThisLine_49532)->length;
    }
    else {
        _14439 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_43ThisLine_49532);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_14439)) ? _14439 : (long)(DBL_PTR(_14439)->dbl);
        int stop = (IS_ATOM_INT(_14439)) ? _14439 : (long)(DBL_PTR(_14439)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_43ThisLine_49532), start, &_43ThisLine_49532 );
            }
            else Tail(SEQ_PTR(_43ThisLine_49532), stop+1, &_43ThisLine_49532);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_43ThisLine_49532), start, &_43ThisLine_49532);
        }
        else {
            assign_slice_seq = &assign_space;
            _43ThisLine_49532 = Remove_elements(start, stop, (SEQ_PTR(_43ThisLine_49532)->ref == 1));
        }
    }
    _14439 = NOVALUE;
    _14439 = NOVALUE;

    /** 			ThisLine[$] = 10*/
    if (IS_SEQUENCE(_43ThisLine_49532)){
            _14441 = SEQ_PTR(_43ThisLine_49532)->length;
    }
    else {
        _14441 = 1;
    }
    _2 = (int)SEQ_PTR(_43ThisLine_49532);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _43ThisLine_49532 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _14441);
    _1 = *(int *)_2;
    *(int *)_2 = 10;
    DeRef(_1);
L4: 
L2: 

    /** 	if atom(ThisLine) then*/
    _14442 = IS_ATOM(_43ThisLine_49532);
    if (_14442 == 0)
    {
        _14442 = NOVALUE;
        goto L5; // [115] 149
    }
    else{
        _14442 = NOVALUE;
    }

    /** 		ThisLine = {END_OF_FILE_CHAR}*/
    _0 = _43ThisLine_49532;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 26;
    _43ThisLine_49532 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 		if src_file >= 0 then*/
    if (_25src_file_12404 < 0)
    goto L6; // [130] 141

    /** 			close(src_file)*/
    EClose(_25src_file_12404);
L6: 

    /** 		src_file = -1*/
    _25src_file_12404 = -1;
L5: 

    /** 	bp = 1*/
    _43bp_49536 = 1;

    /** 	AppendSourceLine()*/
    _60AppendSourceLine();

    /** end procedure*/
    return;
    ;
}


int _60getch()
{
    int _c_25029 = NOVALUE;
    int _0, _1, _2;
    

    /** 	c = ThisLine[bp]*/
    _2 = (int)SEQ_PTR(_43ThisLine_49532);
    _c_25029 = (int)*(((s1_ptr)_2)->base + _43bp_49536);
    if (!IS_ATOM_INT(_c_25029)){
        _c_25029 = (long)DBL_PTR(_c_25029)->dbl;
    }

    /** 	bp += 1*/
    _43bp_49536 = _43bp_49536 + 1;

    /** 	return c*/
    return _c_25029;
    ;
}


void _60ungetch()
{
    int _0, _1, _2;
    

    /** 	bp -= 1*/
    _43bp_49536 = _43bp_49536 - 1;

    /** end procedure*/
    return;
    ;
}


int _60get_file_path(int _s_25041)
{
    int _14453 = NOVALUE;
    int _14451 = NOVALUE;
    int _14450 = NOVALUE;
    int _14449 = NOVALUE;
    int _14448 = NOVALUE;
    int _0, _1, _2;
    

    /** 		for t=length(s) to 1 by -1 do*/
    if (IS_SEQUENCE(_s_25041)){
            _14448 = SEQ_PTR(_s_25041)->length;
    }
    else {
        _14448 = 1;
    }
    {
        int _t_25043;
        _t_25043 = _14448;
L1: 
        if (_t_25043 < 1){
            goto L2; // [8] 50
        }

        /** 				if find(s[t],SLASH_CHARS) then*/
        _2 = (int)SEQ_PTR(_s_25041);
        _14449 = (int)*(((s1_ptr)_2)->base + _t_25043);
        _14450 = find_from(_14449, _36SLASH_CHARS_14735, 1);
        _14449 = NOVALUE;
        if (_14450 == 0)
        {
            _14450 = NOVALUE;
            goto L3; // [28] 43
        }
        else{
            _14450 = NOVALUE;
        }

        /** 						return s[1..t]*/
        rhs_slice_target = (object_ptr)&_14451;
        RHS_Slice(_s_25041, 1, _t_25043);
        DeRefDS(_s_25041);
        return _14451;
L3: 

        /** 		end for*/
        _t_25043 = _t_25043 + -1;
        goto L1; // [45] 15
L2: 
        ;
    }

    /** 		return "." & SLASH*/
    Append(&_14453, _14452, 92);
    DeRefDS(_s_25041);
    DeRef(_14451);
    _14451 = NOVALUE;
    return _14453;
    ;
}


int _60find_file(int _fname_25055)
{
    int _try_25056 = NOVALUE;
    int _full_path_25057 = NOVALUE;
    int _errbuff_25058 = NOVALUE;
    int _currdir_25059 = NOVALUE;
    int _conf_path_25060 = NOVALUE;
    int _scan_result_25061 = NOVALUE;
    int _inc_path_25062 = NOVALUE;
    int _mainpath_25081 = NOVALUE;
    int _32399 = NOVALUE;
    int _32398 = NOVALUE;
    int _14550 = NOVALUE;
    int _14548 = NOVALUE;
    int _14547 = NOVALUE;
    int _14546 = NOVALUE;
    int _14544 = NOVALUE;
    int _14542 = NOVALUE;
    int _14540 = NOVALUE;
    int _14539 = NOVALUE;
    int _14537 = NOVALUE;
    int _14536 = NOVALUE;
    int _14533 = NOVALUE;
    int _14530 = NOVALUE;
    int _14529 = NOVALUE;
    int _14528 = NOVALUE;
    int _14527 = NOVALUE;
    int _14526 = NOVALUE;
    int _14525 = NOVALUE;
    int _14524 = NOVALUE;
    int _14523 = NOVALUE;
    int _14520 = NOVALUE;
    int _14519 = NOVALUE;
    int _14515 = NOVALUE;
    int _14512 = NOVALUE;
    int _14511 = NOVALUE;
    int _14510 = NOVALUE;
    int _14509 = NOVALUE;
    int _14508 = NOVALUE;
    int _14507 = NOVALUE;
    int _14506 = NOVALUE;
    int _14505 = NOVALUE;
    int _14502 = NOVALUE;
    int _14498 = NOVALUE;
    int _14496 = NOVALUE;
    int _14495 = NOVALUE;
    int _14494 = NOVALUE;
    int _14493 = NOVALUE;
    int _14492 = NOVALUE;
    int _14489 = NOVALUE;
    int _14488 = NOVALUE;
    int _14487 = NOVALUE;
    int _14486 = NOVALUE;
    int _14485 = NOVALUE;
    int _14484 = NOVALUE;
    int _14482 = NOVALUE;
    int _14481 = NOVALUE;
    int _14480 = NOVALUE;
    int _14479 = NOVALUE;
    int _14478 = NOVALUE;
    int _14476 = NOVALUE;
    int _14475 = NOVALUE;
    int _14472 = NOVALUE;
    int _14469 = NOVALUE;
    int _14467 = NOVALUE;
    int _14464 = NOVALUE;
    int _14462 = NOVALUE;
    int _14461 = NOVALUE;
    int _14458 = NOVALUE;
    int _14457 = NOVALUE;
    int _14455 = NOVALUE;
    int _14454 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if absolute_path(fname) then*/
    RefDS(_fname_25055);
    _14454 = _9absolute_path(_fname_25055);
    if (_14454 == 0) {
        DeRef(_14454);
        _14454 = NOVALUE;
        goto L1; // [9] 42
    }
    else {
        if (!IS_ATOM_INT(_14454) && DBL_PTR(_14454)->dbl == 0.0){
            DeRef(_14454);
            _14454 = NOVALUE;
            goto L1; // [9] 42
        }
        DeRef(_14454);
        _14454 = NOVALUE;
    }
    DeRef(_14454);
    _14454 = NOVALUE;

    /** 		if not file_exists(fname) then*/
    RefDS(_fname_25055);
    _14455 = _9file_exists(_fname_25055);
    if (IS_ATOM_INT(_14455)) {
        if (_14455 != 0){
            DeRef(_14455);
            _14455 = NOVALUE;
            goto L2; // [18] 35
        }
    }
    else {
        if (DBL_PTR(_14455)->dbl != 0.0){
            DeRef(_14455);
            _14455 = NOVALUE;
            goto L2; // [18] 35
        }
    }
    DeRef(_14455);
    _14455 = NOVALUE;

    /** 			CompileErr(51, {new_include_name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_25new_include_name_12405);
    *((int *)(_2+4)) = _25new_include_name_12405;
    _14457 = MAKE_SEQ(_1);
    _43CompileErr(51, _14457, 0);
    _14457 = NOVALUE;
L2: 

    /** 		return fname*/
    DeRef(_full_path_25057);
    DeRef(_errbuff_25058);
    DeRef(_currdir_25059);
    DeRef(_conf_path_25060);
    DeRef(_scan_result_25061);
    DeRef(_inc_path_25062);
    DeRef(_mainpath_25081);
    return _fname_25055;
L1: 

    /** 	currdir = get_file_path( known_files[current_file_no] )*/
    _2 = (int)SEQ_PTR(_26known_files_11139);
    _14458 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    Ref(_14458);
    _0 = _currdir_25059;
    _currdir_25059 = _60get_file_path(_14458);
    DeRef(_0);
    _14458 = NOVALUE;

    /** 	full_path = currdir & fname*/
    Concat((object_ptr)&_full_path_25057, _currdir_25059, _fname_25055);

    /** 	if file_exists(full_path) then*/
    RefDS(_full_path_25057);
    _14461 = _9file_exists(_full_path_25057);
    if (_14461 == 0) {
        DeRef(_14461);
        _14461 = NOVALUE;
        goto L3; // [70] 80
    }
    else {
        if (!IS_ATOM_INT(_14461) && DBL_PTR(_14461)->dbl == 0.0){
            DeRef(_14461);
            _14461 = NOVALUE;
            goto L3; // [70] 80
        }
        DeRef(_14461);
        _14461 = NOVALUE;
    }
    DeRef(_14461);
    _14461 = NOVALUE;

    /** 		return full_path*/
    DeRefDS(_fname_25055);
    DeRef(_errbuff_25058);
    DeRefDS(_currdir_25059);
    DeRef(_conf_path_25060);
    DeRef(_scan_result_25061);
    DeRef(_inc_path_25062);
    DeRef(_mainpath_25081);
    return _full_path_25057;
L3: 

    /** 	sequence mainpath = main_path[1..rfind(SLASH, main_path)]*/
    RefDS(_25main_path_12403);
    DeRef(_32398);
    _32398 = _25main_path_12403;
    if (IS_SEQUENCE(_32398)){
            _32399 = SEQ_PTR(_32398)->length;
    }
    else {
        _32399 = 1;
    }
    _32398 = NOVALUE;
    RefDS(_25main_path_12403);
    _14462 = _7rfind(92, _25main_path_12403, _32399);
    _32399 = NOVALUE;
    rhs_slice_target = (object_ptr)&_mainpath_25081;
    RHS_Slice(_25main_path_12403, 1, _14462);

    /** 	if not equal(mainpath, currdir) then*/
    if (_mainpath_25081 == _currdir_25059)
    _14464 = 1;
    else if (IS_ATOM_INT(_mainpath_25081) && IS_ATOM_INT(_currdir_25059))
    _14464 = 0;
    else
    _14464 = (compare(_mainpath_25081, _currdir_25059) == 0);
    if (_14464 != 0)
    goto L4; // [111] 139
    _14464 = NOVALUE;

    /** 		full_path = mainpath & new_include_name*/
    Concat((object_ptr)&_full_path_25057, _mainpath_25081, _25new_include_name_12405);

    /** 		if file_exists(full_path) then*/
    RefDS(_full_path_25057);
    _14467 = _9file_exists(_full_path_25057);
    if (_14467 == 0) {
        DeRef(_14467);
        _14467 = NOVALUE;
        goto L5; // [128] 138
    }
    else {
        if (!IS_ATOM_INT(_14467) && DBL_PTR(_14467)->dbl == 0.0){
            DeRef(_14467);
            _14467 = NOVALUE;
            goto L5; // [128] 138
        }
        DeRef(_14467);
        _14467 = NOVALUE;
    }
    DeRef(_14467);
    _14467 = NOVALUE;

    /** 			return full_path*/
    DeRefDS(_fname_25055);
    DeRef(_errbuff_25058);
    DeRefDS(_currdir_25059);
    DeRef(_conf_path_25060);
    DeRef(_scan_result_25061);
    DeRef(_inc_path_25062);
    DeRefDS(_mainpath_25081);
    _32398 = NOVALUE;
    DeRef(_14462);
    _14462 = NOVALUE;
    return _full_path_25057;
L5: 
L4: 

    /** 	scan_result = ConfPath(new_include_name)*/
    RefDS(_25new_include_name_12405);
    _0 = _scan_result_25061;
    _scan_result_25061 = _38ConfPath(_25new_include_name_12405);
    DeRef(_0);

    /** 	if atom(scan_result) then*/
    _14469 = IS_ATOM(_scan_result_25061);
    if (_14469 == 0)
    {
        _14469 = NOVALUE;
        goto L6; // [152] 164
    }
    else{
        _14469 = NOVALUE;
    }

    /** 		scan_result = ScanPath(fname,"EUINC",0)*/
    RefDS(_fname_25055);
    RefDS(_14470);
    _0 = _scan_result_25061;
    _scan_result_25061 = _38ScanPath(_fname_25055, _14470, 0);
    DeRef(_0);
L6: 

    /** 	if atom(scan_result) then*/
    _14472 = IS_ATOM(_scan_result_25061);
    if (_14472 == 0)
    {
        _14472 = NOVALUE;
        goto L7; // [169] 181
    }
    else{
        _14472 = NOVALUE;
    }

    /** 		scan_result = ScanPath(fname, "EUDIR",1)*/
    RefDS(_fname_25055);
    RefDS(_14473);
    _0 = _scan_result_25061;
    _scan_result_25061 = _38ScanPath(_fname_25055, _14473, 1);
    DeRef(_0);
L7: 

    /** 	if atom(scan_result) then*/
    _14475 = IS_ATOM(_scan_result_25061);
    if (_14475 == 0)
    {
        _14475 = NOVALUE;
        goto L8; // [186] 223
    }
    else{
        _14475 = NOVALUE;
    }

    /** 		full_path = get_eudir() & SLASH & "include" & SLASH & fname*/
    _14476 = _26get_eudir();
    {
        int concat_list[5];

        concat_list[0] = _fname_25055;
        concat_list[1] = 92;
        concat_list[2] = _13893;
        concat_list[3] = 92;
        concat_list[4] = _14476;
        Concat_N((object_ptr)&_full_path_25057, concat_list, 5);
    }
    DeRef(_14476);
    _14476 = NOVALUE;

    /** 		if file_exists(full_path) then*/
    RefDS(_full_path_25057);
    _14478 = _9file_exists(_full_path_25057);
    if (_14478 == 0) {
        DeRef(_14478);
        _14478 = NOVALUE;
        goto L9; // [212] 222
    }
    else {
        if (!IS_ATOM_INT(_14478) && DBL_PTR(_14478)->dbl == 0.0){
            DeRef(_14478);
            _14478 = NOVALUE;
            goto L9; // [212] 222
        }
        DeRef(_14478);
        _14478 = NOVALUE;
    }
    DeRef(_14478);
    _14478 = NOVALUE;

    /** 			return full_path*/
    DeRefDS(_fname_25055);
    DeRef(_errbuff_25058);
    DeRef(_currdir_25059);
    DeRef(_conf_path_25060);
    DeRef(_scan_result_25061);
    DeRef(_inc_path_25062);
    DeRef(_mainpath_25081);
    _32398 = NOVALUE;
    DeRef(_14462);
    _14462 = NOVALUE;
    return _full_path_25057;
L9: 
L8: 

    /** 	if sequence(scan_result) then*/
    _14479 = IS_SEQUENCE(_scan_result_25061);
    if (_14479 == 0)
    {
        _14479 = NOVALUE;
        goto LA; // [228] 250
    }
    else{
        _14479 = NOVALUE;
    }

    /** 		close(scan_result[2])*/
    _2 = (int)SEQ_PTR(_scan_result_25061);
    _14480 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_14480))
    EClose(_14480);
    else
    EClose((int)DBL_PTR(_14480)->dbl);
    _14480 = NOVALUE;

    /** 		return scan_result[1]*/
    _2 = (int)SEQ_PTR(_scan_result_25061);
    _14481 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_14481);
    DeRefDS(_fname_25055);
    DeRef(_full_path_25057);
    DeRef(_errbuff_25058);
    DeRef(_currdir_25059);
    DeRef(_conf_path_25060);
    DeRef(_scan_result_25061);
    DeRef(_inc_path_25062);
    DeRef(_mainpath_25081);
    _32398 = NOVALUE;
    DeRef(_14462);
    _14462 = NOVALUE;
    return _14481;
LA: 

    /** 	errbuff = ""*/
    RefDS(_5);
    DeRef(_errbuff_25058);
    _errbuff_25058 = _5;

    /** 	full_path = {}*/
    RefDS(_5);
    DeRef(_full_path_25057);
    _full_path_25057 = _5;

    /** 	if length(currdir) > 0 then*/
    if (IS_SEQUENCE(_currdir_25059)){
            _14482 = SEQ_PTR(_currdir_25059)->length;
    }
    else {
        _14482 = 1;
    }
    if (_14482 <= 0)
    goto LB; // [269] 321

    /** 		if find(currdir[$], SLASH_CHARS) then*/
    if (IS_SEQUENCE(_currdir_25059)){
            _14484 = SEQ_PTR(_currdir_25059)->length;
    }
    else {
        _14484 = 1;
    }
    _2 = (int)SEQ_PTR(_currdir_25059);
    _14485 = (int)*(((s1_ptr)_2)->base + _14484);
    _14486 = find_from(_14485, _36SLASH_CHARS_14735, 1);
    _14485 = NOVALUE;
    if (_14486 == 0)
    {
        _14486 = NOVALUE;
        goto LC; // [289] 313
    }
    else{
        _14486 = NOVALUE;
    }

    /** 			full_path = append(full_path, currdir[1..$-1])*/
    if (IS_SEQUENCE(_currdir_25059)){
            _14487 = SEQ_PTR(_currdir_25059)->length;
    }
    else {
        _14487 = 1;
    }
    _14488 = _14487 - 1;
    _14487 = NOVALUE;
    rhs_slice_target = (object_ptr)&_14489;
    RHS_Slice(_currdir_25059, 1, _14488);
    RefDS(_14489);
    Append(&_full_path_25057, _full_path_25057, _14489);
    DeRefDS(_14489);
    _14489 = NOVALUE;
    goto LD; // [310] 320
LC: 

    /** 			full_path = append(full_path, currdir)*/
    RefDS(_currdir_25059);
    Append(&_full_path_25057, _full_path_25057, _currdir_25059);
LD: 
LB: 

    /** 	if find(main_path[$], SLASH_CHARS) then*/
    if (IS_SEQUENCE(_25main_path_12403)){
            _14492 = SEQ_PTR(_25main_path_12403)->length;
    }
    else {
        _14492 = 1;
    }
    _2 = (int)SEQ_PTR(_25main_path_12403);
    _14493 = (int)*(((s1_ptr)_2)->base + _14492);
    _14494 = find_from(_14493, _36SLASH_CHARS_14735, 1);
    _14493 = NOVALUE;
    if (_14494 == 0)
    {
        _14494 = NOVALUE;
        goto LE; // [339] 361
    }
    else{
        _14494 = NOVALUE;
    }

    /** 		errbuff = main_path[1..$-1]  -- looks better*/
    if (IS_SEQUENCE(_25main_path_12403)){
            _14495 = SEQ_PTR(_25main_path_12403)->length;
    }
    else {
        _14495 = 1;
    }
    _14496 = _14495 - 1;
    _14495 = NOVALUE;
    rhs_slice_target = (object_ptr)&_errbuff_25058;
    RHS_Slice(_25main_path_12403, 1, _14496);
    goto LF; // [358] 371
LE: 

    /** 		errbuff = main_path*/
    RefDS(_25main_path_12403);
    DeRef(_errbuff_25058);
    _errbuff_25058 = _25main_path_12403;
LF: 

    /** 	if not find(errbuff, full_path) then*/
    _14498 = find_from(_errbuff_25058, _full_path_25057, 1);
    if (_14498 != 0)
    goto L10; // [378] 388
    _14498 = NOVALUE;

    /** 		full_path = append(full_path, errbuff)*/
    RefDS(_errbuff_25058);
    Append(&_full_path_25057, _full_path_25057, _errbuff_25058);
L10: 

    /** 	conf_path = get_conf_dirs()*/
    _0 = _conf_path_25060;
    _conf_path_25060 = _38get_conf_dirs();
    DeRef(_0);

    /** 	if length(conf_path) > 0 then*/
    if (IS_SEQUENCE(_conf_path_25060)){
            _14502 = SEQ_PTR(_conf_path_25060)->length;
    }
    else {
        _14502 = 1;
    }
    if (_14502 <= 0)
    goto L11; // [400] 507

    /** 		conf_path = split(conf_path, PATHSEP)*/
    RefDS(_conf_path_25060);
    _0 = _conf_path_25060;
    _conf_path_25060 = _21split(_conf_path_25060, 59, 0, 0);
    DeRefDS(_0);

    /** 		for i = 1 to length(conf_path) do*/
    if (IS_SEQUENCE(_conf_path_25060)){
            _14505 = SEQ_PTR(_conf_path_25060)->length;
    }
    else {
        _14505 = 1;
    }
    {
        int _i_25162;
        _i_25162 = 1;
L12: 
        if (_i_25162 > _14505){
            goto L13; // [422] 506
        }

        /** 			if find(conf_path[i][$], SLASH_CHARS) then*/
        _2 = (int)SEQ_PTR(_conf_path_25060);
        _14506 = (int)*(((s1_ptr)_2)->base + _i_25162);
        if (IS_SEQUENCE(_14506)){
                _14507 = SEQ_PTR(_14506)->length;
        }
        else {
            _14507 = 1;
        }
        _2 = (int)SEQ_PTR(_14506);
        _14508 = (int)*(((s1_ptr)_2)->base + _14507);
        _14506 = NOVALUE;
        _14509 = find_from(_14508, _36SLASH_CHARS_14735, 1);
        _14508 = NOVALUE;
        if (_14509 == 0)
        {
            _14509 = NOVALUE;
            goto L14; // [449] 473
        }
        else{
            _14509 = NOVALUE;
        }

        /** 				errbuff = conf_path[i][1..$-1]  -- looks better*/
        _2 = (int)SEQ_PTR(_conf_path_25060);
        _14510 = (int)*(((s1_ptr)_2)->base + _i_25162);
        if (IS_SEQUENCE(_14510)){
                _14511 = SEQ_PTR(_14510)->length;
        }
        else {
            _14511 = 1;
        }
        _14512 = _14511 - 1;
        _14511 = NOVALUE;
        rhs_slice_target = (object_ptr)&_errbuff_25058;
        RHS_Slice(_14510, 1, _14512);
        _14510 = NOVALUE;
        goto L15; // [470] 482
L14: 

        /** 				errbuff = conf_path[i]*/
        DeRef(_errbuff_25058);
        _2 = (int)SEQ_PTR(_conf_path_25060);
        _errbuff_25058 = (int)*(((s1_ptr)_2)->base + _i_25162);
        Ref(_errbuff_25058);
L15: 

        /** 			if not find(errbuff, full_path) then*/
        _14515 = find_from(_errbuff_25058, _full_path_25057, 1);
        if (_14515 != 0)
        goto L16; // [489] 499
        _14515 = NOVALUE;

        /** 				full_path = append(full_path, errbuff)*/
        RefDS(_errbuff_25058);
        Append(&_full_path_25057, _full_path_25057, _errbuff_25058);
L16: 

        /** 		end for*/
        _i_25162 = _i_25162 + 1;
        goto L12; // [501] 429
L13: 
        ;
    }
L11: 

    /** 	inc_path = getenv("EUINC")*/
    DeRef(_inc_path_25062);
    _inc_path_25062 = EGetEnv(_14470);

    /** 	if sequence(inc_path) then*/
    _14519 = IS_SEQUENCE(_inc_path_25062);
    if (_14519 == 0)
    {
        _14519 = NOVALUE;
        goto L17; // [517] 631
    }
    else{
        _14519 = NOVALUE;
    }

    /** 		if length(inc_path) > 0 then*/
    if (IS_SEQUENCE(_inc_path_25062)){
            _14520 = SEQ_PTR(_inc_path_25062)->length;
    }
    else {
        _14520 = 1;
    }
    if (_14520 <= 0)
    goto L18; // [525] 630

    /** 			inc_path = split(inc_path, PATHSEP)*/
    Ref(_inc_path_25062);
    _0 = _inc_path_25062;
    _inc_path_25062 = _21split(_inc_path_25062, 59, 0, 0);
    DeRefi(_0);

    /** 			for i = 1 to length(inc_path) do*/
    if (IS_SEQUENCE(_inc_path_25062)){
            _14523 = SEQ_PTR(_inc_path_25062)->length;
    }
    else {
        _14523 = 1;
    }
    {
        int _i_25190;
        _i_25190 = 1;
L19: 
        if (_i_25190 > _14523){
            goto L1A; // [545] 629
        }

        /** 				if find(inc_path[i][$], SLASH_CHARS) then*/
        _2 = (int)SEQ_PTR(_inc_path_25062);
        _14524 = (int)*(((s1_ptr)_2)->base + _i_25190);
        if (IS_SEQUENCE(_14524)){
                _14525 = SEQ_PTR(_14524)->length;
        }
        else {
            _14525 = 1;
        }
        _2 = (int)SEQ_PTR(_14524);
        _14526 = (int)*(((s1_ptr)_2)->base + _14525);
        _14524 = NOVALUE;
        _14527 = find_from(_14526, _36SLASH_CHARS_14735, 1);
        _14526 = NOVALUE;
        if (_14527 == 0)
        {
            _14527 = NOVALUE;
            goto L1B; // [572] 596
        }
        else{
            _14527 = NOVALUE;
        }

        /** 					errbuff = inc_path[i][1..$-1]  -- looks better*/
        _2 = (int)SEQ_PTR(_inc_path_25062);
        _14528 = (int)*(((s1_ptr)_2)->base + _i_25190);
        if (IS_SEQUENCE(_14528)){
                _14529 = SEQ_PTR(_14528)->length;
        }
        else {
            _14529 = 1;
        }
        _14530 = _14529 - 1;
        _14529 = NOVALUE;
        rhs_slice_target = (object_ptr)&_errbuff_25058;
        RHS_Slice(_14528, 1, _14530);
        _14528 = NOVALUE;
        goto L1C; // [593] 605
L1B: 

        /** 					errbuff = inc_path[i]*/
        DeRef(_errbuff_25058);
        _2 = (int)SEQ_PTR(_inc_path_25062);
        _errbuff_25058 = (int)*(((s1_ptr)_2)->base + _i_25190);
        Ref(_errbuff_25058);
L1C: 

        /** 				if not find(errbuff, full_path) then*/
        _14533 = find_from(_errbuff_25058, _full_path_25057, 1);
        if (_14533 != 0)
        goto L1D; // [612] 622
        _14533 = NOVALUE;

        /** 					full_path = append(full_path, errbuff)*/
        RefDS(_errbuff_25058);
        Append(&_full_path_25057, _full_path_25057, _errbuff_25058);
L1D: 

        /** 			end for*/
        _i_25190 = _i_25190 + 1;
        goto L19; // [624] 552
L1A: 
        ;
    }
L18: 
L17: 

    /** 	if length(get_eudir()) > 0 then*/
    _14536 = _26get_eudir();
    if (IS_SEQUENCE(_14536)){
            _14537 = SEQ_PTR(_14536)->length;
    }
    else {
        _14537 = 1;
    }
    DeRef(_14536);
    _14536 = NOVALUE;
    if (_14537 <= 0)
    goto L1E; // [639] 667

    /** 		if not find(get_eudir(), full_path) then*/
    _14539 = _26get_eudir();
    _14540 = find_from(_14539, _full_path_25057, 1);
    DeRef(_14539);
    _14539 = NOVALUE;
    if (_14540 != 0)
    goto L1F; // [653] 666
    _14540 = NOVALUE;

    /** 			full_path = append(full_path, get_eudir())*/
    _14542 = _26get_eudir();
    Ref(_14542);
    Append(&_full_path_25057, _full_path_25057, _14542);
    DeRef(_14542);
    _14542 = NOVALUE;
L1F: 
L1E: 

    /** 	errbuff = ""*/
    RefDS(_5);
    DeRef(_errbuff_25058);
    _errbuff_25058 = _5;

    /** 	for i = 1 to length(full_path) do*/
    if (IS_SEQUENCE(_full_path_25057)){
            _14544 = SEQ_PTR(_full_path_25057)->length;
    }
    else {
        _14544 = 1;
    }
    {
        int _i_25222;
        _i_25222 = 1;
L20: 
        if (_i_25222 > _14544){
            goto L21; // [679] 711
        }

        /** 		errbuff &= sprintf("\t%s\n", {full_path[i]})*/
        _2 = (int)SEQ_PTR(_full_path_25057);
        _14546 = (int)*(((s1_ptr)_2)->base + _i_25222);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_14546);
        *((int *)(_2+4)) = _14546;
        _14547 = MAKE_SEQ(_1);
        _14546 = NOVALUE;
        _14548 = EPrintf(-9999999, _14545, _14547);
        DeRefDS(_14547);
        _14547 = NOVALUE;
        Concat((object_ptr)&_errbuff_25058, _errbuff_25058, _14548);
        DeRefDS(_14548);
        _14548 = NOVALUE;

        /** 	end for*/
        _i_25222 = _i_25222 + 1;
        goto L20; // [706] 686
L21: 
        ;
    }

    /** 	CompileErr(52, {new_include_name, errbuff})*/
    RefDS(_errbuff_25058);
    RefDS(_25new_include_name_12405);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _25new_include_name_12405;
    ((int *)_2)[2] = _errbuff_25058;
    _14550 = MAKE_SEQ(_1);
    _43CompileErr(52, _14550, 0);
    _14550 = NOVALUE;
    ;
}


int _60path_open()
{
    int _fh_25234 = NOVALUE;
    int _0, _1, _2;
    

    /** 	new_include_name = find_file(new_include_name)*/
    RefDS(_25new_include_name_12405);
    _0 = _60find_file(_25new_include_name_12405);
    DeRefDS(_25new_include_name_12405);
    _25new_include_name_12405 = _0;

    /** 	new_include_name = maybe_preprocess(new_include_name)*/
    RefDS(_25new_include_name_12405);
    _0 = _64maybe_preprocess(_25new_include_name_12405);
    DeRefDS(_25new_include_name_12405);
    _25new_include_name_12405 = _0;

    /** 	fh = open_locked(new_include_name)*/
    RefDS(_25new_include_name_12405);
    _fh_25234 = _26open_locked(_25new_include_name_12405);
    if (!IS_ATOM_INT(_fh_25234)) {
        _1 = (long)(DBL_PTR(_fh_25234)->dbl);
        if (UNIQUE(DBL_PTR(_fh_25234)) && (DBL_PTR(_fh_25234)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fh_25234);
        _fh_25234 = _1;
    }

    /** 	return fh*/
    return _fh_25234;
    ;
}


int _60NameSpace_declaration(int _sym_25262)
{
    int _h_25263 = NOVALUE;
    int _14574 = NOVALUE;
    int _14572 = NOVALUE;
    int _14570 = NOVALUE;
    int _14568 = NOVALUE;
    int _14567 = NOVALUE;
    int _14566 = NOVALUE;
    int _14564 = NOVALUE;
    int _14563 = NOVALUE;
    int _14562 = NOVALUE;
    int _14561 = NOVALUE;
    int _14560 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_sym_25262)) {
        _1 = (long)(DBL_PTR(_sym_25262)->dbl);
        if (UNIQUE(DBL_PTR(_sym_25262)) && (DBL_PTR(_sym_25262)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_25262);
        _sym_25262 = _1;
    }

    /** 	DefinedYet(sym)*/
    _52DefinedYet(_sym_25262);

    /** 	if find(SymTab[sym][S_SCOPE], {SC_GLOBAL, SC_PUBLIC, SC_EXPORT, SC_PREDEF}) then*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _14560 = (int)*(((s1_ptr)_2)->base + _sym_25262);
    _2 = (int)SEQ_PTR(_14560);
    _14561 = (int)*(((s1_ptr)_2)->base + 4);
    _14560 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 6;
    *((int *)(_2+8)) = 13;
    *((int *)(_2+12)) = 11;
    *((int *)(_2+16)) = 7;
    _14562 = MAKE_SEQ(_1);
    _14563 = find_from(_14561, _14562, 1);
    _14561 = NOVALUE;
    DeRefDS(_14562);
    _14562 = NOVALUE;
    if (_14563 == 0)
    {
        _14563 = NOVALUE;
        goto L1; // [42] 104
    }
    else{
        _14563 = NOVALUE;
    }

    /** 		h = SymTab[sym][S_HASHVAL]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _14564 = (int)*(((s1_ptr)_2)->base + _sym_25262);
    _2 = (int)SEQ_PTR(_14564);
    _h_25263 = (int)*(((s1_ptr)_2)->base + 11);
    if (!IS_ATOM_INT(_h_25263)){
        _h_25263 = (long)DBL_PTR(_h_25263)->dbl;
    }
    _14564 = NOVALUE;

    /** 		sym = NewEntry(SymTab[sym][S_NAME], 0, 0, VARIABLE, h, buckets[h], 0)*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _14566 = (int)*(((s1_ptr)_2)->base + _sym_25262);
    _2 = (int)SEQ_PTR(_14566);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _14567 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _14567 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _14566 = NOVALUE;
    _2 = (int)SEQ_PTR(_52buckets_47095);
    _14568 = (int)*(((s1_ptr)_2)->base + _h_25263);
    Ref(_14567);
    Ref(_14568);
    _sym_25262 = _52NewEntry(_14567, 0, 0, -100, _h_25263, _14568, 0);
    _14567 = NOVALUE;
    _14568 = NOVALUE;
    if (!IS_ATOM_INT(_sym_25262)) {
        _1 = (long)(DBL_PTR(_sym_25262)->dbl);
        if (UNIQUE(DBL_PTR(_sym_25262)) && (DBL_PTR(_sym_25262)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_25262);
        _sym_25262 = _1;
    }

    /** 		buckets[h] = sym*/
    _2 = (int)SEQ_PTR(_52buckets_47095);
    _2 = (int)(((s1_ptr)_2)->base + _h_25263);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_25262;
    DeRef(_1);
L1: 

    /** 	SymTab[sym][S_SCOPE] = SC_LOCAL*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_25262 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 5;
    DeRef(_1);
    _14570 = NOVALUE;

    /** 	SymTab[sym][S_MODE] = M_CONSTANT*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_25262 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _14572 = NOVALUE;

    /** 	SymTab[sym][S_TOKEN] = NAMESPACE -- [S_OBJ] will get the file number referred-to*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_25262 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_TOKEN_11918))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_TOKEN_11918)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_TOKEN_11918);
    _1 = *(int *)_2;
    *(int *)_2 = 523;
    DeRef(_1);
    _14574 = NOVALUE;

    /** 	if TRANSLATE then*/
    if (_25TRANSLATE_11874 == 0)
    {
        goto L2; // [159] 173
    }
    else{
    }

    /** 		num_routines += 1 -- order of ns declaration relative to routines*/
    _25num_routines_12271 = _25num_routines_12271 + 1;
L2: 

    /** 	return sym*/
    return _sym_25262;
    ;
}


void _60default_namespace()
{
    int _tok_25313 = NOVALUE;
    int _sym_25315 = NOVALUE;
    int _14598 = NOVALUE;
    int _14597 = NOVALUE;
    int _14595 = NOVALUE;
    int _14593 = NOVALUE;
    int _14590 = NOVALUE;
    int _14587 = NOVALUE;
    int _14585 = NOVALUE;
    int _14583 = NOVALUE;
    int _14582 = NOVALUE;
    int _14581 = NOVALUE;
    int _14580 = NOVALUE;
    int _14579 = NOVALUE;
    int _14578 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	tok = call_func( scanner_rid, {} )*/
    _0 = (int)_00[_60scanner_rid_25309].addr;
    _1 = (*(int (*)())_0)(
                         );
    DeRef(_tok_25313);
    _tok_25313 = _1;

    /** 	if tok[T_ID] = VARIABLE and equal( SymTab[tok[T_SYM]][S_NAME], "namespace" ) then*/
    _2 = (int)SEQ_PTR(_tok_25313);
    _14578 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_14578)) {
        _14579 = (_14578 == -100);
    }
    else {
        _14579 = binary_op(EQUALS, _14578, -100);
    }
    _14578 = NOVALUE;
    if (IS_ATOM_INT(_14579)) {
        if (_14579 == 0) {
            goto L1; // [23] 177
        }
    }
    else {
        if (DBL_PTR(_14579)->dbl == 0.0) {
            goto L1; // [23] 177
        }
    }
    _2 = (int)SEQ_PTR(_tok_25313);
    _14581 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_14581)){
        _14582 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14581)->dbl));
    }
    else{
        _14582 = (int)*(((s1_ptr)_2)->base + _14581);
    }
    _2 = (int)SEQ_PTR(_14582);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _14583 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _14583 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _14582 = NOVALUE;
    if (_14583 == _14584)
    _14585 = 1;
    else if (IS_ATOM_INT(_14583) && IS_ATOM_INT(_14584))
    _14585 = 0;
    else
    _14585 = (compare(_14583, _14584) == 0);
    _14583 = NOVALUE;
    if (_14585 == 0)
    {
        _14585 = NOVALUE;
        goto L1; // [50] 177
    }
    else{
        _14585 = NOVALUE;
    }

    /** 		tok = call_func( scanner_rid, {} )*/
    _0 = (int)_00[_60scanner_rid_25309].addr;
    _1 = (*(int (*)())_0)(
                         );
    DeRef(_tok_25313);
    _tok_25313 = _1;

    /** 		if tok[T_ID] != VARIABLE then*/
    _2 = (int)SEQ_PTR(_tok_25313);
    _14587 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _14587, -100)){
        _14587 = NOVALUE;
        goto L2; // [71] 83
    }
    _14587 = NOVALUE;

    /** 			CompileErr(114)*/
    RefDS(_22682);
    _43CompileErr(114, _22682, 0);
L2: 

    /** 		sym = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_25313);
    _sym_25315 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_sym_25315)){
        _sym_25315 = (long)DBL_PTR(_sym_25315)->dbl;
    }

    /** 		SymTab[sym][S_FILE_NO] = current_file_no*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_25315 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_FILE_NO_11909))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_FILE_NO_11909)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_FILE_NO_11909);
    _1 = *(int *)_2;
    *(int *)_2 = _25current_file_no_12262;
    DeRef(_1);
    _14590 = NOVALUE;

    /** 		sym  = NameSpace_declaration( sym )*/
    _sym_25315 = _60NameSpace_declaration(_sym_25315);
    if (!IS_ATOM_INT(_sym_25315)) {
        _1 = (long)(DBL_PTR(_sym_25315)->dbl);
        if (UNIQUE(DBL_PTR(_sym_25315)) && (DBL_PTR(_sym_25315)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_25315);
        _sym_25315 = _1;
    }

    /** 		SymTab[sym][S_OBJ] = current_file_no*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_25315 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _25current_file_no_12262;
    DeRef(_1);
    _14593 = NOVALUE;

    /** 		SymTab[sym][S_SCOPE] = SC_PUBLIC*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_25315 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 13;
    DeRef(_1);
    _14595 = NOVALUE;

    /** 		default_namespaces[current_file_no] = SymTab[sym][S_NAME]*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    _14597 = (int)*(((s1_ptr)_2)->base + _sym_25315);
    _2 = (int)SEQ_PTR(_14597);
    if (!IS_ATOM_INT(_25S_NAME_11913)){
        _14598 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_NAME_11913)->dbl));
    }
    else{
        _14598 = (int)*(((s1_ptr)_2)->base + _25S_NAME_11913);
    }
    _14597 = NOVALUE;
    Ref(_14598);
    _2 = (int)SEQ_PTR(_60default_namespaces_24676);
    _2 = (int)(((s1_ptr)_2)->base + _25current_file_no_12262);
    _1 = *(int *)_2;
    *(int *)_2 = _14598;
    if( _1 != _14598 ){
        DeRef(_1);
    }
    _14598 = NOVALUE;
    goto L3; // [174] 185
L1: 

    /** 		bp = 1*/
    _43bp_49536 = 1;
L3: 

    /** end procedure*/
    DeRef(_tok_25313);
    _14581 = NOVALUE;
    DeRef(_14579);
    _14579 = NOVALUE;
    return;
    ;
}


void _60add_exports(int _from_file_25365, int _to_file_25366)
{
    int _exports_25367 = NOVALUE;
    int _direct_25368 = NOVALUE;
    int _14618 = NOVALUE;
    int _14617 = NOVALUE;
    int _14616 = NOVALUE;
    int _14615 = NOVALUE;
    int _14614 = NOVALUE;
    int _14612 = NOVALUE;
    int _14610 = NOVALUE;
    int _14609 = NOVALUE;
    int _14607 = NOVALUE;
    int _14606 = NOVALUE;
    int _14605 = NOVALUE;
    int _14603 = NOVALUE;
    int _14602 = NOVALUE;
    int _14601 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	direct = file_include[to_file]*/
    DeRef(_direct_25368);
    _2 = (int)SEQ_PTR(_26file_include_11143);
    _direct_25368 = (int)*(((s1_ptr)_2)->base + _to_file_25366);
    Ref(_direct_25368);

    /** 	exports = file_public[from_file]*/
    DeRef(_exports_25367);
    _2 = (int)SEQ_PTR(_26file_public_11151);
    _exports_25367 = (int)*(((s1_ptr)_2)->base + _from_file_25365);
    Ref(_exports_25367);

    /** 	for i = 1 to length(exports) do*/
    if (IS_SEQUENCE(_exports_25367)){
            _14601 = SEQ_PTR(_exports_25367)->length;
    }
    else {
        _14601 = 1;
    }
    {
        int _i_25374;
        _i_25374 = 1;
L1: 
        if (_i_25374 > _14601){
            goto L2; // [30] 127
        }

        /** 		if not find( exports[i], direct ) then*/
        _2 = (int)SEQ_PTR(_exports_25367);
        _14602 = (int)*(((s1_ptr)_2)->base + _i_25374);
        _14603 = find_from(_14602, _direct_25368, 1);
        _14602 = NOVALUE;
        if (_14603 != 0)
        goto L3; // [48] 120
        _14603 = NOVALUE;

        /** 			if not find( -exports[i], direct ) then*/
        _2 = (int)SEQ_PTR(_exports_25367);
        _14605 = (int)*(((s1_ptr)_2)->base + _i_25374);
        if (IS_ATOM_INT(_14605)) {
            if ((unsigned long)_14605 == 0xC0000000)
            _14606 = (int)NewDouble((double)-0xC0000000);
            else
            _14606 = - _14605;
        }
        else {
            _14606 = unary_op(UMINUS, _14605);
        }
        _14605 = NOVALUE;
        _14607 = find_from(_14606, _direct_25368, 1);
        DeRef(_14606);
        _14606 = NOVALUE;
        if (_14607 != 0)
        goto L4; // [65] 82
        _14607 = NOVALUE;

        /** 				direct &= -exports[i]*/
        _2 = (int)SEQ_PTR(_exports_25367);
        _14609 = (int)*(((s1_ptr)_2)->base + _i_25374);
        if (IS_ATOM_INT(_14609)) {
            if ((unsigned long)_14609 == 0xC0000000)
            _14610 = (int)NewDouble((double)-0xC0000000);
            else
            _14610 = - _14609;
        }
        else {
            _14610 = unary_op(UMINUS, _14609);
        }
        _14609 = NOVALUE;
        if (IS_SEQUENCE(_direct_25368) && IS_ATOM(_14610)) {
            Ref(_14610);
            Append(&_direct_25368, _direct_25368, _14610);
        }
        else if (IS_ATOM(_direct_25368) && IS_SEQUENCE(_14610)) {
        }
        else {
            Concat((object_ptr)&_direct_25368, _direct_25368, _14610);
        }
        DeRef(_14610);
        _14610 = NOVALUE;
L4: 

        /** 			include_matrix[to_file][exports[i]] = or_bits( PUBLIC_INCLUDE, include_matrix[to_file][exports[i]] )*/
        _2 = (int)SEQ_PTR(_26include_matrix_11145);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26include_matrix_11145 = MAKE_SEQ(_2);
        }
        _3 = (int)(_to_file_25366 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_exports_25367);
        _14614 = (int)*(((s1_ptr)_2)->base + _i_25374);
        _2 = (int)SEQ_PTR(_26include_matrix_11145);
        _14615 = (int)*(((s1_ptr)_2)->base + _to_file_25366);
        _2 = (int)SEQ_PTR(_exports_25367);
        _14616 = (int)*(((s1_ptr)_2)->base + _i_25374);
        _2 = (int)SEQ_PTR(_14615);
        if (!IS_ATOM_INT(_14616)){
            _14617 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14616)->dbl));
        }
        else{
            _14617 = (int)*(((s1_ptr)_2)->base + _14616);
        }
        _14615 = NOVALUE;
        if (IS_ATOM_INT(_14617)) {
            {unsigned long tu;
                 tu = (unsigned long)4 | (unsigned long)_14617;
                 _14618 = MAKE_UINT(tu);
            }
        }
        else {
            _14618 = binary_op(OR_BITS, 4, _14617);
        }
        _14617 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_14614))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_14614)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _14614);
        _1 = *(int *)_2;
        *(int *)_2 = _14618;
        if( _1 != _14618 ){
            DeRef(_1);
        }
        _14618 = NOVALUE;
        _14612 = NOVALUE;
L3: 

        /** 	end for*/
        _i_25374 = _i_25374 + 1;
        goto L1; // [122] 37
L2: 
        ;
    }

    /** 	file_include[to_file] = direct*/
    RefDS(_direct_25368);
    _2 = (int)SEQ_PTR(_26file_include_11143);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26file_include_11143 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _to_file_25366);
    _1 = *(int *)_2;
    *(int *)_2 = _direct_25368;
    DeRef(_1);

    /** end procedure*/
    DeRef(_exports_25367);
    DeRefDS(_direct_25368);
    _14614 = NOVALUE;
    _14616 = NOVALUE;
    return;
    ;
}


void _60patch_exports(int _for_file_25401)
{
    int _export_len_25402 = NOVALUE;
    int _14629 = NOVALUE;
    int _14628 = NOVALUE;
    int _14626 = NOVALUE;
    int _14625 = NOVALUE;
    int _14624 = NOVALUE;
    int _14623 = NOVALUE;
    int _14621 = NOVALUE;
    int _14620 = NOVALUE;
    int _14619 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(file_include) do*/
    if (IS_SEQUENCE(_26file_include_11143)){
            _14619 = SEQ_PTR(_26file_include_11143)->length;
    }
    else {
        _14619 = 1;
    }
    {
        int _i_25404;
        _i_25404 = 1;
L1: 
        if (_i_25404 > _14619){
            goto L2; // [10] 99
        }

        /** 		if find( for_file, file_include[i] ) or find( -for_file, file_include[i] ) then*/
        _2 = (int)SEQ_PTR(_26file_include_11143);
        _14620 = (int)*(((s1_ptr)_2)->base + _i_25404);
        _14621 = find_from(_for_file_25401, _14620, 1);
        _14620 = NOVALUE;
        if (_14621 != 0) {
            goto L3; // [30] 53
        }
        if ((unsigned long)_for_file_25401 == 0xC0000000)
        _14623 = (int)NewDouble((double)-0xC0000000);
        else
        _14623 = - _for_file_25401;
        _2 = (int)SEQ_PTR(_26file_include_11143);
        _14624 = (int)*(((s1_ptr)_2)->base + _i_25404);
        _14625 = find_from(_14623, _14624, 1);
        DeRef(_14623);
        _14623 = NOVALUE;
        _14624 = NOVALUE;
        if (_14625 == 0)
        {
            _14625 = NOVALUE;
            goto L4; // [49] 92
        }
        else{
            _14625 = NOVALUE;
        }
L3: 

        /** 			export_len = length( file_include[i] )*/
        _2 = (int)SEQ_PTR(_26file_include_11143);
        _14626 = (int)*(((s1_ptr)_2)->base + _i_25404);
        if (IS_SEQUENCE(_14626)){
                _export_len_25402 = SEQ_PTR(_14626)->length;
        }
        else {
            _export_len_25402 = 1;
        }
        _14626 = NOVALUE;

        /** 			add_exports( for_file, i )*/
        _60add_exports(_for_file_25401, _i_25404);

        /** 			if length( file_include[i] ) != export_len then*/
        _2 = (int)SEQ_PTR(_26file_include_11143);
        _14628 = (int)*(((s1_ptr)_2)->base + _i_25404);
        if (IS_SEQUENCE(_14628)){
                _14629 = SEQ_PTR(_14628)->length;
        }
        else {
            _14629 = 1;
        }
        _14628 = NOVALUE;
        if (_14629 == _export_len_25402)
        goto L5; // [81] 91

        /** 				patch_exports( i )*/
        _60patch_exports(_i_25404);
L5: 
L4: 

        /** 	end for*/
        _i_25404 = _i_25404 + 1;
        goto L1; // [94] 17
L2: 
        ;
    }

    /** end procedure*/
    _14626 = NOVALUE;
    _14628 = NOVALUE;
    return;
    ;
}


void _60update_include_matrix(int _included_file_25426, int _from_file_25427)
{
    int _add_public_25437 = NOVALUE;
    int _px_25455 = NOVALUE;
    int _indirect_25514 = NOVALUE;
    int _mask_25517 = NOVALUE;
    int _ix_25528 = NOVALUE;
    int _indirect_file_25532 = NOVALUE;
    int _14705 = NOVALUE;
    int _14704 = NOVALUE;
    int _14702 = NOVALUE;
    int _14701 = NOVALUE;
    int _14700 = NOVALUE;
    int _14699 = NOVALUE;
    int _14698 = NOVALUE;
    int _14697 = NOVALUE;
    int _14696 = NOVALUE;
    int _14695 = NOVALUE;
    int _14694 = NOVALUE;
    int _14691 = NOVALUE;
    int _14689 = NOVALUE;
    int _14688 = NOVALUE;
    int _14687 = NOVALUE;
    int _14685 = NOVALUE;
    int _14683 = NOVALUE;
    int _14682 = NOVALUE;
    int _14680 = NOVALUE;
    int _14679 = NOVALUE;
    int _14678 = NOVALUE;
    int _14677 = NOVALUE;
    int _14676 = NOVALUE;
    int _14674 = NOVALUE;
    int _14673 = NOVALUE;
    int _14672 = NOVALUE;
    int _14671 = NOVALUE;
    int _14670 = NOVALUE;
    int _14669 = NOVALUE;
    int _14667 = NOVALUE;
    int _14666 = NOVALUE;
    int _14665 = NOVALUE;
    int _14663 = NOVALUE;
    int _14662 = NOVALUE;
    int _14661 = NOVALUE;
    int _14660 = NOVALUE;
    int _14659 = NOVALUE;
    int _14658 = NOVALUE;
    int _14657 = NOVALUE;
    int _14656 = NOVALUE;
    int _14655 = NOVALUE;
    int _14654 = NOVALUE;
    int _14653 = NOVALUE;
    int _14651 = NOVALUE;
    int _14650 = NOVALUE;
    int _14648 = NOVALUE;
    int _14646 = NOVALUE;
    int _14644 = NOVALUE;
    int _14643 = NOVALUE;
    int _14642 = NOVALUE;
    int _14641 = NOVALUE;
    int _14639 = NOVALUE;
    int _14638 = NOVALUE;
    int _14637 = NOVALUE;
    int _14635 = NOVALUE;
    int _14634 = NOVALUE;
    int _14633 = NOVALUE;
    int _14631 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	include_matrix[from_file][included_file] = or_bits( DIRECT_INCLUDE, include_matrix[from_file][included_file] )*/
    _2 = (int)SEQ_PTR(_26include_matrix_11145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26include_matrix_11145 = MAKE_SEQ(_2);
    }
    _3 = (int)(_from_file_25427 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26include_matrix_11145);
    _14633 = (int)*(((s1_ptr)_2)->base + _from_file_25427);
    _2 = (int)SEQ_PTR(_14633);
    _14634 = (int)*(((s1_ptr)_2)->base + _included_file_25426);
    _14633 = NOVALUE;
    if (IS_ATOM_INT(_14634)) {
        {unsigned long tu;
             tu = (unsigned long)2 | (unsigned long)_14634;
             _14635 = MAKE_UINT(tu);
        }
    }
    else {
        _14635 = binary_op(OR_BITS, 2, _14634);
    }
    _14634 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _included_file_25426);
    _1 = *(int *)_2;
    *(int *)_2 = _14635;
    if( _1 != _14635 ){
        DeRef(_1);
    }
    _14635 = NOVALUE;
    _14631 = NOVALUE;

    /** 	if public_include then*/
    if (_60public_include_24673 == 0)
    {
        goto L1; // [38] 339
    }
    else{
    }

    /** 		sequence add_public = file_include_by[from_file]*/
    DeRef(_add_public_25437);
    _2 = (int)SEQ_PTR(_26file_include_by_11153);
    _add_public_25437 = (int)*(((s1_ptr)_2)->base + _from_file_25427);
    Ref(_add_public_25437);

    /** 		for i = 1 to length( add_public ) do*/
    if (IS_SEQUENCE(_add_public_25437)){
            _14637 = SEQ_PTR(_add_public_25437)->length;
    }
    else {
        _14637 = 1;
    }
    {
        int _i_25441;
        _i_25441 = 1;
L2: 
        if (_i_25441 > _14637){
            goto L3; // [56] 107
        }

        /** 			include_matrix[add_public[i]][included_file] =*/
        _2 = (int)SEQ_PTR(_add_public_25437);
        _14638 = (int)*(((s1_ptr)_2)->base + _i_25441);
        _2 = (int)SEQ_PTR(_26include_matrix_11145);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26include_matrix_11145 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_14638))
        _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_14638)->dbl));
        else
        _3 = (int)(_14638 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_add_public_25437);
        _14641 = (int)*(((s1_ptr)_2)->base + _i_25441);
        _2 = (int)SEQ_PTR(_26include_matrix_11145);
        if (!IS_ATOM_INT(_14641)){
            _14642 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14641)->dbl));
        }
        else{
            _14642 = (int)*(((s1_ptr)_2)->base + _14641);
        }
        _2 = (int)SEQ_PTR(_14642);
        _14643 = (int)*(((s1_ptr)_2)->base + _included_file_25426);
        _14642 = NOVALUE;
        if (IS_ATOM_INT(_14643)) {
            {unsigned long tu;
                 tu = (unsigned long)4 | (unsigned long)_14643;
                 _14644 = MAKE_UINT(tu);
            }
        }
        else {
            _14644 = binary_op(OR_BITS, 4, _14643);
        }
        _14643 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _included_file_25426);
        _1 = *(int *)_2;
        *(int *)_2 = _14644;
        if( _1 != _14644 ){
            DeRef(_1);
        }
        _14644 = NOVALUE;
        _14639 = NOVALUE;

        /** 		end for*/
        _i_25441 = _i_25441 + 1;
        goto L2; // [102] 63
L3: 
        ;
    }

    /** 		add_public = file_public_by[from_file]*/
    DeRef(_add_public_25437);
    _2 = (int)SEQ_PTR(_26file_public_by_11155);
    _add_public_25437 = (int)*(((s1_ptr)_2)->base + _from_file_25427);
    Ref(_add_public_25437);

    /** 		integer px = length( add_public ) + 1*/
    if (IS_SEQUENCE(_add_public_25437)){
            _14646 = SEQ_PTR(_add_public_25437)->length;
    }
    else {
        _14646 = 1;
    }
    _px_25455 = _14646 + 1;
    _14646 = NOVALUE;

    /** 		while px <= length( add_public ) do*/
L4: 
    if (IS_SEQUENCE(_add_public_25437)){
            _14648 = SEQ_PTR(_add_public_25437)->length;
    }
    else {
        _14648 = 1;
    }
    if (_px_25455 > _14648)
    goto L5; // [134] 338

    /** 			include_matrix[add_public[px]][included_file] =*/
    _2 = (int)SEQ_PTR(_add_public_25437);
    _14650 = (int)*(((s1_ptr)_2)->base + _px_25455);
    _2 = (int)SEQ_PTR(_26include_matrix_11145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26include_matrix_11145 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_14650))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_14650)->dbl));
    else
    _3 = (int)(_14650 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_add_public_25437);
    _14653 = (int)*(((s1_ptr)_2)->base + _px_25455);
    _2 = (int)SEQ_PTR(_26include_matrix_11145);
    if (!IS_ATOM_INT(_14653)){
        _14654 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14653)->dbl));
    }
    else{
        _14654 = (int)*(((s1_ptr)_2)->base + _14653);
    }
    _2 = (int)SEQ_PTR(_14654);
    _14655 = (int)*(((s1_ptr)_2)->base + _included_file_25426);
    _14654 = NOVALUE;
    if (IS_ATOM_INT(_14655)) {
        {unsigned long tu;
             tu = (unsigned long)4 | (unsigned long)_14655;
             _14656 = MAKE_UINT(tu);
        }
    }
    else {
        _14656 = binary_op(OR_BITS, 4, _14655);
    }
    _14655 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _included_file_25426);
    _1 = *(int *)_2;
    *(int *)_2 = _14656;
    if( _1 != _14656 ){
        DeRef(_1);
    }
    _14656 = NOVALUE;
    _14651 = NOVALUE;

    /** 			for i = 1 to length( file_public_by[add_public[px]] ) do*/
    _2 = (int)SEQ_PTR(_add_public_25437);
    _14657 = (int)*(((s1_ptr)_2)->base + _px_25455);
    _2 = (int)SEQ_PTR(_26file_public_by_11155);
    if (!IS_ATOM_INT(_14657)){
        _14658 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14657)->dbl));
    }
    else{
        _14658 = (int)*(((s1_ptr)_2)->base + _14657);
    }
    if (IS_SEQUENCE(_14658)){
            _14659 = SEQ_PTR(_14658)->length;
    }
    else {
        _14659 = 1;
    }
    _14658 = NOVALUE;
    {
        int _i_25472;
        _i_25472 = 1;
L6: 
        if (_i_25472 > _14659){
            goto L7; // [190] 249
        }

        /** 				if not find( file_public[add_public[px]][i], add_public ) then*/
        _2 = (int)SEQ_PTR(_add_public_25437);
        _14660 = (int)*(((s1_ptr)_2)->base + _px_25455);
        _2 = (int)SEQ_PTR(_26file_public_11151);
        if (!IS_ATOM_INT(_14660)){
            _14661 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14660)->dbl));
        }
        else{
            _14661 = (int)*(((s1_ptr)_2)->base + _14660);
        }
        _2 = (int)SEQ_PTR(_14661);
        _14662 = (int)*(((s1_ptr)_2)->base + _i_25472);
        _14661 = NOVALUE;
        _14663 = find_from(_14662, _add_public_25437, 1);
        _14662 = NOVALUE;
        if (_14663 != 0)
        goto L8; // [218] 242
        _14663 = NOVALUE;

        /** 					add_public &= file_public[add_public[px]][i]*/
        _2 = (int)SEQ_PTR(_add_public_25437);
        _14665 = (int)*(((s1_ptr)_2)->base + _px_25455);
        _2 = (int)SEQ_PTR(_26file_public_11151);
        if (!IS_ATOM_INT(_14665)){
            _14666 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14665)->dbl));
        }
        else{
            _14666 = (int)*(((s1_ptr)_2)->base + _14665);
        }
        _2 = (int)SEQ_PTR(_14666);
        _14667 = (int)*(((s1_ptr)_2)->base + _i_25472);
        _14666 = NOVALUE;
        if (IS_SEQUENCE(_add_public_25437) && IS_ATOM(_14667)) {
            Ref(_14667);
            Append(&_add_public_25437, _add_public_25437, _14667);
        }
        else if (IS_ATOM(_add_public_25437) && IS_SEQUENCE(_14667)) {
        }
        else {
            Concat((object_ptr)&_add_public_25437, _add_public_25437, _14667);
        }
        _14667 = NOVALUE;
L8: 

        /** 			end for*/
        _i_25472 = _i_25472 + 1;
        goto L6; // [244] 197
L7: 
        ;
    }

    /** 			for i = 1 to length( file_include_by[add_public[px]] ) do*/
    _2 = (int)SEQ_PTR(_add_public_25437);
    _14669 = (int)*(((s1_ptr)_2)->base + _px_25455);
    _2 = (int)SEQ_PTR(_26file_include_by_11153);
    if (!IS_ATOM_INT(_14669)){
        _14670 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14669)->dbl));
    }
    else{
        _14670 = (int)*(((s1_ptr)_2)->base + _14669);
    }
    if (IS_SEQUENCE(_14670)){
            _14671 = SEQ_PTR(_14670)->length;
    }
    else {
        _14671 = 1;
    }
    _14670 = NOVALUE;
    {
        int _i_25490;
        _i_25490 = 1;
L9: 
        if (_i_25490 > _14671){
            goto LA; // [264] 327
        }

        /** 				include_matrix[file_include_by[add_public[px]]][included_file] =*/
        _2 = (int)SEQ_PTR(_add_public_25437);
        _14672 = (int)*(((s1_ptr)_2)->base + _px_25455);
        _2 = (int)SEQ_PTR(_26file_include_by_11153);
        if (!IS_ATOM_INT(_14672)){
            _14673 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14672)->dbl));
        }
        else{
            _14673 = (int)*(((s1_ptr)_2)->base + _14672);
        }
        _2 = (int)SEQ_PTR(_26include_matrix_11145);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26include_matrix_11145 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_14673))
        _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_14673)->dbl));
        else
        _3 = (int)(_14673 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_add_public_25437);
        _14676 = (int)*(((s1_ptr)_2)->base + _px_25455);
        _2 = (int)SEQ_PTR(_26file_include_by_11153);
        if (!IS_ATOM_INT(_14676)){
            _14677 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14676)->dbl));
        }
        else{
            _14677 = (int)*(((s1_ptr)_2)->base + _14676);
        }
        _2 = (int)SEQ_PTR(_26include_matrix_11145);
        if (!IS_ATOM_INT(_14677)){
            _14678 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14677)->dbl));
        }
        else{
            _14678 = (int)*(((s1_ptr)_2)->base + _14677);
        }
        _2 = (int)SEQ_PTR(_14678);
        _14679 = (int)*(((s1_ptr)_2)->base + _included_file_25426);
        _14678 = NOVALUE;
        if (IS_ATOM_INT(_14679)) {
            {unsigned long tu;
                 tu = (unsigned long)4 | (unsigned long)_14679;
                 _14680 = MAKE_UINT(tu);
            }
        }
        else {
            _14680 = binary_op(OR_BITS, 4, _14679);
        }
        _14679 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _included_file_25426);
        _1 = *(int *)_2;
        *(int *)_2 = _14680;
        if( _1 != _14680 ){
            DeRef(_1);
        }
        _14680 = NOVALUE;
        _14674 = NOVALUE;

        /** 			end for*/
        _i_25490 = _i_25490 + 1;
        goto L9; // [322] 271
LA: 
        ;
    }

    /** 			px += 1*/
    _px_25455 = _px_25455 + 1;

    /** 		end while*/
    goto L4; // [335] 131
L5: 
L1: 
    DeRef(_add_public_25437);
    _add_public_25437 = NOVALUE;

    /** 	if indirect_include[from_file][included_file] then*/
    _2 = (int)SEQ_PTR(_26indirect_include_11148);
    _14682 = (int)*(((s1_ptr)_2)->base + _from_file_25427);
    _2 = (int)SEQ_PTR(_14682);
    _14683 = (int)*(((s1_ptr)_2)->base + _included_file_25426);
    _14682 = NOVALUE;
    if (_14683 == 0) {
        _14683 = NOVALUE;
        goto LB; // [353] 545
    }
    else {
        if (!IS_ATOM_INT(_14683) && DBL_PTR(_14683)->dbl == 0.0){
            _14683 = NOVALUE;
            goto LB; // [353] 545
        }
        _14683 = NOVALUE;
    }
    _14683 = NOVALUE;

    /** 		sequence indirect = file_include_by[from_file]*/
    DeRef(_indirect_25514);
    _2 = (int)SEQ_PTR(_26file_include_by_11153);
    _indirect_25514 = (int)*(((s1_ptr)_2)->base + _from_file_25427);
    Ref(_indirect_25514);

    /** 		sequence mask = include_matrix[included_file] != 0*/
    _2 = (int)SEQ_PTR(_26include_matrix_11145);
    _14685 = (int)*(((s1_ptr)_2)->base + _included_file_25426);
    DeRef(_mask_25517);
    if (IS_ATOM_INT(_14685)) {
        _mask_25517 = (_14685 != 0);
    }
    else {
        _mask_25517 = binary_op(NOTEQ, _14685, 0);
    }
    _14685 = NOVALUE;

    /** 		include_matrix[from_file] = or_bits( include_matrix[from_file], mask )*/
    _2 = (int)SEQ_PTR(_26include_matrix_11145);
    _14687 = (int)*(((s1_ptr)_2)->base + _from_file_25427);
    _14688 = binary_op(OR_BITS, _14687, _mask_25517);
    _14687 = NOVALUE;
    _2 = (int)SEQ_PTR(_26include_matrix_11145);
    _2 = (int)(((s1_ptr)_2)->base + _from_file_25427);
    _1 = *(int *)_2;
    *(int *)_2 = _14688;
    if( _1 != _14688 ){
        DeRef(_1);
    }
    _14688 = NOVALUE;

    /** 		mask = include_matrix[from_file] != 0*/
    _2 = (int)SEQ_PTR(_26include_matrix_11145);
    _14689 = (int)*(((s1_ptr)_2)->base + _from_file_25427);
    DeRefDS(_mask_25517);
    if (IS_ATOM_INT(_14689)) {
        _mask_25517 = (_14689 != 0);
    }
    else {
        _mask_25517 = binary_op(NOTEQ, _14689, 0);
    }
    _14689 = NOVALUE;

    /** 		integer ix = 1*/
    _ix_25528 = 1;

    /** 		while ix <= length(indirect) do*/
LC: 
    if (IS_SEQUENCE(_indirect_25514)){
            _14691 = SEQ_PTR(_indirect_25514)->length;
    }
    else {
        _14691 = 1;
    }
    if (_ix_25528 > _14691)
    goto LD; // [425] 544

    /** 			integer indirect_file = indirect[ix]*/
    _2 = (int)SEQ_PTR(_indirect_25514);
    _indirect_file_25532 = (int)*(((s1_ptr)_2)->base + _ix_25528);
    if (!IS_ATOM_INT(_indirect_file_25532))
    _indirect_file_25532 = (long)DBL_PTR(_indirect_file_25532)->dbl;

    /** 			if indirect_include[indirect_file][included_file] then*/
    _2 = (int)SEQ_PTR(_26indirect_include_11148);
    _14694 = (int)*(((s1_ptr)_2)->base + _indirect_file_25532);
    _2 = (int)SEQ_PTR(_14694);
    _14695 = (int)*(((s1_ptr)_2)->base + _included_file_25426);
    _14694 = NOVALUE;
    if (_14695 == 0) {
        _14695 = NOVALUE;
        goto LE; // [447] 531
    }
    else {
        if (!IS_ATOM_INT(_14695) && DBL_PTR(_14695)->dbl == 0.0){
            _14695 = NOVALUE;
            goto LE; // [447] 531
        }
        _14695 = NOVALUE;
    }
    _14695 = NOVALUE;

    /** 				include_matrix[indirect_file] =*/
    _2 = (int)SEQ_PTR(_26include_matrix_11145);
    _14696 = (int)*(((s1_ptr)_2)->base + _indirect_file_25532);
    _14697 = binary_op(OR_BITS, _mask_25517, _14696);
    _14696 = NOVALUE;
    _2 = (int)SEQ_PTR(_26include_matrix_11145);
    _2 = (int)(((s1_ptr)_2)->base + _indirect_file_25532);
    _1 = *(int *)_2;
    *(int *)_2 = _14697;
    if( _1 != _14697 ){
        DeRef(_1);
    }
    _14697 = NOVALUE;

    /** 				for i = 1 to length( file_include_by[indirect_file] ) do*/
    _2 = (int)SEQ_PTR(_26file_include_by_11153);
    _14698 = (int)*(((s1_ptr)_2)->base + _indirect_file_25532);
    if (IS_SEQUENCE(_14698)){
            _14699 = SEQ_PTR(_14698)->length;
    }
    else {
        _14699 = 1;
    }
    _14698 = NOVALUE;
    {
        int _i_25543;
        _i_25543 = 1;
LF: 
        if (_i_25543 > _14699){
            goto L10; // [479] 530
        }

        /** 					if not find( file_include_by[indirect_file][i], indirect ) then*/
        _2 = (int)SEQ_PTR(_26file_include_by_11153);
        _14700 = (int)*(((s1_ptr)_2)->base + _indirect_file_25532);
        _2 = (int)SEQ_PTR(_14700);
        _14701 = (int)*(((s1_ptr)_2)->base + _i_25543);
        _14700 = NOVALUE;
        _14702 = find_from(_14701, _indirect_25514, 1);
        _14701 = NOVALUE;
        if (_14702 != 0)
        goto L11; // [503] 523
        _14702 = NOVALUE;

        /** 						indirect &= file_include_by[indirect_file][i]*/
        _2 = (int)SEQ_PTR(_26file_include_by_11153);
        _14704 = (int)*(((s1_ptr)_2)->base + _indirect_file_25532);
        _2 = (int)SEQ_PTR(_14704);
        _14705 = (int)*(((s1_ptr)_2)->base + _i_25543);
        _14704 = NOVALUE;
        if (IS_SEQUENCE(_indirect_25514) && IS_ATOM(_14705)) {
            Ref(_14705);
            Append(&_indirect_25514, _indirect_25514, _14705);
        }
        else if (IS_ATOM(_indirect_25514) && IS_SEQUENCE(_14705)) {
        }
        else {
            Concat((object_ptr)&_indirect_25514, _indirect_25514, _14705);
        }
        _14705 = NOVALUE;
L11: 

        /** 				end for*/
        _i_25543 = _i_25543 + 1;
        goto LF; // [525] 486
L10: 
        ;
    }
LE: 

    /** 			ix += 1*/
    _ix_25528 = _ix_25528 + 1;

    /** 		end while*/
    goto LC; // [541] 422
LD: 
LB: 
    DeRef(_indirect_25514);
    _indirect_25514 = NOVALUE;
    DeRef(_mask_25517);
    _mask_25517 = NOVALUE;

    /** 	public_include = FALSE*/
    _60public_include_24673 = _5FALSE_242;

    /** end procedure*/
    _14638 = NOVALUE;
    _14650 = NOVALUE;
    _14641 = NOVALUE;
    _14657 = NOVALUE;
    _14653 = NOVALUE;
    _14658 = NOVALUE;
    _14660 = NOVALUE;
    _14665 = NOVALUE;
    _14669 = NOVALUE;
    _14670 = NOVALUE;
    _14672 = NOVALUE;
    _14673 = NOVALUE;
    _14698 = NOVALUE;
    _14676 = NOVALUE;
    _14677 = NOVALUE;
    return;
    ;
}


void _60add_include_by(int _by_file_25561, int _included_file_25562, int _is_public_25563)
{
    int _14752 = NOVALUE;
    int _14751 = NOVALUE;
    int _14750 = NOVALUE;
    int _14748 = NOVALUE;
    int _14747 = NOVALUE;
    int _14746 = NOVALUE;
    int _14745 = NOVALUE;
    int _14743 = NOVALUE;
    int _14742 = NOVALUE;
    int _14741 = NOVALUE;
    int _14740 = NOVALUE;
    int _14739 = NOVALUE;
    int _14738 = NOVALUE;
    int _14737 = NOVALUE;
    int _14736 = NOVALUE;
    int _14734 = NOVALUE;
    int _14733 = NOVALUE;
    int _14732 = NOVALUE;
    int _14731 = NOVALUE;
    int _14729 = NOVALUE;
    int _14728 = NOVALUE;
    int _14727 = NOVALUE;
    int _14726 = NOVALUE;
    int _14724 = NOVALUE;
    int _14723 = NOVALUE;
    int _14722 = NOVALUE;
    int _14721 = NOVALUE;
    int _14719 = NOVALUE;
    int _14718 = NOVALUE;
    int _14717 = NOVALUE;
    int _14716 = NOVALUE;
    int _14715 = NOVALUE;
    int _14713 = NOVALUE;
    int _14712 = NOVALUE;
    int _14711 = NOVALUE;
    int _14710 = NOVALUE;
    int _14708 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	include_matrix[by_file][included_file] = or_bits( DIRECT_INCLUDE, include_matrix[by_file][included_file] )*/
    _2 = (int)SEQ_PTR(_26include_matrix_11145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26include_matrix_11145 = MAKE_SEQ(_2);
    }
    _3 = (int)(_by_file_25561 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26include_matrix_11145);
    _14710 = (int)*(((s1_ptr)_2)->base + _by_file_25561);
    _2 = (int)SEQ_PTR(_14710);
    _14711 = (int)*(((s1_ptr)_2)->base + _included_file_25562);
    _14710 = NOVALUE;
    if (IS_ATOM_INT(_14711)) {
        {unsigned long tu;
             tu = (unsigned long)2 | (unsigned long)_14711;
             _14712 = MAKE_UINT(tu);
        }
    }
    else {
        _14712 = binary_op(OR_BITS, 2, _14711);
    }
    _14711 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _included_file_25562);
    _1 = *(int *)_2;
    *(int *)_2 = _14712;
    if( _1 != _14712 ){
        DeRef(_1);
    }
    _14712 = NOVALUE;
    _14708 = NOVALUE;

    /** 	if is_public then*/
    if (_is_public_25563 == 0)
    {
        goto L1; // [38] 71
    }
    else{
    }

    /** 		include_matrix[by_file][included_file] = or_bits( PUBLIC_INCLUDE, include_matrix[by_file][included_file] )*/
    _2 = (int)SEQ_PTR(_26include_matrix_11145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26include_matrix_11145 = MAKE_SEQ(_2);
    }
    _3 = (int)(_by_file_25561 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_26include_matrix_11145);
    _14715 = (int)*(((s1_ptr)_2)->base + _by_file_25561);
    _2 = (int)SEQ_PTR(_14715);
    _14716 = (int)*(((s1_ptr)_2)->base + _included_file_25562);
    _14715 = NOVALUE;
    if (IS_ATOM_INT(_14716)) {
        {unsigned long tu;
             tu = (unsigned long)4 | (unsigned long)_14716;
             _14717 = MAKE_UINT(tu);
        }
    }
    else {
        _14717 = binary_op(OR_BITS, 4, _14716);
    }
    _14716 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _included_file_25562);
    _1 = *(int *)_2;
    *(int *)_2 = _14717;
    if( _1 != _14717 ){
        DeRef(_1);
    }
    _14717 = NOVALUE;
    _14713 = NOVALUE;
L1: 

    /** 	if not find( by_file, file_include_by[included_file] ) then*/
    _2 = (int)SEQ_PTR(_26file_include_by_11153);
    _14718 = (int)*(((s1_ptr)_2)->base + _included_file_25562);
    _14719 = find_from(_by_file_25561, _14718, 1);
    _14718 = NOVALUE;
    if (_14719 != 0)
    goto L2; // [84] 104
    _14719 = NOVALUE;

    /** 		file_include_by[included_file] &= by_file*/
    _2 = (int)SEQ_PTR(_26file_include_by_11153);
    _14721 = (int)*(((s1_ptr)_2)->base + _included_file_25562);
    if (IS_SEQUENCE(_14721) && IS_ATOM(_by_file_25561)) {
        Append(&_14722, _14721, _by_file_25561);
    }
    else if (IS_ATOM(_14721) && IS_SEQUENCE(_by_file_25561)) {
    }
    else {
        Concat((object_ptr)&_14722, _14721, _by_file_25561);
        _14721 = NOVALUE;
    }
    _14721 = NOVALUE;
    _2 = (int)SEQ_PTR(_26file_include_by_11153);
    _2 = (int)(((s1_ptr)_2)->base + _included_file_25562);
    _1 = *(int *)_2;
    *(int *)_2 = _14722;
    if( _1 != _14722 ){
        DeRef(_1);
    }
    _14722 = NOVALUE;
L2: 

    /** 	if not find( included_file, file_include[by_file] ) then*/
    _2 = (int)SEQ_PTR(_26file_include_11143);
    _14723 = (int)*(((s1_ptr)_2)->base + _by_file_25561);
    _14724 = find_from(_included_file_25562, _14723, 1);
    _14723 = NOVALUE;
    if (_14724 != 0)
    goto L3; // [117] 137
    _14724 = NOVALUE;

    /** 		file_include[by_file] &= included_file*/
    _2 = (int)SEQ_PTR(_26file_include_11143);
    _14726 = (int)*(((s1_ptr)_2)->base + _by_file_25561);
    if (IS_SEQUENCE(_14726) && IS_ATOM(_included_file_25562)) {
        Append(&_14727, _14726, _included_file_25562);
    }
    else if (IS_ATOM(_14726) && IS_SEQUENCE(_included_file_25562)) {
    }
    else {
        Concat((object_ptr)&_14727, _14726, _included_file_25562);
        _14726 = NOVALUE;
    }
    _14726 = NOVALUE;
    _2 = (int)SEQ_PTR(_26file_include_11143);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26file_include_11143 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _by_file_25561);
    _1 = *(int *)_2;
    *(int *)_2 = _14727;
    if( _1 != _14727 ){
        DeRef(_1);
    }
    _14727 = NOVALUE;
L3: 

    /** 	if is_public then*/
    if (_is_public_25563 == 0)
    {
        goto L4; // [139] 209
    }
    else{
    }

    /** 		if not find( by_file, file_public_by[included_file] ) then*/
    _2 = (int)SEQ_PTR(_26file_public_by_11155);
    _14728 = (int)*(((s1_ptr)_2)->base + _included_file_25562);
    _14729 = find_from(_by_file_25561, _14728, 1);
    _14728 = NOVALUE;
    if (_14729 != 0)
    goto L5; // [155] 175
    _14729 = NOVALUE;

    /** 			file_public_by[included_file] &= by_file*/
    _2 = (int)SEQ_PTR(_26file_public_by_11155);
    _14731 = (int)*(((s1_ptr)_2)->base + _included_file_25562);
    if (IS_SEQUENCE(_14731) && IS_ATOM(_by_file_25561)) {
        Append(&_14732, _14731, _by_file_25561);
    }
    else if (IS_ATOM(_14731) && IS_SEQUENCE(_by_file_25561)) {
    }
    else {
        Concat((object_ptr)&_14732, _14731, _by_file_25561);
        _14731 = NOVALUE;
    }
    _14731 = NOVALUE;
    _2 = (int)SEQ_PTR(_26file_public_by_11155);
    _2 = (int)(((s1_ptr)_2)->base + _included_file_25562);
    _1 = *(int *)_2;
    *(int *)_2 = _14732;
    if( _1 != _14732 ){
        DeRef(_1);
    }
    _14732 = NOVALUE;
L5: 

    /** 		if not find( included_file, file_public[by_file] ) then*/
    _2 = (int)SEQ_PTR(_26file_public_11151);
    _14733 = (int)*(((s1_ptr)_2)->base + _by_file_25561);
    _14734 = find_from(_included_file_25562, _14733, 1);
    _14733 = NOVALUE;
    if (_14734 != 0)
    goto L6; // [188] 208
    _14734 = NOVALUE;

    /** 			file_public[by_file] &= included_file*/
    _2 = (int)SEQ_PTR(_26file_public_11151);
    _14736 = (int)*(((s1_ptr)_2)->base + _by_file_25561);
    if (IS_SEQUENCE(_14736) && IS_ATOM(_included_file_25562)) {
        Append(&_14737, _14736, _included_file_25562);
    }
    else if (IS_ATOM(_14736) && IS_SEQUENCE(_included_file_25562)) {
    }
    else {
        Concat((object_ptr)&_14737, _14736, _included_file_25562);
        _14736 = NOVALUE;
    }
    _14736 = NOVALUE;
    _2 = (int)SEQ_PTR(_26file_public_11151);
    _2 = (int)(((s1_ptr)_2)->base + _by_file_25561);
    _1 = *(int *)_2;
    *(int *)_2 = _14737;
    if( _1 != _14737 ){
        DeRef(_1);
    }
    _14737 = NOVALUE;
L6: 
L4: 

    /** 	for propagate = 1 to length( include_matrix[included_file] ) do*/
    _2 = (int)SEQ_PTR(_26include_matrix_11145);
    _14738 = (int)*(((s1_ptr)_2)->base + _included_file_25562);
    if (IS_SEQUENCE(_14738)){
            _14739 = SEQ_PTR(_14738)->length;
    }
    else {
        _14739 = 1;
    }
    _14738 = NOVALUE;
    {
        int _propagate_25615;
        _propagate_25615 = 1;
L7: 
        if (_propagate_25615 > _14739){
            goto L8; // [220] 320
        }

        /** 		if and_bits( PUBLIC_INCLUDE, include_matrix[included_file][propagate] ) then*/
        _2 = (int)SEQ_PTR(_26include_matrix_11145);
        _14740 = (int)*(((s1_ptr)_2)->base + _included_file_25562);
        _2 = (int)SEQ_PTR(_14740);
        _14741 = (int)*(((s1_ptr)_2)->base + _propagate_25615);
        _14740 = NOVALUE;
        if (IS_ATOM_INT(_14741)) {
            {unsigned long tu;
                 tu = (unsigned long)4 & (unsigned long)_14741;
                 _14742 = MAKE_UINT(tu);
            }
        }
        else {
            _14742 = binary_op(AND_BITS, 4, _14741);
        }
        _14741 = NOVALUE;
        if (_14742 == 0) {
            DeRef(_14742);
            _14742 = NOVALUE;
            goto L9; // [245] 313
        }
        else {
            if (!IS_ATOM_INT(_14742) && DBL_PTR(_14742)->dbl == 0.0){
                DeRef(_14742);
                _14742 = NOVALUE;
                goto L9; // [245] 313
            }
            DeRef(_14742);
            _14742 = NOVALUE;
        }
        DeRef(_14742);
        _14742 = NOVALUE;

        /** 			include_matrix[by_file][propagate] = or_bits( DIRECT_INCLUDE, include_matrix[by_file][propagate] )*/
        _2 = (int)SEQ_PTR(_26include_matrix_11145);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26include_matrix_11145 = MAKE_SEQ(_2);
        }
        _3 = (int)(_by_file_25561 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_26include_matrix_11145);
        _14745 = (int)*(((s1_ptr)_2)->base + _by_file_25561);
        _2 = (int)SEQ_PTR(_14745);
        _14746 = (int)*(((s1_ptr)_2)->base + _propagate_25615);
        _14745 = NOVALUE;
        if (IS_ATOM_INT(_14746)) {
            {unsigned long tu;
                 tu = (unsigned long)2 | (unsigned long)_14746;
                 _14747 = MAKE_UINT(tu);
            }
        }
        else {
            _14747 = binary_op(OR_BITS, 2, _14746);
        }
        _14746 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _propagate_25615);
        _1 = *(int *)_2;
        *(int *)_2 = _14747;
        if( _1 != _14747 ){
            DeRef(_1);
        }
        _14747 = NOVALUE;
        _14743 = NOVALUE;

        /** 			if is_public then*/
        if (_is_public_25563 == 0)
        {
            goto LA; // [279] 312
        }
        else{
        }

        /** 				include_matrix[by_file][propagate] = or_bits( PUBLIC_INCLUDE, include_matrix[by_file][propagate] )*/
        _2 = (int)SEQ_PTR(_26include_matrix_11145);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26include_matrix_11145 = MAKE_SEQ(_2);
        }
        _3 = (int)(_by_file_25561 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_26include_matrix_11145);
        _14750 = (int)*(((s1_ptr)_2)->base + _by_file_25561);
        _2 = (int)SEQ_PTR(_14750);
        _14751 = (int)*(((s1_ptr)_2)->base + _propagate_25615);
        _14750 = NOVALUE;
        if (IS_ATOM_INT(_14751)) {
            {unsigned long tu;
                 tu = (unsigned long)4 | (unsigned long)_14751;
                 _14752 = MAKE_UINT(tu);
            }
        }
        else {
            _14752 = binary_op(OR_BITS, 4, _14751);
        }
        _14751 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _propagate_25615);
        _1 = *(int *)_2;
        *(int *)_2 = _14752;
        if( _1 != _14752 ){
            DeRef(_1);
        }
        _14752 = NOVALUE;
        _14748 = NOVALUE;
LA: 
L9: 

        /** 	end for*/
        _propagate_25615 = _propagate_25615 + 1;
        goto L7; // [315] 227
L8: 
        ;
    }

    /** end procedure*/
    _14738 = NOVALUE;
    return;
    ;
}


void _60IncludePush()
{
    int _new_file_handle_25644 = NOVALUE;
    int _old_file_no_25645 = NOVALUE;
    int _new_hash_25646 = NOVALUE;
    int _idx_25647 = NOVALUE;
    int _14839 = NOVALUE;
    int _14836 = NOVALUE;
    int _14834 = NOVALUE;
    int _14833 = NOVALUE;
    int _14832 = NOVALUE;
    int _14830 = NOVALUE;
    int _14829 = NOVALUE;
    int _14823 = NOVALUE;
    int _14822 = NOVALUE;
    int _14821 = NOVALUE;
    int _14820 = NOVALUE;
    int _14819 = NOVALUE;
    int _14818 = NOVALUE;
    int _14817 = NOVALUE;
    int _14814 = NOVALUE;
    int _14812 = NOVALUE;
    int _14810 = NOVALUE;
    int _14809 = NOVALUE;
    int _14808 = NOVALUE;
    int _14806 = NOVALUE;
    int _14805 = NOVALUE;
    int _14803 = NOVALUE;
    int _14802 = NOVALUE;
    int _14800 = NOVALUE;
    int _14799 = NOVALUE;
    int _14798 = NOVALUE;
    int _14797 = NOVALUE;
    int _14796 = NOVALUE;
    int _14795 = NOVALUE;
    int _14794 = NOVALUE;
    int _14790 = NOVALUE;
    int _14788 = NOVALUE;
    int _14787 = NOVALUE;
    int _14786 = NOVALUE;
    int _14785 = NOVALUE;
    int _14784 = NOVALUE;
    int _14783 = NOVALUE;
    int _14782 = NOVALUE;
    int _14781 = NOVALUE;
    int _14780 = NOVALUE;
    int _14778 = NOVALUE;
    int _14777 = NOVALUE;
    int _14776 = NOVALUE;
    int _14774 = NOVALUE;
    int _14773 = NOVALUE;
    int _14772 = NOVALUE;
    int _14771 = NOVALUE;
    int _14769 = NOVALUE;
    int _14768 = NOVALUE;
    int _14767 = NOVALUE;
    int _14766 = NOVALUE;
    int _14765 = NOVALUE;
    int _14763 = NOVALUE;
    int _14762 = NOVALUE;
    int _14761 = NOVALUE;
    int _14760 = NOVALUE;
    int _14758 = NOVALUE;
    int _14754 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	start_include = FALSE*/
    _60start_include_24670 = _5FALSE_242;

    /** 	new_file_handle = path_open() -- sets new_include_name to full path*/
    _new_file_handle_25644 = _60path_open();
    if (!IS_ATOM_INT(_new_file_handle_25644)) {
        _1 = (long)(DBL_PTR(_new_file_handle_25644)->dbl);
        if (UNIQUE(DBL_PTR(_new_file_handle_25644)) && (DBL_PTR(_new_file_handle_25644)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_new_file_handle_25644);
        _new_file_handle_25644 = _1;
    }

    /** 	new_hash = hash(canonical_path(new_include_name,,CORRECT), stdhash:HSIEH32)*/
    RefDS(_25new_include_name_12405);
    _14754 = _9canonical_path(_25new_include_name_12405, 0, 2);
    DeRef(_new_hash_25646);
    _new_hash_25646 = calc_hash(_14754, -5);
    DeRef(_14754);
    _14754 = NOVALUE;

    /** 	idx = find(new_hash, known_files_hash)*/
    _idx_25647 = find_from(_new_hash_25646, _26known_files_hash_11140, 1);

    /** 	if idx then*/
    if (_idx_25647 == 0)
    {
        goto L1; // [44] 337
    }
    else{
    }

    /** 		if new_include_space != 0 then*/
    if (_60new_include_space_24668 == 0)
    goto L2; // [51] 73

    /** 			SymTab[new_include_space][S_OBJ] = idx -- but note any namespace*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_60new_include_space_24668 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _idx_25647;
    DeRef(_1);
    _14758 = NOVALUE;
L2: 

    /** 		close(new_file_handle)*/
    EClose(_new_file_handle_25644);

    /** 		if find( -idx, file_include[current_file_no] ) then*/
    if ((unsigned long)_idx_25647 == 0xC0000000)
    _14760 = (int)NewDouble((double)-0xC0000000);
    else
    _14760 = - _idx_25647;
    _2 = (int)SEQ_PTR(_26file_include_11143);
    _14761 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    _14762 = find_from(_14760, _14761, 1);
    DeRef(_14760);
    _14760 = NOVALUE;
    _14761 = NOVALUE;
    if (_14762 == 0)
    {
        _14762 = NOVALUE;
        goto L3; // [95] 132
    }
    else{
        _14762 = NOVALUE;
    }

    /** 			file_include[current_file_no][ find( -idx, file_include[current_file_no] ) ] = idx*/
    _2 = (int)SEQ_PTR(_26file_include_11143);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26file_include_11143 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25current_file_no_12262 + ((s1_ptr)_2)->base);
    if ((unsigned long)_idx_25647 == 0xC0000000)
    _14765 = (int)NewDouble((double)-0xC0000000);
    else
    _14765 = - _idx_25647;
    _2 = (int)SEQ_PTR(_26file_include_11143);
    _14766 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    _14767 = find_from(_14765, _14766, 1);
    DeRef(_14765);
    _14765 = NOVALUE;
    _14766 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _14767);
    _1 = *(int *)_2;
    *(int *)_2 = _idx_25647;
    DeRef(_1);
    _14763 = NOVALUE;
    goto L4; // [129] 230
L3: 

    /** 		elsif not find( idx, file_include[current_file_no] ) then*/
    _2 = (int)SEQ_PTR(_26file_include_11143);
    _14768 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    _14769 = find_from(_idx_25647, _14768, 1);
    _14768 = NOVALUE;
    if (_14769 != 0)
    goto L5; // [147] 229
    _14769 = NOVALUE;

    /** 			file_include[current_file_no] &= idx*/
    _2 = (int)SEQ_PTR(_26file_include_11143);
    _14771 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    if (IS_SEQUENCE(_14771) && IS_ATOM(_idx_25647)) {
        Append(&_14772, _14771, _idx_25647);
    }
    else if (IS_ATOM(_14771) && IS_SEQUENCE(_idx_25647)) {
    }
    else {
        Concat((object_ptr)&_14772, _14771, _idx_25647);
        _14771 = NOVALUE;
    }
    _14771 = NOVALUE;
    _2 = (int)SEQ_PTR(_26file_include_11143);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26file_include_11143 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _25current_file_no_12262);
    _1 = *(int *)_2;
    *(int *)_2 = _14772;
    if( _1 != _14772 ){
        DeRef(_1);
    }
    _14772 = NOVALUE;

    /** 			add_exports( idx, current_file_no )*/
    _60add_exports(_idx_25647, _25current_file_no_12262);

    /** 			if public_include then*/
    if (_60public_include_24673 == 0)
    {
        goto L6; // [180] 228
    }
    else{
    }

    /** 				if not find( idx, file_public[current_file_no] ) then*/
    _2 = (int)SEQ_PTR(_26file_public_11151);
    _14773 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    _14774 = find_from(_idx_25647, _14773, 1);
    _14773 = NOVALUE;
    if (_14774 != 0)
    goto L7; // [198] 227
    _14774 = NOVALUE;

    /** 					file_public[current_file_no] &= idx*/
    _2 = (int)SEQ_PTR(_26file_public_11151);
    _14776 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    if (IS_SEQUENCE(_14776) && IS_ATOM(_idx_25647)) {
        Append(&_14777, _14776, _idx_25647);
    }
    else if (IS_ATOM(_14776) && IS_SEQUENCE(_idx_25647)) {
    }
    else {
        Concat((object_ptr)&_14777, _14776, _idx_25647);
        _14776 = NOVALUE;
    }
    _14776 = NOVALUE;
    _2 = (int)SEQ_PTR(_26file_public_11151);
    _2 = (int)(((s1_ptr)_2)->base + _25current_file_no_12262);
    _1 = *(int *)_2;
    *(int *)_2 = _14777;
    if( _1 != _14777 ){
        DeRef(_1);
    }
    _14777 = NOVALUE;

    /** 					patch_exports( current_file_no )*/
    _60patch_exports(_25current_file_no_12262);
L7: 
L6: 
L5: 
L4: 

    /** 		indirect_include[current_file_no][idx] = OpIndirectInclude*/
    _2 = (int)SEQ_PTR(_26indirect_include_11148);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26indirect_include_11148 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25current_file_no_12262 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _idx_25647);
    _1 = *(int *)_2;
    *(int *)_2 = _25OpIndirectInclude_12341;
    DeRef(_1);
    _14778 = NOVALUE;

    /** 		add_include_by( current_file_no, idx, public_include )*/
    _60add_include_by(_25current_file_no_12262, _idx_25647, _60public_include_24673);

    /** 		update_include_matrix( idx, current_file_no )*/
    _60update_include_matrix(_idx_25647, _25current_file_no_12262);

    /** 		public_include = FALSE*/
    _60public_include_24673 = _5FALSE_242;

    /** 		read_line() -- we can't return without reading a line first*/
    _60read_line();

    /** 		if not find( idx, file_include_depend[current_file_no] ) and not finished_files[idx] then*/
    _2 = (int)SEQ_PTR(_26file_include_depend_11142);
    _14780 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    _14781 = find_from(_idx_25647, _14780, 1);
    _14780 = NOVALUE;
    _14782 = (_14781 == 0);
    _14781 = NOVALUE;
    if (_14782 == 0) {
        goto L8; // [295] 331
    }
    _2 = (int)SEQ_PTR(_26finished_files_11141);
    _14784 = (int)*(((s1_ptr)_2)->base + _idx_25647);
    _14785 = (_14784 == 0);
    _14784 = NOVALUE;
    if (_14785 == 0)
    {
        DeRef(_14785);
        _14785 = NOVALUE;
        goto L8; // [309] 331
    }
    else{
        DeRef(_14785);
        _14785 = NOVALUE;
    }

    /** 			file_include_depend[current_file_no] &= idx*/
    _2 = (int)SEQ_PTR(_26file_include_depend_11142);
    _14786 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    if (IS_SEQUENCE(_14786) && IS_ATOM(_idx_25647)) {
        Append(&_14787, _14786, _idx_25647);
    }
    else if (IS_ATOM(_14786) && IS_SEQUENCE(_idx_25647)) {
    }
    else {
        Concat((object_ptr)&_14787, _14786, _idx_25647);
        _14786 = NOVALUE;
    }
    _14786 = NOVALUE;
    _2 = (int)SEQ_PTR(_26file_include_depend_11142);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26file_include_depend_11142 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _25current_file_no_12262);
    _1 = *(int *)_2;
    *(int *)_2 = _14787;
    if( _1 != _14787 ){
        DeRef(_1);
    }
    _14787 = NOVALUE;
L8: 

    /** 		return -- ignore it*/
    DeRef(_new_hash_25646);
    DeRef(_14782);
    _14782 = NOVALUE;
    return;
L1: 

    /** 	if length(IncludeStk) >= INCLUDE_LIMIT then*/
    if (IS_SEQUENCE(_60IncludeStk_24679)){
            _14788 = SEQ_PTR(_60IncludeStk_24679)->length;
    }
    else {
        _14788 = 1;
    }
    if (_14788 < 30)
    goto L9; // [344] 356

    /** 		CompileErr(104)*/
    RefDS(_22682);
    _43CompileErr(104, _22682, 0);
L9: 

    /** 	IncludeStk = append(IncludeStk,*/
    _1 = NewS1(22);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _25current_file_no_12262;
    *((int *)(_2+8)) = _25line_number_12263;
    *((int *)(_2+12)) = _25src_file_12404;
    *((int *)(_2+16)) = _25file_start_sym_12268;
    *((int *)(_2+20)) = _25OpWarning_12330;
    *((int *)(_2+24)) = _25OpTrace_12332;
    *((int *)(_2+28)) = _25OpTypeCheck_12333;
    *((int *)(_2+32)) = _25OpProfileTime_12335;
    *((int *)(_2+36)) = _25OpProfileStatement_12334;
    RefDS(_25OpDefines_12336);
    *((int *)(_2+40)) = _25OpDefines_12336;
    *((int *)(_2+44)) = _25prev_OpWarning_12331;
    *((int *)(_2+48)) = _25OpInline_12340;
    *((int *)(_2+52)) = _25OpIndirectInclude_12341;
    *((int *)(_2+56)) = _25putback_fwd_line_number_12265;
    Ref(_43putback_ForwardLine_49534);
    *((int *)(_2+60)) = _43putback_ForwardLine_49534;
    *((int *)(_2+64)) = _43putback_forward_bp_49538;
    *((int *)(_2+68)) = _25last_fwd_line_number_12266;
    Ref(_43last_ForwardLine_49535);
    *((int *)(_2+72)) = _43last_ForwardLine_49535;
    *((int *)(_2+76)) = _43last_forward_bp_49539;
    Ref(_43ThisLine_49532);
    *((int *)(_2+80)) = _43ThisLine_49532;
    *((int *)(_2+84)) = _25fwd_line_number_12264;
    *((int *)(_2+88)) = _43forward_bp_49537;
    _14790 = MAKE_SEQ(_1);
    RefDS(_14790);
    Append(&_60IncludeStk_24679, _60IncludeStk_24679, _14790);
    DeRefDS(_14790);
    _14790 = NOVALUE;

    /** 	file_include = append( file_include, {} )*/
    RefDS(_5);
    Append(&_26file_include_11143, _26file_include_11143, _5);

    /** 	file_include_by = append( file_include_by, {} )*/
    RefDS(_5);
    Append(&_26file_include_by_11153, _26file_include_by_11153, _5);

    /** 	for i = 1 to length( include_matrix) do*/
    if (IS_SEQUENCE(_26include_matrix_11145)){
            _14794 = SEQ_PTR(_26include_matrix_11145)->length;
    }
    else {
        _14794 = 1;
    }
    {
        int _i_25759;
        _i_25759 = 1;
LA: 
        if (_i_25759 > _14794){
            goto LB; // [460] 506
        }

        /** 		include_matrix[i]   &= 0*/
        _2 = (int)SEQ_PTR(_26include_matrix_11145);
        _14795 = (int)*(((s1_ptr)_2)->base + _i_25759);
        if (IS_SEQUENCE(_14795) && IS_ATOM(0)) {
            Append(&_14796, _14795, 0);
        }
        else if (IS_ATOM(_14795) && IS_SEQUENCE(0)) {
        }
        else {
            Concat((object_ptr)&_14796, _14795, 0);
            _14795 = NOVALUE;
        }
        _14795 = NOVALUE;
        _2 = (int)SEQ_PTR(_26include_matrix_11145);
        _2 = (int)(((s1_ptr)_2)->base + _i_25759);
        _1 = *(int *)_2;
        *(int *)_2 = _14796;
        if( _1 != _14796 ){
            DeRef(_1);
        }
        _14796 = NOVALUE;

        /** 		indirect_include[i] &= 0*/
        _2 = (int)SEQ_PTR(_26indirect_include_11148);
        _14797 = (int)*(((s1_ptr)_2)->base + _i_25759);
        if (IS_SEQUENCE(_14797) && IS_ATOM(0)) {
            Append(&_14798, _14797, 0);
        }
        else if (IS_ATOM(_14797) && IS_SEQUENCE(0)) {
        }
        else {
            Concat((object_ptr)&_14798, _14797, 0);
            _14797 = NOVALUE;
        }
        _14797 = NOVALUE;
        _2 = (int)SEQ_PTR(_26indirect_include_11148);
        _2 = (int)(((s1_ptr)_2)->base + _i_25759);
        _1 = *(int *)_2;
        *(int *)_2 = _14798;
        if( _1 != _14798 ){
            DeRef(_1);
        }
        _14798 = NOVALUE;

        /** 	end for*/
        _i_25759 = _i_25759 + 1;
        goto LA; // [501] 467
LB: 
        ;
    }

    /** 	include_matrix = append( include_matrix, repeat( 0, length( file_include ) ) )*/
    if (IS_SEQUENCE(_26file_include_11143)){
            _14799 = SEQ_PTR(_26file_include_11143)->length;
    }
    else {
        _14799 = 1;
    }
    _14800 = Repeat(0, _14799);
    _14799 = NOVALUE;
    RefDS(_14800);
    Append(&_26include_matrix_11145, _26include_matrix_11145, _14800);
    DeRefDS(_14800);
    _14800 = NOVALUE;

    /** 	include_matrix[$][$] = DIRECT_INCLUDE*/
    if (IS_SEQUENCE(_26include_matrix_11145)){
            _14802 = SEQ_PTR(_26include_matrix_11145)->length;
    }
    else {
        _14802 = 1;
    }
    _2 = (int)SEQ_PTR(_26include_matrix_11145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26include_matrix_11145 = MAKE_SEQ(_2);
    }
    _3 = (int)(_14802 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(*(object_ptr)_3)){
            _14805 = SEQ_PTR(*(object_ptr)_3)->length;
    }
    else {
        _14805 = 1;
    }
    _14803 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _14805);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _14803 = NOVALUE;

    /** 	include_matrix[current_file_no][$] = DIRECT_INCLUDE*/
    _2 = (int)SEQ_PTR(_26include_matrix_11145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26include_matrix_11145 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25current_file_no_12262 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(*(object_ptr)_3)){
            _14808 = SEQ_PTR(*(object_ptr)_3)->length;
    }
    else {
        _14808 = 1;
    }
    _14806 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _14808);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _14806 = NOVALUE;

    /** 	indirect_include = append( indirect_include, repeat( 0, length( file_include ) ) )*/
    if (IS_SEQUENCE(_26file_include_11143)){
            _14809 = SEQ_PTR(_26file_include_11143)->length;
    }
    else {
        _14809 = 1;
    }
    _14810 = Repeat(0, _14809);
    _14809 = NOVALUE;
    RefDS(_14810);
    Append(&_26indirect_include_11148, _26indirect_include_11148, _14810);
    DeRefDS(_14810);
    _14810 = NOVALUE;

    /** 	indirect_include[current_file_no][$] = OpIndirectInclude*/
    _2 = (int)SEQ_PTR(_26indirect_include_11148);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26indirect_include_11148 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25current_file_no_12262 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(*(object_ptr)_3)){
            _14814 = SEQ_PTR(*(object_ptr)_3)->length;
    }
    else {
        _14814 = 1;
    }
    _14812 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _14814);
    _1 = *(int *)_2;
    *(int *)_2 = _25OpIndirectInclude_12341;
    DeRef(_1);
    _14812 = NOVALUE;

    /** 	OpIndirectInclude = 1*/
    _25OpIndirectInclude_12341 = 1;

    /** 	file_public  = append( file_public, {} )*/
    RefDS(_5);
    Append(&_26file_public_11151, _26file_public_11151, _5);

    /** 	file_public_by = append( file_public_by, {} )*/
    RefDS(_5);
    Append(&_26file_public_by_11155, _26file_public_by_11155, _5);

    /** 	file_include[current_file_no] &= length( file_include )*/
    if (IS_SEQUENCE(_26file_include_11143)){
            _14817 = SEQ_PTR(_26file_include_11143)->length;
    }
    else {
        _14817 = 1;
    }
    _2 = (int)SEQ_PTR(_26file_include_11143);
    _14818 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    if (IS_SEQUENCE(_14818) && IS_ATOM(_14817)) {
        Append(&_14819, _14818, _14817);
    }
    else if (IS_ATOM(_14818) && IS_SEQUENCE(_14817)) {
    }
    else {
        Concat((object_ptr)&_14819, _14818, _14817);
        _14818 = NOVALUE;
    }
    _14818 = NOVALUE;
    _14817 = NOVALUE;
    _2 = (int)SEQ_PTR(_26file_include_11143);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26file_include_11143 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _25current_file_no_12262);
    _1 = *(int *)_2;
    *(int *)_2 = _14819;
    if( _1 != _14819 ){
        DeRef(_1);
    }
    _14819 = NOVALUE;

    /** 	add_include_by( current_file_no, length(file_include), public_include )*/
    if (IS_SEQUENCE(_26file_include_11143)){
            _14820 = SEQ_PTR(_26file_include_11143)->length;
    }
    else {
        _14820 = 1;
    }
    _60add_include_by(_25current_file_no_12262, _14820, _60public_include_24673);
    _14820 = NOVALUE;

    /** 	if public_include then*/
    if (_60public_include_24673 == 0)
    {
        goto LC; // [675] 709
    }
    else{
    }

    /** 		file_public[current_file_no] &= length( file_public )*/
    if (IS_SEQUENCE(_26file_public_11151)){
            _14821 = SEQ_PTR(_26file_public_11151)->length;
    }
    else {
        _14821 = 1;
    }
    _2 = (int)SEQ_PTR(_26file_public_11151);
    _14822 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    if (IS_SEQUENCE(_14822) && IS_ATOM(_14821)) {
        Append(&_14823, _14822, _14821);
    }
    else if (IS_ATOM(_14822) && IS_SEQUENCE(_14821)) {
    }
    else {
        Concat((object_ptr)&_14823, _14822, _14821);
        _14822 = NOVALUE;
    }
    _14822 = NOVALUE;
    _14821 = NOVALUE;
    _2 = (int)SEQ_PTR(_26file_public_11151);
    _2 = (int)(((s1_ptr)_2)->base + _25current_file_no_12262);
    _1 = *(int *)_2;
    *(int *)_2 = _14823;
    if( _1 != _14823 ){
        DeRef(_1);
    }
    _14823 = NOVALUE;

    /** 		patch_exports( current_file_no )*/
    _60patch_exports(_25current_file_no_12262);
LC: 

    /** ifdef STDDEBUG then*/

    /** 	src_file = new_file_handle*/
    _25src_file_12404 = _new_file_handle_25644;

    /** 	file_start_sym = last_sym*/
    _25file_start_sym_12268 = _52last_sym_47110;

    /** 	if current_file_no >= MAX_FILE then*/
    if (_25current_file_no_12262 < 256)
    goto LD; // [731] 743

    /** 		CompileErr(126)*/
    RefDS(_22682);
    _43CompileErr(126, _22682, 0);
LD: 

    /** 	known_files = append(known_files, new_include_name)*/
    RefDS(_25new_include_name_12405);
    Append(&_26known_files_11139, _26known_files_11139, _25new_include_name_12405);

    /** 	known_files_hash &= new_hash*/
    Ref(_new_hash_25646);
    Append(&_26known_files_hash_11140, _26known_files_hash_11140, _new_hash_25646);

    /** 	finished_files &= 0*/
    Append(&_26finished_files_11141, _26finished_files_11141, 0);

    /** 	file_include_depend = append( file_include_depend, { length( known_files ) } )*/
    if (IS_SEQUENCE(_26known_files_11139)){
            _14829 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _14829 = 1;
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _14829;
    _14830 = MAKE_SEQ(_1);
    _14829 = NOVALUE;
    RefDS(_14830);
    Append(&_26file_include_depend_11142, _26file_include_depend_11142, _14830);
    DeRefDS(_14830);
    _14830 = NOVALUE;

    /** 	file_include_depend[current_file_no] &= length( known_files )*/
    if (IS_SEQUENCE(_26known_files_11139)){
            _14832 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _14832 = 1;
    }
    _2 = (int)SEQ_PTR(_26file_include_depend_11142);
    _14833 = (int)*(((s1_ptr)_2)->base + _25current_file_no_12262);
    if (IS_SEQUENCE(_14833) && IS_ATOM(_14832)) {
        Append(&_14834, _14833, _14832);
    }
    else if (IS_ATOM(_14833) && IS_SEQUENCE(_14832)) {
    }
    else {
        Concat((object_ptr)&_14834, _14833, _14832);
        _14833 = NOVALUE;
    }
    _14833 = NOVALUE;
    _14832 = NOVALUE;
    _2 = (int)SEQ_PTR(_26file_include_depend_11142);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26file_include_depend_11142 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _25current_file_no_12262);
    _1 = *(int *)_2;
    *(int *)_2 = _14834;
    if( _1 != _14834 ){
        DeRef(_1);
    }
    _14834 = NOVALUE;

    /** 	check_coverage()*/
    _49check_coverage();

    /** 	default_namespaces &= 0*/
    Append(&_60default_namespaces_24676, _60default_namespaces_24676, 0);

    /** 	update_include_matrix( length( file_include ), current_file_no )*/
    if (IS_SEQUENCE(_26file_include_11143)){
            _14836 = SEQ_PTR(_26file_include_11143)->length;
    }
    else {
        _14836 = 1;
    }
    _60update_include_matrix(_14836, _25current_file_no_12262);
    _14836 = NOVALUE;

    /** 	old_file_no = current_file_no*/
    _old_file_no_25645 = _25current_file_no_12262;

    /** 	current_file_no = length(known_files)*/
    if (IS_SEQUENCE(_26known_files_11139)){
            _25current_file_no_12262 = SEQ_PTR(_26known_files_11139)->length;
    }
    else {
        _25current_file_no_12262 = 1;
    }

    /** 	line_number = 0*/
    _25line_number_12263 = 0;

    /** 	read_line()*/
    _60read_line();

    /** 	if new_include_space != 0 then*/
    if (_60new_include_space_24668 == 0)
    goto LE; // [875] 899

    /** 		SymTab[new_include_space][S_OBJ] = current_file_no*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_60new_include_space_24668 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _25current_file_no_12262;
    DeRef(_1);
    _14839 = NOVALUE;
LE: 

    /** 	default_namespace( )*/
    _60default_namespace();

    /** end procedure*/
    DeRef(_new_hash_25646);
    DeRef(_14782);
    _14782 = NOVALUE;
    return;
    ;
}


void _60update_include_completion(int _file_no_25869)
{
    int _fx_25878 = NOVALUE;
    int _14849 = NOVALUE;
    int _14848 = NOVALUE;
    int _14847 = NOVALUE;
    int _14846 = NOVALUE;
    int _14844 = NOVALUE;
    int _14843 = NOVALUE;
    int _14842 = NOVALUE;
    int _14841 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length( file_include_depend ) do*/
    if (IS_SEQUENCE(_26file_include_depend_11142)){
            _14841 = SEQ_PTR(_26file_include_depend_11142)->length;
    }
    else {
        _14841 = 1;
    }
    {
        int _i_25871;
        _i_25871 = 1;
L1: 
        if (_i_25871 > _14841){
            goto L2; // [10] 114
        }

        /** 		if length( file_include_depend[i] ) then*/
        _2 = (int)SEQ_PTR(_26file_include_depend_11142);
        _14842 = (int)*(((s1_ptr)_2)->base + _i_25871);
        if (IS_SEQUENCE(_14842)){
                _14843 = SEQ_PTR(_14842)->length;
        }
        else {
            _14843 = 1;
        }
        _14842 = NOVALUE;
        if (_14843 == 0)
        {
            _14843 = NOVALUE;
            goto L3; // [28] 105
        }
        else{
            _14843 = NOVALUE;
        }

        /** 			integer fx = find( file_no, file_include_depend[i] )*/
        _2 = (int)SEQ_PTR(_26file_include_depend_11142);
        _14844 = (int)*(((s1_ptr)_2)->base + _i_25871);
        _fx_25878 = find_from(_file_no_25869, _14844, 1);
        _14844 = NOVALUE;

        /** 			if fx then*/
        if (_fx_25878 == 0)
        {
            goto L4; // [46] 104
        }
        else{
        }

        /** 				file_include_depend[i] = remove( file_include_depend[i], fx )*/
        _2 = (int)SEQ_PTR(_26file_include_depend_11142);
        _14846 = (int)*(((s1_ptr)_2)->base + _i_25871);
        {
            s1_ptr assign_space = SEQ_PTR(_14846);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_fx_25878)) ? _fx_25878 : (long)(DBL_PTR(_fx_25878)->dbl);
            int stop = (IS_ATOM_INT(_fx_25878)) ? _fx_25878 : (long)(DBL_PTR(_fx_25878)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
                RefDS(_14846);
                DeRef(_14847);
                _14847 = _14846;
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_14846), start, &_14847 );
                }
                else Tail(SEQ_PTR(_14846), stop+1, &_14847);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_14846), start, &_14847);
            }
            else {
                assign_slice_seq = &assign_space;
                _1 = Remove_elements(start, stop, 0);
                DeRef(_14847);
                _14847 = _1;
            }
        }
        _14846 = NOVALUE;
        _2 = (int)SEQ_PTR(_26file_include_depend_11142);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26file_include_depend_11142 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_25871);
        _1 = *(int *)_2;
        *(int *)_2 = _14847;
        if( _1 != _14847 ){
            DeRef(_1);
        }
        _14847 = NOVALUE;

        /** 				if not length( file_include_depend[i] ) then*/
        _2 = (int)SEQ_PTR(_26file_include_depend_11142);
        _14848 = (int)*(((s1_ptr)_2)->base + _i_25871);
        if (IS_SEQUENCE(_14848)){
                _14849 = SEQ_PTR(_14848)->length;
        }
        else {
            _14849 = 1;
        }
        _14848 = NOVALUE;
        if (_14849 != 0)
        goto L5; // [79] 103
        _14849 = NOVALUE;

        /** 					finished_files[i] = 1*/
        _2 = (int)SEQ_PTR(_26finished_files_11141);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _26finished_files_11141 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_25871);
        *(int *)_2 = 1;

        /** 					if i != file_no then*/
        if (_i_25871 == _file_no_25869)
        goto L6; // [92] 102

        /** 						update_include_completion( i )*/
        _60update_include_completion(_i_25871);
L6: 
L5: 
L4: 
L3: 

        /** 	end for*/
        _i_25871 = _i_25871 + 1;
        goto L1; // [109] 17
L2: 
        ;
    }

    /** end procedure*/
    _14842 = NOVALUE;
    _14848 = NOVALUE;
    return;
    ;
}


int _60IncludePop()
{
    int _top_25909 = NOVALUE;
    int _14880 = NOVALUE;
    int _14878 = NOVALUE;
    int _14877 = NOVALUE;
    int _14855 = NOVALUE;
    int _14853 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	update_include_completion( current_file_no )*/
    _60update_include_completion(_25current_file_no_12262);

    /** 	Resolve_forward_references()*/
    _29Resolve_forward_references(0);

    /** 	HideLocals()*/
    _52HideLocals();

    /** 	if src_file >= 0 then*/
    if (_25src_file_12404 < 0)
    goto L1; // [21] 39

    /** 		close(src_file)*/
    EClose(_25src_file_12404);

    /** 		src_file = -1*/
    _25src_file_12404 = -1;
L1: 

    /** 	if length(IncludeStk) = 0 then*/
    if (IS_SEQUENCE(_60IncludeStk_24679)){
            _14853 = SEQ_PTR(_60IncludeStk_24679)->length;
    }
    else {
        _14853 = 1;
    }
    if (_14853 != 0)
    goto L2; // [46] 59

    /** 		return FALSE  -- the end*/
    DeRef(_top_25909);
    return _5FALSE_242;
L2: 

    /** 	sequence top = IncludeStk[$]*/
    if (IS_SEQUENCE(_60IncludeStk_24679)){
            _14855 = SEQ_PTR(_60IncludeStk_24679)->length;
    }
    else {
        _14855 = 1;
    }
    DeRef(_top_25909);
    _2 = (int)SEQ_PTR(_60IncludeStk_24679);
    _top_25909 = (int)*(((s1_ptr)_2)->base + _14855);
    RefDS(_top_25909);

    /** 	current_file_no    = top[FILE_NO]*/
    _2 = (int)SEQ_PTR(_top_25909);
    _25current_file_no_12262 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_25current_file_no_12262)){
        _25current_file_no_12262 = (long)DBL_PTR(_25current_file_no_12262)->dbl;
    }

    /** 	line_number        = top[LINE_NO]*/
    _2 = (int)SEQ_PTR(_top_25909);
    _25line_number_12263 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_25line_number_12263)){
        _25line_number_12263 = (long)DBL_PTR(_25line_number_12263)->dbl;
    }

    /** 	src_file           = top[FILE_PTR]*/
    _2 = (int)SEQ_PTR(_top_25909);
    _25src_file_12404 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_25src_file_12404)){
        _25src_file_12404 = (long)DBL_PTR(_25src_file_12404)->dbl;
    }

    /** 	file_start_sym     = top[FILE_START_SYM]*/
    _2 = (int)SEQ_PTR(_top_25909);
    _25file_start_sym_12268 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_25file_start_sym_12268)){
        _25file_start_sym_12268 = (long)DBL_PTR(_25file_start_sym_12268)->dbl;
    }

    /** 	OpWarning          = top[OP_WARNING]*/
    _2 = (int)SEQ_PTR(_top_25909);
    _25OpWarning_12330 = (int)*(((s1_ptr)_2)->base + 5);
    if (!IS_ATOM_INT(_25OpWarning_12330)){
        _25OpWarning_12330 = (long)DBL_PTR(_25OpWarning_12330)->dbl;
    }

    /** 	OpTrace            = top[OP_TRACE]*/
    _2 = (int)SEQ_PTR(_top_25909);
    _25OpTrace_12332 = (int)*(((s1_ptr)_2)->base + 6);
    if (!IS_ATOM_INT(_25OpTrace_12332)){
        _25OpTrace_12332 = (long)DBL_PTR(_25OpTrace_12332)->dbl;
    }

    /** 	OpTypeCheck        = top[OP_TYPE_CHECK]*/
    _2 = (int)SEQ_PTR(_top_25909);
    _25OpTypeCheck_12333 = (int)*(((s1_ptr)_2)->base + 7);
    if (!IS_ATOM_INT(_25OpTypeCheck_12333)){
        _25OpTypeCheck_12333 = (long)DBL_PTR(_25OpTypeCheck_12333)->dbl;
    }

    /** 	OpProfileTime      = top[OP_PROFILE_TIME]*/
    _2 = (int)SEQ_PTR(_top_25909);
    _25OpProfileTime_12335 = (int)*(((s1_ptr)_2)->base + 8);
    if (!IS_ATOM_INT(_25OpProfileTime_12335)){
        _25OpProfileTime_12335 = (long)DBL_PTR(_25OpProfileTime_12335)->dbl;
    }

    /** 	OpProfileStatement = top[OP_PROFILE_STATEMENT]*/
    _2 = (int)SEQ_PTR(_top_25909);
    _25OpProfileStatement_12334 = (int)*(((s1_ptr)_2)->base + 9);
    if (!IS_ATOM_INT(_25OpProfileStatement_12334)){
        _25OpProfileStatement_12334 = (long)DBL_PTR(_25OpProfileStatement_12334)->dbl;
    }

    /** 	OpDefines          = top[OP_DEFINES]*/
    DeRef(_25OpDefines_12336);
    _2 = (int)SEQ_PTR(_top_25909);
    _25OpDefines_12336 = (int)*(((s1_ptr)_2)->base + 10);
    Ref(_25OpDefines_12336);

    /** 	prev_OpWarning     = top[PREV_OP_WARNING]*/
    _2 = (int)SEQ_PTR(_top_25909);
    _25prev_OpWarning_12331 = (int)*(((s1_ptr)_2)->base + 11);
    if (!IS_ATOM_INT(_25prev_OpWarning_12331)){
        _25prev_OpWarning_12331 = (long)DBL_PTR(_25prev_OpWarning_12331)->dbl;
    }

    /** 	OpInline           = top[OP_INLINE]*/
    _2 = (int)SEQ_PTR(_top_25909);
    _25OpInline_12340 = (int)*(((s1_ptr)_2)->base + 12);
    if (!IS_ATOM_INT(_25OpInline_12340)){
        _25OpInline_12340 = (long)DBL_PTR(_25OpInline_12340)->dbl;
    }

    /** 	OpIndirectInclude  = top[OP_INDIRECT_INCLUDE]*/
    _2 = (int)SEQ_PTR(_top_25909);
    _25OpIndirectInclude_12341 = (int)*(((s1_ptr)_2)->base + 13);
    if (!IS_ATOM_INT(_25OpIndirectInclude_12341)){
        _25OpIndirectInclude_12341 = (long)DBL_PTR(_25OpIndirectInclude_12341)->dbl;
    }

    /** 	putback_fwd_line_number = line_number -- top[PUTBACK_FWD_LINE_NUMBER]*/
    _25putback_fwd_line_number_12265 = _25line_number_12263;

    /** 	putback_ForwardLine = top[PUTBACK_FORWARDLINE]*/
    DeRef(_43putback_ForwardLine_49534);
    _2 = (int)SEQ_PTR(_top_25909);
    _43putback_ForwardLine_49534 = (int)*(((s1_ptr)_2)->base + 15);
    Ref(_43putback_ForwardLine_49534);

    /** 	putback_forward_bp = top[PUTBACK_FORWARD_BP]*/
    _2 = (int)SEQ_PTR(_top_25909);
    _43putback_forward_bp_49538 = (int)*(((s1_ptr)_2)->base + 16);
    if (!IS_ATOM_INT(_43putback_forward_bp_49538)){
        _43putback_forward_bp_49538 = (long)DBL_PTR(_43putback_forward_bp_49538)->dbl;
    }

    /** 	last_fwd_line_number = top[LAST_FWD_LINE_NUMBER]*/
    _2 = (int)SEQ_PTR(_top_25909);
    _25last_fwd_line_number_12266 = (int)*(((s1_ptr)_2)->base + 17);
    if (!IS_ATOM_INT(_25last_fwd_line_number_12266)){
        _25last_fwd_line_number_12266 = (long)DBL_PTR(_25last_fwd_line_number_12266)->dbl;
    }

    /** 	last_ForwardLine = top[LAST_FORWARDLINE]*/
    DeRef(_43last_ForwardLine_49535);
    _2 = (int)SEQ_PTR(_top_25909);
    _43last_ForwardLine_49535 = (int)*(((s1_ptr)_2)->base + 18);
    Ref(_43last_ForwardLine_49535);

    /** 	last_forward_bp = top[LAST_FORWARD_BP]*/
    _2 = (int)SEQ_PTR(_top_25909);
    _43last_forward_bp_49539 = (int)*(((s1_ptr)_2)->base + 19);
    if (!IS_ATOM_INT(_43last_forward_bp_49539)){
        _43last_forward_bp_49539 = (long)DBL_PTR(_43last_forward_bp_49539)->dbl;
    }

    /** 	ThisLine = top[THISLINE]*/
    DeRef(_43ThisLine_49532);
    _2 = (int)SEQ_PTR(_top_25909);
    _43ThisLine_49532 = (int)*(((s1_ptr)_2)->base + 20);
    Ref(_43ThisLine_49532);

    /** 	fwd_line_number = line_number --top[FWD_LINE_NUMBER]*/
    _25fwd_line_number_12264 = _25line_number_12263;

    /** 	forward_bp = top[FORWARD_BP]*/
    _2 = (int)SEQ_PTR(_top_25909);
    _43forward_bp_49537 = (int)*(((s1_ptr)_2)->base + 22);
    if (!IS_ATOM_INT(_43forward_bp_49537)){
        _43forward_bp_49537 = (long)DBL_PTR(_43forward_bp_49537)->dbl;
    }

    /** 	ForwardLine = ThisLine*/
    Ref(_43ThisLine_49532);
    DeRef(_43ForwardLine_49533);
    _43ForwardLine_49533 = _43ThisLine_49532;

    /** 	putback_ForwardLine = ThisLine*/
    Ref(_43ThisLine_49532);
    DeRef(_43putback_ForwardLine_49534);
    _43putback_ForwardLine_49534 = _43ThisLine_49532;

    /** 	last_ForwardLine = ThisLine*/
    Ref(_43ThisLine_49532);
    DeRef(_43last_ForwardLine_49535);
    _43last_ForwardLine_49535 = _43ThisLine_49532;

    /** 	IncludeStk = IncludeStk[1..$-1]*/
    if (IS_SEQUENCE(_60IncludeStk_24679)){
            _14877 = SEQ_PTR(_60IncludeStk_24679)->length;
    }
    else {
        _14877 = 1;
    }
    _14878 = _14877 - 1;
    _14877 = NOVALUE;
    rhs_slice_target = (object_ptr)&_60IncludeStk_24679;
    RHS_Slice(_60IncludeStk_24679, 1, _14878);

    /** 	SymTab[TopLevelSub][S_CODE] = Code*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25TopLevelSub_12269 + ((s1_ptr)_2)->base);
    RefDS(_25Code_12355);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25S_CODE_11925))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25S_CODE_11925)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25S_CODE_11925);
    _1 = *(int *)_2;
    *(int *)_2 = _25Code_12355;
    DeRef(_1);
    _14880 = NOVALUE;

    /** 	return TRUE*/
    DeRefDS(_top_25909);
    _14878 = NOVALUE;
    return _5TRUE_244;
    ;
}


int _60MakeInt(int _text_26008, int _nBase_26009)
{
    int _num_26010 = NOVALUE;
    int _fnum_26011 = NOVALUE;
    int _digit_26012 = NOVALUE;
    int _maxchk_26013 = NOVALUE;
    int _14947 = NOVALUE;
    int _14946 = NOVALUE;
    int _14944 = NOVALUE;
    int _14943 = NOVALUE;
    int _14940 = NOVALUE;
    int _14938 = NOVALUE;
    int _14935 = NOVALUE;
    int _14934 = NOVALUE;
    int _14933 = NOVALUE;
    int _14931 = NOVALUE;
    int _14929 = NOVALUE;
    int _14927 = NOVALUE;
    int _14926 = NOVALUE;
    int _14924 = NOVALUE;
    int _14922 = NOVALUE;
    int _14921 = NOVALUE;
    int _14919 = NOVALUE;
    int _14918 = NOVALUE;
    int _14915 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_nBase_26009)) {
        _1 = (long)(DBL_PTR(_nBase_26009)->dbl);
        if (UNIQUE(DBL_PTR(_nBase_26009)) && (DBL_PTR(_nBase_26009)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_nBase_26009);
        _nBase_26009 = _1;
    }

    /** 	switch nBase do*/
    _0 = _nBase_26009;
    switch ( _0 ){ 

        /** 		case 2 then*/
        case 2:

        /** 			maxchk = 536870911*/
        _maxchk_26013 = 536870911;
        goto L1; // [21] 82

        /** 		case 8 then*/
        case 8:

        /** 			maxchk = 134217727*/
        _maxchk_26013 = 134217727;
        goto L1; // [32] 82

        /** 		case 10 then*/
        case 10:

        /** 			num = find(text, common_int_text)*/
        _num_26010 = find_from(_text_26008, _60common_int_text_25983, 1);

        /** 			if num then*/
        if (_num_26010 == 0)
        {
            goto L2; // [49] 65
        }
        else{
        }

        /** 				return common_ints[num]*/
        _2 = (int)SEQ_PTR(_60common_ints_26003);
        _14915 = (int)*(((s1_ptr)_2)->base + _num_26010);
        DeRefDS(_text_26008);
        DeRef(_fnum_26011);
        return _14915;
L2: 

        /** 			maxchk = 107374181*/
        _maxchk_26013 = 107374181;
        goto L1; // [70] 82

        /** 		case 16 then*/
        case 16:

        /** 			maxchk = 67108863*/
        _maxchk_26013 = 67108863;
    ;}L1: 

    /** 	num = 0*/
    _num_26010 = 0;

    /** 	fnum = 0*/
    DeRef(_fnum_26011);
    _fnum_26011 = 0;

    /** 	for i = 1 to length(text) do*/
    if (IS_SEQUENCE(_text_26008)){
            _14918 = SEQ_PTR(_text_26008)->length;
    }
    else {
        _14918 = 1;
    }
    {
        int _i_26028;
        _i_26028 = 1;
L3: 
        if (_i_26028 > _14918){
            goto L4; // [97] 307
        }

        /** 		if text[i] > 'a' then*/
        _2 = (int)SEQ_PTR(_text_26008);
        _14919 = (int)*(((s1_ptr)_2)->base + _i_26028);
        if (binary_op_a(LESSEQ, _14919, 97)){
            _14919 = NOVALUE;
            goto L5; // [110] 133
        }
        _14919 = NOVALUE;

        /** 			digit = text[i] - 'a' + 10*/
        _2 = (int)SEQ_PTR(_text_26008);
        _14921 = (int)*(((s1_ptr)_2)->base + _i_26028);
        if (IS_ATOM_INT(_14921)) {
            _14922 = _14921 - 97;
            if ((long)((unsigned long)_14922 +(unsigned long) HIGH_BITS) >= 0){
                _14922 = NewDouble((double)_14922);
            }
        }
        else {
            _14922 = binary_op(MINUS, _14921, 97);
        }
        _14921 = NOVALUE;
        if (IS_ATOM_INT(_14922)) {
            _digit_26012 = _14922 + 10;
        }
        else {
            _digit_26012 = binary_op(PLUS, _14922, 10);
        }
        DeRef(_14922);
        _14922 = NOVALUE;
        if (!IS_ATOM_INT(_digit_26012)) {
            _1 = (long)(DBL_PTR(_digit_26012)->dbl);
            if (UNIQUE(DBL_PTR(_digit_26012)) && (DBL_PTR(_digit_26012)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_digit_26012);
            _digit_26012 = _1;
        }
        goto L6; // [130] 175
L5: 

        /** 		elsif text[i] > 'A' then*/
        _2 = (int)SEQ_PTR(_text_26008);
        _14924 = (int)*(((s1_ptr)_2)->base + _i_26028);
        if (binary_op_a(LESSEQ, _14924, 65)){
            _14924 = NOVALUE;
            goto L7; // [139] 162
        }
        _14924 = NOVALUE;

        /** 			digit = text[i] - 'A' + 10*/
        _2 = (int)SEQ_PTR(_text_26008);
        _14926 = (int)*(((s1_ptr)_2)->base + _i_26028);
        if (IS_ATOM_INT(_14926)) {
            _14927 = _14926 - 65;
            if ((long)((unsigned long)_14927 +(unsigned long) HIGH_BITS) >= 0){
                _14927 = NewDouble((double)_14927);
            }
        }
        else {
            _14927 = binary_op(MINUS, _14926, 65);
        }
        _14926 = NOVALUE;
        if (IS_ATOM_INT(_14927)) {
            _digit_26012 = _14927 + 10;
        }
        else {
            _digit_26012 = binary_op(PLUS, _14927, 10);
        }
        DeRef(_14927);
        _14927 = NOVALUE;
        if (!IS_ATOM_INT(_digit_26012)) {
            _1 = (long)(DBL_PTR(_digit_26012)->dbl);
            if (UNIQUE(DBL_PTR(_digit_26012)) && (DBL_PTR(_digit_26012)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_digit_26012);
            _digit_26012 = _1;
        }
        goto L6; // [159] 175
L7: 

        /** 			digit = text[i] - '0'*/
        _2 = (int)SEQ_PTR(_text_26008);
        _14929 = (int)*(((s1_ptr)_2)->base + _i_26028);
        if (IS_ATOM_INT(_14929)) {
            _digit_26012 = _14929 - 48;
        }
        else {
            _digit_26012 = binary_op(MINUS, _14929, 48);
        }
        _14929 = NOVALUE;
        if (!IS_ATOM_INT(_digit_26012)) {
            _1 = (long)(DBL_PTR(_digit_26012)->dbl);
            if (UNIQUE(DBL_PTR(_digit_26012)) && (DBL_PTR(_digit_26012)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_digit_26012);
            _digit_26012 = _1;
        }
L6: 

        /** 		if digit >= nBase or digit < 0 then*/
        _14931 = (_digit_26012 >= _nBase_26009);
        if (_14931 != 0) {
            goto L8; // [183] 196
        }
        _14933 = (_digit_26012 < 0);
        if (_14933 == 0)
        {
            DeRef(_14933);
            _14933 = NOVALUE;
            goto L9; // [192] 212
        }
        else{
            DeRef(_14933);
            _14933 = NOVALUE;
        }
L8: 

        /** 			CompileErr(62, {text[i],i})*/
        _2 = (int)SEQ_PTR(_text_26008);
        _14934 = (int)*(((s1_ptr)_2)->base + _i_26028);
        Ref(_14934);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _14934;
        ((int *)_2)[2] = _i_26028;
        _14935 = MAKE_SEQ(_1);
        _14934 = NOVALUE;
        _43CompileErr(62, _14935, 0);
        _14935 = NOVALUE;
L9: 

        /** 		if fnum = 0 then*/
        if (binary_op_a(NOTEQ, _fnum_26011, 0)){
            goto LA; // [214] 255
        }

        /** 			if num <= maxchk then*/
        if (_num_26010 > _maxchk_26013)
        goto LB; // [222] 241

        /** 				num = num * nBase + digit*/
        if (_num_26010 == (short)_num_26010 && _nBase_26009 <= INT15 && _nBase_26009 >= -INT15)
        _14938 = _num_26010 * _nBase_26009;
        else
        _14938 = NewDouble(_num_26010 * (double)_nBase_26009);
        if (IS_ATOM_INT(_14938)) {
            _num_26010 = _14938 + _digit_26012;
        }
        else {
            _num_26010 = NewDouble(DBL_PTR(_14938)->dbl + (double)_digit_26012);
        }
        DeRef(_14938);
        _14938 = NOVALUE;
        if (!IS_ATOM_INT(_num_26010)) {
            _1 = (long)(DBL_PTR(_num_26010)->dbl);
            if (UNIQUE(DBL_PTR(_num_26010)) && (DBL_PTR(_num_26010)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_num_26010);
            _num_26010 = _1;
        }
        goto LC; // [238] 300
LB: 

        /** 				fnum = num * nBase + digit*/
        if (_num_26010 == (short)_num_26010 && _nBase_26009 <= INT15 && _nBase_26009 >= -INT15)
        _14940 = _num_26010 * _nBase_26009;
        else
        _14940 = NewDouble(_num_26010 * (double)_nBase_26009);
        DeRef(_fnum_26011);
        if (IS_ATOM_INT(_14940)) {
            _fnum_26011 = _14940 + _digit_26012;
            if ((long)((unsigned long)_fnum_26011 + (unsigned long)HIGH_BITS) >= 0) 
            _fnum_26011 = NewDouble((double)_fnum_26011);
        }
        else {
            _fnum_26011 = NewDouble(DBL_PTR(_14940)->dbl + (double)_digit_26012);
        }
        DeRef(_14940);
        _14940 = NOVALUE;
        goto LC; // [252] 300
LA: 

        /** 			if fnum >= almost_max_16 then*/
        if (binary_op_a(LESS, _fnum_26011, _60almost_max_16_25980)){
            goto LD; // [259] 289
        }

        /** 				if fnum > (MAX_ATOM - digit)/nBase then*/
        if (IS_ATOM_INT(_60MAX_ATOM_25972)) {
            _14943 = _60MAX_ATOM_25972 - _digit_26012;
            if ((long)((unsigned long)_14943 +(unsigned long) HIGH_BITS) >= 0){
                _14943 = NewDouble((double)_14943);
            }
        }
        else {
            _14943 = binary_op(MINUS, _60MAX_ATOM_25972, _digit_26012);
        }
        if (IS_ATOM_INT(_14943)) {
            _14944 = (_14943 % _nBase_26009) ? NewDouble((double)_14943 / _nBase_26009) : (_14943 / _nBase_26009);
        }
        else {
            _14944 = binary_op(DIVIDE, _14943, _nBase_26009);
        }
        DeRef(_14943);
        _14943 = NOVALUE;
        if (binary_op_a(LESSEQ, _fnum_26011, _14944)){
            DeRef(_14944);
            _14944 = NOVALUE;
            goto LE; // [275] 288
        }
        DeRef(_14944);
        _14944 = NOVALUE;

        /** 					fenv:raise(FE_OVERFLOW)					*/
        _14946 = _62raise(111);
LE: 
LD: 

        /** 			fnum = fnum * nBase + digit*/
        if (IS_ATOM_INT(_fnum_26011)) {
            if (_fnum_26011 == (short)_fnum_26011 && _nBase_26009 <= INT15 && _nBase_26009 >= -INT15)
            _14947 = _fnum_26011 * _nBase_26009;
            else
            _14947 = NewDouble(_fnum_26011 * (double)_nBase_26009);
        }
        else {
            _14947 = NewDouble(DBL_PTR(_fnum_26011)->dbl * (double)_nBase_26009);
        }
        DeRef(_fnum_26011);
        if (IS_ATOM_INT(_14947)) {
            _fnum_26011 = _14947 + _digit_26012;
            if ((long)((unsigned long)_fnum_26011 + (unsigned long)HIGH_BITS) >= 0) 
            _fnum_26011 = NewDouble((double)_fnum_26011);
        }
        else {
            _fnum_26011 = NewDouble(DBL_PTR(_14947)->dbl + (double)_digit_26012);
        }
        DeRef(_14947);
        _14947 = NOVALUE;
LC: 

        /** 	end for*/
        _i_26028 = _i_26028 + 1;
        goto L3; // [302] 104
L4: 
        ;
    }

    /** 	if fnum = 0 then*/
    if (binary_op_a(NOTEQ, _fnum_26011, 0)){
        goto LF; // [309] 322
    }

    /** 		return num*/
    DeRefDS(_text_26008);
    DeRef(_fnum_26011);
    _14915 = NOVALUE;
    DeRef(_14931);
    _14931 = NOVALUE;
    DeRef(_14946);
    _14946 = NOVALUE;
    return _num_26010;
    goto L10; // [319] 329
LF: 

    /** 		return fnum*/
    DeRefDS(_text_26008);
    _14915 = NOVALUE;
    DeRef(_14931);
    _14931 = NOVALUE;
    DeRef(_14946);
    _14946 = NOVALUE;
    return _fnum_26011;
L10: 
    ;
}


int _60GetHexChar(int _cnt_26077, int _errno_26078)
{
    int _val_26079 = NOVALUE;
    int _d_26080 = NOVALUE;
    int _14957 = NOVALUE;
    int _14956 = NOVALUE;
    int _14951 = NOVALUE;
    int _0, _1, _2;
    

    /** 	val = 0*/
    DeRef(_val_26079);
    _val_26079 = 0;

    /** 	while cnt > 0 do*/
L1: 
    if (_cnt_26077 <= 0)
    goto L2; // [15] 88

    /** 		d = find(getch(), "0123456789ABCDEFabcdef_")*/
    _14951 = _60getch();
    _d_26080 = find_from(_14951, _14952, 1);
    DeRef(_14951);
    _14951 = NOVALUE;

    /** 		if d = 0 then*/
    if (_d_26080 != 0)
    goto L3; // [31] 43

    /** 			CompileErr( errno )*/
    RefDS(_22682);
    _43CompileErr(_errno_26078, _22682, 0);
L3: 

    /** 		if d != 23 then*/
    if (_d_26080 == 23)
    goto L1; // [45] 15

    /** 			val = val * 16 + d - 1*/
    if (IS_ATOM_INT(_val_26079)) {
        if (_val_26079 == (short)_val_26079)
        _14956 = _val_26079 * 16;
        else
        _14956 = NewDouble(_val_26079 * (double)16);
    }
    else {
        _14956 = NewDouble(DBL_PTR(_val_26079)->dbl * (double)16);
    }
    if (IS_ATOM_INT(_14956)) {
        _14957 = _14956 + _d_26080;
        if ((long)((unsigned long)_14957 + (unsigned long)HIGH_BITS) >= 0) 
        _14957 = NewDouble((double)_14957);
    }
    else {
        _14957 = NewDouble(DBL_PTR(_14956)->dbl + (double)_d_26080);
    }
    DeRef(_14956);
    _14956 = NOVALUE;
    DeRef(_val_26079);
    if (IS_ATOM_INT(_14957)) {
        _val_26079 = _14957 - 1;
        if ((long)((unsigned long)_val_26079 +(unsigned long) HIGH_BITS) >= 0){
            _val_26079 = NewDouble((double)_val_26079);
        }
    }
    else {
        _val_26079 = NewDouble(DBL_PTR(_14957)->dbl - (double)1);
    }
    DeRef(_14957);
    _14957 = NOVALUE;

    /** 			if d > 16 then*/
    if (_d_26080 <= 16)
    goto L4; // [65] 76

    /** 				val -= 6*/
    _0 = _val_26079;
    if (IS_ATOM_INT(_val_26079)) {
        _val_26079 = _val_26079 - 6;
        if ((long)((unsigned long)_val_26079 +(unsigned long) HIGH_BITS) >= 0){
            _val_26079 = NewDouble((double)_val_26079);
        }
    }
    else {
        _val_26079 = NewDouble(DBL_PTR(_val_26079)->dbl - (double)6);
    }
    DeRef(_0);
L4: 

    /** 			cnt -= 1*/
    _cnt_26077 = _cnt_26077 - 1;

    /** 	end while*/
    goto L1; // [85] 15
L2: 

    /** 	return val*/
    return _val_26079;
    ;
}


int _60GetBinaryChar(int _delim_26100)
{
    int _val_26101 = NOVALUE;
    int _d_26102 = NOVALUE;
    int _vchars_26103 = NOVALUE;
    int _cnt_26106 = NOVALUE;
    int _14971 = NOVALUE;
    int _14970 = NOVALUE;
    int _14964 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence vchars = "01_ " & delim*/
    Append(&_vchars_26103, _14962, _delim_26100);

    /** 	integer cnt = 0*/
    _cnt_26106 = 0;

    /** 	val = 0*/
    DeRef(_val_26101);
    _val_26101 = 0;

    /** 	while 1 do*/
L1: 

    /** 		d = find(getch(), vchars)*/
    _14964 = _60getch();
    _d_26102 = find_from(_14964, _vchars_26103, 1);
    DeRef(_14964);
    _14964 = NOVALUE;

    /** 		if d = 0 then*/
    if (_d_26102 != 0)
    goto L2; // [36] 48

    /** 			CompileErr( 343 )*/
    RefDS(_22682);
    _43CompileErr(343, _22682, 0);
L2: 

    /** 		if d = 5 then*/
    if (_d_26102 != 5)
    goto L3; // [50] 63

    /** 			ungetch()*/
    _60ungetch();

    /** 			exit*/
    goto L4; // [60] 106
L3: 

    /** 		if d = 4 then*/
    if (_d_26102 != 4)
    goto L5; // [65] 74

    /** 			exit*/
    goto L4; // [71] 106
L5: 

    /** 		if d != 3 then*/
    if (_d_26102 == 3)
    goto L1; // [76] 24

    /** 			val = val * 2 + d - 1*/
    if (IS_ATOM_INT(_val_26101) && IS_ATOM_INT(_val_26101)) {
        _14970 = _val_26101 + _val_26101;
        if ((long)((unsigned long)_14970 + (unsigned long)HIGH_BITS) >= 0) 
        _14970 = NewDouble((double)_14970);
    }
    else {
        if (IS_ATOM_INT(_val_26101)) {
            _14970 = NewDouble((double)_val_26101 + DBL_PTR(_val_26101)->dbl);
        }
        else {
            if (IS_ATOM_INT(_val_26101)) {
                _14970 = NewDouble(DBL_PTR(_val_26101)->dbl + (double)_val_26101);
            }
            else
            _14970 = NewDouble(DBL_PTR(_val_26101)->dbl + DBL_PTR(_val_26101)->dbl);
        }
    }
    if (IS_ATOM_INT(_14970)) {
        _14971 = _14970 + _d_26102;
        if ((long)((unsigned long)_14971 + (unsigned long)HIGH_BITS) >= 0) 
        _14971 = NewDouble((double)_14971);
    }
    else {
        _14971 = NewDouble(DBL_PTR(_14970)->dbl + (double)_d_26102);
    }
    DeRef(_14970);
    _14970 = NOVALUE;
    DeRef(_val_26101);
    if (IS_ATOM_INT(_14971)) {
        _val_26101 = _14971 - 1;
        if ((long)((unsigned long)_val_26101 +(unsigned long) HIGH_BITS) >= 0){
            _val_26101 = NewDouble((double)_val_26101);
        }
    }
    else {
        _val_26101 = NewDouble(DBL_PTR(_14971)->dbl - (double)1);
    }
    DeRef(_14971);
    _14971 = NOVALUE;

    /** 			cnt += 1*/
    _cnt_26106 = _cnt_26106 + 1;

    /** 	end while*/
    goto L1; // [103] 24
L4: 

    /** 	if cnt = 0 then*/
    if (_cnt_26106 != 0)
    goto L6; // [108] 120

    /** 		CompileErr(343)*/
    RefDS(_22682);
    _43CompileErr(343, _22682, 0);
L6: 

    /** 	return val*/
    DeRefi(_vchars_26103);
    return _val_26101;
    ;
}


int _60EscapeChar(int _delim_26128)
{
    int _c_26129 = NOVALUE;
    int _0, _1, _2;
    

    /** 	c = getch()*/
    _0 = _c_26129;
    _c_26129 = _60getch();
    DeRef(_0);

    /** 	switch c do*/
    if (IS_SEQUENCE(_c_26129) ){
        goto L1; // [10] 135
    }
    if(!IS_ATOM_INT(_c_26129)){
        if( (DBL_PTR(_c_26129)->dbl != (double) ((int) DBL_PTR(_c_26129)->dbl) ) ){
            goto L1; // [10] 135
        }
        _0 = (int) DBL_PTR(_c_26129)->dbl;
    }
    else {
        _0 = _c_26129;
    };
    switch ( _0 ){ 

        /** 		case 'n' then*/
        case 110:

        /** 			c = 10 -- Newline*/
        DeRef(_c_26129);
        _c_26129 = 10;
        goto L2; // [24] 145

        /** 		case 't' then*/
        case 116:

        /** 			c = 9 -- Tabulator*/
        DeRef(_c_26129);
        _c_26129 = 9;
        goto L2; // [35] 145

        /** 		case '"', '\\', '\'' then*/
        case 34:
        case 92:
        case 39:

        /** 		case 'r' then*/
        goto L2; // [47] 145
        case 114:

        /** 			c = 13 -- Carriage Return*/
        DeRef(_c_26129);
        _c_26129 = 13;
        goto L2; // [56] 145

        /** 		case '0' then*/
        case 48:

        /** 			c = 0 -- Null*/
        DeRef(_c_26129);
        _c_26129 = 0;
        goto L2; // [67] 145

        /** 		case 'e', 'E' then*/
        case 101:
        case 69:

        /** 			c = 27 -- escape char.*/
        DeRef(_c_26129);
        _c_26129 = 27;
        goto L2; // [80] 145

        /** 		case 'x' then*/
        case 120:

        /** 			c = GetHexChar(2, 340)*/
        _0 = _c_26129;
        _c_26129 = _60GetHexChar(2, 340);
        DeRef(_0);
        goto L2; // [93] 145

        /** 		case 'u' then*/
        case 117:

        /** 			c = GetHexChar(4, 341)*/
        _0 = _c_26129;
        _c_26129 = _60GetHexChar(4, 341);
        DeRef(_0);
        goto L2; // [106] 145

        /** 		case 'U' then*/
        case 85:

        /** 			c = GetHexChar(8, 342)*/
        _0 = _c_26129;
        _c_26129 = _60GetHexChar(8, 342);
        DeRef(_0);
        goto L2; // [119] 145

        /** 		case 'b' then*/
        case 98:

        /** 			c = GetBinaryChar(delim)*/
        _0 = _c_26129;
        _c_26129 = _60GetBinaryChar(_delim_26128);
        DeRef(_0);
        goto L2; // [131] 145

        /** 		case else*/
        default:
L1: 

        /** 			CompileErr(155)*/
        RefDS(_22682);
        _43CompileErr(155, _22682, 0);
    ;}L2: 

    /** 	return c*/
    return _c_26129;
    ;
}


int _60my_sscanf(int _yytext_26151)
{
    int _e_sign_26152 = NOVALUE;
    int _ndigits_26153 = NOVALUE;
    int _e_mag_26154 = NOVALUE;
    int _mantissa_26155 = NOVALUE;
    int _c_26156 = NOVALUE;
    int _i_26157 = NOVALUE;
    int _dec_26158 = NOVALUE;
    int _ex_26159 = NOVALUE;
    int _real_overflow_26160 = NOVALUE;
    int _not_zero_26188 = NOVALUE;
    int _back_dec_26197 = NOVALUE;
    int _num_back_26199 = NOVALUE;
    int _num_26200 = NOVALUE;
    int _frac_26226 = NOVALUE;
    int _15036 = NOVALUE;
    int _15035 = NOVALUE;
    int _15033 = NOVALUE;
    int _15032 = NOVALUE;
    int _15031 = NOVALUE;
    int _15025 = NOVALUE;
    int _15024 = NOVALUE;
    int _15023 = NOVALUE;
    int _15022 = NOVALUE;
    int _15016 = NOVALUE;
    int _15015 = NOVALUE;
    int _15012 = NOVALUE;
    int _15011 = NOVALUE;
    int _15010 = NOVALUE;
    int _15006 = NOVALUE;
    int _15005 = NOVALUE;
    int _15004 = NOVALUE;
    int _15003 = NOVALUE;
    int _14999 = NOVALUE;
    int _14998 = NOVALUE;
    int _14995 = NOVALUE;
    int _14994 = NOVALUE;
    int _14993 = NOVALUE;
    int _14987 = NOVALUE;
    int _14985 = NOVALUE;
    int _14984 = NOVALUE;
    int _14982 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer real_overflow = 0*/
    _real_overflow_26160 = 0;

    /** 	if length(yytext) < 2 then*/
    if (IS_SEQUENCE(_yytext_26151)){
            _14982 = SEQ_PTR(_yytext_26151)->length;
    }
    else {
        _14982 = 1;
    }
    if (_14982 >= 2)
    goto L1; // [13] 25

    /** 		CompileErr(121)*/
    RefDS(_22682);
    _43CompileErr(121, _22682, 0);
L1: 

    /** 	fenv:clear(FE_ALL_EXCEPT)*/
    RefDS(_62FE_ALL_EXCEPT_23003);
    _14984 = _62clear(_62FE_ALL_EXCEPT_23003);

    /** 	if find( 'e', yytext ) or find( 'E', yytext ) then*/
    _14985 = find_from(101, _yytext_26151, 1);
    if (_14985 != 0) {
        goto L2; // [40] 54
    }
    _14987 = find_from(69, _yytext_26151, 1);
    if (_14987 == 0)
    {
        _14987 = NOVALUE;
        goto L3; // [50] 67
    }
    else{
        _14987 = NOVALUE;
    }
L2: 

    /** 		mantissa = scientific_to_atom( yytext )*/
    RefDS(_yytext_26151);
    _0 = _mantissa_26155;
    _mantissa_26155 = _61scientific_to_atom(_yytext_26151);
    DeRef(_0);

    /** 		goto "floating_point_check"*/
    goto G4;
L3: 

    /** 	mantissa = 0.0*/
    RefDS(_14990);
    DeRef(_mantissa_26155);
    _mantissa_26155 = _14990;

    /** 	ndigits = 0*/
    _ndigits_26153 = 0;

    /** 	yytext &= 0 -- end marker*/
    Append(&_yytext_26151, _yytext_26151, 0);

    /** 	c = yytext[1]*/
    _2 = (int)SEQ_PTR(_yytext_26151);
    _c_26156 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_c_26156))
    _c_26156 = (long)DBL_PTR(_c_26156)->dbl;

    /** 	i = 2*/
    _i_26157 = 2;

    /** 	while c >= '0' and c <= '9' do*/
L5: 
    _14993 = (_c_26156 >= 48);
    if (_14993 == 0) {
        goto L6; // [103] 152
    }
    _14995 = (_c_26156 <= 57);
    if (_14995 == 0)
    {
        DeRef(_14995);
        _14995 = NOVALUE;
        goto L6; // [112] 152
    }
    else{
        DeRef(_14995);
        _14995 = NOVALUE;
    }

    /** 		ndigits += 1*/
    _ndigits_26153 = _ndigits_26153 + 1;

    /** 		mantissa = mantissa * 10.0 + (c - '0')*/
    if (IS_ATOM_INT(_mantissa_26155)) {
        _14998 = NewDouble((double)_mantissa_26155 * DBL_PTR(_14997)->dbl);
    }
    else {
        _14998 = NewDouble(DBL_PTR(_mantissa_26155)->dbl * DBL_PTR(_14997)->dbl);
    }
    _14999 = _c_26156 - 48;
    if ((long)((unsigned long)_14999 +(unsigned long) HIGH_BITS) >= 0){
        _14999 = NewDouble((double)_14999);
    }
    DeRef(_mantissa_26155);
    if (IS_ATOM_INT(_14999)) {
        _mantissa_26155 = NewDouble(DBL_PTR(_14998)->dbl + (double)_14999);
    }
    else
    _mantissa_26155 = NewDouble(DBL_PTR(_14998)->dbl + DBL_PTR(_14999)->dbl);
    DeRefDS(_14998);
    _14998 = NOVALUE;
    DeRef(_14999);
    _14999 = NOVALUE;

    /** 		c = yytext[i]*/
    _2 = (int)SEQ_PTR(_yytext_26151);
    _c_26156 = (int)*(((s1_ptr)_2)->base + _i_26157);
    if (!IS_ATOM_INT(_c_26156))
    _c_26156 = (long)DBL_PTR(_c_26156)->dbl;

    /** 		i += 1*/
    _i_26157 = _i_26157 + 1;

    /** 	end while*/
    goto L5; // [149] 99
L6: 

    /** 	integer not_zero = 0*/
    _not_zero_26188 = 0;

    /** 	if c = '.' and not fenv:test(FE_OVERFLOW) then*/
    _15003 = (_c_26156 == 46);
    if (_15003 == 0) {
        goto L7; // [163] 409
    }
    _15005 = _62test(111);
    if (IS_ATOM_INT(_15005)) {
        _15006 = (_15005 == 0);
    }
    else {
        _15006 = unary_op(NOT, _15005);
    }
    DeRef(_15005);
    _15005 = NOVALUE;
    if (_15006 == 0) {
        DeRef(_15006);
        _15006 = NOVALUE;
        goto L7; // [177] 409
    }
    else {
        if (!IS_ATOM_INT(_15006) && DBL_PTR(_15006)->dbl == 0.0){
            DeRef(_15006);
            _15006 = NOVALUE;
            goto L7; // [177] 409
        }
        DeRef(_15006);
        _15006 = NOVALUE;
    }
    DeRef(_15006);
    _15006 = NOVALUE;

    /** 		c = yytext[i]*/
    _2 = (int)SEQ_PTR(_yytext_26151);
    _c_26156 = (int)*(((s1_ptr)_2)->base + _i_26157);
    if (!IS_ATOM_INT(_c_26156))
    _c_26156 = (long)DBL_PTR(_c_26156)->dbl;

    /** 		i += 1*/
    _i_26157 = _i_26157 + 1;

    /** 		atom back_dec*/

    /** 		dec = 1.0*/
    RefDS(_15009);
    DeRef(_dec_26158);
    _dec_26158 = _15009;

    /** 		atom num_back, num = 0*/
    DeRef(_num_26200);
    _num_26200 = 0;

    /** 		while c >= '0' and c <= '9' do*/
L8: 
    _15010 = (_c_26156 >= 48);
    if (_15010 == 0) {
        goto L9; // [213] 319
    }
    _15012 = (_c_26156 <= 57);
    if (_15012 == 0)
    {
        DeRef(_15012);
        _15012 = NOVALUE;
        goto L9; // [222] 319
    }
    else{
        DeRef(_15012);
        _15012 = NOVALUE;
    }

    /** 			ndigits += 1*/
    _ndigits_26153 = _ndigits_26153 + 1;

    /** 			if c != '0' then*/
    if (_c_26156 == 48)
    goto LA; // [233] 243

    /** 				not_zero = 1*/
    _not_zero_26188 = 1;
LA: 

    /** 			num_back = num*/
    Ref(_num_26200);
    DeRef(_num_back_26199);
    _num_back_26199 = _num_26200;

    /** 			num = num_back * 10 + (c - '0')*/
    if (IS_ATOM_INT(_num_back_26199)) {
        if (_num_back_26199 == (short)_num_back_26199)
        _15015 = _num_back_26199 * 10;
        else
        _15015 = NewDouble(_num_back_26199 * (double)10);
    }
    else {
        _15015 = NewDouble(DBL_PTR(_num_back_26199)->dbl * (double)10);
    }
    _15016 = _c_26156 - 48;
    if ((long)((unsigned long)_15016 +(unsigned long) HIGH_BITS) >= 0){
        _15016 = NewDouble((double)_15016);
    }
    DeRef(_num_26200);
    if (IS_ATOM_INT(_15015) && IS_ATOM_INT(_15016)) {
        _num_26200 = _15015 + _15016;
        if ((long)((unsigned long)_num_26200 + (unsigned long)HIGH_BITS) >= 0) 
        _num_26200 = NewDouble((double)_num_26200);
    }
    else {
        if (IS_ATOM_INT(_15015)) {
            _num_26200 = NewDouble((double)_15015 + DBL_PTR(_15016)->dbl);
        }
        else {
            if (IS_ATOM_INT(_15016)) {
                _num_26200 = NewDouble(DBL_PTR(_15015)->dbl + (double)_15016);
            }
            else
            _num_26200 = NewDouble(DBL_PTR(_15015)->dbl + DBL_PTR(_15016)->dbl);
        }
    }
    DeRef(_15015);
    _15015 = NOVALUE;
    DeRef(_15016);
    _15016 = NOVALUE;

    /** 			back_dec = dec*/
    Ref(_dec_26158);
    DeRef(_back_dec_26197);
    _back_dec_26197 = _dec_26158;

    /** 			dec *= 10.0*/
    _0 = _dec_26158;
    if (IS_ATOM_INT(_dec_26158)) {
        _dec_26158 = NewDouble((double)_dec_26158 * DBL_PTR(_14997)->dbl);
    }
    else {
        _dec_26158 = NewDouble(DBL_PTR(_dec_26158)->dbl * DBL_PTR(_14997)->dbl);
    }
    DeRef(_0);

    /** 			c = yytext[i]*/
    _2 = (int)SEQ_PTR(_yytext_26151);
    _c_26156 = (int)*(((s1_ptr)_2)->base + _i_26157);
    if (!IS_ATOM_INT(_c_26156))
    _c_26156 = (long)DBL_PTR(_c_26156)->dbl;

    /** 			i += 1*/
    _i_26157 = _i_26157 + 1;

    /** 			if dec = PINF then*/
    if (binary_op_a(NOTEQ, _dec_26158, _20PINF_4355)){
        goto L8; // [289] 209
    }

    /** 				fenv:clear(fenv:FE_OVERFLOW)*/
    _15022 = _62clear(111);

    /** 				num = num_back*/
    Ref(_num_back_26199);
    DeRef(_num_26200);
    _num_26200 = _num_back_26199;

    /** 				dec = back_dec*/
    Ref(_back_dec_26197);
    DeRefDS(_dec_26158);
    _dec_26158 = _back_dec_26197;

    /** 				exit*/
    goto L9; // [311] 319

    /** 		end while*/
    goto L8; // [316] 209
L9: 

    /** 		while c >= '0' and c <= '9' do*/
LB: 
    _15023 = (_c_26156 >= 48);
    if (_15023 == 0) {
        goto LC; // [328] 373
    }
    _15025 = (_c_26156 <= 57);
    if (_15025 == 0)
    {
        DeRef(_15025);
        _15025 = NOVALUE;
        goto LC; // [337] 373
    }
    else{
        DeRef(_15025);
        _15025 = NOVALUE;
    }

    /** 			if c != '0' then*/
    if (_c_26156 == 48)
    goto LD; // [342] 356

    /** 				not_zero = 1*/
    _not_zero_26188 = 1;

    /** 				exit*/
    goto LC; // [353] 373
LD: 

    /** 			c = yytext[i]*/
    _2 = (int)SEQ_PTR(_yytext_26151);
    _c_26156 = (int)*(((s1_ptr)_2)->base + _i_26157);
    if (!IS_ATOM_INT(_c_26156))
    _c_26156 = (long)DBL_PTR(_c_26156)->dbl;

    /** 			i += 1*/
    _i_26157 = _i_26157 + 1;

    /** 		end while*/
    goto LB; // [370] 324
LC: 

    /** 		atom frac*/

    /** 		frac = num / dec*/
    DeRef(_frac_26226);
    if (IS_ATOM_INT(_num_26200) && IS_ATOM_INT(_dec_26158)) {
        _frac_26226 = (_num_26200 % _dec_26158) ? NewDouble((double)_num_26200 / _dec_26158) : (_num_26200 / _dec_26158);
    }
    else {
        if (IS_ATOM_INT(_num_26200)) {
            _frac_26226 = NewDouble((double)_num_26200 / DBL_PTR(_dec_26158)->dbl);
        }
        else {
            if (IS_ATOM_INT(_dec_26158)) {
                _frac_26226 = NewDouble(DBL_PTR(_num_26200)->dbl / (double)_dec_26158);
            }
            else
            _frac_26226 = NewDouble(DBL_PTR(_num_26200)->dbl / DBL_PTR(_dec_26158)->dbl);
        }
    }

    /** 		mantissa += frac  */
    _0 = _mantissa_26155;
    if (IS_ATOM_INT(_mantissa_26155) && IS_ATOM_INT(_frac_26226)) {
        _mantissa_26155 = _mantissa_26155 + _frac_26226;
        if ((long)((unsigned long)_mantissa_26155 + (unsigned long)HIGH_BITS) >= 0) 
        _mantissa_26155 = NewDouble((double)_mantissa_26155);
    }
    else {
        if (IS_ATOM_INT(_mantissa_26155)) {
            _mantissa_26155 = NewDouble((double)_mantissa_26155 + DBL_PTR(_frac_26226)->dbl);
        }
        else {
            if (IS_ATOM_INT(_frac_26226)) {
                _mantissa_26155 = NewDouble(DBL_PTR(_mantissa_26155)->dbl + (double)_frac_26226);
            }
            else
            _mantissa_26155 = NewDouble(DBL_PTR(_mantissa_26155)->dbl + DBL_PTR(_frac_26226)->dbl);
        }
    }
    DeRef(_0);

    /** 		if frac = 0 and not_zero then*/
    if (IS_ATOM_INT(_frac_26226)) {
        _15031 = (_frac_26226 == 0);
    }
    else {
        _15031 = (DBL_PTR(_frac_26226)->dbl == (double)0);
    }
    if (_15031 == 0) {
        goto LE; // [393] 408
    }
    if (_not_zero_26188 == 0)
    {
        goto LE; // [398] 408
    }
    else{
    }

    /** 			fenv:raise(fenv:FE_UNDERFLOW)*/
    _15033 = _62raise(117);
LE: 
L7: 
    DeRef(_back_dec_26197);
    _back_dec_26197 = NOVALUE;
    DeRef(_num_back_26199);
    _num_back_26199 = NOVALUE;
    DeRef(_num_26200);
    _num_26200 = NOVALUE;
    DeRef(_frac_26226);
    _frac_26226 = NOVALUE;

    /** 	if ndigits = 0 then*/
    if (_ndigits_26153 != 0)
    goto LF; // [413] 425

    /** 		CompileErr(121)  -- no digits*/
    RefDS(_22682);
    _43CompileErr(121, _22682, 0);
LF: 

    /** 	label "floating_point_check"*/
G4:

    /** 	if fenv:test(fenv:FE_UNDERFLOW) then*/
    _15035 = _62test(117);
    if (_15035 == 0) {
        DeRef(_15035);
        _15035 = NOVALUE;
        goto L10; // [435] 450
    }
    else {
        if (!IS_ATOM_INT(_15035) && DBL_PTR(_15035)->dbl == 0.0){
            DeRef(_15035);
            _15035 = NOVALUE;
            goto L10; // [435] 450
        }
        DeRef(_15035);
        _15035 = NOVALUE;
    }
    DeRef(_15035);
    _15035 = NOVALUE;

    /** 		CompileErr(NUMBER_IS_TOO_SMALL)*/
    RefDS(_22682);
    _43CompileErr(356, _22682, 0);
    goto L11; // [447] 476
L10: 

    /** 	elsif fenv:test(fenv:FE_OVERFLOW) or real_overflow then -- ex = {FE_OVERFLOW}*/
    _15036 = _62test(111);
    if (IS_ATOM_INT(_15036)) {
        if (_15036 != 0) {
            goto L12; // [456] 465
        }
    }
    else {
        if (DBL_PTR(_15036)->dbl != 0.0) {
            goto L12; // [456] 465
        }
    }
    if (_real_overflow_26160 == 0)
    {
        goto L13; // [461] 475
    }
    else{
    }
L12: 

    /** 		CompileErr(NUMBER_IS_TOO_BIG)*/
    RefDS(_22682);
    _43CompileErr(357, _22682, 0);
L13: 
L11: 

    /** 	return mantissa*/
    DeRefDS(_yytext_26151);
    DeRef(_dec_26158);
    DeRef(_14984);
    _14984 = NOVALUE;
    DeRef(_14993);
    _14993 = NOVALUE;
    DeRef(_15003);
    _15003 = NOVALUE;
    DeRef(_15010);
    _15010 = NOVALUE;
    DeRef(_15022);
    _15022 = NOVALUE;
    DeRef(_15023);
    _15023 = NOVALUE;
    DeRef(_15031);
    _15031 = NOVALUE;
    DeRef(_15033);
    _15033 = NOVALUE;
    DeRef(_15036);
    _15036 = NOVALUE;
    return _mantissa_26155;
    ;
}


void _60maybe_namespace()
{
    int _0, _1, _2;
    

    /** 	might_be_namespace = 1*/
    _60might_be_namespace_26245 = 1;

    /** end procedure*/
    return;
    ;
}


int _60ExtendedString(int _ech_26255)
{
    int _ch_26256 = NOVALUE;
    int _fch_26257 = NOVALUE;
    int _cline_26258 = NOVALUE;
    int _string_text_26259 = NOVALUE;
    int _trimming_26260 = NOVALUE;
    int _15088 = NOVALUE;
    int _15087 = NOVALUE;
    int _15085 = NOVALUE;
    int _15084 = NOVALUE;
    int _15083 = NOVALUE;
    int _15082 = NOVALUE;
    int _15081 = NOVALUE;
    int _15080 = NOVALUE;
    int _15079 = NOVALUE;
    int _15078 = NOVALUE;
    int _15076 = NOVALUE;
    int _15075 = NOVALUE;
    int _15074 = NOVALUE;
    int _15073 = NOVALUE;
    int _15072 = NOVALUE;
    int _15071 = NOVALUE;
    int _15068 = NOVALUE;
    int _15067 = NOVALUE;
    int _15066 = NOVALUE;
    int _15064 = NOVALUE;
    int _15063 = NOVALUE;
    int _15062 = NOVALUE;
    int _15061 = NOVALUE;
    int _15058 = NOVALUE;
    int _15043 = NOVALUE;
    int _15041 = NOVALUE;
    int _0, _1, _2;
    

    /** 	cline = line_number*/
    _cline_26258 = _25line_number_12263;

    /** 	string_text = ""*/
    RefDS(_5);
    DeRefi(_string_text_26259);
    _string_text_26259 = _5;

    /** 	trimming = 0*/
    _trimming_26260 = 0;

    /** 	ch = getch()*/
    _ch_26256 = _60getch();
    if (!IS_ATOM_INT(_ch_26256)) {
        _1 = (long)(DBL_PTR(_ch_26256)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26256)) && (DBL_PTR(_ch_26256)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26256);
        _ch_26256 = _1;
    }

    /** 	if bp > length(ThisLine) then*/
    if (IS_SEQUENCE(_43ThisLine_49532)){
            _15041 = SEQ_PTR(_43ThisLine_49532)->length;
    }
    else {
        _15041 = 1;
    }
    if (_43bp_49536 <= _15041)
    goto L1; // [40] 101

    /** 		read_line()*/
    _60read_line();

    /** 		while ThisLine[bp] = '_' do*/
L2: 
    _2 = (int)SEQ_PTR(_43ThisLine_49532);
    _15043 = (int)*(((s1_ptr)_2)->base + _43bp_49536);
    if (binary_op_a(NOTEQ, _15043, 95)){
        _15043 = NOVALUE;
        goto L3; // [61] 86
    }
    _15043 = NOVALUE;

    /** 			trimming += 1*/
    _trimming_26260 = _trimming_26260 + 1;

    /** 			bp += 1*/
    _43bp_49536 = _43bp_49536 + 1;

    /** 		end while*/
    goto L2; // [83] 53
L3: 

    /** 		if trimming > 0 then*/
    if (_trimming_26260 <= 0)
    goto L4; // [88] 100

    /** 			ch = getch()*/
    _ch_26256 = _60getch();
    if (!IS_ATOM_INT(_ch_26256)) {
        _1 = (long)(DBL_PTR(_ch_26256)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26256)) && (DBL_PTR(_ch_26256)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26256);
        _ch_26256 = _1;
    }
L4: 
L1: 

    /** 	while 1 do*/
L5: 

    /** 		if ch = END_OF_FILE_CHAR then*/
    if (_ch_26256 != 26)
    goto L6; // [110] 122

    /** 			CompileErr(129, cline)*/
    _43CompileErr(129, _cline_26258, 0);
L6: 

    /** 		if ch = ech then*/
    if (_ch_26256 != _ech_26255)
    goto L7; // [124] 180

    /** 			if ech != '"' then*/
    if (_ech_26255 == 34)
    goto L8; // [130] 139

    /** 				exit*/
    goto L9; // [136] 310
L8: 

    /** 			fch = getch()*/
    _fch_26257 = _60getch();
    if (!IS_ATOM_INT(_fch_26257)) {
        _1 = (long)(DBL_PTR(_fch_26257)->dbl);
        if (UNIQUE(DBL_PTR(_fch_26257)) && (DBL_PTR(_fch_26257)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fch_26257);
        _fch_26257 = _1;
    }

    /** 			if fch = '"' then*/
    if (_fch_26257 != 34)
    goto LA; // [148] 175

    /** 				fch = getch()*/
    _fch_26257 = _60getch();
    if (!IS_ATOM_INT(_fch_26257)) {
        _1 = (long)(DBL_PTR(_fch_26257)->dbl);
        if (UNIQUE(DBL_PTR(_fch_26257)) && (DBL_PTR(_fch_26257)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fch_26257);
        _fch_26257 = _1;
    }

    /** 				if fch = '"' then*/
    if (_fch_26257 != 34)
    goto LB; // [161] 170

    /** 					exit*/
    goto L9; // [167] 310
LB: 

    /** 				ungetch()*/
    _60ungetch();
LA: 

    /** 			ungetch()*/
    _60ungetch();
L7: 

    /** 		if ch != '\r' then*/
    if (_ch_26256 == 13)
    goto LC; // [182] 193

    /** 			string_text &= ch*/
    Append(&_string_text_26259, _string_text_26259, _ch_26256);
LC: 

    /** 		if bp > length(ThisLine) then*/
    if (IS_SEQUENCE(_43ThisLine_49532)){
            _15058 = SEQ_PTR(_43ThisLine_49532)->length;
    }
    else {
        _15058 = 1;
    }
    if (_43bp_49536 <= _15058)
    goto LD; // [202] 298

    /** 			read_line() -- sets bp to 1, btw.*/
    _60read_line();

    /** 			if trimming > 0 then*/
    if (_trimming_26260 <= 0)
    goto LE; // [212] 297

    /** 				while bp <= trimming and bp <= length(ThisLine) do*/
LF: 
    _15061 = (_43bp_49536 <= _trimming_26260);
    if (_15061 == 0) {
        goto L10; // [227] 296
    }
    if (IS_SEQUENCE(_43ThisLine_49532)){
            _15063 = SEQ_PTR(_43ThisLine_49532)->length;
    }
    else {
        _15063 = 1;
    }
    _15064 = (_43bp_49536 <= _15063);
    _15063 = NOVALUE;
    if (_15064 == 0)
    {
        DeRef(_15064);
        _15064 = NOVALUE;
        goto L10; // [243] 296
    }
    else{
        DeRef(_15064);
        _15064 = NOVALUE;
    }

    /** 					ch = ThisLine[bp]*/
    _2 = (int)SEQ_PTR(_43ThisLine_49532);
    _ch_26256 = (int)*(((s1_ptr)_2)->base + _43bp_49536);
    if (!IS_ATOM_INT(_ch_26256)){
        _ch_26256 = (long)DBL_PTR(_ch_26256)->dbl;
    }

    /** 					if ch != ' ' and ch != '\t' then*/
    _15066 = (_ch_26256 != 32);
    if (_15066 == 0) {
        goto L11; // [264] 281
    }
    _15068 = (_ch_26256 != 9);
    if (_15068 == 0)
    {
        DeRef(_15068);
        _15068 = NOVALUE;
        goto L11; // [273] 281
    }
    else{
        DeRef(_15068);
        _15068 = NOVALUE;
    }

    /** 						exit*/
    goto L10; // [278] 296
L11: 

    /** 					bp += 1*/
    _43bp_49536 = _43bp_49536 + 1;

    /** 				end while*/
    goto LF; // [293] 221
L10: 
LE: 
LD: 

    /** 		ch = getch()*/
    _ch_26256 = _60getch();
    if (!IS_ATOM_INT(_ch_26256)) {
        _1 = (long)(DBL_PTR(_ch_26256)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26256)) && (DBL_PTR(_ch_26256)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26256);
        _ch_26256 = _1;
    }

    /** 	end while*/
    goto L5; // [307] 106
L9: 

    /** 	if length(string_text) > 0 and string_text[1] = '\n' then*/
    if (IS_SEQUENCE(_string_text_26259)){
            _15071 = SEQ_PTR(_string_text_26259)->length;
    }
    else {
        _15071 = 1;
    }
    _15072 = (_15071 > 0);
    _15071 = NOVALUE;
    if (_15072 == 0) {
        goto L12; // [319] 389
    }
    _2 = (int)SEQ_PTR(_string_text_26259);
    _15074 = (int)*(((s1_ptr)_2)->base + 1);
    _15075 = (_15074 == 10);
    _15074 = NOVALUE;
    if (_15075 == 0)
    {
        DeRef(_15075);
        _15075 = NOVALUE;
        goto L12; // [332] 389
    }
    else{
        DeRef(_15075);
        _15075 = NOVALUE;
    }

    /** 		string_text = string_text[2 .. $]*/
    if (IS_SEQUENCE(_string_text_26259)){
            _15076 = SEQ_PTR(_string_text_26259)->length;
    }
    else {
        _15076 = 1;
    }
    rhs_slice_target = (object_ptr)&_string_text_26259;
    RHS_Slice(_string_text_26259, 2, _15076);

    /** 		if length(string_text) > 0 and string_text[$] = '\n' then*/
    if (IS_SEQUENCE(_string_text_26259)){
            _15078 = SEQ_PTR(_string_text_26259)->length;
    }
    else {
        _15078 = 1;
    }
    _15079 = (_15078 > 0);
    _15078 = NOVALUE;
    if (_15079 == 0) {
        goto L13; // [354] 388
    }
    if (IS_SEQUENCE(_string_text_26259)){
            _15081 = SEQ_PTR(_string_text_26259)->length;
    }
    else {
        _15081 = 1;
    }
    _2 = (int)SEQ_PTR(_string_text_26259);
    _15082 = (int)*(((s1_ptr)_2)->base + _15081);
    _15083 = (_15082 == 10);
    _15082 = NOVALUE;
    if (_15083 == 0)
    {
        DeRef(_15083);
        _15083 = NOVALUE;
        goto L13; // [370] 388
    }
    else{
        DeRef(_15083);
        _15083 = NOVALUE;
    }

    /** 			string_text = string_text[1 .. $-1]*/
    if (IS_SEQUENCE(_string_text_26259)){
            _15084 = SEQ_PTR(_string_text_26259)->length;
    }
    else {
        _15084 = 1;
    }
    _15085 = _15084 - 1;
    _15084 = NOVALUE;
    rhs_slice_target = (object_ptr)&_string_text_26259;
    RHS_Slice(_string_text_26259, 1, _15085);
L13: 
L12: 

    /** 	return {STRING, NewStringSym(string_text)}*/
    RefDS(_string_text_26259);
    _15087 = _52NewStringSym(_string_text_26259);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 503;
    ((int *)_2)[2] = _15087;
    _15088 = MAKE_SEQ(_1);
    _15087 = NOVALUE;
    DeRefDSi(_string_text_26259);
    DeRef(_15061);
    _15061 = NOVALUE;
    DeRef(_15066);
    _15066 = NOVALUE;
    DeRef(_15072);
    _15072 = NOVALUE;
    DeRef(_15079);
    _15079 = NOVALUE;
    DeRef(_15085);
    _15085 = NOVALUE;
    return _15088;
    ;
}


int _60GetHexString(int _maxnibbles_26346)
{
    int _ch_26347 = NOVALUE;
    int _digit_26348 = NOVALUE;
    int _val_26349 = NOVALUE;
    int _cline_26350 = NOVALUE;
    int _nibble_26351 = NOVALUE;
    int _string_text_26352 = NOVALUE;
    int _15102 = NOVALUE;
    int _15101 = NOVALUE;
    int _0, _1, _2;
    

    /** 	cline = line_number*/
    _cline_26350 = _25line_number_12263;

    /** 	string_text = ""*/
    RefDS(_5);
    DeRef(_string_text_26352);
    _string_text_26352 = _5;

    /** 	nibble = 1*/
    _nibble_26351 = 1;

    /** 	val = -1*/
    DeRef(_val_26349);
    _val_26349 = -1;

    /** 	ch = getch()*/
    _ch_26347 = _60getch();
    if (!IS_ATOM_INT(_ch_26347)) {
        _1 = (long)(DBL_PTR(_ch_26347)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26347)) && (DBL_PTR(_ch_26347)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26347);
        _ch_26347 = _1;
    }

    /** 	while 1 do*/
L1: 

    /** 		if ch = END_OF_FILE_CHAR then*/
    if (_ch_26347 != 26)
    goto L2; // [45] 57

    /** 			CompileErr(129, cline)*/
    _43CompileErr(129, _cline_26350, 0);
L2: 

    /** 		if ch = '"' then*/
    if (_ch_26347 != 34)
    goto L3; // [59] 68

    /** 			exit*/
    goto L4; // [65] 224
L3: 

    /** 		digit = find(ch, "0123456789ABCDEFabcdef_ \t\n\r")*/
    _digit_26348 = find_from(_ch_26347, _15092, 1);

    /** 		if digit = 0 then*/
    if (_digit_26348 != 0)
    goto L5; // [77] 89

    /** 			CompileErr(329)*/
    RefDS(_22682);
    _43CompileErr(329, _22682, 0);
L5: 

    /** 		if digit <= 23 then*/
    if (_digit_26348 > 23)
    goto L6; // [91] 177

    /** 			if digit != 23 then*/
    if (_digit_26348 == 23)
    goto L7; // [97] 212

    /** 				if digit > 16 then*/
    if (_digit_26348 <= 16)
    goto L8; // [103] 114

    /** 					digit -= 6*/
    _digit_26348 = _digit_26348 - 6;
L8: 

    /** 				if nibble = 1 then*/
    if (_nibble_26351 != 1)
    goto L9; // [116] 129

    /** 					val = digit - 1*/
    DeRef(_val_26349);
    _val_26349 = _digit_26348 - 1;
    if ((long)((unsigned long)_val_26349 +(unsigned long) HIGH_BITS) >= 0){
        _val_26349 = NewDouble((double)_val_26349);
    }
    goto LA; // [126] 167
L9: 

    /** 					val = val * 16 + digit - 1*/
    if (IS_ATOM_INT(_val_26349)) {
        if (_val_26349 == (short)_val_26349)
        _15101 = _val_26349 * 16;
        else
        _15101 = NewDouble(_val_26349 * (double)16);
    }
    else {
        _15101 = NewDouble(DBL_PTR(_val_26349)->dbl * (double)16);
    }
    if (IS_ATOM_INT(_15101)) {
        _15102 = _15101 + _digit_26348;
        if ((long)((unsigned long)_15102 + (unsigned long)HIGH_BITS) >= 0) 
        _15102 = NewDouble((double)_15102);
    }
    else {
        _15102 = NewDouble(DBL_PTR(_15101)->dbl + (double)_digit_26348);
    }
    DeRef(_15101);
    _15101 = NOVALUE;
    DeRef(_val_26349);
    if (IS_ATOM_INT(_15102)) {
        _val_26349 = _15102 - 1;
        if ((long)((unsigned long)_val_26349 +(unsigned long) HIGH_BITS) >= 0){
            _val_26349 = NewDouble((double)_val_26349);
        }
    }
    else {
        _val_26349 = NewDouble(DBL_PTR(_15102)->dbl - (double)1);
    }
    DeRef(_15102);
    _15102 = NOVALUE;

    /** 					if nibble = maxnibbles then*/
    if (_nibble_26351 != _maxnibbles_26346)
    goto LB; // [145] 166

    /** 						string_text &= val*/
    Ref(_val_26349);
    Append(&_string_text_26352, _string_text_26352, _val_26349);

    /** 						val = -1*/
    DeRef(_val_26349);
    _val_26349 = -1;

    /** 						nibble = 0*/
    _nibble_26351 = 0;
LB: 
LA: 

    /** 				nibble += 1*/
    _nibble_26351 = _nibble_26351 + 1;
    goto L7; // [174] 212
L6: 

    /** 			if val >= 0 then*/
    if (binary_op_a(LESS, _val_26349, 0)){
        goto LC; // [179] 195
    }

    /** 				string_text &= val*/
    Ref(_val_26349);
    Append(&_string_text_26352, _string_text_26352, _val_26349);

    /** 				val = -1*/
    DeRef(_val_26349);
    _val_26349 = -1;
LC: 

    /** 			nibble = 1*/
    _nibble_26351 = 1;

    /** 			if ch = '\n' then*/
    if (_ch_26347 != 10)
    goto LD; // [202] 211

    /** 				read_line()*/
    _60read_line();
LD: 
L7: 

    /** 		ch = getch()*/
    _ch_26347 = _60getch();
    if (!IS_ATOM_INT(_ch_26347)) {
        _1 = (long)(DBL_PTR(_ch_26347)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26347)) && (DBL_PTR(_ch_26347)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26347);
        _ch_26347 = _1;
    }

    /** 	end while*/
    goto L1; // [221] 41
L4: 

    /** 	if val >= 0 then	*/
    if (binary_op_a(LESS, _val_26349, 0)){
        goto LE; // [226] 237
    }

    /** 		string_text &= val*/
    Ref(_val_26349);
    Append(&_string_text_26352, _string_text_26352, _val_26349);
LE: 

    /** 	return string_text*/
    DeRef(_val_26349);
    return _string_text_26352;
    ;
}


int _60GetBitString()
{
    int _ch_26397 = NOVALUE;
    int _digit_26398 = NOVALUE;
    int _val_26399 = NOVALUE;
    int _cline_26400 = NOVALUE;
    int _bitcnt_26401 = NOVALUE;
    int _string_text_26402 = NOVALUE;
    int _15124 = NOVALUE;
    int _15123 = NOVALUE;
    int _0, _1, _2;
    

    /** 	cline = line_number*/
    _cline_26400 = _25line_number_12263;

    /** 	string_text = ""*/
    RefDS(_5);
    DeRef(_string_text_26402);
    _string_text_26402 = _5;

    /** 	bitcnt = 1*/
    _bitcnt_26401 = 1;

    /** 	val = -1*/
    DeRef(_val_26399);
    _val_26399 = -1;

    /** 	ch = getch()*/
    _ch_26397 = _60getch();
    if (!IS_ATOM_INT(_ch_26397)) {
        _1 = (long)(DBL_PTR(_ch_26397)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26397)) && (DBL_PTR(_ch_26397)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26397);
        _ch_26397 = _1;
    }

    /** 	while 1 do*/
L1: 

    /** 		if ch = END_OF_FILE_CHAR then*/
    if (_ch_26397 != 26)
    goto L2; // [43] 55

    /** 			CompileErr(129, cline)*/
    _43CompileErr(129, _cline_26400, 0);
L2: 

    /** 		if ch = '"' then*/
    if (_ch_26397 != 34)
    goto L3; // [57] 66

    /** 			exit*/
    goto L4; // [63] 186
L3: 

    /** 		digit = find(ch, "01_ \t\n\r")*/
    _digit_26398 = find_from(_ch_26397, _15116, 1);

    /** 		if digit = 0 then*/
    if (_digit_26398 != 0)
    goto L5; // [75] 87

    /** 			CompileErr(329)*/
    RefDS(_22682);
    _43CompileErr(329, _22682, 0);
L5: 

    /** 		if digit <= 3 then*/
    if (_digit_26398 > 3)
    goto L6; // [89] 139

    /** 			if digit != 3 then*/
    if (_digit_26398 == 3)
    goto L7; // [95] 174

    /** 				if bitcnt = 1 then*/
    if (_bitcnt_26401 != 1)
    goto L8; // [101] 114

    /** 					val = digit - 1*/
    DeRef(_val_26399);
    _val_26399 = _digit_26398 - 1;
    if ((long)((unsigned long)_val_26399 +(unsigned long) HIGH_BITS) >= 0){
        _val_26399 = NewDouble((double)_val_26399);
    }
    goto L9; // [111] 129
L8: 

    /** 					val = val * 2 + digit - 1*/
    if (IS_ATOM_INT(_val_26399) && IS_ATOM_INT(_val_26399)) {
        _15123 = _val_26399 + _val_26399;
        if ((long)((unsigned long)_15123 + (unsigned long)HIGH_BITS) >= 0) 
        _15123 = NewDouble((double)_15123);
    }
    else {
        if (IS_ATOM_INT(_val_26399)) {
            _15123 = NewDouble((double)_val_26399 + DBL_PTR(_val_26399)->dbl);
        }
        else {
            if (IS_ATOM_INT(_val_26399)) {
                _15123 = NewDouble(DBL_PTR(_val_26399)->dbl + (double)_val_26399);
            }
            else
            _15123 = NewDouble(DBL_PTR(_val_26399)->dbl + DBL_PTR(_val_26399)->dbl);
        }
    }
    if (IS_ATOM_INT(_15123)) {
        _15124 = _15123 + _digit_26398;
        if ((long)((unsigned long)_15124 + (unsigned long)HIGH_BITS) >= 0) 
        _15124 = NewDouble((double)_15124);
    }
    else {
        _15124 = NewDouble(DBL_PTR(_15123)->dbl + (double)_digit_26398);
    }
    DeRef(_15123);
    _15123 = NOVALUE;
    DeRef(_val_26399);
    if (IS_ATOM_INT(_15124)) {
        _val_26399 = _15124 - 1;
        if ((long)((unsigned long)_val_26399 +(unsigned long) HIGH_BITS) >= 0){
            _val_26399 = NewDouble((double)_val_26399);
        }
    }
    else {
        _val_26399 = NewDouble(DBL_PTR(_15124)->dbl - (double)1);
    }
    DeRef(_15124);
    _15124 = NOVALUE;
L9: 

    /** 				bitcnt += 1*/
    _bitcnt_26401 = _bitcnt_26401 + 1;
    goto L7; // [136] 174
L6: 

    /** 			if val >= 0 then*/
    if (binary_op_a(LESS, _val_26399, 0)){
        goto LA; // [141] 157
    }

    /** 				string_text &= val*/
    Ref(_val_26399);
    Append(&_string_text_26402, _string_text_26402, _val_26399);

    /** 				val = -1*/
    DeRef(_val_26399);
    _val_26399 = -1;
LA: 

    /** 			bitcnt = 1*/
    _bitcnt_26401 = 1;

    /** 			if ch = '\n' then*/
    if (_ch_26397 != 10)
    goto LB; // [164] 173

    /** 				read_line()*/
    _60read_line();
LB: 
L7: 

    /** 		ch = getch()*/
    _ch_26397 = _60getch();
    if (!IS_ATOM_INT(_ch_26397)) {
        _1 = (long)(DBL_PTR(_ch_26397)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26397)) && (DBL_PTR(_ch_26397)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26397);
        _ch_26397 = _1;
    }

    /** 	end while*/
    goto L1; // [183] 39
L4: 

    /** 	if val >= 0 then	*/
    if (binary_op_a(LESS, _val_26399, 0)){
        goto LC; // [188] 199
    }

    /** 		string_text &= val*/
    Ref(_val_26399);
    Append(&_string_text_26402, _string_text_26402, _val_26399);
LC: 

    /** 	return string_text*/
    DeRef(_val_26399);
    return _string_text_26402;
    ;
}


int _60Scanner()
{
    int _fwd_inlined_set_qualified_fwd_at_441_26540 = NOVALUE;
    int _ch_26441 = NOVALUE;
    int _i_26442 = NOVALUE;
    int _sp_26443 = NOVALUE;
    int _prev_Nne_26444 = NOVALUE;
    int _pch_26445 = NOVALUE;
    int _cline_26446 = NOVALUE;
    int _yytext_26447 = NOVALUE;
    int _namespaces_26448 = NOVALUE;
    int _d_26449 = NOVALUE;
    int _tok_26451 = NOVALUE;
    int _is_int_26452 = NOVALUE;
    int _class_26453 = NOVALUE;
    int _name_26454 = NOVALUE;
    int _is_namespace_26513 = NOVALUE;
    int _basetype_26747 = NOVALUE;
    int _hdigit_26787 = NOVALUE;
    int _fch_26932 = NOVALUE;
    int _cnest_27118 = NOVALUE;
    int _ach_27147 = NOVALUE;
    int _32397 = NOVALUE;
    int _32396 = NOVALUE;
    int _32395 = NOVALUE;
    int _32394 = NOVALUE;
    int _32393 = NOVALUE;
    int _32392 = NOVALUE;
    int _32391 = NOVALUE;
    int _15525 = NOVALUE;
    int _15524 = NOVALUE;
    int _15522 = NOVALUE;
    int _15520 = NOVALUE;
    int _15519 = NOVALUE;
    int _15518 = NOVALUE;
    int _15516 = NOVALUE;
    int _15515 = NOVALUE;
    int _15514 = NOVALUE;
    int _15513 = NOVALUE;
    int _15511 = NOVALUE;
    int _15510 = NOVALUE;
    int _15508 = NOVALUE;
    int _15506 = NOVALUE;
    int _15505 = NOVALUE;
    int _15503 = NOVALUE;
    int _15501 = NOVALUE;
    int _15500 = NOVALUE;
    int _15498 = NOVALUE;
    int _15496 = NOVALUE;
    int _15495 = NOVALUE;
    int _15494 = NOVALUE;
    int _15493 = NOVALUE;
    int _15492 = NOVALUE;
    int _15490 = NOVALUE;
    int _15489 = NOVALUE;
    int _15479 = NOVALUE;
    int _15466 = NOVALUE;
    int _15462 = NOVALUE;
    int _15461 = NOVALUE;
    int _15457 = NOVALUE;
    int _15456 = NOVALUE;
    int _15455 = NOVALUE;
    int _15454 = NOVALUE;
    int _15452 = NOVALUE;
    int _15451 = NOVALUE;
    int _15450 = NOVALUE;
    int _15449 = NOVALUE;
    int _15446 = NOVALUE;
    int _15445 = NOVALUE;
    int _15444 = NOVALUE;
    int _15443 = NOVALUE;
    int _15442 = NOVALUE;
    int _15441 = NOVALUE;
    int _15439 = NOVALUE;
    int _15438 = NOVALUE;
    int _15437 = NOVALUE;
    int _15436 = NOVALUE;
    int _15435 = NOVALUE;
    int _15434 = NOVALUE;
    int _15432 = NOVALUE;
    int _15431 = NOVALUE;
    int _15428 = NOVALUE;
    int _15425 = NOVALUE;
    int _15420 = NOVALUE;
    int _15419 = NOVALUE;
    int _15418 = NOVALUE;
    int _15417 = NOVALUE;
    int _15416 = NOVALUE;
    int _15415 = NOVALUE;
    int _15413 = NOVALUE;
    int _15412 = NOVALUE;
    int _15411 = NOVALUE;
    int _15410 = NOVALUE;
    int _15409 = NOVALUE;
    int _15408 = NOVALUE;
    int _15406 = NOVALUE;
    int _15405 = NOVALUE;
    int _15402 = NOVALUE;
    int _15399 = NOVALUE;
    int _15398 = NOVALUE;
    int _15396 = NOVALUE;
    int _15395 = NOVALUE;
    int _15391 = NOVALUE;
    int _15390 = NOVALUE;
    int _15386 = NOVALUE;
    int _15385 = NOVALUE;
    int _15384 = NOVALUE;
    int _15382 = NOVALUE;
    int _15377 = NOVALUE;
    int _15374 = NOVALUE;
    int _15373 = NOVALUE;
    int _15372 = NOVALUE;
    int _15371 = NOVALUE;
    int _15365 = NOVALUE;
    int _15363 = NOVALUE;
    int _15358 = NOVALUE;
    int _15357 = NOVALUE;
    int _15356 = NOVALUE;
    int _15355 = NOVALUE;
    int _15354 = NOVALUE;
    int _15353 = NOVALUE;
    int _15352 = NOVALUE;
    int _15350 = NOVALUE;
    int _15348 = NOVALUE;
    int _15347 = NOVALUE;
    int _15346 = NOVALUE;
    int _15345 = NOVALUE;
    int _15344 = NOVALUE;
    int _15343 = NOVALUE;
    int _15341 = NOVALUE;
    int _15340 = NOVALUE;
    int _15336 = NOVALUE;
    int _15335 = NOVALUE;
    int _15334 = NOVALUE;
    int _15333 = NOVALUE;
    int _15332 = NOVALUE;
    int _15330 = NOVALUE;
    int _15329 = NOVALUE;
    int _15327 = NOVALUE;
    int _15323 = NOVALUE;
    int _15320 = NOVALUE;
    int _15319 = NOVALUE;
    int _15317 = NOVALUE;
    int _15316 = NOVALUE;
    int _15315 = NOVALUE;
    int _15312 = NOVALUE;
    int _15310 = NOVALUE;
    int _15309 = NOVALUE;
    int _15305 = NOVALUE;
    int _15301 = NOVALUE;
    int _15298 = NOVALUE;
    int _15292 = NOVALUE;
    int _15284 = NOVALUE;
    int _15283 = NOVALUE;
    int _15282 = NOVALUE;
    int _15280 = NOVALUE;
    int _15277 = NOVALUE;
    int _15275 = NOVALUE;
    int _15273 = NOVALUE;
    int _15270 = NOVALUE;
    int _15267 = NOVALUE;
    int _15265 = NOVALUE;
    int _15263 = NOVALUE;
    int _15261 = NOVALUE;
    int _15260 = NOVALUE;
    int _15256 = NOVALUE;
    int _15253 = NOVALUE;
    int _15251 = NOVALUE;
    int _15248 = NOVALUE;
    int _15241 = NOVALUE;
    int _15239 = NOVALUE;
    int _15236 = NOVALUE;
    int _15230 = NOVALUE;
    int _15229 = NOVALUE;
    int _15228 = NOVALUE;
    int _15227 = NOVALUE;
    int _15226 = NOVALUE;
    int _15225 = NOVALUE;
    int _15224 = NOVALUE;
    int _15222 = NOVALUE;
    int _15220 = NOVALUE;
    int _15218 = NOVALUE;
    int _15216 = NOVALUE;
    int _15214 = NOVALUE;
    int _15213 = NOVALUE;
    int _15212 = NOVALUE;
    int _15211 = NOVALUE;
    int _15210 = NOVALUE;
    int _15208 = NOVALUE;
    int _15205 = NOVALUE;
    int _15203 = NOVALUE;
    int _15202 = NOVALUE;
    int _15201 = NOVALUE;
    int _15199 = NOVALUE;
    int _15194 = NOVALUE;
    int _15190 = NOVALUE;
    int _15187 = NOVALUE;
    int _15185 = NOVALUE;
    int _15184 = NOVALUE;
    int _15182 = NOVALUE;
    int _15180 = NOVALUE;
    int _15178 = NOVALUE;
    int _15177 = NOVALUE;
    int _15176 = NOVALUE;
    int _15174 = NOVALUE;
    int _15169 = NOVALUE;
    int _15166 = NOVALUE;
    int _15164 = NOVALUE;
    int _15161 = NOVALUE;
    int _15160 = NOVALUE;
    int _15158 = NOVALUE;
    int _15157 = NOVALUE;
    int _15156 = NOVALUE;
    int _15155 = NOVALUE;
    int _15154 = NOVALUE;
    int _15153 = NOVALUE;
    int _15152 = NOVALUE;
    int _15151 = NOVALUE;
    int _15150 = NOVALUE;
    int _15149 = NOVALUE;
    int _15148 = NOVALUE;
    int _15147 = NOVALUE;
    int _15146 = NOVALUE;
    int _15141 = NOVALUE;
    int _15139 = NOVALUE;
    int _15136 = NOVALUE;
    int _15134 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer is_int, class*/

    /** 	sequence name*/

    /** 	while TRUE do*/
L1: 
    if (_5TRUE_244 == 0)
    {
        goto L2; // [12] 3868
    }
    else{
    }

    /** 		ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 		while ch = ' ' or ch = '\t' do*/
L3: 
    _15134 = (_ch_26441 == 32);
    if (_15134 != 0) {
        goto L4; // [31] 44
    }
    _15136 = (_ch_26441 == 9);
    if (_15136 == 0)
    {
        DeRef(_15136);
        _15136 = NOVALUE;
        goto L5; // [40] 56
    }
    else{
        DeRef(_15136);
        _15136 = NOVALUE;
    }
L4: 

    /** 			ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 		end while*/
    goto L3; // [53] 27
L5: 

    /** 		class = char_class[ch]*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _class_26453 = (int)*(((s1_ptr)_2)->base + _ch_26441);

    /** 		if class = LETTER or ch = '_' then*/
    _15139 = (_class_26453 == -2);
    if (_15139 != 0) {
        goto L6; // [72] 85
    }
    _15141 = (_ch_26441 == 95);
    if (_15141 == 0)
    {
        DeRef(_15141);
        _15141 = NOVALUE;
        goto L7; // [81] 1282
    }
    else{
        DeRef(_15141);
        _15141 = NOVALUE;
    }
L6: 

    /** 			sp = bp*/
    _sp_26443 = _43bp_49536;

    /** 			pch = ch*/
    _pch_26445 = _ch_26441;

    /** 			ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 			if ch = '"' then*/
    if (_ch_26441 != 34)
    goto L8; // [108] 222

    /** 				switch pch do*/
    _0 = _pch_26445;
    switch ( _0 ){ 

        /** 					case 'x' then*/
        case 120:

        /** 						return {STRING, NewStringSym(GetHexString(2))}*/
        _15146 = _60GetHexString(2);
        _15147 = _52NewStringSym(_15146);
        _15146 = NOVALUE;
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = 503;
        ((int *)_2)[2] = _15147;
        _15148 = MAKE_SEQ(_1);
        _15147 = NOVALUE;
        DeRef(_yytext_26447);
        DeRef(_namespaces_26448);
        DeRef(_d_26449);
        DeRef(_tok_26451);
        DeRef(_name_26454);
        DeRef(_15134);
        _15134 = NOVALUE;
        DeRef(_15139);
        _15139 = NOVALUE;
        return _15148;
        goto L9; // [143] 221

        /** 					case 'u' then*/
        case 117:

        /** 						return {STRING, NewStringSym(GetHexString(4))}*/
        _15149 = _60GetHexString(4);
        _15150 = _52NewStringSym(_15149);
        _15149 = NOVALUE;
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = 503;
        ((int *)_2)[2] = _15150;
        _15151 = MAKE_SEQ(_1);
        _15150 = NOVALUE;
        DeRef(_yytext_26447);
        DeRef(_namespaces_26448);
        DeRef(_d_26449);
        DeRef(_tok_26451);
        DeRef(_name_26454);
        DeRef(_15134);
        _15134 = NOVALUE;
        DeRef(_15139);
        _15139 = NOVALUE;
        DeRef(_15148);
        _15148 = NOVALUE;
        return _15151;
        goto L9; // [169] 221

        /** 					case 'U' then*/
        case 85:

        /** 						return {STRING, NewStringSym(GetHexString(8))}*/
        _15152 = _60GetHexString(8);
        _15153 = _52NewStringSym(_15152);
        _15152 = NOVALUE;
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = 503;
        ((int *)_2)[2] = _15153;
        _15154 = MAKE_SEQ(_1);
        _15153 = NOVALUE;
        DeRef(_yytext_26447);
        DeRef(_namespaces_26448);
        DeRef(_d_26449);
        DeRef(_tok_26451);
        DeRef(_name_26454);
        DeRef(_15134);
        _15134 = NOVALUE;
        DeRef(_15139);
        _15139 = NOVALUE;
        DeRef(_15148);
        _15148 = NOVALUE;
        DeRef(_15151);
        _15151 = NOVALUE;
        return _15154;
        goto L9; // [195] 221

        /** 					case 'b' then*/
        case 98:

        /** 						return {STRING, NewStringSym(GetBitString())}*/
        _15155 = _60GetBitString();
        _15156 = _52NewStringSym(_15155);
        _15155 = NOVALUE;
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = 503;
        ((int *)_2)[2] = _15156;
        _15157 = MAKE_SEQ(_1);
        _15156 = NOVALUE;
        DeRef(_yytext_26447);
        DeRef(_namespaces_26448);
        DeRef(_d_26449);
        DeRef(_tok_26451);
        DeRef(_name_26454);
        DeRef(_15134);
        _15134 = NOVALUE;
        DeRef(_15139);
        _15139 = NOVALUE;
        DeRef(_15148);
        _15148 = NOVALUE;
        DeRef(_15151);
        _15151 = NOVALUE;
        DeRef(_15154);
        _15154 = NOVALUE;
        return _15157;
    ;}L9: 
L8: 

    /** 			while id_char[ch] do*/
LA: 
    _2 = (int)SEQ_PTR(_60id_char_24678);
    _15158 = (int)*(((s1_ptr)_2)->base + _ch_26441);
    if (_15158 == 0)
    {
        _15158 = NOVALUE;
        goto LB; // [233] 248
    }
    else{
        _15158 = NOVALUE;
    }

    /** 				ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 			end while*/
    goto LA; // [245] 227
LB: 

    /** 			yytext = ThisLine[sp-1..bp-2]*/
    _15160 = _sp_26443 - 1;
    if ((long)((unsigned long)_15160 +(unsigned long) HIGH_BITS) >= 0){
        _15160 = NewDouble((double)_15160);
    }
    _15161 = _43bp_49536 - 2;
    rhs_slice_target = (object_ptr)&_yytext_26447;
    RHS_Slice(_43ThisLine_49532, _15160, _15161);

    /** 			ungetch()*/
    _60ungetch();

    /** 			ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 			while ch = ' ' or ch = '\t' do*/
LC: 
    _15164 = (_ch_26441 == 32);
    if (_15164 != 0) {
        goto LD; // [287] 300
    }
    _15166 = (_ch_26441 == 9);
    if (_15166 == 0)
    {
        DeRef(_15166);
        _15166 = NOVALUE;
        goto LE; // [296] 312
    }
    else{
        DeRef(_15166);
        _15166 = NOVALUE;
    }
LD: 

    /** 				ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 			end while*/
    goto LC; // [309] 283
LE: 

    /** 			integer is_namespace*/

    /** 			if might_be_namespace then*/
    if (_60might_be_namespace_26245 == 0)
    {
        goto LF; // [318] 361
    }
    else{
    }

    /** 				tok = keyfind(yytext, -1, , -1 )*/
    RefDS(_yytext_26447);
    _32397 = _52hashfn(_yytext_26447);
    RefDS(_yytext_26447);
    _0 = _tok_26451;
    _tok_26451 = _52keyfind(_yytext_26447, -1, _25current_file_no_12262, -1, _32397);
    DeRef(_0);
    _32397 = NOVALUE;

    /** 				is_namespace = tok[T_ID] = NAMESPACE*/
    _2 = (int)SEQ_PTR(_tok_26451);
    _15169 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_15169)) {
        _is_namespace_26513 = (_15169 == 523);
    }
    else {
        _is_namespace_26513 = binary_op(EQUALS, _15169, 523);
    }
    _15169 = NOVALUE;
    if (!IS_ATOM_INT(_is_namespace_26513)) {
        _1 = (long)(DBL_PTR(_is_namespace_26513)->dbl);
        if (UNIQUE(DBL_PTR(_is_namespace_26513)) && (DBL_PTR(_is_namespace_26513)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_is_namespace_26513);
        _is_namespace_26513 = _1;
    }

    /** 				might_be_namespace = 0*/
    _60might_be_namespace_26245 = 0;
    goto L10; // [358] 384
LF: 

    /** 				is_namespace = ch = ':'*/
    _is_namespace_26513 = (_ch_26441 == 58);

    /** 				tok = keyfind(yytext, -1, , is_namespace )*/
    RefDS(_yytext_26447);
    _32396 = _52hashfn(_yytext_26447);
    RefDS(_yytext_26447);
    _0 = _tok_26451;
    _tok_26451 = _52keyfind(_yytext_26447, -1, _25current_file_no_12262, _is_namespace_26513, _32396);
    DeRef(_0);
    _32396 = NOVALUE;
L10: 

    /** 			if not is_namespace then*/
    if (_is_namespace_26513 != 0)
    goto L11; // [388] 396

    /** 				ungetch()*/
    _60ungetch();
L11: 

    /** 			if is_namespace then*/
    if (_is_namespace_26513 == 0)
    {
        goto L12; // [398] 1119
    }
    else{
    }

    /** 				namespaces = yytext*/
    RefDS(_yytext_26447);
    DeRef(_namespaces_26448);
    _namespaces_26448 = _yytext_26447;

    /** 				if tok[T_ID] = NAMESPACE then -- known namespace*/
    _2 = (int)SEQ_PTR(_tok_26451);
    _15174 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _15174, 523)){
        _15174 = NOVALUE;
        goto L13; // [420] 974
    }
    _15174 = NOVALUE;

    /** 					set_qualified_fwd( SymTab[tok[T_SYM]][S_OBJ] )*/
    _2 = (int)SEQ_PTR(_tok_26451);
    _15176 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_15176)){
        _15177 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_15176)->dbl));
    }
    else{
        _15177 = (int)*(((s1_ptr)_2)->base + _15176);
    }
    _2 = (int)SEQ_PTR(_15177);
    _15178 = (int)*(((s1_ptr)_2)->base + 1);
    _15177 = NOVALUE;
    Ref(_15178);
    DeRef(_fwd_inlined_set_qualified_fwd_at_441_26540);
    _fwd_inlined_set_qualified_fwd_at_441_26540 = _15178;
    _15178 = NOVALUE;
    if (!IS_ATOM_INT(_fwd_inlined_set_qualified_fwd_at_441_26540)) {
        _1 = (long)(DBL_PTR(_fwd_inlined_set_qualified_fwd_at_441_26540)->dbl);
        if (UNIQUE(DBL_PTR(_fwd_inlined_set_qualified_fwd_at_441_26540)) && (DBL_PTR(_fwd_inlined_set_qualified_fwd_at_441_26540)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fwd_inlined_set_qualified_fwd_at_441_26540);
        _fwd_inlined_set_qualified_fwd_at_441_26540 = _1;
    }

    /** 	qualified_fwd = fwd*/
    _60qualified_fwd_24702 = _fwd_inlined_set_qualified_fwd_at_441_26540;

    /** end procedure*/
    goto L14; // [456] 459
L14: 
    DeRef(_fwd_inlined_set_qualified_fwd_at_441_26540);
    _fwd_inlined_set_qualified_fwd_at_441_26540 = NOVALUE;

    /** 					ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 					while ch = ' ' or ch = '\t' do*/
L15: 
    _15180 = (_ch_26441 == 32);
    if (_15180 != 0) {
        goto L16; // [477] 490
    }
    _15182 = (_ch_26441 == 9);
    if (_15182 == 0)
    {
        DeRef(_15182);
        _15182 = NOVALUE;
        goto L17; // [486] 502
    }
    else{
        DeRef(_15182);
        _15182 = NOVALUE;
    }
L16: 

    /** 						ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 					end while*/
    goto L15; // [499] 473
L17: 

    /** 					yytext = ""*/
    RefDS(_5);
    DeRef(_yytext_26447);
    _yytext_26447 = _5;

    /** 					if char_class[ch] = LETTER or ch = '_' then*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _15184 = (int)*(((s1_ptr)_2)->base + _ch_26441);
    _15185 = (_15184 == -2);
    _15184 = NOVALUE;
    if (_15185 != 0) {
        goto L18; // [523] 536
    }
    _15187 = (_ch_26441 == 95);
    if (_15187 == 0)
    {
        DeRef(_15187);
        _15187 = NOVALUE;
        goto L19; // [532] 589
    }
    else{
        DeRef(_15187);
        _15187 = NOVALUE;
    }
L18: 

    /** 						yytext &= ch*/
    Append(&_yytext_26447, _yytext_26447, _ch_26441);

    /** 						ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 						while id_char[ch] = TRUE do*/
L1A: 
    _2 = (int)SEQ_PTR(_60id_char_24678);
    _15190 = (int)*(((s1_ptr)_2)->base + _ch_26441);
    if (_15190 != _5TRUE_244)
    goto L1B; // [562] 584

    /** 							yytext &= ch*/
    Append(&_yytext_26447, _yytext_26447, _ch_26441);

    /** 							ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 						end while*/
    goto L1A; // [581] 554
L1B: 

    /** 						ungetch()*/
    _60ungetch();
L19: 

    /** 					if length(yytext) = 0 then*/
    if (IS_SEQUENCE(_yytext_26447)){
            _15194 = SEQ_PTR(_yytext_26447)->length;
    }
    else {
        _15194 = 1;
    }
    if (_15194 != 0)
    goto L1C; // [594] 606

    /** 						CompileErr(32)*/
    RefDS(_22682);
    _43CompileErr(32, _22682, 0);
L1C: 

    /** 				    if Parser_mode = PAM_RECORD then*/
    if (_25Parser_mode_12388 != 1)
    goto L1D; // [612] 773

    /** 		                Recorded = append(Recorded,yytext)*/
    RefDS(_yytext_26447);
    Append(&_25Recorded_12389, _25Recorded_12389, _yytext_26447);

    /** 		                Ns_recorded = append(Ns_recorded,namespaces)*/
    RefDS(_namespaces_26448);
    Append(&_25Ns_recorded_12390, _25Ns_recorded_12390, _namespaces_26448);

    /** 		                Ns_recorded_sym &= tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_26451);
    _15199 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_25Ns_recorded_sym_12392) && IS_ATOM(_15199)) {
        Ref(_15199);
        Append(&_25Ns_recorded_sym_12392, _25Ns_recorded_sym_12392, _15199);
    }
    else if (IS_ATOM(_25Ns_recorded_sym_12392) && IS_SEQUENCE(_15199)) {
    }
    else {
        Concat((object_ptr)&_25Ns_recorded_sym_12392, _25Ns_recorded_sym_12392, _15199);
    }
    _15199 = NOVALUE;

    /** 		                prev_Nne = No_new_entry*/
    _prev_Nne_26444 = _52No_new_entry_48273;

    /** 						No_new_entry = 1*/
    _52No_new_entry_48273 = 1;

    /** 						tok = keyfind(yytext, SymTab[tok[T_SYM]][S_OBJ])*/
    _2 = (int)SEQ_PTR(_tok_26451);
    _15201 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_15201)){
        _15202 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_15201)->dbl));
    }
    else{
        _15202 = (int)*(((s1_ptr)_2)->base + _15201);
    }
    _2 = (int)SEQ_PTR(_15202);
    _15203 = (int)*(((s1_ptr)_2)->base + 1);
    _15202 = NOVALUE;
    RefDS(_yytext_26447);
    _32395 = _52hashfn(_yytext_26447);
    RefDS(_yytext_26447);
    Ref(_15203);
    _0 = _tok_26451;
    _tok_26451 = _52keyfind(_yytext_26447, _15203, _25current_file_no_12262, 0, _32395);
    DeRef(_0);
    _15203 = NOVALUE;
    _32395 = NOVALUE;

    /** 						if tok[T_ID] = IGNORED then*/
    _2 = (int)SEQ_PTR(_tok_26451);
    _15205 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _15205, 509)){
        _15205 = NOVALUE;
        goto L1E; // [712] 729
    }
    _15205 = NOVALUE;

    /** 							Recorded_sym &= 0 -- must resolve on call site*/
    Append(&_25Recorded_sym_12391, _25Recorded_sym_12391, 0);
    goto L1F; // [726] 746
L1E: 

    /** 							Recorded_sym &= tok[T_SYM] -- fallback when symbol is undefined on call site*/
    _2 = (int)SEQ_PTR(_tok_26451);
    _15208 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_25Recorded_sym_12391) && IS_ATOM(_15208)) {
        Ref(_15208);
        Append(&_25Recorded_sym_12391, _25Recorded_sym_12391, _15208);
    }
    else if (IS_ATOM(_25Recorded_sym_12391) && IS_SEQUENCE(_15208)) {
    }
    else {
        Concat((object_ptr)&_25Recorded_sym_12391, _25Recorded_sym_12391, _15208);
    }
    _15208 = NOVALUE;
L1F: 

    /** 		                No_new_entry = prev_Nne*/
    _52No_new_entry_48273 = _prev_Nne_26444;

    /** 		                return {RECORDED,length(Recorded)}*/
    if (IS_SEQUENCE(_25Recorded_12389)){
            _15210 = SEQ_PTR(_25Recorded_12389)->length;
    }
    else {
        _15210 = 1;
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 508;
    ((int *)_2)[2] = _15210;
    _15211 = MAKE_SEQ(_1);
    _15210 = NOVALUE;
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15134);
    _15134 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    _15190 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    _15201 = NOVALUE;
    return _15211;
    goto L20; // [770] 915
L1D: 

    /** 						tok = keyfind(yytext, SymTab[tok[T_SYM]][S_OBJ])*/
    _2 = (int)SEQ_PTR(_tok_26451);
    _15212 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_15212)){
        _15213 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_15212)->dbl));
    }
    else{
        _15213 = (int)*(((s1_ptr)_2)->base + _15212);
    }
    _2 = (int)SEQ_PTR(_15213);
    _15214 = (int)*(((s1_ptr)_2)->base + 1);
    _15213 = NOVALUE;
    RefDS(_yytext_26447);
    _32394 = _52hashfn(_yytext_26447);
    RefDS(_yytext_26447);
    Ref(_15214);
    _0 = _tok_26451;
    _tok_26451 = _52keyfind(_yytext_26447, _15214, _25current_file_no_12262, 0, _32394);
    DeRef(_0);
    _15214 = NOVALUE;
    _32394 = NOVALUE;

    /** 						if tok[T_ID] = VARIABLE then*/
    _2 = (int)SEQ_PTR(_tok_26451);
    _15216 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _15216, -100)){
        _15216 = NOVALUE;
        goto L21; // [817] 834
    }
    _15216 = NOVALUE;

    /** 							tok[T_ID] = QUALIFIED_VARIABLE*/
    _2 = (int)SEQ_PTR(_tok_26451);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _tok_26451 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 512;
    DeRef(_1);
    goto L22; // [831] 914
L21: 

    /** 						elsif tok[T_ID] = FUNC then*/
    _2 = (int)SEQ_PTR(_tok_26451);
    _15218 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _15218, 501)){
        _15218 = NOVALUE;
        goto L23; // [844] 861
    }
    _15218 = NOVALUE;

    /** 							tok[T_ID] = QUALIFIED_FUNC*/
    _2 = (int)SEQ_PTR(_tok_26451);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _tok_26451 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 520;
    DeRef(_1);
    goto L22; // [858] 914
L23: 

    /** 						elsif tok[T_ID] = PROC then*/
    _2 = (int)SEQ_PTR(_tok_26451);
    _15220 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _15220, 27)){
        _15220 = NOVALUE;
        goto L24; // [871] 888
    }
    _15220 = NOVALUE;

    /** 							tok[T_ID] = QUALIFIED_PROC*/
    _2 = (int)SEQ_PTR(_tok_26451);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _tok_26451 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 521;
    DeRef(_1);
    goto L22; // [885] 914
L24: 

    /** 						elsif tok[T_ID] = TYPE then*/
    _2 = (int)SEQ_PTR(_tok_26451);
    _15222 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _15222, 504)){
        _15222 = NOVALUE;
        goto L25; // [898] 913
    }
    _15222 = NOVALUE;

    /** 							tok[T_ID] = QUALIFIED_TYPE*/
    _2 = (int)SEQ_PTR(_tok_26451);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _tok_26451 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 522;
    DeRef(_1);
L25: 
L22: 
L20: 

    /** 					if atom( tok[T_SYM] ) and  SymTab[tok[T_SYM]][S_SCOPE] != SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_tok_26451);
    _15224 = (int)*(((s1_ptr)_2)->base + 2);
    _15225 = IS_ATOM(_15224);
    _15224 = NOVALUE;
    if (_15225 == 0) {
        goto L26; // [926] 1269
    }
    _2 = (int)SEQ_PTR(_tok_26451);
    _15227 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!IS_ATOM_INT(_15227)){
        _15228 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_15227)->dbl));
    }
    else{
        _15228 = (int)*(((s1_ptr)_2)->base + _15227);
    }
    _2 = (int)SEQ_PTR(_15228);
    _15229 = (int)*(((s1_ptr)_2)->base + 4);
    _15228 = NOVALUE;
    if (IS_ATOM_INT(_15229)) {
        _15230 = (_15229 != 9);
    }
    else {
        _15230 = binary_op(NOTEQ, _15229, 9);
    }
    _15229 = NOVALUE;
    if (_15230 == 0) {
        DeRef(_15230);
        _15230 = NOVALUE;
        goto L26; // [955] 1269
    }
    else {
        if (!IS_ATOM_INT(_15230) && DBL_PTR(_15230)->dbl == 0.0){
            DeRef(_15230);
            _15230 = NOVALUE;
            goto L26; // [955] 1269
        }
        DeRef(_15230);
        _15230 = NOVALUE;
    }
    DeRef(_15230);
    _15230 = NOVALUE;

    /** 						set_qualified_fwd( -1 )*/

    /** 	qualified_fwd = fwd*/
    _60qualified_fwd_24702 = -1;

    /** end procedure*/
    goto L26; // [967] 1269
    goto L26; // [971] 1269
L13: 

    /** 					ungetch()*/
    _60ungetch();

    /** 				    if Parser_mode = PAM_RECORD then*/
    if (_25Parser_mode_12388 != 1)
    goto L26; // [984] 1269

    /** 		                Ns_recorded &= 0*/
    Append(&_25Ns_recorded_12390, _25Ns_recorded_12390, 0);

    /** 		                Ns_recorded_sym &= 0*/
    Append(&_25Ns_recorded_sym_12392, _25Ns_recorded_sym_12392, 0);

    /** 		                Recorded = append(Recorded,yytext)*/
    RefDS(_yytext_26447);
    Append(&_25Recorded_12389, _25Recorded_12389, _yytext_26447);

    /** 		                prev_Nne = No_new_entry*/
    _prev_Nne_26444 = _52No_new_entry_48273;

    /** 						No_new_entry = 1*/
    _52No_new_entry_48273 = 1;

    /** 						tok = keyfind(yytext, -1)*/
    RefDS(_yytext_26447);
    _32393 = _52hashfn(_yytext_26447);
    RefDS(_yytext_26447);
    _0 = _tok_26451;
    _tok_26451 = _52keyfind(_yytext_26447, -1, _25current_file_no_12262, 0, _32393);
    DeRef(_0);
    _32393 = NOVALUE;

    /** 						if tok[T_ID] = IGNORED then*/
    _2 = (int)SEQ_PTR(_tok_26451);
    _15236 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _15236, 509)){
        _15236 = NOVALUE;
        goto L27; // [1060] 1077
    }
    _15236 = NOVALUE;

    /** 							Recorded_sym &= 0 -- must resolve on call site*/
    Append(&_25Recorded_sym_12391, _25Recorded_sym_12391, 0);
    goto L28; // [1074] 1094
L27: 

    /** 							Recorded_sym &= tok[T_SYM] -- fallback when symbol is undefined on call site*/
    _2 = (int)SEQ_PTR(_tok_26451);
    _15239 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_25Recorded_sym_12391) && IS_ATOM(_15239)) {
        Ref(_15239);
        Append(&_25Recorded_sym_12391, _25Recorded_sym_12391, _15239);
    }
    else if (IS_ATOM(_25Recorded_sym_12391) && IS_SEQUENCE(_15239)) {
    }
    else {
        Concat((object_ptr)&_25Recorded_sym_12391, _25Recorded_sym_12391, _15239);
    }
    _15239 = NOVALUE;
L28: 

    /** 		                No_new_entry = prev_Nne*/
    _52No_new_entry_48273 = _prev_Nne_26444;

    /** 		                tok = {RECORDED,length(Recorded)}*/
    if (IS_SEQUENCE(_25Recorded_12389)){
            _15241 = SEQ_PTR(_25Recorded_12389)->length;
    }
    else {
        _15241 = 1;
    }
    DeRef(_tok_26451);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 508;
    ((int *)_2)[2] = _15241;
    _tok_26451 = MAKE_SEQ(_1);
    _15241 = NOVALUE;
    goto L26; // [1116] 1269
L12: 

    /** 				set_qualified_fwd( -1 )*/

    /** 	qualified_fwd = fwd*/
    _60qualified_fwd_24702 = -1;

    /** end procedure*/
    goto L29; // [1128] 1131
L29: 

    /** 			    if Parser_mode = PAM_RECORD then*/
    if (_25Parser_mode_12388 != 1)
    goto L2A; // [1137] 1268

    /** 	                Ns_recorded_sym &= 0*/
    Append(&_25Ns_recorded_sym_12392, _25Ns_recorded_sym_12392, 0);

    /** 						Recorded = append(Recorded, yytext)*/
    RefDS(_yytext_26447);
    Append(&_25Recorded_12389, _25Recorded_12389, _yytext_26447);

    /** 		                Ns_recorded &= 0*/
    Append(&_25Ns_recorded_12390, _25Ns_recorded_12390, 0);

    /** 		                prev_Nne = No_new_entry*/
    _prev_Nne_26444 = _52No_new_entry_48273;

    /** 						No_new_entry = 1*/
    _52No_new_entry_48273 = 1;

    /** 						tok = keyfind(yytext, -1)*/
    RefDS(_yytext_26447);
    _32392 = _52hashfn(_yytext_26447);
    RefDS(_yytext_26447);
    _0 = _tok_26451;
    _tok_26451 = _52keyfind(_yytext_26447, -1, _25current_file_no_12262, 0, _32392);
    DeRef(_0);
    _32392 = NOVALUE;

    /** 						if tok[T_ID] = IGNORED then*/
    _2 = (int)SEQ_PTR(_tok_26451);
    _15248 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _15248, 509)){
        _15248 = NOVALUE;
        goto L2B; // [1213] 1230
    }
    _15248 = NOVALUE;

    /** 							Recorded_sym &= 0 -- must resolve on call site*/
    Append(&_25Recorded_sym_12391, _25Recorded_sym_12391, 0);
    goto L2C; // [1227] 1247
L2B: 

    /** 							Recorded_sym &= tok[T_SYM] -- fallback when symbol is undefined on call site*/
    _2 = (int)SEQ_PTR(_tok_26451);
    _15251 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_25Recorded_sym_12391) && IS_ATOM(_15251)) {
        Ref(_15251);
        Append(&_25Recorded_sym_12391, _25Recorded_sym_12391, _15251);
    }
    else if (IS_ATOM(_25Recorded_sym_12391) && IS_SEQUENCE(_15251)) {
    }
    else {
        Concat((object_ptr)&_25Recorded_sym_12391, _25Recorded_sym_12391, _15251);
    }
    _15251 = NOVALUE;
L2C: 

    /** 		                No_new_entry = prev_Nne*/
    _52No_new_entry_48273 = _prev_Nne_26444;

    /** 	                tok = {RECORDED, length(Recorded)}*/
    if (IS_SEQUENCE(_25Recorded_12389)){
            _15253 = SEQ_PTR(_25Recorded_12389)->length;
    }
    else {
        _15253 = 1;
    }
    DeRef(_tok_26451);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 508;
    ((int *)_2)[2] = _15253;
    _tok_26451 = MAKE_SEQ(_1);
    _15253 = NOVALUE;
L2A: 
L26: 

    /** 			return tok*/
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_name_26454);
    DeRef(_15134);
    _15134 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    _15190 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    _15212 = NOVALUE;
    _15227 = NOVALUE;
    return _tok_26451;
    goto L1; // [1279] 10
L7: 

    /** 		elsif class < ILLEGAL_CHAR then*/
    if (_class_26453 >= -20)
    goto L2D; // [1286] 1303

    /** 			return {class, 0}  -- brackets, punctuation, eof, illegal char etc.*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _class_26453;
    ((int *)_2)[2] = 0;
    _15256 = MAKE_SEQ(_1);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15134);
    _15134 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    _15190 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    _15212 = NOVALUE;
    _15227 = NOVALUE;
    return _15256;
    goto L1; // [1300] 10
L2D: 

    /** 		elsif class = ILLEGAL_CHAR then*/
    if (_class_26453 != -20)
    goto L2E; // [1307] 1321

    /** 			CompileErr(101)*/
    RefDS(_22682);
    _43CompileErr(101, _22682, 0);
    goto L1; // [1318] 10
L2E: 

    /** 		elsif class = NEWLINE then*/
    if (_class_26453 != -6)
    goto L2F; // [1325] 1351

    /** 			if start_include then*/
    if (_60start_include_24670 == 0)
    {
        goto L30; // [1333] 1343
    }
    else{
    }

    /** 				IncludePush()*/
    _60IncludePush();
    goto L1; // [1340] 10
L30: 

    /** 				read_line()*/
    _60read_line();
    goto L1; // [1348] 10
L2F: 

    /** 		elsif class = EQUALS then*/
    if (_class_26453 != 3)
    goto L31; // [1355] 1372

    /** 			return {class, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _class_26453;
    ((int *)_2)[2] = 0;
    _15260 = MAKE_SEQ(_1);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15134);
    _15134 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    _15190 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    _15212 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    return _15260;
    goto L1; // [1369] 10
L31: 

    /** 		elsif class = DOT or class = DIGIT then*/
    _15261 = (_class_26453 == -3);
    if (_15261 != 0) {
        goto L32; // [1380] 1395
    }
    _15263 = (_class_26453 == -7);
    if (_15263 == 0)
    {
        DeRef(_15263);
        _15263 = NOVALUE;
        goto L33; // [1391] 2225
    }
    else{
        DeRef(_15263);
        _15263 = NOVALUE;
    }
L32: 

    /** 			integer basetype*/

    /** 			if class = DOT then*/
    if (_class_26453 != -3)
    goto L34; // [1401] 1435

    /** 				if getch() = '.' then*/
    _15265 = _60getch();
    if (binary_op_a(NOTEQ, _15265, 46)){
        DeRef(_15265);
        _15265 = NOVALUE;
        goto L35; // [1410] 1429
    }
    DeRef(_15265);
    _15265 = NOVALUE;

    /** 					return {SLICE, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 513;
    ((int *)_2)[2] = 0;
    _15267 = MAKE_SEQ(_1);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15134);
    _15134 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    _15190 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    _15212 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    return _15267;
    goto L36; // [1426] 1434
L35: 

    /** 					ungetch()*/
    _60ungetch();
L36: 
L34: 

    /** 			yytext = {ch}*/
    _0 = _yytext_26447;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _ch_26441;
    _yytext_26447 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 			is_int = (ch != '.')*/
    _is_int_26452 = (_ch_26441 != 46);

    /** 			basetype = -1 -- default is decimal*/
    _basetype_26747 = -1;

    /** 			while 1 with entry do*/
    goto L37; // [1454] 1645
L38: 

    /** 				if char_class[ch] = DIGIT then*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _15270 = (int)*(((s1_ptr)_2)->base + _ch_26441);
    if (_15270 != -7)
    goto L39; // [1467] 1480

    /** 					yytext &= ch*/
    Append(&_yytext_26447, _yytext_26447, _ch_26441);
    goto L3A; // [1477] 1642
L39: 

    /** 				elsif equal(yytext, "0") then*/
    if (_yytext_26447 == _14889)
    _15273 = 1;
    else if (IS_ATOM_INT(_yytext_26447) && IS_ATOM_INT(_14889))
    _15273 = 0;
    else
    _15273 = (compare(_yytext_26447, _14889) == 0);
    if (_15273 == 0)
    {
        _15273 = NOVALUE;
        goto L3B; // [1486] 1581
    }
    else{
        _15273 = NOVALUE;
    }

    /** 					basetype = find(ch, nbasecode)*/
    _basetype_26747 = find_from(_ch_26441, _60nbasecode_26249, 1);

    /** 					if basetype > length(nbase) then*/
    if (IS_SEQUENCE(_60nbase_26248)){
            _15275 = SEQ_PTR(_60nbase_26248)->length;
    }
    else {
        _15275 = 1;
    }
    if (_basetype_26747 <= _15275)
    goto L3C; // [1501] 1515

    /** 						basetype -= length(nbase)*/
    if (IS_SEQUENCE(_60nbase_26248)){
            _15277 = SEQ_PTR(_60nbase_26248)->length;
    }
    else {
        _15277 = 1;
    }
    _basetype_26747 = _basetype_26747 - _15277;
    _15277 = NOVALUE;
L3C: 

    /** 					if basetype = 0 then*/
    if (_basetype_26747 != 0)
    goto L3D; // [1517] 1572

    /** 						if char_class[ch] = LETTER then*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _15280 = (int)*(((s1_ptr)_2)->base + _ch_26441);
    if (_15280 != -2)
    goto L3E; // [1531] 1562

    /** 							if ch != 'e' and ch != 'E' then*/
    _15282 = (_ch_26441 != 101);
    if (_15282 == 0) {
        goto L3F; // [1541] 1561
    }
    _15284 = (_ch_26441 != 69);
    if (_15284 == 0)
    {
        DeRef(_15284);
        _15284 = NOVALUE;
        goto L3F; // [1550] 1561
    }
    else{
        DeRef(_15284);
        _15284 = NOVALUE;
    }

    /** 								CompileErr(105, ch)*/
    _43CompileErr(105, _ch_26441, 0);
L3F: 
L3E: 

    /** 						basetype = -1 -- decimal*/
    _basetype_26747 = -1;

    /** 						exit*/
    goto L40; // [1569] 1657
L3D: 

    /** 					yytext &= '0'*/
    Append(&_yytext_26447, _yytext_26447, 48);
    goto L3A; // [1578] 1642
L3B: 

    /** 				elsif basetype = 4 then -- hexadecimal*/
    if (_basetype_26747 != 4)
    goto L40; // [1583] 1657

    /** 					integer hdigit*/

    /** 					hdigit = find(ch, "ABCDEFabcdef")*/
    _hdigit_26787 = find_from(_ch_26441, _15287, 1);

    /** 					if hdigit = 0 then*/
    if (_hdigit_26787 != 0)
    goto L41; // [1598] 1609

    /** 						exit*/
    goto L40; // [1606] 1657
L41: 

    /** 					if hdigit > 6 then*/
    if (_hdigit_26787 <= 6)
    goto L42; // [1611] 1622

    /** 						hdigit -= 6*/
    _hdigit_26787 = _hdigit_26787 - 6;
L42: 

    /** 					yytext &= hexasc[hdigit]*/
    _2 = (int)SEQ_PTR(_60hexasc_26251);
    _15292 = (int)*(((s1_ptr)_2)->base + _hdigit_26787);
    if (IS_SEQUENCE(_yytext_26447) && IS_ATOM(_15292)) {
        Ref(_15292);
        Append(&_yytext_26447, _yytext_26447, _15292);
    }
    else if (IS_ATOM(_yytext_26447) && IS_SEQUENCE(_15292)) {
    }
    else {
        Concat((object_ptr)&_yytext_26447, _yytext_26447, _15292);
    }
    _15292 = NOVALUE;
    goto L3A; // [1634] 1642

    /** 					exit*/
    goto L40; // [1639] 1657
L3A: 

    /** 			entry*/
L37: 

    /** 				ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 			end while*/
    goto L38; // [1654] 1457
L40: 

    /** 			if ch = '.' then*/
    if (_ch_26441 != 46)
    goto L43; // [1659] 1794

    /** 				ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 				if ch = '.' then*/
    if (_ch_26441 != 46)
    goto L44; // [1672] 1683

    /** 					ungetch()*/
    _60ungetch();
    goto L45; // [1680] 1793
L44: 

    /** 					is_int = FALSE*/
    _is_int_26452 = _5FALSE_242;

    /** 					if yytext[1] = '.' then*/
    _2 = (int)SEQ_PTR(_yytext_26447);
    _15298 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _15298, 46)){
        _15298 = NOVALUE;
        goto L46; // [1698] 1712
    }
    _15298 = NOVALUE;

    /** 						CompileErr(124)*/
    RefDS(_22682);
    _43CompileErr(124, _22682, 0);
    goto L47; // [1709] 1719
L46: 

    /** 						yytext &= '.'*/
    Append(&_yytext_26447, _yytext_26447, 46);
L47: 

    /** 					if char_class[ch] = DIGIT then*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _15301 = (int)*(((s1_ptr)_2)->base + _ch_26441);
    if (_15301 != -7)
    goto L48; // [1729] 1784

    /** 						yytext &= ch*/
    Append(&_yytext_26447, _yytext_26447, _ch_26441);

    /** 						ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 						while char_class[ch] = DIGIT do*/
L49: 
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _15305 = (int)*(((s1_ptr)_2)->base + _ch_26441);
    if (_15305 != -7)
    goto L4A; // [1759] 1792

    /** 							yytext &= ch*/
    Append(&_yytext_26447, _yytext_26447, _ch_26441);

    /** 							ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 						end while*/
    goto L49; // [1778] 1751
    goto L4A; // [1781] 1792
L48: 

    /** 						CompileErr(94)*/
    RefDS(_22682);
    _43CompileErr(94, _22682, 0);
L4A: 
L45: 
L43: 

    /** 			if basetype = -1 and find(ch, "eE") then*/
    _15309 = (_basetype_26747 == -1);
    if (_15309 == 0) {
        goto L4B; // [1800] 1936
    }
    _15312 = find_from(_ch_26441, _15311, 1);
    if (_15312 == 0)
    {
        _15312 = NOVALUE;
        goto L4B; // [1810] 1936
    }
    else{
        _15312 = NOVALUE;
    }

    /** 				is_int = FALSE*/
    _is_int_26452 = _5FALSE_242;

    /** 				yytext &= ch*/
    Append(&_yytext_26447, _yytext_26447, _ch_26441);

    /** 				ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 				if ch = '-' or ch = '+' or char_class[ch] = DIGIT then*/
    _15315 = (_ch_26441 == 45);
    if (_15315 != 0) {
        _15316 = 1;
        goto L4C; // [1841] 1853
    }
    _15317 = (_ch_26441 == 43);
    _15316 = (_15317 != 0);
L4C: 
    if (_15316 != 0) {
        goto L4D; // [1853] 1874
    }
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _15319 = (int)*(((s1_ptr)_2)->base + _ch_26441);
    _15320 = (_15319 == -7);
    _15319 = NOVALUE;
    if (_15320 == 0)
    {
        DeRef(_15320);
        _15320 = NOVALUE;
        goto L4E; // [1870] 1883
    }
    else{
        DeRef(_15320);
        _15320 = NOVALUE;
    }
L4D: 

    /** 					yytext &= ch*/
    Append(&_yytext_26447, _yytext_26447, _ch_26441);
    goto L4F; // [1880] 1891
L4E: 

    /** 					CompileErr(86)*/
    RefDS(_22682);
    _43CompileErr(86, _22682, 0);
L4F: 

    /** 				ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 				while char_class[ch] = DIGIT do*/
L50: 
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _15323 = (int)*(((s1_ptr)_2)->base + _ch_26441);
    if (_15323 != -7)
    goto L51; // [1911] 1967

    /** 					yytext &= ch*/
    Append(&_yytext_26447, _yytext_26447, _ch_26441);

    /** 					ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 				end while*/
    goto L50; // [1930] 1903
    goto L51; // [1933] 1967
L4B: 

    /** 			elsif char_class[ch] = LETTER then*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _15327 = (int)*(((s1_ptr)_2)->base + _ch_26441);
    if (_15327 != -2)
    goto L52; // [1946] 1966

    /** 				CompileErr(127, {{ch}})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _ch_26441;
    _15329 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _15329;
    _15330 = MAKE_SEQ(_1);
    _15329 = NOVALUE;
    _43CompileErr(127, _15330, 0);
    _15330 = NOVALUE;
L52: 
L51: 

    /** 			ungetch()*/
    _60ungetch();

    /** 			while i != 0 with entry do*/
    goto L53; // [1973] 2012
L54: 
    if (_i_26442 == 0)
    goto L55; // [1978] 2024

    /** 			    yytext = yytext[1 .. i-1] & yytext[i+1 .. $]*/
    _15332 = _i_26442 - 1;
    rhs_slice_target = (object_ptr)&_15333;
    RHS_Slice(_yytext_26447, 1, _15332);
    _15334 = _i_26442 + 1;
    if (_15334 > MAXINT){
        _15334 = NewDouble((double)_15334);
    }
    if (IS_SEQUENCE(_yytext_26447)){
            _15335 = SEQ_PTR(_yytext_26447)->length;
    }
    else {
        _15335 = 1;
    }
    rhs_slice_target = (object_ptr)&_15336;
    RHS_Slice(_yytext_26447, _15334, _15335);
    Concat((object_ptr)&_yytext_26447, _15333, _15336);
    DeRefDS(_15333);
    _15333 = NOVALUE;
    DeRef(_15333);
    _15333 = NOVALUE;
    DeRefDS(_15336);
    _15336 = NOVALUE;

    /** 			  entry*/
L53: 

    /** 			    i = find('_', yytext)*/
    _i_26442 = find_from(95, _yytext_26447, 1);

    /** 			end while*/
    goto L54; // [2021] 1976
L55: 

    /** 			if is_int then*/
    if (_is_int_26452 == 0)
    {
        goto L56; // [2026] 2126
    }
    else{
    }

    /** 				if basetype = -1 then*/
    if (_basetype_26747 != -1)
    goto L57; // [2031] 2041

    /** 					basetype = 3 -- decimal*/
    _basetype_26747 = 3;
L57: 

    /** 				fenv:clear(FE_OVERFLOW)*/
    _15340 = _62clear(111);

    /** 				d = MakeInt(yytext, nbase[basetype])*/
    _2 = (int)SEQ_PTR(_60nbase_26248);
    _15341 = (int)*(((s1_ptr)_2)->base + _basetype_26747);
    RefDS(_yytext_26447);
    Ref(_15341);
    _0 = _d_26449;
    _d_26449 = _60MakeInt(_yytext_26447, _15341);
    DeRef(_0);
    _15341 = NOVALUE;

    /** 				if fenv:test(FE_OVERFLOW) then*/
    _15343 = _62test(111);
    if (_15343 == 0) {
        DeRef(_15343);
        _15343 = NOVALUE;
        goto L58; // [2068] 2081
    }
    else {
        if (!IS_ATOM_INT(_15343) && DBL_PTR(_15343)->dbl == 0.0){
            DeRef(_15343);
            _15343 = NOVALUE;
            goto L58; // [2068] 2081
        }
        DeRef(_15343);
        _15343 = NOVALUE;
    }
    DeRef(_15343);
    _15343 = NOVALUE;

    /** 					CompileErr(NUMBER_IS_TOO_BIG)*/
    RefDS(_22682);
    _43CompileErr(357, _22682, 0);
L58: 

    /** 				if integer(d) then*/
    if (IS_ATOM_INT(_d_26449))
    _15344 = 1;
    else if (IS_ATOM_DBL(_d_26449))
    _15344 = IS_ATOM_INT(DoubleToInt(_d_26449));
    else
    _15344 = 0;
    if (_15344 == 0)
    {
        _15344 = NOVALUE;
        goto L59; // [2086] 2108
    }
    else{
        _15344 = NOVALUE;
    }

    /** 					return {ATOM, NewIntSym(d)}*/
    Ref(_d_26449);
    _15345 = _52NewIntSym(_d_26449);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 502;
    ((int *)_2)[2] = _15345;
    _15346 = MAKE_SEQ(_1);
    _15345 = NOVALUE;
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    _15323 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    return _15346;
    goto L5A; // [2105] 2125
L59: 

    /** 					return {ATOM, NewDoubleSym(d)}*/
    Ref(_d_26449);
    _15347 = _52NewDoubleSym(_d_26449);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 502;
    ((int *)_2)[2] = _15347;
    _15348 = MAKE_SEQ(_1);
    _15347 = NOVALUE;
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    _15323 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    return _15348;
L5A: 
L56: 

    /** 			if basetype != -1 then*/
    if (_basetype_26747 == -1)
    goto L5B; // [2128] 2144

    /** 				CompileErr(125, nbasecode[basetype])*/
    _2 = (int)SEQ_PTR(_60nbasecode_26249);
    _15350 = (int)*(((s1_ptr)_2)->base + _basetype_26747);
    Ref(_15350);
    _43CompileErr(125, _15350, 0);
    _15350 = NOVALUE;
L5B: 

    /** 			d = my_sscanf(yytext)*/
    RefDS(_yytext_26447);
    _0 = _d_26449;
    _d_26449 = _60my_sscanf(_yytext_26447);
    DeRef(_0);

    /** 			if sequence(d) then*/
    _15352 = IS_SEQUENCE(_d_26449);
    if (_15352 == 0)
    {
        _15352 = NOVALUE;
        goto L5C; // [2155] 2168
    }
    else{
        _15352 = NOVALUE;
    }

    /** 				CompileErr(121)*/
    RefDS(_22682);
    _43CompileErr(121, _22682, 0);
    goto L5D; // [2165] 2220
L5C: 

    /** 			elsif is_int and d <= MAXINT_DBL then*/
    if (_is_int_26452 == 0) {
        goto L5E; // [2170] 2203
    }
    if (IS_ATOM_INT(_d_26449)) {
        _15354 = (_d_26449 <= 1073741823);
    }
    else {
        _15354 = binary_op(LESSEQ, _d_26449, 1073741823);
    }
    if (_15354 == 0) {
        DeRef(_15354);
        _15354 = NOVALUE;
        goto L5E; // [2181] 2203
    }
    else {
        if (!IS_ATOM_INT(_15354) && DBL_PTR(_15354)->dbl == 0.0){
            DeRef(_15354);
            _15354 = NOVALUE;
            goto L5E; // [2181] 2203
        }
        DeRef(_15354);
        _15354 = NOVALUE;
    }
    DeRef(_15354);
    _15354 = NOVALUE;

    /** 				return {ATOM, NewIntSym(d)}  -- 1 to 1.07 billion*/
    Ref(_d_26449);
    _15355 = _52NewIntSym(_d_26449);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 502;
    ((int *)_2)[2] = _15355;
    _15356 = MAKE_SEQ(_1);
    _15355 = NOVALUE;
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    _15323 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    return _15356;
    goto L5D; // [2200] 2220
L5E: 

    /** 				return {ATOM, NewDoubleSym(d)}*/
    Ref(_d_26449);
    _15357 = _52NewDoubleSym(_d_26449);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 502;
    ((int *)_2)[2] = _15357;
    _15358 = MAKE_SEQ(_1);
    _15357 = NOVALUE;
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    _15323 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    return _15358;
L5D: 
    goto L1; // [2222] 10
L33: 

    /** 		elsif class = MINUS then*/
    if (_class_26453 != 10)
    goto L5F; // [2229] 2315

    /** 			ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 			if ch = '-' then*/
    if (_ch_26441 != 45)
    goto L60; // [2242] 2268

    /** 				if start_include then*/
    if (_60start_include_24670 == 0)
    {
        goto L61; // [2250] 2260
    }
    else{
    }

    /** 					IncludePush()*/
    _60IncludePush();
    goto L1; // [2257] 10
L61: 

    /** 					read_line()*/
    _60read_line();
    goto L1; // [2265] 10
L60: 

    /** 			elsif ch = '=' then*/
    if (_ch_26441 != 61)
    goto L62; // [2270] 2289

    /** 				return {MINUS_EQUALS, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 516;
    ((int *)_2)[2] = 0;
    _15363 = MAKE_SEQ(_1);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    _15323 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    return _15363;
    goto L1; // [2286] 10
L62: 

    /** 				bp -= 1*/
    _43bp_49536 = _43bp_49536 - 1;

    /** 				return {MINUS, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 10;
    ((int *)_2)[2] = 0;
    _15365 = MAKE_SEQ(_1);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    _15323 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15365;
    goto L1; // [2312] 10
L5F: 

    /** 		elsif class = DOUBLE_QUOTE then*/
    if (_class_26453 != -4)
    goto L63; // [2319] 2513

    /** 			integer fch*/

    /** 			ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 			if ch = '"' then*/
    if (_ch_26441 != 34)
    goto L64; // [2334] 2370

    /** 				fch = getch()*/
    _fch_26932 = _60getch();
    if (!IS_ATOM_INT(_fch_26932)) {
        _1 = (long)(DBL_PTR(_fch_26932)->dbl);
        if (UNIQUE(DBL_PTR(_fch_26932)) && (DBL_PTR(_fch_26932)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fch_26932);
        _fch_26932 = _1;
    }

    /** 				if fch = '"' then*/
    if (_fch_26932 != 34)
    goto L65; // [2347] 2364

    /** 					return ExtendedString( fch )*/
    _15371 = _60ExtendedString(_fch_26932);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    _15323 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15371;
    goto L66; // [2361] 2369
L65: 

    /** 					ungetch()*/
    _60ungetch();
L66: 
L64: 

    /** 			yytext = ""*/
    RefDS(_5);
    DeRef(_yytext_26447);
    _yytext_26447 = _5;

    /** 			while ch != '\n' and ch != '\r' do -- can't be EOF*/
L67: 
    _15372 = (_ch_26441 != 10);
    if (_15372 == 0) {
        goto L68; // [2386] 2465
    }
    _15374 = (_ch_26441 != 13);
    if (_15374 == 0)
    {
        DeRef(_15374);
        _15374 = NOVALUE;
        goto L68; // [2395] 2465
    }
    else{
        DeRef(_15374);
        _15374 = NOVALUE;
    }

    /** 				if ch = '"' then*/
    if (_ch_26441 != 34)
    goto L69; // [2400] 2411

    /** 					exit*/
    goto L68; // [2406] 2465
    goto L6A; // [2408] 2453
L69: 

    /** 				elsif ch = '\\' then*/
    if (_ch_26441 != 92)
    goto L6B; // [2413] 2430

    /** 					yytext &= EscapeChar('"')*/
    _15377 = _60EscapeChar(34);
    if (IS_SEQUENCE(_yytext_26447) && IS_ATOM(_15377)) {
        Ref(_15377);
        Append(&_yytext_26447, _yytext_26447, _15377);
    }
    else if (IS_ATOM(_yytext_26447) && IS_SEQUENCE(_15377)) {
    }
    else {
        Concat((object_ptr)&_yytext_26447, _yytext_26447, _15377);
    }
    DeRef(_15377);
    _15377 = NOVALUE;
    goto L6A; // [2427] 2453
L6B: 

    /** 				elsif ch = '\t' then*/
    if (_ch_26441 != 9)
    goto L6C; // [2432] 2446

    /** 					CompileErr(145)*/
    RefDS(_22682);
    _43CompileErr(145, _22682, 0);
    goto L6A; // [2443] 2453
L6C: 

    /** 					yytext &= ch*/
    Append(&_yytext_26447, _yytext_26447, _ch_26441);
L6A: 

    /** 				ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 			end while*/
    goto L67; // [2462] 2382
L68: 

    /** 			if ch = '\n' or ch = '\r' then*/
    _15382 = (_ch_26441 == 10);
    if (_15382 != 0) {
        goto L6D; // [2471] 2484
    }
    _15384 = (_ch_26441 == 13);
    if (_15384 == 0)
    {
        DeRef(_15384);
        _15384 = NOVALUE;
        goto L6E; // [2480] 2492
    }
    else{
        DeRef(_15384);
        _15384 = NOVALUE;
    }
L6D: 

    /** 				CompileErr(67)*/
    RefDS(_22682);
    _43CompileErr(67, _22682, 0);
L6E: 

    /** 			return {STRING, NewStringSym(yytext)}*/
    RefDS(_yytext_26447);
    _15385 = _52NewStringSym(_yytext_26447);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 503;
    ((int *)_2)[2] = _15385;
    _15386 = MAKE_SEQ(_1);
    _15385 = NOVALUE;
    DeRefDS(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    _15323 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15386;
    goto L1; // [2510] 10
L63: 

    /** 		elsif class = PLUS then*/
    if (_class_26453 != 11)
    goto L6F; // [2517] 2569

    /** 			ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 			if ch = '=' then*/
    if (_ch_26441 != 61)
    goto L70; // [2530] 2549

    /** 				return {PLUS_EQUALS, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 515;
    ((int *)_2)[2] = 0;
    _15390 = MAKE_SEQ(_1);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    _15323 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15390;
    goto L1; // [2546] 10
L70: 

    /** 				ungetch()*/
    _60ungetch();

    /** 				return {PLUS, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 11;
    ((int *)_2)[2] = 0;
    _15391 = MAKE_SEQ(_1);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    _15323 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    DeRef(_15390);
    _15390 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15391;
    goto L1; // [2566] 10
L6F: 

    /** 		elsif class = res:CONCAT then*/
    if (_class_26453 != 15)
    goto L71; // [2571] 2621

    /** 			ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 			if ch = '=' then*/
    if (_ch_26441 != 61)
    goto L72; // [2584] 2603

    /** 				return {CONCAT_EQUALS, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 519;
    ((int *)_2)[2] = 0;
    _15395 = MAKE_SEQ(_1);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    DeRef(_15391);
    _15391 = NOVALUE;
    _15323 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    DeRef(_15390);
    _15390 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15395;
    goto L1; // [2600] 10
L72: 

    /** 				ungetch()*/
    _60ungetch();

    /** 				return {res:CONCAT, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 15;
    ((int *)_2)[2] = 0;
    _15396 = MAKE_SEQ(_1);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    DeRef(_15391);
    _15391 = NOVALUE;
    _15323 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    DeRef(_15390);
    _15390 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15395);
    _15395 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15396;
    goto L1; // [2618] 10
L71: 

    /** 		elsif class = NUMBER_SIGN then*/
    if (_class_26453 != -11)
    goto L73; // [2625] 3180

    /** 			fenv:clear(FE_OVERFLOW)		*/
    _15398 = _62clear(111);

    /** 			i = 0*/
    _i_26442 = 0;

    /** 			is_int = -1*/
    _is_int_26452 = -1;

    /** 			while i < MAXINT/32 do*/
L74: 
    _15399 = (1073741823 % 32) ? NewDouble((double)1073741823 / 32) : (1073741823 / 32);
    if (binary_op_a(GREATEREQ, _i_26442, _15399)){
        DeRef(_15399);
        _15399 = NOVALUE;
        goto L75; // [2658] 2828
    }
    DeRef(_15399);
    _15399 = NOVALUE;

    /** 				ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 				if char_class[ch] = DIGIT then*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _15402 = (int)*(((s1_ptr)_2)->base + _ch_26441);
    if (_15402 != -7)
    goto L76; // [2679] 2718

    /** 					if ch != '_' then*/
    if (_ch_26441 == 95)
    goto L74; // [2685] 2652

    /** 						i = i * 16 + ch - '0'*/
    if (_i_26442 == (short)_i_26442)
    _15405 = _i_26442 * 16;
    else
    _15405 = NewDouble(_i_26442 * (double)16);
    if (IS_ATOM_INT(_15405)) {
        _15406 = _15405 + _ch_26441;
        if ((long)((unsigned long)_15406 + (unsigned long)HIGH_BITS) >= 0) 
        _15406 = NewDouble((double)_15406);
    }
    else {
        _15406 = NewDouble(DBL_PTR(_15405)->dbl + (double)_ch_26441);
    }
    DeRef(_15405);
    _15405 = NOVALUE;
    if (IS_ATOM_INT(_15406)) {
        _i_26442 = _15406 - 48;
    }
    else {
        _i_26442 = NewDouble(DBL_PTR(_15406)->dbl - (double)48);
    }
    DeRef(_15406);
    _15406 = NOVALUE;
    if (!IS_ATOM_INT(_i_26442)) {
        _1 = (long)(DBL_PTR(_i_26442)->dbl);
        if (UNIQUE(DBL_PTR(_i_26442)) && (DBL_PTR(_i_26442)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_26442);
        _i_26442 = _1;
    }

    /** 						is_int = TRUE*/
    _is_int_26452 = _5TRUE_244;
    goto L74; // [2715] 2652
L76: 

    /** 				elsif ch >= 'A' and ch <= 'F' then*/
    _15408 = (_ch_26441 >= 65);
    if (_15408 == 0) {
        goto L77; // [2724] 2768
    }
    _15410 = (_ch_26441 <= 70);
    if (_15410 == 0)
    {
        DeRef(_15410);
        _15410 = NOVALUE;
        goto L77; // [2733] 2768
    }
    else{
        DeRef(_15410);
        _15410 = NOVALUE;
    }

    /** 					i = (i * 16) + ch - ('A'-10)*/
    if (_i_26442 == (short)_i_26442)
    _15411 = _i_26442 * 16;
    else
    _15411 = NewDouble(_i_26442 * (double)16);
    if (IS_ATOM_INT(_15411)) {
        _15412 = _15411 + _ch_26441;
        if ((long)((unsigned long)_15412 + (unsigned long)HIGH_BITS) >= 0) 
        _15412 = NewDouble((double)_15412);
    }
    else {
        _15412 = NewDouble(DBL_PTR(_15411)->dbl + (double)_ch_26441);
    }
    DeRef(_15411);
    _15411 = NOVALUE;
    _15413 = 55;
    if (IS_ATOM_INT(_15412)) {
        _i_26442 = _15412 - 55;
    }
    else {
        _i_26442 = NewDouble(DBL_PTR(_15412)->dbl - (double)55);
    }
    DeRef(_15412);
    _15412 = NOVALUE;
    _15413 = NOVALUE;
    if (!IS_ATOM_INT(_i_26442)) {
        _1 = (long)(DBL_PTR(_i_26442)->dbl);
        if (UNIQUE(DBL_PTR(_i_26442)) && (DBL_PTR(_i_26442)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_26442);
        _i_26442 = _1;
    }

    /** 					is_int = TRUE*/
    _is_int_26452 = _5TRUE_244;
    goto L74; // [2765] 2652
L77: 

    /** 				elsif ch >= 'a' and ch <= 'f' then*/
    _15415 = (_ch_26441 >= 97);
    if (_15415 == 0) {
        goto L75; // [2774] 2828
    }
    _15417 = (_ch_26441 <= 102);
    if (_15417 == 0)
    {
        DeRef(_15417);
        _15417 = NOVALUE;
        goto L75; // [2783] 2828
    }
    else{
        DeRef(_15417);
        _15417 = NOVALUE;
    }

    /** 					i = (i * 16) + ch - ('a'-10)*/
    if (_i_26442 == (short)_i_26442)
    _15418 = _i_26442 * 16;
    else
    _15418 = NewDouble(_i_26442 * (double)16);
    if (IS_ATOM_INT(_15418)) {
        _15419 = _15418 + _ch_26441;
        if ((long)((unsigned long)_15419 + (unsigned long)HIGH_BITS) >= 0) 
        _15419 = NewDouble((double)_15419);
    }
    else {
        _15419 = NewDouble(DBL_PTR(_15418)->dbl + (double)_ch_26441);
    }
    DeRef(_15418);
    _15418 = NOVALUE;
    _15420 = 87;
    if (IS_ATOM_INT(_15419)) {
        _i_26442 = _15419 - 87;
    }
    else {
        _i_26442 = NewDouble(DBL_PTR(_15419)->dbl - (double)87);
    }
    DeRef(_15419);
    _15419 = NOVALUE;
    _15420 = NOVALUE;
    if (!IS_ATOM_INT(_i_26442)) {
        _1 = (long)(DBL_PTR(_i_26442)->dbl);
        if (UNIQUE(DBL_PTR(_i_26442)) && (DBL_PTR(_i_26442)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_26442);
        _i_26442 = _1;
    }

    /** 					is_int = TRUE*/
    _is_int_26452 = _5TRUE_244;
    goto L74; // [2815] 2652

    /** 					exit*/
    goto L75; // [2820] 2828

    /** 			end while*/
    goto L74; // [2825] 2652
L75: 

    /** 			if is_int = -1 then*/
    if (_is_int_26452 != -1)
    goto L78; // [2830] 2893

    /** 				if ch = '!' then*/
    if (_ch_26441 != 33)
    goto L79; // [2836] 2882

    /** 					if line_number > 1 then*/
    if (_25line_number_12263 <= 1)
    goto L7A; // [2844] 2856

    /** 						CompileErr(161)*/
    RefDS(_22682);
    _43CompileErr(161, _22682, 0);
L7A: 

    /** 					shebang = ThisLine*/
    Ref(_43ThisLine_49532);
    DeRef(_60shebang_24675);
    _60shebang_24675 = _43ThisLine_49532;

    /** 					if start_include then*/
    if (_60start_include_24670 == 0)
    {
        goto L7B; // [2867] 2875
    }
    else{
    }

    /** 						IncludePush()*/
    _60IncludePush();
L7B: 

    /** 					read_line()*/
    _60read_line();
    goto L1; // [2879] 10
L79: 

    /** 					CompileErr(97)*/
    RefDS(_22682);
    _43CompileErr(97, _22682, 0);
    goto L1; // [2890] 10
L78: 

    /** 				if i >= MAXINT/32 then*/
    _15425 = (1073741823 % 32) ? NewDouble((double)1073741823 / 32) : (1073741823 / 32);
    if (binary_op_a(LESS, _i_26442, _15425)){
        DeRef(_15425);
        _15425 = NOVALUE;
        goto L7C; // [2901] 3072
    }
    DeRef(_15425);
    _15425 = NOVALUE;

    /** 					d = i*/
    DeRef(_d_26449);
    _d_26449 = _i_26442;

    /** 					is_int = FALSE*/
    _is_int_26452 = _5FALSE_242;

    /** 					while TRUE do*/
L7D: 
    if (_5TRUE_244 == 0)
    {
        goto L7E; // [2926] 3071
    }
    else{
    }

    /** 						ch = getch()  -- eventually END_OF_FILE_CHAR or new-line*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 						if char_class[ch] = DIGIT then*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _15428 = (int)*(((s1_ptr)_2)->base + _ch_26441);
    if (_15428 != -7)
    goto L7F; // [2946] 2974

    /** 							if ch != '_' then*/
    if (_ch_26441 == 95)
    goto L7D; // [2952] 2924

    /** 								d = (d * 16) + ch - '0'*/
    if (IS_ATOM_INT(_d_26449)) {
        if (_d_26449 == (short)_d_26449)
        _15431 = _d_26449 * 16;
        else
        _15431 = NewDouble(_d_26449 * (double)16);
    }
    else {
        _15431 = binary_op(MULTIPLY, _d_26449, 16);
    }
    if (IS_ATOM_INT(_15431)) {
        _15432 = _15431 + _ch_26441;
        if ((long)((unsigned long)_15432 + (unsigned long)HIGH_BITS) >= 0) 
        _15432 = NewDouble((double)_15432);
    }
    else {
        _15432 = binary_op(PLUS, _15431, _ch_26441);
    }
    DeRef(_15431);
    _15431 = NOVALUE;
    DeRef(_d_26449);
    if (IS_ATOM_INT(_15432)) {
        _d_26449 = _15432 - 48;
        if ((long)((unsigned long)_d_26449 +(unsigned long) HIGH_BITS) >= 0){
            _d_26449 = NewDouble((double)_d_26449);
        }
    }
    else {
        _d_26449 = binary_op(MINUS, _15432, 48);
    }
    DeRef(_15432);
    _15432 = NOVALUE;
    goto L7D; // [2971] 2924
L7F: 

    /** 						elsif ch >= 'A' and ch <= 'F' then*/
    _15434 = (_ch_26441 >= 65);
    if (_15434 == 0) {
        goto L80; // [2980] 3013
    }
    _15436 = (_ch_26441 <= 70);
    if (_15436 == 0)
    {
        DeRef(_15436);
        _15436 = NOVALUE;
        goto L80; // [2989] 3013
    }
    else{
        DeRef(_15436);
        _15436 = NOVALUE;
    }

    /** 							d = (d * 16) + ch - ('A'- 10)*/
    if (IS_ATOM_INT(_d_26449)) {
        if (_d_26449 == (short)_d_26449)
        _15437 = _d_26449 * 16;
        else
        _15437 = NewDouble(_d_26449 * (double)16);
    }
    else {
        _15437 = binary_op(MULTIPLY, _d_26449, 16);
    }
    if (IS_ATOM_INT(_15437)) {
        _15438 = _15437 + _ch_26441;
        if ((long)((unsigned long)_15438 + (unsigned long)HIGH_BITS) >= 0) 
        _15438 = NewDouble((double)_15438);
    }
    else {
        _15438 = binary_op(PLUS, _15437, _ch_26441);
    }
    DeRef(_15437);
    _15437 = NOVALUE;
    _15439 = 55;
    DeRef(_d_26449);
    if (IS_ATOM_INT(_15438)) {
        _d_26449 = _15438 - 55;
        if ((long)((unsigned long)_d_26449 +(unsigned long) HIGH_BITS) >= 0){
            _d_26449 = NewDouble((double)_d_26449);
        }
    }
    else {
        _d_26449 = binary_op(MINUS, _15438, 55);
    }
    DeRef(_15438);
    _15438 = NOVALUE;
    _15439 = NOVALUE;
    goto L7D; // [3010] 2924
L80: 

    /** 						elsif ch >= 'a' and ch <= 'f' then*/
    _15441 = (_ch_26441 >= 97);
    if (_15441 == 0) {
        goto L81; // [3019] 3052
    }
    _15443 = (_ch_26441 <= 102);
    if (_15443 == 0)
    {
        DeRef(_15443);
        _15443 = NOVALUE;
        goto L81; // [3028] 3052
    }
    else{
        DeRef(_15443);
        _15443 = NOVALUE;
    }

    /** 							d = (d * 16) + ch - ('a'-10)*/
    if (IS_ATOM_INT(_d_26449)) {
        if (_d_26449 == (short)_d_26449)
        _15444 = _d_26449 * 16;
        else
        _15444 = NewDouble(_d_26449 * (double)16);
    }
    else {
        _15444 = binary_op(MULTIPLY, _d_26449, 16);
    }
    if (IS_ATOM_INT(_15444)) {
        _15445 = _15444 + _ch_26441;
        if ((long)((unsigned long)_15445 + (unsigned long)HIGH_BITS) >= 0) 
        _15445 = NewDouble((double)_15445);
    }
    else {
        _15445 = binary_op(PLUS, _15444, _ch_26441);
    }
    DeRef(_15444);
    _15444 = NOVALUE;
    _15446 = 87;
    DeRef(_d_26449);
    if (IS_ATOM_INT(_15445)) {
        _d_26449 = _15445 - 87;
        if ((long)((unsigned long)_d_26449 +(unsigned long) HIGH_BITS) >= 0){
            _d_26449 = NewDouble((double)_d_26449);
        }
    }
    else {
        _d_26449 = binary_op(MINUS, _15445, 87);
    }
    DeRef(_15445);
    _15445 = NOVALUE;
    _15446 = NOVALUE;
    goto L7D; // [3049] 2924
L81: 

    /** 						elsif ch = '_' then*/
    if (_ch_26441 != 95)
    goto L7E; // [3054] 3071
    goto L7D; // [3058] 2924

    /** 							exit*/
    goto L7E; // [3063] 3071

    /** 					end while*/
    goto L7D; // [3068] 2924
L7E: 
L7C: 

    /** 				if fenv:test(FE_OVERFLOW) then*/
    _15449 = _62test(111);
    if (_15449 == 0) {
        DeRef(_15449);
        _15449 = NOVALUE;
        goto L82; // [3080] 3093
    }
    else {
        if (!IS_ATOM_INT(_15449) && DBL_PTR(_15449)->dbl == 0.0){
            DeRef(_15449);
            _15449 = NOVALUE;
            goto L82; // [3080] 3093
        }
        DeRef(_15449);
        _15449 = NOVALUE;
    }
    DeRef(_15449);
    _15449 = NOVALUE;

    /** 					CompileErr(NUMBER_IS_TOO_BIG)*/
    RefDS(_22682);
    _43CompileErr(357, _22682, 0);
L82: 

    /** 				fenv:clear(FE_OVERFLOW)*/
    _15450 = _62clear(111);

    /** 				ungetch()*/
    _60ungetch();

    /** 				if is_int then*/
    if (_is_int_26452 == 0)
    {
        goto L83; // [3107] 3129
    }
    else{
    }

    /** 					return {ATOM, NewIntSym(i)}*/
    _15451 = _52NewIntSym(_i_26442);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 502;
    ((int *)_2)[2] = _15451;
    _15452 = MAKE_SEQ(_1);
    _15451 = NOVALUE;
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    DeRef(_15391);
    _15391 = NOVALUE;
    _15323 = NOVALUE;
    DeRef(_15441);
    _15441 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15434);
    _15434 = NOVALUE;
    DeRef(_15398);
    _15398 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    _15428 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    _15402 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15408);
    _15408 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    DeRef(_15390);
    _15390 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15395);
    _15395 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15450);
    _15450 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15452;
    goto L1; // [3126] 10
L83: 

    /** 					if d <= MAXINT_DBL then            -- d is always >= 0*/
    if (binary_op_a(GREATER, _d_26449, 1073741823)){
        goto L84; // [3135] 3158
    }

    /** 						return {ATOM, NewIntSym(d)}*/
    Ref(_d_26449);
    _15454 = _52NewIntSym(_d_26449);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 502;
    ((int *)_2)[2] = _15454;
    _15455 = MAKE_SEQ(_1);
    _15454 = NOVALUE;
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    DeRef(_15391);
    _15391 = NOVALUE;
    _15323 = NOVALUE;
    DeRef(_15441);
    _15441 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15434);
    _15434 = NOVALUE;
    DeRef(_15398);
    _15398 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    _15428 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    _15402 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15408);
    _15408 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    DeRef(_15390);
    _15390 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15395);
    _15395 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15450);
    _15450 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15452);
    _15452 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15455;
    goto L1; // [3155] 10
L84: 

    /** 						return {ATOM, NewDoubleSym(d)}*/
    Ref(_d_26449);
    _15456 = _52NewDoubleSym(_d_26449);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 502;
    ((int *)_2)[2] = _15456;
    _15457 = MAKE_SEQ(_1);
    _15456 = NOVALUE;
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    DeRef(_15391);
    _15391 = NOVALUE;
    _15323 = NOVALUE;
    DeRef(_15441);
    _15441 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15434);
    _15434 = NOVALUE;
    DeRef(_15398);
    _15398 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    _15428 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    _15402 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15408);
    _15408 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    DeRef(_15390);
    _15390 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15395);
    _15395 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15450);
    _15450 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    DeRef(_15455);
    _15455 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15452);
    _15452 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15457;
    goto L1; // [3177] 10
L73: 

    /** 		elsif class = res:MULTIPLY then*/
    if (_class_26453 != 13)
    goto L85; // [3182] 3232

    /** 			ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 			if ch = '=' then*/
    if (_ch_26441 != 61)
    goto L86; // [3195] 3214

    /** 				return {MULTIPLY_EQUALS, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 517;
    ((int *)_2)[2] = 0;
    _15461 = MAKE_SEQ(_1);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    DeRef(_15391);
    _15391 = NOVALUE;
    _15323 = NOVALUE;
    DeRef(_15441);
    _15441 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15457);
    _15457 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15434);
    _15434 = NOVALUE;
    DeRef(_15398);
    _15398 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    _15428 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    _15402 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15408);
    _15408 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    DeRef(_15390);
    _15390 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15395);
    _15395 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15450);
    _15450 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    DeRef(_15455);
    _15455 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15452);
    _15452 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15461;
    goto L1; // [3211] 10
L86: 

    /** 				ungetch()*/
    _60ungetch();

    /** 				return {res:MULTIPLY, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 13;
    ((int *)_2)[2] = 0;
    _15462 = MAKE_SEQ(_1);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    DeRef(_15391);
    _15391 = NOVALUE;
    _15323 = NOVALUE;
    DeRef(_15441);
    _15441 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15457);
    _15457 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15434);
    _15434 = NOVALUE;
    DeRef(_15398);
    _15398 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    _15428 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    _15402 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15408);
    _15408 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    DeRef(_15390);
    _15390 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15395);
    _15395 = NOVALUE;
    DeRef(_15461);
    _15461 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15450);
    _15450 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    DeRef(_15455);
    _15455 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15452);
    _15452 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15462;
    goto L1; // [3229] 10
L85: 

    /** 		elsif class = res:DIVIDE then*/
    if (_class_26453 != 14)
    goto L87; // [3234] 3436

    /** 			ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 			if ch = '=' then*/
    if (_ch_26441 != 61)
    goto L88; // [3247] 3266

    /** 				return {DIVIDE_EQUALS, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 518;
    ((int *)_2)[2] = 0;
    _15466 = MAKE_SEQ(_1);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    DeRef(_15391);
    _15391 = NOVALUE;
    _15323 = NOVALUE;
    DeRef(_15441);
    _15441 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15457);
    _15457 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15434);
    _15434 = NOVALUE;
    DeRef(_15398);
    _15398 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    _15428 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    _15402 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15408);
    _15408 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15462);
    _15462 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    DeRef(_15390);
    _15390 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15395);
    _15395 = NOVALUE;
    DeRef(_15461);
    _15461 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15450);
    _15450 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    DeRef(_15455);
    _15455 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15452);
    _15452 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15466;
    goto L1; // [3263] 10
L88: 

    /** 			elsif ch = '*' then*/
    if (_ch_26441 != 42)
    goto L89; // [3268] 3418

    /** 				cline = line_number*/
    _cline_26446 = _25line_number_12263;

    /** 				integer cnest = 1*/
    _cnest_27118 = 1;

    /** 				while cnest > 0 do*/
L8A: 
    if (_cnest_27118 <= 0)
    goto L8B; // [3291] 3399

    /** 					ch = getch()*/
    _ch_26441 = _60getch();
    if (!IS_ATOM_INT(_ch_26441)) {
        _1 = (long)(DBL_PTR(_ch_26441)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26441);
        _ch_26441 = _1;
    }

    /** 					switch ch do*/
    _0 = _ch_26441;
    switch ( _0 ){ 

        /** 						case  END_OF_FILE_CHAR then*/
        case 26:

        /** 							exit*/
        goto L8B; // [3315] 3399
        goto L8A; // [3317] 3291

        /** 						case '\n' then*/
        case 10:

        /** 							read_line()*/
        _60read_line();
        goto L8A; // [3327] 3291

        /** 						case '*' then*/
        case 42:

        /** 							ch = getch()*/
        _ch_26441 = _60getch();
        if (!IS_ATOM_INT(_ch_26441)) {
            _1 = (long)(DBL_PTR(_ch_26441)->dbl);
            if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_ch_26441);
            _ch_26441 = _1;
        }

        /** 							if ch = '/' then*/
        if (_ch_26441 != 47)
        goto L8C; // [3342] 3355

        /** 								cnest -= 1*/
        _cnest_27118 = _cnest_27118 - 1;
        goto L8A; // [3352] 3291
L8C: 

        /** 								ungetch()*/
        _60ungetch();
        goto L8A; // [3360] 3291

        /** 						case '/' then*/
        case 47:

        /** 							ch = getch()*/
        _ch_26441 = _60getch();
        if (!IS_ATOM_INT(_ch_26441)) {
            _1 = (long)(DBL_PTR(_ch_26441)->dbl);
            if (UNIQUE(DBL_PTR(_ch_26441)) && (DBL_PTR(_ch_26441)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_ch_26441);
            _ch_26441 = _1;
        }

        /** 							if ch = '*' then*/
        if (_ch_26441 != 42)
        goto L8D; // [3375] 3388

        /** 								cnest += 1*/
        _cnest_27118 = _cnest_27118 + 1;
        goto L8E; // [3385] 3393
L8D: 

        /** 								ungetch()*/
        _60ungetch();
L8E: 
    ;}
    /** 				end while*/
    goto L8A; // [3396] 3291
L8B: 

    /** 				if cnest > 0 then*/
    if (_cnest_27118 <= 0)
    goto L8F; // [3401] 3413

    /** 					CompileErr(42, cline)*/
    _43CompileErr(42, _cline_26446, 0);
L8F: 
    goto L1; // [3415] 10
L89: 

    /** 				ungetch()*/
    _60ungetch();

    /** 				return {res:DIVIDE, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 14;
    ((int *)_2)[2] = 0;
    _15479 = MAKE_SEQ(_1);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    DeRef(_15391);
    _15391 = NOVALUE;
    _15323 = NOVALUE;
    DeRef(_15441);
    _15441 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15457);
    _15457 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15434);
    _15434 = NOVALUE;
    DeRef(_15398);
    _15398 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    _15428 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    _15402 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15408);
    _15408 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15462);
    _15462 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    DeRef(_15390);
    _15390 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15395);
    _15395 = NOVALUE;
    DeRef(_15461);
    _15461 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15450);
    _15450 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    DeRef(_15455);
    _15455 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15452);
    _15452 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15466);
    _15466 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15479;
    goto L1; // [3433] 10
L87: 

    /** 		elsif class = SINGLE_QUOTE then*/
    if (_class_26453 != -5)
    goto L90; // [3440] 3581

    /** 			atom ach = getch()*/
    _0 = _ach_27147;
    _ach_27147 = _60getch();
    DeRef(_0);

    /** 			if ach = '\\' then*/
    if (binary_op_a(NOTEQ, _ach_27147, 92)){
        goto L91; // [3451] 3464
    }

    /** 				ach = EscapeChar('\'')*/
    _0 = _ach_27147;
    _ach_27147 = _60EscapeChar(39);
    DeRef(_0);
    goto L92; // [3461] 3515
L91: 

    /** 			elsif ach = '\t' then*/
    if (binary_op_a(NOTEQ, _ach_27147, 9)){
        goto L93; // [3466] 3480
    }

    /** 				CompileErr(145)*/
    RefDS(_22682);
    _43CompileErr(145, _22682, 0);
    goto L92; // [3477] 3515
L93: 

    /** 			elsif ach = '\'' then*/
    if (binary_op_a(NOTEQ, _ach_27147, 39)){
        goto L94; // [3482] 3496
    }

    /** 				CompileErr(137)*/
    RefDS(_22682);
    _43CompileErr(137, _22682, 0);
    goto L92; // [3493] 3515
L94: 

    /** 			elsif ach = '\n' then*/
    if (binary_op_a(NOTEQ, _ach_27147, 10)){
        goto L95; // [3498] 3514
    }

    /** 				CompileErr(68, {"character", "end of line"})*/
    RefDS(_15488);
    RefDS(_15487);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _15487;
    ((int *)_2)[2] = _15488;
    _15489 = MAKE_SEQ(_1);
    _43CompileErr(68, _15489, 0);
    _15489 = NOVALUE;
L95: 
L92: 

    /** 			if getch() != '\'' then*/
    _15490 = _60getch();
    if (binary_op_a(EQUALS, _15490, 39)){
        DeRef(_15490);
        _15490 = NOVALUE;
        goto L96; // [3520] 3532
    }
    DeRef(_15490);
    _15490 = NOVALUE;

    /** 				CompileErr(56)*/
    RefDS(_22682);
    _43CompileErr(56, _22682, 0);
L96: 

    /** 			if integer(ach) then*/
    if (IS_ATOM_INT(_ach_27147))
    _15492 = 1;
    else if (IS_ATOM_DBL(_ach_27147))
    _15492 = IS_ATOM_INT(DoubleToInt(_ach_27147));
    else
    _15492 = 0;
    if (_15492 == 0)
    {
        _15492 = NOVALUE;
        goto L97; // [3537] 3559
    }
    else{
        _15492 = NOVALUE;
    }

    /** 				return {ATOM, NewIntSym(ach)}*/
    Ref(_ach_27147);
    _15493 = _52NewIntSym(_ach_27147);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 502;
    ((int *)_2)[2] = _15493;
    _15494 = MAKE_SEQ(_1);
    _15493 = NOVALUE;
    DeRef(_ach_27147);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    DeRef(_15391);
    _15391 = NOVALUE;
    _15323 = NOVALUE;
    DeRef(_15441);
    _15441 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15457);
    _15457 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15434);
    _15434 = NOVALUE;
    DeRef(_15398);
    _15398 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    _15428 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    _15402 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15408);
    _15408 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15462);
    _15462 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    DeRef(_15390);
    _15390 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15395);
    _15395 = NOVALUE;
    DeRef(_15461);
    _15461 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15450);
    _15450 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    DeRef(_15455);
    _15455 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15479);
    _15479 = NOVALUE;
    DeRef(_15452);
    _15452 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15466);
    _15466 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15494;
    goto L98; // [3556] 3576
L97: 

    /** 				return {ATOM, NewDoubleSym(ach)}*/
    Ref(_ach_27147);
    _15495 = _52NewDoubleSym(_ach_27147);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 502;
    ((int *)_2)[2] = _15495;
    _15496 = MAKE_SEQ(_1);
    _15495 = NOVALUE;
    DeRef(_ach_27147);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    DeRef(_15391);
    _15391 = NOVALUE;
    _15323 = NOVALUE;
    DeRef(_15441);
    _15441 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15457);
    _15457 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15434);
    _15434 = NOVALUE;
    DeRef(_15398);
    _15398 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    _15428 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    _15402 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15408);
    _15408 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15494);
    _15494 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15462);
    _15462 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    DeRef(_15390);
    _15390 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15395);
    _15395 = NOVALUE;
    DeRef(_15461);
    _15461 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15450);
    _15450 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    DeRef(_15455);
    _15455 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15479);
    _15479 = NOVALUE;
    DeRef(_15452);
    _15452 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15466);
    _15466 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15496;
L98: 
    DeRef(_ach_27147);
    _ach_27147 = NOVALUE;
    goto L1; // [3578] 10
L90: 

    /** 		elsif class = LESS then*/
    if (_class_26453 != 1)
    goto L99; // [3585] 3633

    /** 			if getch() = '=' then*/
    _15498 = _60getch();
    if (binary_op_a(NOTEQ, _15498, 61)){
        DeRef(_15498);
        _15498 = NOVALUE;
        goto L9A; // [3594] 3613
    }
    DeRef(_15498);
    _15498 = NOVALUE;

    /** 				return {LESSEQ, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 5;
    ((int *)_2)[2] = 0;
    _15500 = MAKE_SEQ(_1);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    DeRef(_15391);
    _15391 = NOVALUE;
    _15323 = NOVALUE;
    DeRef(_15441);
    _15441 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15457);
    _15457 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15434);
    _15434 = NOVALUE;
    DeRef(_15398);
    _15398 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    _15428 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    _15402 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15408);
    _15408 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15494);
    _15494 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15462);
    _15462 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    DeRef(_15390);
    _15390 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15395);
    _15395 = NOVALUE;
    DeRef(_15461);
    _15461 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15450);
    _15450 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    DeRef(_15455);
    _15455 = NOVALUE;
    DeRef(_15496);
    _15496 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15479);
    _15479 = NOVALUE;
    DeRef(_15452);
    _15452 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15466);
    _15466 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15500;
    goto L1; // [3610] 10
L9A: 

    /** 				ungetch()*/
    _60ungetch();

    /** 				return {LESS, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _15501 = MAKE_SEQ(_1);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    DeRef(_15391);
    _15391 = NOVALUE;
    _15323 = NOVALUE;
    DeRef(_15441);
    _15441 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15457);
    _15457 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15434);
    _15434 = NOVALUE;
    DeRef(_15398);
    _15398 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    _15428 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    _15402 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15408);
    _15408 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15494);
    _15494 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15462);
    _15462 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    DeRef(_15390);
    _15390 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15395);
    _15395 = NOVALUE;
    DeRef(_15461);
    _15461 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15450);
    _15450 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    DeRef(_15455);
    _15455 = NOVALUE;
    DeRef(_15496);
    _15496 = NOVALUE;
    DeRef(_15500);
    _15500 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15479);
    _15479 = NOVALUE;
    DeRef(_15452);
    _15452 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15466);
    _15466 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15501;
    goto L1; // [3630] 10
L99: 

    /** 		elsif class = GREATER then*/
    if (_class_26453 != 6)
    goto L9B; // [3637] 3685

    /** 			if getch() = '=' then*/
    _15503 = _60getch();
    if (binary_op_a(NOTEQ, _15503, 61)){
        DeRef(_15503);
        _15503 = NOVALUE;
        goto L9C; // [3646] 3665
    }
    DeRef(_15503);
    _15503 = NOVALUE;

    /** 				return {GREATEREQ, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 2;
    ((int *)_2)[2] = 0;
    _15505 = MAKE_SEQ(_1);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    DeRef(_15391);
    _15391 = NOVALUE;
    _15323 = NOVALUE;
    DeRef(_15441);
    _15441 = NOVALUE;
    DeRef(_15501);
    _15501 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15457);
    _15457 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15434);
    _15434 = NOVALUE;
    DeRef(_15398);
    _15398 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    _15428 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    _15402 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15408);
    _15408 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15494);
    _15494 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15462);
    _15462 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    DeRef(_15390);
    _15390 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15395);
    _15395 = NOVALUE;
    DeRef(_15461);
    _15461 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15450);
    _15450 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    DeRef(_15455);
    _15455 = NOVALUE;
    DeRef(_15496);
    _15496 = NOVALUE;
    DeRef(_15500);
    _15500 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15479);
    _15479 = NOVALUE;
    DeRef(_15452);
    _15452 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15466);
    _15466 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15505;
    goto L1; // [3662] 10
L9C: 

    /** 				ungetch()*/
    _60ungetch();

    /** 				return {GREATER, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 6;
    ((int *)_2)[2] = 0;
    _15506 = MAKE_SEQ(_1);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    DeRef(_15391);
    _15391 = NOVALUE;
    _15323 = NOVALUE;
    DeRef(_15441);
    _15441 = NOVALUE;
    DeRef(_15501);
    _15501 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15457);
    _15457 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15434);
    _15434 = NOVALUE;
    DeRef(_15398);
    _15398 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    _15428 = NOVALUE;
    DeRef(_15505);
    _15505 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    _15402 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15408);
    _15408 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15494);
    _15494 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15462);
    _15462 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    DeRef(_15390);
    _15390 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15395);
    _15395 = NOVALUE;
    DeRef(_15461);
    _15461 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15450);
    _15450 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    DeRef(_15455);
    _15455 = NOVALUE;
    DeRef(_15496);
    _15496 = NOVALUE;
    DeRef(_15500);
    _15500 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15479);
    _15479 = NOVALUE;
    DeRef(_15452);
    _15452 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15466);
    _15466 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15506;
    goto L1; // [3682] 10
L9B: 

    /** 		elsif class = BANG then*/
    if (_class_26453 != -1)
    goto L9D; // [3689] 3737

    /** 			if getch() = '=' then*/
    _15508 = _60getch();
    if (binary_op_a(NOTEQ, _15508, 61)){
        DeRef(_15508);
        _15508 = NOVALUE;
        goto L9E; // [3698] 3717
    }
    DeRef(_15508);
    _15508 = NOVALUE;

    /** 				return {NOTEQ, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 4;
    ((int *)_2)[2] = 0;
    _15510 = MAKE_SEQ(_1);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    DeRef(_15391);
    _15391 = NOVALUE;
    _15323 = NOVALUE;
    DeRef(_15441);
    _15441 = NOVALUE;
    DeRef(_15501);
    _15501 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15457);
    _15457 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15434);
    _15434 = NOVALUE;
    DeRef(_15398);
    _15398 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    _15428 = NOVALUE;
    DeRef(_15505);
    _15505 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    _15402 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15408);
    _15408 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15494);
    _15494 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15462);
    _15462 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    DeRef(_15390);
    _15390 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15395);
    _15395 = NOVALUE;
    DeRef(_15461);
    _15461 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15450);
    _15450 = NOVALUE;
    DeRef(_15506);
    _15506 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    DeRef(_15455);
    _15455 = NOVALUE;
    DeRef(_15496);
    _15496 = NOVALUE;
    DeRef(_15500);
    _15500 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15479);
    _15479 = NOVALUE;
    DeRef(_15452);
    _15452 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15466);
    _15466 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15510;
    goto L1; // [3714] 10
L9E: 

    /** 				ungetch()*/
    _60ungetch();

    /** 				return {BANG, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = 0;
    _15511 = MAKE_SEQ(_1);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    DeRef(_15391);
    _15391 = NOVALUE;
    _15323 = NOVALUE;
    DeRef(_15441);
    _15441 = NOVALUE;
    DeRef(_15501);
    _15501 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15457);
    _15457 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15434);
    _15434 = NOVALUE;
    DeRef(_15398);
    _15398 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    _15428 = NOVALUE;
    DeRef(_15505);
    _15505 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    _15402 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15408);
    _15408 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15494);
    _15494 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15462);
    _15462 = NOVALUE;
    DeRef(_15510);
    _15510 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    DeRef(_15390);
    _15390 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15395);
    _15395 = NOVALUE;
    DeRef(_15461);
    _15461 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15450);
    _15450 = NOVALUE;
    DeRef(_15506);
    _15506 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    DeRef(_15455);
    _15455 = NOVALUE;
    DeRef(_15496);
    _15496 = NOVALUE;
    DeRef(_15500);
    _15500 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15479);
    _15479 = NOVALUE;
    DeRef(_15452);
    _15452 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15466);
    _15466 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15511;
    goto L1; // [3734] 10
L9D: 

    /** 		elsif class = KEYWORD then*/
    if (_class_26453 != -10)
    goto L9F; // [3741] 3774

    /** 			return {keylist[ch - KEYWORD_BASE][K_TOKEN], 0}*/
    _15513 = _ch_26441 - 128;
    _2 = (int)SEQ_PTR(_63keylist_23599);
    _15514 = (int)*(((s1_ptr)_2)->base + _15513);
    _2 = (int)SEQ_PTR(_15514);
    _15515 = (int)*(((s1_ptr)_2)->base + 3);
    _15514 = NOVALUE;
    Ref(_15515);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _15515;
    ((int *)_2)[2] = 0;
    _15516 = MAKE_SEQ(_1);
    _15515 = NOVALUE;
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    DeRef(_15391);
    _15391 = NOVALUE;
    _15323 = NOVALUE;
    DeRef(_15441);
    _15441 = NOVALUE;
    DeRef(_15501);
    _15501 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15457);
    _15457 = NOVALUE;
    DeRef(_15511);
    _15511 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15434);
    _15434 = NOVALUE;
    DeRef(_15398);
    _15398 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    _15428 = NOVALUE;
    DeRef(_15505);
    _15505 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    _15402 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15408);
    _15408 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15494);
    _15494 = NOVALUE;
    _15513 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15462);
    _15462 = NOVALUE;
    DeRef(_15510);
    _15510 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    DeRef(_15390);
    _15390 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15395);
    _15395 = NOVALUE;
    DeRef(_15461);
    _15461 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15450);
    _15450 = NOVALUE;
    DeRef(_15506);
    _15506 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    DeRef(_15455);
    _15455 = NOVALUE;
    DeRef(_15496);
    _15496 = NOVALUE;
    DeRef(_15500);
    _15500 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15479);
    _15479 = NOVALUE;
    DeRef(_15452);
    _15452 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15466);
    _15466 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15516;
    goto L1; // [3771] 10
L9F: 

    /** 		elsif class = BUILTIN then*/
    if (_class_26453 != -9)
    goto LA0; // [3778] 3831

    /** 			name = keylist[ch - BUILTIN_BASE + NUM_KEYWORDS][K_NAME]*/
    _15518 = _ch_26441 - 170;
    if ((long)((unsigned long)_15518 +(unsigned long) HIGH_BITS) >= 0){
        _15518 = NewDouble((double)_15518);
    }
    if (IS_ATOM_INT(_15518)) {
        _15519 = _15518 + 24;
    }
    else {
        _15519 = NewDouble(DBL_PTR(_15518)->dbl + (double)24);
    }
    DeRef(_15518);
    _15518 = NOVALUE;
    _2 = (int)SEQ_PTR(_63keylist_23599);
    if (!IS_ATOM_INT(_15519)){
        _15520 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_15519)->dbl));
    }
    else{
        _15520 = (int)*(((s1_ptr)_2)->base + _15519);
    }
    DeRef(_name_26454);
    _2 = (int)SEQ_PTR(_15520);
    _name_26454 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_name_26454);
    _15520 = NOVALUE;

    /** 			return keyfind(name, -1)*/
    RefDS(_name_26454);
    _32391 = _52hashfn(_name_26454);
    RefDS(_name_26454);
    _15522 = _52keyfind(_name_26454, -1, _25current_file_no_12262, 0, _32391);
    _32391 = NOVALUE;
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRefDS(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    DeRef(_15391);
    _15391 = NOVALUE;
    _15323 = NOVALUE;
    DeRef(_15441);
    _15441 = NOVALUE;
    DeRef(_15501);
    _15501 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15457);
    _15457 = NOVALUE;
    DeRef(_15511);
    _15511 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15434);
    _15434 = NOVALUE;
    DeRef(_15398);
    _15398 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    _15428 = NOVALUE;
    DeRef(_15505);
    _15505 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    _15402 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15408);
    _15408 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15494);
    _15494 = NOVALUE;
    DeRef(_15513);
    _15513 = NOVALUE;
    DeRef(_15516);
    _15516 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15462);
    _15462 = NOVALUE;
    DeRef(_15519);
    _15519 = NOVALUE;
    DeRef(_15510);
    _15510 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    DeRef(_15390);
    _15390 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15395);
    _15395 = NOVALUE;
    DeRef(_15461);
    _15461 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15450);
    _15450 = NOVALUE;
    DeRef(_15506);
    _15506 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    DeRef(_15455);
    _15455 = NOVALUE;
    DeRef(_15496);
    _15496 = NOVALUE;
    DeRef(_15500);
    _15500 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15479);
    _15479 = NOVALUE;
    DeRef(_15452);
    _15452 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15466);
    _15466 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15522;
    goto L1; // [3828] 10
LA0: 

    /** 		elsif class = BACK_QUOTE then*/
    if (_class_26453 != -12)
    goto LA1; // [3835] 3852

    /** 			return ExtendedString( '`' )*/
    _15524 = _60ExtendedString(96);
    DeRef(_yytext_26447);
    DeRef(_namespaces_26448);
    DeRef(_d_26449);
    DeRef(_tok_26451);
    DeRef(_name_26454);
    DeRef(_15386);
    _15386 = NOVALUE;
    DeRef(_15391);
    _15391 = NOVALUE;
    _15323 = NOVALUE;
    DeRef(_15441);
    _15441 = NOVALUE;
    DeRef(_15501);
    _15501 = NOVALUE;
    DeRef(_15522);
    _15522 = NOVALUE;
    _15176 = NOVALUE;
    DeRef(_15317);
    _15317 = NOVALUE;
    DeRef(_15356);
    _15356 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15157);
    _15157 = NOVALUE;
    DeRef(_15365);
    _15365 = NOVALUE;
    DeRef(_15457);
    _15457 = NOVALUE;
    DeRef(_15511);
    _15511 = NOVALUE;
    DeRef(_15267);
    _15267 = NOVALUE;
    DeRef(_15434);
    _15434 = NOVALUE;
    DeRef(_15398);
    _15398 = NOVALUE;
    DeRef(_15211);
    _15211 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    _15428 = NOVALUE;
    DeRef(_15505);
    _15505 = NOVALUE;
    _15227 = NOVALUE;
    DeRef(_15358);
    _15358 = NOVALUE;
    _15402 = NOVALUE;
    DeRef(_15148);
    _15148 = NOVALUE;
    DeRef(_15154);
    _15154 = NOVALUE;
    _15212 = NOVALUE;
    DeRef(_15260);
    _15260 = NOVALUE;
    DeRef(_15408);
    _15408 = NOVALUE;
    DeRef(_15139);
    _15139 = NOVALUE;
    DeRef(_15185);
    _15185 = NOVALUE;
    DeRef(_15309);
    _15309 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    DeRef(_15382);
    _15382 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    DeRef(_15256);
    _15256 = NOVALUE;
    DeRef(_15494);
    _15494 = NOVALUE;
    DeRef(_15513);
    _15513 = NOVALUE;
    DeRef(_15516);
    _15516 = NOVALUE;
    DeRef(_15151);
    _15151 = NOVALUE;
    DeRef(_15180);
    _15180 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15462);
    _15462 = NOVALUE;
    DeRef(_15519);
    _15519 = NOVALUE;
    DeRef(_15510);
    _15510 = NOVALUE;
    DeRef(_15340);
    _15340 = NOVALUE;
    DeRef(_15390);
    _15390 = NOVALUE;
    _15327 = NOVALUE;
    DeRef(_15334);
    _15334 = NOVALUE;
    DeRef(_15164);
    _15164 = NOVALUE;
    DeRef(_15395);
    _15395 = NOVALUE;
    DeRef(_15461);
    _15461 = NOVALUE;
    DeRef(_15134);
    _15134 = NOVALUE;
    _15270 = NOVALUE;
    DeRef(_15450);
    _15450 = NOVALUE;
    DeRef(_15506);
    _15506 = NOVALUE;
    DeRef(_15261);
    _15261 = NOVALUE;
    DeRef(_15332);
    _15332 = NOVALUE;
    DeRef(_15455);
    _15455 = NOVALUE;
    DeRef(_15496);
    _15496 = NOVALUE;
    DeRef(_15500);
    _15500 = NOVALUE;
    _15201 = NOVALUE;
    DeRef(_15282);
    _15282 = NOVALUE;
    _15305 = NOVALUE;
    DeRef(_15479);
    _15479 = NOVALUE;
    DeRef(_15452);
    _15452 = NOVALUE;
    DeRef(_15348);
    _15348 = NOVALUE;
    DeRef(_15466);
    _15466 = NOVALUE;
    DeRef(_15160);
    _15160 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15190 = NOVALUE;
    _15280 = NOVALUE;
    _15301 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    return _15524;
    goto L1; // [3849] 10
LA1: 

    /** 			InternalErr(268, {class})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _class_26453;
    _15525 = MAKE_SEQ(_1);
    _43InternalErr(268, _15525);
    _15525 = NOVALUE;

    /**    end while*/
    goto L1; // [3865] 10
L2: 
    ;
}


void _60eu_namespace()
{
    int _eu_tok_27244 = NOVALUE;
    int _eu_ns_27246 = NOVALUE;
    int _32390 = NOVALUE;
    int _32389 = NOVALUE;
    int _15534 = NOVALUE;
    int _15532 = NOVALUE;
    int _15530 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	eu_tok = keyfind("eu", -1, , 1)*/
    RefDS(_15528);
    _32389 = _15528;
    _32390 = _52hashfn(_32389);
    _32389 = NOVALUE;
    RefDS(_15528);
    _0 = _eu_tok_27244;
    _eu_tok_27244 = _52keyfind(_15528, -1, _25current_file_no_12262, 1, _32390);
    DeRef(_0);
    _32390 = NOVALUE;

    /** 	eu_ns  = NameSpace_declaration(eu_tok[T_SYM])*/
    _2 = (int)SEQ_PTR(_eu_tok_27244);
    _15530 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_15530);
    _eu_ns_27246 = _60NameSpace_declaration(_15530);
    _15530 = NOVALUE;
    if (!IS_ATOM_INT(_eu_ns_27246)) {
        _1 = (long)(DBL_PTR(_eu_ns_27246)->dbl);
        if (UNIQUE(DBL_PTR(_eu_ns_27246)) && (DBL_PTR(_eu_ns_27246)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_eu_ns_27246);
        _eu_ns_27246 = _1;
    }

    /** 	SymTab[eu_ns][S_OBJ] = 0*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_eu_ns_27246 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _15532 = NOVALUE;

    /** 	SymTab[eu_ns][S_SCOPE] = SC_GLOBAL*/
    _2 = (int)SEQ_PTR(_26SymTab_11138);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26SymTab_11138 = MAKE_SEQ(_2);
    }
    _3 = (int)(_eu_ns_27246 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 6;
    DeRef(_1);
    _15534 = NOVALUE;

    /** end procedure*/
    DeRef(_eu_tok_27244);
    return;
    ;
}


int _60StringToken(int _pDelims_27264)
{
    int _ch_27265 = NOVALUE;
    int _m_27266 = NOVALUE;
    int _gtext_27267 = NOVALUE;
    int _level_27298 = NOVALUE;
    int _15573 = NOVALUE;
    int _15571 = NOVALUE;
    int _15569 = NOVALUE;
    int _15550 = NOVALUE;
    int _15549 = NOVALUE;
    int _15543 = NOVALUE;
    int _15541 = NOVALUE;
    int _15539 = NOVALUE;
    int _15537 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ch = getch()*/
    _ch_27265 = _60getch();
    if (!IS_ATOM_INT(_ch_27265)) {
        _1 = (long)(DBL_PTR(_ch_27265)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27265)) && (DBL_PTR(_ch_27265)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27265);
        _ch_27265 = _1;
    }

    /** 	while ch = ' ' or ch = '\t' do*/
L1: 
    _15537 = (_ch_27265 == 32);
    if (_15537 != 0) {
        goto L2; // [19] 32
    }
    _15539 = (_ch_27265 == 9);
    if (_15539 == 0)
    {
        DeRef(_15539);
        _15539 = NOVALUE;
        goto L3; // [28] 44
    }
    else{
        DeRef(_15539);
        _15539 = NOVALUE;
    }
L2: 

    /** 		ch = getch()*/
    _ch_27265 = _60getch();
    if (!IS_ATOM_INT(_ch_27265)) {
        _1 = (long)(DBL_PTR(_ch_27265)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27265)) && (DBL_PTR(_ch_27265)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27265);
        _ch_27265 = _1;
    }

    /** 	end while*/
    goto L1; // [41] 15
L3: 

    /** 	pDelims &= {' ', '\t', '\n', '\r', END_OF_FILE_CHAR}*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 32;
    *((int *)(_2+8)) = 9;
    *((int *)(_2+12)) = 10;
    *((int *)(_2+16)) = 13;
    *((int *)(_2+20)) = 26;
    _15541 = MAKE_SEQ(_1);
    Concat((object_ptr)&_pDelims_27264, _pDelims_27264, _15541);
    DeRefDS(_15541);
    _15541 = NOVALUE;

    /** 	gtext = ""*/
    RefDS(_5);
    DeRefi(_gtext_27267);
    _gtext_27267 = _5;

    /** 	while not find(ch,  pDelims) label "top" do*/
L4: 
    _15543 = find_from(_ch_27265, _pDelims_27264, 1);
    if (_15543 != 0)
    goto L5; // [77] 391
    _15543 = NOVALUE;

    /** 		if ch = '-' then*/
    if (_ch_27265 != 45)
    goto L6; // [82] 145

    /** 			ch = getch()*/
    _ch_27265 = _60getch();
    if (!IS_ATOM_INT(_ch_27265)) {
        _1 = (long)(DBL_PTR(_ch_27265)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27265)) && (DBL_PTR(_ch_27265)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27265);
        _ch_27265 = _1;
    }

    /** 			if ch = '-' then*/
    if (_ch_27265 != 45)
    goto L7; // [95] 137

    /** 				while not find(ch, {'\n', END_OF_FILE_CHAR}) do*/
L8: 
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 10;
    ((int *)_2)[2] = 26;
    _15549 = MAKE_SEQ(_1);
    _15550 = find_from(_ch_27265, _15549, 1);
    DeRefDS(_15549);
    _15549 = NOVALUE;
    if (_15550 != 0)
    goto L5; // [115] 391
    _15550 = NOVALUE;

    /** 					ch = getch()*/
    _ch_27265 = _60getch();
    if (!IS_ATOM_INT(_ch_27265)) {
        _1 = (long)(DBL_PTR(_ch_27265)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27265)) && (DBL_PTR(_ch_27265)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27265);
        _ch_27265 = _1;
    }

    /** 				end while*/
    goto L8; // [127] 104

    /** 				exit*/
    goto L5; // [132] 391
    goto L9; // [134] 373
L7: 

    /** 				ungetch()*/
    _60ungetch();
    goto L9; // [142] 373
L6: 

    /** 		elsif ch = '/' then*/
    if (_ch_27265 != 47)
    goto LA; // [147] 372

    /** 			ch = getch()*/
    _ch_27265 = _60getch();
    if (!IS_ATOM_INT(_ch_27265)) {
        _1 = (long)(DBL_PTR(_ch_27265)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27265)) && (DBL_PTR(_ch_27265)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27265);
        _ch_27265 = _1;
    }

    /** 			if ch = '*' then*/
    if (_ch_27265 != 42)
    goto LB; // [160] 361

    /** 				integer level = 1*/
    _level_27298 = 1;

    /** 				while level > 0 do*/
LC: 
    if (_level_27298 <= 0)
    goto LD; // [174] 293

    /** 					ch = getch()*/
    _ch_27265 = _60getch();
    if (!IS_ATOM_INT(_ch_27265)) {
        _1 = (long)(DBL_PTR(_ch_27265)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27265)) && (DBL_PTR(_ch_27265)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27265);
        _ch_27265 = _1;
    }

    /** 					if ch = '/' then*/
    if (_ch_27265 != 47)
    goto LE; // [187] 221

    /** 						ch = getch()*/
    _ch_27265 = _60getch();
    if (!IS_ATOM_INT(_ch_27265)) {
        _1 = (long)(DBL_PTR(_ch_27265)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27265)) && (DBL_PTR(_ch_27265)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27265);
        _ch_27265 = _1;
    }

    /** 						if ch = '*' then*/
    if (_ch_27265 != 42)
    goto LF; // [200] 213

    /** 							level += 1*/
    _level_27298 = _level_27298 + 1;
    goto LC; // [210] 174
LF: 

    /** 							ungetch()*/
    _60ungetch();
    goto LC; // [218] 174
LE: 

    /** 					elsif ch = '*' then*/
    if (_ch_27265 != 42)
    goto L10; // [223] 257

    /** 						ch = getch()*/
    _ch_27265 = _60getch();
    if (!IS_ATOM_INT(_ch_27265)) {
        _1 = (long)(DBL_PTR(_ch_27265)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27265)) && (DBL_PTR(_ch_27265)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27265);
        _ch_27265 = _1;
    }

    /** 						if ch = '/' then*/
    if (_ch_27265 != 47)
    goto L11; // [236] 249

    /** 							level -= 1*/
    _level_27298 = _level_27298 - 1;
    goto LC; // [246] 174
L11: 

    /** 							ungetch()*/
    _60ungetch();
    goto LC; // [254] 174
L10: 

    /** 					elsif ch = '\n' then*/
    if (_ch_27265 != 10)
    goto L12; // [259] 270

    /** 						read_line()*/
    _60read_line();
    goto LC; // [267] 174
L12: 

    /** 					elsif ch = END_OF_FILE_CHAR then*/
    if (_ch_27265 != 26)
    goto LC; // [274] 174

    /** 						ungetch()*/
    _60ungetch();

    /** 						exit*/
    goto LD; // [284] 293

    /** 				end while*/
    goto LC; // [290] 174
LD: 

    /** 				ch = getch()*/
    _ch_27265 = _60getch();
    if (!IS_ATOM_INT(_ch_27265)) {
        _1 = (long)(DBL_PTR(_ch_27265)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27265)) && (DBL_PTR(_ch_27265)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27265);
        _ch_27265 = _1;
    }

    /** 				if length(gtext) = 0 then*/
    if (IS_SEQUENCE(_gtext_27267)){
            _15569 = SEQ_PTR(_gtext_27267)->length;
    }
    else {
        _15569 = 1;
    }
    if (_15569 != 0)
    goto L13; // [305] 350

    /** 					while ch = ' ' or ch = '\t' do*/
L14: 
    _15571 = (_ch_27265 == 32);
    if (_15571 != 0) {
        goto L15; // [318] 331
    }
    _15573 = (_ch_27265 == 9);
    if (_15573 == 0)
    {
        DeRef(_15573);
        _15573 = NOVALUE;
        goto L16; // [327] 343
    }
    else{
        DeRef(_15573);
        _15573 = NOVALUE;
    }
L15: 

    /** 						ch = getch()*/
    _ch_27265 = _60getch();
    if (!IS_ATOM_INT(_ch_27265)) {
        _1 = (long)(DBL_PTR(_ch_27265)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27265)) && (DBL_PTR(_ch_27265)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27265);
        _ch_27265 = _1;
    }

    /** 					end while*/
    goto L14; // [340] 314
L16: 

    /** 					continue "top"*/
    goto L4; // [347] 72
L13: 

    /** 				exit*/
    goto L5; // [354] 391
    goto L17; // [358] 371
LB: 

    /** 				ungetch()*/
    _60ungetch();

    /** 				ch = '/'*/
    _ch_27265 = 47;
L17: 
LA: 
L9: 

    /** 		gtext &= ch*/
    Append(&_gtext_27267, _gtext_27267, _ch_27265);

    /** 		ch = getch()*/
    _ch_27265 = _60getch();
    if (!IS_ATOM_INT(_ch_27265)) {
        _1 = (long)(DBL_PTR(_ch_27265)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27265)) && (DBL_PTR(_ch_27265)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27265);
        _ch_27265 = _1;
    }

    /** 	end while*/
    goto L4; // [388] 72
L5: 

    /** 	ungetch() -- put back end-word token.*/
    _60ungetch();

    /** 	return gtext*/
    DeRefDS(_pDelims_27264);
    DeRef(_15537);
    _15537 = NOVALUE;
    DeRef(_15571);
    _15571 = NOVALUE;
    return _gtext_27267;
    ;
}


void _60IncludeScan(int _is_public_27335)
{
    int _ch_27336 = NOVALUE;
    int _gtext_27337 = NOVALUE;
    int _s_27339 = NOVALUE;
    int _32388 = NOVALUE;
    int _15637 = NOVALUE;
    int _15636 = NOVALUE;
    int _15634 = NOVALUE;
    int _15632 = NOVALUE;
    int _15631 = NOVALUE;
    int _15626 = NOVALUE;
    int _15623 = NOVALUE;
    int _15621 = NOVALUE;
    int _15620 = NOVALUE;
    int _15618 = NOVALUE;
    int _15616 = NOVALUE;
    int _15614 = NOVALUE;
    int _15612 = NOVALUE;
    int _15606 = NOVALUE;
    int _15604 = NOVALUE;
    int _15598 = NOVALUE;
    int _15594 = NOVALUE;
    int _15593 = NOVALUE;
    int _15588 = NOVALUE;
    int _15585 = NOVALUE;
    int _15584 = NOVALUE;
    int _15580 = NOVALUE;
    int _15578 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_is_public_27335)) {
        _1 = (long)(DBL_PTR(_is_public_27335)->dbl);
        if (UNIQUE(DBL_PTR(_is_public_27335)) && (DBL_PTR(_is_public_27335)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_is_public_27335);
        _is_public_27335 = _1;
    }

    /** 	ch = getch()*/
    _ch_27336 = _60getch();
    if (!IS_ATOM_INT(_ch_27336)) {
        _1 = (long)(DBL_PTR(_ch_27336)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27336)) && (DBL_PTR(_ch_27336)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27336);
        _ch_27336 = _1;
    }

    /** 	while ch = ' ' or ch = '\t' do*/
L1: 
    _15578 = (_ch_27336 == 32);
    if (_15578 != 0) {
        goto L2; // [19] 32
    }
    _15580 = (_ch_27336 == 9);
    if (_15580 == 0)
    {
        DeRef(_15580);
        _15580 = NOVALUE;
        goto L3; // [28] 44
    }
    else{
        DeRef(_15580);
        _15580 = NOVALUE;
    }
L2: 

    /** 		ch = getch()*/
    _ch_27336 = _60getch();
    if (!IS_ATOM_INT(_ch_27336)) {
        _1 = (long)(DBL_PTR(_ch_27336)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27336)) && (DBL_PTR(_ch_27336)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27336);
        _ch_27336 = _1;
    }

    /** 	end while*/
    goto L1; // [41] 15
L3: 

    /** 	gtext = ""*/
    RefDS(_5);
    DeRef(_gtext_27337);
    _gtext_27337 = _5;

    /** 	if ch = '"' then*/
    if (_ch_27336 != 34)
    goto L4; // [53] 141

    /** 		ch = getch()*/
    _ch_27336 = _60getch();
    if (!IS_ATOM_INT(_ch_27336)) {
        _1 = (long)(DBL_PTR(_ch_27336)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27336)) && (DBL_PTR(_ch_27336)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27336);
        _ch_27336 = _1;
    }

    /** 		while not find(ch, {'\n', '\r', '"', END_OF_FILE_CHAR}) do*/
L5: 
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 10;
    *((int *)(_2+8)) = 13;
    *((int *)(_2+12)) = 34;
    *((int *)(_2+16)) = 26;
    _15584 = MAKE_SEQ(_1);
    _15585 = find_from(_ch_27336, _15584, 1);
    DeRefDS(_15584);
    _15584 = NOVALUE;
    if (_15585 != 0)
    goto L6; // [83] 124
    _15585 = NOVALUE;

    /** 			if ch = '\\' then*/
    if (_ch_27336 != 92)
    goto L7; // [88] 105

    /** 				gtext &= EscapeChar('"')*/
    _15588 = _60EscapeChar(34);
    if (IS_SEQUENCE(_gtext_27337) && IS_ATOM(_15588)) {
        Ref(_15588);
        Append(&_gtext_27337, _gtext_27337, _15588);
    }
    else if (IS_ATOM(_gtext_27337) && IS_SEQUENCE(_15588)) {
    }
    else {
        Concat((object_ptr)&_gtext_27337, _gtext_27337, _15588);
    }
    DeRef(_15588);
    _15588 = NOVALUE;
    goto L8; // [102] 112
L7: 

    /** 				gtext &= ch*/
    Append(&_gtext_27337, _gtext_27337, _ch_27336);
L8: 

    /** 			ch = getch()*/
    _ch_27336 = _60getch();
    if (!IS_ATOM_INT(_ch_27336)) {
        _1 = (long)(DBL_PTR(_ch_27336)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27336)) && (DBL_PTR(_ch_27336)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27336);
        _ch_27336 = _1;
    }

    /** 		end while*/
    goto L5; // [121] 69
L6: 

    /** 		if ch != '"' then*/
    if (_ch_27336 == 34)
    goto L9; // [126] 187

    /** 			CompileErr(115)*/
    RefDS(_22682);
    _43CompileErr(115, _22682, 0);
    goto L9; // [138] 187
L4: 

    /** 		while not find(ch, {' ', '\t', '\n', '\r', END_OF_FILE_CHAR}) do*/
LA: 
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 32;
    *((int *)(_2+8)) = 9;
    *((int *)(_2+12)) = 10;
    *((int *)(_2+16)) = 13;
    *((int *)(_2+20)) = 26;
    _15593 = MAKE_SEQ(_1);
    _15594 = find_from(_ch_27336, _15593, 1);
    DeRefDS(_15593);
    _15593 = NOVALUE;
    if (_15594 != 0)
    goto LB; // [161] 182
    _15594 = NOVALUE;

    /** 			gtext &= ch*/
    Append(&_gtext_27337, _gtext_27337, _ch_27336);

    /** 			ch = getch()*/
    _ch_27336 = _60getch();
    if (!IS_ATOM_INT(_ch_27336)) {
        _1 = (long)(DBL_PTR(_ch_27336)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27336)) && (DBL_PTR(_ch_27336)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27336);
        _ch_27336 = _1;
    }

    /** 		end while*/
    goto LA; // [179] 146
LB: 

    /** 		ungetch()*/
    _60ungetch();
L9: 

    /** 	if length(gtext) = 0 then*/
    if (IS_SEQUENCE(_gtext_27337)){
            _15598 = SEQ_PTR(_gtext_27337)->length;
    }
    else {
        _15598 = 1;
    }
    if (_15598 != 0)
    goto LC; // [192] 204

    /** 		CompileErr(95)*/
    RefDS(_22682);
    _43CompileErr(95, _22682, 0);
LC: 

    /** 	ifdef WINDOWS then*/

    /** 		new_include_name = match_replace(`/`, gtext, `\`)*/
    RefDS(_15600);
    RefDS(_gtext_27337);
    RefDS(_15601);
    _0 = _7match_replace(_15600, _gtext_27337, _15601, 0);
    DeRef(_25new_include_name_12405);
    _25new_include_name_12405 = _0;

    /** 	ch = getch()*/
    _ch_27336 = _60getch();
    if (!IS_ATOM_INT(_ch_27336)) {
        _1 = (long)(DBL_PTR(_ch_27336)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27336)) && (DBL_PTR(_ch_27336)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27336);
        _ch_27336 = _1;
    }

    /** 	while ch = ' ' or ch = '\t' do*/
LD: 
    _15604 = (_ch_27336 == 32);
    if (_15604 != 0) {
        goto LE; // [233] 246
    }
    _15606 = (_ch_27336 == 9);
    if (_15606 == 0)
    {
        DeRef(_15606);
        _15606 = NOVALUE;
        goto LF; // [242] 258
    }
    else{
        DeRef(_15606);
        _15606 = NOVALUE;
    }
LE: 

    /** 		ch = getch()*/
    _ch_27336 = _60getch();
    if (!IS_ATOM_INT(_ch_27336)) {
        _1 = (long)(DBL_PTR(_ch_27336)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27336)) && (DBL_PTR(_ch_27336)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27336);
        _ch_27336 = _1;
    }

    /** 	end while*/
    goto LD; // [255] 229
LF: 

    /** 	new_include_space = 0*/
    _60new_include_space_24668 = 0;

    /** 	if ch = 'a' then*/
    if (_ch_27336 != 97)
    goto L10; // [267] 524

    /** 		ch = getch()*/
    _ch_27336 = _60getch();
    if (!IS_ATOM_INT(_ch_27336)) {
        _1 = (long)(DBL_PTR(_ch_27336)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27336)) && (DBL_PTR(_ch_27336)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27336);
        _ch_27336 = _1;
    }

    /** 		if ch = 's' then*/
    if (_ch_27336 != 115)
    goto L11; // [280] 513

    /** 			ch = getch()*/
    _ch_27336 = _60getch();
    if (!IS_ATOM_INT(_ch_27336)) {
        _1 = (long)(DBL_PTR(_ch_27336)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27336)) && (DBL_PTR(_ch_27336)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27336);
        _ch_27336 = _1;
    }

    /** 			if ch = ' ' or ch = '\t' then*/
    _15612 = (_ch_27336 == 32);
    if (_15612 != 0) {
        goto L12; // [297] 310
    }
    _15614 = (_ch_27336 == 9);
    if (_15614 == 0)
    {
        DeRef(_15614);
        _15614 = NOVALUE;
        goto L13; // [306] 502
    }
    else{
        DeRef(_15614);
        _15614 = NOVALUE;
    }
L12: 

    /** 				ch = getch()*/
    _ch_27336 = _60getch();
    if (!IS_ATOM_INT(_ch_27336)) {
        _1 = (long)(DBL_PTR(_ch_27336)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27336)) && (DBL_PTR(_ch_27336)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27336);
        _ch_27336 = _1;
    }

    /** 				while ch = ' ' or ch = '\t' do*/
L14: 
    _15616 = (_ch_27336 == 32);
    if (_15616 != 0) {
        goto L15; // [326] 339
    }
    _15618 = (_ch_27336 == 9);
    if (_15618 == 0)
    {
        DeRef(_15618);
        _15618 = NOVALUE;
        goto L16; // [335] 351
    }
    else{
        DeRef(_15618);
        _15618 = NOVALUE;
    }
L15: 

    /** 					ch = getch()*/
    _ch_27336 = _60getch();
    if (!IS_ATOM_INT(_ch_27336)) {
        _1 = (long)(DBL_PTR(_ch_27336)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27336)) && (DBL_PTR(_ch_27336)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27336);
        _ch_27336 = _1;
    }

    /** 				end while*/
    goto L14; // [348] 322
L16: 

    /** 				if char_class[ch] = LETTER or ch = '_' then*/
    _2 = (int)SEQ_PTR(_60char_class_24677);
    _15620 = (int)*(((s1_ptr)_2)->base + _ch_27336);
    _15621 = (_15620 == -2);
    _15620 = NOVALUE;
    if (_15621 != 0) {
        goto L17; // [365] 378
    }
    _15623 = (_ch_27336 == 95);
    if (_15623 == 0)
    {
        DeRef(_15623);
        _15623 = NOVALUE;
        goto L18; // [374] 491
    }
    else{
        DeRef(_15623);
        _15623 = NOVALUE;
    }
L17: 

    /** 					gtext = {ch}*/
    _0 = _gtext_27337;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _ch_27336;
    _gtext_27337 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 					ch = getch()*/
    _ch_27336 = _60getch();
    if (!IS_ATOM_INT(_ch_27336)) {
        _1 = (long)(DBL_PTR(_ch_27336)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27336)) && (DBL_PTR(_ch_27336)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27336);
        _ch_27336 = _1;
    }

    /** 					while id_char[ch] = TRUE do*/
L19: 
    _2 = (int)SEQ_PTR(_60id_char_24678);
    _15626 = (int)*(((s1_ptr)_2)->base + _ch_27336);
    if (_15626 != _5TRUE_244)
    goto L1A; // [404] 426

    /** 						gtext &= ch*/
    Append(&_gtext_27337, _gtext_27337, _ch_27336);

    /** 						ch = getch()*/
    _ch_27336 = _60getch();
    if (!IS_ATOM_INT(_ch_27336)) {
        _1 = (long)(DBL_PTR(_ch_27336)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27336)) && (DBL_PTR(_ch_27336)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27336);
        _ch_27336 = _1;
    }

    /** 					end while*/
    goto L19; // [423] 396
L1A: 

    /** 					ungetch()*/
    _60ungetch();

    /** 					s = keyfind(gtext, -1, , 1)*/
    RefDS(_gtext_27337);
    _32388 = _52hashfn(_gtext_27337);
    RefDS(_gtext_27337);
    _0 = _s_27339;
    _s_27339 = _52keyfind(_gtext_27337, -1, _25current_file_no_12262, 1, _32388);
    DeRef(_0);
    _32388 = NOVALUE;

    /** 					if not find(s[T_ID], ID_TOKS) then*/
    _2 = (int)SEQ_PTR(_s_27339);
    _15631 = (int)*(((s1_ptr)_2)->base + 1);
    _15632 = find_from(_15631, _28ID_TOKS_11863, 1);
    _15631 = NOVALUE;
    if (_15632 != 0)
    goto L1B; // [463] 474
    _15632 = NOVALUE;

    /** 						CompileErr(36)*/
    RefDS(_22682);
    _43CompileErr(36, _22682, 0);
L1B: 

    /** 					new_include_space = NameSpace_declaration(s[T_SYM])*/
    _2 = (int)SEQ_PTR(_s_27339);
    _15634 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_15634);
    _0 = _60NameSpace_declaration(_15634);
    _60new_include_space_24668 = _0;
    _15634 = NOVALUE;
    if (!IS_ATOM_INT(_60new_include_space_24668)) {
        _1 = (long)(DBL_PTR(_60new_include_space_24668)->dbl);
        if (UNIQUE(DBL_PTR(_60new_include_space_24668)) && (DBL_PTR(_60new_include_space_24668)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_60new_include_space_24668);
        _60new_include_space_24668 = _1;
    }
    goto L1C; // [488] 633
L18: 

    /** 					CompileErr(113)*/
    RefDS(_22682);
    _43CompileErr(113, _22682, 0);
    goto L1C; // [499] 633
L13: 

    /** 				CompileErr(100)*/
    RefDS(_22682);
    _43CompileErr(100, _22682, 0);
    goto L1C; // [510] 633
L11: 

    /** 			CompileErr(100)*/
    RefDS(_22682);
    _43CompileErr(100, _22682, 0);
    goto L1C; // [521] 633
L10: 

    /** 	elsif find(ch, {'\n', '\r', END_OF_FILE_CHAR}) then*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 10;
    *((int *)(_2+8)) = 13;
    *((int *)(_2+12)) = 26;
    _15636 = MAKE_SEQ(_1);
    _15637 = find_from(_ch_27336, _15636, 1);
    DeRefDS(_15636);
    _15636 = NOVALUE;
    if (_15637 == 0)
    {
        _15637 = NOVALUE;
        goto L1D; // [539] 549
    }
    else{
        _15637 = NOVALUE;
    }

    /** 		ungetch()*/
    _60ungetch();
    goto L1C; // [546] 633
L1D: 

    /** 	elsif ch = '-' then*/
    if (_ch_27336 != 45)
    goto L1E; // [551] 587

    /** 		ch = getch()*/
    _ch_27336 = _60getch();
    if (!IS_ATOM_INT(_ch_27336)) {
        _1 = (long)(DBL_PTR(_ch_27336)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27336)) && (DBL_PTR(_ch_27336)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27336);
        _ch_27336 = _1;
    }

    /** 		if ch != '-' then*/
    if (_ch_27336 == 45)
    goto L1F; // [564] 576

    /** 			CompileErr(100)*/
    RefDS(_22682);
    _43CompileErr(100, _22682, 0);
L1F: 

    /** 		ungetch()*/
    _60ungetch();

    /** 		ungetch()*/
    _60ungetch();
    goto L1C; // [584] 633
L1E: 

    /** 	elsif ch = '/' then*/
    if (_ch_27336 != 47)
    goto L20; // [589] 625

    /** 		ch = getch()*/
    _ch_27336 = _60getch();
    if (!IS_ATOM_INT(_ch_27336)) {
        _1 = (long)(DBL_PTR(_ch_27336)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27336)) && (DBL_PTR(_ch_27336)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27336);
        _ch_27336 = _1;
    }

    /** 		if ch != '*' then*/
    if (_ch_27336 == 42)
    goto L21; // [602] 614

    /** 			CompileErr(100)*/
    RefDS(_22682);
    _43CompileErr(100, _22682, 0);
L21: 

    /** 		ungetch()*/
    _60ungetch();

    /** 		ungetch()*/
    _60ungetch();
    goto L1C; // [622] 633
L20: 

    /** 		CompileErr(100)*/
    RefDS(_22682);
    _43CompileErr(100, _22682, 0);
L1C: 

    /** 	start_include = TRUE -- let scanner know*/
    _60start_include_24670 = _5TRUE_244;

    /** 	public_include = is_public*/
    _60public_include_24673 = _is_public_27335;

    /** end procedure*/
    DeRef(_gtext_27337);
    DeRef(_s_27339);
    DeRef(_15578);
    _15578 = NOVALUE;
    DeRef(_15604);
    _15604 = NOVALUE;
    DeRef(_15612);
    _15612 = NOVALUE;
    DeRef(_15616);
    _15616 = NOVALUE;
    _15626 = NOVALUE;
    DeRef(_15621);
    _15621 = NOVALUE;
    return;
    ;
}


void _60main_file()
{
    int _0, _1, _2;
    

    /** 	ifdef STDDEBUG then*/

    /** 		read_line()*/
    _60read_line();

    /** 		default_namespace( )*/
    _60default_namespace();

    /** end procedure*/
    return;
    ;
}


void _60cleanup_open_includes()
{
    int _15647 = NOVALUE;
    int _15646 = NOVALUE;
    int _15645 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length( IncludeStk ) do*/
    if (IS_SEQUENCE(_60IncludeStk_24679)){
            _15645 = SEQ_PTR(_60IncludeStk_24679)->length;
    }
    else {
        _15645 = 1;
    }
    {
        int _i_27460;
        _i_27460 = 1;
L1: 
        if (_i_27460 > _15645){
            goto L2; // [8] 36
        }

        /** 		close( IncludeStk[i][FILE_PTR] )*/
        _2 = (int)SEQ_PTR(_60IncludeStk_24679);
        _15646 = (int)*(((s1_ptr)_2)->base + _i_27460);
        _2 = (int)SEQ_PTR(_15646);
        _15647 = (int)*(((s1_ptr)_2)->base + 3);
        _15646 = NOVALUE;
        if (IS_ATOM_INT(_15647))
        EClose(_15647);
        else
        EClose((int)DBL_PTR(_15647)->dbl);
        _15647 = NOVALUE;

        /** 	end for*/
        _i_27460 = _i_27460 + 1;
        goto L1; // [31] 15
L2: 
        ;
    }

    /** end procedure*/
    return;
    ;
}



// 0x843C7ADD
