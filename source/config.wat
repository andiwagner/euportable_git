# Configuration for Watcom 
ASSERT=1 
SCP=pscp -C 
SSH=plink -C 
HG=hg 
ARCH=ix86 
EUPHORIA=1 
MANAGED_MEM=1 
DELTREE=del /Q /S 
RM=del /Q 
RMDIR=rmdir /Q/S 
EUDOC=eudoc.exe 
CREOLE=creole.exe 
TRUNKDIR=C:\watcom2_eu_dev\naked\euportable 
BUILDDIR=C:\watcom2_eu_dev\naked\euportable\source\build 
